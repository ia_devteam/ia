﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace jsExtract
{
    class Part
    {
        public int startPos, endPos;
        public string contents;
    }
    class PhpRpl
    {
        public string replace;
        public string value;
        public string origin;
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
