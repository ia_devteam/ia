﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FileOperations;

namespace jsExtract
{
    public partial class Form1 : Form
    {
        List<string> phpFiles = new List<string>();
        List<string> err = new List<string>();
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            phpFiles.Clear();
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                dialog.Description = "Папка с исходниками PHP (внимание, испорчу!)";
                dialog.ShowNewFolderButton = false;
                dialog.RootFolder = Environment.SpecialFolder.MyComputer;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string folder = dialog.SelectedPath;
                    foreach (string fileName in System.IO.Directory.GetFiles(folder, "*.php", System.IO.SearchOption.AllDirectories))
                    {
                        phpFiles.Add(fileName);
                    }
                    if (phpFiles.Count == 0)
                    {
                        MessageBox.Show("В выбранной папке нет подходящих файлов!");
                        return;
                    }
                    textBox1.Text = folder;
                    textBox1.Enabled = false;
                    button1.Enabled = true;
                    button2.Enabled = true;
                    button3.Enabled = true;
                    progressBar1.Maximum = phpFiles.Count;
                }
            }

        }

        /*int countLine(string s, int offset)
        {
            int lines = 0;
            if (s.Length < offset)
                return -1;
            int off = 0;
            while (off < offset && off != -1)
            {
                int off1 = s.IndexOf('\n', off + 1);
                int off2 = s.IndexOf('\r', off + 1);
                int off3 = s.IndexOf('\n', off + 1);
                int off1 = s.IndexOf('\n', off + 1);
                
                lines++;
            }
            if (off == -1)
                return -1;
            return --lines;
        }
        int countColumn(string s, int offset)
        {
            if (s.Length < offset)
                return -1;
            return s.Substring(0, offset - 1).Length - s.Substring(0, offset - 1).LastIndexOf('\n') - 1; 
        }*/

        void extract(string fName, bool add = false)
        {
            AbstractReader sr = new AbstractReader(fName);
            bool gotErr = false;
            string s = sr.getText();
            string js = "";
            int currRep = 0, oldRep = 0;
            if (s.ToLower().Contains("<script"))
            {
                int pos = s.ToLower().IndexOf("<script");
                while (pos > 0)
                {
                    gotErr = false;
                    oldRep = currRep;
                    if (!s.ToLower().Substring(pos, s.IndexOf(">", pos) - pos).Contains("src=") && s.ToLower().IndexOf("<script\"", pos) != pos)
                    {

                        if (s.ToLower().IndexOf("</script>", pos) > 0)
                        {
                            int endpos = s.ToLower().IndexOf("</script>", pos);
                            int startpos = s.IndexOf(">", pos) + 1;
                            if (startpos > endpos)
                            {
                                pos = s.ToLower().IndexOf("<script", pos + 1);
                                continue;
                            }
                            //корректировка всвязи с бредом в виде <![CDATA[ и ]]>
                            int cdata_begin = s.IndexOf("<![CDATA[", startpos);
                            int cdata_end = s.IndexOf("]]>", (cdata_begin > 0)?cdata_begin:0);
                            if (cdata_begin >= startpos && cdata_begin < endpos && cdata_end > startpos && cdata_end < endpos)
                            {
                                startpos = cdata_begin + "<![CDATA[".Length;
                                endpos = cdata_end;
                            }
                            js = s.Substring(startpos, endpos - startpos);
                            bool find = true;
                            Dictionary<string, string> replaced = new Dictionary<string, string>();
                            while (find)
                            {
                                find = false;
                                int ind = js.IndexOf("<?");
                                if (ind > 0 && js.IndexOf("?>", ind) > 0)
                                {
                                    find = true;
                                    replaced.Add("PHPREPL" + (++currRep).ToString(), js.Substring(ind, js.IndexOf("?>", ind) + 2 - ind));
                                    js = js.Substring(0, ind) + "RNTPHPREPL(" + currRep.ToString() + ")" + js.Substring(js.IndexOf("?>", ind) + 2);
                                }
                            }
                            //для того чтобы плагин жаваскрипт правильно рассчитывал строки
                            if (add)
                            {
                                int lTotal = sr.countLineByOffset(startpos);
                                int cTotal = sr.countColumnByOffset(startpos);
                                string toAdd = string.Empty;
                                for (int i = 0; i < lTotal; i++)
                                    toAdd += '\n';
                                for (int i = 0; i < cTotal; i++)
                                    toAdd += ' ';
                                js = toAdd + js;
                            }
                            //
                            
                            //
                            Antlr.Runtime.ANTLRStringStream stream = new Antlr.Runtime.ANTLRStringStream(js);
                            Xebic.Parsers.ES3.ES3Lexer lexer = new Xebic.Parsers.ES3.ES3Lexer(stream);
                            Xebic.Parsers.ES3.ES3Parser parser = new Xebic.Parsers.ES3.ES3Parser(new Antlr.Runtime.CommonTokenStream(lexer));
                            Xebic.Parsers.ES3.ES3Parser.program_return ret = null;
                            
                            try
                            {
                                ret = parser.program();
                                if (parser.NumberOfSyntaxErrors > 0)
                                    gotErr = true;
                            }
                            catch (Exception ex)
                            {
                                gotErr = true;
                                if (!checkBox1.Checked)
                                {
                                    err.Add("File " + fName + "(" + startpos + "," + endpos + ").js" + " " + ex.Message);
                                    label1.Text = "ErrorsCount :" + err.Count;
                                }
                            }
                            if (!gotErr || !checkBox1.Checked)
                            {
                                if (gotErr)
                                    gotErr = gotErr;
                                sr.writeTextToFile(fName + "(" + startpos + "," + endpos + ").js", js);

                                foreach (string key in replaced.Keys)
                                {
                                    sr.writeTextToFile(fName + "(" + startpos + "," + endpos + ").js." + key, replaced[key]);
                                }
                            }
                            else
                                currRep = oldRep;
                        }
                    }
                    pos = s.ToLower().IndexOf("<script", pos + 1);
                }
            }
        }

        void join(string fName)
        {
            List<string> cutJs = new List<string>();
            List<string> phpRepl = new List<string>();
            if (System.IO.File.Exists(fName))
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(fName);
                string path = fileInfo.DirectoryName;
                
                foreach (string file in System.IO.Directory.EnumerateFiles(path))
                {
                    if (file.Contains(fName) && file != fName && file.Contains(").js"))
                    {
                        if (!file.Contains("PHPREPL"))
                            cutJs.Add(file);
                        else
                            phpRepl.Add(file);
                    }
                }
                if (cutJs.Count == 0)
                    return;
            }

            bool sorted;
            do
            {
                sorted = true;
                for (int i = 0; i < cutJs.Count - 1; i++)
                {
                    int num1 = Int32.Parse(cutJs[i].Substring(cutJs[i].IndexOf("(") + 1, cutJs[i].IndexOf(",", cutJs[i].IndexOf("(")) - cutJs[i].IndexOf("(") - 1));
                    int num2 = Int32.Parse(cutJs[i + 1].Substring(cutJs[i + 1].IndexOf("(") + 1, cutJs[i + 1].IndexOf(",", cutJs[i + 1].IndexOf("(")) - cutJs[i + 1].IndexOf("(") - 1));
                    if (num1 < num2)
                    {
                        string temp = cutJs[i];
                        cutJs[i] = cutJs[i + 1];
                        cutJs[i + 1] = temp;
                        sorted = false;
                    }
                }
            } while (!sorted);

            List<PhpRpl> phpRpl = new List<PhpRpl>();
            foreach(string rp in phpRepl)
            {
                string repKey = rp.Substring(rp.IndexOf("PHPREPL"));
                AbstractReader frd = new AbstractReader(rp);
                string repValue = frd.getText().Trim();
                PhpRpl rpl = new PhpRpl();
                rpl.replace = repKey.Replace("PHPREPL", "RNTPHPREPL(") + ")";
                rpl.value = repValue;
                rpl.origin = rp.Substring(0, rp.IndexOf("PHPREPL") - 1);
                phpRpl.Add(rpl);
            }


            List<Part> repl = new List<Part>();
            foreach (string fCut in cutJs)
            {
                Part prt = new Part();
                prt.startPos = Int32.Parse(fCut.Substring(fCut.LastIndexOf("(") + 1, fCut.IndexOf(",", fCut.LastIndexOf("(")) - fCut.LastIndexOf("(") - 1));
                prt.endPos = Int32.Parse(fCut.Substring(fCut.LastIndexOf(",") + 1, fCut.IndexOf(")", fCut.LastIndexOf(",")) - fCut.LastIndexOf(",") - 1));
                AbstractReader frd = new AbstractReader(fCut);
                prt.contents = frd.getText().Trim();
                foreach (PhpRpl rpl in phpRpl)
                    if (fCut == rpl.origin)
                    {
                        if (prt.contents.Contains(rpl.replace))
                        {
                            prt.contents = prt.contents.Replace(rpl.replace, rpl.value);
                        }
                        else
                        {
                            err.Add(rpl.replace + " " + rpl.origin);
                        }

                    }
                repl.Add(prt);
            }
            AbstractReader fr = new AbstractReader(fName);
            string contents = fr.getText();
            foreach (Part r in repl)
            {
                contents = contents.Substring(0, r.startPos) + r.contents + contents.Substring(r.endPos - 1);
            }
            System.IO.File.Copy(fName, fName + ".OLDRNT");
            
            //если была хотя бы одна вставка, вставляем тело датчика в самое начало
            if (cutJs.Count > 0)
	        contents = "<?php echo \"<script>function SensorRnt(a) {var fso = new ActiveXObject(\\\"Scripting.FileSystemObject\\\"); var fh = fso.OpenTextFile(\\\"C:\\\\JSSensor.txt\\\",8,true,0); fh.WriteLine(a); fh.Close();} </script>\"; ?>" + contents;
            
            fr.writeTextToFile(fName, contents);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            foreach (string file in phpFiles)
            {
                extract(file, true);
                progressBar1.Increment(1);
            }
            if (err.Count > 0)
            {
                System.IO.StreamWriter sw = new System.IO.StreamWriter(textBox1.Text + "\\jsError.log");
                foreach (string er in err)
                    sw.WriteLine(er);
                sw.Close();
                MessageBox.Show("Errors were written to " + textBox1.Text + "\\jsError.log");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            err.Clear();
            foreach (string file in phpFiles)
            {
                join(file);
                progressBar1.Increment(1);
            }
            if (err.Count > 0)
            {
                System.IO.StreamWriter sw = new System.IO.StreamWriter(textBox1.Text + "\\jsErrorJoin.log");
                foreach (string er in err)
                    sw.WriteLine(er);
                sw.Close();
                MessageBox.Show("Errors were written to " + textBox1.Text + "\\jsErrorJoin.log");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<string> jsFiles = new List<string>();
            err.Clear();
            foreach (string file in System.IO.Directory.EnumerateFiles(textBox1.Text, "*.js", System.IO.SearchOption.AllDirectories))
                jsFiles.Add(file);
            progressBar1.Value = 0;
            progressBar1.Maximum = jsFiles.Count;
            foreach (string file in jsFiles)
            {
                AbstractReader fr = new AbstractReader(file);
                string js = fr.getText();
                Antlr.Runtime.ANTLRStringStream stream = new Antlr.Runtime.ANTLRStringStream(js);
                Xebic.Parsers.ES3.ES3Lexer lexer = new Xebic.Parsers.ES3.ES3Lexer(stream);
                Xebic.Parsers.ES3.ES3Parser parser = new Xebic.Parsers.ES3.ES3Parser(new Antlr.Runtime.CommonTokenStream(lexer));
                Xebic.Parsers.ES3.ES3Parser.program_return ret = null;

                try
                {
                    ret = parser.program();
                    if (parser.NumberOfSyntaxErrors > 0)
                        err.Add("File " + file);
                }
                catch (Exception ex)
                {
                    err.Add("File " + file + " " + ex.Message);
                }
                progressBar1.Increment(1);
            }
            if (err.Count > 0)
            {
                System.IO.StreamWriter sw = new System.IO.StreamWriter(textBox1.Text + "\\jsErrorAll.log");
                foreach (string er in err)
                    sw.WriteLine(er);
                sw.Close();
                MessageBox.Show("Errors were written to " + textBox1.Text + "\\jsErrorAll.log");
            }
        }

    }
}
