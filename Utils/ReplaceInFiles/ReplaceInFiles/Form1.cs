﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ReplaceInFiles
{
    public partial class Form1 : Form
    {
        Encoding encoding;

        public Form1()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            if (Properties.Settings.Default.InFile)
            {
                radioButton1.Checked = true;
            }
            else
            {
                radioButton2.Checked = true;
            }
            textBox1.Text = Properties.Settings.Default.Source;
            textBox4.Text = Properties.Settings.Default.Destination;
            textBox2.Text = Properties.Settings.Default.ReplaceFrom;
            textBox3.Text = Properties.Settings.Default.ReplaceTo;
            progressBar2.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            if (radioButton1.Checked == true)
            {
                dr = openFileDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                    textBox1.Text = openFileDialog1.FileName;
            }
            else
            {
                dr = folderBrowserDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                    textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
            if (dr != DialogResult.OK)
                return;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == null || textBox1.Text == "")
            {
                MessageBox.Show("Укажите имя источника", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox1.Focus();
                return;
            }
            if (textBox4.Text == null || textBox4.Text == "")
            {
                MessageBox.Show("Укажите имя получателя", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox4.Focus();
                return;
            }
            if (textBox1.Text == textBox4.Text)
            {
                MessageBox.Show("Имя получателя должно отличаться от имени источника", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox4.Focus();
                return;
            }
            if (textBox2.Text == null || textBox2.Text == "")
            {
                MessageBox.Show("Укажите шаблон поиска", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox2.Focus();
                return;
            }

            if (radioButton1.Checked == true)
            {
                Cursor = Cursors.WaitCursor;
                ReplaseInFile(textBox1.Text, textBox4.Text, textBox2.Text, textBox3.Text);
                Cursor = Cursors.Default;
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(textBox1.Text);
                FileInfo[] files = di.GetFiles("*.*", SearchOption.AllDirectories);
                if (files.Length == 0)
                {
                    MessageBox.Show("У источника нет файлов", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    textBox1.Focus();
                    return;
                }
                progressBar2.Minimum = 1;
                // Set Maximum to the total number of files to copy.
                progressBar2.Maximum = (int)files.Length;
                // Set the initial value of the ProgressBar.
                progressBar2.Value = 1;
                progressBar2.Visible = true;
                Cursor = Cursors.WaitCursor;
                for (int i = 0; i < files.Length; i++)
                {
                    ReplaseInFile(files[i].FullName, files[i].FullName.Replace(textBox1.Text, textBox4.Text), textBox2.Text, textBox3.Text);
                    progressBar2.PerformStep();
                }
                Cursor = Cursors.Default;
                progressBar2.Visible = false;
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox4.Text = "";
            Properties.Settings.Default.InFile = radioButton1.Checked;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox4.Text = "";
            Properties.Settings.Default.InFile = radioButton1.Checked;
        }
        public void ReplaseInFile(string fileSource, string fileDestination, string what, string to)
        {
            if (File.Exists(fileDestination))
                File.Delete(fileDestination);
            if (what == null || what == "")
                WriteFile(fileDestination, DefaultDecoding(DefaultEncoding(ReadFile(fileSource))));
            else
            {
                WriteFile(fileDestination, DefaultDecoding(DefaultEncoding(ReadFile(fileSource)).Replace(what, to)));
            }
        }
        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer = null;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;
                buffer = new byte[length];
                if (length != fileStream.Read(buffer, 0, length))
                {
                    MessageBox.Show("Не удалось прочитать файл " + filePath, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }
        public static void WriteFile(string filePath, byte[] buffer)
        {
            if (buffer == null)
                return;
            FileStream fileStream = null;
            try
            {
                if (Directory.Exists(System.IO.Path.GetDirectoryName(filePath)) == false)
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(filePath));
                fileStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Ошибка доступа: " + ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();
            }
        }
        public string DefaultEncoding(byte[] arrayIn)
        {
            if (arrayIn == null)
                return null;
            string txt;
            txt = System.Text.Encoding.Default.GetString(arrayIn);
            encoding = Encoding.Default;
            if (txt.StartsWith("п»ї"))
            {
                txt = System.Text.Encoding.UTF8.GetString(arrayIn);
                encoding = Encoding.UTF8;
            }
            else if (txt.StartsWith("яю"))
            {
                txt = System.Text.Encoding.Unicode.GetString(arrayIn);
                encoding = Encoding.Unicode;
            }

            return txt;
        }
        public byte[] DefaultDecoding(string s)
        {
            if (s == null)
                return null;
            return encoding.GetBytes(s);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            if (radioButton1.Checked == true)
            {
                dr = openFileDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                    textBox4.Text = openFileDialog1.FileName;
            }
            else
            {
                dr = folderBrowserDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                    textBox4.Text = folderBrowserDialog1.SelectedPath;
            }
            if (dr != DialogResult.OK)
                return;

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Source = textBox1.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Destination = textBox4.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ReplaceFrom = textBox2.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ReplaceTo = textBox3.Text;
            Properties.Settings.Default.Save();
        }

    }

}
