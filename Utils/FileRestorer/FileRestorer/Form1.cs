﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using FileOperations;

namespace FileRestorer
{
    public partial class Form1 : Form
    {
        static string clnDir = string.Empty;
        static string dirtyDir = string.Empty;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();
            if (fbd.SelectedPath == "")
                return;

            textBox1.Text = clnDir = fbd.SelectedPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();
            if (fbd.SelectedPath == "")
                return;

            textBox2.Text = dirtyDir = fbd.SelectedPath;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<string> dsp = new List<string>();
            List<string> vcproj = new List<string>();
            List<string> csproj = new List<string>();
            dsp = System.IO.Directory.EnumerateFiles(clnDir, "*.dsp", System.IO.SearchOption.AllDirectories).ToList();
            vcproj = System.IO.Directory.EnumerateFiles(clnDir, "*.vcproj", System.IO.SearchOption.AllDirectories).ToList();
            csproj = System.IO.Directory.EnumerateFiles(clnDir, "*.csproj", System.IO.SearchOption.AllDirectories).ToList();

            List<string> restore = new List<string>();
            foreach (string file in dsp)
            {
                try
                {
                    List<string> toRestore = dspParser(file.Replace(clnDir, dirtyDir));
                    restore.AddRange(toRestore);
                }
                catch
                {
                }
            }
            foreach (string file in vcproj)
            {
                try
                {
                    List<string> toRestore = vcprojParser(file.Replace(clnDir, dirtyDir));
                    restore.AddRange(toRestore);
                }
                catch
                {
                }
            }
            foreach (string file in csproj)
            {
                try
                {
                    List<string> toRestore = csprojParser(file.Replace(clnDir, dirtyDir));
                    restore.AddRange(toRestore);
                }
                catch
                {
                }
            }

            int good = 0;
            foreach (string file in restore)
            {
                if (!System.IO.File.Exists(file.Replace(dirtyDir, clnDir)))
                {
                    System.IO.File.Copy(file, file.Replace(dirtyDir, clnDir));
                    good++;
                }
            }
            MessageBox.Show("Восстановлено " + good.ToString() + " файлов!");
        }

        public List<string> dspParser(string project)
        {
            List<string> ret = new List<string>();
            StreamReader sr = new StreamReader(project);
            string folder = project.Substring(0, project.LastIndexOf("\\"));
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                if (line.Trim().StartsWith("SOURCE="))
                {
                    string file = line.Substring(line.IndexOf("=") + 1);
                    try
                    {
                        file = Path.GetFullPath(folder + "\\" + file);
                        if (System.IO.File.Exists(file))
                            ret.Add(file);
                    }
                    catch
                    {
                    }
                }
            }

            return ret;
        }

        public List<string> vcprojParser(string project)
        {
            List<string> ret = new List<string>();
            AbstractReader ar = new AbstractReader(project);
            XDocument doc = XDocument.Parse(ar.getNormalizedText());
            string folder = project.Substring(0, project.LastIndexOf("\\"));
            List<string> found = doc.Descendants("File").Select(attachedFileElement => attachedFileElement.Attribute("RelativePath").Value).ToList();
            foreach(string file in found)
                try
                {
                    if (System.IO.File.Exists(Path.GetFullPath(folder + "\\" + file)))
                        ret.Add(Path.GetFullPath(folder + "\\" + file));
                }
                catch
                {
                }

            return ret;
        }

        public List<string> csprojParser(string project)
        {
            List<string> ret = new List<string>();
            AbstractReader ar = new AbstractReader(project);
            string folder = project.Substring(0, project.LastIndexOf("\\"));
            string contents = ar.getText();
            int lastInd = 0;
            while (contents.IndexOf("<Compile", lastInd) > 0)
            {
                lastInd = contents.IndexOf("<Compile", lastInd) + 1;
                if (contents.Substring(lastInd, contents.IndexOf(">", lastInd)-lastInd + 1).Contains("Include="))
                {
                    string file = contents.Substring(lastInd, contents.IndexOf(">", lastInd)-lastInd + 1);
                    file = file.Substring(file.IndexOf("Include=") + 9);
                    file = file.Substring(0, file.IndexOf("\""));
                    if (System.IO.File.Exists(Path.GetFullPath(folder + "\\" + file)))
                        ret.Add(Path.GetFullPath(folder + "\\" + file));
                }
            }
            while (contents.IndexOf("<Content", lastInd) > 0)
            {
                lastInd = contents.IndexOf("<Content", lastInd) + 1;
                if (contents.Substring(lastInd, contents.IndexOf(">", lastInd) - lastInd + 1).Contains("Include="))
                {
                    string file = contents.Substring(lastInd, contents.IndexOf(">", lastInd) - lastInd + 1);
                    file = file.Substring(file.IndexOf("Include=") + 9);
                    file = file.Substring(0, file.IndexOf("\""));
                    if (System.IO.File.Exists(Path.GetFullPath(folder + "\\" + file)))
                        ret.Add(Path.GetFullPath(folder + "\\" + file));
                }
            }
            return ret;
        }
    }
}
