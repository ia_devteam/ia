﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;

namespace cppcheckreport
{
    public partial class Form1 : Form
    {
        /*
"arrayIndexOutOfBounds"
"arrayIndexThenCheck"
"autovarInvalidDeallocation"
"bufferAccessOutOfBounds"
"catchExceptionByValue"
"charArrayIndex"
"clarifyCalculation"
"clarifyCondition"
"comparisonError"
"comparisonOfBoolWithInt"
"constStatement"
"cppcheckError"
"cstyleCast"
"duplicateBreak"
"duplicateExpression"
"duplicateIf"
"erase"
"exceptRethrowCopy"
"incorrectLogicOperator"
"incorrectStringBooleanError"
"initializerList"
"insecureCmdLineArgs"
"invalidscanf"
"memleak"
"memleakOnRealloc"
"mismatchAllocDealloc"
"noConstructor"
"nullPointer"
"operatorEqRetRefThis"
"operatorEqVarError"
"passedByValue"
"postfixOperator"
"redundantAssignInSwitch"
"resourceLeak"
"selfAssignment"
"stlSize"
"syntaxError"
"terminateStrncpy"
"thisSubtraction"
"unassignedVariable"
"uninitstring"
"uninitvar"
"uninitVar"
"unnecessaryQualification"
"unreachableCode"
"unreadVariable"
"unsignedLessThanZero"
"unsignedPositive"
"unusedAllocatedMemory"
"unusedScopedObject"
"unusedStructMember"
"unusedVariable"
"useAutoPointerArray"
"uselessCallsSubstr"
"variableHidingEnum"
"variableScope"
"wrongPrintfScanfArgs"
*/         
        public class ErrorItem
        {
            public string filename;
            public int line;
            public string id, severity, msg;            
            public ErrorItem(XmlNode n)
            {
                filename = n.Attributes["file"].Value;
                line = int.Parse(n.Attributes["line"].Value);
                id = n.Attributes["id"].Value;
                severity = n.Attributes["severity"].Value;
                msg = n.Attributes["msg"].Value;
            }
            public override string ToString()
            {
                return "["+severity+"]"+"["+id+"] "+filename + ":" + line;
            }

        }

        public Form1()
        {
            InitializeComponent();
        }

        private void ShowFile(string filename,int line)
        {
            byte[] data,d2;
            data=System.IO.File.ReadAllBytes (filename);
            d2 = Encoding.Convert(System.Text.Encoding.Default, System.Text.Encoding.Unicode, data);
            string s = Encoding.Unicode.GetString(d2);
            code.Text = s;
            code.Caret.LineNumber = line-1;
            code.Scrolling.ScrollBy(0, 10);
            //code.Focus();
            code.Markers.AddInstanceSet(line-1, 2);
            code.Markers[1].ForeColor = Color.Green;
            code.Markers[1].BackColor = Color.Pink;
            //code.Markers[1].Symbol = ScintillaNet.MarkerSymbol.LCorner;
        }

        private XmlDocument report;

        private void btnLoadReport_Click(object sender, EventArgs e)
        {
            DialogResult r=OD.ShowDialog();
            if (r != DialogResult.OK) return;
            report = new XmlDocument();
            report.Load(OD.FileName);
            
            XmlNodeList errs;

            errs=report.GetElementsByTagName("error");
            lbErrs.Items.Clear();

            int totalmsgs = 0;
            foreach (XmlNode err in errs)
            {
                ErrorItem ei = new ErrorItem(err);
                if (ei.severity == "style" || ei.severity == "performance") continue;
                lbErrs.Items.Add(ei);
                totalmsgs++;
            }
            tbTotalMsgs.Text = " "+totalmsgs;
            lbErrs.Sorted = true;
        }

        private void lbErrs_DoubleClick(object sender, EventArgs e)
        {
            ErrorItem ei = (ErrorItem)lbErrs.SelectedItem;
            ShowFile(ei.filename, ei.line);


        }

        private void lbErrs_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index >= lbErrs.Items.Count || e.Index<0) return;
            ErrorItem ei = (ErrorItem)lbErrs.Items[e.Index];
            Graphics g=e.Graphics;

            //tblog.Text+="\r\n"+e.State.ToString();
            
            Brush b=null;
            if (!e.State.HasFlag(DrawItemState.Selected))
            {
                switch (ei.severity)
                {
                    case "style":
                        b = new SolidBrush(Color.Green);
                        break;
                    case "performance":
                        b = new SolidBrush(Color.Lime);
                        break;
                    case "warning":
                        b = new SolidBrush(Color.Yellow);
                        break;
                    case "error":
                        b = new SolidBrush(Color.Red);
                        break;
                    default:
                        b = new SolidBrush(Color.Pink);
                        break;
                }
                g.FillRectangle(b, e.Bounds);
            }
            else e.DrawBackground();

            //if (e.State == DrawItemState.Selected) b = new SolidBrush(Color.DarkBlue);
            

            
            
            
            string text = ei.ToString();
            b = new SolidBrush(Color.Black);

            g.DrawString(text, e.Font, b, e.Bounds);
            e.DrawFocusRectangle();
           

        }
        private int fStop=0;
        private void lbErrs_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrorItem ei = (ErrorItem)lbErrs.SelectedItem;
            tbID.Text = ei.id;
            tbSeverity.Text = ei.severity;
            tbMessage.Text = ei.ToString()+"\r\n"+System.Net.WebUtility.HtmlDecode(ei.msg).Replace("\n",Environment.NewLine);
            lbErrs_DoubleClick(sender, e);
            
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void сгенерироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Register the IOleMessageFilter to handle any threading 
            // errors.
            MessageFilter.Register();
            Word.Application app = new Word.Application();
            object oEndOfDoc = "\\endofdoc";
//            app.ScreenUpdating = false;
            //tbMessage.Text = System.IO.Directory.GetCurrentDirectory();
           
            //app.Visible = true;
            Word.Document wd = app.Documents.Add(System.IO.Directory.GetCurrentDirectory()+"\\Шаблон-cppcheck.dot");
            Word.Paragraph p = wd.Content.Paragraphs.Add();
            p.Range.Text = "Отчёт по результатам анализа cppcheck";
            p.Format.set_Style("Заголовок 1");
            p.Range.InsertParagraphAfter();
            
            
            p = wd.Content.Paragraphs.Add();
            p.Range.Text = "Исходные данные:";
            p.Range.Text += "Отчёт:" + OD.FileName;
            p.Range.InsertParagraphAfter();
            app.Selection.ClearFormatting();
            


            string previd="",prevsev="";
            int total=0;
//            r = wd.Bookmarks[oEndOfDoc].Range;
            //r = Type.Missing;
            foreach(ErrorItem ei in lbErrs.Items)
            {
                
                total++;
                tbTotalMsgs.Text = " " + total;
                if (prevsev != ei.severity)
                {
                    p = wd.Content.Paragraphs.Add();
                    p.Range.Text = ei.severity;
                    p.Format.set_Style("Нашел 1");
                    p.Range.InsertParagraphAfter();
                    prevsev = ei.severity;
                }

                if (previd != ei.id)
                {
                    p = wd.Content.Paragraphs.Add();
                    //wd.Range(p.Range.Start, p.Range.End).set_Style("Нашел 2");
                    p.Range.Text = ei.id;
                    p.Range.set_Style("Нашел 2");
                    p.Range.InsertParagraphAfter();
                    previd = ei.id;
                }
                p = wd.Content.Paragraphs.Add();
                wd.Range(p.Range.Start).set_Style("Обычный");
                //p.Range
                p.Range.Text = "В файле: "+ei.filename+" строка: "+ei.line;
                wd.Range(p.Range.Start + 9, p.Range.Start + 9+ei.filename.Length).set_Style("Путь");
                p.Range.InsertParagraphAfter();
                
                p = wd.Content.Paragraphs.Add();
                //p.Range.
                p.Range.Text = System.Net.WebUtility.HtmlDecode(ei.msg).Replace("\n", Environment.NewLine);
                p.Range.set_Style("Вывод");
                p.Range.InsertParagraphAfter();
                p.Range.InsertParagraphAfter();
                
                Application.DoEvents();
                if (fStop==1) break;
                    

            }
            app.Visible = true;
            MessageFilter.Revoke();
        }

        private void остановитьГенерациюToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fStop = 1;
        }
    }
}
