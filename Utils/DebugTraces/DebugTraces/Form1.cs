﻿/*
 * Утилита Трассировка датчиков
 * Файл Form1.cs
 *
 * ЗАО "РНТ" (с)
 * Разработчики: Кокин
 *
 * Утилита предназначена для визуализации последовательности полученных датчиков после динамических исследований объекта испытаний.
 * Отчет (или папка с несколькими отчетами) о вставке датчиков должен соответствовать отчету плагина IA о вставке датчиков.
 * Датчики могут быть в виде отдельных файлов, папок, упакованных архивов. Поддерживаемые форматы: бинарные, тестовые с шаблоном, тестовые цифровые.
 * Папка исходных текстов должна располагаться в месте, соответствующем отчету о вставке датчиков
 * Допустимы операции: шаг +n - переход на n датчиков вперед, шаг -n - переход на n датчиков назад, автомат вперед до совпадения с указанным датчиком.
 * В списке трассы датчиков отображается не более 1000 последних датчиков.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace DebugTraces
{
    public partial class Form1 : Form
    {
        bool AlreadyInTrace = false;
        const int DefFileSIZE = 4000;
        enum TraceTYPES
        {
            DIGIT = 1,
            TEXT = 2,
            BINARY = 3
        }
        /// <summary>
        /// Признак, установлен ли шаблон поиска для текстовых файлов
        /// </summary>
        public bool isShablon = false;
        /// <summary>
        /// Шаблон поиска для текстовых файлов
        /// </summary>
        public string KeyWord = "";
        SortedList<int, string> lsSensFiles = new SortedList<int, string>();
        List<string> lsFiles = new List<string>();
        List<int> lsSensors = new List<int>();
        int nMaxSensor = 0, nMinSensor = -1;
        SortedList<string, List<int>> lsFilesSens = new SortedList<string, List<int>>();
        SortedList<string, PartS> lsFilesTotFound = new SortedList<string, PartS>();
        listQF lQF = new listQF();
        int[] listSensors = null;
        int SensorIndex = -1;
        int nFindPos = 0;
        bool bFileSensor = true;
        bool bFileReport = true;
        bool bReportDone = false;
        List<int> viewSensors = new List<int>();
        List<string> viewFiles = new List<string>();


        public Form1()
        {
            InitializeComponent();
            groupBox5.Visible = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            if (Properties.Settings.Default.IsReportFile)
            {
                radioButton2.Checked = true;
                bFileReport = true;
            }
            else
            {
                radioButton1.Checked = true;
                bFileReport = false;
            }
            if (Properties.Settings.Default.IsSensorFile)
            {
                radioButton4.Checked = true;
                bFileSensor = true;
            }
            else
            {
                radioButton3.Checked = true;
                bFileSensor = false;
            }
            textBox1.Text = Properties.Settings.Default.Report;
            textBox2.Text = Properties.Settings.Default.Sensors;
            textBox3.Text = Properties.Settings.Default.Sources;
            ClearAllFields();
        }

        private class PartS
        {
            public int t;
            public int f;
            public PartS(int a, int b)
            {
                t = a;
                f = b;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            if (AlreadyInTrace)
            {
                dr = MessageBox.Show("Прекратить трассировку. Вы уверены?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (dr != DialogResult.OK)
                {
                    return;
                }
                AlreadyInTrace = false;
                ClearAllFields();
            }
            if (radioButton2.Checked == true)
            {
                OpenFileDialog fd = new  OpenFileDialog();
                dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox1.Text = fd.FileName;
                    bFileReport = true;
                }
            }
            else
            {
                FolderBrowserDialog fd = new FolderBrowserDialog();
                dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox1.Text = fd.SelectedPath;
                    bFileReport = false;
                }
            }
            if (dr != DialogResult.OK)
                return;

            FileInfo[] files;
            if (bFileReport)
            {
                files = new FileInfo[] { new FileInfo(textBox1.Text) };
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(textBox1.Text);
                files = di.GetFiles("*.*", SearchOption.AllDirectories);
            }

            foreach (FileInfo file in files)
            {
                string buf;
                Cursor.Current = Cursors.WaitCursor;
                StreamReader sr = new StreamReader(file.FullName);
                while ((buf = sr.ReadLine()) != null)
                {
                    string[] sa = buf.Split(' ');
                    if (sa.Length < 5)
                        break;
                    string s = sa[3];
                    int nMaxS = int.Parse(sa[1]);
                    lsSensors.Add(nMaxS);
                    if (nMaxSensor < nMaxS)
                        nMaxSensor = nMaxS;
                    if (nMinSensor == -1 || nMinSensor > nMaxS)
                        nMinSensor = nMaxS;
                    if (sa.Length > 5)
                    {
                        for (int i = 4; i < sa.Length - 1; i++)
                            s += " " + sa[i];
                    }
                    lsFiles.Add(s + "," + sa[sa.Length - 1]);

                    lsSensFiles.Add(nMaxS, s);
                    if (lsFilesSens.Keys.Contains(s))
                    {
                        lsFilesSens[s].Add(nMaxS);
                    }
                    else
                    {
                        lsFilesSens.Add(s, new List<int>() { nMaxS });
                    }
                }
            }
            foreach (string s in lsSensFiles.Values)
            {
                if (!lsFilesTotFound.Keys.Contains(s))
                {
                    lsFilesTotFound.Add(s, new PartS(1, 0));
                }
                else
                {
                    lsFilesTotFound[s].t++;
                }
            }
            listSensors = new int[nMaxSensor + 1];
            bReportDone = true;
            MessageBox.Show("Общее количество датчиков в отчетах: "+lsSensors.Count, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Text = "";
            Properties.Settings.Default.IsReportFile = radioButton2.Checked;
            Properties.Settings.Default.Save();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Text = "";
            Properties.Settings.Default.IsReportFile = radioButton2.Checked;
            Properties.Settings.Default.Save();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            textBox2.Text = "";
            Properties.Settings.Default.IsSensorFile = radioButton4.Checked;
            Properties.Settings.Default.Save();
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            textBox2.Text = "";
            Properties.Settings.Default.IsSensorFile = radioButton4.Checked;
            Properties.Settings.Default.Save();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            if (AlreadyInTrace)
            {
                dr = MessageBox.Show("Прекратить трассировку. Вы уверены?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (dr != DialogResult.OK)
                {
                    return;
                }
                AlreadyInTrace = false;
                ClearAllFields();
                bReportDone = false;
            }

            
            if (radioButton4.Checked == true)
            {
                OpenFileDialog fd = new OpenFileDialog();
                dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox2.Text = fd.FileName;
                    bFileSensor = true;
                }
            }
            else
            {
                FolderBrowserDialog fd = new FolderBrowserDialog();
                dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox2.Text = fd.SelectedPath;
                    bFileSensor = false;
                }
            }
            if (dr != DialogResult.OK)
                return;
        }

        /// <summary>
        /// Очистка полей формы.
        /// </summary>
        private void ClearAllFields()
        {
            label2.Text = "";
            label12.Text = "";
            richTextBox1.Text = "";
            groupBox4.Text = "Исходники";
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            textBox4.Text = "1";
            textBox5.Text = "";
            SensorIndex = -1;
            lsSensFiles.Clear();
            lsFiles.Clear();
            lsSensors.Clear();
            viewFiles.Clear();
            viewSensors.Clear();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!bReportDone)
            {
                if(textBox1.Text==null || textBox1.Text=="" ||textBox2.Text==null || textBox2.Text=="" ||textBox3.Text==null || textBox3.Text=="")
                {
                    MessageBox.Show("Установите пути к данным","Внимание",MessageBoxButtons.OK,MessageBoxIcon.Stop);
                    return;
                }

                FileInfo[] files;
                if (bFileReport)
                {
                    files = new FileInfo[] { new FileInfo(textBox1.Text) };
                }
                else
                {
                    DirectoryInfo di = new DirectoryInfo(textBox1.Text);
                    files = di.GetFiles("*.*", SearchOption.AllDirectories);
                }

                foreach (FileInfo file in files)
                {
                    string buf;
                    Cursor.Current = Cursors.WaitCursor;
                    StreamReader sr = new StreamReader(file.FullName);
                    while ((buf = sr.ReadLine()) != null)
                    {
                        string[] sa = buf.Split(' ');
                        if (sa.Length < 5)
                            break;
                        string s = sa[3];
                        int nMaxS = int.Parse(sa[1]);
                        lsSensors.Add(nMaxS);
                        if (nMaxSensor < nMaxS)
                            nMaxSensor = nMaxS;
                        if (nMinSensor == -1 || nMinSensor > nMaxS)
                            nMinSensor = nMaxS;
                        if (sa.Length > 5)
                        {
                            for (int i = 4; i < sa.Length - 1; i++)
                                s += " " + sa[i];
                        }
                        lsFiles.Add(s + "," + sa[sa.Length - 1]);

                        lsSensFiles.Add(nMaxS, s);
                        if (lsFilesSens.Keys.Contains(s))
                        {
                            lsFilesSens[s].Add(nMaxS);
                        }
                        else
                        {
                            lsFilesSens.Add(s, new List<int>() { nMaxS });
                        }
                    }
                }
                foreach (string s in lsSensFiles.Values)
                {
                    if (!lsFilesTotFound.Keys.Contains(s))
                    {
                        lsFilesTotFound.Add(s, new PartS(1, 0));
                    }
                    else
                    {
                        lsFilesTotFound[s].t++;
                    }
                }
                listSensors = new int[nMaxSensor + 1];
                bReportDone = true;
                MessageBox.Show("Общее количество датчиков в отчетах: " + lsSensors.Count, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            if (lsSensors.Count == 0)
                return;
            button5.Text = "Найти";
            nFindPos = 0;
            AlreadyInTrace = true;
            int delta;
            int nSensor;
            if(int.TryParse(textBox4.Text,out delta))
            {
                if (delta <= 0)
                {
                    AddSensor(delta);
                    RemoveFiles(-delta);
                    SensorIndex += delta;
                }
                else
                {
                    
                    for (int i = 0; i < delta; i++)
                    {
                        SensorIndex++;
                        if (TakeNextSensor().Count() > SensorIndex)
                        {
                            AddSensor(TakeNextSensor().ElementAt(SensorIndex));
                            if(lsFiles.Count>TakeNextSensor().ElementAt(SensorIndex) - nMinSensor)
                                AddFile(lsFiles[TakeNextSensor().ElementAt(SensorIndex) - nMinSensor].Split(',')[0]);
                            else
                                AddFile("Ненайденный файл");
                        }
                        else
                        {
                            MessageBox.Show("Поток датчиков закончился", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            SensorIndex--;
                            break;
                        }
                    }
                }
                if (SensorIndex < 0 || SensorIndex >= nMaxSensor - nMinSensor)
                {
                    MessageBox.Show("Выход за границы диапазона номеров датчиков", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                nSensor = TakeNextSensor().ElementAt(SensorIndex);
                groupBox4.Text = "Исходный код: " + lsFiles[nSensor - nMinSensor];
                string[] sa = lsFiles[nSensor - nMinSensor].Split(',');
                richTextBox1.Text = lQF.FileBody(textBox3.Text + "\\" + sa[0], false);
                richTextBox1.SelectionStart = int.Parse(sa[1]);
                nFindPos = richTextBox1.Text.IndexOf("\n", richTextBox1.SelectionStart);
                richTextBox1.SelectionLength = nFindPos - richTextBox1.SelectionStart;
                richTextBox1.SelectionColor = Color.Blue;
                richTextBox1.ScrollToCaret();
                label2.Text = nSensor.ToString();
                label12.Text = SensorIndex.ToString();
                ShowListSensors();
                ShowListFiles();
            }

        }

        /// <summary>
        /// Добавляет датчик или удаляет диапазон датчиков.
        /// </summary>
        private void AddSensor(int sens)
        {
            if (sens > 0)
                viewSensors.Add(sens);
            else
            {
                if (sens < 0 && viewSensors.Count >= -sens)
                {
                    viewSensors.RemoveRange(viewSensors.Count + sens, -sens);
                }
            }
        }

        /// <summary>
        /// Удаляет диапазон функций.
        /// </summary>
        private void RemoveFiles(int cnt)
        {
            viewFiles.RemoveRange(viewFiles.Count - cnt, cnt);
        }

        /// <summary>
        /// Добавляет функцию.
        /// </summary>
        private void AddFile(string fName)
        {
            viewFiles.Add(fName);
        }

        /// <summary>
        /// Отображает не более 100000 последних датчиков.
        /// </summary>
        private void ShowListSensors()
        {
            listBox2.Items.Clear();
            for (int i = viewSensors.Count - 1; i >= 0 && i >= viewSensors.Count-100000; i--)
                listBox2.Items.Add(viewSensors[i].ToString());
        }

        /// <summary>
        /// Отображает список файлов.
        /// </summary>
        private void ShowListFiles()
        {
            listBox1.Items.Clear();
            string stmp = "";
            int ncnt = 0;
            for (int i = viewFiles.Count - 1; i >= 0; i--)
            {
                if (stmp == viewFiles[i])
                {
                    ncnt++;
                }
                else
                {
                    if (stmp != "")
                    {
                        listBox1.Items.Add(stmp + "  " + (ncnt + 1).ToString());
                    }
                    ncnt = 0;
                    stmp = viewFiles[i];
                }
            }
            if (stmp != "")
                listBox1.Items.Add(stmp + "  " + (ncnt + 1).ToString());
        }
        /// <summary>
        /// Читает датчики из потока.
        /// </summary>
        /// <param name="fs">Поток файла</param>
        /// <returns name="IEnumerable(int)">Очередной датчик</returns>
        private IEnumerable<int> ReadTrace(FileStream fs)
        {
            string filename = fs.Name;
            byte[] arrA = new byte[DefFileSIZE];
            BinaryReader br = new BinaryReader(new FileStream(filename, FileMode.Open, FileAccess.Read));
            int length = br.Read(arrA, 0, DefFileSIZE);
            br.Close();
            if (length != 0)
            {
                if (length < DefFileSIZE)
                    arrA = arrA.Take(length).ToArray();
                int countLines = 0;
                switch (TraceType(arrA))
                {
                    case TraceTYPES.TEXT:
                        while (true)
                        {
                            if (!isShablon || KeyWord == null || KeyWord == "")
                            {
                                Form4 fm = new Form4(this);
                                fm.ShowDialog();
                            }
                            else
                            {
                                groupBox5.Visible = true;
                                textBox7.Text = KeyWord;
                                break;
                            }
                        }
                        StreamReader srt = new StreamReader(fs.Name);
                        while (true)
                        {
                            string s = srt.ReadLine();
                            countLines++;
                            if (s == null)
                                break;
                            int index = -1;
                            while (true)
                            {
                                index = s.IndexOf(KeyWord, index + 1);
                                if (index == -1)
                                    break;
                                int nD = index + KeyWord.Length;
                                while (nD < s.Length && s[nD] >= '0' && s[nD] <= '9')
                                    nD++;
                                int iS;
                                if (int.TryParse(s.Substring(index + KeyWord.Length, nD - index - KeyWord.Length), out iS))
                                {
                                    if (lsSensFiles.Keys.Contains(iS))
                                        yield return iS;
                                    else
                                        continue;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }
                        srt.Close();
                        break;
                    case TraceTYPES.DIGIT:
                        StreamReader srd = new StreamReader(fs);
                        while (true)
                        {
                            string s = srd.ReadLine();
                            countLines++;
                            if (s == null)
                                break;
                            int iS;
                            if (int.TryParse(s, out iS))
                            {
                                if (lsSensFiles.Keys.Contains(iS))
                                    yield return iS;
                                else
                                    continue;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        srd.Close();
                        break;
                    case TraceTYPES.BINARY:
                        BinaryReader brb = new BinaryReader(fs);
                        arrA = new byte[DefFileSIZE];
                        while (true)
                        {
                            length = brb.Read(arrA, 0, DefFileSIZE);
                            if (length == 0)
                                break;
                            for (int j = 0; j < length - 3; j += sizeof(Int32))
                            {
                                countLines++;
                                int iS = BitConverter.ToInt32(arrA, j);
                                if (iS > 0)
                                {
                                    if (lsSensFiles.Keys.Contains(iS))
                                        yield return iS;
                                    else
                                        continue;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }
                        brb.Close();
                        break;
                }
            }
        }

        /// <summary>
        /// Формирует временную папку.
        /// </summary>
        private TempDirectory GetTempDirectory()
        {
            return new TempDirectory(Directory.GetCurrentDirectory());
        }

        /// <summary>
        /// Для заданной папки или файла анализирует формат данных, при необходимости распаковывает архивы и для каждого файла выдает датчики в соответствии с форматом файла датчиков.
        /// </summary>
        /// <returns name="IEnumerable<int>">список датчиков</returns>
        private IEnumerable<int> TakeNextSensor()
        {
            byte[] arrA = new byte[1000000 * sizeof(Int32)];
            FileInfo[] files;
            if (bFileSensor)
            {
                files = new FileInfo[] { new FileInfo(textBox2.Text) };
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(textBox2.Text);
                files = di.GetFiles("*.*", SearchOption.AllDirectories);
            }

            foreach (FileInfo file in files)
            {
                string ext = file.Extension.ToLower();
                string[] knownArchives = new string[] { ".7z", ".xz", ".bz2", ".gz", ".tar", ".zip", ".wim", ".arj", ".cab", ".chm", ".CPIO", ".CramFS", ".deb", ".dmg", ".fat", ".hfs", ".iso", ".lzh", ".LZMA", ".mbr", ".msi", ".NSIS", ".NTFS", ".rar", ".rpm", ".SquashFS", ".udf", ".vhd", ".wim", ".xar", ".z" };
                if (knownArchives.Contains(ext)) //Архивы
                {
                    TempDirectory tDir = GetTempDirectory();
                    foreach (ArchiveReader.MyStream fs in ArchiveReader.Read(file.FullName, tDir.path))
                    {
                        foreach (int i in ReadTrace(fs))
                            yield return i;
                    }
                }
                else
                {
                    FileStream fs=null;
                    try
                    {

                        fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read);
                        foreach (int i in ReadTrace(fs))
                            yield return i;
                    }
                    finally
                    {
                        fs.Close();
                    }
                }
            }

        }

        /// <summary>
        /// Подсчитывает сумму чисел по маске.
        /// </summary>
        /// <param name="sF">массив чисел</param>
        /// <param name="mask">битовая маска</param>
        /// <returns>сумма чисел</returns>
        private Int64 MaskSum(Int64[] sF, byte[] mask)
        {
            Int64 ret = 0;
            for (int i = 0; i < 256; i++)
            {
                if (((0x80 >> (i % 8)) & mask[i / 8]) != 0)
                {
                    ret += sF[i];
                }
            }
            return ret;
        }

        /// <summary>
        /// Определяет тип массива по его содержимому.
        /// </summary>
        /// <param name="arr">массив данных</param>
        /// <returns>тип массива</returns>
        private TraceTYPES TraceType(byte[] arr)
        {
            byte[] TextMask = { 0xFF, 0x9B, 0xFF, 0xFF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            byte[] DigitMask = { 0xFF, 0xDB, 0xFF, 0xFF, 0xFF, 0xFF, 0, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

            Int64[] Signature = new Int64[256];
            for (int i = 0; i < arr.Length; i++)
            {
                Signature[arr[i]]++;
            }
            if (MaskSum(Signature, DigitMask) == 0)
                return TraceTYPES.DIGIT;
            if (MaskSum(Signature, TextMask) == 0)
                return TraceTYPES.TEXT;
            return TraceTYPES.BINARY;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!bReportDone)
            {
                if(textBox1.Text==null || textBox1.Text=="" ||textBox2.Text==null || textBox2.Text=="" ||textBox3.Text==null || textBox3.Text=="")
                {
                    MessageBox.Show("Установите пути к данным","Внимание",MessageBoxButtons.OK,MessageBoxIcon.Stop);
                    return;
                }

                FileInfo[] files;
                if (bFileReport)
                {
                    files = new FileInfo[] { new FileInfo(textBox1.Text) };
                }
                else
                {
                    DirectoryInfo di = new DirectoryInfo(textBox1.Text);
                    files = di.GetFiles("*.*", SearchOption.AllDirectories);
                }

                foreach (FileInfo file in files)
                {
                    string buf;
                    Cursor.Current = Cursors.WaitCursor;
                    StreamReader sr = new StreamReader(file.FullName);
                    while ((buf = sr.ReadLine()) != null)
                    {
                        string[] sa = buf.Split(' ');
                        if (sa.Length < 5)
                            break;
                        string s = sa[3];
                        int nMaxS = int.Parse(sa[1]);
                        lsSensors.Add(nMaxS);
                        if (nMaxSensor < nMaxS)
                            nMaxSensor = nMaxS;
                        if (nMinSensor == -1 || nMinSensor > nMaxS)
                            nMinSensor = nMaxS;
                        if (sa.Length > 5)
                        {
                            for (int i = 4; i < sa.Length - 1; i++)
                                s += " " + sa[i];
                        }
                        lsFiles.Add(s + "," + sa[sa.Length - 1]);

                        lsSensFiles.Add(nMaxS, s);
                        if (lsFilesSens.Keys.Contains(s))
                        {
                            lsFilesSens[s].Add(nMaxS);
                        }
                        else
                        {
                            lsFilesSens.Add(s, new List<int>() { nMaxS });
                        }
                    }
                }
                foreach (string s in lsSensFiles.Values)
                {
                    if (!lsFilesTotFound.Keys.Contains(s))
                    {
                        lsFilesTotFound.Add(s, new PartS(1, 0));
                    }
                    else
                    {
                        lsFilesTotFound[s].t++;
                    }
                }
                listSensors = new int[nMaxSensor + 1];
                bReportDone = true;
            }

            button5.Text = "Найти";
            nFindPos = 0;
            AlreadyInTrace = true;
            int delta;
            int nSensor=0;
            if (int.TryParse(textBox5.Text, out delta))
            {
                while (true)
                {

                    SensorIndex++;
                    if (TakeNextSensor().Count() > SensorIndex)
                    {
                        nSensor = TakeNextSensor().ElementAt(SensorIndex);
                        AddSensor(nSensor);
                        if (lsFiles.Count > nSensor - nMinSensor)
                            AddFile(lsFiles[nSensor - nMinSensor].Split(',')[0]);
                        else
                            AddFile("Ненайденный файл");
                        if (nSensor == delta)
                            break;
                    }
                    else
                    {
                        MessageBox.Show("Поток датчиков закончился", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        SensorIndex--;
                        nSensor = TakeNextSensor().ElementAt(SensorIndex);
                        break;
                    }
                }


                nSensor = TakeNextSensor().ElementAt(SensorIndex);
                groupBox4.Text = "Исходный код: " + lsFiles[nSensor - nMinSensor];
                string[] sa = lsFiles[nSensor - nMinSensor].Split(',');
                richTextBox1.Text = lQF.FileBody(textBox3.Text + "\\" + sa[0], false);
                richTextBox1.SelectionStart = int.Parse(sa[1]);
                nFindPos = richTextBox1.Text.IndexOf("\n", richTextBox1.SelectionStart);
                richTextBox1.SelectionLength = nFindPos - richTextBox1.SelectionStart;
                richTextBox1.SelectionColor = Color.Blue;
                richTextBox1.ScrollToCaret();
                label2.Text = nSensor.ToString();
                label12.Text = SensorIndex.ToString();
                ShowListSensors();
                ShowListFiles();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dr;
            if (AlreadyInTrace)
            {
                dr = MessageBox.Show("Прекратить трассировку. Вы уверены?", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (dr != DialogResult.OK)
                {
                    return;
                }
                AlreadyInTrace = false;
                ClearAllFields();
                bReportDone = false;
            }


                FolderBrowserDialog fd = new FolderBrowserDialog();
                dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                    textBox3.Text = fd.SelectedPath;

        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (textBox4.Text != null && textBox4.Text != "")
            {
                nFindPos = richTextBox1.Text.IndexOf(textBox6.Text, nFindPos);
                if (nFindPos != -1)
                {
                    richTextBox1.SelectionColor = SystemColors.ControlText;
                    richTextBox1.SelectionStart = nFindPos;
                    richTextBox1.SelectionLength = textBox4.Text.Length;
                    richTextBox1.SelectionColor = Color.Red;
                    richTextBox1.ScrollToCaret();
                    //label14.Text = (richTextBox1.GetLineFromCharIndex(richTextBox1.SelectionStart) + 1).ToString();
                    nFindPos++;
                    button5.Text = "Далее";
                }
                else
                    nFindPos = 0;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Report = textBox1.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Sensors = textBox2.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Sources = textBox3.Text;
            Properties.Settings.Default.Save();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
            {

                int nSensor;
                if (int.TryParse(listBox2.Items[listBox2.SelectedIndex].ToString(), out nSensor))
                {
                    groupBox4.Text = "Исходный код: " + lsFiles[nSensor - nMinSensor];
                    string[] sa = lsFiles[nSensor - nMinSensor].Split(',');
                    richTextBox1.Text = lQF.FileBody(textBox3.Text + "\\" + sa[0], false);
                    richTextBox1.SelectionStart = int.Parse(sa[1]);
                    nFindPos = richTextBox1.Text.IndexOf("\n", richTextBox1.SelectionStart);
                    richTextBox1.SelectionLength = nFindPos - richTextBox1.SelectionStart;
                    richTextBox1.SelectionColor = Color.Blue;
                    richTextBox1.ScrollToCaret();
                }
            }

        }

    }
    /// <summary>
    /// Класс представляет список классов QuickWeightFile.
    /// </summary>
    public class QuickFile
    {
        public string FileName;
        public List<int> LinePositions;
        public QuickFile(string fname, bool compressed)
        {
            FileName = fname;
            LinePositions = new List<int>();
            QuickRead(compressed);
        }
        public void QuickRead(bool compressed)
        {
            StreamReader sr = new StreamReader(FileName, Encoding.GetEncoding(1251));
            string s = "";
            FileBody = "";
            LinePositions.Add(FileBody.Length);
            while ((s = sr.ReadLine()) != null)
            {
                if (compressed && s.Trim() == "")
                {
                }
                else
                    FileBody += s + '\n';
                LinePositions.Add(FileBody.Length);
            }
        }
        public string FileBody
        {
            get;
            set;
        }

    }
    
    /// <summary>
    /// Класс представляет список классов QuickWeightFile.
    /// </summary>
    public class listQF
    {
        public SortedList<string, QuickWeightFile> listFiles = new SortedList<string, QuickWeightFile>();
        const int MaxCount = 50;
        static int CountFile = 1;
        public void AddFile(string fname, bool compressed)
        {
            if (!listFiles.Keys.Contains(fname))
            {
                if (listFiles.Count < MaxCount)
                {
                    listFiles.Add(fname, new QuickWeightFile(fname, CountFile++, compressed));
                }
                else
                {
                    int nmin = 0;
                    string srem = "";
                    foreach (string s in listFiles.Keys)
                    {
                        if (nmin == 0)
                        {
                            nmin = listFiles[s].Weight;
                            srem = s;
                        }
                        if (nmin > listFiles[s].Weight)
                        {
                            nmin = listFiles[s].Weight;
                            srem = s;
                        }

                    }
                    if (srem != "")
                        listFiles.Remove(srem);
                    listFiles.Add(fname, new QuickWeightFile(fname, CountFile++, compressed));
                }
            }
        }
        public string FileBody(string fname, bool compressed)
        {
            if (listFiles.Keys.Contains(fname))
            {
                listFiles[fname].Weight = CountFile++;
                return listFiles[fname].QF.FileBody;
            }
            else
            {
                AddFile(fname, compressed);
                return listFiles[fname].QF.FileBody;
            }
        }
        public int Position(string fname, int line)
        {
            return listFiles[fname].QF.LinePositions[line];
        }
    }
      
    /// <summary>
    /// Класс представляет механиъм для запуска внешней консольной программы.
    /// </summary>
    public class ExtProg
    {
        public static string ExecuteExtProg(string ProgName, string progargs)
        {

            string s = null;
            string extprog = Directory.GetCurrentDirectory() + @"\" + ProgName;
            if (!System.IO.File.Exists(extprog))
            {
                throw new NotImplementedException("Программа " + extprog + " не найдена!");
                //return null;
            }
            try
            {
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.EnableRaisingEvents = false;
                p.StartInfo.FileName = extprog;
                p.StartInfo.Arguments = progargs;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.Start();
                StreamReader sr = p.StandardOutput;
                s = sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException("Ошибка: " + ex.Message);
            }
            return s;
        }

    }

    /// <summary>
    /// Класс представляет структуру для хранения имени файла и его веса.
    /// </summary>
    public class QuickWeightFile
    {
        public QuickFile QF;
        int weight;
        public QuickWeightFile(string fn, int w, bool compressed)
        {
            QF = new QuickFile(fn, compressed);
            weight = w;
            Weight = w;
        }
        /// <summary>
        /// Вес файла
        /// </summary>
        public int Weight
        {
            get;
            set;
        }
    }
    /// <summary>
    /// Класс представляет временную папку. При его создании папка создается, при уничтожении - удаляется
    /// </summary>
    public class TempDirectory : IDisposable
    {
        string thePath;

        internal TempDirectory(string storagePath)
        {
            int i = 0;
            string tempName = "Temp";

            while (System.IO.Directory.Exists(thePath = System.IO.Path.Combine(storagePath, tempName + i.ToString())))
                i++;

            System.IO.Directory.CreateDirectory(thePath);
        }

        /// <summary>
        /// Возвращает путь к временной папке
        /// </summary>
        public string path
        { get { return thePath; } }



        #region IDisposable Members

        bool isDisposed = false;
        /// <summary>
        /// Закрыть объект и освободить ресурсы
        /// </summary>
        public void Dispose()
        {
            if (!isDisposed)
            {
                System.IO.Directory.Delete(thePath, true);
                isDisposed = true;
            }
        }

        ~TempDirectory()
        {
            Dispose();
        }

        #endregion
    }

    public class ArchiveReader
    {
        /// <summary>
        /// Распаковывает архив и для каждого запакованного в архив файла выдает потоки для его чтения.
        /// </summary>
        /// <param name="archiveFileName">Полное имя файла архива</param>
        /// <param name="tempPath">Путь к временной папке</param>
        /// <returns name="IEnumerable(MyStream)">Потоки для чтения файлов архива</returns>
        public static IEnumerable<MyStream> Read(string archiveFileName, string tempPath)
        {

            List<string> filelist = new List<string>();
            string stdout = ExtProg.ExecuteExtProg("7z.exe", "l " + "\"" + archiveFileName + "\"");
            Encoding enc3 = System.Text.Encoding.UTF8;
            MemoryStream ms = new MemoryStream(enc3.GetBytes(stdout));
            StreamReader sr = new StreamReader(ms);

            string s;
            Encoding enc = sr.CurrentEncoding;
            bool bStart = false;
            while ((s = sr.ReadLine()) != null)
            {
                s = s.Trim();
                const string sStartD = "------------------- ----- ------------ ------------  ------------------------";
                if (s == sStartD)
                {
                    if (!bStart)
                        bStart = true;
                    else
                        bStart = false;
                }
                if (s != "" && bStart && s != sStartD)
                {

                    Encoding enc1 = System.Text.Encoding.GetEncoding(1251);
                    byte[] b = enc1.GetBytes(s);
                    Encoding enc2 = System.Text.Encoding.GetEncoding(866);
                    string ss = enc2.GetString(b);
                    Regex re = new Regex(" ");
                    List<string> ls = new List<string>();
                    foreach (String sub in re.Split(ss))
                    {
                        if (sub != null && sub != "")
                        {
                            ls.Add(sub);
                        }


                    }
                    Int32 itmp;
                    if (Int32.TryParse(ls[4], out itmp))
                    {
                        if (ls.Count > 6)
                        {
                            for (int i = 6; i < ls.Count; i++)
                            {
                                ls[5] += " " + ls[i];
                            }
                        }
                        if (ls[2][0] != 'D')
                        {
                            filelist.Add(ls[5]);
                        }
                    }
                    else
                    {
                        if (ls.Count > 5)
                        {
                            for (int i = 5; i < ls.Count; i++)
                            {
                                ls[4] += " " + ls[i];
                            }
                        }
                        if (ls[2][0] != 'D')
                        {
                            filelist.Add(ls[4]);
                        }
                    }

                }
            }

            foreach (string file in filelist)
            {
                string stdout1 = ExtProg.ExecuteExtProg("7z.exe", "e -y -o" + "\"" + tempPath + "\"" + " " + "\"" + archiveFileName + "\"" + " " + "\"" + file + "\"");
                Encoding enc1 = System.Text.Encoding.GetEncoding(1251);
                byte[] b = enc1.GetBytes(stdout1);
                Encoding enc2 = System.Text.Encoding.GetEncoding(866);
                string result = enc2.GetString(b);
                FileInfo fi0 = new FileInfo(file);
                FileInfo fi = new FileInfo(Path.Combine(tempPath, fi0.Name));
                MyStream fileStream = null;
                try
                {
                    fileStream = new MyStream(fi.FullName, FileMode.Open, FileAccess.Read);
                    yield return fileStream;
                }
                finally
                {
                    fileStream.Dispose();
                }

            }

        }

        /// <summary>
        /// Класс на базе FileStream, дополнительно: путь к файлу, признак удаления.
        /// </summary>
        /// <constructor name="MyStream"></param>
        /// <param name="path">Полное имя файла потока</param>
        /// <param name="mode">FileMode потока FilrStream</param>
        /// <param name="access">FileAccess потока FilrStream</param>
        /// <returns></returns>
        public class MyStream : FileStream, IDisposable
        {
            string savedPath = null;
            bool withDelete = true;
            //            private bool disposed = false;

            public MyStream(string path, FileMode mode, FileAccess access)
                : base(path, mode, access)
            {
                savedPath = path;
                withDelete = true;

            }

            public void SetwithDelete(bool cur)
            {
                withDelete = cur;
            }

            //public new void Dispose()
            //{
            //    DisposeMe(true);
            //    GC.SuppressFinalize(this);
            //}

            public string GetSavedPath
            {
                get
                {
                    return savedPath;
                }
            }

            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);

                if (withDelete && savedPath != null)
                {
                    try
                    {
                        File.Delete(savedPath);
                    }
                    catch (Exception ex)
                    {
                        throw new NotImplementedException("Ошибка удаления файла " + savedPath + Environment.NewLine + ex.Message);
                    }
                }
            }

            //~MyStream()
            //{
            //    Dispose(false);
            //}
        }


    }
}
