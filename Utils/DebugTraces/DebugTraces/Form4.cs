﻿/*
 * Утилита Трассировка датчиков
 * Файл Form4.cs
 *
 * ЗАО "РНТ" (с)
 * Разработчики: Кокин
 *
 * Форма предназначена для для указания шаблона, после которого располагается номер датчика в файлах текстовых датчиков.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DebugTraces
{
    public partial class Form4 : Form
    {
        Form1 fmParent;
        public Form4(Form1 fmP)
        {
            fmParent = fmP;
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != null && textBox1.Text != "")
            {
                fmParent.isShablon = true;
                fmParent.KeyWord = textBox1.Text;
                this.Close();
            }
            else
            {
                MessageBox.Show("Шаблон должен соответствовать фразе: <фраза>#, где # - номер датчика","Внимание",MessageBoxButtons.OK,MessageBoxIcon.Error);
                this.Close();
            }
        }
    }
}
