﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileRestore
{
    public partial class Form1 : Form
    {
        string folderClean = string.Empty;
        string folderDirty = string.Empty;
        string folderRemote = string.Empty;

        List<string> listClean = new List<string>();
        List<string> listDirty = new List<string>();
        List<string> minus = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                folderClean = folderBrowserDialog1.SelectedPath;
            }
            listClean = System.IO.Directory.EnumerateFiles(folderClean, "*.*", System.IO.SearchOption.AllDirectories).ToList<string>();
            List<string> listTmp = new List<string>();
            foreach (string str in listClean)
                listTmp.Add(str.Substring(folderClean.Length));

            listClean = listTmp;
            textBox1.Text = folderClean;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                folderDirty = folderBrowserDialog1.SelectedPath;
            }
            listDirty = System.IO.Directory.EnumerateFiles(folderDirty, "*.*", System.IO.SearchOption.AllDirectories).ToList<string>();
            List<string> listTmp = new List<string>();
            foreach (string str in listDirty)
                listTmp.Add(str.Substring(folderDirty.Length));

            listDirty = listTmp;
            foreach (string str in listDirty)
                if (!listClean.Contains(str))
                    minus.Add(str);
            textBox2.Text = folderDirty;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                folderRemote = folderBrowserDialog1.SelectedPath;
            }
            textBox3.Text = folderRemote;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            if (textBox4.Text != "")
            {
                textBox4.Text = textBox4.Text.ToLower();
                foreach (string str in minus)
                    if (str.ToLower().Contains(textBox4.Text))
                        listBox1.Items.Add(str);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndices.Count != 0)
            {
                foreach (int ind in listBox1.SelectedIndices)
                {
                    string str = (string)listBox1.Items[ind];
                    System.IO.File.Copy(folderDirty + str, folderClean + str);
                    System.IO.File.Copy(folderDirty + str, folderRemote + str);
                    minus.Remove(str);
                }
                button4_Click(null, null);
            }
        }



    }
}
