﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CorrSensReport
{
    public partial class Form1 : Form
    {
        public List<Record> repOld = new List<Record>();
        public List<Record> repOld_corr = new List<Record>();
        public List<Record> repNew = new List<Record>();

        public Form1()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        public class Record
        {
            public int num;
            public string func;
            public string path;
            public int sensor;
            public Record(int n, string f, string p, int s)
            {
                num = n;
                func = f;
                path = p;
                sensor = s;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = null;
            try
            {
                fd = new OpenFileDialog();

                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox1.Text = fd.FileName;

                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }

            listBox1.Items.Clear();
            if (textBox1.Text != "")
            {
                StreamReader sr = new StreamReader(textBox1.Text);
                while (true)
                {
                    string s = sr.ReadLine();
                    if (s == null) break;
                    listBox1.Items.Add(s);
                    string[] sa = s.Split(' ');
                    if (sa.Length < 5)
                        break;
                    string sT = sa[3];
                    if (sa.Length > 5)
                    {
                        sT = sa.Skip(3).ToArray().Take(sa.Length - 4).Aggregate((a, b) => a + " " + b);
                    }
                    repOld.Add(new Record(int.Parse(sa[1]), sa[2], sT, int.Parse(sa[sa.Length-1])));
                }
                sr.Close();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = null;
            try
            {
                fd = new OpenFileDialog();

                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox2.Text = fd.FileName;

                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }
            listBox2.Items.Clear();
            if (textBox2.Text != "")
            {
                StreamReader sr = new StreamReader(textBox2.Text);
                while (true)
                {
                    string s = sr.ReadLine();
                    if (s == null) break;
                    listBox2.Items.Add(s);
                    string[] sa = s.Split(' ');
                    if (sa.Length < 5)
                        break;
                    string sT = sa[3];
                    if (sa.Length > 5)
                    {
                        sT = sa.Skip(3).ToArray().Take(sa.Length - 4).Aggregate((a, b) => a + " " + b);
                    }
                    repNew.Add(new Record(int.Parse(sa[1]), sa[2], sT, int.Parse(sa[sa.Length - 1])));
                }
                sr.Close();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            int count = 0;
            Cursor = Cursors.WaitCursor;
            repOld_corr.Clear();
            StreamWriter sw = new StreamWriter(textBox1.Text + "_corr");

            var listObj = (from r in repNew
                           orderby r.num
                           select new { func = r.func, path = r.path }).Distinct();
            foreach (var no in listObj)
            {
                var queryRecO = (from reco in repOld
                                 where reco.func == no.func && reco.path == no.path
                                 orderby reco.num
                                 select reco).ToList();
                var queryRecN = (from reco in repNew
                                 where reco.func == no.func && reco.path == no.path
                                 orderby reco.num
                                 select reco).ToList();
                if (queryRecN.Count == queryRecO.Count)
                {
                    for (int i = 0; i < queryRecN.Count; i++)
                    {
                        count++;
                        queryRecO[i].sensor = queryRecN[i].sensor;
                        repOld_corr.Add(queryRecO[i]);
                    }
                }
            }
            var querySort = (from r in repOld_corr
                             orderby r.num
                             select r).Distinct();
            foreach (var r in querySort)
            {
                sw.WriteLine("s " + r.num + " " + r.func + " " + r.path + " " + r.sensor);
            }
            sw.Close();
            Cursor = Cursors.Default;
            MessageBox.Show("Сформирован файл: "+textBox1.Text+"_corr"+"\n  количество датчиков: "+count, "Выполнено", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }
    }
}
