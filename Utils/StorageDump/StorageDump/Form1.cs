﻿/*
 * Утилита Просмотр хранилища
 * Проект StorageDump
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Кокин
 * 
 * Позволяет просмотреть содержимое таблиц хранилища (Berkeley DB)
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace StorageDump
{
    public partial class Form1 : Form
    {
        mStorage storage = null;
        List<string[]> listHexData = new List<string[]>();
        bool Busy = false;

        public Form1()
        {
            InitializeComponent();
            textBox1.Text = Properties.Settings.Default.Storage;
        }

        /// <summary>
        /// В выбранной папке ищет файлы, которые могут быть таблицами Berkeley DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = null;
            try
            {
                fd = new FolderBrowserDialog();

                fd.SelectedPath = Properties.Settings.Default.Storage;
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox1.Text = fd.SelectedPath;
                }
                else
                {
                    textBox1.Text = "";
                    return;
                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }

            DirectoryInfo di = new DirectoryInfo(textBox1.Text);
            FileInfo[] files = di.GetFiles("*.*", SearchOption.TopDirectoryOnly);
            listBox1.Items.Clear();
            foreach (FileInfo file in files)
            {
                if (!file.Name.Contains("__db") && !file.Name.Contains("log") && !file.Name.Contains(".xml") && !file.Name.Contains(".version") && !file.Name.Contains(".lock"))
                {
                    listBox1.Items.Add(file.Name);
                }
            }
            if (storage != null)
            {
                storage.Close();
                storage = null;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Storage = textBox1.Text;
            Properties.Settings.Default.Save();
        }

        /// <summary>
        /// При выборе таблицы из списка делается попытка отобразить содержимое таблицы в виде набора записей <ключ,данные>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Busy)
                return;
            dataGridView1.Rows.Clear();
            listHexData.Clear();
            storage = new mStorage();
            storage.Open(textBox1.Text);

            bool brecno = true;
            int tableType = 0;
            if (listBox1.SelectedIndex != -1)
            {
                string tbname = listBox1.Text;

                try
                {
                    storage.t003 = storage.GetTableIndexDuplicate(tbname);
                    storage.t000 = storage.t003;
                    brecno = false;
                    tableType = 3;
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                }
                if (storage.t000 == null)
                {

                    try
                    {
                        storage.t004 = storage.GetTableIndexNoDuplicate(tbname);
                        storage.t000 = storage.t004;
                        tableType = 1;
                    }
                    catch (Exception ex)
                    {
                        string error = ex.Message;
                    }
                }
                if (storage.t000 == null)
                {

                    try
                    {
                        storage.t001 = storage.GetTableIDDuplicate(tbname);
                        storage.t000 = storage.t001;
                        tableType = 1;
                    }
                    catch (Exception ex)
                    {
                        string error = ex.Message;
                    }
                }
                if (storage.t000 == null)
                {
                    try
                    {
                        storage.t002 = storage.GetTableIDNoDuplicate(tbname);
                        storage.t000 = storage.t002;
                        tableType = 2;
                    }
                    catch (Exception ex)
                    {
                        string error = ex.Message;
                    }
                }
                if (storage.t000 == null)
                {
                    try
                    {
                        storage.t003 = storage.GetTableIndexDuplicate(tbname);
                        storage.t000 = storage.t003;
                        brecno = false;
                        tableType = 3;
                    }
                    catch (Exception ex)
                    {
                        string error = ex.Message;
                    }
                }
                if (storage.t000 == null)
                {
                    try
                    {
                        storage.t004 = storage.GetTableIndexNoDuplicate(tbname);
                        storage.t000 = storage.t004;
                        brecno = false;
                        tableType = 4;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        if (storage != null)
                        {
                            storage.Close();
                            storage = null;
                        }

                        return;
                    }
                }
                if (storage.t000 == null)
                {
                    MessageBox.Show("Конфликт версий при попытке открытия таблицы", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (storage != null)
                    {
                        storage.Close();
                        storage = null;
                    }

                    return;
                }
                if (brecno)
                    label1.Text = "RecNo " + tableType;
                else
                    label1.Text = "BTree " + tableType;
                Cursor = Cursors.WaitCursor;
                Busy = true;
                int cnt = 1;
                try
                {
                    switch (tableType)
                    {
                        case 1:
                            foreach (var x in storage.t001)
                            {
                                ulong key = x.Key;
                                string data = GetString1(x.Value.GetAllBytes());
                                if (data == "")
                                    data = BitConverter.ToString(x.Value.GetAllBytes()).Replace("-", "");
                                string[] sa = new string[] { key.ToString(), data };
                                string data1 = BitConverter.ToString(x.Value.GetAllBytes()).Replace("-", "");
                                string[] sa1 = new string[] { key.ToString(), data1 };
                                listHexData.Add(sa1);
                                dataGridView1.Rows.Add(sa);
                                label2.Text = cnt.ToString();
                                if (cnt % 100 == 0)
                                {
                                    Application.DoEvents();
                                }
                                cnt++;
                            }
                            break;
                        case 2:
                            foreach (var x in storage.t002)
                            {
                                ulong key = x.Key;
                                string data = GetString1(x.Value.GetAllBytes());
                                if (data == "")
                                    data = BitConverter.ToString(x.Value.GetAllBytes()).Replace("-", "");
                                string[] sa = new string[] { key.ToString(), data };
                                dataGridView1.Rows.Add(sa);
                                string data1 = BitConverter.ToString(x.Value.GetAllBytes()).Replace("-", "");
                                string[] sa1 = new string[] { key.ToString(), data1 };
                                listHexData.Add(sa1);
                                label2.Text = cnt.ToString();
                                if (cnt % 100 == 0)
                                {
                                    Application.DoEvents();
                                }
                                cnt++;
                            }
                            break;
                        case 3:
                            foreach (var x in storage.t003)
                            {
                                string key = GetString1(x.Key.GetAllBytes());
                                if (key == "")
                                    key = BitConverter.ToString(x.Key.GetAllBytes()).Replace("-", "");
                                string data = GetString1(x.Value.GetAllBytes());
                                if (data == "")
                                    data = BitConverter.ToString(x.Value.GetAllBytes()).Replace("-", "");
                                string[] sa = new string[] { key, data };
                                string data1 = BitConverter.ToString(x.Value.GetAllBytes()).Replace("-", "");
                                string key1 = BitConverter.ToString(x.Key.GetAllBytes()).Replace("-", "");
                                string[] sa1 = new string[] { key1, data1 };
                                listHexData.Add(sa1);
                                label2.Text = cnt.ToString();
                                dataGridView1.Rows.Add(sa);
                                if (cnt % 100 == 0)
                                {
                                    Application.DoEvents();
                                }
                                cnt++;
                            }
                            break;
                        case 4:
                            foreach (var x in storage.t004)
                            {
                                string key = GetString1(x.Key.GetAllBytes());
                                if (key == "")
                                    key = BitConverter.ToString(x.Key.GetAllBytes()).Replace("-", "");
                                string data = GetString1(x.Value.GetAllBytes());
                                if (data == "")
                                    data = BitConverter.ToString(x.Value.GetAllBytes()).Replace("-", "");
                                string[] sa = new string[] { key, data };
                                string data1 = BitConverter.ToString(x.Value.GetAllBytes()).Replace("-", "");
                                string key1 = BitConverter.ToString(x.Key.GetAllBytes()).Replace("-", "");
                                string[] sa1 = new string[] { key1, data1 };
                                listHexData.Add(sa1);
                                dataGridView1.Rows.Add(sa);
                                label2.Text = cnt.ToString();
                                if (cnt % 100 == 0)
                                {
                                    Application.DoEvents();
                                }
                                cnt++;
                            }
                            break;
                    }

                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    if (storage != null)
                    {
                        storage.Close();
                        storage = null;
                    }

                    return;
                }
                if (storage != null)
                {
                    storage.Close();
                    storage = null;
                }
                Busy = false;
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Преобразование шестнадцатеричной строки в байтовый массив
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static byte[] HexStringToByteArray(string hex)
        {
            if (hex.Length % 2 != 0)
                return null;
            return Enumerable.Range(0, hex.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
        }
        /// <summary>
        /// Получение строки из массива байт: - если это набор: <количество символов,строка символов>, то возвращается строка символов, иначе - шестнадцатеричное представление массива байт.
        /// Если результат велик (более 8000 символов), то он усекается до 8000 символов.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public string GetString(byte[] buffer)
        {
            string ret = "";
            if (buffer == null)
                return ret;
            if (buffer.Length < 4)
                return ret;
            else
            {
                int length = BitConverter.ToInt32(buffer, 0);
                if (length == 0)
                    return ret;
                if (length > buffer.Length)
                    return ret;
                byte[] tmp = Encoding.Default.GetBytes(Encoding.UTF8.GetString(buffer.Skip(4).Take(length).ToArray()));
                if (ArrayIsText(tmp))
                    ret = Encoding.UTF8.GetString(buffer.Skip(4).Take(length).ToArray());
                else
                    ret = BitConverter.ToString(tmp).Replace("-", "");
            }
            if (ret.Length > 8000)
                ret = ret.Substring(0, 8000);
            return ret;
        }
        /// <summary>
        /// Ищется следующий объект в записи таблицы Berkeley DB (с учетом формата хранения данных в таблицах IA)
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="ptr"></param>
        /// <param name="optr"></param>
        /// <param name="ostr"></param>
        /// <returns></returns>
        int NextObject(byte[] buffer, int ptr, out int optr,out string ostr)
        {
            int sptr=ptr;
            ostr = "";
            while (ptr<buffer.Length)
            {
                if (buffer[ptr] == 0)
                    break;
                ptr++;
            }
            if (ptr == buffer.Length)
            {
                optr = -1;
                return 5;
            }
            optr = ptr;
            while (optr < buffer.Length)
            {
                if (buffer[optr] != 0)
                    break;
                optr++;
            }
            if (optr == buffer.Length)
            {
                optr = -1;
                return 5;
            }
            if (optr - sptr == 4)
            {
                int len = BitConverter.ToInt32(buffer, sptr);
                int min_ = Math.Min(len,buffer.Length - sptr - 4);
                if (min_ == 1) //Особый случай
                {
                    ostr = (BitConverter.ToInt32(buffer, sptr)).ToString();
                    return 2;
                }
                else
                {
                    byte[] tmp = Encoding.Default.GetBytes(Encoding.UTF8.GetString(buffer.Skip(sptr + 4).Take(min_).ToArray()));
                    if (ArrayIsText(tmp))
                    {
                        ostr = Encoding.UTF8.GetString(buffer.Skip(sptr + 4).Take(min_).ToArray());
                        optr += min_;
                        return 1;
                    }
                    else
                    {
                        ostr = (BitConverter.ToInt32(buffer.Skip(sptr).Take(4).ToArray(), 0)).ToString();
                        return 2;
                    }
                }
            }
            if (optr - sptr > 4)
            {
                if(sptr==0)
                    ostr = (BitConverter.ToString(buffer.Skip(sptr).Take(optr - sptr-4).ToArray(), 0)).Replace("-", "");
                else
                    ostr = (BitConverter.ToString(buffer.Skip(sptr).Take(optr - sptr-4).ToArray(), 0)).Replace("-", "");
                optr -= 4;
                return 3;
            }
            if (optr - sptr < 4)
            {
                ostr = (BitConverter.ToString(buffer.Skip(sptr).Take(optr - sptr).ToArray(), 0)).Replace("-", "");
                return 4;
            }
            optr = -1;
            ostr = "";
            return 5;
        }
        /// <summary>
        /// Получение строкового представления записи таблицы Berkeley DB
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public string GetString1(byte[] buffer)
        {
            string ret = "";
            int ptr = 0;
            int optr = 0;
            string curr;
            while (true)
            {
                switch (NextObject(buffer, ptr, out optr, out curr))
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                }
                ret += " " + curr;
                if(optr==-1)
                break;
                ptr = optr;
                if (ret.Length > 8000)
                    break;
            }
            if (ret.Length > 8000)
                ret = ret.Substring(0, 8000);
            return ret;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (storage != null)
                storage.Close();
        }

        /// <summary>
        /// Статическая переменная о кодировке последнего открытого файла
        /// </summary>
        static Encoding encoding = Encoding.Default;
        /// <summary>
        /// Подсчитывает сумму чисел по маске.
        /// </summary>
        /// <param name="sF">массив чисел</param>
        /// <param name="mask">битовая маска</param>
        /// <returns>сумма чисел</returns>
        static private Int64 MaskSum(Int64[] sF, byte[] mask)
        {
            Int64 ret = 0;
            for (int i = 0; i < 256; i++)
            {
                if (((0x80 >> (i % 8)) & mask[i / 8]) != 0)
                {
                    ret += sF[i];
                }
            }
            return ret;
        }
        /// <summary>
        /// Определяет тип массива по его содержимому.
        /// </summary>
        /// <param name="arr">массив данных</param>
        /// <returns>тип массива</returns>
        static private bool ArrayIsText(byte[] arr)
        {
            byte[] TextMask = { 0xFF, 0x9B, 0xFF, 0xDF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            Int64[] Signature = new Int64[256];
            for (int i = 0; i < arr.Length; i++)
            {
                Signature[arr[i]]++;
            }
            if (MaskSum(Signature, TextMask) == 0)
                return true;
            return false;
        }
        /// <summary>
        /// Отображение шестнадцатеричного представления содержимого поля в виде ToolTip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            string res = "";
            int row = e.RowIndex;
            int col = e.ColumnIndex;
            if (row < 0 || col < 0)
                return;
            try
            {
                res = listHexData[row][col];
            }
            catch(Exception ex)
            {
                string error = ex.Message;
            }
            dataGridView1.Rows[row].Cells[col].ToolTipText = res;

        }
    }
}
