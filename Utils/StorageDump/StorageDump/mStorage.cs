﻿/*
 * Утилита Просмотр хранилища
 * Файл mStorage.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Русаков
 * Модификация: Кокин
 * 
 * Позволяет просмотреть содержимое таблиц хранилища (Berkeley DB)
 * По аналогии со стандартным хранилищем используются таблицы разных типов для открытия существующих таблиц хранилища
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Store.Table;


namespace StorageDump
{
    internal delegate void CloseHandle();

    /// <summary>
    /// Головной класс хранилища. При любом сценарии использования библиотеки, работа начинается с создания экземпляра данного класса.
    /// 
    /// Данный класс выполняет открытие и закрытие базы данных Хранилища (функции <see cref="Store.Storage.Open(System.String)">Open</see>, 
    /// <see cref="Store.Storage.Create(System.String)">Create</see>, <see cref="Store.Storage.Close">Close</see>).
    /// </summary>
    public class mStorage : IDisposable
    {

        /// <summary>
        /// Состояние Хранилища: закрыто/открыто
        /// </summary>
        internal bool isClosed = true;

        /// <summary>
        /// Низкоуровневый объект для работы с базой данных.
        /// </summary>
        internal DB_Operations DB = new DB_Operations();

        /// <summary>
        /// Конструктор используется для создания экземпляра Хранилища
        /// </summary>
        public mStorage()
        {
        }
        public object t000;
        public TableIDDuplicate t001;
        public TableIDNoDuplicate t002;
        public TableIndexedDuplicates t003;
        public TableIndexedNoDuplicates t004;

        /// <summary>
        /// Деструктор хранилища
        /// </summary>
        ~mStorage()
        {
            if (!isClosed) //Выдать исключение, если Хранилище пытаются уничтожить, не закрыв его. В этом случае невозможно корректно сохранить 
                //состояние объектов в базу данных.
                throw new Exception("Перед завершением выполнения необходимо вызвать метод Storage.Close(). Данный метод не был вызван!");
        }

        /// <summary>
        /// Открывает Хранилище, расположенное в директории name. Хранилище должно существовать.
        /// </summary>
        /// <param name="name"></param>
        public void Open(string name)
        {
            //Открываем Хранилище
            DB.Open(name);
            isClosed = false;
        }

        /// <summary>
        /// Это событие срабатывает при закрытие Хранилища. Позволяет сохранять несохраненные данные.
        /// </summary>
        internal event CloseHandle CloseEvent;

        /// <summary>
        /// Это событие срабатывает при закрытие Хранилища. Позволяет закрыть все открытые таблицы.
        /// </summary>
        event CloseHandle CloseEvent_System;

        /// <summary>
        /// Закрывает Хранилище. При этом сохраняются состояния всех открытых объектов.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods", MessageId = "System.GC.Collect")]
        public void Close()
        {
            if (!isClosed)
            {
                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();

                if (CloseEvent != null)
                    CloseEvent();
                if (CloseEvent_System != null)
                    CloseEvent_System();
                DB.Close();
                isClosed = true;
            }

            NullEverything();

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();
        }

        private void NullEverything()
        {
            t000 = null;
            t001 = null;
            t002 = null;
            t003 = null;
            t004 = null;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Close();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Возвращает имя папки, в которой открыто Хранилище.
        /// null, если хранилище не открыто.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public string GetStorageFolderName()
        {
            if (isClosed)
                return null;
            else
                return DB.GetDBDirectory();
        }

        /// <summary>
        /// Возвращает объект для низкойуровневой работы с БД.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        internal DB_Operations Get_DB()
        {
            return DB;
        }

        /// <summary>
        /// Открывает таблицу типа TableIDNoDuplicate с указанным именем. Если такая таблица не существует, она будет создана.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIDNoDuplicate GetTableIDNoDuplicate(string name)
        {
            TableIDNoDuplicate table = DB.GetTableIDNoDuplicate(name);
            if (!table.IsTableNull())
            {
                CloseEvent_System += table.Close;
                return table;
            }
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIDNoDuplicate с указанным именем. 
        /// Если такая таблица не существует, она НЕ будет создана, результатом работы будет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIDNoDuplicate TryOpenTableIDNoDuplicate(string name)
        {
            if (DB.IsDBExists(name))
                return GetTableIDNoDuplicate(name);
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIDDuplicate с указанным именем. Если такая таблица не существует, она будет создана.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIDDuplicate GetTableIDDuplicate(string name)
        {
            TableIDDuplicate table = DB.GetTableIDDuplicate(name);
            if (!table.IsTableNull())
            {
                CloseEvent_System += table.Close;
                return table;
            }
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIDDuplicate с указанным именем. 
        /// Если такая таблица не существует, она НЕ будет создана, результатом работы будет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIDDuplicate TryOpenTableIDDuplicates(string name)
        {
            if (DB.IsDBExists(name))
                return GetTableIDDuplicate(name);
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIndexedDuplicates с указанным именем. Если такая таблица не существует, она будет создана.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIndexedDuplicates GetTableIndexDuplicate(string name)
        {
            TableIndexedDuplicates table = DB.GetTableIndexedDuplicates(name);
            if (!table.IsTableNull())
            {
                CloseEvent_System += table.Close;
                return table;
            }
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIndexedDuplicates с указанным именем. 
        /// Если такая таблица не существует, она НЕ будет создана, результатом работы будет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal TableIndexedDuplicates TryOpenTableIndexedDuplicates(string name)
        {
            if (DB.IsDBExists(name))
                return GetTableIndexDuplicate(name);
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIndexedNoDuplicates с указанным именем. Если такая таблица не существует, она будет создана.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TableIndexedNoDuplicates GetTableIndexNoDuplicate(string name)
        {
            TableIndexedNoDuplicates table = DB.GetTableIndexedNoDuplicates(name);
            if (!table.IsTableNull())
            {
                CloseEvent_System += table.Close;
                return table;
            }
            else
                return null;
        }

        /// <summary>
        /// Открывает таблицу типа TableIndexedNoDuplicates с указанным именем. 
        /// Если такая таблица не существует, она НЕ будет создана, результатом работы будет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TableIndexedNoDuplicates TryOpenTableIndexedNoDuplicates(string name)
        {
            if (DB.IsDBExists(name))
                return GetTableIndexNoDuplicate(name);
            else
                return null;
        }
    }

}
