﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ParseIISLog
{
    public partial class Form1 : Form
    {
        List<string> listAddition = new List<string>();

        public Form1()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            textBox1.Text = Properties.Settings.Default.Source;
            textBox2.Text = Properties.Settings.Default.LogFile;
            textBox3.Text = Properties.Settings.Default.Clear;
            progressBar1.Visible = false;
            ListLog.Setup(listBox1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = null;
            try
            {
                fd = new FolderBrowserDialog();

                fd.SelectedPath = Properties.Settings.Default.Source;
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox1.Text = fd.SelectedPath;

                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = null;
            try
            {
                fd = new FolderBrowserDialog();

                fd.SelectedPath = Properties.Settings.Default.LogFile;
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox2.Text = fd.SelectedPath;

                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }
        }

        public class ListLog
        {
            static ListBox slb = null;
            static DateTime dStart = DateTime.Now;
            static double DT;
            static int cnt;
            public static void Setup(ListBox x)
            {
                slb = x;
                cnt = 1;
            }
            public static void Log(string s, bool newL)
            {
                if (slb != null)
                {
                    if (s == null)
                    {
                        slb.Items.Clear();
                        dStart = DateTime.Now;
                    }
                    else
                    {
                        DT = DateTime.Now.Subtract(dStart).TotalSeconds;
                        if (newL || slb.Items.Count == 0)
                            slb.Items.Add(string.Format("{0,10:F3} => " + s, DT));
                        else
                            slb.Items[slb.Items.Count - 1] = string.Format("{0,10:F3} => " + s, DT);
                        slb.SelectedIndex = slb.Items.Count - 1;
                        if (cnt % 100 == 0)
                            Application.DoEvents();
                        cnt++;
                    }
                }
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            ListLog.Log(null, true);
            groupBox1.Text = "Протокол";
            listBox1.ForeColor = Color.Blue;
            listBox1.BackColor = Color.LightSteelBlue;
            bool bClearExists = !string.IsNullOrEmpty(textBox3.Text);
            listAddition.Clear();
            listBox1.Items.Clear();
            List<string> listSource = (new DirectoryInfo(textBox1.Text)).GetFiles("*.*", SearchOption.AllDirectories).Select(x => x.FullName).ToList();
            List<string> listClear;
            if (bClearExists)
                listClear = (new DirectoryInfo(textBox3.Text)).GetFiles("*.*", SearchOption.AllDirectories).Select(x => x.FullName).ToList();
            else
                listClear = new List<string>();
            DirectoryInfo dir = new DirectoryInfo(textBox2.Text);
            FileInfo[] fil = dir.GetFiles("*.*", SearchOption.AllDirectories);
            progressBar1.Visible = true;
            progressBar1.Maximum = fil.Length;
            progressBar1.Value = 1;
            progressBar1.Step = 1;
            foreach (FileInfo file in fil)
            {
                progressBar1.PerformStep();
                ListLog.Log(file.FullName, true);

                StreamReader sr = new StreamReader(file.FullName);
                try
                {
                    while (true)
                    {
                        string s = sr.ReadLine();
                        if (s == null)
                            break;
                        string[] sa = s.Split(new char[] { ' ', '&' });
                        if (sa.Length < 6)
                            continue;

                        string filePHP = sa[5];

                        if (filePHP.ToLower().EndsWith(".php") || filePHP.ToLower().EndsWith(".tpl") || filePHP.ToLower().EndsWith(".js") || filePHP.ToLower().EndsWith(".css") || filePHP.ToLower().EndsWith(".gif") || filePHP.ToLower().EndsWith(".jpg") || filePHP.ToLower().EndsWith(".ico") || filePHP.ToLower().EndsWith(".png"))
                        {
                            filePHP = filePHP.Replace("/", "\\");
                            if (!listClear.Contains(textBox3.Text + filePHP))
                                if (listSource.Contains(textBox1.Text + filePHP))
                                    if (!listAddition.Contains(filePHP))
                                        listAddition.Add(filePHP);
                        }
                        for (int i = 6; i < sa.Length; i++)
                        {
                            string fileTPL = sa[i];
                            string sExt = ".tpl";
                            if (!fileTPL.ToLower().StartsWith("name_tpl=") && !fileTPL.ToLower().StartsWith("name_php=") && !fileTPL.ToLower().StartsWith("ajax_request_inclusion_file="))
                                continue;
                            if (fileTPL.ToLower().StartsWith("name_php="))
                                sExt = ".php";
                            if (fileTPL.ToLower().StartsWith("ajax_request_inclusion_file="))
                                sExt = ".js";
                            string[] sb = fileTPL.Split(new char[] { '=', '?' });
                            if (sb.Length < 2)
                                continue;
                            fileTPL = sb[1].Replace("%2F", "\\").Replace("/", "\\");
                            if (fileTPL == "")
                                continue;
                            //fileTPL = fileTPL.ToLower();

                            if (!fileTPL.ToLower().EndsWith(".php") && !fileTPL.ToLower().EndsWith(".tpl") && !fileTPL.ToLower().EndsWith(".js"))
                                fileTPL += sExt;
                            if (fileTPL.StartsWith("\\"))
                            {
                                if (!listClear.Contains(textBox3.Text + fileTPL) && listSource.Contains(textBox1.Text + fileTPL))
                                    if (!listAddition.Contains(fileTPL))
                                        listAddition.Add(fileTPL);
                            }
                            else
                            {
                                if (bClearExists)
                                {
                                    DirectoryInfo di = new DirectoryInfo(textBox3.Text + filePHP);
                                    DirectoryInfo di1 = new DirectoryInfo(textBox1.Text + filePHP);

                                    if (!File.Exists(di.FullName.Remove(di.FullName.LastIndexOf(di.Name)) + fileTPL) && File.Exists(di1.FullName.Remove(di1.FullName.LastIndexOf(di1.Name)) + fileTPL))
                                    {
                                        FileInfo fi = new FileInfo(di1.FullName.Remove(di1.FullName.LastIndexOf(di1.Name)) + fileTPL);
                                        if (!listAddition.Contains(fi.FullName.Replace(textBox1.Text, "")))
                                            listAddition.Add(fi.FullName.Replace(textBox1.Text, ""));
                                    }
                                }
                                else
                                {
                                    DirectoryInfo di1 = new DirectoryInfo(textBox1.Text + filePHP);
                                    DirectoryInfo di2 = new DirectoryInfo(di1.FullName.Remove(di1.FullName.LastIndexOf(di1.Name)) + fileTPL);

                                    string sss = di2.FullName;
                                    if (listSource.Contains(sss))
                                    {
                                        if (!listAddition.Contains(sss.Replace(textBox1.Text, "")))
                                            listAddition.Add(sss.Replace(textBox1.Text, ""));
                                    }
                                }
                            }
                        }
                    }
                }
                finally
                {
                    sr.Close();
                }
            }
            groupBox1.Text = "Результаты";
            listAddition.Sort();
            label4.Text = listAddition.Count.ToString();
            listBox1.Items.Clear();
            listBox1.BackColor = Color.DarkRed;
            listBox1.ForeColor = Color.Yellow;
            foreach (string s in listAddition)
            {
                listBox1.Items.Add(s);
            }
            progressBar1.Visible = false;
            Cursor = Cursors.Default;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string savefile = null;
            SaveFileDialog fd = null;
            try
            {
                fd = new SaveFileDialog();
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    savefile = fd.FileName;
                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }
            if (savefile != null)
            {
                if (listAddition.Count > 0)
                {
                    var ls = listAddition.Select(x => x.Trim('\\'));
                    File.WriteAllLines(savefile, ls);
                }
                else
                {
                    File.WriteAllText(savefile, "");
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = null;
            try
            {
                fd = new FolderBrowserDialog();

                fd.SelectedPath = Properties.Settings.Default.Clear;
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox3.Text = fd.SelectedPath;

                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Source = textBox1.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.LogFile = textBox2.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Clear = textBox3.Text;
            Properties.Settings.Default.Save();
        }
    }
}
