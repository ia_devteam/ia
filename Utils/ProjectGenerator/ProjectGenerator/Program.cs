﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Build.Construction;
using System.IO;
using System.Linq;
using System.Xml;

namespace ProjectGenerator
{
    class Program
    {
        static string[] compilers = { "gmcs", "smcs", "dmcs", "mcs" };
        static string configuration = "Debug";
        static string platform = "x64";
        static void Main(string[] args)
        {
            if (args.Count() != 2 || !File.Exists(args[0]) || !Directory.Exists(args[1])) 
            {
                Console.WriteLine("First argument - makefile, second file - outputpath");
                Console.ReadKey();
                return; 
            }

            if (args[0].ToLower().EndsWith("makefile"))
            {
                ProcessMakefile(args);
            }
            else
            {
                ProcessWebConfig(args);
            }
        }

        private static void ProcessWebConfig(string[] args)
        {
            var outputFile = string.Empty;
            var defines = string.Empty;
            var compile = new List<string>();
            var reference = new HashSet<string>();
            var targetFramework = string.Empty;
            using (StreamReader sr = new StreamReader(args[0]))
            {
                var contents = sr.ReadToEnd();
                string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                if (contents.StartsWith(_byteOrderMarkUtf8))
                {
                    contents = contents.Remove(0, _byteOrderMarkUtf8.Length - 1);
                }
                using (XmlReader xRd = XmlReader.Create(new System.IO.StringReader(contents)))
                {
                    while (xRd.Read())
                    {
                        if (xRd.HasAttributes)
                        {
                            var assembly = string.Empty;
                            if ((assembly = xRd.GetAttribute("assembly")) == null)
                            {
                                continue;
                            }

                            if (targetFramework == string.Empty && assembly.IndexOf("Version=") > 0)
                            {
                                //set as the first assembly verison
                                targetFramework = assembly.Substring(assembly.IndexOf("Version=") + ("Version=").Length, 3);//3.5 4.0 - we need three symbols only
                            }
                            assembly = assembly.Split(',')[0];
                            reference.Add(assembly);
                        }
                    }
                }
            }
            Directory.EnumerateFiles(args[1], "*.cs", SearchOption.AllDirectories).ToList().ForEach(cs =>
            {
                compile.Add(Path.GetFullPath(cs).Replace(Path.GetFullPath(args[1] + "\\"), ""));
            });
            var name = "project_web.csproj";
            GenerateSolution(args[1], defines, compile, reference, targetFramework, name);
        }

        private static void ProcessMakefile(string[] args)
        {
            List<string> projectString = new List<string>();
            using (StreamReader sr = new StreamReader(args[0]))
            {
                var str = string.Empty;
                while ((str = sr.ReadLine()) != null)
                {
                    str = str.Trim();
                    if (compilers.Any(g => str.StartsWith(g + " ")))
                    {
                        projectString.Add(str);
                    }
                }
            }

            int i = 0;
            projectString.ForEach(g =>
            {
                var outputFile = string.Empty;
                var defines = string.Empty;
                var compile = new List<string>();
                var reference = new List<string>();
                var targetFramework = string.Empty;

                g.Split(' ').ToList().ForEach(l =>
                {

                    if (l.StartsWith("\""))
                    {
                        //file
                        compile.Add(l.Split('"')[1]);
                    }
                    else if (l.StartsWith("-d:NET_"))
                    {
                        //framework
                        targetFramework = l.Replace("-d:NET_", string.Empty).Replace("_", ".");
                    }
                    else if (l.StartsWith("-out:"))
                    {
                        outputFile = l.Replace("-out:", string.Empty);
                    }
                    else if (l.StartsWith("-define:"))
                    {
                        defines = l.Replace("-define:", string.Empty).Split('"')[1];
                    }
                    else if (l.StartsWith("-reference:"))
                    {
                        reference.Add(l.Replace("-reference:", string.Empty).Split('"')[1]);
                    }
                });

                var name = String.Format("project{0}.csproj", i++);
                GenerateSolution(args[1], defines, compile, reference, targetFramework, name);
            });
        }

        private static void GenerateSolution(string path, string defines, List<string> compile, IEnumerable<string> reference, string targetFramework, string name)
        {
            var root = ProjectRootElement.Create();
            var group = root.AddPropertyGroup();
            group.AddProperty("Configuration", configuration);
            group.AddProperty("Platform", platform);
            group.AddProperty("TargetFrameworkVersion", String.Format("v{0}", targetFramework));
            //group with define symbols
            group = root.AddPropertyGroup();
            group.Condition = string.Format(" '$(Configuration)|$(Platform)' == '{0}|{1}' ", configuration, platform);
            group.AddProperty("DefineConstants", defines);
            group.AddProperty("OutputPath", "\\");
            group.AddProperty("PlatformTarget", platform);
            // references
            AddItems(root, "Reference", reference.ToArray());

            // items to compile
            AddItems(root, "Compile", compile.ToArray());

            var target = root.AddTarget("Build");
            var task = target.AddTask("Csc");
            task.SetParameter("Sources", "@(Compile)");
            task.SetParameter("OutputAssembly", "test.dll");

            root.Save(Path.GetFullPath(string.Format("{0}/{1}", path, name)));
        }

        private static void AddItems(ProjectRootElement elem, string groupName, params string[] items)
        {
            var group = elem.AddItemGroup();
            foreach (var item in items)
            {
                group.AddItem(groupName, item);
            }
        }
    }
}
