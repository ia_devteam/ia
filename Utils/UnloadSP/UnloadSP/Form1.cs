﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using System.Data.SqlClient;
using System.IO;


namespace UnloadSP
{
    public partial class Form1 : Form
    {
        SQLAccess.SQLAccess mSQL;
        public string ConnString;
        public string DatabaseName;
        public static string SavePath="";
        public static List<string> Tables = new List<string>();
        public static List<string> STables = new List<string>();
        List<string> StoredProcedures = new List<string>();
        List<string> Functions = new List<string>();
        List<string> Triggers = new List<string>();
        List<string> DdlTriggers = new List<string>();
        List<StoredProcedure> ListSP = new List<StoredProcedure>();
        List<UserDefinedFunction> ListUDF = new List<UserDefinedFunction>();
        List<Trigger> ListTR = new List<Trigger>();
        List<DatabaseDdlTrigger> ListDdlTrigers = new List<DatabaseDdlTrigger>();
        List<string> ListTableTR = new List<string>();
        public static List<DBProc> ListProc = new List<DBProc>();
        List<DBTable> listTables = new List<DBTable>();

        public Form1()
        {
            InitializeComponent();
            checkBox1.Checked = false;
            checkBox2.Checked = true;
        }

        public void Log(string s, bool newL)
        {
            if (s == null)
                listBox1.Items.Clear();
            else
            {
                if (newL || listBox1.Items.Count == 0)
                    listBox1.Items.Add(s);
                else
                    listBox1.Items[listBox1.Items.Count - 1] = s;
                listBox1.SelectedIndex = listBox1.Items.Count - 1;
            }
        }

        /// <summary>
        /// Настройка соединения с базой данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Form2 fm = new Form2(this, textBox1);
            fm.ShowDialog();
        }

        /// <summary>
        /// Выгрузка объектов базы данных в текстовые файлы.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == null || textBox1.Text == "")
            {
                MessageBox.Show("Не сформировано соединение с базой", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            FolderBrowserDialog fd = null;
            try
            {
                fd = new FolderBrowserDialog();
                fd.Description = "Укажите каталог для сохранения файлов выгрузки";
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox2.Text = fd.SelectedPath;
                    SavePath = textBox2.Text;
                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }
            if (fd == null)
                return;
            Application.DoEvents();
            Cursor = Cursors.WaitCursor;
            Log(null, false);
            Tables.Clear();
            STables.Clear();
            StoredProcedures.Clear();
            Functions.Clear();

            ListSP.Clear();
            ListUDF.Clear();
            ListTR.Clear();
            ListTableTR.Clear();
            ListProc.Clear();
            int CountProc=1;

            mSQL = new SQLAccess.SQLAccess(textBox1.Text);

            Log("Таблицы", true);
            StringBuilder sTables = new StringBuilder();
            if (!Directory.Exists(textBox2.Text + "\\Tables"))
            {
                Directory.CreateDirectory(textBox2.Text + "\\Tables");
            }

            TableCollection tbl = (new Server(new ServerConnection(new SqlConnection(ConnString)))).Databases[DatabaseName].Tables;
            foreach (Table t in tbl)
            {
                //if(!t.IsSystemObject)
                Tables.Add(t.ToString());
                string shortname="";
                if (t.ToString().Contains("."))
                {
                    shortname=t.ToString().Split('.')[1].Trim(new char[] { '[', ']' });
                    STables.Add(shortname);
                }
                else
                {
                    shortname = t.ToString().Trim(new char[] { '[', ']' });
                    STables.Add(shortname);
                }
                DBTable dbt = new DBTable(1, t.ToString(), shortname);
                listTables.Add(dbt);
                sTables.Append(Environment.NewLine + t.ToString() + Environment.NewLine);
                string colname = "";
                string coltype = "";
                int colcnt=0;
                foreach (Column c in t.Columns)
                {
                    colname = c.Name;
                    if (c.DataType.ToString().Contains("char"))
                    {
                        if (c.DataType.MaximumLength == -1)
                        {
                            coltype = c.DataType + "(max)";
                            sTables.Append("    " + c.Name + " " + c.DataType + "(max)"+ Environment.NewLine);
                        }
                        else
                        {
                            coltype = c.DataType + "(" + c.DataType.MaximumLength + ")";
                            sTables.Append("    " + c.Name + " " + c.DataType + "(" + c.DataType.MaximumLength + ")"+ Environment.NewLine);
                        }
                    }
                    else
                    {
                        coltype = c.DataType.ToString();
                        sTables.Append("    " + c.Name + " " + c.DataType + Environment.NewLine);
                    }
                    dbt.ColumnsT.Add(new DBVar(colcnt++, colname, coltype));
                }
                string sSchema;
                string sSPName;
                try
                {
                    sSchema = t.ToString().Split('.')[0].Trim(new char[] { '[', ']' });
                    sSPName = t.ToString().Split('.')[1].Trim(new char[] { '[', ']' });
                }
                catch (Exception)
                {
                    return;
                }
                if (checkBox2.Checked)
                {
                    List<string[]> lstblContent = mSQL.Go("select * from " + t.ToString());
                    List<string> tableContent = lstblContent.Select(x => x.Aggregate((a, b) => a + "|" + b)).ToList();
                    if (tableContent.Count > 0)
                        File.WriteAllLines(textBox2.Text + "\\Tables\\" + t.ToString() + ".txt", tableContent);
                    else
                        File.WriteAllText(textBox2.Text + "\\Tables\\" + t.ToString() + ".txt", "");

                    Log("    Tables\\" + t.ToString() + ".txt", true);
                }

                foreach (Trigger tr in t.Triggers)
                {
                    ListTR.Add(tr);
                    ListTableTR.Add(t.ToString());
                    if (!tr.IsEncrypted && !tr.IsSystemObject)
                    {
                        
                        StringCollection sc = null;
                        StringBuilder sBody = new StringBuilder();
                        try
                        {
                            sc = tr.Script();
                        }
                        catch (Exception)
                        {
                        }
                        if (sc == null)
                            continue;
                        foreach (string ss in sc)
                        {
                            sBody.Append(ss + Environment.NewLine);
                        }

                        DBTrig dbtr = new DBTrig(CountProc, tr.Name, "Триггер", sBody.ToString(), t.ToString());
                        CountProc++;
                        int CountVars = 1;
                        if (tr.Insert)
                            dbtr.listParams.Add(new DBVar(CountVars++, "INSERT", ""));
                        if (tr.Update)
                            dbtr.listParams.Add(new DBVar(CountVars++, "UPDATE", ""));
                        if (tr.Delete)
                            dbtr.listParams.Add(new DBVar(CountVars++, "DELETE", ""));
                        ListProc.Add(dbtr);

                        if (!Directory.Exists(textBox2.Text + "\\" + sSchema + "\\tr\\" + sSPName))
                        {
                            Directory.CreateDirectory(textBox2.Text + "\\" + sSchema + "\\tr\\" + sSPName);
                        }
                        File.WriteAllText(textBox2.Text + "\\" + sSchema + "\\tr\\" + sSPName + "\\" + tr.Name + ".sql", sBody.ToString());
                        Log("    " + sSchema + "\\tr\\" + sSPName + "\\" + tr.Name + ".sql", true);
                    }

                }
            }
            if (!Directory.Exists(textBox2.Text))
            {
                Directory.CreateDirectory(textBox2.Text);
            }
            File.WriteAllText(textBox2.Text + "\\SqlTables.txt", sTables.ToString());
            Log("    " + textBox2.Text + "\\SqlTables.txt", true);

            Log("Таблицы "+Tables.Count, true);
            Log("Триггеры " + ListTR.Count, true);
            Log("Хранимые процедуры", true);
            StoredProcedureCollection spc = (new Server(new ServerConnection(new SqlConnection(ConnString)))).Databases[DatabaseName].StoredProcedures;
            foreach (StoredProcedure sp in spc)
            {
                if (!sp.IsEncrypted && !sp.IsSystemObject)
                {
                    StringBuilder sBody = new StringBuilder();
                    StoredProcedures.Add(sp.ToString());
                    ListSP.Add(sp);
                    string sSchema;
                    string sSPName;
                    try
                    {
                        sSchema = sp.ToString().Split('.')[0].Trim(new char[] { '[', ']' });
                        sSPName = sp.ToString().Split('.')[1].Trim(new char[] { '[', ']' });
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    StringCollection sc = null;
                    try
                    {
                        sc = sp.Script();
                    }
                    catch (Exception)
                    {
                    }
                    if (sc == null)
                        continue;
                    foreach (string ss in sc)
                    {
                        sBody.Append(ss + Environment.NewLine);
                    }

                    DBProc dbpr = new DBProc(CountProc, sp.Name, "Хранимая процедура", sBody.ToString(), sp.ToString());
                    CountProc++;
                    int CountVars = 1;
                    foreach (StoredProcedureParameter spp in sp.Parameters)
                    {
                        string sOut = "";
                        if (spp.IsOutputParameter)
                            sOut = " out";
                        if (spp.DataType.ToString().Contains("char"))
                        {
                            if (spp.DataType.MaximumLength == -1)
                                dbpr.listParams.Add(new DBVar(CountVars++, spp.Name, spp.DataType + "(max)" + sOut));
                            else
                                dbpr.listParams.Add(new DBVar(CountVars++, spp.Name, spp.DataType + "(" + spp.DataType.MaximumLength + ")" + sOut));
                        }
                        else
                            dbpr.listParams.Add(new DBVar(CountVars++, spp.Name, spp.DataType + sOut));
                    }
                    ListProc.Add(dbpr);


                    if (!Directory.Exists(textBox2.Text + "\\" + sSchema + "\\sp"))
                    {
                        Directory.CreateDirectory(textBox2.Text + "\\" + sSchema + "\\sp");
                    }
                    File.WriteAllText(textBox2.Text + "\\" + sSchema + "\\sp\\" + sSPName + ".sql", sBody.ToString());
                    Log("    " + textBox2.Text + "\\" + sSchema + "\\sp\\" + sSPName + ".sql", true);

                }

            }
            Log("Хранимые процедуры " + StoredProcedures.Count, true);
            Log("Функции", true);
            UserDefinedFunctionCollection usps = (new Server(new ServerConnection(new SqlConnection(ConnString)))).Databases[DatabaseName].UserDefinedFunctions;
            foreach (UserDefinedFunction usp in usps)
            {
                if (!usp.IsEncrypted && !usp.IsSystemObject)
                {
                    Functions.Add(usp.ToString());
                    ListUDF.Add(usp);
                    StringBuilder sBody = new StringBuilder();
                    string sSchema;
                    string sSPName;
                    try
                    {
                        sSchema = usp.ToString().Split('.')[0].Trim(new char[] { '[', ']' });
                        sSPName = usp.ToString().Split('.')[1].Trim(new char[] { '[', ']' });
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    StringCollection sc = null;
                    try
                    {
                        sc = usp.Script();
                    }
                    catch (Exception)
                    {
                    }
                    if (sc == null)
                        continue;
                    foreach (string ss in sc)
                    {
                        sBody.Append(ss + Environment.NewLine);
                    }

                    DBProc dbpr = new DBProc(CountProc, usp.Name, "Функция", sBody.ToString(), usp.ToString());
                    CountProc++;
                    int CountVars = 1;
                    foreach (UserDefinedFunctionParameter spp in usp.Parameters)
                    {
                        if (spp.DataType.ToString().Contains("char"))
                        {
                            if (spp.DataType.MaximumLength == -1)
                                dbpr.listParams.Add(new DBVar(CountVars++, spp.Name, spp.DataType + "(max)"));
                            else
                                dbpr.listParams.Add(new DBVar(CountVars++, spp.Name, spp.DataType + "(" + spp.DataType.MaximumLength + ")"));
                        }
                        else
                            dbpr.listParams.Add(new DBVar(CountVars++, spp.Name, spp.DataType.ToString()));
                    }
                    ListProc.Add(dbpr);


                    if (!Directory.Exists(textBox2.Text + "\\" + sSchema + "\\fn"))
                    {
                        Directory.CreateDirectory(textBox2.Text + "\\" + sSchema + "\\fn");
                    }
                    File.WriteAllText(textBox2.Text + "\\" + sSchema + "\\fn\\" + sSPName + ".sql", sBody.ToString());
                    Log("    " + textBox2.Text + "\\" + sSchema + "\\fn\\" + sSPName + ".sql", true);

                }
            }
            Log("Функции " + Functions.Count, true);

            Log("DLL Триггеры", true);

            DatabaseDdlTriggerCollection ddlTriggers = (new Server(new ServerConnection(new SqlConnection(ConnString)))).Databases[DatabaseName].Triggers;
            foreach (DatabaseDdlTrigger ddltr in ddlTriggers)
            {
                if (!ddltr.IsEncrypted || !ddltr.IsSystemObject)
                {
                    ListDdlTrigers.Add(ddltr);
                    StringBuilder sBody = new StringBuilder();
                    string sSchema;
                    string sSPName;
                    try
                    {
                        sSchema = ddltr.ToString().Split('.')[0].Trim(new char[] { '[', ']' });
                        sSPName = ddltr.ToString().Split('.')[1].Trim(new char[] { '[', ']' });
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    StringCollection sc = null;
                    try
                    {
                        sc = ddltr.Script();
                    }
                    catch (Exception)
                    {
                    }
                    if (sc == null)
                        continue;
                    foreach (string ss in sc)
                    {
                        sBody.Append(ss + Environment.NewLine);
                    }

                    DBTrig dbtr = new DBTrig(CountProc, ddltr.Name, "DDL Триггер", sBody.ToString(), "");
                    CountProc++;
                    int CountVars = 1;
                    DatabaseDdlTriggerEventSet esddltr = ddltr.DdlTriggerEvents;
                    if (esddltr.AddRoleMember)
                        dbtr.listParams.Add(new DBVar(CountVars++,"AddRoleMember",""));
                    if (esddltr.AddSignature)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AddSignature", ""));
                    if (esddltr.AddSignatureSchemaObject)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AddSignatureSchemaObject", ""));
                    if (esddltr.AlterApplicationRole)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterApplicationRole", ""));
                    if (esddltr.AlterAssembly)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterAssembly", ""));
                    if (esddltr.AlterAsymmetricKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterAsymmetricKey", ""));
                    if (esddltr.AlterAuthorizationDatabase)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterAuthorizationDatabase", ""));
                    if (esddltr.AlterBrokerPriority)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterBrokerPriority", ""));
                    if (esddltr.AlterCertificate)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterCertificate", ""));
                    if (esddltr.AlterDatabaseAuditSpecification)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterDatabaseAuditSpecification", ""));
                    if (esddltr.AlterDatabaseEncryptionKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterDatabaseEncryptionKey", ""));
                    if (esddltr.AlterExtendedProperty)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterExtendedProperty", ""));
                    if (esddltr.AlterFulltextCatalog)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterFulltextCatalog", ""));
                    if (esddltr.AlterFulltextIndex)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterFulltextIndex", ""));
                    if (esddltr.AlterFulltextStoplist)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterFulltextStoplist", ""));
                    if (esddltr.AlterFunction)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterFunction", ""));
                    if (esddltr.AlterIndex)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterIndex", ""));
                    if (esddltr.AlterMasterKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterMasterKey", ""));
                    if (esddltr.AlterMessageType)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterMessageType", ""));
                    if (esddltr.AlterPartitionFunction)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterPartitionFunction", ""));
                    if (esddltr.AlterPartitionScheme)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterPartitionScheme", ""));
                    if (esddltr.AlterPlanGuide)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterPlanGuide", ""));
                    if (esddltr.AlterProcedure)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterProcedure", ""));
                    if (esddltr.AlterQueue)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterQueue", ""));
                    if (esddltr.AlterRemoteServiceBinding)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterRemoteServiceBinding", ""));
                    if (esddltr.AlterRole)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterRole", ""));
                    if (esddltr.AlterRoute)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterRoute", ""));
                    if (esddltr.AlterSchema)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterSchema", ""));
                    if (esddltr.AlterService)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterService", ""));
                    if (esddltr.AlterSymmetricKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterSymmetricKey", ""));
                    if (esddltr.AlterTable)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterTable", ""));
                    if (esddltr.AlterTrigger)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterTrigger", ""));
                    if (esddltr.AlterUser)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterUser", ""));
                    if (esddltr.AlterView)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterView", ""));
                    if (esddltr.AlterXmlSchemaCollection)
                        dbtr.listParams.Add(new DBVar(CountVars++, "AlterXmlSchemaCollection", ""));
                    if (esddltr.BindDefault)
                        dbtr.listParams.Add(new DBVar(CountVars++, "BindDefault", ""));
                    if (esddltr.BindRule)
                        dbtr.listParams.Add(new DBVar(CountVars++, "BindRule", ""));
                    if (esddltr.CreateApplicationRole)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateApplicationRole", ""));
                    if (esddltr.CreateAssembly)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateAssembly", ""));
                    if (esddltr.CreateAsymmetricKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateAsymmetricKey", ""));
                    if (esddltr.CreateBrokerPriority)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateBrokerPriority", ""));
                    if (esddltr.CreateCertificate)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateCertificate", ""));
                    if (esddltr.CreateContract)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateContract", ""));
                    if (esddltr.CreateDatabaseAuditSpecification)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateDatabaseAuditSpecification", ""));
                    if (esddltr.CreateDatabaseEncryptionKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateDatabaseEncryptionKey", ""));
                    if (esddltr.CreateDefault)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateDefault", ""));
                    if (esddltr.CreateEventNotification)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateEventNotification", ""));
                    if (esddltr.CreateExtendedProperty)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateExtendedProperty", ""));
                    if (esddltr.CreateFulltextCatalog)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateFulltextCatalog", ""));
                    if (esddltr.CreateFulltextIndex)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateFulltextIndex", ""));
                    if (esddltr.CreateFulltextStoplist)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateFulltextStoplist", ""));
                    if (esddltr.CreateFunction)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateFunction", ""));
                    if (esddltr.CreateIndex)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateFunction", ""));
                    if (esddltr.CreateMasterKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateMasterKey", ""));
                    if (esddltr.CreateMessageType)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateMessageType", ""));
                    if (esddltr.CreatePartitionFunction)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreatePartitionFunction", ""));
                    if (esddltr.CreatePartitionScheme)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreatePartitionScheme", ""));
                    if (esddltr.CreatePlanGuide)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreatePlanGuide", ""));
                    if (esddltr.CreateProcedure)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateProcedure", ""));
                    if (esddltr.CreateQueue)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateQueue", ""));
                    if (esddltr.CreateRemoteServiceBinding)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateRemoteServiceBinding", ""));
                    if (esddltr.CreateRole)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateRole", ""));
                    if (esddltr.CreateRoute)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateRoute", ""));
                    if (esddltr.CreateRule)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateRule", ""));
                    if (esddltr.CreateSchema)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateSchema", ""));
                    if (esddltr.CreateService)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateService", ""));
                    if (esddltr.CreateSpatialIndex)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateSpatialIndex", ""));
                    if (esddltr.CreateStatistics)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateStatistics", ""));
                    if (esddltr.CreateSymmetricKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateSymmetricKey", ""));
                    if (esddltr.CreateSynonym)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateSynonym", ""));
                    if (esddltr.CreateTable)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateTable", ""));
                    if (esddltr.CreateTrigger)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateTrigger", ""));
                    if (esddltr.CreateType)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateType", ""));
                    if (esddltr.CreateUser)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateUser", ""));
                    if (esddltr.CreateView)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateView", ""));
                    if (esddltr.CreateXmlIndex)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateXmlIndex", ""));
                    if (esddltr.CreateXmlSchemaCollection)
                        dbtr.listParams.Add(new DBVar(CountVars++, "CreateXmlSchemaCollection", ""));
                    if (esddltr.DdlApplicationRoleEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlApplicationRoleEventsEvents", ""));
                    if (esddltr.DdlAssemblyEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlAssemblyEventsEvents", ""));
                    if (esddltr.DdlAsymmetricKeyEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlAsymmetricKeyEventsEvents", ""));
                    if (esddltr.DdlAuthorizationDatabaseEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlAuthorizationDatabaseEventsEvents", ""));
                    if (esddltr.DdlBrokerPriorityEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlBrokerPriorityEventsEvents", ""));
                    if (esddltr.DdlCertificateEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlCertificateEventsEvents", ""));
                    if (esddltr.DdlContractEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlContractEventsEvents", ""));
                    if (esddltr.DdlCryptoSignatureEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlCryptoSignatureEventsEvents", ""));
                    if (esddltr.DdlDatabaseAuditSpecificationEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlDatabaseAuditSpecificationEventsEvents", ""));
                    if (esddltr.DdlDatabaseEncryptionKeyEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlDatabaseEncryptionKeyEventsEvents", ""));
                    if (esddltr.DdlDatabaseLevelEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlDatabaseLevelEventsEvents", ""));
                    if (esddltr.DdlDatabaseSecurityEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlDatabaseSecurityEventsEvents", ""));
                    if (esddltr.DdlDefaultEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlDefaultEventsEvents", ""));
                    if (esddltr.DdlEventNotificationEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlEventNotificationEventsEvents", ""));
                    if (esddltr.DdlExtendedPropertyEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlExtendedPropertyEventsEvents", ""));
                    if (esddltr.DdlFulltextCatalogEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlFulltextCatalogEventsEvents", ""));
                    if (esddltr.DdlFulltextStoplistEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlFulltextStoplistEventsEvents", ""));
                    if (esddltr.DdlFunctionEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlFunctionEventsEvents", ""));
                    if (esddltr.DdlGdrDatabaseEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlGdrDatabaseEventsEvents", ""));
                    if (esddltr.DdlIndexEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlIndexEventsEvents", ""));
                    if (esddltr.DdlMasterKeyEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlMasterKeyEventsEvents", ""));
                    if (esddltr.DdlMessageTypeEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlMessageTypeEventsEvents", ""));
                    if (esddltr.DdlPartitionEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlPartitionEventsEvents", ""));
                    if (esddltr.DdlPartitionFunctionEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlPartitionFunctionEventsEvents", ""));
                    if (esddltr.DdlPartitionSchemeEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlPartitionSchemeEventsEvents", ""));
                    if (esddltr.DdlPlanGuideEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlPlanGuideEventsEvents", ""));
                    if (esddltr.DdlProcedureEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlProcedureEventsEvents", ""));
                    if (esddltr.DdlQueueEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlQueueEventsEvents", ""));
                    if (esddltr.DdlRemoteServiceBindingEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlRemoteServiceBindingEventsEvents", ""));
                    if (esddltr.DdlRoleEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlRoleEventsEvents", ""));
                    if (esddltr.DdlRuleEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlRuleEventsEvents", ""));
                    if (esddltr.DdlSchemaEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlSchemaEventsEvents", ""));
                    if (esddltr.DdlServiceEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlServiceEventsEvents", ""));
                    if (esddltr.DdlSsbEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlSsbEventsEvents", ""));
                    if (esddltr.DdlStatisticsEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlStatisticsEventsEvents", ""));
                    if (esddltr.DdlSymmetricKeyEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlSymmetricKeyEventsEvents", ""));
                    if (esddltr.DdlSynonymEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlSynonymEventsEvents", ""));
                    if (esddltr.DdlTableEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlTableEventsEvents", ""));
                    if (esddltr.DdlTableViewEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlTableViewEventsEvents", ""));
                    if (esddltr.DdlTriggerEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlTriggerEventsEvents", ""));
                    if (esddltr.DdlTypeEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlTypeEventsEvents", ""));
                    if (esddltr.DdlUserEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlUserEventsEvents", ""));
                    if (esddltr.DdlViewEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlViewEventsEvents", ""));
                    if (esddltr.DdlXmlSchemaCollectionEventsEvents)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DdlXmlSchemaCollectionEventsEvents", ""));
                    if (esddltr.DenyDatabase)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DenyDatabase", ""));
                    if (esddltr.Dirty)
                        dbtr.listParams.Add(new DBVar(CountVars++, "Dirty", ""));
                    if (esddltr.DropApplicationRole)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropApplicationRole", ""));
                    if (esddltr.DropAssembly)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropAssembly", ""));
                    if (esddltr.DropAsymmetricKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropAsymmetricKey", ""));
                    if (esddltr.DropBrokerPriority)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropBrokerPriority", ""));
                    if (esddltr.DropCertificate)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropCertificate", ""));
                    if (esddltr.DropContract)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropContract", ""));
                    if (esddltr.DropDatabaseAuditSpecification)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropDatabaseAuditSpecification", ""));
                    if (esddltr.DropDatabaseEncryptionKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropDatabaseEncryptionKey", ""));
                    if (esddltr.DropDefault)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropDefault", ""));
                    if (esddltr.DropEventNotification)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropEventNotification", ""));
                    if (esddltr.DropExtendedProperty)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropExtendedProperty", ""));
                    if (esddltr.DropFulltextCatalog)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropFulltextCatalog", ""));
                    if (esddltr.DropFulltextIndex)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropFulltextIndex", ""));
                    if (esddltr.DropFulltextStoplist)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropFulltextStoplist", ""));
                    if (esddltr.DropFunction)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropFunction", ""));
                    if (esddltr.DropIndex)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropIndex", ""));
                    if (esddltr.DropMasterKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropMasterKey", ""));
                    if (esddltr.DropMessageType)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropMessageType", ""));
                    if (esddltr.DropPartitionFunction)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropPartitionFunction", ""));
                    if (esddltr.DropPartitionScheme)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropPartitionScheme", ""));
                    if (esddltr.DropPlanGuide)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropPlanGuide", ""));
                    if (esddltr.DropProcedure)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropProcedure", ""));
                    if (esddltr.DropQueue)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropQueue", ""));
                    if (esddltr.DropRemoteServiceBinding)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropRemoteServiceBinding", ""));
                    if (esddltr.DropRole)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropRole", ""));
                    if (esddltr.DropRoleMember)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropRoleMember", ""));
                    if (esddltr.DropRoute)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropRoute", ""));
                    if (esddltr.DropRule)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropRule", ""));
                    if (esddltr.DropSchema)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropSchema", ""));
                    if (esddltr.DropService)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropService", ""));
                    if (esddltr.DropSignature)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropSignature", ""));
                    if (esddltr.DropSignatureSchemaObject)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropSignatureSchemaObject", ""));
                    if (esddltr.DropStatistics)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropStatistics", ""));
                    if (esddltr.DropSymmetricKey)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropSymmetricKey", ""));
                    if (esddltr.DropSynonym)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropSynonym", ""));
                    if (esddltr.DropTable)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropTable", ""));
                    if (esddltr.DropTrigger)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropTrigger", ""));
                    if (esddltr.DropType)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropType", ""));
                    if (esddltr.DropUser)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropUser", ""));
                    if (esddltr.DropView)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropView", ""));
                    if (esddltr.DropXmlSchemaCollection)
                        dbtr.listParams.Add(new DBVar(CountVars++, "DropXmlSchemaCollection", ""));
                    if (esddltr.GrantDatabase)
                        dbtr.listParams.Add(new DBVar(CountVars++, "GrantDatabase", ""));
                    if (esddltr.Rename)
                        dbtr.listParams.Add(new DBVar(CountVars++, "Rename", ""));
                    if (esddltr.RevokeDatabase)
                        dbtr.listParams.Add(new DBVar(CountVars++, "RevokeDatabase", ""));
                    if (esddltr.UnbindDefault)
                        dbtr.listParams.Add(new DBVar(CountVars++, "UnbindDefault", ""));
                    if (esddltr.UnbindRule)
                        dbtr.listParams.Add(new DBVar(CountVars++, "UnbindRule", ""));
                    if (esddltr.UpdateStatistics)
                        dbtr.listParams.Add(new DBVar(CountVars++, "UpdateStatistics", ""));
                    
                    ListProc.Add(dbtr);


                    if (!Directory.Exists(textBox2.Text + "\\" + sSchema + "\\ddltr"))
                    {
                        Directory.CreateDirectory(textBox2.Text + "\\" + sSchema + "\\ddltr");
                    }
                    File.WriteAllText(textBox2.Text + "\\" + sSchema + "\\ddltr\\" + sSPName + ".sql", sBody.ToString());
                    Log("    " + textBox2.Text + "\\" + sSchema + "\\ddltr\\" + sSPName + ".sql", true);

                }
            }

            Log("DLL Триггеры " + ListDdlTrigers.Count, true);

            Log("Выгрузка", true);
            string sReport = "";
            foreach (DBProc dbp in ListProc)
            {
                if (dbp.TypeP == "Триггер")
                {
                    DBTrig dbt = dbp as DBTrig;
                    sReport += "\n" + dbt.TypeP + " " + dbt.TableN + ".[" + dbt.NameP + "]\n";
                }
                else
                    sReport += "\n" + dbp.TypeP + " " + dbp.NameP + "\n";
                if(dbp.TypeP.Contains("Триггер"))
                    sReport += "    События: " + "\n";
                else
                    sReport += "    Параметры: " + "\n";
                foreach (DBVar v in dbp.listParams)
                {
                    sReport += "               " + v.NameV + " " + v.FormatV + "\n";
                }
            }

            File.WriteAllText(textBox2.Text + "\\Список процедур.txt", sReport);
            Log("    " + textBox2.Text + "\\Список процедур.txt", true);
            Log("Выгрузка завершена", true);
            Cursor = Cursors.Default;
        }

        /// <summary>
        /// Удаление кмментариев из t-sql скрипта
        /// </summary>
        /// <param name="script">скрипт</param>
        /// <returns>скрипт без комментариев</returns>
        public static string RemoveComments(string script)
        {
            string Ret = "";
            int start = 0;
            int end = -1;
            int tmp = 0;
            while (start != -1)
            {
                end = script.IndexOf("/*",start);
                if (end != -1)
                {
                    Ret += script.Substring(start, end - start);
                    start = end + 2;
                    end = script.IndexOf("*/", start);
                    if (end == -1)
                    {
                        // Ошибка t-sql
                        break;
                    }
                    tmp = start-2;
                    while (true)
                    {
                        int tmp0 = tmp+2;
                        tmp = script.Substring(tmp0, end - tmp0).IndexOf("/*");
                        if (tmp != -1)
                        {
                            tmp += tmp0;
                            end = script.IndexOf("*/", end+2);
                            if (end == -1)
                            {
                                // Ошибка t-sql
                                break;
                            }
                        }
                        else
                            break;
                    }
                    start = end + 2;
                }
                else
                {
                    Ret+=script.Substring(start);
                    break;
                }
            }
            // --
            string nocomm = "";
            start = 0;
            end = -1;
            while (true)
            {
                end = Ret.IndexOf("--", start);
                if (end != -1)
                {
                    nocomm += Ret.Substring(start, end - start);
                    start = end+2;
                    end = Ret.IndexOf("\n", start);
                    if (end == -1)
                    {
                        // Ошибка t-sql
                        break;
                    }
                    start = end;
                }
                else
                {
                    nocomm += Ret.Substring(start);
                    break;
                }
            }
            return nocomm;
        }

        /// <summary>
        /// Удаление кмментариев и заголовка из t-sql скрипта
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        public static string RemoveCommentsAndHeader(string script)
        {
            string nocomments = RemoveComments(script);
            int start = nocomments.IndexOf("create", 0, StringComparison.InvariantCultureIgnoreCase);
            if (start > 0)
            {
                start = nocomments.IndexOf("as", start, StringComparison.InvariantCultureIgnoreCase);
                if (start > 0)
                {
                    start = nocomments.IndexOf("begin", start, StringComparison.InvariantCultureIgnoreCase);
                    if(start>0)
                        return nocomments.Substring(start);
                    else
                        return nocomments;
                }
                else
                    return nocomments;
            }
            else
                return nocomments;
        }

        /// <summary>
        /// Переменные
        /// </summary>
        public class DBVar
        {
            int ID;
            public string NameV;
            public string FormatV;
            public DBVar(int id, string n, string f)
            {
                ID = id;
                NameV = n;
                FormatV = f;
            }
        }

        /// <summary>
        /// Процедуры
        /// </summary>
        public class DBProc
        {
            public int ID;
            public string NameP;
            public string TypeP;
            public string ScriptP;
            public string FullName;
            public DBVar RetVal;
            public List<DBVar> listParams;
            public List<DBVar> listLocal;
            public List<DBVar> listGlobal;
            public List<DBVar> listTables;
            public List<Calling> listCalling;
            public DBProc(int id, string n, string t, string s, string f)
            {
                ID = id;
                NameP = n;
                TypeP = t;
                ScriptP = s;
                RetVal = null;
                FullName = f;
                listParams = new List<DBVar>();
                listLocal = new List<DBVar>();
                listGlobal = new List<DBVar>();
                listTables = new List<DBVar>();
                listCalling = new List<Calling>();
            }
        }

        /// <summary>
        /// Вызываемые процедуры
        /// </summary>
        public class Calling
        {
            public string FullName;
            public string Params;
            public int LineOrPos;
            public string TypeP;
            public Calling(string f, string p, string t, int l)
            {
                FullName = f;
                Params = p;
                TypeP = t;
                LineOrPos = l;
            }
        }

        /// <summary>
        /// Триггеры
        /// </summary>
        public class DBTrig : DBProc
        {
            public string TableN;
            public DBTrig(int id, string n, string t, string s, string tn) :base(id,n,t,s,tn+".["+n+"]")
            {
                TableN = tn;
            }
        }

        /// <summary>
        /// Таблицы
        /// </summary>
        public class DBTable
        {
            public int ID;
            public string NameT;
            public string NameS;
            public List<DBVar> ColumnsT;
            public DBTable(int id, string n,string s)
            {
                ID = id;
                NameT = n;
                NameS = s;
                ColumnsT = new List<DBVar>();
            }
        }

        /// <summary>
        /// Разбор выгруженных ткстовых файлов.
        /// Построение функциональных зависимостей.
        /// Связи функциональных объектов по управлению.
        /// Информационные объекты процедур.
        /// Связи функциональных объектов по информации.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == null || textBox2.Text == "")
            {
                MessageBox.Show("Не выполнена выгрузка из базы данных", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            ClearDepends(ListProc);
            Depends dp = new Depends(this);
            Log("Построение функциональных зависимостей", true);
            dp.DependsOfProc(ListProc);
            Log("Построение глобальных переменных", true);
            dp.CollectGlobalVars(ListProc);
            Log("Построение локальных переменных", true);
            dp.CollectLocalVars(ListProc);
            Log("Построение используемых таблиц", true);
            dp.CollectTables(ListProc, listTables);

            Log("Отчет: Связи функциональных объектов по управлению", true);
            ReportFuncDepends(ListProc);
            Log("Отчет: Информационные объекты процедур", true);
            ReportObjectInfo(ListProc);
            Log("Отчет: Связь функц. объектов по информации", true);
            ReportObjectDepends(ListProc);
            // Старые отчеты от Н.Г.
            if (checkBox1.Checked)
            {
                Analiz an = new Analiz(this);
                Log("Создание PrimaryDB", true);
                an.MakePrimaryDB();
                Log("Создание SecDB", true);
                an.MakeSecDB();
                Log("Создание Отчетов", true);
                an.MakeReport();
            }
            Log("Завершено", true);
        }
        /// <summary>
        /// Отчет по функц. зависимостям
        /// </summary>
        /// <param name="ls"></param>
        void ReportFuncDepends(List<DBProc> ls)
        {
            StringBuilder Report = new StringBuilder();
            Report.Append("Связи функциональных объектов по управлению" + Environment.NewLine + Environment.NewLine);
            foreach (var x in ls)
            {
                if (x.listCalling.Count > 0)
                {
                    if (x.TypeP == "Триггер")
                    {
                        Report.Append(Environment.NewLine + "Вызывающий триггер " + x.FullName + Environment.NewLine + Environment.NewLine);
                    }
                    else if (x.TypeP == "Функция")
                    {
                        Report.Append(Environment.NewLine + "Вызывающая функция " + x.FullName + Environment.NewLine + Environment.NewLine);
                    }
                    else
                    {
                        Report.Append(Environment.NewLine + "Вызывающая процедура " + x.FullName + Environment.NewLine + Environment.NewLine);
                    }

                    foreach (var y in x.listCalling)
                    {
                        if (y.TypeP == "Триггер")
                        {
                            Report.Append("\t" + "Вызываемый триггер " + y.FullName + Environment.NewLine);
                        }
                        else if (y.TypeP == "Функция")
                        {
                            Report.Append("\t" + "Вызываемая функция " + y.FullName + Environment.NewLine);
                        }
                        else
                        {
                            Report.Append("\t" + "Вызываемая процедура " + y.FullName + Environment.NewLine);
                        }

                        Report.Append("\t\t" + y.LineOrPos + "\t" + "Параметры вызова: " + y.Params + Environment.NewLine);
                    }
                    Report.Append(Environment.NewLine);
                }
            }
            System.IO.File.WriteAllText(Form1.SavePath + "\\Связи функциональных объектов по управлению.txt", Report.ToString());

        }
        /// <summary>
        /// Отчет по информационным объектам
        /// </summary>
        /// <param name="ls"></param>
        void ReportObjectInfo(List<DBProc> ls)
        {
            StringBuilder Report = new StringBuilder();
            Report.Append("Информационные объекты процедур" + Environment.NewLine + Environment.NewLine);

            foreach (var x in ls)
            {
                if (x.TypeP == "Триггер")
                {
                    Report.Append("Триггер " + x.FullName + Environment.NewLine);
                }
                else if (x.TypeP == "Функция")
                {
                    Report.Append("Функция " + x.FullName + Environment.NewLine);
                }
                else
                {
                    Report.Append("Процедура " + x.FullName + Environment.NewLine);
                }
                Report.Append("\t" + "Информационные объекты" + Environment.NewLine);
                if (x.TypeP == "Триггер")
                {
                    Report.Append("\t\t" + "События:" + Environment.NewLine);
                }
                else
                {
                    Report.Append("\t\t" + "Параметры:" + Environment.NewLine);
                }
                foreach (var t in x.listParams)
                {
                    Report.Append("\t\t\t" + t.NameV + "\t" + t.FormatV + Environment.NewLine);
                }
                Report.Append("\t\t" + "Таблицы:" + Environment.NewLine);
                foreach (var t in x.listTables)
                {
                    Report.Append("\t\t\t" + t.NameV + Environment.NewLine);
                }
                Report.Append("\t\t" + "Глобальные переменные:" + Environment.NewLine);
                foreach (var t in x.listGlobal)
                {
                    Report.Append("\t\t\t" + t.NameV + "\t" + t.FormatV + Environment.NewLine);
                }
                Report.Append("\t\t" + "Локальные переменные:"  + Environment.NewLine);
                foreach (var t in x.listLocal)
                {
                    Report.Append("\t\t\t" + t.NameV + "\t" + t.FormatV + Environment.NewLine);
                }
                Report.Append(Environment.NewLine);
            }
            System.IO.File.WriteAllText(Form1.SavePath + "\\Информационные объекты процедур.txt", Report.ToString());

        }
        /// <summary>
        /// Отчет по информационным зависимостям
        /// </summary>
        /// <param name="ls"></param>
        void ReportObjectDepends(List<DBProc> ls)
        {
            StringBuilder Report = new StringBuilder();
            Report.Append("Связи функциональных объектов по информации" + Environment.NewLine + Environment.NewLine);

            foreach (var x in ls)
            {
                if (x.TypeP == "Триггер")
                {
                    Report.Append("Триггер " + x.FullName + Environment.NewLine);
                }
                else if (x.TypeP == "Функция")
                {
                    Report.Append("Функция " + x.FullName + Environment.NewLine);
                }
                else
                {
                    Report.Append("Процедура " + x.FullName + Environment.NewLine);
                }

                Dictionary<string, List<string>> dv = new Dictionary<string, List<string>>();
                Dictionary<string, List<string>> dp = new Dictionary<string, List<string>>();
                Dictionary<string, List<string>> dt = new Dictionary<string, List<string>>();
                StringBuilder repName = new StringBuilder();
                foreach (var y in x.listCalling)
                {
                    foreach (var t in x.listLocal)
                    {
                        if (y.Params.Contains(t.NameV))
                        {
                            repName.Clear();
                            if (y.TypeP == "Триггер")
                            {
                                repName.Append("Триггера  " + y.FullName);
                            }
                            else if (y.TypeP == "Функция")
                            {
                                repName.Append("Функции   " + y.FullName);
                            }
                            else
                            {
                                repName.Append("Процедуры " + y.FullName);
                            }
                            string key = repName.ToString();
                            if (!dp.Keys.Contains(key))
                            {
                                dp.Add(key, new List<string>());
                            }
                            if (!dp[key].Contains(t.NameV))
                                dp[key].Add(t.NameV);
                        }
                    }
                    foreach (var t in x.listParams)
                    {
                        if (y.Params.Contains(t.NameV))
                        {
                            repName.Clear();
                            if (y.TypeP == "Триггер")
                            {
                                repName.Append("Триггера  " + y.FullName);
                            }
                            else if (y.TypeP == "Функция")
                            {
                                repName.Append("Функции   " + y.FullName);
                            }
                            else
                            {
                                repName.Append("Процедуры " + y.FullName);
                            }
                            string key = repName.ToString();
                            if (!dp.Keys.Contains(key))
                            {
                                dp.Add(key, new List<string>());
                            }
                            if (!dp[key].Contains(t.NameV))
                                dp[key].Add(t.NameV);
                        }
                    }
                }
                foreach (var y in ls)
                {
                    if (x == y)
                        continue;
                    List<string> X_Global = x.listGlobal.Select(x0 => x0.NameV).ToList();
                    List<string> Y_Global = y.listGlobal.Select(x0 => x0.NameV).ToList();
                    foreach (string t in Y_Global)
                    {
                        if (X_Global.Contains(t))
                        {
                            repName.Clear();
                            if (y.TypeP == "Триггер")
                            {
                                repName.Append("Триггера  " + y.FullName);
                            }
                            else if (y.TypeP == "Функция")
                            {
                                repName.Append("Функции   " + y.FullName);
                            }
                            else
                            {
                                repName.Append("Процедуры " + y.FullName);
                            }
                            string key = repName.ToString();
                            if (!dp.Keys.Contains(key))
                            {
                                dp.Add(key, new List<string>());
                            }
                            if (!dp[key].Contains(t))
                                dp[key].Add(t);
                        }
                    }
                }
                foreach (var y in ls)
                {
                    if (x == y)
                        continue;
                    List<string> X_tblnames = x.listTables.Select(x0 => x0.NameV).ToList();
                    List<string> Y_tblnames = y.listTables.Select(x0 => x0.NameV).ToList();

                    foreach (string t in Y_tblnames)
                    {
                        if (X_tblnames.Contains(t))
                        {
                            repName.Clear();
                            if (y.TypeP == "Триггер")
                                repName.Append("Триггера  " + y.FullName);
                            else if (y.TypeP == "Функция")
                                repName.Append("Функции   " + y.FullName);
                            else
                                repName.Append("Процедуры " + y.FullName);
                            string key = repName.ToString();
                            if (!dt.Keys.Contains(key))
                            {
                                dt.Add(key, new List<string>());
                            }
                            if (!dt[key].Contains(t))
                                dt[key].Add(t);
                        }
                    }

                }
                if (dp.Count == 0)
                {
                    Report.Append("\tСвязей по переменным нет."+Environment.NewLine);
                }
                else
                {
                    Report.Append("\tСвязи по переменным и параметрам для:"+Environment.NewLine);
                    foreach (string s in dp.Keys)
                    {
                        Report.Append("\t\t" + s + Environment.NewLine);
                        foreach (string p in dp[s])
                        {
                           Report.Append("\t\t\t" + p+Environment.NewLine);
                        }
                    }
                }
                if (dt.Count == 0)
                {
                    Report.Append("\tСвязей по таблицам нет."+Environment.NewLine);
                }
                else
                {
                   Report.Append("\tСвязи по таблицам БД для:"+ Environment.NewLine);
                    foreach (string s in dt.Keys)
                    {
                        Report.Append("\t\t" + s + Environment.NewLine);
                        foreach (string p in dt[s])
                        {
                            Report.Append("\t\t\t" + p + Environment.NewLine);
                        }
                    }
                }
    
                Report.Append(Environment.NewLine);
            }
            System.IO.File.WriteAllText(Form1.SavePath + "\\Связь функц. объектов по информации.txt", Report.ToString());

        }
        /// <summary>
        /// Очистка списков зависимостей (для повторного запуска "Анализ")
        /// </summary>
        /// <param name="ls"></param>
        void ClearDepends(List<DBProc> ls)
        {
            foreach (var x in ls)
            {
                x.listCalling.Clear();
                x.listGlobal.Clear();
                x.listLocal.Clear();
                x.listTables.Clear();
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
