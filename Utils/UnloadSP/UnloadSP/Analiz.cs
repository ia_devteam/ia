﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace UnloadSP
{
    class Analiz
    {
        public Form1 pF;
        static string strDynamic;
        //static int CurPos;
        static bool NothingToDo = false;
        static bool TablesDone = false;
        public static char[] arDelimitersT = { '\r', '\n', '\t', ' ', ';' };
        public static char[] arBrackets = { '(', ')', '[', ']' };
        public static char[] arOperations = { '+', '-', '/', '*', '>', '<', '=', '!', '%' };
        public static string[] arCreate = { "CREATE", "ALTER" };
        public static string[] arDeclare = { "DECLARE", "declare" };
        public static string[] arProcType = { "PROCEDURE", "FUNCTION", "TRIGGER", "TABLE", "VIEW" };
        public static string[] arExec = { "EXEC", "EXECUTE" };
        public static string[] arBegEnd = { "BEGIN", "END" };
        public static string[] arVarTypesT = { "tinyint", "smallint", "int", "bigint", "integer", "numeric", "decimal", "float", "real", "datetime", "char", "varchar", 
                                                "nchar","nvarchar","text","binary","varbinary","image","bit"};
        static ArrayList arGlobalVars = new ArrayList();
        static ArrayList arLocalVars = new ArrayList();
        static ArrayList arVarType = new ArrayList();
        static ArrayList arDeclarePos = new ArrayList();
        static int GlobalVarsIndx = 0;
        static int LocalVarsIndx = 0;

        public Analiz(Form1 f)
        {
            pF = f;
        }

        public List<Procedures> listProcedures = new List<Procedures>();
        public List<DBTables> listDBTables = new List<DBTables>();
        public List<GlobalVars> listGlobalVars = new List<GlobalVars>();
        public List<LocalVars> listLocalVars = new List<LocalVars>();
        public List<ProcDepend> listProcDepend = new List<ProcDepend>();
        public List<ProcInfoLinks> listProcInfoLinks = new List<ProcInfoLinks>();
        public List<ProcTableLinks> listProcTableLinks = new List<ProcTableLinks>();
        //public List<RouteChart> listRouteChart = new List<RouteChart>();

        public struct Procedure
        {
            public string ProcName;
            public string Params;
            public string RetValType;
            public string FileName;
            public string ProjectName;
            public int PosBodyBeg;
            public int PosBodyEnd;
            public int SensorPos;
            public string ProcType;
            public string ProcedureText;
        }


        public struct Procedures
        {
            public int ProcID;
            public string ProcName;
            public string Params;
            public string RetValType;
            public string FName;
            public string DBName;
            public int PosBeg;
            public int PosEnd;
            public int SensorPos;
            public string ProcType;
            public string ProcText;
            public Procedures(int id, string pn, string par, string ret, string fn, string dbn, int pb, int pe, int sens, string pt, string txt)
            {
                ProcID = id;
                ProcName = pn;
                Params = par;
                RetValType = ret;
                FName = fn;
                DBName = dbn;
                PosBeg = pb;
                PosEnd = pe;
                SensorPos = sens;
                ProcType = pt;
                ProcText = txt;
            }
        }

        public struct DBTables
        {
            public int TableID;
            public string TableName;
            public string STableName;
            public DBTables(int id, string n, string sn)
            {
                TableID = id;
                TableName = n;
                STableName = sn;
            }
        }

        public struct GlobalVars
        {
            public int ID;
            public string VarName;
            public int ProcID;
            public GlobalVars(int id, string vn, int pid)
            {
                ID = id;
                VarName = vn;
                ProcID = pid;
            }
        }

        public struct LocalVars
        {
            public int ID;
            public int ProcID;
            public string Name;
            public string Type;
            public int DeclarePos;
            public LocalVars(int id, int pid, string n, string t, int dp)
            {
                ID = id;
                ProcID = pid;
                Name = n;
                Type = t;
                DeclarePos = dp;
            }
        }

        public struct ProcDepend
        {
            public int ID;
            public int ProcID;
            public int CalledProcID;
            public string CallPars;
            public ProcDepend(int id, int pid, int cpid, string cp)
            {
                ID = id;
                ProcID = pid;
                CalledProcID = cpid;
                CallPars = cp;
            }
        }

        public struct ProcInfoLinks
        {
            public int Proc1ID;
            public int Proc2ID;
            public string VarName;
            public string VarType;
            public ProcInfoLinks(int id1, int id2, string vn, string vt)
            {
                Proc1ID = id1;
                Proc2ID = id2;
                VarName = vn;
                VarType = vt;
            }
        }

        public struct ProcTableLinks
        {
            public int Proc1ID;
            public int Proc2ID;
            public string TableName;
            public ProcTableLinks(int id1, int id2, string tn)
            {
                Proc1ID = id1;
                Proc2ID = id2;
                TableName = tn;
            }
        }

        Procedure Proc = new Procedure();
        string strProcedure;

        public bool MakeDBRecords(string SrsCode, string FileName)
        {
            strProcedure = SrsCode;
            Proc.Params = "";
            TSQL_FillProcedureAttrib(strProcedure, FileName);   // Write to table Procedures
            TSQL_FillVars(strProcedure);    // Write to tables LocalVars and GlobalVars
            // Write to DB
            WritePrimaryDB();
            return true;
        }

        private void TSQL_FillProcedureAttrib(string strProcedure, string FileName)
        {
            CleanProcStruct();
            //ClearFile cf = new ClearFile();

            strDynamic = NormalizeText(Form1.RemoveComments(strProcedure));  // Will be modified during processing
            strDynamic = strDynamic.TrimStart(arDelimitersT);

            string CurWord, Value;
            int Indx, Length;

            Proc.ProcedureText = strProcedure;
            Proc.FileName = FileName;

            Indx = Get1stPosOfWord(arCreate[0], strDynamic, out Length, out Value);
            if (Indx < 0) Indx = Get1stPosOfWord(arCreate[1], strDynamic, out Length, out Value);
            if (Indx < 0)
            {
                Proc.ProcName = FileName.Substring(0, FileName.IndexOf("."));
                Proc.ProcName = "sys." + Proc.ProcName.Substring(Proc.ProcName.LastIndexOf("\\") + 1);
                Proc.Params = strProcedure;
                NothingToDo = true;
                return;
            }
            else NothingToDo = false;

            strDynamic = strDynamic.Substring(Indx + Length);  // Position after CREATE or ALTER
            Proc.ProcType = GetWordAt1stPos(strDynamic).ToUpper();  // ProcType. 

            CurWord = GetWordAt1stPos(strDynamic);  // ProcName. 
            Proc.ProcName = ExcludeStr(CurWord, "]"); // Exclude if it is in string
            Proc.ProcName = ExcludeStr(Proc.ProcName, "[");
            Proc.ProcName = (Proc.ProcName.TrimStart(arDelimitersT)).TrimEnd(arDelimitersT);

            string D1 = strDynamic;            //strDynamic (D1) begins with parameter block now

            // !!!!!!!!!!!!!!!!!!! This place need to be corrected to cover the cases when BEGIN absents or WITH EXECUTE AS is presented
            /*            int IndxAS = Get1stPosOfWord("AS", strDynamic, out Length, out Value); // GetASIndex(strDynamic);    // strDynamic begins with parameter block now
                        if (IndxAS < 0) IndxAS = Get1stPosOfWord("BEGIN", strDynamic, out Length, out Value);
                        int ParBlockLen;
                        Indx = Get1stPosOfWord("RETURNS", strDynamic, out Length, out Value);  // GetRETURNSIndex(strDynamic); //.IndexOf("RETURNS");
                        if (Indx < 0)   // No RETURNS
                        {
                            Proc.RetValType = "";
                            ParBlockLen = IndxAS;   // Now IndxAS equals to the length of params list
                        }
                        else
                        {
                            ParBlockLen = Indx; // Now Indx equals to the length of params list
                            Proc.RetValType = GetWordAt1stPos(strDynamic.Substring(Indx+Length));
                        }
            */
            //!!!!!!!!!!!!!!!!!! New 19.11.09
            int ParBlockLen = 0;
            if (Proc.ProcType == "FUNCTION")
            {
                Indx = Get1stPosOfWord("RETURNS", strDynamic, out Length, out Value);
                ParBlockLen = Indx;
                Proc.RetValType = TSQL_GetRetValType(strDynamic.Substring(Indx + Length)); //GetWordAt1stPos(strDynamic.Substring(Indx + Length));
            }
            if (Proc.ProcType == "PROCEDURE")
            {
                Proc.RetValType = "";
                Indx = Get1stPosOfWord("WITH\\s*(ENCRYPTION|RECOMPILE|EXECUTE\\s*AS)", strDynamic, out Length, out Value);  // 

                if (Indx >= 0) ParBlockLen = Indx;
                else
                {
                    Indx = Get1stPosOfWord("FOR\\s*REPLICATION", strDynamic, out Length, out Value);

                    if (Indx >= 0) ParBlockLen = Indx;
                    else ParBlockLen = Get1stPosOfWord("AS", strDynamic, out Length, out Value);
                }
            }
            //DML
            //DDL
            //LOGON
            /*
            if (Proc.ProcType == "TRIGGER")
            {
                Proc.RetValType = "";
                Indx = Get1stPosOfWord("[FOR|AFTER|INSTEAD OF]\\s*(INSERT|UPDATE|DELETE\\s*AS)", strDynamic, out Length, out Value);  // 

                if (Indx >= 0) 
                    ParBlockLen = Indx;
                else
                {
                    ParBlockLen = Get1stPosOfWord("AS", strDynamic, out Length, out Value);
                }
            }
            */
            //!!!!!!!!!!!!!!!!!!!!!!!! Old part of code
            string strTemp = D1.Substring(0, ParBlockLen);

            Indx = Get1stPosOfWord("WITH\\s*EXECUTE\\s*AS", strProcedure, out Length, out Value);
            if (Indx < 0)
            {
                Indx = Get1stPosOfWord("AS", strProcedure, out Length, out Value);
                Proc.PosBodyBeg = Indx + Length;
            }
            else
            {
                Proc.PosBodyBeg = Indx + Length;
                Indx = Get1stPosOfWord("AS", strProcedure.Substring(Indx + Length), out Length, out Value);
                Proc.PosBodyBeg = Proc.PosBodyBeg + Indx + Length;
            }
            Proc.PosBodyEnd = strProcedure.Length - 1;

            // Now take substring with params 
            //string strTemp = strProcedure.Substring(ParBegPos, ParBlockLen);
            strTemp = strTemp.TrimStart(arDelimitersT);
            strTemp = strTemp.TrimEnd(arDelimitersT);
            Proc.Params = TSQL_GetParams(strTemp);
            Proc.Params = Proc.Params.Trim();
            Proc.SensorPos = Proc.PosBodyBeg;
        }

        // Makes records for GlobalVars and LocalVars
        private void TSQL_FillVars(string strProcedure)
        {
            arLocalVars.Clear();
            arDeclarePos.Clear();
            arVarType.Clear();
            arGlobalVars.Clear();
            int Length;
            string Value;
            //ClearFile cf = new ClearFile();

            if (NothingToDo) return;

            // Find GlobalVars
            string strProcBody = NormalizeText(Form1.RemoveComments(strProcedure));   //.Substring(Proc.PosBeg);
            int Indx = Get1stPosOfWord("AS", strProcBody, out Length, out Value);
            if (Indx < 0) Indx = Get1stPosOfWord("BEGIN", strProcBody, out Length, out Value);
            strProcBody = strProcBody.Substring(Indx + Length);
            Indx = strProcBody.IndexOf("@@");

            while (Indx > 0)
            {
                string GlobalVar = "";
                string strTempDyn = strProcBody.Substring(Indx);  // String begins with @@
                strTempDyn = GetWordAt1stPos(strTempDyn);
                int x = strTempDyn.IndexOf(',');
                if (x > 0) strTempDyn = strTempDyn.Substring(0, x); // GetWord does not consider ',' as delimiter but it happens
                GlobalVar = strTempDyn.Trim();

                GlobalVar = ExcludeStr(GlobalVar, "'");
                if (GlobalVar.Contains("0") && (GlobalVar.Length == (GlobalVar.IndexOf("0") + 1)))    // Truncate "0" at the end of VarName
                    GlobalVar = GlobalVar.Substring(0, GlobalVar.IndexOf("0"));
                if (!arGlobalVars.Contains(GlobalVar))
                {
                    arGlobalVars.Add(GlobalVar);     //.SetValue(GlobalVar, nArIndx);
                }
                strProcBody = strProcBody.Substring(Indx + 3);
                Indx = strProcBody.IndexOf("@@");
            }

            // Find local vars

            //strProcBody = strProcedure.Substring(Proc.PosBeg);
            strProcBody = NormalizeText(Form1.RemoveComments(strProcedure));
            Indx = Get1stPosOfWord("AS", strProcBody, out Length, out Value);
            if (Indx < 0) Indx = Get1stPosOfWord("BEGIN", strProcBody, out Length, out Value);
            strProcBody = strProcBody.Substring(Indx + Length);
            Indx = strProcBody.IndexOf(arDeclare[0], StringComparison.OrdinalIgnoreCase);
            if (Indx < 0) Indx = strProcBody.IndexOf(arDeclare[1], StringComparison.OrdinalIgnoreCase); // Any case Indx points to "declare" if any
            if (Indx >= 0)
            {
                strProcBody = strProcBody.Substring(Indx + "DECLARE".Length);
                CharEnumerator cEn2 = strProcBody.GetEnumerator();
                bool Flag = false;
                string LocalVar = "@";
                while (cEn2.MoveNext())  // Make var name
                {
                    //DecPtr++;
                    if ((cEn2.Current == '@') && !Flag)  // Meet for the 1-st time
                    {
                        Flag = true;
                        continue;
                    }

                    if ((cEn2.Current == '@') && Flag)    // This is @@
                    {
                        Flag = false;
                        continue;
                    }

                    if (Flag)  // Candidate has only one "@"
                    {
                        if (char.IsLetterOrDigit(cEn2.Current) || (cEn2.Current == '_') || (cEn2.Current == '$'))
                        {
                            LocalVar = LocalVar + cEn2.Current.ToString();
                        }
                        else
                        {
                            LocalVar = LocalVar.TrimEnd(arOperations);
                            LocalVar = LocalVar.TrimEnd(arDelimitersT);
                            LocalVar = LocalVar.TrimEnd(arBrackets);
                            LocalVar = ExcludeStr(LocalVar, "'");
                            if ((!arLocalVars.Contains(LocalVar)) && (Proc.Params.IndexOf(LocalVar) < 0))
                            {
                                arLocalVars.Add(LocalVar);
                                arDeclarePos.Add(Proc.ProcedureText.IndexOf(LocalVar));
                            }
                            Flag = false;
                            LocalVar = "@";
                        }
                    }
                }



                // Make var type
                // There may be user defined types
                /*
                    Here is the syntax for sp_addtype:
        
                    sp_addtype datatypename,
                    phystype [ (length) | (precision [, scale] ) ]
                    [, "identity" |nulltype]
        
                    Here is how tid was defined:
                    sp_addtype tid, "char(6)", "not null"
                 */

                int ii = 0;
                int VarNum = arLocalVars.Count;
                for (ii = 0; ii < VarNum; ii++)
                {
                    string strType = "";
                    string strVarBeg = Proc.ProcedureText.Substring(Convert.ToInt32(Convert.ToInt32(arDeclarePos[ii]) + arLocalVars[ii].ToString().Length));
                    strVarBeg = NormalizeText(Form1.RemoveComments(strVarBeg));
                    strVarBeg = strVarBeg.Replace(" ( ", "(");
                    strVarBeg = strVarBeg.Replace(" ) ", ") ");
                    strDynamic = strVarBeg;
                nextWord: strType = GetWordAt1stPos(strDynamic);    // Only strDynamic is modified by this function!
                    strType = ExcludeStr(strType, ",");
                    strType = ExcludeStr(strType, "'");
                    if (strType == "AS" || strType == "as") goto nextWord;
                    if (strType.Contains("=") || strType.Contains("<") || strType.Contains(">"))  // This is not variable declaration
                    {
                        VarNum--;   // = arLocalVars.Count - 1;
                        arLocalVars.RemoveAt(ii);
                        arDeclarePos.RemoveAt(ii);
                        ii--;
                        continue;
                    }
                    arVarType.Add(strType);
                }
            }
            else return;
        }

        // Извлекает из строки, первый участок, заканчивающийся разделительным символом
        // Предварительно удаляет разделительные символы в начале строки
        // Модифицирует строку strDynamic!
        public string GetWordAt1stPos(string strCur)
        {
            string strWord;
            int IndxEnd;       // Index of found word begining and end

            strWord = "";
            strCur = strCur.TrimStart(arDelimitersT);
            IndxEnd = strCur.IndexOfAny(arDelimitersT);
            strWord = strCur.Substring(0, IndxEnd + 1);
            strWord = strWord.TrimStart(arDelimitersT);
            strWord = strWord.TrimEnd(arDelimitersT);
            strDynamic = strCur.Substring(IndxEnd + 1);
            //CurPos = CurPos + (IndxEnd + 1);    // This is indx in ProcedureText pointing to the begining of strDynamic
            return strWord;
        }

        private string TSQL_GetParams(string S0) // S0 is a substring of ProcedureText containing parameter list
        {                                            // and ends before the procedure body or RETURNS in case of function
            S0 = (S0.TrimEnd(arDelimitersT)).TrimStart(arDelimitersT);
            if (S0 == "") return S0;
            //            if (S0.Length == 0) return S0;
            int Indx1 = S0.IndexOf('@');  // 1-st param indx if any
            if (Indx1 < 0) return "";

            int Indx2 = S0.IndexOf('(', 0, Indx1);
            if (Indx2 == 0)
            {
                int ll = S0.LastIndexOf(')'); // If so then find corresponding right paranthesis
                if (ll > 0)
                    S0 = S0.Substring(Indx1, ll - Indx1);
            }
            else S0 = S0.Substring(Indx1);

            return RefactString(S0);
        }

        bool WritePrimaryDB()
        {
            // Fill DB with table list
            if (!TablesDone)
            {
                FillDBTables();
                TablesDone = true;
            }

            int ProcID = listProcedures.Count + 1;
            listProcedures.Add(new Procedures(ProcID,Proc.ProcName,Proc.Params, Proc.RetValType,
                Proc.FileName, Proc.ProjectName, Proc.PosBodyBeg, Proc.PosBodyEnd, Proc.SensorPos, Proc.ProcType, Proc.ProcedureText));
            for (int i = 0; i < arGlobalVars.Count; i++)
            {
                listGlobalVars.Add(new GlobalVars(GlobalVarsIndx++, arGlobalVars[i].ToString(), ProcID));
            }
            for (int i = 0; i < arLocalVars.Count; i++)
            {
                listLocalVars.Add(new LocalVars(LocalVarsIndx++, ProcID, arLocalVars[i].ToString(), arVarType[i].ToString(), Convert.ToInt32(arDeclarePos[i].ToString())));
            }
            return true;
        }

        // Fills derivated (secondary) DB tables built of primary DB tables infomation
        public void WriteSecDB()
        {
            // Make procedure dependense table
            MakeProcDepend();

            // Make procedure infomation links table
            // Fill ProcInfoLinks
            FormInfoLinks();
        }

        string GetCalledProcParams(string S, int NumOfParams)    // Receive string S begining with called procedure name followed parameters
        {
            int i = 0;
            string RetStr = "";
            int Indx;
            Indx = S.IndexOf(' ');  // The end of procedure name
            S = S.Substring(Indx + 1);
            S = S.Trim();   // Begings with 1-st parameter
            while (i < NumOfParams)
            {
                i++;
                if (i == NumOfParams)   // The last parameter
                {
                    string tmpStr = GetWordAt1stPos(S);

                    RetStr = RetStr + tmpStr;
                    return RetStr;
                }
                else
                {
                    int Indx1 = S.IndexOf(',');
                    string tmpStr = S.Substring(0, Indx1 + 1);

                    RetStr = RetStr + tmpStr;  // Include comma
                    S = S.Substring(Indx1 + 1);
                    S = S.TrimStart(arDelimitersT);
                }
            }
            return RetStr;
        }

        string ExcludeStr(string S, string c)
        {
            int indx = S.IndexOf(c);
            while (indx >= 0)
            {
                string tmp = "";
                if (indx > 0) tmp = S.Substring(0, indx);
                S = tmp + S.Substring(indx + c.Length);
                indx = S.IndexOf(c);
            }
            return S;
        }

        // For each record in ProcDepend take from LocalVars of calling procedure those that are in CallPars
        // then form record for ProcInfoLinks: (ProcID1,ProcID2,VarName,VarType)
        void FormInfoLinks()
        {

            foreach (ProcDepend pd in listProcDepend)
            {
                string Params = pd.CallPars;
                foreach (LocalVars lv in listLocalVars)
                {
                    if (lv.ProcID != pd.ProcID) continue; // for calling procedures
                    if (Params.IndexOf(lv.Name) < 0) continue;    // and if it belongs to parameter list
                    listProcInfoLinks.Add(new ProcInfoLinks(pd.ProcID,pd.CalledProcID,lv.Name,lv.Type));
                }
            }
            
            foreach (GlobalVars gv in listGlobalVars)
            {
                foreach (GlobalVars gv1 in listGlobalVars)
                {
                    if (gv.ProcID == gv1.ProcID) continue;
                    if (gv.VarName.ToUpper() != gv1.VarName.ToUpper()) continue;
                    listProcInfoLinks.Add(new ProcInfoLinks(gv.ProcID,gv1.ProcID,gv.VarName,""));
                }
            }

            List<int> arID1 = new List<int>();
            int i, k, Length;

            foreach (DBTables dbt in listDBTables)
            {
                int Len;
                string V1;
                i = 0;
                arID1.Clear();
                string TableName = dbt.STableName;  // fetch table name

                foreach (Procedures pr in listProcedures)
                {
                    string ProcFileName =pr.FName;
                    string ProcText = Form1.RemoveComments(pr.ProcText);
                    string str2Search = @"\b" + TableName + @"\b";
                    int Indx = Get1stPosOfWord(str2Search, ProcText, out Len, out V1);
                    if (Indx > 0)
                    {
                        arID1.Add(pr.ProcID);
                    }

                }

                Length = arID1.Count;
                for (i = 0; i < Length; i++)    // Save info to the ds.ProcTableLinks
                {
                    for (k = i + 1; k < Length; k++)
                    {
                        listProcTableLinks.Add(new ProcTableLinks(arID1[i], arID1[k], TableName)); 
                        listProcTableLinks.Add(new ProcTableLinks(arID1[k], arID1[i], TableName));
                    }
                }
            }
        }

        void FillDBTables()
        {
            listDBTables.Clear();
            for (int i = 0; i < Form1.Tables.Count; i++)
                listDBTables.Add(new DBTables(i + 1, Form1.Tables[i], Form1.STables[i]));
        }

        private int GetASIndex(string S)
        {
            int IndxAS = strDynamic.IndexOf("\r\nAS\r\n");
            if (IndxAS >= 0) return (IndxAS + 2);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\r\nAS ");
            if (IndxAS >= 0) return (IndxAS + 2);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(" AS\r\n");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("AS\r\n");
            if (IndxAS >= 0) return IndxAS;
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("AS\t");
            if (IndxAS >= 0) return IndxAS;
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("AS ");
            if (IndxAS >= 0) return IndxAS;
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\nAS\r\n");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\nAS ");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\tAS\r\n");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\tAS ");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\tAS\t");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(")AS ");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(")AS\r");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(")AS\t");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(" AS ");
            if (IndxAS >= 0) return (IndxAS + 1);
            return -1;
        }

        private int GetRETURNSIndex(string S)
        {
            int IndxAS = strDynamic.IndexOf("\r\nRETURNS\r\n");
            if (IndxAS >= 0) return (IndxAS + 2);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\r\nRETURNS ");
            if (IndxAS >= 0) return (IndxAS + 2);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(" RETURNS\r\n");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("RETURNS\r\n");
            if (IndxAS >= 0) return IndxAS;
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("RETURNS\t");
            if (IndxAS >= 0) return IndxAS;
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("RETURNS ");
            if (IndxAS >= 0) return IndxAS;
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\nRETURNS\r\n");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\nRETURNS ");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\tRETURNS\r\n");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\tRETURNS ");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\tRETURNS\t");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(")RETURNS ");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(")RETURNS\r");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(")RETURNS\t");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(" RETURNS ");
            if (IndxAS >= 0) return (IndxAS + 1);
            return -1;
        }

        private int GetBEGINIndex(string S)
        {
            int IndxAS = strDynamic.IndexOf("\r\nRETURNS\r\n");
            if (IndxAS >= 0) return (IndxAS + 2);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\r\nBEGIN ");
            if (IndxAS >= 0) return (IndxAS + 2);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(" BEGIN\r\n");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("BEGIN\r\n");
            if (IndxAS >= 0) return IndxAS;
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("BEGIN\t");
            if (IndxAS >= 0) return IndxAS;
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("BEGIN ");
            if (IndxAS >= 0) return IndxAS;
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\nBEGIN\r\n");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\nBEGIN ");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\tBEGIN\r\n");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\tBEGIN ");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf("\tBEGIN\t");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(")BEGIN ");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(")BEGIN\r");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(")BEGIN\t");
            if (IndxAS >= 0) return (IndxAS + 1);
            if (IndxAS < 0) IndxAS = strDynamic.IndexOf(" BEGIN ");
            if (IndxAS >= 0) return (IndxAS + 1);
            return -1;
        }

        void CleanProcStruct()
        {
            Proc.FileName = "";
            Proc.Params = "";
            Proc.PosBodyBeg = 0;
            Proc.PosBodyEnd = 0;
            Proc.ProcedureText = "";
            Proc.ProcName = "";
            Proc.ProcType = "";
            Proc.ProjectName = "";
            Proc.RetValType = "";
            Proc.SensorPos = 0;
        }

        int Get1stPosOfWord(string word, string Text, out int Length, out string Value)
        {
            string pat = @"\b" + word + @"\b";   // (\s|\n|\r)+
            // Compile the regular expression.
            Regex r = new Regex(pat, RegexOptions.IgnoreCase);
            // Match the regular expression pattern against a text string.
            Match m = r.Match(Text);
            int Ind = -1;
            Length = -1;
            Value = "";
            if (m.Success)
            {
                Group g = m.Groups[0];
                CaptureCollection cc = g.Captures;
                Capture c = cc[0];
                Ind = c.Index;
                Length = c.Length;
                Value = c.Value;
            }
            return Ind;
        }

        void MakeProcDepend()
        {

            ArrayList CalledProcID = new ArrayList();
            ArrayList CalledProcName = new ArrayList();
            ArrayList CalledProcParNum = new ArrayList();

            foreach (Procedures pr in listProcedures)
            {
                CalledProcID.Add(pr.ProcID);
                CalledProcName.Add(pr.ProcName);
                string Params = pr.Params;
                int ParNum = 0, Indx;
                if (Params.Length > 0) ParNum++;
                Indx = Params.IndexOf(',');
                while (Indx > 0)
                {
                    ParNum++;
                    Params = Params.Substring(Indx + 1);
                    Indx = Params.IndexOf(',');
                }
                CalledProcParNum.Add(ParNum);
            }

            int recCnt = 0;
            foreach(Procedures pr in listProcedures)
            {

                string Proc1File = pr.FName;
                string Proc1Name = pr.ProcName;

                string Proc1Text = Form1.RemoveComments(pr.ProcText);   // Calling procedure text cleaned of comments
                Proc1Text = NormalizeText(Proc1Text).Trim();
                int Proc2Indx, j, Len;
                string strTemp, Params, V1, str2Search;
                for (j = 0; j < CalledProcName.Count; j++)
                {
                    strTemp = Proc1Text;
                    string procName = CalledProcName[j].ToString();
                    if (procName == Proc1Name) continue;
                    str2Search = @"\b" + procName + @"\b";
                    Proc2Indx = Get1stPosOfWord(str2Search, strTemp, out Len, out V1);
                    if (Proc2Indx > 0) strTemp = strTemp.Substring(Proc2Indx + Len).TrimStart();   // strTemp begins now with parameter list
                    else continue;

                    int parNum = (int)CalledProcParNum[j];
                    if (parNum == 0) Params = "";
                    else Params = TSQL_GetCalledProcParams(strTemp, parNum);

                    // Save procedure dependens to ds
                    int ID2 = (int)CalledProcID[j];

                    listProcDepend.Add(new ProcDepend(recCnt++, pr.ProcID, ID2, Params));
                }
            }
        }

        string TSQL_GetCalledProcParamsList(string S, out string Text)  // S should begin with the first parameter (place after called procedure name)
        {
            S = S.Trim();
            bool parFlag = false;
            string ParamsList = "";

            for (; ; )
            {
                string tmp = GetWordAt1stPos(S);    // Now S is modified for the next step
                int Len = tmp.Length;
                if (Len == 0) break;
                S = S.Substring(Len).Trim();
                int indAt = tmp.IndexOf("@");
                if ((indAt < 0) && (Len > 0)) // This is not variable
                {
                    if (tmp[Len - 1] == ',') parFlag = true;
                    else
                    {
                        for (int i = 0; i < arVarTypesT.Length; i++)
                        {
                            if ((tmp == "output") || (tmp == "NULL") || Char.IsDigit(tmp, 0))
                            {
                                parFlag = true;
                                break;
                            }
                            if (tmp.Contains(arVarTypesT[i].ToString()))
                            {
                                parFlag = true;
                                break;
                            }
                            else parFlag = false;
                        }
                    }
                }
                else parFlag = true;
                if (parFlag)
                {
                    ParamsList = ParamsList + tmp + " ";
                    parFlag = false;
                }
                else break;
            }
            Text = S;
            return ParamsList;
        }

        string NormalizeText(string InputText)
        {
            string tmpStr = InputText;
            string strRep;
            string strInitial;

            foreach (char c in arDelimitersT)
            {
                strInitial = c.ToString();
                strRep = " ";
                if (strInitial != " ") tmpStr = tmpStr.Replace(strInitial, strRep);
            }
            tmpStr = tmpStr.Replace(",", " , ");
            tmpStr = tmpStr.Replace("(", " ( ");
            tmpStr = tmpStr.Replace(")", " ) ");
            foreach (char c in arOperations)
            {
                strInitial = c.ToString();
                strRep = " " + strInitial + " ";
                tmpStr = tmpStr.Replace(strInitial, strRep);
            }

            int L1 = tmpStr.Length;
            while (tmpStr.IndexOf("  ") > 0)
            {
                tmpStr = tmpStr.Replace("  ", " ");
                int L2 = tmpStr.Length;
                if (L1 == L2) break;
                L1 = L2;
            }
            tmpStr = tmpStr.Replace("null", "NULL");
            tmpStr = tmpStr.Replace("OUTPUT", "output");

            return tmpStr;
        }

        string TSQL_GetRetValType(string afterReturns)   //Get return type for function
        {
            int Len;
            string Val;
            int endRVT = Get1stPosOfWord("WITH", afterReturns, out Len, out Val);   // (ENCRYPTION | SCHEMABINDING | RETURNS NULL ON NULL INPUT | CALLED ON NULL INPUT | EXECUTE AS)
            if (endRVT < 0) endRVT = Get1stPosOfWord("AS", afterReturns, out Len, out Val);
            if (endRVT < 0) endRVT = Get1stPosOfWord("BEGIN", afterReturns, out Len, out Val);
            Val = afterReturns.Substring(0, endRVT);
            return RefactString(Val);
        }

        // Delete extra blankspace and unicode options
        string RefactString(string S)
        {
            S = S.Trim();
            S = S.Replace(" ( ", "(");
            S = S.Replace(" )", ")");
            S = S.Replace(" ,", ",");
            int Indx = S.IndexOf("N'");
            if (Indx >= 0)
            {
                S = ExcludeStr(S, "N'");
                Indx = S.LastIndexOf("'");
                S = S.Substring(0, Indx);
            }
            //28.07.10 There is problem when variable recieve value. It may cause error at saving stage. So we exclude all default initialization
            // Find symbol "=" and "," in S
            // Cut substring =XXXXX from S beginig at "=" and ending before "," or to the end of S
            int EqIndx;
            int ComeIndx;
            string S1;
            do
            {
                EqIndx = S.IndexOf("=");
                if (EqIndx > 0)
                {
                    ComeIndx = S.IndexOf(",", EqIndx);
                    if (ComeIndx > 0) S1 = S.Substring(0, EqIndx).TrimEnd() + S.Substring(ComeIndx);
                    else S1 = S.Substring(0, EqIndx).TrimEnd();
                    S = S1;
                }
            }
            while (EqIndx >= 0);

            return S;
        }

        string TSQL_GetCalledProcParams(string inStr, int parsNum)   // inStr should beging after ProcName, outStr will start after params
        {
            inStr = RefactString(inStr);
            int cnt = 0;
            string strRet = "";
            int Indx = inStr.IndexOf('(');
            if (Indx == 0) { inStr = inStr.Substring(1); inStr = inStr.Substring(0, inStr.Length - 1); inStr.Trim(); }
            while (cnt < parsNum)
            {
                cnt++;
                if (cnt < parsNum)
                {
                    Indx = inStr.IndexOf(',');
                    strRet = strRet + inStr.Substring(0, Indx + 1) + " ";
                    inStr = inStr.Substring(Indx + 1).TrimStart();
                }
                else  // if last parameter
                {
                    Indx = inStr.IndexOf(' ');
                    if (Indx < 0)
                    {
                        return strRet;
                    }
                    strRet = strRet + inStr.Substring(0, Indx);
                    string tmp = inStr.Substring(Indx + 1);
                    int n = tmp.IndexOf(' ');
                    if (n > 0)
                    {
                        tmp = tmp.Substring(0, n).ToUpper();
                        if ((tmp == "OUT") || (tmp == "OUTPUT")) strRet = strRet + " " + tmp;
                    }
                }
            }
            return strRet;
        }

        void TSQLProceduresWithInfoObjectsReport()
        {


            string Report =  "Информационные объекты процедур" + "\r\n\r\n";
            ArrayList arTemp = new ArrayList();
            foreach(Procedures pr in listProcedures)
            {
                //  Информационные объекты
                Report = Report + "Процедура " + pr.ProcName + ":\r\n\t";      //      Таблицы
                Report = Report + "Информационные объекты \r\n\t\t Таблицы: \r\n\t\t\t";
                int ProcID = pr.ProcID;                                 //      Глоб.переменные
                int i = 0;
                foreach(ProcTableLinks ptl in listProcTableLinks)
                {
                    if ((ProcID == ptl.Proc1ID) || (ProcID == ptl.Proc2ID))
                    {
                        string TName = ptl.TableName;
                        if (arTemp.Contains(TName)) continue;
                        arTemp.Add(TName);
                        //i++;
                    }
                }
                //i--;
                for (i = 0; i < arTemp.Count; i++)
                {
                    Report = Report + arTemp[i].ToString() + "\r\n\t\t\t";
                    //arTemp[i] = "";
                    //i--;
                }
                arTemp.Clear();

                Report = Report.Substring(0, Report.Length - 3) + "\r\n\t";   // Eliminate \t and add \r\n\t after end of clause

                // Make procedure global var list
                Report = Report + "\tГлобальные переменные: \r\n\t\t\t";
                //i = 0;
                foreach(GlobalVars gv in listGlobalVars)
                {
                    if (ProcID == gv.ProcID)
                    {
                        string GVName = gv.VarName;
                        if (arTemp.Contains(GVName)) continue;
                        arTemp.Add(GVName);
                        //i++;
                    }
                }
                //i--;
                for (i = 0; i < arTemp.Count; i++)
                {
                    Report = Report + arTemp[i].ToString() + "\r\n\t\t\t";
                }
                arTemp.Clear();

                Report = Report.Substring(0, Report.Length - 3) + "\r\n\t";   // Eliminate \t and add \r\n\t after end of clause

                // Make procedure local vars list
                Report = Report + "\tЛокальные переменные: \r\n\t\t\t";
                //i = 0;
                //string[] arType = new string[1000];
                ArrayList arType = new ArrayList();

                foreach(LocalVars lv in listLocalVars)
                {
                    if (ProcID == lv.ProcID)
                    {
                        string GVName = lv.Name;
                        if (arTemp.Contains(GVName)) continue;
                        else
                        {
                            arTemp.Add(GVName);
                            arType.Add(lv.Type);
                        }
                        //i++;
                    }
                }
                //i--;
                for (i = 0; i < arTemp.Count; i++)
                {
                    Report = Report + arTemp[i].ToString() + "\t" + arType[i].ToString() + "\r\n\t\t\t";
                }
                arTemp.Clear();
                arType.Clear();

                Report = Report.Substring(0, Report.Length - 3) + "\r\n";   // Eliminate \t and add \r\n after end of clause
            }

            string Path = Form1.SavePath;
            Path = Path + "\\OLD Инф. Объекты Процедур.txt";
            System.IO.File.WriteAllText(Path, Report);

        }

        void ProceduresControlLinks()
        {
            //int nProcs = listProcDepend.Count;

            int PrevID = -1;
            string Report = "Связи функциональных объектов по управлению" + "\r\n\r\n";
            foreach(ProcDepend pd in listProcDepend)
            {

                int ProcID = pd.ProcID;
                int CalledProcID = pd.CalledProcID;
                string CallPars = pd.CallPars;
                
                string ProcName="";
                var PN = (from r in listProcedures
                                   where r.ProcID==ProcID
                                   select new { ProcName = r.ProcName }).ToList();
                if (PN.Count > 0)
                    ProcName = PN[0].ProcName;

                string CalledProcName = "";
                var CP = (from r in listProcedures
                          where r.ProcID == CalledProcID
                          select new { ProcName = r.ProcName }).ToList();
                if (CP.Count > 0)
                    CalledProcName = CP[0].ProcName;

                if (ProcID != PrevID)
                    Report = Report + "\r\n" + "Вызывающая процедура " + ProcName + "\r\n\r\n";
                foreach (var x in CP)
                {
                    Report = Report + "\t" + "Вызываемая процедура " + x.ProcName + "\r\n\t";
                    Report = Report + "Параметры вызова: " + CallPars + "\r\n\r\n";
                }
                PrevID = ProcID;
                //procReader.Close();
            }

            string Path = Form1.SavePath;
            Path = Path + "\\OLD Связи функц. объектов по управлению.txt";
            System.IO.File.WriteAllText(Path, Report);
        }

        // Make lists of tables, local and global vars common for different procedures

        void ProceduresInfoLinksReport()
        {
            string Report =  "Связи функциональных объектов по информации" + "\r\n\r\n\t";
            int Prev1ID = -1, Prev2ID = -1;
            ArrayList arVars = new ArrayList();
            ArrayList arType = new ArrayList();

            foreach (Procedures pr in listProcedures)
            {

                int Proc1ID = pr.ProcID;
                string Proc1Name = pr.ProcName;
                Report = Report + "Процедура1 " + Proc1Name + "\r\n";
                Prev1ID = Proc1ID;
                string Proc2Name = "";

                var P2 = (from r in listProcInfoLinks
                          where r.Proc1ID == Proc1ID
                          select new { Proc2ID = r.Proc2ID, VarName = r.VarName, VarType = r.VarType }).ToList();
                if (P2.Count > 0)
                {
                    Report = Report + "\r\n\t" + "Связи по переменным:" + "\r\n";
                    foreach (var pif in P2)
                    {
                        int Proc2ID = pif.Proc2ID;
                        string VarName = pif.VarName;
                        string VarType = pif.VarType;
                        if (Prev2ID != Proc2ID)     // If new procedure2
                        {
                            if (Prev2ID >= 0)   // New but not first one
                            {
                                // Update record with previous procedure
                                if (arVars.Count > 0) Report = Report + "\t\t" + "Процедура2 " + Proc2Name + "\r\n";
                                for (int i = 0; i < arVars.Count; i++)
                                {
                                    Report = Report + "\t\t\t" + arVars[i].ToString() + "\t\t" + arType[i].ToString() + "\r\n";
                                }
                                arType.Clear(); arVars.Clear();
                            }
                            // Begin new record for new procedure
                            arVars.Add(VarName);
                            arType.Add(VarType);

                            // Retrieve Proc2Name
                            var PN = (from r in listProcedures
                                      where r.ProcID == Proc2ID
                                      select new { ProcName = r.ProcName }).ToList();
                            if (PN.Count > 0)
                                Proc2Name = PN[0].ProcName;

                            Prev2ID = Proc2ID;
                        }
                        else    // Old Procedure
                        {
                            arVars.Add(VarName);
                            arType.Add(VarType);
                        }

                    }

                    // Write last result
                    if (arVars.Count > 0) Report = Report + "\t\t" + "Процедура2 " + Proc2Name + "\r\n";

                    for (int i = 0; i < arVars.Count; i++)
                    {
                        Report = Report + "\t\t\t" + arVars[i].ToString() + "\t\t" + arType[i].ToString() + "\r\n";
                    }
                    arType.Clear(); arVars.Clear();
                    Report = Report + "\r\n";

                }
                else    // No rows
                {
                    Report = Report + "\r\n\t" + "Связей по переменным нет." + "\r\n";
                }
                Prev2ID = -1; Prev1ID = -1;

                var TBL = (from r in listProcTableLinks
                           where r.Proc1ID == Proc1ID
                           select new { Proc2ID = r.Proc2ID, TableName = r.TableName }).ToList();
                if (TBL.Count > 0)
                {
                    Report = Report + "\r\n\t" + "Связи по таблицам БД:" + "\r\n";
                    foreach (var ptl in TBL)
                    {
                        int Proc2ID = ptl.Proc2ID;
                        string TableName = ptl.TableName;

                        if (Prev2ID != Proc2ID)     // If new procedure
                        {
                            if (Prev2ID >= 0)   // New but not first
                            {   // Note as new procedure and add record for previouse one to report
                                if (arVars.Count > 0) Report = Report + "\t\t" + "Процедура2 " + Proc2Name + "\r\n";

                                for (int i = 0; i < arVars.Count; i++)
                                {
                                    Report = Report + "\t\t\t" + arVars[i].ToString() + "\r\n";
                                }
                                arVars.Clear();
                            }

                            arVars.Add(TableName);

                            var PN = (from r in listProcedures
                                      where r.ProcID == Proc2ID
                                      select new { ProcName = r.ProcName }).ToList();
                            if (PN.Count > 0)
                                Proc2Name = PN[0].ProcName;
                            Prev2ID = Proc2ID;
                        }
                        else    // Not new procedure
                        {
                            arVars.Add(TableName);
                        }

                    }
                    // Write last record to report
                    if (arVars.Count > 0) Report = Report + "\t\t" + "Процедура2 " + Proc2Name + "\r\n";

                    for (int i = 0; i < arVars.Count; i++)
                    {
                        Report = Report + "\t\t\t" + arVars[i].ToString() + "\r\n";
                    }
                    arVars.Clear();
                    Report = Report + "\r\n";

                }
                else    // No rows
                {
                    Report = Report + "\r\n\t" + "Связей по таблицам нет." + "\r\n\r\n";
                }
                Prev2ID = -1; Prev1ID = -1;

                ///////////////////////////
                string Path = Form1.SavePath;
                Path = Path + "\\OLD Связь функц. объектов по информации.txt";
                System.IO.File.WriteAllText(Path, Report);
            }

        }

        public void MakePrimaryDB()
        {
            foreach (Form1.DBProc pr in Form1.ListProc)
                MakeDBRecords(pr.ScriptP, pr.FullName);
        }

        public void MakeSecDB()
        {
            WriteSecDB();
        }

        public void MakeReport()
        {
            pF.Log("    Информационные объекты процедур", true);
            TSQLProceduresWithInfoObjectsReport();
            pF.Log("    Связи функциональных объектов по управлению", true);
            ProceduresControlLinks();
            pF.Log("    Связи функциональных объектов по информации", true);
            ProceduresInfoLinksReport();
        }

    }

}
