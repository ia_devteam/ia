﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace UnloadSP
{
    class Depends
    {
        public Form1 pFM;
        public Depends(Form1 fm)
        {
            pFM = fm;

        }
        /// <summary>
        /// зависимость по управленю
        /// </summary>
        /// <param name="ls"></param>
        public void DependsOfProc(List<Form1.DBProc> ls)
        {
            string sFileBody = null;
            for (int i = 0; i < ls.Count; i++)
            {
                Form1.DBProc currNode = ls[i];
                sFileBody = Form1.RemoveCommentsAndHeader(currNode.ScriptP);
                for (int j = 0; j < ls.Count; j++)
                {
                    if (i == j)
                        continue;
                    int ptr = 0;
                    int optr = 0;
                    int start = 0;
                    while (ptr >= 0)
                    {
                        string procname = GetNextProcName(ls[j].NameP, sFileBody, ptr, out optr);
                        string procFullName = ls[j].FullName;
                        ptr = optr;
                        if (optr >= 0)
                        {
                            start = optr;
                            string procparam = GetNextProcParams(sFileBody, ptr, out optr);
                            ptr = optr;
                            if (optr >= 0)
                            {
                                ls[i].listCalling.Add(new Form1.Calling(procFullName, procparam, ls[j].TypeP, start));
                            }
                        }

                    }

                }
            }
        }
        /// <summary>
        /// Поиск имени процедуры (функции)
        /// </summary>
        /// <param name="name">шаблон поиска</param>
        /// <param name="filebody">скрипт процедуры</param>
        /// <param name="ptr">начальный указатель</param>
        /// <param name="optr">указатель результата</param>
        /// <returns>найденная строка</returns>
        public string GetNextProcName(string name, string filebody,int ptr, out int optr)
        {
            optr = filebody.IndexOf(name, ptr, StringComparison.OrdinalIgnoreCase);
            if (optr < 0)
                return "";
            else
            {
                while (optr >= 0)
                {
                    if (optr > 0)
                    {
                        int str = optr - 1;
                        char lc = filebody[str];
                        if (IsNotDelim(lc))
                        {
                            optr = filebody.IndexOf(name, optr+1);
                            continue;
                        }
                    }
                    if (optr + name.Length < filebody.Length - 1)
                    {
                        int end = optr + name.Length;
                        char lc = filebody[end];
                        if (IsNotDelim(lc))
                        {
                            optr = filebody.IndexOf(name, optr + 1, StringComparison.OrdinalIgnoreCase);
                            continue;
                        }
                    }
                    return name;
                }
                optr = -1;
                return "";
            }
        }
        /// <summary>
        /// Поиск параметров следующих за именем процедуры
        /// </summary>
        /// <param name="filebody">скрипт процедуры</param>
        /// <param name="ptr">начальный указатель</param>
        /// <param name="optr">указатель результата</param>
        /// <returns>найденная строка</returns>
        public string GetNextProcParams(string filebody, int ptr, out int optr)
        {
            int level = 0;
            bool done = false;
            int str = 0;
            int end = 0;
            while (!done)
            {
                if (ptr >= filebody.Length)
                {
                    optr = -1;
                    return "";
                }
                switch (filebody[ptr])
                {
                    case '(':
                        if (level == 0)
                        {
                            str = ptr;
                        }
                        level++;
                        break;
                    case ')':
                        level--;
                        if (level == 0)
                        {
                            done = true;
                            end = ptr;
                        }
                        break;
                }
                ptr++;
            }
            optr = ptr;
            return filebody.Substring(str+1, end - str - 1);
        }
        /// <summary>
        /// Поиск имени следующей переменной
        /// </summary>
        /// <param name="filebody">скрипт процедуры</param>
        /// <param name="ptr">начальный указатель</param>
        /// <param name="optr">указатель результата</param>
        /// <param name="declare">строка "declare" или ","</param>
        /// <returns>найденная строка</returns>
        public string GetNextVarName(string filebody, int ptr, out int optr, string declare)
        {
            optr = filebody.IndexOf(declare, ptr, StringComparison.OrdinalIgnoreCase);
            if (optr < 0)
                return "";
            else
            {
                if (declare == ",")
                {
                    string delem = filebody.Substring(ptr, optr - ptr).Trim(new char[] {' ','\t','\r','\n' });
                    if (delem != "")
                    {
                        optr = -1;
                        return "";
                    }
                    else
                    {
                        ptr = optr;
                        return GetNextLocalVar(filebody, ptr, out optr);
                    }

                }
                else
                {
                    while (optr >= 0)
                    {
                        if (optr > 0)
                        {
                            int str = optr - 1;
                            char lc = filebody[str];
                            if (IsNotDelim(lc))
                            {
                                optr = filebody.IndexOf(declare, optr + 1);
                                continue;
                            }
                        }
                        if (optr + declare.Length < filebody.Length - 1)
                        {
                            int end = optr + declare.Length;
                            char lc = filebody[end];
                            if (IsNotDelim(lc))
                            {
                                optr = filebody.IndexOf(declare, optr + 1, StringComparison.OrdinalIgnoreCase);
                                continue;
                            }
                        }
                        ptr = optr + declare.Length;
                        return GetNextLocalVar(filebody, ptr, out optr);
                    }
                    return "";
                }
            }
        }
        /// <summary>
        /// Поиск следующей локальной переменной
        /// </summary>
        /// <param name="script">скрипт процедуры</param>
        /// <param name="ptr">начальный указатель</param>
        /// <param name="optr">указатель результата</param>
        /// <returns>найденная строка</returns>
        public string GetNextLocalVar(string script, int ptr, out int optr)
        {
            int ptr1 = ptr;
            while (IsNotDelim(script[ptr1], new char[] { '@' }))
            {
                ptr1++;
            }
            while (!IsNotDelim(script[ptr1], new char[] { '@' }))
            {
                ptr1++;
            }
            if (script[ptr1] != '@')
            {
                ptr1--;
                return NextWord(script, ptr1, out optr);
            }
            else
            {
                optr = script.IndexOf("@", ptr, StringComparison.OrdinalIgnoreCase);
                if (optr >= 0)
                {
                    ptr = optr;
                    optr++;
                    while (optr < script.Length)
                    {
                        char lc = script[optr];
                        if (IsNotDelim(lc))
                        {
                            optr++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    return script.Substring(ptr, optr - ptr);
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// Поиск типа переменной
        /// </summary>
        /// <param name="filebody">скрипт процедуры</param>
        /// <param name="ptr">начальный указатель</param>
        /// <param name="optr">указатель результата</param>
        /// <returns>найденная строка</returns>
        public string GetNextVarType(string filebody, int ptr, out int optr)
        {
            optr = ptr;
            char lc = filebody[optr];
            while (optr < filebody.Length)
            {
                lc = filebody[optr];
                if (IsNotDelim(lc))
                {
                    break;
                }
                else
                {
                    optr++;
                }
            }
            ptr = optr;
            while (optr < filebody.Length)
            {
                lc = filebody[optr];
                if (IsNotDelim(lc, new char[]{'(',')'}))
                {
                    optr++;
                }
                else
                {
                    break;
                }
            }
            string vartype = filebody.Substring(ptr, optr - ptr);
            ptr = optr;
            if (vartype.ToLower() == "as")
                vartype = GetNextVarType(filebody, ptr, out optr);
            if (optr == filebody.Length)
                optr = -1;
            return vartype;
        }
        /// <summary>
        /// Поиск следующей глобальной переменной
        /// </summary>
        /// <param name="filebody">скрипт процедуры</param>
        /// <param name="ptr">начальный указатель</param>
        /// <param name="optr">указатель результата</param>
        /// <returns>найденная строка</returns>
        public string GetNextGlobalVarName(string filebody, int ptr, out int optr)
        {
            int str=0, end=0;
            optr = filebody.IndexOf("@@", ptr, StringComparison.OrdinalIgnoreCase);
            if (optr < 0)
                return "";
            else
            {
                while (optr >= 0)
                {
                    if (optr > 0)
                    {
                        str = optr - 1;
                        char lc = filebody[str];
                        if (IsNotDelim(lc))
                        {
                            optr = filebody.IndexOf("@@", optr + 1);
                            continue;
                        }
                    }
                    if (optr + "@@".Length < filebody.Length - 1)
                    {
                        str = optr;
                        end = optr + "@@".Length;
                        char lc = filebody[end];
                        while (IsNotDelim(lc))
                        {
                            end++;
                            lc = filebody[end];
                        }
                        optr = end;

                    }

                    return filebody.Substring(str, end - str);
                }
                return "";
            }
        }
        /// <summary>
        /// Формирование списка локальных переменных
        /// </summary>
        /// <param name="ls"></param>
        public void CollectLocalVars(List<Form1.DBProc> ls)
        {
            foreach (var x in ls)
            {
                int ptr = 0;
                int optr = 0;
                int LocalCnt=0;
                string declare = "declare";
                string script = Form1.RemoveCommentsAndHeader(x.ScriptP);
                while (ptr >= 0)
                {
                    string varname = GetNextVarName(script, ptr, out optr, declare);
                    declare = ",";
                    if (optr >= 0)
                    {
                        ptr = optr;
                        string vartype = GetNextVarType(script, ptr, out optr);
                        ptr = optr;
                        if (optr >= 0)
                        {
                            ptr = optr;
                            x.listLocal.Add(new Form1.DBVar(LocalCnt++, varname, vartype));
                        }
                    }
                    else
                    {
                        declare = "declare";
                        varname = GetNextVarName(script, ptr, out optr, declare);
                        declare = ",";
                        ptr = optr;
                        if (optr >= 0)
                        {
                            string vartype = GetNextVarType(script, ptr, out optr);
                            ptr = optr;
                            if (optr >= 0)
                            {
                                ptr = optr;
                                x.listLocal.Add(new Form1.DBVar(LocalCnt++, varname, vartype));
                            }
                        }

                    }

                }

            }
        }
        /// <summary>
        /// Формирование списка глобальных переменных
        /// </summary>
        /// <param name="ls"></param>
        public void CollectGlobalVars(List<Form1.DBProc> ls)
        {
            foreach (var x in ls)
            {
                int ptr = 0;
                int optr = 0;
                string script = Form1.RemoveCommentsAndHeader(x.ScriptP);
                int LocalCnt = 0;
                while (ptr >= 0)
                {
                    string varname = GetNextGlobalVarName(script, ptr, out optr);
                    ptr = optr;
                    if (optr >= 0)
                    {
                        ptr = optr;
                        if (optr >= 0)
                        {
                            if(!x.listGlobal.Select(a=>a.NameV).Contains(varname))
                                x.listGlobal.Add(new Form1.DBVar(LocalCnt++,varname,""));
                        }
                    }

                }

            }
        }
        /// <summary>
        /// Формирование списка используемых таблиц
        /// </summary>
        /// <param name="ls"></param>
        /// <param name="lt"></param>
        public void CollectTables(List<Form1.DBProc> ls,List<Form1.DBTable> lt)
        {
            foreach (var x in ls)
            {
                int cnt = 0;
                string script = Form1.RemoveCommentsAndHeader(x.ScriptP);
                foreach (var y in lt)
                {
                    if (x is Form1.DBTrig)
                    {
                        Form1.DBTrig x1 = x as Form1.DBTrig;
                        if (x1.TableN == y.NameT)
                            continue;
                    }
                    int ptr = 0;
                    int optr = 0;
                    string tblname = GetNextProcName(y.NameS, script, ptr, out optr);
                    if (optr >= 0)
                    {
                        x.listTables.Add(new Form1.DBVar(cnt++, y.NameT, tblname));
                    }
                }
            }
        }
        /// <summary>
        /// Следующее слово
        /// </summary>
        /// <param name="script">скрипт процедуры</param>
        /// <param name="ptr">начальный указатель</param>
        /// <param name="optr">указатель результата</param>
        /// <returns>найденная строка</returns>
        public string NextWord(string script, int ptr, out int optr)
        {
            optr = ptr;
            char lc = script[optr];
            while (optr < script.Length)
            {
                lc = script[optr];
                if (IsNotDelim(lc))
                {
                    break;
                }
                else
                {
                    optr++;
                }
            }
            ptr = optr;
            while (optr < script.Length)
            {
                lc = script[optr];
                if (IsNotDelim(lc))
                {
                    optr++;
                }
                else
                {
                    break;
                }
            }
            if (optr == script.Length)
                optr = -1;
            return script.Substring(ptr, optr - ptr);
        }
        /// <summary>
        /// Символ - не разделитель
        /// </summary>
        /// <param name="lc">символ</param>
        /// <returns></returns>
        public bool IsNotDelim(char lc)
        {
            return (lc >= 'A' && lc <= 'Z') || (lc >= 'a' && lc <= 'z') || (lc == '_') || (lc >= '0' && lc <= '9') || (lc >= 'А' && lc <= 'Я') || (lc >= 'а' && lc <= 'я' || lc == '$');
        }
        /// <summary>
        /// Символ - не разделитель
        /// </summary>
        /// <param name="lc">символ</param>
        /// <param name="ach">дополнительный массив символов - не разделителей</param>
        /// <returns></returns>
        public bool IsNotDelim(char lc, char[] ach )
        {
            bool res = IsNotDelim(lc);
            if (!res)
            {
                return ach.Contains(lc);
            }
            return res;
        }
    }
}
