﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;

namespace UnloadMDB
{
    public partial class Form1 : Form
    {
        List<string> lsTables = new List<string>();
        public Form1()
        {
            InitializeComponent();
            textBox1.Text = Properties.Settings.Default.SavePath;
            StartPosition = FormStartPosition.CenterScreen;
        }

        public void Log(string s, bool newL)
        {
            if (s == null)
                listBox1.Items.Clear();
            else
            {
                if (newL || listBox1.Items.Count == 0)
                    listBox1.Items.Add(s);
                else
                    listBox1.Items[listBox1.Items.Count - 1] = s;
                listBox1.SelectedIndex = listBox1.Items.Count - 1;
            }
        }

        private static DataTable GetSchemaTable(Guid guid, FileInfo file)
        {
            string connStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=True;Data Source=" + file.FullName + ";"; // + @"Jet OLEDB:Create System Database=true;Jet OLEDB:System database=z:\Shared\Utils\005;";
            DataTable schema;
            using (OleDbConnection conn = new OleDbConnection(connStr))
            {
                conn.Open();
                try
                {
                    schema = conn.GetOleDbSchemaTable(guid, new object[] { null, null, null, null });
                    return schema;
                }
                catch
                {
                    return null;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = null;
            try
            {
                fd = new FolderBrowserDialog();
                fd.SelectedPath = textBox1.Text;
                fd.Description = "Укажите каталог для сохранения файлов выгрузки";
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox1.Text = fd.SelectedPath;
                    
                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }
            if (fd == null)
                return;

            Log(null,true);
            if (!Directory.Exists(textBox1.Text))
            {
                MessageBox.Show("Такой директории не существует", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            DirectoryInfo di = new DirectoryInfo(textBox1.Text);
            FileInfo[] files = di.GetFiles("*.mdb", SearchOption.AllDirectories);
            listBox2.Items.Clear();
            Application.DoEvents();
            foreach (FileInfo file in files)
            {
                string fl = file.FullName.Substring(textBox1.Text.Length);
                Log(fl, true);
                //string connStr = @"Provider=Microsoft.ACE.12.0;Persist Security Info=True;Data Source=" + file.FullName + ";"; // + @"Jet OLEDB:Create System Database=true;Jet OLEDB:System database=z:\Shared\Utils\005;";
                string connStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=True;Data Source=" + file.FullName + ";"; // + @"Jet OLEDB:Create System Database=true;Jet OLEDB:System database=z:\Shared\Utils\005;";
                OleDbConnection dbCon = new OleDbConnection(connStr);
                dbCon.Open();

                DataTable tbls = dbCon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                //DataTable tbls = dbCon.GetSchema("Tables", new string[] { null, null, null, "TABLE" }); //список всех таблиц
                lsTables = new List<string>();
                foreach (DataRow row in tbls.Rows)
                {
                    string TableName = row["TABLE_NAME"].ToString();
                    lsTables.Add(TableName);
                }
                Log(fl + ".  Таблиц: " + lsTables.Count + ".", false);
                //Log(fl + " Таблиц: " + lsTables.Count, false);

                //DataTable tbls1 = dbCon.GetSchema("Procedures"); //список всех таблиц
                /*
                DataTable x;

                x = GetSchemaTable(OleDbSchemaGuid.Assertions, file); if(x != null) Log("  Assertions: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Catalogs, file); if (x != null) Log("  Catalogs: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Character_Sets, file); if (x != null) Log("  Character_Sets: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Check_Constraints, file); if (x != null) Log("  Check_Constraints: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Check_Constraints_By_Table, file); if (x != null) Log("  Check_Constraints_By_Table: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Collations, file); if (x != null) Log("  Collations: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Column_Domain_Usage, file); if (x != null) Log("  Column_Domain_Usage: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Column_Privileges, file); if (x != null) Log("  Column_Privileges: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Columns, file); if (x != null) Log("  Columns: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Constraint_Column_Usage, file); if (x != null) Log("  Constraint_Column_Usage: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Constraint_Table_Usage, file); if (x != null) Log("  Constraint_Table_Usage: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.DbInfoKeywords, file); if (x != null) Log("  DbInfoKeywords: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.DbInfoLiterals, file); if (x != null) Log("  DbInfoLiterals: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Foreign_Keys, file); if (x != null) Log("  Foreign_Keys: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Indexes, file); if (x != null) Log("  Indexes: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Key_Column_Usage, file); if (x != null) Log("  Key_Column_Usage: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Primary_Keys, file); if (x != null) Log("  Primary_Keys: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Procedure_Columns, file); if (x != null) Log("  Procedure_Columns: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Procedure_Parameters, file); if (x != null) Log("  Procedure_Parameters: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Procedures, file); if (x != null) Log("  Procedures: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Provider_Types, file); if (x != null) Log("  Provider_Types: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Referential_Constraints, file); if (x != null) Log("  Referential_Constraints: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.SchemaGuids, file); if (x != null) Log("  SchemaGuids: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Schemata, file); if (x != null) Log("  Schemata: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Sql_Languages, file); if (x != null) Log("  Sql_Languages: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Statistics, file); if (x != null) Log("  Statistics: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Table_Constraints, file); if (x != null) Log("  Table_Constraints: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Table_Privileges, file); if (x != null) Log("  Table_Privileges: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Table_Statistics, file); if (x != null) Log("  Table_Statistics: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Tables, file); if (x != null) Log("  Tables: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Tables_Info, file); if (x != null) Log("  Tables_Info: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Translations, file); if (x != null) Log("  Translations: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Trustee, file); if (x != null) Log("  Trustee: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Usage_Privileges, file); if (x != null) Log("  Usage_Privileges: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.View_Column_Usage, file); if (x != null) Log("  View_Column_Usage: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.View_Table_Usage, file); if (x != null) Log("  View_Table_Usage: " + x.Rows.Count, true);
                x = GetSchemaTable(OleDbSchemaGuid.Views, file); if (x != null) Log("  Views: " + x.Rows.Count, true);
                */
                List<string>  lsTables1 = new List<string>();
                DataTable tbls1 = dbCon.GetOleDbSchemaTable(OleDbSchemaGuid.Procedures, new object[] { null, null, null, null });
                foreach (DataRow row in tbls1.Rows)
                {
                    string TableName = row["PROCEDURE_NAME"].ToString();
                    lsTables1.Add(TableName);
                }
                Log(fl + ".  Таблиц: " + lsTables.Count + ".  Процедур: " + lsTables1.Count + ".", false);
                if (lsTables1.Count > 0)
                    listBox2.Items.Add(fl);

                /*
                OleDbPermission perm = new OleDbPermission(System.Security.Permissions.PermissionState.Unrestricted);
                string cmdStr = @"SELECT MsysObjects.Name, MsysObjects.Type FROM MsysObjects WHERE (((Left$([Name],1))<>'~') AND ((Left$([Name],4))<>'Msys')) ORDER BY MsysObjects.Name";

                OleDbCommand queries = new OleDbCommand(cmdStr, dbCon);
                OleDbDataReader reader = queries.ExecuteReader();
                */
                dbCon.Close();
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.SavePath = textBox1.Text;
            Properties.Settings.Default.Save();
        }

    }
}
