﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ExtDepends
{
    public partial class Form1 : Form
    {
        Dictionary<string, List<Word>> dcontent = new Dictionary<string, List<Word>>();
        public Form1()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            ListLog.Setup(listBox1);
            textBox1.Text = Properties.Settings.Default.Extern;
            textBox2.Text = Properties.Settings.Default.Sources;
            textBox3.Text = Properties.Settings.Default.ReportFile;
        }

        public class Word
        {
            public string word;
            public int position;
            public Word(string w, int p)
            {
                word = w;
                position = p;
            }
        }
        public class ListLog
        {
            static ListBox slb = null;
            static DateTime dStart = DateTime.Now;
            static double DT;
            static int cnt;
            public static void Setup(ListBox x)
            {
                slb = x;
                cnt = 1;
            }
            public static void Log(string s, bool newL)
            {
                if (slb != null)
                {
                    if (s == null)
                    {
                        slb.Items.Clear();
                        dStart = DateTime.Now;
                    }
                    else
                    {
                        DT = DateTime.Now.Subtract(dStart).TotalSeconds;
                        if (newL || slb.Items.Count == 0)
                            slb.Items.Add(string.Format("{0,10:F3} => " + s, DT));
                        else
                            slb.Items[slb.Items.Count - 1] = string.Format("{0,10:F3} => " + s, DT);
                        slb.SelectedIndex = slb.Items.Count - 1;
                        if (cnt % 100 == 0)
                            Application.DoEvents();
                        cnt++;
                    }
                }
            }

        }
        public class ApkFiles
        {
            static Encoding encoding = Encoding.Default;
            static public int DefFileSIZE = 16384;
            static public Encoding CurrEncoding
            {
                get
                {
                    return encoding;
                }
            }
            public static string UTF8Encoding(byte[] arrayIn)
            {
                encoding = Encoding.UTF8;
                return System.Text.Encoding.UTF8.GetString(arrayIn);
            }
            public static byte[] ReadFile(string filePath)
            {
                byte[] buffer = null;
                System.IO.FileStream fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                try
                {
                    int length = (int)fileStream.Length;
                    buffer = new byte[length];
                    if (length != fileStream.Read(buffer, 0, length))
                    {
                        throw new Exception("Не удалось прочитать файл " + filePath);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    fileStream.Close();
                }
                return buffer;
            }
            public static byte[] ArrayEncoding(byte[] arrayIn)
            {
                encoding = Encoding.Default;
                if (arrayIn == null || arrayIn.Length < 3)
                    return arrayIn;
                string txt = Encoding.Default.GetString(arrayIn.Take(3).ToArray());
                if (txt.StartsWith("п»ї"))
                {
                    encoding = Encoding.UTF8;
                    return Encoding.Default.GetBytes(Encoding.UTF8.GetString(arrayIn));
                }
                else if (txt.StartsWith("яю"))
                {
                    encoding = Encoding.Unicode;
                    return Encoding.Default.GetBytes(Encoding.Unicode.GetString(arrayIn));
                }
                return arrayIn;
            }
            public static string DefaultEncoding(string filePath)
            {
                byte[] arrayIn = ReadFile(filePath);
                if (arrayIn == null || arrayIn.Length < 3)
                    return Encoding.Default.GetString(arrayIn);
                string txt = Encoding.Default.GetString(arrayIn.Take(3).ToArray());
                if (txt.StartsWith("п»ї"))
                {
                    encoding = Encoding.UTF8;
                    return Encoding.UTF8.GetString(arrayIn);
                }
                else if (txt.StartsWith("яю"))
                {
                    encoding = Encoding.Unicode;
                    return Encoding.Unicode.GetString(arrayIn);
                }
                return Encoding.Default.GetString(arrayIn);

            }
            public static string DefaultEncoding(byte[] arrayIn)
            {
                if (arrayIn == null || arrayIn.Length < 3)
                    return Encoding.Default.GetString(arrayIn);
                string txt = Encoding.Default.GetString(arrayIn.Take(3).ToArray());
                if (txt.StartsWith("п»ї"))
                {
                    encoding = Encoding.UTF8;
                    return Encoding.UTF8.GetString(arrayIn);
                }
                else if (txt.StartsWith("яю"))
                {
                    encoding = Encoding.Unicode;
                    return Encoding.Unicode.GetString(arrayIn);
                }
                return Encoding.Default.GetString(arrayIn);

            }
            public static string UTF8Encoding(string filePath)
            {
                return UTF8Encoding(ReadFile(filePath));
            }
            static private Int64 MaskSum(Int64[] sF, byte[] mask)
            {
                Int64 ret = 0;
                for (int i = 0; i < 256; i++)
                {
                    if (((0x80 >> (i % 8)) & mask[i / 8]) != 0)
                    {
                        ret += sF[i];
                    }
                }
                return ret;
            }
            static private bool TextFile(byte[] arr)
            {
                byte[] TextMask = { 0xFF, 0x9B, 0xFF, 0xDF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

                Int64[] Signature = new Int64[256];
                for (int i = 0; i < arr.Length; i++)
                {
                    Signature[arr[i]]++;
                }
                if (MaskSum(Signature, TextMask) == 0)
                    return true;
                return false;

            }
            public static bool TextOrNot(string filename, out byte[] arrA)
            {

                arrA = new byte[DefFileSIZE];
                System.IO.BinaryReader br = null;
                System.IO.FileStream fs = null;
                int length = 0;
                try
                {
                    fs = new System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    br = new System.IO.BinaryReader(fs);
                    length = br.Read(arrA, 0, DefFileSIZE);
                }
                finally
                {
                    if (fs != null)
                        fs.Close();
                }
                if (length != 0)
                {
                    if (length < DefFileSIZE)
                        arrA = arrA.Take(length).ToArray();
                    return (TextFile(ArrayEncoding(arrA)) == true);
                }
                return false;
            }
            public static string LastBytes(string filename)
            {
                string txt = "";
                System.IO.BinaryReader br = null;
                System.IO.FileStream fs = null;
                long length = 0;
                try
                {
                    fs = new System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    br = new System.IO.BinaryReader(fs);
                    length = fs.Length;
                    if (length > DefFileSIZE)
                    {
                        fs.Seek(DefFileSIZE, System.IO.SeekOrigin.End);
                        length = DefFileSIZE;
                    }
                    byte[] arrA = new byte[length];
                    br.Read(arrA, 0, (int)length);
                    txt = BitConverter.ToString(arrA).Replace("-", "");
                }
                finally
                {
                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
                return txt;
            }
            public static byte[] DefaultDecoding(string s)
            {
                if (s == null)
                    return null;
                return encoding.GetBytes(s);
            }
            public static void WriteFile(string filePath, byte[] buffer)
            {
                System.IO.FileStream fileStream = null;
                try
                {
                    if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(filePath)) == false)
                        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(filePath));
                    fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                    fileStream.Write(buffer, 0, buffer.Length);
                }
                catch (UnauthorizedAccessException ex)
                {
                    throw ex;
                }
                finally
                {
                    if (fileStream != null)
                        fileStream.Close();
                }
            }
            // ReadLineNumber: reads the Nth line from the file specified by filepath.
            // Requires System.IO namespace
            public static String readLineNumber(String filepath, int linenumber)
            {

                if (linenumber > 0)
                {
                    if (File.Exists(filepath))
                    {
                        using (StreamReader reader = new System.IO.StreamReader(filepath))
                        {
                            var count = 1;

                            // Peek to see if there is a line to be read.
                            while (reader.Peek() >= 0)
                            {
                                if (count == linenumber)
                                {
                                    return reader.ReadLine();
                                }
                                reader.ReadLine();
                                count++;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("File specified does not exist.");
                    }

                    throw new Exception("File has fewer lines than line number specified.");
                }
                throw new Exception("Line number must be larger than 0");
            }
        }
        public class ScanText
        {
            string Text;
            List<char> listChars;
            public ScanText(string text, string chars = null)
            {
                Text = text;
                listChars = CharsInit(chars);

            }
            public string Scan(string shablon, int pos, out int opos)
            {
                opos = Text.IndexOf(shablon, pos);
                if (opos != -1)
                    return shablon;
                else
                    return "";
            }
            public string TryGetMaxWord(string shablon, int ptr, out int optr)
            {
                optr = Text.IndexOf(shablon, ptr);
                if (optr == -1)
                    return "";
                ptr = optr;
                while (ptr >= 0)
                {
                    if (IsChar(Text[ptr], listChars))
                        ptr--;
                    else
                        break;
                }
                ptr++;
                optr += shablon.Length;
                while (optr < Text.Length)
                {
                    if (IsChar(Text[optr], listChars))
                        optr++;
                    else
                        break;
                }
                return Text.Substring(ptr, optr - ptr);
            }
            public string TryGetMaxPath(string shablon, int ptr, out int optr)
            {
                optr = Text.IndexOf(shablon, ptr);
                if (optr == -1)
                    return "";
                ptr = optr;
                // Ищем в тексте влево и вправо от найденного слова все допустимые символы в наименовании пути к файлу
                while (ptr >= 0)
                {
                    if (IsChar(Text[ptr], listChars))
                        ptr--;
                    else
                        break;
                }
                ptr++;
                optr += shablon.Length;
                while (optr < Text.Length)
                {
                    if (IsChar(Text[optr], listChars))
                        optr++;
                    else
                        break;
                }
                // Делаем знак разделения папок одинаковым
                string sret = Text.Substring(ptr, optr - ptr).Replace("/", "\\");
                // Если путь включаает в себя элементы ..\, то все, что до них (включая) - нужно убрать, т.к. мы не знаем стартовой папки
                int ptr1 = sret.LastIndexOf("..\\");
                if (ptr1 >= 0)
                {
                    optr += ptr1 + "..\\".Length;
                    sret = sret.Replace("..\\", "");

                }
                return sret;
            }
            static bool IsChar(char x, List<char> ls)
            {
                return ls.Contains(x);
            }
            List<char> CharsInit(string lci)
            {
                List<char> lc = new List<char>();
                string listCharInit;
                if (string.IsNullOrEmpty(lci))
                    listCharInit = @"A-Z|a-z|А-Я|а-я|0-9|_";
                else
                    listCharInit = lci;
                lc.Clear();
                string[] sa = listCharInit.Split(new char[] { '|' }).Where(x => x != "").ToArray();
                foreach (string s in sa)
                {
                    if (s == "-")
                    {
                        lc.Add(s[0]);
                    }
                    else
                    {
                        if (s.Contains("-"))
                        {
                            string[] sb = s.Split('-').Where(x => x != "").ToArray();
                            if (sb.Length >= 2)
                            {
                                List<char> listt = Enumerable.Range((int)sb[0][0], (int)sb[1][0] - (int)sb[0][0] + 1).Select(x => Convert.ToChar(x)).ToList();
                                lc.AddRange(listt);
                            }
                        }
                        else
                            lc.Add(s[0]);
                    }
                }
                return lc.Distinct().ToList();
            }
            public string NextWord(int ptr, out int optr)
            {
                optr = ptr;
                while (optr < Text.Length)
                {
                    char lc = Text[optr];
                    if (IsChar(lc, listChars))
                    {
                        break;
                    }
                    else
                    {
                        optr++;
                    }
                }
                ptr = optr;
                while (optr < Text.Length)
                {
                    char lc = Text[optr];
                    if (IsChar(lc, listChars))
                    {
                        optr++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (optr == Text.Length)
                {
                    optr = -1;
                    return null;
                }
                return Text.Substring(ptr, optr - ptr);
            }
        }
        public class ScanList : IEnumerable
        {
            private string Text;

            public ScanList(string text)
            {
                Text = text;
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return (IEnumerator)GetEnumerator();
            }

            public ScanEnum GetEnumerator()
            {
                return new ScanEnum(Text);
            }

        }
        public class ScanEnum : IEnumerator
        {
            int cursor = -1;
            string x = null;
            ScanText st = null;

            public ScanEnum(string t)
            {
                st = new ScanText(t);
            }

            public bool MoveNext()
            {
                int optr;
                if (st == null)
                    return false;
                cursor++;
                x = st.NextWord(cursor, out optr);
                cursor = optr;
                if (cursor == -1)
                {
                    return false;
                }
                return true;
            }


            public void Reset()
            {
                cursor = -1;
            }

            object IEnumerator.Current
            {
                get
                {
                    return Current;
                }
            }
            public Word Current
            {
                get
                {
                    try
                    {
                        return new Word(x, cursor);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidOperationException();
                    }
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = null;
            try
            {
                fd = new FolderBrowserDialog();

                fd.SelectedPath = Properties.Settings.Default.Extern;
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox1.Text = fd.SelectedPath;

                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Extern = textBox1.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.Sources = textBox2.Text;
            Properties.Settings.Default.Save();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ReportFile = textBox3.Text;
            Properties.Settings.Default.Save();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = null;
            try
            {
                fd = new FolderBrowserDialog();

                fd.SelectedPath = Properties.Settings.Default.Sources;
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox2.Text = fd.SelectedPath;

                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog fd = null;
            try
            {
                fd = new SaveFileDialog();

                fd.FileName = Properties.Settings.Default.ReportFile;
                DialogResult dr = fd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox3.Text = fd.FileName;

                }
            }
            finally
            {
                if (fd != null)
                    fd.Dispose();
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ListLog.Log(null, true);
            DirectoryInfo die = new DirectoryInfo(textBox1.Text);
            DirectoryInfo dis = new DirectoryInfo(textBox2.Text);
            List<string> report = new List<string>();
            ListLog.Log("Индексирование содержимого", true);

            foreach (FileInfo fie in die.GetFiles("*.*", SearchOption.AllDirectories))
            {
                int cnt = 1;
                ListLog.Log(fie.Name, true);
                string sFileBody;
                byte[] arrA;
                if (!ApkFiles.TextOrNot(fie.FullName, out arrA))
                    continue;
                if (arrA.Length < ApkFiles.DefFileSIZE)
                    sFileBody = ApkFiles.UTF8Encoding(arrA).ToLower();
                else
                    sFileBody = ApkFiles.UTF8Encoding(ApkFiles.ReadFile(fie.FullName)).ToLower();
                ScanText st = new ScanText(sFileBody);

                ScanList slist;
                slist = new ScanList(sFileBody);
                foreach (Word id in slist)
                {
                    if (!dcontent.Keys.Contains(id.word))
                    {
                        dcontent.Add(id.word, new List<Word>());
                    }
                    dcontent[id.word].Add(new Word(fie.FullName, id.position));
                    ListLog.Log(fie.Name+" "+cnt, false);
                    cnt++;
                }

            }


            foreach (FileInfo fie in die.GetFiles("*.*", SearchOption.AllDirectories))
            {
                ListLog.Log(fie.Name, true);
                string sFileBody;
                byte[] arrA;
                if (!ApkFiles.TextOrNot(fie.FullName, out arrA))
                    continue;
                if (arrA.Length < ApkFiles.DefFileSIZE)
                    sFileBody = ApkFiles.UTF8Encoding(arrA).ToLower();
                else
                    sFileBody = ApkFiles.UTF8Encoding(ApkFiles.ReadFile(fie.FullName)).ToLower();
                ScanText st = new ScanText(sFileBody);






                int cnt = 1;
                foreach (FileInfo fis in dis.GetFiles("*.*", SearchOption.AllDirectories))
                {
                    string swe = fis.Name.Substring(0,fis.Name.LastIndexOf(fis.Extension)).ToLower();
                    int index = sFileBody.IndexOf(swe);
                    if (index != -1)
                    {
                        int ptr = index;
                        int optr;
                        string sres = st.TryGetMaxPath(swe, ptr, out optr);
                        while (optr != -1)
                        {
                            if (fis.FullName.IndexOf(sres) != -1)
                            {
                                if(!report.Contains(fis.FullName))
                                    report.Add(fis.FullName);
                                break;
                            }
                            else
                            {
                                ptr = optr;
                                sres = st.TryGetMaxPath(swe, ptr, out optr);
                            }
                        }
                    }
                    if(cnt%100==0)
                        ListLog.Log(fie.Name+Enumerable.Repeat('.',cnt/100).Select(x=>(char)x).Select(x=>x.ToString()) .Aggregate((a,b)=>a+b), false);
                    cnt++;

                }
            }
            File.WriteAllLines(textBox3.Text, report);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ListLog.Log(null, true);
            List<string> listStep1 = new List<string>();
            DirectoryInfo die = new DirectoryInfo(textBox1.Text);
            DirectoryInfo dis = new DirectoryInfo(textBox2.Text);
            List<string> files = dis.GetFiles("*.*", SearchOption.AllDirectories).Select(x => x.FullName).ToList();
            List<string> lFwE = dis.GetFiles("*.*", SearchOption.AllDirectories).Select(x => x.Name.Substring(0, x.Name.LastIndexOf(x.Extension)).ToLower()).ToList();
            List<List<char>> lfwech = lFwE.Select(y => Enumerable.Range(0, y.Length - 1).Select(x => y[x]).Distinct().ToList()).ToList();
            foreach (FileInfo fie in die.GetFiles("*.*", SearchOption.AllDirectories))
            {
                int cnt = 0;
                int cnt1 = 1;

                ListLog.Log(fie.Name, true);
                string sFileBody;
                byte[] arrA;
                if (!ApkFiles.TextOrNot(fie.FullName, out arrA))
                    continue;
                if (arrA.Length < ApkFiles.DefFileSIZE)
                    sFileBody = ApkFiles.UTF8Encoding(arrA).ToLower();
                else
                    sFileBody = ApkFiles.UTF8Encoding(ApkFiles.ReadFile(fie.FullName)).ToLower();

                List<char> lch = Enumerable.Range(0, sFileBody.Length - 1).Select(x => sFileBody[x]).Distinct().ToList();
                for(int i = 0;i<lfwech.Count;i++)
                {
                    bool bgood = true;
                    for (int j = 0; j < lfwech[i].Count; j++)
                    {
                        if (!lch.Contains(lfwech[i][j]))
                        {
                            bgood = false;
                            break;
                        }
                    }
                    if (bgood)
                    {
                        if (!listStep1.Contains(fie.FullName))
                        {
                            listStep1.Add(fie.FullName);
                            cnt++;
                        }
                    }
                    if (cnt1 % 50 ==0)
                    {
                        if (cnt > 0)
                            ListLog.Log(fie.Name + " " + cnt + " " + Enumerable.Repeat(".", cnt1 / 50).Aggregate((a, b) => a + b), false);
                        else
                            ListLog.Log(fie.Name + " " + Enumerable.Repeat(".", cnt1 / 50).Aggregate((a, b) => a + b), false);
                    }
                    cnt1++;
                }

                if (listStep1.Contains(fie.FullName))
                {
                    ScanText st = new ScanText(sFileBody);

                    ScanList slist;
                    slist = new ScanList(sFileBody);
                    foreach (Word id in slist)
                    {
                        if (lFwE.Contains(id.word))
                        {
                            if (!dcontent.Keys.Contains(id.word))
                            {
                                dcontent.Add(id.word, new List<Word>());
                            }
                            for (int i = 0; i < lFwE.Count; i++)
                            {
                                if (lFwE[i] == id.word)
                                {
                                    dcontent[id.word].Add(new Word(files[i], id.position));
                                    cnt++;
                                }
                            }
                        }
                    }
                }

            }

            List<string> report=new List<string>();
            foreach (string k in dcontent.Keys)
            {
                foreach (var x in dcontent[k])
                {
                    if (!report.Contains(x.word))
                        report.Add(x.word);
                }
            }


            File.WriteAllLines(textBox3.Text, report);

        }
        public List<Word> myAddRange(List<Word> a, List<Word> b)
        {
            a.Concat(b);
            return a;
        }
    }
}
