#pragma once


using namespace System;
using namespace System::Collections::Generic;


#include "..\..\..\Externals\Berkeley\db_cxx.h"
#include <errno.h>
#include "stBufferRead.h"
#include "stBufferWrite.h"
#include "table.h"
#include <malloc.h>

namespace Store {
namespace Table {


enum enCursorControl
{
	CURSORMATCH = DB_SET, //������ �������� ���������� ������ �� ���������, ����������� � ������.
	CURSORSTARTFROM = DB_SET_RANGE //������ ��������� � ��������, �������� ��� ������� ������ � �������� ����������� �����.
};

ref class CursorKey :IDisposable
{
protected:
	    Dbc **cursor;
		Dbt *key;
		Table ^table;
		enCursorControl control;
		void *buffer;

public:
	CursorKey(Table ^table, IBufferWriter ^keyBuffer, enCursorControl control)
	{
		System::Threading::Monitor::Enter(table->lockObj);
		this->table = table;
		this->control = control;

		stBufferWrite ^keyWriter = (stBufferWrite^)keyBuffer;
		buffer = malloc(keyWriter->GetUsedBufferSize());
		memcpy(buffer, keyWriter->GetBuffer(), keyWriter->GetUsedBufferSize());
		this->key = new Dbt(buffer, keyWriter->GetUsedBufferSize());

		cursor = new Dbc*;
		
		try{
			table->GetDb()->cursor(NULL, cursor, 0);
		} catch(DbException &e) {
					cursor = NULL;

					System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
					build->Append("Error getting cursor database ObjectWithIDManager_table: ");
					build->Append(". Error DbException: ");
					build->Append(gcnew String(e.what()));
					build->Append(".");
					IA::Monitor::Log::Error("���������", build->ToString(), false);
		} catch(std::exception &e) {
					cursor = NULL;

					System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
					build->Append("Error getting cursor database ObjectWithIDManager_table: ");
					build->Append(". Error STD: ");
					build->Append(gcnew String(e.what()));
					build->Append(".");
					IA::Monitor::Log::Error("���������", build->ToString(), false);
		}		
		finally
		{
			System::Threading::Monitor::Exit(table->lockObj);
		}
	}

	~CursorKey()
	{
		Destroy();
	}

private:
	void Destroy()
	{
		if(cursor != NULL)
		{
			try
			{
				(*cursor)->close();
			}
			catch(DbException &e) { e=e;
			} 
			catch(std::exception &e) { e=e;
			}		
			delete cursor;
			cursor = NULL;
			delete key;
			delete buffer;
		}
	}

public:
	stBufferRead ^First()
	{
		System::Threading::Monitor::Enter(table->lockObj);
		
		Dbt data;
		data.set_flags(DB_DBT_MALLOC /*| DB_DBT_APPMALLOC*/);

		try{
			if((*cursor)->get(key, &data, control) != DB_NOTFOUND && data.get_size() != 0)
			{
				stBufferRead ^res = (stBufferRead ^) ReaderPool::Get();
				res->Set(data.get_data(), data.get_size());
				return res;
			}
			else
				return nullptr;
		} catch(DbException &e) {
			System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
			build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error DbException: ");
			build->Append(gcnew String(e.what()));
			build->Append(".");
			IA::Monitor::Log::Error("���������", build->ToString(), false);
		} catch(std::exception &e) {
			System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
			build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error STD: ");
			build->Append(gcnew String(e.what()));
			build->Append(".");
			IA::Monitor::Log::Error("���������", build->ToString(), false);
		}
		finally
		{
			System::Threading::Monitor::Exit(table->lockObj);
		}

		return nullptr;
	}

	stBufferRead ^Next()
	{
		System::Threading::Monitor::Enter(table->lockObj);

		Dbt data;
		data.set_flags(DB_DBT_MALLOC /*| DB_DBT_APPMALLOC*/);

		try{
			u_int32_t param = (control == DB_SET) ? DB_NEXT_DUP : DB_NEXT;

			if((*cursor)->get(key, &data, param) != DB_NOTFOUND && data.get_size() != 0)
			{
				stBufferRead ^res = (stBufferRead ^)ReaderPool::Get();
				res->Set(data.get_data(), data.get_size());
				return res;
			}
			else
				return nullptr;
		} catch(DbException &e) {
			System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
			build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error DbException: ");
			build->Append(gcnew String(e.what()));
			build->Append(".");
			IA::Monitor::Log::Error("���������", build->ToString(), false);
		} catch(std::exception &e) {
			System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
			build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error STD: ");
			build->Append(gcnew String(e.what()));
			build->Append(".");
			IA::Monitor::Log::Error("���������", build->ToString(), false);
		}
		finally
		{
			System::Threading::Monitor::Exit(table->lockObj);
		}

		return nullptr;
	}

};

ref class Cursor : IDisposable
{
protected:
	    Dbc **cursor;
	    bool is_cursor_at_end;
		Table ^table;

public:
	Cursor(Table ^table)
	{
		System::Threading::Monitor::Enter(table->lockObj);
		this->table = table;

		is_cursor_at_end = false;
		cursor = new Dbc*;
		
		try{
			table->GetDb()->cursor(NULL, cursor, 0);
		} catch(DbException &e) {
					cursor = NULL;

					System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
					build->Append("Error getting cursor database ObjectWithIDManager_table: ");
					build->Append(". Error DbException: ");
					build->Append(gcnew String(e.what()));
					build->Append(".");
					IA::Monitor::Log::Error("���������", build->ToString(), false);
		} catch(std::exception &e) {
					cursor = NULL;

					System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
					build->Append("Error getting cursor database ObjectWithIDManager_table: ");
					build->Append(". Error STD: ");
					build->Append(gcnew String(e.what()));
					build->Append(".");
					IA::Monitor::Log::Error("���������", build->ToString(), false);
		}		
		finally
		{
			System::Threading::Monitor::Exit(table->lockObj);
		}
	}

	~Cursor()
	{
		Destroy();
	}

private:
	void Destroy()
	{
		if(cursor != NULL)
		{
			try
			{
				(*cursor)->close();
			}
			catch(DbException &e) { e=e;
			} 
			catch(std::exception &e) { e=e;
			}		
			catch(void *e)
			{e=e;}
			catch(...)
			{}
			delete cursor;
			cursor = NULL;
		}
	}
public:

	KeyValuePair<IBufferReader ^, IBufferReader ^> First()
	{
		System::Threading::Monitor::Enter(table->lockObj);

		is_cursor_at_end = false;
		Dbt key, data;
		key.set_flags(DB_DBT_MALLOC | DB_DBT_APPMALLOC);
		data.set_flags(DB_DBT_MALLOC | DB_DBT_APPMALLOC);

		try{
			int ret = (*cursor)->get(&key, &data, DB_FIRST );
			if(ret == DB_NOTFOUND)
				return KeyValuePair<IBufferReader ^, IBufferReader ^>(nullptr, nullptr);
			else
			{
				stBufferRead ^keyReader = (stBufferRead ^) ReaderPool::Get();
				stBufferRead ^valueReader = (stBufferRead ^) ReaderPool::Get();
				keyReader->Set(key.get_data(), key.get_size());
				valueReader->Set(data.get_data(), data.get_size());

				return KeyValuePair<IBufferReader ^, IBufferReader ^>(keyReader, valueReader);
			}
		} catch(DbException &e) {
			System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
			build->Append("Error ObjectWithIDManager_table::FirstProject(int). Error DbException: ");
			build->Append(gcnew String(e.what()));
			build->Append(".");
			IA::Monitor::Log::Error("���������", build->ToString(), false);
		} catch(std::exception &e) {
			System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
			build->Append("Error ObjectWithIDManager_table::FirstProject(int). Error STD: ");
			build->Append(gcnew String(e.what()));
			build->Append(".");
			IA::Monitor::Log::Error("���������", build->ToString(), false);
		}
		finally
		{
			System::Threading::Monitor::Exit(table->lockObj);
		}

		return KeyValuePair<IBufferReader ^, IBufferReader ^>(nullptr, nullptr);
	}

protected:
	KeyValuePair<IBufferReader ^, IBufferReader ^> Next(int flags)
	{
		System::Threading::Monitor::Enter(table->lockObj);

		Dbt key, data;
		key.set_flags(DB_DBT_MALLOC | DB_DBT_APPMALLOC);
		data.set_flags(DB_DBT_MALLOC | DB_DBT_APPMALLOC);

		if(is_cursor_at_end)
		{
			return KeyValuePair<IBufferReader ^, IBufferReader ^>(nullptr, nullptr);
		}

		try{
			if( (*cursor)->get(&key, &data, flags) == DB_NOTFOUND) 
			{
				is_cursor_at_end = true;
				return KeyValuePair<IBufferReader ^, IBufferReader ^>(nullptr, nullptr);
			}
			else
			{
				stBufferRead ^keyReader = (stBufferRead ^) ReaderPool::Get();
				stBufferRead ^valueReader = (stBufferRead ^) ReaderPool::Get();
				keyReader->Set(key.get_data(), key.get_size());
				valueReader->Set(data.get_data(), data.get_size());

				return KeyValuePair<IBufferReader ^, IBufferReader ^>(keyReader, valueReader);
			}
		} catch(DbException &e) {
			System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
			build->Append("Error ObjectWithIDManager_table::GetNextProject(int). Error DbException: ");
			build->Append(gcnew String(e.what()));
			build->Append(".");
			IA::Monitor::Log::Error("���������", build->ToString(), false);
		} catch(std::exception &e) {
			System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
			build->Append("Error ObjectWithIDManager_table::GetNextProject(int). Error STD: ");
			build->Append(gcnew String(e.what()));
			build->Append(".");
			IA::Monitor::Log::Error("���������", build->ToString(), false);
		}
		finally
		{
			System::Threading::Monitor::Exit(table->lockObj);
		}

		return KeyValuePair<IBufferReader ^, IBufferReader ^>(nullptr, nullptr);
	}

public:
	KeyValuePair<IBufferReader ^, IBufferReader ^> NextNoDub()
	{
		return Next(DB_NEXT_NODUP);
	}

	KeyValuePair<IBufferReader ^, IBufferReader ^> NextDub()
	{
		return Next(DB_NEXT);
	}

};


///��������, Current �������� �� ����� ������ ����!
ref class IEnumeratorValueByKey : IEnumerator<IBufferReader ^>, IEnumerable<IBufferReader ^>
{
	bool befor_first;
	CursorKey ^cursorKey;
	stBufferRead ^Current;
	bool isRemoveCurrent;
	void *buffer;

internal:
	IEnumeratorValueByKey(Table ^table, IBufferWriter ^keyBuffer, enCursorControl control)
	{
		befor_first = true;

		cursorKey = gcnew CursorKey(table, keyBuffer, control);

		isRemoveCurrent = true;
	}

	~IEnumeratorValueByKey()
	{
		if(isRemoveCurrent && Current != nullptr)
			delete Current;
		delete cursorKey;
		delete buffer;
	}

	virtual Object^ get_() = System::Collections::IEnumerator::Current::get
	{
		isRemoveCurrent = false;
		return Current;
	}

	virtual IBufferReader ^ get() = System::Collections::Generic::IEnumerator<IBufferReader ^>::Current::get
	{
		isRemoveCurrent = false;
		return Current;
	}

	virtual bool MoveNext() = System::Collections::IEnumerator::MoveNext
	{
		isRemoveCurrent = true;
		if(befor_first)
		{
			befor_first = false;
			Current = cursorKey->First();
		}
		else
		{
			if(isRemoveCurrent)
				delete Current;

			Current = cursorKey->Next();
		}

		return Current != nullptr;
	}

	virtual void Reset() = System::Collections::IEnumerator::Reset
	{
		isRemoveCurrent = true;

		if(isRemoveCurrent)
			Current = nullptr;

		befor_first = true;
	}

	virtual IEnumerator<IBufferReader ^> ^GetEnumerator() = System::Collections::Generic::IEnumerable<IBufferReader ^>::GetEnumerator
	{
		return this;
	}

internal:
	virtual System::Collections::IEnumerator ^GetEnumerator1() = System::Collections::IEnumerable::GetEnumerator
	{
		return this;
	}
};


///��������, Current �������� �� ����� ������ ����!
ref class IEnumeratorUInt64ValuePair abstract: IEnumerator<KeyValuePair<UInt64, IBufferReader ^>>
{
protected:
	bool befor_first;
	Cursor ^cursor;
	KeyValuePair<UInt64, IBufferReader ^> Current;

internal:

	virtual KeyValuePair<IBufferReader ^, IBufferReader ^> Next() = 0;

	IEnumeratorUInt64ValuePair(Table ^table)
	{
		befor_first = true;

		cursor = gcnew Cursor(table);
	}

	~IEnumeratorUInt64ValuePair()
	{
		delete cursor;
	}

	virtual Object^ get_() = System::Collections::IEnumerator::Current::get
	{
		return get();
	}

	virtual KeyValuePair<UInt64, IBufferReader ^> get() = System::Collections::Generic::IEnumerator<KeyValuePair<UInt64, IBufferReader ^>>::Current::get
	{
		return Current;
	}

	virtual bool MoveNext() = System::Collections::IEnumerator::MoveNext
	{
		KeyValuePair<IBufferReader ^, IBufferReader ^> cur;
		if(befor_first)
		{
			befor_first = false;
			cur = cursor->First();
		}
		else
			cur = Next();

		if(cur.Key == nullptr)
		{
			Current = KeyValuePair<UInt64, IBufferReader ^>(0, nullptr);
			return false;
		}
		else
		{
			Current = KeyValuePair<UInt64, IBufferReader ^>(cur.Key->GetUInt32(), cur.Value);
			return true;
		}
	}

	virtual void Reset() = System::Collections::IEnumerator::Reset
	{
		befor_first = true;
	}
};

///��������, Current �������� �� ����� ������ ����!
ref class IEnumeratorUInt64ValuePair_Dub : IEnumeratorUInt64ValuePair
{
internal:
	virtual KeyValuePair<IBufferReader ^, IBufferReader ^> Next() override
	{
		return cursor->NextDub();
	}

	IEnumeratorUInt64ValuePair_Dub(Table ^table) : IEnumeratorUInt64ValuePair(table){}

	~IEnumeratorUInt64ValuePair_Dub()
	{
		delete cursor;
	}
};

///��������, Current �������� �� ����� ������ ����!
ref class IEnumeratorUInt64ValuePair_NoDub : IEnumeratorUInt64ValuePair
{
internal:

	virtual KeyValuePair<IBufferReader ^,IBufferReader ^> Next() override
	{
		return cursor->NextNoDub();
	}

	IEnumeratorUInt64ValuePair_NoDub(Table ^table) : IEnumeratorUInt64ValuePair(table){}

	~IEnumeratorUInt64ValuePair_NoDub()
	{
	}

};


///��������, Current �������� �� ����� ������ ����!
ref class IEnumeratorKeyValuePair abstract: IEnumerator<KeyValuePair<IBufferReader ^, IBufferReader ^>>
{
protected:
	bool befor_first;
	Cursor ^cursor;
	KeyValuePair<IBufferReader ^, IBufferReader ^> Current;

internal:

	virtual KeyValuePair<IBufferReader ^, IBufferReader ^> Next() = 0;

	IEnumeratorKeyValuePair(Table ^table)
	{
		befor_first = true;

		cursor = gcnew Cursor(table);
	}

	~IEnumeratorKeyValuePair()
	{
		delete cursor;
	}

	virtual Object^ get_() = System::Collections::IEnumerator::Current::get
	{
		return get();
	}

	virtual KeyValuePair<IBufferReader ^, IBufferReader ^> get() = System::Collections::Generic::IEnumerator<KeyValuePair<IBufferReader ^, IBufferReader ^>>::Current::get
	{
		return Current;
	}

	virtual bool MoveNext() = System::Collections::IEnumerator::MoveNext
	{
		if(befor_first)
		{
			befor_first = false;
			Current = cursor->First();
		}
		else
			Current = Next();

		return Current.Key != nullptr;
	}

	virtual void Reset() = System::Collections::IEnumerator::Reset
	{
		befor_first = true;
	}
};

///��������, Current �������� �� ����� ������ ����!
ref class IEnumeratorKeyValuePair_Dub : IEnumeratorKeyValuePair
{
internal:

	virtual KeyValuePair<IBufferReader ^, IBufferReader ^> Next() override
	{
		return cursor->NextDub();
	}

	IEnumeratorKeyValuePair_Dub(Table ^table) : IEnumeratorKeyValuePair(table){}

	~IEnumeratorKeyValuePair_Dub()
	{
	}
};

///��������, Current �������� �� ����� ������ ����!
ref class IEnumeratorKeyValuePair_NoDub : IEnumeratorKeyValuePair
{
internal:

	virtual KeyValuePair<IBufferReader ^, IBufferReader ^> Next() override
	{
		return cursor->NextNoDub();
	}

	IEnumeratorKeyValuePair_NoDub(Table ^table) : IEnumeratorKeyValuePair(table){}

	~IEnumeratorKeyValuePair_NoDub()
	{
	}

};
}}