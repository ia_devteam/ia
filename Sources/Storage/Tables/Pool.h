#pragma once

using namespace System;

#include "..\..\..\Externals\Berkeley\db_cxx.h"
#include <errno.h>
#include "stBufferRead.h"
#include "stBufferWrite.h"
#include <malloc.h>

namespace Store {
namespace Table {


	public ref class ReaderPool sealed
	{
		ReaderPool(){}

	public:
		static IBufferReader ^Get()
		{
			return gcnew stBufferRead();
		}
	};

	public ref class WriterPool sealed
	{
		WriterPool(){}
	public:
		static IBufferWriter ^Get()
		{
			return gcnew stBufferWrite();
		}
	};

	///TODO: ����������� ��� ��������

}}