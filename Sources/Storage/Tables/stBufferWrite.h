#pragma once

#using "..\..\..\Binaries\Common\Storage_Constants.dll"

using namespace System;
using namespace System::Text;
#include "..\..\..\Externals\Berkeley\db_cxx.h"

namespace Store {
namespace Table {

	public interface class IBufferWriter : System::IDisposable
	{
	public:
		//������� ������ ����� ������� ������
		virtual IBufferWriter ^Clone();

		virtual void Add(Byte value);
		virtual void Add(Int32 value);
		virtual void Add(UInt32 value);
		virtual void Add(Int64 value);
		virtual void Add(UInt64 value);
		//���������� Uint64 � �����, ���������� ����� � ���������� ������������ ������� (���������� ����� �������� � ������������ �����)
		virtual void AddTransformed(UInt64 value);
        virtual void Add(Int128 value);
		virtual void Add(Double value);
        virtual void Add(MegaDouble value);
        virtual void Add(Boolean value);
		virtual void Add(Boolean value1, Boolean value2);
		virtual void Add(Boolean value1, Boolean value2, Boolean value3);
        virtual void Add(String ^value);
		virtual void Add(array<Byte> ^value);
		
		//������� ��� ���������� � ����� ����������
		virtual void Clear();

		virtual int Size();
    };


	public ref class stBufferWrite : IBufferWriter
	{
	public:
		stBufferWrite(void)
		{
			buffer = malloc(1024);
			if(buffer == NULL)
			{
				buffer = malloc(1024);
				if(buffer == NULL)
				{
					int error = errno;
					throw gcnew Exception("�� ������� �������� ������. stBufferWrite.stBufferWrite ������ � malloc");
				}
			}
			size = 1024;
			UsedSize = 0;
		}

		~stBufferWrite()
		{
			if(buffer != NULL)
			{
				free(buffer);
				buffer = NULL;
				UsedSize = 0;
			}
			System::GC::SuppressFinalize(this);
		}

		!stBufferWrite()
		{
			if(buffer != NULL)
			{
				free(buffer);
				buffer = NULL;
				UsedSize = 0;
			}
		}

		virtual IBufferWriter ^ Clone()
		{
			stBufferWrite ^writer = gcnew stBufferWrite();
			writer->CheckSize(size);
			memcpy(writer->buffer, buffer, UsedSize);
			writer->UsedSize = UsedSize;
			return writer;
		}


		void *GetBuffer()
		{
			return buffer;
		}

		int GetUsedBufferSize()
		{
			return UsedSize;
		}

		virtual void Clear()
		{
			UsedSize = 0;
		}

		virtual void Add(System::Byte value)
		{
			char val = value;
			CheckSize(1);
			*(char *)GetOff() = val;
			UsedSize += 1;
		}

		virtual void Add(System::Int32 value)
		{
			int val = value;
			CheckSize(4);
			*(int *)GetOff() = val;
			UsedSize += 4;
		}

		virtual void Add(System::UInt32 value)
		{
			unsigned int val = value;
			CheckSize(4);
			*(unsigned int *)GetOff() = val;
			UsedSize += 4;
		}

		virtual void Add(System::Int64 value)
		{
			__int64 val = value;
			CheckSize(8);
			*(__int64 *)GetOff() = val;
			UsedSize += 8;
		}

		virtual void Add(System::UInt64 value)
		{
			__int64 val = value;
			CheckSize(8);
			*(__int64 *)GetOff() = val;
			UsedSize += 8;
		}

		virtual void AddTransformed(System::UInt64 value)
		{
			__int64 val = value;
			__int64 res;
			char *ref_val = (char *)&val;
			char *ref_res = (char *)&res;
			for(int i=0;i<8;i++)
				ref_res[i] = ref_val[7-i];

			CheckSize(8);
			*(__int64 *)GetOff() = res;
			UsedSize += 8;
		}

		virtual void Add(Store::Const::Int128 value)
		{
			throw gcnew Exception("Not Realized");
		}

		virtual void Add(System::Double value)
		{
			double v = value;
			__int64 val = *(__int64 *)&v;
			CheckSize(8);
			*(__int64 *)GetOff() = val;
			UsedSize += 8;
		}

		virtual void Add(Store::Const::MegaDouble value)
		{
			throw gcnew Exception("Not Realized");
		}

		virtual void Add(System::Boolean value)
		{
			bool val = value;
			CheckSize(1);
			*(char *)GetOff() = val;
			UsedSize += 1;
		}

		virtual void Add(System::Boolean value1, System::Boolean value2)
		{
			bool val1 = value1;
			bool val2 = value2;
			char res = (char)0 | (val1 ? 1 : 0) | (val2 ? 2 : 0);
			CheckSize(1);
			*(char *)GetOff() = res;
			UsedSize += 1;
		}

		virtual void Add(System::Boolean value1, System::Boolean value2, System::Boolean value3)
		{
			bool val1 = value1;
			bool val2 = value2;
			bool val3 = value3;
			char res = (char)0 | (val1 ? 1 : 0) | (val2 ? 2 : 0) | (val3 ? 4 : 0);
			CheckSize(1);
			*(char *)GetOff() = res;
			UsedSize += 1;
		}

		virtual void Add(System::String ^str)
		{
			int len;
			if(str==nullptr)
			{
				len=-1;
				Add(len);
			}
			else
			{
				Encoding^ u8 = Encoding::UTF8;
				array<Byte> ^arr = u8->GetBytes(str);
				len = u8->GetByteCount(str);
				Add(len);
				CheckSize(len);
				Byte * dst = (Byte*)GetOff();
				for(int i=0; i<len; i++)
				{
					*(dst+i)=arr[i];
				}
				UsedSize += len;
			}
		}

        virtual void Add(array<Byte> ^value)
		{
			Add(value->Length);
			for(int i=0; i<value->Length; i++)
				Add(value[i]);
		}

		virtual int Size()
		{
			return UsedSize;
		}

	protected:
		void *GetOff()
		{
			return (char *)buffer + UsedSize;
		}

		void CheckSize(int addSize)
		{
			if(UsedSize + addSize > size)
			{
				if(addSize < 1024)
				{
					void *buffer1 = realloc(buffer, size + 1024);
					
					if(buffer1 == NULL)
						throw gcnew System::Exception("�� ������� �������� ������. stbufferwriter. realloc � CheckSize. size = " + System::Convert::ToString(size) + ", usedSize = " + System::Convert::ToString(UsedSize) + ", addSize = " + System::Convert::ToString(addSize));
					
					buffer = buffer1;

					size += 1024;
				}
				else
				{
//					int count = div(addSize, 1024).quot + 1;
					
					void *buffer1 = realloc(buffer, size + addSize + 1024 );
					if(buffer1 == NULL)
						throw gcnew System::Exception("�� ������� �������� ������ 1. stbufferwriter. realloc � CheckSize. size = " + System::Convert::ToString(size) + ", usedSize = " + System::Convert::ToString(UsedSize) + ", addSize = " + System::Convert::ToString(addSize) + ", count = ");
					
					buffer = buffer1;

					size += addSize + 1024;
				}
			}
		}

	private:
		void *buffer;
		int size;
		int UsedSize;
	};

}}