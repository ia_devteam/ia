#pragma once

using namespace System;
using namespace System::Collections::Generic;


#include "..\..\..\Externals\Berkeley\db_cxx.h"
#include "stBufferRead.h"
#include "stBufferWrite.h"
#include "Pool.h"
#include <malloc.h>
#include <errno.h>


namespace Store {
namespace Table {

	//���� ������
	public enum class enTableType
	{
		MAIN_TABLE = 3,
		INDEX = 1
	};

	public enum class enResult
	{
		OK, 
		NOT_FOUND
	};


	//�������
	ref class Table 
	{
	public:
		Table(DbEnv *environment, String ^ContainerName, enTableType tableType, bool dublicateIndex)
		{
			lockObj = gcnew System::Object();
			System::Threading::Monitor::Enter(lockObj);

			db = NULL;

			char *nm = NULL;
			try{
				db = new Db(environment, 0);

				nm = (char*) System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(ContainerName ).ToPointer();

				if(dublicateIndex)
					db->set_flags(DB_DUP);

				db->open(NULL, nm, NULL, (DBTYPE)tableType, 0, 0);	
			} 
			catch(DbException &e) {

				if(e.get_errno() == ENOENT)	//�� �� ���������� 
				{
					try{
						db->close(0);
					//	delete db;
						db = new Db(environment, 0);

						if(dublicateIndex)
							db->set_flags(DB_DUP);
				
						db->open(NULL, nm, NULL, (DBTYPE)tableType, DB_CREATE , 0);	

					} catch(DbException &e1) {
						db->close(0);
						//delete db;
						db = NULL;

						System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
						build->Append("Error creating database ObjectWithIDManager_table: ");
						build->Append(ContainerName);
						build->Append(". Error DbException: ");
						build->Append(gcnew String(e1.what()));
						build->Append(".");
						IA::Monitor::Log::Error("���������", build->ToString(), false);
						throw gcnew Store::Const::StorageException(build->ToString());
					}
					catch (std::exception &e2) {
						db->close(0);
						//delete db;
						db = NULL;

						System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
						build->Append("Error creating database ObjectWithIDManager_table: ");
						build->Append(ContainerName);
						build->Append(". Error STD: ");
						build->Append(gcnew String(e2.what()));
						build->Append(".");
						IA::Monitor::Log::Error("���������", build->ToString(), false);
						throw gcnew Store::Const::StorageException(build->ToString());
					}
				}
				else
				{
						db = NULL;

						System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
						build->Append("Error opening database ObjectWithIDManager_table 1. Error DbException: ");
						build->Append(gcnew String(e.what()));
						build->Append(". Arguments \"");
						build->Append(ContainerName);
						build->Append("\", \"");
						build->Append(tableType);
						build->Append("\".");
						IA::Monitor::Log::Error("���������", build->ToString(), false);
						throw gcnew Store::Const::StorageException(build->ToString());
				}
			} catch(std::exception &e) {

						db = NULL;

						System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
						build->Append("Error opening database ObjectWithIDManager_table 2. Error STD: ");
						build->Append(gcnew String(e.what()));
						build->Append(". Arguments \"");
						build->Append(gcnew String(nm));
						build->Append("\", \"");
						build->Append(tableType);
						build->Append("\".");
						IA::Monitor::Log::Error("���������", build->ToString(), false);
						throw gcnew Store::Const::StorageException(build->ToString());
			}
			System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(nm));

			System::Threading::Monitor::Exit(lockObj);

			cursorContains = nullptr;
		}

		//������ �� �������
		enResult Get(Dbt &key, [System::Runtime::InteropServices::Out] IBufferReader ^%buffer)
		{
			System::Threading::Monitor::Enter(lockObj);

			enResult res = enResult::NOT_FOUND;
			
			Dbt data;
			data.set_flags(DB_DBT_MALLOC | DB_DBT_APPMALLOC);
			
			buffer = nullptr;

			try{
				if(db->get(0, &key, &data, 0) != DB_NOTFOUND && data.get_size() != 0)
				{
					buffer = ReaderPool::Get();
					((stBufferRead ^)buffer)->Set(data.get_data(), data.get_size());
					res = enResult::OK;
				}
				else
					res = enResult::NOT_FOUND;
			} catch(DbException &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error DbException: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");

				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			} catch(std::exception &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error STD: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			finally
			{
				System::Threading::Monitor::Exit(lockObj);
			}

			return res;
		}

		//�������� ������ � ����
		void Put(Dbt &key, IBufferWriter ^buffer)
		{
			System::Threading::Monitor::Enter(lockObj);
		
			stBufferWrite ^Writer = (stBufferWrite^)buffer;

			Dbt b_data(Writer->GetBuffer(), Writer->GetUsedBufferSize());

			try{
				int res = db->put(NULL, &key, &b_data, 0);

				if(res != 0)
					if(res == DB_LOCK_DEADLOCK)
						IA::Monitor::Log::Error("���������", "DB_LOCK_DEADLOCK", false);
					else if(res == EACCES)
						IA::Monitor::Log::Error("���������", "EACCES", false);
					else if(res == EINVAL)
						IA::Monitor::Log::Error("���������", "EINVAL", false);
					else if(res == ENOSPC)
						IA::Monitor::Log::Error("���������", "ENOSPC", false);
				
			} catch(DbException &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error stAcceptChanger::AcceptChanges. Error DbException: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());

			} catch(std::exception &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error stAcceptChanger::AcceptChanges. Error STD: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			finally
			{
				System::Threading::Monitor::Exit(lockObj);
			}
		}

		//�������� ����� ������ � ����. ����� ����������� ������ � ����� �� ���������������.
		unsigned int Add(IBufferWriter ^buffer)
		{
			System::Threading::Monitor::Enter(lockObj);
			
			unsigned int res = 0;
			
			stBufferWrite ^Writer = (stBufferWrite^)buffer;

			Dbt b_data(Writer->GetBuffer(), Writer->GetUsedBufferSize());

			try{
					Dbt kdbt;
					kdbt.set_flags(DB_DBT_MALLOC | DB_DBT_APPMALLOC);
					db_recno_t key = 0;
					db->put(NULL, &kdbt, &b_data, DB_APPEND);
					key = *(db_recno_t *) kdbt.get_data();
					res = (unsigned int) key;
			} catch(DbException &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error stAcceptChanger::AcceptChanges. Error DbException: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			catch (std::exception &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error stAcceptChanger::AcceptChanges. Error STD: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			finally
			{
				System::Threading::Monitor::Exit(lockObj);
			}

			if(res >= UInt32::MaxValue)
			{
				IA::Monitor::Log::Error("���������", "������� �������� �������������, ����������� UInt32.MaxValue. ����� Add", false);
				throw gcnew Store::Const::StorageException("������� �������� �������������, ����������� UInt32.MaxValue. ����� Add");
			}

			return res;
		}


		//�������� � ���� �� ���������� �����
		void Remove(Dbt &key)
		{
			System::Threading::Monitor::Enter(lockObj);

			try{
					db->del(0, &key, 0);
			} catch(DbException &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error DbException: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			catch (std::exception &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error STD: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			finally
			{
				System::Threading::Monitor::Exit(lockObj);
			}
		}

		//�������� � ���� � �����������
		void Remove(Dbt &key, IBufferWriter ^valueBuffer)
		{
			System::Threading::Monitor::Enter(lockObj);
			
			stBufferWrite ^valueWriter = (stBufferWrite^)valueBuffer;
			Dbt data(valueWriter->GetBuffer(), valueWriter->GetUsedBufferSize());

			try{
				Dbc **cursor = new Dbc*;
				
				db->cursor(NULL, cursor, DB_WRITECURSOR);
				int ret = (*cursor)->get(&key, &data, DB_GET_BOTH);
				if(!ret)
				{
					(*cursor)->del(0);
					(*cursor)->close();
				}
				else
				{
						System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
						build->Append("������� ������� �� ������");
						IA::Monitor::Log::Error("���������", build->ToString(), false);
						throw gcnew Store::Const::StorageException(build->ToString());
				}
				delete cursor;
			} catch(DbException &e) {
						System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
						build->Append("Error getting cursor database ObjectWithIDManager_table: ");
						build->Append(". Error DbException: ");
						build->Append(gcnew String(e.what()));
						build->Append(".");
						IA::Monitor::Log::Error("���������", build->ToString(), false);
						throw gcnew Store::Const::StorageException(build->ToString());
			}
			catch (std::exception &e) {
						System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
						build->Append("Error getting cursor database ObjectWithIDManager_table: ");
						build->Append(". Error STD: ");
						build->Append(gcnew String(e.what()));
						build->Append(".");
						IA::Monitor::Log::Error("���������", build->ToString(), false);
						throw gcnew Store::Const::StorageException(build->ToString());
			}
			finally
			{
				System::Threading::Monitor::Exit(lockObj);
			}
		}

		//������, ������������ � Contains. ������������ ��� ����, ����� �� ��������� ������ �������� ��������. ������ ����� ������ ���� ��� � ��������.
		Dbc **cursorContains;
		//���������� true, ���� ����� ������ (����-��������) ������������ � ��.
		//false - ����� ������ � ���� ���.
		bool Contains(Dbt &key, IBufferWriter ^valueBuffer)
		{
			System::Threading::Monitor::Enter(lockObj);
			

			stBufferWrite ^valueWriter = (stBufferWrite^)valueBuffer;
			Dbt data(valueWriter->GetBuffer(), valueWriter->GetUsedBufferSize());

			try{
				if(cursorContains == nullptr)
				{
					cursorContains = new Dbc*;
					int res = db->cursor(NULL, cursorContains, 0);
					res=res;
				}

				int ret = (*cursorContains)->get(&key, &data, DB_GET_BOTH);

				return !ret;
			} catch(DbException &e) {
						System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
						build->Append("Error getting cursor database ObjectWithIDManager_table: ");
						build->Append(". Error DbException: ");
						build->Append(gcnew String(e.what()));
						build->Append(".");
						IA::Monitor::Log::Error("���������", build->ToString(), false);
						throw gcnew Store::Const::StorageException(build->ToString());
			}
			catch (std::exception &e) {
						System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
						build->Append("Error getting cursor database ObjectWithIDManager_table: ");
						build->Append(". Error STD: ");
						build->Append(gcnew String(e.what()));
						build->Append(".");
						IA::Monitor::Log::Error("���������", build->ToString(), false);
						throw gcnew Store::Const::StorageException(build->ToString());
			}
			finally
			{
				System::Threading::Monitor::Exit(lockObj);
			}
		}

		bool IsOpen()
		{
			return db != NULL;
		}

		void Close()
		{
			System::Threading::Monitor::Enter(lockObj);
			
			if(!IsOpen())
				return;

			try{
				if(cursorContains != nullptr)
				{
					(*cursorContains)->close();
					delete cursorContains;
					cursorContains = nullptr;
				}

				db->close (0);
				db = NULL;
			}
			catch(DbException &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error ObjectWithIDManager_table::Close(). Error DbException: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			catch (std::exception &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error ObjectWithIDManager_table::Close(). Error STD: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			finally
			{
				System::Threading::Monitor::Exit(lockObj);
			}
		}

		unsigned int Count()
		{
			System::Threading::Monitor::Enter(lockObj);
			DB_BTREE_STAT *st;
			unsigned int res;

			try {
				db->stat(0, &st, 0);
				res = st->bt_ndata;
				free(st);
			}
			catch (DbException &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error DbException: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			catch (std::exception &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error ObjectWithIDManager_table::Get_floating(int). Error STD: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException(build->ToString());
			}
			finally
			{
				System::Threading::Monitor::Exit(lockObj);
			}

			return res;
		}

		Db *GetDb()
		{
			return db;
		}
protected:
		Db *db;

internal:
		System::Object ^lockObj;

	};
}}