// Storage_Berkley.h

#pragma once

using namespace System;

#include "..\..\..\Externals\Berkeley\db_cxx.h"
#include "Table.h"
#include "Tables.h"

#define ENVIRONMENT_FLAGS	 DB_INIT_CDB | DB_INIT_MPOOL /*| DB_PRIVATE /*| DB_INIT_LOCK | DB_THREAD*/
#define CACHE_SIZE_BYTES	256 * 1024 * 1024
#define CACHE_SIZE_GIGABYTES 0

namespace Store {
namespace Table {

	//�������� �����, � �������� ���������� ������������� ����������
	public ref class DB_Operations sealed 
	{
	public:
		DB_Operations()
		{
			environment = NULL;
		}

		~DB_Operations()
		{
			Close();
		}

		//��������� ��, ������������� � ����� name
		void Open(System::String ^name)
		{
			Close();
			DB_Directory = name;

			char *nm = (char*) System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(name).ToPointer();

			try{
				environment = new DbEnv(0);
				environment->set_alloc(&malloc, &realloc, &free);
				environment->set_cachesize(CACHE_SIZE_GIGABYTES, CACHE_SIZE_BYTES, 1);

				environment->open(nm, ENVIRONMENT_FLAGS, 0);

			} catch(DbException &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error opening database environment: ");
				build->Append(gcnew String(name));
				build->Append(". Error DbException: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return;
			} catch(std::exception &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error opening database environment: ");
				build->Append(gcnew String(name));
				build->Append(". Error STD: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return;
			}

			 System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(nm));
			 return;
		}

		//������� �� � ���������� name. ��������� ���������� ���������� �������� name!!!
		void Create(System::String ^name)
		{
			Close();

			DB_Directory = name;
/*
			try{         
				System::IO::Directory::Delete(name, true);
			}
			catch(System::IO::DirectoryNotFoundException ^ex) {}
*/
			System::IO::Directory::CreateDirectory(name);

			char *nm = (char*)  System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(name ).ToPointer();

			try{
				environment = new DbEnv(0);
				environment->set_alloc(&malloc, &realloc, &free);
				environment->set_cachesize(CACHE_SIZE_GIGABYTES, CACHE_SIZE_BYTES, 1);

				environment->open(nm, ENVIRONMENT_FLAGS | DB_CREATE, 0);
			} catch(DbException &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error creating database environment: ");
				build->Append(gcnew String(name));
				build->Append(". Error DbException: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return;
			} catch(std::exception &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error creating database environment: ");
				build->Append(gcnew String(name));
				build->Append(". Error STD: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return;
			}

			 System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(nm));
			 return;
		}

		//��������� ��
		bool Close()
		{
			try{
				if(environment)
				{
					environment->close(0);
					//delete environment;
					environment = NULL;
				}
			} catch(DbException &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error closing database environment. Error DbException: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return false;
			} catch(std::exception &e) {
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("Error closing database environment. Error STD: ");
				build->Append(gcnew String(e.what()));
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return false;
			}
			return true;
		}

		//������� �������, ������������� �� �������������� � �� ���������� ����������
		//���� ������� �� ����������, ��� ����� �������.
		TableIDNoDuplicate ^GetTableIDNoDuplicate(System::String ^name)
		{
			if(environment != nullptr)
				return gcnew TableIDNoDuplicate(environment, name);
			else
			{
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("������� �������� ��� �������� ���� ��� �������� ���������. ������� �������� DB_Operations.Open() ��� DB_Operations.Create(). ��� ����������� ���� ");
				build->Append(name);
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return nullptr;
			}				
		}

		//������� �������, ������������� �� �������������� � ���������� ���������
		//���� ������� �� ����������, ��� ����� �������.
		TableIDDuplicate ^GetTableIDDuplicate(System::String ^name)
		{
			if(environment != nullptr)
				return gcnew TableIDDuplicate(environment, name);
			else
			{
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("������� �������� ��� �������� ���� ��� �������� ���������. ������� �������� DB_Operations.Open() ��� DB_Operations.Create(). ��� ����������� ���� ");
				build->Append(name);
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return nullptr;
			}				
		}

		//������� �������, ������������� �� ������������� ������� � �� ���������� ���������
		//���� ������� �� ����������, ��� ����� �������.
		TableIndexedNoDuplicates ^GetTableIndexedNoDuplicates(System::String ^name)
		{
			if(environment != nullptr)
				return gcnew TableIndexedNoDuplicates(environment, name);
			else
			{
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("������� �������� ��� �������� ���� ��� �������� ���������. ������� �������� DB_Operations.Open() ��� DB_Operations.Create(). ��� ����������� ���� ");
				build->Append(name);
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return nullptr;
			}				
		}

		//������� �������, ������������� �� ������������� ������� � ���������� ���������
		//���� ������� �� ����������, ��� ����� �������.
		TableIndexedDuplicates ^GetTableIndexedDuplicates(System::String ^name)
		{
			if(environment != nullptr)
				return gcnew TableIndexedDuplicates(environment, name);
			else
			{
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("������� �������� ��� �������� ���� ��� �������� ���������. ������� �������� DB_Operations.Open() ��� DB_Operations.Create(). ��� ����������� ���� ");
				build->Append(name);
				build->Append(".");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				return nullptr;
			}				
		}

		//������� �������
		//���� ������� �� ����������, ��� ����� �������.
		void DeleteDB(System::String ^name)
		{
			char *nm;

			nm = (char*) System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(name ).ToPointer();
			environment->dbremove(NULL, nm, NULL, 0);
			System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(nm));
		}

		//���������, ���������� �� �������
		//���� ������� �� ����������, ��� ����� �������.
		bool IsDBExists(System::String ^name)
		{
			String ^str = DB_Directory + "\\"+ name;
			return System::IO::File::Exists(str);
		}


		System::String ^GetDBDirectory()
		{
			return DB_Directory;
		}
	private:
		//DerkleyDB
		DbEnv *environment;
		//�����, � ������� ��������� ��
		System::String ^DB_Directory;
				
	};
}}
