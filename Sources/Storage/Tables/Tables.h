#pragma once


using namespace System;
using namespace System::Collections::Generic;
using namespace Store::Const;
#using "..\..\..\Binaries\Common\Monitor.dll"


#include "..\..\..\Externals\Berkeley\db_cxx.h"
#include "stBufferRead.h"
#include "stBufferWrite.h"
#include "Pool.h"
#include "Table.h"
#include "Enumerator.h"
#include <malloc.h>

namespace Store {
namespace Table {

	//�������, ������������� �� �������������� � �� ���������� ��������� �����
	public ref class TableIDNoDuplicate sealed : public IEnumerable<KeyValuePair<UInt64, IBufferReader ^>>
	{
		Table ^table;
	internal:
		//Berkley �� ����� �������� � 64-������� �������. ��������, ��� ���� �� �������� 32-������ ��������. ����� ���� �������� 32-������ 
		//��������, �������� ������� � ����� ���������� ���� ������ Berkley.
		static void CheckKey(UInt64 ID)
		{
			if(ID >= UInt32::MaxValue)
			{
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("������������� �������� UInt32.MaxValue-1: ");
				build->Append(ID);
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException("������� �������� � ������, �� �������� ����������, WrongID. ����� ��������� ���������");
			}
			if(ID == 0)
			{
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("������������� �� ����� ����� �������� 0");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException("������� �������� � ��������� �������������, ������ 0");
			}
		}

		TableIDNoDuplicate (DbEnv *environment, String ^ContainerName)
		{
			table = gcnew Table(environment, ContainerName, enTableType::MAIN_TABLE, false);
			if(table->GetDb()==nullptr)
				table = nullptr;
		}

	public:
		//����� ������ �� ������� �� �����
		enResult Get(UInt64 ID, [System::Runtime::InteropServices::Out] IBufferReader ^%buffer)
		{
			CheckKey(ID);
			unsigned int id = (unsigned int) ID;
			Dbt key(&id, sizeof(id));

			return table->Get(key, buffer);
		}

    	public: bool IsTableNull()
		{
			return table==nullptr;
		}

		//������ ������� ������ � ������� �� �����
		void Put(System::UInt64 ID, IBufferWriter ^buffer)
		{
			CheckKey(ID);

			unsigned int id = (unsigned int) ID;
			Dbt key(&id, sizeof(id));

			table->Put(key, buffer);
		}
		//�������� ������ � �������. ��� ���� ����� ����������� ����� ���������� ����, ������� � ������������ ��������.
		UInt64 Add(IBufferWriter ^buffer)
		{
			return (UInt64) table->Add(buffer);
		}
		//������� ������ �� ������� �� �����
		void Remove(System::UInt64 ID)
		{
			CheckKey(ID);
			unsigned int id = (unsigned int) ID;
			Dbt key(&id, sizeof(id));

			table->Remove(key);
		}

		//���������� true, ���� ����� ������ (����-��������) ������������ � ��.
		//false - ����� ������ � ���� ���.
		bool Contains(System::UInt64 ID, IBufferWriter ^data)
		{
			CheckKey(ID);
			unsigned int id = (unsigned int) ID;
			Dbt key(&id, sizeof(id));

			return table->Contains(key, data);
		}

		//������� �� ��������� ������ � ��������
		void Close()
		{
			table->Close();
		}

		UInt64 Count()
		{
			return table->Count();
		}

		virtual IEnumerator<KeyValuePair<UInt64, IBufferReader ^>> ^GetEnumerator() = System::Collections::Generic::IEnumerable<KeyValuePair<UInt64, IBufferReader ^>>::GetEnumerator
		{
			return gcnew IEnumeratorUInt64ValuePair_NoDub(table);
		}

	internal:

		virtual System::Collections::IEnumerator ^GetEnumerator1() = System::Collections::IEnumerable::GetEnumerator
		{
			return gcnew IEnumeratorUInt64ValuePair_NoDub(table);
		}
	};

	//�������, ������������� �� �������������� � ���������� ��������� �����
	public ref class TableIDDuplicate sealed : public IEnumerable<KeyValuePair<UInt64, IBufferReader ^>>
	{
		Table ^table;
	internal:
		//Berkley �� ����� �������� � 64-������� �������. ��������, ��� ���� �� �������� 32-������ ��������. ����� ���� �������� 32-������ 
		//��������, �������� ������� � ����� ���������� ���� ������ Berkley.
		static void CheckID(UInt64 ID)
		{
			if(ID > UInt32::MaxValue)
			{
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("������������� �������� UInt32.MaxValue: ");
				build->Append(ID);
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException("������� �������� � ������ �������������, ����������� UInt32.MaxValue");
			}
			if (ID == 0)
			{
				System::Text::StringBuilder ^build = gcnew System::Text::StringBuilder();
				build->Append("������������� �� ����� ����� �������� 0");
				IA::Monitor::Log::Error("���������", build->ToString(), false);
				throw gcnew Store::Const::StorageException("������� �������� � ��������� �������������, ������ 0");
			}
		}

		TableIDDuplicate (DbEnv *environment, String ^ContainerName)
		{
			table = gcnew Table(environment, ContainerName, enTableType::INDEX, true);
			if(table->GetDb()==nullptr)
				table = nullptr;

		}
	
	public:
		//�������� �������� ��������, ����������� �� ���������� ��������������
		IEnumerable<IBufferReader ^> ^Get(UInt64 ID)
		{
			CheckID(ID);
			IBufferWriter ^writer;
			IEnumerable<IBufferReader ^> ^res;
			try
			{
				writer = WriterPool::Get();
				writer->Add((UInt32) ID);

				res = gcnew IEnumeratorValueByKey(table, writer, CURSORMATCH);
			}
			finally
			{
				delete writer;
			}
			return res;
		}
    	public: bool IsTableNull()
		{
			return table==nullptr;
		}

		//�������� � ������� ������ �� ���������� �����
		void Put(UInt64 ID, IBufferWriter ^buffer)
		{
			CheckID(ID);
			unsigned int id = (unsigned int) ID;
			Dbt key(&id, sizeof(id));

			table->Put(key, buffer);
		}

		//������� ������ �� �������, ������ �������� ����� ����-��������
		void Remove(UInt64 ID, IBufferWriter ^valueBuffer)
		{
			CheckID(ID);
			unsigned int id = (unsigned int) ID;
			Dbt key(&id, sizeof(id));

			table->Remove(key, valueBuffer);
		}

		//������� ������ �� �������, �������� ������ (��� �������� �� ���������� �����)
		void Remove(UInt64 id)
		{
			CheckID(id);
			unsigned int ID = (unsigned int) id;
			Dbt key(&ID, sizeof(ID));

			table->Remove(key);
		}

		//������� ��� ������ �� ������� � �������� ������
		void RemoveAll(UInt64 id)
		{
			CheckID(id);
			unsigned int ID = (unsigned int) id;
			Dbt key(&ID, sizeof(ID));

			table->Remove(key);
		}

		//���������� true, ���� ����� ������ (����-��������) ������������ � ��.
		//false - ����� ������ � ���� ���.
		bool Contains(System::UInt64 id, IBufferWriter ^data)
		{
			CheckID(id);
			unsigned int ID = (unsigned int) id;
			Dbt key(&ID, sizeof(ID));

			return table->Contains(key, data);
	}
		//������� �������
		void Close()
		{
			table->Close();
		}
		virtual IEnumerator<KeyValuePair<UInt64, IBufferReader ^>> ^GetEnumerator() = System::Collections::Generic::IEnumerable<KeyValuePair<UInt64, IBufferReader ^>>::GetEnumerator
		{
			return gcnew IEnumeratorUInt64ValuePair_Dub(table);
		}

	internal:
		virtual System::Collections::IEnumerator ^GetEnumerator1() = System::Collections::IEnumerable::GetEnumerator
		{
			return gcnew IEnumeratorUInt64ValuePair_Dub(table);
		}

	};

	ref class Enumerable_TableIndexedDuplicates : IEnumerable<KeyValuePair<IBufferReader ^, IBufferReader ^>>
	{
		Table ^table;
	internal:
		Enumerable_TableIndexedDuplicates(Table ^table)
		{
			this->table = table;
		}
	public:
		virtual IEnumerator<KeyValuePair<IBufferReader ^, IBufferReader ^>> ^GetEnumerator() = System::Collections::Generic::IEnumerable<KeyValuePair<IBufferReader ^, IBufferReader ^>>::GetEnumerator
		{
			return gcnew IEnumeratorKeyValuePair_NoDub(table);
		}

	internal:
		virtual System::Collections::IEnumerator ^GetEnumerator1() = System::Collections::IEnumerable::GetEnumerator
		{
			return gcnew IEnumeratorKeyValuePair_NoDub(table);
		}

	};


	//�������, ������������� �� ������������� ����� � �� ���������� ��������� �����
	public ref class TableIndexedNoDuplicates sealed : public IEnumerable<KeyValuePair<IBufferReader ^, IBufferReader ^>>
	{
		Table ^table;

	internal:
		TableIndexedNoDuplicates (DbEnv *environment, String ^ContainerName) 
		{
			table = gcnew Table(environment, ContainerName, enTableType::INDEX, false);
			if(table->GetDb()==nullptr)
				table = nullptr;

		}
	
	public:
		//������ �� �������
		enResult Get(IBufferWriter ^index, [System::Runtime::InteropServices::Out] IBufferReader ^%buffer)
		{
			stBufferWrite ^indexBuf = (stBufferWrite^)index;
			Dbt key(indexBuf->GetBuffer(), indexBuf->GetUsedBufferSize());
			
			return table->Get(key, buffer);
		}

    	public: bool IsTableNull()
		{
			return table==nullptr;
		}

		enResult GetStartFrom(IBufferWriter ^index, [System::Runtime::InteropServices::Out] IBufferReader ^%buffer)
		{
			stBufferWrite ^indexBuf = (stBufferWrite^)index;
			IEnumeratorValueByKey ^en;
			try{
				en = gcnew IEnumeratorValueByKey(table, index, CURSORSTARTFROM);
				if(en->MoveNext())
				{
					buffer = en->get();
					return enResult::OK;
				}
				else
				{
					buffer = nullptr;
					return enResult::NOT_FOUND;
				}
			}
			finally
			{
				delete en;
			}
		}


		//������� �������� �������, ������������ �� ���������� �����
		void Put(IBufferWriter ^keyBuffer, IBufferWriter ^buffer)
		{
			stBufferWrite ^keyWriter = (stBufferWrite^)keyBuffer;

			Dbt kdbt(keyWriter->GetBuffer(), keyWriter->GetUsedBufferSize());
			table->Put(kdbt, buffer);
		}

		//�������� �������� �������
		void Remove(IBufferWriter ^keyBuffer)
		{
			stBufferWrite ^keyWriter = (stBufferWrite^)keyBuffer;

			Dbt kdbt(keyWriter->GetBuffer(), keyWriter->GetUsedBufferSize());
			table->Remove(kdbt);
		}

		//���������� true, ���� ����� ������ (����-��������) ������������ � ��.
		//false - ����� ������ � ���� ���.
		bool Contains(IBufferWriter ^keyBuffer, IBufferWriter ^data)
		{
			stBufferWrite ^keyWriter = (stBufferWrite^)keyBuffer;

			Dbt kdbt(keyWriter->GetBuffer(), keyWriter->GetUsedBufferSize());
			return table->Contains(kdbt, data);
		}
		//������� �� ��������� ������ � ��������
		void Close()
		{
			table->Close();
		}

		virtual IEnumerator<KeyValuePair<IBufferReader ^, IBufferReader ^>> ^GetEnumerator() = System::Collections::Generic::IEnumerable<KeyValuePair<IBufferReader ^, IBufferReader ^>>::GetEnumerator
		{
			return gcnew IEnumeratorKeyValuePair_NoDub(table);
		}

	internal:
		virtual System::Collections::IEnumerator ^GetEnumerator1() = System::Collections::IEnumerable::GetEnumerator
		{
			return gcnew IEnumeratorKeyValuePair_NoDub(table);
		}
	};

	//�������, ������������� �� ������������� ����� � ���������� ��������� �����
	public ref class TableIndexedDuplicates sealed : IEnumerable<KeyValuePair<IBufferReader ^, IBufferReader ^>>
	{
		Table ^table;

	internal:
		TableIndexedDuplicates (DbEnv *environment, String ^ContainerName) 
		{
			table = gcnew Table(environment, ContainerName, enTableType::INDEX, true);
			if(table->GetDb()==nullptr)
				table = nullptr;

		}

	
	public:
		//���������� ���������� �� ���� ���������, ����������� �� ������� �����
		IEnumerable<IBufferReader ^> ^Get(IBufferWriter ^keyBuffer)
		{
			return gcnew IEnumeratorValueByKey(table, keyBuffer, CURSORMATCH);
		}

    	public: bool IsTableNull()
		{
			return table==nullptr;
		}

		//���������� ���������� �� ��������� �������, ���� ������� ������ ��� ����� ������� ������� � ����� ��������� ������
		IEnumerable<IBufferReader ^> ^GetStartFrom(IBufferWriter ^keyBuffer)
		{
			return gcnew IEnumeratorValueByKey(table, keyBuffer, CURSORSTARTFROM);
		}

		//�������� �������� �� ���������� �����
		void Put(IBufferWriter ^keyBuffer, IBufferWriter ^buffer)
		{
			stBufferWrite ^keyWriter = (stBufferWrite^)keyBuffer;

			Dbt kdbt(keyWriter->GetBuffer(), keyWriter->GetUsedBufferSize());
			table->Put(kdbt, buffer);
		}
		//������� �������� �������� �� ��������� �����
		void Remove(IBufferWriter ^keyBuffer, IBufferWriter ^valueBuffer)
		{
			stBufferWrite ^keyWriter = (stBufferWrite^)keyBuffer;
			Dbt key(keyWriter->GetBuffer(), keyWriter->GetUsedBufferSize());

			table->Remove(key, valueBuffer);
		}

		//���������� true, ���� ����� ������ (����-��������) ������������ � ��.
		//false - ����� ������ � ���� ���.
		bool Contains(IBufferWriter ^keyBuffer, IBufferWriter ^data)
		{
			stBufferWrite ^keyWriter = (stBufferWrite^)keyBuffer;

			Dbt kdbt(keyWriter->GetBuffer(), keyWriter->GetUsedBufferSize());
			return table->Contains(kdbt, data);
		}
		//������� �� ��������� ������ � ��������
		void Close()
		{
			table->Close();
		}

		IEnumerable<KeyValuePair<IBufferReader ^, IBufferReader ^>> ^EnumerateWithoutDuplicates()
		{
			return gcnew Enumerable_TableIndexedDuplicates(table);
		}

		virtual IEnumerator<KeyValuePair<IBufferReader ^, IBufferReader ^>> ^GetEnumerator() = System::Collections::Generic::IEnumerable<KeyValuePair<IBufferReader ^, IBufferReader ^>>::GetEnumerator
		{
			return gcnew IEnumeratorKeyValuePair_Dub(table);
		}

	internal:
		virtual System::Collections::IEnumerator ^GetEnumerator1() = System::Collections::IEnumerable::GetEnumerator
		{
			return gcnew IEnumeratorKeyValuePair_Dub(table);
		}
};


}}
