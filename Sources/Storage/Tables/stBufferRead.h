#pragma once

#using "..\..\..\Binaries\Common\Storage_Constants.dll"

using namespace System;
using namespace Store::Const;
#include "..\..\..\Externals\Berkeley\db_cxx.h"
#include "stBufferWrite.h"

namespace Store {
namespace Table {

	public interface class IBufferReader : System::IDisposable
    {
	public:
        virtual Byte GetByte();
        virtual Int32 GetInt32();
        virtual UInt32 GetUInt32();
        virtual Int64 GetInt64();
        virtual UInt64 GetUInt64();
		//���������� Uint64 � �����, ���������� ����� � ���������� ������������ ������� (���������� ����� �������� � ������������ �����)
		virtual UInt64 GetUInt64Transformed();
        virtual Int128 GetInt128();
        virtual Double GetDouble();
        virtual MegaDouble GetMegaDouble();
        virtual void GetBool(bool %bool1);
        virtual void GetBool(bool %bool2, bool %bool4);
        virtual void GetBool(bool %bool3, bool %bool5, bool %bool6);
        virtual String ^GetString();
        virtual array<Byte> ^GetAllBytes();
        //virtual array<Byte> ^GetObjectBytes();
		virtual array<Byte> ^GetByteArray();
	};



	public ref class stBufferRead : public IBufferReader
	{
	public:
		stBufferRead()
		{
			buffer = NULL;
			size = 0;
			cursor = 0;
		}

		~stBufferRead()
		{
			if(buffer != NULL)
			{
				free(buffer);
				buffer = NULL;
			}
			System::GC::SuppressFinalize(this);
		}

		!stBufferRead()
		{
			if(buffer != NULL)
			{
				free(buffer);
				buffer = NULL;
			}
			System::GC::SuppressFinalize(this);
		}
		
		//������������ ��� ������������
		stBufferRead(stBufferWrite ^writer)
		{
			buffer = malloc(writer->GetUsedBufferSize());
			memcpy(buffer, writer->GetBuffer(), writer->GetUsedBufferSize());
			size = writer->GetUsedBufferSize();
			cursor = 0;
		}

		void Set(void *buf, int sz)
		{
			this->buffer = buf;
			this->size = sz;
			cursor = 0;
		}

		virtual System::Byte GetByte() 
		{
			CheckSize(1);
			char *i = (char *) GetOff();
			cursor += 1;
			return *i;
		}

		virtual System::Int32 GetInt32() 
		{
			CheckSize(4);
			int *i = (int *) GetOff();
			cursor += 4;
			return *i;
		}

		virtual System::UInt32 GetUInt32() 
		{
			CheckSize(4);
			unsigned int *i = (unsigned int *) GetOff();
			cursor += 4;
			return *i;
		}

		virtual System::Int64 GetInt64() 
		{
			CheckSize(8);
			__int64 *i = (__int64 *) GetOff();
			cursor += 8;
			return *i;
		}

		virtual System::UInt64 GetUInt64() 
		{
			CheckSize(8);
			__int64 *i = (__int64 *) GetOff();
			cursor += 8;
			return *i;
		}

		virtual System::UInt64 GetUInt64Transformed() 
		{
			CheckSize(8);
			__int64 *val = (__int64 *) GetOff();
			cursor += 8;

			__int64 res;
			char *ref_val = (char *)val;
			char *ref_res = (char *)&res;
			for(int i=0;i<8;i++)
				ref_res[i] = ref_val[7-i];

			return res;
		}

		virtual Int128 GetInt128()
		{
			throw gcnew Exception("Not Implemented");
		}

		virtual System::Double GetDouble() 
		{
			CheckSize(8);
			double *i = (double *) GetOff();
			cursor += 8;
			return *i;
		}

		virtual MegaDouble GetMegaDouble() 
		{
			throw gcnew Exception("Not Implemented");
		}

		virtual void GetBool([System::Runtime::InteropServices::Out] bool %bool1) 
		{
			CheckSize(1);
			char ch = *(char *) GetOff();
			cursor += 1;
			bool1 = ch & 1;
		}

		virtual void GetBool([System::Runtime::InteropServices::Out] bool %bool1, [System::Runtime::InteropServices::Out] bool %b2) 
		{	
			CheckSize(1);
			char ch = *(char *) GetOff();
			cursor += 1;
			bool1 = ((ch & 1) == 1);
			b2 = ((ch & 2) == 2);
		}

		virtual void GetBool([System::Runtime::InteropServices::Out] bool %bool1, [System::Runtime::InteropServices::Out] bool %b2, [System::Runtime::InteropServices::Out] bool %b3) 
		{
			CheckSize(1);
			char ch = *(char *) GetOff();
			cursor += 1;
			bool1 = (ch & 1) == 1;
			b2 = (ch & 2) == 2;
			b3 = (ch & 4) == 4;
		}

		virtual System::String ^GetString() 
		{
			int len = GetInt32();
			if(len<0)
				return nullptr;
			Encoding^ u8 = Encoding::UTF8;
			array<Byte> ^arr = gcnew array<Byte>(len);
			Byte* src = (Byte*)GetOff();
			for(int i=0; i< len;i++)
			{
				arr[i]=*(src+i);
			}
			cursor += len;
			return u8->GetString(arr);
		}
		virtual array<Byte> ^GetAllBytes()
		{
			array<Byte> ^arr = gcnew array<Byte>(size);
			Byte* src = (Byte*)GetOff();
			for(int i=0; i< size;i++)
			{
				arr[i]=*(src+i);
			}
			return arr;
		}
		//virtual array<Byte> ^GetObjectBytes()
		//{
		//	int len = GetInt32();
		//	if(len<0)
		//		return nullptr;
		//	array<Byte> ^arr = gcnew array<Byte>(len);
		//	Byte* src = (Byte*)GetOff();
		//	for(int i=0; i< len; i++)
		//	{
		//		arr[i]=*(src+i);
		//	}
		//	return arr;
		//}

        virtual array<Byte> ^GetByteArray()
		{
			Int32 len = GetInt32();
			array<Byte> ^res = gcnew array<Byte>(len);
			for(int i=0;i<len;i++)
				res[i] = GetByte();

			return res;
		}


	protected:
		void *GetOff()
		{
			return (char *)buffer + cursor;
		}

		void CheckSize(int sz)
		{
			if(cursor + sz > size)
				throw;
		}
	private:
		void *buffer;
		int size;
		int cursor;
	};

}}