using System;
using Store.BaseDatatypes;

namespace Store
{
    /// <summary>
    /// ������, ����������� � ���������
    /// </summary>
    public interface ITrace : IDisposable
    {
        /// <summary>
        /// ���������� ID ������
        /// </summary>
        ulong Id{get;}

        /// <summary>
        /// ���������� ��������� ������ � ������ � ��������� ������� �� ���� ������ ������.
        /// </summary>
        /// <returns></returns>
        Sensor NextSensor();

        /// <summary>
        /// �������
        /// </summary>
        void Close();
    }


    /// <summary>
    /// ������������� ����������� ������ ������, ����������� � ���������.
    /// </summary>
    internal sealed class Trace_Impl : DataObjectLeader, ITrace
    {
        System.IO.FileStream fileStream;
        System.IO.Compression.GZipStream gzip;
        System.IO.BinaryReader reader;
        
        Sensors sensors;
        Traces traces;

        internal Trace_Impl(Traces traces)
        {
            this.traces = traces;
            this.sensors = traces.storage.sensors;
        }

        /// <summary>
        /// ���������� �����, � ������� ����������� ������
        /// </summary>
        /// <returns></returns>
        private string GetFolder()
        {
            string folder = traces.storage.Get_DB().GetDBDirectory() + "/TracesStorage";
            if (!System.IO.Directory.Exists(folder))
                System.IO.Directory.CreateDirectory(folder);
            return folder;
        }

        /// <summary>
        /// ���������� ��� �����, ��������� ������
        /// </summary>
        /// <returns></returns>
        private string GenerateFileName()
        {
            return System.IO.Path.Combine(GetFolder(), theID.ToString(System.Globalization.CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// � ������, ���� ���� � ������� ��� �� ������, �� ��������� ����
        /// </summary>
        /// <returns></returns>
        private System.IO.BinaryReader getReader()
        {
            if (reader == null)
            {
                fileStream = new System.IO.FileStream(GenerateFileName(), System.IO.FileMode.Open, System.IO.FileAccess.Read);
                gzip = new System.IO.Compression.GZipStream(fileStream, System.IO.Compression.CompressionMode.Decompress);
                reader = new System.IO.BinaryReader(gzip);
            }

            return reader;
        }

        /// <summary>
        /// ���������� ��������� ������ � ������ � ��������� ������� �� ���� ������ ������.
        /// </summary>
        /// <returns></returns>
        public Sensor NextSensor()
        {
            UInt64 sensorID;
            try
            {
                sensorID = getReader().ReadUInt32();
            }
            catch (System.IO.EndOfStreamException)
            {
                return null;
            }

            return sensors.GetSensor(sensorID);
        }

        /// <summary>
        /// �������
        /// </summary>
        public void Close()
        {
            Dispose();
        }

        #region IDisposable Members

        /// <summary>
        /// ���������
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        public void Dispose()
        {
            if (reader != null)
            {
                reader.Dispose();
                gzip.Dispose();
                fileStream.Dispose();
            }

            reader = null;
            GC.SuppressFinalize(this);
        }

        #endregion
        /// <summary>
        /// ���������
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        ~Trace_Impl()
        {
            if(reader != null)
                IA.Monitor.Log.Error(Storage.objectName, "TraceReader �� ��� ����������");
        }

        /// <summary>
        /// ����������� ������ ������. ��� ��� ������������ TableLeader, ����� ������ �������� ���������. ����� � ���������, ���������� ���� �����.
        /// </summary>
        /// <param name="fileName">��� ����� � �������</param>
        /// <param name="checksum">����������� �����</param>
        internal void Import(string fileName, byte[] checksum)
        {
            const int buffSize = 4000;
            byte[] buffer;

            using (System.IO.FileStream fileStreamW = new System.IO.FileStream(GenerateFileName(), System.IO.FileMode.CreateNew, System.IO.FileAccess.Write))
            using (System.IO.Compression.GZipStream gzipW = new System.IO.Compression.GZipStream(fileStreamW, System.IO.Compression.CompressionMode.Compress))
            using (System.IO.BinaryWriter writer = new System.IO.BinaryWriter(gzipW))

            using (System.IO.FileStream fileStreamR =  new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            using (System.IO.BinaryReader readerR = new System.IO.BinaryReader(fileStreamR))
            {
                do
                {
                    buffer = readerR.ReadBytes(buffSize);
                    writer.Write(buffer);
                }
                while (buffer.Length == buffSize);
                traces.indexCheckSums.Put((wr) => { wr.Add(checksum); }, theID);
            }
        }
    }
}
