using System;
using System.Collections.Generic;

namespace Store
{
    /// <summary>
    /// �������� ����� ��� ������ � ��������.
    /// </summary>
    public sealed class Traces 
    {
        internal Storage storage;
        Store.BaseDatatypes.TableLeader<Trace_Impl> tableLeader;
        internal Store.BaseDatatypes.Table_ImmediateCreation_KeyBuffer_ValueId_NoDuplicate indexCheckSums;

        const string indexCheckSumsName = "TracesCheckSums";
        const string tableLeaderName = "Traces";


        /// <summary>
        /// ��������� ���������� Traces �������������� ���������.
        /// </summary>
        /// <param name="storage"></param>
        internal Traces(Storage storage)
        {
            this.storage = storage;
            tableLeader = new BaseDatatypes.TableLeader<Trace_Impl>(storage, tableLeaderName, (kind) => new Trace_Impl(this));
            indexCheckSums = new BaseDatatypes.Table_ImmediateCreation_KeyBuffer_ValueId_NoDuplicate(storage, indexCheckSumsName);
        }

        /// <summary>
        /// ����������� ��� ����������� ������
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ITrace> EnumerateTraces()
        {
            return tableLeader.EnumerateEveryone();
        }


        /// <summary>
        /// �������� ����� ������.
        /// ������ ����� ��������� ������ �������. ��� ������ ��������� � ����� ������������ �������. � ����� ������ ���� ������ ���� ������ (�����������, ����� -1, �� �����������). 
        /// ������� ������ ���� ������������ � �������� ����. ������ ������� ������� - 4 �����.
        /// ���� � ������� ����������� � ��������� (�������� ���� ���������).
        /// </summary>
        /// <returns>��������� ������ ��������������� � ��������� ������, ���� ��������� ������ �� ������, �� null</returns>
        public ITrace AddTrace(string fileName)
        {
            Trace_Impl trace = null;
            byte[] checksum = GetChecksum(fileName);

            if (!CheckTraceExistence(checksum))
            {
                trace = tableLeader.Add();
                trace.Import(fileName, checksum);
            }

            System.IO.File.Delete(fileName);

            return trace;
        }


        /// <summary>
        /// ��������� ������������� � ��������� ������ � ��������� ����������� ������
        /// </summary>
        /// <param name="checksum"></param>
        /// <returns>���������� true, ���� ������ � ����� ����������� ������ ���������� � ���������</returns>
        private bool CheckTraceExistence(byte[] checksum)
        {
            UInt64 Id = indexCheckSums.GetValueByKey((writer)=>{writer.Add(checksum);});
            return  Id != Store.Const.CONSTS.WrongID;
        }

        /// <summary>
        /// ������� �������� ���������� ����� � ���������
        /// </summary>
        /// <returns>���������� ���������� ����� � ���������</returns>
        public ulong Count()
        {
            return tableLeader.Count();
        }

        /// <summary>
        /// �������� �������� ����������� ����� �����
        /// </summary>
        /// <param name="fileName">���� � �����</param>
        /// <returns>���������� ����������� ����� ����� � ���� ��������� �������</returns>
        static private byte[] GetChecksum(string fileName)
        {
            using (System.IO.FileStream stream = System.IO.File.OpenRead(fileName))
                using (System.Security.Cryptography.SHA256Managed sha = new System.Security.Cryptography.SHA256Managed())
                   return sha.ComputeHash(stream);
        }
    }

}
