﻿using System;
using System.Collections.Generic;
using Store.BaseDatatypes;

namespace Store
{
    /// <summary>
    /// Описатель пространства имён
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Namespace")]
    public interface Namespace : IDataObjectLeader
    {
        /// <summary>
        /// Имя данного namespace.
        /// </summary>
        string Name { get; }
        

        /// <summary>
        /// Полное имя namespace
        /// </summary>
        string FullName { get; }

        /// <summary>
        /// Найти вложенный namespace. Если такого нет, то namespace будет создан.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Namespace FindOrCreateSubNamespace(string name);
        /// <summary>
        /// Найти вложенный namespace. Если такого нет, то namespace будет создан.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        UInt64 FindOrCreateSubNamespace_Id(string name);

        /// <summary>
        /// Найти вложенный namespace. Если такого нет, то namespace будет создан.
        /// </summary>
        /// <param name="name">Полное имя namespace. Например System.IO будет содержаться в массиве из двух элементов.</param>
        /// <returns></returns>
        Namespace FindOrCreateSubNamespace(string[] name);
        /// <summary>
        /// Найти вложенный namespace. Если такого нет, то namespace будет создан.
        /// </summary>
        /// <param name="name">Полное имя namespace. Например System.IO будет содержаться в массиве из двух элементов.</param>
        /// <returns></returns>
        UInt64 FindOrCreateSubNamespace_Id(string[] name);

        /// <summary>
        /// Найти вложенный namespace с указанным именем. Если такого namespace не существует, то возвращает null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Namespace FindSubNamespace(string name);
        /// <summary>
        /// Найти вложенный namespace с указанным именем. Если такого namespace не существует, то возвращает null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        UInt64 FindSubNamespace_Id(string name);

        /// <summary>
        /// Найти вложенный namespace с указанным именем. Если такого namespace не существует, то возвращает null.
        /// </summary>
        /// <param name="name">Полное имя namespace. Например System.IO будет содержаться в массиве из двух элементов.</param>
        /// <returns></returns>
        Namespace FindSubNamespace(string[] name);
        /// <summary>
        /// Найти вложенный namespace с указанным именем. Если такого namespace не существует, то возвращает null.
        /// </summary>
        /// <param name="name">Полное имя namespace. Например System.IO будет содержаться в массиве из двух элементов.</param>
        /// <returns></returns>
        UInt64 FindSubNamespace_Id(string[] name);

        /// <summary>
        /// Указать, что класс находится в данном пространстве имен
        /// </summary>
        /// <param name="clazz"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "clazz")]
        void AddClassToNamespace(Class clazz);

        /// <summary>
        /// Перечислить все классы из данного пространства имен
        /// </summary>
        /// <returns></returns>
        IEnumerable<Class> EnumerateClassesInNamespace();

        /// <summary>
        /// Перечисляет все классы в Namespace с указанным именем
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        IEnumerable<Class> FindClass(string className);


        /// <summary>
        /// Указать, что функция находится в данном пространстве имен
        /// </summary>
        /// <param name="function"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Function")]
        void AddFunctionToNamespace(IFunction function);
        /// <summary>
        /// Перечислить все функции в данном пространстве имен
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunction> EnumerateFunctionsInNamespace();

        /// <summary>
        /// Указать, что функция находится в данном пространстве имен
        /// </summary>
        /// <param name="variable"></param>
        void AddVariableToNamespace(Variable variable);
        /// <summary>
        /// Перечислить все функции в данном пространстве имен
        /// </summary>
        /// <returns></returns>
        IEnumerable<Variable> EnumerateVariablesInNamespace();
    }

    internal class Namespace_Impl : BaseDatatypes.DataObjectLeaderWithName, Namespace
    {
        Namespaces theNamespaces;

        internal Namespace_Impl(Namespaces ns) : base(ns.tableNaming)
        {
            theNamespaces = ns;
        }

        #region Namespace Members

        public Namespace FindOrCreateSubNamespace(string name)
        {
            Namespace ns = FindSubNamespace(name);
            if (ns == null)
                ns = CreateSubNamespace(name);

            return ns;
        }

        private Namespace CreateSubNamespace(string name)
        {
            Namespace_Impl ns = theNamespaces.tableLeader.Add();
            theNamespaces.tableNaming.SetName(ref ns.naming, ns.Id, name);
            theNamespaces.tableNestedNamespaces.PutWithoutRepetition(this.Id, ns.Id);
            theNamespaces.tableParrentNamespaces.Put(ns.Id, this.Id);

            return ns;
        }

        public UInt64 FindOrCreateSubNamespace_Id(string name)
        {
            UInt64 id = FindSubNamespace_Id(name);
            
            if (id == Store.Const.CONSTS.WrongID)
            {
                Namespace ns = CreateSubNamespace(name);
                id = ns.Id;
            }

            return id;
        }

        public Namespace FindOrCreateSubNamespace(string[] name)
        {
            if (name != null)
            {
                Namespace ns = FindOrCreateSubNamespace(name[0]);

                for (int i = 1; i < name.Length; i++)
                    ns = ns.FindOrCreateSubNamespace(name[i]);

                return ns;
            }
            return null;
        }

        public UInt64 FindOrCreateSubNamespace_Id(string[] name)
        {
            if (name != null)
            {
                Namespace ns = FindOrCreateSubNamespace(name[0]);

                for (int i = 1; i < name.Length; i++)
                    ns = ns.FindOrCreateSubNamespace(name[i]);

                return ns.Id;
            }
            return 0;
        }

        public Namespace FindSubNamespace(string name)
        {
            UInt64 id = FindSubNamespace_Id(name);
            if (id == Store.Const.CONSTS.WrongID)
                return null;
            else
                return theNamespaces.tableLeader.Get(id);
        }

        public ulong FindSubNamespace_Id(string name)
        {
            foreach (UInt64 id in theNamespaces.tableNaming.Find(name))
                if (theNamespaces.tableParrentNamespaces.GetValueByID(id) == this.Id)
                    return id;

            return Store.Const.CONSTS.WrongID;
        }

        public Namespace FindSubNamespace(string[] name)
        {
            if (name != null)
            {
                Namespace ns = FindSubNamespace(name[0]);

                for (int i = 1; i < name.Length; i++)
                    ns = ns.FindSubNamespace(name[i]);

                return ns;
            }
            return null;
        }

        public ulong FindSubNamespace_Id(string[] name)
        {
            if (name != null)
            {
                Namespace ns = FindSubNamespace(name[0]);

                for (int i = 1; i < name.Length; i++)
                    ns = ns.FindSubNamespace(name[i]);

                return ns.Id;
            }
            return 0;
        }

        public void AddClassToNamespace(Class clazz)
        {
            if (clazz != null)
            {
                theNamespaces.tableNamespaceClasses.PutWithoutRepetition(theID, clazz.Id);
                theNamespaces.tableClassNamespace.Put(clazz.Id, theID);
            }
        }

        public IEnumerable<Class> EnumerateClassesInNamespace()
        {
            foreach (UInt64 id in theNamespaces.tableNamespaceClasses.GetValueByKey(theID))
                yield return theNamespaces.storage.classes.GetClass(id);
        }

        public IEnumerable<Class> FindClass(string className)
        {
            foreach (Class clazz in theNamespaces.storage.classes.GetClass(className))
                if (theNamespaces.tableClassNamespace.GetValueByID(clazz.Id) == theID)
                    yield return clazz;
        }


        public void AddFunctionToNamespace(IFunction function)
        {
            if (function != null)
            {
                theNamespaces.tableNamespaceFunctions.PutWithoutRepetition(theID, function.Id);
                theNamespaces.tableFunctionNamespace.Put(function.Id, theID);
            }
        }

        public IEnumerable<IFunction> EnumerateFunctionsInNamespace()
        {
            foreach (UInt64 id in theNamespaces.tableNamespaceFunctions.GetValueByKey(theID))
                yield return theNamespaces.storage.functions.GetFunction(id);
        }

        public void AddVariableToNamespace(Variable variable)
        {
            if (variable != null)
            {
                theNamespaces.tableNamespaceVariables.PutWithoutRepetition(theID, variable.Id);
                theNamespaces.tableVariableNamespace.Put(variable.Id, theID);
            }
        }

        public IEnumerable<Variable> EnumerateVariablesInNamespace()
        {
            foreach (UInt64 id in theNamespaces.tableNamespaceVariables.GetValueByKey(theID))
                yield return theNamespaces.storage.variables.GetVariable(id);
        }
 
	    #endregion    

        internal override DataObjectLeaderWithName GetContainer()
        {
            UInt64 id = theNamespaces.tableParrentNamespaces.GetValueByID(theID);

            if (id != Store.Const.CONSTS.WrongID)
                return theNamespaces.tableLeader.Get(id);
            else
                return null;
        }

        internal override string NameForReports
        {
            get { return Name; }
        }

        internal override string NameForFileName
        {
            get
            {
                string ret = DataObjectLeaderWithName.DataObjectNameForFileName(Name);
                return ret;
            }
        }
    }
}
