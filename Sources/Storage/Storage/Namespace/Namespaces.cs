﻿using System;
using System.Collections.Generic;
using Store.BaseDatatypes;
using Store.Table;


namespace Store
{
    /// <summary>
    /// Раздел Хранилища, содержащий пространства имён.
    /// </summary>
    public sealed class Namespaces
    {
        internal Storage storage;

        const string tableName_Leader = "Namespaces_Leader";
        const string tableName_Naming = "Namespaces_Naming";
        const string tableName_NamingIndex = "iNamespaces_Naming";
        const string tableName_NestedNamespaces = "Namespaces_NestedNamespaces";
        const string tableName_ParentNamespaces = "iNamespaces_ParentNamespaces";
        const string tableName_Classes = "NamespacesClasses";
        const string tableName_ClassesIn = "iNamespacesClasses";
        const string tableName_Functions = "NamespacesFunctions";
        const string tableName_FunctionsIn = "iNamespacesFunctions";
        const string tableName_Variables = "NamespacesVariables";
        const string tableName_VariablesIn = "iNamespacesVariables";


        internal BaseDatatypes.TableLeader<Namespace_Impl> tableLeader;
        internal BaseDatatypes.TableObjectNamingWithIndex tableNaming;

        /// <summary>
        /// Ключ - идентификатор namespace-родителя или WrongId, если ребенок является namespace верхнего уровня.
        /// Значение - Вложенный namespace
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableNestedNamespaces;

        /// <summary>
        /// Классы, принадлежащие данному namespace
        /// Ключ - идентификатор namespace
        /// Значение - идентификатор класса
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableNamespaceClasses;

        /// <summary>
        /// Функции, принадлежащие данному namespace
        /// Ключ - идентификатор namespace
        /// Значение - идентификатор функции
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableNamespaceFunctions;

        /// <summary>
        /// Переменные, принадлежащие данному namespace
        /// Ключ - идентификатор namespace
        /// Значение - идентификатор переменной
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableNamespaceVariables;

        /// <summary>
        /// Ключ - идентификатор вложенного namespace
        /// Значение - идентификатор namespace-родителя или WrongId, если ребенок является namespace верхнего уровня.
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueId_NoDuplicate tableParrentNamespaces;

        /// <summary>
        /// Namespace, в который вложен данный класс
        /// Ключ - идентификатор класса
        /// Значение - идентификатор namespace
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueId_NoDuplicate tableClassNamespace;

        /// <summary>
        /// Namespace, в который вложена данная функция
        /// Ключ - идентификатор функции
        /// Значение - идентификатор namespace
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueId_NoDuplicate tableFunctionNamespace;

        /// <summary>
        /// Namespace, в который вложена данная переменнная
        /// Ключ - идентификатор переменной
        /// Значение - идентификатор namespace
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueId_NoDuplicate tableVariableNamespace;

        internal Namespaces(Storage storage)
        {
            this.storage = storage;

            tableLeader = new TableLeader<Namespace_Impl>(storage, tableName_Leader, (kind) => { return  new Namespace_Impl(this); });
            tableNaming = new TableObjectNamingWithIndex(storage, tableName_Naming, tableName_NamingIndex, EnumerateNaming);
            tableNestedNamespaces = new Table_KeyId_ValueId_Dublicates(storage, tableName_NestedNamespaces);
            tableParrentNamespaces = new Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, tableName_ParentNamespaces, ParrentNamespacesRebuild);
            tableNamespaceClasses = new Table_KeyId_ValueId_Dublicates(storage, tableName_Classes);
            tableNamespaceFunctions = new Table_KeyId_ValueId_Dublicates(storage, tableName_Functions);
            tableNamespaceVariables = new Table_KeyId_ValueId_Dublicates(storage, tableName_Variables);
            tableClassNamespace = new Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, tableName_ClassesIn, rebuildClasses);
            tableFunctionNamespace = new Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, tableName_FunctionsIn, rebuildFunctions);
            tableVariableNamespace = new Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, tableName_VariablesIn, rebuildVariables);
        }

        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult> EnumerateNaming(IBufferWriter key, IBufferWriter value)
        {
            foreach (Namespace ns in EnumerateNamespaces())
            {
                key.Add(ns.Name);
                value.Add(ns.Id);

                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, null);
            }
        }

        private IEnumerable<KeyValuePair<UInt64, UInt64>> ParrentNamespacesRebuild()
        {
            foreach (KeyValuePair<UInt64, UInt64> pair in tableNestedNamespaces.EnumerateEveryone())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Value, pair.Key);
        }

        private IEnumerable<KeyValuePair<UInt64, UInt64>> rebuildClasses()
        {
            foreach (KeyValuePair<UInt64, UInt64> pair in tableNamespaceClasses.EnumerateEveryone())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Value, pair.Key);
        }

        private IEnumerable<KeyValuePair<UInt64, UInt64>> rebuildFunctions()
        {
            foreach (KeyValuePair<UInt64, UInt64> pair in tableNamespaceFunctions.EnumerateEveryone())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Value, pair.Key);
        }

        private IEnumerable<KeyValuePair<UInt64, UInt64>> rebuildVariables()
        {
            foreach (KeyValuePair<UInt64, UInt64> pair in tableNamespaceVariables.EnumerateEveryone())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Value, pair.Key);
        }
        /// <summary>
        /// Найти namespace с указанным именем. Если такого namespace не существует, он будет создан.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Namespace FindOrCreate(string name)
        {
            Namespace ns = Find(name);
            if (ns == null)
                return Create(name);
            else
                return ns;
        }

        private Namespace Create(string name)
        {
            Namespace_Impl ns = tableLeader.Add();
            tableNaming.SetName(ref ns.naming, ns.Id, name);
            return ns;
        }

        /// <summary>
        /// Найти namespace с указанным именем. Если такого namespace не существует, он будет создан.
        /// </summary>
        /// <param name="name">Полное имя namespace. Например System.IO будет содержаться в массиве из двух элементов.</param>
        /// <returns></returns>
        public Namespace FindOrCreate(string[] name)
        {
            if (name != null)
            {
                Namespace ns = FindOrCreate(name[0]);

                for (int i = 1; i < name.Length; i++)
                    ns = ns.FindOrCreateSubNamespace(name[i]);
                return ns;
            }
            return null;
        }

        /// <summary>
        /// Найти namespace с указанным именем. Если такого namespace не существует, он будет создан.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public UInt64 FindOrCreate_Id(string name)
        {
            UInt64 id = Find_Id(name);
            if (id == Store.Const.CONSTS.WrongID)
                return Create(name).Id;
            else
                return id;
        }

        /// <summary>
        /// Найти namespace с указанным именем. Если такого namespace не существует, он будет создан.
        /// </summary>
        /// <param name="name">Полное имя namespace. Например System.IO будет содержаться в массиве из двух элементов.</param>
        /// <returns></returns>
        public UInt64 FindOrCreate_Id(string[] name)
        {
            if (name != null)
            {
                Namespace ns = FindOrCreate(name[0]);

                for (int i = 1; i < name.Length - 1; i++)
                    ns = ns.FindOrCreateSubNamespace(name[i]);

                return ns.FindOrCreateSubNamespace_Id(name[name.Length - 1]);
            }
            return 0;
        }

        /// <summary>
        /// Найти namespace с указанным именем. Если namespace нет, вернет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Namespace Find(string name)
        {
            UInt64 id = Find_Id(name);
            if (id == Store.Const.CONSTS.WrongID)
                return null;
            else
                return tableLeader.Get(id);
        }
        /// <summary>
        /// Найти namespace с указанным именем. Если namespace нет, вернет null.
        /// </summary>
        /// <param name="name">Полное имя namespace. Например System.IO будет содержаться в массиве из двух элементов.</param>
        /// <returns></returns>
        public Namespace Find(string[] name)
        {
            if (name != null)
            {
                Namespace ns = Find(name[0]);

                for (int i = 1; i < name.Length; i++)
                    ns = ns.FindSubNamespace(name[i]);

                return ns;
            }
            return null;
        }
        
        /// <summary>
        /// Найти namespace с указанным именем. Если namespace нет, вернет null.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public UInt64 Find_Id(string name)
        {
            foreach (UInt64 id in tableNaming.Find(name))
                if (tableParrentNamespaces.GetValueByID(id) == Store.Const.CONSTS.WrongID)
                    return id;

            return Store.Const.CONSTS.WrongID;
        }

        /// <summary>
        /// Найти namespace с указанным именем. Если namespace нет, вернет null.
        /// </summary>
        /// <param name="name">Полное имя namespace. Например System.IO будет содержаться в массиве из двух элементов.</param>
        /// <returns></returns>
        public UInt64 Find_Id(string[] name)
        {
            if (name != null)
            {
                Namespace ns = Find(name[0]);

                for (int i = 1; i < name.Length - 1; i++)
                    ns = ns.FindSubNamespace(name[i]);

                return ns.FindSubNamespace_Id(name[name.Length - 1]);
            }
            return 0;
        }

        /// <summary>
        /// Перечислить все известные namespace.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Namespace> EnumerateNamespaces()
        {
            return tableLeader.EnumerateEveryone();
        }

    }
}
