﻿using System;

namespace Store
{
    /// <summary>
    /// Класс представляет временную папку. При его создании папка создается, при уничтожении - удаляется
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
    public class TempDirectory : IDisposable
    {
        string thePath;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="pluginDataFolder"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString")]
        public TempDirectory(string pluginDataFolder)
        {
            int i = 0;
            string tempName = "Temp";

            while (IOController.DirectoryController.IsExists(thePath = System.IO.Path.Combine(pluginDataFolder, tempName + i.ToString())))
                i++;

            IOController.DirectoryController.Create(thePath);
        }

        /// <summary>
        /// Возвращает путь к временной папке
        /// </summary>
        public string Path
        { get { return thePath; } }



        #region IDisposable Members

        bool isDisposed = false;
        /// <summary>
        /// Закрыть объект и освободить ресурсы
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        public void Dispose()
        {
            try
            {
                if (!isDisposed)
                {
                    if (IOController.DirectoryController.IsExists(thePath))
                        IOController.DirectoryController.Remove(thePath);
                    isDisposed = true;
                }
            }
            catch { }
        }

        /// <summary>
        /// Деструтор
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        ~TempDirectory()
        {
            Dispose();
        }

        #endregion
    }
}
