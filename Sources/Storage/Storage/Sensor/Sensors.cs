﻿using System;
using System.Collections.Generic;
using IA;
using Store.BaseDatatypes;
using Store.Table;


namespace Store
{
    /// <summary>
    /// Исключение, возникающее при попытке добавить в Хранилище датчик, уже в нём присутсвующий.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable")]
    public sealed class SensorExistsException : Store.Const.StorageException
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public SensorExistsException(string name)
            : base(name)
        { }
    }

    /// <summary>
    /// Головной класс для работы с датчиками.
    /// </summary>
    public sealed class Sensors
    {
        //Имена таблиц
        const string TableName_sensors = "Sensors";
        const string TableName_BeforeStatements = "iSensorsBeforeStatements";

        internal Storage storage;

        //Основные таблицы
        TableIDNoDuplicate tableSensors;
        WeakRefTable<Sensor_Impl> openedSensors = new WeakRefTable<Sensor_Impl>();


        //Индексные таблицы

        /// <summary>
        /// Ключ - идентификатор оператора
        /// Значение - идентификатор датчика, расположенного перед этим оператором
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueId_NoDuplicate indexBeforeStatements;


        /// <summary>
        /// Максимальный номер датчика в Хранилище. Store.Const.CONSTS.WrongID, если не заполнен.
        /// </summary>
        UInt64 maxSensorId = Store.Const.CONSTS.WrongID;

        /// <summary>
        /// Номер датчика, который будет вставлен при следующей последовательной установке датчиков
        /// </summary>
        UInt64 NextSensorId = Store.Const.CONSTS.WrongID;

        #region Generate Sensors

        /// <summary>
        /// Создавать экземпляры Sensors самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal Sensors(Storage storage)
        {
            this.storage = storage;
            tableSensors = storage.GetTableIDNoDuplicate(TableName_sensors);
            indexBeforeStatements = new Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, TableName_BeforeStatements, enumateBeforeStatements);
        }


        private IEnumerable<KeyValuePair<UInt64, UInt64>> enumateBeforeStatements()
        {
            foreach (Sensor sensor in EnumerateSensors())
            {
                UInt64 id = sensor.GetBeforeStatementId();
                if(id != Store.Const.CONSTS.WrongID)        //Датчики, для каторый оператор не указан, добавлять в индекс не надо
                    yield return new KeyValuePair<UInt64, UInt64>(id, sensor.ID);
            }
        }

        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult>
                        EnumerateItemsInIndexLocationsSortedByFileAndLine(IBufferWriter key, IBufferWriter value)
        {
            foreach (Sensor sensor in EnumerateSensors())
            {
                (sensor as Sensor_Impl).GenerateIndexKey(key);

                value.Add(sensor.ID);

                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, (sensor as Sensor_Impl).marker);
            }
        }

        #endregion

        /// <summary>
        /// Установить номер датчика, с которого будут расставляться датчики функциями <see cref="Store.Sensors.AddSensor(UInt64, Store.Kind)"/> и <see cref="AddSensorBeforeStatement(Store.IStatement, Store.Kind)"/>.
        /// </summary>
        /// <param name="number"></param>
        public void SetSensorStartNumber(UInt64 number)
        {
            if (number >= UInt32.MaxValue)
                throw new Store.Const.StorageException("Максимальный номер датчика не может превышать UInt32.MaxValue");
            IBufferReader reader;
            tableSensors.Get(number, out reader);  //Проверили, есть ли такой номер в Хранилище, просто попытавшись его считать
            if (reader != null)
            {
                reader.Dispose();
                throw new Store.Const.StorageException("Попытка начать вставку датчиков с номера, который уже использован в Хранилище");
            }

            NextSensorId = number;
        }

        /// <summary>
        /// Установить номер датчика, с которого будут расставляться датчики функциями <see cref="Store.Sensors.AddSensor(UInt64, Store.Kind)"/> и <see cref="AddSensorBeforeStatement(Store.IStatement, Store.Kind)"/>, 
        /// равный максимальному номеру вставленных датчиков плюс один.
        /// </summary>
        /// <returns>Значение номера датчика, с которого будет произведена расстановка.</returns>
        public ulong SetSensorStartNumberAsMaximum()
        {
            GetMaxSensorNumber();

            NextSensorId = maxSensorId + 1;

            if (NextSensorId >= UInt32.MaxValue)
                throw new Store.Const.StorageException("Максимальный номер датчика не может превышать UInt32.MaxValue");

            return NextSensorId;
        }

        /// <summary>
        /// Получить максимальный номер датчика, находящегося в Хранилище
        /// </summary>
        public UInt64 GetMaxSensorNumber()
        {
            if (maxSensorId == Store.Const.CONSTS.WrongID)
            {
                maxSensorId = 0;
                foreach (Sensor sensor in EnumerateSensors())
                    maxSensorId = Math.Max(maxSensorId, sensor.ID);
            }

            return maxSensorId;
        }


        /// <summary>
        /// Добавить новый датчик в базу.
        /// В случае, если такой датчик в базе уже присутствует, выдается исключение типа <see cref="Store.SensorExistsException"/>
        /// </summary>
        /// <param name="sensorId">идентификатор датчика</param>
        /// <param name="function">идентификатор функции, в которую вставлен датчик</param>
        /// <param name="kind"></param>
        public void AddSensor(UInt64 sensorId, UInt64 function, Kind kind)
        {
            Sensor_Impl.Add(this, tableSensors, sensorId, function, kind);

            //Поддерживаем максимальный номер датчика и номер следующего датчика
            maxSensorId = Math.Max(maxSensorId, sensorId);
            NextSensorId = maxSensorId + 1;

            if (NextSensorId >= UInt32.MaxValue)
                throw new Store.Const.StorageException("Максимальный номер датчика не может превышать UInt32.MaxValue");
        }

        /// <summary>
        /// Добавить новый датчик в базу. 
        /// Номер вставляемого датчика вычисляется по следующему алгоритму: 
        /// <list type="bullet">
        /// <item>если ранее был вставлен датчик (в текущее открытие Хранилища) и после этого не вызывались функции <see cref="SetSensorStartNumber(UInt64)"/> и <see cref="SetSensorStartNumberAsMaximum"/>, то берется номер на 1 больший, нежели номер вставленного датчика;</item>
        /// <item>если ранее была вызвана одна из функций <see cref="SetSensorStartNumber(UInt64)"/> и <see cref="SetSensorStartNumberAsMaximum"/> и после этого не было вставлено ни одного датчика, то берется номер, установленный вызовом функции функции <see cref="SetSensorStartNumber(UInt64)"/>;</item>
        /// <item>если ранее не было вставлено датчиков и не была вызвана одна из функций <see cref="SetSensorStartNumber(UInt64)"/> и <see cref="SetSensorStartNumberAsMaximum"/>, то выдается ошибка;</item>
        /// </list>
        /// </summary>
        /// <param name="function">идентификатор функции, в которую вставлен датчик</param>
        /// <param name="kind"></param>
        public void AddSensor(UInt64 function, Kind kind)
        {
            if (NextSensorId == Store.Const.CONSTS.WrongID)
                throw new Exception("До вызова функции AddSensor(UInt64 function, Kind kind) необходимо установить номер следующего датчика");

            Sensor_Impl.Add(this, tableSensors, NextSensorId, function, kind);

            //Поддерживаем максимальный номер датчика
            maxSensorId = Math.Max(maxSensorId, NextSensorId);
            NextSensorId = NextSensorId + 1;

            if (NextSensorId >= UInt32.MaxValue)
                throw new Store.Const.StorageException("Максимальный номер датчика не может превышать UInt32.MaxValue");
        }


        /// <summary>
        /// Добавить новый датчик в базу.
        /// </summary>
        /// <param name="sensorId">идентификатор датчика</param>
        /// <param name="statement">идентификатор оператора, перед которым вставлен датчик</param>
        /// <param name="kind"></param>
        public void AddSensorBeforeStatement(UInt64 sensorId, IStatement statement, Kind kind)
        {
            Sensor_Impl.Add(this, tableSensors, sensorId, statement, kind);

            //Поддерживаем максимальный номер датчика и номер следующего датчика
            maxSensorId = Math.Max(maxSensorId, sensorId);
            NextSensorId = maxSensorId + 1;

            if (NextSensorId >= UInt32.MaxValue)
                throw new Store.Const.StorageException("Максимальный номер датчика не может превышать UInt32.MaxValue");
        }

        /// <summary>
        /// Добавить новый датчик в базу.
        /// Номер вставляемого датчика вычисляется по следующему алгоритму: 
        /// <list type="bullet">
        /// <item>если ранее был вставлен датчик (в текущее открытие Хранилища) и после этого не вызывались функции <see cref="SetSensorStartNumber(UInt64)"/> и <see cref="SetSensorStartNumberAsMaximum"/>, то берется номер на 1 больший, нежели номер вставленного датчика;</item>
        /// <item>если ранее была вызвана одна из функций <see cref="SetSensorStartNumber(UInt64)"/> и <see cref="SetSensorStartNumberAsMaximum"/> и после этого не было вставлено ни одного датчика, то берется номер, установленный вызовом функции функции <see cref="SetSensorStartNumber(UInt64)"/>;</item>
        /// <item>если ранее не было вставлено датчиков и не была вызвана одна из функций <see cref="SetSensorStartNumber(UInt64)"/> и <see cref="SetSensorStartNumberAsMaximum"/>, то выдается ошибка;</item>
        /// </list>
        /// </summary>
        /// <param name="statement">идентификатор оператора, перед которым вставлен датчик</param>
        /// <param name="kind"></param>
        public void AddSensorBeforeStatement(IStatement statement, Kind kind)
        {
            if (NextSensorId == Store.Const.CONSTS.WrongID)
                throw new Exception("До вызова функции AddSensor(UInt64 function, Kind kind) необходимо установить номер следующего датчика");

            IBufferReader get;
            if (tableSensors.Get(NextSensorId, out get) == enResult.OK)
            {
                Monitor.Log.Error(Storage.objectName, "Расстановка датчиков с номера <" + NextSensorId + "> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <" + (this.GetMaxSensorNumber() + 1) + ">.");
                NextSensorId = GetMaxSensorNumber() + 1;
            }

            Sensor_Impl.Add(this, tableSensors, NextSensorId, statement, kind);

            //Поддерживаем максимальный номер датчика
            maxSensorId = Math.Max(maxSensorId, NextSensorId);
            NextSensorId = NextSensorId + 1;

            if (NextSensorId >= UInt32.MaxValue)
                throw new Store.Const.StorageException("Максимальный номер датчика не может превышать UInt32.MaxValue");
        }

        /// <summary>
        /// Получить описание датчика с указанным идентификатором.
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns>Датчик, если он присутствует в базе. null, если такого датчика в базе нет.</returns>
        public Sensor GetSensor(UInt64 objectId)
        {
            Sensor_Impl res = openedSensors.Get(objectId);
            if (res == null)
            {
                res = new Sensor_Impl(this);
                if (res.TryInitialize(tableSensors, objectId))
                    openedSensors.Add(objectId, res);
                else
                    return null;
            }

            return res;
        }

        /// <summary>
        /// Переисляет все имеющиеся в Хранилище датчики.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Sensor> EnumerateSensors()
        {
            foreach (KeyValuePair<UInt64, IBufferReader> pair in tableSensors)
            {
                Sensor_Impl res = openedSensors.Get(pair.Key);
                if (res == null)
                {
                    res = new Sensor_Impl(this);
                    res.Initialize(tableSensors, pair.Key, pair.Value);
                    openedSensors.Add(pair.Key, res);
                }
                yield return res;
            }
        }

        /// <summary>
        /// Возвращает количество датчиков в Хранилище
        /// </summary>
        /// <returns></returns>
        public ulong Count()
        {
            return tableSensors.Count();
        }

        /// <summary>
        /// Удаляет все датчики из Хранилища.
        /// </summary>
        public void Clear()
        {
            storage.DeleteDB(TableName_sensors, tableSensors);
            indexBeforeStatements.Clear();
            openedSensors.Clear();

            tableSensors = storage.GetTableIDNoDuplicate(TableName_sensors);
        }

        /// <summary>
        /// Удаляем датчик с указанным номером из хранилища.
        /// Если датчика с таким номером найдено не было - бросаем StorageExcepion
        /// </summary>
        /// <param name="objectId"></param>
        public void RemoveSensor(UInt64 objectId)
        {
            //ищем объект в кеше
            Sensor_Impl res = openedSensors.Get(objectId);
            //если не нашли - достаем напрямую из таблицы
            if (res == null)
            {
                res = new Sensor_Impl(this);
                if (res.TryInitialize(tableSensors, objectId))
                    openedSensors.Add(objectId, res);
                else
                    throw new Const.StorageException("Датчик с указанным номером не зарегистрирован в Хранилище");
            }

            //очищаем таблицы
            indexBeforeStatements.Remove(res.GetBeforeStatementId());
            tableSensors.Remove(res.ID);
            //очищаем кеш
            openedSensors.Remove(objectId);
        }
    }
}
