﻿using System;
using Store.Table;

namespace Store
{
    /// <summary>
    /// Тип датчика
    /// </summary>
    public enum Kind
    {
        /// <summary>
        /// Датчик находится на входе функции
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "START")]
        START, 
        
        /// <summary>
        /// Датчик находится на выходе функции
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "FINAL")]
        FINAL, 

        /// <summary>
        /// Датчик в середине функции
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "INTERNAL")]
        INTERNAL,

        /// <summary>
        /// В случае, когда датчик указывается для оператора, то тип датчика можно вычислить автоматически. В этом случае указывается данный параметр
        /// </summary>
        NOT_SPECIFIED
    }

    /// <summary>
    /// Датчик в исходных текстах
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface Sensor
    {
        /// <summary>
        /// Идентификатор датчика
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID")]
        UInt64 ID {get;}

        /// <summary>
        /// Идентификатор функции, в которую вставлен датчик
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        UInt64 GetFunctionID();

        /// <summary>
        /// Функция, в которую вставлен датчик
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IFunction GetFunction();

        /// <summary>
        /// Возвращает оператор, перед которым вставлен датчик
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IStatement GetBeforeStatement();
        /// <summary>
        /// Возвращает идентификатор оператора, перед которым вставлен датчик
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        UInt64 GetBeforeStatementId();

        /// <summary>
        /// Местоположение датчика в функции
        /// </summary>
        Kind Kind
        { get; set; }

    }


    /// <summary>
    /// Релизация датчика
    /// </summary>
    internal class Sensor_Impl : Store.BaseDatatypes.DataObject_IDNoDuplicate_Immediate, Sensor
    {
        UInt64 functionID;
        UInt64 statementId;
        Kind kind = 0;
        Sensors sensors;

        /// <summary>
        /// Маркер данного объекта в индексе
        /// </summary>
        internal Store.BaseDatatypes.Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates marker;


        internal Sensor_Impl(Sensors sensors)
        {
            this.sensors = sensors;
        }

        /// <summary>
        /// Инициализировать
        /// </summary>
        /// <param name="table"></param>
        /// <param name="ID"></param>
        /// <returns>true, если загрузить датчик удалось; false - загрузить не удалось</returns>
        internal bool TryInitialize(Store.Table.TableIDNoDuplicate table, UInt64 ID)
        {
            this.theTable = table;
            theID = ID;

            IBufferReader reader;
            if (table.Get(ID, out reader) != enResult.OK)
                return false;

            Load(reader);
            return true;
        }

        protected override string GetLoadErrorString()
        {
            //Этот метод никогнда не будет вызван
            throw new NotImplementedException();
        }

        protected override void SaveToBuffer(IBufferWriter buffer)
        {
            buffer.Add(functionID);
            buffer.Add(statementId);
            buffer.Add((byte)kind);
        }

        protected override void SaveIndexes()
        {
        }

        protected override void Load(Store.Table.IBufferReader reader)
        {
            functionID = reader.GetUInt64();
            statementId = reader.GetUInt64();
            kind = (Kind)reader.GetByte();

            if (marker != null)
                marker.InformLoaded(buffer => GenerateIndexKey(buffer));
        }

        /// <summary>
        /// Генерирует ключ для индекса, используемого для перечисления датчиков в порядке в соответствии с местом их вставки в тексты
        /// </summary>
        /// <param name="key"></param>
        internal void GenerateIndexKey(IBufferWriter key)
        {
            IStatement st = GetBeforeStatement();
            if (st == null)
            {
                Location loc = GetFunction().Definition();
                if (loc != null)
                {
                    key.AddTransformed(loc.GetFileID());
                    key.AddTransformed(UInt64.MaxValue - loc.GetOffset());
                }
                else
                {
                    key.AddTransformed(0);
                    key.AddTransformed(0);
                }
            }
            else
            {
                key.AddTransformed(st.FirstSymbolLocation.GetFileID());
                key.AddTransformed(UInt64.MaxValue - st.FirstSymbolLocation.GetOffset());
            }
        }

        #region Sensor Members

        public ulong GetFunctionID()
        {
            if (statementId == Store.Const.CONSTS.WrongID)
                return functionID;
            else
            {
                IStatement st = GetBeforeStatement();
                return st.ImplementsFunctionId;
            }
        }

        public IFunction GetFunction()
        {
            UInt64 funcId = GetFunctionID();
            return sensors.storage.functions.GetFunction(funcId);
        }

        public IStatement GetBeforeStatement()
        {
            return sensors.storage.statements.GetStatement(statementId);
        }

        public UInt64 GetBeforeStatementId()
        {
            return statementId;
        }

        public Kind Kind
        {
            get
            {
                return kind;
            }
            set
            {
                kind = value;
                Save();
            }

        }

        #endregion

        /// <summary>
        /// Добавляет новый датчик в Хранилище
        /// </summary>
        /// <param name="sensors"></param>
        /// <param name="tableSensors">таблица с датчиками</param>
        /// <param name="ID">идентификатор датчика</param>
        /// <param name="functionID">идентификатор функции, в которую вставлен датчик</param>
        /// <param name="kind"></param>
        internal static void Add(Sensors sensors, TableIDNoDuplicate tableSensors, UInt64 ID, UInt64 functionID, Kind kind)
        {
            CheckExistence(tableSensors, ID);

            //Тупо пишем датчик в базу.
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(functionID);
            buffer.Add(Store.Const.CONSTS.WrongID);
            buffer.Add((byte)kind);
            tableSensors.Put(ID, buffer);
        }

        internal static void Add(Sensors sensors, TableIDNoDuplicate tableSensors, UInt64 id, IStatement statement, Store.Kind kind)
        {
            CheckExistence(tableSensors, id);

            //Тупо пишем датчик в базу.
            IBufferWriter buffer = WriterPool.Get();
            if (statement.ImplementsFunction != null)
                buffer.Add(statement.ImplementsFunction.Id);
            else
                buffer.Add(Store.Const.CONSTS.WrongID);
            buffer.Add(statement.Id);
            buffer.Add((byte)kind);
            tableSensors.Put(id, buffer);

            sensors.indexBeforeStatements.Put(statement.Id, id);
        }
    
        private static void CheckExistence(TableIDNoDuplicate tableSensors, UInt64 ID)
        {
            IBufferReader reader;
            if (tableSensors.Get(ID, out reader) != enResult.NOT_FOUND) //Два датчика с одним идентификатором быть не могут. Запрещено
                throw new SensorExistsException("Попытка добавить новый датчик с идентификатором "
                    + ID.ToString(System.Globalization.CultureInfo.InvariantCulture)
                    + " хотя такой датчик с таким идентификатором уже присутствует в базе");

            if (ID > (UInt32)Int32.MaxValue) //Хранилище не умеет работать с такими большими числами. Связано с тем, что номер датчика много где используется как индекс массива
                throw new SensorExistsException("Попытка добавить новый датчик со слишком большим идентификатором "
                    + ID.ToString(System.Globalization.CultureInfo.InvariantCulture));
        }
    }
}
