﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Linq;

using IA.Extensions;

using IOController;
using FileOperations;
using IA;

namespace Store
{
    /// <summary>
    /// Класс, реализующий рабочую директорию (Директория, в которую помещается Хранилище и прочая информация необходимая для работы приложения)
    /// </summary>
    public class WorkDirectory
    {
        /// <summary>
        /// Поддиректории рабочей директории
        /// </summary>
        public enum enSubDirectories
        {
            /// <summary>
            /// Поддиректория для хранения Хранилища
            /// </summary>
            [Description("str")]
            STORAGE,
            /// <summary>
            /// Поддиректория для оригинальных исходных текстов
            /// </summary>
            [Description("src_original")]
            SOURCES_ORIGINAL,
            /// <summary>
            /// 
            /// Поддиректория для очищенных исходных текстов
            /// </summary>
            [Description("src_clear")]
            SOURCES_CLEAR,
            /// <summary>
            /// Поддиректория для лабораторных исходных текстов
            /// </summary>
            [Description("src_lab")]
            SOURCES_LAB
        }



        /// <summary>
        /// Путь до рабочей директории
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// Класс описывает сущность подкаталога в рабочем каталоге IA
        /// </summary>
        public class SubDirectory
        {
            /// <summary>
            /// Создать сущность подкаталога IA по полному пути.
            /// </summary>
            /// <param name="Path"></param>
            public SubDirectory(string Path)
            {
                this.Path = Files.CanonizeFileName(Path);
                if (!this.Path.EndsWith("\\"))
                    this.Path += "\\";
                IsLoaded = false;
            }

            /// <summary>
            /// Полный путь до подкаталога. На конце слеш.
            /// </summary>
            public string Path { get; private set; }

            /// <summary>
            /// Признак выполненной распаковки заархивированного сожержимого подкаталога.
            /// </summary>
            public bool IsLoaded { get; set; }
        }

        /// <summary>
        /// Получить идентификатор подкаталога по имени каталога в ФС
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public enSubDirectories GetSubDirectoryByName(string name)
        {
            enSubDirectories result = enSubDirectories.SOURCES_ORIGINAL;
            IA.Extensions.EnumLoop<WorkDirectory.enSubDirectories>.ForEach(subDir =>
            {
                if (subDir.Description() == name)
                    result = subDir;
            });
            return result;
        }

        /// <summary>
        /// Словарь поддиректорий рабочей директории
        /// </summary>
        private Dictionary<enSubDirectories, SubDirectory> subDirectories = null;

        /// <summary>
        /// Было ли распаковано содержимое подкаталога. 
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public bool IsFolderUnpacked(enSubDirectories folder)
        {
            return subDirectories[folder].IsLoaded;
        }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="path"></param>
        public WorkDirectory(string path)
        {
            try
            {
                //Проверяем переданный путь
                string message;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                if (!WorkDirectory.IsValidPath(path, out message))
                    throw new Exception(message);

                //Получаем путь до самой директории
                this.Path = path;

                //Инициалазируем словарь поддиректорий
                subDirectories = new Dictionary<enSubDirectories, SubDirectory>();

                //Формируем набор поддиректорий
                EnumLoop<enSubDirectories>.ForEach(subDirectory =>
                {
                    //Получаем путь до поддиректории
                    string subDirectoryPath = System.IO.Path.Combine(path, subDirectory.Description());

                    //Добавляем поддиректорию в словарь поддиректорий
                    this.subDirectories.Add(subDirectory, new SubDirectory(subDirectoryPath));

                    //Если директория по указанному пути пуста
                    DirectoryController.Create(subDirectoryPath);
                });
            }
            catch (Exception ex)
            {
                //Формируем ошибку
                throw new Exception("Ошибика при создании рабочей директории.", ex);
            }
        }

        /// <summary>
        /// Получить путь к подкаталогу рабочего каталога. Если архив в подкаталоге после скачивания не был распакован - производится распаковка.
        /// </summary>
        /// <param name="subDirectory">Идентификатор подкаталога</param>
        /// <returns></returns>
        public string GetSubDirectoryPathWithUnpack(enSubDirectories subDirectory)
        {
            SubDirectory sub = subDirectories[subDirectory];
            string subPath = sub.Path;
            string[] filesInSub = null;
            if (!sub.IsLoaded)
            {
                filesInSub = Directory.GetFiles(subPath);
                if (filesInSub.Length == 1)
                    WaitWindow.Show(
                            delegate (WaitWindow window)
                            {
                                string archive = filesInSub[0];
                                //Разархивируем 
                                ArchiveReader.ExtractAll(archive, subPath);

                                //Удаляем локальную копию архива
                                FileController.Remove(archive, true);

                                sub.IsLoaded = true;
                            }, "Распаковка папки " + subDirectory.Description());
                else if (filesInSub.Length == 0 && Directory.GetDirectories(subPath).Length == 0)
                {
                    // такая ситуация возникает только при новой папке (для нового хранилища, например)
                    sub.IsLoaded = true;
                }
            }

            if (!sub.IsLoaded)
            {
                string errorstring;
                if (filesInSub.Length == 0)
                    errorstring = "не содержит файлов.";
                else if (filesInSub.Length > 1)
                    errorstring = "не был распакован ранее и содержит более 1 файла.";
                else errorstring = "не содержит архива для распаковки.";

                Monitor.Log.Error("IA Main", String.Format("Запрошенный каталог <{0}> {1}", subDirectory, errorstring));
            }

            return subPath;
        }

        /// <summary>
        /// Получить путь к подкаталогу рабочего каталога. Никаких действий с содержимым подкаталога не производится.
        /// </summary>
        /// <param name="subDirectory">Идентификатор подкаталога</param>
        /// <returns></returns>
        public string GetSubDirectoryPath(enSubDirectories subDirectory)
        {
            return subDirectories[subDirectory].Path;
        }

        /// <summary>
        /// Получить название каталога с актуальными исходниками.
        /// </summary>
        /// <returns>SOURCES_CLEAR если есть, иначе SOURCES_ORIGINAL</returns>
        public enSubDirectories CurrentSourcesName
        {
            get
            {
                string sourcesClearPath = GetSubDirectoryPath(enSubDirectories.SOURCES_CLEAR);

                if (System.IO.Directory.EnumerateFiles(sourcesClearPath).Any())
                    return enSubDirectories.SOURCES_CLEAR;
                else
                    return enSubDirectories.SOURCES_ORIGINAL;
            }
        }

        /// <summary>
        /// Получить название каталога с актуальными исходниками.
        /// </summary>
        /// <returns>SOURCES_CLEAR если есть, иначе SOURCES_ORIGINAL</returns>
        public string CurrentSourcesDirectory
        {
            get
            {
                return GetSubDirectoryPath(CurrentSourcesName);
            }
        }

        /// <summary>
        /// Получить название каталога с актуальными исходниками.
        /// </summary>
        /// <returns>SOURCES_CLEAR если есть, иначе SOURCES_ORIGINAL</returns>
        public string GetCurrentSourcesDirectoryWithUnpack()
        {
                return GetSubDirectoryPathWithUnpack(CurrentSourcesName);
        }

        /// <summary>
        /// Проверка - может ли указанный путь использоваться как путь до рабочей директории Хранилища
        /// </summary>
        /// <param name="path">Путь</param>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        public static bool IsValidPath(string path, out string message)
        {
            message = "Путь корректен.";

            if (String.IsNullOrWhiteSpace(path))
            {
                message = "Путь не определён.";
                return false;
            }

            if (!DirectoryController.IsExists(path))
            {
                message = "Путь <" + path + "> не существует.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(path))
            {
                message = "Не задана рабочая директория приложения.";
                return false;
            }

            if (!Regex.IsMatch(path, @"^[_a-zA-Z\d\s/\\:.]*$"))
            {
                message = "Путь может содержать только латинские символы, цифры и знак нижнего подчёркивания .";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Получить путь к рабочей директории, выбранной пользователем
        /// </summary>
        /// <param name="defaultPath">Путь для отображения по умолчанию</param>
        /// <returns>Выбранный путь, null - если директория не выбрана.</returns>
        public static string SelectWorkDirectoryPathFromUser(string defaultPath = null)
        {
            try
            {
                //Отображаем предупреждение
                MessageBox.Show(
                    "Внимание! Путь должен содержать только латинские символы!",
                    "Предупреждение",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                );

                bool isValid = false, isEmpty = false;

                using (FolderSelectDialog dialog = new FolderSelectDialog(selectedPath: defaultPath, enableCreateFolder: true))
                {
                    do
                    {
                        //Отображаем диалог выбор директории
                        if (dialog.ShowDialog() != DialogResult.OK)
                            return null;

                        //Если директория не соответствует требованиям
                        string message;
                        if (!(isValid = WorkDirectory.IsValidPath(dialog.SelectedPath, out message)))
                        {
                            //Отображаем ошибку
                            MessageBox.Show(
                                message,
                                "Ошибка",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error
                            );
                        }
                        //Если директория не пуста
                        else if (!(isEmpty = DirectoryController.IsEmpty(dialog.SelectedPath)))
                        {
                            //Отображаем вопрос
                            switch (MessageBox.Show("Директория не пуста. Удалить содержимое?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                case DialogResult.Yes:
                                    DirectoryController.Clear(dialog.SelectedPath);
                                    break;
                                case DialogResult.No:
                                    continue;
                            }
                            break;
                        }

                    } while (!isValid || !isEmpty);

                    //Возвращаем путь к выбранной директории
                    return dialog.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при выборе пути до рабочей директории пользователем.",
                    ex
                );
            }
        }
    }
}
