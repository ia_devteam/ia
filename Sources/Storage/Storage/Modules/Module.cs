﻿using System;
using System.Collections.Generic;
using Store.Table;
using Store.BaseDatatypes;

namespace Store
{
    /// <summary>
    /// Описатель экспорта функции модулем.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface ExportFunction
    {
        /// <summary>
        /// Экспортируемая функция.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Function"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "function")]
        IFunction function { get; }

        /// <summary>
        /// Ординал (номер) функции
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ordinal")]
        UInt32 ordinal { get; }


    }

    /// <summary>
    /// Описатель экспорта функции модулем.
    /// </summary>
    internal sealed class ExportFunction_Impl : DataObject_IDDuplicate, ExportFunction
    {
        IFunction theFunction = null;
        UInt32 theOrdinal = Store.Const.CONSTS.WrongUInt32;

        Functions functions;

        /// <summary>
        /// Экспортируемая функция.
        /// </summary>
        public IFunction function
        {
            get { return theFunction; }
        }

        /// <summary>
        /// Ординал (номер) функции
        /// </summary>
        public UInt32 ordinal
        {
            get { return theOrdinal; }
        }

        internal void SetValue(IFunction function, UInt32 ordinal, UInt64 theModuleID)
        {
            this.theFunction = function;
            this.theOrdinal = ordinal;
            ID = theModuleID;
        }

        /// <summary>
        /// После выполнения этого конструктора вызывается метод Load.
        /// </summary>
        /// <param name="functions"></param>
        internal ExportFunction_Impl(Functions functions)
        {
            this.functions = functions;
        }

        protected override void Load(IBufferReader reader)
        {
            theFunction = functions.GetFunction(reader.GetUInt64());
            theOrdinal = reader.GetUInt32();
        }

        protected override void SaveToBuffer(IBufferWriter buffer)
        {
            buffer.Add(theFunction.Id);
            buffer.Add(theOrdinal);
        }

        protected override void SaveIndexes()
        {
        }
    }


    /// <summary>
    /// Описатель импорта модуля другим модулем.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface ImportModule
    {
        /// <summary>
        /// Импортируемый модуль
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Module"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "module")]
        Module module
        {
            get;
        }
    }


    /// <summary>
    /// Описатель импорта модуля другим модулем.
    /// </summary>
    internal class ImportModule_Impl : DataObject_IDDuplicate, ImportModule
    {
        private Modules modules;

        /// <summary>
        /// Импортируемый модуль
        /// </summary>
        public Module module
        {
            get { return modules.GetModule(ID); }
        }

        internal ImportModule_Impl(Modules modules)
        {
            this.modules = modules;
        }

        internal void SetValue(Module module)
        {
            ID = module.Id;
        }

        protected override void Load(IBufferReader reader)
        {
            ID = reader.GetUInt64();
        }

        protected override void SaveToBuffer(IBufferWriter buffer)
        {
            buffer.Add(ID);
        }

        protected override void SaveIndexes()
        {
        }
    }

    /// <summary>
    /// Описатель импорта функции другой функцией.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface ImportFunction
    {
        /// <summary>
        /// Импортируемая функция
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Function"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "function")]
        IFunction function
        {
            get;
        }
    }


    /// <summary>
    /// Описатель импорта функции другой функцией.
    /// </summary>
    internal class ImportFunction_Impl : DataObject_IDDuplicate, ImportFunction
    {
        IFunction theFunction;
        Functions functions;


        /// <summary>
        /// Импортируемая функция
        /// </summary>
        public IFunction function
        {
            get { return theFunction; }
        }

        internal void SetValue(IFunction function, Module module)
        {
            this.theFunction = function;
            this.ID = module.Id;
        }


        internal ImportFunction_Impl(Functions functions)
        {
            this.functions = functions;
        }

        protected override void Load(IBufferReader reader)
        {
            theFunction = functions.GetFunction(reader.GetUInt64());
        }

        protected override void SaveToBuffer(IBufferWriter buffer)
        {
            buffer.Add(theFunction.Id);
        }

        protected override void SaveIndexes()
        {
        }
    }

    /// <summary>
    /// Классификация модулей по отношению к исходным текстам.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1717:OnlyFlagsEnumsShouldHavePluralNames"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "en")]
    public enum enModuleKinds
    {
        /// <summary>
        /// Модуль является внешним, то есть не реализован в исходных текстах.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "EXTERNAL")]
        EXTERNAL, 
        /// <summary>
        /// Модуль присутствует в составе исходных текстов в бинарном виде.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "BINARY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "INTERNAL")]
        INTERNAL_BINARY,
        /// <summary>
        /// Модуль является результатом компиляции исходных текстов.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "INTERNAL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SOURCE")]
        INTERNAL_SOURCE
    }

    /// <summary>
    /// Описатель модуля.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Module"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface Module
    {
        /// <summary>
        /// Файла, хранящий имя модуля.
        /// </summary>
        IFile File
        {
            get;
        }

        /// <summary>
        /// Идентификатор файла, хранящего имя модуля.
        /// </summary>
        UInt64 FileId
        {
            get;
        }

        /// <summary>
        /// Имя модуля. Например library.dll
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "assembly")]
        string Name
        {
            get;
        }

        /// <summary>
        /// Строгое имя сборки .NET, если модуль реализован сборкой .NET и если оно присвоено
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "assembly")]
        AssemblyStrongName AssemblyStrongName
        {
            get;
        }

        /// <summary>
        /// Тип модуля по классификации модулей по отношению к исходным текстам.
        /// </summary>
        enModuleKinds Kind
        {
            get;
        }

        /// <summary>
        /// Идентификатор модуля.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID")]
        UInt64 Id
        { get; }

        #region Export/Import

        /// <summary>
        /// Перечень функций, экспортируемых модулем.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ExportFunction> ExportsFunctions();

        /// <summary>
        /// Указать, что модуль экспортирует функцию.
        /// </summary>
        /// <param name="function"></param>
        /// <param name="ordinal"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Function"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddExportFunction(IFunction function, UInt32 ordinal);

        /// <summary>
        /// Перечень модулей, импортируемых данным модулем.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ImportModule> ImportsModules();

        /// <summary>
        /// Перечень модулей, импортирующих данный модуль.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Module> ImportedByModules();

        /// <summary>
        /// Указать, что модуль импортирует другой модуль.
        /// </summary>
        /// <param name="module"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Module"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "modulee")]
        void AddImportModule(Module module);

        /// <summary>
        /// Перечень функций, импортируемых данным модулем.
        /// Данный перечень не обновляется автоматически по содержимому модуля и должен быть задан явно.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ImportFunction> ImportsFunctions();

        /// <summary>
        /// Указать, что модуль импортирует функцию.
        /// </summary>
        /// <param name="function"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Function"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddImportFunction(IFunction function);

        #endregion

        #region Implemets
        /// <summary>
        /// Перечень функций, реализованных в данном модуле
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunction> ImplementsFunctions();

        /// <summary>
        /// Указать, что данная функция реализована в данном модуле
        /// Запрещено указывать методы классов: они добавляются вместе с классом
        /// </summary>
        /// <param name="function"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Function")]
        void AddImplementedFunction(IFunction function);

        /// <summary>
        /// Перечень классов, реализованных в данном модуле
        /// </summary>
        /// <returns></returns>
        IEnumerable<Class> ImplementsClasses();
        /// <summary>
        /// Указать, что данный класс реализован в данном модуле
        /// </summary>
        /// <param name="clazz"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "clazz")]
        void AddImplementedClass(Class clazz);

        /// <summary>
        /// Перечень переменных, реализованных в данном модуле.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Variable> ImplementsGlobalVariables();
        /// <summary>
        /// Указать, что данная переменная реализована в данном модуле. Передавать в качестве параметра можно только глобальные переменные, не являющиеся полями классов. 
        /// Для локальных переменных и полей класс будет выдано исключение.
        /// </summary>
        /// <param name="variable"></param>
        void AddImplementedGlobalVariable(Variable variable);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class AssemblyStrongName
    {
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public byte[] PublicKey;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public string Name;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public Version Version;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public string CultureName;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "name: " + Name + " version: " + Version.ToString() + " culture: " + CultureName + " PublicKeyToken: " + PublicKey.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ass"></param>
        public AssemblyStrongName(System.Reflection.Assembly ass)
        {
            if (ass == null)
            {
                return;
            }
            System.Reflection.AssemblyName name = ass.GetName();
                
            PublicKey = name.GetPublicKey();
            Name = name.Name;
            Version = name.Version;
            CultureName = name.CultureInfo.Name;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        internal AssemblyStrongName(IBufferReader reader)
        {
            byte count = reader.GetByte();
            PublicKey = new byte[count];
            for (int i = 0; i < count; i++)
                PublicKey[i] = reader.GetByte();

            Name = reader.GetString();
            Version = new Version(reader.GetInt32(), reader.GetInt32(), reader.GetInt32(), reader.GetInt32());
            CultureName = reader.GetString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        internal void Pack(IBufferWriter writer)
        {
            writer.Add((byte)PublicKey.Length);
            for (int i = 0; i < PublicKey.Length; i++)
                writer.Add(PublicKey[i]);

            writer.Add(Name);
            writer.Add(Version.Major);
            writer.Add(Version.Minor);
            writer.Add(Version.Build);
            writer.Add(Version.Revision);
            writer.Add(CultureName);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    internal class Module_Impl : Store.BaseDatatypes.DataObject_IDNoDuplicate_Immediate, Module
    {
        Modules modules;

        UInt64 theFile;
        string theName;
        AssemblyStrongName theAssemblyStrongName;
        enModuleKinds theKind;

        internal Module_Impl(Modules modules)
        {
            this.modules = modules;
        }

        /// <summary>
        /// Данный метод можно вызывать только при инициализации нового объекта. 
        /// </summary>
        /// <param name="file"></param>
        internal void SetValue(IFile file)
        {
            theFile = file.Id;
            System.AppDomain domain = System.AppDomain.CreateDomain("del_me");

            try
            {
                System.Reflection.Assembly ass = domain.Load(file.FullFileName_Canonized);

                theAssemblyStrongName = new AssemblyStrongName(ass);
                theName = theAssemblyStrongName.Name;
            }
            catch (BadImageFormatException)
            {
                theAssemblyStrongName = null;
                theName = file.NameAndExtension;
            }
            catch (System.IO.FileNotFoundException)
            {
                theAssemblyStrongName = null;
                theName = file.NameAndExtension;
            }
            catch(System.IO.FileLoadException)
            {
                theAssemblyStrongName = null;
                theName = file.NameAndExtension;
            }
            finally
            {
                System.AppDomain.Unload(domain);
            }

            theKind = file.isPlacedInSources() ? enModuleKinds.INTERNAL_BINARY : enModuleKinds.EXTERNAL;

            Save();
        }

        internal void SetValue(AssemblyStrongName strongName)
        {
            theFile = Store.Const.CONSTS.WrongID;
            theName = strongName.Name;
            theAssemblyStrongName = strongName;
            theKind = enModuleKinds.INTERNAL_SOURCE;

            Save();
        }

        internal void SetValueImplemented(string name)
        {
            theFile = Store.Const.CONSTS.WrongID;
            theName = name;
            theAssemblyStrongName = null;
            theKind = enModuleKinds.INTERNAL_SOURCE;

            Save();
        }

        internal void SetValueExternal(string name)
        {
            theFile = Store.Const.CONSTS.WrongID;
            theName = name;
            theAssemblyStrongName = null;
            theKind = enModuleKinds.EXTERNAL;

            Save();
        }

        protected override void SaveIndexes()
        {
            if(theFile != Store.Const.CONSTS.WrongID)
                modules.tableModuleFileName.Put(theFile, buf => buf.Add(theID));

            modules.tableModuleByName.UpdateIndex(null, buf => buf.Add(theName), buf => buf.Add(theID));
            if (theAssemblyStrongName != null)
                modules.tableModuleByStrongName.Put(buf => theAssemblyStrongName.Pack(buf), buf => buf.Add(theID));
        }

        protected override void SaveToBuffer(Store.Table.IBufferWriter buffer)
        {
            buffer.Add(theFile);
            buffer.Add(theName);
            if (theAssemblyStrongName != null)
            {
                buffer.Add(true);
                theAssemblyStrongName.Pack(buffer);
            }
            else
                buffer.Add(false);

            buffer.Add((byte)theKind);
        }

        protected override void Load(Store.Table.IBufferReader reader)
        {
            theFile = reader.GetUInt64();
            theName = reader.GetString();
            bool haveStrongName = false;
            reader.GetBool(ref haveStrongName);
            if(haveStrongName)
                theAssemblyStrongName = new AssemblyStrongName(reader);
            theKind = (enModuleKinds) reader.GetByte();
        }

        protected override string GetLoadErrorString()
        {
            return  "Попытка загрузить модуль с идентификатором %s. Такого модуля в базе не существует";
        }

        #region Module Common Members
        /// <summary>
        /// 
        /// </summary>
        public IFile File
        {
            get { return modules.storage.files.GetFile(theFile); }
        }
        /// <summary>
        /// 
        /// </summary>
        public UInt64 FileId
        {
            get { return theFile; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AssemblyName
        {
            get { return theName; }
        }
        /// <summary>
        /// 
        /// </summary>
        public AssemblyStrongName AssemblyStrongName
        {
            get { return theAssemblyStrongName; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return theName; }
        }
        /// <summary>
        /// 
        /// </summary>
        public enModuleKinds Kind
        {
            get { return theKind; }
        }

        /// <summary>
        /// 
        /// </summary>
        public UInt64 Id
        {
            get { return theID; }
        }

        public override string ToString()
        {
            if (theAssemblyStrongName != null)
                return theAssemblyStrongName.ToString();
            else if (theFile != Store.Const.CONSTS.WrongID)
                return File.FileNameForReports;
            else
                return theName;
        }

        #endregion

        #region Module Import/Export members

        public IEnumerable<ExportFunction> ExportsFunctions()
        {
            return modules.tableModuleExportFunc.GetValueByID(theID);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public void AddExportFunction(IFunction function, UInt32 ordinal)
        {
            //Сохраняем в основную таблицу
            ExportFunction_Impl imp = (ExportFunction_Impl) modules.tableModuleExportFunc.CreateObject();
            imp.SetValue(function, ordinal, Id);
            imp.SaveWithoutRepetition();

            //Сохраняем в индекс
            modules.tableFunctionExported.Put(function.Id, (buffer) => buffer.Add(theID));
        }

        public IEnumerable<ImportModule> ImportsModules()
        {
            return modules.tableModuleImportsModule.GetValueByID(theID);
        }

        /// <summary>
        /// Перечисляет все модули, которые импортируют данный
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Module> ImportedByModules()
        {
            modules.Get_tableModuleImports();

            foreach (IBufferReader buf in modules.tableModuleImports.Get(Id))
                yield return modules.GetModule(buf.GetUInt64());
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public void AddImportModule(Module module)
        {
            ImportModule_Impl imp = modules.tableModuleImportsModule.CreateObject();
            imp.SetValue(module);
            imp.SaveWithoutRepetition();

            if (modules.tableModuleImports != null)
                using (IBufferWriter buf = WriterPool.Get())
                {
                    buf.Add(theID);
                    modules.tableModuleImports.Put(module.Id, buf);
                }
        }

        public IEnumerable<ImportFunction> ImportsFunctions()
        {
            return modules.tableModuleImportsFunction.GetValueByID(theID);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public void AddImportFunction(IFunction function)
        {
            ImportFunction_Impl imp = modules.tableModuleImportsFunction.CreateObject();
            imp.SetValue(function, this);
            imp.SaveWithoutRepetition();

            if (modules.tableFunctionImports != null)
                using (IBufferWriter buf = WriterPool.Get())
                {
                    buf.Add(theID);
                    modules.tableFunctionImports.Put(function.Id, buf);
                }
        }

        #endregion


        public IEnumerable<IFunction> ImplementsFunctions()
        {
            foreach (UInt64 Id in modules.tableImplementsFunctions.GetValueByKey(theID))
                yield return modules.storage.functions.GetFunction(Id);
        }

        public void AddImplementedFunction(IFunction function)
        {
            if (function != null)
            {
                if (function is IMethod)
                    throw new Store.Const.StorageException("Запрещено для методов указывать, в каком модуле они реализованы");

                if (modules.TableFunctionImplementedInModule.GetValueByKey(function.Id) != Store.Const.CONSTS.WrongID)
                    throw new Store.Const.StorageException("Функция может быть реализована только в одном модуле");

                SetMethodModule(modules, theID, function);
            }
        }

        public IEnumerable<Class> ImplementsClasses()
        {
            foreach (UInt64 Id in modules.tableImplementsClasses.GetValueByKey(theID))
                yield return modules.storage.classes.GetClass(Id);
        }

        public void AddImplementedClass(Class clazz)
        {
            if (clazz != null)
            {
                if (modules.TableClassImplementedInModule.GetValueByKey(clazz.Id) != Store.Const.CONSTS.WrongID)
                    throw new Store.Const.StorageException("Класс может быть реализован только в одном модуле");

                //Заносим информацию о соответствии класса модулю
                modules.TableClassImplementedInModule.Put(clazz.Id, theID);
                modules.tableImplementsClasses.PutWithoutRepetition(theID, clazz.Id);

                //Заносим информацию о соответствии методов класса модулю
                foreach (IMethod method in clazz.EnumerateMethods())
                {
                    SetMethodModule(modules, theID, method);
                }

                //Заносим информацию о соответствии полей класса модулю
                foreach (UInt64 fieldId in clazz.EnumerateFieldIds())
                {
                    modules.TableVariableImplementedInModule.Put(fieldId, theID);
                    modules.tableImplementsVariables.PutWithRepetition(theID, fieldId);
                }
            }
        }

        public IEnumerable<Variable> ImplementsGlobalVariables()
        {
            foreach (UInt64 Id in modules.tableImplementsVariables.GetValueByKey(theID))
                yield return modules.storage.variables.GetVariable(Id);
        }

        public void AddImplementedGlobalVariable(Variable variable)
        {
            if (variable != null)
            {
                if (modules.TableVariableImplementedInModule.GetValueByKey(variable.Id) != Store.Const.CONSTS.WrongID)
                    throw new Store.Const.StorageException("Переменная может быть реализована только в одном модуле");

                if (!variable.isGlobal || variable is Field)
                    throw new Store.Const.StorageException("Только глобальная переменная, не являющаяся полем, может быть реализована в одном модуле");

                modules.TableVariableImplementedInModule.Put(variable.Id, theID);
                modules.tableImplementsVariables.PutWithoutRepetition(theID, variable.Id);
            }
        }

        static internal void SetMethodModuleWithCheck(Modules modules, UInt64 moduleId, IFunction function)
        {
            if (moduleId != Store.Const.CONSTS.WrongID)
            {
                SetMethodModule(modules, moduleId, function);
            }
        }

        private static void SetMethodModule(Modules modules, UInt64 moduleId, IFunction function)
        {
            modules.TableFunctionImplementedInModule.Put(function.Id, moduleId);
            modules.tableImplementsFunctions.PutWithRepetition(moduleId, function.Id);

            foreach (Variable var in function.EnumerateDefinedVariables())
                SetVariableModule(modules, var, moduleId);
        }

        internal static void SetFieldModule(Modules modules, UInt64 moduleId, UInt64 fieldId)
        {
            if (moduleId != Store.Const.CONSTS.WrongID)
            {
                modules.TableVariableImplementedInModule.Put(fieldId, moduleId);
                modules.tableImplementsVariables.PutWithRepetition(moduleId, fieldId);
            }
        }

        internal static void SetVariableModuleWithCheck(Modules modules, Variable variable, IFunction function)
        {
            //Для функции модуль еще не указан
            UInt64 functionModuleId = function.ImplementedInModuleId;
            if (functionModuleId == Store.Const.CONSTS.WrongID)
                return;

            SetVariableModule(modules, variable, functionModuleId);
        }

        private static void SetVariableModule(Modules modules, Variable variable, UInt64 functionModuleId)
        {
            //Переменная может быть определена только в одном модуле
            UInt64 variableModuleId = variable.ImplementedInModuleId;

            if (variableModuleId != Store.Const.CONSTS.WrongID)
            {
                if (variableModuleId != functionModuleId)
                    throw new Store.Const.StorageException("Попытка указать определение одной переменной в разных модулях");

            //Соответствие переменной модулю уже задано. Дублировать не требуется.
            }
            else
            {
                modules.TableVariableImplementedInModule.Put(variable.Id, functionModuleId);
                modules.tableImplementsVariables.PutWithRepetition(functionModuleId, variable.Id);
            }
        }
    }
}
