﻿using System;
using System.Collections.Generic;
using Store.BaseDatatypes;
using Store.Table;

namespace Store
{
    /// <summary>
    /// Головной класс для работы с модулями.
    /// </summary>
    public sealed class Modules
    {
        //Имена таблиц
        const string TableName_modules = "Modules";
        const string TableName_moduleFileName = "iModuleFileNames";
        const string TableName_moduleByName = "iModuleNames";
        const string TableName_moduleByStrongName = "iModuleStrongNames";
        const string TableName_moduleExportFunc = "ModuleExportFunc";
        const string TableName_functionsExported = "iModuleFunctionExported";
        const string TableName_moduleImportsModule = "ModuleImportsModule";
        const string TableName_modulesImported = "iModuleModulesImported";
        const string TableName_moduleImportsFuc = "ModuleImportFunc";
        const string TableName_functionsImportes = "iModuleFunctionImported";
        const string TableName_moduleImplementsFunctions = "ModuleImplementsFunc";
        const string TableName_functionImplementedInModule = "FuncImplementedInModule";
        const string TableName_moduleImplementsClasses = "ModuleImplementsClasses";
        const string TableName_classImplementedInModule = "ClassImplementedInModule";
        const string TableName_moduleImplementsVariables = "ModuleImplementsVariables";
        const string TableName_variableImplementedInModule = "VariableImplementedInModule";

        internal Storage storage;

        //Основные таблицы
        TableIDNoDuplicate_with_WeakRefTable<Module_Impl> tableModules;

        /// <summary>
        /// Ключ - идентификатор модуля. Значение - идентификатор экспортируемой модулем функции.
        /// </summary>
        internal TableIDDuplicate_MainTable<ExportFunction_Impl> tableModuleExportFunc;

        /// <summary>
        /// Ключ - идентификатор модуля. Значение - ImportModule.
        /// </summary>
        internal TableIDDuplicate_MainTable<ImportModule_Impl> tableModuleImportsModule;

        /// <summary>
        /// Ключ - идентификатор модуля. Значение - ImportFunction.
        /// </summary>
        internal TableIDDuplicate_MainTable<ImportFunction_Impl> tableModuleImportsFunction;

        /// <summary>
        /// Соответствие между модулем и реализуемыми им функциями.
        /// Должен устанавливаться использующей стороной для обычных функций
        /// Для методов устанавливается либо при назначении модуля классу, либо при создании метода, когда классу модуль уже назначен
        /// Для полей устанавливается либо при назначении модуля классу, либо при создании поля, когда классу модуль уже назначен
        /// Ключ - идентификатор модуля
        /// Значение - идентификатор функции.
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableImplementsFunctions;

        /// <summary>
        /// Соответствие между модулем и реализуемыми им классами.
        /// Ключ - идентификатор модуля
        /// Значение - идентификатор класса
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableImplementsClasses;

        /// <summary>
        /// Соответствие между модулем и реализуемыми им переменными.
        /// Ключ - идентификатор модуля
        /// Значение - идентификатор переменной
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableImplementsVariables;

        
        //Индексные таблицы

        /// <summary>
        /// Ключ - идентификатор файла с модулем (UInt64), значение - идентификатор модуля (UInt64).
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueBuffer_NoDuplicate tableModuleFileName;

        /// <summary>
        /// Имя модуля, значение - идентификатор модуля (UInt64).
        /// </summary>
        internal Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates tableModuleByName;

        /// <summary>
        /// Строгое имя сборки, значение - идентификатор модуля (UInt64).
        /// </summary>
        internal Index_DelayCreation_KeyBuffer_ValueBuffer_NoDuplicate tableModuleByStrongName;

        /// <summary>
        /// Ключ - идентификатор экспортируемой модулем функции. Значение - идентификатор модуля.
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueBuffer_NoDuplicate tableFunctionExported;

        /// <summary>
        /// Ключ - идентификатор импортируемого модулем модуля. Значение - идентификатор модуля.
        /// </summary>
        internal TableIDDuplicate tableModuleImports;

        /// <summary>
        /// Ключ - идентификатор импортируемой модулем функции. Значение - идентификатор модуля.
        /// </summary>
        internal TableIDDuplicate tableFunctionImports;

        /// <summary>
        /// Для функции определяет, в каком модуле она реализована
        /// Ключ - идентификатор функции
        /// Значение - идентификатор модуля
        /// </summary>
        internal Table_KeyId_ValueId_NoDublicates TableFunctionImplementedInModule;

        /// <summary>
        /// Для класса определяет, в каком модуле он реализован
        /// Ключ - идентификатор класса
        /// Значение - идентификатор модуля
        /// </summary>
        internal Table_KeyId_ValueId_NoDublicates TableClassImplementedInModule;

        /// <summary>
        /// Для переменной определяет, в каком модуле она реализована
        /// Ключ - идентификатор переменной
        /// Значение - идентификатор модуля
        /// </summary>
        internal Table_KeyId_ValueId_NoDublicates TableVariableImplementedInModule;


        #region Generate Modules

        /// <summary>
        /// Создавать экземпляры Files самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal Modules(Storage storage)
        {
            this.storage = storage;

            tableModules = new TableIDNoDuplicate_with_WeakRefTable<Module_Impl>(storage, TableName_modules, 
                                                                                     () => new Module_Impl(this));

            tableModuleFileName = new Index_DelayCreation_KeyId_ValueBuffer_NoDuplicate(storage, TableName_moduleFileName, EnumerateTableModuleFileName);

            tableModuleByName = new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates(storage, TableName_moduleByName, EnumerateModuleNames);

            tableModuleByStrongName = new Index_DelayCreation_KeyBuffer_ValueBuffer_NoDuplicate(storage, TableName_moduleByStrongName, EnumerateModuleStrongNames);

            tableModuleExportFunc = new TableIDDuplicate_MainTable<ExportFunction_Impl>(storage, TableName_moduleExportFunc,
                                                                                () => new ExportFunction_Impl(storage.functions));

            tableFunctionExported = new Index_DelayCreation_KeyId_ValueBuffer_NoDuplicate(storage, TableName_functionsExported,
                                                                            EnumerateTableFunctionExported);

            tableModuleImportsModule = new TableIDDuplicate_MainTable<ImportModule_Impl>(storage, TableName_moduleImportsModule,
                                                                                () => new ImportModule_Impl(this));
            tableModuleImports = storage.TryOpenTableIDDuplicates(TableName_modulesImported);

            tableModuleImportsFunction = new TableIDDuplicate_MainTable<ImportFunction_Impl>(storage, TableName_moduleImportsFuc,
                                                                                () => new ImportFunction_Impl(storage.functions));
            tableFunctionImports = storage.TryOpenTableIDDuplicates(TableName_functionsImportes);
        
            tableImplementsFunctions = new Table_KeyId_ValueId_Dublicates(storage, TableName_moduleImplementsFunctions);

            tableImplementsClasses = new Table_KeyId_ValueId_Dublicates(storage, TableName_moduleImplementsClasses);

            tableImplementsVariables = new Table_KeyId_ValueId_Dublicates(storage, TableName_moduleImplementsVariables);

            TableClassImplementedInModule = new Table_KeyId_ValueId_NoDublicates(storage, TableName_classImplementedInModule);

            TableFunctionImplementedInModule = new Table_KeyId_ValueId_NoDublicates(storage, TableName_functionImplementedInModule);

            TableVariableImplementedInModule = new Table_KeyId_ValueId_NoDublicates(storage, TableName_variableImplementedInModule);
        }

        /// <summary>
        /// Перебирает все элементы, которые должны оказаться в индексе tableModuleFileName.
        /// </summary>
        /// <returns></returns>
        IEnumerable<KeyValuePair<UInt64, IBufferWriter>> EnumerateTableModuleFileName(IBufferWriter buffer)
        {
            foreach (Module module in EnumerateModules())
            {
                buffer.Add(module.Id);
                yield return new KeyValuePair<UInt64, IBufferWriter> (module.FileId, buffer);
            }
        }

        /// <summary>
        /// Перебирает все элементы, которые должны оказаться в индексе tableFunctionExported.
        /// </summary>
        /// <returns></returns>
        IEnumerable<KeyValuePair<UInt64, IBufferWriter>> EnumerateTableFunctionExported(IBufferWriter buffer)
        {
            foreach (KeyValuePair<UInt64, ExportFunction_Impl> pair in tableModuleExportFunc.EnumerateEveryone())
            {
                UInt64 funcID = pair.Value.function.Id;
                buffer.Add(pair.Key);
                yield return new KeyValuePair<UInt64, IBufferWriter>(funcID, buffer);
            }
        }

        IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult> EnumerateModuleNames(IBufferWriter key, IBufferWriter value)
        {
            foreach (Module module in EnumerateModules())
            {
                key.Add(module.Name);
                value.Add(module.Id);
                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, null);
            }
        }

        IEnumerable<KeyValuePair<IBufferWriter, IBufferWriter>> EnumerateModuleStrongNames(IBufferWriter key, IBufferWriter value)
        {
            foreach (Module module in EnumerateModules())
            {
                if (module.AssemblyStrongName != null)
                {
                    module.AssemblyStrongName.Pack(key);
                    value.Add(module.Id);
                    yield return new KeyValuePair<IBufferWriter, IBufferWriter>(key, value);
                }
            }
        }

        #endregion

        /// <summary>
        /// Перечислить модули.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Module> EnumerateModules()
        {
            foreach (Module_Impl module in tableModules.EnumerateEveryone())
                yield return module;
        }

        /// <summary>
        /// Получить модуль по его идентификатору
        /// </summary>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        public Module GetModule(UInt64 moduleId)
        {
            return tableModules.Load(moduleId);
        }


        /// <summary>
        /// Найти модуль по его файлу.
        /// </summary>
        /// <param name="file"></param>
        /// <returns>Модуль или null, если ничего не найдено.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public Module FindModule(IFile file)
        {
            return FindModule(file.Id);
        }

        /// <summary>
        /// Найти модуль по его файлу.
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns>Модуль или null, если ничего не найдено.</returns>
        public Module FindModule(UInt64 fileId)
        {
            IBufferReader buffer = tableModuleFileName.GetValueByID(fileId);
            if (buffer != null)
                return GetModule(buffer.GetUInt64());
            else
                return null;
        }

        /// <summary>
        /// Найти модуль по полному имени его файла.
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <returns>Модуль или null, если ничего не найдено.</returns>
        public Module FindModule(string fullFileName)
        {
            IFile file = storage.files.Find(fullFileName);
            if (file == null)
                return null;
            else
                return FindModule(file);
        }

        /// <summary>
        /// Найти модуль по его имени, без указания файла.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IEnumerable<Module> FindModuleByName(string name)
        {
            foreach (IBufferReader reader in tableModuleByName.FindValuesByKey(buf => buf.Add(name)))
                yield return GetModule(reader.GetUInt64());
        }


        /// <summary>
        /// Найти сборку .Net со строгим именем
        /// </summary>
        /// <param name="strongName"></param>
        /// <returns></returns>
        public Module FindModule(AssemblyStrongName strongName)
        {
            IBufferReader reader = tableModuleByStrongName.GetValueByKey(buf => strongName.Pack(buf));
            if (reader != null)
                return GetModule(reader.GetUInt64());
            else
                return null;
        }


        /// <summary>
        /// Найти модуль, реализованный в исходных текстах
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IEnumerable<Module> FindImplementedModule(string name)
        {
            foreach (IBufferReader reader in tableModuleByName.FindValuesByKey(buf => buf.Add(name)))
            {
                Module module = GetModule(reader.GetUInt64());
                if(module.Kind == enModuleKinds.INTERNAL_SOURCE)
                    yield return module;
            }
        }

        /// <summary>
        /// Добавить модуль, реализованный в исходных текстах.
        /// </summary>
        /// <param name="name">Имя модуля. Здесь должно быть указано имя файла, в который помещаются результаты компиляции исходных текстов, составляющих данный модуль. Например, <c>library.dll</c></param>
        /// <returns></returns>
        public Module AddImplementedModule(string name)
        {
            Module_Impl res = (Module_Impl) tableModules.Add();
            res.SetValueImplemented(name);
            return res;
        }

        /// <summary>
        /// Добавить сборку, реализованную в исходных текстах.
        /// При вызове данного метода подразумевается, что модулем является сборка .Net, имеющая строгое имя. Если сборка .nEt не имеет строгого имени, то вместо этого метода следует вызывать метод 
        /// <see cref="Store.Modules.AddImplementedModule(string)"/>
        /// </summary>
        /// <param name="strongName">Строгое имя сборки</param>
        /// <returns></returns>
        public Module AddImplementedModule(AssemblyStrongName strongName)
        {
            Module_Impl res = (Module_Impl)tableModules.Add();
            res.SetValue(strongName);
            return res;
        }

        /// <summary>
        /// Добавить модуль, реализованный в указанном файле
        /// </summary>
        /// <param name="fileId">Идентификатор файла.</param>
        /// <returns></returns>
        public Module AddModule(UInt64 fileId)
        {
            return AddModule(storage.files.GetFile(fileId));
        }

        /// <summary>
        /// Добавить модуль, реализованный в указанном файле
        /// </summary>
        /// <param name="file">Файл, в котором реализован модуль</param>
        /// <returns></returns>
        public Module AddModule(IFile file)
        {
            Module_Impl res = (Module_Impl)tableModules.Add();
            res.SetValue(file);
            return res;
        }

        /// <summary>
        /// Добавить модуль, реализованный в указанном файле
        /// </summary>
        /// <param name="fullFileName">Полное имя файла</param>
        /// <param name="fileKind">Является ли файл модуля внешним по отношению к исходным текстам? То есть требуется ли от полного имени отрезать префикс</param>
        /// <returns></returns>
        public Module AddModule(string fullFileName, enFileKindForAdd fileKind)
        {
            return AddModule(storage.files.FindOrGenerate(fullFileName, fileKind));
        }

        /// <summary>
        /// Добавить модуль, не реализованный в исходных текстах и файл для которого отсутствует.
        /// </summary>
        /// <param name="name">Имя модуля. Например library.dll</param>
        /// <returns></returns>
        public Module AddExternalModule(string name)
        {
            Module_Impl res = (Module_Impl)tableModules.Add();
            res.SetValueExternal(name);
            return res;
        }


        /// <summary>
        /// Гарантированно загрузить tableModuleImports.
        /// Если при этом приходится создавать новую таблицу, сразу производится построекние индекса.
        /// </summary>
        /// <returns></returns>
        internal TableIDDuplicate Get_tableModuleImports()
        {
            if (tableModuleImports == null)
            {
                tableModuleImports = storage.GetTableIDDuplicate(TableName_modulesImported);
                foreach (KeyValuePair<UInt64, ImportModule_Impl> pair in tableModuleImportsModule.EnumerateEveryone())
                {
                    UInt64 modID = pair.Value.module.Id;
                    using (IBufferWriter buffer = WriterPool.Get())
                    {
                        buffer.Add(pair.Key);
                        tableModuleImports.Put(modID, buffer);
                    }
                }
            }

            return tableModuleImports;
        }

        /// <summary>
        /// Гарантированно загрузить tableFunctionImports.
        /// Если при этом приходится создавать новую таблицу, сразу производится построекние индекса.
        /// </summary>
        /// <returns></returns>
        internal TableIDDuplicate Get_tableFunctionImports()
        {
            if (tableFunctionImports == null)
            {
                tableFunctionImports = storage.GetTableIDDuplicate(TableName_functionsImportes);
                foreach (KeyValuePair<UInt64, ImportFunction_Impl> pair in tableModuleImportsFunction.EnumerateEveryone())
                {
                    UInt64 funcID = pair.Value.function.Id;
                    using (IBufferWriter buffer = WriterPool.Get())
                    {
                        buffer.Add(pair.Key);
                        tableFunctionImports.Put(funcID, buffer);
                    }
                }
            }

            return tableFunctionImports;
        }
    }
}
