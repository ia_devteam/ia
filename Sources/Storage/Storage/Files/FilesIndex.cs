﻿using System;
using System.Collections.Generic;
using Store.Table;
using Store.BaseDatatypes;

namespace Store
{
    /// <summary>
    /// Представляет индекс идентификаторов по всем файлам
    /// </summary>
    public sealed class FilesIndex
    {
        /// <summary>
        /// Структура описывает вхождение идентификатора в файл
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public struct Occurrence 
        {
            internal UInt64 theFileID;
            internal UInt64 theOffset;
            internal Files files;
            internal Locations locations;
            internal bool isLineRowCalculated;
            internal UInt64 theLine;
            internal UInt64 theCol;

            internal Occurrence(UInt64 theFileID, UInt64 theOffset, Files files, Locations locations)
            {
                this.isLineRowCalculated = false;
                this.theFileID = theFileID;
                this.theOffset = theOffset;
                this.files = files;
                this.locations = locations;
                this.theCol = 0;
                this.theLine = 0;
            }

            /// <summary>
            /// Идентификатор файла, в котором обнаружено вхождение идентификатора
            /// </summary>
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID")]
            public UInt64 FileID
            { get { return theFileID; } }

            /// <summary>
            /// Смещение, по которому обнаружено вхождение идентификатора
            /// </summary>
            public UInt64 Offset
            { get {return theOffset;}}

            /// <summary>
            /// Строка, на которой обнаружено вхождение идентификатора
            /// </summary>
            public UInt64 Line
            {
                get
                {
                    if (!isLineRowCalculated)
                        locations.CalculateLineColumn(theFileID, theOffset, out theLine, out theCol);

                    return theLine;
                }
            }

            /// <summary>
            /// Столбец, в котором обнаружено вхождение идентификатора
            /// </summary>
            public UInt64 Column
            {
                get
                {
                    if (!isLineRowCalculated)
                        locations.CalculateLineColumn(theFileID, theOffset, out theLine, out theCol);

                    return theCol;
                }
            }

            /// <summary>
            /// Файл, в котором обнаружено вхождение идентификатора
            /// </summary>
            public IFile File
            {
                get { return files.GetFile(theFileID);}
            }
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public override int GetHashCode()
            {
                return (int) (theFileID ^ theOffset);
            }     
            /// <summary>
            /// 
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public override bool Equals(object obj)
            {
                if (obj is Occurrence)
                {
                    Occurrence occ = (Occurrence)obj;
                    return theFileID == occ.theFileID
                        && theOffset == occ.theOffset;
                }
                else
                    return false;
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="occurrence1"></param>
            /// <param name="occurrence2"></param>
            /// <returns></returns>
            public static bool operator ==(Occurrence occurrence1, Occurrence occurrence2)
            {
                return occurrence1.Equals(occurrence2);
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="occurrence1"></param>
            /// <param name="occurrence2"></param>
            /// <returns></returns>
            public static bool operator !=(Occurrence occurrence1, Occurrence occurrence2)
            {
                return ! occurrence1.Equals(occurrence2);
            }
        }

        const string tabelIndexWithRegisterName = "FileIdentifierIndexWithRegister";
        const string tabelIndexWithRegisterName_SearchWithoutRegister = "FileIdentifierIndexWithRegister_SearchWithoutRegister";
        const string tabelIndexWithOutRegisterName = "FileIdentifierIndexWithoutRegister";

        Storage storage;
        Locations locations;

        /// <summary>
        /// Таблица всех вхождений идентификатора в файлах, поиск в которых выполняется с учетом регистра
        /// </summary>
        TableIndexedDuplicates_MainTable tableIndexWithRegister;
        TableIndexedDuplicates_MainTable tableIndexWithRegister_SearchWithoutRegister;
        /// <summary>
        /// Таблица всех вхождений идентификатора в файлах, поиск в которых выполняется без учета регистра
        /// </summary>
        TableIndexedDuplicates_MainTable tableIndexWithoutRegister;
        
        /// <summary>
        /// Создавать экземпляры Files самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal FilesIndex(Storage storage)
        {
            this.storage = storage;
            locations = storage.locationsForSingle("empty_locations");
            tableIndexWithRegister = new TableIndexedDuplicates_MainTable(storage, tabelIndexWithRegisterName);
            tableIndexWithRegister_SearchWithoutRegister = new TableIndexedDuplicates_MainTable(storage, tabelIndexWithRegisterName_SearchWithoutRegister);
            tableIndexWithoutRegister = new TableIndexedDuplicates_MainTable(storage, tabelIndexWithOutRegisterName);
        }

        /// <summary>
        /// Удалить все содержимое индекса
        /// </summary>
        public void Clear()
        {
            tableIndexWithRegister.Clear();
            tableIndexWithRegister_SearchWithoutRegister.Clear();
            tableIndexWithoutRegister.Clear();
        }

        /// <summary>
        /// Добавить запись в индекс
        /// </summary>
        /// <param name="file">Файл, в котором находится идентификатор</param>
        /// <param name="identifier">Идентификатор</param>
        /// <param name="offset">Смещение от начала файла</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public void Add(IFile file, string identifier, UInt64 offset)
        {
            if (file.fileType.Contains().HasFlag(enTypeOfContents.DONTCONSIDERREGISTER))
                tableIndexWithoutRegister.Put(buffer => buffer.Add(identifier.ToLowerInvariant()),
                                buffer => { buffer.Add(file.Id); buffer.Add(offset); });
            else
            {
                tableIndexWithRegister.Put(buffer => buffer.Add(identifier),
                                buffer => { buffer.Add(file.Id); buffer.Add(offset); });

                tableIndexWithRegister_SearchWithoutRegister.Put(buffer => buffer.Add(identifier.ToLowerInvariant()),
                                buffer => { buffer.Add(file.Id); buffer.Add(offset); });
            }
        }

        /// <summary>
        /// Получить все вхождения указанного идентификатора
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public IEnumerable<Occurrence> Find(string identifier)
        {
            foreach (IBufferReader reader in tableIndexWithRegister.FindValuesByKey( buffer => buffer.Add(identifier)))
            {
                Occurrence occ = new Occurrence(reader.GetUInt64(), reader.GetUInt64(), storage.files, locations);

                yield return occ;
            }

            foreach (IBufferReader reader in tableIndexWithoutRegister.FindValuesByKey(buffer => buffer.Add(identifier.ToLowerInvariant())))
            {
                Occurrence occ = new Occurrence(reader.GetUInt64(), reader.GetUInt64(), storage.files, locations);

                yield return occ;
            }
        }


        /// <summary>
        /// Получить все вхождения указанного идентификатора. При поиске даже для файлов, в которых регистр имеет значение, искать вне зависимости от регистра
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public IEnumerable<Occurrence> FindWithoutRegister(string identifier)
        {
            foreach (IBufferReader reader in tableIndexWithRegister_SearchWithoutRegister.FindValuesByKey(buffer => buffer.Add(identifier.ToLowerInvariant())))
            {
                Occurrence occ = new Occurrence(reader.GetUInt64(), reader.GetUInt64(), storage.files, locations);

                yield return occ;
            }

            foreach (IBufferReader reader in tableIndexWithoutRegister.FindValuesByKey(buffer => buffer.Add(identifier.ToLowerInvariant())))
            {
                Occurrence occ = new Occurrence(reader.GetUInt64(), reader.GetUInt64(), storage.files, locations);

                yield return occ;
            }
        }

        /// <summary>
        /// Получить все индексы. Сначала выдаются индексы, полученные с учётом регистра, затем - без учёта. Единственное разумное назначение: тестирование.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, Occurrence>> Enumerate()
        {
            foreach (KeyValuePair<IBufferReader, IBufferReader> el in tableIndexWithRegister.Enumerate())
                yield return new KeyValuePair<string, Occurrence>(el.Key.GetString(), new Occurrence(el.Value.GetUInt64(), el.Value.GetUInt64(), storage.files, locations));

            foreach (KeyValuePair<IBufferReader, IBufferReader> el in tableIndexWithoutRegister.Enumerate())
                yield return new KeyValuePair<string, Occurrence>(el.Key.GetString(), new Occurrence(el.Value.GetUInt64(), el.Value.GetUInt64(), storage.files, locations));
        }
    }
}
