﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace Store
{
    /// <summary>
    /// Тип содержимого, хранимого в файле
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "en"), Flags]
    public enum enTypeOfContents : ulong
    {
        /// <summary>
        /// Файл содержит данные
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "DATA")]
        [Description("Файл с данными")]
        DATA = 1,
        /// <summary>
        /// Файл содержит исполняемый код
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "EXECUTABLE")]
        [Description("Исполняемый файл")]
        EXECUTABLE = 2,
        /// <summary>
        /// Файл содержит исходные коды в текстовом виде
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "TEXTSOURCES")]
        [Description("Исходные тексты в текстовом формате")]
        TEXTSOURCES = 4,
        /// <summary>
        /// Файл содержит исходные коды в бинарном виде
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "BINARYSOURCES")]
        [Description("Исходные тексты в бинарном формате")]
        BINARYSOURCES = 8,
        /// <summary>
        /// Содержимое этого файла такого, что при его индексировании не следует учитывать регистр
        /// </summary>
        [Description("Индексация без учета регистра содержимого")]
        DONTCONSIDERREGISTER = 16,
        /// <summary>
        /// Как правило, файл является избыточным
        /// </summary>
        [Description("Обычно является избыточным")]
        REDUNDANT = 32
    }

    /// <summary>
    /// Перечень поддерживаемых языков программирования
    /// </summary>
    [Flags]
    public enum enLanguage : ulong
    {
        None = 0,
        C = 1 << 0,
        Cpp = 1 << 1,
        C_Sharp = 1 << 2,
        Pascal = 1 << 3,
        Delphi = 1 << 4,
        VisualBasic = 1 << 5,
        VisualBasicScript = 1 << 6,
        JavaScript = 1 << 7,
        VBA = 1 << 8,
        PHP = 1 << 9,
        Python = 1 << 10,
        Perl = 1 << 11,
        Java = 1 << 26,
        Assembler86 = 1 << 43
        /*
        Python = 0x400,
        Ruby = 0x800,
        Perl = 0x1000,
        Ada = 0x2000,
        Basic = 0x4000,
        Cobol = 0x8000,
        Fortran = 0x10000,
        Modula2 = 0x20000,
        HAL_S = 0x40000,
        PL_1 = 0x100000,
        Bat = 0x200000,
        Groovy = 0x400000,
        LISP = 0x800000,
        Haskell = 0x1000000,
        F_Sharp = 0x2000000,
        ObjectPascal = 0x8000000,
        ActionScript = 0x10000000,
        Lua = 0x20000000,
        Smalltalk = 0x40000000,
        FlashActionScript = 0x80000000,
        Tcl = 0x100000000,
        Forth = 0x200000000,
        PostScript = 0x400000000,
        AssemblerX86 = 0x800000000,
        Eiffel = 0x1000000000,
        Simula = 0x2000000000,
        Objective_C = 0x4000000000,
        Prolog = 0x8000000000,
        Sh_script = 0x10000000000,
        AppleScript = 0x20000000000,
        Cocoa = 0x40000000000,
         */
    }
    /// <summary>
    /// Типы файла. Развернутой описание каждого типа файла хранится в отдельном XML-файле.
    /// </summary>
    public class FileType : IComparable<FileType>, IEquatable<FileType>
    {

        static IA.Sql.DatabaseConnections.SignatureFileDatabaseConnection sigFile = new IA.Sql.DatabaseConnections.SignatureFileDatabaseConnection(); //все типы из одной БД брать надо и никак иначе.

        /// <summary>
        /// Тип файла
        /// </summary>
        /// <param name="shortDescription"></param>
        public FileType(string shortDescription)
        {
            typeDescription = "";
            contains = 0;
            extensions = null;
            shortTypeDescription = shortDescription;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        override public string ToString()
        {
            string res = "";
            foreach (string ext in Extensions())
            {
                res += ext + " ";
            }
            if (!String.IsNullOrEmpty(ShortTypeDescription()))
            {
                return "(" + res + ") " + ShortTypeDescription();
            }
            return TypeDescription();
        }

        /// <summary>
        /// Используется в случае, если уже получен объект FileType, но этот объект пустой. Применяется при загрузке ссылки на тип из Хранилища - достаточно загрузить shortDescription и FileType сам загрузит всё остальное.
        /// </summary>
        /// <param name="shortDescription"></param>
        public void FromString(string shortDescription)
        {
            shortTypeDescription = shortDescription;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type1"></param>
        /// <param name="type2"></param>
        /// <returns></returns>
        static public bool operator ==(FileType type1, FileType type2)
        {
            if (object.Equals(type1, null) || object.Equals(type2, null))
                return false;
            else
                return type1.shortTypeDescription == type2.shortTypeDescription;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type1"></param>
        /// <param name="type2"></param>
        /// <returns></returns>
        static public bool operator !=(FileType type1, FileType type2)
        {
            return !(type1 == type2);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return shortTypeDescription.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        string shortTypeDescription;

        /// <summary>
        /// Возвращает короткое описание данного типа.
        /// Короткое описание типа является уникальным идентификатором объекта.
        /// </summary>
        /// <returns></returns>
        public string ShortTypeDescription()
        {
            return shortTypeDescription;
        }

        string typeDescription;
        /// <summary>
        /// Возвращает полное описание данного типа.
        /// </summary>
        /// <returns></returns>
        public string TypeDescription()
        {
            if (!String.IsNullOrEmpty(shortTypeDescription))
            {
                if (string.IsNullOrEmpty(typeDescription))
                    typeDescription = sigFile.FileFormat.Where(x => x[4] == shortTypeDescription).Select(x => x[5]).First();
            }
            return typeDescription;
        }
        /// <summary>
        /// 
        /// </summary>
        public static Dictionary<string, enTypeOfContents> _contains = new Dictionary<string, enTypeOfContents>();

        enTypeOfContents contains;
        /// <summary>
        /// Возвращает перечисление, указывающее тип содержимого файлов
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        public enTypeOfContents Contains()
        {
            enTypeOfContents type;
            if (!_contains.TryGetValue(shortTypeDescription, out type))
            {
                if (!string.IsNullOrEmpty(shortTypeDescription))
                {
                    contains = (enTypeOfContents)Convert.ToInt32(sigFile.FileFormat.Where(x => x[4] == shortTypeDescription).Select(x => x[3]).FirstOrDefault());
                    _contains.Add(shortTypeDescription, contains);
                }
                return contains;
            }
            return type;
        }

        /// <summary>
        /// Функция возвращает имя константы, указывающей на стиль отображения текста файла в графическом 
        /// интерфейсе. Пустая строка, если тип отображения по умолчанию.
        /// </summary>
        /// <returns></returns>
        static public string BrowseIndex()
        {
            return "";
        }

        List<string> extensions;

        /// <summary>
        /// Возвращает список расширений, которые могут быть у файлов данного типа.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> Extensions()
        {
            if (extensions == null)
            {
                if (!string.IsNullOrEmpty(shortTypeDescription))
                {
                    string exts = sigFile.FileFormat.Where(x => x[4] == shortTypeDescription).Select(x => x[6]).FirstOrDefault();
                    if (exts != null)
                    {
                        extensions = new List<string>(exts.Split('.'));
                    }
                }
            }
            return extensions;
        }

        /// <summary>
        /// Данный метод применим только в том случае, если данный тип содержит исходные тексты (см. <see cref="Store.FileType.Contains"/>). Возвращает перечень языков программирования, которые могут содержаться в даннном типе.
        /// В случае, если файлы данного типа не содержат исходных текстов, будет выдана ошибка в <see cref="IA.Monitor"/>, и возвращен пустой список
        /// </summary>
        /// <returns></returns>
        public string[] ContainsLanguages()
        {
            if ((Contains() & enTypeOfContents.TEXTSOURCES) != enTypeOfContents.TEXTSOURCES && (Contains() & enTypeOfContents.BINARYSOURCES) != enTypeOfContents.BINARYSOURCES)
                return new string[0];

            string[] res;

            ulong lang;
            if (!ulong.TryParse(sigFile.FileFormat.Where(x => x[4] == shortTypeDescription).Select(x => x[7]).FirstOrDefault(), out lang))
                lang = 0;
            res = ((enLanguage)lang).ToString().Split(',').Select(x => x.Trim()).ToArray();

            if (res.Length == 0)
                throw new Exception("Наружена целостность базы данных сигнатур. Для типа " + ShortTypeDescription() + " отсутствует указание языков исходных текстов");
            return res;
        }

        /// <summary>
        /// Метод отвечает на вопрос: могут ли содержаться в файлах данного типа указанные языки программирования.
        /// </summary>
        /// <param name="languagesToContain">Перечень языков для проверки.</param>
        /// <returns>true если хотя бы один из запрашиваемых языков может содержаться в данном типе файла.</returns>
        public bool ContainsLanguages(enLanguage languagesToContain)
        {
            if ((Contains() & enTypeOfContents.TEXTSOURCES) != enTypeOfContents.TEXTSOURCES && (Contains() & enTypeOfContents.BINARYSOURCES) != enTypeOfContents.BINARYSOURCES)
                return false;

            ulong lang = 0;
            ulong.TryParse(sigFile.FileFormat.Where(x => x[4] == shortTypeDescription).Select(x => x[7]).FirstOrDefault(), out lang);

            enLanguage typeLanguages = (enLanguage)lang;
            return (typeLanguages & languagesToContain) != 0;
        }

        /// <summary>
        ///  Реализация интерфейса IComparable
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(FileType other)
        {
            return this.shortTypeDescription.CompareTo(other.shortTypeDescription);
        }

        /// <summary>
        /// Реализация интерфейса IEquatable
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(FileType other)
        {
            return this.shortTypeDescription.Equals(other.shortTypeDescription);
        }
    }

    /// <summary>
    /// Класс содержит константы заданных типов файлов
    /// </summary>
    public static class SpecialFileTypes
    {

        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "UNKNOWN")]
        public static FileType UNKNOWN
        {
            get
            {
                FileType type = new FileType("Unknown File Format");
                return type;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "HTML"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "COMMON"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType COMMON_HTML
        {
            get
            {
                FileType type = new FileType("Hypertext Markup Language");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "EXE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "FILE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType EXE_FILE
        {
            get
            {
                FileType type = new FileType("Исполняемый файл");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "LIB"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "FILE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType LIB_FILE
        {
            get
            {
                FileType type = new FileType("Static Library (lib)");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "WIN"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "LIBRARY"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType WIN32_LIBRARY
        {
            get
            {
                FileType type = new FileType("Динамическая библиотека в Windows");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SOURCE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "JAVA"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType JAVA_SOURCE
        {
            get
            {
                FileType type = new FileType("Исходные тексты на языке Java");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SOURCE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "PASCAL"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType PASCAL_SOURCE
        {
            get
            {
                FileType type = new FileType("Source Code Delphi");
                return type;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SOURCE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SCRIPT"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "JAVA"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType JAVA_SCRIPT_SOURCE
        {
            get
            {
                FileType type = new FileType("Исходные тексты на языке JavaScript");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SOURCE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CSHARP"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType CSHARP_SOURCE
        {
            get
            {
                FileType type = new FileType("Source Code C#");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "XML"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "COMMON"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType COMMON_XML
        {
            get
            {
                FileType type = new FileType("Документ XML v. 1.0");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "DFM"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "BORLAND"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType BORLAND_DFM
        {
            get
            {
                FileType type = new FileType("Текстовый файл ресурсов Delphi");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MDB"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType MDB
        {
            get
            {
                FileType type = new FileType("База данных Microsoft Access");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MDB"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType MDB_No_Source
        {
            get
            {
                FileType type = new FileType("База данных Microsoft Access без форм и модулей");
                return type;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MDB"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType PHP_SOURCE
        {
            get
            {
                FileType type = new FileType("PHP Script Page");
                return type;
            }
        }

        /// <summary>
        /// Исходные тексты на языке С (основной файл)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SOURCE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType C_SOURCE
        {
            get
            {
                FileType type = new FileType("Source Code C");
                return type;
            }
        }

        /// <summary>
        /// Исходные тексты на языке СPP (основной файл)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SOURCE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CPP"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType CPP_SOURCE
        {
            get
            {
                FileType type = new FileType("Source Code C++");
                return type;
            }
        }

        /// <summary>
        /// Заголовочный файл исходных текстов на языках С и С++ (разделить заголовочные файлы на этих языках сложно
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SOURCE"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "HEADER"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CPP"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static FileType C_CPP_HEADER_SOURCE
        {
            get
            {
                FileType type = new FileType("C/C++ Header");
                return type;
            }
        }

        /// <summary>
        /// Проверяет, содержит ли данный тип исходные тексты на языках C/C++
        /// </summary>
        /// <param name="type"></param>
        /// <returns>true, если содержит исходных тексты на языках C/C++</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "is"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CPP"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static bool is_C_CPP_Source(FileType type)
        {
            return type == C_SOURCE || type == CPP_SOURCE || type == C_CPP_HEADER_SOURCE;
        }

        /// <summary>
        /// Проверяет, содержит ли данный файл исходные тексты на языках C/C++
        /// </summary>
        /// <param name="file"></param>
        /// <returns>true, если содержит исходных тексты на языках C/C++</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "is"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CPP"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        public static bool is_C_CPP_Source(IFile file)
        {
            if (file == null)
                return false;
            else
                return is_C_CPP_Source(file.fileType);
        }



    }

}
