﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Linq;

namespace Store
{

    /// <summary>
    /// Хранит настройки, использованные сработавшими плагинами и которые могут быть использованы другими плагинами.
    /// </summary>
    public class AppliedSettings
    {
        /// <summary>
        /// Хранится в этом файле
        /// </summary>
        const string appliedSettingsFileName = @"appliedSettings.xml";

        Storage storage;
        System.Xml.XmlDocument document = new XmlDocument();

        #region Generate AppliedSettings

        /// <summary>
        /// Создавать экземпляры AppliedSettings самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal AppliedSettings(Storage storage)
        {
            this.storage = storage;

            //лучше одна проверка, чем регулярный catch FileNotFound при дебаге
            string filename = Path.Combine(storage.Get_DB().GetDBDirectory(), appliedSettingsFileName);
            if (File.Exists(filename))
                document.Load(filename);

        }

        #endregion

        #region Common Functions

        private string GetString(string xmlName)
        {
            XmlNodeList list = document.SelectNodes("/descendant-or-self::" + xmlName);
            if (list.Count == 0)
                return null;
            else
                if (list.Count > 1)
            {
                throw new Exception("В xml-настройках присутствует более одного тега" + xmlName);
            }
            else
            {
                string str = list[0].InnerText;
                list = null;
                return str;
            }
        }

        private string SetPath(string path, string xmlName)
        {
            if (path != null && path[path.Length - 1] != '\\' && path[path.Length - 1] != '/')
                path += '\\';

            return SetString(path, xmlName);
        }

        private string SetString(string path, string xmlName)
        {
            XmlNodeList list = document.GetElementsByTagName(xmlName);
            XmlElement elem = null;
            if (list.Count == 0)
            {
                XmlElement top = GetTopElements(document);
                elem = document.CreateElement(xmlName);
                top.AppendChild(elem);
            }
            else
                if (list.Count > 1)
                throw new Exception("В xml-настройках присутствует более одного тега" + xmlName);
            else
                elem = (XmlElement)list[0];

            elem.InnerText = path;
            document.Save(storage.Get_DB().GetDBDirectory() + appliedSettingsFileName);
            return path;
        }

        static private XmlElement GetTopElements(XmlDocument document)
        {
            XmlElement res = document.FirstChild as XmlElement;
            if (res == null)
            {
                res = document.CreateElement("TOP");
                document.AppendChild(res);
            }
            return res;
        }
        #endregion

        #region FileListPaths


        const string nameFileListPath = "FileListPath";
        const string nameFileListPathFromUser = "FileListPathFromUser";

        string FileListPath_saved;
        WorkDirectory.enSubDirectories FileListPath_saved_en;

        /// <summary>
        /// Путь к дереву папок, который был использован плагином FillFileList для заполнения перечня файлов объекта анализа
        /// </summary>
        public string FileListPathNoUnpack
        {
            get
            {
                return FileListPath_saved;
            }
        }

        /// <summary>
        /// Путь к дереву папок, который был использован плагином FillFileList для заполнения перечня файлов объекта анализа
        /// </summary>
        public string FileListPath
        {
            get
            {
                // распакуем, если надо
                storage.WorkDirectory.GetSubDirectoryPathWithUnpack(FileListPath_saved_en);
                return FileListPath_saved;
            }
        }

        /* Merge from trunk 2014.02.12 */
        /// <summary>
        /// Установить путь к дереву исходных файлов
        /// Это нужно только для тестов
        /// </summary>
        /// <param name="path"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "тега"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "присутствует"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "одного"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "более"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "xml-настройках"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "FileListPath")]
        public void SetFileListPath(string path)
        {
            if (!path.EndsWith(@"\")) path += @"\";
            path = SetPath(Files.CanonizeFileName(path), nameFileListPath);
            FileListPath_saved = path;

            // распакуем папку, которая точно нужна
            WorkDirectory.enSubDirectories subdir = storage.WorkDirectory.GetSubDirectoryByName(Path.GetFileName(Path.GetDirectoryName(FileListPath_saved)));
            storage.WorkDirectory.GetSubDirectoryPathWithUnpack(subdir);
        }

        /// <summary>
        /// Установить путь к дереву исходных файлов
        /// </summary>
        /// <param name="path"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "тега"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "присутствует"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "одного"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "более"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "xml-настройках"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "FileListPath")]
        public void SetFileListPath(Store.WorkDirectory.enSubDirectories path)
        {
            FileListPath_saved = storage.WorkDirectory.GetSubDirectoryPath(path);
            SetPath(FileListPath_saved, nameFileListPath);
            FileListPath_saved_en = path;
        }


        /// <summary>
        /// Путь к дереву папок, который введен в настройке плагина FillFileList. 
        /// Может не совпадать с FileListPath, если плагин FillFileList не был запущен.
        /// </summary>
        public string FileListPathFromUser
        {
            get
            {
                return GetString(nameFileListPathFromUser);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public void SetFileListPathFromUser(string path)
        {
            //path = SetPath(path, nameFileListPathFromUser);
            path = SetPath(Files.CanonizeFileName(path), nameFileListPath);
        }

        /* End merge from trunk */
        #endregion


        #region FileEssential

        const string nameFileEssentialPath = "FileEssentialPath";
        /// <summary>
        /// Путь к дереву папок, в которое плагин удаления избыточности по файлам положил очищенную версию исходных текстов
        /// </summary>
        public string FileEssentialPath
        {
            get
            {
                return GetString(nameFileEssentialPath);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public void SetFileEssentialPath(string path)
        {
            SetPath(path, nameFileEssentialPath);
        }

        #endregion

        #region IndexChars

        const string nameIndexCharsPath = "IndexChars";
        /// <summary>
        /// Перечень символов, который IndexingFileContent использовал при индексации
        /// </summary>
        public List<char> IndexChars
        {
            get
            {
                string str = GetString(nameIndexCharsPath);

                List<char> listChar = new List<char>();

                //Это предположительно код Кокина из IndexingFileContent. Перенес сюда, чтобы он был доступен другим плагинам
                if (!string.IsNullOrEmpty(str))
                {
                    listChar.Clear();
                    string[] sa = str.Split(new char[] { '|' }).Where(x => x != "").ToArray();
                    foreach (string s in sa)
                    {
                        if (s == "-")
                        {
                            listChar.Add(s[0]);
                        }
                        else
                        {
                            if (s.Contains("-"))
                            {
                                string[] sb = s.Split('-').Where(x => x != "").ToArray();
                                if (sb.Length >= 2)
                                {
                                    List<char> listt = Enumerable.Range((int)sb[0][0], (int)sb[1][0] - (int)sb[0][0] + 1).Select(x => Convert.ToChar(x)).ToList();
                                    listChar.AddRange(listt);
                                }
                            }
                            else
                                listChar.Add(s[0]);
                        }
                    }
                    listChar = listChar.Distinct().ToList();
                }

                return listChar;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public void SetIndexChars(string str)
        {
            SetString(str, nameIndexCharsPath);
        }

        #endregion

        const string isCheckCopiesFoundWorkedPath = "isCheckCopiesFoundWorked";
        /// <summary>
        /// Хранит флаг, используемый в разделе Files. Показывает, требуется ли повторять поиск дублей
        /// </summary>
        internal bool isCheckCopiesFoundWorked
        {
            get
            {
                return GetString(isCheckCopiesFoundWorkedPath) == "t" ? true : false;
            }
            set
            {
                SetString(value ? "t" : "f", isCheckCopiesFoundWorkedPath);
            }
        }
    }


}
