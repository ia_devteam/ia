﻿using System;
using System.Collections.Generic;

namespace Store
{
    /// <summary>
    /// Описатель для операторов.
    /// Подробнее см. <a href="b2e5c5e1-b1bd-4795-96cf-3f23fae5bab9.htm">Раздел Хранилища "Операторы"</a>.    /// </summary>
    public sealed class Statements
    {
        const string TableName_Leader = "StatementsLeader";
        const string TableName_FirstSymbolLocations = "StatementsFirstSymbolLocations";
        const string TableName_LastSymbolLocations = "StatementsLastSymbolLocations";
        const string TableName_TableBlockingForward = "StatementsBlockingForward";
        const string TableName_IndexBlockingBackward = "iStatementsBlockingBackward";
        const string TableName_IndexAboveStatements = "iStatementsAboveStatements";
        const string TableName_StatementsData = "StatementsData";
        const string TableName_StatementsControlFlow = "StatementsControlFlow";
        const string TableName_StatementsAdditionalData = "StatementsAdditionalData";
        const string TableName_ImplementsInFunction = "StatementsImplementsInFunction";
        

        internal Storage storage;
        internal Operations operations;

        //Основные таблицы

        internal Store.BaseDatatypes.TableLeader<Statement_Impl> TableLeader;

        /// <summary>
        /// Ключ - идентификатор объекта, предшествующего в линейном блоке
        /// Значение - идентификатор объекта, последующего в линейном блоке
        /// </summary>
        internal Store.BaseDatatypes.Table_KeyId_ValueId_NoDublicates TableBlockingForward;

        /// <summary>
        /// Хранение данных, специфичных для каждого типа оператора
        /// </summary>
        internal Store.BaseDatatypes.Table_KeyId_ValueSaver_Nodublicate TableStatementsData;

        /// <summary>
        /// Ключ - ID текущего оператора
        /// Значения - ID операторов, которые могут быть вызваны следующими в потоке управления
        /// </summary>
        internal Store.BaseDatatypes.Table_KeyID_ValueSaver_Dublicate TableStatementsNextInControl;

        internal Store.BaseDatatypes.Table_KeyID_ValueSaver_Dublicate TableStatementsAdditionalData;

        /// <summary>
        /// Хранение идентификатора функции, в теле которой находится оператор, идентификатор которого является ключем.
        /// </summary>
        internal Store.BaseDatatypes.Table_KeyId_ValueSaver_Nodublicate TableImplementsInFunction;



        //Индексные таблицы

        /// <summary>
        /// Ключ - идентификатор оператора, предшествующего в линейном блоке
        /// Значение - идентификатор оператора, последующего в линейном блоке
        /// </summary>
        internal Store.BaseDatatypes.Index_DelayCreation_KeyId_ValueId_NoDuplicate IndexBlockingBackward;

        /// <summary>
        /// Ключ - идентификатор оператора, являющегося вложенным в другой оператор
        /// Значение - идентификатор оператора, в который вложен данный оператор.
        /// </summary>
        internal Store.BaseDatatypes.Index_DelayCreation_KeyId_ValueId_NoDuplicate IndexAboveStatements;

        internal Statements(Storage storage)
        {
            this.storage = storage;
            this.operations = storage.operations;

            TableLeader = new BaseDatatypes.TableLeader<Statement_Impl>(storage, TableName_Leader, (kind) => Creater(kind));
            TableBlockingForward = new BaseDatatypes.Table_KeyId_ValueId_NoDublicates(storage, TableName_TableBlockingForward);
            IndexBlockingBackward = new BaseDatatypes.Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, TableName_IndexBlockingBackward, RebuildIndexBlockingBackward);
            IndexAboveStatements = new BaseDatatypes.Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, TableName_IndexAboveStatements, RebuildIndexAboveStatements);
            TableStatementsData = new BaseDatatypes.Table_KeyId_ValueSaver_Nodublicate(storage, TableName_StatementsData);
            TableStatementsNextInControl = new BaseDatatypes.Table_KeyID_ValueSaver_Dublicate(storage, TableName_StatementsControlFlow);
            TableStatementsAdditionalData = new BaseDatatypes.Table_KeyID_ValueSaver_Dublicate(storage, TableName_StatementsAdditionalData);
            TableImplementsInFunction = new BaseDatatypes.Table_KeyId_ValueSaver_Nodublicate(storage, TableName_ImplementsInFunction);
        }

        private Statement_Impl Creater(int kind)
        {
            Statement_Impl res;
            switch((ENStatementKind) kind)
            {
                case ENStatementKind.STBreak:
                    res = new StatementBreak_Impl(this);
                    break;
                case ENStatementKind.STContinue:
                    res = new StatementContinue_Impl(this);
                    break;
                case ENStatementKind.STDoAndWhile:
                    res = new StatementDoAndWhile_Impl(this);
                    break;
                case ENStatementKind.STFor:
                    res = new StatementFor_Impl(this);
                    break;
                case ENStatementKind.STForEach:
                    res = new StatementForEach_Impl(this);
                    break;
                case ENStatementKind.STGoto:
                    res = new StatementGoto_Impl(this);
                    break;
                case ENStatementKind.STIf:
                    res = new StatementIf_Impl(this);
                    break;
                case ENStatementKind.STOperational:
                    res = new StatementOperational_Impl(this);
                    break;
                case ENStatementKind.STReturn:
                    res = new StatementReturn_Impl(this);
                    break;
                case ENStatementKind.STSwitch:
                    res = new StatementSwitch_Impl(this);
                    break;
                case ENStatementKind.STThrow:
                    res = new StatementThrow_Impl(this);
                    break;
                case ENStatementKind.STTryCatchFinally:
                    res = new StatementTryCatchFinally_Impl(this);
                    break;
                case ENStatementKind.STUsing:
                    res = new StatementUsing_Impl(this);
                    break;
                case ENStatementKind.STYieldReturn:
                    res = new StatementYieldReturn_Impl(this);
                    break;
                case ENStatementKind.STExit:
                    res = new StatementExit_Impl(this);
                    break;
                default:
                    throw new Store.Const.StorageException("Неизвестный вид оператора");
            }
            return res;
        }

        private IEnumerable<KeyValuePair<UInt64, UInt64>> RebuildIndexAboveStatements()
        {
            foreach (IStatement st in EnumerateStatements())
                foreach (IStatement sub in st.SubStatements())
                    yield return new KeyValuePair<UInt64, UInt64>((sub as Statement_Impl).Id, (st as Statement_Impl).Id);
        }

        private IEnumerable<KeyValuePair<UInt64, UInt64>> RebuildIndexBlockingBackward()
        {
            foreach (KeyValuePair<UInt64, UInt64> pair in TableBlockingForward.EnumerateEveryone())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Value, pair.Key);
        }

        /// <summary>
        /// Добавить оператор
        /// </summary>
        /// <param name="kind">Тип оператора</param>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого символа оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. 
        ///                                 Если ноль (по умолчанию), то значение не задано и информация не заносится</param>
        /// <returns></returns>
        public IStatement AddStatement(ENStatementKind kind, UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            if (firstSymbolOffset == 0)
                throw new Store.Const.StorageException("Вызов AddStatement(ENStatementKind kind, IFile file, UInt64 firstSymbolOffset) некорректные аргументы. Offset == 0");

            Statement_Impl st = TableLeader.Add((Int32)kind);
            GetFirstSymbolLocations().SetSingleLocation(st.Id, file, firstSymbolOffset, null);

            if (lastSymbolOffset != 0)
                GetLastSymbolLocations().SetSingleLocation(st.Id, file, lastSymbolOffset, null);


            return st;
        }

        /// <summary>
        /// Добавить оператор
        /// </summary>
        /// <param name="kind">Тип оператора</param>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatement AddStatement(ENStatementKind kind, UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            if (firstSymbolLine == 0 || firstSymbolColumn == 0 )
                IA.Monitor.Log.Error("Хранилище", "Вызов AddStatement(ENStatementKind kind, IFile file, UInt64 firstSymbolLine, UInt64 firstSymbolColumn) некорректные аргументы. line == 0");

            Statement_Impl st = TableLeader.Add((Int32)kind);
            GetFirstSymbolLocations().SetSingleLocation(st.Id, file, firstSymbolLine, firstSymbolColumn, null);

            if(lastSymbolColumn != 0)
                if(lastSymbolLine != 0)
                    GetLastSymbolLocations().SetSingleLocation(st.Id, file, lastSymbolLine, lastSymbolColumn, null);

            if ((lastSymbolColumn == 0 || lastSymbolLine == 0) && (lastSymbolColumn != 0 || lastSymbolLine != 0))
                throw new Store.Const.StorageException("Вызов AddStatement(ENStatementKind kind, IFile file, UInt64 firstSymbolLine, UInt64 firstSymbolColumn) некорректные аргументы. lastSymbolLine и lastSymbolColumn либо одновременно должны быть равны 0, либо одновременно не равны 0");

            return st;
        }

        /// <summary>
        /// Добавить оператор. 
        /// <remarks>
        /// Местоположение оператора в исходных текстах необходимо определить потом при помощи функции
        /// </remarks>
        /// </summary>
        /// <param name="kind">Тип оператора</param>
        /// <returns></returns>
        public IStatement AddStatement(ENStatementKind kind)
        {
            Statement_Impl st = TableLeader.Add((Int32)kind);
            return st;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementBreak AddStatementBreak()
        {
            return AddStatement(ENStatementKind.STBreak) as IStatementBreak;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementBreak AddStatementBreak(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STBreak, firstSymbolOffset, file, lastSymbolOffset) as IStatementBreak;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementBreak AddStatementBreak(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STBreak, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementBreak;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementContinue AddStatementContinue()
        {
            return AddStatement(ENStatementKind.STContinue) as IStatementContinue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementContinue AddStatementContinue(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STContinue, firstSymbolOffset, file, lastSymbolOffset) as IStatementContinue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementContinue AddStatementContinue(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STContinue, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementContinue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementDoAndWhile AddStatementDoAndWhile()
        {
            return AddStatement(ENStatementKind.STDoAndWhile) as IStatementDoAndWhile;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementDoAndWhile AddStatementDoAndWhile(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STDoAndWhile, firstSymbolOffset, file, lastSymbolOffset) as IStatementDoAndWhile;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementDoAndWhile AddStatementDoAndWhile(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STDoAndWhile, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementDoAndWhile;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementFor AddStatementFor()
        {
            return AddStatement(ENStatementKind.STFor) as IStatementFor;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementFor AddStatementFor(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STFor, firstSymbolOffset, file, lastSymbolOffset) as IStatementFor;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementFor AddStatementFor(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STFor, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementFor;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementForEach AddStatementForEach()
        {
            return AddStatement(ENStatementKind.STForEach) as IStatementForEach;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementForEach AddStatementForEach(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STForEach, firstSymbolOffset, file, lastSymbolOffset) as IStatementForEach;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementForEach AddStatementForEach(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STForEach, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementForEach;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementGoto AddStatementGoto()
        {
            return AddStatement(ENStatementKind.STGoto) as IStatementGoto;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementGoto AddStatementGoto(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STGoto,firstSymbolOffset, file, lastSymbolOffset) as IStatementGoto;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementGoto AddStatementGoto(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STGoto, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementGoto;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementIf AddStatementIf()
        {
            return AddStatement(ENStatementKind.STIf) as IStatementIf;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementIf AddStatementIf(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STIf,firstSymbolOffset, file, lastSymbolOffset) as IStatementIf;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementIf AddStatementIf(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STIf, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementIf;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementOperational AddStatementOperational()
        {
            return AddStatement(ENStatementKind.STOperational) as IStatementOperational;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementOperational AddStatementOperational(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STOperational,firstSymbolOffset, file, lastSymbolOffset) as IStatementOperational;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementOperational AddStatementOperational(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STOperational, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementOperational;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementReturn AddStatementReturn()
        {
            return AddStatement(ENStatementKind.STReturn) as IStatementReturn;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementReturn AddStatementReturn(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STReturn,firstSymbolOffset, file, lastSymbolOffset) as IStatementReturn;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementReturn AddStatementReturn(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STReturn, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementReturn;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementYieldReturn AddStatementYieldReturn()
        {
            return AddStatement(ENStatementKind.STYieldReturn) as IStatementYieldReturn;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementYieldReturn AddStatementYieldReturn(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STYieldReturn,firstSymbolOffset, file, lastSymbolOffset) as IStatementYieldReturn;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementYieldReturn AddStatementYieldReturn(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STYieldReturn, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementYieldReturn;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementSwitch AddStatementSwitch()
        {
            return AddStatement(ENStatementKind.STSwitch) as IStatementSwitch;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementSwitch AddStatementSwitch(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STSwitch,firstSymbolOffset, file, lastSymbolOffset) as IStatementSwitch;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementSwitch AddStatementSwitch(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STSwitch, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementSwitch;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementThrow AddStatementThrow()
        {
            return AddStatement(ENStatementKind.STThrow) as IStatementThrow;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementThrow AddStatementThrow(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STThrow,firstSymbolOffset, file, lastSymbolOffset) as IStatementThrow;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementThrow AddStatementThrow(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STThrow, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementThrow;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementTryCatchFinally AddStatementTryCatchFinally()
        {
            return AddStatement(ENStatementKind.STTryCatchFinally) as IStatementTryCatchFinally;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementTryCatchFinally AddStatementTryCatchFinally(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STTryCatchFinally,firstSymbolOffset, file, lastSymbolOffset) as IStatementTryCatchFinally;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementTryCatchFinally AddStatementTryCatchFinally(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STTryCatchFinally, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementTryCatchFinally;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IStatementUsing AddStatementUsing()
        {
            return AddStatement(ENStatementKind.STUsing) as IStatementUsing;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolOffset">Смещение от начала файла до первого сим вола оператора. Обязательно указывать реальное смещение, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolOffset">Необязательный параметр. Смещение от начала файла до последнего символа оператора. Если 0 (по умолчанию), то значение параметра считается не заданным</param>
        /// <returns></returns>
        public IStatementUsing AddStatementUsing(UInt64 firstSymbolOffset, IFile file, UInt64 lastSymbolOffset = 0)
        {
            return AddStatement(ENStatementKind.STUsing,firstSymbolOffset, file, lastSymbolOffset) as IStatementUsing;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="firstSymbolLine">Номер строки, в которой находится первый символ оператора. Обязательно указывать реальную строку, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="firstSymbolColumn">Номер столбца, в котором находится первый символ оператора. Обязательно указывать реальный столбец, указание нуля приведёт к ошибке времени выполнения</param>
        /// <param name="lastSymbolLine">Необязательный параметр. Номер строки, в которой находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется</param>
        /// <param name="lastSymbolColumn">Необязательный параметр. Номер столбца, в котором находится последний символ оператора. Если ноль (по умолчанию), то значение не задано и параметр игнорируется. 
        ///                                 При этом обязательно, чтобы lastSymbolLine равнялись или не равнялись 0 одновременно.</param>
        /// <returns></returns>
        public IStatementUsing AddStatementUsing(UInt64 firstSymbolLine, UInt64 firstSymbolColumn, IFile file, UInt64 lastSymbolLine = 0, UInt64 lastSymbolColumn = 0)
        {
            return AddStatement(ENStatementKind.STUsing, firstSymbolLine, firstSymbolColumn, file, lastSymbolLine, lastSymbolColumn) as IStatementUsing;
        }

        internal IStatementExit AddExitStatement()
        {
            return AddStatement(ENStatementKind.STExit) as IStatementExit;
        }

        /// <summary>
        /// Получить имеющийся оператор
        /// </summary>
        /// <param name="statementId"></param>
        /// <returns></returns>
        public IStatement GetStatement(UInt64 statementId)
        {
            return TableLeader.Get(statementId);
        }

        /// <summary>
        /// Перечисляет все операторы.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IStatement> EnumerateStatements()
        {
            return TableLeader.EnumerateEveryone();
        }

        LocationsForSingle firstSymbolLocations;
        /// <summary>
        /// Таблица, содержащая местоположения первых символов операторов.
        /// </summary>
        /// <returns></returns>
        internal LocationsForSingle GetFirstSymbolLocations()
        {
            if (firstSymbolLocations == null)
                firstSymbolLocations = storage.locationsForSingle(TableName_FirstSymbolLocations);

            return firstSymbolLocations;
        }

        LocationsForSingle lastSymbolLocations;
        /// <summary>
        /// Таблица, содержащая местоположения последних символов операторов.
        /// </summary>
        /// <returns></returns>
        internal LocationsForSingle GetLastSymbolLocations()
        {
            if (lastSymbolLocations == null)
                lastSymbolLocations = storage.locationsForSingle(TableName_LastSymbolLocations);

            return lastSymbolLocations;
        }

        /// <summary>
        /// Возвращает количество операторов, имеющихся в Хранилище
        /// </summary>
        public UInt64 GetStatementCount()
        {
            return TableLeader.Count();
        }
    }
}
