﻿using System.Collections.Generic;
using System.Linq;

namespace Store
{
    internal abstract partial class Statement_Impl : Store.BaseDatatypes.DataObjectLeader, IStatement
    {
        internal sealed class StatementLinker
        {
            //Очередь элементов для обработки
            Queue<Node> queue = new Queue<Node>();

            /// <summary>
            /// Пул всех операторов
            /// </summary>
            Dictionary<IStatement, Node> nodes = new Dictionary<IStatement, Node>();

            /// <summary>
            /// Текущая обрабатываемая функция
            /// </summary>
            IFunction function;


            internal struct LinkerEdge
            {
                internal LinkerEdge(Node node, bool isReverse)
                {
                    this.destination = node;
                    this.isReverse = isReverse;
                }

                /// <summary>
                /// Цель, куда ведёт дуга
                /// </summary>
                internal Node destination;

                /// <summary>
                /// true - если дума обратная. То есть ведёт из конца цикла в его голову
                /// 
                /// Построение данных дуг в первую очередь ориентировано на то, может ли при данном переходе сработать датчик, стоящий перед destination. 
                /// </summary>
                internal bool isReverse;
            }

            /// <summary>
            /// Поток управления - граф, вершины графа - элементы класса
            /// </summary>
            internal class Node
            {
                /// <summary>
                /// Вершина графа потока управления
                /// </summary>
                public IStatement elem;

                /// <summary>
                /// Оператор, линейно следующий за текущим блоком. Когда увидем завершение текущего блока, то надо будет перейти в него
                /// </summary>
                public List<LinkerEdge> nextLinearAfterBlock = new List<LinkerEdge>();

                /// <summary>
                /// Стэк текущих catch-finally
                /// </summary>
                public CatchFinallyStack cfs = CatchFinallyStack.GetEmpty();
                
                /// <summary>
                /// В случае, если мы попали в break, который не знает своего цикла, то его надо соединить с этим оператором
                /// </summary>
                Node LoopOnBreak = null;

                /// <summary>
                /// Список вершин, которые должны быть обновлены после того, как обновилась данная вершина. То есть подписчики на обновление
                /// </summary>
                public List<Node> updateList = new List<Node>();

                /// <summary>
                /// Флаг, показывающий, находится ли данный оператор в мертвом (висящем) коде. false - оператор в мертвом коде
                /// </summary>
                internal bool isNeedAnalyse = false;

                /// <summary>
                /// Получить у ноды все дуги, которые могут из неё выходить
                /// </summary>
                /// <param name="linker"></param>
                /// <returns></returns>
                internal IEnumerable<Edge> NextStatements(StatementLinker linker)
                {
                    switch (elem.Kind)
                    {
                        case ENStatementKind.STDoAndWhile:
                            {
                                IStatementDoAndWhile s = elem as IStatementDoAndWhile;

                                //Цикл выглядит следующим образом
                                // Do {} while (); next;
                                // либо
                                // while () do {}; next;
                                // В первом случае оператор "цикл" получит управление после первой итерации исполнения тела
                                // Во втором случае оператор "цикл" получит управление до исполнения тела - для проверки условия
                                // В любом случае после цикла может исполниться как его тело, так и следующщий оператор

                                //Вначале ищем линейно следующие операторы
                                List<LinkerEdge> NextInLinearBlock = StandartNextLiner(linker, s);

                                foreach (LinkerEdge n in NextInLinearBlock)
                                    yield return new Edge (n);

                                //Теперь обрабатываем Body
                                if (s.Body != null)
                                {
                                    Node body = linker.GetNode(s.Body, true);
                                    body.UpdateKnowledge(new List<LinkerEdge>() {new LinkerEdge(this, true)}, cfs, this, linker, true); //Возможно, добавляем новые знания в body
                                
                                    yield return new Edge(body, false);
                                }

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge (node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём
                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STFor:
                            {
                                IStatementFor s = elem as IStatementFor;

                                //Вначале ищем линейно следующие операторы
                                List<LinkerEdge> NextInLinearBlock = StandartNextLiner(linker, s);

                                foreach (LinkerEdge n in NextInLinearBlock)
                                    yield return new Edge(n);

                                //Теперь обрабатываем Body
                                if (s.Body != null)
                                {
                                    Node body = linker.GetNode(s.Body, true);
                                    body.UpdateKnowledge(new List<LinkerEdge>() {new LinkerEdge(this, true)}, cfs, this, linker, true); //Возможно, добавляем новые знания в body

                                    yield return new Edge(body, false);
                                }

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём
                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STForEach:
                            {
                                IStatementForEach s = elem as IStatementForEach;

                                //Вначале ищем линейно следующие операторы
                                List<LinkerEdge> NextInLinearBlock = StandartNextLiner(linker, s);

                                foreach (LinkerEdge n in NextInLinearBlock)
                                    yield return new Edge(n);

                                //Теперь обрабатываем Body
                                if (s.Body != null)
                                {
                                    Node body = linker.GetNode(s.Body, true);
                                    body.UpdateKnowledge(new List<LinkerEdge>() { new LinkerEdge(this, true) }, cfs, this, linker, true); //Возможно, добавляем новые знания в body

                                    yield return new Edge(body, false);
                                }

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём
                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STBreak:
                        case ENStatementKind.STContinue:
                            {
                                //различающийся код
                                Node loop;
                                if (elem is IStatementBreak)
                                {
                                    IStatementBreak s = elem as IStatementBreak;
                                    if(s.BreakingLoop != null)
                                        loop = linker.GetNode(s.BreakingLoop, true); 
                                    else  //Парсер таких знаний не занес
                                    {
                                        if (this.LoopOnBreak == null)
                                            loop = null;
                                        else
                                        {
                                            s.BreakingLoop = this.LoopOnBreak.elem;
                                            loop = this.LoopOnBreak;
                                        }
                                    }
                                }
                                else
                                {
                                    IStatementContinue s = elem as IStatementContinue;
                                    if (s.ContinuingLoop != null)
                                        loop = linker.GetNode(s.ContinuingLoop, true);
                                    else
                                    {
                                        if (this.LoopOnBreak == null)
                                            loop = null;
                                        else
                                        {
                                            s.ContinuingLoop = this.LoopOnBreak.elem;
                                            loop = this.LoopOnBreak;
                                        }
                                    }
                                }

                                //Вначале думаем про линейно следующий оператор. Мы не передаём на него управление, но если он каким-то 
                                //чудом окажется используемым, то он должен знать свою историю
                                if (elem.NextInLinearBlock != null)
                                {
                                    Node next = linker.GetNode(elem, false);
                                    next.UpdateKnowledge(this.nextLinearAfterBlock, cfs, this.LoopOnBreak, linker, false);
                                }

                                //Не возращаем NextInLinearBlock, так как перехода на него из даного оператора нет

                                //break может нас выбить из finally. Надо построить цепочку, если она нужна
                                Node final;
                                if (cfs.FindDifferentFinally(loop, out final)) //Ищем finally, в который мы можем перейти
                                //Cоответственно, все finally цикла являются finally нашего оператора
                                //Есть ли у нашего оператора отличия от finally цикла?
                                {
                                    //Создаём переход из нашего break в самый нижний finally. Ведь на самом деле управление перейдет туда
                                    yield return new Edge(final, false);

                                    //Создаём переход из finally  который является первым, в котором не участвует цикл, в сам цикл. Ведь когда
                                    //отработает вся цепочка finally, переход произойдет именно в цикл
                                    yield return new Edge(cfs.GetUpSideDifferentFinally(loop),true); //GetUpSideDifferentFinally всегда должен 
                                                                                                    //что-то вернуть, так как только что 
                                                                                                    //вызывали FindDifferentFinally

                                    //С промежуточным finally и catch ничего делать не надо. Цепочка построится автоматически, когда будет проходить через finally
                                }
                                else //Никакие finally учитывать не требуется. Валим напрямую в цикл
                                {
                                    if (elem is IStatementContinue) //Если continue, то цикл продолжит выполнение
                                        if(loop != null)
                                            yield return new Edge(loop, true);
                                        else
                                            IA.Monitor.Log.Error("Хранилище", "Оператор continue находится вне цикла на строке " + elem.FirstSymbolLocation.GetLine() + " столбце " + elem.FirstSymbolLocation.GetColumn() + " файла " + elem.FirstSymbolLocation.GetFile().FileNameForReports);
                                    else //Если break, то цикл закончится
                                    //Надо переходить в последующий за ним оператор
                                    {
                                        if (loop != null)
                                        {
                                            //Выбираем следующий элемент по управлению за loop
                                            //Не пользуемся стандартными средствами, так как в них есть side effect
                                            if (loop.elem.NextInLinearBlock != null)
                                                yield return new Edge(linker.GetNode(loop.elem.NextInLinearBlock, true), false);
                                            else
                                                if (loop.nextLinearAfterBlock.Count > 0)
                                                    foreach (LinkerEdge edge in loop.nextLinearAfterBlock)
                                                        yield return new Edge(edge);
                                                else
                                                    yield return new Edge( linker.GetExitNode(), false);

                                            loop.updateList.Add(this);
                                        }
                                        else //В JavaScript break может быть вне циклов и т.п. Воспринимае его как выход из функции
                                            yield return new Edge(linker.GetExitNode(), false);
                                    }
                                }

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём                                
                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STIf:
                            {
                                IStatementIf s = elem as IStatementIf;

                                //Вначале ищем линейно следующие операторы
                                List<LinkerEdge> NextInLinearBlock = StandartNextLiner(linker, s);

                                //Теперь then
                                Node then = linker.GetNode(s.ThenStatement, true);
                                then.UpdateKnowledge(NextInLinearBlock, cfs, this.LoopOnBreak, linker, true); //Возможно, добавляем новые знания в then

                                yield return new Edge(then, false);

                                //Теперь else
                                if (s.ElseStatement != null)
                                {
                                    Node els = linker.GetNode(s.ElseStatement, true);
                                    els.UpdateKnowledge(NextInLinearBlock, cfs, this.LoopOnBreak, linker, true); //Возможно, добавляем новые знания в then

                                    yield return new Edge(els, false);
                                }
                                else //Если else нет, то переходим в последующие
                                    foreach (LinkerEdge n in NextInLinearBlock)
                                        yield return new Edge(n);

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём       
                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STReturn:
                            {
                                IStatementReturn s = elem as IStatementReturn;

                                //Вначале думаем про линейно следующий оператор. Мы не передаём на него управление, но если он каким-то 
                                //чудом окажется используемым, то он должен знать свою историю
                                if (s.NextInLinearBlock != null)
                                {
                                    Node next = linker.GetNode(s, false);
                                    next.UpdateKnowledge(this.nextLinearAfterBlock, cfs, this.LoopOnBreak, linker, false);
                                }

                                //Не возращаем NextInLinearBlock, так как перехода на него из даного оператора нет

                                Node final;
                                if (!cfs.FindFirstFinally(out final)) //Если финализаторов нет, то мы попадаем прямо в выход
                                    yield return new Edge(linker.GetExitNode(), false);
                                else //Если есть финализаторы
                                    //То идем в самый ближний из них
                                    yield return new Edge(final, false);
                                    //Больше ничего делать не надо. Связь между финализаторами и выходом будет построена сама

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём       
                                AcceptListUpdate(linker);

                                break;
                            }

                        case ENStatementKind.STThrow:
                            {
                                //Вначале думаем про линейно следующий оператор. Мы не передаём на него управление, но если он каким-то 
                                //чудом окажется используемым, то он должен знать свою историю
                                if (elem.NextInLinearBlock != null)
                                {
                                    Node next = linker.GetNode(elem, false);
                                    next.UpdateKnowledge(this.nextLinearAfterBlock, cfs, this.LoopOnBreak, linker, false);
                                }

                                //Не возращаем NextInLinearBlock, так как перехода на него из даного оператора нет

                                //Стандартно обрабатываем случившеееся исключение. Как в любом другом обычном операторе
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                if (cfs.isEmpty()) //Если выходить по исключениям было некуда, 
                                                   //то валим в выход из функции
                                    yield return new Edge(linker.GetExitNode(), false);

                                break;
                            }
                        case ENStatementKind.STYieldReturn:
                            {
                                //Сейчас yield return рассматривается как простой передаватель управления в следующий опертор и в выход

                                var s = elem as IStatementYieldReturn;

                                //Вначале думаем про линейно следующий оператор. Он должен знать свою историю
                                if (s.NextInLinearBlock != null)
                                {
                                    Node next = linker.GetNode(s, true);
                                    next.UpdateKnowledge(this.nextLinearAfterBlock, cfs, this.LoopOnBreak, linker, true);
                                }

                                //Вначале ищем линейно следующие операторы
                                List<LinkerEdge> NextInLinearBlock = StandartNextLiner(linker, s);

                                //Теперь обрабатываем линейно следующие операторы
                                foreach(LinkerEdge edge in NextInLinearBlock)
                                    yield return new Edge(edge);

                                Node final;
                                if (!cfs.FindFirstFinally(out final)) //Если финализаторов нет, то мы попадаем прямо в выход
                                    yield return new Edge(linker.GetExitNode(), false);
                                else //Если есть финализаторы
                                    //То идем в самый верхний из них
                                    yield return new Edge(final, false);
                                //Больше ничего делать не надо. Связь между финализаторами и выходом будет построена сама

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём       
                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STSwitch:
                            {
                                IStatementSwitch s = elem as IStatementSwitch;

                                //Вначале ищем линейно следующие операторы
                                List<LinkerEdge> NextInLinearBlock = StandartNextLiner(linker, s);

                                if (s.DefaultCase() == null) //Если default-блока нет, то мы имеем возможность перейти прямо за if сразу после 
                                    //проверки условия
                                    foreach (LinkerEdge n in NextInLinearBlock)
                                        yield return new Edge(n);

                                //Теперь идем по всем условиям
                                Node[] caseStatements = s.Cases().Select(a => linker.GetNode(a.Value, true)).ToArray(); //КЭШируем список обработок условий
                                for (int i = 0; i < caseStatements.Length; i++)
                                {
                                    Node cur = caseStatements[i]; //Текущий обрабатываемый оператор
                                    Node next = null; //Оператор, в который перетечет управление из данного оператора

                                    if (i + 1 < caseStatements.Length) //Предполагаем, что управление перетечет в следующий в массиве оператор, если он есть
                                        next = caseStatements[i + 1];
                                    else
                                        if (s.DefaultCase() != null) //Если следующего в массиве оператора нет, то предполагаем, что управление
                                            //перетечет в default
                                            next = linker.GetNode(s.DefaultCase(), true);

                                    if (next != null) //Если последующий оператор найден, то надо задать его для текущего оператора
                                        cur.UpdateKnowledge(new List<LinkerEdge> { new LinkerEdge(next, false) }, cfs, this, linker, true);
                                    else //Если следующего оператора нет, то поступаем стандартно - показываем список линейно последующих оперторов
                                        cur.UpdateKnowledge(NextInLinearBlock, cfs, this, linker, true);

                                    yield return new Edge(cur, false);
                                }

                                if (s.DefaultCase() != null)  //Отдельно разбираемся с default. Предполагаем, что у него последующих операторов нет
                                {
                                    Node defa = linker.GetNode(s.DefaultCase(), true);
                                    defa.UpdateKnowledge(NextInLinearBlock, cfs, this, linker, true);

                                    yield return new Edge(defa, false);
                                }

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём       
                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STUsing:
                            {
                                IStatementUsing s = elem as IStatementUsing;

                                //Вначале ищем линейно следующие операторы
                                List<LinkerEdge> NextInLinearBlock = StandartNextLiner(linker, s);

                                //Теперь обрабатываем
                                Node us = linker.GetNode(s.Body, true);
                                us.UpdateKnowledge(NextInLinearBlock, cfs, this.LoopOnBreak, linker, true); //Возможно, добавляем новые знания в using

                                yield return new Edge(us, false);

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём       
                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STTryCatchFinally:
                            {
                                IStatementTryCatchFinally s = elem as IStatementTryCatchFinally;

                                //Оператор try блока
                                Node tr = linker.GetNode(s.TryBlock, true);

                                //Обновлений стэка не бывает. Так что либо надо строить новый, либо используем старый
                                bool isFirstVisit = tr.cfs == CatchFinallyStack.GetEmpty();

                                //Вначале ищем линейно следующие операторы
                                List<LinkerEdge> NextInLinearBlock = StandartNextLiner(linker, s);

                                //try и catch блоки после завершения переходят в finally, если он есть. Так что для них  надо сформировать
                                //отдельный список
                                List<LinkerEdge> NextForInternalFinally;
                                if (s.FinallyBlock != null) //если есть finally блок
                                    NextForInternalFinally = new List<LinkerEdge>() { new LinkerEdge(linker.GetNode(s.FinallyBlock, true), false) };
                                else //Если нет finally, то будем просто проскакивать
                                    NextForInternalFinally = NextInLinearBlock;

                                List<LinkerEdge> NextForInternalElse;
                                if (s.ElseBlock != null) //если есть else блок
                                    NextForInternalElse = new List<LinkerEdge>() { new LinkerEdge(linker.GetNode(s.ElseBlock, true), false) };
                                else //Если нет except, то будем просто проскакивать на finally
                                    NextForInternalElse = NextForInternalFinally;

                                if (isFirstVisit) //Надо ли менять стек catch finally?
                                {
                                    CatchFinallyStack newStackTry = cfs.CreateSubStackFull(this.elem as IStatementTryCatchFinally, linker);

                                    //Обходим Try
                                    tr.UpdateKnowledge(NextForInternalElse, newStackTry, this.LoopOnBreak, linker, true); //Возможно, добавляем новые знания
                                    yield return new Edge(tr, false);

                                    //Обходим catch
                                    CatchFinallyStack newStackCatches = cfs.CreateSubStackFinally(this.elem as IStatementTryCatchFinally, linker);

                                    if (s.ElseBlock != null)
                                    {
                                        Node cat = linker.GetNode(s.ElseBlock, true);
                                        cat.UpdateKnowledge(NextForInternalFinally, newStackCatches, this.LoopOnBreak, linker, true);
                                    }

                                    foreach (IStatement catStat in s.Catches())
                                    {
                                        Node cat = linker.GetNode(catStat, true);
                                        cat.UpdateKnowledge(NextForInternalFinally, newStackCatches, this.LoopOnBreak, linker, true); //Возможно, добавляем новые знания
//                                        yield return cat;
                                    }
                                }
                                else //Менять не надо, используем старый
                                {
                                    //Обходим Try
                                    tr.UpdateKnowledge(NextForInternalElse, tr.cfs, this.LoopOnBreak, linker, true); //Возможно, добавляем новые знания
                                    yield return new Edge(tr, false);


                                    if (s.ElseBlock != null)
                                    {
                                        Node cat = linker.GetNode(s.ElseBlock, true);
                                        cat.UpdateKnowledge(NextForInternalFinally, cat.cfs, this.LoopOnBreak, linker, true);
                                    }
                                    //Обходим catch
                                    foreach (IStatement catStat in s.Catches())
                                    {
                                        Node cat = linker.GetNode(catStat, true);
                                        cat.UpdateKnowledge(NextForInternalFinally, cat.cfs, this.LoopOnBreak, linker, true); //Возможно, добавляем новые знания
                                    }
                                }

                                //Обходим finally
                                if (s.FinallyBlock != null)
                                {
                                    List<LinkerEdge> nextBlock = new List<LinkerEdge>();
                                    nextBlock.AddRange(NextInLinearBlock); //Могло ведь и не быть исключений и мы тихо-мирно движемся далее
                                    //Теперь добавляем варианты вылета по исключению
                                    Node final;
                                    if (!cfs.FindFirstFinally(out final)) //Если финализаторов нет, то мы попадаем прямо в выход
                                        //В таком случае вылетим из функции
                                        nextBlock.Add(new LinkerEdge(linker.GetExitNode(), false));
                                    else
                                        //если есть более верхние финализаторы, то попадём в них
                                        nextBlock.Add(new LinkerEdge(final, false));

                                    //Соединяем
                                    Node fin = linker.GetNode(s.FinallyBlock, true);
                                    fin.UpdateKnowledge(NextInLinearBlock, this.cfs, this.LoopOnBreak, linker, true); //Возможно, добавляем новые знания
  //                                  yield return fin;
                                }

                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STExit: //Ничего делать не надо. По хорошему, управление сюда никогда и не придёт.
                                break;

                        case ENStatementKind.STOperational:
                            {
                                IStatementOperational s = elem as IStatementOperational;

                                //Вначале ищем линейно следующие операторы
                                List<LinkerEdge> NextInLinearBlock = StandartNextLiner(linker, s);

                                //Теперь обрабатываем
                                foreach(LinkerEdge edge in NextInLinearBlock)
                                    yield return new Edge(edge);

                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false);

                                //Если кто-то подписался на наше обновление, то не забываем о нём       
                                AcceptListUpdate(linker);

                                break;
                            }
                        case ENStatementKind.STGoto:
                            {
                                IStatementGoto s = elem as IStatementGoto;

                                //Вначале думаем про линейно следующий оператор. Мы не передаём на него управление, но если он каким-то 
                                //чудом окажется используемым, то он должен знать свою историю
                                if (elem.NextInLinearBlock != null)
                                {
                                    Node next = linker.GetNode(elem, false);
                                    next.UpdateKnowledge(this.nextLinearAfterBlock, cfs, this.LoopOnBreak, linker, false);
                                }

                                //Не возращаем NextInLinearBlock, так как перехода на него из даного оператора нет

                                if(s.Destination != null) //Не все парсеры заполняют это поле.
                                    yield return new Edge(linker.GetNode(s.Destination, true), false);
                                else  //В случае, если парсер не смог указать, куда направлен goto, то считаем, что можем перейти куда угодно внутри текущей функции
                                {
                                    foreach(IStatement st in elem.ImplementsFunction.EnumerateStatements())
                                        yield return new Edge(linker.GetNode(st, true), false);
                                }


                                //Не забываем, что любой оператор может генерировать исключения
                                foreach (Node node in cfs.IterateNears(linker))
                                    yield return new Edge(node, false); 

                                //Если кто-то подписался на наше обновление, то не забываем о нём       
                                AcceptListUpdate(linker);

                                break;
                            }
                        default:
                            throw new Store.Const.StorageException("StatementLinker.NextStatements. Неизвестный тип оператора " + elem.GetType().Name);
                    }
                }

                /// <summary>
                /// В алгоритме используется стек возможных переходов управления в catch и finally
                /// </summary>
                internal class CatchFinallyStack
                {
                    /// <summary>
                    /// Знания одного уровня стека
                    /// </summary>
                    class Level
                    {
                        /// <summary>
                        /// Список catch или finally уровня стека
                        /// </summary>
                        internal Node[] stack;

                        /// <summary>
                        /// Оператор, по которому сгенерирован данный уровень стека
                        /// </summary>
                        internal IStatement tryStatementOfLevel;

                        /// <summary>
                        /// true, если данный уровень стека содержит fibnally
                        /// </summary>
                        internal bool isFinally;
                    }

                    /// <summary>
                    /// Стек catch и finally
                    /// </summary>
                    Level[] stack;


                    /// <summary>
                    /// Выдать место, куда наиболее близко мы могли перейти
                    /// </summary>
                    /// <returns></returns>
                    internal IEnumerable<Node> IterateNears(StatementLinker linker)
                    {
                        if (stack.Length != 0) //Мы входили во что-то
                        {
                            //Не достаточно показать только верхний слой, надо дойти до finally, либо до выхода из функции
                            for (int i = stack.Length - 1; i >= 0; i--)
                            {
                                foreach (Node n in stack[i].stack)
                                    yield return n;

                                if (stack[i].isFinally)
                                    yield break;
                            }

                            yield return linker.GetExitNode(); //Если в стеке нет ни одного finally, то мы можем упрыгать за функцию
                        }
                        else
                            yield break;
                    }

                    /// <summary>
                    /// Выполняет проверку, изменился ли стек. Если stack не содержит некоторых элементов текущего класса, то информация уменьшилась
                    /// и надо генерировать исключение.
                    /// 
                    /// true, только если stack == this.
                    /// </summary>
                    /// <param name="stack"></param>
                    /// <returns></returns>
                    internal bool CheckUpdate(CatchFinallyStack other)
                    {
                        if (this == other)
                            return false;

                        if (stack.Except(other.stack).Count() > 0) //Проверяем, что все наши элементы содержатся в other
                            throw new Store.Const.StorageException("CatchFinallyStack.CheckUpdate ошибка. Уменьшение множества");

                        return true;
                    }

                    /// <summary>
                    /// Возвращает, есть ли что-то в стеке
                    /// </summary>
                    /// <returns></returns>
                    internal bool isEmpty()
                    {
                        return stack.Length == 0;
                    }



                    static CatchFinallyStack empty = null;
                    /// <summary>
                    /// Используется для получения элемента с пустым стеком. Всегда возвращает один и тот же элемент
                    /// </summary>
                    /// <returns></returns>
                    internal static CatchFinallyStack GetEmpty()
                    {
                        if(empty == null)
                        {
                            empty = new CatchFinallyStack();
                            empty.stack = new Level[0];
                        }

                        return empty;
                    }

                    /// <summary>
                    /// Возвращает самый нижний Finally
                    /// </summary>
                    /// <returns></returns>
                    //internal Node IterateNearsFinally()
                    //{
                    //    for(int i = stack.Length - 1; i >= 0; i--)
                    //        if (stack[i].isFinally)
                    //            return stack[i];

                    //    return null;
                    //}


                    IStatement[] blankStatArr = new IStatement[] { };
                    /// <summary>
                    /// Ищет самый нижний finally, находящийся в различии между текущим стеком и стеком catchFinallyStack
                    /// При этом стек upper является более короткой версией текущего стека (если ничего не менялось в итерациях).
                    /// </summary>
                    /// <param name="catchFinallyStack"></param>
                    /// <param name="final"></param>
                    /// <returns></returns>
                    internal bool FindDifferentFinally(Node upper, out Node final)
                    {
                        IStatement[] Tries;
                        if (upper != null)
                            Tries = upper.cfs.stack.Select(a => a.tryStatementOfLevel).ToArray();
                        else
                            Tries = blankStatArr;

                        for (int i = stack.Length - 1; i >= 0; i--)
                        {
                            if (Tries.Contains(stack[i].tryStatementOfLevel)) //Различие закончилось. Прекращаем поиск
                                break;

                            if (stack[i].isFinally) //Нашли, ура, товарищи!!!
                            {
                                final = stack[i].stack[0];
                                return true;
                            }
                        }

                        final = null;
                        return false;
                    }

                    /// <summary>
                    /// Тоже самое, что и <see cref="FindDifferentFinally"/>, но ищем не первый finally, а последний
                    /// Возвращает null, если ничего не нашлось
                    /// </summary>
                    /// <param name="upper"></param>
                    /// <returns></returns>
                    internal Node GetUpSideDifferentFinally(Node upper)
                    {
                        IStatement[] Tries;
                        if (upper != null)
                            Tries = upper.cfs.stack.Select(a => a.tryStatementOfLevel).ToArray();
                        else
                            Tries = blankStatArr;
                        
                        Node result = null;

                        for (int i = stack.Length - 1; i >= 0; i--)
                        {
                            if (Tries.Contains(stack[i].tryStatementOfLevel)) //Различие закончилось. Прекращаем поиск
                                break;

                            if (stack[i].isFinally) //Нашли, ура, товарищи!!!
                                result = stack[i].stack[0];
                        }

                        return result;
                    }

                    /// <summary>
                    /// Найти самый глубокий в стеке finally
                    /// </summary>
                    /// <param name="final"></param>
                    /// <returns></returns>
                    internal bool FindFirstFinally(out Node final)
                    {
                        for (int i = stack.Length - 1; i >= 0; i--)
                            if (stack[i].isFinally) //Нашли, ура, товарищи!!!
                            {
                                final = stack[i].stack[0];
                                return true;
                            }

                        final = null;
                        return false;
                    }

                    internal CatchFinallyStack CreateSubStackFull(IStatementTryCatchFinally statement, StatementLinker linker)
                    {
                        IStatement[] catches = statement.Catches().ToArray();
                        
                        CatchFinallyStack newOne;
                        if (statement.FinallyBlock != null && catches.Length > 0)
                        {
                            newOne = GenerateNewCopy(stack.Length + 2);
                            newOne.stack[stack.Length] = new Level() { stack = new Node[] { linker.GetNode(statement.FinallyBlock, false) }, isFinally = true, tryStatementOfLevel = statement };
                            newOne.stack[stack.Length + 1] = new Level() { stack = statement.Catches().Select(a => linker.GetNode(a, false)).ToArray(), isFinally = false, tryStatementOfLevel = statement };
                        }
                        else
                            if(statement.FinallyBlock != null)
                            {
                                newOne = GenerateNewCopy(stack.Length + 1);
                                newOne.stack[stack.Length] = new Level() { stack = new Node[] { linker.GetNode(statement, false) }, isFinally = true, tryStatementOfLevel = statement };
                            }
                            else
                                if(catches.Length > 0)
                                {
                                   newOne = GenerateNewCopy(stack.Length + 1);
                                    newOne.stack[stack.Length] = new Level() { stack = statement.Catches().Select(a => linker.GetNode(a, false)).ToArray(), isFinally = false, tryStatementOfLevel = statement };
                                }
                                else
                                    return this;

                        return newOne;
                    }

                    internal CatchFinallyStack CreateSubStackFinally(IStatementTryCatchFinally statement, StatementLinker linker)
                    {
                        if (statement.FinallyBlock != null)
                        {
                            CatchFinallyStack newOne = GenerateNewCopy(stack.Length + 1);

                            newOne.stack[stack.Length] = new Level() { stack = new Node[] { linker.GetNode(statement.FinallyBlock, false) }, isFinally = true, tryStatementOfLevel = statement };

                            return newOne;
                        }
                        else
                            return this;
                    }

                    private CatchFinallyStack GenerateNewCopy(int AdditionalSize)
                    {
                        CatchFinallyStack newOne = new CatchFinallyStack();
                        newOne.stack = new Level[AdditionalSize];
                        for (int i = 0; i < stack.Length; i++)
                            newOne.stack[i] = stack[i];
                        return newOne;
                    }
                }

                private void AcceptListUpdate(StatementLinker linker)
                {
                    foreach (Node n in updateList)
                        linker.Enqueue(n);
                }


                /// <summary>
                /// Стандартное получение линейно следующего оператора
                /// </summary>
                /// <param name="linker"></param>
                /// <param name="s"></param>
                /// <returns></returns>
                private List<LinkerEdge> StandartNextLiner(StatementLinker linker, IStatement s)
                {
                    List<LinkerEdge> NextInLinearBlock;
                    if (s.NextInLinearBlock != null) //Если в текущем линейном блоке ещё что-то есть
                    {
                        //Не забываем передать свои знания дальше
                        Node nodeNext = linker.GetNode(s.NextInLinearBlock, true);
                        nodeNext.UpdateKnowledge(this.nextLinearAfterBlock, this.cfs, this.LoopOnBreak, linker, true);

                        NextInLinearBlock = new List<LinkerEdge> { new LinkerEdge(nodeNext, false) };
                    }
                    else //если вываливаемся наружу
                        if (this.nextLinearAfterBlock.Count > 0)
                            NextInLinearBlock = this.nextLinearAfterBlock;
                        else
                            NextInLinearBlock = new List<LinkerEdge> { new LinkerEdge(linker.GetExitNode(), false) };

                    return NextInLinearBlock;
                }

                /// <summary>
                /// Обновляем поле nextLinearAfterBlock. Если есть новые элементы, то добавляем их и ставим себя на обработку
                /// </summary>
                /// <param name="linearFlow"></param>
                /// <param name="linker"></param>
                /// <param name="isNeedAnalyse">Если равно false, то вызывающая сторона считает оператор висящим. Если противоположных знаний нет,
                /// то ставить на обработку не следует</param>
                private void UpdateKnowledge(List<LinkerEdge> linearFlow, CatchFinallyStack stack, Node LoopOnBreak, StatementLinker linker, bool isNeedAnalyse)
                {
                    //Смотрим, что нового принёс Update. Это вычитание множеств. Но так как дуги не являются уникальными, то приходится добираться до Node
                    LinkerEdge[] interKnol = linearFlow.FindAll(edge => !nextLinearAfterBlock.Any(e => e.destination == edge.destination)).ToArray();

                    bool isStackUpdated = cfs.CheckUpdate(stack);

                    if (interKnol.Length == 0 && !isStackUpdated && this.LoopOnBreak == LoopOnBreak) //Если ничего нового, то и делать ничего не будем
                        return;

                    if (this.LoopOnBreak != null && this.LoopOnBreak != LoopOnBreak) //Такого случиться не должно
                        throw new Store.Const.StorageException("UpdateNextLinearAfterBlock - непредвиденная ситуация 2");

                    //Добавляем Update к себе
                    nextLinearAfterBlock.AddRange(interKnol);
                    this.cfs = stack;
                    this.LoopOnBreak = LoopOnBreak;

                    //Раз изменились, то надо провести своё обновление
                    if (isNeedAnalyse || this.isNeedAnalyse)
                        linker.Enqueue(this);
                }
            }




            System.Collections.Generic.Dictionary<Node, HashSet<Edge>> controlIndex = new Dictionary<Node, HashSet<Edge>>();
            /// <summary>
            /// Построение связей по управлению внутри функции для функции, переданной как параметр
            /// </summary>
            /// <param name="f">Функция, для которой строятся связи</param>
            public void buildNodes(IFunction f)
            {
                this.function = f;

                //Берем корневой элемент. Метод GetNode автоматически проставит его в очередь на анализ
                GetNode(f.EntryStatement, true);

                //Очистка индекса IStatement.NextInControl производится при создании Node в методе GetNode(). Таким образом
                //точечно очищаем все интересующие нас операторы

                while (queue.Count != 0) //Работаем, пока очередь обработки не станет пустой
                {
                    //Берем из очереди очередной элемент
                    Node cur = queue.Dequeue();

                    HashSet<Edge> set;
                    if(!controlIndex.TryGetValue(cur, out set)) //Контролируем, что этот оператор есть в индексе
                    {
                        set = new HashSet<Edge>();
                        controlIndex.Add(cur, set);
                    }

                    foreach (Edge next in cur.NextStatements(this)) //запрашиваем у ноды все операторы, которые могут оказаться последующими
                    {
                        if (next.destination == null)
                            throw new Store.Const.StorageException("StatementLinker.buildNodes. Непредвиденное значение");
                        set.Add(next); //и добавляем их в индекс
                    }
                }

                foreach (KeyValuePair<Node, HashSet<Edge>> pair in controlIndex)  //Используем индекс, чтобы заполнить все связи
                    foreach (Edge next in pair.Value)
                        (pair.Key.elem as Statement_Impl).AddNextInControl(next);
            }

            /// <summary>
            /// Получить Node, соответствующий оператору
            /// Если такого Node еще не было, то он будет создан
            /// Если оператор задействован в потоке управления (не висящий, мертвый код), то он будет автоматически поставлен в очередь на анализ
            /// </summary>
            /// <param name="statement"></param>
            /// <param name="isNeedAnalyse">true - если код не висящий, false - висящий</param>
            /// <returns></returns>
            private Node GetNode(IStatement statement, bool isNeedAnalyse)
            {
                Node result;

                //ищем уже знакомый
                if (nodes.TryGetValue(statement, out result))
                    return result;

                //такой оператор мы еще не видели
                result = new Node();
                result.elem = statement;
                nodes.Add(statement, result);

                //Новый элемент обязательно добавляем в очередь на обработку
                if (isNeedAnalyse)
                {
                    queue.Enqueue(result);
                    (statement as Statement_Impl).EraseNextInControl();
                    result.isNeedAnalyse = true;
                }
                else
                    result.isNeedAnalyse = false;

                return result;
            }

            /// <summary>
            ////Если элемент еще не в очереди на обработку, то надо его добавить
            /// </summary>
            /// <param name="node"></param>
            void Enqueue(Node node)
            {
                if (!queue.Contains(node))
                {
                    node.isNeedAnalyse = true;
                    queue.Enqueue(node);
                }
            }

            Node GetExitNode()
            {
                return GetNode(function.ExitStatement, false);
            }
        }
    }
}
