﻿using System;
using System.Collections.Generic;
using System.Linq;
using Store.Table;

namespace Store
{
    /// <summary>
    /// Тип оператора
    /// </summary>
    public enum ENStatementKind
    {
        /// <summary>
        /// Оператор прерывания цикла или switch
        /// <see cref="Store.IStatementBreak"/>
        /// </summary>
        STBreak,
        /// <summary>
        /// Оператор перехода к следующей итерации цикла
        /// <see cref="Store.IStatementContinue"/>
        /// </summary>
        STContinue,
        /// <summary>
        /// Оператор цикла типа do {} while [условие] или типа while [условие] do {}
        /// <see cref="Store.IStatementDoAndWhile"/>
        /// </summary>
        STDoAndWhile,
        /// <summary>
        /// Оператор цикла for
        /// <see cref="Store.IStatementFor"/>
        /// </summary>
        STFor,
        /// <summary>
        /// Оператор цикла foreach
        /// <see cref="Store.IStatementForEach"/>
        /// </summary>
        STForEach,
        /// <summary>
        /// Оператор goto
        /// <see cref="Store.IStatementGoto"/>
        /// </summary>
        STGoto,
        /// <summary>
        /// Оператор if-then-else
        /// <see cref="Store.IStatementIf"/>
        /// </summary>
        STIf,
        /// <summary>
        /// Оператор, состоящий из операций. То есть обычный математический и т.п., но не управляющий
        /// <see cref="Store.IStatementOperational"/>
        /// </summary>
        STOperational,
        /// <summary>
        /// Оператор возврата из функции
        /// <see cref="Store.IStatementReturn"/>
        /// </summary>
        STReturn,
        /// <summary>
        /// Оператор switch
        /// <see cref="Store.IStatementSwitch"/>
        /// </summary>
        STSwitch,
        /// <summary>
        /// Оператор вызова исключения
        /// <see cref="Store.IStatementThrow"/>
        /// </summary>
        STThrow,
        /// <summary>
        /// Оператор try-catch-finally
        /// <see cref="Store.IStatementTryCatchFinally"/>
        /// </summary>
        STTryCatchFinally,
        /// <summary>
        /// Оператор using
        /// <see cref="Store.IStatementUsing"/>
        /// </summary>
        STUsing,
        /// <summary>
        /// Оператор yieldReturn
        /// <see cref="Store.IStatementYieldReturn"/>
        /// </summary>
        STYieldReturn,
        /// <summary>
        /// Оператор, который ставится как опознавание общей точки выхода из функции
        /// </summary>
        STExit
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IStatement : IDataObjectLeader
    {
        /// <summary>
        /// Возвращает тип данного оператора
        /// </summary>
        ENStatementKind Kind { get; }

        /// <summary>
        /// Перечисляет все операторы, вложенные в данный
        /// </summary>
        /// <returns></returns>
        IEnumerable<IStatement> SubStatements();

        /// <summary>
        /// Оператор, в который вложен данный оператор.
        /// null, если оператор находится в блоке, с которого начинается выполнение функции.
        /// </summary>
        IStatement AboveStatement { get; }

        /// <summary>
        /// Оператор, следующий после данного в линейном блоке. Если оператор последний - то содержит null.
        /// Линейным блоком является последовательность 
        /// операторов, записанных один за другим, выполнение которых происходит последовательно один за другим.
        /// </summary>
        IStatement NextInLinearBlock { get; set; }
        /// <summary>
        /// Оператор, предшествующий данному в линейном блоке. 
        /// Если данный оператор первый, то содержит null.
        /// </summary>
        IStatement PreviousInLinearBlock { get; }

        /// <summary>
        /// Местоположение в исходных текстах, в котором находится первый символ оператора
        /// </summary>
        Location FirstSymbolLocation { get; }

        /// <summary>
        /// Местоположение в исходных текстах, в котором находится последний символ оператора
        /// </summary>
        Location LastSymbolLocation { get; }

        /// <summary>
        /// Изменить местоположение первого символа оператора в исходных текстах.
        /// </summary>
        /// <param name="file">Файл исходных текстов.</param>
        /// <param name="line">Номер строки в файле. Строка не может быть нулевой. Если будет указано нулевой номер строки, то будет выдана ошибка времени выполнения</param>
        /// <param name="column">Номер столбца в файле.</param>
        /// <returns></returns>
        void SetFirstSymbolLocation(IFile file, UInt64 line, UInt64 column);

        /// <summary>
        /// Изменить местоположение первого символа оператора в исходных текстах.
        /// </summary>
        /// <param name="file">Файл исходных текстов.</param>
        /// <param name="Offset">Смещение от начала файла. Смещение не может быть нулевым. Если будет указано нулевое смещение, будет выдана ошибка времени выполнения</param>
        /// <returns></returns>
        void SetFirstSymbolLocation(IFile file, UInt64 Offset);

        /// <summary>
        /// Изменить местоположение последнего символа оператора в исходных текстах.
        /// </summary>
        /// <param name="file">Файл исходных текстов.</param>
        /// <param name="line">Номер строки в файле. Строка не может быть нулевой. Если будет указано нулевой номер строки, то будет выдана ошибка времени выполнения</param>
        /// <param name="column">Номер столбца в файле.</param>
        /// <returns></returns>
        void SetLastSymbolLocation(IFile file, UInt64 line, UInt64 column);

        /// <summary>
        /// Изменить местоположение последнего символа оператора в исходных текстах.
        /// </summary>
        /// <param name="file">Файл исходных текстов.</param>
        /// <param name="Offset">Смещение от начала файла. Смещение не может быть нулевым. Если будет указано нулевое смещение, будет выдана ошибка времени выполнения</param>
        /// <returns></returns>
        void SetLastSymbolLocation(IFile file, UInt64 Offset);

        /// <summary>
        /// Возвращает датчик, установленный перед данным оператором
        /// </summary>
        Sensor SensorBeforeTheStatement { get; }

        /// <summary>
        /// Указывает функцию, в тело которой входит данный оператор
        /// </summary>
        IFunction ImplementsFunction { get; }

        /// <summary>
        /// Указывает идентификатор функции, в тело которой входит данный оператор
        /// </summary>
        UInt64 ImplementsFunctionId { get; }

        /// <summary>
        /// Удаляет данный оператор и все подчинённые (вложенные в него по структуре программы) операторы из хранилища.
        /// Функция может быть плохо отлажена, необходимо тщательно тестировать использующий её код. Возможно, какие-то индексы не очищаются.
        /// </summary>
        void RemoveMe();

        /// <summary>
        /// Возвращает список операторов, в которые может перейти управление из данного оператора
        /// </summary>
        /// <param name="isWithoutReverse">Если true, то будет проскакивать вглубь обратных дуг. Например, для кода
        /// for(something)
        /// { op1;}
        /// 
        /// op1.NextInControl(true) будет выдавать не for, а сам op1 и иператор, следующий за for 
        /// </param>
        /// <returns></returns>
        IEnumerable<IStatement> NextInControl(bool isWithoutReverse);

        /// <summary>
        /// Возвращает список функций, которые могут вызываться непосредственно этим оператором. Последовательность выдачи не определена. При выполнении оператора данные функции не обязательно будут вызваны, 
        /// они могут быть вызваны
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunction> CallesFunctions();
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementBreak : IStatement
    {
        /// <summary>
        /// Цикл или StatementSwitch, чье выполнение прерывает данный оператор
        /// </summary>
        IStatement BreakingLoop { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementContinue : IStatement
    {
        /// <summary>
        /// Цикл, чью итерацию начинает данный оператор
        /// </summary>
        IStatement ContinuingLoop { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementDoAndWhile : IStatement
    {
        /// <summary>
        /// Первый оператор тела цикла
        /// </summary>
        IStatement Body { get; set; }

        /// <summary>
        /// Исловие цикла
        /// </summary>
        IOperation Condition { get; set; }

        /// <summary>
        /// Определяет тип цикла. 
        /// Если true, то условие цикла бует проверять перед выполнением первого прохода.
        /// Если false - не будет.
        /// </summary>
        bool IsCheckConditionBeforeFirstRun { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementGoto : IStatement
    {
        /// <summary>
        /// Оператор, к которому выполняется переход
        /// </summary>
        IStatement Destination { get; set; }
        /// <summary>
        /// Выражение, которое вычисляется для перехода в одну из меток
        /// </summary>
        IOperation Condition { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementFor : IStatement
    {
        /// <summary>
        /// Операция, выполняемая перед началом выполнения цикла
        /// </summary>
        IOperation Start { get; set; }
        /// <summary>
        /// Условие цикла. Выполняется после первого выполнения тела цикла
        /// </summary>
        IOperation Condition { get; set; }
        /// <summary>
        /// Операция, выполняемая после каждой итерации цикла
        /// </summary>
        IOperation Iteration { get; set; }
        /// <summary>
        /// Первый оператор блока тела цикла.
        /// </summary>
        IStatement Body { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementForEach : IStatement
    {
        /// <summary>
        /// Заголовок цикла
        /// </summary>
        IOperation Head { get; set; }
        /// <summary>
        /// Первый оператор тела цикла
        /// </summary>
        IStatement Body { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementIf : IStatement
    {
        /// <summary>
        /// 
        /// </summary>
        IStatement ThenStatement { get; set; }
        /// <summary>
        /// 
        /// </summary>
        IStatement ElseStatement { get; set; }
        /// <summary>
        /// 
        /// </summary>
        IOperation Condition { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementOperational : IStatement
    {
        /// <summary>
        /// Операция, выполняемая оператором
        /// </summary>
        IOperation Operation { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementReturn : IStatement
    {
        /// <summary>
        /// Операция, вычисляющая возвращаемое значение.
        /// </summary>
        IOperation ReturnOperation { get; set; }
    }

    /// <summary>
    /// Оператор, соответствующий операции yield return. Каким образом выполнять обход потока управления с таким оператором - не придумано.
    /// Семантика оператора полностью соответствует семантике одноимённого оператора из С#.
    /// </summary>
    public interface IStatementYieldReturn : IStatement
    {
        /// <summary>
        /// Операция, вычисляющая возвращаемое yield return значение.
        /// </summary>
        IOperation YieldReturnOperation { get; set; }
    }

    /// <summary>
    /// Виды поведения оператора Switch
    /// <remarks>
    /// Подробнее о поведении оператора Switch смотри <see cref="Store.IStatementSwitch"/>
    /// </remarks>
    /// </summary>
    [Flags]
    public enum enSwitchTypes
    {
        /// <summary>
        /// Оператор ведет себя аналогично тому, как ведет себя switch в языке C++.
        /// <remarks>
        /// Подробнее о поведении оператора Switch смотри <see cref="Store.IStatementSwitch"/>
        /// </remarks>
        /// </summary>
        LIKE_CPP = 1,
        /// <summary>
        /// Оператор ведет себя аналогично тому, как ведет себя в языке PHP.
        /// <remarks>
        /// Подробнее о поведении оператора Switch смотри <see cref="Store.IStatementSwitch"/>
        /// </remarks>
        /// </summary>
        LIKE_PHP
    }

    /// <summary>
    /// <para>Оператор switch.</para>
    /// <remarks>
    /// <para>Семантика оператора:</para>
    /// <code>
    /// switch(condition)
    /// {
    ///     case operation1: statement1;
    ///     case operation2: statement2;
    ///     default: statement3;
    /// }
    /// </code>
    /// <para>Вначале выяисляется значение <c>condition</c>. После этого вычисляется значение <c>operation1</c>. В зависимости от того, какой тип оператора (<see cref="Store.IStatementSwitch.SwitchType"/>) производится следующее. </para>
    /// <para>Если тип <see cref="Store.enSwitchTypes.LIKE_CPP"/>, то производится сравнение полученного занчение с ранее вычисленным значением <c>condition</c>. Если совпало, то выполняется statement1.</para>
    /// <para>Если тип оператора <see cref="Store.enSwitchTypes.LIKE_PHP"/>, то проверяется тип полученного значения. Если тип boolean, то в случае, если значение true, то выполняется statement1. При этом выяисленное значение
    /// condition не рассматривается. Если же тип другой, то выполняется те же действия, что и для <see cref="Store.enSwitchTypes.LIKE_CPP"/>.</para>
    /// <para>После отработки первого case идет переход ко второму, который обрабатывается аналогичным образом. Однако в случае, если производилось выполнение statement1, то он мог вызвать обрыв выполнения switch путём
    /// выполнения break или другого управляющего действия (throw, return, goto и т.п.). </para>
    /// <para>В случае, когда ни один case не подошел, выполняется оператор statement3, находящийся в default-ветке.</para>
    /// </remarks>
    /// </summary>
    public interface IStatementSwitch : IStatement
    {
        /// <summary>
        /// Тип поведения оператора Switch. В разных языках программирования данный оператор выдет себя по-своему.
        /// </summary>
        enSwitchTypes SwitchType { get; set; }

        /// <summary>
        /// Условие ветвления
        /// </summary>
        IOperation Condition { get; set; }

        /// <summary>
        /// Перечень всех ветвей, кроме default
        /// Возвращаются операторы-условия и первые операторы ветвей.
        /// 
        /// Предполагается, что когда исполняется какой-то case и управление проскакивает в следующий (нет break), то этим следующим оператором является 
        /// следующий, выдаваемый данной функцией
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        IEnumerable<KeyValuePair<IOperation, IStatement>> Cases();

        /// <summary>
        /// Возвращает ветвь, исполняемую в случае, когда все остальные ветви не подхлодят. Предполагается, что если выполнение проскакивает из
        /// одного оператора в другой насквозь, то этот оператор выполняется последним
        /// </summary>
        /// <returns>Первый оператор ветви по умолчанию</returns>
        IStatement DefaultCase();

        /// <summary>
        /// Указать ветвь, исполняемую в случае, когда все остальные ветви не подхлодят
        /// </summary>
        /// <param name="statement">JВыполняемый оператор.</param>
        void AddDefaultCase(IStatement statement);

        /// <summary>
        /// Добавить ветвление.
        /// Вызывать данный метод следует в строго определённом порядке. Если выполняется некотый case1 и управление из него проскакивает в 
        /// следующий case2 (нет break), то данный метод должен быть вызван сначала для case1, потом для case2, причем между ними не должно быть вызовов с
        /// другими case.
        /// </summary>
        /// <param name="operation">Операция, возвращающее условие выполнения ветви. Подробнее смотри <see cref="Store.IStatementSwitch"/></param>
        /// <param name="statement">Оператор, с которого начинается выполнение ветви</param>
        void AddCase(IOperation operation, IStatement statement);
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementThrow : IStatement
    {
        /// <summary>
        /// Исключение, вырабатываемое оператором.
        /// </summary>
        IOperation Exception { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementTryCatchFinally : IStatement
    {
        /// <summary>
        /// Первый оператор блока try
        /// </summary>
        IStatement TryBlock { get; set; }

        /// <summary>
        /// Перечень первых операторов блоков catch
        /// </summary>
        /// <returns></returns>
        IEnumerable<IStatement> Catches();
        
        /// <summary>
        /// Добавить блок catch
        /// </summary>
        /// <param name="stCatch">Оператор, с которого блок начинается</param>
        void AddCatch(IStatement stCatch);

        /// <summary>
        /// Первый оператор блока finally
        /// </summary>
        IStatement FinallyBlock { get; set; }

        /// <summary>
        /// Первый оператор блока except
        /// </summary>
        IStatement ElseBlock { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IStatementUsing : IStatement
    {
        /// <summary>
        /// Операция указания 
        /// </summary>
        IOperation Head { get; set; }
        /// <summary>
        /// 
        /// </summary>
        IStatement Body { get; set; }
    }

    /// <summary>
    /// Оператор, являющийся единой точкой выхода из функции. Не соответствует никакому оператору языка программирования.
    /// Является технологическим оператором, добавляемым в каждую функцию. Является необходимым для опознавания выхода из функции в случае, 
    /// когда он достигается естественным потоком управления. Между тем любой return также ведёт в данный оператор
    /// </summary>
    public interface IStatementExit : IStatement
    {

    }

    /// <summary>
    /// Дуга перехода NextInControl
    /// </summary>
    internal struct Edge
    {

        internal Edge(IStatement destination, bool isReverse)
        {
            this.destination = destination;
            this.isReverse = isReverse;
        }

        internal Edge(Statement_Impl.StatementLinker.Node node, bool isReverse)
        {
            this.destination = node.elem;
            this.isReverse = isReverse;
        }

        internal Edge(Statement_Impl.StatementLinker.LinkerEdge edge)
        {
            this.destination = edge.destination.elem;
            this.isReverse = edge.isReverse;
        }

        /// <summary>
        /// Цель, куда ведёт дуга
        /// </summary>
        internal IStatement destination;

        /// <summary>
        /// true - если дума обратная. То есть ведёт из конца цикла в его голову
        /// 
        /// Построение данных дуг в первую очередь ориентировано на то, может ли при данном переходе сработать датчик, стоящий перед destination. 
        /// </summary>
        internal bool isReverse;

        internal void Save(IBufferWriter buf)
        {
            buf.Add(destination.Id);
            buf.Add(isReverse);
        }

        internal void Load(IBufferReader buf, Storage storage)
        {
            UInt64 Id = buf.GetUInt64();
            destination = storage.statements.GetStatement(Id);
            isReverse = false;
            buf.GetBool(ref isReverse);
        }

        public override int GetHashCode()
        {
            return destination.Id.GetHashCode() + isReverse.GetHashCode();
        }
    }

    internal abstract partial class Statement_Impl: Store.BaseDatatypes.DataObjectLeader, IStatement
    {
        protected Statements statements;

        protected Store.BaseDatatypes.RemovedCheck check = new BaseDatatypes.RemovedCheck();
        
        internal Statement_Impl(Statements stat)
        {
            this.statements = stat;
        }

        #region Statement Members

        public abstract ENStatementKind Kind { get; }
        /// <summary>
        /// Перечисляет все подоператоры данного оператора (только непосредственно этого оператора, рекурсивно не лезет)
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerable<IStatement> SubStatements();
        /// <summary>
        /// Перечисляет все подоперации данного оператора (только непосредственно этого оператора, рекурсивно не лезет)
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerable<IOperation> SubOperations();

        public IStatement AboveStatement 
        { 
            get
            {
                check.Check();

                UInt64 resId = statements.IndexAboveStatements.GetValueByID(theID);
                return statements.GetStatement(resId);
            }
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        public IStatement NextInLinearBlock
        {
            get
            {
                check.Check();

                UInt64 nextId = statements.TableBlockingForward.GetValueByKey(theID);
                if (nextId != Store.Const.CONSTS.WrongID)
                    return statements.GetStatement(nextId);
                else
                    return null;
            }
            set
            {
                check.Check();

                //Смотрим, был ли ранее установлен следующий оператор
                UInt64 nextId = statements.TableBlockingForward.GetValueByKey(theID);
                if (nextId != Store.Const.CONSTS.WrongID) //тогда очищаем индексы
                {
                    (statements.GetStatement(nextId) as Statement_Impl).EraseStatementAbove();
                }

                if (value != null) //Если устанавлнивается новое значение
                {
                    //Сохранить направления
                    UInt64 valueId = (value as Statement_Impl).theID;
                    statements.TableBlockingForward.Put(theID, valueId);
                    statements.IndexBlockingBackward.Put(valueId, theID);

                    //Обновить индекс вышестоящих операторов
                    UInt64 aboveStatementId = statements.IndexAboveStatements.GetValueByID(theID);
                    PutAboveRecursive(statements.GetStatement(aboveStatementId), this.ImplementsFunction, value, (st) => { });
                }
                else if (nextId != Store.Const.CONSTS.WrongID) //Если для данного оператора просто обрезается цепочка следования
                {   //Обрезаем цепочку
                    statements.TableBlockingForward.Remove(theID);
                    statements.IndexBlockingBackward.Remove(nextId);
                }
            }
        }

        /// <summary>
        /// Используется, когда текущий оператор перестаёт иметь AboveStatement, либо когда от перестаёт быть EntryStatement в функции
        /// Убирает AboveStatement у текущего оператора и NextInLinearBlock
        /// Убирает ImplementsInFunction у всего дерева, растущего из текущего оператора, в том числе NextInLinearBlock
        /// Убирает из индекса вызовов функций все вызовы, находящиеся в дереве, растущем из текущего оператора, в том числе NextInLinearBlock
        /// </summary>
        private void EraseStatementAbove()
        {
                if (ImplementsFunction != null)

                //Здесь решаем сразу много задач:
                //1. Убираем для старых подчинённых операторов ссылку на реализующую их функцию
                //2. Убираем соответствующие связи в индексе вызовов
                PutAboveRecursive(
                    null,
                    this,
                    //Удалятель из индекса связи вызова по функциям
                    (statement) => statement.EraseOperationsFunctionCalls(statement.CallesFunctions())
                );

            else
                PutAboveRecursive(null, this);
        }

        /// <summary>
        /// Удаляет из индекса вызовов функции вызовы функций functions
        /// Используется, когда оператор прекращает реализовывать функцию. И когда операция прекращает реализовывать оператор.
        /// </summary>
        /// <param name="functions"></param>
        protected void EraseOperationsFunctionCalls(IEnumerable<IFunction> functions) 
        {
            if (ImplementsFunction != null)
            {
                //для каждого вызова из данной функции
                IFunctionCall[] c = ImplementsFunction.Calles()
                    //Выбрать все вызовы, которые выполняются в данном операторе
                                        .Where((functionCall) =>
                                        {
                                            //Вызов выполняется в данном операторе, если совпадают вызываемая функция и Location
                                            return FirstSymbolLocation == functionCall.CallLocation && functions.Contains(functionCall.Calles);
                                        })
                                        .ToArray();

                //Удалить все такие FunctionCall
                Array.ForEach(c, (functionCall) => { (functionCall as FunctionCall_Impl).Remove(); });
            }
        }

        /// <summary>
        /// Удаляет из индекса все вызовы функций, где вызываемая функция -  которая реализуется текщим оператором, и вызывающая вызывается из операции op.
        /// При этом Location обязательно совпадает.
        /// </summary>
        /// <param name="op"></param>
        protected void EraseOperationsFunctionCalls(IOperation op)
        {
            if (op != null)
            {
                (op as Operation_Impl).ImplementsInStatement = null;

                EraseOperationsFunctionCalls(op.SequentiallyCallsFunctions().SelectMany(f => f));
            }
        }
        
        /// <summary>
        /// Установить оператор, в которой вложен данный. 
        /// Заодно для подчинённых операторов устанавливает функцию, в которой оператор реализован
        /// Заодно добавляются связи в индекс вызовов функций
        /// </summary>
        /// <param name="aboveStatement">Оператор, у которого появляется новый подчинённый оператор</param>
        /// <param name="bottomStatement">Оператор, который становится подчинённым</param>
        internal void PutAboveRecursive(IStatement aboveStatement, IStatement bottomStatement, IFunction aboveFunction = null)
            {
            if (aboveStatement != null)
                PutAboveRecursive(aboveStatement, aboveStatement.ImplementsFunction, bottomStatement, (st) => { });
            else
                PutAboveRecursive(aboveStatement, aboveFunction, bottomStatement, (st) => { });
        }

        public delegate void StatementHandler(Statement_Impl statement);

        /// <summary>
        /// Установить оператор, в которой вложен данный. 
        /// Заодно для подчинённых операторов устанавливает функцию, в которой оператор реализован
        /// Заодно добавляются связи в индекс вызовов функций
        /// </summary>
        /// <param name="aboveStatement">Оператор, у которого появляется новый подчинённый оператор</param>
        /// <param name="bottomStatement">Оператор, который становится подчинённым</param>
        /// <param name="handler">Функция, вызываемая для каждого оператора, для которого выполняется обход</param>
        internal void PutAboveRecursive(IStatement aboveStatement, IStatement bottomStatement, StatementHandler handler)
        {
            if (aboveStatement != null)
                PutAboveRecursive(aboveStatement, aboveStatement.ImplementsFunction, bottomStatement, handler);
            else
                PutAboveRecursive(aboveStatement, null, bottomStatement, handler);

        }

        /// <summary>
        /// Установить оператор, в который вложен данный.
        /// Заодно для подчинённых операторов устанавливает функцию, в которой оператор реализован
        /// Заодно добавляются связи в индекс вызовов функций
        /// </summary>
        /// <param name="aboveStatement">Оператор, в который вложен оператор value</param>
        /// <param name="aboveFunction">Функция, которую отныне реализуют bottomStatement. Задание имеет смысл, так как в случае aboveStatement == null, при 
        /// усиановки EntryStatement, функция имеет место быть</param>
        /// <param name="bottomStatement">Оператор, которому надо установить значения</param>
        internal void PutAboveRecursive(IStatement aboveStatement, IFunction aboveFunction, IStatement bottomStatement)
            {
            PutAboveRecursive(aboveStatement, aboveFunction, bottomStatement, (st) => { });
        }        

        /// <summary>
        /// Установить оператор, в который вложен данный.
        /// Заодно для подчинённых операторов устанавливает функцию, в которой оператор реализован
        /// Заодно добавляются связи в индекс вызовов функций
        /// </summary>
        /// <param name="aboveStatement">Оператор, в который вложен оператор value</param>
        /// <param name="aboveFunction">Функция, которую отныне реализуют bottomStatement. Задание имеет смысл, так как в случае aboveStatement == null, при 
        /// усиановки EntryStatement, функция имеет место быть</param>
        /// <param name="bottomStatement">Оператор, которому надо установить значения</param>
        /// <param name="handler">Обработчик, который будет вызван для каждого оператора, подчинённого или следующего за данным, но только если поменялась реализующая 
        /// функция</param>
        internal void PutAboveRecursive(IStatement aboveStatement, IFunction aboveFunction, IStatement bottomStatement, StatementHandler handler)
        {
            if (bottomStatement != null)
            {
                UInt64 aboveStatementId = aboveStatement == null ? Store.Const.CONSTS.WrongID : aboveStatement.Id;

                //Удаляем из таблицы предыдущее указание верхнего оператора
                statements.TableImplementsInFunction.Remove(bottomStatement.Id);

                //Задаём новое значение
                if( aboveStatement != null)
                    statements.IndexAboveStatements.Put(bottomStatement.Id, aboveStatementId);

                //Устанавливаем функцию, к которой принадлежат подчинённые операторы
                //Напрямую вызывать функцию SetImplementsFunction для bottomStatement не следует, так как она полезет работать со всей цепочкой, после чего 
                //SetImplementsFunction будет вызвана повторно в следующей интерации
                if ((aboveFunction == null && this.ImplementsFunctionId == Store.Const.CONSTS.WrongID) || bottomStatement.ImplementsFunctionId != aboveFunction.Id)
                    (bottomStatement as Statement_Impl).SetImplementsMyFunction((Function_Impl)aboveFunction, handler);
                //Обрабатываем все подчинённые операторы
                foreach (IStatement st in bottomStatement.SubStatements())
                    (st as Statement_Impl).SetImplementsFunction((Function_Impl) aboveFunction, handler);

                //Продолжаем рекурсивно устанавливать окружающий оператор для всей цепочки.
                PutAboveRecursive(aboveStatement, bottomStatement.NextInLinearBlock, aboveFunction);
            }
        }

        /// <summary>
        /// Устанавливаем функцию, в которую вложен данный оператор.
        /// установка производится для всех подчиненных операторов
        /// Заодно добавляются связи в индекс вызовов функций
        /// </summary>
        /// <param name="implementsFunction"></param>
        /// <param name="handler">Обработчик, который будет вызван для каждого оператора, подчинённого или следующего за данным, но только если поменялась реализующая 
        /// функция</param>
        internal void SetImplementsFunction(Function_Impl implementsFunction, StatementHandler handler)
        {
            if ((implementsFunction == null && this.ImplementsFunctionId == Store.Const.CONSTS.WrongID) || (implementsFunction != null && this.ImplementsFunctionId == implementsFunction.Id))
                return;

            SetImplementsFunctionIdRecursive(implementsFunction, handler);
        }

        /// <summary>
        /// Устанавливаем функцию, в которую вложен данный оператор.
        /// установка производится для всех подчиненных операторов
        /// Заодно добавляются связи в индекс вызовов функций
        /// Данная функция используется только в функции <see cref="Store.Satement_Impl.SetImplementsFunction(Store.Function_Impl, Store.Satement_Impl.StatementHandler)"/>
        /// </summary>
        /// <param name="implementsFunction"></param>
        /// <param name="handler">Обработчик, который будет вызван для каждого оператора, подчинённого или следующего за данным</param>
        private void SetImplementsFunctionIdRecursive(Function_Impl implementsFunction, StatementHandler handler)
        {
            SetImplementsMyFunction(implementsFunction, handler);

            //Делаем то же самое для подчинённых операторов
            foreach (IStatement st in SubStatements())
                (st as Statement_Impl).SetImplementsFunctionIdRecursive(implementsFunction, handler);

            IStatement s = NextInLinearBlock;
            if(s!= null)
                (s as Statement_Impl).SetImplementsFunctionIdRecursive(implementsFunction, handler);
        }

        /// <summary>
        /// Заносит для в данный оператор, какую функцию он реализует. Значения подчинённых операторов не затрагивается.
        /// Заодно добавляются связи в индекс вызовов функций
        /// </summary>
        /// <param name="implementsFunction">Функция, в которой реализован данный оператор</param>
        /// <param name="handler">Обработчик, вызываемый для данного оператора</param>
        private void SetImplementsMyFunction(Function_Impl implementsFunction, StatementHandler handler)
        {
            //Заносим указание на реализующую функцию
            if (implementsFunction != null)
                statements.TableImplementsInFunction.PutUInt64Value(theID, implementsFunction.Id);
            else
                statements.TableImplementsInFunction.PutUInt64Value(theID, Store.Const.CONSTS.WrongID);

            //Добавляем в индекс вызовов функций
            SetupMyFunctionCalles(implementsFunction);

            //Вызываем обработчик для оператора
            handler(this);
        }

        public IStatement PreviousInLinearBlock
        {
            get 
            {
                check.Check();

                UInt64 prevId = statements.IndexBlockingBackward.GetValueByID(theID);
                return statements.GetStatement(prevId);
            }
        }

        public Location FirstSymbolLocation
        {
            get 
            {
                check.Check();

                Location locs = statements.GetFirstSymbolLocations().GetObjectLocation(theID);
                if (locs != null)
                    return locs;
                else
                    return null;
            }
        }

        public Location LastSymbolLocation
        {
            get
            {
                check.Check();

                Location locs = statements.GetLastSymbolLocations().GetObjectLocation(theID);
                if (locs != null)
                    return locs;
                else
                    return null;
            }
        }


        public void SetFirstSymbolLocation(IFile file, ulong line, ulong column)
        {
            check.Check();

            if (line == 0)
                throw new Exception("Некорректный аргумент Statement_Impl.SetFirstSymbolLocation(IFile file, ulong line, ulong column). line == 0");

            statements.GetFirstSymbolLocations().SetSingleLocation(theID, file, line, column, null);
        }

        public void SetFirstSymbolLocation(IFile file, ulong offset)
        {
            check.Check();

            if (offset == 0)
                throw new Exception("Некорректный аргумент Statement_Impl.SetFirstSymbolLocation(IFile file, ulong offset). offset == 0");

            statements.GetFirstSymbolLocations().SetSingleLocation(theID, file, offset, null);
        }

        public void SetLastSymbolLocation(IFile file, ulong line, ulong column)
        {
            check.Check();

            if (line == 0)
                throw new Exception("Некорректный аргумент Statement_Impl.SetLastSymbolLocation(IFile file, ulong line, ulong column). line == 0");

            statements.GetLastSymbolLocations().SetSingleLocation(theID, file, line, column, null);
        }

        public void SetLastSymbolLocation(IFile file, ulong offset)
        {
            check.Check();

            if (offset == 0)
                throw new Exception("Некорректный аргумент Statement_Impl.SetLastSymbolLocation(IFile file, ulong offset). offset == 0");

            statements.GetLastSymbolLocations().SetSingleLocation(theID, file, offset, null);
        }

        public Sensor SensorBeforeTheStatement
        { 
            get 
            {
                check.Check();

                UInt64 sensorId = statements.storage.sensors.indexBeforeStatements.GetValueByID(theID);
                
                if (sensorId == Store.Const.CONSTS.WrongID)
                    return null;
                else
                    return statements.storage.sensors.GetSensor(sensorId);
            } 
        }

        public IFunction ImplementsFunction 
        { 
            get
            {
                check.Check();

                UInt64 funcId = statements.TableImplementsInFunction.GetUInt64ValueByID(theID);
                if (funcId != Store.Const.CONSTS.WrongID)
                    return statements.storage.functions.GetFunction(funcId);
                else
                    return null;
            }
        }

        public UInt64 ImplementsFunctionId
        {
            get
            {
                check.Check();

                UInt64 funcId = statements.TableImplementsInFunction.GetUInt64ValueByID(theID);
                return funcId;
            }
        }

        public virtual void RemoveMe()
        {
            check.Check();

            //Очищаем основную таблицу
            statements.TableLeader.Remove(theID);

            //Очищаем последователей, предшественников
            UInt64 nextId = statements.TableBlockingForward.GetValueByKey(theID);
            UInt64 prevId = statements.IndexBlockingBackward.GetValueByID(theID);
            statements.TableBlockingForward.Put(prevId, nextId);
            statements.IndexBlockingBackward.Put(nextId, prevId);

            //Очищаем данные
            statements.TableStatementsData.Remove(theID);

            statements.TableStatementsNextInControl.RemoveAllForKey(theID);

            statements.TableStatementsAdditionalData.RemoveAllForKey(theID);

            statements.TableImplementsInFunction.Remove(theID);

            statements.IndexAboveStatements.Remove(theID);

            check.SetRemoved();
        }
        
        public IEnumerable<IStatement> NextInControl(bool isWithoutReverse)
        {
            if (this.ImplementsFunction != null && this.ImplementsFunction.StatementLinked == enFunctionStatementLinked.NOT_PROCESSED)
            {
                StatementLinker stLiner = new StatementLinker();
                stLiner.buildNodes(this.ImplementsFunction);
                this.ImplementsFunction.StatementLinked = enFunctionStatementLinked.PROCESSED;
            }

            if (isWithoutReverse)
            {
                Queue<IStatement> list = new Queue<IStatement>();
                list.Enqueue(this);

                Edge edge = new Edge();
                while(list.Count > 0)
                    foreach (IBufferReader reader in statements.TableStatementsNextInControl.GetValuesByID(list.Dequeue().Id))
                    {
                        edge.Load(reader, this.statements.storage);

                        if (edge.isReverse)
                            list.Enqueue(edge.destination);
                        else
                            yield return edge.destination;
                    }
            }
            else
                foreach (IBufferReader reader in statements.TableStatementsNextInControl.GetValuesByID(theID))
                {
                    UInt64 nextId = reader.GetUInt64();
                    yield return statements.GetStatement(nextId);
                }

        }

        /// <summary>
        /// Вызывается при обновлении индекса nextInControl для очистки старого значения индекса
        /// </summary>
        internal void EraseNextInControl()
        {
            if (this.ImplementsFunction != null)
                statements.TableStatementsNextInControl.RemoveAllForKey(theID);
        }

        /// <summary>
        /// Добавить  связь NextInControl.
        /// Эту задачу может делать только хранилище при помощи своего алгоритма создания графа потока управления по дереву разбора
        /// </summary>
        /// <param name="edge"></param>
        public void AddNextInControl(Edge edge)
        {
            statements.TableStatementsNextInControl.AddAtId(theID, (buf) => { buf.Add(edge.destination.Id); buf.Add(edge.isReverse); });
        }
        #endregion

        internal delegate void AddCall(IFunction function, Location location);

        /// <summary>
        /// Добавляет в индексы функции все вызовы в данном и подчиненном операторах.
        /// </summary>
        /// <param name="caller"></param>
        internal void CollectFunctionCalles(Function_Impl caller)
        {
            //Добавляем свои вызовы
            SetupMyFunctionCalles(caller);

            //Добавляем вызовы подчинённых операторов
            IStatement stat = this.NextInLinearBlock;
            if(stat != null)
                (stat as Statement_Impl).CollectFunctionCalles(caller);
            
            foreach (IStatement st in SubStatements())
                (st as Statement_Impl).CollectFunctionCalles(caller);
        }

        /// <summary>
        /// Добавляет в индексы функции все вызовы только в данном операторе.
        /// </summary>
        /// <param name="caller"></param>
        protected void SetupMyFunctionCalles(Function_Impl caller)
            {
            if(caller != null)
                foreach (IOperation op in SubOperations())
                    SetupMyOperationsFunctionCalles(caller, op);
        }

        /// <summary>
        /// Добавляет в индексы функции все вызовы в данной операции op текущего оператора.
        /// </summary>
        /// <param name="caller">Функция, реализующая данный оператор</param>
        /// <param name="op">Операциия</param>
        protected void SetupMyOperationsFunctionCalles(Function_Impl caller, IOperation op)
        {
            foreach (IFunction[] arFunc in op.SequentiallyCallsFunctions())
                foreach (IFunction func in arFunc)
                    caller.AddCall_basic(func, this.FirstSymbolLocation);
            }

        /// <summary>
        /// Добавляет в индексы функции все вызовы в данной операции op текущего оператора.
        /// </summary>
        /// <param name="op"></param>
        internal void SetupMyOperationsFunctionCalles(IOperation op)
        {
            if (op != null)
            {
                (op as Operation_Impl).ImplementsInStatement = this;

                if(ImplementsFunction != null)
                    SetupMyOperationsFunctionCalles(ImplementsFunction as Function_Impl, op);
            }
        }

        /// <summary>
        /// Перечисляет все функции, которые вызываются из операций данного оператора (но не его подоператоров)
        /// </summary>
        public IEnumerable<IFunction> CallesFunctions()
        {
            return SubOperations().SelectMany(a => a.SequentiallyCallsFunctions()).SelectMany(a => a).Distinct();

            //foreach (IOperation op in SubOperations())
            //    foreach (Function[] arFunc in op.SequentiallyCallsFunctions())
            //        foreach (Function func in arFunc)
            //            yield return func;

            //yield break;
        }

        /// <summary>
        /// Используется после изменения значения подчиненного оператора
        /// </summary>
        /// <param name="newValue"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        protected void ChangeValue(IStatement oldValue, IStatement newValue)
        {
            if (oldValue == null)
            {
                if (newValue != null)
                    //Устанавливаем верхний оператор для нового значения
                    PutAboveRecursive(this, newValue);
            }
            else if (newValue == null)
            {
                //Старый оператор теперь не имеет верхний оператор
                (oldValue as Statement_Impl).EraseStatementAbove();
            }
            else if (oldValue.Id != newValue.Id)
            {
                    //Старый оператор теперь не имеет верхний оператор
                    (oldValue as Statement_Impl).EraseStatementAbove();

                    //Устанавливаем верхний оператор для нового значения
                    PutAboveRecursive(this, newValue);
            }
        }



    }

    internal sealed class StatementBreak_Impl : Statement_Impl, IStatementBreak
    {
        internal StatementBreak_Impl(Statements statements) : base(statements)
        {}

        public override ENStatementKind Kind
        {
            get 
            {
                check.Check();

                return ENStatementKind.STBreak;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            yield break;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            yield break;
        }

        public override void RemoveMe()
        {
            BreakingLoop.RemoveMe();
            base.RemoveMe();
        }

        #region StatementBreak Members
        UInt64 loopId = Store.Const.CONSTS.WrongID;
        public IStatement BreakingLoop
        {
            get
            {
                check.Check();

                LoadLoopId();

                if (loopId != Store.Const.CONSTS.WrongID)
                    return statements.GetStatement(loopId);
                else
                    return null;
            }
            set
            {
                check.Check();

                if (value == null)
                    statements.TableStatementsData.Remove(theID);
                else
                {
                    loopId = (value as Statement_Impl).Id;

                    //Сохраняем данные
                    statements.TableStatementsData.PutUInt64Value(theID, loopId);
                }
            }
        }

        private void LoadLoopId()
        {
            if (loopId == Store.Const.CONSTS.WrongID)
                loopId = statements.TableStatementsData.GetUInt64ValueByID(theID);
        }

        #endregion
    }

    internal sealed class StatementContinue_Impl : Statement_Impl, IStatementContinue
    {
        internal StatementContinue_Impl(Statements statements)
            : base(statements)
        {}


        public override ENStatementKind Kind
        {
            get 
            {
                check.Check();

                return ENStatementKind.STContinue;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            yield break;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            yield break;
        }

        #region StatementContinue Members
        UInt64 loopId = Store.Const.CONSTS.WrongID;
        public IStatement ContinuingLoop
        {
            get
            {
                check.Check();

                LoadLoopId();

                if (loopId != Store.Const.CONSTS.WrongID)
                    return statements.GetStatement(loopId);
                else
                    return null;
            }
            set
            {
                check.Check();

                if (value == null)
                    statements.TableStatementsData.Remove(theID);
                else
                {
                    loopId = (value as Statement_Impl).Id;

                    //Сохраняем данные
                    statements.TableStatementsData.PutUInt64Value(theID, loopId);
                }
            }
        }
        private void LoadLoopId()
        {
            if (loopId == Store.Const.CONSTS.WrongID)
                loopId = statements.TableStatementsData.GetUInt64ValueByID(theID);
        }
        #endregion
    }


    internal abstract class StatementLoop_Impl : Statement_Impl
    {
        protected StatementLoop_Impl(Statements statements) : base(statements)
        {}

        protected UInt64 bodyId = Store.Const.CONSTS.WrongID;
        protected UInt64 conditionId = Store.Const.CONSTS.WrongID;

        protected abstract void LoadIds();
        protected abstract void SaveIds();

        public override void RemoveMe()
        {
            Body.RemoveMe();
            Condition.RemoveMe();
            base.RemoveMe();
        }

        public IStatement Body
        {
            get
            {
                check.Check();

                LoadIds();
                return statements.GetStatement(bodyId);
            }
            set
            {
                check.Check();

                //Убеждаемся, что данные из таблицы были загружены
                LoadIds();

                IStatement oldValue = statements.GetStatement(bodyId);

                //Берем новые данные
                if (value != null)
                    bodyId = (value as Statement_Impl).Id;
                else
                    bodyId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();

                ChangeValue(oldValue, value);
            }
        }

        public IOperation Condition
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.operations.GetOperation(conditionId);
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(Condition);

                //Берем новые данные
                if (value != null)
                    conditionId = (value as Operation_Impl).Id;
                else
                    conditionId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(Condition);
            }
        }
    }

    internal sealed class StatementDoAndWhile_Impl : StatementLoop_Impl, IStatementDoAndWhile
    {
        internal StatementDoAndWhile_Impl(Statements statements)
            : base(statements)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STDoAndWhile;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            IStatement st = Body;
            if (st != null)
                yield return st;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation op = Condition;
            if (op != null)
                yield return op;

            yield break;
        }

        #region StatementDoAndWhile Members
        
        bool theIsCheckConditionBeforFirstRun = false;
        bool isLoaded = false;

        protected override void LoadIds()
        {
            if (!isLoaded)
            {
                isLoaded = true;
                
                IBufferReader reader = statements.TableStatementsData.GetValueByID(theID);

                if (reader == null)
                     return;
                 
                bodyId = reader.GetUInt64();
                conditionId = reader.GetUInt64();
                reader.GetBool(ref theIsCheckConditionBeforFirstRun);

            }
        }

        protected override void SaveIds()
        {
            statements.TableStatementsData.Put(theID, (buffer) =>
                    {
                        buffer.Add(bodyId);
                        buffer.Add(conditionId);
                        buffer.Add(theIsCheckConditionBeforFirstRun);
                    });
        }

        public bool IsCheckConditionBeforeFirstRun
        {
            get
            {
                check.Check();

                LoadIds();

                return theIsCheckConditionBeforFirstRun;
            }
            set
            {
                check.Check();

                theIsCheckConditionBeforFirstRun = value;

                SaveIds();
            }
        }

        #endregion
    }

    internal sealed class StatementFor_Impl : StatementLoop_Impl, IStatementFor
    {
        internal StatementFor_Impl(Statements stat) : base(stat)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STFor;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            IStatement next = Body;
            if(Body != null)
                yield return Body;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            foreach (IOperation next in new IOperation[] {Start, Iteration,  Condition}) 
                if(next != null)
                    yield return next;

            yield break;
        }

        public override void RemoveMe()
        {
            Start.RemoveMe();
            Iteration.RemoveMe();
            base.RemoveMe();
        }


        #region StatementFor Members

        UInt64 startId = Store.Const.CONSTS.WrongID;
        UInt64 iterationId = Store.Const.CONSTS.WrongID;
        bool isLoaded = false;

        protected override void LoadIds()
        {
            if (!isLoaded)
            {
                isLoaded = true;

                IBufferReader reader = statements.TableStatementsData.GetValueByID(theID);

                if (reader == null)
                    return;

                startId = reader.GetUInt64();
                conditionId = reader.GetUInt64();
                iterationId = reader.GetUInt64();
                bodyId = reader.GetUInt64();
            }
        }

        protected override void SaveIds()
        {
            statements.TableStatementsData.Put(theID, (buffer) =>
            {
                buffer.Add(startId);
                buffer.Add(conditionId);
                buffer.Add(iterationId);
                buffer.Add(bodyId);
            });
        }

        public IOperation Start
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.operations.GetOperation(startId);
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(Start);

                //Берем новые данные
                if (value != null)
                    startId = (value as Operation_Impl).Id;
                else
                    startId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(Start);
            }
        }

        public IOperation Iteration
        {
            get
            {
                check.Check(); 
                
                LoadIds();

                return statements.operations.GetOperation(iterationId);
            }
            set
            {
                check.Check();
                
                //Убираем индекс
                EraseOperationsFunctionCalls(Iteration);
               
                //Берем новые данные
                if (value != null)
                    iterationId = (value as Operation_Impl).Id;
                else
                    iterationId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(Iteration);
            }
        }
        #endregion
    }

    internal sealed class StatementForEach_Impl : StatementLoop_Impl, IStatementForEach
    {
        internal StatementForEach_Impl(Statements stat) : base(stat)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STForEach;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            IStatement next = Body;
            if (next != null)
                yield return next;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation next = Head;
                if (next != null)
                    yield return next;

            yield break;
        }

        public override void RemoveMe()
        {
            check.Check();

            Head.RemoveMe();
            base.RemoveMe();
        }

        #region StatementForEach Members

        bool isLoaded = false;

        protected override void LoadIds()
        {
            if (!isLoaded)
            {
                isLoaded = true;

                IBufferReader reader = statements.TableStatementsData.GetValueByID(theID);
                
                if (reader == null)
                    return;

                conditionId = reader.GetUInt64();
                bodyId = reader.GetUInt64();
            }
        }

        protected override void SaveIds()
        {
            statements.TableStatementsData.Put(theID, (buffer) =>
            {
                buffer.Add(conditionId);
                buffer.Add(bodyId);
            });
    
        }

        public IOperation Head
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.operations.GetOperation(conditionId);
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(Head);

                //Берем новые данные
                if (value != null)
                    conditionId = (value as Operation_Impl).Id;
                else
                    conditionId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(Head);
            }
        }

        #endregion
    
    }

    internal sealed class StatementGoto_Impl : Statement_Impl, IStatementGoto
    {
        internal StatementGoto_Impl(Statements stats) : base(stats)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STGoto;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            yield break;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation next = Condition;
            if (next != null)
                yield return next;

            yield break;
        }


        #region StatementGoto Members

        UInt64 destinationId = Store.Const.CONSTS.WrongID;
        UInt64 conditionId = Store.Const.CONSTS.WrongID;
        bool isLoaded = false;

        private void LoadIds()
        {
            if (!isLoaded)
            {
                isLoaded = true;

                IBufferReader reader = statements.TableStatementsData.GetValueByID(theID);

                if (reader == null)
                    return;

                destinationId = reader.GetUInt64();
                conditionId = reader.GetUInt64();
            }
        }

        private void SaveIds()
        {
            statements.TableStatementsData.Put(theID, (buffer) =>
            {
                buffer.Add(destinationId);
                buffer.Add(conditionId);
            });

        }

        public IStatement Destination
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.GetStatement(destinationId);
            }
            set
            {
                check.Check();

                //Убеждаемся, что данные из таблицы были загружены
                LoadIds();

                if (Condition != null && value != null) throw new Exception("Нельзя одновременно задавать параметры Destination и Condition у goto");

                //Перестраиваем индексы, связанные с подчинённостью
                ChangeValue(statements.GetStatement(destinationId), value);

                //Берем новые данные
                if (value != null)
                    destinationId = (value as Statement_Impl).Id;
                else
                    destinationId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();
            }
        }

        public IOperation Condition
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.operations.GetOperation(conditionId);
            }
            set
            {
                check.Check();

                LoadIds();

                if (Destination != null && value != null) throw new Exception("Нельзя одновременно задавать параметры Destination и Condition у goto");

                //Убираем индекс
                EraseOperationsFunctionCalls(Condition);

                //Берем новые данные
                if (value != null)
                    conditionId = (value as Operation_Impl).Id;
                else
                    conditionId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();

                //Добавляем индекс
                if (conditionId != Store.Const.CONSTS.WrongID)
                    SetupMyOperationsFunctionCalles(Condition);
            }
        }

        #endregion
    }

    internal sealed class StatementIf_Impl : Statement_Impl, IStatementIf
    {
        internal StatementIf_Impl(Statements stats) : base(stats)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STIf;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            IStatement next = ThenStatement;
            if (next != null)
                yield return next;

            next = ElseStatement;
            if (next != null)
                yield return next;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation next = Condition;
            if (next != null)
                yield return next;

            yield break;
        }


        public override void RemoveMe()
        {
            check.Check();

            ThenStatement.RemoveMe();
            ElseStatement.RemoveMe();
            Condition.RemoveMe();
            base.RemoveMe();
        }

        #region StatementIf Members

        UInt64 thenId = Store.Const.CONSTS.WrongID;
        UInt64 elseId = Store.Const.CONSTS.WrongID;
        UInt64 conditionId = Store.Const.CONSTS.WrongID;
        bool isLoaded = false;

        private void LoadIds()
        {
            if (!isLoaded)
            {
                isLoaded = true;

                IBufferReader reader = statements.TableStatementsData.GetValueByID(theID);

                if (reader == null)
                    return;

                thenId = reader.GetUInt64();
                elseId = reader.GetUInt64();
                conditionId = reader.GetUInt64();
            }
        }

        private void SaveIds()
        {
            statements.TableStatementsData.Put(theID, (buffer) =>
            {
                buffer.Add(thenId);
                buffer.Add(elseId);
                buffer.Add(conditionId);
            });

        }
        public IStatement ThenStatement
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.GetStatement(thenId);
            }
            set
            {
                check.Check();

                //Убеждаемся, что данные из таблицы были загружены
                LoadIds();

                //Перестраиваем индексы, связанные с подчинённостью
                ChangeValue(statements.GetStatement(thenId), value);

                //Берем новые данные
                if (value != null)
                    thenId = (value as Statement_Impl).Id;
                else
                    thenId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();
            }
        }


        public IStatement ElseStatement
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.GetStatement(elseId);
            }
            set
            {
                check.Check();

                //Убеждаемся, что данные из таблицы были загружены
                LoadIds();

                //Перестраиваем индексы, связанные с подчинённостью
                ChangeValue(statements.GetStatement(elseId), value);

                //Берем новые данные
                if (value != null)
                    elseId = (value as Statement_Impl).Id;
                else
                    elseId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();
            }
        }

        public IOperation Condition
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.operations.GetOperation(conditionId);
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(Condition);

                //Берем новые данные
                if (value != null)
                    conditionId = (value as Operation_Impl).Id;
                else
                    conditionId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();

                //Добавляем индекс
                if (conditionId != Store.Const.CONSTS.WrongID) 
                    SetupMyOperationsFunctionCalles(Condition);
            }
        }

        #endregion
    }

    internal sealed class StatementOperational_Impl : Statement_Impl, IStatementOperational
    {
        internal StatementOperational_Impl(Statements stats)
            : base(stats)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STOperational;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            yield break;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation next = Operation;
            if (next != null)
                yield return next;

            yield break;
        }

        public override void RemoveMe()
        {
            check.Check();

            Operation.RemoveMe();
            base.RemoveMe();
        }

        #region StatementOperational Members
        UInt64 operationId = Store.Const.CONSTS.WrongID;

        private void LoadIds()
        {
            if (operationId == Store.Const.CONSTS.WrongID)
                operationId = statements.TableStatementsData.GetUInt64ValueByID(theID);
        }

        public IOperation Operation
        {
            get
            {
                check.Check();

                LoadIds();

                if (operationId != Store.Const.CONSTS.WrongID)
                    return statements.operations.GetOperation(operationId);
                else
                    return null;
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(Operation);

                //Берем новые данные
                if (value != null)
                    operationId = (value as Operation_Impl).Id;
                else
                    operationId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                statements.TableStatementsData.PutUInt64Value(theID, operationId);

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(Operation);
            }
        }

        #endregion
    }


    internal sealed class StatementReturn_Impl : Statement_Impl, IStatementReturn
    {
        internal StatementReturn_Impl(Statements stats) : base(stats)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STReturn;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            yield break;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation next = ReturnOperation;
            if (next != null)
                yield return next;

            yield break;
        }

        public override void RemoveMe()
        {
            check.Check();

            ReturnOperation.RemoveMe();
            base.RemoveMe();
        }

        #region StatementReturn Members
        UInt64 returnId = Store.Const.CONSTS.WrongID;

        private void LoadIds()
        {
            if (returnId == Store.Const.CONSTS.WrongID)
                returnId = statements.TableStatementsData.GetUInt64ValueByID(theID);
        }

        public IOperation ReturnOperation
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.operations.GetOperation(returnId);
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(ReturnOperation);

                //Берем новые данные
                if (value != null)
                    returnId = (value as Operation_Impl).Id;
                else
                    returnId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                statements.TableStatementsData.PutUInt64Value(theID, returnId);

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(ReturnOperation);
            }
        }

        #endregion
    }

    internal sealed class StatementYieldReturn_Impl : Statement_Impl, IStatementYieldReturn
    {
        internal StatementYieldReturn_Impl(Statements stats)
            : base(stats)
        { }

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STYieldReturn;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            yield break;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation next = YieldReturnOperation;
            if (next != null)
                yield return next;

            yield break;
        }

        public override void RemoveMe()
        {
            YieldReturnOperation.RemoveMe();
            base.RemoveMe();
        }

        #region StatementReturn Members
        UInt64 yieldReturnId = Store.Const.CONSTS.WrongID;

        private void LoadIds()
        {
            if (yieldReturnId == Store.Const.CONSTS.WrongID)
                yieldReturnId = statements.TableStatementsData.GetUInt64ValueByID(theID);
        }

        public IOperation YieldReturnOperation
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.operations.GetOperation(yieldReturnId);
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(YieldReturnOperation);

                //Берем новые данные
                if (value != null)
                    yieldReturnId = (value as Operation_Impl).Id;
                else
                    yieldReturnId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                statements.TableStatementsData.PutUInt64Value(theID, yieldReturnId);

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(YieldReturnOperation);
            }
        }

        #endregion
    }

    internal sealed class StatementThrow_Impl : Statement_Impl, IStatementThrow
    {
        internal StatementThrow_Impl(Statements stats)
            : base(stats)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STThrow;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            yield break;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation next = Exception;
            if (next != null)
                yield return next;

            yield break;
        }

        public override void RemoveMe()
        {
            Exception.RemoveMe();
            base.RemoveMe();
        }

        #region StatementThrow Members
        UInt64 throwId = Store.Const.CONSTS.WrongID;

        private void LoadIds()
        {
            if (throwId == Store.Const.CONSTS.WrongID)
                throwId = statements.TableStatementsData.GetUInt64ValueByID(theID);
        }
        public IOperation Exception
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.operations.GetOperation(throwId);
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(Exception);

                //Берем новые данные
                if (value != null)
                    throwId = (value as Operation_Impl).Id;
                else
                    throwId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                statements.TableStatementsData.PutUInt64Value(theID, throwId);

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(Exception);
            }
        }

        #endregion
    }

    internal sealed class StatementUsing_Impl : Statement_Impl, IStatementUsing
    {
        internal StatementUsing_Impl(Statements stats) : base(stats)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STUsing;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            IStatement st = Body;
            if(st != null)
                yield return st;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation next = Head;
            if (next != null)
                yield return next;

            yield break;
        }

        public override void RemoveMe()
        {
            Head.RemoveMe();
            Body.RemoveMe();
            base.RemoveMe();
        }

        #region StatementUsing Members
        UInt64 headId = Store.Const.CONSTS.WrongID;
        UInt64 bodyId = Store.Const.CONSTS.WrongID;
        bool isLoaded = false;

        private void LoadIds()
        {
            if (!isLoaded)
            {
                isLoaded = true;

                IBufferReader reader = statements.TableStatementsData.GetValueByID(theID);

                if (reader == null)
                    return;

                headId = reader.GetUInt64();
                bodyId = reader.GetUInt64();

            }
        }

        private void SaveIds()
        {
            statements.TableStatementsData.Put(theID, (buffer) =>
            {
                buffer.Add(headId);
                buffer.Add(bodyId);
            });

        }

        public IOperation Head
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.operations.GetOperation(headId);
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(Head);

                //Берем новые данные
                if (value != null)
                    headId = (value as Operation_Impl).Id;
                else
                    headId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                statements.TableStatementsData.PutUInt64Value(theID, headId);

                SaveIds();

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(Head);
            }
        }

        public IStatement Body
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.GetStatement(bodyId);
            }
            set
            {
                check.Check();

                //Убеждаемся, что данные из таблицы были загружены
                LoadIds();

                //Перестраиваем индексы, связанные с подчинённостью
                ChangeValue(statements.GetStatement(bodyId), value);

                //Берем новые данные
                if (value != null)
                    bodyId = (value as Statement_Impl).Id;
                else
                    bodyId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();
            }
        }

        #endregion
    }

    internal class StatementSwitch_Impl : Statement_Impl, IStatementSwitch
    {
        UInt64 defaultStatementId = Store.Const.CONSTS.WrongID;
        UInt64 conditionId = Store.Const.CONSTS.WrongID;
        enSwitchTypes theSwitchType = 0;


        internal StatementSwitch_Impl(Statements stats) :  base(stats)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STSwitch;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            Load();

            if (defaultStatementId != Store.Const.CONSTS.WrongID)
                yield return base.statements.GetStatement(defaultStatementId);

            foreach (KeyValuePair<UInt64, UInt64> pair in CasesIds())
                yield return base.statements.GetStatement(pair.Value);
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            IOperation next = Condition;
            if (next != null)
                yield return next;


            foreach (KeyValuePair<IOperation, IStatement> pair in Cases())
                yield return pair.Key;

            yield break;
        }

        #region StatementSwitch Members

        bool isLoaded = false;

        private void Load()
        {
            if (!isLoaded)
            {
                isLoaded = true;

                IBufferReader reader = statements.TableStatementsData.GetValueByID(theID);

                if (reader == null)
                    return;

                conditionId = reader.GetUInt64();
                defaultStatementId = reader.GetUInt64();
                theSwitchType = (enSwitchTypes)reader.GetByte();
            }
        }

        protected void SaveIds()
        {
            statements.TableStatementsData.Put(theID, (buffer) =>
            {
                buffer.Add(conditionId);
                buffer.Add(defaultStatementId);
                buffer.Add((byte)theSwitchType);
            });
        }

        public enSwitchTypes SwitchType
        {
            get
            {
                check.Check();

                return theSwitchType;
            }
            set
            {
                check.Check();

                theSwitchType = value;
            }
        }

        public override void RemoveMe()
        {
            Condition.RemoveMe();
            foreach(KeyValuePair<IOperation, IStatement> pair in Cases())
            {
                pair.Key.RemoveMe();
                pair.Value.RemoveMe();
            }
            DefaultCase().RemoveMe();

            base.RemoveMe();
        }
        public IOperation Condition
        {
            get
            {
                check.Check();

                Load();

                if (conditionId != Store.Const.CONSTS.WrongID)
                    return statements.operations.GetOperation(conditionId);
                else
                    return null;
            }
            set
            {
                check.Check();

                //Убираем индекс
                EraseOperationsFunctionCalls(Condition);

                //Берем новые данные
                if (value != null)
                    conditionId = (value as Operation_Impl).Id;
                else
                    conditionId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                statements.TableStatementsData.PutUInt64Value(theID, conditionId);

                //Сохраняем
                SaveIds();

                //Добавляем индекс
                SetupMyOperationsFunctionCalles(Condition);
            }
        }

        public IEnumerable<KeyValuePair<IOperation, IStatement>> Cases()
        {
            check.Check();

            foreach (KeyValuePair<UInt64, UInt64> pair in CasesIds())
            {
                UInt64 operationId = pair.Key;
                UInt64 statementId = pair.Value;
                Storage storage = statements.storage;
                Operations operations = storage.operations;
                IOperation op = operations.GetOperation(operationId);
                IStatement st = statements.GetStatement(statementId);
                yield return new KeyValuePair<IOperation, IStatement>(op, st);
            }
        }

        public IEnumerable<KeyValuePair<UInt64, UInt64>> CasesIds()
        {
            check.Check();

            foreach (IBufferReader buffer in statements.TableStatementsAdditionalData.GetValuesByID(theID))
                yield return new KeyValuePair<UInt64, UInt64>(buffer.GetUInt64(), buffer.GetUInt64());
        }

        public IStatement DefaultCase()
        {
            check.Check();

            Load();

            if (defaultStatementId != Store.Const.CONSTS.WrongID)
                return statements.GetStatement(defaultStatementId);
            else
                return null;
        }

        public void AddCase(IOperation operation, IStatement statement)
        {
            check.Check();

            if (operation == null || statements == null)
                throw new Exception("Операция или оператор в Switch пусты");

            statements.TableStatementsAdditionalData.AddAtId(theID, buffer => { buffer.Add(operation.Id); buffer.Add(statement.Id); });

            //Перестраиваем индексы, связанные с подчинённостью
            ChangeValue(null, statement);

            //Добавляем индекс
            SetupMyOperationsFunctionCalles(operation);
        }

        public void AddDefaultCase(IStatement statement)
        {
            check.Check();

            if (statements == null)
                throw new Exception("Операция или оператор в Switch пусты");

            defaultStatementId = (statement as Statement_Impl).Id;

            //Сохраняем
            SaveIds();

            //Перестраиваем индексы, связанные с подчинённостью
            ChangeValue(null, statement);
        }

        #endregion
    }

    internal class StatementTryCatchFinally_Impl : Statement_Impl, IStatementTryCatchFinally
    {
        internal StatementTryCatchFinally_Impl(Statements stats) : base(stats)
        {}

        public override ENStatementKind Kind
        {
            get
            {
                check.Check();

                return ENStatementKind.STTryCatchFinally;
            }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            check.Check();

            IStatement next = TryBlock;

            if (next != null)
                yield return next;

            foreach (IStatement c in Catches())
                yield return c;

            next = ElseBlock;

            if (next != null)
                yield return next;

            next = FinallyBlock;

            if (next != null)
                yield return next;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            check.Check();

            yield break;
        }

        public override void RemoveMe()
        {
            check.Check();

            TryBlock.RemoveMe();
            foreach (IStatement st in Catches())
                st.RemoveMe();
            FinallyBlock.RemoveMe();
            ElseBlock.RemoveMe();

            base.RemoveMe();
        }

        #region StatementTryCatchFinally Members

        UInt64 tryId = Store.Const.CONSTS.WrongID;
        UInt64[] catchIds = new UInt64[0];
        UInt64 finallyId = Store.Const.CONSTS.WrongID;
        UInt64 elseId = Store.Const.CONSTS.WrongID;
        bool isLoaded = false;

        private void LoadIds()
        {
            if (!isLoaded)
            {
                isLoaded = true;

                IBufferReader reader = statements.TableStatementsData.GetValueByID(theID);

                if (reader == null)
                    return;

                int count = reader.GetInt32();
                catchIds = new UInt64[count];
                for (int i = 0; i < count; i++)
                    catchIds[i] = reader.GetUInt64();

                tryId = reader.GetUInt64();
                finallyId = reader.GetUInt64();
                elseId = reader.GetUInt64();
            }
        }

        protected void SaveIds()
        {
            statements.TableStatementsData.Put(theID, (buffer) =>
            {
                buffer.Add(catchIds.Length);
                foreach (UInt64 id in catchIds) buffer.Add(id);
                buffer.Add(tryId);
                buffer.Add(finallyId);
                buffer.Add(elseId);
            });

        }

        public IStatement TryBlock
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.GetStatement(tryId);
            }
            set
            {
                check.Check();

                //Убеждаемся, что данные из таблицы были загружены
                LoadIds();

                //Перестраиваем индексы, связанные с подчинённостью
                ChangeValue(statements.GetStatement(tryId), value);

                //Берем новые данные
                if (value != null)
                    tryId = (value as Statement_Impl).Id;
                else
                    tryId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();
            }
        }

        public IEnumerable<IStatement> Catches()
        {
            check.Check();

            LoadIds();

            foreach (UInt64 id in catchIds)
                yield return statements.GetStatement(id);
        }

        public void AddCatch(IStatement stCatch)
        {
            check.Check();

            check.Check();

            //Добавляем новое значение в массив
            UInt64[] newAr = new UInt64[catchIds.Length + 1];
            for (int i = 0; i < catchIds.Length; i++)
                newAr[i] = catchIds[i];

            UInt64 catchId = (stCatch as Statement_Impl).Id;

            newAr[newAr.Length - 1] = catchId;
            catchIds = newAr;

            //Сохраняем
            SaveIds();

            //Перестраиваем индексы, связанные с подчинённостью
            ChangeValue(null, stCatch);
        }



        public IStatement FinallyBlock
        {
            get
            {
                check.Check();

                LoadIds();

                return statements.GetStatement(finallyId);
            }
            set
            {
                check.Check();

                //Убеждаемся, что данные из таблицы были загружены
                LoadIds();

                //Перестраиваем индексы, связанные с подчинённостью
                ChangeValue(statements.GetStatement(finallyId), value);

                //Берем новые данные
                if (value != null)
                    finallyId = (value as Statement_Impl).Id;
                else
                    finallyId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();
            }
        }

        public IStatement ElseBlock {
            get
            {
                check.Check();

                LoadIds();

                return statements.GetStatement(elseId);
            }
            set
            {
                check.Check();

                //Убеждаемся, что данные из таблицы были загружены
                LoadIds();

                //Перестраиваем индексы, связанные с подчинённостью
                ChangeValue(statements.GetStatement(elseId), value);

                //Берем новые данные
                if (value != null)
                    elseId = (value as Statement_Impl).Id;
                else
                    elseId = Store.Const.CONSTS.WrongID;

                //Сохраняем данные. Возможно, сохранение будет выполняться много раз - при изменении каждого поля.
                //Но так надежнее.
                SaveIds();
            }
        }

        #endregion
    }

    internal class StatementExit_Impl : Statement_Impl, IStatementExit
    {
        internal StatementExit_Impl(Statements stats)
            : base(stats)
        {}

        public override ENStatementKind Kind
        {
            get { return ENStatementKind.STExit; }
        }

        public override IEnumerable<IStatement> SubStatements()
        {
            yield break;
        }

        public override IEnumerable<IOperation> SubOperations()
        {
            yield break;
        }
    }
}
