﻿using System.Collections.Generic;

namespace Store.SharedFiles
{
    public enum enSharedFiles
    {
        UnderstandProtocolProgramUnits
    }

    /// <summary>
    /// В  исключительных случаях плагинам требуется хранить файлы, доступные другим плагинам. Данный класс позволяет это обеспечить
    /// </summary>
    static class SharedFiles
    {
        static string SharedFolder = "SharedFiles";
        static Dictionary<enSharedFiles, string> fileNames = new Dictionary<enSharedFiles, string> 
        { 
            {enSharedFiles.UnderstandProtocolProgramUnits, "ProgramUnits.txt"}
        };

        public static string GetFilePath(Storage storage, enSharedFiles file)
        {
            string directoryName = System.IO.Path.Combine(storage.DB.GetDBDirectory(), SharedFolder);

            if (!System.IO.Directory.Exists(directoryName))
                System.IO.Directory.CreateDirectory(directoryName);

            return System.IO.Path.Combine(directoryName, fileNames[file]);
        }

    }


}
