﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using IA.Extensions;
using IOController;

namespace Store
{
    /// <summary>
    /// Класс, реализующий полезные методы извлечения информации из Хранилища
    /// </summary>
    public static class DumpExtensions
    {
        /// <summary>
        /// Получить полный путь до каталога для дампов внутри структуры каталогов Хранилища.
        /// </summary>
        /// <param name="storage"></param>
        /// <returns></returns>
        public static string GetInternalDumpsDirectory(this Storage storage)
        {
            var dumpsDirectory = Path.Combine(storage.WorkDirectory.Path, "Dumps");
            if (!Directory.Exists(dumpsDirectory))
                Directory.CreateDirectory(dumpsDirectory);
            return dumpsDirectory;
        }

        /// <summary>
        /// Дамп файлов в Хранилище.
        /// </summary>
        /// <param name="storage">Хранилище.</param>
        /// <param name="outputFolder">Выходной каталог с дампом. Если не задан - дамп производится в каталог по-умолчанию. См. <see cref="GetInternalDumpsDirectory"/>.</param>
        /// <param name="outputFileName">Имя файла дампа. По умолчанию - files.log.</param>
        public static void FilesDump(this Storage storage, string outputFolder = null, string outputFileName = "files.log")
        {
            try
            {
                if (storage == null)
                    throw new Exception("Хранилище не определено.");

                if (string.IsNullOrEmpty(outputFolder))
                {
                    outputFolder = storage.GetInternalDumpsDirectory();
                }

                if (!DirectoryController.IsExists(outputFolder))
                    throw new Exception("Выходной каталог не существует.");

                using (StreamWriter wr = new StreamWriter(Path.Combine(outputFolder, outputFileName), false, Encoding.Default))
                {
                    wr.WriteLine("Дамп файлов");
                    wr.WriteLine();

                    foreach (IFile file in storage.files.EnumerateFiles(enFileKind.anyFile))
                    {
                        wr.WriteLine(file.FileNameForReports);

                        wr.WriteLine("\t Общие сведения:");
                        wr.WriteLine("\t\t ID: " + file.Id);
                        wr.WriteLine("\t\t Избыточность: " + file.FileEssential.ToString());
                        wr.WriteLine("\t\t Существование: " + file.FileExistence.ToString());

                        wr.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при дампе файлов Хранилища.", ex);
            }
        }        

        /// <summary>
        /// Дамп классов в Хранилище
        /// </summary>
        /// <param name="storage">Хранилище.</param>
        /// <param name="outputFolder">Выходной каталог с дампом.</param>
        /// <param name="outputFileName">Имя файла дампа. По умолчанию - classes.log.</param>
        public static void ClassesDump(this Storage storage, string outputFolder, string outputFileName = "classes.log")
        {
            try
            {
                if (storage == null)
                    throw new Exception("Хранилище не определено.");

                if (string.IsNullOrEmpty(outputFolder))
                    throw new Exception("Выходной каталог не определен.");

                if (!DirectoryController.IsExists(outputFolder))
                    throw new Exception("Выходной каталог не существует.");

                using (StreamWriter wr = new StreamWriter(Path.Combine(outputFolder, outputFileName), false, Encoding.Default))
                {
                    wr.WriteLine("Дамп классов");
                    wr.WriteLine();

                    foreach (Class curClass in storage.classes.EnumerateClasses())
                    {
                        wr.WriteLine(curClass.Id + "\t" + curClass.FullName);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при дампе классов Хранилища.", ex);
            }
        }

        /// <summary>
        /// Дамп функций в Хранилище
        /// </summary>
        /// <param name="storage">Хранилище.</param>
        /// <param name="outputFolder">Выходной каталог с дампом.</param>
        /// <param name="outputFileName">Имя файла дампа. По умолчанию - functions.log.</param>
        public static void FunctionsDump(this Storage storage, string outputFolder, string outputFileName = "functions.log")
        {
            try
            {
                if (storage == null)
                    throw new Exception("Хранилище не определено.");

                if (string.IsNullOrEmpty(outputFolder))
                    throw new Exception("Выходной каталог не определен.");

                if (!DirectoryController.IsExists(outputFolder))
                    throw new Exception("Выходной каталог не существует.");

                var sensorsForFunction = new Dictionary<ulong, List<ulong>>();
                storage.sensors.EnumerateSensors().ForEach(x =>
                {
                    var funcId = x.GetFunctionID();
                    if (funcId != Const.CONSTS.WrongID)
                    {
                        if(!sensorsForFunction.ContainsKey(funcId)) sensorsForFunction.Add(funcId, new List<ulong>());
                        sensorsForFunction[funcId].Add(x.ID);
                    }
                });

                using (StreamWriter wr = new System.IO.StreamWriter(Path.Combine(outputFolder, outputFileName), false, Encoding.Default))
                {
                    wr.WriteLine("Дамп функций");
                    wr.WriteLine();
                    foreach (IFunction func in storage.functions.EnumerateFunctions(enFunctionKind.ALL)
                        .OrderBy(f => f.FullNameForReports)
                        .ThenBy(f =>
                        {
                            var loc = f.Definition();
                            if (loc != null)
                                return loc.GetFile().FileNameForReports;
                            return string.Empty;
                        })
                        .ThenBy(f =>
                        {
                            var loc = f.Definition();
                            if (loc != null)
                                return loc.GetLine();
                            return (ulong)0;
                        }))
                    {
                        wr.WriteLine(func.FullNameForReports);

                        wr.WriteLine("\t Общие сведения:");
                        wr.WriteLine("\t\t Существенность: " + func.Essential.ToString());
                        wr.WriteLine("\t\t Причина существенности: " + func.EssentialMarkedType.ToString());
                        wr.WriteLine("\t\t Маркер вида функции: " + func.UseKind.ToString());

                        wr.WriteLine("\t Определение: ");
                        Location location = func.Definition();
                        if (location != null)
                            wr.WriteLine("\t\t " + location.GetFile().FileNameForReports
                                + " (" + location.GetLine() + ":" + location.GetColumn() + ")");

                        wr.WriteLine("\t Объявления: ");
                        DumpLocation(func.Declarations(), wr);

                        wr.WriteLine("\t Вызывает: ");
                        if (func.Calles().GetEnumerator().MoveNext())
                        {
                            wr.WriteLine("\t\t " + string.Join(", ", func.Calles().Select(f => f.Calles.NameForReports).OrderBy(s => s)));
                        }

                        wr.WriteLine("\t Вызывается из: ");
                        if (func.CalledBy().GetEnumerator().MoveNext())
                        {
                            wr.WriteLine("\t\t " + string.Join(", ", func.CalledBy().Select(f => f.Caller.NameForReports).OrderBy(s => s)));
                        }

                        wr.WriteLine("\t Датчики: ");
                        if (sensorsForFunction.ContainsKey(func.Id))
						{
							var sensors = sensorsForFunction[func.Id];
							if (sensors.Count() != 0)
							{
								wr.WriteLine("\t\t " + string.Join(", ", sensors));
							}
						}

                        wr.WriteLine();
                    }

                    sensorsForFunction.Clear();
                    sensorsForFunction = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при дампе функций Хранилища.", ex);
            }
        }

        /// <summary>
        /// Дамп переменных в Хранилище
        /// </summary>
        /// <param name="storage">Хранилище.</param>
        /// <param name="outputFolder">Выходной каталог с дампом.</param>
        /// <param name="outputFileName">Имя файла дампа. По умолчанию - variables.log.</param>
        public static void VariablesDump(this Storage storage, string outputFolder, string outputFileName = "variables.log")
        {
            try
            {
                if (storage == null)
                    throw new Exception("Хранилище не определено.");

                if (string.IsNullOrEmpty(outputFolder))
                    throw new Exception("Выходной каталог не определен.");

                if (!DirectoryController.IsExists(outputFolder))
                    throw new Exception("Выходной каталог не существует.");

                using (StreamWriter wr = new StreamWriter(Path.Combine(outputFolder, outputFileName), false, Encoding.Default))
                {
                    wr.WriteLine("Дамп переменных");
                    wr.WriteLine();

                    foreach (Variable var in storage.variables.EnumerateVariables()
                        .OrderBy(v => v.FullName)
                        .ThenBy(v =>
                        {
                            var loc = v.Definitions();
                            if (loc != null && loc.Count() != 0)
                            {
                                return loc[0].GetFile().FileNameForReports;
                            }
                            return string.Empty;
                        })
                        .ThenBy(v =>
                        {
                            var loc = v.Definitions();
                            if (loc != null && loc.Count() != 0)
                            {
                                return loc[0].GetLine();
                            }
                            return (ulong)0;
                        })
                        .ThenBy(v =>
                        {
                            var points = v.Enumerate_PointsToFunctions();
                            if (points != null && points.Count() != 0)
                            {
                                var pos = points.First().position;
                                if (pos != null)
                                    return pos.GetFile().FileNameForReports;
                            }
                            return string.Empty;
                        })
                        )
                    {
                        wr.WriteLine((!string.IsNullOrWhiteSpace(var.FullName)) ? var.FullName : "{Указатель на функцию}");

                        wr.WriteLine("\t Определения: ");
                        DumpLocation(var.Definitions(), wr);

                        wr.WriteLine("\t Запись: ");
                        DumpLocation(var.ValueSetPosition(), wr);

                        wr.WriteLine("\t Чтение: ");
                        DumpLocation(var.ValueGetPosition(), wr);

                        wr.WriteLine("\t Вызов по указателю: ");
                        DumpLocation(var.ValueCallPosition(), wr);

                        wr.WriteLine("\t Присвоения между: ");
                        var assigns = var.Enumerate_AssignmentsFrom().OrderBy(a => a.Position.GetFile().FileNameForReports);
                        foreach (IAssignment ass in assigns)
                            wr.WriteLine("\t\t от:" + ass.From.Id + " к:" + ass.To.Id + " в " +
                                "(" + ass.Position.GetFile().FileNameForReports + ", " + ass.Position.GetLine() + ":" + ass.Position.GetColumn() + ")");

                        wr.WriteLine("\t Присвоения указателей: ");
                        foreach (PointsToFunction points in var.Enumerate_PointsToFunctions())
                        {
                            Location pos = points.position;
                            if (pos != null)
                            {
                                wr.WriteLine("\t\t переменная:" + points.variable.Name + " функция:" + storage.functions.GetFunction(points.function).FullNameForReports + " в ("
                                    + pos.GetFile().FileNameForReports + ", " + pos.GetLine() + ":" + pos.GetColumn() + ")");
                            }
                        }

                        wr.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при дампе переменных Хранилища.", ex);
            }
        }

        /// <summary>
        /// Дамп местоположения сущности в файле
        /// </summary>
        /// <param name="location">Коллекция объектов класса Location. Хранит информацию о местоположении сущности.</param>
        /// <param name="writer">Поток файла, используемого для записи дампа.</param>
        private static void DumpLocation(Location[] location, StreamWriter writer)
        {
            if (location == null)
                throw new Exception("Местоположение не определено.");

            if (writer == null)
                throw new Exception("Поток на запись не определен");

            location = location.OrderBy(l => l.GetFile().FileNameForReports).ToArray();
            foreach (Location loc in location)
                writer.WriteLine("\t\t " + loc.GetFile().FileNameForReports + " (" + loc.GetLine() + ":" + loc.GetColumn() + ")");
        }
    }
}
