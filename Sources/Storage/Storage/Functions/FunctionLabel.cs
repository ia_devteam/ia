﻿using System;
using Store.BaseDatatypes;

namespace Store
{
    /// <summary>
    /// Виды необходимости объекта для сборки исходных текстов.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1717:OnlyFlagsEnumsShouldHavePluralNames"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "en"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue")]
    public enum enFunctionEssentials
    {
        /// <summary>
        /// Необходимость объекта для сборки исходных текстов не выяснена.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "UNKNOWN")]
        UNKNOWN = 1,
        /// <summary>
        /// Объект необходим для сборки исходных текстов
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ESSENTIAL")]
        ESSENTIAL,
        /// <summary>
        /// Объект не требуется для сборки исходных текстов
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "REDUNDANT")]
        REDUNDANT
    }

    /// <summary>
    /// Виды необходимости объекта для сборки исходных текстов по мнению Understand.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1717:OnlyFlagsEnumsShouldHavePluralNames"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "en"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue")]
    public enum enFunctionEssentialsUnderstand
    {
        /// <summary>
        /// Объект необходим для сборки исходных текстов
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ESSENTIAL")]
        ESSENTIAL,
        /// <summary>
        /// Объект не требуется для сборки исходных текстов
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "REDUNDANT")]
        REDUNDANT
    }

    /// <summary>
    /// Флаги, определяющие, откуда может быть вызвада функция
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "en"), Flags]
    public enum enFunctionUseKinds
    {
        /// <summary>
        /// Выполнение программы начается с выполнения данной функции.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "START")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "PROGRAM")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        PROGRAM_START = 1,

        /// <summary>
        /// Функция является обработчиком событий, генерируемых либо программой, либо ее окружением.
        /// Такими обработчиками могут быть функции, адрес которых передается во вне (С++), 
        /// либо вызываемые по имени (Java).
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "HANDLER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "EVENT")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ENVIRONMENT")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        ENVIRONMENT_EVENT_HANDLER,

        /// <summary>
        /// Функция является обработчиком событий, генерируемых внутри программы. 
        /// Например, переопределенная функция в классе, наследующем от MFC-класса, и вызываемая из MFC 
        /// будет иметь данные флаг.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "PROGRAM")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "HANDLER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "EVENT")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        PROGRAM_EVENT_HANDLER,     //обработчик сообщений, генерируемых только программой

        /// <summary>
        /// Функция экспортируется модулем.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "EXPORTS")]
        EXPORTS,
    }

    /// <summary>
    /// Варианты, описывающие сценарии пометки функции избыточной или неизбыточной.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1717:OnlyFlagsEnumsShouldHavePluralNames"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "en"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue")]
    public enum enFunctionEssentialMarkedTypes
    {
        /// <summary>
        /// Функция не была помечена избыточной.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "NOTHING")]
        NOTHING = 1,

        /// <summary>
        /// При прогоне плагина ручного распознавания избыточности пользовать заявил, что функция нужная.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "USER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "REDUNDANCY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MANUAL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ACCEPT")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        MANUAL_REDUNDANCY_USER_ACCEPT,

        /// <summary>
        /// При прогоне плагина ручного распознавания избыточности пользовать заявил, что функция избыточна.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "USER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "REDUNDANCY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "REJECT")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MANUAL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        MANUAL_REDUNDANCY_USER_REJECT,

        /// <summary>
        /// При прогоне плагина ручного распознавания избыточности автомат установил нужность функции. 
        /// Такая функция вызывается (возможно, опосредованно) из другой функции, которую пользователь назвал нужной.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "USER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "REDUNDANCY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MANUAL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AUTO")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AFTER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        MANUAL_REDUNDANCY_AUTO_AFTER_USER,

        /// <summary>
        /// При прогоне плагина ручного распознавания избыточности избыточность или существенность функции установлена
        /// анализом поиска одного вхождения.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "REDUNDANCY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "SEARCH")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MANUAL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ONE")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "FULL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ENTRY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AUTO")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        MANUAL_REDUNDANCY_FULL_AUTO_ONE_ENTRY_SEARCH,

        /// <summary>
        /// При прогоне плагина ручного распознавания избыточности избыточность или существенность функции установлена
        /// анализом dfm-файлов (файлы описания форм Borland, в них задаются обработчики оконных событий).
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "REDUNDANCY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MANUAL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "FULL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "DFM")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AUTO")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        MANUAL_REDUNDANCY_FULL_AUTO_DFM,

        /// <summary>
        /// При прогоне плагина ручного распознавания избыточности избыточность или существенность функции установлена
        /// исходя из того, что функция достижима из существенной функции, выявленной с признаком 
        /// MANUAL_REDUNDANCY_FULL_AUTO_DFM.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "REDUNDANCY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MANUAL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "FULL")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "DFM")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AUTO")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AFTER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        MANUAL_REDUNDANCY_FULL_AUTO_AFTER_DFM,

        /// <summary>
        /// Функция признана неизбыточной при анализе трасс, накопленных при тестировании. То есть функция в 
        /// действительности была вызвана. Функция признана входом в программу и не имеет вызовов.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "TRACE")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "PROGRAM")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ENTRY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CHECKER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CALLES")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES,

        /// <summary>
        /// Функция признана неизбыточной при анализе трасс, накопленных при тестировании. То есть функция в 
        /// действительности была вызвана. Функция признана входом в программу, но имеются ее вызовы
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "WITH")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "TRACE")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "PROGRAM")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ENTRY")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CHECKER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CALLES")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES,

        /// <summary>
        /// Функция признана неизбыточной при анализе трасс, накопленных при тестировании. То есть функция в 
        /// действительности была вызвана. Функция просто встретилась в трассе.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "TRACE")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CHECKER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CALLED")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        TRACE_CHECKER_CALLED,

        /// <summary>
        /// Функция признана неизбыточной, так как она вызывается (возможно, опосредованно) из функции, признанной неизбыточной при анализе трасс
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "TRACE")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "CHECKER")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "AUTO")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        TRACE_CHECKER_AUTO,
        
        /// <summary>
        /// Значимость функции установлена по отчёту АИСТ-С
        /// </summary>
        AIST_IMPORTED
    }

    /// <summary>
    /// Энумератор, описывающий, были ли построены связи между стейтментами внутри функции или нет
    /// </summary>
    public enum enFunctionStatementLinked
    {
        //не были построены
        NOT_PROCESSED = 1,
        //были построены
        PROCESSED
    }

    /// <summary>
    /// 
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface FunctionLabels
    {
        /// <summary>
        /// 
        /// </summary>
        enFunctionEssentials Essential
        { get; set; }
        /// <summary>
        /// 
        /// </summary>
        enFunctionEssentialsUnderstand EssentialUnderstand
        { get; set; }
        /// <summary>
        /// 
        /// </summary>
        enFunctionUseKinds Kind
        { get; set; }
        /// <summary>
        /// 
        /// </summary>
        enFunctionEssentialMarkedTypes EssentialMarkedType
        { get; set; }

        /// <summary>
        /// Обработана ли последовательность satementов связыванием
        /// </summary>
        enFunctionStatementLinked StatementLinked
        { get; set; }
        
        /// <summary>
        /// Дублирует Function.IsExternal
        /// </summary>
        bool IsExternal
        { get; set; }

        /// <summary>
        /// Тип возвращаемого функцией значения
        /// </summary>
        string FunctionReturnType { get; set; }
    }


    internal class FunctionLabels_Impl : AdditionalDataObject, FunctionLabels
    {
        enFunctionEssentials essential = enFunctionEssentials.UNKNOWN;
        enFunctionEssentialsUnderstand essentialUnderstand = enFunctionEssentialsUnderstand.ESSENTIAL;
        enFunctionUseKinds kind = 0;
        enFunctionEssentialMarkedTypes essentialMarkedType = enFunctionEssentialMarkedTypes.NOTHING;
        enFunctionStatementLinked statementLinked = enFunctionStatementLinked.NOT_PROCESSED;
        bool isCallIndexCorrect = true;
        bool isExternal = false;
        string functionReturnType = "";

        #region Save and Load

        internal FunctionLabels_Impl(Functions functions, UInt64 Id)
            : base(functions.tableLabels, Id)
        {
            Load();
        }

        protected override void SaveIndexes()
        {
        }

        protected override void SaveToBuffer(Store.Table.IBufferWriter buffer)
        {
            buffer.Add((byte)essential);
            buffer.Add((byte)essentialUnderstand);
            buffer.Add((byte)kind);
            buffer.Add((byte)essentialMarkedType);
            buffer.Add((byte)statementLinked);
            buffer.Add(isCallIndexCorrect, isExternal);
            buffer.Add(functionReturnType);
        }

        protected override void Load(Store.Table.IBufferReader reader)
        {
            essential = (enFunctionEssentials) reader.GetByte();
            essentialUnderstand = (enFunctionEssentialsUnderstand)reader.GetByte();
            kind = (enFunctionUseKinds)reader.GetByte();
            essentialMarkedType = (enFunctionEssentialMarkedTypes)reader.GetByte();
            statementLinked = (enFunctionStatementLinked)reader.GetByte();
            reader.GetBool(ref isCallIndexCorrect, ref isExternal);
            functionReturnType = reader.GetString();
        }

        protected override string GetLoadErrorString()
        {
            return "Попытка загрузить FunctionLables с идентификатором %s. Такого в базе не существует";
        }

        #endregion

        #region Interface implementation

        public enFunctionEssentials Essential
        {
            get
            {
                return essential;
            }
            set
            {
                essential = value;
                Save();
            }
        }

        public enFunctionEssentialsUnderstand EssentialUnderstand
        {
            get
            {
                return essentialUnderstand;
            }
            set
            {
                essentialUnderstand = value;
                Save();
            }
        }

        public enFunctionUseKinds Kind
        {
            get
            {
                return kind;
            }
            set
            {
                kind = value;
                Save();
            }
        }

        public enFunctionEssentialMarkedTypes EssentialMarkedType
        { 
            get
            {
                return essentialMarkedType;
            }
            set
            {
                essentialMarkedType = value;
                Save();
            }
        }

        public enFunctionStatementLinked StatementLinked
        {
            get
            {
                return statementLinked;
            }
            set
            {
                statementLinked = value;
                Save();
            }
        }

        public bool IsStatementIndexesCorrect
        { 
            get
            {
                return isCallIndexCorrect;
            }
            set
            {
                isCallIndexCorrect = value;
                Save();
            }
        }

        public bool IsExternal
        {
            get
            {
                return isExternal;
            }
            set
            {
                isExternal = value;
                Save();
            }
        }

        public string FunctionReturnType
        {
            get { return functionReturnType; }
            set
            {
                functionReturnType = value;
                Save();
            }
        }
        #endregion
    }
}
