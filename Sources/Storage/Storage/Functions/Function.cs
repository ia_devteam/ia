﻿using System;
using System.Collections.Generic;
using Store.BaseDatatypes;
using Store.Table;
using System.Linq;

namespace Store
{

    /// <summary>
    /// Описание вызова функцией другой функции
    /// </summary>
    public interface IFunctionCall
    {
        /// <summary>
        /// Вызывающая функция
        /// Допустимо тольк  однократная установка значения. При попытке последующего изменения зачения будет выдана ошибка.
        /// </summary>
        IFunction Caller { get; }

        /// <summary>
        /// Вызываемая функция
        /// Допустимо тольк  однократная установка значения. При попытке последующего изменения зачения будет выдана ошибка.
        /// </summary>
        IFunction Calles { get; }

        /// <summary>
        /// Местоположение в исходных текстах, где производится вызов
        /// 
        /// </summary>
        /// <remarks> В текущей реализации есть ограничения. 
        /// 
        /// В случае, если у вызывающей функции есть тело (занесённое в хранилище), то указывается местоположение Statement, в котором выполняется вызов. То есть в ряде случаев - это начало линейного блока операторов, в котором находится вызов.
        /// 
        /// В случае, если у функции нет тла, это поле не заполняется
        /// </remarks>
        Location CallLocation { get; }
    }

    internal class FunctionCall_Impl : DataObject_IDNoDuplicate_Immediate, IFunctionCall
    {
        UInt64 callerFunction;
        bool isCallerSeted = false;
        UInt64 callesFunction;
        bool isCallesSeted = false;
        Functions functions;

        internal FunctionCall_Impl(Functions functions)
        { 
            this.functions = functions; 
        }

        protected override string GetLoadErrorString()
        {
            return "Объект FunctionCall отсутствует";
        }

        protected override void Load(IBufferReader reader)
        {
            callerFunction = reader.GetUInt64();
            isCallerSeted = callerFunction != Store.Const.CONSTS.WrongID;

            callesFunction = reader.GetUInt64();
            isCallesSeted = callesFunction != Store.Const.CONSTS.WrongID;
        }

        protected override void SaveToBuffer(IBufferWriter buffer)
        {
            buffer.Add(callerFunction);
            buffer.Add(callesFunction);
        }

        protected override void SaveIndexes()
        {
            if (isCallerSeted && isCallesSeted)
            {
            functions.indexFunctionCalledBy.PutWithoutRepetition(callesFunction, buffer => buffer.Add(theID));
            functions.indexFunctionCalles.PutWithoutRepetition(callerFunction, buffer => buffer.Add(theID));
            }
        }

        #region FunctionCall Members

        public IFunction Caller
        {
            get { return functions.GetFunction(callerFunction); }
            set 
            {
                if (isCallerSeted)
                    throw new Store.Const.StorageException("Запрещено изменять значение IFunctionCall.Caller. Допустимо только однократное установление значения");

                if(value.Id == Store.Const.CONSTS.WrongID)
                    throw new Store.Const.StorageException("Запрещено присваивать IFunctionCall.Caller Store.Const.CONSTS.WrongID.");
                
                callerFunction = value.Id; 
                isCallerSeted = true;  

                if(isCallerSeted && isCallesSeted)
                    Save(); 
            }
        }

        public IFunction Calles
        {
            get { return functions.GetFunction(callesFunction); }
            set 
            {
                if (isCallesSeted)
                    throw new Store.Const.StorageException("Запрещено изменять значение IFunctionCall.Calles. Допустимо только однократное установление значения");

                if (value.Id == Store.Const.CONSTS.WrongID)
                    throw new Store.Const.StorageException("Запрещено присваивать IFunctionCall.Calles Store.Const.CONSTS.WrongID.");

                callesFunction = value.Id; 
                isCallesSeted = true;
                
                if (isCallerSeted && isCallesSeted)
                Save(); 
            }
        }

        public Location CallLocation
        {
            get 
            {
                Location locs = functions.GetLocationsCalles().GetObjectLocation(theID);
                if (locs == null)
                    return null;
                else return locs;
            }
            set
            {
                functions.GetLocationsCalles().SetSingleLocation(theID, value);
            }
        }

        #endregion

        /// <summary>
        /// Удаляет данный FunctionCall из Хранилища
        /// </summary>
        internal void Remove()
        {
            functions.indexFunctionCalledBy.Remove(callesFunction, buffer => buffer.Add(theID));
            functions.indexFunctionCalles.Remove(callerFunction, buffer => buffer.Add(theID));
            functions.tableFunctionCalls.Remove(theID);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
    public struct LocationFunctionPair
    {
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public IFunction function;
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
        public Location location;

        #region Structure implementation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (int)(function.Id ^ location.GetOffset());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is LocationFunctionPair)
            {
                LocationFunctionPair locc = (LocationFunctionPair)obj;
                return function.Id == locc.function.Id
                    && location.GetFileID() == locc.location.GetFileID()
                    && location.GetOffset() == locc.location.GetOffset();
            }
            else
                return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="location1"></param>
        /// <param name="location2"></param>
        /// <returns></returns>
        public static bool operator ==(LocationFunctionPair location1, LocationFunctionPair location2)
        {
            return location1.Equals(location2);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="location1"></param>
        /// <param name="location2"></param>
        /// <returns></returns>
        public static bool operator !=(LocationFunctionPair location1, LocationFunctionPair location2)
        {
            return !location1.Equals(location2);
        }

        #endregion
    }

    /// <summary>
    /// Описание функции
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Function"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface IFunction
    {
        /// <summary>
        /// Идентификатор функции (ее имя)
        /// Имя можно установить только один раз.
        /// </summary>
        string Name
        { get; set; }

        /// <summary>
        /// Имя функции, которое следует использовать при генерации отчётов. Преобразует некоторые дефолтные имена в читаемые
        /// </summary>
        string NameForReports
        { get; }

        /// <summary>
        /// Полное имя функции
        /// </summary>
        string FullName
        { get; }

        /// <summary>
        /// Полное функции, которое следует использовать при генерации отчётов. Преобразует некоторые дефолтные имена в читаемые
        /// </summary>
        string FullNameForReports
        { get; }

        /// <summary>
        /// Полное функции, которое следует использовать при генерации отчётов для генерации уникальных имён файлоа
        /// </summary>
        string FullNameForFileName
        { get; }


        /// <summary>
        /// Номер функции в Хранилище
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID")]
        UInt64 Id
        {
            get;
        }

        /// <summary>
        /// Модуль, в котором реализована данная функция
        /// </summary>
        Module ImplementedInModule
        {
            get;
        }

        /// <summary>
        /// Идентификатор модуля, в котором реализована данная функция
        /// </summary>
        UInt64 ImplementedInModuleId { get; }

        /// <summary>
        /// Тип возвращаемого функцией значения
        /// </summary>
        string FunctionReturnType { get; set; }

        /// <summary>
        /// Возвращает место определения функции, если оно задано. Если не задано, возвращает null
        /// </summary>
        /// <returns></returns>
        Location Definition();

        /// <summary>
        /// Возвращает массив, хранящий все места деклараций функции.
        /// </summary>
        /// <returns></returns>
        Location[] Declarations();
        /// <summary>
        /// Задаёт определение функции. У одной функции может быть только одно определение, при повторном вызове данный метод выдаст ошибку
        /// </summary>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        void AddDefinition(IFile file, UInt64 offset);
        /// <summary>
        /// Задаёт определение функции. У одной функции может быть только одно определение, при повторном вызове данный метод выдаст ошибку
        /// </summary>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        void AddDefinition(IFile file, UInt64 line, UInt64 column);
        /// <summary>
        /// Задаёт определение функции. У одной функции может быть только одно определение, при повторном вызове данный метод выдаст ошибку
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл обязательно должен содержать префикс</param>
        /// <param name="offset"></param>
        void AddDefinition(string fullFileName, UInt64 offset);
        /// <summary>
        /// Задаёт определение функции. У одной функции может быть только одно определение, при повторном вызове данный метод выдаст ошибку
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл обязательно должен содержать префикс</param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        void AddDefinition(string fullFileName, UInt64 line, UInt64 column);
        /// <summary>
        /// Задаёт определение функции. У одной функции может быть только одно определение, при повторном вызове данный метод выдаст ошибку
        /// Создать определение без указания местоположения внутри файла. "Находится где-то в файле"
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <param name="fileKind">Указание, является ли файл внешним или он имеет префикс исходныхъ текстов</param>
        void AddDefinition(string fullFileName, enFileKindForAdd fileKind);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        void AddDeclaration(IFile file, UInt64 offset);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        void AddDeclaration(IFile file, UInt64 line, UInt64 column);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл обязательно должен содержать префикс</param>
        /// <param name="offset"></param>
        void AddDeclaration(string fullFileName, UInt64 offset);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл обязательно должен содержать префикс</param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        void AddDeclaration(string fullFileName, UInt64 line, UInt64 column);
        /// <summary>
        /// Создать декларацию без указания местоположения внутри файла. "Находится где-то в файле"
        /// </summary>
        /// <param name="fullFileName"></param>
        /// <param name="fileKind">Указание, является ли файл внешним или он имеет префикс исходныхъ текстов</param>
        void AddDeclaration(string fullFileName, enFileKindForAdd fileKind);

        /// <summary>
        /// Перечисляет все вызовы функции, имеющиеся в данной функции.
        /// Подробнее см. <a href="31d573a6-8c67-4aed-887c-f102c8f1a04c.htm">Вызовы функций</a>.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunctionCall> Calles();

        /// <summary>
        /// Перечисляет все функции, вызываемых из данной. В отличии от Calles, если функция вызывается несколько раз внутри тела данной функции, то это перечисление 
        /// вернёт её значение только один раз.
        /// Подробнее см. <a href="31d573a6-8c67-4aed-887c-f102c8f1a04c.htm">Вызовы функций</a>.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunction> CallesUniq();

        /// <summary>
        /// Перечисляет все места вызовов данной функции.
        /// Подробнее см. <a href="31d573a6-8c67-4aed-887c-f102c8f1a04c.htm">Вызовы функций</a>.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunctionCall> CalledBy();

        /// <summary>
        /// Перечисляет все функции, вызывающих данную. В отличии от CalledBy, если данная функция вызывается несколько раз внутри тела другой функции, то это перечисление 
        /// вернёт её значение только один раз.
        /// Подробнее см. <a href="31d573a6-8c67-4aed-887c-f102c8f1a04c.htm">Вызовы функций</a>.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunction> CalledByUniq();

        /// <summary>
        /// Перечисляет все операции взятия указателя на функцию
        /// </summary>
        /// <returns></returns>
        IEnumerable<PointsToFunction> OperatorsGetPointerToFunction();

        /// <summary>
        /// Добавить вызов функции из данной функции. 
        /// <remarks>
        ///     <para>Если такой вызов уже существует в БД, копия добавлена не будет. </para>
        ///     <para>Если у функции есть тело, то будет сгенерирована ошибка.</para>
        /// </remarks>
        /// </summary>
        /// <param name="calledFunction">Вызываемая функция</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddCall(IFunction calledFunction);
        /// <summary>
        /// Добавить вызов функции из данной функции. 
        /// <remarks>
        ///     <para>Если такой вызов уже существует в БД, копия добавлена не будет. </para>
        ///     <para>Если у функции есть тело, то будет сгенерирована ошибка.</para>
        /// </remarks>
        /// </summary>
        /// <param name="calledFunction">Вызываемая функция</param>
        /// <param name="file">Файл, в котором находится вызов</param>
        /// <param name="offset">Смещение от начала файла до вызова функции</param>
        void AddCall(IFunction calledFunction, IFile file, UInt64 offset);
        /// <summary>
        /// Добавить вызов функции из данной функции. 
        /// <remarks>
        ///     <para>Если такой вызов уже существует в БД, копия добавлена не будет. </para>
        ///     <para>Если у функции есть тело, то будет сгенерирована ошибка.</para>
        /// </remarks>
        /// </summary>
        /// <param name="calledFunction">Вызываемая функция</param>
        /// <param name="file">Файл, в котором находится вызов</param>
        /// <param name="line">Номер строки, в которой находится вызов функции</param>
        /// <param name="column">Номер столбца, в котором находится вызов функции</param>
        void AddCall(IFunction calledFunction, IFile file, UInt64 line, UInt64 column);
        /// <summary>
        /// Добавить вызов функции из данной функции. 
        /// <remarks>
        ///     <para>Если такой вызов уже существует в БД, копия добавлена не будет. </para>
        ///     <para>Если у функции есть тело, то будет сгенерирована ошибка.</para>
        /// </remarks>
        /// </summary>
        /// <param name="calledFunction">Вызываемая функция</param>
        /// <param name="fullFileName">Имя файла, в котором находится вызов. Необходимо передавать имя файла в исходных текстах, то есть от него должен отрезаться префикс</param>
        /// <param name="offset">Смещение от начала файла до вызова функции</param>
        void AddCall(IFunction calledFunction, string fullFileName, UInt64 offset);
        /// <summary>
        /// Добавить вызов функции из данной функции. 
        /// <remarks>
        ///     <para>Если такой вызов уже существует в БД, копия добавлена не будет. </para>
        ///     <para>Если у функции есть тело, то будет сгенерирована ошибка.</para>
        /// </remarks>
        /// </summary>
        /// <param name="calledFunction">Вызываемая функция</param>
        /// <param name="fullFileName">Имя файла, в котором находится вызов. Необходимо передавать имя файла в исходных текстах, то есть от него должен отрезаться префикс</param>
        /// <param name="line">Номер строки, в которой находится вызов функции</param>
        /// <param name="column">Номер столбца, в котором находится вызов функции</param>
        void AddCall(IFunction calledFunction, string fullFileName, UInt64 line, UInt64 column);

        /// <summary>
        /// Добавить датчик вставленный в эту функцию.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="kind"></param>
        void AddSensor(UInt64 id, Kind kind);

        /// <summary>
        /// Перечисляет модули, в которых импортируется данная функция
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunction> ImportedByModules();

        /// <summary>
        /// Маркер существенности функции
        /// </summary>
        enFunctionEssentials Essential
        { get; set; }

        /// <summary>
        /// Маркер существенности функции по мнению Understand 
        /// </summary>
        enFunctionEssentialsUnderstand EssentialUnderstand
        { get; set; }

        /// <summary>
        /// Маркер вида функции
        /// </summary>
        enFunctionUseKinds UseKind
        { get; set; }

        enFunctionKind kind
        { get; }

        /// <summary>
        /// Причина, по которой маркер существенности функции установлен именно в это значение
        /// </summary>
        enFunctionEssentialMarkedTypes EssentialMarkedType
        { get; set; }

        /// <summary>
        /// Обработана ли последовательность statement-ов или нет
        /// </summary>
        enFunctionStatementLinked StatementLinked
        { get; set; }

        /// <summary>
        /// Оператор, с которого начинается выполнение тела функции.
        /// <remarks>
        /// Запрещается устанавливать данное значение, когда уже были указаны функции, вызываемые из данной. Подробнее см. <a href="31d573a6-8c67-4aed-887c-f102c8f1a04c.htm">Вызовы функций</a>.
        /// </remarks>
        /// </summary>
        IStatement EntryStatement { get; set; }

        /// <summary>
        /// Оператор, которым заканчивается выполнение функции. Является технологическим оператором и не соответствует ни одной конструкции 
        /// языка программирвоания
        /// </summary>
        IStatementExit ExitStatement { get; }

        /// <summary>
        /// Добавить функцию, вложенную в данную по вложенности определений.
        /// Подробнее <a href="6326713e-da32-4c6c-827d-35134fc08c94.htm">Вложенность сущностей по области видимости</a>.
        /// </summary>
        /// <param name="function">Функция, вложенная в данную.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Function")]
        void AddInnerFunction(IFunction function);

        /// <summary>
        /// Перечислить все функции, вложенные в данную.
        /// Подробнее <a href="6326713e-da32-4c6c-827d-35134fc08c94.htm">Вложенность сущностей по области видимости</a>.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IFunction> EnumerateInnerFunctions();

        /// <summary>
        /// Возвращает true, если функция не реализована в исходных текстах.
        /// Отличие значения, возвращаемого данной функцией, от проверки <c>EntryStatement==null</c>, в том, что если парсер не смог найти связь между функциями, то <c>EntryStatement==null &amp;&amp; IsExternal == false</c>, а если
        /// парсер смог распознать функцию и её реализация является либо системной, либо находится в файле с исполняемым кодом (например, .dll), то <c>EntryStatement==null &amp;&amp; IsExternal == true</c>.
        /// </summary>
        /// <remarks>По умолчанию, при создании экземпляра класса Function, значение IsExternal равно false. Парсер, когда определяет функцию как системную или реализованную в библиотеке без исходных текстов, должен явно указать 
        /// значение true.</remarks>
        /// <returns></returns>
        bool IsExternal { get; set; }

        /// <summary>
        /// Перечисляет переменные, определенные в данной функции
        /// </summary>
        /// <returns></returns>
        IEnumerable<Variable> EnumerateDefinedVariables();

        /// <summary>
        /// Перечисляет все операторы, реализованные в данной функции
        /// </summary>
        /// <returns></returns>
        IEnumerable<IStatement> EnumerateStatements();
    }

    /// <summary>
    /// Дополнительный интерфейс для функций, являющихся методами (то есть тех, для которых для некоторого класса вызван метод AddMethod(Store.Function)). 
    /// Для получения необходимо использовать поле Store.Function.asMethod
    /// </summary>
    public interface IMethod : IFunction
    {
        /// <summary>
        /// Указывает класс, методом которого является данная функция.
        /// </summary>
        Class methodOfClass { get; set; }

        /// <summary>
        /// true, если данный метод является переопределением одноимённого метода базового класса.
        /// Например в C#
        /// <code>
        /// class A
        /// {
        ///  void f() {}
        /// }
        /// class B : A
        /// {
        ///  override void f() {}
        /// }
        /// В этом случае для метода B.f() флаг isOverrideBase должен быть установлен true.
        /// </code>
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "is")]
        bool isOverrideBase { get; set; }
    }


    /// <summary>
    /// Реализация описателя функции.
    /// </summary>
    internal class Function_Impl : DataObjectLeaderWithName, IFunction
    {
        const string errorOnSecondDefinition = "Попытка добавить второе определение функции. Добавление не выполнено. ";
        const string errorSenderName = "Storage";
        const string errorOnFoundSecondDefinition = "Выявлено более одного определения функции";

        protected Functions theFunctions;
        FunctionLabels labels;
        UInt64 entryStatementId = Store.Const.CONSTS.WrongID;
        UInt64 exitStatementId = Store.Const.CONSTS.WrongID;

        internal Function_Impl(Functions functions) : base(functions.TableNaming)
        {
            theFunctions = functions;
        }

        public override string ToString()
        {
            return FullName;
        }

        #region Interface implementation

        public Module ImplementedInModule
        {
            get
            {
                UInt64 moduleId = ImplementedInModuleId;

                if (moduleId == Store.Const.CONSTS.WrongID)
                    return null;
                else
                    return theFunctions.storage.modules.GetModule(moduleId);
            }
        }

        public UInt64 ImplementedInModuleId
        {
            get
            {
                return theFunctions.storage.modules.TableFunctionImplementedInModule.GetValueByKey(theID);
            }
        }

        /// <summary>
        /// Тип возвращаемого функцией значения
        /// </summary>
        public string FunctionReturnType
        {
          get { return getLabels().FunctionReturnType; }
          set { getLabels().FunctionReturnType = value; }
        }

        public new virtual string Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                theFunctions.TableNaming.SetName(ref naming, theID, value);
            }
        }

        string IFunction.NameForReports
        {
            get
            {
                string name = Name;
                if (name == "")
                    name = "<Анонимная функция>";

                return name;
            }
        }

        internal override string NameForReports
        {
            get
            {
                return (this as IFunction).NameForReports;
            }
        }

        internal override string NameForFileName
        {
            get 
            {
                string name = Name;
                if (name == "")
                    name = "#Анонимная функция#";
                else
                    name = DataObjectLeaderWithName.DataObjectNameForFileName(name);

                name += "_" + System.Convert.ToString(Id);

                return name;
            }
        }

        #region Definition And  Declarations

        public Location Definition()
        {
            var locs = theFunctions.GetLocationsDefinitions().GetObjectLocation(theID);
            if (locs == null)
                return null;
            else
            {
                return locs;
            }
        }

        public Location[] Declarations()
        {
            return theFunctions.GetLocationsDeclarations().GetObjectLocations(theID);
        }

        public void AddDefinition(IFile file, ulong offset)
        {
            LocationsForSingle locs = theFunctions.GetLocationsDefinitions();
            locs.SetSingleLocation(theID, file, offset, null);
        }

        public void AddDefinition(IFile file, ulong line, ulong column)
        {
            LocationsForSingle locs = theFunctions.GetLocationsDefinitions();
            locs.SetSingleLocation(theID, file, line, column, null);
        }

        public void AddDefinition(string fullFileName, ulong offset)
        {
            LocationsForSingle locs = theFunctions.GetLocationsDefinitions();
            locs.SetSingleLocation(theID, theFunctions.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), offset, null);
        }

        public void AddDefinition(string fullFileName, enFileKindForAdd fileKind)
        {
            LocationsForSingle locs = theFunctions.GetLocationsDefinitions();
            locs.SetSingleLocationToVitrualFile(theID, theFunctions.storage.files.FindOrGenerate(fullFileName, fileKind));
        }


        public void AddDefinition(string fullFileName, ulong line, ulong column)
        {
            LocationsForSingle locs = theFunctions.GetLocationsDefinitions();
            locs.SetSingleLocation(theID, theFunctions.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), line, column, null);
        }

        public void AddDeclaration(IFile file, ulong offset)
        {
            theFunctions.GetLocationsDeclarations().AddLocation(theID, file, offset, null);
        }

        public void AddDeclaration(IFile file, ulong line, ulong column)
        {
            theFunctions.GetLocationsDeclarations().AddLocation(theID, file, line, column, null);
        }

        public void AddDeclaration(string fullFileName, ulong offset)
        {
            theFunctions.GetLocationsDeclarations().AddLocation(theID, theFunctions.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), offset, null);
        }

        public void AddDeclaration(string fullFileName, ulong line, ulong column)
        {
            theFunctions.GetLocationsDeclarations().AddLocation(theID, theFunctions.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), line, column, null);
        }
        public void AddDeclaration(string fullFileName, enFileKindForAdd fileKind)
        {
            theFunctions.GetLocationsDeclarations().AddToVitrualFile(theID, theFunctions.storage.files.FindOrGenerate(fullFileName, fileKind));
        }


        #endregion

        public IEnumerable<IFunctionCall> Calles()
        {
            foreach (IBufferReader buffer in theFunctions.indexFunctionCalles.GetValueByID(theID))
            {
                FunctionCall_Impl impl = theFunctions.tableFunctionCalls.Load(buffer.GetUInt64());
                yield return impl;
            }
        }

        public IEnumerable<IFunction> CallesUniq()
        {
            return Calles().Select(call => call.Calles).Distinct();

        }


        public IEnumerable<IFunctionCall> CalledBy()
        {
            foreach (IBufferReader buffer in theFunctions.indexFunctionCalledBy.GetValueByID(theID))
            {
                FunctionCall_Impl impl = theFunctions.tableFunctionCalls.Load(buffer.GetUInt64());
                yield return impl;
            }
        }

        public IEnumerable<IFunction> CalledByUniq()
        {
            return CalledBy().Select(call => call.Caller).Distinct();
        }

        public IEnumerable<PointsToFunction> OperatorsGetPointerToFunction()
        {
            foreach (IBufferReader buffer in theFunctions.tableOperationsGetPointerToFunction.FindValuesByKey(writer => writer.Add(Id)))
            {
                PointsToFunction p = theFunctions.storage.variables.GetPointsToFunction(buffer.GetUInt64());
                yield return p;
            }
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public void AddCall(IFunction function)
        {
            FunctionCall_Impl res = AddCall_impl(function);
            
            if(res != null)
                res.Save();
        }

        private FunctionCall_Impl AddCall_impl(IFunction function)
        {
            //Проверяем, что у данной функции нет тела. При наличии AddCall должен выдавать ошибку.
            LoadEntryStatement();
            if (entryStatementId != Store.Const.CONSTS.WrongID)
                throw new Exception("Попытка вызвать AddCall для функции, у которой есть тело. Функция " + function.FullName);

            return AddCall_basic(function, null);
        }

        /// <summary>
        /// Занести в индекс новую связь между функциями по вызову. Вызывающая функция - this, вызываемая - calles.
        /// </summary>
        /// <param name="calles">Вызываемая функция</param>
        /// <param name="location">Местоположение в исходных текстах, в котором происходит вызов</param>
        /// <returns></returns>
        internal FunctionCall_Impl AddCall_basic(IFunction calles, Location location)
        {
            //Проверяем, что не вносится повторений. Внесение повторений бессмысленно и может привести к непониманию пользователя.
            foreach (FunctionCall_Impl call in Calles())
                if (call.Caller == this && call.Calles == calles && ((call.CallLocation == null && location == null) || (call.CallLocation!= null && call.CallLocation.Equals( location ))))
                    return null;

            //Собственно заносим вызов
            FunctionCall_Impl res = theFunctions.tableFunctionCalls.Add();
            res.Caller = this;
            res.Calles = calles;
            
            if(location != null)
                res.CallLocation = location;

            return res;
        }

        //public void AddCall(ulong Id)
        //{
        //    if(theFunctions.tableFunctionCalls.PutWithoutRepetition(theID, buffer => buffer.Add(Id)))
        //        theFunctions.tableFunctionCalledBy.PutWithRepetition(Id, buffer => buffer.Add(theID));
        //}

        public void AddCall(IFunction functionn, IFile file, ulong offset)
        {
            FunctionCall_Impl res = AddCall_impl(functionn);
            if (res != null)
            {
                theFunctions.GetLocationsCalles().SetSingleLocation(res.ID, file, offset, this);
                res.Save();
            }
        }

        public void AddCall(IFunction functionn, IFile file, ulong line, ulong column)
        {
            FunctionCall_Impl res = AddCall_impl(functionn);
            theFunctions.GetLocationsCalles().SetSingleLocation(res.ID, file, line, column, this);
            res.Save();
        }

        public void AddCall(IFunction functionn, string fullFileName, ulong offset)
        {
            FunctionCall_Impl res = AddCall_impl(functionn);
            IFile file = theFunctions.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix);
            theFunctions.GetLocationsCalles().SetSingleLocation(res.ID, file, offset, this);
            res.Save();
        }

        public void AddCall(IFunction functionn, string fullFileName, ulong line, ulong column)
        {
            FunctionCall_Impl res = AddCall_impl(functionn);
            IFile file = theFunctions.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix);
            theFunctions.GetLocationsCalles().SetSingleLocation(res.ID, file, line, column, this);
            res.Save();
        }

        public void AddSensor(ulong ID, Kind kind)
        {
            theFunctions.storage.sensors.AddSensor(ID, theID, kind);
        }

        public IEnumerable<IFunction> ImportedByModules()
        {
            theFunctions.storage.modules.Get_tableFunctionImports();

            foreach (IBufferReader buf in theFunctions.storage.modules.tableFunctionImports.Get(Id))
                yield return theFunctions.GetFunction(buf.GetUInt64());

        }

        public enFunctionEssentials Essential
        {
            get
            {
                return getLabels().Essential;
            }
            set
            {
                getLabels().Essential = value;
            }
        }

        public enFunctionEssentialsUnderstand EssentialUnderstand
        {
            get
            {
                return getLabels().EssentialUnderstand;
            }
            set
            {
                getLabels().EssentialUnderstand = value;
            }
        }

        public enFunctionUseKinds UseKind
        {
            get
            {
                return getLabels().Kind;
            }
            set
            {
                getLabels().Kind = value;
            }
        }

        public enFunctionKind kind
        {
            get
            {
                if (Name.Equals("Глобальное окружение"))
                    return enFunctionKind.PSEUDO;
                else
                    return enFunctionKind.REAL;
            }
        }

        public enFunctionStatementLinked StatementLinked
        {
            get
            {
                return getLabels().StatementLinked;
            }
            set
            {
                getLabels().StatementLinked = value;
            }
        }

        public enFunctionEssentialMarkedTypes EssentialMarkedType
        {
            get
            {
                return getLabels().EssentialMarkedType;
            }
            set
            {
                getLabels().EssentialMarkedType = value;
            }
        }

        private FunctionLabels getLabels()
        {
            if (labels == null)
                labels = new FunctionLabels_Impl(theFunctions, theID);

            return labels;
        }

        bool isLoaded = false;
        void LoadEntryStatement()
        {
            if (!isLoaded)
            {
                isLoaded = true;
                entryStatementId = theFunctions.tableEntryStatements.GetUInt64ValueByID(theID);
            }
        }

        public IStatement EntryStatement
        {
            get
            {
                LoadEntryStatement();
                return theFunctions.storage.statements.GetStatement(entryStatementId);
            }
            set
            {
                //Проверяем, что для функции не вызывался AddCall
                if (entryStatementId == Store.Const.CONSTS.WrongID && theFunctions.indexFunctionCalles.GetValueByID(theID).GetEnumerator().MoveNext())
                    throw new Exception("Попытка добавить тело функции, у которой уже заполнены вызовы");

                //Для старого значения указываем, что оно более не принадлежит ни одной функции
                if (entryStatementId != Store.Const.CONSTS.WrongID)
                    (EntryStatement as Statement_Impl).PutAboveRecursive(null, null, EntryStatement);

                //Запомниаем идентификатор начального оператора
                entryStatementId = (value as Statement_Impl).Id;
                theFunctions.tableEntryStatements.PutUInt64Value(theID, entryStatementId);

                //Перестраиваем индексы
                (value as Statement_Impl).SetImplementsFunction(this, (st) => { });
            }
        }

        public IStatementExit ExitStatement
        {
            get
            {
                if (exitStatementId == Store.Const.CONSTS.WrongID)
                    exitStatementId = theFunctions.tableExitStatements.GetUInt64ValueByID(theID);
                if (exitStatementId == Store.Const.CONSTS.WrongID)
                {
                    IStatementExit res = theFunctions.storage.statements.AddExitStatement();
                    exitStatementId = res.Id;
                    return res;
                }
                return (IStatementExit)theFunctions.storage.statements.GetStatement(exitStatementId);
            }
        }

        internal override DataObjectLeaderWithName GetContainer()
        {
            UInt64 id = theFunctions.tableFunctionInFunction.GetValueByID(theID);

            if (id != Store.Const.CONSTS.WrongID)
                return (DataObjectLeaderWithName)theFunctions.GetFunction(id);
            
            id = theFunctions.storage.classes.tableFunctionInClass.GetValueByID(theID);
            
            if (id != Store.Const.CONSTS.WrongID)
                return (DataObjectLeaderWithName) theFunctions.storage.classes.GetClass(id);

            id = theFunctions.storage.namespaces.tableFunctionNamespace.GetValueByID(theID);

            if (id != Store.Const.CONSTS.WrongID)
                return theFunctions.storage.namespaces.tableLeader.Get(id);
                
            return null;
        }

        public void AddInnerFunction(IFunction function)
        {
            if (function != null)
            {
                theFunctions.tableInnerFunctions.PutWithoutRepetition(Id, function.Id);
                theFunctions.tableFunctionInFunction.Put(function.Id, Id);
            }
        }

        public IEnumerable<IFunction> EnumerateInnerFunctions()
        {
            foreach (UInt64 identif in theFunctions.tableInnerFunctions.GetValueByKey(this.Id))
                yield return theFunctions.GetFunction(identif);
        }

        public bool IsExternal
        {
            get
            {
                getLabels();
                return labels.IsExternal;
            }
            set
            {
                getLabels();
                labels.IsExternal = value;
            }
        }

        public IEnumerable<Variable> EnumerateDefinedVariables()
        {
            Variables vars = theFunctions.storage.variables;
            foreach (UInt64 varId in vars.tableFunctionHaveVariableDefinitions.GetValueByKey(theID))
                yield return vars.GetVariable(varId);
        }

        public IEnumerable<IStatement> EnumerateStatements()
        {
            //Используем очередь на отработку
            Queue<IStatement> list = new Queue<IStatement>();
            list.Enqueue(EntryStatement);
            list.Enqueue(ExitStatement);

            while (list.Count > 0)
            {
                IStatement cur = list.Dequeue();
                //Каждый элемент очереди является оператором функции
                yield return cur;

                //Добавляем в очередь все подчинённые операторы
                cur.SubStatements().All(a => { list.Enqueue(a); return true; });
                if (cur.NextInLinearBlock != null)
                    list.Enqueue(cur.NextInLinearBlock);
            }
        }


        #endregion
    }


    internal class Method_Impl : Function_Impl, IMethod
    {
        Functions functions;

        internal Method_Impl(Functions functions)
            : base(functions)
        {
            this.functions = functions;
        }

        public Class methodOfClass
        {
            get 
            {
                Classes classes = theFunctions.storage.classes;
                UInt64 classId = classes.tableFunctionInClass.GetValueByID(theID);
                return classes.GetClass(classId);
            }
            set
            {
                Classes classes = theFunctions.storage.classes;
                classes.tableClassContainsFunction.PutWithoutRepetition(value.Id, theID);
                classes.tableFunctionInClass.Put(theID, value.Id);
            }
        }

        public bool isOverrideBase
        {
            get 
            {
                bool res = false;
                
                using (IBufferReader r = functions.tableMethodsData.GetValueByID(theID))
                    r.GetBool(ref res);
                
                return res;
            }
            set
            {
                functions.tableMethodsData.Put(theID, buf => buf.Add(value));
            }
        }

        public override string Name
        {
            get { return base.Name; }
            set 
            { 
                base.Name = value;
                functions.storage.classes.UpdateVisibleMethodsByNewMethodName(this);
            }
        }
    }
}
