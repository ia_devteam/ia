﻿using System;
using System.Collections.Generic;
using Store.BaseDatatypes;
using Store.Table;

namespace Store
{
    /// <summary>
    /// Тип функции
    /// </summary>
    internal enum enFunctionDefinitionKind
    {
        /// <summary>
        /// Функция вне класса.
        /// </summary>
        FUNCTION, 
        /// <summary>
        /// Функция является методом некоторого класса.
        /// </summary>
        METHOD
    }

    public enum enFunctionKind
    {
        /// <summary>
        /// Функция получена в исходных текстах
        /// </summary>
        REAL = 1,

        /// <summary>
        /// Функция сгенерирована анализатором и не определена в исходных текстах. Пример - "Глобальное окружение"
        /// </summary>
        PSEUDO = 2, 

        /// <summary>
        /// Все функции вне зависимости от типа
        /// </summary>
        ALL = REAL | PSEUDO
    }

    /// <summary>
    /// Головной класс для работы с функциями.
    /// </summary>
    public sealed class Functions
    {
        //Имена таблиц
        const string TableName_Functions = "Functions";
        const string TableName_functionLables = "functionLabels";
        const string TableName_LocationsDefinitions = "FunctionLocationsDefinitions";
        const string TableName_LocationsDeclarations = "FunctionLocationsDeclarations";
        const string TableName_LocationsCalles = "FunctionLocationsCalles";
        const string TableName_IndexByName = "iFunctionByName";
        const string TableName_Naming = "FunctionsNaming";
        const string TableName_FunctionCall = "FunctionCall";
        const string TableName_FunctionCalledBy = "iFunctionCalledBy";
        const string TableName_FunctionCalles = "iFunctionCalles";
        const string TableName_OperationsGetPointerToFunction = "iFunctionOperationsGetPointerToFunctionn";
        const string TableName_tableImplementedInModule = "FunctionImplementedInModule";
        const string TableName_EntryStatements = "FunctionEntryStatements";
        const string TableName_ExitStatements = "FunctionExitStatements";
        const string TableName_InnerFunctions = "InnerFunctions";
        const string TableName_FunctionInFunction = "FunctionInFunction";
        const string TableName_MethodsData = "MethodsData";



        LocationsForSingle locationsDefinitions;
        LocationsForMultiple locationsDeclarations;
        LocationsForSingle locationsCalles;

        internal Storage storage;

        //Основные таблицы
        TableLeader<Function_Impl> tableFunctions;

        /// <summary>
        /// Хранение имен функций
        /// </summary>
        internal TableObjectNamingWithIndexAuto<IFunction> TableNaming;


        internal Table_KeyId_ValueSaver_Nodublicate tableLabels;
       
        /// <summary>
        /// Ключ - идентификатор вызывающей функции.
        /// Значение - идентификатор вызываемой функции.
        /// </summary>
        internal TableIDNoDuplicate_with_WeakRefTable<FunctionCall_Impl> tableFunctionCalls;

        internal Table_KeyId_ValueSaver_Nodublicate tableEntryStatements;
        internal Table_KeyId_ValueSaver_Nodublicate tableExitStatements;


        /// <summary>
        /// Ключ - идентификатор функции, в которую вложеннна функция
        /// Значение - идентификатор вложенной функции
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableInnerFunctions;

        /// <summary>
        /// Ключ - идентификатор функции
        /// Значение - данные метода.
        /// </summary>
        internal Table_KeyId_ValueSaver_Nodublicate tableMethodsData;


        //Индексные таблицы

        /// <summary>
        /// Ключ - идентификатор вызываемой функции.
        /// Значение - идентификатор FunctionCall из таблицы tableFunctionCalls.
        /// </summary>
        internal Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates indexFunctionCalledBy;

        /// <summary>
        /// Ключ - идентификатор вызывающей функции.
        /// Значение - идентификатор FunctionCall из таблицы tableFunctionCalls.
        /// </summary>
        internal Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates indexFunctionCalles;

        /// <summary>
        /// Ключ - идентификатор функции, в которой производится присвоение указателя.
        /// Значение - идентификатор PointsToFunction, описывающей присвоение указателя.
        /// </summary>
        internal Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates tableOperationsGetPointerToFunction;

        /// <summary>
        /// Ключ - идентификатор вложенной функции
        /// Значение - идентификатор функции, в которую вложена функция, указанная по ключу
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueId_NoDuplicate tableFunctionInFunction;




        #region Generate Functions

        /// <summary>
        /// Создавать экземпляры Functions самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal Functions(Storage storage)
        {
            this.storage = storage;

            tableFunctions = new TableLeader<Function_Impl>(storage, TableName_Functions, CreateFunction);
            //tableFuncByName = new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates(storage, TableName_IndexByName, 
            //                                                                EnumerateItemsIndexFuncByName);
            TableNaming = new TableObjectNamingWithIndexAuto<IFunction>(storage, TableName_Naming, TableName_IndexByName, EnumerateItemsIndexFuncByName);
            tableFunctionCalls = new TableIDNoDuplicate_with_WeakRefTable<FunctionCall_Impl>(storage, TableName_FunctionCall, () => new FunctionCall_Impl(this));
            indexFunctionCalledBy = new Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates(storage, TableName_FunctionCalledBy);
            indexFunctionCalles = new Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates(storage, TableName_FunctionCalles);
            tableLabels = new Table_KeyId_ValueSaver_Nodublicate(storage, TableName_functionLables);
            tableOperationsGetPointerToFunction = new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates(storage, 
                                            TableName_OperationsGetPointerToFunction,
                                            EnumerateItemsIndexOperationsGetPointerToFunction);

            tableEntryStatements = new Table_KeyId_ValueSaver_Nodublicate(storage, TableName_EntryStatements);
            tableExitStatements = new Table_KeyId_ValueSaver_Nodublicate(storage, TableName_ExitStatements);
            tableInnerFunctions = new Table_KeyId_ValueId_Dublicates(storage, TableName_InnerFunctions);
            tableFunctionInFunction = new Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, TableName_FunctionInFunction, EnumerateFunctionInFunction);
            tableMethodsData = new Table_KeyId_ValueSaver_Nodublicate(storage, TableName_MethodsData);
        }

        private Function_Impl CreateFunction(Int32 kind)
        {
            switch ((enFunctionDefinitionKind)kind)
            {
                case enFunctionDefinitionKind.FUNCTION:
                    return new Function_Impl(this);
                case enFunctionDefinitionKind.METHOD:
                    return new Method_Impl(this);
                default:
                    throw new Exception("Неизвестный вид функции");
                    return null;
            }
        }


        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult> EnumerateItemsIndexFuncByName(IBufferWriter key, IBufferWriter value)
        {
            foreach (Function_Impl function in tableFunctions.EnumerateEveryone())
            {
                key.Add(function.Name);
                value.Add(function.Id);

                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, null );
            }
        }

        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult> EnumerateItemsIndexOperationsGetPointerToFunction(IBufferWriter key, IBufferWriter value)
        {
            foreach (PointsToFunction p in storage.variables.EnumeratePointsToFunction())
            {
                PointsToFunction_Impl pi = p as PointsToFunction_Impl;

                key.Add(p.position.GetFunctionId());
                value.Add(pi.ID);

                pi.Marker_tableOperationsGetPointerToFunction.InformLoaded(b => b.Add(pi.position.GetFunctionId()));

                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, pi.Marker_tableOperationsGetPointerToFunction);
            }
        }

        private IEnumerable<KeyValuePair<UInt64, UInt64>> EnumerateFunctionInFunction()
        {
            foreach (KeyValuePair<UInt64, UInt64> pair in tableInnerFunctions.EnumerateEveryone())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Value, pair.Key);
        }

        #endregion

        /// <summary>
        /// Получить функцию по ее идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IFunction GetFunction(UInt64 id)
        {
            return tableFunctions.Get(id);
        }

        /// <summary>
        /// Получить все функции, записанные в Хранилище с указанным именем.
        /// </summary>
        /// <param name="name">Идентификатор функции</param>
        /// <returns></returns>
        public IFunction[] GetFunction(string name)
        {
            return TableNaming.GetEntity(name, GetFunction);
        }

        /// <summary>
        /// Получить все функции, записанные в Хранилище с указанным именем.
        /// </summary>
        /// <param name="name">Полное имя функции</param>
        /// <returns></returns>
        public IFunction[] GetFunction(string []name)
        {
            return TableNaming.GetEntity(name, GetFunction);
        }

        /// <summary>
        /// Найти все функции с данным частичным именем (суффиксом). Например, по имени C1.C2 будет найдена функция C0.C1.C2.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IFunction[] FindFunction(string[] name)
        {
            return TableNaming.FindEntityBySubname(name, GetFunction);
        }

        /// <summary>
        /// Перечислить все функции, имеющиеся в Хранилище
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IFunction> EnumerateFunctions(enFunctionKind type)
        {
            foreach (Function_Impl func in tableFunctions.EnumerateEveryone())
            {
                if((type & func.kind ) == func.kind)
                    yield return func;
            }
        }

        /// <summary>
        /// Перечислить все функции, имеющиеся в Хранилище, в сортированном по местоположению порядке.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LocationFunctionPair> EnumerateFunctionLocationsInSortOrder()
        {
            foreach (Locations.IDLocationPair pair in GetLocationsDefinitions().EnumerateLocationsInFileLineOrder())
            {
                LocationFunctionPair res;
                res.location = pair.location;
                res.function = GetFunction(pair.ID);

                yield return res;
            }
        }

        /// <summary>
        /// Возвращает количество функций, имеющихся в Хранилище.
        /// </summary>
        public UInt64 Count
        {
            get
            {
                return tableFunctions.Count();
            }
        }

        internal LocationsForSingle GetLocationsDefinitions()
        {
            if (locationsDefinitions == null)
                locationsDefinitions = storage.locationsForSingle(TableName_LocationsDefinitions);

            return locationsDefinitions;
        }

        internal LocationsForMultiple GetLocationsDeclarations()
        {
            if (locationsDeclarations == null)
                locationsDeclarations = storage.locationsForMultiple(TableName_LocationsDeclarations);

            return locationsDeclarations;
        }

        internal LocationsForSingle GetLocationsCalles()
        {
            if (locationsCalles == null)
                locationsCalles = storage.locationsForSingle(TableName_LocationsCalles);

            return locationsCalles;
        }

        /// <summary>
        /// Добавить новую функцию в Хранилище
        /// </summary>
        /// <returns></returns>
        public IFunction AddFunction()
        {
            return tableFunctions.Add((Int32) enFunctionDefinitionKind.FUNCTION);
        }

        /// <summary>
        /// Добавить новый метод в Хранилище
        /// </summary>
        /// <returns></returns>
        public IMethod AddMethod()
        {
            return (IMethod) tableFunctions.Add((Int32)enFunctionDefinitionKind.METHOD);
        }


        /// <summary>
        /// Хэширует актуальной ссылки на Locations.
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<UInt64> EnumerateFunctionsDefinedInFile(IFile file)
        {
            return GetLocationsDefinitions().EnumerateObjectsLocatedInFile(file);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder"></param>
        public void Dump(string folder)
        {
            using (System.IO.StreamWriter wr = new System.IO.StreamWriter(System.IO.Path.Combine(folder, "functions.log")))
            {
                wr.WriteLine("Дамп функций");

                foreach (IFunction func in EnumerateFunctions(enFunctionKind.ALL))
                {
                    wr.Write(func.Id + "\t" +
                                    func.Name + "\t" +
                                    func.Essential.ToString() + "\t" +
                                    func.EssentialMarkedType.ToString() + "\t" +
                                    func.UseKind.ToString());

                    wr.Write("\t определения: ");
                    Location location = func.Definition();
                    if (location != null)
                        wr.Write("(" + location.GetFileID().ToString(System.Globalization.CultureInfo.InvariantCulture)
                            + ", " + location.GetLine() + ", " + location.GetColumn() + ")");

                    wr.Write("\t декларации: ");
                    foreach (Location loc in func.Declarations())
                        wr.Write("(" + loc.GetFileID().ToString(System.Globalization.CultureInfo.InvariantCulture)
                            + ", " + loc.GetLine() + ", " + loc.GetColumn() + ")");

                    wr.Write("\t вызывает: ");
                    foreach (IFunctionCall func1 in func.Calles())
                        wr.Write(func1.Calles.Id.ToString(System.Globalization.CultureInfo.InvariantCulture) + " ");

                    wr.Write("\t вызывается из: ");
                    foreach (IFunctionCall func1 in func.CalledBy())
                        wr.Write(func1.Caller.Id.ToString(System.Globalization.CultureInfo.InvariantCulture) + " ");

                    wr.WriteLine();
                }
            }
        }
    }
}
