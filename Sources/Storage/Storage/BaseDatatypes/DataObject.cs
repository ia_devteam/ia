﻿using System;
using System.Text;
using Store.Table;

namespace Store
{
    /// <summary>
    /// Интерфейс должен реализовываться всеми классами, имеющими идентификатор
    /// </summary>
    public interface IDataObjectLeader
    {
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        UInt64 Id { get; }
    }
}

namespace Store.BaseDatatypes
{

    /// <summary>
    /// Большинство разделов в Хранилище представлены набором таблиц. 
    /// Головной является таблица, хранящая перечень идентификаторов объектов, без каких-либо значений.
    /// Именно такие объекты представляет DataObjectLeader
    /// </summary>
    internal class DataObjectLeader : WeakRefTableObject, IDataObjectLeader
    {
        protected UInt64 theID;

        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        public UInt64 Id
        {
            get { return theID; }
        }

        internal void SetId(UInt64 Id)
        {
            theID = Id;
        }
    }


    /// <summary>
    /// Объекты DataObjectLeader, имеющие имя.
    /// Имя хранится в отдельной таблице, указываемой в конструкторе.
    /// </summary>
    internal abstract class DataObjectLeaderWithName : DataObjectLeader
    {
        internal Naming naming;
        TableObjectNamingWithIndex tableNaming;

        internal DataObjectLeaderWithName(TableObjectNamingWithIndex tableNaming)
        {
            this.tableNaming = tableNaming;
        }

        /// <summary>
        /// Идентификатор данной сущности
        /// </summary>
        public string Name
        {
            get { return tableNaming.GetName(ref naming, theID); }
        }

        /// <summary>
        /// Возвращает полное имя
        /// </summary>
        public string FullName
        {
            get 
            { 
                DataObjectLeaderWithName cont = GetContainer();
                if (cont == null)
                    return Name;
                else
                    return cont.FullName + "." + Name;
            }
        }

        /// <summary>
        /// Возвращает полное имя, которое следует использовать при формировании отчётов
        /// </summary>
        public string FullNameForReports
        {
            get
            {
                DataObjectLeaderWithName cont = GetContainer();
                if (cont == null)
                    return NameForReports;
                else
                    return cont.FullName + "." + NameForReports;
            }
        }

        /// <summary>
        /// Возвращает полное имя, которое следует использовать при формировании имен файлоа
        /// </summary>
        public string FullNameForFileName
        {
            get
            {
                DataObjectLeaderWithName cont = GetContainer();
                if (cont == null)
                    return NameForFileName;
                else
                    return cont.FullNameForFileName + "." + NameForFileName;
            }
        }

        /// <summary>
        /// замена в имени сущности символов, недопустимых в имени файла
        /// </summary>
        /// <param name="name">имя</param>
        /// <returns></returns>
        public static string DataObjectNameForFileName(string name)
        {
            return name.Replace("<", "#LessThan#").Replace(">", "#GraterThan#").Replace("+", "#Plus#").Replace("-", "#Minus#").Replace("\\", "#BackSlash#").Replace("/", "#Slash#").Replace("\"", "#DoubleQuote#").Replace("'", "#SingleQuote#").Replace("*", "#Multiply#").Replace("=", "#Equal#").Replace(":", "#Colon#").Replace("|", "#Line#");
        }

        /// <summary>
        /// Проверяет, что текущий объект имеет полное имя, указанное в параметре
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool isFullName(string[] name)
        {
            return isFullName_isSubName(name, true);
        }

        /// <summary>
        /// Проверяет, что полное имя текущего объекта включает имя, указанное в параметре
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool isSubName(string[] name)
        {
            return isFullName_isSubName(name, false);
        }

        /// <summary>
        ///Реализация двух функций <see cref="DataObjectLeaderWithName.isFullName"/> и <see cref="DataObjectLeaderWithName.isSubName"/> одновременно.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isFullName"></param>
        /// <returns></returns>
        private bool isFullName_isSubName(string[] name, bool isFullName)
        {
            DataObjectLeaderWithName cur = this;

            for (int i = name.Length - 1; i >= 0; i--)
            {
                if (cur == null)
                    return false;

                if (cur.Name != name[i])
                    return false;

                cur = cur.GetContainer();
            }

            if (isFullName)
                //Реализцем isFullName
                return cur == null;
            else
                //Реализцем isSubName
                return true;
        }


        /// <summary>
        /// Получить сущность, в которую включена данная.
        /// Возвращает null, если данная сущность не входит ни в какую другую сущность
        /// </summary>
        /// <returns></returns>
        internal abstract DataObjectLeaderWithName GetContainer();

        /// <summary>
        /// Имя сущности, которое следует использовать при формировании отчетов
        /// </summary>
        internal abstract string NameForReports { get; }

        /// <summary>
        /// Имя сущности, которое следует использовать при формировании имён файлов
        /// </summary>
        internal abstract string NameForFileName { get; }
    }


    internal abstract class DataObject_IDNoDuplicate : WeakRefTableObject
    {
        /// <summary>
        /// Возвращает сторку, используемую для выдачи сообющения об ошибке в случае попытки загрузить элемент,
        /// отсутствующий в базе.
        /// </summary>
        /// <returns></returns>
        protected abstract string GetLoadErrorString();
        /// <summary>
        /// Загрузить свое состояние из буфера
        /// </summary>
        /// <param name="reader"></param>
        protected abstract void Load(IBufferReader reader);
        /// <summary>
        /// Сохранить свое состояние в буффер
        /// </summary>
        protected abstract void SaveToBuffer(IBufferWriter buffer);
        /// <summary>
        /// Обновить все индексы для этого элемента.
        /// </summary>
        protected abstract void SaveIndexes();


        protected UInt64 theID;
        protected TableIDNoDuplicate theTable;

        public UInt64 ID
        {
            get { return theID; }
        }

        /// <summary>
        /// Сохранить свое состояние в базу
        /// </summary>
        public virtual void Save()
        {
            //Сохраняем сам объект
            using (IBufferWriter buffer = WriterPool.Get())
            {
                SaveToBuffer(buffer);
                theTable.Put(ID, buffer);
            }

            //Обновляем все индексы
            SaveIndexes();
        }

        /// <summary>
        /// Загружает объект, если он присутствует в БД. Иначе IA.Monitor.Log.TerminationError
        /// </summary>
        /// <param name="table"></param>
        /// <param name="ID"></param>
        public void Load(TableIDNoDuplicate table, UInt64 ID)
        {
            this.theTable = table;
            theID = ID;

            IBufferReader reader;
            if (table.Get(ID, out reader) != enResult.OK)
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendFormat(GetLoadErrorString(), ID);
                throw new Exception(builder.ToString());
            }

            Load(reader);
            reader.Dispose();
        }

        /// <summary>
        /// Инициализация объекта, уже прочитанного из базы данных
        /// </summary>
        /// <param name="table">Таблица, в которой живет объект</param>
        /// <param name="ID">Идентификатор объекта</param>
        /// <param name="reader">Буфер с данными объекта</param>
        public void Initialize(TableIDNoDuplicate table, UInt64 ID, IBufferReader reader)
        {
            this.theTable = table;
            theID = ID;

            if(reader != null)
                Load(reader);
        }

        /// <summary>
        /// Загрузка объекта из базы данных. Если объект отсутствует, он будет создан.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="ID"></param>
        public void LoadOrGenerate(TableIDNoDuplicate table, UInt64 ID)
        {
            this.theTable = table;
            theID = ID;

            IBufferReader reader;
            if (table.Get(ID, out reader) == enResult.OK)
            {
                Load(reader);
                reader.Dispose();
            }
            else
            {
                using (IBufferWriter writer = WriterPool.Get())
                {
                    SaveToBuffer(writer);
                    table.Put(ID, writer);
                }
            }
        }

        /// <summary>
        /// Добавление нового объекта в БД
        /// </summary>
        /// <param name="table"></param>
        public void Generate(TableIDNoDuplicate table)
        {
            this.theTable = table;

            using (IBufferWriter buffer = WriterPool.Get())
            {
                SaveToBuffer(buffer);
                theID = table.Add(buffer);
            }
        }
    }

    /// <summary>
    /// Объекты этого типа используют ленивое сохранение. То есть сохраняют свое состояние в базу только
    /// при выгрузке из памяти.
    /// </summary>
    internal abstract class DataObject_IDNoDuplicate_Lazy : DataObject_IDNoDuplicate, IDisposable
    {
        protected bool isChanged = false;
        private bool theIsFinalized = false;

        ~DataObject_IDNoDuplicate_Lazy()
        {
            Save();
            theIsFinalized = true;
        }

        public void Dispose()
        {
            Save();
            theIsFinalized = true;

            GC.SuppressFinalize(this);
        }

        public override sealed void Save()
        {
            if (isChanged)
            {
                base.Save();
                isChanged = false;
            }
        }

        internal override bool isFinalized()
        {
            return theIsFinalized;
        }

    }

    /// <summary>
    /// Объекты этого типа сохраняют свое состояние в базу сразу же после изменения состояния.
    /// </summary>
    internal abstract class DataObject_IDNoDuplicate_Immediate : DataObject_IDNoDuplicate
    {
    }
}
