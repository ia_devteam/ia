﻿using System;
using System.Collections.Generic;
using Store.Table;

namespace Store.BaseDatatypes
{
    class Table_KeyId_ValueId_Dublicates
    {
        TableIDDuplicate table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;

        /// <summary>
        /// Открывает таблицу
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName">Имя таблицы с индексом</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public Table_KeyId_ValueId_Dublicates(Storage storage, string tableName)
        {
            this.storage = storage;
            this.tableName = tableName;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void AcquireTable()
        {
            if(table == null)
                table = storage.GetTableIDDuplicate(tableName);
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null)
                    table = storage.TryOpenTableIDDuplicates(tableName);
            }
        }


        /// <summary>
        /// Возвращает все значения по указанному ключу.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<UInt64> GetValueByKey(UInt64 ID)
        {
            TryAcquireTable();
            if (table == null)
                yield break;

            foreach(IBufferReader reader in table.Get(ID))
                yield return reader.GetUInt64();
        }

        /// <summary>
        /// Положить (ключ,значение) в базу. Если такие (ключ,значение) уже имеются в базе, то не класть.
        /// </summary>
        /// Возвращает true, если добавление потребовалось. Иначе false.
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void PutWithoutRepetition(UInt64 key, UInt64 value)
        {
            AcquireTable();
            using (IBufferWriter valBuf = WriterPool.Get())
            {
                valBuf.Add(value);

                if (!table.Contains(key, valBuf))
                    table.Put(key, valBuf);
            }
        }

        /// <summary>
        /// Положить (ключ,значение) в базу. Если такие (ключ,значение) уже имеются в базе, то положить еще один.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void PutWithRepetition(UInt64 key, UInt64 value)
        {
            AcquireTable();
            using (IBufferWriter valBuf = WriterPool.Get())
            {
                valBuf.Add(value);
                table.Put(key, valBuf);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<KeyValuePair<UInt64, UInt64>> EnumerateEveryone()
        {
            TryAcquireTable();
            if (table != null)
                foreach (KeyValuePair<UInt64, IBufferReader> pair in table)
                    yield return new KeyValuePair<UInt64, UInt64>(pair.Key, pair.Value.GetUInt64());
        }

        public void Clear()
        {
            if (table != null)
            {
                storage.DeleteDB(tableName, ref table);
                table = null;
            }
        }
    }


    class Table_KeyId_ValueId_NoDublicates
    {
        TableIDNoDuplicate table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;

        /// <summary>
        /// Открывает таблицу
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName">Имя таблицы с индексом</param>
        public Table_KeyId_ValueId_NoDublicates(Storage storage, string tableName)
        {
            this.storage = storage;
            this.tableName = tableName;
        }

        private void AcquireTable()
        {
            if (table == null)
                table = storage.GetTableIDNoDuplicate(tableName);
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null)
                    table = storage.TryOpenTableIDNoDuplicate(tableName);
            }
        }


        /// <summary>
        /// Возвращает значение по указанному ключу.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public UInt64 GetValueByKey(UInt64 ID)
        {
            TryAcquireTable();
            if (table == null)
                return Store.Const.CONSTS.WrongID;

            IBufferReader reader;
            table.Get(ID, out reader);
            if (reader == null)
                return Store.Const.CONSTS.WrongID;
            else
            {
                UInt64 res = reader.GetUInt64();
                reader.Dispose();
                return res;
            }
        }

        /// <summary>
        /// Положить (ключ,значение) в базу. Если такие (ключ,значение) уже имеются в базе, то не класть.
        /// </summary>
        /// Возвращает true, если добавление потребовалось. Иначе false.
        public void Put(UInt64 key, UInt64 value)
        {
            AcquireTable();
            using (IBufferWriter valBuf = WriterPool.Get())
            {
                valBuf.Add(value);
                table.Put(key, valBuf);
            }
        }

        public IEnumerable<KeyValuePair<UInt64, UInt64>> EnumerateEveryone()
        {
            TryAcquireTable();
            if (table != null)
                foreach (KeyValuePair<UInt64, IBufferReader> pair in table)
                    yield return new KeyValuePair<UInt64, UInt64>(pair.Key, pair.Value.GetUInt64());
        }

        internal void Remove(ulong theID)
        {
            TryAcquireTable();
            if (table != null)
                table.Remove(theID);
        }
    }
}
