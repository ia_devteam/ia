﻿using System;
using System.Collections.Generic;
using Store.Table;


namespace Store.BaseDatatypes
{
    internal class Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates
    {
        TableIDDuplicate table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;

        public Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates(Storage storage, string tableName)
        {
            this.storage = storage;
            this.tableName = tableName;
        }

        public IEnumerable<IBufferReader> GetValueByID(ulong theID)
        {
            TryAcquireTable();
            if (table != null)
                return table.Get(theID);
            else
                return NullIterator();
        }

        private IEnumerable<IBufferReader> NullIterator()
        {
            yield break;
        }

        public delegate void Saver(IBufferWriter writer);

        public bool PutWithoutRepetition(UInt64 ID, Saver saverValue)
        {
            AcqireTable();

            using (IBufferWriter buffer = WriterPool.Get())
            {
                saverValue(buffer);

                if (!table.Contains(ID, buffer))
                {
                    table.Put(ID, buffer);
                    return true;
                }
            }
            return false;
        }

        public void PutWithRepetition(UInt64 ID, Saver saverValue)
        {
            AcqireTable();

            using (IBufferWriter buffer = WriterPool.Get())
            {
                saverValue(buffer);
                table.Put(ID, buffer);
            }

        }

        /// <summary>
        /// Удаляет пару ключ-значение из базы данных
        /// </summary>
        /// <param name="Id">Ключ</param>
        /// <param name="deletedBuffer">Значение</param>
        internal void Remove(ulong Id, Saver deletedBuffer)
        {
            TryAcquireTable();
            if (table != null)
                using (IBufferWriter buffer = WriterPool.Get())
                {
                    deletedBuffer(buffer);
                    table.Remove(Id, buffer);
                }
        }


        /// <summary>
        /// Удалить из индекса все записи. Согласованность индекса и основной таблицы остается на совести удаляющего.
        /// </summary>
        public void RemoveAll()
        {
            storage.DeleteDB(tableName, ref table);
        }

        private void AcqireTable()
        {
            if (table == null)
                table = storage.GetTableIDDuplicate(tableName);
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null) 
                    table = storage.TryOpenTableIDDuplicates(tableName);
            }
        }

    }



    /// <summary>
    /// Таблица, хранящая данные, ключём которых является буфер, а значением - идентификатор.
    /// Таблица без дубликатов.
    /// </summary>
    internal class Table_ImmediateCreation_KeyBuffer_ValueId_NoDuplicate
    {
        TableIndexedNoDuplicates table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;

        public Table_ImmediateCreation_KeyBuffer_ValueId_NoDuplicate(Storage storage, string tableName)
        {
            this.storage = storage;
            this.tableName = tableName;
        }

        public delegate void Saver(IBufferWriter writer);

        /// <summary>
        /// Возвращает элемент, хранимый в индексе по указанному ключу.
        /// Возвращает null, если по указанному ключу в индексе ничего не хранится.
        /// </summary>
        /// <param name="writerKey"></param>
        /// <returns></returns>
        public UInt64 GetValueByKey(Saver writerKey)
        {
            TryAcquireTable();
            if (table == null)
                return Store.Const.CONSTS.WrongID;

            IBufferReader ValueBuf;
            using (IBufferWriter buffer = WriterPool.Get())
            {
                writerKey(buffer);
                table.Get(buffer, out ValueBuf);
            }

            if(ValueBuf == null)
                return Store.Const.CONSTS.WrongID;

            UInt64 res = ValueBuf.GetUInt64();
            ValueBuf.Dispose();

            return res;
        }


        public bool Put(Saver saverKey, UInt64 Id)
        {
            AcquireTable();

            using (IBufferWriter bufferKey = WriterPool.Get())
            using (IBufferWriter bufferValue = WriterPool.Get())
            {
                saverKey(bufferKey);
                bufferValue.Add(Id);

                table.Put(bufferKey, bufferValue);
            }

            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<KeyValuePair<IBufferReader, UInt64>> Enumerate()
        {
            TryAcquireTable();
            foreach (KeyValuePair<IBufferReader, IBufferReader> pair in table)
            {
                yield return new KeyValuePair<IBufferReader, UInt64>(pair.Key, pair.Value.GetUInt64());
                pair.Key.Dispose();
                pair.Value.Dispose();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Remove(Saver saverKey)
        {
            TryAcquireTable();
            using (IBufferWriter bufferKey = WriterPool.Get())
            {
                saverKey(bufferKey);

                if (table != null)
                    table.Remove(bufferKey);
            }
        }

        private void AcquireTable()
        {
            if (table == null)
                table = storage.GetTableIndexNoDuplicate(tableName);
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null)
                    table = storage.TryOpenTableIndexedNoDuplicates(tableName);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal void Clear()
        {
            storage.DeleteDB(tableName, ref table);
            table = null;
        }
    }
}
