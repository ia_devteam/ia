﻿using System.Collections.Generic;
using Store.Table;

namespace Store.BaseDatatypes
{
    internal class TableIndexedDuplicates_MainTable
    {
        /// <summary>
        /// Используемая таблица.
        /// </summary>
        TableIndexedDuplicates table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;

        /// <summary>
        /// Открывает таблицу
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName">Имя таблицы с индексом</param>
        public TableIndexedDuplicates_MainTable(Storage storage, string tableName)
        {
            this.storage = storage;
            this.tableName = tableName;
        }

        public delegate void Saver(IBufferWriter buffer);


        /// <summary>
        /// Добавляет элемент в таблицу.
        /// </summary>
        /// <param name="saveKey">Запись в буфер ключа сохраняемого элемента</param>
        /// <param name="saveValue">Запись в буфер значения сохраняемого элемента</param>
        public void Put(Saver saveKey, Saver saveValue)
        {
            AcquireTable();

            using (IBufferWriter key = WriterPool.Get())
            using (IBufferWriter value = WriterPool.Get())
            {
                saveValue(value);
                saveKey(key);

                table.Put(key, value);
            }
        }

        /// <summary>
        /// Удаляет из таблицы все значения. 
        /// </summary>
        public void Clear()
        {
            AcquireTable();

            storage.DeleteDB(tableName, table);
            table = null;
        }

        public IEnumerable<IBufferReader> FindValuesByKey(Saver saverValue)
        {
            AcquireTable();

            using (IBufferWriter writer = WriterPool.Get())
            {
                saverValue(writer);

                return table.Get(writer);
            }
        }

        private void AcquireTable()
        {
            if(table == null)
                table = storage.GetTableIndexDuplicate(tableName);
        }

        /// <summary>
        /// Перечислить всё содержимое таблицы
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<KeyValuePair<IBufferReader, IBufferReader>> Enumerate()
        {
            if(table != null)
                foreach(KeyValuePair<IBufferReader, IBufferReader> el in table)
                    yield return el;
        }
    }
}
