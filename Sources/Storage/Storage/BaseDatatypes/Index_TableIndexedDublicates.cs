﻿using System;
using System.Collections.Generic;
using Store.Table;


namespace Store.BaseDatatypes
{
    /// <summary>
    /// Маркер, используемый для поддержания состояния индекса Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.
    /// При реализации индекса можно создавать собственную реализацию этого интерфейса (для экономии памяти) 
    /// или получать маркер при помощи Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.GetMarker
    /// </summary>
    internal interface Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates
    {
        /// <summary>
        /// Проинформаировать индекс, что его значения уже сохранены в базе
        /// </summary>
        /// <param name="keySaver"></param>
        void InformLoaded(Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.Saver keySaver);

        IBufferWriter key
        {
            get;
            set;
        }

        bool isLoaded { get; }
    }

    /// <summary>
    /// Применять только метод InformLoaded, остальные используются в Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates
    /// </summary>
    internal class Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates_Impl : Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates
    {
        private IBufferWriter theKey = null;

        /// <summary>
        /// Проинформаировать индекс, что его значения уже сохранены в базе
        /// </summary>
        /// <param name="keySaver"></param>
        public void InformLoaded(Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.Saver keySaver)
        {
            theKey = WriterPool.Get();
            keySaver(theKey);
        }

        public IBufferWriter key
        {
            get { return theKey; }
            set { theKey = value; }
        }

        public bool isLoaded { get { return theKey != null; } }
    }


    /// <summary>
    /// Индекс с отложенным построением на основе TableIndexedDuplicates.
    /// Строится в момент первой попытки найти в нем элемент.
    /// После построения индекс начинает поддерживать свое состояние.
    /// </summary>
    internal class Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates
    {
        /// <summary>
        /// Используемая таблица.
        /// </summary>
        TableIndexedDuplicates table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;
        EnumerateRebuild enumerateRebuild;

        public struct EnumerateRebuildResult
        {
            public IBufferWriter key;
            public IBufferWriter value;
            public Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates marker;

            public EnumerateRebuildResult(IBufferWriter key, IBufferWriter value, Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates marker)
            {
                this.key = key;
                this.value = value;
                this.marker = marker;
            }
        }

        public delegate IEnumerable<EnumerateRebuildResult> EnumerateRebuild(IBufferWriter key, IBufferWriter value);

        /// <summary>
        /// Создает новый индекс
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName">Имя таблицы с индексом</param>
        /// <param name="enumerateRebuild">Делегат, используемый при построении индекса "с нуля".
        /// На вход принимает пару буферов и затем выдает (ключ, значение) с этими буферами. Буферы уже
        /// инициализированы и после каждой выдачи обнуляются.
        /// Должен перебрать все значения, ис которых состоит индекс.</param>
        public Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates(Storage storage, string tableName, EnumerateRebuild enumerateRebuild)
        {
            this.storage = storage;
            this.tableName = tableName;
            this.enumerateRebuild = enumerateRebuild;

            table = storage.TryOpenTableIndexedDuplicates(tableName);
        }


        /// <summary>
        /// Получить маркер
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates GetMarker()
        {
            return new Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates_Impl();
        }

        public delegate void Saver(IBufferWriter buffer);

        /// <summary>
        /// Добавляет элемент в индекс в случае, когда индекс уже построен.
        /// Элемент уже был сохранен ранее, он удаляется из индекса.
        /// Функция считает, что значения ранее сохраненного элемента индекса и нового элемента индекса совпадают!
        /// </summary>
        /// <param name="marker"></param>
        /// <param name="saveNewKey">Запись в буфер ключа сохраняемого (нового) элемента</param>
        /// <param name="saveValue">Запись в буфер значения сохраняемого (нового) элемента</param>
        public void UpdateIndex(Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates marker, Saver saveNewKey, Saver saveValue)
        {
            if (table != null)
                using (IBufferWriter value = WriterPool.Get())
                {
                    saveValue(value);

                    if (marker != null && marker.isLoaded)
                    {
                        table.Remove(marker.key, value);
                        marker.key.Dispose();
                        marker.key = null;
                    }

                    IBufferWriter key = WriterPool.Get();

                    saveNewKey(key);
                    table.Put(key, value);

                    if(marker != null)
                        marker.key = key;
                }
        }

        /// <summary>
        /// Удаляет элемент из таблицы.
        /// </summary>
        /// <param name="saveKey">Запись в буфер ключа удаляемого элемента</param>
        /// <param name="saveValue">Запись в буфер значения удаляемого элемента</param>
        public void Remove(Saver saveKey, Saver saveValue)
        {
            if(table != null) //Если индекса не существует, то и удалять нечего
                using (IBufferWriter key = WriterPool.Get())
                    using (IBufferWriter value = WriterPool.Get())
                    {
                        saveKey(key);
                        saveValue(value);

                        table.Remove(key, value);
                    }
        }

        /// <summary>
        /// Удаляет из индекса все значения и уничтожает индекс. 
        /// При последующем вызове SaveIfIndexExistsSameValue будет считаться, что буфер никогда не был создан. 
        /// </summary>
        public void Clear()
        {
            if (table != null)
            {
                storage.DeleteDB(tableName, table);
                table = null;
            }
        }

        /// <summary>
        /// Перечисляет все значения в буфере без повторения. То есть если некоторое значение записано по
        /// двум ключам, оно будет выдано только один раз.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public IEnumerable<KeyValuePair<IBufferReader, IBufferReader>> EnumerateWithoutDuplicates()
        {
            AcquireIndex();
            return table.EnumerateWithoutDuplicates();
        }


        public IEnumerable<IBufferReader> FindValuesByKey(Saver saverValue)
        {
            AcquireIndex();

            using (IBufferWriter writer = WriterPool.Get())
            {
                saverValue(writer);

                return table.Get(writer);
            }
        }

        /// <summary>
        /// Начинает перечислять элементы в порядке возрастания ключа начиная с элемента с минимальным ключем, 
        /// бинарная форма которого больше или равна заданному
        /// </summary>
        /// <param name="saverValue"></param>
        /// <returns></returns>
        public IEnumerable<IBufferReader> TakeValuesWithLargerKey(Saver saverValue)
        {
            AcquireIndex();

            using (IBufferWriter writer = WriterPool.Get())
            {
                saverValue(writer);

                return table.GetStartFrom(writer);
            }
        }

        /// <summary>
        /// В случае, если индекс не построен, строит индекс.
        /// Вызывается при первой попытке получить данные из буфера.
        /// </summary>
        private void AcquireIndex()
        {
            if (table == null) //Если индекс не был построен, то строим его
            {
                table = storage.GetTableIndexDuplicate(tableName);

                //Заполняем индекс
                IBufferWriter key = WriterPool.Get();
                IBufferWriter value = WriterPool.Get();
                foreach (EnumerateRebuildResult res in enumerateRebuild(key, value))
                {
                    table.Put(res.key, res.value);

                    if (res.marker != null)
                        res.marker.key = res.key.Clone();

                    key.Clear();
                    value.Clear();
                }
            }
        }
    }



    /// <summary>
    /// Маркер, используемый для поддержания состояния индекса Index_DelayCreation_KeyId_ValueId_Duplicates.
    /// При реализации индекса можно создавать собственную реализацию этого интерфейса (для экономии памяти) 
    /// или получать маркер при помощи Index_DelayCreation_KeyId_ValueId_Duplicates.GetMarker
    /// </summary>
    internal interface Marker_Index_DelayCreation_KeyId_ValueId_Duplicates
    {
        /// <summary>
        /// Проинформаировать индекс, что его значения уже сохранены в базе
        /// </summary>
        /// <param name="key"></param>
        void InformLoaded(UInt64 key);

        UInt64 key
        {
            get;
        }

        bool isLoaded { get; }
    }

    /// <summary>
    /// Применять только метод InformLoaded, остальные используются в Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates
    /// </summary>
    internal class Marker_Index_DelayCreation_KeyId_ValueId_Duplicates_Impl : Marker_Index_DelayCreation_KeyId_ValueId_Duplicates
    {
        private UInt64 theKey = Store.Const.CONSTS.WrongID;

        /// <summary>
        /// Проинформаировать индекс, что его значения уже сохранены в базе
        /// </summary>
        /// <param name="keyId"></param>
        public void InformLoaded(UInt64 keyId)
        {
            theKey = keyId;
        }

        public UInt64 key
        {
            get { return theKey; }
        }

        public bool isLoaded { get { return theKey != Store.Const.CONSTS.WrongID; } }
    }


    /// <summary>
    /// Индекс с отложенным построением на основе TableIndexedDuplicates.
    /// Строится в момент первой попытки найти в нем элемент.
    /// После построения индекс начинает поддерживать свое состояние.
    /// </summary>
    internal class Index_DelayCreation_KeyId_ValueId_Duplicates
    {
        /// <summary>
        /// Используемая таблица.
        /// </summary>
        TableIndexedDuplicates table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;
        EnumerateRebuild enumerateRebuild;

        public struct EnumerateRebuildResult
        {
            public UInt64 key;
            public UInt64 value;
            public Marker_Index_DelayCreation_KeyId_ValueId_Duplicates marker;

            public EnumerateRebuildResult(UInt64 key, UInt64 value, Marker_Index_DelayCreation_KeyId_ValueId_Duplicates marker)
            {
                this.key = key;
                this.value = value;
                this.marker = marker;
            }
        }

        public delegate IEnumerable<EnumerateRebuildResult> EnumerateRebuild();

        /// <summary>
        /// Создает новый индекс
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName">Имя таблицы с индексом</param>
        /// <param name="enumerateRebuild">Делегат, используемый при построении индекса "с нуля".
        /// На вход принимает пару буферов и затем выдает (ключ, значение) с этими буферами. Буферы уже
        /// инициализированы и после каждой выдачи обнуляются.
        /// Должен перебрать все значения, ис которых состоит индекс.</param>
        public Index_DelayCreation_KeyId_ValueId_Duplicates(Storage storage, string tableName, EnumerateRebuild enumerateRebuild)
        {
            this.storage = storage;
            this.tableName = tableName;
            this.enumerateRebuild = enumerateRebuild;

            table = storage.TryOpenTableIndexedDuplicates(tableName);
        }

        /// <summary>
        /// Возвращает true, если индекс уже построен. Если false, то заниматься обновлением индекса бесполезно.
        /// </summary>
        /// <returns></returns>
        public bool isIndexActivated()
        {
            return table != null;
        }

        /// <summary>
        /// Получить маркер
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public Marker_Index_DelayCreation_KeyId_ValueId_Duplicates GetMarker()
        {
            return new Marker_Index_DelayCreation_KeyId_ValueId_Duplicates_Impl();
        }

        /// <summary>
        /// Добавляет элемент в индекс в случае, когда индекс уже построен.
        /// Элемент уже был сохранен ранее, он удаляется из индекса.
        /// Функция считает, что значения ранее сохраненного элемента индекса и нового элемента индекса совпадают!
        /// </summary>
        /// <param name="marker"></param>
        /// <param name="newKey"></param>
        /// <param name="newValue"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void UpdateIndex(Marker_Index_DelayCreation_KeyId_ValueId_Duplicates marker, UInt64 newKey, UInt64 newValue)
        {
            if (table != null)
                using (IBufferWriter value = WriterPool.Get())
                {
                    value.Add(newValue);

                    if (marker != null && marker.isLoaded)
                    {
                        using (IBufferWriter oldKey = WriterPool.Get())
                        {
                            oldKey.Add(marker.key);
                            table.Remove(oldKey, value);
                        }
                    }

                    IBufferWriter key = WriterPool.Get();

                    key.Add(newKey);
                    table.Put(key, value);

                    if(marker != null)
                        marker.InformLoaded(newKey);
                }
        }

        /// <summary>
        /// Удаляет элемент из таблицы.
        /// </summary>
        /// <param name="keyId"></param>
        /// <param name="valueId"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Remove(UInt64 keyId, UInt64 valueId)
        {
            if (table != null) //Если индекса не существует, то и удалять нечего
                using (IBufferWriter key = WriterPool.Get())
                using (IBufferWriter value = WriterPool.Get())
                {
                    key.Add(keyId);
                    value.Add(valueId);

                    table.Remove(key, value);
                }
        }

        /// <summary>
        /// Удаляет из индекса все значения и уничтожает индекс. 
        /// При последующем вызове SaveIfIndexExistsSameValue будет считаться, что буфер никогда не был создан. 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Clear()
        {
            if (table != null)
            {
                storage.DeleteDB(tableName, table);
                table = null;
            }
        }

        /// <summary>
        /// Перечисляет все значения в буфере без повторения. То есть если некоторое значение записано по
        /// двум ключам, оно будет выдано только один раз.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public IEnumerable<KeyValuePair<UInt64, UInt64>> EnumerateWithoutDuplicates()
        {
            AcquireIndex();
            foreach (KeyValuePair<IBufferReader, IBufferReader> pair in table.EnumerateWithoutDuplicates())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Key.GetUInt64(), pair.Value.GetUInt64());
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<UInt64> FindValuesByKey(UInt64 valueId)
        {
            AcquireIndex();

            using (IBufferWriter writer = WriterPool.Get())
            {
                writer.Add(valueId);

                foreach (IBufferReader reader in table.Get(writer))
                    yield return reader.GetUInt64();
            }
        }

        /// <summary>
        /// В случае, если индекс не построен, строит индекс.
        /// Вызывается при первой попытке получить данные из буфера.
        /// </summary>
        private void AcquireIndex()
        {
            if (table == null) //Если индекс не был построен, то строим его
            {
                table = storage.GetTableIndexDuplicate(tableName);

                //Заполняем индекс
                using (IBufferWriter key = WriterPool.Get())
                using (IBufferWriter value = WriterPool.Get())
                    foreach (EnumerateRebuildResult res in enumerateRebuild())
                    {
                        key.Add(res.key);
                        value.Add(res.value);
                        table.Put(key, value);

                        if (res.marker != null)
                            res.marker.InformLoaded(res.key);

                        key.Clear();
                        value.Clear();
                    }
            }
        }
    }


    /// <summary>
    /// Индекс с отложенным построением на основе TableIndexedDuplicates.
    /// Строится в момент первой попытки найти в нем элемент.
    /// После построения индекс начинает поддерживать свое состояние.
    /// </summary>
    internal class Index_DelayCreation_KeyBuffer_ValueId_Duplicates
    {
        /// <summary>
        /// Используемая таблица.
        /// </summary>
        TableIndexedDuplicates table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;
        EnumerateRebuild enumerateRebuild;

        public struct EnumerateRebuildResult
        {
            public IBufferWriter key;
            public UInt64 value;

            public EnumerateRebuildResult(IBufferWriter key, UInt64 value)
            {
                this.key = key;
                this.value = value;
            }
        }

        public delegate IEnumerable<EnumerateRebuildResult> EnumerateRebuild(IBufferWriter key);

        /// <summary>
        /// Создает новый индекс
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName">Имя таблицы с индексом</param>
        /// <param name="enumerateRebuild">Делегат, используемый при построении индекса "с нуля".
        /// На вход принимает пару буферов и затем выдает (ключ, значение) с этими буферами. Буферы уже
        /// инициализированы и после каждой выдачи обнуляются.
        /// Должен перебрать все значения, ис которых состоит индекс.</param>
        public Index_DelayCreation_KeyBuffer_ValueId_Duplicates(Storage storage, string tableName, EnumerateRebuild enumerateRebuild)
        {
            this.storage = storage;
            this.tableName = tableName;
            this.enumerateRebuild = enumerateRebuild;

            table = storage.TryOpenTableIndexedDuplicates(tableName);
        }

        /// <summary>
        /// Возвращает true, если индекс уже построен. Если false, то заниматься обновлением индекса бесполезно.
        /// </summary>
        /// <returns></returns>
        public bool isIndexActivated()
        {
            return table != null;
        }

        public delegate void Saver(IBufferWriter writer);

        /// <summary>
        /// Добавляет элемент в индекс в случае, когда индекс уже построен.
        /// Элемент уже был сохранен ранее, он удаляется из индекса.
        /// Функция считает, что значения ранее сохраненного элемента индекса и нового элемента индекса совпадают!
        /// </summary>
        /// <param name="saverKey"></param>
        /// <param name="newValue"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void UpdateIndex(Saver saverKey, UInt64 newValue)
        {
            if (table != null)
                using (IBufferWriter value = WriterPool.Get())
                using (IBufferWriter key = WriterPool.Get())
                {
                    value.Add(newValue);
                    saverKey(key);

                    table.Put(key, value);
                }
        }

        /// <summary>
        /// Удаляет элемент из таблицы.
        /// </summary>
        /// <param name="keySaver"></param>
        /// <param name="valueId"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Remove(Saver keySaver, UInt64 valueId)
        {
            if (table != null) //Если индекса не существует, то и удалять нечего
                using (IBufferWriter key = WriterPool.Get())
                using (IBufferWriter value = WriterPool.Get())
                {
                    keySaver(key);
                    value.Add(valueId);

                    table.Remove(key, value);
                }
        }

        /// <summary>
        /// Удаляет из индекса все значения и уничтожает индекс. 
        /// При последующем вызове SaveIfIndexExistsSameValue будет считаться, что буфер никогда не был создан. 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Clear()
        {
            if (table != null)
            {
                storage.DeleteDB(tableName, table);
                table = null;
            }
        }

        /// <summary>
        /// Перечисляет все значения в буфере без повторения. То есть если некоторое значение записано по
        /// двум ключам, оно будет выдано только один раз.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public IEnumerable<KeyValuePair<UInt64, UInt64>> EnumerateWithoutDuplicates()
        {
            AcquireIndex();
            foreach (KeyValuePair<IBufferReader, IBufferReader> pair in table.EnumerateWithoutDuplicates())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Key.GetUInt64(), pair.Value.GetUInt64());
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<UInt64> FindValuesByKey(Saver keySaver)
        {
            AcquireIndex();

            using (IBufferWriter writer = WriterPool.Get())
            {
                keySaver(writer);

                foreach (IBufferReader reader in table.Get(writer))
                {
                    yield return reader.GetUInt64();
                    reader.Dispose();
                }
            }
        }

        /// <summary>
        /// В случае, если индекс не построен, строит индекс.
        /// Вызывается при первой попытке получить данные из буфера.
        /// </summary>
        private void AcquireIndex()
        {
            if (table == null) //Если индекс не был построен, то строим его
            {
                table = storage.GetTableIndexDuplicate(tableName);

                //Заполняем индекс
                using (IBufferWriter key = WriterPool.Get())
                using (IBufferWriter value = WriterPool.Get())
                    foreach (EnumerateRebuildResult res in enumerateRebuild(key))
                    {
                        value.Add(res.value);
                        table.Put(key, value);

                        key.Clear();
                        value.Clear();
                    }
            }
        }
    }
}
