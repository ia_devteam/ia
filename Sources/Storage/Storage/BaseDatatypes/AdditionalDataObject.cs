﻿using System;
using Store.Table;

namespace Store.BaseDatatypes
{
    abstract class AdditionalDataObject
    {
        /// <summary>
        /// Возвращает сторку, используемую для выдачи сообщения об ошибке в случае попытки загрузить элемент,
        /// отсутствующий в базе.
        /// </summary>
        /// <returns></returns>
        protected abstract string GetLoadErrorString();        
        /// <summary>
        /// Загрузить свое состояние из буфера
        /// </summary>
        /// <param name="reader"></param>
        protected abstract void Load(IBufferReader reader);
        /// <summary>
        /// Сохранить свое состояние в буффер
        /// </summary>
        protected abstract void SaveToBuffer(IBufferWriter buffer);
        /// <summary>
        /// Обновить все индексы для этого элемента.
        /// </summary>
        protected abstract void SaveIndexes();

        protected Table_KeyId_ValueSaver_Nodublicate theTable;
        protected UInt64 theId;

        internal AdditionalDataObject(Table_KeyId_ValueSaver_Nodublicate table, UInt64 Id)
        {
            theTable = table;
            theId = Id;
        }

        /// <summary>
        /// Сохранить свое состояние в базу
        /// </summary>
        internal virtual void Save()
        {
            //Сохраняем сам объект
            theTable.Put(theId, SaveToBuffer);

            //Обновляем все индексы
            SaveIndexes();
        }


        /// <summary>
        /// Загружает объект, если он присутствует в БД. Иначе IA.Monitor.Log.TerminationError
        /// </summary>
        internal void Load()
        {
            IBufferReader reader = theTable.GetValueByID(theId);
            if (reader != null)
            {
                Load(reader);
                reader.Dispose();
            }
        }
    }
}
