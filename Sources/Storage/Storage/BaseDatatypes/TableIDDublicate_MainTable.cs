﻿using System;
using System.Collections.Generic;
using Store.Table;

namespace Store.BaseDatatypes
{
    internal class TableIDDuplicate_MainTable<T>
        where T:DataObject_IDDuplicate
    {
        TableIDDuplicate table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;
        CreateNew createNew;

        /// <summary>
        /// Выполнить создание нового элемента и произвести его загрузку из БД
        /// </summary>
        /// <returns></returns>
        public delegate T CreateNew();

        /// <summary>
        /// Открывает таблицу
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName">Имя таблицы с индексом</param>
        /// <param name="createNew"></param>
        public TableIDDuplicate_MainTable(Storage storage, string tableName, CreateNew createNew)
        {
            this.storage = storage;
            this.tableName = tableName;
            this.createNew = createNew;
        }

        private void AcquireTable()
        {
            if(table == null)
                table = storage.GetTableIDDuplicate(tableName);
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null)
                    table = storage.TryOpenTableIDDuplicates(tableName);
            }
        }

        public T CreateObject()
        {
            AcquireTable();
            T t = createNew();
            t.Initialize(table);
            return t;
        }

        /// <summary>
        /// Возвращает все значения по указанному ключу.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public IEnumerable<T> GetValueByID(UInt64 ID)
        {
            TryAcquireTable();
            if (table == null)
                yield break;

            foreach(IBufferReader reader in table.Get(ID))
            {
                T t = createNew();
                t.Initialize(table, ID, reader);
                yield return t;
            }
        }

        /// <summary>
        /// Положить (ключ,значение) в базу. Если такие (ключ,значение) уже имеются в базе, то не класть.
        /// </summary>
        /// Возвращает true, если добавление потребовалось. Иначе false.
        public bool PutWithoutRepetition(T t)
        {
            AcquireTable();
            return t.SaveWithoutRepetition();
        }

        /// <summary>
        /// Положить (ключ,значение) в базу. Если такие (ключ,значение) уже имеются в базе, то положить еще один.
        /// </summary>
        public void PutWithRepetition(T t)
        {
            AcquireTable();
            t.SaveWithRepetition();
        }

        public IEnumerable<KeyValuePair<UInt64, T>> EnumerateEveryone()
        {
            TryAcquireTable();
            if (table != null)
            {
                foreach (KeyValuePair<UInt64, IBufferReader> pair in table)
                {
                    T t = createNew();
                    t.Initialize(table, pair.Key, pair.Value);

                    yield return new KeyValuePair<UInt64, T>(pair.Key, t);
                }
            }
        }

        internal void DeleteById(ulong id)
        {
            TryAcquireTable();
            if(table!= null)
                table.Remove(id);
        }
    }
}
