﻿using System;
using System.Collections.Generic;
using Store.Table;

namespace Store.BaseDatatypes
{
    class Table_KeyId_ValueSaver_Nodublicate
    {
        TableIDNoDuplicate table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;

        //[System.Diagnostics.CodeAnalysis.SuppressMessage ("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        //public delegate IEnumerable<KeyValuePair<UInt64, IBufferWriter>> EnumerateRebuild(IBufferWriter value);

        public Table_KeyId_ValueSaver_Nodublicate(Storage storage, string tableName)
        {
            this.storage = storage;
            this.tableName = tableName;
        }

        /// <summary>
        /// Возвращает элемент, хранимый в индексе по указанному ключу.
        /// Возвращает null, если по указанному ключу в индексе ничего не хранится.
        /// </summary>
        /// <param name="theID"></param>
        /// <returns></returns>
        public IBufferReader GetValueByID(ulong theID)
        {
            TryAcquireTable();
            if (table != null)
            {
                IBufferReader buf;
                table.Get(theID, out buf);
                return buf;
            }
            else
                return null;
        }

        public delegate void Saver(IBufferWriter writer);

        public bool Put(UInt64 ID, Saver saverValue)
        {
            AcquireTable();

            using (IBufferWriter buffer = WriterPool.Get())
            {
                saverValue(buffer);

                table.Put(ID, buffer);
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<KeyValuePair<UInt64, IBufferReader>> Enumerate()
        {
            TryAcquireTable();
            if (table != null)
                return table;
            else
                return NullEnum();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        static private IEnumerable<KeyValuePair<ulong, IBufferReader>> NullEnum()
        {
            yield break;
        }

        public void Remove(UInt64 Id)
        {
            TryAcquireTable();
            if(table != null)        
                table.Remove(Id);
        }

        private void AcquireTable()
        {
            if (table == null)
                table = storage.TryOpenTableIDNoDuplicate(tableName);

            if (table == null)
                table = storage.GetTableIDNoDuplicate(tableName);
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null)
                    table = storage.TryOpenTableIDNoDuplicate(tableName);
            }
        }

        internal void Clear()
        {
            if (table != null)
            {
                storage.DeleteDB(tableName, table);
                table = null;
            }
        }    


        /// <summary>
        /// Возвращает элемент, являющийся UInt64 и хранимый в индексе по указанному ключу.
        /// Возвращает Store.Const.CONSTS.WrongID, если по указанному ключу в таблице ничего не хранится.
        /// </summary>
        /// <param name="theID"></param>
        /// <returns></returns>
        public UInt64 GetUInt64ValueByID(ulong theID)
        {
            IBufferReader reader = GetValueByID(theID);

            UInt64 res;
            if (reader != null)
            {
                res = reader.GetUInt64();
                reader.Dispose();
            }
            else
                res = Store.Const.CONSTS.WrongID;

            return res;
        }

        public bool PutUInt64Value(UInt64 ID, UInt64 value)
        {
            AcquireTable();

            using (IBufferWriter buffer = WriterPool.Get())
            {
                buffer.Add(value);
                table.Put(ID, buffer);
            }

            return false;
        }
    }



    class Table_KeyID_ValueSaver_Dublicate
    {
        TableIDDuplicate table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;

        //[System.Diagnostics.CodeAnalysis.SuppressMessage ("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        //public delegate IEnumerable<KeyValuePair<UInt64, IBufferWriter>> EnumerateRebuild(IBufferWriter value);

        public Table_KeyID_ValueSaver_Dublicate(Storage storage, string tableName)
        {
            this.storage = storage;
            this.tableName = tableName;
        }

        /// <summary>
        /// Возвращает элемент, хранимый в индексе по указанному ключу.
        /// Возвращает null, если по указанному ключу в индексе ничего не хранится.
        /// </summary>
        /// <param name="theID"></param>
        /// <returns></returns>
        public IEnumerable<IBufferReader> GetValuesByID(ulong theID)
        {
            TryAcquireTable();
            if (table != null)
                foreach (IBufferReader buf in table.Get(theID))
                {
                    yield return buf;
                    buf.Dispose();
                }
            else
                yield break;
        }

        public delegate void Saver(IBufferWriter writer);

        public bool AddAtId(UInt64 ID, Saver saverValue)
        {
            AcquireTable();

            using (IBufferWriter buffer = WriterPool.Get())
            {
                saverValue(buffer);

                table.Put(ID, buffer);
            }
            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<KeyValuePair<UInt64, IBufferReader>> Enumerate()
        {
            TryAcquireTable();
            if (table != null)
                return table;
            else
                return NullEnum();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        static private IEnumerable<KeyValuePair<ulong, IBufferReader>> NullEnum()
        {
            yield break;
        }

        public void Remove(UInt64 Id, Saver saverValue)
        {
            TryAcquireTable();
            if (table != null)
                using (IBufferWriter buffer = WriterPool.Get())
                {
                    saverValue(buffer);
                    table.Remove(Id);
                }
        }

        internal void RemoveAllForKey(UInt64 Id)
        {
            if(table != null)
                table.Remove(Id);
        }


        private void AcquireTable()
        {
            if (table == null)
                table = storage.TryOpenTableIDDuplicates(tableName);

            if (table == null)
                table = storage.GetTableIDDuplicate(tableName);
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null)
                    table = storage.TryOpenTableIDDuplicates(tableName);
            }
        }

        internal void Clear()
        {
            if (table != null)
            {
                storage.DeleteDB(tableName, ref table);
                table = null;
            }
        }
    }
}
