﻿using System;
using System.Collections.Generic;
using Store.Table;

namespace Store.BaseDatatypes
{
    /// <summary>
    /// Таблица, содержащая головные объекты разделов. Такие объекты состоят только из идентификаторов, но не содержат данных.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class TableLeader<T>
        where T : DataObjectLeader
    {
        //Собственные данные

        TableIDNoDuplicate table;
        WeakRefTable<T> memoried = new WeakRefTable<T>();

        //Запомненные данные, переданные при инициализации объекта
        Storage storage;
        string tableName;
        CreateNew createNew;

        /// <summary>
        /// Выполнить создание нового элемента и произвести его загрузку из БД
        /// </summary>
        /// <param name="kind"></param>
        /// <returns></returns>
        public delegate T CreateNew(Int32 kind);

        public TableLeader(Storage storage, string tableName, CreateNew createNew)
        {
            this.storage = storage;
            this.tableName = tableName;
            this.createNew = createNew;

            //Пытаемся загрузить таблицу. Если таблица еще не создана, то она не будет загружена.
            table = storage.TryOpenTableIDNoDuplicate(tableName);

        }

        /// <summary>
        /// Возращает объект с указанным идентификатором. Если объект был загружен в память, 
        /// то берется уже загруженный экземпляр.
        /// Если в базе объекта с таким идентификатором не было, выдается ошибка.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public T Get(UInt64 ID)
        {
            if (ID == Store.Const.CONSTS.WrongID)
                return null;

            T res = memoried.Get(ID);
            if (res == null)
            {
                AcquireTable();

                //Гарантируем наличие в базе объекта с таким идентификатором
                IBufferReader buffer;
                if (table.Get(ID, out buffer) != enResult.NOT_FOUND)
                {
                    res = createNew(buffer.GetInt32());
                    res.SetId(ID);

                    buffer.Dispose();

                    memoried.Add(ID, res);
                }
            }

            return res;
        }

        /// <summary>
        /// Добавить новый объект в таблицу
        /// </summary>
        /// <returns></returns>
        public T Add()
        {
            return Add(0);
        }

        /// <summary>
        /// Добавить новый объект в таблицу
        /// </summary>
        /// <returns></returns>
        public T Add(Int32 kind)
        {
            AcquireTable();
            T res = createNew(kind);

            using (IBufferWriter buffer = WriterPool.Get())
            {
                buffer.Add(kind);
                res.SetId(table.Add(buffer));
            }

            memoried.Add(res.Id, res);
            return res;
        }


        /// <summary>
        /// Удаляет элемент из таблицы, если он в ней существовал.
        /// </summary>
        /// <param name="ID"></param>
        public void Remove(UInt64 ID)
        {
            if (table != null)
            {
                table.Remove(ID);
                memoried.Remove(ID);
            }
        }

        /// <summary>
        /// Очищает таблицу.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods", MessageId = "System.GC.Collect")]
        public void Clear()
        {
            if (table != null)
            {
                memoried.Clear();
                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();
                storage.DeleteDB(tableName, table);
                table = null;
            }
        }

        /// <summary>
        /// Перечисляем все имеющиеся записи, вне зависимости от того, загружены ли они в память или 
        /// сохранены на диске.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> EnumerateEveryone()
        {
            if (table != null) //Если таблицы нет, то и выдавать нечего
                foreach (KeyValuePair<UInt64, IBufferReader> pair in table)
                    yield return Get(pair.Key);
        }

        /// <summary>
        /// Возвращает количество объектов в таблице
        /// </summary>
        /// <returns></returns>
        public ulong Count()
        {
            if (table == null)
                return 0;
            else
                return table.Count();
        }



        /// <summary>
        /// Загрузить таблицу, если она еще не загружена. Если таблица не существовала, она будет создана.
        /// </summary>
        private void AcquireTable()
        {
            if (table == null)
                table = storage.GetTableIDNoDuplicate(tableName);
        }

        /// <summary>
        /// Возвращает массив все в настоящее время находящих в памяти объектов из таблицы
        /// </summary>
        /// <returns></returns>
        private T[] Alives()
        {
            return memoried.Alives();
        }
    }




    /// <summary>
    /// Представляет таблицу TableIDNoDuplicate, сгрупированную с объектом WeakRefTable, выполняющем менеджмент
    /// загруженных в память объектов из таблицы.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class TableIDNoDuplicate_with_WeakRefTable<T>
        where T : DataObject_IDNoDuplicate
    {
        //Собственные данные

        TableIDNoDuplicate table;
        WeakRefTable<T> memoried = new WeakRefTable<T>();

        //Запомненные данные, переданные при инициализации объекта
        Storage storage;
        string tableName;
        CreateNew createNew;

        /// <summary>
        /// Выполнить создание нового элемента и произвести его загрузку из БД
        /// </summary>
        /// <returns></returns>
        public delegate T CreateNew();

        public TableIDNoDuplicate_with_WeakRefTable(Storage storage, string tableName, CreateNew createNew)
        {
            this.storage = storage;
            this.tableName = tableName;
            this.createNew = createNew;

            //Добавляем в очередь закрытия Хранилища сохранение всех загруженных в память объектов.
            //Должно выполняться до закрытия самой таблицы!
            storage.CloseEvent += new CloseHandle(Close);

            //Пытаемся загрузить таблицу. Если таблица еще не создана, то она не будет загружена.
            table = storage.TryOpenTableIDNoDuplicate(tableName);

        }

        public T Add()
        {
            AcquireTable();
            T res = createNew();
            res.Generate(table);
            memoried.Add(res.ID, res);
            return res;
        }

        /// <summary>
        /// Возращает объект из таблицы с указанным идентификатором. Если объект был загружен в память, 
        /// то берется уже загруженный экземпляр, иначе объект загружается из таблицы.
        /// Если такого объекта не существовало, он будет создан.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public T LoadOrGenerate(UInt64 ID)
        {
            T res = memoried.Get(ID);
            if (res == null)
            {
                AcquireTable();
                res = createNew();
                res.LoadOrGenerate(table, ID);
                memoried.Add(ID, res);
            }

            return res;
        }


        /// <summary>
        /// Возращает объект из таблицы с указанным идентификатором. Если объект был загружен в память, 
        /// то берется уже загруженный экземпляр, иначе объект загружается из таблицы.
        /// Если такого объекта не существовало, выдается исключение.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public T Load(UInt64 ID)
        {
            T res = memoried.Get(ID);
            if (res == null)
            {
                AcquireTable();
                res = createNew();
                res.Load(table, ID);
                memoried.Add(ID, res);
            }

            return res;
        }

        /// <summary>
        /// Возращает объект из таблицы с указанным идентификатором. Если объект был загружен в память, 
        /// то берется уже загруженный экземпляр, иначе объект загружается из таблицы.
        /// Если такого объекта не существовало, выдается исключение.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public T GetExistingWithoutLoad(UInt64 ID)
        {
            T res = memoried.Get(ID);
            if (res == null)
            {
                AcquireTable();
                res = createNew();
                res.Initialize(table, ID, null);
                memoried.Add(ID, res);
            }

            return res;
        }


        /// <summary>
        /// Удаляет элемент из таблицы, если он в ней существовал.
        /// </summary>
        /// <param name="ID"></param>
        public void Remove(UInt64 ID)
        {
            if (table != null)
            {
                table.Remove(ID);
                memoried.Remove(ID);
            }
        }

        /// <summary>
        /// Очищает таблицу.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2001:AvoidCallingProblematicMethods", MessageId = "System.GC.Collect")]
        public void Clear()
        {
            if (table != null)
            {
                memoried.Clear();
                GC.Collect(GC.MaxGeneration);
                GC.WaitForPendingFinalizers();
                storage.DeleteDB(tableName, table);
                table = null;
            }
        }

        /// <summary>
        /// Перечисляем все имеющиеся записи, вне зависимости от того, загружены ли они в память или 
        /// сохранены на диске.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> EnumerateEveryone()
        {
            if (table != null) //Если таблицы нет, то и выдавать нечего
            {
                List<UInt64> memoriedIDs = null;

                if (isLazyObjects()) //В памяти могут находиться объекты, отсутствующие в базе данных. Их необходимо не забыть.
                {
                    //Перебираем загруженные в память и запоминаем их Id.

                    memoriedIDs = new List<UInt64>();

                    foreach (T obj in memoried.Alives())
                    {
                        memoriedIDs.Add(obj.ID);
                        yield return obj;
                    }

                    memoriedIDs.Sort();
                }

                //Теперь выдаем все остальные объекты. Если lazy == false, то здесь выдаются все объекты
                foreach (KeyValuePair<UInt64, IBufferReader> pair in table)
                {
                    if (memoriedIDs == null || !memoriedIDs.Contains(pair.Key)) //Если lazy==true,  
                    //то memoriedIDs != null
                    //и не надо дублировать уже выданнные
                    //объекты.
                    {
                        T obj = createNew();
                        obj.Initialize(table, pair.Key, pair.Value);

                        memoried.Add(pair.Key, obj);

                        yield return obj;
                    }
                }
            }
            
        }

        /// <summary>
        /// Возвращает количество объектов в таблице
        /// </summary>
        /// <returns></returns>
        public ulong Count()
        {
            if (table == null)
                return 0;
            else
                return table.Count();
        }



        /// <summary>
        /// Загрузить таблицу, если она еще не загружена. Если таблица не существовала, она будет создана.
        /// </summary>
        private void AcquireTable()
        {
            if (table == null)
                table = storage.GetTableIDNoDuplicate(tableName);
        }

        /// <summary>
        /// Возвращает массив все в настоящее время находящих в памяти объектов из таблицы
        /// </summary>
        /// <returns></returns>
        private T[] Alives()
        {
            return memoried.Alives();
        }
        
        /// <summary>
        /// Выгружает в таблицу все повисшие в памяти объекты.
        /// </summary>
        private void Close()
        {
            if (table != null)
            {
                if (isLazyObjects())    //Если lazy == true, то надо сохранить состояния всех объектов.
                    //Если lazy == false, ничего сохранять не надо
                    foreach (T impl in Alives())
                        ((IDisposable)impl).Dispose();
            }
        }

        /// <summary>
        /// Определяет, используется ли для элементов, с которыми работает таблица, ленивый метод сохранения.
        /// true - Объекты используют ленивый метод сохранения. То есть сохранение в базу
        /// выполняется только тогда, когда объект выгружается из памяти.
        /// 
        /// false - Объекты сохраняются в базу сразу же после изменения их состояния.
        /// </summary>
        /// <returns></returns>
        static private bool isLazyObjects()
        {
            Type type = typeof(T).BaseType;
            while (type.FullName != "Store.BaseDatatypes.DataObject_IDNoDuplicate_Lazy"
                                    && type.FullName != "Store.BaseDatatypes.DataObject_IDNoDuplicate_Immediate")
                type = type.BaseType;

            if (type.FullName == "Store.BaseDatatypes.DataObject_IDNoDuplicate_Lazy")
                return true;

            if (type.FullName == "Store.BaseDatatypes.DataObject_IDNoDuplicate_Immediate")
                return false;

            throw new Store.Const.StorageException("Сюда управление никогда не приходит.");
        }

    }

}
