﻿using System;
using System.Collections.Generic;
using Store.Table;

namespace Store.BaseDatatypes
{
    /// <summary>
    /// Таблица хранения имен объектов
    /// </summary>
    internal class TableObjectNaming
    {
        internal Table_KeyId_ValueSaver_Nodublicate table;

        internal TableObjectNaming(Storage storage, string tableName)
        {
            table = new Table_KeyId_ValueSaver_Nodublicate(storage, tableName);
        }

        internal string Accure(UInt64 Id)
        {
            string theName;
            IBufferReader reader = table.GetValueByID(Id);

            if (reader != null)
                theName = reader.GetString();
            else
                theName = null;

            return theName;
        }

        internal void Set(UInt64 Id, string name)
        {
            table.Put(Id, buf => buf.Add(name));
        }

        internal void Clear()
        {
            table.Clear();
        }
    }

    /// <summary>
    /// Хранение таблица имен объектов с возможностью поиска объекта по его имени.
    /// </summary>
    internal class TableObjectNamingWithIndex
    {
        /// <summary>
        /// Перечень всех имен файлов
        /// ключ - строковое имя файла
        /// значение - UInt64 идентификатора файла
        /// </summary>
        internal Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates tableIndex;

        internal TableObjectNaming table;


        internal TableObjectNamingWithIndex(Storage storage, string tableName, string indexName, Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuild EnumerateIndex) 
        {
            tableIndex = new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates(storage, indexName, EnumerateIndex);
            table = new TableObjectNaming(storage, tableName);
        }

        /// <summary>
        /// Ищет идентификатор объекта по его имени
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal IEnumerable<UInt64> Find(string name)
        {
            foreach (IBufferReader reader in tableIndex.FindValuesByKey(buffer => buffer.Add(name)))
                yield return reader.GetUInt64();
        }

        /// <summary>
        /// Удаляет имя объекта из Хранилища
        /// </summary>
        /// <param name="Id"></param>
        internal void Remove(UInt64 Id)
        {
            Naming_Impl naming = new Naming_Impl(table.Accure(Id), this);
            if (naming.CurrentName != null)
            {
                tableIndex.Remove(buffer => buffer.Add(naming.CurrentName), buffer => buffer.Add(Id));
                table.table.Remove(Id);
            }
            naming.Erase();
        }

        internal void Clear()
        {
            tableIndex.Clear();
            table.Clear();
        }

        /// <summary>
        /// Получить имя, хранимое по указанному Id в данной таблице
        /// </summary>
        /// <param name="naming">Маркер имени</param>
        /// <param name="theID"></param>
        /// <returns></returns>
        internal string GetName(ref Naming naming, ulong theID)
        {
            if (theID == 0)
                return null;

            if (naming == null)
                naming = new Naming_Impl(table.Accure(theID), this);

            return (naming as Naming_Impl).CurrentName;
        }

        /// <summary>
        /// Установить имя, хранимое по указанному Id в данной таблице.
        /// Запрещено изменять однажды установленное имя
        /// </summary>
        /// <param name="naming">Маркер имени</param>
        /// <param name="theID"></param>
        /// <param name="value"></param>
        internal void SetName(ref Naming naming, ulong theID, string value)
        {
            if (naming == null)
                naming = new Naming_Impl(null, this);

            (naming as Naming_Impl).Set(value, theID);
        }

        internal void SetNameWithCanonize(ref Naming naming, ulong theID, string value)
        {
            if (naming == null)
                naming = new Naming_Impl(null, this);

            (naming as Naming_Impl).SetWithCanonize(value, theID);
        }
    }

    /// <summary>
    /// Расширение TableObjectNamingWithIndex, автоматизирующее стандартные действия.
    /// </summary>
    internal class TableObjectNamingWithIndexAuto<T> : TableObjectNamingWithIndex
    {
        internal TableObjectNamingWithIndexAuto(Storage storage, string tableName, string indexName, Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuild EnumerateIndex) : base(storage, tableName, indexName, EnumerateIndex)
        {
        }

        /// <summary>
        /// Создание сущности по ее идентификатору
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        internal delegate T EntityById(UInt64 objectId);

        /// <summary>
        /// Получить все сущности, записанные в Хранилище с указанным именем.
        /// </summary>
        /// <param name="name">Идентификатор сущности</param>
        /// <param name="creator"></param>
        /// <returns></returns>
        public T[] GetEntity(string name, EntityById creator)
        {
            List<T> list = new List<T>();

            foreach (UInt64 ID in Find(name))
                list.Add(creator(ID));

            return list.ToArray();
        }

        /// <summary>
        /// Получить все сущности, записанные в Хранилище с указанным именем.
        /// </summary>
        /// <param name="name">Полное имя функции</param>
        /// <param name="creator"></param>
        /// <returns></returns>
        public T[] GetEntity(string[] name, EntityById creator)
        {
            return GetEntity_FindEntityBySubname(name, creator, true);
        }

        /// <summary>
        /// Получить все сущности, записанные в Хранилище с указанным частичным именем. Например, если name = C1.C2, то будет возвращено в том числе C0.C1.C2.
        /// </summary>
        /// <param name="name">Полное имя функции</param>
        /// <param name="creator"></param>
        /// <returns></returns>
        public T[] FindEntityBySubname(string[] name, EntityById creator)
        {
            return GetEntity_FindEntityBySubname(name, creator, false);
        }

        /// <summary>
        ///Реализация двух функций <see cref="Store.BaseDatatypes.TableObjectNamingWithIndexAuto{T}.GetEntity(string[], Store.BaseDatatypes.TableObjectNamingWithIndexAuto{T}.EntityById)"/> и <see cref="Store.BaseDatatypes.TableObjectNamingWithIndexAuto{T}.FindEntityBySubname(string[], Store.BaseDatatypes.TableObjectNamingWithIndexAuto{T}.EntityById)"/> одновременно.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="creator"></param>
        /// <param name="isGetEntity"></param>
        /// <returns></returns>
        private T[] GetEntity_FindEntityBySubname(string[] name, EntityById creator, bool isGetEntity)
        {
            List<T> list = new List<T>();

            foreach (UInt64 ID in Find(name[name.Length - 1]))
            {
                T func = creator(ID);

                //Если полное имя функции совпадает с указанным
                DataObjectLeaderWithName func_d = func as DataObjectLeaderWithName;

                if (func_d == null)
                    throw new Store.Const.StorageException("GetEntity_FindEntityBySubname func is not DataObjectLeaderWithName");

                bool check;
                if(isGetEntity)
                    //Реализуем GetEntity
                    check = func_d.isFullName(name);
                else
                    //Реализуем FindEntityBySubname
                    check = func_d.isSubName(name);
                //Собственно действие
                if (check)
                    list.Add(creator(ID));
            }

            return list.ToArray();
        }
    }

    /// <summary>
    /// Маркер имени объекта. Интерфейс для хранения маркера в других классах.
    /// Интерфейс реализует Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates для использования в индексах по именам файлов.
    /// </summary>
    internal interface Naming
    {
    }

    /// <summary>
    /// Реалиация маркера. Этот класс должен использовать только TableObjectNamingWithIndex
    /// </summary>
    internal class Naming_Impl : Naming
    {
        string currentName = null;
        TableObjectNamingWithIndex table;


        internal Naming_Impl(string loadName, TableObjectNamingWithIndex table)
        {
            if (loadName != null)
            {
                currentName = loadName;
            }

            this.table = table;
        }

        internal string CurrentName
        {
            get 
            {
                return currentName == null ? "" : currentName; 
            }
        }

        /// <summary>
        /// Устанавливаем имя объекта. Имя можно установить только один раз, изменять запрещено.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="Id"></param>
        internal void Set(string name, UInt64 Id)
        {
            if (currentName != null)
                throw new Store.Const.StorageException("Запрещено переименовывать существующие объекты");

            table.table.Set(Id, name);
            table.tableIndex.UpdateIndex(null, buffer => buffer.Add(name),
                                                 buffer => buffer.Add(Id));

            currentName = name;
        }

        internal void SetWithCanonize(string name, UInt64 Id)
        {
            table.table.Set(Id, name);
            table.tableIndex.UpdateIndex(null, buffer => buffer.Add(Files.CanonizeFileName(name)),
                                                 buffer => buffer.Add(Id));

            currentName = name;
        }

        internal void Erase()
        {
            currentName = null;
        }

        #region implementing Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "keySaver")]
        public void InformLoaded(Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.Saver keySaver)
        {
        }

        public IBufferWriter key
        {
            get
            {
                if (CurrentName == null)
                    throw new Exception("Попытка добавить в индекс пустой идентификатор");

                IBufferWriter writer = WriterPool.Get();
                writer.Add(currentName);
                return writer;
            }
        }

        public bool isLoaded
        {
            get
            {
                return currentName != null;
            }
        }
        #endregion

    }

}
