﻿using System;
using Store.Table;

namespace Store.BaseDatatypes
{
    internal abstract class DataObject_IDDuplicate
    {
        /// <summary>
        /// Загрузить свое состояние из буфера
        /// </summary>
        /// <param name="reader"></param>
        protected abstract void Load(IBufferReader reader);
        /// <summary>
        /// Сохранить свое состояние в буффер
        /// </summary>
        protected abstract void SaveToBuffer(IBufferWriter buffer);
        /// <summary>
        /// Обновить все индексы для этого элемента.
        /// </summary>
        protected abstract void SaveIndexes();


        private UInt64 theID = Store.Const.CONSTS.WrongID;
        private TableIDDuplicate table;

        public UInt64 ID
        {
            get { return theID; }
            protected set { theID = value; }
        }

        /// <summary>
        /// Сохранить свое состояние в базу
        /// </summary>
        public virtual bool SaveWithoutRepetition()
        {
            //Обновляем все индексы
            SaveIndexes();

            //Сохраняем сам объект
            using (IBufferWriter buffer = WriterPool.Get())
            {
                SaveToBuffer(buffer);

                if (!table.Contains(theID, buffer))
                {
                    table.Put(ID, buffer);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Сохранить свое состояние в базу
        /// </summary>
        public virtual void SaveWithRepetition()
        {
            //Обновляем все индексы
            SaveIndexes();

            //Сохраняем сам объект
            using (IBufferWriter buffer = WriterPool.Get())
            {
                SaveToBuffer(buffer);
                table.Put(theID, buffer);
            }
        }

        /// <summary>
        /// Инициализация объекта, уже прочитанного из базы данных
        /// </summary>
        /// <param name="tbl">Таблица, в которой живет объект</param>
        /// <param name="ID">Идентификатор объекта</param>
        /// <param name="reader">Буфер с данными объекта</param>
        public void Initialize(TableIDDuplicate tbl, UInt64 ID, IBufferReader reader)
        {
            this.table = tbl;
            theID = ID;

            Load(reader);
        }

        public void Initialize(TableIDDuplicate tbl)
        {
            this.table = tbl;
        }
    }
}
