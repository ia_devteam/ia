﻿using System;
using System.Collections.Generic;
using Store.Table;

namespace Store.BaseDatatypes
{
    internal class Index_DelayCreation_KeyId_ValueBuffer_NoDuplicate
    {
        TableIDNoDuplicate table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;
        EnumerateRebuild enumerateRebuild;

        [System.Diagnostics.CodeAnalysis.SuppressMessage ("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public delegate IEnumerable<KeyValuePair<UInt64, IBufferWriter>> EnumerateRebuild(IBufferWriter value);
        
        public Index_DelayCreation_KeyId_ValueBuffer_NoDuplicate(Storage storage, string tableName, EnumerateRebuild enumerateRebuild)
        {
            this.storage = storage;
            this.tableName = tableName;
            this.enumerateRebuild = enumerateRebuild;
        }

        /// <summary>
        /// Возвращает элемент, хранимый в индексе по указанному ключу.
        /// Возвращает null, если по указанному ключу в индексе ничего не хранится.
        /// </summary>
        /// <param name="theID"></param>
        /// <returns></returns>
        public IBufferReader GetValueByID(ulong theID)
        {
            AcqireTable();

            IBufferReader buf;
            table.Get(theID, out buf);
            return buf;
        }

        public delegate void Saver(IBufferWriter writer);

        public bool Put(UInt64 ID, Saver saverValue)
        {
            if (ID == Store.Const.CONSTS.WrongID)
                throw new Exception("Попытка добавить индект с ключом WrongID");

            TryAcquireTable();

            if (table != null)
            {
                using (IBufferWriter buffer = WriterPool.Get())
                {
                    saverValue(buffer);

                    table.Put(ID, buffer);
                }
            }

            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<KeyValuePair<UInt64, IBufferReader>> Enumerate()
        {
            AcqireTable();
            return table;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Remove(UInt64 Id)
        {
            TryAcquireTable();
            if (table != null)
            {
                table.Remove(Id);
                table = null;
            }
        }

        private void AcqireTable()
        {
            if (table == null)
                table = storage.TryOpenTableIDNoDuplicate(tableName);

            if (table == null)
            {
                table = storage.GetTableIDNoDuplicate(tableName);

                using (IBufferWriter writer = WriterPool.Get())
                    foreach (KeyValuePair<UInt64, IBufferWriter> pair in enumerateRebuild(writer))
                    {
                        table.Put(pair.Key, pair.Value);
                        writer.Clear();
                    }
            }
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null)
                    table = storage.TryOpenTableIDNoDuplicate(tableName);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal void Clear()
        {
            storage.DeleteDB(tableName, table);
            table = null;
        }
    }


    internal class Index_DelayCreation_KeyBuffer_ValueBuffer_NoDuplicate
    {
        TableIndexedNoDuplicates table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;
        EnumerateRebuild enumerateRebuild;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public delegate IEnumerable<KeyValuePair<IBufferWriter, IBufferWriter>> EnumerateRebuild(IBufferWriter key, IBufferWriter value);

        public Index_DelayCreation_KeyBuffer_ValueBuffer_NoDuplicate(Storage storage, string tableName, EnumerateRebuild enumerateRebuild)
        {
            this.storage = storage;
            this.tableName = tableName;
            this.enumerateRebuild = enumerateRebuild;
        }

        public delegate void Saver(IBufferWriter writer);

        /// <summary>
        /// Возвращает элемент, хранимый в индексе по указанному ключу.
        /// Возвращает null, если по указанному ключу в индексе ничего не хранится.
        /// </summary>
        /// <param name="writerKey"></param>
        /// <returns></returns>
        public IBufferReader GetValueByKey(Saver writerKey)
        {
            AcqireTable();

            IBufferReader ValueBuf;
            using (IBufferWriter buffer = WriterPool.Get())
            {
                writerKey(buffer);
                table.Get(buffer, out ValueBuf);
            }

            return ValueBuf;
        }


        public bool Put(Saver saverKey, Saver saverValue)
        {
            TryAcquireTable();

            if (table != null)
            {
                using (IBufferWriter bufferKey = WriterPool.Get())
                using (IBufferWriter bufferValue = WriterPool.Get())
                {
                    saverKey(bufferKey);
                    saverValue(bufferValue);

                    table.Put(bufferKey, bufferValue);
                }
            }

            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<KeyValuePair<IBufferReader, IBufferReader>> Enumerate()
        {
            AcqireTable();
            return table;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public void Remove(Saver saverKey)
        {
            TryAcquireTable();
            using (IBufferWriter bufferKey = WriterPool.Get())
            {
                saverKey(bufferKey);

                if (table != null)
                    table.Remove(bufferKey);
            }
        }

        private void AcqireTable()
        {
            TryAcquireTable();

            if (table == null)
            {
                table = storage.GetTableIndexNoDuplicate(tableName);

                using (IBufferWriter key = WriterPool.Get())
                using(IBufferWriter value = WriterPool.Get())
                    foreach (KeyValuePair<IBufferWriter, IBufferWriter> pair in enumerateRebuild(key, value))
                    {
                        table.Put(pair.Key, pair.Value);
                        key.Clear();
                        value.Clear();
                    }
            }
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null)
                    table = storage.TryOpenTableIndexedNoDuplicates(tableName);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal void Clear()
        {
            storage.DeleteDB(tableName, ref table);
            table = null;
        }
    }
}
