﻿using System;
using System.Collections.Generic;
using Store.Table;

namespace Store.BaseDatatypes
{
    internal class Index_DelayCreation_KeyId_ValueId_NoDuplicate
    {
        TableIDNoDuplicate table;

        //Сохраняемые данные, задаваемые конструктору
        Storage storage;
        string tableName;
        EnumerateRebuild enumerateRebuild;

        [System.Diagnostics.CodeAnalysis.SuppressMessage ("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public delegate IEnumerable<KeyValuePair<UInt64, UInt64>> EnumerateRebuild();

        public Index_DelayCreation_KeyId_ValueId_NoDuplicate(Storage storage, string tableName, EnumerateRebuild enumerateRebuild)
        {
            this.storage = storage;
            this.tableName = tableName;
            this.enumerateRebuild = enumerateRebuild;
        }

        /// <summary>
        /// Возвращает элемент, хранимый в индексе по указанному ключу.
        /// Возвращает null, если по указанному ключу в индексе ничего не хранится.
        /// </summary>
        /// <param name="theID"></param>
        /// <returns></returns>
        public UInt64 GetValueByID(UInt64 theID)
        {
            AcqireTable();

            IBufferReader buf;
            if (table.Get(theID, out buf) == enResult.NOT_FOUND)
                return Store.Const.CONSTS.WrongID;
            else
            {
                UInt64 res = buf.GetUInt64();
                buf.Dispose();

                return res;
            }
        }

        internal ulong GetValueByIDIfIndexBuilded(ulong theID)
        {
            TryAcquireTable();

            if (table != null)
            {
                IBufferReader buf;
                if (table.Get(theID, out buf) == enResult.NOT_FOUND)
                    return Store.Const.CONSTS.WrongID;
                else
                {
                    UInt64 res = buf.GetUInt64();
                    buf.Dispose();

                    return res;
                }
            }
            else
                return Store.Const.CONSTS.WrongID;
        }

        public bool Put(UInt64 ID, UInt64 Value)
        {
            TryAcquireTable();

            if (table != null)
            {
                using (IBufferWriter buffer = WriterPool.Get())
                {
                    buffer.Add(Value);
                    table.Put(ID, buffer);
                }
            }

            return false;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public IEnumerable<KeyValuePair<UInt64, UInt64>> Enumerate()
        {
            AcqireTable();
            foreach(KeyValuePair<UInt64, IBufferReader> pair in table)
            yield return new KeyValuePair<UInt64, UInt64> (pair.Key, pair.Value.GetUInt64());
        }

        public void Remove(UInt64 Id)
        {
            TryAcquireTable();
            if(table != null)        
                table.Remove(Id);
        }

        private void AcqireTable()
        {
            if (table == null)
                table = storage.TryOpenTableIDNoDuplicate(tableName);

            if (table == null)
            {
                table = storage.GetTableIDNoDuplicate(tableName);

                using (IBufferWriter writer = WriterPool.Get())
                    foreach (KeyValuePair<UInt64, UInt64> pair in enumerateRebuild())
                    {
                        writer.Add(pair.Value);
                        table.Put(pair.Key, writer);
                        writer.Clear();
                    }
            }
        }

        bool isTried = false;
        private void TryAcquireTable()
        {
            if (!isTried) //Повторно делать попытку не стоит. При отсутствии таблицы на попытку её открытия тратится много времени.
            {
                isTried = true;
                if (table == null)
                    table = storage.TryOpenTableIDNoDuplicate(tableName);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal void Clear()
        {
            if (table != null)
            {
                storage.DeleteDB(tableName, table);
                table = null;
            }
        }


        internal bool isIndexBuilded()
        {
            TryAcquireTable();
            return table != null;
        }
    }
}
