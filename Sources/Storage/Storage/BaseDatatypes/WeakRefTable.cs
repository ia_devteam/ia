﻿using System;
using System.Collections.Generic;

namespace Store.BaseDatatypes
{

    internal class WeakRefTableObject
    {
        internal virtual bool isFinalized()
        {
            return false;
        }
    }

    internal class WeakRefTable<T>
        where T : WeakRefTableObject
    {
        Dictionary<UInt64, WeakReference> existing = new Dictionary<UInt64, WeakReference>();
        int count = 0;

        public T Get(UInt64 ID)
        {
            WeakReference reference;
            if (existing.TryGetValue(ID, out reference))
            {
                Object obj = reference.Target;
                if (obj == null)
                    existing.Remove(ID);
                else
                {
                    T t = (T) obj;

                    if (t.isFinalized())
                        existing.Remove(ID);
                    else
                        return t;
                }
            }

            return default(T);
        }

        public void Add(UInt64 ID, T obj)
        {
            //Периодически выполняем прочистку таблицы
            count++;
            List<UInt64> Dead = new List<UInt64>();
            if (count % 10000 == 0)
            {
                foreach (KeyValuePair<UInt64, WeakReference> pair in existing)
                    if (pair.Value.Target == null)
                        Dead.Add(pair.Key);

                foreach (UInt64 key in Dead)
                    existing.Remove(key);
            }

            existing[ID] = new WeakReference(obj, true);
        }

        public void Clear()
        {
            existing.Clear();
        }

        public void Remove(ulong ID)
        {
            existing.Remove(ID);
        }

        /// <summary>
        /// Возвращает массив все в настоящее время находящих в памяти объектов из таблицы.
        /// Только те объекты, которые еще не финализированы.
        /// </summary>
        /// <returns></returns>
        public T[] Alives()
        {
            List<T> Alive = new List<T>();
            List<UInt64> Dead = new List<UInt64>();
            
            foreach (KeyValuePair<UInt64, WeakReference> pair in existing)
            {
                Object obj = pair.Value.Target;
                if (obj == null)
                    Dead.Add(pair.Key);
                else
                {
                    T t = (T)obj;
                    if (t.isFinalized())
                        Dead.Add(pair.Key);
                    else
                        Alive.Add(t);
                }
            }

            foreach (UInt64 ID in Dead)
                existing.Remove(ID);

            return Alive.ToArray();
        }
    }
}
