﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values To modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Storage")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Storage")]
[assembly: AssemblyCopyright("Copyright ©  2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible To false makes the types in this assembly not visible 
// To COM components.  If you need To access a type in this assembly From 
// COM, set the ComVisible attribute To true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the Id of the typelib if this project is exposed To COM
[assembly: Guid("60b896ad-3caa-4ec3-8487-2249b3c0dacb")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.0.2.4")]
[assembly: AssemblyFileVersion("3.0.2.4")]


[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("TestStorage")]

public class AssemblyInfo
{
    private Type type;
    public AssemblyInfo()
    {
        type = typeof(Store.Storage);
    }

    public string Version
    {
        get { string version = type.Assembly.GetName().Version.ToString(); return version.Substring(0, version.IndexOf('.')); }
    }
}
