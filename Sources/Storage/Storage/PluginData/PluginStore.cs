﻿using System;
using Store.Table;

namespace Store
{
    /// <summary>
    /// Менеджер настроек плагинов. По идентификатору плагина хранит его настройки.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Plugin")]
    public sealed class PluginSettings
    {
        /// <summary>
        /// Имя таблицы
        /// </summary>
        const string SettingsFileName = "PluginSettings";
        
        TableIDNoDuplicate tableSettings;

        #region Generate PluginSettings

        /// <summary>
        /// Создавать экземпляры PluginSettings самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal PluginSettings(Storage storage)
        {
            tableSettings = storage.GetTableIDNoDuplicate(SettingsFileName);
        }

        #endregion

        /// <summary>
        /// Возвращает настройки текущего плагина. 
        /// </summary>
        /// <param name="objectId">Идентификатор плагина.</param>
        /// <returns>Буфер с настройками плагина. Если настроек в Хранилище нет, возвращает null.</returns>
        public IBufferReader LoadSettings(UInt64 objectId)
        {
            IBufferReader reader;
            tableSettings.Get(objectId, out reader);
            return reader;
        }

        /// <summary>
        /// Сохраняет настройки текущего плагина. 
        /// </summary>
        /// <param name="objectId">Идентификатор плагина.</param>
        /// <param name="buffer">Настройки плагина. Если buffer==null, сохранение не производится.</param>
        public void SaveSettings(UInt64 objectId, IBufferWriter buffer)
        {
            if(buffer != null)
                tableSettings.Put(objectId, buffer);
        }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Plugin")]
    public sealed class PluginResults
    {
        /// <summary>
        /// Имя таблицы
        /// </summary>
        const string ResultControlTable = "ResultControl";

        TableIDNoDuplicate tableResult;

        #region Generate PluginResult

        /// <summary>
        /// Создавать экземпляры PluginSettings самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>

        internal PluginResults(Storage storage)
        {
            tableResult = storage.GetTableIDNoDuplicate(ResultControlTable);
        }

        #endregion

        /// <summary>
        /// Возвращает настройки текущего плагина. 
        /// </summary>
        /// <param name="objectId">Идентификатор плагина.</param>
        /// <returns>Буфер с настройками плагина. Если настроек в Хранилище нет, возвращает null.</returns>
        public IBufferReader LoadResult(UInt64 objectId)
        {
            IBufferReader reader;
            tableResult.Get(objectId, out reader);
            return reader;
        }

        /// <summary>
        /// Сохраняет настройки текущего плагина. 
        /// </summary>
        /// <param name="objectId">Идентификатор плагина.</param>
        /// <param name="buffer">Настройки плагина. Если buffer==null, сохранение не производится.</param>
        public void SaveResults(UInt64 objectId, IBufferWriter buffer)
        {
            if (buffer != null)
                tableResult.Put(objectId, buffer);
        }
    }

    /// <summary>
    /// Менеджер специальных данных плагина, используемых при работе (Run) плагина и которые между разичными запусками необходимо сохранять.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Plugin")]
    public sealed class PluginData
    {
        /// <summary>
        /// Имя каталога для хранения служебной информации плагинов.
        /// </summary>
        const string DataFolder = "Data";
        /// <summary>
        /// Имя каталога для хранения отчётов плагинов.
        /// </summary>
        const string ReportsFolder = "Reports";

        Storage storage;

        /// <summary>
        /// Создавать экземпляры PluginData самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal PluginData(Storage storage)
        {
            this.storage = storage;
        }

        /// <summary>
        /// Получить имя файла, в котором следует хранить данные плагина. Если файл не существует, он создаётся пустым.
        /// </summary>
        /// <param name="plugInId">Идентификатор плагина</param>
        /// <returns></returns>
        public string GetDataFileName(UInt64 plugInId)
        {
            string fileName = System.IO.Path.Combine(GetDataFolder(plugInId), plugInId.ToString(System.Globalization.CultureInfo.InvariantCulture));

            if (!System.IO.File.Exists(fileName))
                System.IO.File.Open(fileName, System.IO.FileMode.CreateNew).Close();

            return fileName;
        }

        /// <summary>
        /// Получить имя каталога для хранения служебной информации плагина. Если не существует - будет создан.
        /// </summary>
        /// <param name="plugInId">Идентификатор плагина.</param>
        /// <returns></returns>
        public string GetDataFolder(UInt64 plugInId)
        {
            if (plugInId == 0)
                throw new Exception("GetDataFolder: неверный идентификатор плагина");

            string directoryName = System.IO.Path.Combine(storage.DB.GetDBDirectory(), DataFolder, plugInId.ToString(System.Globalization.CultureInfo.InvariantCulture));

            if (!System.IO.Directory.Exists(directoryName))
                System.IO.Directory.CreateDirectory(directoryName);

            return directoryName;
        }
    }
}
