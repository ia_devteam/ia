﻿using System;
using System.Collections.Generic;
using Store.BaseDatatypes;
using Store.Table;


namespace Store
{
    internal enum enVariableKind
    {
        VARIABLE,
        FIELD
    }

    /// <summary>
    /// Описыватель присвоения некоторой переменной значения другой переменной. Используется для PointTo анализа.
    /// </summary>
    public interface IAssignment
    {
        /// <summary>
        /// Переменная, значение которой берётся
        /// </summary>
        Variable From { get; }
        /// <summary>
        /// Переменная, значение которой изменяется
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "To")]
        Variable To { get; set; }
        /// <summary>
        /// Позиция в исходных текстах, в которой находится присвоение
        /// </summary>
        Location Position { get; }

        /// <summary>
        /// Указать позицию в исходных текстах, в которой находится присваивание
        /// </summary>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddPosition(IFile file, UInt64 offset, IFunction functionn);
        /// <summary>
        /// Указать позицию в исходных текстах, в которой находится присваивание
        /// </summary>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddPosition(IFile file, UInt64 line, UInt64 column, IFunction functionn);
        /// <summary>
        /// Указать позицию в исходных текстах, в которой находится присваивание
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddPosition(string fullFileName, UInt64 offset, IFunction functionn);
        /// <summary>
        /// Указать позицию в исходных текстах, в которой находится присваивание
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddPosition(string fullFileName, UInt64 line, UInt64 column, IFunction functionn);

        /// <summary>
        /// Данную функцию необходимо вызвать, чтобы внесённые в полях <see cref="Store.IAssignment.From"/> и <see cref="Store.IAssignment.To"/> изменения были сохранены в базе данных
        /// </summary>
        void Save();
    }

    internal class Assignment_Impl : DataObject_IDNoDuplicate_Lazy, IAssignment
    {
        Variables variables;

        UInt64 id_from = Store.Const.CONSTS.WrongID;
        UInt64 id_to;

        internal Assignment_Impl(Variables variables)
        {
            this.variables = variables;
            isChanged = true;
        }

        #region Assignment Members

        public Variable From
        {
            get
            {
                return variables.GetVariable(id_from);
            }
            set
            {
                id_from = value.Id;
                isChanged = true;

                //RUS: это костыль. Приношу производительность в жертву качеству. красивое выпиливание трудоёмко и не имеет смысла ввиду переписывания Хранилища на объектную СУБД.
                Save();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0")]
        public Variable To
        {
            get
            {
                return variables.GetVariable(id_to);
            }
            set
            {
                id_to = value.Id;
                isChanged = true;

                //RUS: это костыль. Приношу производительность в жертву качеству. красивое выпиливание трудоёмко и не имеет смысла ввиду переписывания Хранилища на объектную СУБД.
                Save();
            }
        }

        public Location Position
        {
            get
            {
                return variables.GetLocationsValueAssignment().GetObjectLocation(theID);
            }
        }

        public void AddPosition(IFile file, ulong offset, IFunction function)
        {
            variables.GetLocationsValueAssignment().SetSingleLocation(theID, file, offset, function);
        }

        public void AddPosition(IFile file, ulong line, ulong column, IFunction function)
        {
            variables.GetLocationsValueAssignment().SetSingleLocation( theID, file, line, column, function);
        }

        public void AddPosition(string fullFileName, ulong offset, IFunction function)
        {
            variables.GetLocationsValueAssignment().SetSingleLocation(theID, variables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), offset, function);
        }

        public void AddPosition(string fullFileName, ulong line, ulong column, IFunction function)
        {
            variables.GetLocationsValueAssignment().SetSingleLocation(theID, variables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), line, column, function);
        }

        #endregion

        protected override string GetLoadErrorString()
        {
            return "Попытка загрузить описвание присвоения переменной с идентификатором %s. Такого в базе не существует";
        }

        protected override void Load(IBufferReader reader)
        {
            id_from = reader.GetUInt64();
            id_to = reader.GetUInt64();
        }

        protected override void SaveToBuffer(IBufferWriter buffer)
        {
            buffer.Add(id_from);
            buffer.Add(id_to);
        }

        protected override void SaveIndexes()
        {
            if (id_from != Store.Const.CONSTS.WrongID)
                variables.tableAssignmentsFrom.PutWithoutRepetition(id_from, writer => writer.Add(theID));
        }
    }
    /// <summary>
    /// Класс используется для хранения информации о том, что переменная может хранить указатель на функцию.
    /// 
    /// ВНИМАНИЕ: реализация использует ленивое сохранение информации в таблицы. Для того, чтобы объект отобразился в индексах (перечислениях и т.п.), для него либо должен сработать 
    /// финализатор, либо необходимо вызвать его метод Save().
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface PointsToFunction
    {
        /// <summary>
        /// Функция, на которую указывает переменная
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Function"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "function")]
        UInt64 function { get; set; }
        /// <summary>
        /// Переменная, которой присваивается указатель на функцию
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "variable")]
        Variable variable { get; }
        /// <summary>
        /// true, если в данном месте ведётся взятие адреса,
        /// false, если в данном месте идётся только присвоение одной переменной другой и происходит передача уже взятого указателя.
        /// Поле введено для целей генерации отчётов. Иначе вводятся ссылки на места, где функция не упоминается (происходит передача указателя)
        /// </summary>
        bool isGetAddress { get; set; }
        /// <summary>
        /// Место присвоения указателя
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "position")]
        Location position { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddPosition(IFile file, UInt64 offset, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddPosition(IFile file, UInt64 line, UInt64 column, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddPosition(string fullFileName, UInt64 offset, IFunction functionn);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddPosition(string fullFileName, UInt64 line, UInt64 column, IFunction functionn);

        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddPosition(Location pos);
        
        /// <summary>
        /// Сохранить состояние объекта в БД
        /// </summary>
        void Save();
    }

    internal class PointsToFunction_Impl : DataObject_IDNoDuplicate_Lazy, PointsToFunction
    {
        Variables variables;

        UInt64 id_function = Store.Const.CONSTS.WrongID;
        UInt64 id_variable = Store.Const.CONSTS.WrongID;
        bool theIsGetAddress = true;

        /// <summary>
        /// Маркер, используемый для поддержания состояния индекса variables.storage.functions.tableOperationsGetPointerToFunction
        /// </summary>
        internal Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates Marker_tableOperationsGetPointerToFunction;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variables">Переменная, которой присваивается указатель</param>
        internal PointsToFunction_Impl(Variables variables)
        {
            this.variables = variables;
            theIsGetAddress = isGetAddress;
            Marker_tableOperationsGetPointerToFunction = variables.storage.functions.tableOperationsGetPointerToFunction.GetMarker();
        }

        #region PointsToFunction Members

        public UInt64 function
        {
            get
            {
                return id_function;
            }
            set
            {
                id_function = value;
                isChanged = true;

                //RUS: это костыль. Приношу производительность в жертву качеству. красивое выпиливание трудоёмко и не имеет смысла ввиду переписывания Хранилища на объектную СУБД.
                Save();
            }
        }

        public Variable variable
        {
            get
            {
                return variables.GetVariable(id_variable);
            }
            set
            {
                id_variable = value.Id;
                isChanged = true;

                //RUS: это костыль. Приношу производительность в жертву качеству. красивое выпиливание трудоёмко и не имеет смысла ввиду переписывания Хранилища на объектную СУБД.
                Save();
            }
        }

        public bool isGetAddress 
        { 
            get { return theIsGetAddress; }
            set 
            {
                theIsGetAddress = value;
                isChanged = true; 
                Save();
            }
        }

        public Location position
        {
            get
            {
                Location locs = variables.GetLocationsGettingPointerToFunction().GetObjectLocation(theID);
                if (locs != null)
                    return locs;
                else 
                    return null;
            }
        }
        
        public void AddPosition(IFile file, ulong offset, IFunction function)
        {
            variables.GetLocationsGettingPointerToFunction().SetSingleLocation(theID, file, offset, function);
            isChanged = true;

            //RUS: это костыль. Приношу производительность в жертву качеству. красивое выпиливание трудоёмко и не имеет смысла ввиду переписывания Хранилища на объектную СУБД.
            Save();
        }

        public void AddPosition(IFile file, ulong line, ulong column, IFunction function)
        {
            variables.GetLocationsGettingPointerToFunction().SetSingleLocation(theID, file, line, column, function);
            isChanged = true;

            //RUS: это костыль. Приношу производительность в жертву качеству. красивое выпиливание трудоёмко и не имеет смысла ввиду переписывания Хранилища на объектную СУБД.
            Save();
        }

        public void AddPosition(string fullFileName, ulong offset, IFunction function)
        {
            variables.GetLocationsGettingPointerToFunction().SetSingleLocation(theID, variables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), offset, function);
            isChanged = true;

            //RUS: это костыль. Приношу производительность в жертву качеству. красивое выпиливание трудоёмко и не имеет смысла ввиду переписывания Хранилища на объектную СУБД.
            Save();
        }

        public void AddPosition(string fullFileName, ulong line, ulong column, IFunction function)
        {
            variables.GetLocationsGettingPointerToFunction().SetSingleLocation(theID, variables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), line, column, function);
            isChanged = true;

            //RUS: это костыль. Приношу производительность в жертву качеству. красивое выпиливание трудоёмко и не имеет смысла ввиду переписывания Хранилища на объектную СУБД.
            Save();
        }

        public void AddPosition(Location pos)
        {
            variables.GetLocationsGettingPointerToFunction().SetSingleLocation(theID, pos);
            isChanged = true;

            //RUS: это костыль. Приношу производительность в жертву качеству. красивое выпиливание трудоёмко и не имеет смысла ввиду переписывания Хранилища на объектную СУБД.
            Save();
        }

        #endregion

        protected override string GetLoadErrorString()
        {
            return "Попытка загрузить описвание присвоения переменной с идентификатором %s. Такого в базе не существует";
        }

        protected override void Load(IBufferReader reader)
        {
            id_function = reader.GetUInt64();
            id_variable = reader.GetUInt64();
            reader.GetBool(ref theIsGetAddress);

            if (position != null)
            {
                Marker_tableOperationsGetPointerToFunction.InformLoaded(writer => writer.Add(position.GetFunctionId()));
            }
        }

        protected override void SaveToBuffer(IBufferWriter buffer)
        {
            buffer.Add(id_function);
            buffer.Add(id_variable);
            buffer.Add(theIsGetAddress);
        }

        protected override void SaveIndexes()
        {
            if (id_variable != Store.Const.CONSTS.WrongID)
                variables.tablePointsToFunctionsIndex.PutWithoutRepetition(id_variable, writer => writer.Add(theID));
            if (id_function != Store.Const.CONSTS.WrongID)
                variables.tableFunctionPointerGetsIndex.PutWithoutRepetition(id_function, writer => writer.Add(theID));
            if (position != null)
                variables.storage.functions.tableOperationsGetPointerToFunction.UpdateIndex(Marker_tableOperationsGetPointerToFunction, writer => writer.Add(position.GetFunctionId()), writer => writer.Add(theID));
            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface Variable
    {
        /// <summary>
        /// Идентификатор переменной (его имя)
        /// Имя можно установить только один раз, после чего модификация становится невозможна.
        /// </summary>
        string Name
        { get; set; }

        /// <summary>
        /// Полное имя переменной
        /// </summary>
        string FullName
        { get; }

        string ToString();


        /// <summary>
        /// Номер класса в Хранилище
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID")]
        UInt64 Id
        {
            get;
        }

        /// <summary>
        /// Модуль, в котором реализована данная переменная. Может быть установлен при помощи метода <see cref="Store.Module.AddImplementedGlobalVariable(Store.Variable)"/>
        /// </summary>
        Module ImplementedInModule { get; }
        
        /// <summary>
        /// Идентификатор модуля, в котором реализована данная переменная. Может быть установлен при помощи метода <see cref="Store.Module.AddImplementedGlobalVariable(Store.Variable)"/>
        /// </summary>
        ulong ImplementedInModuleId { get; }

        /// <summary>
        /// Указыват на глобальность переменной в соответствии с требованиями РД НДВ. Во всех остальных случаях применять аккуратно, 
        /// так как может выдавать странные значения.
        /// <c>true</c>, если переменная глобальная,
        /// <c>false</c>, если переменная глобальная.
        /// 
        /// По умолчанию равняется <c>false</c>
        /// 
        /// Для полей значение флага совпадает со значением флага isStatic.
        /// Устанавливать значение можно только для переменных, не являющихся полями.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "is")]
        bool isGlobal { get; set; }

        /// <summary>
        /// Перечислить присвоения значения данной переменной другим переменным
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        IEnumerable<IAssignment> Enumerate_AssignmentsFrom();

        /// <summary>
        /// Добавить присвоение переменной значения другой переменной
        /// </summary>
        /// <returns></returns>
        IAssignment AddAssignmentsFrom();

        /// <summary>
        /// Перечислить присвоения данной переменной указателя на функцию
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        IEnumerable<PointsToFunction> Enumerate_PointsToFunctions();

        /// <summary>
        /// Добавить присвоение переменной значения другой переменной
        /// </summary>
        /// <returns></returns>
        PointsToFunction AddPointsToFunctions();

        

        /// <summary>
        /// Возвращает массив, хранящий все места определений переменной.
        /// </summary>
        /// <returns></returns>
        Location[] Definitions();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddDefinition(IFile file, UInt64 offset, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddDefinition(IFile file, UInt64 line, UInt64 column, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddDefinition(string fullFileName, UInt64 offset, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddDefinition(string fullFileName, UInt64 line, UInt64 column, IFunction functionn);

        /// <summary>
        /// Возвращает массив, хранящий все места присвоения переменной значения.
        /// </summary>
        /// <returns></returns>
        Location[] ValueSetPosition();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueSetPosition(IFile file, UInt64 offset, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueSetPosition(IFile file, UInt64 line, UInt64 column, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="offset">смещение</param>
        /// <param name="functionn">функция</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueSetPosition(string fullFileName, UInt64 offset, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="line">строка</param>
        /// <param name="column">столбец</param>
        /// <param name="functionn">функция</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueSetPosition(string fullFileName, UInt64 line, UInt64 column, IFunction functionn);

        /// <summary>
        /// Возвращает массив, хранящий все места получения значения переменной.
        /// </summary>
        /// <returns></returns>
        Location[] ValueGetPosition();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueGetPosition(IFile file, UInt64 offset, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueGetPosition(IFile file, UInt64 line, UInt64 column, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueGetPosition(string fullFileName, UInt64 offset, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueGetPosition(string fullFileName, UInt64 line, UInt64 column, IFunction functionn);
        
        /// <summary>
        /// Возвращает массив, хранящий все места вызова функции, указатель на которую хранится в данной переменной.
        /// </summary>
        /// <returns></returns>
        Location[] ValueCallPosition();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueCallPosition(IFile file, UInt64 offset, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueCallPosition(IFile file, UInt64 line, UInt64 column, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="offset"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueCallPosition(string fullFileName, UInt64 offset, IFunction functionn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFileName">Имя файла. Файл должен присутствовать в исходных текстах. То есть он должен содержать префикс</param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="functionn"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "functionn")]
        void AddValueCallPosition(string fullFileName, UInt64 line, UInt64 column, IFunction functionn);
    }

    internal class Variable_Impl : DataObjectLeaderWithName, Variable
    {
        protected Variables theVariables;
        
        /// <summary>
        /// true, если данные из таблицы уже загружены
        /// </summary>
        protected bool isModifiersLoaded = false;

        /// <summary>
        /// Является ли переменная глобальной?
        /// </summary>
        private bool theIsGlobal = false;


        #region Save and load

        internal Variable_Impl(Variables variables) : base(variables.tableNaming)
        {
            theVariables = variables;
        }

        /// <summary>
        /// Переопределяется потомками, если им нужно сохранять свои модификаторы
        /// Используется для загрузки модификаторов
        /// Непосредственно может вызываться только из функции LoadModifiersIfNeed().
        /// </summary>
        /// <param name="buffer"></param>
        virtual protected void LoadOwnModifiers(Store.Table.IBufferReader buffer)
        {
            buffer.GetBool(ref theIsGlobal);
        }

        /// <summary>
        /// Переопределяется потомками, если им нужно сохранять свои модификаторы
        /// Используется для сохранения модификаторов
        /// Непосредственно может вызываться только из функции SaveModifiers().
        /// </summary>
        /// <param name="buffer"></param>
        virtual protected void SaveOwnModifiers(Store.Table.IBufferWriter buffer)
        {
            buffer.Add(theIsGlobal);
        }

        /// <summary>
        /// Загрузить модификаторы, если они не были загружены
        /// </summary>
        protected void LoadModifiersIfNeed()
        {
            if (!isModifiersLoaded)
            {
                using (Store.Table.IBufferReader buffer = theVariables.tableModifiers.GetValueByID(theID))
                    if(buffer != null)
                        LoadOwnModifiers(buffer);
                
                isModifiersLoaded = true;
            }
        }

        /// <summary>
        /// Сохранить модификаторы
        /// </summary>
        virtual protected void SaveModifiers()
        {
            theVariables.tableModifiers.Put(theID, (buf) =>
            {
                SaveOwnModifiers(buf);
            });
            isModifiersLoaded = true;
        }

        #endregion

        #region Variable Members

        string Variable.Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                theVariables.tableNaming.SetName(ref naming, theID, value);
            }
        }

        internal override string NameForReports
        {
            get { return Name; }
        }

        internal override string NameForFileName
        {
            get
            {
                string ret = DataObjectLeaderWithName.DataObjectNameForFileName(Name);
                return ret;
            }
        }

        public virtual bool isGlobal
        {
            get
            {
                LoadModifiersIfNeed();
                return theIsGlobal;
            }
            set
            {
                LoadModifiersIfNeed();
                theIsGlobal = value;
                SaveModifiers();
            }
        }

        public Module ImplementedInModule
        {
            get 
            {
                UInt64 moduleId = ImplementedInModuleId;

                if (moduleId == Store.Const.CONSTS.WrongID)
                    return null;
                else
                    return theVariables.storage.modules.GetModule(moduleId);
            }
        }

        public override string ToString()   
        {
            return FullName;
        }

        public UInt64 ImplementedInModuleId
        {
            get
            {
                return theVariables.storage.modules.TableVariableImplementedInModule.GetValueByKey(theID);
            }
        }

        public IEnumerable<IAssignment> Enumerate_AssignmentsFrom()
        {
            foreach (IBufferReader reader in theVariables.tableAssignmentsFrom.GetValueByID(theID))
                yield return theVariables.tableAssignments.Load(reader.GetUInt64());
        }

        public IAssignment AddAssignmentsFrom()
        {
            Assignment_Impl impl = theVariables.tableAssignments.Add();
            impl.From = this;
            return impl;
        }

        public IEnumerable<PointsToFunction> Enumerate_PointsToFunctions()
        {
            foreach (IBufferReader reader in theVariables.tablePointsToFunctionsIndex.GetValueByID(theID))
                yield return theVariables.tablePointsToFunctions.Load(reader.GetUInt64());
        }

        public PointsToFunction AddPointsToFunctions()
        {
            PointsToFunction_Impl impl = theVariables.tablePointsToFunctions.Add();
            impl.variable = this;
            return impl;
        }

        public Location[] Definitions()
        {
            return theVariables.GetLocationsDefinition().GetObjectLocations(theID);
        }

        public void AddDefinition(IFile file, ulong offset, IFunction function)
        {
            theVariables.GetLocationsDefinition().AddLocation(theID, file, offset, function);
            IndexDefinitionInFunction(function);
        }

        public void AddDefinition(IFile file, ulong line, ulong column, IFunction function)
        {
            theVariables.GetLocationsDefinition().AddLocation(theID, file, line, column, function);
            IndexDefinitionInFunction(function);
        }

        public void AddDefinition(string fullFileName, ulong offset, IFunction function)
        {
            theVariables.GetLocationsDefinition().AddLocation(theID, theVariables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), offset, function);
            IndexDefinitionInFunction(function);
        }

        public void AddDefinition(string fullFileName, ulong line, ulong column, IFunction function)
        {
            theVariables.GetLocationsDefinition().AddLocation(theID, theVariables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), line, column, function);
            IndexDefinitionInFunction(function);
        }

        private void IndexDefinitionInFunction(IFunction function)
        {
            if (function == null)
                return;

            //Содерим индекс переменных, определенных в функции
            theVariables.tableFunctionHaveVariableDefinitions.PutWithRepetition(function.Id, theID);

            //Поддерживаем индекс модулей, в которых реализована переменная
            Module_Impl.SetVariableModuleWithCheck(theVariables.storage.modules, this, function);
        }

        public Location[] ValueSetPosition()
        {
            return theVariables.GetLocationsValueSetPosition().GetObjectLocations(theID);
        }

        public void AddValueSetPosition(IFile file, ulong offset, IFunction function)
        {
            theVariables.GetLocationsValueSetPosition().AddLocation(theID, file, offset, function);
        }

        public void AddValueSetPosition(IFile file, ulong line, ulong column, IFunction function)
        {
            theVariables.GetLocationsValueSetPosition().AddLocation(theID, file, line, column, function);
        }

        public void AddValueSetPosition(string fullFileName, ulong offset, IFunction function)
        {
            theVariables.GetLocationsValueSetPosition().AddLocation(theID, theVariables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), offset, function);
        }

        public void AddValueSetPosition(string fullFileName, ulong line, ulong column, IFunction function)
        {
            theVariables.GetLocationsValueSetPosition().AddLocation(theID, theVariables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), line, column, function);
        }

        public Location[] ValueGetPosition()
        {
            return theVariables.GetLocationsValueGetPosition().GetObjectLocations(theID);
        }

        public void AddValueGetPosition(IFile file, ulong offset, IFunction function)
        {
            theVariables.GetLocationsValueGetPosition().AddLocation(theID, file, offset, function);
        }

        public void AddValueGetPosition(IFile file, ulong line, ulong column, IFunction function)
        {
            theVariables.GetLocationsValueGetPosition().AddLocation(theID, file, line, column, function);
        }

        public void AddValueGetPosition(string fullFileName, ulong offset, IFunction function)
        {
            theVariables.GetLocationsValueGetPosition().AddLocation(theID, theVariables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), offset, function);
        }

        public void AddValueGetPosition(string fullFileName, ulong line, ulong column, IFunction function)
        {
            theVariables.GetLocationsValueGetPosition().AddLocation(theID, theVariables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), line, column, function);
        }

        public Location[] ValueCallPosition()
        {
            return theVariables.GetLocationsValueCallPosition().GetObjectLocations(theID);
        }

        public void AddValueCallPosition(IFile file, ulong offset, IFunction function)
        {
            theVariables.GetLocationsValueCallPosition().AddLocation(theID, file, offset, function);
        }

        public void AddValueCallPosition(IFile file, ulong line, ulong column, IFunction function)
        {
            theVariables.GetLocationsValueCallPosition().AddLocation(theID, file, line, column, function);
        }

        public void AddValueCallPosition(string fullFileName, ulong offset, IFunction function)
        {
            theVariables.GetLocationsValueCallPosition().AddLocation(theID, theVariables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), offset, function);
        }

        public void AddValueCallPosition(string fullFileName, ulong line, ulong column, IFunction function)
        {
            theVariables.GetLocationsValueCallPosition().AddLocation(theID, theVariables.storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.fileWithPrefix), line, column, function);
        }

        internal override DataObjectLeaderWithName GetContainer()
        {
            UInt64 id = theVariables.storage.namespaces.tableVariableNamespace.GetValueByID(theID);
            return theVariables.storage.namespaces.tableLeader.Get(id);
        }

        #endregion
    }

}
