﻿using System;
using System.Collections.Generic;
using Store.Table;
using Store.BaseDatatypes;

namespace Store
{
    /// <summary>
    /// Описатель для переменных классов
    /// </summary>
    public sealed class Variables
    {
        const string TableName_Variables = "Variables";
        const string TableName_VariableNaming = "VariableNaming";
        const string TableName_LocationsValueAssignment = "VariableLocationsValueAssignment";
        const string TableName_LocationsGettingPointerToFunction = "VariableLocationsGettingPointerToFunction";
        const string TableName_LocationsDefinition = "VariableLocationsDefinition";
        const string TableName_LocationsValueSetPosition = "VariableLocationsValueSetPosition";
        const string TableName_LocationsValueGetPosition = "VariableLocationsValueGetPosition";
        const string TableName_LocationsValueCallPosition = "VariableLocationsValueCallPosition";
        const string TableName_Assignment = "VariablesAssignments";
        const string TableName_AssignmentsFrom = "VariablesAssignmentFrom";
        const string TableName_PointsToFunction = "VariablesPointsToFunctions";
        const string TableName_PointsToFunctionIndex = "VariablesPointsToFunctionsIndex";
        const string TableName_FunctionPointerGetsIndex = "FunctionsPointerGetsIndex";
        const string TableName_Modifiers = "VariableModifiers";
        const string TableName_VariableNamingIndex = "iVariableNaming";
        const string TableName_FunctionHaveVariableDefinitions = "iFunctionHaveVariableDefinitions";


        //Основные таблицы
        TableLeader<Variable_Impl> tableVariables;
        internal TableObjectNamingWithIndexAuto<Variable> tableNaming;

        internal TableIDNoDuplicate_with_WeakRefTable<Assignment_Impl> tableAssignments;
        internal TableIDNoDuplicate_with_WeakRefTable<PointsToFunction_Impl> tablePointsToFunctions;

        internal Table_KeyId_ValueSaver_Nodublicate tableModifiers;

        /// <summary>
        /// Ключ - идентификатор переменной, значение которой присваивается.
        /// Объект Assignment
        /// </summary>
        internal Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates tableAssignmentsFrom;

        /// <summary>
        /// Ключ - идентификатор переменной, которой присваивается указатель.
        /// Объект PointsToFunctions
        /// </summary>
        internal Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates tablePointsToFunctionsIndex;

        /// <summary>
        /// Ключ - идентификатор функции, указатель которой присваивается.
        /// Объект PointsToFunctions
        /// </summary>
        internal Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates tableFunctionPointerGetsIndex;

        /// <summary>
        /// Ключ - идентификатор функции, в которой находится определение переменной
        /// Значение - идентификатор переменной
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableFunctionHaveVariableDefinitions;


        LocationsForSingle locationValueAssignment;
        LocationsForSingle locationGettingPointerToFunction;
        LocationsForMultiple locationGettingDefinition;
        LocationsForMultiple locationGettingValueSetPosition;
        LocationsForMultiple locationGettingValueGetPosition;
        LocationsForMultiple locationGettingValueCallPosition;



        internal Storage storage;

        /// <summary>
        /// Создавать экземпляры Variables самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal Variables(Storage storage)
        {
            this.storage = storage;

            tableVariables = new TableLeader<Variable_Impl>(storage, TableName_Variables, CreateVariable);

            tableNaming = new TableObjectNamingWithIndexAuto<Variable>(storage, TableName_VariableNaming, TableName_VariableNamingIndex, NamingRebuild);
            tableAssignments = new TableIDNoDuplicate_with_WeakRefTable<Assignment_Impl>(storage, TableName_Assignment,
                                                                                     () => new Assignment_Impl(this));

            tableAssignmentsFrom = new Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates(storage, TableName_AssignmentsFrom);
            tablePointsToFunctions = new TableIDNoDuplicate_with_WeakRefTable<PointsToFunction_Impl>(storage, TableName_PointsToFunction,
                                                                         () => new PointsToFunction_Impl(this));

            tablePointsToFunctionsIndex = new Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates(storage, TableName_PointsToFunctionIndex);
            tableFunctionPointerGetsIndex = new Index_ImmediateCreation_KeyId_ValueBuffer_Duplicates(storage, TableName_FunctionPointerGetsIndex);

            tableModifiers = new Table_KeyId_ValueSaver_Nodublicate(storage, TableName_Modifiers);

            tableFunctionHaveVariableDefinitions = new Table_KeyId_ValueId_Dublicates(storage, TableName_FunctionHaveVariableDefinitions);
        }

        private Variable_Impl CreateVariable(Int32 kind)
        {
            switch((enVariableKind) kind)
            {
                case enVariableKind.FIELD:
                    return new Field_Impl(this);
                case enVariableKind.VARIABLE:
                    return new Variable_Impl(this);
                default:
                    throw new Exception("Неизвестный вид переменной");
            }
        }

        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult> NamingRebuild(IBufferWriter key, IBufferWriter value)
        {
            foreach (Variable var in EnumerateVariables())
            {
                key.Add(var.Name);
                value.Add(var.Id);

                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, null);
            }
        }

        /// <summary>
        /// Получить переменную по ее идентификатору
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public Variable GetVariable(UInt64 objectId)
        {
            return tableVariables.Get(objectId);
        }

        /// <summary>
        /// Получить переменные с указанным идентификатором
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Variable[] GetVariable(string name)
        {
            return tableNaming.GetEntity(name, GetVariable);
        }

        /// <summary>
        /// Получить переменные с указанным полным имененм
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Variable[] GetVariable(string[] name)
        {
            return tableNaming.GetEntity(name, GetVariable);
        }


        /// <summary>
        /// Найти переменную по её частичному имени (суффиксу). Например, по имени C1.C2 будет найдена переменная C0.C1.C2.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Variable[] FindVariable(string[] name)
        {
            return tableNaming.FindEntityBySubname(name, GetVariable);
        }

        /// <summary>
        /// Получить PointsToFunction по его идентификатору
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        internal PointsToFunction GetPointsToFunction(UInt64 objectId)
        {
            return tablePointsToFunctions.Load(objectId);
        }

        /// <summary>
        /// Перечислить все функции, имеющиеся в Хранилище
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Variable> EnumerateVariables()
        {
            foreach (Variable_Impl variable in tableVariables.EnumerateEveryone())
                yield return variable;
        }

        /// <summary>
        /// Перечислить все PointsToFunction, имеющиеся в Хранилище
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<PointsToFunction> EnumeratePointsToFunction()
        {
            foreach (PointsToFunction p in tablePointsToFunctions.EnumerateEveryone())
                yield return p;
        }

        /// <summary>
        /// Добавить новую переменную в Хранилище
        /// </summary>
        /// <returns></returns>
        public Variable AddVariable()
        {
            return tableVariables.Add((Int32) enVariableKind.VARIABLE);
        }

        /// <summary>
        /// Добавить новое поле в Хранилище
        /// </summary>
        /// <returns></returns>
        public Field AddField()
        {
            return (Field_Impl)tableVariables.Add((Int32)enVariableKind.FIELD);
        }

        /// <summary>
        /// Хэширует актуальной ссылки на Locations.
        /// </summary>
        /// <returns></returns>
        internal LocationsForSingle GetLocationsValueAssignment()
        {
            if (locationValueAssignment == null)
                locationValueAssignment = storage.locationsForSingle(TableName_LocationsValueAssignment);

            return locationValueAssignment;
        }

        /// <summary>
        /// Хэширует актуальной ссылки на Locations.
        /// </summary>
        /// <returns></returns>
        internal LocationsForSingle GetLocationsGettingPointerToFunction()
        {
            if (locationGettingPointerToFunction == null)
                locationGettingPointerToFunction = storage.locationsForSingle(TableName_LocationsGettingPointerToFunction);

            return locationGettingPointerToFunction;
        }

        /// <summary>
        /// Хэширует актуальной ссылки на Locations.
        /// </summary>
        /// <returns></returns>
        internal LocationsForMultiple GetLocationsDefinition()
        {
            if (locationGettingDefinition == null)
                locationGettingDefinition = storage.locationsForMultiple(TableName_LocationsDefinition);

            return locationGettingDefinition;
        }

        /// <summary>
        /// Хэширует актуальной ссылки на Locations.
        /// </summary>
        /// <returns></returns>
        internal LocationsForMultiple GetLocationsValueSetPosition()
        {
            if (locationGettingValueSetPosition == null)
                locationGettingValueSetPosition = storage.locationsForMultiple(TableName_LocationsValueSetPosition);

            return locationGettingValueSetPosition;
        }

        /// <summary>
        /// Хэширует актуальной ссылки на Locations.
        /// </summary>
        /// <returns></returns>
        internal LocationsForMultiple GetLocationsValueGetPosition()
        {
            if (locationGettingValueGetPosition == null)
                locationGettingValueGetPosition = storage.locationsForMultiple(TableName_LocationsValueGetPosition);

            return locationGettingValueGetPosition;
        }

        /// <summary>
        /// Хэширует актуальной ссылки на Locations.
        /// </summary>
        /// <returns></returns>
        internal LocationsForMultiple GetLocationsValueCallPosition()
        {
            if (locationGettingValueCallPosition == null)
                locationGettingValueCallPosition = storage.locationsForMultiple(TableName_LocationsValueCallPosition);

            return locationGettingValueCallPosition;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder"></param>
        public void Dump(string folder)
        {
            using (System.IO.StreamWriter wr = new System.IO.StreamWriter(System.IO.Path.Combine(folder, "variables.log")))
            {
                wr.WriteLine("Дамп переменных");

                foreach (Variable var in EnumerateVariables())
                {
                    wr.Write(var.Id + "\t" + var.Name);
                    wr.Write("\t определение : ");
                    DumpLocation(var.Definitions(), wr);
                    wr.Write("\t запись : ");
                    DumpLocation(var.ValueSetPosition(), wr);
                    wr.Write("\t чтение : ");
                    DumpLocation(var.ValueGetPosition(), wr);
                    wr.Write("\t вызов по указателю : ");
                    DumpLocation(var.ValueCallPosition(), wr);
                    wr.Write("\t присвоения между : ");
                    foreach (IAssignment ass in var.Enumerate_AssignmentsFrom())
                        wr.Write("от:" + ass.From.Id + " к:" + ass.To + " в " +
                            "(" + ass.Position.GetFileID() + ", " + ass.Position.GetLine() + ":" + ass.Position.GetColumn() + ")");
                    wr.Write("\t присвоения указателей : ");
                    foreach (PointsToFunction points in var.Enumerate_PointsToFunctions())
                    {
                        wr.Write("переменная:" + points.variable.Id + " функция:" + points.function + " в ");
                        Location pos = points.position;
                        if(pos!= null)
                            wr.Write("(" + points.position.GetFileID() + ", " + points.position.GetLine() + ":" + points.position.GetColumn() + ")");
                    }
                    wr.WriteLine();
                }
            }
        }

        static private void DumpLocation(Location[] locat, System.IO.StreamWriter wr)
        {
            foreach (Location loc in locat)
                wr.Write("(" + loc.GetFileID() + ", " + loc.GetLine() + ":" + loc.GetColumn() + ")");
        }
    }
}
