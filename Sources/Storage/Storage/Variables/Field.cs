﻿using System;
using Store.BaseDatatypes;

namespace Store
{
    /// <summary>
    /// Описатель для полей классов.
    /// </summary>
    public interface Field : Variable
    {
        /// <summary>
        /// Является ли поле статическим.
        /// </summary>
        /// <remarks>
        /// <para>Статическое поле класса едино для всех экземплятор класса (является полем суперкласса).
        /// Нестатические поля уникальны для каждого экземпляра класса.</para>
        /// <para>Если <c>true</c>, то поле является статическим.</para>
        /// </remarks>
        bool isStatic { get; set; }

        /// <summary>
        /// Операция, которая инициализирует значение поля.
        /// null, если для поля нет инициализатора.
        /// </summary>
        /// <remarks>
        /// Пример заполнения 
        /// <code>
        ///    Class cl = storage.classes.AddClass();
        ///    Field f = cl.AddField(false);
        ///    IOperation op = storage.operations.AddOperation();
        ///    f.Initializer = op;
        /// </code>
        /// </remarks>
        IOperation Initializer { get; set; }

        /// <summary>
        /// Класс, в который входит данное поле.
        /// </summary>
        Class OwnerClass { get; set; }
    }

    internal class Field_Impl: Variable_Impl, Field
    {
        bool theIsStatic;
        UInt64 operationInitializerId = Store.Const.CONSTS.WrongID;
        UInt64 theNextInCallInitializersOrderId = Store.Const.CONSTS.WrongID;
        UInt64 ownerClassId = Store.Const.CONSTS.WrongID;

        internal Field_Impl(Variables vars) :base(vars)
        {
        }

        protected override void LoadOwnModifiers(Store.Table.IBufferReader buffer)
        {
            bool var = false;
            buffer.GetBool(ref var);
            theIsStatic = var;
            operationInitializerId = buffer.GetUInt64();
            theNextInCallInitializersOrderId = buffer.GetUInt64();
            ownerClassId = buffer.GetUInt64();
        }

        protected override void SaveOwnModifiers(Store.Table.IBufferWriter buffer)
        {
            buffer.Add(theIsStatic);
            buffer.Add(operationInitializerId);
            buffer.Add(theNextInCallInitializersOrderId);
            buffer.Add(ownerClassId);
        }

        #region Field Members

        public bool isStatic
        {
            get 
            {
                LoadModifiersIfNeed();
                return theIsStatic;
            }
            set
            {
                LoadModifiersIfNeed();
                theIsStatic = value;
                SaveModifiers();
            }
        }

        public override bool isGlobal
        {
            get { return isStatic; }
            set { throw new Store.Const.StorageException("Попытка изменить флаг isGlobal у поля. Для полей флаг isGlobal определяется полем isStatic. "); }
        }

        public IOperation Initializer
        {
            get
            {
                LoadModifiersIfNeed();
                if (operationInitializerId == Store.Const.CONSTS.WrongID)
                    return null;
                else
                    return theVariables.storage.operations.GetOperation(operationInitializerId);
            }
            set
            {
                if (value != null)
                {
                    LoadModifiersIfNeed();
                    operationInitializerId = value.Id;
                    SaveModifiers();
                }
            }
        }

        #endregion

        internal ulong NextInCallInitializersOrderId 
        { 
            get
            {
                LoadModifiersIfNeed();
                return theNextInCallInitializersOrderId;
            }
            set
            {
                LoadModifiersIfNeed();
                theNextInCallInitializersOrderId = value;
                SaveModifiers();
            }
        }

        internal override DataObjectLeaderWithName GetContainer()
        {
            return OwnerClass as Class_Impl;
        }

        public Class OwnerClass 
        { 
            get
            {
                LoadModifiersIfNeed();
                if (ownerClassId == Store.Const.CONSTS.WrongID)
                    return null;
                else
                    return theVariables.storage.classes.GetClass(ownerClassId);
            }
            set 
            {
                LoadModifiersIfNeed();
                ownerClassId = value.Id;
                SaveModifiers();
            }
        }
    }
}
