﻿using System;
using System.Collections.Generic;
using Store.BaseDatatypes;
using Store.Table;
using System.Linq;

namespace Store
{
    /// <summary>
    /// Головной класс раздела Хранилища, выполняющего работу с позициями в файлах.
    /// </summary>
    public abstract class Locations
    {
        internal Storage storage;
        
        /// <summary>
        /// Ключ - идентификатор объекта
        /// Значение - описатель местоположения его определения
        /// </summary>

        internal TableIDDuplicate_MainTable<Location_Impl> tableLocations;

        /// <summary>
        /// Ключ  - &lt;Идентификатор файла, смещение&gt;
        /// Значение - идентификатор объекта
        /// </summary>
        internal Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates tableLocationsSortedByFileAndLine;


        #region Создание

        /// <summary>
        /// Создавать экземпляры Locations самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName"></param>
        internal Locations(Storage storage, string tableName)
        {
            this.storage = storage;

            tableLocationsSortedByFileAndLine = new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates(storage,
                                                                        Index_DefsFileLine_Name(tableName),
                                                                        EnumerateItemsInIndexLocationsSortedByFileAndLine);

            tableLocations = new TableIDDuplicate_MainTable<Location_Impl>(storage, tableName,
                                                                                    () => new Location_Impl(this, tableLocationsSortedByFileAndLine));
        }

        private static string Index_DefsFileLine_Name(string tableName)
        {
            return "i" + tableName + "_Def" + "_SortedByFileNameAndLine";
        }

        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult>
                                EnumerateItemsInIndexLocationsSortedByFileAndLine(IBufferWriter key, IBufferWriter value)
        {
            foreach (KeyValuePair<UInt64, Location_Impl> pair in tableLocations.EnumerateEveryone())
            {
                key.AddTransformed(pair.Value.GetFileID());
                key.AddTransformed(pair.Value.GetOffset());

                value.Add(pair.Key);

                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, pair.Value.marker);
            }
        }


        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="fileId"></param>
        /// <param name="offset"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        protected void Add(ulong objectId, UInt64 fileId, ulong offset, ulong line, ulong column, UInt64 funcId)
        {
            Location_Impl location = tableLocations.CreateObject();
            location.Set(objectId, fileId, offset, line, column, funcId);
            location.SaveWithoutRepetition();
        }

        protected void AddLoc(UInt64 objectId, IFile file, UInt64 offset, IFunction function)
        {
            CheckOffset(offset);

            if (file != null)
            {
                UInt64 line;
                UInt64 column;
                storage.locationsIndex.Get(file.Id, offset, out line, out column);

                if(function != null)
                    Add(objectId, file.Id, offset, line, column, function.Id);
                else
                    Add(objectId, file.Id, offset, line, column, Store.Const.CONSTS.WrongID);
            }
            else
                throw new Store.Const.StorageException("Вызов AddLoc без указания файла");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="func"></param>
        protected void AddLoc(UInt64 objectId, IFile file, UInt64 line, UInt64 column, IFunction func)
        {
            CheckLine(line);
            CheckColumn(column);

            if (file != null)
            {
                if (func != null)
                    Add(objectId, file.Id, storage.locationsIndex.Get(file.Id, line, column), line, column, func.Id);
                else
                    Add(objectId, file.Id, storage.locationsIndex.Get(file.Id, line, column), line, column, Store.Const.CONSTS.WrongID);
            }
            else
                throw new Store.Const.StorageException("Вызов AddLoc без указания файла");
        }

        /// <summary>
        /// Проверить, что пользователь указал корректное значение для смещения
        /// </summary>
        /// <param name="offset"></param>
        private static void CheckOffset(UInt64 offset)
        {
            if (offset == LocationConstants.wrongOffset)
                IA.Monitor.Log.Warning("Storage", "Попытка установить смещение, равное -1");
            //            throw new Store.Const.StorageException("Попытка установить смещение, равное -1");
        }

        /// <summary>
        /// Проверить, что пользователь указал корректное значение для столбца
        /// </summary>
        /// <param name="column"></param>
        private static void CheckColumn(UInt64 column)
        {
            if (column == LocationConstants.wrongColumn)
                IA.Monitor.Log.Warning("Storage", "Попытка установить номер столбца, равный 0");
            //            throw new Store.Const.StorageException("Попытка установить номер столбца, равный 0");
        }
        /// <summary>
        /// Проверить, что пользователь указал корректное значение для строки
        /// </summary>
        /// <param name="line"></param>
        private static void CheckLine(UInt64 line)
        {
            if (line == LocationConstants.wrongLine)
                IA.Monitor.Log.Warning("Storage", "Попытка установить номер строки, равный 0");
//            throw new Store.Const.StorageException("Попытка установить номер строки, равный 0");
        }
        /// <summary>
        /// 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public struct IDLocationPair
        {
            /// <summary>
            /// 
            /// </summary>
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public UInt64 ID;
            /// <summary>
            /// 
            /// </summary>
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1051:DoNotDeclareVisibleInstanceFields")]
            public Location location;
        
            #region Structure implementation
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public override int GetHashCode()
            {
                return (int)(ID ^ location.GetOffset());
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public override bool Equals(object obj)
            {
                if (obj is IDLocationPair)
                {
                    IDLocationPair locc = (IDLocationPair)obj;
                    return ID == locc.ID
                        && location.GetFileID() == locc.location.GetFileID()
                        && location.GetOffset() == locc.location.GetOffset();
                }
                else
                    return false;
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="location1"></param>
            /// <param name="location2"></param>
            /// <returns></returns>
            public static bool operator ==(IDLocationPair location1, IDLocationPair location2)
            {
                return location1.Equals(location2);
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="location1"></param>
            /// <param name="location2"></param>
            /// <returns></returns>
            public static bool operator !=(IDLocationPair location1, IDLocationPair location2)
            {
                return !location1.Equals(location2);
            }

            #endregion        
        }

        /// <summary>
        /// Перечисляет все известные места определений, упорядочивая по имени файла и номеру строки (сначала по идентификатору
        /// файла, и для одного файла упорядочивает по смещению)
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "правильно"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "построенный"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "Не"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "индекс")]
        public IEnumerable<IDLocationPair> EnumerateLocationsInFileLineOrder()
        {
            foreach (KeyValuePair<IBufferReader, IBufferReader> pair in tableLocationsSortedByFileAndLine.EnumerateWithoutDuplicates())
            {
                UInt64 fileId = pair.Key.GetUInt64Transformed();
                UInt64 offset = pair.Key.GetUInt64Transformed();

                UInt64 ID = pair.Value.GetUInt64();

                Location founded = null;
                foreach (Location location in tableLocations.GetValueByID(ID))
                {
                    if (location.GetOffset() == offset)
                        if (location.GetFileID() == fileId)
                        {
                            founded = location;
                            break;
                        }
                }

                if (founded == null)
                    throw new ApplicationException("Не правильно построенный индекс");

                IDLocationPair res;
                res.ID = ID;
                res.location = founded;

                yield return res;
            }
        }

        /// <summary>
        /// Перечисляет все объекты, определенные в указанном файле
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public IEnumerable<UInt64> EnumerateObjectsLocatedInFile(IFile file)
        {
            foreach (IBufferReader ObjectBuf in tableLocationsSortedByFileAndLine.TakeValuesWithLargerKey((buf) => { buf.AddTransformed(file.Id); buf.AddTransformed((UInt64)0); }))
            {
                UInt64 ObjectID = ObjectBuf.GetUInt64();
                Location[] locations = GetObjectLocations(ObjectID);
                foreach (Location loc in locations)
                    if (loc.GetFileID() != file.Id)
                        yield break; //Мы перешли к следующему файлу. Пора остановиться.

                yield return ObjectID;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "3#")]
        public void CalculateLineColumn(UInt64 file, ulong offset, out ulong line, out ulong column)
        {
            storage.locationsIndex.Get(file, offset, out line, out column);
        }

        /// <summary>
        /// Получить все места определения для сущности с указанным идентификатором
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected Location[] GetObjectLocations(UInt64 id)
        {
            List<Location> res = new List<Location>();
            foreach (Location location in tableLocations.GetValueByID(id))
                res.Add(location);

            return res.ToArray();
        }
    }

    /// <summary>
    /// Работает с местами для сущностей, для которых для каждой сущности может быть только одно место
    /// </summary>
    public sealed class LocationsForSingle : Locations
    {
        /// <summary>
        /// Создавать экземпляры Locations самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName"></param>
        internal LocationsForSingle(Storage storage, string tableName) : base(storage, tableName)
        {
        }

        /// <summary>
        /// Получить все места определения для сущности с указанным идентификатором
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Location GetObjectLocation(UInt64 id)
        {
            Location[] locs = tableLocations.GetValueByID(id).ToArray();

            if (locs.Count() > 1)
                throw new Store.Const.StorageException("Более одного местоположения для объекта, для которого такое не предполагалось");
            if (locs.Count() == 0)
                return null;

            return locs[0];
        }

        /// <summary>
        /// Установить значение единственно Location для объекта sensorId. У данного объекта может быть 0 или 1 Location, не более.
        /// </summary>
        /// <param name="objectId">Идентификатор объекта для установления <see cref="Location"/>.</param>
        /// <param name="file">Файл, в который будет указывать Location.</param>
        /// <param name="line">Строка в исходных текстах, на которую будет указывать Location.</param>
        /// <param name="column">Столбец в исходных текстах, на который будет указывать <see cref="Location"/>.</param>
        /// <returns></returns>
        public void SetSingleLocation(ulong objectId, IFile file, UInt64 line, UInt64 column, IFunction func)
        {
            tableLocations.DeleteById(objectId);
            AddLoc(objectId, file, line, column, func);
        }

        /// <summary>
        /// Установить значение единственно Location для объекта sensorId. У данного объекта может быть 0 или 1 Location, не более.
        /// </summary>
        /// <param name="objectId">Идентификатор объекта для установления Location.</param>
        /// <param name="file">Файл, в который будет указывать Location.</param>
        /// <param name="offset">Смещение от начала файла, на которое будет указывать Location.</param>
        /// <returns></returns>
        public void SetSingleLocation(ulong objectId, IFile file, UInt64 offset, IFunction func)
        {
            tableLocations.DeleteById(objectId);
            AddLoc(objectId, file, offset,func);
        }

        /// <summary>
        /// Установить значение единственно Location для объекта sensorId. У данного объекта может быть 0 или 1 Location, не более.
        /// </summary>
        /// <param name="objectId">Идентификатор объекта для установления Location.</param>
        /// <param name="location">Значение Location</param>
        public void SetSingleLocation(ulong objectId, Location location)
        {
            if (location == null)
                throw new Store.Const.StorageException();

            Add(objectId, location.GetFileID(), location.GetOffset(), location.GetLine(), location.GetColumn(), location.GetFunctionId());
        }

        /// <summary>
        /// Установить значение единственного Location, указывающее на не существующий файл
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="fullFileName"></param>
        public void SetSingleLocationToVitrualFile(UInt64 objectId, IFile file)
        {
            tableLocations.DeleteById(objectId);
            Add(objectId, file.Id, 0, 0, 0, Store.Const.CONSTS.WrongID);
        }

        /// <summary>
        /// Возвращает количество Location данного объекта
        /// <param name="id">Идентификатор объекта, количество Location которого ищется</param>
        /// </summary>
        public bool isHaveLocation(UInt64 id)
        {
            return tableLocations.GetValueByID(id).Count() == 1;
        }

    }

    /// <summary>
    /// Работа с местами при их множественном определении, но не внутри функции
    /// </summary>
    public class LocationsForMultiple : Locations
    {
        /// <summary>
        /// Создавать экземпляры Locations самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="tableName"></param>
        internal LocationsForMultiple(Storage storage, string tableName) : base(storage, tableName)
        {
        }

        /// <summary>
        /// Получить все места определения для сущности с указанным идентификатором
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public new Location[] GetObjectLocations(UInt64 id)
        {
            return base.GetObjectLocations(id);
        }

        /// <summary>
        /// Возвращает количество Location данного объекта
        /// <param name="id">Идентификатор объекта, количество Location которого ищется</param>
        /// </summary>
        public int Count(UInt64 id)
        {
            return tableLocations.GetValueByID(id).Count();
        }
 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="file"></param>
        /// <param name="offset"></param>
        /// <param name="function"></param>
        public void AddLocation(UInt64 objectId, IFile file, UInt64 offset, IFunction function)
        {
            AddLoc(objectId, file, offset, function);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="file"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="func"></param>
        public void AddLocation(UInt64 objectId, IFile file, UInt64 line, UInt64 column, IFunction func)
        {
            AddLoc(objectId, file, line, column, func);
        }

        public void AddLocation(ulong theID, Location pos)
        {
            Add(theID, pos.GetFileID(), pos.GetOffset(), pos.GetLine(), pos.GetColumn(), pos.GetFunctionId());
        }

        public void AddToVitrualFile(UInt64 objectId, IFile file)
        {
            Add(objectId, file.Id, 0, 0, 0, Store.Const.CONSTS.WrongID);
        }

        public void AddToVitrualFile(UInt64 objectId, string fullFileName)
        {
            Add(objectId, storage.files.FindOrGenerate(fullFileName, enFileKindForAdd.externalFile).Id, 0, 0, 0, Store.Const.CONSTS.WrongID);
        }
    }
}
