﻿using System;
using Store.Table;

namespace Store
{
    /// <summary>
    /// При добавлении Location в качестве параметра передаётся либо номер строки и столбца, либо смещение. При этом вычисляется соответственно либо смещение, либо номер строки и столбца.
    /// Такое выясиление требует открытия файла и анализа его содержимого. При этом файлы могут открываться в разнобой. Если решать задачу "в лоб", время работы анализатора становится неприемлимо долгим.
    /// Чтобы ускорить выполнение анализатора делается индексирование номеров строк. Данный класс инкапсулирует такой индекс.
    /// </summary>
    class LocationIndex
    {
        const string tableOffsetByLine_Name = "iLocationOffsetByLine";
        const string tableLineByOffset_Name = "iLocationLineByOffset";
        /// <summary>
        /// Таблица по ключу (Id файла, MaxUInt64 - номер строки) содержит смещение первого символа строки.
        /// </summary>
        TableIndexedNoDuplicates tableOffsetByLine;

        /// <summary>
        /// Таблица по ключу (Id файла, MaxUInt64 - смещение) содержит номер строки, начинающейся по этому смещению. В данную таблицу попадают только смещения, на которых начинаются строки.
        /// </summary>
        TableIndexedNoDuplicates tableLineByOffset;

        Storage storage;

        internal LocationIndex(Storage storage)
        {
            this.storage = storage;
        }

        /// <summary>
        /// Получить по номеру строки и столбца смещение в файле
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <returns>Смещение в файле, если файл найден. UInt64.MaxValue в противном случае</returns>
        internal UInt64 Get(UInt64 fileId, UInt64 line, UInt64 column)
        {
            if (line == LocationConstants.wrongLine || column == LocationConstants.wrongColumn) //Если указываем в никуда, то возвращаем ничто
                return LocationConstants.wrongOffset;

            CalculateIndexIfNeed(storage.files.GetFile(fileId));

            IBufferWriter buf = Store.Table.WriterPool.Get();
            buf.Add(fileId);
            buf.Add(line);

            IBufferReader reader;
            tableOffsetByLine.Get(buf, out reader);

            if (reader == null)
                return LocationConstants.wrongOffset;
            else
            {
                UInt64 res = reader.GetUInt64();
                return res + column - 1; //Смещение есть смещение начала строки + столбец - 1. Вычитаение 1 требуется в связи с тем, что столбцы нумеруются с 1, а смещения - с 0.
            }
        }




        /// <summary>
        /// Получить по смещению в файле номер строки и столбца. Если смещение совпадает с <see cref="LocationConstants.wrongOffset"/>, то будут выданы значения <see cref="LocationConstants.wrongLine "/> 
        /// и <see cref="LocationConstants.wrongColumn"/>
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="offset">Смещение в файле</param>
        /// <param name="line">строка в файле, если файл найден. <see cref="LocationConstants.wrongLine"/>, если файл не найден, либо смещение <see cref="LocationConstants.wrongOffset"/></param>
        /// <param name="column">столбец в фале, если файл найден. <see cref="LocationConstants.wrongColumn"/>, если файл не найден, либо смещение <see cref="LocationConstants.wrongOffset"/></param>
        internal void Get(UInt64 fileId, UInt64 offset, out UInt64 line, out UInt64 column)
        {
            if (offset == LocationConstants.wrongOffset) //Если смещения нет, то и строки со столбцом нет
            {
                line = LocationConstants.wrongLine;
                column = LocationConstants.wrongColumn;
                return;
            }
                

            //Формируем индекс по требованию
            IFile found = storage.files.GetFile(fileId);
            CalculateIndexIfNeed(found);

            //Осуществляем поиск по индексу
            IBufferWriter buf = Store.Table.WriterPool.Get();
            buf.Add(fileId);
            buf.AddTransformed(UInt64.MaxValue - offset);

            IBufferReader reader;
            tableLineByOffset.GetStartFrom(buf, out reader);

            if (reader == null ) //Если ничего не нашли
            {
                line = LocationConstants.wrongLine;
                column = LocationConstants.wrongColumn;
                return;
            }

            //Зачем-то читаем идентификатор файла
            UInt64 newFileId = reader.GetUInt64();
            if (newFileId != fileId)  //Не понятно, когда такое может случиться. Проверка "на всякий случай"
                if(found.FileExistence == enFileExistence.EXTERNAL) //такой случай описан в тесте FunctionInExtrenalFile - возвращаем 1 1
                {
                    line = 0;
                    column = 1;
                }
                else
                    throw new Store.Const.StorageException("Непредвиденная ситуация в LocationIndex.Get");

            //Вот теперь читаем номер строки
            line = reader.GetUInt64();

            //Вычисляем номер столбца. Простая разница между смещением начала строки и текущим смещением. Единица требуется из-за того, что нумерация столбцов начинается с 1
            UInt64 off = Get(fileId, line, 1);
            column = offset - off + 1;

            reader.Dispose();

            return;
        }

        /// <summary>
        /// При необходимости пересчитывает индекс для файла
        /// </summary>
        /// <param name="file"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        public void CalculateIndexIfNeed(IFile file)
        {
            GetIndexes();
            if ((file as File_Impl).isFileIndexedByLocations)
                return;
            else
            {
                CalculateIndexForFile(file);
                (file as File_Impl).isFileIndexedByLocations = true;
            }
        }

        private void GetIndexes()
        {
            if (tableOffsetByLine != null && tableLineByOffset != null)
                return;

            TryOpenLineByffset();
            TryOpenOffsetByLine();

            if (tableOffsetByLine != null && tableLineByOffset != null)
                return;

            tableOffsetByLine = storage.GetTableIndexNoDuplicate(tableOffsetByLine_Name);
            tableLineByOffset = storage.GetTableIndexNoDuplicate(tableLineByOffset_Name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>На момент вызова функции индексы должны быть уже загружены</remarks>
        /// <param name="file"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        internal void CalculateIndexForFile(IFile file)
        {
            if (file.FileExistence == enFileExistence.EXTERNAL)
                return;

            FileOperations.AbstractReader fs = null;
            try
            {
                fs = new FileOperations.AbstractReader(file.FullFileName_Original);
                
                UInt64 cur_line = 1;

                do
                {
                    PutToIndex(file.Id, cur_line, (ulong)fs.getLineOffset((int)cur_line));
                    cur_line += 1;
                } while ((int)cur_line <= fs.getLinesCount());
            }
            catch (System.IO.FileNotFoundException)
            {
                IA.Monitor.Log.Error(Storage.objectName, "Файл " + file.FullFileName_Original + " отсутствует на жестком диске. Смещение посчитано неверно");
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                IA.Monitor.Log.Error(Storage.objectName, "Файл " + file.FullFileName_Original + " отсутствует на жестком диске. Смещение посчитано неверно");
            }
            catch (System.IO.PathTooLongException)
            {
                IA.Monitor.Log.Error(Storage.objectName, "Файл " + file.FullFileName_Original + " имеет слишком длинный путь и не может быть открыт. Смещение посчитано неверно");
            }
            catch (System.ArgumentException)
            {
                IA.Monitor.Log.Error(Storage.objectName, "Путь к файлу пустой. Смещение не может быть посчитано");
            }
            catch (System.UnauthorizedAccessException ex)
            {
                IA.Monitor.Log.Error(Storage.objectName,"Не удалось получить доступ к файлу " + file.FullFileName_Original + ". " + ex.Message);
            }
            catch (System.IO.IOException ex)
            {
                if (ex.Message == "Устройство не готово.\r\n")
                    IA.Monitor.Log.Error(Storage.objectName, "Файл " + file.FullFileName_Original + " отсутствует на жестком диске. Смещение посчитано неверно");
                else
                    throw new Exception(" Some thing wrong in function LocationIndex.CalculateIndexOffsetByLineForFile. " + ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(" Some thing wrong in function LocationIndex.CalculateIndexOffsetByLineForFile. " + ex.Message);
            }
        }

        bool isTryedOpenOffsetByLine = false;
        private void TryOpenOffsetByLine()
        {
            if (!isTryedOpenOffsetByLine)
            {
                tableOffsetByLine = storage.TryOpenTableIndexedNoDuplicates(tableOffsetByLine_Name);
                isTryedOpenOffsetByLine = true;
            }
        }

        bool isTryedOpenLineByffset = false;
        private void TryOpenLineByffset()
        {
            if (!isTryedOpenLineByffset)
            {
                tableLineByOffset = storage.TryOpenTableIndexedNoDuplicates(tableLineByOffset_Name);
                isTryedOpenLineByffset = true;
            }
        }

        private void PutToIndex(UInt64 fileId, UInt64 line, UInt64 offset)
        {
            IBufferWriter writerKey = WriterPool.Get();
            writerKey.Add(fileId);
            writerKey.Add(line);

            IBufferWriter writerValue = WriterPool.Get();
            writerValue.Add(offset);

            tableOffsetByLine.Put(writerKey, writerValue);
            writerKey.Dispose();
            writerValue.Dispose();

            writerKey = WriterPool.Get();
            writerKey.Add(fileId);
            writerKey.AddTransformed(UInt64.MaxValue - offset);

            writerValue = WriterPool.Get();
            writerValue.Add(fileId);
            writerValue.Add(line);

            tableLineByOffset.Put(writerKey, writerValue);
        }
    }
}
