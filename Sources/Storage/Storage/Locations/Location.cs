﻿using System;
using Store.Table;
using Store.BaseDatatypes;

namespace Store
{

    public static class LocationConstants
    {
        /// <summary>
        /// Некорректное значение строки
        /// </summary>
        public const UInt64 wrongLine = 0;
        /// <summary>
        /// Некорректное значение столбца
        /// </summary>
        public const UInt64 wrongColumn = 0;
        /// <summary>
        /// Некорректное значение смещения
        /// </summary>
        public const UInt64 wrongOffset = UInt64.MaxValue;
    }

    /// <summary>
    /// Указатель на позицию в файле
    /// DataObject_IDDuplicate.Id является идентификатором объекта, на позицию которого указываем.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface Location
    {
        /// <summary>
        /// Получить ссылку на файл
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IFile GetFile();

        /// <summary>
        /// Получить идентификатор файла
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        UInt64 GetFileID();

        /// <summary>
        /// Получить смещение в файле. Нумерация начинается с 0 (то есть первый символ в файле имеет позицию 0)
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        UInt64 GetOffset();

        /// <summary>
        /// Получить номер строки в файле. Строки нумеруются с 1
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        UInt64 GetLine();

        /// <summary>
        /// Получить номер столбца в файле. Строки нумеруются с 1
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        UInt64 GetColumn();

        /// <summary>
        /// Возвращает идентификатор функции, в которой находится позиция.
        /// Если позиция нахождится вне функций, то возвращается
        /// Store.Const.CONSTS.WrongID
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        UInt64 GetFunctionId();

        /// <summary>
        /// Возвращает строку местоположения, которую следует использовать для отображения пользователю и для вывода в отчётах
        /// </summary>
        /// <returns></returns>
        string ToString();
    }

    /// <summary>
    /// Указатель на позицию в файле
    /// DataObject_IDDuplicate.Id является идентификатором объекта, на позицию которого указываем.
    /// </summary>
    internal class Location_Impl : DataObject_IDDuplicate, Location
    {
        UInt64 theFileID;
        UInt64 theOffset = LocationConstants.wrongOffset;
        UInt64 theLine = LocationConstants.wrongLine;
        UInt64 theColumn = LocationConstants.wrongColumn;
        UInt64 theFunctionId;

        Locations locations;
        /// <summary>
        /// Ссылка на таблицу с индексом
        /// </summary>
        Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates index;
        /// <summary>
        /// Маркер данного объекта в индексе
        /// </summary>
        internal Marker_Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates marker;
        

        /// <summary>
        /// Создавать экземпляры извне запрещено
        /// Следом обязательно вызвать Initialize.
        /// Применять только при автозагрузке из базы
        /// </summary>
        /// <param name="locations"></param>
        /// <param name="index"></param>
        internal Location_Impl(Locations locations, Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates index)
        {
            this.locations = locations;
            this.index = index;

            if (index != null)
                marker = index.GetMarker();
        }

        internal void Set(UInt64 objectID, UInt64 fileID, UInt64 offset, ulong line, ulong column)
        {
            if (line >= uint.MaxValue)
                IA.Monitor.Log.Warning("Storage", "Попытка установить номер строки, больше или равный -1");
//            throw new Store.Const.StorageException("Попытка установить номер строки, больше или равный -1");
            if (column >= uint.MaxValue)
                IA.Monitor.Log.Warning("Storage", "Попытка установить номер столбца, больше или равный -1");
//            throw new Store.Const.StorageException("Попытка установить номер столбца, больше или равный -1");

            this.ID = objectID;
            this.theFileID = fileID;
            theOffset = offset;
            theLine = line;
            theColumn = column;
            theFunctionId = Store.Const.CONSTS.WrongID;
        }

        internal void Set(ulong objectID, ulong fileID, ulong offset, ulong line, ulong column, ulong function)
        {
            Set(objectID, fileID, offset, line, column);
            theFunctionId = function;
        }

        override protected void Load(IBufferReader reader)
        {
            theFileID = reader.GetUInt64();
            theOffset = reader.GetUInt64();
            theLine = reader.GetUInt64();
            theColumn = reader.GetUInt64();
            theFunctionId = reader.GetUInt64();

            if(marker != null)
                marker.InformLoaded(buffer =>
                                    {
                                        buffer.AddTransformed(theFileID);
                                        buffer.AddTransformed(theOffset);
                                    });
        }

        protected override void SaveToBuffer(IBufferWriter buffer)
        {
            buffer.Add(theFileID);
            buffer.Add(theOffset);
            buffer.Add(theLine);
            buffer.Add(theColumn);
            buffer.Add(theFunctionId);
        }

        protected override void SaveIndexes()
        {
            if(index != null)
                index.UpdateIndex(marker, buffer =>
                                                {
                                                    buffer.AddTransformed(theFileID);
                                                    buffer.AddTransformed(theOffset);
                                                },
                                                buffer => buffer.Add(ID));
        }

        /// <summary>
        /// Получить ссылку на файл
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public IFile GetFile()
        {
            return locations.storage.files.GetFile(theFileID);
        }

        /// <summary>
        /// Полуйсить идентификатор файла
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public UInt64 GetFileID()
        {
            return theFileID;
        }

        /// <summary>
        /// Получить смещение в файле
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public UInt64 GetOffset()
        {
            return theOffset;
        }

        /// <summary>
        /// Получить номер строки в файле
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public UInt64 GetLine()
        {
            return theLine;
        }

        /// <summary>
        /// Получить номер столбца в файле
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public UInt64 GetColumn()
        {
            return theColumn;
        }

        /// <summary>
        /// Возвращает идентификатор функции, в которой находится позиция.
        /// Если позиция нахождится вне функций, то возвращается
        /// Store.Const.CONSTS.WrongID
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public UInt64 GetFunctionId()
        {
            return theFunctionId;
        }

        public override bool Equals(object obj)
        {
            Location_Impl impl = obj as Location_Impl;
            if (impl == null)
                return false;

            return impl.theFileID == theFileID && impl.theLine == theLine && impl.theOffset == theOffset && impl.theColumn == theColumn;
        }

        public override string ToString()
        {
            string res = "в файле \"" + GetFile().FileNameForReports + "\"";

            if (theLine > 0 && theColumn > 0)
                res += " на строке " + theLine + " в столбце " + theColumn;
                        
            if (theFunctionId != Store.Const.CONSTS.WrongID)
            {
                IFunction func = locations.storage.functions.GetFunction(theFunctionId);
                string funcName = func.FullName;
                Location funcDefinition = func.Definition();
                //проверка, вероятно, должна выявлять "Глобальное окружение" исходя из предположения, что другой функции, у которой совпадёт имя файла и полное имя функции быть не должно
                if(funcDefinition != null && funcDefinition.GetFile().FullFileName_Original == funcName)
                    res += " в коде вне функций";
                else
                    res += " в функции " + func.FullNameForReports;
            } 
            //TODO: возможно есть смысл добавить ветку
            //else
            //{
            //    res += " в коде вне функций";
            //}

            return res;
        }

    }
}
