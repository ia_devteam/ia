﻿using System;

namespace Store
{
    /// <summary>
    /// Раздел Хранилища "Операции". 
    /// Подробнее см. <a href="0b5c8eec-7ee6-4b71-b292-1cc25a2865b4.htm">Раздел Хранилища "Операции"</a>.
    /// </summary>
    public class Operations
    {
        const string TableName_Leader = "OperationsLeader";
        const string TableName_OperationCalls = "OperationsCalls";
        const string TableName_ImplementsInStatement = "TableName_ImplementsInStatement";

        /// <summary>
        /// Хранение идентификатора операторов, в теле которой находится операции, идентификатор которого является ключем.
        /// </summary>
        internal Store.BaseDatatypes.Table_KeyId_ValueSaver_Nodublicate TableImplementsInStatement;


        //Основные таблицы

        internal Store.BaseDatatypes.TableLeader<Operation_Impl> TableLeader;

        /// <summary>
        /// Хранение вызовов функций
        /// </summary>
        internal Store.BaseDatatypes.Table_KeyID_ValueSaver_Dublicate TableOperationCalls;

        internal Storage storage;

        internal Operations(Storage storage)
        {
            this.storage = storage;

            TableLeader = new BaseDatatypes.TableLeader<Operation_Impl>(storage, TableName_Leader, (st) => new Operation_Impl(this));
            TableOperationCalls = new BaseDatatypes.Table_KeyID_ValueSaver_Dublicate(storage, TableName_OperationCalls);
            TableImplementsInStatement = new BaseDatatypes.Table_KeyId_ValueSaver_Nodublicate(storage, TableName_ImplementsInStatement);
        }

        /// <summary>
        /// Получить операциюб по её идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IOperation GetOperation(UInt64 id)
        {
            return TableLeader.Get(id);
        }

        /// <summary>
        /// Добавить новую операцию
        /// </summary>
        /// <returns></returns>
        public IOperation AddOperation()
        {
            return TableLeader.Add();
        }
    }
}
