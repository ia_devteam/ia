﻿using System;
using System.Collections.Generic;
using System.Linq;
using Store.Table;

namespace Store
{
    /// <summary>
    /// Основной объект раздела Operations Хранилища.
    /// Операция абстрагирует внутреннее устройство оператора. 
    /// </summary>
    public interface IOperation : Store.IDataObjectLeader
    {
        /// <summary>
        /// Последовательно выдает функции в последовательности, в которой они вызываются при исполнении операции. Если на очередном шаге выдается более одной функции, то это означает, что анализатор не может 
        /// определить, какая из функций будет вызываться и считает, что будет вызываться одна из перечисленных. Или некоторые из перечисленных в любой последовательсности и в любом количестве
        /// </summary>
        /// <remarks>
        /// Подробнее см. <a href="0b5c8eec-7ee6-4b71-b292-1cc25a2865b4.htm">Вызовы функций</a>.
        /// </remarks>
        /// <returns>Последовательное перечисление вызываемых функций. Вызывается только одна из массива, выдаваемого на очередном шаге.</returns>
        IEnumerable<IFunction []> SequentiallyCallsFunctions();

        /// <summary>
        /// Добавить вызов функции в последовательность вызовов функций внутри операции.
        /// </summary>
        /// <remarks>
        /// Подробнее см. <a href="0b5c8eec-7ee6-4b71-b292-1cc25a2865b4.htm">Вызовы функций</a>.
        /// </remarks>
        /// <param name="func">Вызываемая функция. В некоторых случаях невозможно определить конкретно вызываемую функцию, но можно определить, что это одна из нескольких функций. В этом случае добавляется массив из этих нескольких функций.</param>
        void AddFunctionCallInSequenceOrder(IFunction []func);

        /// <summary>
        /// Добавить вызов функции в последовательность вызовов функций внутри операции.
        /// </summary>
        /// <remarks>
        /// Подробнее см. <a href="0b5c8eec-7ee6-4b71-b292-1cc25a2865b4.htm">Вызовы функций</a>.
        /// </remarks>
        /// <param name="func">Вызываемая функция. В некоторых случаях невозможно определить конкретно вызываемую функцию, но можно определить, что это одна из нескольких функций. В этом случае добавляется массив из этих нескольких функций.</param>
        /// <param name="vars">Вызываемая по указателю переменная. В некоторых случаях невозможно определить конкретно вызываемую функцию, но можно определить, что это одна из нескольких функций. В этом случае добавляется массив из этих нескольких функций.</param>
        void AddFunctionCallInSequenceOrder(IFunction[] func, Variable[] vars);


        /// <summary>
        /// Добавить вызов функции по указателю в последовательность вызовов функций внутри операции.
        /// </summary>
        /// <remarks>
        /// Подробнее см. <a href="0b5c8eec-7ee6-4b71-b292-1cc25a2865b4.htm">Вызовы функций</a>.
        /// </remarks>
        /// <param name="var">Переменная, в которой хранится указатель на функцию. </param>
        void AddFunctionCallInSequenceOrder(Variable var);

        /// <summary>
        /// Удаляет операцию из Хранилища
        /// Реализация данного метода плохо отлажена. Необходимо тщательное тестирование использующего её кода.
        /// </summary>
        void RemoveMe();
    }

    internal struct CallImplFuncVar
    {
        IFunction[] funcs;
        Variable[] vars;

        internal CallImplFuncVar(IFunction[] funcs, Variable[] vars)
        {
            this.funcs = funcs;
            this.vars = vars;
        }

        internal CallImplFuncVar(IBufferReader reader, Storage storage)
        {
            Functions functions = storage.functions;
            Variables variables = storage.variables;


            Int32 size = reader.GetInt32();
            funcs = new IFunction[size];
            for (int i = 0; i < size; i++)
                funcs[i] = functions.GetFunction(reader.GetUInt64());

            size = reader.GetInt32();
            vars = new Variable[size];
            for (int i = 0; i < size; i++)
                vars[i] = variables.GetVariable(reader.GetUInt64());
        }

        internal void Save(IBufferWriter writer)
        {
            if (funcs != null)
            {
                writer.Add((Int32)funcs.Length);
                foreach (IFunction func in funcs)
                    writer.Add(func.Id);
            }
            else
                writer.Add((Int32)0);

            if (vars != null)
            {
                writer.Add((Int32)vars.Length);
                foreach (Variable var in vars)
                    writer.Add(var.Id);
            }
            else
                writer.Add((Int32) 0);
        }

        internal IFunction[] GetFunctions()
        {
            return funcs;
        }

        internal Variable[] GetVariables()
        {
            return vars;
        }
    }



    internal class Operation_Impl : Store.BaseDatatypes.DataObjectLeader, IOperation
    {
        Operations operations;

        Statement_Impl statement = null;

        Store.BaseDatatypes.RemovedCheck check = new BaseDatatypes.RemovedCheck();

        internal Operation_Impl(Operations operations)
        {
            this.operations = operations;
        }

        #region Operation Members

        public IEnumerable<IFunction []> SequentiallyCallsFunctions()
        {
            check.Check();

            foreach (IBufferReader reader in operations.TableOperationCalls.GetValuesByID(theID))
            {
                CallImplFuncVar call = new CallImplFuncVar(reader, operations.storage);

                IFunction[] funcs = call.GetFunctions();
                if (funcs.Length != 0)
                    yield return funcs;

                //Теперь переменные. Для этого надо раскрыть, на что указывает каждая переменная
                Variable[] vars = call.GetVariables();
                Functions functions = operations.storage.functions;
                funcs = vars.SelectMany(v => v.Enumerate_PointsToFunctions()).Select(p => functions.GetFunction(p.function)).ToArray();
                if (funcs.Length != 0)
                    yield return funcs;
            }
            yield break;
        }

        public void AddFunctionCallInSequenceOrder(IFunction[] arrayFunction, Variable[] arrayVariable)
        {
            check.Check();

            CallImplFuncVar impl = new CallImplFuncVar(arrayFunction, arrayVariable);
            operations.TableOperationCalls.AddAtId(theID, buffer => { impl.Save(buffer); });

            IStatement st = ImplementsInStatement;
            if (st != null)
                (st as Statement_Impl).SetupMyOperationsFunctionCalles(this);
        }

        public void AddFunctionCallInSequenceOrder(IFunction[] arrayFunction)
        {
            AddFunctionCallInSequenceOrder(arrayFunction, null);
        }


        public void AddFunctionCallInSequenceOrder(Variable var)
        {
            AddFunctionCallInSequenceOrder(null, new Variable[] { var });
        }

        public void RemoveMe()
        {
            check.Check();

            operations.TableLeader.Remove(theID);
            operations.TableOperationCalls.RemoveAllForKey(theID);
            check.SetRemoved();
        }
        #endregion

        internal IStatement ImplementsInStatement
        {
            get
            {
                UInt64 stId = operations.TableImplementsInStatement.GetUInt64ValueByID(theID);
                if (stId == Store.Const.CONSTS.WrongID)
                    return null;
                else
                    return operations.storage.statements.GetStatement(stId);
            }
            set
            {
                if (value == null)
                    operations.TableImplementsInStatement.PutUInt64Value(theID, Store.Const.CONSTS.WrongID);
                else
                    operations.TableImplementsInStatement.PutUInt64Value(theID, value.Id);
            }
        }
    }
}
