﻿using System;
using System.Collections.Generic;
using Store.BaseDatatypes;
using Store.Table;
using System.Linq;

namespace Store
{
    /// <summary>
    /// Раздел Хранилища Classes содержит информацию о классах в анализируемых исходных текстах
    /// </summary>
    public sealed class Classes
    {
        const string TableName_Classes = "Classes";
        const string TableName_ClassesNames = "ClassesNames";
        const string TableName_ClassesNamesIndex = "iClassesNames";
        const string TabelName_ClassContainsFunction = "Classes_ClassContainsFunction";
        const string TabelName_Parrents = "Classes_Parrents";
        const string TabelName_Chields = "Classes_Chields";
        const string TabelName_Fields = "Classes_Fields";
        const string TabelName_InnerClasses = "Classes_InnerClasses";
        const string TabelName_UpperClasses = "iClasses_UpperClass";
        const string TableName_FunctionInClass = "iFunctionInClass";
        const string TableName_VisibleMethods = "iVisibleMethods";

        internal Storage storage;

        //Основные таблицы
        TableLeader<Class_Impl> tableLeader;
        /// <summary>
        /// Хранит имя функции
        /// </summary>
        internal TableObjectNamingWithIndexAuto<Class> tableNaming;
        /// <summary>
        /// Ключ - идентификатор класса.
        /// Значение - идентификатор вызываемой функции.
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableClassContainsFunction;
        
        /// <summary>
        /// Ключ - идентификатор потомка
        /// Значение - идентификатор родителя
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableParrents;

        /// <summary>
        /// Ключ - идентификатор класса
        /// Значение - идентификатор поля
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableFields;

        /// <summary>
        /// Ключ - идентификатор класса, в который вложеннны классы
        /// Значение - идентификатор вложенного класса
        /// </summary>
        internal Table_KeyId_ValueId_Dublicates tableInnerClasses;


        //Индексные таблицы

        /// <summary>
        /// Ключ - идентификатор функции
        /// Значение - идентификатор класса, чьим методом является функция
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueId_NoDuplicate tableFunctionInClass;

        /// <summary>
        /// Ключ - идентификатор вложенного класса
        /// Значение  - идентификатор класса, в который вложеннны классы
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueId_NoDuplicate tableUpperClass;

        /// <summary>
        /// Ключ - идентификатор родителя
        /// Значение - идентификатор потомка
        /// </summary>
        internal Index_DelayCreation_KeyId_ValueId_Duplicates tableChields;


        /// <summary>
        /// Индекс содержит для каждого класса перечень всех видимых в нём методов. При этом модификаторы вилимости не учитываются: на самом деле некоторые находящиеся в индексе методы видны не будут
        /// Обновление индекса происходит в двух случаях:
        /// 1. При указании для класса базового класса.
        /// 2. При Указании для метода его имени.
        /// ключ - пара [идентификатор класса, короткое имя метода]
        /// значение - идентификатор метода
        /// </summary>
        internal Index_DelayCreation_KeyBuffer_ValueId_Duplicates tableVisibleMethods;

        /// <summary>
        /// Создавать экземпляры Classes самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        internal Classes(Storage storage)
        {
            this.storage = storage;

            tableLeader = new TableLeader<Class_Impl>(storage, TableName_Classes, (kind) => new Class_Impl(this));
            tableNaming = new TableObjectNamingWithIndexAuto<Class>(storage, TableName_ClassesNames, TableName_ClassesNamesIndex, EnumerateItemsIndexClassByName);
            tableClassContainsFunction = new Table_KeyId_ValueId_Dublicates(storage, TabelName_ClassContainsFunction);
            tableFields = new Table_KeyId_ValueId_Dublicates(storage, TabelName_Fields);
            tableParrents = new Table_KeyId_ValueId_Dublicates(storage, TabelName_Parrents);
            tableChields = new Index_DelayCreation_KeyId_ValueId_Duplicates(storage, TabelName_Chields, EnumerateRebuildChields);
            tableInnerClasses = new Table_KeyId_ValueId_Dublicates(storage, TabelName_InnerClasses);
            tableUpperClass = new Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, TabelName_UpperClasses, RebuildUpperClasses);
            tableFunctionInClass = new Index_DelayCreation_KeyId_ValueId_NoDuplicate(storage, TableName_FunctionInClass, RebuildFunctionInClass);
            tableVisibleMethods = new Index_DelayCreation_KeyBuffer_ValueId_Duplicates(storage, TableName_VisibleMethods, RebuildVisibleMethods);
        }

        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult> EnumerateItemsIndexClassByName(IBufferWriter key, IBufferWriter value)
        {
            foreach (Class_Impl clazz in tableLeader.EnumerateEveryone())
            {
                key.Add(clazz.Name);
                value.Add(clazz.Id);

                yield return new Index_DelayCreation_KeyBuffer_ValueBuffer_Duplicates.EnumerateRebuildResult(key, value, null);
            }
        }

        #region Поддержание индекса tableVisibleMethods

        private IEnumerable<Index_DelayCreation_KeyBuffer_ValueId_Duplicates.EnumerateRebuildResult> RebuildVisibleMethods(IBufferWriter key)
        {
            Stack<Class> stack = new Stack<Class>();
            stack.Push(null);
            List<Class> walked = new List<Class>();
            foreach (Class_Impl clazz in tableLeader.EnumerateEveryone())
            {
                stack.Push(clazz);
                do
                {
                    //Берем очередного предка
                    Class clazzParent = stack.Pop();

                    //Запрещаем дважды добавлять методы одного класса
                    if (walked.Contains(clazzParent))
                        continue;
                    walked.Add(clazzParent);

                    //Добавляем поля предка в индекс
                    foreach (IMethod method in clazzParent.EnumerateMethods())
                        if (method.Name != "")
                        {
                            key.Add(clazz.Id);
                            key.Add(method.Name);
                            yield return new Index_DelayCreation_KeyBuffer_ValueId_Duplicates.EnumerateRebuildResult(key, method.Id);
                        }

                    //Добавляем предков текущего предка в стек
                    foreach (Class parent in clazzParent.EnumerateParents())
                        stack.Push(parent);
                }
                while (stack.Peek() != null);

                walked.Clear();
            }
        }

        /// <summary>
        /// Срабатывает, когда классу child назначается базовый класс parent.
        /// Должен вызываться до того, как в базу данных записана связь между parent и child.
        /// </summary>
        /// <param name="child">Потомок</param>
        /// <param name="parent">Предок</param>
        internal void UpdateVisibleMethodsByNewParent(Class child, Class parent)
        {
            //Если индекс не построен, то тратить время на перечисление элементов бессмысленно
            if (!tableVisibleMethods.isIndexActivated())
                return;

            //Получаем перечень методов для добавления
            List<IMethod> AdditionalMethods = new List<IMethod>();
            UpdateVisibleMethodsByNewParent_EnumerateAccesibleMethods(parent, AdditionalMethods, new List<Class>());

            //Рекурсивно добавляем методы в индекс
            UpdateVisibleMethodsByNewParent_AddMethodsForTree(child, AdditionalMethods);
        }

        private void UpdateVisibleMethodsByNewParent_EnumerateAccesibleMethods(Class clazz, List<IMethod> AdditionalMethods, List<Class> list)
        {
            if (list.Contains(clazz))
                return;
            list.Add(clazz);

            AdditionalMethods.AddRange(clazz.EnumerateMethods());

            foreach (Class parent in clazz.EnumerateParents())
                UpdateVisibleMethodsByNewParent_EnumerateAccesibleMethods(parent, AdditionalMethods, list);
        }


        /// <summary>
        /// Добавляем методы из AdditionalMethods в clazz и все его потомки.
        /// </summary>
        /// <param name="clazz"></param>
        /// <param name="AdditionalMethods"></param>
        private void UpdateVisibleMethodsByNewParent_AddMethodsForTree(Class clazz, List<IMethod> AdditionalMethods)
        {
            //Добавляем методы в текущий класс
            foreach (IMethod method in AdditionalMethods)
                if (!tableVisibleMethods.FindValuesByKey(buf=>{buf.Add(clazz.Id); buf.Add(method.Name);}).Contains(method.Id))
                    tableVisibleMethods.UpdateIndex(
                                    buf =>  { 
                                                buf.Add(clazz.Id); 
                                                buf.Add(method.Name); 
                                            }, 
                                    method.Id);

            //Обходим всех потомков
            foreach (Class clazzChild in clazz.EnumerateChilds())
                UpdateVisibleMethodsByNewParent_AddMethodsForTree(clazzChild, AdditionalMethods);
        }

        /// <summary>
        /// Срабатывает, когда методу класса устанавливается имя.
        /// </summary>
        /// <param name="method"></param>
        internal void UpdateVisibleMethodsByNewMethodName(IMethod method)
        {
            //Если индекс не построен, то тратить время на перечисление элементов бессмысленно
            if (!tableVisibleMethods.isIndexActivated())
                return;
            
            //Добавляем рекурсивно
            Class clazz = method.methodOfClass;
            UpdateVisibleMethodsByNewMethodName_recursive(clazz, method);
        }

        private void UpdateVisibleMethodsByNewMethodName_recursive(Class clazz, IMethod method)
        {
            //Добавляем текущий
            tableVisibleMethods.UpdateIndex(
                                    buf =>
                                    {
                                        buf.Add(clazz.Id);
                                        buf.Add(method.Name);
                                    },
                                    method.Id);
            
            //Обходим потомков
            foreach (Class clazzChield in clazz.EnumerateChilds())
                UpdateVisibleMethodsByNewMethodName_recursive(clazzChield, method);
        }


        #endregion

        private IEnumerable<Index_DelayCreation_KeyId_ValueId_Duplicates.EnumerateRebuildResult> EnumerateRebuildChields()
        {
            foreach (KeyValuePair<UInt64, UInt64> pair in tableParrents.EnumerateEveryone())
                yield return new Index_DelayCreation_KeyId_ValueId_Duplicates.EnumerateRebuildResult(pair.Value, pair.Key, null);
        }

        private IEnumerable<KeyValuePair<UInt64, UInt64>> RebuildUpperClasses()
        {
            foreach (KeyValuePair<UInt64, UInt64> pair in tableInnerClasses.EnumerateEveryone())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Value, pair.Key);
        }

        private IEnumerable<KeyValuePair<UInt64, UInt64>> RebuildFunctionInClass()
        {
            foreach (KeyValuePair<UInt64, UInt64> pair in tableClassContainsFunction.EnumerateEveryone())
                yield return new KeyValuePair<UInt64, UInt64>(pair.Value, pair.Key);
        }

        /// <summary>
        /// Получить класс по его идентификатору
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public Class GetClass(UInt64 objectId)
        {
            return tableLeader.Get(objectId);
        }

        /// <summary>
        /// Получить класс по его имени.
        /// <remarks>
        /// В анализируемых исходных текстах может присутствовать более одного класса с данным именем. GetClass возвращает их все.
        /// </remarks>
        /// </summary>
        /// <param name="name">Имя класса, не содержащее маркеров области видимости. Для получения класса по его полному имени используется <see cref="Store.Classes.GetClass(System.String[])"/>.</param>
        /// <returns></returns>
        public Class[] GetClass(string name)
        {
            return tableNaming.GetEntity(name, GetClass);
        }

        /// <summary>
        /// Получить класс по его полному имени
        /// </summary>
        /// <remarks>
        /// В анализируемых исходных текстах может присутствовать более одного класса с данным именем. GetClass возвращает их все.
        /// </remarks>        
        /// <param name="name">Полное имя класса. Каждый элкемент массива содержит отдельную часть области видимости. 
        /// Например, для класса <c>Name1.Name2.Name3</c> должен быть передан массив <c>new string[]{"Name1", "Name2", "Name3"}</c></param>
        /// <returns></returns>
        public Class[] GetClass(string[] name)
        {
            return tableNaming.GetEntity(name, GetClass);
        }

        /// <summary>
        /// Найти все классы с данным частичным именем (суффиксом). Например, по имени C1.C2 будет найден класс C0.C1.C2.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Class[] FindClass(string[] name)
        {
            return tableNaming.FindEntityBySubname(name, GetClass);
        }

        #region Find and FindOrCreate

        /// <summary>
        /// Найти класс с указанным полным именем
        /// </summary>
        /// <param name="module">Модуль, в котором реализован класс</param>
        /// <param name="classNamespace">Namespace класса. Например, <c>n1.n2</c> должен быть подан на вход как <c>new string[] {"n1", "n2"}</c></param>
        /// <param name="topClasses">Класс, в который вложен данный класс. Заполняется аналогично classNamespace</param>
        /// <param name="className">Имя класса (короткое)</param>
        /// <returns></returns>
        public Class Find(Module module, string[] classNamespace, string[] topClasses, string className)
        {
            Namespace ns = storage.namespaces.Find(classNamespace);

            if (ns == null)
                return null;
            else
                return Find(module, ns, topClasses, className);
        }

        /// <summary>
        /// Найти класс с указанным полным именем
        /// </summary>
        /// <param name="module">Модуль, в котором реализован класс</param>
        /// <param name="classNamespace">Namespace класса</param>
        /// <param name="topClasses">Класс, в который вложен данный класс. Заполняется аналогично classNamespace</param>
        /// <param name="className">Имя класса (короткое)</param>
        /// <returns></returns>
        public Class Find(Module module, Namespace classNamespace, string[] topClasses, string className)
        {
            //Ищем верхний класс для исхомого класса
            Class top;
            if (topClasses!= null && topClasses.Length > 0)
            {
                top = Find(module, classNamespace, topClasses[0]);
                for (int i = 1; i < topClasses.Length; i++)
                {
                    if (top == null)
                        return null;

                    top = top.GetInnerClass(topClasses[i]);
                }

                if (top == null)
                    return null;

                return top.GetInnerClass(className);
            }
            else
                return Find(module, classNamespace, className);
        }

        private Class Find(Module module, Namespace classNamespace, string className)
        {
            Class[] classes = (from clazz in classNamespace.FindClass(className) where clazz.ImplementedInModule == module select clazz).ToArray();

            if (classes.Length == 1)
                return classes[0];
            else if (classes.Length == 0)
                return null;
            else throw new Store.Const.StorageException("В модуле реализовано более одного класса с именем " + className + " и пространством имён " + classNamespace.FullName);
        }


        /// <summary>
        /// Найти класс с указанным полным именем. Если класс не существует, он будет создан. Также будут созданы все объекты, входящие в полное имя класса.
        /// </summary>
        /// <param name="module">Модуль, в котором реализован класс</param>
        /// <param name="classNamespace">Namespace класса. Например, <c>n1.n2</c> должен быть подан на вход как <c>new string[] {"n1", "n2"}</c></param>
        /// <param name="topClasses">Класс, в который вложен данный класс. Заполняется аналогично classNamespace</param>
        /// <param name="className">Имя класса (короткое)</param>
        /// <returns></returns>
        public Class FindOrCreate(Module module, string[] classNamespace, string[] topClasses, string className)
        {
            Namespace ns = storage.namespaces.FindOrCreate(classNamespace);

            return FindOrCreate(module, ns, topClasses, className);
        }

        /// <summary>
        /// Найти класс с указанным полным именем
        /// </summary>
        /// <param name="module">Модуль, в котором реализован класс</param>
        /// <param name="classNamespace">Namespace класса</param>
        /// <param name="topClasses">Класс, в который вложен данный класс. Заполняется аналогично classNamespace</param>
        /// <param name="className">Имя класса (короткое)</param>
        /// <returns></returns>
        public Class FindOrCreate(Module module, Namespace classNamespace, string[] topClasses, string className)
        {
            //Ищем верхний класс для исхомого класса
            Class top;
            if (topClasses != null && topClasses.Length > 0)
            {
                top = FindOrCreate(module, classNamespace, topClasses[0]);
                for (int i = 1; i < topClasses.Length; i++)
                    top = GetOrGenerateInnerClass(topClasses[i], top, module);

                return GetOrGenerateInnerClass(className, top, module);
            }
            else
                return FindOrCreate(module, classNamespace, className);
        }

        private Class GetOrGenerateInnerClass(string topClasses, Class top, Module module)
        {
            Class top1;
            top1 = top.GetInnerClass(topClasses);

            if (top1 == null)
            {
                top1 = AddClass();
                top1.Name = topClasses;
                top.AddInnerClass(top1);
                module.AddImplementedClass(top1);
            }

            return top1;
        }

        private Class FindOrCreate(Module module, Namespace classNamespace, string className)
        {
            Class clazz = Find(module, classNamespace, className);
            if(clazz == null)
            {
                clazz = AddClass();
                clazz.Name = className;
                classNamespace.AddClassToNamespace(clazz);
                module.AddImplementedClass(clazz);
            }
            return clazz;
        }

        #endregion

        /// <summary>
        /// Перечисляет все классы, имеющиеся в Хранилище.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Class> EnumerateClasses()
        {
            return tableLeader.EnumerateEveryone();
        }

        /// <summary>
        /// Добавить новый класс в Хранилище. После добавления класса необходимо заполнить его поля.
        /// <remarks>
        /// Пример использования:
        /// <code>
        /// Class class1 = storage.classes.AddClass();
        /// class1.Name = "name";
        /// </code>
        /// </remarks>
        /// </summary>
        /// <returns></returns>
        public Class AddClass()
        {
            return tableLeader.Add();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder"></param>
        public void Dump(string folder)
        {
            using (System.IO.StreamWriter wr = new System.IO.StreamWriter(System.IO.Path.Combine(folder, "classes.log")))
            {
                wr.WriteLine("Дамп классов");

                foreach (Class clazz in EnumerateClasses())
                    wr.WriteLine(clazz.Id.ToString(System.Globalization.CultureInfo.InvariantCulture) + "\t" + clazz.Name);
            }
        }

    }
}
