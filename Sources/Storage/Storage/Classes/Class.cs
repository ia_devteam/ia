﻿using System;
using System.Collections.Generic;
using Store.BaseDatatypes;


namespace Store
{
    /// <summary>
    /// Описания класса анализируемых исходных текстов. Является элементом раздела <see cref="Store.Classes"/> Хранилища.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Class"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1715:IdentifiersShouldHaveCorrectPrefix", MessageId = "I")]
    public interface Class
    {
        /// <summary>
        /// Идентификатор класса (его имя)
        /// Имя можно установить только один раз, после чего модификация становится невозможна.
        /// </summary>
        string Name
        { get; set; }

        /// <summary>
        /// Полное имя файла, включая его область видимости. Например <c>Name1.Name2.Name3</c>. 
        /// Формат имени не зависит от того, на каком языке программирования реализованы анализируемые исходные тексты (в которых реализован данный класс).
        /// </summary>
        string FullName { get; }

        /// <summary>
        /// Уникальный идентификатор класса в Хранилище.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID")]
        UInt64 Id
        {
            get;
        }
        
        /// <summary>
        /// Модуль, в котором реализован данный класс. Значение данного метода задаётся при вызове метода <see cref="Store.Module.AddImplementedClass(Store.Class)"/>
        /// </summary>
        Module ImplementedInModule { get; }

        /// <summary>
        /// Идентификатор модуля, в котором реализован данный класс. Значение данного метода задаётся при вызове метода <see cref="Store.Module.AddImplementedClass(Store.Class)"/>
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "ID")]
        UInt64 ImplementedInModuleID { get; }

        /// <summary>
        /// Перечисляет методы класса.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IMethod> EnumerateMethods();

        /// <summary>
        /// Перечисляет идентификаторы методов класса
        /// </summary>
        /// <returns></returns>
        IEnumerable<UInt64> EnumerateMethodIds();

        /// <summary>
        /// Добавить метод в класс
        /// </summary>
        /// <param name="isOverrideBase">        
        /// true, если данный метод является переопределением одноимённого метода базового класса.
        /// Например в C#
        /// <code>
        /// class A
        /// {
        ///  void f() {}
        /// }
        /// class B : A
        /// {
        ///  override void f() {}
        /// }
        /// В этом случае для метода B.f() флаг isOverrideBase должен быть установлен true.
        /// </code></param>
        IMethod AddMethod(bool isOverrideBase);

        /// <summary>
        /// Перечень классов, для которых данный является потомком.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Class> EnumerateParents();

        /// <summary>
        /// Перечень потомков данного класса.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Class> EnumerateChilds();

        /// <summary>
        /// Задает родителя данного класса.
        /// </summary>
        /// <param name="clazz"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "clazz")]
        void AddParent(Class clazz);

        /// <summary>
        /// Перечисляет все поля данного класса
        /// </summary>
        /// <returns></returns>
        IEnumerable<Field> EnumerateFields();

        /// <summary>
        /// Перечисляет идентификаторы всех полей класса
        /// </summary>
        /// <returns></returns>
        IEnumerable<UInt64> EnumerateFieldIds();

        /// <summary>
        /// Добавить поле данного класса.
        /// </summary>
        /// <remarks>
        /// Для каждого класса данный метод надо вызывать в той последовательности, в которой должны работать инициализаторы данного класса
        /// </remarks>
        /// <param name="isStatic">true, если поле является статическим</param>
        /// <returns></returns>
        Field AddField(bool isStatic);

        /// <summary>
        /// Добавить вложенный класс
        /// </summary>
        /// <param name="clazz"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "clazz")]
        void AddInnerClass(Class clazz);
        
        /// <summary>
        /// Перечислить вложенные классы
        /// </summary>
        /// <returns></returns>
        IEnumerable<Class> EnumerateInnerClasses();

        /// <summary>
        /// Найти вложенный класс с указанным именем
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        Class GetInnerClass(string className);

        /// <summary>
        /// Перечисляет все методы с данным именем для данного класса и его базовых классов.
        /// Для реализации используется индекс, построение и поддержание которого требует значительных временных затрат. Выясиление индекса производится при первом обращениии к методу.
        /// </summary>
        /// <param name="Name"> Короткое имя метода
        /// </param>   
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Name")]
        IEnumerable<IMethod> EnumerateAccessibleMethods(string Name);
    }

    internal class Class_Impl : DataObjectLeaderWithName, Class
    {
        Classes theClasses;

        /// <summary>
        /// Поле, добавленное в класс последним. Используется для установления порядка вызова инициализаторов.
        /// </summary>
        Field_Impl lastAddedField;

        /// <summary>
        /// Конструктор используется при перечислении классов
        /// </summary>
        /// <param name="classes"></param>
        internal Class_Impl(Classes classes) : base(classes.tableNaming)
        {
            this.theClasses = classes;
        }

        #region Class Members

        string Class.Name
        {
            get
            {
                return base.Name;
            }
            set
            {
                theClasses.tableNaming.SetName(ref naming, theID, value);
            }
        }

        internal override string NameForReports
        {
            get { return Name; }
        }

        internal override string NameForFileName
        {
            get
            {
                string ret = Name.Replace("<", "#LessThan#").Replace(">", "#GraterThan#").Replace("+", "#Plus#").Replace("-", "#Minus#").Replace("\\", "#BackSlash#").Replace("/", "#Slash#").Replace("\"", "#DoubleQuote#").Replace("'", "#SingleQuote#").Replace("*", "#Multiply#").Replace("=", "#Equal#").Replace(":", "#Colon#").Replace("|", "#Line#");
                return ret;
            }
        }

        public UInt64 ImplementedInModuleID
        {
            get
            {
                Modules modules = theClasses.storage.modules;
                return modules.TableClassImplementedInModule.GetValueByKey(theID);
            }
        }

        public Module ImplementedInModule
        {
            get 
            { 
                Modules modules = theClasses.storage.modules;
                UInt64 moduleId = ImplementedInModuleID;
                if (moduleId == Store.Const.CONSTS.WrongID)
                    return null;
                else
                    return modules.GetModule(moduleId);
            }
        }


        public IEnumerable<IMethod> EnumerateMethods()
        {
            foreach (UInt64 id in theClasses.tableClassContainsFunction.GetValueByKey(theID))
                yield return (IMethod) theClasses.storage.functions.GetFunction(id);
        }

        public IEnumerable<UInt64> EnumerateMethodIds()
        {
            return theClasses.tableClassContainsFunction.GetValueByKey(theID);
        }

        public IMethod AddMethod(bool isOverrideBase)
        {
            IMethod method = theClasses.storage.functions.AddMethod();

            (method as Method_Impl).isOverrideBase = isOverrideBase;

            //Устанавливаем принадлежность функции классу
            theClasses.tableClassContainsFunction.PutWithoutRepetition(theID, method.Id);
            theClasses.tableFunctionInClass.Put(method.Id, theID);

            //Устанавливаем принадлежность метода модулю
            Module_Impl.SetMethodModuleWithCheck(theClasses.storage.modules, ImplementedInModuleID, method);

            return method;
        }

        public IEnumerable<Class> EnumerateParents()
        {
            foreach (UInt64 parrent in theClasses.tableParrents.GetValueByKey(theID))
                yield return theClasses.GetClass(parrent);
        }

        public IEnumerable<Class> EnumerateChilds()
        {
            foreach (UInt64 chield in theClasses.tableChields.FindValuesByKey(theID))
                yield return theClasses.GetClass(chield);
        }

        public void AddParent(Class clazz)
        {
            if (clazz != null)
            {
                //Обновление индекса Classes.tableVisibleMethods
                theClasses.UpdateVisibleMethodsByNewParent(this, clazz);

                theClasses.tableParrents.PutWithoutRepetition(theID, clazz.Id);
                theClasses.tableChields.UpdateIndex(null, clazz.Id, theID);
            }
        }

        public IEnumerable<Field> EnumerateFields()
        {
            foreach (UInt64 id in theClasses.tableFields.GetValueByKey(theID))
                yield return (Field) theClasses.storage.variables.GetVariable(id);
        }

        public IEnumerable<UInt64> EnumerateFieldIds()
        {
            return theClasses.tableFields.GetValueByKey(theID);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        public Field AddField(bool isStatic)
        {
            Field_Impl field = (Field_Impl) theClasses.storage.variables.AddField();
            field.OwnerClass = this;

            field.isStatic = isStatic;

            if (!isStatic)
            {
                //Устанавливаем последовательность вызова инициализаторов
                if (lastAddedField == null)
                    foreach (Field fld in EnumerateFields())
                        if ((fld as Field_Impl).NextInCallInitializersOrderId == Store.Const.CONSTS.WrongID)
                            lastAddedField = fld as Field_Impl;

                if(lastAddedField != null)
                    lastAddedField.NextInCallInitializersOrderId = field.Id;

                lastAddedField = field;
            }

            //Сохраняем связь с классом
            theClasses.tableFields.PutWithRepetition(theID, field.Id);

            //Сохраняем связь с модулем
            Module_Impl.SetFieldModule(theClasses.storage.modules, ImplementedInModuleID, field.Id);

            return field;
        }

        public void AddInnerClass(Class clazz)
        {
            if (clazz != null)
            {
                theClasses.tableInnerClasses.PutWithoutRepetition(theID, clazz.Id);
                theClasses.tableUpperClass.Put(clazz.Id, theID);
            }
        }

        public IEnumerable<Class> EnumerateInnerClasses()
        {
            foreach (UInt64 Id in theClasses.tableInnerClasses.GetValueByKey(theID))
                yield return theClasses.GetClass(Id);
        }

        public Class GetInnerClass(string className)
        {
            foreach (Class clazz in EnumerateInnerClasses())
                if (clazz.Name == className)
                    return clazz;

            return null;
        }


        public IEnumerable<IMethod> EnumerateAccessibleMethods(string name)
        {
            foreach(UInt64 id in theClasses.tableVisibleMethods.FindValuesByKey(buf => {buf.Add(this.theID); buf.Add(name);}))
                yield return (IMethod) theClasses.storage.functions.GetFunction(id);
        }

        #endregion

        internal override DataObjectLeaderWithName GetContainer()
        {
            UInt64 id = theClasses.tableUpperClass.GetValueByID(theID);
            if (id != Store.Const.CONSTS.WrongID)
                return (DataObjectLeaderWithName) theClasses.GetClass(id);

            id = theClasses.storage.namespaces.tableClassNamespace.GetValueByID(theID);
            if (id != Store.Const.CONSTS.WrongID)
                return theClasses.storage.namespaces.tableLeader.Get(id);

            return null;            
        }
    }
}
