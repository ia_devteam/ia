using System;
using System.Collections.Generic;
using System.IO;

namespace Store
{
    /// <summary>
    /// Сущность хранилища, хранящая журнал. Может использоваться для хранения любого журнала любой длины.
    /// </summary>
    public sealed class Log : IDisposable
    {
        string FileName_log;

        Storage storage;

        //Основные таблицы
        System.IO.StreamWriter writer = null;
        System.IO.Compression.GZipStream gzip = null;
        System.IO.FileStream file = null;
        int readersCount = 0;

        #region Generate Log
        /// <summary>
        /// Создавать экземпляры Log самостоятельно запрещено.
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="logFileName"></param>
        internal Log(Storage storage, string logFileName)
        {
            this.storage = storage;
            this.FileName_log = logFileName;

            storage.CloseEvent += new Store.CloseHandle(Close);
        }

        /// <summary>
        /// Вызывается при уничтожении объекта класса. В случае, если объект после хотя бы одной записи в лог не был закрыт при помощи вызова метода <see cref="Store.Log.StopWriting()"/>, то пользователю быдет выдана ошибка.
        /// </summary>
        ~Log()
        {
            if (writer != null)
                IA.Monitor.Log.Error(Storage.objectName, "Log " + FileName_log + "не был закрыт.");
        }

        private void Close()
        {
            StopWriting();
        }

        #endregion


        /// <summary>
        /// Пишет запись в лог. При этом файл с логом остается открытым. По окончании записи всего что надо в лог, надо вызвать метод StopWriting().
        /// </summary>
        /// <param name="message">Запись. В записе запрещается вставлять символ нового абзаца.</param>
        public void Warning(string message)
        {
            if (writer == null)
            {
                if (readersCount > 0)
                    throw new Exception("Попытка записать в лог " + FileName_log + " при открытом итераторе для чтения этого лога");

                //Генерируем имя нового файла
                string fileName = GenerateFullFileName();
                
                //Лог можно закрывать и дописывать. Однако GZip этого не позволяет. 
                //В связи с этим один лог может состоять из нескольких файлов. Ищем свободное имя для нового файла.
                int count = 0;
                string name;
                while (System.IO.File.Exists(name = combineName(fileName, count)))
                    count++;

                //Теперь открываем файл
                file = new FileStream(name, FileMode.CreateNew, FileAccess.Write);
                gzip = new System.IO.Compression.GZipStream(file,System.IO.Compression.CompressionMode.Compress);
                writer = new System.IO.StreamWriter(gzip);
            }

            writer.WriteLine(message);
        }

        /// <summary>
        /// Закрывает дескриптор записи в лог, если он был открыт.
        /// Необходимо вызвать после хотя бы одного вызова Warning.
        /// </summary>
        public void StopWriting()
        {
            if (writer != null)
            {
                writer.Dispose();
                
                gzip.Close();
                gzip.Dispose();
                file.Dispose();
                writer = null;
            }
        }
        /// <summary>
        /// Деструтор
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1816:CallGCSuppressFinalizeCorrectly")]
        public void Dispose()
        {
            StopWriting();
        }


        /// <summary>
        /// Возвращает enumerable по записям в логе.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> ReadLog()
        {
            if(writer != null)
                throw new Exception("Попытка чтения из лога " + FileName_log + " при открытом писателе в этот лог");

            string name = GenerateFullFileName();
            int fileNum = 0;
            while (System.IO.File.Exists(name + fileNum.ToString() + ".gz"))
            {
                using (System.IO.FileStream file1 = new FileStream(name + fileNum.ToString() + ".gz", FileMode.Open, FileAccess.Read))
                    using (System.IO.Compression.GZipStream gzip = new System.IO.Compression.GZipStream(file1, System.IO.Compression.CompressionMode.Decompress))
                    using (System.IO.StreamReader reader1 = new System.IO.StreamReader(gzip))
                    {
                        string str;
                        while ((str = reader1.ReadLine()) != null)
                            yield return str;
                    }

                fileNum++;
            }
        }

        /// <summary>
        /// Полностью очищает лог.
        /// </summary>
        public void Clear()
        {
            string logname = GenerateFullFileName();
            int i = 0;
            string curName;
            while (System.IO.File.Exists(curName = combineName(logname, i)))
            {
                System.IO.File.Delete(curName);
                i++;
            }
        }
        static private string combineName(string logname, int num)
        {
            if (num == 0)
                return logname + ".gz";
            return logname + Convert.ToString(num, System.Globalization.CultureInfo.InvariantCulture) + ".gz";
        }

        /// <summary>
        /// Генерирует полное имя файла с логом.
        /// </summary>
        /// <returns></returns>
        string GenerateFullFileName()
        {
            return System.IO.Path.Combine(storage.Get_DB().GetDBDirectory(), FileName_log);
        }

        /// <summary>
        /// Генерирует отчет в виде копии лога
        /// </summary>
        /// <param name="reportFileName"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public bool GenerateReport(string reportFileName)
        {
            using (FileStream theFile = new FileStream(reportFileName, FileMode.Create, FileAccess.Write))
            {
                using (System.IO.Compression.GZipStream theGzip = new System.IO.Compression.GZipStream(theFile, System.IO.Compression.CompressionMode.Compress))
                {
                    using (System.IO.StreamWriter theWriter = new System.IO.StreamWriter(theGzip))
                    {
                        foreach (string str in ReadLog())
                            theWriter.WriteLine(str);

                        theWriter.Flush();
                    }
                }
            }

            return true;
        }
    }
}
