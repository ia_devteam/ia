﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Linq;
using TestUtils;
using System.IO;
using System.Text;
using FileOperations;

namespace TestAbstractReader
{
    /// <summary> Тест предназначен для проверки функционала 'AbstractReader'  </summary>
    [TestClass]
    public class TestAbstractReader
    {
        private TestUtils.TestUtilsClass _testUtils;
        private TestContext _testContextInstance;
        private string _headFolder;     // путь до каталога с материалами для данного теста
        private string _pathChinese;    // путь для теста работы китайской кодировки
        private string _pathRussian;    // путь для теста работы русской кодировки
        private string _pathEnglish;    // путь для теста работы английской кодировки

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return _testContextInstance; }
            set { _testContextInstance = value; }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            _testUtils = new TestUtils.TestUtilsClass(_testContextInstance.TestName);
            _headFolder = _testUtils.TestMatirialsDirectoryPath + @"\TestFileReader\TestAbstractReader\";
            _pathChinese = _headFolder + "ch_hello.cxx";
            _pathRussian = _headFolder + "ru_hello.cxx";
            _pathEnglish = _headFolder + "en_hello.cxx";
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

        }

        /// <summary>
        /// Тестирование работы получения смещения при заданной кодировке 
        /// int getOffset(int line, int column, Encoding enc)
        /// </summary>
        [TestMethod]
        public void TestOffsetEncodingChinese()
        {
            AbstractReader aReader = new AbstractReader(_pathChinese);
            int j = aReader.getOffset(4, 52, Encoding.UTF8);    // на вход позиция символа если 
            //int j = aReader.getOffset(4, 46, Encoding.UTF8);    // на вход позиция символа если 
            // отрыкть в Notepad++ при кодировке ASCII
            string chText = aReader.getText();
            string chLitera = chText[j].ToString();
            Assert.IsTrue(chText[j].ToString() == "世", "Offset test for Chinese encoding was NOT passed");
            //Assert.IsTrue(chLitera == "好", "Offset test for Chinese encoding was NOT passed"); 
        }

        [TestMethod]
        public void TestOffsetEncodingRussian()
        {
            AbstractReader aReader = new AbstractReader(_pathRussian);
            // ищем вторую букву 'м'
            int j = aReader.getOffset(2, 90, Encoding.UTF8);    // на вход позиция символа если
            //int j = aReader.getOffset(5, 81, Encoding.UTF8);    // на вход позиция символа если 
            // отрыкть в Notepad++ при кодировке ASCII
            string ruText = aReader.getText();
            string ruLitera = ruText[j].ToString();
            Assert.IsTrue(ruLitera == "м", "Offset test for Russian encoding was NOT passed");
        }

        // Убедиться, что не испорчено для английского языка
        [TestMethod]
        public void TestOffsetEncodingEnglish()
        {
            AbstractReader aReader = new AbstractReader(_pathEnglish);
            // ищем вторую букву 'H'
            int j = aReader.getOffset(5, 45);    // на вход позиция символа если 
            // отрыкть в Notepad++ при кодировке ASCII
            string enText = aReader.getText();
            string enLitera = enText[j].ToString();
            Assert.IsTrue(enLitera == "H", "Offset test for English encoding was NOT passed");
        }

        // на выходе должны получить слово 'ref' - из букв в разных местах файла 
        // 'm17n-lib-1.6.4\src\m17n-core.c'
        // отрыкть в Notepad++ при кодировке ASCII и узнать смещение любой буквы
        [TestMethod]
        public void TestOffsetsEncodingm17n_lib()
        {
            string m17n_path = _headFolder + @"m17n-lib-1.6.4\src\m17n-core.c";
            AbstractReader aReader = new AbstractReader(m17n_path);
            string m17nTxt = aReader.getText();
            int j1 = aReader.getOffset(1, 11, Encoding.UTF8);      // 'r' - на первой строке 
            int j2 = aReader.getOffset(25, 9, Encoding.UTF8);      // 'e' - в слове @brief, экзотических символов еще не было 
            int j3 = aReader.getOffset(180, 21, Encoding.UTF8);    // 'f' - в слове @ref, после экзотики
            Assert.IsTrue(m17nTxt[j1].ToString().ToLower() == "r", "Offset test for file 'm17n-core.c' was NOT passed");
            Assert.IsTrue(m17nTxt[j2].ToString().ToLower() == "e", "Offset test for file 'm17n-core.c' was NOT passed");
            Assert.IsTrue(m17nTxt[j3].ToString().ToLower() == "f", "Offset test for file 'm17n-core.c' was NOT passed");
        }


    }
} /* END: namespace TestAbstractReader */