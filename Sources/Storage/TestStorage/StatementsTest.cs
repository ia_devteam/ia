﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;

namespace TestStorage
{
    /// <summary>
    /// Summary description for Statements
    /// </summary>
    [TestClass]
    public class StatementsTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        [TestMethod]
        public void TestStatement()
        {
            IFunction func = storage.functions.AddFunction();
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            //1
            IStatement st1 = storage.statements.AddStatementOperational(1, file);
            IStatementIf st2 = storage.statements.AddStatementIf(1, file);
            st1.NextInLinearBlock = st2;

            //2
            func.EntryStatement = st1;

            //3
            IStatement st3 = storage.statements.AddStatement(ENStatementKind.STSwitch, 1, file);
            IStatement st4 = storage.statements.AddStatement(ENStatementKind.STBreak, 1, file);
            st2.ThenStatement = st3;
            st2.ElseStatement = st4;

            testUtils.Storage_Reopen(ref storage);

            st1 = storage.statements.GetStatement(1);
            st2 = (IStatementIf) storage.statements.GetStatement(2);
            st3 = storage.statements.GetStatement(3);
            st4 = storage.statements.GetStatement(4);
            func = storage.functions.GetFunction(1);

            //1
            Assert.IsTrue(st2.PreviousInLinearBlock is IStatementOperational);
            Assert.IsTrue(storage.statements.GetStatement(2) is IStatementIf);

            Assert.IsTrue(st1.PreviousInLinearBlock == null);

            //2
            Assert.IsTrue(func.EntryStatement is IStatementOperational);

            //3
            st2 = storage.statements.GetStatement(2) as IStatementIf;
            Assert.IsTrue(st2.ThenStatement is IStatementSwitch);
            Assert.IsTrue(st2.ElseStatement is IStatementBreak);
            Assert.IsTrue(st2.ThenStatement.NextInLinearBlock == null);

            int i = 0;
            foreach (IStatement st in st2.SubStatements())
            {
                switch(i)
                {
                    case 0:
                        Assert.IsTrue(st is IStatementSwitch);
                        break;
                    case 1:
                        Assert.IsTrue(st is IStatementBreak);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);

        }

        /// <summary>
        ///A test for AddStatement
        ///</summary>
        [TestMethod()]
        public void AddStatementTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatement(ENStatementKind.STFor, 1, file);

            Assert.IsTrue(st is IStatementFor);
        }

        /// <summary>
        ///A test for AddStatementBreak
        ///</summary>
        [TestMethod()]
        public void AddStatementBreakTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementBreak(1, file);
            Assert.IsTrue(st is IStatementBreak);
        }

        /// <summary>
        ///A test for AddStatementContinue
        ///</summary>
        [TestMethod()]
        public void AddStatementContinueTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementContinue(1, file);
            Assert.IsTrue(st is IStatementContinue);
        }

        /// <summary>
        ///A test for AddStatementDoAndWhile
        ///</summary>
        [TestMethod()]
        public void AddStatementDoAndWhileTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementDoAndWhile(1, file);
            Assert.IsTrue(st is IStatementDoAndWhile);
        }

        /// <summary>
        ///A test for AddStatementFor
        ///</summary>
        [TestMethod()]
        public void AddStatementForTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementFor(1, file);
            Assert.IsTrue(st is IStatementFor);
        }

        /// <summary>
        ///A test for AddStatementForEach
        ///</summary>
        [TestMethod()]
        public void AddStatementForEachTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementForEach(1, file);
            Assert.IsTrue(st is IStatementForEach);
        }

        /// <summary>
        ///A test for AddStatementGoto
        ///</summary>
        [TestMethod()]
        public void AddStatementGotoTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementGoto(1, file);
            Assert.IsTrue(st is IStatementGoto);
        }

        /// <summary>
        ///A test for AddStatementIf
        ///</summary>
        [TestMethod()]
        public void AddStatementIfTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementIf(1, file);
            Assert.IsTrue(st is IStatementIf);
        }

        /// <summary>
        ///A test for AddStatementOperational
        ///</summary>
        [TestMethod()]
        public void AddStatementOperationalTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementOperational(1, file);
            Assert.IsTrue(st is IStatementOperational);
        }

        /// <summary>
        ///A test for AddStatementReturn
        ///</summary>
        [TestMethod()]
        public void AddStatementReturnTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementReturn(1, file);
            Assert.IsTrue(st is IStatementReturn);
        }

        /// <summary>
        ///A test for AddStatementSwitch
        ///</summary>
        [TestMethod()]
        public void AddStatementSwitchTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementSwitch(1, file);
            Assert.IsTrue(st is IStatementSwitch);
        }

        /// <summary>
        ///A test for AddStatementThrow
        ///</summary>
        [TestMethod()]
        public void AddStatementThrowTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementThrow(1, file);
            Assert.IsTrue(st is IStatementThrow);
        }

        /// <summary>
        ///A test for AddStatementTryCatchFinally
        ///</summary>
        [TestMethod()]
        public void AddStatementTryCatchFinallyTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementTryCatchFinally(1, file);
            Assert.IsTrue(st is IStatementTryCatchFinally);
        }

        /// <summary>
        ///A test for AddStatementUsing
        ///</summary>
        [TestMethod()]
        public void AddStatementUsingTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatement st = storage.statements.AddStatementUsing(1, file);
            Assert.IsTrue(st is IStatementUsing);
        }

        /// <summary>
        ///A test for EnumerateStatements
        ///</summary>
        [TestMethod()]
        public void EnumerateStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            storage.statements.AddStatementBreak(1, file);
            storage.statements.AddStatementContinue(1, file);
            storage.statements.AddStatementDoAndWhile(1, file);
            storage.statements.AddStatementFor(1, file);
            storage.statements.AddStatementForEach(1, file);
            storage.statements.AddStatementGoto(1, file);
            storage.statements.AddStatementIf(1, file);
            storage.statements.AddStatementOperational(1, file);
            storage.statements.AddStatementReturn(1, file);
            storage.statements.AddStatementSwitch(1, file);
            storage.statements.AddStatementThrow(1, file);
            storage.statements.AddStatementTryCatchFinally(1, file);
            storage.statements.AddStatementUsing(1, file);


            int i = 0;
            foreach (IStatement st in storage.statements.EnumerateStatements())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(st is IStatementBreak);
                        break;
                    case 1:
                        Assert.IsTrue(st is IStatementContinue);
                        break;
                    case 2:
                        Assert.IsTrue(st is IStatementDoAndWhile);
                        break;
                    case 3:
                        Assert.IsTrue(st is IStatementFor);
                        break;
                    case 4:
                        Assert.IsTrue(st is IStatementForEach);
                        break;
                    case 5:
                        Assert.IsTrue(st is IStatementGoto);
                        break;
                    case 6:
                        Assert.IsTrue(st is IStatementIf);
                        break;
                    case 7:
                        Assert.IsTrue(st is IStatementOperational);
                        break;
                    case 8:
                        Assert.IsTrue(st is IStatementReturn);
                        break;
                    case 9:
                        Assert.IsTrue(st is IStatementSwitch);
                        break;
                    case 10:
                        Assert.IsTrue(st is IStatementThrow);
                        break;
                    case 11:
                        Assert.IsTrue(st is IStatementTryCatchFinally);
                        break;
                    case 12:
                        Assert.IsTrue(st is IStatementUsing);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i==13);
        }

        /// <summary>
        ///A test for GetStatement
        ///</summary>
        [TestMethod()]
        public void GetStatementTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            storage.statements.AddStatementBreak(1, file);
            storage.statements.AddStatementContinue(1, file);
            storage.statements.AddStatementDoAndWhile(1, file);
            storage.statements.AddStatementFor(1, file);
            storage.statements.AddStatementForEach(1, file);
            storage.statements.AddStatementGoto(1, file);
            storage.statements.AddStatementIf(1, file);
            storage.statements.AddStatementOperational(1, file);
            storage.statements.AddStatementReturn(1, file);
            storage.statements.AddStatementSwitch(1, file);
            storage.statements.AddStatementThrow(1, file);
            storage.statements.AddStatementTryCatchFinally(1, file);
            storage.statements.AddStatementUsing(1, file);

            Assert.IsTrue(storage.statements.GetStatement(2) is IStatementContinue);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue(storage.statements.GetStatement(5) is IStatementForEach);

        }

        /// <summary>
        /// Тестируется работа с Location при создании Statement
        /// </summary>
        [TestMethod]
        public void StatementLocationWithCreationTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            
            //Штатное добавление оператора
            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            Assert.IsTrue(st1.LastSymbolLocation == null);
            Assert.IsTrue(st1.FirstSymbolLocation.GetOffset() == 1);

            IStatement st2 = storage.statements.AddStatementBreak(1, 1, file);
            Assert.IsTrue(st2.LastSymbolLocation == null);
            Assert.IsTrue(st2.FirstSymbolLocation.GetLine() == 1);
            Assert.IsTrue(st2.FirstSymbolLocation.GetColumn() == 1);

            //Добавление оператора с некорректным смещением начала
            bool isCatch = false;
            try
            {
                storage.statements.AddStatementContinue(0, file);
            }
            catch(Store.Const.StorageException ex)
            {
                isCatch = true;
            }
            finally
            {
                if (!isCatch)
                    Assert.Fail();
            }


            //Формируем список типов отлавливаемых сообщений
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>()
            {
                IA.Monitor.Log.Message.enType.ERROR
            };

            //Формируем список ожидаемых сообщений
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>()
            {
                new IA.Monitor.Log.Message(
                    "Хранилище",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Вызов AddStatement(ENStatementKind kind, IFile file, UInt64 firstSymbolLine, UInt64 firstSymbolColumn) некорректные аргументы. line == 0"
                    ),
                new IA.Monitor.Log.Message(
                    "Хранилище",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Вызов AddStatement(ENStatementKind kind, IFile file, UInt64 firstSymbolLine, UInt64 firstSymbolColumn) некорректные аргументы. line == 0"
                    )
            };

            using (TestUtils.TestUtilsClass.LogListener listener = new TestUtils.TestUtilsClass.LogListener(messageTypes, messages))
            {
                storage.statements.AddStatementContinue(0, 1, file);
                storage.statements.AddStatementContinue(1, 0, file);
            }

            //Добавление оператора со всеми смещениями
            IStatement st3 = storage.statements.AddStatementBreak(1, file, 1);
            Assert.IsTrue(st3.LastSymbolLocation.GetOffset() == 1);
            Assert.IsTrue(st3.FirstSymbolLocation.GetOffset() == 1);

            IStatement st4 = storage.statements.AddStatementBreak(1, 1, file, 1, 1);
            Assert.IsTrue(st4.LastSymbolLocation.GetLine() == 1);
            Assert.IsTrue(st4.LastSymbolLocation.GetColumn() == 1);
            Assert.IsTrue(st4.FirstSymbolLocation.GetLine() == 1);
            Assert.IsTrue(st4.FirstSymbolLocation.GetColumn() == 1);

            //Добавление оператора с некорректным смещением конца            
            isCatch = false;
            try
            {
                storage.statements.AddStatementContinue(1, 1, file, 1, 0);
            }
            catch (Store.Const.StorageException ex)
            {
                isCatch = true;
            }
            finally
            {
                if (!isCatch)
                    Assert.Fail();
            }

            isCatch = false;
            try
            {
                storage.statements.AddStatementContinue(1, 1, file, 0, 1);
            }
            catch (Store.Const.StorageException ex)
            {
                isCatch = true;
            }
            finally
            {
                if (!isCatch)
                    Assert.Fail();
            }
        }
    }
}
