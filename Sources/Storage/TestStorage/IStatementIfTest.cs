﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementIfTest and is intended
    ///To contain all IStatementIfTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementIfTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for Condition
        ///</summary>
        [TestMethod()]
        public void ConditionTest()
        {

            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementIf d = storage.statements.AddStatementIf(1, file);
            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();

            Assert.IsTrue(d.Condition == null);

            d.Condition = op2;

            Assert.IsTrue(d.Condition.Id == 2);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementIf).Condition.Id == 2);
        }

        /// <summary>
        ///A test for ElseStatement
        ///</summary>
        [TestMethod()]
        public void ElseStatementTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementIf d = storage.statements.AddStatementIf(1, file);
            IStatement st = storage.statements.AddStatementBreak(1, file);

            Assert.IsTrue(d.ElseStatement == null);

            d.ElseStatement = st;

            Assert.IsTrue(d.ElseStatement is IStatementBreak);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementIf).ElseStatement is IStatementBreak);
        }

        /// <summary>
        ///A test for ThenStatement
        ///</summary>
        [TestMethod()]
        public void ThenStatementTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementIf d = storage.statements.AddStatementIf(1, file);
            IStatement st = storage.statements.AddStatementBreak(1, file);

            Assert.IsTrue(d.ThenStatement == null);

            d.ThenStatement = st;

            Assert.IsTrue(d.ThenStatement is IStatementBreak);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementIf).ThenStatement is IStatementBreak);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void IfSubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatementIf d = storage.statements.AddStatementIf(1, file);

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);

            IStatementBreak c = storage.statements.AddStatementBreak(1, file);
            IStatementContinue c1 = storage.statements.AddStatementContinue(1, file);
            d.ThenStatement = c;
            d.ElseStatement = c1;

            int i = 0;
            foreach (IStatement st in d.SubStatements())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(st is IStatementBreak);
                        break;
                    case 1:
                        Assert.IsTrue(st is IStatementContinue);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);
        }
    }
}
