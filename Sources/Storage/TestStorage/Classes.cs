﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;

namespace TestStorage
{
    /// <summary>
    /// Summary description for Classes
    /// </summary>
    [TestClass]
    public class Test_Classes
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        [TestMethod]
        public void Classes_Methods()
        {            
            //Наполняем
            Classes classes = storage.classes;
            Class clazz = classes.AddClass();
            clazz.Name = "asd";
            clazz = classes.AddClass();
            clazz.Name = "qwe";

            Functions functions = storage.functions;
            IFunction func1 = clazz.AddMethod(true);
            func1.Name = "f1";
            IFunction func2 = clazz.AddMethod(true);
            func2.Name = "f2";

            clazz = classes.GetClass(1);
            IFunction func3 = clazz.AddMethod(true);
            func3.Name = "f3";

            //Проверяем
            testUtils.Storage_Reopen(ref storage);

            classes = storage.classes;
            functions = storage.functions;
            int i = 0;
            foreach (Class cl in classes.EnumerateClasses())
            {
                i++;
                if (i == 1)
                {
                    Assert.IsTrue(cl.Name == "asd");
                    int j = 0;
                    foreach (IFunction func in cl.EnumerateMethods())
                    {
                        j++;
                        if (j == 1)
                            Assert.IsTrue(func.Name == "f3");
                        else
                            Assert.Fail();
                    }
                    Assert.IsTrue(j == 1);
                }
                if (i == 2)
                {
                    Assert.IsTrue(cl.Name == "qwe");
                    int j = 0;
                    foreach (IFunction func in cl.EnumerateMethods())
                    {
                        j++;
                        if (j == 1)
                            Assert.IsTrue(func.Name == "f1");
                        else if (j == 2)
                            Assert.IsTrue(func.Name == "f2");
                        else
                            Assert.Fail();
                    }
                    Assert.IsTrue(j == 2);
                }
            }
            Assert.IsTrue(i == 2);
        }
    }
}
