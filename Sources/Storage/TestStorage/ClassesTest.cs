﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using TestUtils;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for ClassesTest and is intended
    ///To contain all ClassesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ClassesTest
    {
        private TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            storage = testUtils.Storage_CreateAndOpen();
        }
        
        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }


        /// <summary>
        ///A test for GetClass
        ///</summary>
        [TestMethod()]
        public void GetClassTest()
        {
            Class class1 = storage.classes.AddClass();
            Class class2 = storage.classes.AddClass();

            UInt64 id1 = class1.Id;
            UInt64 id2 = class2.Id;

            class1.Name = "qqq";
            class2.Name = "w";

            Class class11 = storage.classes.GetClass(id1);
            Class class12 = storage.classes.GetClass(id2);

            Assert.IsTrue(class11 == class1);
            Assert.IsTrue(class12 == class2);

            class1 = null;
            class2 = null;

            testUtils.Storage_Reopen(ref storage);

            class11 = storage.classes.GetClass(id1);
            class12 = storage.classes.GetClass(id2);

            Assert.IsTrue(class11.Name == "qqq");
            Assert.IsTrue(class12.Name == "w");
        }

        /// <summary>
        ///A test for EnumerateClasses
        ///</summary>
        [TestMethod()]
        public void EnumerateClassesTest()
        {
            Class class1 = storage.classes.AddClass();
            Class class2 = storage.classes.AddClass();
            Class class3 = storage.classes.AddClass();

            class1.Name = "qqq";
            class2.Name = "w";
            class3.Name = "w3";

            int i = 0;
            foreach (Class clazz in storage.classes.EnumerateClasses())
            {
                switch(i)
                {
                    case 0:
                        Assert.IsTrue(clazz.Name == "qqq");
                        break;
                    case 1:
                        Assert.IsTrue(clazz.Name == "w");
                        break;
                    case 2:
                        Assert.IsTrue(clazz.Name == "w3");
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 3);
        }

        /// <summary>
        ///A test for GetClass
        ///</summary>
        [TestMethod()]
        public void GetClassTest1()
        {
            Class cl1 = storage.classes.AddClass();
            Class cl2 = storage.classes.AddClass();
            Class cl3 = storage.classes.AddClass();

            cl1.Name = "a";
            cl2.Name = "a";
            cl3.Name = "b";

            TestUtilsClass.CheckArray<Class>((c1, c2) => { return c1 == c2; }, storage.classes.GetClass("a"), new Class[] { cl1, cl2 });
            TestUtilsClass.CheckArray<Class>((c1, c2) => { return c1 == c2; }, storage.classes.GetClass("b"), new Class[] { cl3 });
        }

        /// <summary>
        ///A test for GetClass
        ///</summary>
        [TestMethod()]
        public void GetClassTest2()
        {
            Namespace ns1 = storage.namespaces.FindOrCreate("a");
            Namespace ns2 = storage.namespaces.FindOrCreate(new string[]{"b", "a"});

            Class cl1 = storage.classes.AddClass();
            Class cl2 = storage.classes.AddClass();
            Class cl3 = storage.classes.AddClass();

            ns1.AddClassToNamespace(cl1);
            ns2.AddClassToNamespace(cl2);
            ns2.AddClassToNamespace(cl3);

            cl1.Name = "a";
            cl2.Name = "a";
            cl3.Name = "b";

            TestUtilsClass.CheckArray<Class>((c1, c2) => { return c1 == c2; }, storage.classes.GetClass(new string[]{"a", "a"}), new Class[] { cl1 });
            TestUtilsClass.CheckArray<Class>((c1, c2) => { return c1 == c2; }, storage.classes.GetClass(new string[] { "b", "a", "a" }), new Class[] { cl2 });
        }

        /// <summary>
        ///A test for Find
        ///</summary>
        [TestMethod()]
        public void FindTest()
        {
            storage.appliedSettings.SetFileListPath(@"c:\1");

            Store.Module mod1 = storage.modules.AddModule(@"c:\1\1", enFileKindForAdd.externalFile);
            Store.Module mod2 = storage.modules.AddModule(@"c:\1\2", enFileKindForAdd.externalFile);

            Namespace ns = storage.namespaces.FindOrCreate("ns1");

            Class cl1 = storage.classes.AddClass();
            cl1.Name = "cl1";
            ns.AddClassToNamespace(cl1);
            mod1.AddImplementedClass(cl1);
            Class cl2 = storage.classes.AddClass();
            cl2.Name = "cl2";
            cl1.AddInnerClass(cl2);
            mod1.AddImplementedClass(cl2);

            Assert.IsTrue(storage.classes.Find(mod1, new string[]{"ns1"}, null, "cl2") == null);
            Class res = storage.classes.Find(mod1, new string[] { "ns1" }, null, "cl1");
            Assert.IsTrue(res == cl1);
            Assert.IsTrue(storage.classes.Find(mod1, new string[] { "ns1" }, new string[] { } , "cl1") == cl1);
            Assert.IsTrue(storage.classes.Find(mod1, new string[] { "ns1" }, new string[] { "cl1"}, "cl2") == cl2);
            Assert.IsTrue(storage.classes.Find(mod2, new string[] { "ns1" }, new string[] { "cl1" }, "cl2") == null);
        }

        /// <summary>
        ///A test for FindOrCreate
        ///</summary>
        [TestMethod()]
        public void FindOrCreateTest()
        {
            storage.appliedSettings.SetFileListPath(@"c:\1");

            Store.Module mod1 = storage.modules.AddModule(@"c:\1\1", enFileKindForAdd.externalFile);

            Class cl3 = storage.classes.FindOrCreate(mod1, new string[] { "ns1", "ns2" }, new string[] { "cl1", "cl2" }, "cl3");
            Class cl1 = storage.classes.GetClass("cl1")[0];
            Class cl2 = storage.classes.GetClass("cl2")[0];

            TestUtilsClass.CheckArray<Class>((c1, c2) => { return c1 == c2; }, storage.classes.EnumerateClasses(), new Class[] { cl1, cl2, cl3 });

            Assert.IsTrue(cl1.ImplementedInModule == mod1);
            Assert.IsTrue(cl2.ImplementedInModule == mod1);
            Assert.IsTrue(cl3.ImplementedInModule == mod1);

            Assert.IsTrue(cl1.FullName == "ns1.ns2.cl1");
            Namespace ns = storage.namespaces.Find(new string[] { "ns1", "ns2" });
            TestUtilsClass.CheckArray<Class>((c1, c2) => { return c1 == c2; }, ns.EnumerateClassesInNamespace(), new Class[] { cl1 });

            TestUtilsClass.CheckArray<Class>((c1, c2) => { return c1 == c2; }, cl1.EnumerateInnerClasses(), new Class[] { cl2 });
            TestUtilsClass.CheckArray<Class>((c1, c2) => { return c1 == c2; }, cl2.EnumerateInnerClasses(), new Class[] { cl3 });

            Class cl4 = storage.classes.FindOrCreate(mod1, new string[] { "ns1", "ns2" }, new string[] { "cl1", "cl2" }, "cl3");

            Assert.IsTrue(cl4 == cl3);
            TestUtilsClass.CheckArray<Class>((c1, c2) => { return c1 == c2; }, storage.classes.EnumerateClasses(), new Class[] { cl1, cl2, cl3 });
        }
    }
}
