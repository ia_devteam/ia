﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for Function_ImplTest and is intended
    ///To contain all Function_ImplTest Unit Tests
    ///</summary>
    [TestClass()]
    public class Function_ImplTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for EnumerateInnerFunctions
        ///</summary>
        [TestMethod()]
        public void EnumerateInnerFunctionsTest()
        {
            Class class1 = storage.classes.AddClass();

            IFunction func1 = class1.AddMethod(true);
            IFunction func2 = storage.functions.AddFunction();


            func1.Name = "func1";
            func2.Name = "func2";
            class1.Name = "class1";

            func1.AddInnerFunction(func2);

            string str = func2.FullName;
            Assert.IsTrue(str == "class1.func1.func2");
        }


        [TestMethod()]
        public void FunctionNameNameTest()
        {
            IFunction func = storage.functions.AddFunction();

            func.Name = null;
            func.Name = "aaa";

            Assert.IsTrue(func.Name == "aaa");

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue(storage.functions.GetFunction(1).Name == "aaa");
        }
    }
}
