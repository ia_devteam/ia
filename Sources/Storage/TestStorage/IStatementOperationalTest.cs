﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementOperationalTest and is intended
    ///To contain all IStatementOperationalTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementOperationalTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for Operation
        ///</summary>
        [TestMethod()]
        public void OperationTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementOperational d = storage.statements.AddStatementOperational(1, file);
            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();

            Assert.IsTrue(d.Operation == null);

            d.Operation = op2;

            Assert.IsTrue(d.Operation.Id == 2);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementOperational).Operation.Id == 2);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void OperationalSubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatementGoto d = storage.statements.AddStatementGoto(1, file);

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);
        }

        [TestMethod()]
        public void OperationalCallesTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);

            IFunction f1 = storage.functions.AddFunction();
            IFunction f2 = storage.functions.AddFunction();
            Variable v = storage.variables.AddVariable();

            IStatementOperational op = storage.statements.AddStatementOperational(1, file);
            f1.EntryStatement = op;
            op.Operation = storage.operations.AddOperation();
            op.Operation.AddFunctionCallInSequenceOrder(new IFunction[] { f2 });
            op.Operation.AddFunctionCallInSequenceOrder(v);

            int i = 0;
            foreach (IFunctionCall call in f1.Calles())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(Checker(call, f1, f2, op.FirstSymbolLocation));
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 1);

        }

        bool Checker(IFunctionCall arg, IFunction Caller, IFunction Calles, Location loc)
        {
            if (arg.Caller != Caller)
                return false;

            if (arg.Calles != Calles)
                return false;

            if (arg.CallLocation.GetOffset() != loc.GetOffset() || arg.CallLocation.GetFileID() != loc.GetFileID())
                return false;

            return true;
        }
    }
}
