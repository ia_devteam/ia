﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementReturnTest and is intended
    ///To contain all IStatementReturnTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementReturnTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for ReturnOperation
        ///</summary>
        [TestMethod()]
        public void ReturnOperationTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementReturn d = storage.statements.AddStatementReturn(1, file);
            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();

            Assert.IsTrue(d.ReturnOperation == null);

            d.ReturnOperation = op2;

            Assert.IsTrue(d.ReturnOperation.Id == 2);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementReturn).ReturnOperation.Id == 2);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void ReturnSubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatementReturn d = storage.statements.AddStatementReturn(1, file);

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);
        }


        /// <summary>
        ///A test for YieldReturnOperation
        ///</summary>
        [TestMethod()]
        public void YieldReturnOperationTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementYieldReturn d = storage.statements.AddStatementYieldReturn(1, file);
            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();

            Assert.IsTrue(d.YieldReturnOperation == null);

            d.YieldReturnOperation = op2;

            Assert.IsTrue(d.YieldReturnOperation.Id == 2);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementYieldReturn).YieldReturnOperation.Id == 2);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void YieldReturnSubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatementYieldReturn d = storage.statements.AddStatementYieldReturn(1, file);

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);
        }
    }
}
