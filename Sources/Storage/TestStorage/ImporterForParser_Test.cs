﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using System.Xml;
using StorageImporter;
using System.Linq;
using IA.Plugins.Parsers.CommonUtils;
using TestUtils;
using System.IO;

namespace TestStorage
{
    /// <summary> Тест предназначен для проверки функционала загрузчика xml файлов от парсеров  </summary>
    [TestClass]
    public class ImporterForParserTest
    {
        private TestUtils.TestUtilsClass _testUtils;
        Storage _storage;
        private TestContext _testContextInstance;
        private string _headFolder;
        private string _rootFolderImport = @"\ImpPrsr_Tst\Load\";
        private string _rootFolderPrint = "";
        private string _rootFolderEtalon = @"\ImpPrsr_Tst\Etalon\";
        private string rootXmlPath = @"ImpPrsr_Tst\XML";
        private string rootFilePath = @"ImpPrsr_Tst\Files";
        private string rootReportPath = @"ImpPrsr_Tst\Report";

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return _testContextInstance; }
            set { _testContextInstance = value; }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            _testUtils = new TestUtils.TestUtilsClass(_testContextInstance.TestName);

            _headFolder = _testUtils.TestMatirialsDirectoryPath;
            _rootFolderImport = _headFolder + _rootFolderImport;
            _rootFolderPrint = _testUtils.TestRunDirectoryPath;
            _rootFolderEtalon = _headFolder + _rootFolderEtalon;

            this._storage = _testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            _testUtils.Storage_Remove(ref _storage);
        }

        ///// <summary> открываем пустой xml файл (header, один тег без начинки) </summary>
        //[TestMethod]
        //public void ImporterParser_Case_01()
        //{
        //    string importPath = _rootFolderImport + "Export_Case_01.xml";
        //    string reportPath = _rootFolderPrint + ".xml";
        //    string etalonPath = _rootFolderEtalon + "Etalon_Export_Case_01.xml";
        //    XMLStorageImporterForParser xmlImporter = new XMLStorageImporterForParser();
        //    XmlDocument exportCase01 = new XmlDocument();
        //    exportCase01.Load(importPath);
        //    xmlImporter.ImportXML(exportCase01, _storage);
        //    StatementPrinter sprintCase01 = new StatementPrinter(reportPath, _storage);
        //    sprintCase01.Execute_NoFile();
        //    bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
        //    Assert.IsTrue(isEqual, "Case_01 was passed"); // Проходит, проверено
        //}

        ///// <summary> данный xml файл не соответствует xsd файлу </summary>
        //[TestMethod]
        //public void ImporterParser_Case_02()
        //{
        //    string importPath = _rootFolderImport + "Export_Case_02.xml";
        //    string reportPath = _rootFolderPrint + ".xml";
        //    string etalonPath = _rootFolderEtalon + "Etalon_Export_Case_02.xml";
        //    XMLStorageImporterForParser xmlImporter = new XMLStorageImporterForParser();
        //    XmlDocument exportCase02 = new XmlDocument();
        //    exportCase02.Load(importPath);
        //    try
        //    {// Должно выдаваться исключение: xml от парсера не соответствует схеме
        //     // НО(!) выдается позже, при потпытке загрузки параметров
        //        xmlImporter.ImportXML(exportCase02, _storage);
        //    }
        //    catch (Exception e)
        //    {
        //        Assert.IsTrue(true, "Exception thrown - Case_02 was passed");
        //    }
        //}

        ///// <summary> у класса Class_1 не указано имя </summary>
        //[TestMethod]
        //public void ImporterParser_Case_03()
        //{
        //    string importPath = _rootFolderImport + "Export_Case_02.xml";
        //    string reportPath = _rootFolderPrint + ".xml";
        //    string etalonPath = _rootFolderEtalon + "Etalon_Export_Case_02.xml";
        //    XMLStorageImporterForParser xmlImporter = new XMLStorageImporterForParser();
        //    XmlDocument exportCase03 = new XmlDocument();
        //    exportCase03.Load(importPath);
        //    try
        //    {
        //        xmlImporter.ImportXML(exportCase03, _storage);
        //    }
        //    catch (Exception e)
        //    {
        //        Assert.IsTrue(true, "Exception thrown - Case_03 was passed");
        //    }
        //}

        ///// <summary> Namespace="Minor" содержит класс: переменную, метод, датчик в методе </summary>
        //[TestMethod]
        //public void ImporterParser_Case_1()
        //{
        //    // Тест не проходит при попытке загрузить поле класса :
        //    // Попытка изменить флаг isGlobal у поля. Для полей флаг isGlobal определяется полем isStatic.
        //    string importPath = _rootFolderImport + "Export_Case_1.xml";
        //    string reportPath = _rootFolderPrint + ".xml";
        //    string etalonPath = _rootFolderEtalon + "Etalon_Export_Case_1.xml";
        //    XMLStorageImporterForParser xmlImporter = new XMLStorageImporterForParser();
        //    XmlDocument xmlCase1 = new XmlDocument();
        //    xmlCase1.Load(importPath);
        //    xmlImporter.ImportXML(xmlCase1, _storage);
        //    StatementPrinter sprintCase1 = new StatementPrinter(reportPath, _storage);
        //    sprintCase1.Execute_NoFile();
        //    bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
        //    Assert.IsTrue(isEqual, "Case_01 was passed");
        //}

        ///// <summary> Namespace="" содержит 3 переменных, функции main, min_max с датчиками </summary>
        //[TestMethod]
        //public void ImporterParser_Case_2()
        //{
        //    string importPath = _rootFolderImport + "Export_Case_2.xml";
        //    string reportPath = _rootFolderPrint + ".xml";
        //    string etalonPath = _rootFolderEtalon + "Etalon_Export_Case_2.xml";
        //    XMLStorageImporterForParser xmlImporter = new XMLStorageImporterForParser();
        //    XmlDocument xmlCase2 = new XmlDocument();
        //    xmlCase2.Load(importPath);
        //    xmlImporter.ImportXML(xmlCase2, _storage);
        //    StatementPrinter sprintCase2 = new StatementPrinter(reportPath, _storage);
        //    sprintCase2.Execute_NoFile();
        //    bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
        //    Assert.IsTrue(isEqual, "Case_02 was passed");
        //}

        ///// <summary> Namespace "Minor" содержит value_1,value_2,result функции main, minfind </summary>
        //[TestMethod]
        //public void ImporterParser_Case_3()
        //{
        //    string importPath = _rootFolderImport + "Export_Case_3.xml";
        //    string reportPath = _rootFolderPrint + ".xml";
        //    string etalonPath = _rootFolderEtalon + "Etalon_Export_Case_3.xml";
        //    XMLStorageImporterForParser xmlImporter = new XMLStorageImporterForParser();
        //    XmlDocument xmlCase3 = new XmlDocument();
        //    xmlCase3.Load(importPath);
        //    xmlImporter.ImportXML(xmlCase3, _storage);
        //    StatementPrinter sprintCase3 = new StatementPrinter(reportPath, _storage);
        //    sprintCase3.Execute_NoFile();
        //    bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
        //    Assert.IsTrue(isEqual, "Case_03 was passed"); // Проходит, проверено
        //}

        private void ReplacePrefix(string xmlPath, string newXmlPath, string newMaterialsPath)
        {
            var sr = new StreamReader(xmlPath);
            //newMaterialsPath = newMaterialsPath.Replace("\\", "\\\\");
            var contents = sr.ReadToEnd().Replace("##PREFIX##", newMaterialsPath);
            var sw = new StreamWriter(newXmlPath);
            sw.Write(contents);
            sw.Close();
        }

        private void TestGoodCase(string testCase, string xmlFileName, string[] sourceFileNameArray)
        {
            var prefix = Path.Combine(_headFolder, rootFilePath, testCase);
            var reportOutput = Path.Combine(_testUtils.TestRunDirectoryPath, testCase, xmlFileName);
            var reportOriginal = Path.Combine(_headFolder, rootReportPath, testCase, xmlFileName);
            reportOutput = reportOutput.Replace(".json", ".xml");
            reportOriginal = reportOriginal.Replace(".json", ".xml");
            var xmlOriginalPath = Path.Combine(_headFolder, rootXmlPath, testCase, xmlFileName);
            var xmlPath = Path.Combine(_testUtils.TestRunDirectoryPath, xmlFileName);
            Directory.CreateDirectory(Path.GetDirectoryName(reportOutput));
            ReplacePrefix(xmlOriginalPath, xmlPath, prefix);

            _storage.appliedSettings.SetFileListPath(prefix);
            foreach (var s in sourceFileNameArray)
            {
                _storage.files.FindOrGenerate(Path.Combine(prefix, s), enFileKindForAdd.fileWithPrefix);
            }

            XMLStorageImporterForParser xmlImporter = new XMLStorageImporterForParser();
            string ext = Path.GetExtension(xmlPath);
            ext = ext.ToLower();
            if (ext == ".xml")
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(xmlPath);
                xmlImporter.ImportXML(xmlDocument, _storage);
            }
            else 
            {
                //xmlImporter.ImportXML(null, _storage, xmlPath);
            }

            StatementPrinter statementPrinter = new StatementPrinter(reportOutput, _storage);
            statementPrinter.Execute_NoFile();
            bool isEqual = TestUtilsClass.Reports_XML_Compare(reportOutput, reportOriginal);
            Assert.IsTrue(isEqual);
        }

        private void TestBadCase(string testCase, string xmlFileName, string[] sourceFileNameArray, string message)
        {
            var prefix = Path.Combine(_headFolder, rootFilePath, testCase);
            var xmlOriginalPath = Path.Combine(_headFolder, rootXmlPath, testCase, xmlFileName);
            var xmlPath = Path.Combine(_testUtils.TestRunDirectoryPath, xmlFileName);
            ReplacePrefix(xmlOriginalPath, xmlPath, prefix);

            _storage.appliedSettings.SetFileListPath(prefix);
            foreach (var s in sourceFileNameArray)
            {
                _storage.files.FindOrGenerate(Path.Combine(prefix, s), enFileKindForAdd.fileWithPrefix);
            }

            XMLStorageImporterForParser xmlImporter = new XMLStorageImporterForParser();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(xmlPath);
            try
            {
                xmlImporter.ImportXML(xmlDocument, _storage);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == message);
                return;
            }

            Assert.Fail();
        }

        /// <summary> </summary>
        [TestMethod]
        public void ImporterParser_TestFileId_Good()
        {
            TestGoodCase("Simple", "test_file_id.xml", new[] { "test_file_id.xml" });
        }

        /// <summary> </summary>
        [Ignore]
        [TestMethod]
        public void ImporterParser_TestFileId_JSONGood()
        {
            TestGoodCase("Simple", "test_file_id.json", new[] { "test_file_id.xml" });
        }

        /// <summary> </summary>
        [TestMethod]
        public void ImporterParser_TestFileId_Bad()
        {
            TestBadCase("Simple", "test_file_id_bad.xml", new[] { "test_file_id.xml" }, "Указана ссылка на файл, id которого не упомянут в секции Files");
        }

        /// <summary> </summary>
        [TestMethod]
        public void ImporterParser_TestFunctionLocation_Good()
        {
            TestGoodCase("Simple", "test_function_location.xml", new[] { "test_file_id.xml" });
        }

        /// <summary> </summary>
        [Ignore]
        [TestMethod]
        public void ImporterParser_TestFunctionLocation_JSONGood()
        {
            TestGoodCase("Simple", "test_function_location.json", new[] { "test_file_id.xml" });
        }

        /// <summary> </summary>
        [TestMethod]
        public void ImporterParser_TestFunctionLocation_Bad()
        {
            TestBadCase("Simple", "test_function_location_bad.xml", new[] { "test_file_id.xml" }, "Указана ссылка на функцию, id которой не упомянут в секции Functions");
        }

        /// <summary> </summary>
        [TestMethod]
        public void ImporterParser_TestSensorType_Good()
        {
            TestGoodCase("Simple", "test_sensor_type.xml", new[] { "test_file_id.xml" });
        }

        /// <summary> </summary>
        [Ignore]
        [TestMethod]
        public void ImporterParser_TestSensorType_JSONGood()
        {
            TestGoodCase("Simple", "test_sensor_type.json", new[] { "test_file_id.xml" });
        }
    }
} /* END: namespace TestStorage */
