﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for FunctionTest and is intended
    ///To contain all FunctionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FunctionTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for FullName
        ///</summary>
        [TestMethod()]
        public void FullNameFunctionTest()
        {
            Namespace ns = storage.namespaces.FindOrCreate(new string[] { "ns1", "ns2" });

            IFunction f1 = storage.functions.AddFunction();
            f1.Name = "f1";
            ns.AddFunctionToNamespace(f1);

            Assert.IsTrue(f1.FullName == "ns1.ns2.f1");

            IFunction f2 = storage.functions.AddFunction();
            f2.Name = "f2";
            Assert.IsTrue(f2.FullName == "f2");

            Class c = storage.classes.AddClass();
            IFunction f3 = c.AddMethod(true);
            ns.AddClassToNamespace(c);
            c.Name = "c1";
            f3.Name = "f3";

            Assert.IsTrue(f3.FullName == "ns1.ns2.c1.f3");
        }

        /// <summary>
        ///A test for IsExternal
        ///</summary>
        [TestMethod()]
        public void IsExternalTest()
        {
            IFunction func = storage.functions.AddFunction();
            func.Name = "1";

            Assert.IsTrue(func.IsExternal == false);

            func.IsExternal = true;
            Assert.IsTrue(func.IsExternal == true);

            testUtils.Storage_Reopen(ref storage);

            func = storage.functions.GetFunction(1);
            Assert.IsTrue(func.IsExternal == true);

            func.IsExternal = false;
            Assert.IsTrue(func.IsExternal == false);
        }

        /// <summary>
        ///A test for IsExternal
        ///</summary>
        [TestMethod()]
        public void FunctionReturnTypeTest()
        {
            IFunction func = storage.functions.AddFunction();
            func.Name = "1";

            Assert.IsTrue(func.FunctionReturnType == "");

            func.FunctionReturnType = "sdfsdf";

            Assert.IsTrue(func.FunctionReturnType == "sdfsdf");

            testUtils.Storage_Reopen(ref storage);

            func = storage.functions.GetFunction(1);
            Assert.IsTrue(func.FunctionReturnType == "sdfsdf");
        }
    }
}
