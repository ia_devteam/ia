﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementDoAndWhileTest and is intended
    ///To contain all IStatementDoAndWhileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementDoAndWhileTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for Body
        ///</summary>
        [TestMethod()]
        public void BodyTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementDoAndWhile d = storage.statements.AddStatementDoAndWhile(1, file);
            IStatement st = storage.statements.AddStatementGoto(1, file);

            Assert.IsTrue(d.Body == null);

            d.Body = st;

            Assert.IsTrue(d.Body is IStatementGoto);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementDoAndWhile).Body is IStatementGoto);
        }

        /// <summary>
        ///A test for Condition
        ///</summary>
        [TestMethod()]
        public void ConditionTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementDoAndWhile d = storage.statements.AddStatementDoAndWhile(1, file);
            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();

            Assert.IsTrue(d.Condition == null);

            d.Condition = op2;

            Assert.IsTrue(d.Condition.Id == 2);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementDoAndWhile).Condition.Id == 2);
        }

        /// <summary>
        ///A test for IsCheckConditionBeforeFirstRun
        ///</summary>
        [TestMethod()]
        public void IsCheckConditionBeforeFirstRunTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementDoAndWhile d = storage.statements.AddStatementDoAndWhile(1, file);

            Assert.IsTrue(d.IsCheckConditionBeforeFirstRun == false);

            d.IsCheckConditionBeforeFirstRun = true;
            Assert.IsTrue(d.IsCheckConditionBeforeFirstRun == true);

            testUtils.Storage_Reopen(ref storage);
            d = (IStatementDoAndWhile) storage.statements.GetStatement(1);

            Assert.IsTrue(d.IsCheckConditionBeforeFirstRun == true);

            d.IsCheckConditionBeforeFirstRun = false;
            Assert.IsTrue(d.IsCheckConditionBeforeFirstRun == false);

            testUtils.Storage_Reopen(ref storage);
            d = (IStatementDoAndWhile)storage.statements.GetStatement(1);

            Assert.IsTrue(d.IsCheckConditionBeforeFirstRun == false);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void DoAndWhileSubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatementDoAndWhile d = storage.statements.AddStatementDoAndWhile(1, file);

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);

            IStatementBreak c = storage.statements.AddStatementBreak(1, file);
            d.Body = c;

            int i = 0;
            foreach(IStatement st in d.SubStatements())
            {
                switch(i)
                {
                    case 0:
                        Assert.IsTrue(st is IStatementBreak);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 1);
        }
    }
}
