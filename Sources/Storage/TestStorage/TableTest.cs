﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store.Table;

using IOController;

namespace TestStorage
{
    /// <summary>
    /// Summary description for TableTest
    /// </summary>
    [TestClass]
    public class TableTest
    {
        private TestUtils.TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize() 
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);
        }

        [TestCleanup]
        public void MyTestCleanUp()
        {
            //Удаляем рабочую директорию теста
            DirectoryController.Remove(testUtils.TestRunDirectoryPath);
        }

        [TestMethod]
        public void Table_Operations()
        {
            DB_Operations ops = new DB_Operations();
            ops.Create(testUtils.TestRunDirectoryPath);
            ops.Close();

            Assert.IsFalse(System.IO.Directory.Exists("table"), "message1");

            ops.Open(testUtils.TestRunDirectoryPath);

            Assert.IsFalse(ops.IsDBExists(testUtils.TestRunDirectoryPath), "message2");

            TableIDDuplicate tbl = ops.GetTableIDDuplicate("table");
            IBufferWriter wr = WriterPool.Get();
            wr.Add("ast");
            tbl.Put(1, wr);
            tbl.Close();
            Assert.IsTrue(ops.IsDBExists("table"), "message4");

            ops.DeleteDB("table");

            Assert.IsFalse(ops.IsDBExists("table"), "message3");
            ops.Close();
        }

        [TestMethod]
        public void Table_TableIDNoDuplicate()
        {
            DB_Operations ops = new DB_Operations();
            ops.Create(testUtils.TestRunDirectoryPath);

            TableIDNoDuplicate table = ops.GetTableIDNoDuplicate("name");
            IBufferWriter writer = WriterPool.Get();
            writer.Add((UInt64) 123);
            table.Add(writer);
            table.Close();

            table = ops.GetTableIDNoDuplicate("name");
            IBufferReader reader;
            table.Get(1, out reader);
            Assert.IsTrue(reader.GetUInt64() == 123, "message 1");

            writer = WriterPool.Get();
            writer.Add((UInt64)456);
            table.Add(writer);

            IEnumerator<KeyValuePair<UInt64, IBufferReader>> en = ((IEnumerable<KeyValuePair<UInt64, IBufferReader>>)table).GetEnumerator();
            Assert.IsTrue(en.MoveNext(), "mes 2");
            Assert.IsTrue(en.Current.Key == 1, "mes 3");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 123, "mes 4");
            Assert.IsTrue(en.MoveNext(), "mes 5");
            Assert.IsTrue(en.Current.Key == 2, "mes 6");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 456, "mes 7");
            Assert.IsFalse(en.MoveNext(), "mes 8");
            en = null;

            writer = WriterPool.Get();
            writer.Add((UInt64)789);
            table.Put(10, writer);

            Assert.IsTrue(table.Get(10, out reader) == enResult.OK, "mess 12");
            Assert.IsTrue(reader.GetUInt64() == 789, "message 9");

            table.Remove(10);
            Assert.IsTrue(table.Get(10, out reader) == enResult.NOT_FOUND, "mess 10");

            writer = WriterPool.Get();
            writer.Add((UInt64)456);
            Assert.IsTrue(table.Contains(2, writer), "mess 14");

            Assert.IsFalse(table.Contains(1, writer), "mess 15");

            Assert.IsTrue(table.Count() == 2, "mess 16");

            table.Close();
            ops.Close();
        }

        [TestMethod]
        public void Table_TableIDdublicate()
        {
            DB_Operations ops = new DB_Operations();
            ops.Create(testUtils.TestRunDirectoryPath);

            TableIDDuplicate table = ops.GetTableIDDuplicate("name");
            IBufferWriter writer = WriterPool.Get();
            writer.Add((UInt64)123);
            table.Put(1, writer);
            table.Close();

            table = ops.GetTableIDDuplicate("name");
            IEnumerator<IBufferReader> en1 = table.Get(1).GetEnumerator();
            Assert.IsTrue(en1.MoveNext(), "mess 13");
            Assert.IsTrue(en1.Current.GetUInt64() == 123, "message 1");
            Assert.IsFalse(en1.MoveNext(), "mes 8");
            
            writer = WriterPool.Get();
            writer.Add((UInt64)456);
            table.Put(2, writer);

            IEnumerator<KeyValuePair<UInt64, IBufferReader>> en = ((IEnumerable<KeyValuePair<UInt64, IBufferReader>>)table).GetEnumerator();
            Assert.IsTrue(en.MoveNext(), "mes 2");
            Assert.IsTrue(en.Current.Key == 1, "mes 3");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 123, "mes 4");
            Assert.IsTrue(en.MoveNext(), "mes 5");
            Assert.IsTrue(en.Current.Key == 2, "mes 6");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 456, "mes 7");
            Assert.IsFalse(en.MoveNext(), "mes 8");
            en = null;

            writer = WriterPool.Get();
            writer.Add((UInt64)789);
            table.Put(10, writer);

            en1 = table.Get(10).GetEnumerator();
            Assert.IsTrue(en1.MoveNext(), "mess 12");
            Assert.IsTrue(en1.Current.GetUInt64() == 789, "message 9");

            table.Remove(10, writer);
            Assert.IsFalse(table.Get(10).GetEnumerator().MoveNext(), "mess 10");

            //Перейдем к тестированию дублей
            writer = WriterPool.Get();
            writer.Add((UInt64)369);
            table.Put(1, writer);

            en = ((IEnumerable<KeyValuePair<UInt64, IBufferReader>>)table).GetEnumerator();
            Assert.IsTrue(en.MoveNext(), "mes 14");
            
            UInt64 val = en.Current.Value.GetUInt64();
            Assert.IsTrue(val == 123, "mes 19");
            Assert.IsTrue(en.Current.Key == 1, "mes 15");
            Assert.IsTrue(en.MoveNext(), "mes 20");

            Assert.IsTrue(en.Current.Key == 1, "mes 18");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 369, "mes 22");
            Assert.IsTrue(en.MoveNext(), "mes 17");

            val = en.Current.Value.GetUInt64();
            Assert.IsTrue(val == 456, "mes 16");
            Assert.IsTrue(en.Current.Key == 2, "mes 21");

            Assert.IsFalse(en.MoveNext(), "mes 23");
            en = null;

            en1 = table.Get(1).GetEnumerator();
            Assert.IsTrue(en1.MoveNext(), "mess 24");
            Assert.IsTrue(en1.Current.GetUInt64() == 123, "message 25");
            Assert.IsTrue(en1.MoveNext(), "mess 26");
            Assert.IsTrue(en1.Current.GetUInt64() == 369, "message 27");
            Assert.IsFalse(en1.MoveNext(), "mes 28");

            writer = WriterPool.Get();
            writer.Add((UInt64)123);
            table.Remove(1, writer);

            en1 = table.Get(1).GetEnumerator();
            Assert.IsTrue(en1.MoveNext(), "mess 29");
            Assert.IsTrue(en1.Current.GetUInt64() == 369, "message 30");
            Assert.IsFalse(en1.MoveNext(), "mes 31");

            writer = WriterPool.Get();
            writer.Add((UInt64)456);
            Assert.IsTrue(table.Contains(2, writer));
            Assert.IsFalse(table.Contains(1, writer));

            table.Close();
            ops.Close();
        }

        [TestMethod]
        public void Table_TableIndexedNoDuplicates()
        {
            DB_Operations ops = new DB_Operations();
            ops.Create(testUtils.TestRunDirectoryPath);

            TableIndexedNoDuplicates table = ops.GetTableIndexedNoDuplicates("name");
            IBufferWriter writer = WriterPool.Get();
            writer.Add((UInt64)123);
            IBufferWriter writer1 = WriterPool.Get();
            writer1.Add((UInt64)1);
            table.Put(writer1, writer);
            table.Close();

            table = ops.GetTableIndexedNoDuplicates("name");
            IBufferReader reader;
            table.Get(writer1, out reader);
            Assert.IsTrue(reader.GetUInt64() == 123, "message 1");

            writer = WriterPool.Get();
            writer.Add((UInt64)456);
            writer1 = WriterPool.Get();
            writer1.Add((UInt64)2);
            table.Put(writer1, writer);

            IEnumerator<KeyValuePair<IBufferReader, IBufferReader>> en = ((IEnumerable<KeyValuePair<IBufferReader, IBufferReader>>)table).GetEnumerator();
            Assert.IsTrue(en.MoveNext(), "mes 2");
            Assert.IsTrue(en.Current.Key.GetUInt64() == 1, "mes 3");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 123, "mes 4");
            Assert.IsTrue(en.MoveNext(), "mes 5");
            Assert.IsTrue(en.Current.Key.GetUInt64() == 2, "mes 6");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 456, "mes 7");
            Assert.IsFalse(en.MoveNext(), "mes 8");
            en = null;

            writer = WriterPool.Get();
            writer.Add((UInt64)789);
            writer1 = WriterPool.Get();
            writer1.Add((UInt64)10);
            table.Put(writer1, writer);

            writer1 = WriterPool.Get();
            writer1.Add((UInt64)10);
            Assert.IsTrue(table.Get(writer1, out reader) == enResult.OK, "mess 12");
            Assert.IsTrue(reader.GetUInt64() == 789, "message 9");

            Assert.IsTrue(table.Contains(writer1, writer));
            writer = WriterPool.Get();
            writer.Add((UInt64)456);
            Assert.IsFalse(table.Contains(writer1, writer));

            table.Remove(writer1);
            Assert.IsTrue(table.Get(writer1, out reader) == enResult.NOT_FOUND, "mess 10");

            table.Close();
            ops.Close();
        }

        [TestMethod]
        public void Table_TableIndexedNoDuplicates_GetStartFrom()
        {
            DB_Operations ops = new DB_Operations();
            ops.Create(testUtils.TestRunDirectoryPath);

            TableIndexedNoDuplicates table = ops.GetTableIndexedNoDuplicates("name");
            IBufferWriter writer = WriterPool.Get();
            writer.AddTransformed((UInt64)123);
            IBufferWriter writer1 = WriterPool.Get();
            writer1.AddTransformed((UInt64)1);
            IBufferWriter writer3 = WriterPool.Get();
            writer3.AddTransformed((UInt64)10);
            table.Put(writer1, writer);
            table.Put(writer3, writer);
            table.Close();

            table = ops.GetTableIndexedNoDuplicates("name");
            IBufferReader reader;
            IBufferWriter writer2 = WriterPool.Get();
            writer2.AddTransformed((UInt64)2);
            Assert.IsTrue(table.GetStartFrom(writer2, out reader) == enResult.OK);
            Assert.IsTrue(reader.GetUInt64Transformed() == 123);

            table.Close();
            ops.Close();
        }


        [TestMethod]
        public void Table_TableIndexedDuplicates()
        {
            DB_Operations ops = new DB_Operations();
            ops.Create(testUtils.TestRunDirectoryPath);

            TableIndexedDuplicates table = ops.GetTableIndexedDuplicates("name");
            IBufferWriter writer = WriterPool.Get();
            writer.Add((UInt64)123);
            IBufferWriter writer1 = WriterPool.Get();
            writer1.Add((UInt64)1);
            table.Put(writer1, writer);
            table.Close();

            table = ops.GetTableIndexedDuplicates("name");
            writer1 = WriterPool.Get();
            writer1.Add((UInt64)1);
            IEnumerator<IBufferReader> en1 = table.Get(writer1).GetEnumerator();
            Assert.IsTrue(en1.MoveNext(), "mess 13");
            Assert.IsTrue(en1.Current.GetUInt64() == 123, "message 1");
            Assert.IsFalse(en1.MoveNext(), "mes 8");

            writer = WriterPool.Get();
            writer.Add((UInt64)456);
            writer1 = WriterPool.Get();
            writer1.Add((UInt64)2);
            table.Put(writer1, writer);

            IEnumerator<KeyValuePair<IBufferReader, IBufferReader>> en = ((IEnumerable<KeyValuePair<IBufferReader, IBufferReader>>)table).GetEnumerator();
            Assert.IsTrue(en.MoveNext(), "mes 2");
            Assert.IsTrue(en.Current.Key.GetUInt64() == 1, "mes 3");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 123, "mes 4");
            Assert.IsTrue(en.MoveNext(), "mes 5");
            Assert.IsTrue(en.Current.Key.GetUInt64() == 2, "mes 6");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 456, "mes 7");
            Assert.IsFalse(en.MoveNext(), "mes 8");
            en = null;

            writer = WriterPool.Get();
            writer.Add((UInt64)789);
            writer1 = WriterPool.Get();
            writer1.Add((UInt64)10);
            table.Put(writer1, writer);

            en1 = table.Get(writer1).GetEnumerator();
            Assert.IsTrue(en1.MoveNext(), "mess 12");
            Assert.IsTrue(en1.Current.GetUInt64() == 789, "message 9");

            table.Remove(writer1, writer);
            Assert.IsFalse(table.Get(writer1).GetEnumerator().MoveNext(), "mess 10");

            //Перейдем к тестированию дублей
            writer = WriterPool.Get();
            writer.Add((UInt64)369);
            writer1 = WriterPool.Get();
            writer1.Add((UInt64)1);
            table.Put(writer1, writer);

            en = ((IEnumerable<KeyValuePair<IBufferReader, IBufferReader>>)table).GetEnumerator();
            Assert.IsTrue(en.MoveNext(), "mes 14");

            UInt64 val = en.Current.Value.GetUInt64();
            Assert.IsTrue(val == 123, "mes 19");
            Assert.IsTrue(en.Current.Key.GetUInt64() == 1, "mes 15");
            Assert.IsTrue(en.MoveNext(), "mes 20");

            Assert.IsTrue(en.Current.Key.GetUInt64() == 1, "mes 18");
            Assert.IsTrue(en.Current.Value.GetUInt64() == 369, "mes 22");
            Assert.IsTrue(en.MoveNext(), "mes 17");

            val = en.Current.Value.GetUInt64();
            Assert.IsTrue(val == 456, "mes 16");
            Assert.IsTrue(en.Current.Key.GetUInt64() == 2, "mes 21");

            Assert.IsFalse(en.MoveNext(), "mes 23");
            en = null;

            writer1 = WriterPool.Get();
            writer1.Add((UInt64)1);
            en1 = table.Get(writer1).GetEnumerator();
            Assert.IsTrue(en1.MoveNext(), "mess 24");
            Assert.IsTrue(en1.Current.GetUInt64() == 123, "message 25");
            Assert.IsTrue(en1.MoveNext(), "mess 26");
            Assert.IsTrue(en1.Current.GetUInt64() == 369, "message 27");
            Assert.IsFalse(en1.MoveNext(), "mes 28");

            writer = WriterPool.Get();
            writer.Add((UInt64)123);
            writer1 = WriterPool.Get();
            writer1.Add((UInt64)1);
            Assert.IsTrue(table.Contains(writer1, writer));
            table.Remove(writer1, writer);
            Assert.IsFalse(table.Contains(writer1, writer));

            en1 = table.Get(writer1).GetEnumerator();
            Assert.IsTrue(en1.MoveNext(), "mess 29");
            Assert.IsTrue(en1.Current.GetUInt64() == 369, "message 30");
            Assert.IsFalse(en1.MoveNext(), "mes 31");

            table.Close();
            ops.Close();
        }

        [TestMethod]
        public void BufferReaderWriter()
        {
            byte[] arr1 = new byte[]{12,15,18};
            byte[] arr2 = new byte[] { 55, 66, 77 };
            Store.Table.IBufferWriter ibw = Store.Table.WriterPool.Get();
            ibw.Add(arr1);
            ibw.Add(arr2);

            stBufferRead reader = new stBufferRead(ibw as stBufferWrite);

            byte[] arr3 = reader.GetByteArray();
            byte[] arr4 = reader.GetByteArray();

            TestUtils.TestUtilsClass.CheckArray((a,b) => a==b,  arr1, arr3);
        }

    }
}
