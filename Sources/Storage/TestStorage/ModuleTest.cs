﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for ModuleTest and is intended
    ///To contain all ModuleTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ModuleTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }


        /// <summary>
        ///A test for ImplementsClasses
        ///</summary>
        [TestMethod()]
        public void ImplementsClassesTest()
        {
            storage.appliedSettings.SetFileListPath("C:");

            Store.Module mod1 = storage.modules.AddModule(@"c:\1", enFileKindForAdd.externalFile);
            Store.Module mod2 = storage.modules.AddModule(@"c:\2", enFileKindForAdd.externalFile);
            Class cl = storage.classes.AddClass();

            Assert.IsTrue(cl.ImplementedInModule == null);
            TestUtilsClass.CheckArray<Class>((m1, m2) => { return m1.Id == m2.Id; }, mod1.ImplementsClasses(), new Class[] { });

            mod1.AddImplementedClass(cl);

            Assert.IsTrue(cl.ImplementedInModule == mod1);
            TestUtilsClass.CheckArray<Class>((m1, m2) => { return m1.Id == m2.Id; }, mod1.ImplementsClasses(), new Class[] { cl });

            try
            {
                mod2.AddImplementedClass(cl);
                Assert.Fail();
            }
            catch
            { }

        
        }

        /// <summary>
        ///A test for ImplementsFunctions
        ///</summary>
        [TestMethod()]
        public void ImplementsFunctionsTest()
        {
            storage.appliedSettings.SetFileListPath("C:");

            Store.Module mod1 = storage.modules.AddModule(@"c:\1", enFileKindForAdd.externalFile);
            Store.Module mod2 = storage.modules.AddModule(@"c:\2", enFileKindForAdd.externalFile);
            IFunction cl = storage.functions.AddFunction();

            Assert.IsTrue(cl.ImplementedInModule == null);
            TestUtilsClass.CheckArray<IFunction>((m1, m2) => { return m1.Id == m2.Id; }, mod1.ImplementsFunctions(), new IFunction[] { });

            mod1.AddImplementedFunction(cl);

            Assert.IsTrue(cl.ImplementedInModule == mod1);
            TestUtilsClass.CheckArray<IFunction>((m1, m2) => { return m1.Id == m2.Id; }, mod1.ImplementsFunctions(), new IFunction[] { cl });

            //Запрещено указывать реализацию функции сразу в двух модулях
            try
            {
                mod2.AddImplementedFunction(cl);
                Assert.Fail();
            }
            catch
            { }

            //Проверка корректности работы для методов
            
            //Прямо указать модуль для метода запрещено
            IMethod m = storage.functions.AddMethod();
            try
            {
                mod1.AddImplementedFunction(m);
                Assert.Fail();
            }
            catch
            { }

            //Метод может быть установлен при создании метода
            Class clazz = storage.classes.AddClass();
            mod1.AddImplementedClass(clazz);
            IMethod meth = clazz.AddMethod(true);
            Assert.IsTrue(meth.ImplementedInModule == mod1);

            //Метод может назначаться при указании модуля классу
            Class clazz1 = storage.classes.AddClass();
            IMethod meth1 = clazz1.AddMethod(false);
            mod1.AddImplementedClass(clazz1);
            Assert.IsTrue(meth1.ImplementedInModule == mod1);
        }

        /// <summary>
        ///A test for ImplementsGlobalVariables
        ///</summary>
        [TestMethod()]
        public void ImplementsGlobalVariablesTest()
        {
            storage.appliedSettings.SetFileListPath("C:");

            Store.Module mod1 = storage.modules.AddModule(@"c:\1", enFileKindForAdd.externalFile);
            Store.Module mod2 = storage.modules.AddModule(@"c:\2", enFileKindForAdd.externalFile);
            Variable cl = storage.variables.AddVariable();
            cl.isGlobal = true;

            Assert.IsTrue(cl.ImplementedInModule == null);
            TestUtilsClass.CheckArray<Variable>((m1, m2) => { return m1.Id == m2.Id; }, mod1.ImplementsGlobalVariables(), new Variable[] { });

            mod1.AddImplementedGlobalVariable(cl);

            Assert.IsTrue(cl.ImplementedInModule == mod1);
            TestUtilsClass.CheckArray<Variable>((m1, m2) => { return m1.Id == m2.Id; }, mod1.ImplementsGlobalVariables(), new Variable[] { cl });

            //Одна и та же переменная не может находится в двух классах одновременно
            try
            {
                mod2.AddImplementedGlobalVariable(cl);
                Assert.Fail();
            }
            catch
            { }

    //Локальные переменные

            //Локальным переменным нельзя указывать модули. Это делается автоматически
            Variable cl1 = storage.variables.AddVariable();
            cl1.isGlobal = false;
            try
            {
                mod1.AddImplementedGlobalVariable(cl1);
                Assert.Fail();
            }
            catch
            { }

            //Если функция, в которой находится определение переменной, уже имеет модуль, то переменной должен быть установлен модуль
            IFunction func = storage.functions.AddFunction();
            mod1.AddImplementedFunction(func);
            Variable v = storage.variables.AddVariable();
            v.isGlobal = false;
            v.AddDefinition(@"c:\1", 1, func);
            Assert.IsTrue(v.ImplementedInModule == mod1);

            //Если функции, в которой реализована переменная, устанавливается модуль, то он должен быть установлен и переменной
            IFunction func1 = storage.functions.AddFunction();
            Variable v1 = storage.variables.AddVariable();
            v1.isGlobal = false;
            v1.AddDefinition(@"c:\1", 1, func1);
            mod1.AddImplementedFunction(func1);
            Assert.IsTrue(v1.ImplementedInModule == mod1);



    //Поля классов 

            //Полям классов нельзя указывать модули. Это делается автоматически
            Variable cl2 = storage.variables.AddField();
            try
            {
                mod1.AddImplementedGlobalVariable(cl2);
                Assert.Fail();
            }
            catch
            { }

            //Модуль может быть установлен при создании поля
            Class clazz = storage.classes.AddClass();
            mod1.AddImplementedClass(clazz);
            Field f1 = clazz.AddField(true);
            Assert.IsTrue(f1.ImplementedInModule == mod1);

            //Поле может назначаться при указании модуля классу
            Class clazz1 = storage.classes.AddClass();
            Field f2 = clazz1.AddField(false);
            mod1.AddImplementedClass(clazz1);
            Assert.IsTrue(f1.ImplementedInModule == mod1);
        }
    }
}
