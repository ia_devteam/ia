﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;
using System.Linq;

namespace TestStorage
{
    /// <summary>
    /// Summary description for Files
    /// </summary>
    [TestClass]
    public class FilesTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize() 
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup() 
        {
            testUtils.Storage_Remove(ref storage);
        }

        [TestMethod]
        public void File1()
        {
            string fileName = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.rrr");

            Files files = storage.files;
 
            //тестируем сохраняемость
            IFile file = files.Add(enFileKindForAdd.fileWithPrefix);
            UInt64 ID = file.Id;
            
            file = null;
            files = null;
            testUtils.Storage_Reopen(ref storage);

            files = storage.files;
            file = files.GetFile(ID);

            //Тестируем сохраняемость полей
            file.FullFileName_Original = fileName;

            file = null;
            testUtils.Storage_Reopen(ref storage);
            files = storage.files;
            file = files.GetFile(ID);

            Assert.IsTrue(file.FullFileName_Canonized == fileName, "mes 2");

            //Тестируем корректность автоматических полей
            Assert.IsTrue(file.Extension == "rrr", "mes 1");
        }

        [TestMethod]
        public void FunctionsDefinedInFile()
        {
            IFile file1 = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            IFile file2 = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            IFile file3 = storage.files.Add(enFileKindForAdd.fileWithPrefix);

            file1.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file1.txt");
            file2.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file2.txt");
            file3.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file3.txt");


            IFunction func1 = storage.functions.AddFunction();
            IFunction func2 = storage.functions.AddFunction();
            IFunction func3 = storage.functions.AddFunction();
            IFunction func4 = storage.functions.AddFunction();
            IFunction func5 = storage.functions.AddFunction();
            IFunction func6 = storage.functions.AddFunction();
            IFunction func7 = storage.functions.AddFunction();
            IFunction func8 = storage.functions.AddFunction();

            func1.AddDefinition(file1, 1);
            func2.AddDefinition(file2, 4);
            func3.AddDefinition(file2, 1);
            func4.AddDefinition(file2, 10);
            func5.AddDefinition(file3, 1);
            func6.AddDefinition(file3, 4);
            func7.AddDefinition(file2, 5);

            int i = 0;
            foreach (IFunction func in file1.FunctionsDefinedInTheFile())
            {
                switch(i++)
                {
                    case 0: Assert.IsTrue(func.Id == 1);
                        break;
                    default: Assert.Fail();
                        break;
                }
            }
            Assert.IsTrue(i == 1);

            i = 0;
            foreach (IFunction func in file2.FunctionsDefinedInTheFile())
            {
                switch (i++)
                {
                    case 0: Assert.IsTrue(func.Id == 3);
                        break;
                    case 1: Assert.IsTrue(func.Id == 2);
                        break;
                    case 2: Assert.IsTrue(func.Id == 7);
                        break;
                    case 3: Assert.IsTrue(func.Id == 4);
                        break;
                    default: Assert.Fail();
                        break;
                }
            }
            Assert.IsTrue(i == 4);


            i = 0;
            foreach (IFunction func in file3.FunctionsDefinedInTheFile())
            {
                switch (i++)
                {
                    case 0: Assert.IsTrue(func.Id == 5);
                        break;
                    case 1: Assert.IsTrue(func.Id == 6);
                        break;
                    default: Assert.Fail();
                        break;
                }
            }
            Assert.IsTrue(i == 2);
        }

        [TestMethod]
        public void FileLabled()
        {
            Files files = storage.files;

            //тестируем сохраняемость
            IFile file = files.Add(enFileKindForAdd.fileWithPrefix);
            UInt64 ID = file.Id;
            IFile label = files.GetFile(ID);

            file = null;
            label = null;
            files = null;
            testUtils.Storage_Reopen(ref storage);

            files = storage.files;
            label = files.GetFile(ID);
            
            //Тестируем сохраняемость полей
            label.fileType = SpecialFileTypes.COMMON_XML;
            label.FileEssential = enFileEssential.SUPERFLUOUS;
            label.FileExistence = enFileExistence.EXISTS_AND_UNKNOWN;

            label = null;
            testUtils.Storage_Reopen(ref storage);
            files = storage.files;
            label = files.GetFile(ID);

            Assert.IsTrue(label.fileType == SpecialFileTypes.COMMON_XML, "mes 2");
            Assert.IsTrue(label.FileEssential == enFileEssential.SUPERFLUOUS, "mes 3");
            Assert.IsTrue(label.FileExistence == enFileExistence.EXISTS_AND_UNKNOWN, "mes 4");

            //Тестируем корректность автоматических полей

            //Тестируем удаление файлов без создания FileLabeled
            file = files.GetFile(ID);
            file.RemoveMe();
        }


        [TestMethod]
        public void TestFiles()
        {
        //File
            Files files = storage.files;
            files.Add(enFileKindForAdd.fileWithPrefix);
            files.Add(enFileKindForAdd.fileWithPrefix);
            files.Add(enFileKindForAdd.fileWithPrefix);
            files.GetFile(1);
            files.GetFile(2);

            UInt64 i=1;
            foreach (IFile file in files.EnumerateFiles(enFileKind.anyFile))
            {
                Assert.IsTrue(file.Id == i, "mess 1, " + i.ToString());
                i++;
            }

            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();
            files.GetFile(1).RemoveMe();

            i = 2;
            foreach (IFile file in files.EnumerateFiles(enFileKind.anyFile))
            {
                Assert.IsTrue(file.Id == i, "mess 2, " + i.ToString());
                i++;
            }

            files.Clear();

            IEnumerator<IFile> en = files.EnumerateFiles(enFileKind.anyFile).GetEnumerator();
            Assert.IsFalse(en.MoveNext(), "mess 3");

        }

        [TestMethod]
        public void FileTypes()
        {
            TestUtils.TestUtilsClass.SQL_SetupTestConnection();

            //File
            Files files = storage.files;
            files.Add(enFileKindForAdd.fileWithPrefix);
            files.Add(enFileKindForAdd.fileWithPrefix);
            files.Add(enFileKindForAdd.fileWithPrefix);
            IFile label = files.GetFile(1);
            label.fileType = SpecialFileTypes.PHP_SOURCE;
            label = files.GetFile(2);
            label.fileType = SpecialFileTypes.JAVA_SOURCE;
            label = files.GetFile(3);
            label.fileType = SpecialFileTypes.MDB;

            label = null;
            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Store.FileType[] enumer = files.UsedFileTypes();

            Assert.IsTrue(enumer.Length == 3);
            Assert.IsFalse(enumer.Contains(SpecialFileTypes.PHP_SOURCE), "mes 222");
            Assert.IsFalse(enumer.Contains(SpecialFileTypes.JAVA_SOURCE), "mes 2");
            Assert.IsFalse(enumer.Contains(SpecialFileTypes.MDB), "mes 4");

            files.Add(enFileKindForAdd.fileWithPrefix);
            label = files.GetFile(4);
            label.fileType = SpecialFileTypes.MDB;

            
            label = null;
            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            enumer = files.UsedFileTypes();

            Assert.IsTrue(enumer.Length == 3, "mes 11");
            Assert.IsFalse(enumer.Contains(SpecialFileTypes.PHP_SOURCE), "mes 2232");
            Assert.IsFalse(enumer.Contains(SpecialFileTypes.JAVA_SOURCE), "mes 24");
            Assert.IsFalse(enumer.Contains(SpecialFileTypes.MDB), "mes 467");

            files.GetFile(1).RemoveMe();

            Assert.IsTrue(SpecialFileTypes.BORLAND_DFM.Extensions().Count == 1);
            Assert.IsTrue(SpecialFileTypes.BORLAND_DFM.Extensions()[0] == "dfm");
        }

        [TestMethod]
        public void FileNamesIndex()
        {
            string name1 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "aaa");
            string name2 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "bbb");
            string name3 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "ccc");
            string name4 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "kkkk");
            string nameWrong = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "sss");

            //File
            Files files = storage.files;
            IFile file = files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = name1;
            file = files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, name2);
            file = files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, name3);

            file = null;
            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Assert.IsTrue(files.Find(name1).Id == 1, "mes 1");
            Assert.IsTrue(files.Find(name2).Id == 2, "mes 2");
            Assert.IsTrue(files.Find(name3).Id == 3, "mes 3");
            Assert.IsTrue(files.Find(nameWrong) == null, "mes 4");

            file = files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, name4);


            file = null;
            GC.Collect(GC.MaxGeneration);
            GC.WaitForPendingFinalizers();

            Assert.IsTrue(files.Find(name1).Id == 1, "mes 4");
            Assert.IsTrue(files.Find(name2).Id == 2, "mes 5");
            Assert.IsTrue(files.Find(name3).Id == 3, "mes 6");
            Assert.IsTrue(files.Find(name4).Id == 4, "mes 7");
            Assert.IsTrue(files.Find(nameWrong) == null, "mes 8");

            files.GetFile(1).RemoveMe();
        }

        [TestMethod]
        public void FilesIndexTest()
        {
            TestUtils.TestUtilsClass.SQL_SetupTestConnection();

            string name1 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "aaa");
            string name2 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "bbb");
            string name3 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "ccc");
            string name4 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "aaa");


            Files files = storage.files;
            IFile file1 = files.Add(enFileKindForAdd.fileWithPrefix);
            file1.FullFileName_Original = name1;
            IFile file2 = files.Add(enFileKindForAdd.fileWithPrefix);
            file2.FullFileName_Original = name2;
            IFile file3 = files.Add(enFileKindForAdd.fileWithPrefix);
            file3.FullFileName_Original = name3;


            FilesIndex filesIndex = storage.filesIndex;

            filesIndex.Add(file1, "qqq", 10);
            filesIndex.Add(file1, "qqq", 12);
            filesIndex.Add(file2, "qqq", 14);
            filesIndex.Add(file3, "ttt", 14);

            UInt64[] i={10,12,14};
            UInt64[] k={file1.Id,file1.Id,file2.Id};
            int j = 0;
            foreach (FilesIndex.Occurrence occ in filesIndex.Find("qqq"))
            {
                Assert.IsTrue(occ.FileID == k[j], "mes 1 " + j.ToString());
                Assert.IsTrue(occ.Offset == i[j], "mes 2 " + j.ToString());
                j++;
            }

            filesIndex.Clear();

            foreach (FilesIndex.Occurrence occ in filesIndex.Find("qqq"))
            {
                Assert.IsTrue(false, "mes 3 ");
                j++;
            }

        }

        /// <summary>
        /// Тест проверяет разделение на внешние и внутренние файлы. Тестирует запреты.
        /// </summary>
        [TestMethod]
        public void TestFileConsistence()
        {
            //Проверяем, что задать имя без префикса файлу, для которого декларирован префикс - нельзя
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            bool res = true;
            try
            {
                file.FullFileName_Original = "sdfsdfdsf";
            }
            catch(Store.Const.StorageException ex)
            {
                res = false;
            }
            Assert.IsFalse(res);


            //Аналогично проверяем второй метод FindOrGenerate
            res = true;
            try
            {
            file = storage.files.FindOrGenerate("sdfsdf", enFileKindForAdd.fileWithPrefix);
            }
            catch(Store.Const.StorageException ex)
            {
                res = false;
            }
            Assert.IsFalse(res);
        }


        /// <summary>
        /// Тест проверяет разделение на внешние и внутренние файлы. Тестирует разделение.
        /// </summary>
        [TestMethod]
        public void TestFileDuality()
        {
            string name1 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "aaa");
            string name2 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "bbb");


            storage.files.FindOrGenerate(name1, enFileKindForAdd.fileWithPrefix);
            storage.files.FindOrGenerate(name2, enFileKindForAdd.externalFile);

            Assert.IsTrue(storage.files.EnumerateFiles(enFileKind.externalFile).ToArray().Select(a => a.FullFileName_Original).SequenceEqual(new string[] { name2 }));
            Assert.IsTrue(storage.files.EnumerateFiles(enFileKind.fileWithPrefix).ToArray().Select(a => a.FullFileName_Original).SequenceEqual(new string[] { name1 }));
            Assert.IsTrue(storage.files.EnumerateFiles(enFileKind.anyFile).ToArray().Select(a => a.FullFileName_Original).SequenceEqual(new string[] { name1, name2 }));

        }

        /// <summary>
        /// Тестируем то, как хранилище распознаёт копии файлов. Этот функционал задевает работу с контрольными суммами файлов
        /// </summary>
        [TestMethod]
        public void FileCopiesANDCheckSums()
        {
            string Folder = System.IO.Path.Combine(testUtils.TestMatirialsDirectoryPath, @"Storage.Files\");
            storage.appliedSettings.SetFileListPath(Folder);

            storage.files.FindOrGenerate(System.IO.Path.Combine(Folder, "LabelWindow.js"), enFileKindForAdd.fileWithPrefix);
            storage.files.FindOrGenerate(System.IO.Path.Combine(Folder, "Label.js"), enFileKindForAdd.fileWithPrefix);
            storage.files.FindOrGenerate(System.IO.Path.Combine(Folder, "LabelWinw.j"), enFileKindForAdd.fileWithPrefix);
            storage.files.FindOrGenerate(System.IO.Path.Combine(Folder, @"tr\LabelWindow.js"), enFileKindForAdd.fileWithPrefix);


            Assert.IsTrue(storage.files.EnumerateUniqFiles(enFileKind.fileWithPrefix).Count() == 2);
            Check();

            Assert.IsTrue(storage.files.EnumerateUniqFiles(enFileKind.fileWithPrefix).Count() == 2);
            Check();
        }

        private void Check()
        {
            string[] list = { "LabelWindow.js", "LabelWinw.js", "LabelWindow.js" };
            List<IFile> ar = new List<IFile> { storage.files.Find("LabelWindow.js"), storage.files.Find("LabelWinw.j"), storage.files.Find(@"tr\LabelWindow.js") };
            int idx = ar.FindIndex(a => a.CopyOf() == null);
            for (int i = 0; i < 3; i++)
                if (i != idx)
                {
                    Assert.IsTrue(ar[i].CopyOf() != null && ar[i].CopyOf().FileNameForReports == list[idx]);
                    Assert.IsTrue(ar[i].GetCopies().Count() == 0);
                }

            Assert.IsTrue(ar[idx].GetCopies().Except(ar.Where(a => a != ar[idx])).Count() == 0);
        }
    }

}
