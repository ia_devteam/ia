﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for VariableTest and is intended
    ///To contain all VariableTest Unit Tests
    ///</summary>
    [TestClass()]
    public class VariableTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }


        /// <summary>
        ///A test for FullName
        ///</summary>
        [TestMethod()]
        public void FullNameTest()
        {
            Namespace ns = storage.namespaces.FindOrCreate(new string[] { "ns1", "ns2" });

            Variable v1 = storage.variables.AddVariable();
            v1.Name = "v1";
            ns.AddVariableToNamespace(v1);

            Assert.IsTrue(v1.FullName == "ns1.ns2.v1");

            Variable v2 = storage.variables.AddVariable();
            v2.Name = "v2";
            Assert.IsTrue(v2.FullName == "v2");
        }


        /// <summary>
        ///A test for isGlobal
        ///</summary>
        [TestMethod()]
        public void isGlobalVariableTest()
        {
            Variable var = storage.variables.AddVariable();
            Assert.IsFalse(var.isGlobal);

            var.isGlobal = true;
            testUtils.Storage_Reopen(ref storage);
            Assert.IsTrue(storage.variables.GetVariable(1).isGlobal);

            Class cl = storage.classes.AddClass();
            Field f = cl.AddField(true);
            try
            {
                f.isGlobal = true;
                Assert.Fail();
            }
            catch
            {

            }

            testUtils.Storage_Reopen(ref storage);
            f = (Field)storage.variables.GetVariable(2);
            Assert.IsTrue(f.isStatic);
            Assert.IsTrue(f.isGlobal);


        }
    }
}
