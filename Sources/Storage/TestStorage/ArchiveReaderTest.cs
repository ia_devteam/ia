﻿using FileOperations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

using IOController;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for ArchiveReaderTest and is intended
    ///To contain all ArchiveReaderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ArchiveReaderAndWriterTest
    {
        private TestUtils.TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        ///A test for Read
        ///</summary>
        [TestMethod()]
        public void TestArchiveReaderAndWriter()
        {
            string tempDirectoryPath = System.IO.Path.Combine(testUtils.TestRunDirectoryPath, @"temp");
            string archiveFilePath = System.IO.Path.Combine(testUtils.TestRunDirectoryPath,"test" + ArchiveWriter.ArchiveDefaultExtension);
            string DirectoryToArchivePath = System.IO.Path.Combine(testUtils.TestMatirialsDirectoryPath, @"Sources\JavaScript\");

            /*
             * Тест не будет работать пока не будет тестовых данных. Генерить самим по-хорошему надо.
             */
            using (ArchiveWriter writer = new ArchiveWriter(archiveFilePath, true))
            {
                //Проверка на то, что архив пуст
                Assert.IsTrue(writer.ArchiveSize == 0);

                //Копируем во временную диеркторию, так как будем архивировать с удалением
                DirectoryController.Copy(DirectoryToArchivePath, tempDirectoryPath);

                //Архивируем все файлы из директории
                foreach (string file in DirectoryController.GetFiles(tempDirectoryPath))
                    writer.AddFile(file);
            }

            //Разорхивируем и сравниваем с оригиналом
            foreach (FileOperations.ArchiveReader.ModifiedFileStream my in ArchiveReader.Read(archiveFilePath, tempDirectoryPath))
                Compare(my, System.IO.Path.Combine(DirectoryToArchivePath, System.IO.Path.GetFileName(my.FilePath)));

            Directory.Delete(testUtils.TestRunDirectoryPath, true);
        }

        private void Compare(ArchiveReader.ModifiedFileStream my, string file)
        {
            System.IO.FileStream orig = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            int i;
            while ((i = my.ReadByte()) != -1)
                Assert.IsTrue(i == orig.ReadByte());

            Assert.IsTrue(orig.ReadByte() == -1);
        }
    }
}
