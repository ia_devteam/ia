﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for FieldTest and is intended
    ///To contain all FieldTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FieldTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }
        
        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for isStatic
        ///</summary>
        [TestMethod()]
        public void FieldisStaticTest()
        {
            Class clazz1 = storage.classes.AddClass();
            Field f1 = clazz1.AddField(true);
            Field f2 =clazz1.AddField(false);

            Assert.IsTrue(f1.isStatic == true);
            Assert.IsTrue(f2.isStatic == false);

            testUtils.Storage_Reopen(ref storage);

            f1 = (Field) storage.variables.GetVariable(1);
            f2 = (Field) storage.variables.GetVariable(2);

            Assert.IsTrue(f1.isStatic == true);
            Assert.IsTrue(f2.isStatic == false);
        }

        /// <summary>
        ///A test for isStatic
        ///</summary>
        [TestMethod()]
        public void FieldFullNameTest()
        {
            Namespace ns = storage.namespaces.FindOrCreate("sdd");
            Class clazz1 = storage.classes.AddClass();
            clazz1.Name = "cl";
            ns.AddClassToNamespace(clazz1);
            Field f1 = clazz1.AddField(true);
            f1.Name = "f";
            
            Assert.IsTrue(f1.FullName == "sdd.cl.f");

            testUtils.Storage_Reopen(ref storage);

            f1 = (Field)storage.variables.GetVariable(1);
            Assert.IsTrue(f1.FullName == "sdd.cl.f");
        }
    }
}
