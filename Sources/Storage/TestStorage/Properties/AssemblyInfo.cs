﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values To modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TestStorage")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("TestStorage")]
[assembly: AssemblyCopyright("Copyright ©  2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible To false makes the types in this assembly not visible 
// To COM componenets.  If you need To access a type in this assembly From 
// COM, set the ComVisible attribute To true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the Id of the typelib if this project is exposed To COM
[assembly: Guid("bf582d46-3678-4b39-97f0-eba308ae9d01")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("3.0.2.4")]
[assembly: AssemblyFileVersion("3.0.2.4")]
