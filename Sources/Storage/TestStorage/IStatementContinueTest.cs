﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementContinueTest and is intended
    ///To contain all IStatementContinueTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementContinueTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for ContinuingLoop
        ///</summary>
        [TestMethod()]
        public void ContinuingLoopTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementContinue d = storage.statements.AddStatementContinue(1, file);
            IStatement st = storage.statements.AddStatementFor(2, file);

            Assert.IsTrue(d.ContinuingLoop == null);

            d.ContinuingLoop = st;

            Assert.IsTrue(d.ContinuingLoop is IStatementFor);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementContinue).ContinuingLoop is IStatementFor);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void ContinueSubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatementContinue d = storage.statements.AddStatementContinue(1, file);

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);

            IStatementBreak c = storage.statements.AddStatementBreak(1, file);
            d.ContinuingLoop = c;

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);
        }
    }
}
