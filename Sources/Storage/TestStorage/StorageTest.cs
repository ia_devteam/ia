﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for StorageTest and is intended
    ///To contain all StorageTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StorageTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for namespaces
        ///</summary>
        [TestMethod()]
        public void namespacesTest()
        {

            Namespaces ns = storage.namespaces;
            Assert.IsTrue(ns == storage.namespaces);
        }

        /// <summary>
        /// Проверка корректности <see cref="Store.Storage.GetSharedFilePath"/>
        /// </summary>
        [TestMethod()]
        public void SharedFiles()
        {
            string name = storage.GetSharedFilePath(Store.SharedFiles.enSharedFiles.UnderstandProtocolProgramUnits);
            Assert.IsTrue(name.StartsWith(storage.Get_DB().GetDBDirectory()) && name.EndsWith("ProgramUnits.txt"));
        }
    }
}
