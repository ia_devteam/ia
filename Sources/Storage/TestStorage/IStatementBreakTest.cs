﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementBreakTest and is intended
    ///To contain all IStatementBreakTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementBreakTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for BreakingLoop
        ///</summary>
        [TestMethod()]
        public void BreakingLoopTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementBreak d = storage.statements.AddStatementBreak(1, file);
            IStatement st = storage.statements.AddStatementGoto(1, file);

            Assert.IsTrue(d.BreakingLoop == null);

            d.BreakingLoop = st;

            Assert.IsTrue(d.BreakingLoop is IStatementGoto);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementBreak).BreakingLoop is IStatementGoto);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void BreakSubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatementBreak d = storage.statements.AddStatementBreak(1, file);

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);

            IStatementContinue c = storage.statements.AddStatementContinue(1, file);
            d.BreakingLoop = c;

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void BreakSetLocationTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatementBreak d = storage.statements.AddStatementBreak();

            d.SetFirstSymbolLocation(file, 1, 1);

            Assert.IsTrue(d.FirstSymbolLocation.GetFileID() == 1);

            testUtils.Storage_Reopen(ref storage);
            
            d = (IStatementBreak) storage.statements.GetStatement(1);
            Assert.IsTrue(d.FirstSymbolLocation.GetFileID() == 1);
        }
    }
}
