﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;

using TestUtils;

using System.Linq;

namespace TestStorage
{
    /// <summary>
    /// Summary description for Variables
    /// </summary>
    [TestClass]
    public class Test_Variables
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        [TestMethod]
        public void Variable_Name()
        {
            //Наполняем
            Variables variables = storage.variables;
            Variable var = variables.AddVariable();
            var.Name = "asd";
            var = variables.AddVariable();
            var.Name = "qwe";


            //Проверяем
            testUtils.Storage_Reopen(ref storage);

            variables = storage.variables;
            int i = 0;
            foreach (Variable v in variables.EnumerateVariables())
            {
                i++;
                if (i == 1)
                    Assert.IsTrue(v.Name == "asd");
                if (i == 2)
                    Assert.IsTrue(v.Name == "qwe");
            }
            Assert.IsTrue(i == 2);
        }

        [TestMethod]
        public void Field_Definition()
        {
            string fileName = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            //Добавляем файл
            Files files = storage.files;
            IFile file = files.FindOrGenerate(fileName, enFileKindForAdd.fileWithPrefix);

            //Добавляем класс
            Classes classes = storage.classes;
            Class cls = classes.AddClass();
            cls.Name = "MyClass";
            
            //Добавляем поля класса
            Field fld = cls.AddField(false);
            fld.Name = "field1";
            fld.AddDefinition(file, 1000, null);

            fld = cls.AddField(true);
            fld.Name = "field2";
            fld.AddDefinition(file, 10, 20, null);

            //Проверяем
            testUtils.Storage_Reopen(ref storage);

            int i = 0;
            foreach (IFile f in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                i++;
                if (i == 1)
                    Assert.IsTrue(f.FullFileName_Original == fileName);
            }
            Assert.IsTrue(i == 1);

            i = 0;
            foreach (Class c in storage.classes.EnumerateClasses())
            {
                i++;
                if (i == 1)
                {
                    Assert.IsTrue(c.Name == "MyClass");

                    int j = 0;
                    foreach (Field fl in c.EnumerateFields())
                    {
                        j++;
                        if (j == 1)
                            Assert.IsTrue(fl.Name == "field1");
                        if (j == 2)
                            Assert.IsTrue(fl.Name == "field2");
                    }
                    Assert.IsTrue(j == 2);

                }
            }
            Assert.IsTrue(i == 1);
        }

        [TestMethod]
        public void Variable_Find()
        {
            //Наполняем
            Namespace ns1 = storage.namespaces.FindOrCreate("n1");
            Namespace ns2 = ns1.FindOrCreateSubNamespace("n2");
            Namespace ns3 = storage.namespaces.FindOrCreate("n3");
            Namespace ns4 = ns3.FindOrCreateSubNamespace("n2");

            Variables variables = storage.variables;
            Variable var = variables.AddVariable();
            var.Name = "v1";
            ns2.AddVariableToNamespace(var);

            var = variables.AddVariable();
            var.Name = "v1";
            ns4.AddVariableToNamespace(var);


            //Проверяем
            testUtils.Storage_Reopen(ref storage);


            Variable[] res = storage.variables.GetVariable(new string[] { "n1", "n2", "v1" });
            TestUtilsClass.CheckArray<Variable>((var1, var2) => { return var1.Id == var2.Id; }, res , new Variable[] { storage.variables.GetVariable(1)});

            res = storage.variables.FindVariable(new string[] { "n2", "v1" });
            TestUtilsClass.CheckArray<Variable>((var1, var2) => { return var1.Id == var2.Id; }, res, new Variable[] { storage.variables.GetVariable(1), storage.variables.GetVariable(2) });

            res = storage.variables.FindVariable(new string[] { "n3", "n2", "v1" });
            TestUtilsClass.CheckArray<Variable>((var1, var2) => { return var1.Id == var2.Id; }, res, new Variable[] { storage.variables.GetVariable(2) });

            res = storage.variables.FindVariable(new string[] { "v1" });
            TestUtilsClass.CheckArray<Variable>((var1, var2) => { return var1.Id == var2.Id; }, res, new Variable[] { storage.variables.GetVariable(1), storage.variables.GetVariable(2) });
        }

        /// <summary>
        /// Тест на появление указателя на функцию в перечислении сразу после добавления (на основе ошибки)
        /// </summary>
        [TestMethod]
        public void Variables_PointsToFunction()
        {
            Variable var = storage.variables.AddVariable();
            IFunction func = storage.functions.AddFunction();

            PointsToFunction ptf = var.AddPointsToFunctions();
            ptf.function = func.Id;
            ptf.Save();

            var res = var.Enumerate_PointsToFunctions().ToArray();

            Assert.IsTrue(res.Count() == 1);
            Assert.IsTrue(res[0].variable == var);
            Assert.IsTrue(res[0].function == func.Id);

        }
    }
}
