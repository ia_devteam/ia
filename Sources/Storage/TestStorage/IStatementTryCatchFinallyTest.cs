﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementTryCatchFinallyTest and is intended
    ///To contain all IStatementTryCatchFinallyTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementTryCatchFinallyTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for AddCatch
        ///</summary>
        [TestMethod()]
        public void AddCatchTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            IStatementTryCatchFinally tcf = storage.statements.AddStatementTryCatchFinally(1, file);
            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            IStatement st2 = storage.statements.AddStatementContinue(1, file);
            
            tcf.AddCatch(st1);
            tcf.AddCatch(st2);

            CheckCatches(tcf);

            testUtils.Storage_Reopen(ref storage);

            tcf = (IStatementTryCatchFinally) storage.statements.GetStatement(1);

            CheckCatches(tcf);
        }

        private static void CheckCatches(IStatementTryCatchFinally tcf)
        {
            int i = 0;
            foreach (IStatement st in tcf.Catches())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(st is IStatementBreak);
                        break;
                    case 1:
                        Assert.IsTrue(st is IStatementContinue);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        /// <summary>
        ///A test for Catches
        ///</summary>
        [TestMethod()]
        public void CatchesTest()
        {
            //Тестируется тестом AddCatchTest
        }

        /// <summary>
        ///A test for ElseBlock
        ///</summary>
        [TestMethod()]
        public void ExceptBlockTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            IStatementTryCatchFinally d = storage.statements.AddStatementTryCatchFinally(1, file);
            IStatement st = storage.statements.AddStatementGoto(1, file);

            Assert.IsTrue(d.ElseBlock == null);

            d.ElseBlock = st;

            Assert.IsTrue(d.ElseBlock is IStatementGoto);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementTryCatchFinally).ElseBlock is IStatementGoto);
        }

        /// <summary>
        ///A test for FinallyBlock
        ///</summary>
        [TestMethod()]
        public void FinallyBlockTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            IStatementTryCatchFinally d = storage.statements.AddStatementTryCatchFinally(1, file);
            IStatement st = storage.statements.AddStatementGoto(1, file);

            Assert.IsTrue(d.FinallyBlock == null);

            d.FinallyBlock = st;

            Assert.IsTrue(d.FinallyBlock is IStatementGoto);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementTryCatchFinally).FinallyBlock is IStatementGoto);
        }

        /// <summary>
        ///A test for TryBlock
        ///</summary>
        [TestMethod()]
        public void TryBlockTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            IStatementTryCatchFinally d = storage.statements.AddStatementTryCatchFinally(1, file);
            IStatement st = storage.statements.AddStatementGoto(1, file);

            Assert.IsTrue(d.TryBlock == null);

            d.TryBlock = st;

            Assert.IsTrue(d.TryBlock is IStatementGoto);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementTryCatchFinally).TryBlock is IStatementGoto);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void TryCatchFinallySubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            IStatementTryCatchFinally tcf = storage.statements.AddStatementTryCatchFinally(1, file);
            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            IStatement st2 = storage.statements.AddStatementContinue(1, file);
            IStatement st3 = storage.statements.AddStatementFor(1, file);
            IStatement st4 = storage.statements.AddStatementGoto(1, file);
            IStatement st5 = storage.statements.AddStatementThrow(1, file);

            tcf.AddCatch(st1);
            tcf.AddCatch(st2);
            tcf.TryBlock = st3;
            tcf.FinallyBlock = st4;
            tcf.ElseBlock = st5;

            testUtils.Storage_Reopen(ref storage);

            tcf = (IStatementTryCatchFinally) storage.statements.GetStatement(1);

            int i = 0;
            foreach (IStatement st in tcf.SubStatements())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(st is IStatementFor);
                        break;
                    case 1:
                        Assert.IsTrue(st is IStatementBreak);
                        break;
                    case 2:
                        Assert.IsTrue(st is IStatementContinue);
                        break;
                    case 3:
                        Assert.IsTrue(st is IStatementThrow);
                        break;
                    case 4:
                        Assert.IsTrue(st is IStatementGoto);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 5);
        }
    }
}
