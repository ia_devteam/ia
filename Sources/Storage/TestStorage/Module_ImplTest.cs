﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for Module_ImplTest and is intended
    ///To contain all Module_ImplTest Unit Tests
    ///</summary>
    [TestClass()]
    public class Module_ImplTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        public void ToStringTest()
        {
            string testSrcFolder = storage.appliedSettings.FileListPath;
            

            System.Reflection.Assembly asm = System.Reflection.Assembly.GetAssembly(storage.GetType());
            AssemblyStrongName sn = new AssemblyStrongName(asm);
            Store.Module mod = storage.modules.AddImplementedModule(sn);

            var tmp = sn.Version.ToString();
            string str = mod.ToString();
            Assert.IsTrue(mod.ToString() == "name: Storage version: "+ tmp + " culture:  PublicKeyToken: System.Byte[]");

            mod = storage.modules.AddModule(System.IO.Path.Combine(testSrcFolder, @"1.txt"), enFileKindForAdd.fileWithPrefix);
            Assert.IsTrue(mod.ToString() == "1.txt");

            mod = storage.modules.AddImplementedModule("1");
            Assert.IsTrue(mod.ToString() == "1");
        }
    }
}
