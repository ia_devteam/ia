﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using TestUtils;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for NamespacesTest and is intended
    ///To contain all NamespacesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class NamespacesTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }
        
        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for EnumerateNamespaces
        ///</summary>
        [TestMethod()]
        public void EnumerateNamespacesTest()
        {
            Namespace ns2 = storage.namespaces.FindOrCreate(new string[]{"ns1", "ns2"});
            Namespace ns3 = storage.namespaces.FindOrCreate("ns3");
            Namespace ns1 = storage.namespaces.Find("ns1");

            TestUtilsClass.CheckArray<Namespace>((arg,correct) => {return arg.Id == correct.Id;}, storage.namespaces.EnumerateNamespaces(), new Namespace[]{ns1, ns2, ns3});
        }

 
        /// <summary>
        ///A test for Find
        ///</summary>
        [TestMethod()]
        public void FindNamespacesTest()
        {
            Namespace ns2 = storage.namespaces.FindOrCreate(new string[] { "ns1", "ns2" });
            Namespace ns3 = storage.namespaces.FindOrCreate("ns2");

            Assert.IsTrue(storage.namespaces.Find("ns2").Id == 3);
            Assert.IsTrue(storage.namespaces.Find(new string[]{"ns1", "ns2"}).Id == 2);
        }


        /// <summary>
        ///A test for FindOrCreate
        ///</summary>
        [TestMethod()]
        public void FindOrCreateNamespacesTest()
        {
            Namespace ns2 = storage.namespaces.FindOrCreate(new string[] { "ns1", "ns2" });
            Namespace ns22 = storage.namespaces.FindOrCreate(new string[] { "ns1", "ns2" });
            Namespace ns1 = storage.namespaces.FindOrCreate("ns1");


            Assert.IsTrue(ns2 == ns22);
            Assert.IsTrue(ns1.Id == 1);
        }

         /// <summary>
        ///A test for FindOrCreate_Id
        ///</summary>
        [TestMethod()]
        public void FindOrCreate_IdNamespacesTest()
        {
            UInt64 ns2 = storage.namespaces.FindOrCreate_Id(new string[] { "ns1", "ns2" });
            UInt64 ns22 = storage.namespaces.FindOrCreate_Id(new string[] { "ns1", "ns2" });
            UInt64 ns1 = storage.namespaces.FindOrCreate_Id("ns1");


            Assert.IsTrue(ns2 == 2);
            Assert.IsTrue(ns22 == 2);
            Assert.IsTrue(ns1 == 1);
        }

        /// <summary>
        ///A test for Find_Id
        ///</summary>
        [TestMethod()]
        public void Find_IdNamespacesTest()
        {
            UInt64 ns2 = storage.namespaces.FindOrCreate_Id(new string[] { "ns1", "ns2" });
            UInt64 ns1 = storage.namespaces.Find_Id("ns1");
            UInt64 ns3 = storage.namespaces.Find_Id("ns3");

            Assert.IsTrue(ns1 == 1);
            Assert.IsTrue(ns3 == Store.Const.CONSTS.WrongID);
        }
    }
}
