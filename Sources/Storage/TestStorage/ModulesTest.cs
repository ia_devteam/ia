﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for ModulesTest and is intended
    ///To contain all ModulesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ModulesTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for FindModule
        ///</summary>
        [TestMethod()]
        public void FindModuleTest()
        {
            string name1 = "1.txt";
            string name2 = "2.txt";

            string test1Path = System.IO.Path.Combine(testUtils.TestRunDirectoryPath, storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_ORIGINAL), name1);
            string test2Path = System.IO.Path.Combine(testUtils.TestRunDirectoryPath, storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_ORIGINAL), name2);

            IFile file = storage.files.FindOrGenerate(test1Path, enFileKindForAdd.fileWithPrefix);
            Store.Module mod = storage.modules.AddModule(file);

            Assert.IsTrue(storage.modules.FindModule(test2Path) == null);
            Assert.IsTrue(storage.modules.FindModule(test1Path).Name == name1);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue(storage.modules.FindModule(test2Path) == null);
            Assert.IsTrue(storage.modules.FindModule(test1Path).Name == name1);
        }

        /// <summary>
        ///A test for AddImplementedModule
        ///</summary>
        [TestMethod()]
        public void AddModuleTest()
        {
            string name1 = "1.txt";
            string name2 = "2.txt";

            string test1Path = System.IO.Path.Combine(storage.appliedSettings.FileListPath, name1);
            string test2Path = System.IO.Path.Combine(storage.appliedSettings.FileListPath, name2);

            Store.Module mod = storage.modules.AddModule(test1Path, enFileKindForAdd.fileWithPrefix);

            mod = storage.modules.GetModule(1);
            Assert.IsTrue(mod.Name == name1);

            testUtils.Storage_Reopen(ref storage);

            mod = storage.modules.GetModule(1);
            Assert.IsTrue(mod.Name == name1);

            IFile file = storage.files.FindOrGenerate(test2Path, enFileKindForAdd.fileWithPrefix);
            mod = storage.modules.AddModule(test2Path, enFileKindForAdd.fileWithPrefix);

            Assert.IsTrue(storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Count() == 2);
        }

        /// <summary>
        ///A test for AddImplementedModule
        ///</summary>
        [TestMethod()]
        public void AddModuleTest1()
        {
            string test1Path = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1.txt");

            IFile file = storage.files.FindOrGenerate(test1Path, enFileKindForAdd.fileWithPrefix);
            Store.Module mod = storage.modules.AddModule(file);

            mod = storage.modules.GetModule(1);
            Assert.IsTrue(mod.Name == "1.txt");

            testUtils.Storage_Reopen(ref storage);

            mod = storage.modules.GetModule(1);
            Assert.IsTrue(mod.Name == "1.txt");
        }

        /// <summary>
        ///A test for AddImplementedModule
        ///</summary>
        [TestMethod()]
        public void AddModuleTest2()
        {
            string test1Path = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1.txt");

            IFile file = storage.files.FindOrGenerate(test1Path, enFileKindForAdd.fileWithPrefix);
            Store.Module mod = storage.modules.AddModule(1);

            mod = storage.modules.GetModule(1);
            Assert.IsTrue(mod.Name == "1.txt");

            testUtils.Storage_Reopen(ref storage);

            mod = storage.modules.GetModule(1);
            Assert.IsTrue(mod.Name == "1.txt");
        }

        /// <summary>
        ///A test for FindModule
        ///</summary>
        [TestMethod()]
        public void FindModuleTest1()
        {
            string test1Path = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1.txt");

            IFile file = storage.files.FindOrGenerate(test1Path, enFileKindForAdd.fileWithPrefix);
            Store.Module mod = storage.modules.AddModule(file);

            Assert.IsTrue(storage.modules.FindModule(2) == null);
            Assert.IsTrue(storage.modules.FindModule(1).Name == "1.txt");

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue(storage.modules.FindModule(2) == null);
            Assert.IsTrue(storage.modules.FindModule(1).Name == "1.txt");
        }

        /// <summary>
        ///A test for FindModule
        ///</summary>
        [TestMethod()]
        public void FindModuleTest2()
        {
            string test1Path = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1.txt");

            IFile file = storage.files.FindOrGenerate(test1Path, enFileKindForAdd.fileWithPrefix);
            Store.Module mod = storage.modules.AddModule(file);

            Assert.IsTrue(storage.modules.FindModule(file).Name == "1.txt");

            testUtils.Storage_Reopen(ref storage);

            file = storage.files.GetFile(1);

            Assert.IsTrue(storage.modules.FindModule(file).Name == "1.txt");
        }

        /// <summary>
        ///A test for FindModule
        ///</summary>
        [TestMethod()]
        public void FindModuleTest3()
        {
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetAssembly(storage.GetType());
            AssemblyStrongName sn = new AssemblyStrongName(asm);
            Store.Module mod = storage.modules.AddImplementedModule(sn);

            Assert.IsTrue(storage.modules.FindModule(sn).Name == "Storage");

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue(storage.modules.FindModule(sn).Name == "Storage");

            sn.Name = "111";

            Assert.IsTrue(storage.modules.FindModule(sn) == null);

        }

        /// <summary>
        ///A test for FindModuleByName
        ///</summary>
        [TestMethod()]
        public void FindModuleByNameTest()
        {
            Store.Module mod1 = storage.modules.AddExternalModule("11");
            Store.Module mod2 = storage.modules.AddImplementedModule("11");
            Store.Module mod3 = storage.modules.AddImplementedModule("22");

            int i = 0;
            foreach (Store.Module mod in storage.modules.FindModuleByName("11"))
            {
                switch (i)
                {
                    case 0: Assert.IsTrue(mod.Name == "11");
                        Assert.IsTrue(mod.Kind == enModuleKinds.EXTERNAL);
                        break;
                    case 1:Assert.IsTrue(mod.Name == "11");
                        Assert.IsTrue(mod.Kind == enModuleKinds.INTERNAL_SOURCE);
                        break;
                    case 2:
                        Assert.Fail();
                        break;
                }
                i++;
            }

            Assert.IsTrue(i==2);
        }

        /// <summary>
        ///A test for FindImplementedModule
        ///</summary>
        [TestMethod()]
        public void FindImplementedModuleTest()
        {
            Store.Module mod1 = storage.modules.AddExternalModule("11");
            Store.Module mod2 = storage.modules.AddImplementedModule("11");
            Store.Module mod3 = storage.modules.AddImplementedModule("22");

            int i = 0;
            foreach (Store.Module mod in storage.modules.FindImplementedModule("11"))
            {
                switch (i)
                {
                    case 0:Assert.IsTrue(mod.Name == "11");
                        Assert.IsTrue(mod.Kind == enModuleKinds.INTERNAL_SOURCE);
                        break;
                    case 1:
                        Assert.Fail();
                        break;
                }
                i++;
            }

            Assert.IsTrue(i==1);
        }
    }
}
