﻿using System;
using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementGotoTest and is intended
    ///To contain all IStatementGotoTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementGotoTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for Destination
        ///</summary>
        [TestMethod()]
        public void GotoDestinationTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementGoto d = storage.statements.AddStatementGoto(1, file);
            IStatement st = storage.statements.AddStatementBreak(1, file);

            Assert.IsTrue(d.Destination == null);

            d.Destination = st;

            Assert.IsTrue(d.Destination is IStatementBreak);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementGoto).Destination is IStatementBreak);

            try
            {
                d.Condition = storage.operations.AddOperation();
            }
            catch (Exception)
            {
                return;
            }

            Assert.Fail("Нельзя одновременно задавать оба параметра");
        }

        /// <summary>
        ///A test for Condition
        ///</summary>
        [TestMethod()]
        public void GotoConditionTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IStatementGoto d = storage.statements.AddStatementGoto(1, file);
            IStatement st = storage.statements.AddStatementBreak(1, file);
            IOperation op = storage.operations.AddOperation();

            Assert.IsTrue(d.Condition == null);

            d.Condition = op;

            Assert.IsTrue(d.Condition is IOperation);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementGoto).Condition is IOperation);

            try
            {
                d.Destination = storage.statements.GetStatement(2);
            }
            catch (Exception)
            {
                return;
            }

            Assert.Fail("Нельзя одновременно задавать оба параметра");
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void GotoSubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatementGoto d = storage.statements.AddStatementGoto(1, file);

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);

            IStatementBreak c = storage.statements.AddStatementBreak(1, file);
            d.Destination = c;

            Assert.IsTrue(d.SubStatements().GetEnumerator().MoveNext() == false);
        }
    }
}
