﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;
using System.Linq;

namespace TestStorage
{
    /// <summary>
    /// Summary description for Functions
    /// </summary>
    [TestClass]
    public class FunctionsTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize() 
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup() 
        {
            testUtils.Storage_Remove(ref storage);
        }

        [TestMethod]
        public void FuncionName()
        {
            Functions functions = storage.functions;
            IFunction func = functions.AddFunction();

            func.Name = "funcName";
            func = null;

            testUtils.Storage_Reopen(ref storage);

            functions = storage.functions;
            func = functions.GetFunction(1);

            Assert.IsTrue(func.Name == "funcName", "mes 1");

            func = functions.AddFunction();
            func.Name = "eee";

            int i = 0;
            foreach (IFunction fun in functions.EnumerateFunctions(enFunctionKind.ALL))
            {
                switch(i)
                {
                    case 0:
                        Assert.IsTrue(fun.Name == "funcName", "mes 2");
                        break;
                    case 1:
                        Assert.IsTrue(fun.Name == "eee", "mes 3");
                        break;
                    case 2:
                        Assert.Fail("mes 4");
                        break;
                }
                i++;
            }

            func = functions.AddFunction();
            func.Name = "funcName";

            IFunction[] ar = functions.GetFunction("funcName");
            Assert.IsTrue(ar.Length == 2, "mes 5");
            Assert.IsTrue(ar[0].Id == 1 && ar[1].Id == 3 || ar[1].Id == 1 && ar[0].Id == 3, "mes 6");
        }

        [TestMethod]
        public void FunctionPositions()
        {
            string name1 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1111");
            string name2 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            string name3 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "2222");
            string name4 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "2");
            string name5 = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "SignatureFile.xml");


            System.IO.StreamWriter wr = new System.IO.StreamWriter(name1);
            for (int i = 0; i < 1000; i++)
                wr.WriteLine("sdfsdf");
            wr.Close();

            wr = new System.IO.StreamWriter(name2);
            for (int i = 0; i < 1000; i++)
                wr.WriteLine("sdfsdfgsdf");
            wr.Close();

            wr = new System.IO.StreamWriter(name3);
            for (int i = 0; i < 1000; i++)
                wr.WriteLine("sdfsdfgsdf");
            wr.Close();

            wr = new System.IO.StreamWriter(name4);
            for (int i = 0; i < 1000; i++)
                wr.WriteLine("sdfsdfgsdf");
            wr.Close();

            Functions functions = storage.functions;
            IFunction func = functions.AddFunction();

            func.AddDefinition(name5, 400);
            IFile file = storage.files.GetFile(1);

            func.AddDeclaration(name5, 500);
            func.AddDeclaration(storage.files.GetFile(1), 499);
            func.AddDeclaration(name5, 3, 3);
            func.AddDeclaration(storage.files.GetFile(1), 3, 4);

            func.AddDeclaration(name2, 300);

            IFunction func1 = functions.AddFunction();
            func1.AddDefinition(name3, 900);
            func1.AddDeclaration(name4, 800);

            Store.Location ar1 = func.Definition();
            Store.Location[] ar2 = func.Declarations();

            Assert.IsTrue(ar1 != null, "mes 1");
            Assert.IsTrue(ar1.GetFileID() == 1, "mes 1");
            Assert.IsTrue(ar1.GetOffset() == 400, "mes 6");
            Assert.IsTrue(ar1.GetFile().FullFileName_Original == name5, "mes 8");

            Assert.IsTrue(ar2.Length == 5, "mes 9");
            Assert.IsTrue(ar2[0].GetFileID() == 1, "mes 10");
            Assert.IsTrue(ar2[1].GetFileID() == 1, "mes 12");
            Assert.IsTrue(ar2[2].GetFileID() == 1, "mes 13");
            Assert.IsTrue(ar2[3].GetFileID() == 1, "mes 14");
            Assert.IsTrue(ar2[4].GetFileID() == 2, "mes 15");
            Assert.IsTrue(ar2[0].GetOffset() == 500, "mes 16");
            Assert.IsTrue(ar2[1].GetOffset() == 499, "mes 17");
            Assert.IsTrue(ar2[0].GetFile().FullFileName_Original == name5, "mes 18");

            int j = 0;
            foreach (LocationFunctionPair pair in functions.EnumerateFunctionLocationsInSortOrder())
            {
                switch(j)
                {
                    case 0:
                        Assert.IsTrue(pair.function.Id == 1, "mes 20");
                        Assert.IsTrue(pair.location.GetFileID() == 1, "mes 32");
                        Assert.IsTrue(pair.location.GetOffset() == 400, "mes 21");
                        break;
                    case 1:
                        Assert.IsTrue(pair.function.Id == 2, "mes 28");
                        Assert.IsTrue(pair.location.GetFileID() == 3, "mes 36");
                        Assert.IsTrue(pair.location.GetOffset() == 900, "mes 29");
                        break;
                    default:
                        Assert.Fail("mess 1");
                        break;
                }

                j++;
            }

        }

        [TestMethod]
        public void FunctionCallesByOperation_break()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);

            IFunction func = functions.AddFunction();
            IStatementDoAndWhile st = storage.statements.AddStatementDoAndWhile();

            st.Condition = op1;
            st.Body = st2;

            func.EntryStatement = st;

            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2, func3, func4 }).Count() == 4);
        }

        [TestMethod]
        public void FunctionCallesByOperation_for()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);

            IFunction func = functions.AddFunction();
            IStatementFor st = storage.statements.AddStatementFor();
            func.EntryStatement = st;

            st.Condition = op1;
            st.Body = st2;
            st.Start = op3;
            st.Iteration = op4;

            var res = func.Calles().Select(a => a.Calles).ToArray();
            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2, func3, func4, func5, func6, func7, func8 }).Count() == 8);
        }

        [TestMethod]
        public void FunctionCallesByOperation_foreach()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);

            IFunction func = functions.AddFunction();
            IStatementForEach st = storage.statements.AddStatementForEach();

            st.Head = op1;
            st.Body = st2;

            func.EntryStatement = st;

            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2, func3, func4 }).Count() == 4);
        }

        [TestMethod]
        public void FunctionCallesByOperation_if()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);

            IFunction func = functions.AddFunction();
            IStatementIf st = storage.statements.AddStatementIf();

            st.Condition = op1;
            st.ThenStatement = st2;
            st.ElseStatement = st2;

            func.EntryStatement = st;

            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2, func3, func4, func5, func6 }).Count() == 4);
        }

        [TestMethod]
        public void FunctionCallesByOperation_switch()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);

            IFunction func9 = functions.AddFunction();
            IFunction func10 = functions.AddFunction();
            IOperation op5 = storage.operations.AddOperation();
            op5.AddFunctionCallInSequenceOrder(new[] { func9, func10 });

            IFunction func = functions.AddFunction();
            IStatementSwitch st = storage.statements.AddStatementSwitch();

            st.Condition = op1;
            st.AddCase(op4, st2);
            st.AddCase(op5, st3);

            func.EntryStatement = st;

            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2, func3, func4, func5, func6, func7, func8, func9, func10 }).Count() == 10);
        }

        [TestMethod]
        public void FunctionCallesByOperation_return()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);

            IFunction func = functions.AddFunction();
            IStatementReturn st = storage.statements.AddStatementReturn();

            st.ReturnOperation = op1;

            func.EntryStatement = st;

            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2}).Count() == 2);
        }

        [TestMethod]
        public void FunctionCallesByOperation_throw()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);

            IFunction func = functions.AddFunction();
            IStatementThrow st = storage.statements.AddStatementThrow();

            st.Exception = op1;

            func.EntryStatement = st;

            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2 }).Count() == 2);
        }

        [TestMethod]
        public void FunctionCallesByOperation_try()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);
            IStatementOperational st4 = storage.statements.AddStatementOperational();
            st4.Operation = op4;

            IFunction func = functions.AddFunction();
            IStatementTryCatchFinally st = storage.statements.AddStatementTryCatchFinally();

            st.TryBlock = st1;
            st.AddCatch(st2);
            st.AddCatch(st3);
            st.FinallyBlock = st4;

            func.EntryStatement = st;

            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2, func3, func4, func5, func6, func7, func8 }).Count() == 8);
        }

        [TestMethod]
        public void FunctionCallesByOperation_using()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);

            IFunction func = functions.AddFunction();
            IStatementUsing st = storage.statements.AddStatementUsing();

            st.Head = op1;
            st.Body = st2;

            func.EntryStatement = st;

            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2, func3, func4}).Count() == 4);
        }

        [TestMethod]
        public void FunctionCallesByOperation_yield()
        {
            Functions functions;
            IFunction func1;
            IFunction func2;
            IFunction func3;
            IFunction func4;
            IFunction func5;
            IFunction func6;
            IFunction func7;
            IFunction func8;
            IStatementOperational st1;
            IStatementOperational st2;
            IStatementOperational st3;
            IOperation op1;
            IOperation op2;
            IOperation op3;
            IOperation op4;
            Generate(out functions, out func1, out func2, out func3, out func4, out func5, out func6, out func7, out func8, out st1, out st2, out st3, out op1, out op2, out op3, out op4);

            IFunction func = functions.AddFunction();
            IStatementYieldReturn st = storage.statements.AddStatementYieldReturn();

            st.YieldReturnOperation = op1;

            func.EntryStatement = st;

            Assert.IsTrue(func.Calles().Select(a => a.Calles).Intersect(new[] { func1, func2 }).Count() == 2);
        }

        private void Generate(out Functions functions, out IFunction func1, out IFunction func2, out IFunction func3, out IFunction func4, out IFunction func5, out IFunction func6, out IFunction func7, out IFunction func8, out IStatementOperational st1, out IStatementOperational st2, out IStatementOperational st3, out IOperation op1, out IOperation op2, out IOperation op3, out IOperation op4)
        {
            functions = storage.functions;

            func1 = functions.AddFunction();
            func2 = functions.AddFunction();
            func3 = functions.AddFunction();
            func4 = functions.AddFunction();
            func5 = functions.AddFunction();
            func6 = functions.AddFunction();
            func7 = functions.AddFunction();
            func8 = functions.AddFunction();

            st1 = storage.statements.AddStatementOperational();
            st2 = storage.statements.AddStatementOperational();
            st3 = storage.statements.AddStatementOperational();

            op1 = storage.operations.AddOperation();
            op1.AddFunctionCallInSequenceOrder(new[] { func1, func2 });
            st1.Operation = op1;
            op2 = storage.operations.AddOperation();
            op2.AddFunctionCallInSequenceOrder(new[] { func3, func4 });
            st2.Operation = op2;
            op3 = storage.operations.AddOperation();
            op3.AddFunctionCallInSequenceOrder(new[] { func5, func6 });
            st3.Operation = op3;
            op4 = storage.operations.AddOperation();
            op4.AddFunctionCallInSequenceOrder(new[] { func7, func8 });
        }

        [TestMethod]
        public void FunctionCalles()
        {
            Functions functions = storage.functions;
            IFunction func1 = functions.AddFunction();
            IFunction func2 = functions.AddFunction();
            IFunction func3 = functions.AddFunction();

            func1.AddCall(func2);
            func1.AddCall(func3);
            func1.AddCall(func2);

            int i=0;
            foreach (IFunctionCall func in func1.Calles())
            {
                switch(i)
                {
                    case 0:
                        Assert.IsTrue(func.Calles.Id == 2);
                        break;
                    case 1:
                        Assert.IsTrue(func.Calles.Id == 3);
                        break;
                    case 2:
                        Assert.Fail();
                        break;
                }
                i++;
            }
            Assert.IsTrue(i==2);

            func3.AddCall(func2);
            
            i = 0;
            foreach (IFunctionCall func in func2.CalledBy())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(func.Caller.Id == 1);
                        break;
                    case 1:
                        Assert.IsTrue(func.Caller.Id == 3);
                        break;
                    case 2:
                        Assert.Fail();
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        [TestMethod]
        public void FunctionCallThrowStatememnts()
        {
            IFunction func = storage.functions.AddFunction();
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            func.Name = "f1";
            func.AddDeclaration(file, 1, 1);
            func = storage.functions.AddFunction();
            func.Name = "f2";
            func.AddDeclaration(file, 1, 1);
            func = storage.functions.AddFunction();
            func.Name = "function";
            func.AddDeclaration(file, 2, 2);
            IStatementOperational stop = storage.statements.AddStatementOperational(1, file);
            IStatementOperational stop2 = storage.statements.AddStatementOperational(1, file);
            stop.NextInLinearBlock = stop2;
            IOperation op = storage.operations.AddOperation();
            IOperation op1 = storage.operations.AddOperation();
            op.AddFunctionCallInSequenceOrder(new IFunction[] { storage.functions.GetFunction("f1")[0] });
            op.AddFunctionCallInSequenceOrder(new IFunction[] { storage.functions.GetFunction("f1")[0] });
            op.AddFunctionCallInSequenceOrder(new IFunction[] { storage.functions.GetFunction("f2")[0] });
            stop.Operation = op;
            stop2.Operation = op1;
            storage.functions.GetFunction("function")[0].EntryStatement = stop;
            IFunction fff = storage.functions.GetFunction("function")[0];

            int i = 0;
            foreach (IFunctionCall fcall in fff.Calles())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(fcall.Caller.Id == fff.Id);
                        Assert.IsTrue(fcall.Calles.Id == storage.functions.GetFunction("f1")[0].Id);
                        break;
                    case 1:
                        Assert.IsTrue(fcall.Caller.Id == fff.Id);
                        Assert.IsTrue(fcall.Calles.Id == storage.functions.GetFunction("f2")[0].Id);
                        break;
                    case 2:
                        Assert.Fail();
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        [TestMethod]
        public void FunctionSensor()
        {
            Functions functions = storage.functions;
            IFunction func1 = functions.AddFunction();
            IFunction func2 = functions.AddFunction();

            func1.AddSensor(1, Store.Kind.START);
            func1.AddSensor(2, Store.Kind.INTERNAL);
            func1.AddSensor(3, Store.Kind.FINAL);

            func2.AddSensor(4, Store.Kind.START);
            func2.AddSensor(5, Store.Kind.START);
            func2.AddSensor(6, Store.Kind.START);

            Store.Sensors sensors = storage.sensors;

            int i = 0;
            foreach (Store.Sensor sens in sensors.EnumerateSensors())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(sens.ID == 1);
                        Assert.IsTrue(sens.GetFunctionID() == 1);
                        Assert.IsTrue(sens.Kind == Store.Kind.START );
                        break;
                    case 1:
                        Assert.IsTrue(sens.ID == 2);
                        Assert.IsTrue(sens.GetFunctionID() == 1);
                        Assert.IsTrue(sens.Kind == Store.Kind.INTERNAL);
                        break;
                    case 2:
                        Assert.IsTrue(sens.ID == 3);
                        Assert.IsTrue(sens.GetFunctionID() == 1);
                        Assert.IsTrue(sens.Kind == Store.Kind.FINAL);
                        break;
                    case 3:
                        Assert.IsTrue(sens.ID == 4);
                        Assert.IsTrue(sens.GetFunctionID() == 2);
                        break;
                    case 4:
                        Assert.IsTrue(sens.ID == 5);
                        Assert.IsTrue(sens.GetFunctionID() == 2);
                        break;
                    case 5:
                        Assert.IsTrue(sens.ID == 6);
                        Assert.IsTrue(sens.GetFunctionID() == 2);
                        break;
                    default:
                        Assert.Fail();
                        break;
                }

                i++;
            }

            sensors.Clear();
            foreach (Store.Sensor sens in sensors.EnumerateSensors())
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void FunctionLabels()
        {
            Functions functions = storage.functions;
            IFunction func1 = functions.AddFunction();
            IFunction func2 = functions.AddFunction();

            func1.Essential = enFunctionEssentials.REDUNDANT;
            func1.EssentialUnderstand = enFunctionEssentialsUnderstand.ESSENTIAL;
            func1.EssentialMarkedType = enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_AFTER_DFM;
            func1.UseKind = enFunctionUseKinds.PROGRAM_EVENT_HANDLER;

            Assert.IsTrue(func2.UseKind == 0);
            Assert.IsTrue(func2.Essential == enFunctionEssentials.UNKNOWN);
            Assert.IsTrue(func2.EssentialMarkedType == enFunctionEssentialMarkedTypes.NOTHING);
            func2.Essential = enFunctionEssentials.ESSENTIAL;
            func2.EssentialMarkedType = enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER;
            func2.UseKind = enFunctionUseKinds.ENVIRONMENT_EVENT_HANDLER;
            func2.StatementLinked = enFunctionStatementLinked.PROCESSED;

            testUtils.Storage_Reopen(ref storage);

            functions = storage.functions;
            func1 = functions.GetFunction(1);

            Assert.IsTrue(func1.Essential == enFunctionEssentials.REDUNDANT);
            Assert.IsTrue(func1.EssentialMarkedType == enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_AFTER_DFM);
            Assert.IsTrue(func1.UseKind == enFunctionUseKinds.PROGRAM_EVENT_HANDLER);
            Assert.IsTrue(func1.StatementLinked == enFunctionStatementLinked.NOT_PROCESSED);

            IFunction labels = functions.GetFunction(2);
            Assert.IsTrue(labels.Essential == enFunctionEssentials.ESSENTIAL);
            Assert.IsTrue(labels.EssentialMarkedType == enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER);
            Assert.IsTrue(labels.UseKind == enFunctionUseKinds.ENVIRONMENT_EVENT_HANDLER);
            Assert.IsTrue(func2.StatementLinked == enFunctionStatementLinked.PROCESSED);
        }

        [TestMethod]
        public void FunctionOperatorsGetPointerToFunction()
        {
            Variable var = storage.variables.AddVariable();

            IFunction func1 = storage.functions.AddFunction();
            IFunction func2 = storage.functions.AddFunction();
            IFunction func3 = storage.functions.AddFunction();
            IFunction func4 = storage.functions.AddFunction();

            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            //Так добавлять
            PointsToFunction ptf1 = var.AddPointsToFunctions();
            ptf1.function = func1.Id;
            ptf1.AddPosition(file, 1, func2);
            ptf1.Save();

            //Так использовать
            int i = 0;
            foreach (PointsToFunction ptf in func2.OperatorsGetPointerToFunction())
            {
                switch(i++)
                {
                    case 0: Assert.IsTrue(ptf.function == func1.Id);
                        break;
                    default: Assert.Fail();
                        break;
                }
            }
            Assert.IsTrue(i == 1);

            PointsToFunction ptf2 = var.AddPointsToFunctions();
            ptf2.function = func2.Id;
            ptf2.AddPosition(file, 1, func1);
            ptf2.Save();

            PointsToFunction ptf3 = var.AddPointsToFunctions();
            ptf3.function = func3.Id;
            ptf3.AddPosition(file, 1, func2);
            ptf3.Save();

            i = 0;
            foreach (PointsToFunction ptf in func2.OperatorsGetPointerToFunction())
            {
                switch (i++)
                {
                    case 0: Assert.IsTrue(ptf.function == func1.Id);
                        break;
                    case 1: Assert.IsTrue(ptf.function == func3.Id);
                        break;
                    default: Assert.Fail();
                        break;
                }
            }
            Assert.IsTrue(i == 2);

            i = 0;
            foreach (PointsToFunction ptf in func1.OperatorsGetPointerToFunction())
            {
                switch (i++)
                {
                    case 0: Assert.IsTrue(ptf.function == func2.Id);
                        break;
                    default: Assert.Fail();
                        break;
                }
            }
            Assert.IsTrue(i == 1);

        }

        [TestMethod]
        public void FunctionBreak()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            IFunction func1 = storage.functions.AddFunction();
            func1.Name = "func1";
            IFunction func2 = storage.functions.AddFunction();
            func2.Name = "func2";
            IFunction func3 = storage.functions.AddFunction();
            func3.Name = "func3";
            
            IStatementIf ifst = storage.statements.AddStatementIf();
            IOperation op = storage.operations.AddOperation();
            op.AddFunctionCallInSequenceOrder(new IFunction[] { func1 });
            ifst.Condition = op;

            IStatementIf ifst2 = storage.statements.AddStatementIf();
            IOperation op2 = storage.operations.AddOperation();
            op2.AddFunctionCallInSequenceOrder(new IFunction[] { func3 });
            ifst2.Condition = op2;

            ifst.NextInLinearBlock = ifst2;
            func2.EntryStatement = ifst;

            //TestUtils.ResetStorage(ref storage);

            //func1 = storage.functions.GetFunction("func1")[0];
            //func2 = storage.functions.GetFunction("func2")[0];
            //func3 = storage.functions.GetFunction("func3")[0];

            IFunctionCall []called = func1.CalledBy().ToArray();
            IFunctionCall[] calles = func2.Calles().ToArray();
            Assert.IsTrue(func1.CalledBy().Where(g => g.Caller == func2).Count() == 1);
            Assert.IsTrue(func3.CalledBy().Where(g => g.Caller == func2).Count() == 1);
            Assert.IsTrue(func2.Calles().Where(g => g.Calles == func1).Count() == 1);
            Assert.IsTrue(func2.Calles().Where(g => g.Calles == func3).Count() == 1);
        }
        [TestMethod]
        public void FunctionInExternalFile()
        {
            string filename = storage.appliedSettings.FileListPath + Guid.NewGuid().ToString();
            System.IO.File.AppendAllText(filename, "a\na\na\na\n");
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = filename;

            IFunction tempFunc = storage.functions.AddFunction();
            tempFunc.Name = "test";
            tempFunc.AddDefinition(file, 1, 1);

            IFunction newFunc = storage.functions.AddFunction();
            newFunc.Name = "System.Console.Write";
            newFunc.AddDefinition(storage.files.FindOrGenerate("Unknown SourceFile", enFileKindForAdd.externalFile), 0); // этот код не падает
            newFunc.IsExternal = true;

            filename = storage.appliedSettings.FileListPath + Guid.NewGuid().ToString();
            System.IO.File.AppendAllText(filename, "a\na\na\na\n");
            IFile file2 = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file2.FullFileName_Original = filename;

            IFunction tempFunc2 = storage.functions.AddFunction();
            tempFunc2.Name = "test";
            tempFunc2.AddDefinition(file2, 1, 1);

            IFunction newFunc2 = storage.functions.AddFunction();
            newFunc2.Name = "System.Console.Write2";
            newFunc2.AddDefinition(storage.files.FindOrGenerate("Unknown SourceFile", enFileKindForAdd.externalFile), 0); // этот код падал - потому что по ID файла (2) - он находит реальный файл с занесенными смещениями, а для 2го ничего не находится!
            newFunc2.IsExternal = true;

            Assert.IsTrue(newFunc2.Name == "System.Console.Write2");

        }
    }
}
