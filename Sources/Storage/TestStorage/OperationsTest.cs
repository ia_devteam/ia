﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for OperationsTest and is intended
    ///To contain all OperationsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class OperationsTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }


        /// <summary>
        ///A test for AddOperation
        ///</summary>
        [TestMethod()]
        public void AddOperationTest()
        {
            IOperation op = storage.operations.AddOperation();
            UInt64 id = op.Id;

            testUtils.Storage_Reopen(ref storage);

            Assert.IsNotNull(storage.operations.GetOperation(id));
        }
    }
}
