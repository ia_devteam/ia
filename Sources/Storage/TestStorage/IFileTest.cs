﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IFileTest and is intended
    ///To contain all IFileTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IFileTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for FunctionsDefinedInTheFile
        ///</summary>
        [TestMethod()]
        public void FunctionsDefinedInTheFileTest()
        {
            Namespace ns1 = storage.namespaces.FindOrCreate("Main");

            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IFunction func = storage.functions.AddFunction();
            func.Name = "f";
            Namespace ns = storage.namespaces.FindOrCreate("main");

            ns.AddFunctionToNamespace(func);
            func.AddDefinition(file, 10);

            TestUtilsClass.CheckArray<IFunction>((funct, n) => { return funct.FullName == "main.f"; }, file.FunctionsDefinedInTheFile(), new IFunction[] { func });
        }
    }
}
