﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for SensorTest and is intended
    ///To contain all SensorTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SensorTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }


        internal virtual Sensor CreateSensor()
        {
            // TODO: Instantiate an appropriate concrete class.
            Sensor target = null;
            return target;
        }

        /// <summary>
        ///A test for GetBeforeStatement
        ///</summary>
        [TestMethod()]
        public void GetBeforeStatementTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st = storage.statements.AddStatementBreak(1, file);

            storage.sensors.AddSensorBeforeStatement(1, st, Kind.INTERNAL);
            Sensor sensor = storage.sensors.GetSensor(1);

            Assert.IsTrue(sensor.GetBeforeStatement() is IStatementBreak);

            IFunction func = storage.functions.AddFunction();

            storage.sensors.AddSensor(2, func.Id, Kind.INTERNAL);
            Sensor sensor1 = storage.sensors.GetSensor(2);

            Assert.IsTrue(sensor1.GetBeforeStatement() == null);

            testUtils.Storage_Reopen(ref storage);

            sensor = storage.sensors.GetSensor(1);
            sensor1 = storage.sensors.GetSensor(2);

            Assert.IsTrue(sensor.GetBeforeStatement() is IStatementBreak);
            Assert.IsTrue(sensor1.GetBeforeStatement() == null);
        }

        /// <summary>
        ///A test for GetBeforeStatementId
        ///</summary>
        [TestMethod()]
        public void GetBeforeStatementIdTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st = storage.statements.AddStatementBreak(1, file);

            storage.sensors.AddSensorBeforeStatement(1, st, Kind.INTERNAL);
            Sensor sensor = storage.sensors.GetSensor(1);

            Assert.IsTrue(sensor.GetBeforeStatementId() == 1);

            IFunction func = storage.functions.AddFunction();

            storage.sensors.AddSensor(2, func.Id, Kind.INTERNAL);
            Sensor sensor1 = storage.sensors.GetSensor(2);

            Assert.IsTrue(sensor1.GetBeforeStatementId() == Store.Const.CONSTS.WrongID);

            testUtils.Storage_Reopen(ref storage);

            sensor = storage.sensors.GetSensor(1);
            sensor1 = storage.sensors.GetSensor(2);

            Assert.IsTrue(sensor.GetBeforeStatementId() == 1);
            Assert.IsTrue(sensor1.GetBeforeStatementId() == Store.Const.CONSTS.WrongID);

        }
    }
}
