﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;
using System.IO;

namespace TestStorage
{
    /// <summary>
    /// Summary description for Traces
    /// </summary>
    [TestClass]
    public class Traces
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;
        string testUniqName;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        [TestMethod]
        public void Storage_Traces()
        {
            //создаём директорию для тестовых трасс
            string tracesFolder =  System.IO.Path.Combine(testUtils.TestRunDirectoryPath, "test_traces");
            if (!Directory.Exists(tracesFolder))
                Directory.CreateDirectory(tracesFolder);

            string traceOddName = System.IO.Path.Combine(tracesFolder, "trace_odd.RNTLOG");
            string traceOdd2Name = System.IO.Path.Combine(tracesFolder, "trace_odd_2.RNTLOG");
            string traceEvenName = System.IO.Path.Combine(tracesFolder, "trace_even.RNTLOG");


            //создаём файл с нечётной трассой
            GenerateTrace(traceOddName, 1, 20, 2);
            //создаём файл с чётной трассой
            GenerateTrace(traceEvenName, 2, 21, 2);
            
            //Добавляем датчики в Хранилище
            IFunction function = storage.functions.AddFunction();
            storage.sensors.SetSensorStartNumberAsMaximum();
            for (int num = 1; num < 21; num++)
                storage.sensors.AddSensor(function.Id, Kind.START);

            Store.Traces traces = new Store.Traces(storage);
            
            //Проверяем что в начале в Хранилище нет трасс
            Assert.IsTrue(traces.Count() == 0);
            
            //Добавляем созданные трассы в Хранилище
            ITrace trace_odd = traces.AddTrace(traceOddName);
            ITrace trace_even = traces.AddTrace(traceEvenName);
            Assert.IsTrue(trace_odd != null);
            Assert.IsTrue(trace_even != null);

            //Проверяем что теперь в Хранилище ровно 2 трассы
            Assert.IsTrue(traces.Count() == 2);

            //Проверяем, что трассы после импорта удалились
            Assert.IsFalse(File.Exists(traceOddName));
            Assert.IsFalse(File.Exists(traceEvenName));

            //создаём дубликат нечётной трассы, но с другим именем
            GenerateTrace(traceOdd2Name, 1, 20, 2);
            Assert.IsTrue(traces.AddTrace(traceOdd2Name) == null);

            ////проверяем, что в Хранилище помещено именно то, что нужно
            Assert.IsTrue(trace_odd.NextSensor().ID == 1);
            Assert.IsTrue(trace_odd.NextSensor().ID == 3);
            Assert.IsTrue(trace_odd.NextSensor().ID == 5);
            Assert.IsTrue(trace_odd.NextSensor().ID == 7);
            Assert.IsTrue(trace_odd.NextSensor().ID == 9);
            Assert.IsTrue(trace_odd.NextSensor().ID == 11);
            Assert.IsTrue(trace_odd.NextSensor().ID == 13);
            Assert.IsTrue(trace_odd.NextSensor().ID == 15);
            Assert.IsTrue(trace_odd.NextSensor().ID == 17);
            Assert.IsTrue(trace_odd.NextSensor().ID == 19);
            Assert.IsTrue(trace_odd.NextSensor() == null);

            Assert.IsTrue(trace_even.NextSensor().ID == 2);
            Assert.IsTrue(trace_even.NextSensor().ID == 4);
            Assert.IsTrue(trace_even.NextSensor().ID == 6);
            Assert.IsTrue(trace_even.NextSensor().ID == 8);
            Assert.IsTrue(trace_even.NextSensor().ID == 10);
            Assert.IsTrue(trace_even.NextSensor().ID == 12);
            Assert.IsTrue(trace_even.NextSensor().ID == 14);
            Assert.IsTrue(trace_even.NextSensor().ID == 16);
            Assert.IsTrue(trace_even.NextSensor().ID == 18);
            Assert.IsTrue(trace_even.NextSensor().ID == 20);
            Assert.IsTrue(trace_even.NextSensor() == null);
        }

        private void GenerateTrace(string filePath, int start, int end, int iter)
        {
            FileStream fileStreamOdd = new FileStream(filePath, FileMode.OpenOrCreate);
            BinaryWriter binaryWriterOdd = new BinaryWriter(fileStreamOdd);

            for (int num = start; num < end; num += iter)
                binaryWriterOdd.Write(num);

            binaryWriterOdd.Close();
            fileStreamOdd.Close();
        }
    }
}
