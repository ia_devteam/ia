﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementTest and is intended
    ///To contain all IStatementTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for Kind
        ///</summary>
        [TestMethod()]
        public void KindTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STBreak);

            st1 = storage.statements.AddStatementContinue(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STContinue);

            st1 = storage.statements.AddStatementDoAndWhile(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STDoAndWhile);

            st1 = storage.statements.AddStatementFor(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STFor);

            st1 = storage.statements.AddStatementForEach(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STForEach);

            st1 = storage.statements.AddStatementGoto(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STGoto);

            st1 = storage.statements.AddStatementIf(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STIf);

            st1 = storage.statements.AddStatementOperational(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STOperational);

            st1 = storage.statements.AddStatementReturn(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STReturn);

            st1 = storage.statements.AddStatementSwitch(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STSwitch);

            st1 = storage.statements.AddStatementThrow(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STThrow);

            st1 = storage.statements.AddStatementTryCatchFinally(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STTryCatchFinally);

            st1 = storage.statements.AddStatementUsing(1, file);
            Assert.IsTrue(st1.Kind == ENStatementKind.STUsing);

        }

        /// <summary>
        ///A test for Location
        ///</summary>
        [TestMethod()]
        public void LocationTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            Assert.IsTrue(st1.FirstSymbolLocation.GetFile() == file);
            Assert.IsTrue(st1.FirstSymbolLocation.GetOffset() == 1);

            testUtils.Storage_Reopen(ref storage);
            st1 = storage.statements.GetStatement(1);

            Assert.IsTrue(st1.FirstSymbolLocation.GetFileID() == 1);
            Assert.IsTrue(st1.FirstSymbolLocation.GetOffset() == 1);
        }

        /// <summary>
        ///A test for NextInLinearBlock
        ///</summary>
        [TestMethod()]
        public void NextInLinearBlockTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            IStatement st2 = storage.statements.AddStatementContinue(1, file);
            IStatement st3 = storage.statements.AddStatementDoAndWhile(1, file);
            IStatement st4 = storage.statements.AddStatementFor(1, file);
            IStatement st5 = storage.statements.AddStatementForEach(1, file);
            IStatement st6 = storage.statements.AddStatementGoto(1, file);
            IStatement st7 = storage.statements.AddStatementIf(1, file);
            IStatement st8 = storage.statements.AddStatementOperational(1, file);
            IStatement st9 = storage.statements.AddStatementReturn(1, file);
            IStatement st10 = storage.statements.AddStatementSwitch(1, file);
            IStatement st11 = storage.statements.AddStatementThrow(1, file);
            IStatement st12 = storage.statements.AddStatementTryCatchFinally(1, file);
            IStatement st13 = storage.statements.AddStatementUsing(1, file);

            st1.NextInLinearBlock = st2;
            st2.NextInLinearBlock = st3;
            st3.NextInLinearBlock = st4;
            st4.NextInLinearBlock = st5;
            st5.NextInLinearBlock = st6;
            st6.NextInLinearBlock = st7;
            st7.NextInLinearBlock = st8;
            st8.NextInLinearBlock = st9;
            st9.NextInLinearBlock = st10;
            st10.NextInLinearBlock = st11;
            st11.NextInLinearBlock = st12;
            st12.NextInLinearBlock = st13;

            Assert.IsTrue(st1.NextInLinearBlock == st2);
            Assert.IsTrue(st2.NextInLinearBlock == st3);
            Assert.IsTrue(st3.NextInLinearBlock == st4);
            Assert.IsTrue(st4.NextInLinearBlock == st5);
            Assert.IsTrue(st5.NextInLinearBlock == st6);
            Assert.IsTrue(st6.NextInLinearBlock == st7);
            Assert.IsTrue(st7.NextInLinearBlock == st8);
            Assert.IsTrue(st8.NextInLinearBlock == st9);
            Assert.IsTrue(st9.NextInLinearBlock == st10);
            Assert.IsTrue(st10.NextInLinearBlock == st11);
            Assert.IsTrue(st11.NextInLinearBlock == st12);
            Assert.IsTrue(st12.NextInLinearBlock == st13);
        }

        /// <summary>
        ///A test for PreviousInLinearBlock
        ///</summary>
        [TestMethod()]
        public void PreviousInLinearBlockTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            IStatement st2 = storage.statements.AddStatementContinue(1, file);
            IStatement st3 = storage.statements.AddStatementDoAndWhile(1, file);
            IStatement st4 = storage.statements.AddStatementFor(1, file);
            IStatement st5 = storage.statements.AddStatementForEach(1, file);
            IStatement st6 = storage.statements.AddStatementGoto(1, file);
            IStatement st7 = storage.statements.AddStatementIf(1, file);
            IStatement st8 = storage.statements.AddStatementOperational(1, file);
            IStatement st9 = storage.statements.AddStatementReturn(1, file);
            IStatement st10 = storage.statements.AddStatementSwitch(1, file);
            IStatement st11 = storage.statements.AddStatementThrow(1, file);
            IStatement st12 = storage.statements.AddStatementTryCatchFinally(1, file);
            IStatement st13 = storage.statements.AddStatementUsing(1, file);

            st1.NextInLinearBlock = st2;
            st2.NextInLinearBlock = st3;
            st3.NextInLinearBlock = st4;
            st4.NextInLinearBlock = st5;
            st5.NextInLinearBlock = st6;
            st6.NextInLinearBlock = st7;
            st7.NextInLinearBlock = st8;
            st8.NextInLinearBlock = st9;
            st9.NextInLinearBlock = st10;
            st10.NextInLinearBlock = st11;
            st11.NextInLinearBlock = st12;
            st12.NextInLinearBlock = st13;

            Assert.IsTrue(st2.PreviousInLinearBlock == st1);
            Assert.IsTrue(st3.PreviousInLinearBlock == st2);
            Assert.IsTrue(st4.PreviousInLinearBlock == st3);
            Assert.IsTrue(st5.PreviousInLinearBlock == st4);
            Assert.IsTrue(st6.PreviousInLinearBlock == st5);
            Assert.IsTrue(st7.PreviousInLinearBlock == st6);
            Assert.IsTrue(st8.PreviousInLinearBlock == st7);
            Assert.IsTrue(st9.PreviousInLinearBlock == st8);
            Assert.IsTrue(st10.PreviousInLinearBlock == st9);
            Assert.IsTrue(st11.PreviousInLinearBlock == st10);
            Assert.IsTrue(st12.PreviousInLinearBlock == st11);
            Assert.IsTrue(st13.PreviousInLinearBlock == st12);
        }

        internal virtual IStatement CreateIStatement()
        {
            // TODO: Instantiate an appropriate concrete class.
            IStatement target = null;
            return target;
        }

        /// <summary>
        ///A test for SensorBeforTheStatement
        ///</summary>
        [TestMethod()]
        public void SensorBeforTheStatementTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st = storage.statements.AddStatementDoAndWhile(1, file);
            Assert.IsTrue(st.SensorBeforeTheStatement == null);

            storage.sensors.AddSensorBeforeStatement(1, st, Kind.FINAL);
            Assert.IsTrue(st.SensorBeforeTheStatement.ID == 1);

            IStatement st1 = storage.statements.AddStatementDoAndWhile(1, file);
            storage.sensors.AddSensorBeforeStatement(2, st1, Kind.FINAL);
            Assert.IsTrue(st1.SensorBeforeTheStatement.ID == 2);

            testUtils.Storage_Reopen(ref storage);
            st = storage.statements.GetStatement(1);
            st1 = storage.statements.GetStatement(2);
            Assert.IsTrue(st.SensorBeforeTheStatement.ID == 1);
            Assert.IsTrue(st1.SensorBeforeTheStatement.ID == 2);
        }

        /// <summary>
        ///A test for ImplementsFunction
        ///</summary>
        [TestMethod()]
        public void ImplementsFunctionTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st = storage.statements.AddStatementDoAndWhile(1, file);
            Assert.IsTrue(st.ImplementsFunction == null);

            IFunction func = storage.functions.AddFunction();

            func.EntryStatement = st;
            Assert.IsTrue(st.ImplementsFunction == func);

            testUtils.Storage_Reopen(ref storage);
            st = storage.statements.GetStatement(1);
            func = storage.functions.GetFunction(1);

            Assert.IsTrue(st.ImplementsFunction == func);
        }

        /// <summary>
        ///A test for ImplementsFunctionId
        ///</summary>
        [TestMethod()]
        public void ImplementsFunctionIdTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st = storage.statements.AddStatementDoAndWhile(1, file);
            Assert.IsTrue(st.ImplementsFunctionId == Store.Const.CONSTS.WrongID);

            IFunction func = storage.functions.AddFunction();

            func.EntryStatement = st;
            Assert.IsTrue(st.ImplementsFunctionId == func.Id);

            testUtils.Storage_Reopen(ref storage);
            st = storage.statements.GetStatement(1);
            func = storage.functions.GetFunction(1);

            Assert.IsTrue(st.ImplementsFunctionId == func.Id);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void SubStatementsTest()
        {
            IFunction func = storage.functions.AddFunction();

            IStatementSwitch sw = storage.statements.AddStatementSwitch();
            IOperation op = storage.operations.AddOperation();
            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();
            sw.AddCase(op, op1);
            sw.AddDefaultCase(op2);
            func.EntryStatement = sw;

            Assert.IsTrue(op1.ImplementsFunctionId == 1);
        }


        /// <summary>
        /// A test for AddNextInControl and NextInControl
        /// </summary>
        [TestMethod()]
        public void NextInControlSwitchBreakTest()
        {
            /*
             * Схема теста
             * switch/sw/ {
             *      case break /br/  
             *      case /cn/
             *      }
             * 
             * Результат:
             * 
             * Entry -> sw
             * sw -> br, cn, exit
             * br -> exit
             * cn -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementSwitch sw = storage.statements.AddStatementSwitch();
            IStatementBreak br = storage.statements.AddStatementBreak();
            IStatementOperational cn = storage.statements.AddStatementOperational();
            sw.AddCase(storage.operations.AddOperation(), br);
            sw.AddCase(storage.operations.AddOperation(), cn);

            func.EntryStatement = sw;

            IStatement[] sw_res = sw.NextInControl(false).ToArray();

            Assert.IsTrue(sw_res.SequenceEqual(new IStatement[] { func.ExitStatement, br, cn }));
            Assert.IsTrue(br.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
            Assert.IsTrue(cn.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl
        /// </summary>
        [TestMethod()]
        public void NextInControlHardTest()
        {
            /*
             * Схема теста 1
             * if /si/ 
             * then { return /sr/ }
             * else { /so/ }
             * /so2/
             * 
             * Результат:
             * 
             * Entry -> si
             * si -> sr, so
             * sr -> exit
             * so -> so2
             * so2 -> exit
            */


            IFunction func = storage.functions.AddFunction();

            IStatementIf si = storage.statements.AddStatementIf();
            func.EntryStatement = si;
            IStatementReturn sr = storage.statements.AddStatementReturn();
            si.ThenStatement = sr;
            IStatementOperational so = storage.statements.AddStatementOperational();
            si.ElseStatement = so;
            IStatementOperational so2 = storage.statements.AddStatementOperational();
            si.NextInLinearBlock = so2;

            Assert.IsTrue(si.NextInControl(false).SequenceEqual(new IStatement[] { sr, so }));
            Assert.IsTrue(sr.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
            Assert.IsTrue(so.NextInControl(false).SequenceEqual(new IStatement[] { so2 }));
            Assert.IsTrue(so2.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl for if statement
        /// </summary>
        [TestMethod()]
        public void NextInControlIf()
        {
            /*
             * Схема теста 1
             * if /si1/ 
             * then { /op1/ }
             * /op2/
             * 
             * Результат:
             * 
             * Entry -> si1
             * si1 -> op1, op2
             * op1 -> op2
             * op2 -> exit
            */

            /*
             * Схема теста 2
             * if /si2/ 
             * then { /op3/ }
             * else { /op4/ }
             * /op5/
             * 
             * Результат:
             * 
             * Entry -> si2
             * si2 -> op3, op4
             * op3 -> op5
             * op4 -> op5
             * op5 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementIf si1 = storage.statements.AddStatementIf();
            func.EntryStatement = si1;

            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();
            si1.ThenStatement = op1;
            si1.NextInLinearBlock = op2;

            Assert.IsTrue(si1.NextInControl(false).SequenceEqual(new IStatement[] { op1, op2 }));
            Assert.IsTrue(op1.NextInControl(false).SequenceEqual(new IStatement[] { op2 }));
            Assert.IsTrue(op2.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));

            IFunction func2 = storage.functions.AddFunction();

            IStatementIf si2 = storage.statements.AddStatementIf();
            func2.EntryStatement = si2;

            IStatementOperational op3 = storage.statements.AddStatementOperational();
            IStatementOperational op4 = storage.statements.AddStatementOperational();
            IStatementOperational op5 = storage.statements.AddStatementOperational();
            si2.ThenStatement = op3;
            si2.ElseStatement = op4;
            si2.NextInLinearBlock = op5;

            Assert.IsTrue(si2.NextInControl(false).SequenceEqual(new IStatement[] { op3, op4 }));
            Assert.IsTrue(op3.NextInControl(false).SequenceEqual(new IStatement[] { op5 }));
            Assert.IsTrue(op4.NextInControl(false).SequenceEqual(new IStatement[] { op5 }));
            Assert.IsTrue(op5.NextInControl(false).SequenceEqual(new IStatement[] { func2.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl for break statement
        /// </summary>
        [TestMethod()]
        public void NextInControlBreak()
        {
            /*
             * Схема теста
             * for/sf/ {
             *      /op1/
             *      /ob/ -> break
             *      /op2/
             *      }
             * /op3/
             * 
             * Результат:
             * 
             * Entry -> sf
             * sf -> op1, op3
             * op1 -> ob
             * ob -> op3
             * op2 ->
             * op3 -> exit
            */


            IFunction func = storage.functions.AddFunction();

            IStatementFor sf = storage.statements.AddStatementFor();
            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();
            IStatementOperational op3 = storage.statements.AddStatementOperational();
            IStatementBreak ob = storage.statements.AddStatementBreak();

            func.EntryStatement = sf;
            sf.Body = op1;
            op1.NextInLinearBlock = ob;
            ob.NextInLinearBlock = op2;
            sf.NextInLinearBlock = op3;

            IStatement[] sf_res = sf.NextInControl(false).ToArray();
            IStatement[] op1_res = op1.NextInControl(false).ToArray();
            IStatement[] op2_res = op2.NextInControl(false).ToArray();
            IStatement[] op3_res = op3.NextInControl(false).ToArray();
            IStatement[] ob_res = ob.NextInControl(false).ToArray();

            Assert.IsTrue(sf_res.SequenceEqual(new IStatement[] { op3, op1 }));
            Assert.IsTrue(ob_res.SequenceEqual(new IStatement[] { op3 }));
            Assert.IsTrue(op1_res.SequenceEqual(new IStatement[] { ob }));
            Assert.IsTrue(op2_res.SequenceEqual(new IStatement[] {  }));
            Assert.IsTrue(op3_res.Select(a => a.Id).SequenceEqual(new UInt64[] { 6 }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl for continue statement
        /// </summary>
        [TestMethod()]
        public void NextInControlContinue()
        {
            /*
             * Схема теста
             * for/sf/ {
             *      /op1/
             *      /ob/  continue
             *      /op2/
             *      }
             * /op3/
             * 
             * Результат:
             * 
             * Entry -> sf
             * sf -> op1, op3
             * op1 -> ob
             * ob -> sf
             * op2 ->
             * op3 -> exit
            */
            
            IFunction func = storage.functions.AddFunction();

            IStatementFor sf = storage.statements.AddStatementFor();
            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();
            IStatementOperational op3 = storage.statements.AddStatementOperational();
            IStatementContinue ob = storage.statements.AddStatementContinue();

            func.EntryStatement = sf;
            sf.Body = op1;
            op1.NextInLinearBlock = ob;
            ob.NextInLinearBlock = op2;
            sf.NextInLinearBlock = op3;

            IStatement[] sf_res = sf.NextInControl(false).ToArray();
            IStatement[] op1_res = op1.NextInControl(false).ToArray();
            IStatement[] op2_res = op2.NextInControl(false).ToArray();
            IStatement[] op3_res = op3.NextInControl(false).ToArray();
            IStatement[] ob_res = ob.NextInControl(false).ToArray();

            Assert.IsTrue(sf_res.SequenceEqual(new IStatement[] { op3, op1 }));
            Assert.IsTrue(ob_res.SequenceEqual(new IStatement[] { sf }));
            Assert.IsTrue(op1_res.SequenceEqual(new IStatement[] { ob }));
            Assert.IsTrue(op2_res.SequenceEqual(new IStatement[] { }));
            Assert.IsTrue(op3_res.Select(a => a.Id).SequenceEqual(new UInt64[] { 6 }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl for continue statement
        /// </summary>
        [TestMethod()]
        public void NextInControlContinueLoop()
        {
            /*
             * Схема теста
             * for/sf/ {
             *      /ob/  continue
             *      }
             * /op1/
             * 
             * Результат:
             * 
             * Entry -> sf
             * sf -> ob, op1
             * ob -> sf
             * op1 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementFor sf = storage.statements.AddStatementFor();
            IStatementContinue ob = storage.statements.AddStatementContinue();
            IStatementOperational op1 = storage.statements.AddStatementOperational();

            func.EntryStatement = sf;
            sf.Body = ob;
            sf.NextInLinearBlock = op1;

            Assert.IsTrue(sf.NextInControl(false).SequenceEqual(new IStatement[] { op1, ob }));
            Assert.IsTrue(ob.NextInControl(false).SequenceEqual(new IStatement[] { sf }));
            Assert.IsTrue(op1.NextInControl(false).Select(a => a.Id).SequenceEqual(new UInt64[] { 4 }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl for continue statement
        /// </summary>
        [TestMethod()]
        public void NextInControlContinueLoopFail()
        {
            /*
             * Схема теста
             * for/sf/ {
             *      /ob/  continue
             *      }
             * /op1/
             * 
             * Результат:
             * 
             * Entry -> sf
             * sf -> ob, op1
             * ob -> sf
             * op1 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementFor sf = storage.statements.AddStatementFor();
            IStatementContinue ob = storage.statements.AddStatementContinue();
            IStatementOperational op1 = storage.statements.AddStatementOperational();

            func.EntryStatement = sf;
            sf.Body = ob;
            sf.NextInLinearBlock = op1;

            var res1 = sf.NextInControl(false).ToList();
            Assert.IsTrue(sf.NextInControl(false).ToList().Count() == res1.Count());
        }


        /// <summary>
        /// A test for AddNextInControl and NextInControl for continue statement
        /// </summary>
        [TestMethod()]
        public void NextInControlOperational()
        {
            /*
             * Схема теста
             * /op1/
             * /op2/
             * 
             * Результат:
             * 
             * Entry -> op1
             * op1 -> op2
             * op2 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();

            func.EntryStatement = op1;
            op1.NextInLinearBlock = op2;


            Assert.IsTrue(op1.NextInControl(false).SequenceEqual(new IStatement[] { op2 }));
            Assert.IsTrue(op2.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl for for statement
        /// </summary>
        [TestMethod()]
        public void NextInControlFor()
        {
            /*
             * Схема теста
             * for/sf/ {
             *      /op1/
             *      }
             * /op2/
             * 
             * Результат:
             * 
             * Entry -> sf
             * sf -> op1, op2
             * op1 -> sf
             * op2 -> exit
            */
            
            IFunction func = storage.functions.AddFunction();

            IStatementFor sf = storage.statements.AddStatementFor();
            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();
            
            func.EntryStatement = sf;
            sf.Body = op1;
            sf.NextInLinearBlock = op2;

            Assert.IsTrue(sf.NextInControl(false).SequenceEqual(new IStatement[] { op2, op1 }));
            Assert.IsTrue(op1.NextInControl(false).SequenceEqual(new IStatement[] { sf }));
            Assert.IsTrue(op2.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl for for statement
        /// </summary>
        [TestMethod()]
        public void NextInControlForInner()
        {
            /*
             sens(1)
             * for1()
             * {
             *  sens(2)
             *  op();
             *  sens(3)
             *  for2()
             *  {
             *      sens(4)
             *      op2();
             *  }
             * }
             
             
             
             */
            storage.sensors.SetSensorStartNumber(1);
            var func = storage.functions.AddFunction();
            var for1 = func.EntryStatement = storage.statements.AddStatementFor();
            storage.sensors.AddSensorBeforeStatement(1, func.EntryStatement, Kind.START);
            var op = (for1 as IStatementFor).Body = storage.statements.AddStatementOperational();
            storage.sensors.AddSensorBeforeStatement(2, op, Kind.INTERNAL);
            var for2 = op.NextInLinearBlock = storage.statements.AddStatementFor();
            storage.sensors.AddSensorBeforeStatement(3, op.NextInLinearBlock, Kind.INTERNAL);
            var op2 = (for2 as IStatementFor).Body = storage.statements.AddStatementOperational();
            storage.sensors.AddSensorBeforeStatement(4, op2, Kind.INTERNAL);
            Assert.IsTrue(for1.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement, op }));
            Assert.IsTrue(op.NextInControl(false).SequenceEqual(new IStatement[] { for2 }));
            Assert.IsTrue(for2.NextInControl(false).SequenceEqual(new IStatement[] { for1, op2 }));
            Assert.IsTrue(op2.NextInControl(false).SequenceEqual(new IStatement[] { for2 }));
        }
        /// <summary>
        /// A test for AddNextInControl and NextInControl for foreach statement
        /// </summary>
        [TestMethod()]
        public void NextInControlForEach()
        {
            /*
             * Схема теста
             * foreach/sf/ {
             *      /op1/
             *      }
             * /op2/
             * 
             * Результат:
             * 
             * Entry -> sf
             * sf -> op1, op2
             * op1 -> sf
             * op2 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementForEach sf = storage.statements.AddStatementForEach();
            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();

            func.EntryStatement = sf;
            sf.Body = op1;
            sf.NextInLinearBlock = op2;

            Assert.IsTrue(sf.NextInControl(false).SequenceEqual(new IStatement[] { op2, op1 }));
            Assert.IsTrue(op1.NextInControl(false).SequenceEqual(new IStatement[] { sf }));
            Assert.IsTrue(op2.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl for do while statement
        /// </summary>
        [TestMethod()]
        public void NextInControlWhile()
        {
            /*
             * Схема теста
             * while/sf/ (something) {
             *      /op1/
             *      }
             * /op2/
             * 
             * Результат:
             * 
             * Entry -> sf
             * sf -> op1, op2
             * op1 -> sf
             * op2 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementDoAndWhile sf = storage.statements.AddStatementDoAndWhile();
            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();

            func.EntryStatement = sf;
            sf.IsCheckConditionBeforeFirstRun = true;
            sf.Body = op1;
            sf.NextInLinearBlock = op2;

            IStatement[] sf_res = sf.NextInControl(false).ToArray();

            Assert.IsTrue(TestUtils.TestUtilsClass.compareSets<IStatement>(sf_res, new IStatement[] { op1, op2 }));
            Assert.IsTrue(op1.NextInControl(false).SequenceEqual(new IStatement[] { sf }));
            Assert.IsTrue(op2.NextInControl(false).Select(a => a.Id).SequenceEqual(new UInt64[] { 4 }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl for do while statement
        /// </summary>
        [TestMethod()]
        public void NextInControlDo()
        {
            /*
             * Схема теста
             * do/sf/ {
             *      /op1/
             *      }
             * while (something)
             
             * /op2/
             * 
             * Результат:
             * 
             * Entry -> sf
             * sf -> op1, op2
             * op1 -> sf
             * op2 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementDoAndWhile sf = storage.statements.AddStatementDoAndWhile();
            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();

            func.EntryStatement = sf;
            sf.IsCheckConditionBeforeFirstRun = false;
            sf.Body = op1;
            sf.NextInLinearBlock = op2;

            IStatement[] sf_res = sf.NextInControl(false).ToArray();
            IStatement[] op1_res = op1.NextInControl(false).ToArray();

            Assert.IsTrue(TestUtils.TestUtilsClass.compareSets<IStatement>(sf_res , new IStatement[] { op1, op2 }));
            Assert.IsTrue(op1_res.SequenceEqual(new IStatement[] { sf }));
            Assert.IsTrue(op2.NextInControl(false).Select(a => a.Id).SequenceEqual(new UInt64[] { 4 }));
        }


        /// <summary>
        /// A test for AddNextInControl and NextInControl for do while statement
        /// </summary>
        [TestMethod()]
        public void NextInControlDoAndWhileBefore()
        {
            /*
             * Схема теста
             * while (something) /sf/ {
             *      /op1/
             *      }
             * /op2/
             * 
             * Результат:
             * 
             * Entry -> sf
             * sf -> op1, op2
             * op1 -> sf
             * op2 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementDoAndWhile sf = storage.statements.AddStatementDoAndWhile();
            IStatementOperational op1 = storage.statements.AddStatementOperational();
            IStatementOperational op2 = storage.statements.AddStatementOperational();

            func.EntryStatement = sf;
            sf.IsCheckConditionBeforeFirstRun = true;
            sf.Body = op1;
            sf.NextInLinearBlock = op2;

            Assert.IsTrue(sf.NextInControl(false).SequenceEqual(new IStatement[] { op2, op1 }));
            Assert.IsTrue(op1.NextInControl(false).SequenceEqual(new IStatement[] { sf }));
            Assert.IsTrue(op2.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl
        /// </summary>
        [TestMethod()]
        public void NextInControlReturnTest()
        {
            /*
             * Схема теста
             * if /si/ 
             * then { return /sr/ }
             * else { /so/ }
             * /so2/
             * 
             * Результат:
             * 
             * Entry -> si
             * si -> sr, so
             * sr -> exit
             * so -> so2
             * so2 -> exit
            */



            IFunction func = storage.functions.AddFunction();

            IStatementIf si = storage.statements.AddStatementIf();
            func.EntryStatement = si;
            IStatementReturn sr = storage.statements.AddStatementReturn();
            si.ThenStatement = sr;
            IStatementOperational so = storage.statements.AddStatementOperational();
            si.ElseStatement = so;
            IStatementOperational so2 = storage.statements.AddStatementOperational();
            si.NextInLinearBlock = so2;

            Assert.IsTrue(si.NextInControl(false).SequenceEqual(new IStatement[] { sr, so }));
            Assert.IsTrue(sr.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
            Assert.IsTrue(so.NextInControl(false).SequenceEqual(new IStatement[] { so2 }));
            Assert.IsTrue(so2.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl
        /// </summary>
        [TestMethod()]
        public void NextInControlSwitchTest()
        {
            /*
             * Схема теста
             * switch/sw/ {
             *      case /so/  
             *      case /so2/
             *      default /so3/
             *      }
             * 
             * Результат:
             * 
             * Entry -> sw
             * sw -> so, so2, so3
             * so -> so2
             * so2 -> so3
             * so3 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementSwitch sw = storage.statements.AddStatementSwitch();
            IStatementOperational so = storage.statements.AddStatementOperational();
            IStatementOperational so2 = storage.statements.AddStatementOperational();
            IStatementOperational so3 = storage.statements.AddStatementOperational();

            func.EntryStatement = sw;

            sw.AddCase(storage.operations.AddOperation(), so);
            sw.AddCase(storage.operations.AddOperation(), so2);
            sw.AddDefaultCase(so3);

            Assert.IsTrue(sw.NextInControl(false).SequenceEqual(new IStatement[] { so, so2, so3 }));
            Assert.IsTrue(so.NextInControl(false).SequenceEqual(new IStatement[] { so2 }));
            Assert.IsTrue(so2.NextInControl(false).SequenceEqual(new IStatement[] { so3 }));
            Assert.IsTrue(so3.NextInControl(false).Select(a => a.Id).SequenceEqual(new UInt64[] { 5 }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl
        /// </summary>
        [TestMethod()]
        public void NextInControlTryCatchTest()
        {
            /*
             * Схема теста
             * try /tr/ {
             *      /so/  
             *      /so2/
             *      }
             * catch { /so3/ }
             * 
             * Результат:
             * 
             * Entry -> tc
             * tc -> so
             * so -> so2, so3, exit
             * so2 -> so3, exit
             * so3 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementTryCatchFinally tc = storage.statements.AddStatementTryCatchFinally();
            IStatementOperational so = storage.statements.AddStatementOperational();
            IStatementOperational so2 = storage.statements.AddStatementOperational();
            IStatementOperational so3 = storage.statements.AddStatementOperational();

            func.EntryStatement = tc;
            tc.TryBlock = so;
            so.NextInLinearBlock = so2;
            tc.AddCatch(so3);

            IStatement[] tc_res = tc.NextInControl(false).ToArray();
            IStatement[] so_res = so.NextInControl(false).ToArray();
            IStatement[] so2_res = so2.NextInControl(false).ToArray();

            Assert.IsTrue(tc_res.SequenceEqual(new IStatement[] { so }));
            Assert.IsTrue(so_res.SequenceEqual(new IStatement[] { so2, so3, func.ExitStatement }));
            Assert.IsTrue(so2_res.SequenceEqual(new IStatement[] { func.ExitStatement, so3 }));
            Assert.IsTrue(so3.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl
        /// </summary>
        [TestMethod()]
        public void NextInControlTryCatchFinallyTest()
        {
            /*
             * Схема теста
             * try /tr/ {
             *      /so/  
             *      /so2/
             *      }
             * catch { /so3/ }
             * finally { /so4/ }
             * 
             * Результат:
             * 
             * Entry -> tc
             * tc -> so
             * so -> so2, so3, so4
             * so2 -> so3, so4
             * so3 -> so4
             * so4 -> exit
            */



            IFunction func = storage.functions.AddFunction();

            IStatementTryCatchFinally tc = storage.statements.AddStatementTryCatchFinally();
            IStatementOperational so = storage.statements.AddStatementOperational();
            IStatementOperational so2 = storage.statements.AddStatementOperational();
            IStatementOperational so3 = storage.statements.AddStatementOperational();
            IStatementOperational so4 = storage.statements.AddStatementOperational();

            func.EntryStatement = tc;
            tc.TryBlock = so;
            so.NextInLinearBlock = so2;
            tc.AddCatch(so3);
            tc.FinallyBlock = so4;

            IStatement[] tc_res = tc.NextInControl(false).ToArray();
            IStatement[] so_res = so.NextInControl(false).ToArray();
            IStatement[] so2_res = so2.NextInControl(false).ToArray();

            Assert.IsTrue(tc_res.SequenceEqual(new IStatement[] { so }));
            Assert.IsTrue(so_res.SequenceEqual(new IStatement[] { so2, so3, so4 }));
            var v = so2.NextInControl(false).ToArray();
            Assert.IsTrue(so2_res.SequenceEqual(new IStatement[] { so4, so3 }));
            Assert.IsTrue(so3.NextInControl(false).SequenceEqual(new IStatement[] { so4 }));
            Assert.IsTrue(so4.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl
        /// </summary>
        [TestMethod()]
        public void NextInControlTryCatchExceptFinallyTest()
        {
            /*
             * Схема теста
             * try /tr/ {
             *      /so/  
             *      /so2/
             *      }
             * catch { /so3/ }
             * else { /so4/ }
             * finally { /so5/ }
             * 
             * Результат:
             * 
             * Entry -> tc
             * tc -> so
             * so -> so2, so3, so5
             * so2 -> so3, so4, so5
             * so3 -> so5
             * so4 -> so5
             * so5 -> exit
            */



            IFunction func = storage.functions.AddFunction();

            IStatementTryCatchFinally tc = storage.statements.AddStatementTryCatchFinally();
            IStatementOperational so = storage.statements.AddStatementOperational();
            IStatementOperational so2 = storage.statements.AddStatementOperational();
            IStatementOperational so3 = storage.statements.AddStatementOperational();
            IStatementOperational so4 = storage.statements.AddStatementOperational();
            IStatementOperational so5 = storage.statements.AddStatementOperational();

            func.EntryStatement = tc;
            tc.TryBlock = so;
            so.NextInLinearBlock = so2;
            tc.AddCatch(so3);
            tc.ElseBlock = so4;
            tc.FinallyBlock = so5;

            IStatement[] tc_res = tc.NextInControl(false).ToArray();
            IStatement[] so_res = so.NextInControl(false).ToArray();
            IStatement[] so2_res = so2.NextInControl(false).ToArray();

            Assert.IsTrue(tc_res.SequenceEqual(new IStatement[] { so }));
            Assert.IsTrue(so_res.SequenceEqual(new IStatement[] { so2, so3, so5 }));
            var v = so2.NextInControl(false).ToArray();
            Assert.IsTrue(so2_res.SequenceEqual(new IStatement[] { so4, so3, so5 }));
            Assert.IsTrue(so3.NextInControl(false).SequenceEqual(new IStatement[] { so5 }));
            Assert.IsTrue(so4.NextInControl(false).SequenceEqual(new IStatement[] { so5 }));
            Assert.IsTrue(so5.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl
        /// </summary>
        [TestMethod()]
        public void NextInControlThrowTest()
        {
            /*
             * Схема теста
             * try /tr/ {
             *      /so/  
             *      throw /th/
             *      /so2/
             *      }
             * catch { /so3/ }
             * catch { /so4/ }
             * 
             * Результат:
             * 
             * Entry -> tc
             * tc -> so
             * so -> th, so3, so4, exit
             * th -> so3, so4, exit
             * so2 -> 
             * so3 -> exit
             * so4 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementTryCatchFinally tc = storage.statements.AddStatementTryCatchFinally();
            IStatementOperational so = storage.statements.AddStatementOperational();
            IStatementOperational so2 = storage.statements.AddStatementOperational();
            IStatementOperational so3 = storage.statements.AddStatementOperational();
            IStatementOperational so4 = storage.statements.AddStatementOperational();
            IStatementThrow th = storage.statements.AddStatementThrow();

            func.EntryStatement = tc;
            tc.TryBlock = so;
            so.NextInLinearBlock = th;
            th.NextInLinearBlock = so2;
            tc.AddCatch(so3);
            tc.AddCatch(so4);

            IStatement[] tc_res = tc.NextInControl(false).ToArray();
            IStatement[] so_res = so.NextInControl(false).ToArray();


            Assert.IsTrue(tc_res.SequenceEqual(new IStatement[] { so }));
            Assert.IsTrue(so_res.SequenceEqual(new IStatement[] { th, so3, so4, func.ExitStatement }));
            Assert.IsTrue(th.NextInControl(false).SequenceEqual(new IStatement[] { so3, so4, func.ExitStatement }));
            Assert.IsTrue(so2.NextInControl(false).SequenceEqual(new IStatement[] { }));
            Assert.IsTrue(so3.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
            Assert.IsTrue(so4.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for AddNextInControl and NextInControl
        /// </summary>
        [TestMethod()]
        public void NextInControlUsingTest()
        {
            /*
             * Схема теста
             * using /tc/ {
             *      /so/  
             *      }
             * /so2/
             * 
             * Результат:
             * 
             * Entry -> tc
             * tc -> so
             * so -> so2
             * so2 -> exit
            */

            IFunction func = storage.functions.AddFunction();

            IStatementUsing tc = storage.statements.AddStatementUsing();
            IStatementOperational so = storage.statements.AddStatementOperational();
            IStatementOperational so2 = storage.statements.AddStatementOperational();

            func.EntryStatement = tc;
            tc.Body = so;
            tc.NextInLinearBlock = so2;

            Assert.IsTrue(tc.NextInControl(false).SequenceEqual(new IStatement[] { so }));
            Assert.IsTrue(so.NextInControl(false).SequenceEqual(new IStatement[] { so2 }));
            Assert.IsTrue(so2.NextInControl(false).SequenceEqual(new IStatement[] { func.ExitStatement }));
        }

        /// <summary>
        /// A test for 
        /// </summary>
        [TestMethod()]
        public void CallesFunctionsTest()
        {
            IFunction func = storage.functions.AddFunction();
            IFunction func1 = storage.functions.AddFunction();
            IFunction func2 = storage.functions.AddFunction();
            IFunction func3 = storage.functions.AddFunction();
            IFunction func4 = storage.functions.AddFunction();

            IStatementSwitch sw = storage.statements.AddStatementSwitch();
            sw.Condition = storage.operations.AddOperation();

            IStatementOperational op = storage.statements.AddStatementOperational();
            op.Operation = storage.operations.AddOperation();

            Assert.IsTrue(sw.CallesFunctions().Count() == 0);

            sw.Condition.AddFunctionCallInSequenceOrder(new IFunction[]{func1});
            Assert.IsTrue(sw.CallesFunctions().SequenceEqual(new IFunction[] { func1 }));
            Assert.IsTrue(op.CallesFunctions().Count() == 0);

            sw.Condition.AddFunctionCallInSequenceOrder(new IFunction[] { func2 });
            Assert.IsTrue(sw.CallesFunctions().SequenceEqual(new IFunction[] { func1, func2 }));
            Assert.IsTrue(op.CallesFunctions().Count() == 0);

            sw.Condition.AddFunctionCallInSequenceOrder(new IFunction[] { func2 });
            Assert.IsTrue(sw.CallesFunctions().SequenceEqual(new IFunction[] { func1, func2 }));
            Assert.IsTrue(op.CallesFunctions().Count() == 0);

            op.Operation.AddFunctionCallInSequenceOrder(new IFunction[] { func2 });
            Assert.IsTrue(sw.CallesFunctions().SequenceEqual(new IFunction[] { func1, func2 }));
            Assert.IsTrue(op.CallesFunctions().SequenceEqual(new IFunction[] { func2 }));

        }

    }
}
