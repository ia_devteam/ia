using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;

namespace TestStorage
{
    /// <summary>
    /// Summary description for Log
    /// </summary>
    [TestClass]
    public class Log
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// ���������� � ������� �����
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        [TestMethod]
        public void LogTest()
        {
            Store.Log log = storage.logs("test.log");

            log.Warning("aaa");
            log.Warning("aaa");
            log.Warning("aaa");
            log.Warning("bbb");
            log.StopWriting();

            int i = 0;
            foreach (string str in log.ReadLog())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(str == "aaa");
                        break;
                    case 1:
                        Assert.IsTrue(str == "aaa");
                        break;
                    case 2:
                        Assert.IsTrue(str == "aaa");
                        break;
                    case 3:
                        Assert.IsTrue(str == "bbb");
                        break;
                    default:
                        Assert.Fail();
                        break;
                }
                i++;
            }

            log.Clear();

            foreach (string str in log.ReadLog())
            {
                Assert.Fail();
            }
        }
    }
}
