﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for ClassTest and is intended
    ///To contain all ClassTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ClassTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for EnumerateMethods
        ///</summary>
        [TestMethod()]
        public void EnumerateMethodsTest()
        {
            Class clazz1 = storage.classes.AddClass();
            Class clazz2 = storage.classes.AddClass();

            IFunction func1 = clazz1.AddMethod(true);
            IFunction func2 = clazz1.AddMethod(true);
            IFunction func3 = clazz1.AddMethod(false);
            IFunction func4 = clazz2.AddMethod(false);
            IFunction func5 = clazz2.AddMethod(false);

            func1.Name = "func1";
            func2.Name = "func2";
            func3.Name = "func3";
            func4.Name = "func4";
            func5.Name = "func5";

            int i = 0;
            foreach (IFunction func in clazz1.EnumerateMethods())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(func == func1);
                        Assert.IsTrue((func as IMethod).isOverrideBase == true );
                        break;
                    case 1:
                        Assert.IsTrue(func == func2);
                        break;
                    case 2:
                        Assert.IsTrue(func == func3);
                        Assert.IsTrue((func as IMethod).isOverrideBase == false );
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 3);

            i = 0;
            foreach (IFunction func in clazz2.EnumerateMethods())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(func == func4);
                        break;
                    case 1:
                        Assert.IsTrue(func == func5);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);

            testUtils.Storage_Reopen(ref storage);

            clazz1 = storage.classes.GetClass(1);
            clazz2 = storage.classes.GetClass(2);

            i = 0;
            foreach (IFunction func in clazz1.EnumerateMethods())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(func.Name == "func1");
                        break;
                    case 1:
                        Assert.IsTrue(func.Name == "func2");
                        break;
                    case 2:
                        Assert.IsTrue(func.Name == "func3");
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 3);

            i = 0;
            foreach (IFunction func in clazz2.EnumerateMethods())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(func.Name == "func4");
                        break;
                    case 1:
                        Assert.IsTrue(func.Name == "func5");
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        /// <summary>
        ///A test for Name
        ///</summary>
        [TestMethod()]
        public void ClassNameTest()
        {
            Class class1 = storage.classes.AddClass();
            Assert.IsTrue(class1.Name == "");

            class1.Name = "a1";
            Assert.IsTrue(class1.Name == "a1");

            testUtils.Storage_Reopen(ref storage);

            class1 = storage.classes.GetClass(1);
            Assert.IsTrue(class1.Name == "a1");
        }

        /// <summary>
        ///A test for Childs
        ///</summary>
        [TestMethod()]
        public void ChildsTest()
        {
            Class class1;
            Class class2;
            Class class3;
            Class class4;
            CreateTree(out class1, out class2, out class3, out class4);
            
            CheckChields(class1, class2, class3, class4, null);

            Class class5;
            class5 = storage.classes.AddClass();
            class5.AddParent(class2);

            CheckChields(class1, class2, class3, class4, class5);

            testUtils.Storage_Reopen(ref storage);

            class1 = storage.classes.GetClass(1);
            class2 = storage.classes.GetClass(2);
            class3 = storage.classes.GetClass(3);
            class4 = storage.classes.GetClass(4);
            class5 = storage.classes.GetClass(5);


            CheckChields(class1, class2, class3, class4, class5);
        }

        private void CreateTree(out Class class1, out Class class2, out Class class3, out Class class4)
        {
            class1 = storage.classes.AddClass();
            class2 = storage.classes.AddClass();
            class3 = storage.classes.AddClass();
            class4 = storage.classes.AddClass();

            class2.AddParent(class1);
            class2.AddParent(class3);
            class4.AddParent(class2);
        }

        private static void CheckParrents(Class class1, Class class2, Class class3, Class class4, Class class5)
        {
            int i = 0;
            foreach (Class clazz in class2.EnumerateParents())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(clazz == class1);
                        break;
                    case 1:
                        Assert.IsTrue(clazz == class3);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);

            i = 0;
            foreach (Class clazz in class4.EnumerateParents())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(clazz == class2);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 1);

            if (class5 != null)
            {
                i = 0;
                foreach (Class clazz in class5.EnumerateParents())
                {
                    switch (i)
                    {
                        case 0:
                            Assert.IsTrue(clazz == class2);
                            break;
                    }
                    i++;
                }
                Assert.IsTrue(i == 1);
            }

            Assert.IsTrue(class1.EnumerateParents().GetEnumerator().MoveNext() == false);
            Assert.IsTrue(class3.EnumerateParents().GetEnumerator().MoveNext() == false);


        }

        /// <summary>
        ///A test for Parrents
        ///</summary>
        [TestMethod()]
        public void ParrentsTest()
        {
            Class class1;
            Class class2;
            Class class3;
            Class class4;
            CreateTree(out class1, out class2, out class3, out class4);

            CheckParrents(class1, class2, class3, class4, null);

            Class class5;
            class5 = storage.classes.AddClass();
            class5.AddParent(class2);

            CheckParrents(class1, class2, class3, class4, class5);

            testUtils.Storage_Reopen(ref storage);

            class1 = storage.classes.GetClass(1);
            class2 = storage.classes.GetClass(2);
            class3 = storage.classes.GetClass(3);
            class4 = storage.classes.GetClass(4);
            class5 = storage.classes.GetClass(5);

            CheckParrents(class1, class2, class3, class4, class5);
        }

        private static void CheckChields(Class class1, Class class2, Class class3, Class class4, Class class5)
        {
            int i = 0;
            foreach (Class clazz in class1.EnumerateChilds())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(clazz == class2);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 1);

            i = 0;
            foreach (Class clazz in class2.EnumerateChilds())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(clazz == class4);
                        break;
                    case 1:
                        if (class5 != null)
                            Assert.IsTrue(clazz == class5);
                        else
                            Assert.Fail();
                        break;
                }
                i++;
            }
            if(class5 == null)
                Assert.IsTrue(i == 1);
            else
                Assert.IsTrue(i == 2);

            i = 0;
            foreach (Class clazz in class3.EnumerateChilds())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(clazz == class2);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 1);

            Assert.IsTrue(class4.EnumerateChilds().GetEnumerator().MoveNext() == false);
            
            if(class5 != null)
                Assert.IsTrue(class5.EnumerateChilds().GetEnumerator().MoveNext() == false);
        }

        /// <summary>
        ///A test for EnumerateFields
        ///</summary>
        [TestMethod()]
        public void EnumerateFieldsTest()
        {
            Class clazz1 = storage.classes.AddClass();
            Class clazz2 = storage.classes.AddClass();

            Assert.IsTrue(clazz1.EnumerateFields().GetEnumerator().MoveNext() == false);

            Field f1 = clazz1.AddField(false);
            Field f2 = clazz1.AddField(true);

            clazz2.AddField(false);

            Assert.IsTrue(f1.Name == "");

            f1.Name = "a";
            f2.Name = "b";

            TestUtilsClass.CheckArray<Field>((a, b) => { return a == b; }, clazz1.EnumerateFields(), f1, f2);

            testUtils.Storage_Reopen(ref storage);

            clazz1 = storage.classes.GetClass(1);

            f1 = (Field) storage.variables.GetVariable(1);
            f2 = (Field)storage.variables.GetVariable(2);

            TestUtilsClass.CheckArray<Field>((a, b) => { return a == b; }, clazz1.EnumerateFields(), f1, f2);
        }

        /// <summary>
        ///A test for EnumerateInnerClasses
        ///</summary>
        [TestMethod()]
        public void EnumerateInnerClassesTest()
        {
            Class cl1 = storage.classes.AddClass();
            Class cl2 = storage.classes.AddClass();
            Class cl3 = storage.classes.AddClass();

            Class c1 = storage.classes.AddClass();
            Class c2 = storage.classes.AddClass();
            c1.AddInnerClass(cl1);
            c2.AddInnerClass(cl2);
            c1.AddInnerClass(cl3);

            TestUtilsClass.CheckArray<Class>((arg, orig) => { return arg == orig; }, c1.EnumerateInnerClasses(), new Class[] { cl1, cl3 });
            TestUtilsClass.CheckArray<Class>((arg, orig) => { return arg == orig; }, c2.EnumerateInnerClasses(), new Class[] { cl2 });
        }

        /// <summary>
        ///A test for FullName
        ///</summary>
        [TestMethod()]
        public void FullNameClassTest()
        {
            Namespace ns = storage.namespaces.FindOrCreate(new string[] { "ns1", "ns2" });
            
            Class cl1 = storage.classes.AddClass();
            cl1.Name = "cl1";
            ns.AddClassToNamespace(cl1);

            Class cl2 = storage.classes.AddClass();
            cl2.Name = "cl2";
            cl1.AddInnerClass(cl2);

            Assert.IsTrue(cl2.FullName == "ns1.ns2.cl1.cl2");

            Class cl3 = storage.classes.AddClass();
            cl3.Name = "cl3";
            Assert.IsTrue(cl3.FullName == "cl3");
        }

        /// <summary>
        ///A test for EnumerateAccessibleMethods
        ///</summary>
        [TestMethod()]
        public void EnumerateAccesibleMethodsTest()
        {
            Class cl1 = storage.classes.AddClass();
            cl1.Name = "cl1";
            Class cl2 = storage.classes.AddClass();
            cl2.Name = "cl2";
            Class cl3 = storage.classes.AddClass();
            cl3.Name = "cl3";

            cl2.AddParent(cl1);
            cl3.AddParent(cl2);

            IMethod m1 = cl1.AddMethod(false);
            IMethod m2 = cl1.AddMethod(false);
            IMethod m11 = cl3.AddMethod(false);

            m1.Name = "m1";
            m11.Name = "m11";

            Assert.IsTrue(cl3.EnumerateAccessibleMethods("m2").GetEnumerator().MoveNext() == false);
            
            TestUtilsClass.CheckArray<IMethod>((a1, a2) => {return a1.Name == a2.Name;}, cl3.EnumerateAccessibleMethods("m1"), new IMethod[] {m1} );
            TestUtilsClass.CheckArray<IMethod>((a1, a2) => { return a1.Name == a2.Name; }, cl3.EnumerateAccessibleMethods("m11"), new IMethod[] { m11 });

            m2.Name = "m2";
            TestUtilsClass.CheckArray<IMethod>((a1, a2) => { return a1.Name == a2.Name; }, cl3.EnumerateAccessibleMethods("m2"), new IMethod[] { m2 });

            IMethod m3 = cl1.AddMethod(false);
            m3.Name = "m3";
            TestUtilsClass.CheckArray<IMethod>((a1, a2) => { return a1.Name == a2.Name; }, cl3.EnumerateAccessibleMethods("m3"), new IMethod[] { m3 });

            IMethod m4 = cl2.AddMethod(false);
            m4.Name = "m4";
            TestUtilsClass.CheckArray<IMethod>((a1, a2) => { return a1.Name == a2.Name; }, cl3.EnumerateAccessibleMethods("m4"), new IMethod[] { m4 });

            IMethod m5 = cl3.AddMethod(false);
            m5.Name = "m5";
            TestUtilsClass.CheckArray<IMethod>((a1, a2) => { return a1.Name == a2.Name; }, cl3.EnumerateAccessibleMethods("m5"), new IMethod[] { m5 });


            Class cl4 = storage.classes.AddClass();
            IMethod m6 = cl4.AddMethod(false);
            m6.Name = "m6";
            cl2.AddParent(cl4);
            TestUtilsClass.CheckArray<IMethod>((a1, a2) => { return a1.Name == a2.Name; }, cl3.EnumerateAccessibleMethods("m6"), new IMethod[] { m6 });

        }
    }
}
