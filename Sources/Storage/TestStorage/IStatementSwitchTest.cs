﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IStatementSwitchTest and is intended
    ///To contain all IStatementSwitchTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IStatementSwitchTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }
        
        /// <summary>
        ///A test for AddCase
        ///</summary>
        [TestMethod()]
        public void AddCaseTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            IStatementSwitch sw = storage.statements.AddStatementSwitch(1, file);
            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            IStatement st2 = storage.statements.AddStatementContinue(1, file);
            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();
            sw.AddCase(op1, st1);
            
            sw.AddCase(op2, st2);

            CheckCases(sw);

            testUtils.Storage_Reopen(ref storage);

            sw = (IStatementSwitch) storage.statements.GetStatement(1);

            CheckCases(sw);
        }

        private static void CheckCases(IStatementSwitch sw)
        {
            int i = 0;
            foreach (KeyValuePair<IOperation,  IStatement> pair in sw.Cases())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(pair.Value is IStatementBreak);
                        Assert.IsTrue(pair.Key.Id == 1);
                        break;
                    case 1:
                        Assert.IsTrue(pair.Value is IStatementContinue);
                        Assert.IsTrue(pair.Key.Id == 2);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        /// <summary>
        ///A test for Cases
        ///</summary>
        [TestMethod()]
        public void CasesTest()
        {
            //Тестируется методом AddCaseTest
        }

        /// <summary>
        ///A test for DefaultCase
        ///</summary>
        [TestMethod()]
        public void DefaultCaseTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            IStatementSwitch sw = storage.statements.AddStatementSwitch(1, file);
            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            IStatement st2 = storage.statements.AddStatementContinue(1, file);
            IStatement st3 = storage.statements.AddStatementFor(1, file);

            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();

            Assert.IsTrue(sw.DefaultCase() == null);

            sw.AddCase(op1, st3);
            Assert.IsTrue(sw.DefaultCase() == null);

            sw.AddDefaultCase(st2);
            Assert.IsTrue(sw.DefaultCase() is IStatementContinue);

            sw.AddCase(op2, st1);
            Assert.IsTrue(sw.DefaultCase() is IStatementContinue);



            testUtils.Storage_Reopen(ref storage);

            sw = (IStatementSwitch) storage.statements.GetStatement(1);

            Assert.IsTrue(sw.DefaultCase() is IStatementContinue);
        }

        /// <summary>
        ///A test for Condition
        ///</summary>
        [TestMethod()]
        public void ConditionTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            IStatementSwitch d = storage.statements.AddStatementSwitch(1, file);
            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();

            Assert.IsTrue(d.Condition == null);

            d.Condition = op2;

            Assert.IsTrue(d.Condition.Id == 2);

            testUtils.Storage_Reopen(ref storage);

            Assert.IsTrue((storage.statements.GetStatement(1) as IStatementSwitch).Condition.Id == 2);
        }

        /// <summary>
        ///A test for SubStatements
        ///</summary>
        [TestMethod()]
        public void SwitchSubStatementsTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "1");
            IStatementSwitch sw = storage.statements.AddStatementSwitch(1, file);
            IStatement st1 = storage.statements.AddStatementBreak(1, file);
            IStatement st2 = storage.statements.AddStatementContinue(1, file);
            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();

            sw.AddCase(op1, st1);
            sw.AddCase(op2, st2);

            int i = 0;
            foreach (IStatement st in sw.SubStatements())
            {
                switch(i)
                {
                    case 0:
                        Assert.IsTrue(st is IStatementBreak);
                        break;
                    case 1:
                        Assert.IsTrue(st is IStatementContinue);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);
        }

        /// <summary>
        ///A test for Cases
        ///</summary>
        [TestMethod()]
        public void CasesTest1()
        {
            int arrSize = 2000;
            int swSize = 10;


            //Все это творение закидываем в функцию
            IFunction func = storage.functions.AddFunction();

            IStatementSwitch[] list = new IStatementSwitch[arrSize];

            //Генерим 10000 операторов switch, в каждом из которых по 100 case
            for (int i = 0; i < arrSize; i++)
            {
                list[i] = storage.statements.AddStatementSwitch();
                for (int j = 0; j < swSize; j++)
                {
                    IOperation op = storage.operations.AddOperation();
                    IStatementOperational s = storage.statements.AddStatementOperational();
                    list[i].AddCase(op, s);
                }

                if(i>0)
                    list[i-1].NextInLinearBlock = list[i];
                else
                    func.EntryStatement = list[0];
            }


            //Теперь проверяем, что все сохранилось
            int k = 0;
            IStatementSwitch sw = (IStatementSwitch) func.EntryStatement;
            while (sw != null)
            {
                k++;
                int l = 0;
                foreach (IStatement st in sw.SubStatements())
                {
                    l++;
                    Assert.IsTrue(st.ImplementsFunctionId == 1);
                }
                Assert.IsTrue(l == swSize);
                sw = (IStatementSwitch) sw.NextInLinearBlock;
            }

            Assert.IsTrue(k == arrSize);


        }

        /// <summary>
        ///Another test for Cases
        ///</summary>
        [TestMethod()]
        public void HardCoreTest()
        {
            IFunction func = storage.functions.AddFunction();
            int i = 0;
            IStatement sw = storage.statements.AddStatementSwitch();
            IStatementBreak br = storage.statements.AddStatementBreak();
            IStatementBreak br2 = storage.statements.AddStatementBreak();
            IStatementBreak br3 = storage.statements.AddStatementBreak();
            IOperation op = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();
            ((IStatementSwitch)sw).AddCase(op, br);
            ((IStatementSwitch)sw).AddCase(op2, br2);
            ((IStatementSwitch)sw).AddDefaultCase(br3);
            IStatement first = sw;
            for (i = 0; i < 3000; i++)
            {
                sw = sw.NextInLinearBlock = storage.statements.AddStatementSwitch();
                br = storage.statements.AddStatementBreak();
                br2 = storage.statements.AddStatementBreak();
                br3 = storage.statements.AddStatementBreak();
                op = storage.operations.AddOperation();
                op2 = storage.operations.AddOperation();
                ((IStatementSwitch)sw).AddCase(op, br);
                ((IStatementSwitch)sw).AddCase(op2, br2);
                ((IStatementSwitch)sw).AddDefaultCase(br3);
            }
            func.EntryStatement = first;
            foreach (IStatement sss in storage.statements.EnumerateStatements())
                Assert.IsTrue(sss.ImplementsFunctionId != Store.Const.CONSTS.WrongID);
        }


        internal virtual IStatementSwitch CreateIStatementSwitch()
        {
            // TODO: Instantiate an appropriate concrete class.
            IStatementSwitch target = null;
            return target;
        }

        /// <summary>
        ///A test for AddCase
        ///</summary>
        [TestMethod()]
        public void SubStatementsSwitch()
        {
            IStatementSwitch sw = storage.statements.AddStatementSwitch();

            IStatement c1 = storage.statements.AddStatementGoto();
            IStatement c2 = storage.statements.AddStatementGoto();
            IStatement c3 = storage.statements.AddStatementGoto();
            IStatement c4 = storage.statements.AddStatementGoto();

            IOperation op1 = storage.operations.AddOperation();
            IOperation op2 = storage.operations.AddOperation();
            IOperation op3 = storage.operations.AddOperation();
            IOperation op4 = storage.operations.AddOperation();

            sw.AddCase(op1, c1);
            sw.AddCase(op2, c2);
            sw.AddCase(op3, c3);
            sw.AddCase(op4, c4);

            testUtils.Storage_Reopen(ref storage);
            sw = (IStatementSwitch) storage.statements.GetStatement(1);

            int i = 0;
            sw.SubStatements();
            foreach (IStatement st in sw.SubStatements())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(st.Id == 2);
                        break;
                    case 1:
                        Assert.IsTrue(st.Id == 3);
                        break;
                    case 2:
                        Assert.IsTrue(st.Id == 4);
                        break;
                    case 3:
                        Assert.IsTrue(st.Id == 5);
                        break;
                    default:
                        Assert.Fail();
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 4);
        }
    }
}
