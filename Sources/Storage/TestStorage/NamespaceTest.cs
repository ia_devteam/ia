﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

using TestUtils;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for NamespaceTest and is intended
    ///To contain all NamespaceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class NamespaceTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for FindOrCreateSubNamespace
        ///</summary>
        [TestMethod()]
        public void FindOrCreateSubNamespaceTest()
        {
            Namespace ns1 = storage.namespaces.FindOrCreate("ns1");
            Namespace ns2 = ns1.FindOrCreateSubNamespace("ns2");
            Namespace ns3 = ns1.FindOrCreateSubNamespace("ns3");
            Namespace ns4 = ns1.FindOrCreateSubNamespace("ns2");

            Assert.IsTrue(ns4 == ns2);

            Namespace ns5 = ns1.FindOrCreateSubNamespace(new string[] { "ns2", "ns2" });
            Namespace ns6 = ns1.FindOrCreateSubNamespace(new string[] { "ns2", "ns2" });
            Assert.IsTrue(ns5 == ns6);

            Namespace ns7 = ns1.FindOrCreateSubNamespace("ns2");
            Namespace ns8 = ns7.FindOrCreateSubNamespace("ns2");
            Assert.IsTrue(ns6 == ns8);
        }

        /// <summary>
        ///A test for FindOrCreateSubNamespace_Id
        ///</summary>
        [TestMethod()]
        public void FindOrCreateSubNamespace_IdTest()
        {
            Namespace ns1 = storage.namespaces.FindOrCreate("ns1");
            UInt64 ns2 = ns1.FindOrCreateSubNamespace_Id("ns2");
            UInt64 ns3 = ns1.FindOrCreateSubNamespace_Id("ns3");
            UInt64 ns4 = ns1.FindOrCreateSubNamespace_Id("ns2");

            Assert.IsTrue(ns4 == ns2);

            UInt64 ns5 = ns1.FindOrCreateSubNamespace_Id(new string[] { "ns2", "ns2" });
            UInt64 ns6 = ns1.FindOrCreateSubNamespace_Id(new string[] { "ns2", "ns2" });
            Assert.IsTrue(ns5 == ns6);

            Namespace ns7 = ns1.FindOrCreateSubNamespace("ns2");
            UInt64 ns8 = ns7.FindOrCreateSubNamespace_Id("ns2");
            Assert.IsTrue(ns6 == ns8);

        }

        /// <summary>
        ///A test for FindSubNamespace
        ///</summary>
        [TestMethod()]
        public void FindSubNamespaceTest()
        {
            Namespace ns1 = storage.namespaces.FindOrCreate("ns1");
            Namespace ns2 = ns1.FindOrCreateSubNamespace("ns2");
            Namespace ns3 = ns1.FindOrCreateSubNamespace("ns3");
            Namespace ns4 = ns1.FindSubNamespace("ns2");

            Assert.IsTrue(ns4 == ns2);

            Namespace ns5 = ns1.FindOrCreateSubNamespace(new string[] { "ns2", "ns2" });
            Namespace ns6 = ns1.FindSubNamespace(new string[] { "ns2", "ns2" });
            Assert.IsTrue(ns5 == ns6);

            Namespace ns7 = ns1.FindSubNamespace("ns2");
            Namespace ns8 = ns7.FindSubNamespace("ns2");
            Assert.IsTrue(ns6 == ns8);

            Namespace ns9 = ns1.FindSubNamespace("ns10");
            Assert.IsTrue(ns9 == null);
        }

        /// <summary>
        /// A test for FindSubNamespace_Id
        /// </summary>
        [TestMethod()]
        public void FindSubNamespace_IdTest()
        {
            Namespace ns1 = storage.namespaces.FindOrCreate("ns1");
            UInt64 ns2 = ns1.FindOrCreateSubNamespace_Id("ns2");
            Namespace ns3 = ns1.FindOrCreateSubNamespace("ns3");
            UInt64 ns4 = ns1.FindSubNamespace_Id("ns2");

            Assert.IsTrue(ns4 == ns2);

            Namespace ns5 = ns1.FindOrCreateSubNamespace(new string[] { "ns2", "ns2" });
            UInt64 ns6 = ns1.FindSubNamespace_Id(new string[] { "ns2", "ns2" });
            Assert.IsTrue(ns5.Id == ns6);

            Namespace ns7 = ns1.FindSubNamespace("ns2");
            UInt64 ns8 = ns7.FindSubNamespace_Id("ns2");
            Assert.IsTrue(ns6 == ns8);

            UInt64 ns9 = ns1.FindSubNamespace_Id("ns10");
            Assert.IsTrue(ns9 == Store.Const.CONSTS.WrongID);
        }

        /// <summary>
        ///A test for Name
        ///</summary>
        [TestMethod()]
        public void NameTest()
        {
            Namespace ns1 = storage.namespaces.FindOrCreate("ns1");
            Assert.IsTrue(ns1.Name == "ns1");

            testUtils.Storage_Reopen(ref storage);

            ns1 = storage.namespaces.Find("ns1");
            Assert.IsTrue(ns1.Name == "ns1");
        }

        /// <summary>
        ///A test for EnumerateClassesInNamespace
        ///</summary>
        [TestMethod()]
        public void EnumerateClassesInNamespaceTest()
        {
            Class cl1 = storage.classes.AddClass();
            Class cl2 = storage.classes.AddClass();
            Class cl3 = storage.classes.AddClass();
            Namespace ns = storage.namespaces.FindOrCreate("ns1");
            Namespace ns1 = storage.namespaces.FindOrCreate("ns2");
            ns.AddClassToNamespace(cl1);
            ns1.AddClassToNamespace(cl2);
            ns.AddClassToNamespace(cl3);

            TestUtilsClass.CheckArray<Class>((arg, orig) => { return arg == orig; }, ns.EnumerateClassesInNamespace(), new Class[] { cl1, cl3 });
            TestUtilsClass.CheckArray<Class>((arg, orig) => { return arg == orig; }, ns1.EnumerateClassesInNamespace(), new Class[] { cl2 });
        }

        /// <summary>
        ///A test for EnumerateFunctionsInNamespace
        ///</summary>
        [TestMethod()]
        public void EnumerateFunctionsInNamespaceTest()
        {
            IFunction func1 = storage.functions.AddFunction();
            IFunction func2 = storage.functions.AddFunction();
            IFunction func3 = storage.functions.AddFunction();
            Namespace ns = storage.namespaces.FindOrCreate("ns1");
            Namespace ns1 = storage.namespaces.FindOrCreate("ns2");
            ns.AddFunctionToNamespace(func1);
            ns1.AddFunctionToNamespace(func2);
            ns.AddFunctionToNamespace(func3);

            TestUtilsClass.CheckArray<IFunction>((arg, orig) => { return arg == orig; }, ns.EnumerateFunctionsInNamespace(), new IFunction[] { func1, func3 });
            TestUtilsClass.CheckArray<IFunction>((arg, orig) => { return arg == orig; }, ns1.EnumerateFunctionsInNamespace(), new IFunction[] { func2 });
        }

        /// <summary>
        ///A test for EnumerateVariablesInNamespace
        ///</summary>
        [TestMethod()]
        public void EnumerateVariablesInNamespaceTest()
        {
            Variable var1 = storage.variables.AddVariable();
            Variable var2 = storage.variables.AddVariable();
            Variable var3 = storage.variables.AddVariable();
            Namespace ns = storage.namespaces.FindOrCreate("ns1");
            Namespace ns1 = storage.namespaces.FindOrCreate("ns2");
            ns.AddVariableToNamespace(var1);
            ns1.AddVariableToNamespace(var2);
            ns.AddVariableToNamespace(var3);

            TestUtilsClass.CheckArray<Variable>((arg, orig) => { return arg == orig; }, ns.EnumerateVariablesInNamespace(), new Variable[] { var1, var3 });
            TestUtilsClass.CheckArray<Variable>((arg, orig) => { return arg == orig; }, ns1.EnumerateVariablesInNamespace(), new Variable[] { var2 });
        }
    }
}
