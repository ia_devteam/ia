﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for IOperationTest and is intended
    ///To contain all IOperationTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IOperationTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for SequentiallyCallsFunctions
        ///</summary>
        [TestMethod()]
        public void SequentiallyCallsFunctionsTest()
        {
            IFunction func1 = storage.functions.AddFunction();
            IFunction func2 = storage.functions.AddFunction();
            IFunction func3 = storage.functions.AddFunction();
            IFunction func4 = storage.functions.AddFunction();

            IOperation op = storage.operations.AddOperation();
            op.AddFunctionCallInSequenceOrder(new IFunction[]{func1});
            op.AddFunctionCallInSequenceOrder(new IFunction[]{func2});
            op.AddFunctionCallInSequenceOrder(new IFunction[]{func3, func4});

            CompareResults(func1, func2, func3, func4, op);

            testUtils.Storage_Reopen(ref storage);

            CompareResults(storage.functions.GetFunction(1), storage.functions.GetFunction(2), storage.functions.GetFunction(3), storage.functions.GetFunction(4), storage.operations.GetOperation(1));
        }

        private static void CompareResults(IFunction func1, IFunction func2, IFunction func3, IFunction func4, IOperation op)
        {
            int i = 0;
            foreach (IFunction []func in op.SequentiallyCallsFunctions())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(func[0] == func1);
                        break;
                    case 1:
                        Assert.IsTrue(func[0] == func2);
                        break;
                    case 2:
                        Assert.IsTrue(func[0] == func3);
                        Assert.IsTrue(func[1] == func4);
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 3);
        }
    }
}
