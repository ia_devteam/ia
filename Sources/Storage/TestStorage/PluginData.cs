﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;
using Store.Table;

namespace TestStorage
{
    /// <summary>
    /// Summary description for PluginData
    /// </summary>
    [TestClass]
    public class PluginDataTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        
        [TestMethod]
        public void PluginSettings()
        {
            Store.PluginSettings settings = storage.pluginSettings;
            IBufferReader buf = settings.LoadSettings(1);
            Assert.IsNull(buf, "Mes 1");

            IBufferWriter wr = WriterPool.Get();
            wr.Add("aaa");
            settings.SaveSettings(1, wr);

            buf = settings.LoadSettings(1);
            Assert.IsNotNull(buf, "Mes 2");
            Assert.IsTrue(buf.GetString() == "aaa", "Mes 3");
        }

        [TestMethod]
        public void PluginData()
        {
            //проверяем, что файл с данными плагина создаётся и существует
            Store.PluginData pluginData = storage.pluginData;
            Assert.IsTrue(System.IO.File.Exists(pluginData.GetDataFileName(Store.Const.PluginIdentifiers.CS_SENSORS)));
        }
    }
}
