﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for LocationsTest and is intended
    ///To contain all LocationsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class LocationsTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for AddLocation
        ///</summary>
        [TestMethod()]
        public void AddLocationTest()
        {
            string test1Path = System.IO.Path.Combine(testUtils.TestRunDirectoryPath, "Test1");
            string fileName1 = System.IO.Path.Combine(test1Path,  @"1.txt");
            string fileName2 = System.IO.Path.Combine(test1Path,  @"2.txt");

            System.IO.Directory.CreateDirectory(test1Path);

            System.IO.StreamWriter writer = new System.IO.StreamWriter(fileName1);
            writer.WriteLine("dsfhgfd");
            writer.WriteLine("dsfhgfdsdf");
            writer.WriteLine("dsfhgfdsdfg");
            writer.Close();

            writer = new System.IO.StreamWriter(fileName2);
            writer.WriteLine("dsfhgfd333");
            writer.WriteLine("dsfhgfdsd444f");
            writer.WriteLine("dsfhgfdsdfg54656");
            writer.Close();

            storage.appliedSettings.SetFileListPath(test1Path);
            TestUtils.TestUtilsClass.Run_FillFileList(storage, test1Path);

            storage.locationsForMultiple("qqq").AddLocation(1, storage.files.FindOrGenerate(fileName1, enFileKindForAdd.fileWithPrefix), 10, null);
            var en = storage.locationsForMultiple("qqq").GetObjectLocations(1).GetEnumerator();
            Assert.IsTrue(en.MoveNext());
            
            Location loc = (Location) en.Current;
            UInt64 q = loc.GetLine();
            Assert.IsTrue(loc.GetLine() == 2);
            q = loc.GetColumn();
            Assert.IsTrue(loc.GetColumn() == 2);

            storage.locationsForMultiple("qqq").AddLocation(2, storage.files.FindOrGenerate(fileName2, enFileKindForAdd.fileWithPrefix), 2, 13, null);
            en = storage.locationsForMultiple("qqq").GetObjectLocations(2).GetEnumerator();
            Assert.IsTrue(en.MoveNext());

            loc = (Location)en.Current;
            q = loc.GetOffset();
            Assert.IsTrue(loc.GetOffset() == 24);
        }
    }
}
