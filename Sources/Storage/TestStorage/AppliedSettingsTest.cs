﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for AppliedSettingsTest and is intended
    ///To contain all AppliedSettingsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AppliedSettingsTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        /// <summary>
        ///A test for FileListPath
        ///</summary>
        [TestMethod()]
        public void FileListPathTest()
        {
            string str = @"c:\";
            storage.appliedSettings.SetFileListPath(str);
            Assert.IsTrue(storage.appliedSettings.FileListPath == str);
        }
    }
}
