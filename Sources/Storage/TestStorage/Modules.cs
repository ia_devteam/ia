﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;

namespace TestStorage
{
    /// <summary>
    /// Summary description for Modules
    /// </summary>
    [TestClass]
    public class Modules
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }

        [TestMethod]
        public void TestModules()
        {
            this.storage = testUtils.Storage_CreateAndOpen();

            string fileName = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "fil2222");


            Store.Modules modules = storage.modules;
            Files files = storage.files;
            IFile file1 = files.Add(enFileKindForAdd.fileWithPrefix);
            file1.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");
            IFile file2 = files.Add(enFileKindForAdd.fileWithPrefix);
            file2.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file1.txt");

            modules.AddModule(file1.Id);
            modules.AddModule(file2);

            int i = 0;
            foreach (Store.Module module in modules.EnumerateModules())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(module.FileId == file1.Id);
                        break;
                    case 1:
                        Assert.IsTrue(module.FileId == file2.Id);
                        break;
                    default:
                        Assert.Fail();
                        break;
                }
                i += 1;
            }

            modules.AddModule(fileName, enFileKindForAdd.fileWithPrefix);

            testUtils.Storage_Reopen(ref storage);

            modules = storage.modules;
            files = storage.files;

            i=0;
            foreach (Store.Module module in modules.EnumerateModules())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(module.FileId == file1.Id);
                        break;
                    case 1:
                        Assert.IsTrue(module.FileId == file2.Id);
                        break;
                    case 2:
                        Assert.IsTrue(module.FileId == 3);
                        Assert.IsTrue(files.GetFile(module.FileId).FullFileName_Original == fileName);

                        break;

                    default:
                        Assert.Fail();
                        break;
                }
                i += 1;
            }

            file1 = files.GetFile(1);
            Store.Module mod1 = modules.FindModule(2);
            Assert.IsTrue(mod1.FileId == 2);
            Assert.IsTrue(modules.FindModule(file1).FileId == 1);
            Assert.IsTrue(modules.FindModule(fileName).FileId == 3);

            Store.Module module1 = modules.GetModule(1);
            Store.Module module2 = modules.GetModule(2);

            Functions functions = storage.functions;
            IFunction func1 = functions.AddFunction();
            IFunction func2 = functions.AddFunction();
            module1.AddExportFunction(func1, 1);
            module1.AddExportFunction(func2, 2);

            i = 0;
            foreach (ExportFunction func in module1.ExportsFunctions())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(func.function.Id == 1);
                        break;
                    case 1:
                        Assert.IsTrue(func.function.Id == 2);
                        break;
                    default:
                        Assert.Fail();
                        break;
                }
                i++;
            }

            module1.AddImportModule(module2);

            i = 0;
            foreach (ImportModule mod in module1.ImportsModules())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(mod.module.Id == 2);
                        break;
                    default:
                        Assert.Fail();
                        break;
                }
                i++;

            }

            module1.AddImportFunction(func1);

            i = 0;
            foreach (ImportFunction func in module1.ImportsFunctions())
            {
                switch (i)
                {
                    case 0:
                        Assert.IsTrue(func.function.Id == 1);
                        break;
                    default:
                        Assert.Fail();
                        break;
                }
                i++;

            }

        }
    }
}
