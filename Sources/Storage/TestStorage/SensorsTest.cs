﻿using Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestStorage
{


    /// <summary>
    ///This is a test class for SensorsTest and is intended
    ///To contain all SensorsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SensorsTest
    {
        private TestUtils.TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtils.TestUtilsClass(testContextInstance.TestName);

            this.storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }


        /// <summary>
        ///A test for AddSensor
        ///</summary>
        [TestMethod()]
        public void AddSensorBeforStatementTest()
        {
            IFile file = storage.files.Add(enFileKindForAdd.fileWithPrefix);
            file.FullFileName_Original = System.IO.Path.Combine(storage.appliedSettings.FileListPath, "file.txt");

            IStatement st = storage.statements.AddStatementBreak(1, file);

            storage.sensors.AddSensorBeforeStatement(1, st, Kind.INTERNAL);
            Sensor sensor = storage.sensors.GetSensor(1);

            Assert.IsTrue(sensor.GetBeforeStatement() == st);
            Assert.IsTrue(sensor.GetFunctionID() == Store.Const.CONSTS.WrongID);

            IFunction func = storage.functions.AddFunction();
            IFunction func1 = storage.functions.AddFunction();
            func.EntryStatement = st;

            Assert.IsTrue(sensor.GetBeforeStatement() == st);
            Assert.IsTrue(sensor.GetFunctionID() == 1);

            IStatement st1 = storage.statements.AddStatementFor(1, file);
            func1.EntryStatement = st1;
            storage.sensors.AddSensorBeforeStatement(2, st1, Kind.START);

            sensor = storage.sensors.GetSensor(2);
            Assert.IsTrue(sensor.GetBeforeStatement() == st1);
            Assert.IsTrue(sensor.GetFunctionID() == 2);

            testUtils.Storage_Reopen(ref storage);

            st = storage.statements.GetStatement(1);
            st1 = storage.statements.GetStatement(2);
           
            sensor = storage.sensors.GetSensor(1);
            Assert.IsTrue(sensor.GetBeforeStatement() == st);
            Assert.IsTrue(sensor.GetFunctionID() == 1);
            
            sensor = storage.sensors.GetSensor(2);
            Assert.IsTrue(sensor.GetBeforeStatement() == st1);
            Assert.IsTrue(sensor.GetFunctionID() == 2);
        }

        /// <summary>
        ///A test for EnumerateSensors
        ///</summary>
        [TestMethod()]
        public void EnumerateSensorsTest()
        {
            Assert.IsTrue(storage.sensors.EnumerateSensors().GetEnumerator().MoveNext() == false);
     
            storage.sensors.AddSensor(1, 1, Kind.FINAL);
            storage.sensors.AddSensor(2, 1, Kind.INTERNAL);

            int i = 0;
            foreach (Sensor sensor in storage.sensors.EnumerateSensors())
            {
                switch (i)
                {
                    case 0: Assert.IsTrue(sensor.ID == 1);
                        break;

                    case 1: Assert.IsTrue(sensor.ID == 2);
                        break;

                    case 2: Assert.Fail();
                        break;
                }
                i++;
            }
            Assert.IsTrue(i == 2);

        }

        /// <summary>
        ///A test for AddSensor
        ///</summary>
        [TestMethod()]
        public void AddSensorTest()
        {
            //Вставляем первый датчик как номер по умолчанию
            storage.sensors.SetSensorStartNumberAsMaximum();
            IFunction func = storage.functions.AddFunction();
            storage.sensors.AddSensor(func.Id, Kind.FINAL);

            Assert.IsTrue(storage.sensors.GetSensor(1).ID == 1);

            //Вставляем второй датчик как автоматически вычисленный номер по умолчанию
            testUtils.Storage_Reopen(ref storage);

            storage.sensors.SetSensorStartNumberAsMaximum();
            storage.sensors.AddSensor(func.Id, Kind.FINAL);
            Assert.IsTrue(storage.sensors.GetSensor(2).ID == 2);

            //Проверяем SetSensorStartNumber
            storage.sensors.SetSensorStartNumber(100);
            storage.sensors.AddSensor(func.Id, Kind.FINAL);
            Assert.IsTrue(storage.sensors.GetSensor(100).ID == 100);

            //Переводим ход в середину интервала без датчиков
            storage.sensors.SetSensorStartNumber(10);
            storage.sensors.AddSensor(func.Id, Kind.FINAL);
            Assert.IsTrue(storage.sensors.GetSensor(10).ID == 10);

            storage.sensors.AddSensor(func.Id, Kind.FINAL);
            Assert.IsTrue(storage.sensors.GetSensor(11).ID == 11);

            //Проверяем SetSensorStartNumberAsMaximum
            storage.sensors.SetSensorStartNumberAsMaximum();
            storage.sensors.AddSensor(func.Id, Kind.FINAL);
            Assert.IsTrue(storage.sensors.GetSensor(101).ID == 101);

            storage.sensors.AddSensor(func.Id, Kind.FINAL);
            Assert.IsTrue(storage.sensors.GetSensor(102).ID == 102);
        }

        /// <summary>
        /// Тестируется возможность удалить все датчики из Хранилища
        /// </summary>
        [TestMethod]
        public void RemoveAllSensors()
        {
            storage.sensors.SetSensorStartNumberAsMaximum();

            IFunction func = storage.functions.AddFunction();
            IStatement st = storage.statements.AddStatement(ENStatementKind.STBreak);

            //Тестируем удаление, когда индексы не использовались
            storage.sensors.AddSensor(func.Id, Kind.START);

            storage.sensors.Clear();
            Assert.IsTrue(storage.sensors.Count() == 0);

            //Тестируем удаление, когда использовались индексы
            storage.sensors.AddSensor(func.Id, Kind.START);
            storage.sensors.AddSensorBeforeStatement(st, Kind.START);

            storage.sensors.Clear();
            Assert.IsTrue(storage.sensors.Count() == 0);

            //Проверяем, что состояние хранилища вернулось в рабочее
            storage.sensors.AddSensor(1, func.Id, Kind.START);
            storage.sensors.AddSensorBeforeStatement(st, Kind.START);

            ulong count = storage.sensors.Count();
            Assert.IsTrue(count == 2);
        }

        /// <summary>
        /// Тестируется возможность удаление 1 датчика по идентификатору из Хранилища
        /// </summary>
        [TestMethod]
        public void RemoveSensorTest()
        {
            storage.sensors.SetSensorStartNumberAsMaximum();

            IFunction func = storage.functions.AddFunction();
            IStatement st = storage.statements.AddStatement(ENStatementKind.STBreak);

            //Тестируем удаление, когда индексы не использовались
            storage.sensors.AddSensor(1, func.Id, Kind.START);

            storage.sensors.RemoveSensor(1);
            Assert.IsTrue(storage.sensors.Count() == 0);

            //Тестируем удаление, когда использовались индексы
            storage.sensors.AddSensor(1, func.Id, Kind.START);
            storage.sensors.AddSensorBeforeStatement(st, Kind.START); //номер 2

            storage.sensors.RemoveSensor(2);
            Assert.IsTrue(storage.sensors.Count() == 1);
            storage.sensors.RemoveSensor(1);
            Assert.IsTrue(storage.sensors.Count() == 0);

            //Проверяем, что состояние хранилища вернулось в рабочее
            storage.sensors.AddSensor(1, func.Id, Kind.START);
            storage.sensors.AddSensor(2, func.Id, Kind.START);
            storage.sensors.AddSensor(3, func.Id, Kind.START);

            storage.sensors.RemoveSensor(2);
            Assert.IsTrue(storage.sensors.Count() == 2);

            //проверяем что удалили датчик нужного номера - 2
            Assert.IsTrue(storage.sensors.GetSensor(1) != null);
            Assert.IsTrue(storage.sensors.GetSensor(3) != null);
            Assert.IsTrue(storage.sensors.GetSensor(2) == null);
        }

        /// <summary>
        /// Попытка вставить слишком большой номер датчика в Хранилище
        /// </summary>
        [TestMethod]
        public void TooBigSensorNumber()
        {
            try {
                IFunction func1 = storage.functions.AddFunction();
                storage.sensors.AddSensor((ulong)int.MaxValue + 1, func1.Id, Kind.START);
            }
            catch (Store.Const.StorageException ex)
            {
                Assert.IsTrue(ex.Message == "Попытка добавить новый датчик со слишком большим идентификатором 2147483648");
            }
        }

    }
}
