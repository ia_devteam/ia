﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Store;

namespace StorageImporter
{
    public class XMLStorageImporterForParser
    {
        Dictionary<UInt64, Store.IFile> filesTable;
        Dictionary<UInt64, Store.Class> classesTable;
        Dictionary<UInt64, Store.IOperation> operationsTable;
        Dictionary<UInt64, Store.IStatement> statementsTable;
        Dictionary<UInt64, Store.IFunction> functionsTable;
        Dictionary<UInt64, Store.Variable> variablesTable;
        List<UInt64> pastedSensors;

        Storage storage;

        public void ImportXML(XmlDocument xml, Storage storage)
        {
            this.storage = storage;

            //Проверяем XML на соответствие схеме
            string correctUncPath = System.IO.Path.GetDirectoryName(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            xml.Schemas.Add(System.Xml.Schema.XmlSchema.Read(System.IO.File.OpenRead(Path.Combine(correctUncPath, "..", "Common", "ParserToStorageData.xsd")), (s, e) => { }));
            xml.Validate((s, e) => { throw new Store.Const.StorageException("xml от парсера не соответствует схеме"); });

            //Инициализируем таблицы
            this.filesTable = new Dictionary<ulong, IFile>();
            this.classesTable = new Dictionary<ulong, Class>();
            this.operationsTable = new Dictionary<ulong, IOperation>();
            this.statementsTable = new Dictionary<ulong, IStatement>();
            this.functionsTable = new Dictionary<ulong, IFunction>();
            this.variablesTable = new Dictionary<ulong, Variable>();
            this.pastedSensors = new List<ulong>();

            //Последовательно загружаем все разделы
            var section = xml.GetElementsByTagName("Files");
            ImportFiles(section[0] as XmlElement);

            section = xml.GetElementsByTagName("Classes");
            ImportClasses(section[0] as XmlElement);

            section = xml.GetElementsByTagName("Functions");
            ImportFunctions(section[0] as XmlElement);

            section = xml.GetElementsByTagName("Variables");
            ImportVariables(section[0] as XmlElement);

            //storage.sensors.SetSensorStartNumber(1);
            section = xml.GetElementsByTagName("Statements");
            ImportStatements(section[0] as XmlElement);

            section = xml.GetElementsByTagName("Operations");
            ImportOperations(section[0] as XmlElement);

            TestSensors();

            //Освобождаем таблицы
            filesTable = null;
            classesTable = null;
            operationsTable = null;
            statementsTable = null;
            functionsTable = null;
            variablesTable = null;
            pastedSensors = null;
            storage = null;
        }

        /// <summary>
        /// Заполняет раздел Files
        /// </summary>
        /// <param name="files"></param>
        private void ImportFiles(XmlElement files)
        {
            foreach (XmlNode flNode in files.GetElementsByTagName("File"))
            {
                XmlElement flElem = flNode as XmlElement;

                //Идентификатор фыйла в XML. Не соответствует идентификатору фыйла в Хранилище!
                UInt64 fileId = System.Convert.ToUInt64(flElem.GetAttribute("ID"));

                //Ищем фыйл в хранилище, если такого файла нет - килаем исключение
                IFile fl;
                if (!filesTable.TryGetValue(fileId, out fl))
                {
                    fl = storage.files.Find(flElem.GetAttribute("fullname"));
                    if (fl == null) throw new Exception("Обнаружено несуществующее имя файла в узле File");
                    filesTable.Add(fileId, fl);
                }
            }
        }

        /// <summary>
        /// Заполняет раздел classes
        /// </summary>
        /// <param name="classes"></param>
        private void ImportClasses(XmlElement classes)
        {
            foreach (XmlNode clNode in classes.GetElementsByTagName("Class"))
            {
                XmlElement clElem = clNode as XmlElement;

                //Идентификатор класса в XML. Не соответствует идентификатору класса в Хранилище!
                UInt64 classId = System.Convert.ToUInt64(clElem.GetAttribute("ID"));

                //Создаём новый класс в Хранилище, если он ещё не создан
                Class cl;
                if (!classesTable.TryGetValue(classId, out cl))
                {
                    cl = storage.classes.AddClass();
                    classesTable[classId] = cl;
                }

                //Читаем имя класса
                cl.Name = clElem.GetAttribute("Name");

                //Добавляем класс в Namespace
                string nsName = clElem.GetAttribute("Namespace");
                if (!String.IsNullOrEmpty(nsName)) //Если namespace не задан, то значит класс находится в глобальном namespace
                {
                    Namespace ns = storage.namespaces.FindOrCreate(nsName.Split('.'));
                    ns.AddClassToNamespace(cl);
                }

                //Загружаем родителей класса
                foreach (string parrentClassString in clElem.GetElementsByTagName("ParrentClassId"))
                {
                    UInt64 parrentId = System.Convert.ToUInt64(parrentClassString);

                    //Создаём новый класс в Хранилище, если он ещё не создан
                    Class parrentClass;
                    if (!classesTable.TryGetValue(parrentId, out parrentClass))
                    {
                        parrentClass = storage.classes.AddClass();
                        classesTable[parrentId] = parrentClass;
                    }

                    cl.AddParent(parrentClass);
                }

                //Загружаем поля класса
                foreach (XmlNode fNode in clElem.GetElementsByTagName("Field"))
                {
                    XmlElement fieldElem = fNode as XmlElement;

                    UInt64 fieldId = System.Convert.ToUInt64(fieldElem.Value);

                    Field field;
                    Variable v;
                    if (!variablesTable.TryGetValue(fieldId, out v))
                    {
                        field = storage.variables.AddField();
                        variablesTable[fieldId] = field;
                    }
                    else
                    { //В таблице находятся перменные, не только поля. Поэтому надо конвертировать
                        if (!(v is Field))
                            throw new Store.Const.StorageException("Попытка в xml назначить классу в качестве поля переменную, не являющуюся полем. ID = " + fieldId.ToString());

                        field = v as Field;
                    }

                    //Боролись ради этой строки - выполняем связывание
                    field.OwnerClass = cl;
                }

                //Загружаем методы класса
                foreach (XmlNode mNode in clElem.GetElementsByTagName("Method"))
                {
                    XmlElement methodElem = mNode as XmlElement;

                    UInt64 methodId = System.Convert.ToUInt64(methodElem.Value);

                    IMethod method;
                    IFunction function;
                    if (!functionsTable.TryGetValue(methodId, out function))
                    {
                        method = storage.functions.AddMethod();
                        functionsTable[methodId] = method;
                    }
                    else
                    { //В таблице находятся ыункции, не только методы. Поэтому надо конвертировать
                        if (!(function is IMethod))
                            throw new Store.Const.StorageException("Попытка в xml назначить классу в качестве метода функцию, не являющуюся методом. ID = " + methodId.ToString());

                        method = function as IMethod;
                    }

                    //Боролись ради этой строки - выполняем связывание
                    method.methodOfClass = cl;
                }
            }
        }

        /// <summary>
        /// Получить переменную по ссылке на переменную
        /// </summary>
        /// <param name="xmlRef"></param>
        /// <returns></returns>
        private Variable GetVariable(XmlElement xmlRef)
        {
            UInt64 id = System.Convert.ToUInt64(xmlRef.GetAttribute("ID"));

            string kind = xmlRef.GetAttribute("kind");
            bool isField;
            if (kind == "variable")
                isField = false;
            else
                if (kind == "field")
                isField = true;
            else
                throw new Store.Const.StorageException("Неизвестный вид переменной в ссылке ID = " + id + " вид = " + kind);

            Variable res;
            if (!variablesTable.TryGetValue(id, out res))
            {
                if (!isField)
                    res = storage.variables.AddVariable();
                else
                    res = storage.variables.AddField();

                variablesTable.Add(id, res);
            }
            else
                if ((res is Field) != isField)
                throw new Store.Const.StorageException("Несоответствие видов переменной в xml. ID = " + id);

            return res;
        }

        /// <summary>
        /// Заполняет раздел variables
        /// </summary>
        /// <param name="classes"></param>
        private void ImportVariables(XmlElement variables)
        {
            foreach (XmlNode varNode in variables.GetElementsByTagName("Variable"))
                ImportVariable(varNode as XmlElement, false);

            foreach (XmlNode varNode in variables.GetElementsByTagName("Field"))
                ImportVariable(varNode as XmlElement, true);
        }

        /// <summary>
        /// Импортируем одну переменную или поле
        /// </summary>
        /// <param name="varElem"></param>
        /// <param name="isField">true, если переменная является полем класса. false в противном случае</param>
        private void ImportVariable(XmlElement varElem, bool isField)
        {
            //Идентификатор переменной в XML. Не соответствует идентификатору переменной в Хранилище!
            UInt64 varId = System.Convert.ToUInt64(varElem.GetAttribute("ID"));

            //Создаём новую переменную в Хранилище, если она ещё не создана
            Variable var;
            if (!variablesTable.TryGetValue(varId, out var))
            {
                if (!isField)
                    var = storage.variables.AddVariable();
                else
                    var = storage.variables.AddField();

                variablesTable[varId] = var;
            }
            else
                if (var is Field != isField) //Проверяем, что в таблице указано корректно
                throw new Store.Const.StorageException("Вид ранее загруженной из xml переменной не соответствует указанному в XML. ID = " + varId);

            //Читаем имя переменной
            var.Name = varElem.GetAttribute("Name");

            //Добавляем переменную в Namespace
            string nsName = varElem.GetAttribute("Namespace");
            if (!String.IsNullOrEmpty(nsName)) //Если namespace не задан, то значит класс находится в глобальном namespace
            {
                if (var is Field)
                    throw new Store.Const.StorageException("В XML для поля класса указан Namespace. ID = " + varId);

                Namespace ns = storage.namespaces.FindOrCreate(nsName.Split('.'));
                ns.AddVariableToNamespace(var);
            }

            //Загружаем признак того, является ли переменная глобальной с точки зрения РД НДВ
            var.isGlobal = System.Convert.ToBoolean(varElem.GetAttribute("isGlobal"));

            //Места определения переменной
            AddVariableDefinition(var, varElem);

            //Место присвоения переменной значения другой переменной
            foreach (XmlNode defNode in varElem.GetElementsByTagName("AssignmentFrom"))
                AddVariableAssignmentFrom(var, defNode as XmlElement);

            //Места присвоения указателя на функцию
            foreach (XmlNode defNode in varElem.GetElementsByTagName("PointsToFunctions"))
                AddVariablePointsToFunctions(var, defNode as XmlElement);

            //Места вызова по указателю
            foreach (XmlNode defNode in varElem.GetElementsByTagName("ValueCallPosition"))
                AddVariableValueCallPosition(var, defNode as XmlElement);

            //Места взятия значения переменной
            foreach (XmlNode defNode in varElem.GetElementsByTagName("ValueGetPosition"))
                AddVariableValueGetPosition(var, defNode as XmlElement);

            //Места присвоения значения переменной
            foreach (XmlNode defNode in varElem.GetElementsByTagName("ValueSetPosition"))
                AddVariableValueSetPosition(var, defNode as XmlElement);

            if (var is Field) //Специфичное для полей
            {
                Field field = var as Field;

                //Загружаем инициализатор
                field.Initializer = GetOperationReferenceIfExist(varElem.GetAttribute("Initializer"));

                //Загружаем признак того, является ли поле статическим
                field.isStatic = System.Convert.ToBoolean(varElem.GetAttribute("isStatic"));
            }
        }

        private void AddVariableValueSetPosition(Variable var, XmlElement xmlElement)
        {
            XmlNodeList vLineNode = xmlElement.GetElementsByTagName("LocationLine");
            XmlNodeList vOffNode = xmlElement.GetElementsByTagName("LocationOffset");

            if (vLineNode.Count + vOffNode.Count == 0)
                throw new Store.Const.StorageException("Внутри тега ValueSetPosition должен быть указн тег LocationLine или LocationOffset.");
            else if (vLineNode.Count + vOffNode.Count > 1)
            {
                throw new Store.Const.StorageException("Внутри тега ValueSetPosition должен быть указн ТОЛЬКО один тег LocationLine или LocationOffset.");
            }
            else
            {
                if (vOffNode.Count > 0)
                {
                    XmlElement lDefNode = (XmlElement)vOffNode[0];
                    var loc = GetDataFromOffsetLocation(lDefNode);
                    var.AddValueSetPosition(loc.file.FullFileName_Original, (ulong)loc.offset, loc.function);
                }
                else
                {
                    XmlElement lDefNode = (XmlElement)vLineNode[0];
                    var loc = GetDataFromLineColumnLocation(lDefNode);
                    var.AddValueSetPosition(loc.file.FullFileName_Original, (ulong)loc.line, (ulong)loc.column, loc.function);
                }
            }
        }

        private void AddVariableValueGetPosition(Variable var, XmlElement xmlElement)
        {
            XmlNodeList vLineNode = xmlElement.GetElementsByTagName("LocationLine");
            XmlNodeList vOffNode = xmlElement.GetElementsByTagName("LocationOffset");

            if (vLineNode.Count + vOffNode.Count == 0)
                throw new Store.Const.StorageException("Внутри тега ValueGetPosition должен быть указн тег LocationLine или LocationOffset.");
            else if (vLineNode.Count + vOffNode.Count > 1)
            {
                throw new Store.Const.StorageException("Внутри тега ValueGetPosition должен быть указн ТОЛЬКО один тег LocationLine или LocationOffset.");
            }
            else
            {
                if (vOffNode.Count > 0)
                {
                    XmlElement lDefNode = (XmlElement)vOffNode[0];
                    var loc = GetDataFromOffsetLocation(lDefNode);
                    var.AddValueGetPosition(loc.file.FullFileName_Original, (ulong)loc.offset, loc.function);
                }
                else
                {
                    XmlElement lDefNode = (XmlElement)vLineNode[0];
                    var loc = GetDataFromLineColumnLocation(lDefNode);
                    var.AddValueGetPosition(loc.file.FullFileName_Original, (ulong)loc.line, (ulong)loc.column, loc.function);
                }
            }
        }

        private class LocationParams
        {
            public IFile file { get; set; }
            public ulong? offset { get; set; }
            public ulong? line { get; set; }
            public ulong? column { get; set; }
            public IFunction function { get; set; }
        }

        private LocationParams GetDataFromOffsetLocation(XmlElement lDefNode)
        {
            if (!filesTable.ContainsKey(Convert.ToUInt64(lDefNode.GetAttribute("file")))) throw new Exception("Указана ссылка на файл, id которого не упомянут в секции Files");
            if (lDefNode.GetAttribute("function") != String.Empty && !functionsTable.ContainsKey(Convert.ToUInt64(lDefNode.GetAttribute("function")))) throw new Exception("Указана ссылка на функцию, id которой не упомянут в секции Functions");
            return new LocationParams()
            {
                file = filesTable[Convert.ToUInt64(lDefNode.GetAttribute("file"))],
                offset = Convert.ToUInt64(lDefNode.GetAttribute("offset")),
                function = (lDefNode.GetAttribute("function") != String.Empty) ? functionsTable[Convert.ToUInt64(lDefNode.GetAttribute("function"))] : null
            };
        }

        private LocationParams GetDataFromLineColumnLocation(XmlElement lDefNode)
        {
            if (!filesTable.ContainsKey(Convert.ToUInt64(lDefNode.GetAttribute("file")))) throw new Exception("Указана ссылка на файл, id которого не упомянут в секции Files");
            if (lDefNode.GetAttribute("function") != String.Empty && !functionsTable.ContainsKey(Convert.ToUInt64(lDefNode.GetAttribute("function")))) throw new Exception("Указана ссылка на функцию, id которой не упомянут в секции Functions");
            return new LocationParams()
            {
                file = filesTable[Convert.ToUInt64(lDefNode.GetAttribute("file"))],
                line = Convert.ToUInt64(lDefNode.GetAttribute("line")),
                column = Convert.ToUInt64(lDefNode.GetAttribute("column")),
                function = (lDefNode.GetAttribute("function") != String.Empty) ? functionsTable[Convert.ToUInt64(lDefNode.GetAttribute("function"))] : null
            };
        }

        private void AddVariableValueCallPosition(Variable var, XmlElement xmlElement)
        {
            XmlNodeList vAssNode = xmlElement.GetElementsByTagName("FunctionReference");
            if (vAssNode.Count > 1)
                throw new Store.Const.StorageException("Внутри тега ValueCallPosition может быть только один тег FunctionReference");
            else
            {
                IFunction theFunc = GetFunction((XmlElement)vAssNode[0]);

                XmlNodeList vLineNode = xmlElement.GetElementsByTagName("LocationLine");
                XmlNodeList vOffNode = xmlElement.GetElementsByTagName("LocationOffset");

                if (vLineNode.Count + vOffNode.Count == 0)
                    throw new Store.Const.StorageException("Внутри тега PointsToFunctions должен быть указн тег LocationLine или LocationOffset.");
                else if (vLineNode.Count + vOffNode.Count > 1)
                {
                    throw new Store.Const.StorageException("Внутри тега PointsToFunctions должен быть указан ТОЛЬКО один тег LocationLine или LocationOffset.");
                }
                else
                {
                    if (vOffNode.Count > 0)
                    {
                        XmlElement lDefNode = (XmlElement)vOffNode[0];
                        var loc = GetDataFromOffsetLocation(lDefNode);
                        var.AddValueCallPosition(loc.file.FullFileName_Original, (ulong)loc.offset, theFunc);
                    }
                    else
                    {
                        XmlElement lDefNode = (XmlElement)vLineNode[0];
                        var loc = GetDataFromLineColumnLocation(lDefNode);
                        var.AddValueCallPosition(loc.file.FullFileName_Original, (ulong)loc.line, (ulong)loc.column, theFunc);
                    }
                }
            }
        }

        private void AddVariablePointsToFunctions(Variable var, XmlElement xmlElement)
        {
            XmlNodeList vAssNode = xmlElement.GetElementsByTagName("FunctionReference");
            if (vAssNode.Count > 1)
                throw new Store.Const.StorageException("Внутри тега PointsToFunctions может быть только один тег FunctionReference");
            else
            {
                PointsToFunction ptf = var.AddPointsToFunctions();
                IFunction theFunc = GetFunction((XmlElement)vAssNode[0]);
                ptf.function = theFunc.Id;
                ptf.isGetAddress = true; // Парсеры должны как-то указывать что они делают в этом месте ?

                XmlNodeList vLineNode = xmlElement.GetElementsByTagName("LocationLine");
                XmlNodeList vOffNode = xmlElement.GetElementsByTagName("LocationOffset");

                if (vLineNode.Count + vOffNode.Count == 0)
                    throw new Store.Const.StorageException("Внутри тега PointsToFunctions должен быть указн тег LocationLine или LocationOffset.");
                else if (vLineNode.Count + vOffNode.Count > 1)
                {
                    throw new Store.Const.StorageException("Внутри тега PointsToFunctions должен быть указан ТОЛЬКО один тег LocationLine или LocationOffset.");
                }
                else
                {
                    if (vOffNode.Count > 0)
                    {
                        XmlElement lDefNode = (XmlElement)vOffNode[0];
                        var loc = GetDataFromOffsetLocation(lDefNode);
                        ptf.AddPosition(loc.file.FullFileName_Original, (ulong)loc.offset, loc.function);
                    }
                    else
                    {
                        XmlElement lDefNode = (XmlElement)vLineNode[0];
                        var loc = GetDataFromLineColumnLocation(lDefNode);
                        ptf.AddPosition(loc.file.FullFileName_Original, (ulong)loc.line, (ulong)loc.column, loc.function);
                    }
                    ptf.Save();
                }
            }
        }

        private void AddVariableAssignmentFrom(Variable var, XmlElement xmlElement)
        {
            XmlNodeList vAssNode = xmlElement.GetElementsByTagName("VariableReference");
            if (vAssNode.Count > 1)
                throw new Store.Const.StorageException("Внутри тега AssignmentFrom может быть только один тег VariableReference");
            else
            {
                IAssignment assFrom = var.AddAssignmentsFrom();
                assFrom.To = GetVariable((XmlElement)vAssNode[0]);

                XmlNodeList vLineNode = xmlElement.GetElementsByTagName("LocationLine");
                XmlNodeList vOffNode = xmlElement.GetElementsByTagName("LocationOffset");

                if (vLineNode.Count + vOffNode.Count == 0)
                    throw new Store.Const.StorageException("Внутри тега AssignmentFrom должен быть указн тег LocationLine или LocationOffset.");
                else if (vLineNode.Count + vOffNode.Count > 1)
                {
                    throw new Store.Const.StorageException("Внутри тега AssignmentFrom должен быть указн ТОЛЬКО один тег LocationLine или LocationOffset.");
                }
                else
                {
                    if (vOffNode.Count > 0)
                    {
                        XmlElement lDefNode = (XmlElement)vOffNode[0];
                        var loc = GetDataFromOffsetLocation(lDefNode);
                        assFrom.AddPosition(loc.file.FullFileName_Original, (ulong)loc.offset, loc.function);
                    }
                    else
                    {
                        XmlElement lDefNode = (XmlElement)vLineNode[0];
                        var loc = GetDataFromLineColumnLocation(lDefNode);
                        assFrom.AddPosition(loc.file.FullFileName_Original, (ulong)loc.line, (ulong)loc.column, loc.function);
                    }
                    assFrom.Save();
                }
            }
        }

        private void AddVariableDefinition(Variable var, XmlElement xmlElement)
        {
            XmlNodeList vDefNode = xmlElement.GetElementsByTagName("Definition");
            XmlElement eDNode = (XmlElement)vDefNode[0];
            foreach (XmlElement cDefNode in eDNode.GetElementsByTagName("LocationOffset"))
            {
                var loc = GetDataFromOffsetLocation(cDefNode);
                var.AddDefinition(loc.file.FullFileName_Original, (ulong)loc.offset, loc.function);
            }
            foreach (XmlElement cDecNode in eDNode.GetElementsByTagName("LocationLine"))
            {
                var loc = GetDataFromLineColumnLocation(cDecNode);
                var.AddDefinition(loc.file.FullFileName_Original, (ulong)loc.line, (ulong)loc.column, loc.function);
            }
        }

        /// <summary>
        /// Получить функцию по ссылке на функцию
        /// </summary>
        /// <param name="xmlRef"></param>
        /// <returns></returns>
        private IFunction GetFunction(XmlElement xmlRef)
        {
            UInt64 id = System.Convert.ToUInt64(xmlRef.GetAttribute("ID"));

            string kind = xmlRef.GetAttribute("kind");
            bool isMethod;
            if (kind == "function")
                isMethod = false;
            else
                if (kind == "method")
                isMethod = true;
            else
                throw new Store.Const.StorageException("Неизвестный вид функции в ссылке ID = " + id + " вид = " + kind);

            IFunction res;
            if (!functionsTable.TryGetValue(id, out res))
            {
                if (isMethod)
                    res = storage.functions.AddMethod();
                else
                    res = storage.functions.AddFunction();

                functionsTable.Add(id, res);
            }
            else
                if ((res is IMethod) != isMethod)
                throw new Store.Const.StorageException("Несоответствие видов функции в xml. ID = " + id);

            return res;
        }

        /// <summary>
        /// Заполняем раздел functions
        /// </summary>
        /// <param name="functions"></param>
        private void ImportFunctions(XmlElement functions)
        {
            foreach (XmlNode funcNode in functions.GetElementsByTagName("Function"))
                ImportFunction(funcNode as XmlElement, false);

            foreach (XmlNode funcNode in functions.GetElementsByTagName("Method"))
                ImportFunction(funcNode as XmlElement, true);
        }

        /// <summary>
        /// Загрузка функции
        /// </summary>
        /// <param name="xmlElement"></param>
        /// <param name="isMethod">true, если функция является методом класса. false  в противном случае</param>
        private void ImportFunction(XmlElement funcElem, bool isMethod)
        {
            //Идентификатор функции в XML. Не соответствует идентификатору класса в Хранилище!
            UInt64 funcId = System.Convert.ToUInt64(funcElem.GetAttribute("ID"));

            //Создаём новый класс в Хранилище, если он ещё не создан
            IFunction func;
            if (!functionsTable.TryGetValue(funcId, out func))
            {
                if (!isMethod)
                    func = storage.functions.AddFunction();
                else
                    func = storage.functions.AddMethod();

                functionsTable[funcId] = func;
            }
            else
                if (func is Field != isMethod) //Проверяем, что в таблице указано корректно
                throw new Store.Const.StorageException("Вид ранее загруженной из xml функции не соответствует указанному в XML. ID = " + funcId);

            //Читаем имя функции
            func.Name = funcElem.GetAttribute("Name");

            //Добавляем переменную в Namespace
            string nsName = funcElem.GetAttribute("Namespace");
            if (!String.IsNullOrEmpty(nsName)) //Если namespace не задан, то значит класс находится в глобальном namespace
            {
                if (func is IMethod)
                    throw new Store.Const.StorageException("В XML для метода класса указан Namespace. ID = " + funcId);

                Namespace ns = storage.namespaces.FindOrCreate(nsName.Split('.'));
                ns.AddFunctionToNamespace(func);
            }

            //Место определения функции
            AddFunctionDefinition(func, funcElem);

            //Места декларации функции
            AddFunctionDeclaration(func, funcElem);

            //Первый оператор функции
            if (funcElem.GetElementsByTagName("EntryStatement").Count > 0)
            {
                func.EntryStatement = GetStatement(funcElem.GetElementsByTagName("EntryStatement")[0] as XmlElement); //Наличие такого элемента гарантируется xsd-схемой
            }

            if (func is IMethod) //Заполняем поля, специфичные для метода
            {
                IMethod method = (IMethod)func;
                method.isOverrideBase = System.Convert.ToBoolean(funcElem.GetAttribute("isOverrideBase"));
            }
        }

        /// <summary>
        /// Получает ссылку на оператор по ссылке в XML. 
        /// </summary>
        /// <param name="xmlRef"></param>
        /// <returns></returns>
        private IStatement GetStatement(XmlElement xmlRef)
        {
            UInt64 id = System.Convert.ToUInt64(xmlRef.GetAttribute("ID"));

            //Разбираемся с типом оператора и таблицей
            string kind = xmlRef.GetAttribute("kind"); // говорят избыточный код и нужен только ID от EntryStatement
            IStatement res = GetStatement(id, (ENStatementKind)Enum.Parse(typeof(ENStatementKind), kind));

            return res;
        }

        private void AddFunctionDeclaration(IFunction func, XmlElement xmlElement)
        {
            XmlNodeList vDeсNode = xmlElement.GetElementsByTagName("Declaration");
            if (vDeсNode.Count > 0)
            {
                for (var i = 0; i < vDeсNode.Count; i++)
                {
                    XmlElement eDNode = (XmlElement)vDeсNode[i];
                    XmlNodeList vDecNodeOff = eDNode.GetElementsByTagName("LocationOffset");
                    foreach (XmlElement iTdNode in vDecNodeOff)
                    {
                        var loc = GetDataFromOffsetLocation(iTdNode);
                        func.AddDeclaration(loc.file, (ulong)loc.offset);
                    }
                    XmlNodeList vDecNodeLoc = eDNode.GetElementsByTagName("LocationLine");
                    foreach (XmlElement iTdNode in vDecNodeLoc)
                    {
                        var loc = GetDataFromLineColumnLocation(iTdNode);
                        func.AddDeclaration(loc.file, (ulong)loc.line, (ulong)loc.column);
                    }
                }
            }
        }

        private void AddFunctionDefinition(IFunction func, XmlElement xmlElement)
        {
            XmlNodeList vDefNode = xmlElement.GetElementsByTagName("Definition");
            if (vDefNode.Count > 1)
                throw new Store.Const.StorageException("У функции может быть только одно определение.");
            else if (vDefNode.Count == 1)
            {
                XmlElement eDNode = (XmlElement)vDefNode[0];
                var lDefNode = eDNode.GetElementsByTagName("LocationOffset");
                if (lDefNode.Count != 0)
                {
                    var el = (XmlElement)lDefNode[0];
                    var loc = GetDataFromOffsetLocation(el);
                    func.AddDefinition(loc.file, loc.offset.Value);
                }
                else
                {
                    lDefNode = eDNode.GetElementsByTagName("LocationLine");  //FIXME
                    if (lDefNode.Count != 0)
                    {
                        var el = (XmlElement)lDefNode[0];
                        var loc = GetDataFromLineColumnLocation(el);
                        func.AddDefinition(loc.file, (ulong)loc.line, (ulong)loc.column);
                    }
                    else
                        throw new Store.Const.StorageException("Тег Definition (функция) внутри должен содержать LocationOffset или LocationLine.");
                }
            }
        }

        /// <summary>
        /// Заполняет раздел statements
        /// </summary>
        /// <param name="classes"></param>
        private void ImportStatements(XmlElement statements)
        {
            foreach (XmlNode stNode in statements.GetElementsByTagName("STBreak"))
                ImportSTBreak(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STContinue"))
                ImportSTContinue(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STDoAndWhile"))
                ImportSTDoAndWhile(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STFor"))
                ImportSTFor(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STForEach"))
                ImportSTForEach(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STGoto"))
                ImportSTGoto(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STIf"))
                ImportSTIf(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STOperational"))
                ImportSTOperational(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STReturn"))
                ImportSTReturn(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STSwitch"))
                ImportSTSwitch(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STThrow"))
                ImportSTThrow(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STTryCatchFinally"))
                ImportSTTryCatchFinally(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STUsing"))
                ImportSTUsing(stNode as XmlElement);

            foreach (XmlNode stNode in statements.GetElementsByTagName("STYieldReturn"))
                ImportSTYieldReturn(stNode as XmlElement);
        }

        /// <summary>
        /// Импортировать оператор break
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTBreak(XmlElement stElement)
        {
            IStatementBreak st = (IStatementBreak)ImportStatement(stElement, ENStatementKind.STBreak);
            var destination = stElement.GetElementsByTagName("breakingLoop");
            if (destination.Count == 1)
                st.BreakingLoop = GetStatement(destination[0] as XmlElement);
        }

        /// <summary>
        /// Импортировать оператор continue
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTContinue(XmlElement stElement)
        {
            IStatementContinue st = (IStatementContinue)ImportStatement(stElement, ENStatementKind.STContinue);
            var destination = stElement.GetElementsByTagName("continuingLoop");
            if (destination.Count == 1)
                st.ContinuingLoop = GetStatement(destination[0] as XmlElement);
        }

        /// <summary>
        /// Импортировать оператор do-while и while-do
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTDoAndWhile(XmlElement stElement)
        {
            IStatementDoAndWhile st = (IStatementDoAndWhile)ImportStatement(stElement, ENStatementKind.STDoAndWhile);
            st.IsCheckConditionBeforeFirstRun = System.Convert.ToBoolean(Int32.Parse(stElement.GetAttribute("isCheckConditionBeforeFirstRun")));
            st.Condition = GetOperationReference(stElement.GetAttribute("condition"));
            st.Body = GetStatement(stElement.GetElementsByTagName("body")[0] as XmlElement);
        }


        /// <summary>
        /// Импортировать оператор for
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTFor(XmlElement stElement)
        {
            IStatementFor st = (IStatementFor)ImportStatement(stElement, ENStatementKind.STFor);
            st.Start = GetOperationReference(stElement.GetAttribute("start"));
            st.Condition = GetOperationReference(stElement.GetAttribute("condition"));
            st.Iteration = GetOperationReference(stElement.GetAttribute("iteration"));
            st.Body = GetStatement(stElement.GetElementsByTagName("body")[0] as XmlElement);
        }

        /// <summary>
        /// Импортировать оператор foreach
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTForEach(XmlElement stElement)
        {
            IStatementForEach st = (IStatementForEach)ImportStatement(stElement, ENStatementKind.STForEach);
            st.Head = GetOperationReference(stElement.GetAttribute("head"));
            st.Body = GetStatement(stElement.GetElementsByTagName("body")[0] as XmlElement);
        }

        /// <summary>
        /// Импортировать оператор goto
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTGoto(XmlElement stElement)
        {
            IStatementGoto st = (IStatementGoto)ImportStatement(stElement, ENStatementKind.STGoto);
            if (stElement.GetElementsByTagName("destination").Count > 0)
            {
                st.Destination = GetStatement(stElement.GetElementsByTagName("destination")[0] as XmlElement);
            }
            else if (!string.IsNullOrEmpty(stElement.GetAttribute("operation")))
            {
                st.Condition = GetOperationReference(stElement.GetAttribute("operation"));
            }
        }

        /// <summary>
        /// Импортировать оператор if-then-else
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTIf(XmlElement stElement)
        {
            IStatementIf st = (IStatementIf)ImportStatement(stElement, ENStatementKind.STIf);
            st.Condition = GetOperationReference(stElement.GetAttribute("condition"));
            st.ThenStatement = GetStatement(stElement.GetElementsByTagName("then")[0] as XmlElement);
            if (stElement.GetElementsByTagName("elseif").Count > 0)
            {
                // implemented as described in https://www.w3schools.com/php/php_if_else.asp
                IStatementIf lastElseIf = null;
                foreach (var elseIfCandidate in stElement.GetElementsByTagName("elseif"))
                {
                    var elseIfGuy = (XmlElement)elseIfCandidate;
                    var inner = GetStatement(elseIfGuy.GetElementsByTagName("then")[0] as XmlElement);
                    var elseIfSt = storage.statements.AddStatementIf(inner.FirstSymbolLocation.GetLine(), inner.FirstSymbolLocation.GetColumn(), inner.FirstSymbolLocation.GetFile(), inner.LastSymbolLocation.GetLine(), inner.LastSymbolLocation.GetColumn());
                    elseIfSt.Condition = GetOperationReference(elseIfGuy.GetAttribute("condition"));
                    elseIfSt.ThenStatement = inner;
                    if (lastElseIf == null)
                    {
                        st.ElseStatement = elseIfSt;
                    }
                    lastElseIf = elseIfSt;
                }
                if (stElement.GetElementsByTagName("else").Count == 1)
                {
                    lastElseIf.ElseStatement = GetStatement(stElement.GetElementsByTagName("else")[0] as XmlElement);
                }
            }
            else if (stElement.GetElementsByTagName("else").Count == 1)
            {
                st.ElseStatement = GetStatement(stElement.GetElementsByTagName("else")[0] as XmlElement);
            }
        }

        /// <summary>
        /// Импортировать арифметический оператор 
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTOperational(XmlElement stElement)
        {
            IStatementOperational st = (IStatementOperational)ImportStatement(stElement, ENStatementKind.STOperational);
            if (stElement.GetAttribute("operation") != String.Empty)
            {
                st.Operation = GetOperationReference(stElement.GetAttribute("operation"));
            }
        }

        /// <summary>
        /// Импортировать оператор return
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTReturn(XmlElement stElement)
        {
            IStatementReturn st = (IStatementReturn)ImportStatement(stElement, ENStatementKind.STReturn);
            st.ReturnOperation = GetOperationReference(stElement.GetAttribute("ReturnOperation"));
        }

        /// <summary>
        /// Импортировать оператор switch
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTSwitch(XmlElement stElement)
        {
            IStatementSwitch st = (IStatementSwitch)ImportStatement(stElement, ENStatementKind.STSwitch);
            st.Condition = GetOperationReference(stElement.GetAttribute("head"));

            foreach (XmlNode catchNode in stElement.ChildNodes)
            {
                XmlElement catchElem = (XmlElement)catchNode;
                if (catchNode.Name == "case")
                {
                    IStatement caseSt = null;
                    if (catchElem.GetElementsByTagName("body").Count == 1)
                    {
                        caseSt = GetStatement(catchElem.GetElementsByTagName("body")[0] as XmlElement);
                    }
                    else
                    {
                        caseSt = storage.statements.AddStatementOperational(); // пустой стейтмент, чтобы добавление кейса не умерло
                    }
                    st.AddCase(GetOperationReference(catchElem.GetAttribute("condition")), caseSt);
                }
                else {
                    if (catchNode.Name == "default")
                    {
                        st.AddDefaultCase(GetStatement(stElement.GetElementsByTagName("block")[0] as XmlElement));
                    }
                }
                //else
                //    throw new Store.Const.StorageException("Непредвиденная структура case-default"); - бред, тут другие не относящиеся к теме узлы
            }
        }

        /// <summary>
        /// Импортировать оператор throw
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTThrow(XmlElement stElement)
        {
            IStatementThrow st = (IStatementThrow)ImportStatement(stElement, ENStatementKind.STThrow);
            st.Exception = GetOperationReference(stElement.GetAttribute("exception"));
        }

        /// <summary>
        /// Импортировать оператор try-catch-finally
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTTryCatchFinally(XmlElement stElement)
        {
            IStatementTryCatchFinally st = (IStatementTryCatchFinally)ImportStatement(stElement, ENStatementKind.STTryCatchFinally);

            st.TryBlock = GetStatement(stElement.GetElementsByTagName("tryBlock")[0] as XmlElement);

            foreach (XmlNode catchNode in stElement.GetElementsByTagName("catchBlock"))
                st.AddCatch(GetStatement(catchNode as XmlElement));

            if (stElement.GetElementsByTagName("finallyBlock").Count != 0)
            {
                st.FinallyBlock = GetStatement(stElement.GetElementsByTagName("finallyBlock")[0] as XmlElement);
            }

            if (stElement.GetElementsByTagName("elseBlock").Count != 0)
            {
                st.ElseBlock = GetStatement(stElement.GetElementsByTagName("elseBlock")[0] as XmlElement);
            }
        }

        /// <summary>
        /// Импортировать оператор using
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTUsing(XmlElement stElement)
        {
            IStatementUsing st = (IStatementUsing)ImportStatement(stElement, ENStatementKind.STUsing);
            st.Head = GetOperationReference(stElement.GetAttribute("head"));
            st.Body = GetStatement(stElement.GetElementsByTagName("body")[0] as XmlElement);
        }

        /// <summary>
        /// Импортировать оператор YieldReturn
        /// </summary>
        /// <param name="stElement"></param>
        private void ImportSTYieldReturn(XmlElement stElement)
        {
            IStatementYieldReturn st = (IStatementYieldReturn)ImportStatement(stElement, ENStatementKind.STYieldReturn);
            st.YieldReturnOperation = GetOperationReference(stElement.GetAttribute("YieldReturnOperation"));
        }

        /// <summary>
        /// Формируем поля оператора, общие для всех операторов
        /// </summary>
        /// <param name="stElement"></param>
        /// <param name="kind">Тип оператора</param>
        private IStatement ImportStatement(XmlElement stElem, ENStatementKind kind)
        {
            //Идентификатор оператора в XML. Не соответствует идентификатору оператора в Хранилище!
            UInt64 statementId = System.Convert.ToUInt64(stElem.GetAttribute("ID"));

            //Создаём новый оператор в Хранилище, если он ещё не создан
            IStatement st = GetStatement(statementId, kind);

            if (stElem.GetElementsByTagName("NextInLinearBlock").Count != 0)
            {
                st.NextInLinearBlock = GetStatement(stElem.GetElementsByTagName("NextInLinearBlock")[0] as XmlElement);
            }

            if (stElem.GetElementsByTagName("SensorBeforeTheStatement").Count != 0)
            {
                ImportSensor(st, stElem.GetElementsByTagName("SensorBeforeTheStatement")[0] as XmlElement);
            }

            ImportFirstSymbolLocation(st, stElem.GetElementsByTagName("FirstSymbolLocation")[0] as XmlElement);
            ImportLastSymbolLocation(st, stElem.GetElementsByTagName("LastSymbolLocation")[0] as XmlElement);

            return st;
        }

        private void ImportLastSymbolLocation(IStatement st, XmlElement xmlElement)
        {
            var lDefNode = xmlElement.GetElementsByTagName("LocationOffset");  //FIXME
            if (lDefNode.Count != 0)
            {
                var locationNode = (XmlElement)lDefNode[0];
                var loc = GetDataFromOffsetLocation(locationNode);
                st.SetLastSymbolLocation(loc.file, (ulong)loc.offset);
            }
            else
            {
                lDefNode = xmlElement.GetElementsByTagName("LocationLine");  //FIXME
                if (lDefNode.Count != 0)
                {
                    var locationNode = (XmlElement)lDefNode[0];
                    var loc = GetDataFromLineColumnLocation(locationNode);
                    st.SetLastSymbolLocation(loc.file, (ulong)loc.line, (ulong)loc.column);
                }
                //else
                //    throw new Store.Const.StorageException("Тег LastSymbolLocation (функция) внутри должен содержать LocationOffset или LocationLine.");
            }
        }

        private void ImportFirstSymbolLocation(IStatement st, XmlElement xmlElement)
        {
            var lDefNode = xmlElement.GetElementsByTagName("LocationOffset");  //FIXME
            if (lDefNode.Count != 0)
            {
                var locationNode = (XmlElement)lDefNode[0];
                var loc = GetDataFromOffsetLocation(locationNode);
                st.SetFirstSymbolLocation(loc.file, (ulong)loc.offset);
            }
            else
            {
                lDefNode = xmlElement.GetElementsByTagName("LocationLine");  //FIXME
                if (lDefNode.Count != 0)
                {
                    var locationNode = (XmlElement)lDefNode[0];
                    var loc = GetDataFromLineColumnLocation(locationNode);
                    st.SetFirstSymbolLocation(loc.file, (ulong)loc.line, (ulong)loc.column);
                }
                else
                    throw new Store.Const.StorageException("Тег FirstSymbolLocation (функция) внутри должен содержать LocationOffset или LocationLine.");
            }
        }

        private void ImportSensor(IStatement st, XmlElement xmlElement)
        {
            UInt64 SensorId = Convert.ToUInt64(xmlElement.InnerText);
            Kind kind = Kind.INTERNAL;
            if (st is IStatementReturn || st is IStatementExit || st is IStatementYieldReturn)
            {
                kind = Kind.FINAL; //what to do if it's also the start one??/
            }
            else
            {
                if (st.AboveStatement == null)  //faulty logic if above is assigned later
                {
                    if (st.PreviousInLinearBlock == null)
                    {
                        kind = Kind.START;
                    }
                    else
                    {
                        if (st.NextInLinearBlock == null)
                        {
                            kind = Kind.FINAL;
                        }
                    }
                }
            }
            storage.sensors.AddSensorBeforeStatement(SensorId, st, kind);
            pastedSensors.Add(SensorId);
        }

        /// <summary>
        /// Получить оператор по ID и типу
        /// </summary>
        /// <param name="id">идентификатор оператора в XML. Не соответствует идентификатору оператора в Хранилище</param>
        /// <param name="kind">Тип оператора</param>
        /// <returns></returns>
        private IStatement GetStatement(ulong id, ENStatementKind kind)
        {
            IStatement res;
            if (!statementsTable.TryGetValue(id, out res))
            {
                res = storage.statements.AddStatement(kind);
                statementsTable.Add(id, res);
            }
            else
                if (res.Kind != kind)
                throw new Store.Const.StorageException("Несоответствие типов операторов в xml. ID = " + id);

            return res;
        }

        /// <summary>
        /// Загружаем ссылку на операцию. Если соответствующиего атрибута нет, то будет возвращён null
        /// </summary>
        /// <param name="idString">Текстовая строка, содержащая идентификатор операции</param>
        /// <returns></returns>
        private IOperation GetOperationReferenceIfExist(string idString)
        {
            if (!String.IsNullOrEmpty(idString))
                return GetOperationReference(idString);

            return null;
        }

        /// <summary>
        /// Загружаем ссылку на операцию. Предполагается, что ссылка на операцию точно присутствует
        /// </summary>
        /// <param name="idString">Текстовая строка, содержащая идентификатор операции</param>
        /// <returns></returns>
        private IOperation GetOperationReference(string idString)
        {
            UInt64 initializerId = System.Convert.ToUInt64(idString);
            IOperation op;
            if (!operationsTable.TryGetValue(initializerId, out op))
            {
                op = storage.operations.AddOperation();
                operationsTable[initializerId] = op;
            }
            return op;
        }


        /// <summary>
        /// Заполняет раздел operations
        /// </summary>
        /// <param name="classes"></param>
        private void ImportOperations(XmlElement operations)
        {
            foreach (XmlNode opNode in operations.GetElementsByTagName("Operation"))
            {
                XmlElement opElem = opNode as XmlElement;

                //Идентификатор оператора в XML. Не соответствует идентификатору оператора в Хранилище!
                UInt64 OperationId = System.Convert.ToUInt64(opElem.GetAttribute("ID"));

                //Создаём новый класс в Хранилище, если он ещё не создан
                IOperation op;
                if (!operationsTable.TryGetValue(OperationId, out op))
                {
                    op = storage.operations.AddOperation();
                    operationsTable[OperationId] = op;
                }

                //Загружаем последовательность вызова функций
                foreach (XmlNode callNode in opElem.GetElementsByTagName("callNode"))
                {
                    List<IFunction> funcs = new List<IFunction>();
                    List<Variable> vars = new List<Variable>();

                    foreach (XmlNode node in (callNode as XmlElement).GetElementsByTagName("callFunction"))
                        funcs.Add(GetFunction(node as XmlElement));

                    foreach (XmlNode node in (callNode as XmlElement).GetElementsByTagName("callVariable"))
                        vars.Add(GetVariable(node as XmlElement));

                    op.AddFunctionCallInSequenceOrder(funcs.ToArray(), vars.ToArray());
                }
            }
        }

        delegate void LocationByOffset(string filePath, UInt64 offset);
        delegate void LocationByLine(string filePath, UInt64 line, UInt64 column);
        private void ImportLocation(XmlElement xmlElement, LocationByOffset setLocByOffset, LocationByLine setLocBuLine)
        {
            XmlNodeList vDefNode = xmlElement.GetElementsByTagName("Definition");
            if (vDefNode.Count > 1)
                throw new Store.Const.StorageException("У функции может быть только одно определение.");
            else
            {
                XmlElement eDNode = (XmlElement)vDefNode[0];
                var lDefNode = eDNode.GetElementsByTagName("LocationOffset");
                if (lDefNode.Count != 0)
                {
                    var locationNode = (XmlElement)lDefNode[0];
                    var loc = GetDataFromOffsetLocation(locationNode);
                    setLocByOffset(loc.file.FullFileName_Original, (ulong)loc.offset);
                }
                else
                {
                    lDefNode = eDNode.GetElementsByTagName("LocationLine");
                    if (lDefNode.Count != 0)
                    {
                        var locationNode = (XmlElement)lDefNode[0];
                        var loc = GetDataFromLineColumnLocation(locationNode);
                        setLocBuLine(loc.file.FullFileName_Original, (ulong)loc.line, (ulong)loc.column);
                    }
                    else
                        throw new Store.Const.StorageException("Тег Definition (функция) внутри должен содержать LocationOffset или LocationLine.");
                }
            }
        }

        private void TestSensors()
        {
            foreach (var pastedSensor in pastedSensors)
            {
                var sensor = storage.sensors.GetSensor(pastedSensor);
                if (sensor.GetFunction() == null) IA.Monitor.Log.Error("XMLStorageImporter", String.Format("Датчику с номером {0} не присвоена функция", pastedSensor));
            }
        }
    }
}
