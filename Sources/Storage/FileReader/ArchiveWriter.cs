﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using IOController;

namespace FileOperations
{
    /// <summary>
    /// Генератор архива. 
    /// Один экземпляр можно использовать для создания только одного архива.
    /// </summary>
    public sealed class ArchiveWriter : IDisposable
    {
        #region Константы
        /// <summary>
        /// Расширение для архива по умолчанию
        /// </summary>
        public const string ArchiveDefaultExtension = ".7z";

        /// <summary>
        /// Максимальное количество файлов, ожидающих добавления в архив
        /// </summary>
        const long MaxPendingFilesСount = 1000;

        /// <summary>
        /// Максимальный суммарный размер файлов, ожидающих добавления в архив
        /// </summary>
        const long MaxPendingFilesSize = 100 * 1024 * 1024; // 100 Мегабайт 
        #endregion

        /// <summary>
        /// Путь к файлу архива
        /// </summary>
        public string ArchiveFilePath { get; private set; }

        /// <summary>
        /// Каталог для хранения временной копии создаваемого архива. Копия архива удаляется после успешного завершения архивации.
        /// </summary>
        public string Temp7zDirectoryPath { get; private set; }

        /// <summary>
        /// Размер архива в байтах (если архив состоит из нескольких файлов, то суммарный размер)
        /// </summary>
        public long ArchiveSize
        {
            get
            {
                try
                {
                    //Добавляем ожидающие файлы в архив
                    AddPendingFiles();

                    //Получаем текущий размер файла архива
                    return this.InnerFilesCount == 0 ? 0 : new FileInfo(this.ArchiveFilePath).Length;
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new ArchiveWriterException("Ошибка при получении размера файла архива.", ex);
                }
            }
        }

        /// <summary>
        /// Количество файлов в архиве
        /// </summary>
        public int InnerFilesCount { get; private set; }

        /// <summary>
        /// Оригинальный суммарный размер файлов, помещённых в архив (может использоваться для оценки степени сжатия)
        /// </summary>
        public long InnerFilesOriginalSize { get; private set; }

        /// <summary>
        /// Требуется ли удалять файлы после архивации?
        /// </summary>
        private bool isNeedToRemoveAfter;

        /// <summary>
        /// Список файлов, ожидающих добавления в архив
        /// </summary>
        private List<string> pendingFiles = new List<string>();

        /// <summary>
        /// Суммарный размер файлов, ожидающих добавления в архив
        /// </summary>
        private Int64 pendingFilesSize = 0;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="archiveFilePath">Путь к файлу архива</param>
        /// <param name="isNeedToRemoveAfter">Требуется ли удалять файлы после архивации?</param>
        /// <param name="temp7zDirectoryPath">Каталог для хранения временной копии создаваемого архива.</param>
        public ArchiveWriter(string archiveFilePath, bool isNeedToRemoveAfter = false, string temp7zDirectoryPath = null)
        {
            //Проверка на разумность
            if (String.IsNullOrWhiteSpace(archiveFilePath))
                throw new Exception("Путь к файлу архива не задан.");

            this.ArchiveFilePath = archiveFilePath;
            this.isNeedToRemoveAfter = isNeedToRemoveAfter;
            this.InnerFilesCount = 0;
            this.InnerFilesOriginalSize = 0;
            this.Temp7zDirectoryPath = temp7zDirectoryPath;
        }

        /// <summary>
        /// Деструктор
        /// </summary>
        ~ArchiveWriter()
        {
            //Проверка на то, что в архив были добавлены все файлы
            try
            {
                //Если архив не был завершён, то удаляем целиком, чтобы ошибка в работе была заметна.
                if (this.pendingFiles.Count != 0 && FileController.IsExists(this.ArchiveFilePath))
                    FileController.Remove(this.ArchiveFilePath);
            }
            catch (Exception ex)
            {
                throw new System.MemberAccessException("Проверка условия при вызове деструктора ArchiveWriter не выполнена. Проверяемое поле недоступно.", ex);
            }
        }

        /// <summary>
        /// Добавить файл в архив
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        public void AddFile(string filePath)
        {
            //Добавляем файл в число ожидающих добавления
            pendingFiles.Add(filePath);

            //Узнаём суммарный размер ожидающих файлов
            this.pendingFilesSize += (new FileInfo(filePath)).Length;

            //Если количество ожидающий файлов или их общий размер больше максимума, то добавляем файлы в архив
            if (this.pendingFiles.Count >= MaxPendingFilesСount || this.pendingFilesSize >= MaxPendingFilesSize)
                AddPendingFiles();
        }

        /// <summary>
        /// Добавление ожидающих файлов в архив
        /// </summary>        
        private void AddPendingFiles()
        {
            //Если добавлять нечего, ничего не делаем
            if (pendingFiles.Count == 0)
                return;
            
            //Формируем путь до временного файла со списком архивируемых файлов
            string tempFilePath = this.ArchiveFilePath + ".tmp";

            //Создаём временный файл со списком архивируемых файлов
            File.WriteAllLines(tempFilePath, pendingFiles.ToArray());

            //Запускаем архивацию
            string args = "@\"" + tempFilePath + "\" -mx3 -mmt" + ((!string.IsNullOrWhiteSpace(Temp7zDirectoryPath)) ? " -w" + Temp7zDirectoryPath : "");
            string output = ExtProg.Execute("common\\7z.exe", "a " + "\"" + ArchiveFilePath + "\"" + " " + args);

            //Перекодируем строку из Windows-1251 в cp866
            output = ArchiveReader.ReencodeLine(output, 1251, 866);

            //Проверяем результат
            if (!output.Contains("Everything is Ok"))
                throw new ArchiveWriterException("Ошибка добавления " + args + " в архив " + ArchiveFilePath + Environment.NewLine + output);

            //Если требуется удалить после архивации, то делаем это
            if (this.isNeedToRemoveAfter)
                foreach (string file in this.pendingFiles)
                    if (FileController.IsExists(file))
                        FileController.Remove(file);

            //Обновляем информацию об архиве
            this.InnerFilesCount += this.pendingFiles.Count;
            this.InnerFilesOriginalSize += pendingFilesSize;

            //Очищаем список ожидающий добавления файлов
            this.pendingFiles.Clear();
            this.pendingFilesSize = 0;

            //Удаляем временный файл
            FileController.Remove(tempFilePath, true);
        }

        /// <summary>
        /// Освобождение неуправляемых ресурсов
        /// </summary>
        public void Dispose()
        {
            //Добавляем все ожидающие файлы в архив
            this.AddPendingFiles();
        }

        /// <summary>
        /// Принудительный сброс буфера данных на диск.
        /// </summary>
        public void Flush()
        {
            this.AddPendingFiles();
        }

        #region Статические методы
        /// <summary>
        /// Заархивировать указанную директорию.
        /// </summary>
        /// <param name="sourceDirectoryPath">Путь до директории для архивации.</param>
        /// <param name="archiveFilePath">Путь до файла архива.</param>
        /// <param name="isNeedToRemoveAfter">Требуется ли удалить директорию после архивации?</param>
        /// <param name="temp7zDirectoryPath">Каталог для хранения временной копии создаваемого архива.</param>
        public static void ArchiveDirectory(string sourceDirectoryPath, string archiveFilePath, bool isNeedToRemoveAfter = false, string temp7zDirectoryPath = null)
        {
            try
            {
                //Проверка на разумность
                if (sourceDirectoryPath == null)
                    throw new Exception("Путь до директории для архивации не определён.");

                //Проверка на разумность
                if (archiveFilePath == null)
                    throw new Exception("Путь до файла архива не определён.");

                //Проверка на существование директории для архивации
                if (!DirectoryController.IsExists(sourceDirectoryPath))
                    throw new Exception("Путь до директории для архивации не существует.");

                //Получаем директорию для архивации содержимого с разделителем на конце
                string sourceDirectoryPathEndsWithSeparator = PathController.TrimEndSeparator(sourceDirectoryPath) + Path.DirectorySeparatorChar;

                //Проверка на то, что архив не лежит в архивируемой директории
                if (archiveFilePath.StartsWith(sourceDirectoryPathEndsWithSeparator))
                    throw new Exception("Архивация не может быть произведена в архивируемую директорию.");
                
                //Запускаем 7z
                string args = String.Format(" a \"{0}\" \"{1}\" -mx3 -mmt" + ((!string.IsNullOrWhiteSpace(temp7zDirectoryPath)) ? " -w" + temp7zDirectoryPath : ""), archiveFilePath, sourceDirectoryPath);
                string output = ExtProg.Execute("common\\7z.exe", args);

                //Проверяем результат
                if (!output.Contains("Everything is Ok"))
                    throw new ArchiveWriterException("Ошибка добавления " + args + " в архив " + archiveFilePath + Environment.NewLine + output);

                //Удаляем директорию после архивации, если требуется
                if (isNeedToRemoveAfter)
                    DirectoryController.Remove(sourceDirectoryPath, true);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при архивировании директории.", ex);
            }
        }

        /// <summary>
        /// Заархивировать содержимое указанной директории.
        /// </summary>
        /// <param name="sourceDirectoryPath">Путь до директории для архивации.</param>
        /// <param name="archiveFilePath">Путь до файла архива.</param>
        /// <param name="isNeedToRemoveAfter">Требуется ли удалить содержимое директории после архивации?</param>
        /// <param name="temp7zDirectoryPath">Каталог для хранения временной копии создаваемого архива.</param>
        public static void ArchiveDirectoryContents(string sourceDirectoryPath, string archiveFilePath, bool isNeedToRemoveAfter = false, string temp7zDirectoryPath = null)
        {
            try
            {
                //Проверка на разумность
                if (sourceDirectoryPath == null)
                    throw new Exception("Путь до директории для архивации содержимого не определён.");

                //Проверка на разумность
                if (archiveFilePath == null)
                    throw new Exception("Путь до файла архива не определён.");

                //Проверка на существование директории для архивации содержимого
                if (!DirectoryController.IsExists(sourceDirectoryPath))
                    throw new Exception("Путь до директории для архивации содержимого не существует.");

                //Получаем директорию для архивации содержимого с разделителем на конце
                string sourceDirectoryPathEndsWithSeparator = PathController.TrimEndSeparator(sourceDirectoryPath) + Path.DirectorySeparatorChar;

                //Проверка на то, что архив не лежит в архивируемой директории
                if (archiveFilePath.StartsWith(sourceDirectoryPathEndsWithSeparator))
                    throw new Exception("Архивация не может быть произведена в директорию с архивируемым содержимым.");

                //Запускаем 7z
                string args = String.Format(" a \"{0}\" \"{1}\" -mx3 -mmt" + ((!string.IsNullOrWhiteSpace(temp7zDirectoryPath)) ? " -w" + temp7zDirectoryPath : ""), archiveFilePath, sourceDirectoryPathEndsWithSeparator + "*");
                string output = ExtProg.Execute("common\\7z.exe", args);

                //Проверяем результат
                if (!output.Contains("Everything is Ok"))
                    throw new ArchiveWriterException("Ошибка добавления " + args + " в архив " + archiveFilePath + Environment.NewLine + output);

                //Очищаем директорию после архивации, если требуется
                if (isNeedToRemoveAfter)
                    DirectoryController.Clear(sourceDirectoryPath, true);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при архивировании содержимого директории.", ex);
            }
        }
        #endregion

        /// <summary>
        /// Класс ошибки модуля ArchiveWriter. Простая обёртка.
        /// </summary>
        public class ArchiveWriterException : Exception
        {
            /// <summary>
            /// Конструктор по-умолчанию.
            /// </summary>
            public ArchiveWriterException()
                : base()
            { }
            /// <summary>
            /// Конструктор с передачей сообщения.
            /// </summary>
            /// <param name="message">Сообщение для отображения.</param>
            public ArchiveWriterException(string message)
                : base(message)
            { }

            /// <summary>
            /// Конструктор с передачей сообщения и внутренней ошибкой.
            /// </summary>
            /// <param name="message">Сообщение для отображения.</param>
            /// <param name="inner">Внутрення ошибка с дополнительной информацией.</param>
            public ArchiveWriterException(string message, Exception inner)
                : base(message, inner)
            { }
        }


    }

    /// <summary>
    /// Класс для запуска сторонних приложений с указанными параметрами
    /// </summary>
    public class ExtProg
    {
        /// <summary>
        /// Выполнить сторонее приложение с указанными параметрами.
        /// Запуск приложения осуществляется из текущей директории.
        /// </summary>
        /// <param name="programFileName">Путь до файла приложения</param>
        /// <param name="parameters">Параметры</param>
        /// <returns>Содержимое стандартного потока вывода</returns>
        public static string Execute(string programFileName, string parameters)
        {
            try
            {
                //Проверка на разумность
                if (String.IsNullOrWhiteSpace(programFileName))
                    throw new Exception("Имя приложения для запуска не определено.");

                //Получаем полный путь до приложения
                string programFilePath = AppDomain.CurrentDomain.BaseDirectory + @"\" + programFileName;

                //Проверка на существование пути до приложения
                if (!System.IO.File.Exists(programFilePath))
                    throw new Exception("Путь до приложения <" + programFilePath + "> не существует.");

                //Запускаем приложение
                using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                {
                    process.EnableRaisingEvents = false;
                    process.StartInfo.FileName = programFilePath;
                    process.StartInfo.Arguments = parameters;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.StandardOutputEncoding = Encoding.GetEncoding(866);
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.CreateNoWindow = true;
                    process.Start();

                    using (StreamReader reader = process.StandardOutput)
                        return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при запуске стороннего приложения <" + (programFileName ?? "имя неизвестно") + ">.", ex);
            }
        }

    }

}
