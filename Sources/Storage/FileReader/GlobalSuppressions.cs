﻿// Этот файл используется анализом кода для поддержки атрибутов 
// SuppressMessage, примененных в этом проекте. 
// Подавления на уровне проекта либо не имеют целевого объекта, либо для них задан 
// конкретный объект и область пространства имен, тип, член и т. д.
//
// Чтобы добавить подавление к этому файлу, щелкните правой кнопкой 
// мыши сообщение в списке ошибок, укажите на команду "Подавить сообщения" и выберите вариант 
// "В файле проекта для блокируемых предупреждений".
// Нет необходимости вручную добавлять подавления к этому файлу.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2122:DoNotIndirectlyExposeMethodsWithLinkDemands", Scope = "member", Target = "FileOperations.ExtProg.#ExecuteExtProg(System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Scope = "member", Target = "FileOperations.ArchiveWriter.#ArchiveSize")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Ликвидировать объекты перед потерей области", Scope = "member", Target = "FileOperations.ExtProg.#ExecuteExtProg(System.String,System.String)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "member", Target = "FileOperations.CachedReader.#Dispose()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Scope = "member", Target = "FileOperations.ArchiveWriter.#Finalize()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Scope = "member", Target = "FileOperations.ArchiveReader+MyStream.#Dispose()")]
