﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using Mozilla.NUniversalCharDet;
using System.Runtime.InteropServices;
using System.Runtime.Caching;

namespace FileOperations
{
    /// <summary>
    /// режим открытия файла
    /// </summary>
    public enum OpenMode { 
        /// <summary>
        /// обычный
        /// </summary>
        USUAL, 
        /// <summary>
        /// игнорировать символ \0
        /// </summary>
        SLASH0IGNORE 
    };

    /// <summary>
    /// Класс для чтения содержимого файла
    /// </summary>
    public class AbstractReader
    {
        //Структура, содержащая кэшируемые данные
        private class CachedInfo
        {
            internal Encoding encoding;
            internal StringBuilder contents;
            internal List<int> stringStarts;
            internal List<int> stringEnds;

            internal CachedInfo(StringBuilder contents, Encoding encoding, List<int> stringStarts, List<int> stringEnds)
            {
                this.contents = contents;
                this.encoding = encoding;
                this.stringStarts = stringStarts;
                this.stringEnds = stringEnds;
            }
        }

        private string filePath;

        private string contentsString;

        private int currentLine = 0;

        private Encoding encoding;
        private StringBuilder contents;
        private  List<int> stringStarts;
        private  List<int> stringEnds;

        private OpenMode Mode; 

        /**
         * Determine string encoding
         */
        private Encoding GetEncoding()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    int len = 0;
                    byte[] buff = new byte[512];

                    while ((len = fileStream.Read(buff, 0, 512)) > 0)
                        memoryStream.Write(buff, 0, len);
                }

                if (memoryStream.Length > 0)
                {
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    byte[] PageBytes = new byte[memoryStream.Length];
                    memoryStream.Read(PageBytes, 0, PageBytes.Length);

                    memoryStream.Seek(0, SeekOrigin.Begin);
                    int DetLen = 0;
                    byte[] DetectBuff = new byte[4096];  
                    //CharsetListener listener = new CharsetListener();
                    UniversalDetector Det = new UniversalDetector(null);

                    while ((DetLen = memoryStream.Read(DetectBuff, 0, DetectBuff.Length)) > 0 && !Det.IsDone())
                        Det.HandleData(DetectBuff, 0, DetectBuff.Length);
                    
                    Det.DataEnd();
                    
                    return Det.GetDetectedCharset() != null ? Encoding.GetEncoding(Det.GetDetectedCharset()) : Encoding.Default;
                }

                return Encoding.Default;
            }
        }

        /**
         * Determine line offset
         */
        private void ParseLines()
        {
            restoreText();

            stringStarts.Clear();
            stringEnds.Clear();

            stringStarts.Add(0); //начало
            for (int i = 0; i < contentsString.Length; i++)
            {
                if (contentsString[i] == '\n')
                {
                    stringEnds.Add(i);
                    if (i != contentsString.Length - 1)
                    {
                        stringStarts.Add(i + 1);
                    }
                }
                if (contentsString[i] == '\r')
                {
                    stringEnds.Add(i);
                    if (i != contentsString.Length - 1)
                    {
                        if (contentsString[i + 1] == '\n')
                        {
                            stringEnds.RemoveAt(stringEnds.Count - 1);
                            stringEnds.Add(i + 1);
                            stringStarts.Add(i + 2);
                            i++;
                        }
                        else
                        {
                            stringStarts.Add(i + 1);
                        }
                    }
                }
            }
            if (stringStarts.Count > stringEnds.Count) stringEnds.Add(contentsString.Length);
        }

        /**
         * Read contents from file
         */
        private StringBuilder ReadContents()
        {
            StringBuilder res = new StringBuilder();

            int len = 0;
            byte[] buff = new byte[512];

            using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                while ((len = stream.Read(buff, 0, 512)) > 0)
                {
                    byte[] writeBuff;
                    if (!(encoding == Encoding.UTF8 && len == 512 && buff[511] == 208))
                    {
                        writeBuff = new byte[len];
                        Buffer.BlockCopy(buff, 0, writeBuff, 0, len);
                        res.Append(encoding.GetString(writeBuff));
                    }
                    else
                    {
                        writeBuff = new byte[len+1];
                        Buffer.BlockCopy(buff, 0, writeBuff, 0, len);
                        stream.Read(writeBuff, 512, 1);
                        res.Append(encoding.GetString(writeBuff));
                    }
                }
            }

            if (Mode == OpenMode.SLASH0IGNORE)
            {
                res = res.Replace("\0", " ");
            }

            return res;
        }
        
        #region mime stuff
        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        private extern static System.UInt32 FindMimeFromData(
            System.UInt32 pBC,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
            System.UInt32 cbSize,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
            System.UInt32 dwMimeFlags,
            out System.UInt32 ppwzMimeOut,
            System.UInt32 dwReserverd
        );

        private string GetMimeFromRegistry(string Filename)
        {
            string mime = "application/octetstream";
            string ext = System.IO.Path.GetExtension(Filename).ToLower();
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
        }

        private string GetMimeTypeFromFileAndRegistry(string filename)
        {
            if (!File.Exists(filename))
            {
                return GetMimeFromRegistry(filename);
            }

            byte[] buffer = new byte[256];

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                if (fs.Length >= 256)
                    fs.Read(buffer, 0, 256);
                else
                    fs.Read(buffer, 0, (int)fs.Length);
            }

            try
            {
                System.UInt32 mimetype;

                FindMimeFromData(0, null, buffer, 256, null, 0, out mimetype, 0);

                System.IntPtr mimeTypePtr = new IntPtr(mimetype);

                string mime = Marshal.PtrToStringUni(mimeTypePtr);

                Marshal.FreeCoTaskMem(mimeTypePtr);

                if (string.IsNullOrWhiteSpace(mime))
                {
                    return GetMimeFromRegistry(filename);
                }

                return mime;
            }
            catch (Exception)
            {
                return GetMimeFromRegistry(filename);
            }
        }
        #endregion

        /**
         * Getter for path
         */
        public string getPath()
        {
            return filePath;
        }

        public Encoding getEncoding()
        {
            return this.encoding;
        }

        /// <summary>
        /// конструктор класса
        /// </summary>
        /// <param name="filePath">путь</param>
        /// <param name="mode">режим чтения</param>
        public AbstractReader(string filePath, OpenMode mode =  OpenMode.USUAL)
        {
            this.filePath = filePath;

            //Объект для кеширования
            MemoryCache cache = MemoryCache.Default;
            CachedInfo info = (CachedInfo) cache.Get(filePath);

            //string mime = GetMimeTypeFromFileAndRegistry(this.fileName); //временно под комментом, пока не ловится эксепшн

            if (info != null)
            {
                encoding = info.encoding;
                contents = info.contents;
                stringStarts = info.stringStarts;
                stringEnds = info.stringEnds;
                currentLine = -1;
                restoreText();
                return;
            }

            encoding = GetEncoding();
            contents = ReadContents();
            stringStarts = new List<int>();
            stringEnds = new List<int>();
            
            ParseLines();
            //Кэшируем то, что получили
            cache.Add(this.filePath, new CachedInfo(contents, encoding, stringStarts, stringEnds),
                      DateTime.Now.AddDays(30));

            currentLine = -1; //чтобы правильно отрабатывал гетлайн

            Mode = mode;
        }

        /// <summary>
        /// Вычислить номер строки по смещению
        /// </summary>
        /// <param name="offset">смещение</param>
        /// <returns>номер строки</returns>
        public int countLineByOffset(int offset)
        {
            for (int i = 0; i < stringEnds.Count; i++)
            {
                if (offset <= stringEnds[i] + 1)
                    return i + 1;
            }

            throw new Exception("Line by offset was not found");
        }

        /**
         * Get column number by offset
         */
        public int countColumnByOffset(int offset)
        {
            int line = this.countLineByOffset(offset);

            int column = offset - stringStarts[line - 1];

            if (column < 0)
                throw new Exception("Column by offset was not found");

            return column;
        }

        /**
         * Get offset of the next line. Нумерация смещения начинается с 0 
         */
        public int currentLineOffset()
        {
            return stringStarts[currentLine];
        }

        /**
         * Get the number of lines in the file
         */
        public int getLinesCount()
        {
            return stringStarts.Count();
        }

        /**
         * Get offset for the line and column given. Нумерация строк и столбцов начинается с 1. Нумерация смещения начинается с 0 
         */
        public int getOffset(int line, int column)
        {
            if (stringStarts.Count > line && line > 0 && stringStarts[line] - stringStarts[line - 1] >= column || line == stringStarts.Count && stringEnds[line - 1] - stringStarts[line - 1] + 1 >= column)
            {
                return stringStarts[line - 1] + column-1; //Нумерация строк в Хранилище ведётся с 1
            }
            else
                return -1;
        }

        /**
         * Get offset for the line and column given. Нумерация строк и столбцов начинается с 1. Нумерация смещения начинается с 0
         * Encoding enc - исходя из входной кодировки получаем число байт, которые содержат символы в строке. 
         */
        public int getOffset(int line, int column, Encoding enc)
        {
            string sline = getLineN(line);
            if (enc.GetString(encoding.GetBytes(sline)).Length == sline.Length) return getOffset(line, column);
            int i = 0;
            int limit = stringEnds[line - 1] - stringStarts[line - 1] + 1;
            for (; i < column; i++)
            {
                if (i == limit) throw new Exception("Wrong offset");
                string symbol = sline[i].ToString();
                int bcOr = encoding.GetByteCount(symbol);
                int bcEx = enc.GetByteCount(symbol);
                if (bcOr != bcEx)
                {
                    if (bcOr < bcEx)
                    {
                        i += bcEx - bcOr; // check if not +1
                    }
                    else
                    {
                        column -= bcOr - bcEx; // check if not +1
                    }
                }
            }
            return stringStarts[line - 1] + i - 1;
        }

        /**
         * Get offset of the first character of the line. Нумерация строк начинается с 1. Нумерация смещения начинается с 0
         */
        public int getLineOffset(int line)
        {
            return getOffset(line, 1); //Нумерация столбцов в Хранилище ведётся с 1
        }


        /**
         * Get line number N. Нумерация строк начинается с 1
         */
        public string getLineN(int lineNumber)
        {
            if (lineNumber > stringStarts.Count() || lineNumber == 0)
                return null;

            //if (lineNumber == stringStarts.Count() - 1 && stringStarts[lineNumber - 1] == stringEnds[lineNumber - 1])
            //    return "";

            if (lineNumber == stringStarts.Count())
                return contentsString.Substring(stringStarts[lineNumber - 1]);

            string res = contentsString.Substring(stringStarts[lineNumber - 1], stringEnds[lineNumber - 1] - stringStarts[lineNumber - 1] + 1);
            
            if (Mode == OpenMode.SLASH0IGNORE)
            {
                res = res.Replace("\0", " ");
            }

            return res;
        }
        
        
        /**
         * Get current line (if exceeds line number returns null value
         */
        public string getLine()
        {
            currentLine++;

            if (currentLine > stringStarts.Count() - 1)
                return null;

            if (currentLine == stringStarts.Count() - 1)
                return contentsString.Substring(stringStarts[currentLine]);

            string res = contentsString.Substring(stringStarts[currentLine], stringEnds[currentLine] - stringStarts[currentLine] + 1);

            if (Mode == OpenMode.SLASH0IGNORE)
            {
                res = res.Replace("\0", " ");
            }

            return res;
        }

        /**
         * Returns text for GUI-purposes (without \r symbol)
         */
        public string getNormalizedText()
        {
            string res = contentsString.Replace("\r\n", "\n").Replace("\n\r", "\n").Replace("\r", "");
            if (Mode == OpenMode.SLASH0IGNORE)
            {
                res = res.Replace("\0", " ");
            }
            return res;
        }

        /**
         * Returns the text as it is in the file. Note - text may differ after calling replaceLine
         */
        public string getText()
        {
            string res = contentsString;
            if (Mode == OpenMode.SLASH0IGNORE)
            {
                res = res.Replace("\0", " ");
            }
            return res;
        }

        /**
         * Replaces the line in the text. Нумерация строк начинается с 1
         */
        public void replaceLine(int lineNum, string replacement)
        {
            if (!replacement.Contains("\n"))
                throw new Exception("No new line in the replacement!");
            
            contentsString = contentsString.Substring(0, stringStarts[lineNum - 1]) + replacement + contentsString.Substring(stringEnds[lineNum - 1] + 1);
            ParseLines();
        }

        /**
         * Write current contents to file
         */
        public void writeToFile(string fileName)
        {
            byte[] write = encoding.GetBytes(contentsString);
            
            using (FileStream stream = new FileStream(fileName, FileMode.Create))
            {
                stream.Write(write, 0, write.Length);
            }

            MemoryCache cache = MemoryCache.Default;
            cache.Remove(filePath);
        }

        /**
         * Write text from string to file using encoding
         */
        public void writeTextToFile(string fileName, string contentsString)
        {
            byte[] write = encoding.GetBytes(contentsString);

            using (FileStream myStream = new FileStream(fileName, FileMode.Create))
            {
                myStream.Write(write, 0, write.Length);
            }

            MemoryCache cache = MemoryCache.Default;
            cache.Remove(filePath);
        }

        /**
         * Restore current text to file
         */
        public void restoreText()
        {
            try
            {
                contentsString = contents.ToString();
            }
            catch (Exception)
            {
            }
        }
        
    }
}
