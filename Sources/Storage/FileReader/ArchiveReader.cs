﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using IOController;

namespace FileOperations
{
    /// <summary>
    /// Выполняет чтение архивов различных форматов
    /// </summary>
    public static class ArchiveReader
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "ArchiveReader";

        /// <summary>
        /// Распаковывает архив и для каждого запакованного в архив файла выдает потоки для его чтения.
        /// </summary>
        /// <param name="archiveFilePath">Путь до файла архива</param>
        /// <param name="tempDirectoryPath">Путь до произвольной временной директории</param>
        /// <returns></returns>
        public static IEnumerable<ModifiedFileStream> Read(string archiveFilePath, string tempDirectoryPath)
        {
            var files = ReadFileList(archiveFilePath);

            //Для каждого файла в архиве
            foreach (string fileName in files)
            {
                //Получаем результат выполнения программы 7z.exe
                //string output =
                ExtProg.Execute("common\\7z.exe", "x -y -o" + "\"" + tempDirectoryPath + "\"" + " " + "\"" + archiveFilePath + "\"" + " " + "\"" + fileName + "\"");

                //Получаем путь до файлв во временной директории
                string tempFilePath = Path.Combine(tempDirectoryPath, fileName);

                ModifiedFileStream fileStream = null;
                try
                {
                    yield return fileStream = new ModifiedFileStream(tempFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                }
                finally
                {
                    if (fileStream != null)
                        fileStream.Dispose();
                }
            }

        }

        /// <summary>
        /// Распаковать архив
        /// </summary>
        /// <param name="archiveFilePath">путь к архиву</param>
        /// <param name="destinationDirectoryPath">место распаковки</param>
        /// <returns></returns>
        public static List<string> ExtractAll(string archiveFilePath, string destinationDirectoryPath)
        {
            if (FileController.IsExists(archiveFilePath) && DirectoryController.IsExists(destinationDirectoryPath))
                ExtProg.Execute("common\\7z.exe", String.Format("x -y -o\"{0}\" \"{1}\"", destinationDirectoryPath, archiveFilePath));
            return DirectoryController.GetFiles(destinationDirectoryPath, true);
        }

        /// <summary>
        /// Получить список файлов в архиве.
        /// </summary>
        /// <param name="archiveFilePath">Путь до файла архива.</param>
        /// <returns></returns>
        public static List<string> ReadFileList(string archiveFilePath)
        {

            //Входная интересубщая строка идёт после constBlockStartOrEndLine и выглядит как-то так:
            //2014-09-05 14:24:34 ....A          629      1860123  Sources\Web\resource\icons\2humans_add.gif
            //или так
            //                                    40           33  Маршруты testSimple1.testSimple1class.dummyfunc0.log
            //но главное - фиксированные позиции подстрок.
            //Поэтому нет смысла включать эвристику, которая может иногда не срабатывать (для того же .gz возможна строка 
            //2014-09-12 10:24:24                 53           48  Маршруты testSimple1.testSimple1class.dummyfunc0.log
            //).
            
            //Константы
            const string constBlockStartOrEndLine = "------------------- ----- ------------ ------------  ------------------------";

            //Результат выполнения сторонней программы
            string output;

            //Список файлов внутри архива
            List<string> files = new List<string>();

            //Получаем результат выполнения программы 7z.exe
            output = ExtProg.Execute("common\\7z.exe", "l " + "\"" + archiveFilePath + "\"");

            using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(output)))
            {
                using (StreamReader streamReader = new StreamReader(memoryStream))
                {
                    //Находимся ли мы внутри блока?
                    bool isInBlock = false;

                    string line = null;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        //Если строка пуста, ничего не делаем
                        if (String.IsNullOrWhiteSpace(line))
                            continue;

                        //Нормализуем строку
                        //line = line.Trim();

                        //Отслеживаем начало и конец блока
                        if (line == constBlockStartOrEndLine)
                        {
                            isInBlock = !isInBlock;
                            continue;
                        }

                        //Если находимся не внутри блока, переходим к следующей строке
                        if (!isInBlock)
                            continue;

                        if (line[20] == 'D') //признак каталога
                            continue;

                        files.Add(line.Substring(53));
                        #region oldlogic
                        ////Перекодируем строку из Windows-1251 в cp866
                        //line = ArchiveReader.ReencodeLine(line, 1251, 866);

                        ////Получаем список всех значащих подстрок
                        //List<string> sublines = line.Split(' ').Where(i => !String.IsNullOrEmpty(i)).ToList();

                        ////Дополняем список подстрок, если надо
                        //// В gz не проставлены даты создания и изменения файлов
                        //while (sublines.Count < 5)
                        //    sublines.Insert(0, "");

                        //Int32 itmp;
                        //if (Int32.TryParse(sublines[4], out itmp))
                        //{
                        //    if (sublines.Count > 6)
                        //        for (int i = 6; i < sublines.Count; i++)
                        //            sublines[5] += " " + sublines[i];

                        //    if (sublines[2][0] != 'D')
                        //        files.Add(sublines[5]);
                        //}
                        //else
                        //{
                        //    if (sublines.Count > 5)
                        //        for (int i = 5; i < sublines.Count; i++)
                        //            sublines[4] += " " + sublines[i];

                        //    if (sublines[2][0] != 'D')
                        //        files.Add(sublines[4]);
                        //}
                        #endregion
                    }

                }
            }

            return files;
        }

        /// <summary>
        /// Смена кодировки строки
        /// </summary>
        /// <param name="line">Строка</param>
        /// <param name="currentEncodingCodePage">Идентификатор текущей кодировки строки</param>
        /// <param name="preferredEncodingCodePage">Идентификатор желаемой кодировки строки</param>
        /// <returns>Строка в желаемой кодировке</returns>
        internal static string ReencodeLine(string line, int currentEncodingCodePage, int preferredEncodingCodePage)
        {
            return Encoding.GetEncoding(preferredEncodingCodePage).GetString(Encoding.GetEncoding(currentEncodingCodePage).GetBytes(line));
        }

        /// <summary>
        /// Класс-модификация файлового потока
        /// </summary>
        public class ModifiedFileStream : FileStream, IDisposable
        {
            /// <summary>
            /// Путь к файлу
            /// </summary>
            public string FilePath { get; private set; }

            /// <summary>
            /// Требуется ли удалить файл, после закрытия потока
            /// </summary>
            public bool IsNeedToRemoveAfter { private get; set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="filePath"></param>
            /// <param name="mode"></param>
            /// <param name="access"></param>
            /// <param name="share"></param>
            public ModifiedFileStream(string filePath, FileMode mode, FileAccess access, FileShare share)
                : base(filePath, mode, access, share)
            {
                this.FilePath = filePath;
                this.IsNeedToRemoveAfter = true;
            }

            /// <summary>
            /// Освобождение неуправляемых ресурсов
            /// </summary>
            /// <param name="disposing"></param>
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);

                //Если требуется удалить файл, делаем это, сбрасывая атрибуты
                if (IsNeedToRemoveAfter)
                    if (FileController.IsExists(this.FilePath))
                        FileController.Remove(this.FilePath, true);
            }
        }
    }

}
