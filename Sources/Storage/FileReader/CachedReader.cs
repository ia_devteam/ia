﻿using System;
using System.Collections.Generic;

using IA.Extensions;

namespace FileOperations
{
    /// <summary>
    /// Класс для чтения файлов с кэшем
    /// </summary>
    public sealed class CachedReader:IDisposable
    {
        private const string thisName = "CachedReader";

        Dictionary<int, int> skeleton;
        string contents;

        /// <summary>
        /// конструктор класса
        /// </summary>
        /// <param name="fName"></param>
        public CachedReader(string fName)
        {
            skeleton = new Dictionary<int, int>();//инициализация словаря с указателями на строки

            AbstractReader fr = null;
            try
            {
                fr = new AbstractReader(fName);//открыли файл
            }
            catch (Exception ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    "Не удалось открыть файл " + fName + ".",
                    ex.ToFullMultiLineMessage()
                }.ToMultiLineString();

                IA.Monitor.Log.Error(thisName, message);
            }

            //инициализация скелета
            if ((contents = fr.getNormalizedText()) == null) throw new Exception();//мы не смогли считать такой файл

            //иначе - читаем
            int strCount = 1;
            int offset = 0;
            skeleton.Add(strCount, offset);
            for (int i = 0; i < contents.Length; i++)
            {
                //ридлайн часто глючит - делаем посимвольное чтение
                if (contents[i] == '\n')
                {
                    strCount++;
                    offset = i + 1;
                    skeleton.Add(strCount, offset);//добавили строку в словарь
                }
            }
            //все считали - теперь все есть
        }
        /// <summary>
        /// прочитать строку с номером N
        /// </summary>
        /// <param name="lineNum">номер строки</param>
        /// <returns></returns>
        public string readLineN(int lineNum)
        {
            try
            {
                int startOffset = skeleton[lineNum];
                if (skeleton.ContainsKey(lineNum + 1))
                    return contents.Substring(startOffset, skeleton[lineNum + 1] - startOffset - 1);
                else
                    return contents.Substring(startOffset);
            }
            catch (Exception)
            {
                IA.Monitor.Log.Error(thisName, "Ошибка подачи параметров");
                throw new Exception();
            }
        }

        /// <summary>
        /// получить полный текст файла
        /// </summary>
        /// <returns></returns>
        public string getText()
        {
            return contents;
        }

        /// <summary>
        /// получить количество строк в файле
        /// </summary>
        /// <returns></returns>
        public int getLinesCount()
        {
            return skeleton.Keys.Count;
        }

        /// <summary>
        /// прочитать участок содержимое файла
        /// </summary>
        /// <param name="startCol">начальная строка</param>
        /// <param name="startRow">начальный столбец</param>
        /// <param name="endCol">конечный столбец</param>
        /// <param name="endRow">конечная строка</param>
        /// <returns></returns>
        public string readFromTo(int startCol, int startRow, int endCol, int endRow)
        {
            //проверки на глупость
            try{
                int startOffset = skeleton[startCol] + startRow - 1;
                int endOffset = skeleton[endCol] + endRow - 1;
                //жесткий код под экспешном
                return contents.Substring(startOffset, endOffset - startOffset+1);
                
            }catch(Exception)
            {
                IA.Monitor.Log.Error(thisName, "Ошибка подачи параметров");
                throw new Exception();
            }
        }
        
        /// <summary>
        /// высвобождение ресурсов
        /// </summary>
        public void Dispose()
        {
            foreach (int key in skeleton.Keys)
                skeleton.Remove(key); //можно этот этап и убрать
        }
    }
}
