﻿/*
 * Главный интерфейс
 * Файл StorageViewer.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Лошкарёв, Юхтин
 * 
 * форма просмотра содержимого открытого хранилища
 */

using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using FileOperations;

using IA.Extensions;

namespace Store.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class StorageContentsViewControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Просмотрщик содержимого Хранилища";

        /// <summary>
        /// Какую часть Хранилища показывать
        /// </summary>
        public enum ViewMode
        {
            /// <summary>
            /// Показывать функции
            /// </summary>
            Functions,

            /// <summary>
            /// Показывать переменные
            /// </summary>
            Variables,

            /// <summary>
            /// Показывать файлы
            /// </summary>
            Files
        }

        /// <summary>
        ///  Делегат для события - Элемент управления закрывается
        /// </summary>
        public delegate void ClosedEventHandler();

        /// <summary>
        /// Событие - Элемент управления закрывается
        /// </summary>
        public event ClosedEventHandler ClosedEvent;

        /// <summary>
        /// Хранилище
        /// </summary>
        private Store.Storage storage;

        /// <summary>
        /// Что показывать
        /// </summary>
        private ViewMode state;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="state"></param>
        /// <param name="storage"></param>
        public StorageContentsViewControl(ViewMode state, Store.Storage storage)
        {
            this.state = state;
            this.storage = storage;

            Dock = DockStyle.Fill;

            InitializeComponent();

            treeView.Nodes.Clear();
            switch (state)
            {
                case ViewMode.Files:
                    {
                        foreach (Store.IFile file in storage.files.EnumerateFiles(enFileKind.anyFile))
                            treeView.Nodes.Add(new TreeNode() { Text = file.FileNameForReports, Tag = file });

                        break;
                    }
                case ViewMode.Functions:
                    {
                        foreach (Store.IFunction function in storage.functions.EnumerateFunctions(enFunctionKind.ALL))
                        {
                            Store.Location functionDefenition = function.Definition();
                            if (functionDefenition != null)
                            {
                                TreeNode functionNode = new TreeNode()
                                {
                                    Text = function.FullName + " (" + function.CalledBy().ToArray<Store.IFunctionCall>().Length.ToString() + " вызовов)",
                                    Tag = function
                                };

                                TreeNode definitionsNode = new TreeNode() { Text = "Определения" };

                                if (functionDefenition != null)
                                {
                                    TreeNode childNode = new TreeNode()
                                    {
                                        Text = "Define:" + functionDefenition.GetFile().FileNameForReports + ", " + functionDefenition.GetColumn().ToString(),
                                        Tag = functionDefenition
                                    };
                                    definitionsNode.Nodes.Add(childNode);
                                }
                                
                                definitionsNode.Expand();
                                functionNode.Nodes.Add(definitionsNode);

                                TreeNode declarationsNode = new TreeNode() { Text = "Декларации" };
                                foreach (Store.Location location in function.Declarations())
                                {
                                    TreeNode childNode = new TreeNode()
                                    {
                                        Text = "Declare:" + location.GetFile().FileNameForReports + ", " + location.GetColumn().ToString(),
                                        Tag = location
                                    };
                                    declarationsNode.Nodes.Add(childNode);
                                }
                                declarationsNode.Expand();
                                functionNode.Nodes.Add(declarationsNode);

                                TreeNode callsNode = new TreeNode() { Text = "Вызовы" };
                                foreach (Store.IFunctionCall caller in function.CalledBy())
                                {
                                    TreeNode childNode = new TreeNode()
                                    {
                                        Text = "Call:" + caller.Caller.Definition().GetFile().FileNameForReports + ", " + caller.Caller.Definition().GetColumn().ToString(),
                                        Tag = caller.CallLocation
                                    };
                                    callsNode.Nodes.Add(childNode);
                                }
                                callsNode.Expand();
                                functionNode.Nodes.Add(callsNode);

                                treeView.Nodes.Add(functionNode);
                            }
                        }
                        break;
                    }
                case ViewMode.Variables:
                    {
                        foreach (Store.Variable variable in storage.variables.EnumerateVariables())
                        {
                            if (variable.FullName != String.Empty && variable.Definitions().Length != 0)
                            {
                                TreeNode variableNode = new TreeNode() { Text = variable.FullName, Tag = variable };

                                TreeNode defsNode = new TreeNode() { Text = "Определения" };
                                foreach (Store.Location location in variable.Definitions())
                                {
                                    TreeNode childNode = new TreeNode()
                                    {
                                        Text = "Define:" + location.GetFile().FileNameForReports + ", " + location.GetLine().ToString(),
                                        Tag = location
                                    };
                                    defsNode.Nodes.Add(childNode);
                                }
                                defsNode.Expand();
                                variableNode.Nodes.Add(defsNode);

                                TreeNode setsNode = new TreeNode() { Text = "Присвоения" };
                                foreach (Store.Location location in variable.ValueSetPosition())
                                {
                                    TreeNode childNode = new TreeNode()
                                    {
                                        Text = "type:" + location.GetFile().FileNameForReports + ", " + location.GetLine().ToString(),
                                        Tag = location
                                    };
                                    setsNode.Nodes.Add(childNode);
                                }
                                setsNode.Expand();
                                variableNode.Nodes.Add(setsNode);

                                TreeNode getsNode = new TreeNode() { Text = "Использования" };
                                foreach (Store.Location location in variable.ValueGetPosition())
                                {
                                    TreeNode childNode = new TreeNode()
                                    {
                                        Text = "Get:" + location.GetFile().FileNameForReports + ", " + location.GetLine().ToString(),
                                        Tag = location
                                    };
                                    getsNode.Nodes.Add(childNode);
                                }
                                getsNode.Expand();
                                variableNode.Nodes.Add(getsNode);

                                treeView.Nodes.Add(variableNode);
                            }
                        }
                        break;
                    }
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            switch (state)
            {
                case ViewMode.Files:
                    {
                        textBox.Text = new AbstractReader(((Store.IFile)treeView.SelectedNode.Tag).FullFileName_Original).getText().Replace("\r"," ");
                        break;
                    }
                case ViewMode.Functions:
                    {
                        Store.Location currentLocation = null;
                        Store.IFunction currentFunction = null;
                        if (e.Node.Level == 0)
                        {
                            currentFunction = (Store.IFunction)e.Node.Tag;
                            currentLocation = currentFunction.Definition();
                        }
                        else if (e.Node.Level == 2)
                        {
                            if (e.Node.Parent.Text == "Определения")
                            {
                                currentFunction = (Store.IFunction)e.Node.Parent.Parent.Tag;
                                currentLocation = (Store.Location)e.Node.Tag;
                            }
                            else if (e.Node.Parent.Text == "Вызовы")
                            {
                                currentFunction = (Store.IFunction)e.Node.Parent.Parent.Tag;
                                currentLocation = (Store.Location)e.Node.Tag;
                            }
                        }

                        if (currentLocation != null && currentFunction != null)
                        {
                            int startOffset = (int)currentLocation.GetOffset();
                            if (startOffset < 1)
                                startOffset = 1;
                            SelectTextInTextbox(
                                currentLocation.GetFile().FullFileName_Original,
                                startOffset,
                                currentFunction.Name.Length
                                );
                        }
                        break;
                    }
                case ViewMode.Variables:
                    {
                        Store.Variable currentVariable = null;
                        Store.Location currentLocation = null;

                        if (e.Node.Level == 0)
                        {
                            currentVariable = (Store.Variable)e.Node.Tag;
                            currentLocation = currentVariable.Definitions().FirstOrDefault();
                            #region old realization
                            //FileReader reader = null;
                            //try
                            //{
                            //    reader = new FileReader(((Store.Variable)e.Node.Tag).Definition()[0].GetFile().FullFileName_Original, OpenMode.SLASH0IGNORE);
                            //}
                            //catch (Exception ex)
                            //{
                            //    IA.Monitor.Log.Error(Storage.objectName, "Не удалось открыть файл " + ((Store.Variable)e.Node.Tag).Definition()[0].GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                            //}

                            //textBox.Text = reader.GetText();
                            //int index = textBox.Lines[(int)((Store.Variable)e.Node.Tag).Definition()[0].GetLine() - 1].IndexOf(((Store.Variable)e.Node.Tag).Name.Substring(((Store.Variable)e.Node.Tag).Name.LastIndexOf('.') + 1));
                            //if (index < 0) index = 0;
                            //textBox.Select((int)((Store.Variable)e.Node.Tag).Definition()[0].GetOffset() + index, ((Store.Variable)e.Node.Tag).Name.Length - ((Store.Variable)e.Node.Tag).Name.LastIndexOf('.') - 1);
                            //textBox.SelectionBackColor = Color.Black;
                            //textBox.SelectionColor = StorageContentsViewControl.DefaultBackColor;
                            //textBox.ScrollToCaret(); 
                            #endregion
                        }
                        else if (e.Node.Level == 2)
                        {
                            currentVariable = (Store.Variable)e.Node.Parent.Parent.Tag;
                            currentLocation = (Store.Location)e.Node.Tag;
                            #region old realization
                            //if (e.Node.Parent.Text == "Определения" || e.Node.Parent.Text == "Декларации")
                            //{
                            //    FileReader reader = null;
                            //    try
                            //    {
                            //        reader = new FileReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original, OpenMode.SLASH0IGNORE);
                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        IA.Monitor.Log.Error(Storage.objectName, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                            //    }

                            //    textBox.Text = reader.GetText();
                            //    int index = textBox.Lines[(int)((Store.Location)e.Node.Tag).GetLine() - 1].IndexOf(((Store.Variable)e.Node.Parent.Parent.Tag).Name.Substring(((Store.Variable)e.Node.Parent.Parent.Tag).Name.LastIndexOf('.') + 1));
                            //    if (index < 0) index = 0;
                            //    textBox.Select((int)((Store.Location)e.Node.Tag).GetOffset() + index, ((Store.Variable)e.Node.Parent.Parent.Tag).Name.Length - ((Store.Variable)e.Node.Parent.Parent.Tag).Name.LastIndexOf('.') - 1);
                            //    textBox.SelectionBackColor = Color.Black;
                            //    textBox.SelectionColor = StorageContentsViewControl.DefaultBackColor;
                            //    textBox.ScrollToCaret();
                            //}
                            //else if (e.Node.Parent.Text == "Присвоения")
                            //{
                            //    FileReader reader = null;
                            //    try
                            //    {
                            //        reader = new FileReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original, OpenMode.SLASH0IGNORE);
                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        IA.Monitor.Log.Error(Storage.objectName, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                            //    }

                            //    textBox.Text = reader.GetText();
                            //    int index = textBox.Lines[(int)((Store.Location)e.Node.Tag).GetLine() - 1].IndexOf(((Store.Variable)e.Node.Parent.Parent.Tag).Name.Substring(((Store.Variable)e.Node.Parent.Parent.Tag).Name.LastIndexOf('.') + 1));
                            //    if (index < 0) index = 0;
                            //    textBox.Select((int)((Store.Location)e.Node.Tag).GetOffset() + index, ((Store.Variable)e.Node.Parent.Parent.Tag).Name.Length - ((Store.Variable)e.Node.Parent.Parent.Tag).Name.LastIndexOf('.') - 1);
                            //    textBox.SelectionBackColor = Color.Black;
                            //    textBox.SelectionColor = StorageContentsViewControl.DefaultBackColor;
                            //    textBox.ScrollToCaret();
                            //}
                            //else if (e.Node.Parent.Text == "Использования")
                            //{
                            //    FileReader reader = null;
                            //    try
                            //    {
                            //        reader = new FileReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original, OpenMode.SLASH0IGNORE);
                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        IA.Monitor.Log.Error(Storage.objectName, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                            //    }

                            //    textBox.Text = reader.GetText();
                            //    int index = textBox.Lines[(int)((Store.Location)e.Node.Tag).GetLine() - 1].IndexOf(((Store.Variable)e.Node.Parent.Parent.Tag).Name.Substring(((Store.Variable)e.Node.Parent.Parent.Tag).Name.LastIndexOf('.') + 1));
                            //    if (index < 0) index = 0;
                            //    textBox.Select((int)((Store.Location)e.Node.Tag).GetOffset() + index, ((Store.Variable)e.Node.Parent.Parent.Tag).Name.Length - ((Store.Variable)e.Node.Parent.Parent.Tag).Name.LastIndexOf('.') - 1);
                            //    textBox.SelectionBackColor = Color.Black;
                            //    textBox.SelectionColor = StorageContentsViewControl.DefaultBackColor;
                            //    textBox.ScrollToCaret();
                            //} 
                            #endregion
                        }

                        if (currentLocation != null && currentVariable != null)
                        {
                            int simpleNameOffsetInFullName = currentVariable.Name.LastIndexOf('.') + 1;
                            if (simpleNameOffsetInFullName < 1)
                                simpleNameOffsetInFullName = 0;

                            SelectTextInTextbox(
                                currentLocation.GetFile().FullFileName_Original,
                                (int)currentLocation.GetOffset() + simpleNameOffsetInFullName,
                                currentVariable.Name.Length - simpleNameOffsetInFullName
                                );
                        }

                        break;
                    }
            }
        }


        void SelectTextInTextbox(string fileToReadFrom, int selectionStartOffset, int selectionLength)
        {
            AbstractReader reader = null;
            try
            {
                reader = new AbstractReader(fileToReadFrom);
            }
            catch (Exception ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    "Не удалось открыть файл <" + fileToReadFrom + ">.",
                    ex.ToFullMultiLineMessage()
                }.ToMultiLineString();

                IA.Monitor.Log.Error(Storage.objectName, message);
            }
            textBox.Text = reader.getText().Replace("\r", " ");
            textBox.Select(selectionStartOffset, selectionLength);
            textBox.SelectionBackColor = Color.Black;
            textBox.SelectionColor = StorageContentsViewControl.DefaultBackColor;
            textBox.ScrollToCaret();
        }

        private void ready_button_Click(object sender, EventArgs e)
        {
            try
            {
                if (ClosedEvent != null)
                    ClosedEvent();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Сохранение результатов работы с содержимым Хранилища", (message) => IA.Monitor.Log.Error(ObjectName, message, true));
            }
        }

        private void filter_textBox_TextChanged(object sender, EventArgs e)
        {
            treeView.Nodes.Clear();

            switch (state)
            {
                case ViewMode.Files:
                    {
                        foreach (Store.IFile file in storage.files.EnumerateFiles(enFileKind.anyFile))
                            if (file.FullFileName_Original.Contains(filter_textBox.Text))
                                treeView.Nodes.Add(new TreeNode() { Text = file.FileNameForReports, Tag = file });

                        break;
                    }
                case ViewMode.Functions:
                    {
                        foreach (Store.IFunction function in storage.functions.EnumerateFunctions(enFunctionKind.ALL))
                        {
                            Location functionDefenition = function.Definition();
                            if (function.FullName.ToLower().Contains(filter_textBox.Text.ToLower()) && functionDefenition != null)
                            {
                                TreeNode functionNode = new TreeNode()
                                {
                                    Text = function.FullName + " (" + function.CalledBy().ToArray<Store.IFunctionCall>().Length.ToString() + " вызовов)",
                                    Tag = function

                                };

                                TreeNode definitionsNode = new TreeNode() { Text = "Определения" };

                                TreeNode childNode1 = new TreeNode()
                                {
                                    Text = "Define:" + functionDefenition.GetFile().FileNameForReports + ", " + functionDefenition.GetColumn().ToString(),
                                    Tag = functionDefenition
                                };
                                definitionsNode.Nodes.Add(childNode1);

                                definitionsNode.Expand();
                                functionNode.Nodes.Add(definitionsNode);

                                TreeNode declarationsNode = new TreeNode() { Text = "Декларации" };
                                foreach (Store.Location location in function.Declarations())
                                {
                                    TreeNode childNode = new TreeNode()
                                    {
                                        Text = "Declare:" + location.GetFile().FileNameForReports + ", " + location.GetColumn().ToString(),
                                        Tag = location
                                    };
                                    declarationsNode.Nodes.Add(childNode);
                                }
                                declarationsNode.Expand();
                                functionNode.Nodes.Add(declarationsNode);

                                TreeNode callsNode = new TreeNode() { Text = "Вызовы" };
                                foreach (Store.IFunctionCall caller in function.CalledBy())
                                {
                                    TreeNode childNode = new TreeNode()
                                    {
                                        Text = "Call:" + caller.Caller.Definition().GetFile().FileNameForReports + ", " + caller.Caller.Definition().GetColumn().ToString(),
                                        Tag = caller.Caller
                                    };
                                    callsNode.Nodes.Add(childNode);
                                }
                                callsNode.Expand();
                                functionNode.Nodes.Add(callsNode);

                                treeView.Nodes.Add(functionNode);
                            }
                        }
                        break;
                    }
                case ViewMode.Variables:
                    {
                        foreach (Store.Variable variable in storage.variables.EnumerateVariables())
                        {
                            if (!variable.FullName.ToLower().Contains(filter_textBox.Text.ToLower()))
                                continue;

                            if (variable.isGlobal)
                            {
                                if (variable.Definitions().Length != 0)
                                {
                                    TreeNode variableNode = new TreeNode() { Text = variable.FullName, Tag = variable };

                                    TreeNode defsNode = new TreeNode();
                                    defsNode.Text = "Определения";
                                    foreach (Store.Location location in variable.Definitions())
                                    {
                                        TreeNode childNode = new TreeNode();
                                        childNode.Tag = location;
                                        childNode.Text = "Define:" + location.GetFile().FileNameForReports + ", " + location.GetLine().ToString();
                                        defsNode.Nodes.Add(childNode);
                                    }
                                    defsNode.Expand();
                                    variableNode.Nodes.Add(defsNode);

                                    TreeNode setsNode = new TreeNode();
                                    setsNode.Text = "Присвоения";
                                    foreach (Store.Location location in variable.ValueSetPosition())
                                    {
                                        TreeNode childNode = new TreeNode();
                                        childNode.Tag = location;
                                        childNode.Text = "type:" + location.GetFile().FileNameForReports + ", " + location.GetLine().ToString();
                                        setsNode.Nodes.Add(childNode);
                                    }
                                    setsNode.Expand();
                                    variableNode.Nodes.Add(setsNode);

                                    TreeNode getsNode = new TreeNode();
                                    getsNode.Text = "Использования";
                                    foreach (Store.Location location in variable.ValueGetPosition())
                                    {
                                        TreeNode childNode = new TreeNode();
                                        childNode.Tag = location;
                                        childNode.Text = "Get:" + location.GetFile().FileNameForReports + ", " + location.GetLine().ToString();
                                        getsNode.Nodes.Add(childNode);
                                    }
                                    getsNode.Expand();
                                    variableNode.Nodes.Add(getsNode);

                                    treeView.Nodes.Add(variableNode);
                                }
                            }
                        }
                        break;
                    }
            }
        }
    }
}
