﻿namespace Store.Controls
{
    partial class StorageContentsViewControl
    {
        ///// <summary> 
        ///// Требуется переменная конструктора.
        ///// </summary>
        //private System.ComponentModel.IContainer components = null;


        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.treeView = new System.Windows.Forms.TreeView();
            this.textBox = new System.Windows.Forms.RichTextBox();
            this.filter_groupBox = new System.Windows.Forms.GroupBox();
            this.filter_textBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.filter_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.treeView, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.filter_groupBox, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(696, 569);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // treeView
            // 
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.Location = new System.Drawing.Point(3, 3);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(342, 516);
            this.treeView.TabIndex = 0;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            // 
            // textBox
            // 
            this.textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox.Location = new System.Drawing.Point(351, 3);
            this.textBox.Name = "textBox";
            this.tableLayoutPanel1.SetRowSpan(this.textBox, 2);
            this.textBox.Size = new System.Drawing.Size(342, 563);
            this.textBox.TabIndex = 1;
            this.textBox.Text = "";
            this.textBox.WordWrap = false;
            // 
            // filter_groupBox
            // 
            this.filter_groupBox.Controls.Add(this.filter_textBox);
            this.filter_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filter_groupBox.Location = new System.Drawing.Point(3, 525);
            this.filter_groupBox.Name = "filter_groupBox";
            this.filter_groupBox.Size = new System.Drawing.Size(342, 41);
            this.filter_groupBox.TabIndex = 3;
            this.filter_groupBox.TabStop = false;
            this.filter_groupBox.Text = "Фильтр";
            // 
            // filter_textBox
            // 
            this.filter_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filter_textBox.Location = new System.Drawing.Point(3, 16);
            this.filter_textBox.Name = "filter_textBox";
            this.filter_textBox.Size = new System.Drawing.Size(336, 20);
            this.filter_textBox.TabIndex = 0;
            this.filter_textBox.TextChanged += new System.EventHandler(this.filter_textBox_TextChanged);
            // 
            // StorageContentsViewControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "StorageContentsViewControl";
            this.Size = new System.Drawing.Size(696, 569);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.filter_groupBox.ResumeLayout(false);
            this.filter_groupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.RichTextBox textBox;
        private System.Windows.Forms.GroupBox filter_groupBox;
        private System.Windows.Forms.TextBox filter_textBox;
    }
}
