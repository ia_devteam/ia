using System;

namespace Store.Const
{
    public static class PluginIdentifiers
    {
        public const UInt32 DRIVER                              = 0001;//not used?
        public const UInt32 FILL_FILE_LIST                      = 0002;        
        public const UInt32 IDENTIFY_FILE_TYPES                 = 0003;
        public const UInt32 FILES_ESSENTIAL_FILE_MONITORS       = 0004;
        public const UInt32 MANUAL_FUNCTION_REDUNDANCY          = 0006;
        public const UInt32 UNDERSTAND_IMPORTER                 = 0007;
        public const UInt32 AIST_IMPORTER                       = 0008;
        public const UInt32 XR_F_IFDEF_IMPORTER                 = 0009;
        public const UInt32 HANDMADE_SENSORS_IMPORTER           = 0011;
        public const UInt32 INDEXING_FILE_CONTENT               = 0012;
        public const UInt32 CS_SENSORS                          = 0013;
        public const UInt32 WAY_GENERATOR                       = 0014;
        public const UInt32 BINARIES_READER                     = 0015;
        public const UInt32 FILE_ESSENTIAL_WEB                  = 0016;
        public const UInt32 JAVASCRIPT_PARSER                   = 0017;
        public const UInt32 SENSOR_GENERATOR		            = 0018;
        public const UInt32 POINTS_TO_ANALYSES_SIMPLE           = 0019;
        public const UInt32 DELETE_FILE_BY_TYPE                 = 0021;
        public const UInt32 REPLACE_SENSORS                     = 0022;
        public const UInt32 REPLACE_TRACES                      = 0023;
        public const UInt32 TASK_SEPARATOR_WORD                 = 0024;
        public const UInt32 BINARY_RIGHTS_CHECKER               = 0025;
        public const UInt32 FILE_SAVER                          = 0028;
        public const UInt32 CSHARP_PARSER                       = 0031;
        public const UInt32 PHP_PARSER                          = 0032;
        public const UInt32 DELPHI_SENSORS                      = 0037;
        public const UInt32 REPORTER                            = 0038;
        public const UInt32 DYNAMIC_ANALYSIS_RESULTS_PROCESSING = 0039;
        public const UInt32 SOURCE_REDUCER                      = 0041;
        public const UInt32 VB_PARSER                           = 0042;
        public const UInt32 FUNCTION_REDUNDANCY_COMPARE         = 0043;
        public const UInt32 DYNAMIC_REPORTS                     = 0044;
        public const UInt32 DLL_PURPOSE                         = 0046;
        public const UInt32 KERNEL_SEARCHER                     = 0048;
        public const UInt32 REMOVE_STORAGE_SENSORS              = 0049;
        public const UInt32 SENSORS_REENUMERATOR                = 0050;
        public const UInt32 SQL_ANALYSE                         = 0051;
        public const UInt32 RESTORE_SOURCES                     = 0052;
        public const UInt32 GRAPH_GENERATOR                     = 0053;
        public const UInt32 BIN_PROPERTY_READER                 = 0054;
        public const UInt32 SENSOR_TYPE_FIXER                   = 0055;
        public const UInt32 NDV_FISH                            = 0056;
        public const UInt32 CHECK_SUM                           = 0057;
        public const UInt32 MASTER_PLUGIN                       = 0058;
        public const UInt32 CXX_PARSER                          = 0059;
        public const UInt32 PERL_PARSER                         = 0060;
        public const UInt32 PYTHON_PARSER                       = 0061;
		public const UInt32 JAVASCRIPT_NODEJS_PARSER            = 0062;
        public const UInt32 XMLIMPORTER                         = 0063;
        public const UInt32 PHP5_LITE_PARSER                    = 0064;
    }
}
