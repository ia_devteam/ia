using System;

namespace Store.Const
{
    /// <summary>
    /// �����, ���������� �������� ������������ ����������. � ������, ��������� ���������� ���������� ������ 
    /// ����� ����!
    /// </summary>
    [Serializable]
    public class StorageException : System.Exception
    {

        #region Special Unused Constructors
        public StorageException() { }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "str")]
        public StorageException(string str, Exception ex) :base(str, ex) { }

        protected StorageException(
           System.Runtime.Serialization.SerializationInfo info,
           System.Runtime.Serialization.StreamingContext context) : base(info, context) {}

        public override void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion

        public StorageException(string error) : base(error)
        {
            this.error = error;
        }

        /// <summary>
        /// ����� ����������
        /// </summary>
        public string What
        {
            get { return error; }
        }
    	
	    private string error;    
    }
}
