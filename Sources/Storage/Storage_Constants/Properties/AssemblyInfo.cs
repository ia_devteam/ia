﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Storage_Constants")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RNT")]
[assembly: AssemblyProduct("Storage_Constants")]
[assembly: AssemblyCopyright("Copyright © RNT 2007")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]


// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a3aae371-5966-4d57-bef6-20e9be42c873")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("3.0.2.4")]
[assembly: AssemblyFileVersion("3.0.2.4")]
