﻿using System;
using System.Collections.Generic;
using System.IO;
using Word = Microsoft.Office.Interop.Word;

namespace DocExplorer
{
    public class DocTable
    {
        public string templateFile = "";
        public string Name = "";

        public Columns Cols = new Columns();

        /// <summary>
        /// Генерация файла "Связи по управлению.doc"
        /// </summary>
        public void GenerateDocFunc()
        {            
            object missing = System.Reflection.Missing.Value;
            object SaveChanges = Word.WdSaveOptions.wdSaveChanges;              // сохраняем изменения в документе

            object template1 = Directory.GetCurrentDirectory() + "\\" + templateFile;
            Word.Document template;
            Word.Application wordapp1 = new Word.Application();

            // Итоговый документ
            Word.Document worddocumentRez = null;

            try
            {
                template = wordapp1.Documents.Add(ref template1, ref missing, ref missing, ref missing);

                object fileName = (Name);

                object FileFormat = Word.WdSaveFormat.wdFormatDocument;
                template.SaveAs(ref fileName, ref FileFormat, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                worddocumentRez = template;

                Object begin = missing;
                Object end = missing;
                Object defaultTableBehavior = Type.Missing;
                Object autoFitBehavior = Type.Missing;
                
                Word.Range wordrange = worddocumentRez.Range(ref begin, ref end);

                // Добавляем таблицу
                Word.Table table = worddocumentRez.Tables.Add(wordrange, Cols.Rows.Count + 1, 3, ref defaultTableBehavior, ref autoFitBehavior);

                table.Borders.Enable = 1;

                table.Range.Font.Size = 14;
                table.Range.Font.Name = "Times New Roman";

                // Формируем заголовок
                table.Cell(1, 1).Range.Text = Cols.Title[0];
                table.Cell(1, 2).Range.Text = Cols.Title[1];
                table.Cell(1, 3).Range.Text = Cols.Title[2];

                // формируем содержимое столбцов
                int i = 2;
                foreach (Description desc in Cols.Rows)
                {
                    table.Cell(i, 1).Range.Text = desc.Name;
                    table.Cell(i, 2).Range.Text = "Файл " + desc.DefinitionPath + ", строка " + desc.Line.ToString();

                    table.Cell(i, 3).Range.Text = "Функция " + desc.Name + " предназначена для .";
                    
                    ++i;
                }
                
            }
            finally
            {
                worddocumentRez.Save();

                //закрываем приложения. Если этого не делать то процессы WORD.exe остаются в памяти, что приводит к понятным последствиям при множественных запусках данной программки ;)
                ((Word._Application)wordapp1).Quit(ref SaveChanges, ref missing, ref missing);
            }
        }

        /// <summary>
        /// Генерация файла "Связи по информации.doc" или файла "Информационные объекты.doc"
        /// </summary>
        public void GenerateDocVariable(bool extendedVariablesInfo)
        {
            object missing = System.Reflection.Missing.Value;
            object SaveChanges = Word.WdSaveOptions.wdSaveChanges;              // сохраняем изменения в документе

            object template1 = Directory.GetCurrentDirectory() + "\\" + templateFile;
            Word.Document template;
            Word.Application wordapp1 = new Word.Application();

            // Итоговый документ
            Word.Document worddocumentRez = null;

            try
            {
                template = wordapp1.Documents.Add(ref template1, ref missing, ref missing, ref missing);

                object fileName = (Name);

                object FileFormat = Word.WdSaveFormat.wdFormatDocument;
                template.SaveAs(ref fileName, ref FileFormat, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);
                worddocumentRez = template;

                Object begin = missing;
                Object end = missing;
                Object defaultTableBehavior = Type.Missing;
                Object autoFitBehavior = Type.Missing;

                Word.Range wordrange = worddocumentRez.Range(ref begin, ref end);

                // добавляем таблицу
                Word.Table table = worddocumentRez.Tables.Add(wordrange, Cols.Rows.Count + 1, 3, ref defaultTableBehavior, ref autoFitBehavior);

                table.Borders.Enable = 1;

                table.Range.Font.Size = 14;
                table.Range.Font.Name = "Times New Roman";

                // формируем заголовок
                table.Cell(1, 1).Range.Text = Cols.Title[0];
                table.Cell(1, 2).Range.Text = Cols.Title[1];
                table.Cell(1, 3).Range.Text = Cols.Title[2];

                // заполняем таблицу
                int i = 2;
                foreach (Description desc in Cols.Rows)
                {
                    table.Cell(i, 1).Range.Text = desc.Name;
                    table.Cell(i, 2).Range.Text = "Файл " + desc.DefinitionPath + ", строка " + desc.Line.ToString();
                    string text = "Переменная " + desc.Name + " предназначена для .";
                    if (extendedVariablesInfo)
                    {
                        if (((VariableDescription)desc).Setters.Count != 0)
                        {
                            text += "\nЗначение переменной присваивается в следующих функциях:";
                            foreach (Description setter in ((VariableDescription)desc).Setters)
                            {
                                text += "\n" + setter.Name + ".";
                            }
                        }
                        if (((VariableDescription)desc).Getters.Count != 0)
                        {
                            text += "\nЗначение переменной используется в следующих функциях:";
                            foreach (Description getter in ((VariableDescription)desc).Getters)
                            {
                                text += "\n" + getter.Name + ".";
                            }
                        }
                    }

                    table.Cell(i, 3).Range.Text = text;
                                        
                    ++i;
                }

            }
            finally
            {
                worddocumentRez.Save();

                //закрываем приложения. Если этого не делать то процессы WORD.exe остаются в памяти, что приводит к понятным последствиям при множественных запусках данной программки ;)
                ((Word._Application)wordapp1).Quit(ref SaveChanges, ref missing, ref missing);
            }
        }
    }

    /// <summary>
    /// Класс, описывающий колонки таблицы. Список Title должен содержать 3 элемента
    /// </summary>
    public class Columns
    {
        public List<string> Title = new List<string>();
        public List<Description> Rows = new List<Description>();
    }

    /// <summary>
    /// Описание сущности
    /// </summary>
    public class Description
    {
        /// <summary>
        /// имя сущности
        /// </summary>
        public string Name = "";
        
        /// <summary>
        /// файл определения
        /// </summary>
        public string DefinitionPath = "";

        /// <summary>
        /// строка определения
        /// </summary>
        public int Line = 0;

        /// <summary>
        /// строка вызова данной функции или обращения к данной переменной
        /// </summary>
        public int Called = 0;

        public Description()
        {
        }
    }

    /// <summary>
    /// описание функции
    /// </summary>
    public class FuncDescription : Description
    {   
        /// <summary>
        /// функции, вызывающие данную
        /// </summary>
        public List<Description> Callers = new List<Description>();

        public FuncDescription(string name, string definitionPath, int line)
        {
            Name = name;
            DefinitionPath = definitionPath;
            Line = line;
        }

        public FuncDescription(string name, string definitionPath, int line, int called)
        {
            Name = name;
            DefinitionPath = definitionPath;
            Line = line;
            Called = called;
        }
    }

    /// <summary>
    /// описание переменной
    /// </summary>
    public class VariableDescription : Description
    {
        /// <summary>
        /// функции, осуществляющие присвоение данной переменной
        /// </summary>
        public List<Description> Setters = new List<Description>();

        /// <summary>
        /// функции, осуществляющие использование значение данной переменной
        /// </summary>
        public List<Description> Getters = new List<Description>();

        public VariableDescription(string name, string definitionPath, int line)
        {
            Name = name;
            DefinitionPath = definitionPath;
            Line = line;
        }

        public VariableDescription(string name, string definitionPath, int line, int called)
        {
            Name = name;
            DefinitionPath = definitionPath;
            Line = line;
            Called = called;
        }
    }
}
