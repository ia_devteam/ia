using System.Collections.Generic;
using System.Windows.Forms;
using Store;
using Store.Table;
using DocExplorer;

namespace IA.Plugins.Analyses.NDV_Fish
{

    /// <summary>
    /// Класс настроек плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Шаблон для анализа связей";
        
        /// <summary>
        /// Иденитификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.NDV_FISH;

        /// <summary>
        /// выводить максимум информации о переменных 
        /// </summary>
        internal static bool extendedVariablesInfo;

    }

    public sealed class NDV_Fish : IA.Plugin.Interface
    {

        /// <summary>
        /// Текущие настройки
        /// </summary>
        CustomSettings settings; 

        /// <summary>
        /// выбранные функции
        /// </summary>
        public List<Store.IFunction> SelectedFunctions = new List<Store.IFunction>();

        /// <summary>
        /// выбранные связи по информации
        /// </summary>
        public List<Store.Variable> SelectedStaticVars = new List<Store.Variable>();

        /// <summary>
        /// выбранные информационные объекты
        /// </summary>
        public List<Store.Variable> SelectedVars = new List<Store.Variable>();

        Results resultForm;
        public Store.Storage storage;




        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.RESULT_WINDOW |
                                    Plugin.Capabilities.REPORTS |
                                        Plugin.Capabilities.RERUN |
                                        Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;
            SelectedFunctions = new List<Store.IFunction>();
            SelectedStaticVars = new List<Store.Variable>();
            SelectedVars = new List<Store.Variable>();
            PluginSettings.extendedVariablesInfo = Properties.Settings.Default.extendedVariablesInfo;

        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.extendedVariablesInfo);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            PluginSettings.extendedVariablesInfo = true;

        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            Properties.Settings.Default.extendedVariablesInfo = PluginSettings.extendedVariablesInfo;
            Properties.Settings.Default.Save();

            settingsBuffer.Add(PluginSettings.extendedVariablesInfo);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = "";
            if (settings != null)
                settings.Save();
            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="path"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string path)
        {
            System.IO.Directory.CreateDirectory(path);

            // переменные, хранящие списки выбранных пользователем сущностей
            DocTable FuncsTable = new DocTable();
            DocTable StaticVarsTable = new DocTable();
            DocTable LocalVarsTable = new DocTable();

            // строим Связи по управлению
            FuncsTable.Name = path + "\\" + "Связи по управлению.doc";
            FuncsTable.templateFile = "Common\\Normal1.dot";

            FuncsTable.Cols.Title.Add("Имя функции");
            FuncsTable.Cols.Title.Add("Место определения");
            FuncsTable.Cols.Title.Add("Описание");

            foreach (Store.IFunction func in SelectedFunctions)
            {
                Location locDef = func.Definition();

                Description descr;
                if(locDef != null)
                    descr = new FuncDescription(func.FullName, locDef.GetFile().FileNameForReports, (int)func.Definition().GetLine());
                else
                    descr = new FuncDescription(func.FullName, "Не известно", -1);


                foreach (Store.IFunctionCall caller in func.CalledBy())
                {
                    ((FuncDescription)descr).Callers.Add(new FuncDescription(caller.Caller.FullName, caller.Caller.Definition().GetFile().FileNameForReports, (int)caller.Caller.Definition().GetLine(), (int)caller.CallLocation.GetLine()));
                }
                FuncsTable.Cols.Rows.Add(descr);
            }
            FuncsTable.GenerateDocFunc();

            // строим Связи по информации
            StaticVarsTable.Name = path + "\\" + "Связи по информации.doc";
            StaticVarsTable.templateFile = "Common\\Normal1.dot";

            StaticVarsTable.Cols.Title.Add("Имя переменной");
            StaticVarsTable.Cols.Title.Add("Место определения");
            StaticVarsTable.Cols.Title.Add("Описание");

            foreach (Store.Variable var in SelectedStaticVars)
            {
                Description descr = new VariableDescription(var.FullName, var.Definitions()[0].GetFile().FileNameForReports, (int)var.Definitions()[0].GetLine());

                foreach (Store.Location setter in var.ValueSetPosition())
                {
                    try
                    {
                        ((VariableDescription)descr).Setters.Add(new FuncDescription(storage.functions.GetFunction(setter.GetFunctionId()).FullName, setter.GetFile().FileNameForReports, 0, (int)setter.GetLine()));
                    }
                    catch { }
                }

                foreach (Store.Location getter in var.ValueGetPosition())
                {
                    try
                    {
                        ((VariableDescription)descr).Getters.Add(new FuncDescription(storage.functions.GetFunction(getter.GetFunctionId()).FullName, getter.GetFile().FileNameForReports, 0, (int)getter.GetLine()));
                    }
                    catch { }
                }

                StaticVarsTable.Cols.Rows.Add(descr);
            }
            StaticVarsTable.GenerateDocVariable(PluginSettings.extendedVariablesInfo);

            // строим Связи по Информационные объекты
            LocalVarsTable.Name = path + "\\" + "Информационные объекты.doc";
            LocalVarsTable.templateFile = "Common\\Normal1.dot";

            LocalVarsTable.Cols.Title.Add("Имя переменной");
            LocalVarsTable.Cols.Title.Add("Место определения");
            LocalVarsTable.Cols.Title.Add("Описание");

            foreach (Store.Variable var in SelectedVars)
            {
                Description descr = new VariableDescription(var.FullName, var.Definitions()[0].GetFile().FileNameForReports, (int)var.Definitions()[0].GetLine());

                foreach (Store.Location setter in var.ValueSetPosition())
                {
                    try
                    {
                        ((VariableDescription)descr).Setters.Add(new FuncDescription(storage.functions.GetFunction(setter.GetFunctionId()).FullName, setter.GetFile().FileNameForReports, 0, (int)setter.GetLine()));
                    }
                    catch { }
                }

                foreach (Store.Location getter in var.ValueGetPosition())
                {
                    try
                    {
                        ((VariableDescription)descr).Getters.Add(new FuncDescription(storage.functions.GetFunction(getter.GetFunctionId()).FullName, getter.GetFile().FileNameForReports, 0, (int)getter.GetLine()));
                    }
                    catch { }
                }

                LocalVarsTable.Cols.Rows.Add(descr);
            }
            LocalVarsTable.GenerateDocVariable(PluginSettings.extendedVariablesInfo);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {                
                return settings = new CustomSettings();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return new Results(this);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        #endregion
    }
}
