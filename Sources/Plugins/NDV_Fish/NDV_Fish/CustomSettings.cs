﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.NDV_Fish
{
    public partial class CustomSettings : UserControl
    {
        public CustomSettings()
        {
            InitializeComponent();

            variablesExtendedInfoCheckBox.Checked = PluginSettings.extendedVariablesInfo;

        }

        public void Save()
        {
            PluginSettings.extendedVariablesInfo = variablesExtendedInfoCheckBox.Checked;

        }
    }
}
