﻿/*
 * Плагин Генератор шаблона для НДВ
 * Файл Results.Designer.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Лошкарёв
 * 
 * Плагин выдает пользователю списки функций, статических переменных и локальных переменных пользователю, который выбирает среди них те, которые
 * кажутся ему подозрительными. Данные объекты помещаются в таблицы, являющиеся результатами работ по НДВ. В таблицу помещаются места вызова функций, 
 * присвоений и использований переменных, места определений объектов. 
 */
namespace IA.Plugins.Analyses.NDV_Fish
{
    partial class Results
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Tabs = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.RemoveZeroFuncsButton = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.MoveSelectedFunctionsUp = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.CounterLabel = new System.Windows.Forms.Label();
            this.AllEntriesFuns = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.FunctionsListBox = new System.Windows.Forms.TreeView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.RemoveZeroStatButton = new System.Windows.Forms.Button();
            this.MoveSelectedInfLinksUp = new System.Windows.Forms.Button();
            this.StaticListBox = new System.Windows.Forms.TreeView();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.AllEntriesInfLinks = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.RemoveZeroVarsButton = new System.Windows.Forms.Button();
            this.MoveSelectedInfObjsUp = new System.Windows.Forms.Button();
            this.VariablesListBox = new System.Windows.Forms.TreeView();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.AllEntriesInf = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.Tabs.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.Tabs, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(723, 644);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // Tabs
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.Tabs, 2);
            this.Tabs.Controls.Add(this.tabPage1);
            this.Tabs.Controls.Add(this.tabPage2);
            this.Tabs.Controls.Add(this.tabPage3);
            this.Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabs.Location = new System.Drawing.Point(3, 3);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(717, 638);
            this.Tabs.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(709, 612);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Связи по управлению";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.RemoveZeroFuncsButton, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.richTextBox1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.MoveSelectedFunctionsUp, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.FunctionsListBox, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(703, 606);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // RemoveZeroFuncsButton
            // 
            this.RemoveZeroFuncsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemoveZeroFuncsButton.Location = new System.Drawing.Point(3, 576);
            this.RemoveZeroFuncsButton.Name = "RemoveZeroFuncsButton";
            this.RemoveZeroFuncsButton.Size = new System.Drawing.Size(345, 27);
            this.RemoveZeroFuncsButton.TabIndex = 9;
            this.RemoveZeroFuncsButton.Text = "Показать функции без вызовов";
            this.RemoveZeroFuncsButton.UseVisualStyleBackColor = true;
            this.RemoveZeroFuncsButton.Click += new System.EventHandler(this.RemoveZeroFuncsButton_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(354, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.tableLayoutPanel2.SetRowSpan(this.richTextBox1, 5);
            this.richTextBox1.Size = new System.Drawing.Size(346, 600);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // MoveSelectedFunctionsUp
            // 
            this.MoveSelectedFunctionsUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MoveSelectedFunctionsUp.Location = new System.Drawing.Point(3, 539);
            this.MoveSelectedFunctionsUp.Name = "MoveSelectedFunctionsUp";
            this.MoveSelectedFunctionsUp.Size = new System.Drawing.Size(345, 31);
            this.MoveSelectedFunctionsUp.TabIndex = 5;
            this.MoveSelectedFunctionsUp.Text = "Переместить выбранные наверх";
            this.MoveSelectedFunctionsUp.UseVisualStyleBackColor = true;
            this.MoveSelectedFunctionsUp.Click += new System.EventHandler(this.MoveSelectedUp_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.CounterLabel, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.AllEntriesFuns, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 519);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(345, 14);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // CounterLabel
            // 
            this.CounterLabel.AutoSize = true;
            this.CounterLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CounterLabel.Location = new System.Drawing.Point(3, 0);
            this.CounterLabel.Name = "CounterLabel";
            this.CounterLabel.Size = new System.Drawing.Size(166, 14);
            this.CounterLabel.TabIndex = 3;
            this.CounterLabel.Text = "Всего выбрано: 0";
            this.CounterLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AllEntriesFuns
            // 
            this.AllEntriesFuns.AutoSize = true;
            this.AllEntriesFuns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AllEntriesFuns.Location = new System.Drawing.Point(175, 0);
            this.AllEntriesFuns.Name = "AllEntriesFuns";
            this.AllEntriesFuns.Size = new System.Drawing.Size(167, 14);
            this.AllEntriesFuns.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 473);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(345, 40);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтр";
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 16);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(339, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // FunctionsListBox
            // 
            this.FunctionsListBox.CheckBoxes = true;
            this.FunctionsListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FunctionsListBox.Location = new System.Drawing.Point(3, 3);
            this.FunctionsListBox.Name = "FunctionsListBox";
            this.FunctionsListBox.Size = new System.Drawing.Size(345, 464);
            this.FunctionsListBox.TabIndex = 0;
            this.FunctionsListBox.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.FunctionsListBox_AfterCheck);
            this.FunctionsListBox.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.FunctionsListBox_AfterSelect);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(709, 612);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Связи по информации";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.RemoveZeroStatButton, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.MoveSelectedInfLinksUp, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.StaticListBox, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.richTextBox2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(703, 606);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // RemoveZeroStatButton
            // 
            this.RemoveZeroStatButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemoveZeroStatButton.Location = new System.Drawing.Point(3, 577);
            this.RemoveZeroStatButton.Name = "RemoveZeroStatButton";
            this.RemoveZeroStatButton.Size = new System.Drawing.Size(345, 26);
            this.RemoveZeroStatButton.TabIndex = 9;
            this.RemoveZeroStatButton.Text = "Показать переменные без вызовов";
            this.RemoveZeroStatButton.UseVisualStyleBackColor = true;
            this.RemoveZeroStatButton.Click += new System.EventHandler(this.RemoveZeroStatsButton_Click);
            // 
            // MoveSelectedInfLinksUp
            // 
            this.MoveSelectedInfLinksUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MoveSelectedInfLinksUp.Location = new System.Drawing.Point(3, 545);
            this.MoveSelectedInfLinksUp.Name = "MoveSelectedInfLinksUp";
            this.MoveSelectedInfLinksUp.Size = new System.Drawing.Size(345, 26);
            this.MoveSelectedInfLinksUp.TabIndex = 6;
            this.MoveSelectedInfLinksUp.Text = "Переместить выбранные наверх";
            this.MoveSelectedInfLinksUp.UseVisualStyleBackColor = true;
            this.MoveSelectedInfLinksUp.Click += new System.EventHandler(this.MoveSelectedInfLinksUp_Click);
            // 
            // StaticListBox
            // 
            this.StaticListBox.CheckBoxes = true;
            this.StaticListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StaticListBox.Location = new System.Drawing.Point(3, 3);
            this.StaticListBox.Name = "StaticListBox";
            this.StaticListBox.Size = new System.Drawing.Size(345, 467);
            this.StaticListBox.TabIndex = 1;
            this.StaticListBox.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.StaticListBox_AfterCheck);
            this.StaticListBox.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.StaticListBox_AfterSelect);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox2.Location = new System.Drawing.Point(354, 3);
            this.richTextBox2.Name = "richTextBox2";
            this.tableLayoutPanel3.SetRowSpan(this.richTextBox2, 5);
            this.richTextBox2.Size = new System.Drawing.Size(346, 600);
            this.richTextBox2.TabIndex = 2;
            this.richTextBox2.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 476);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(345, 43);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Фильтр";
            // 
            // textBox2
            // 
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.Location = new System.Drawing.Point(3, 16);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(339, 20);
            this.textBox2.TabIndex = 4;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.AllEntriesInfLinks, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 525);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(345, 14);
            this.tableLayoutPanel6.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 14);
            this.label1.TabIndex = 4;
            this.label1.Text = "Всего выбрано: 0";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AllEntriesInfLinks
            // 
            this.AllEntriesInfLinks.AutoSize = true;
            this.AllEntriesInfLinks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AllEntriesInfLinks.Location = new System.Drawing.Point(175, 0);
            this.AllEntriesInfLinks.Name = "AllEntriesInfLinks";
            this.AllEntriesInfLinks.Size = new System.Drawing.Size(167, 14);
            this.AllEntriesInfLinks.TabIndex = 5;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(709, 612);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Информационные объекты";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.RemoveZeroVarsButton, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.MoveSelectedInfObjsUp, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.VariablesListBox, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.richTextBox3, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupBox3, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 0, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(703, 606);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // RemoveZeroVarsButton
            // 
            this.RemoveZeroVarsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemoveZeroVarsButton.Location = new System.Drawing.Point(3, 582);
            this.RemoveZeroVarsButton.Name = "RemoveZeroVarsButton";
            this.RemoveZeroVarsButton.Size = new System.Drawing.Size(345, 21);
            this.RemoveZeroVarsButton.TabIndex = 10;
            this.RemoveZeroVarsButton.Text = "Показать переменные без вызовов";
            this.RemoveZeroVarsButton.UseVisualStyleBackColor = true;
            this.RemoveZeroVarsButton.Click += new System.EventHandler(this.RemoveZeroVarsButton_Click);
            // 
            // MoveSelectedInfObjsUp
            // 
            this.MoveSelectedInfObjsUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MoveSelectedInfObjsUp.Location = new System.Drawing.Point(3, 553);
            this.MoveSelectedInfObjsUp.Name = "MoveSelectedInfObjsUp";
            this.MoveSelectedInfObjsUp.Size = new System.Drawing.Size(345, 23);
            this.MoveSelectedInfObjsUp.TabIndex = 7;
            this.MoveSelectedInfObjsUp.Text = "Переместить выбранные наверх";
            this.MoveSelectedInfObjsUp.UseVisualStyleBackColor = true;
            this.MoveSelectedInfObjsUp.Click += new System.EventHandler(this.MoveSelectedInfObjsUp_Click);
            // 
            // VariablesListBox
            // 
            this.VariablesListBox.CheckBoxes = true;
            this.VariablesListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VariablesListBox.Location = new System.Drawing.Point(3, 3);
            this.VariablesListBox.Name = "VariablesListBox";
            this.VariablesListBox.Size = new System.Drawing.Size(345, 472);
            this.VariablesListBox.TabIndex = 1;
            this.VariablesListBox.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.VariablesListBox_AfterCheck);
            this.VariablesListBox.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.VariablesListBox_AfterSelect);
            // 
            // richTextBox3
            // 
            this.richTextBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox3.Location = new System.Drawing.Point(354, 3);
            this.richTextBox3.Name = "richTextBox3";
            this.tableLayoutPanel4.SetRowSpan(this.richTextBox3, 5);
            this.richTextBox3.Size = new System.Drawing.Size(346, 600);
            this.richTextBox3.TabIndex = 2;
            this.richTextBox3.Text = "";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 481);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(345, 41);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Фильтр";
            // 
            // textBox3
            // 
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox3.Location = new System.Drawing.Point(3, 16);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(339, 20);
            this.textBox3.TabIndex = 4;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.AllEntriesInf, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 528);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(345, 19);
            this.tableLayoutPanel7.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 19);
            this.label2.TabIndex = 5;
            this.label2.Text = "Всего выбрано: 0";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AllEntriesInf
            // 
            this.AllEntriesInf.AutoSize = true;
            this.AllEntriesInf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AllEntriesInf.Location = new System.Drawing.Point(175, 0);
            this.AllEntriesInf.Name = "AllEntriesInf";
            this.AllEntriesInf.Size = new System.Drawing.Size(167, 19);
            this.AllEntriesInf.TabIndex = 6;
            // 
            // Results
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Results";
            this.Size = new System.Drawing.Size(723, 644);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.Tabs.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TreeView FunctionsListBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TreeView StaticListBox;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TreeView VariablesListBox;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label CounterLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label AllEntriesFuns;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label AllEntriesInfLinks;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label AllEntriesInf;
        private System.Windows.Forms.Button MoveSelectedFunctionsUp;
        private System.Windows.Forms.Button MoveSelectedInfLinksUp;
        private System.Windows.Forms.Button MoveSelectedInfObjsUp;
        private System.Windows.Forms.Button RemoveZeroStatButton;
        private System.Windows.Forms.Button RemoveZeroFuncsButton;
        private System.Windows.Forms.Button RemoveZeroVarsButton;
    }
}
