﻿/*
 * Плагин Генератор шаблона для НДВ
 * Файл Results.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Лошкарёв
 * 
 * Плагин выдает пользователю списки функций, статических переменных и локальных переменных пользователю, который выбирает среди них те, которые
 * кажутся ему подозрительными. Данные объекты помещаются в таблицы, являющиеся результатами работ по НДВ. В таблицу помещаются места вызова функций, 
 * присвоений и использований переменных, места определений объектов. 
 */

using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using FileOperations;

namespace IA.Plugins.Analyses.NDV_Fish
{
    public partial class Results : UserControl
    {
        int counter1 = 0;
        int counter2 = 0;
        int counter3 = 0;

        /// <summary>
        /// плагин, вызвавший окно результатов
        /// </summary>
        NDV_Fish plugin;

        class FuncContents
        {
            public Store.IFunction function
            {
                get;
                set;
            }
            public TreeNode contents
            {
                get;
                set;
            }

            public FuncContents(Store.IFunction function, TreeNode node)
            {
                this.function = function;
                this.contents = node;
            }
        }

        class VarContents
        {
            public Store.Variable variable
            {
                get;
                set;
            }
            public TreeNode contents
            {
                get;
                set;
            }

            public VarContents(Store.Variable variable, TreeNode node)
            {
                this.variable = variable;
                this.contents = node;
            }
        }
        
        /// <summary>
        ///  Формирование формы, на которой пользователи выбирают подозрительные объекты
        /// </summary>
        /// <param name="plugin"></param>
        public Results(NDV_Fish plugin)
        {
            InitializeComponent();
            this.plugin = plugin;

            // вывод связей по управлению для дальнейшего анализа

            LoadFuncs(true);

            // вывод связей по информации для дальнейшего анализа
            LoadStatic(true);

            LoadVars(true);

            // выставление отметок для потенциально подозрительных по имени объеков
            StreamReader sr = new StreamReader("Plugins\\Keywords.txt");
            if (sr.ReadLine() != "1")
            {
                sr.Close();
                return;
            }
            string line;
            while ((line = sr.ReadLine()) != "2")
            {
                line = line.Substring(1);
                foreach (TreeNode node in FunctionsListBox.Nodes)
                {
                    if (((FuncContents)node.Tag).function.Name.ToLower().Contains(line))
                    {
                        node.Checked = true;
                    }
                }
            }
            while ((line = sr.ReadLine()) != "3")
            {
                line = line.Substring(1);
                foreach (TreeNode node in StaticListBox.Nodes)
                {
                    if (((VarContents)node.Tag).variable.Name.ToLower().Contains(line))
                    {
                        node.Checked = true;
                    }
                }
            }
            while ((line = sr.ReadLine()) != null)
            {
                line = line.Substring(1);
                foreach (TreeNode node in VariablesListBox.Nodes)
                {
                    if (((VarContents)node.Tag).variable.Name.ToLower().Contains(line))
                    {
                        node.Checked = true;
                    }
                }
            }
            sr.Close();
        }

        TreeNode prevNode2 = null;
        private void StaticListBox_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 0)
            {
                if (prevNode2 != null)
                {
                    prevNode2.Nodes.Clear();
                }

                prevNode2 = e.Node;

                // попали по имени функции
                AbstractReader reader = null;
                try
                {
                    reader = new AbstractReader(((VarContents)e.Node.Tag).variable.Definitions()[0].GetFile().FullFileName_Original);
                }
                catch (Exception ex)
                {
                    Monitor.Log.Error(Name, "Не удалось открыть файл " + ((VarContents)e.Node.Tag).variable.Definitions()[0].GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                }
                
                richTextBox2.Text = reader.getNormalizedText();
                int index = richTextBox2.Lines[(int)((VarContents)e.Node.Tag).variable.Definitions()[0].GetLine() - 1].IndexOf(((VarContents)e.Node.Tag).variable.Name.Substring(((VarContents)e.Node.Tag).variable.Name.LastIndexOf('.') + 1));
                richTextBox2.Select((int)((VarContents)e.Node.Tag).variable.Definitions()[0].GetOffset() + index, ((VarContents)e.Node.Tag).variable.Name.Length - ((VarContents)e.Node.Tag).variable.Name.LastIndexOf('.') - 1);

                richTextBox2.SelectionBackColor = Color.Black;
                richTextBox2.SelectionColor = Results.DefaultBackColor;
                richTextBox2.ScrollToCaret();

                int i = StaticListBox.Nodes.IndexOf(e.Node);
                foreach (TreeNode nd in ((VarContents)e.Node.Tag).contents.Nodes)
                {
                    StaticListBox.Nodes[i].Nodes.Add(nd);
                }
                StaticListBox.Nodes[i].ExpandAll();
            }
            if (e.Node.Level == 2)
            {
                if (e.Node.Parent.Text == "Определения" || e.Node.Parent.Text == "Декларации")
                {
                    AbstractReader reader = null;
                    try
                    {
                        reader = new AbstractReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original);
                    }
                    catch (Exception ex)
                    {
                        Monitor.Log.Error(Name, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                    }

                    richTextBox2.Text = reader.getNormalizedText();
                    int index = richTextBox2.Lines[(int)((Store.Location)e.Node.Tag).GetLine() - 1].IndexOf(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Substring(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') + 1));
                    richTextBox2.Select((int)((Store.Location)e.Node.Tag).GetOffset() + index, ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Length - ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') - 1);

                    richTextBox2.SelectionBackColor = Color.Black;
                    richTextBox2.SelectionColor = Results.DefaultBackColor;
                    richTextBox2.ScrollToCaret();
                }

                if (e.Node.Parent.Text == "Присвоения")
                {
                    AbstractReader reader = null;
                    try
                    {
                        reader = new AbstractReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original);
                    }
                    catch (Exception ex)
                    {
                        Monitor.Log.Error(Name, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                    }

                    richTextBox2.Text = reader.getNormalizedText();
                    int index = richTextBox2.Lines[(int)((Store.Location)e.Node.Tag).GetLine() - 1].IndexOf(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Substring(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') + 1));
                    richTextBox2.Select((int)((Store.Location)e.Node.Tag).GetOffset() + index, ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Length - ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') - 1);

                    richTextBox2.SelectionBackColor = Color.Black;
                    richTextBox2.SelectionColor = Results.DefaultBackColor;
                    richTextBox2.ScrollToCaret();
                }

                if (e.Node.Parent.Text == "Использования")
                {
                    AbstractReader reader = null;
                    try
                    {
                        reader = new AbstractReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original);
                    }
                    catch (Exception ex)
                    {
                        Monitor.Log.Error(Name, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                    }

                    richTextBox2.Text = reader.getNormalizedText();
                    int index = richTextBox2.Lines[(int)((Store.Location)e.Node.Tag).GetLine() - 1].IndexOf(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Substring(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') + 1));
                    richTextBox2.Select((int)((Store.Location)e.Node.Tag).GetOffset() + index, ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Length - ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') - 1);

                    richTextBox2.SelectionBackColor = Color.Black;
                    richTextBox2.SelectionColor = Results.DefaultBackColor;
                    richTextBox2.ScrollToCaret();
                }
            }
        }

        TreeNode prevNode1 = null;

        private void FunctionsListBox_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 0)
            {
                if (prevNode1 != null)
                {
                    prevNode1.Nodes.Clear();
                } 
                
                prevNode1 = e.Node;

                AbstractReader reader = null;
                try
                {
                    reader = new AbstractReader(((FuncContents)e.Node.Tag).function.Definition().GetFile().FullFileName_Original);
                }
                catch (Exception ex)
                {
                    Monitor.Log.Error(Name, "Не удалось открыть файл " + ((FuncContents)e.Node.Tag).function.Definition().GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                }

                richTextBox1.Text = reader.getNormalizedText();
                richTextBox1.Select((int)((FuncContents)e.Node.Tag).function.Definition().GetOffset(), ((FuncContents)e.Node.Tag).function.Name.Length);

                richTextBox1.SelectionBackColor = Color.Black;
                richTextBox1.SelectionColor = Results.DefaultBackColor;
                richTextBox1.ScrollToCaret();

                int i = FunctionsListBox.Nodes.IndexOf(e.Node);
                foreach (TreeNode nd in ((FuncContents)e.Node.Tag).contents.Nodes)
                {
                    FunctionsListBox.Nodes[i].Nodes.Add(nd);
                }
                FunctionsListBox.Nodes[i].ExpandAll();
            }

            if (e.Node.Level == 2)
            {
                if (e.Node.Parent.Text == "Определения")
                {
                    AbstractReader reader = null;
                    try
                    {
                        reader = new AbstractReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original);
                    }
                    catch (Exception ex)
                    {
                        Monitor.Log.Error(Name, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                    }

                    richTextBox1.Text = reader.getNormalizedText();
                    richTextBox1.Select((int)((Store.Location)e.Node.Tag).GetOffset(), ((FuncContents)e.Node.Parent.Parent.Tag).function.Name.Length);

                    richTextBox1.SelectionBackColor = Color.Black;
                    richTextBox1.SelectionColor = Results.DefaultBackColor;
                    richTextBox1.ScrollToCaret();
                }

                if (e.Node.Parent.Text == "Вызовы")
                {
                    AbstractReader reader = null;
                    try
                    {
                        reader = new AbstractReader(((Store.IFunction)e.Node.Tag).Definition().GetFile().FullFileName_Original);
                    }
                    catch (Exception ex)
                    {
                        Monitor.Log.Error(Name, "Не удалось открыть файл " + ((Store.IFunction)e.Node.Tag).Definition().GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                    }

                    richTextBox1.Text = reader.getNormalizedText();
                    richTextBox1.Select((int)((Store.IFunction)e.Node.Tag).Definition().GetOffset(), ((Store.IFunction)e.Node.Tag).Name.Length);

                    richTextBox1.SelectionBackColor = Color.Black;
                    richTextBox1.SelectionColor = Results.DefaultBackColor;
                    richTextBox1.ScrollToCaret();
                }
            }       
        }


        TreeNode prevNode3 = null;

        private void VariablesListBox_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 0)
            {
                // попали по имени функции
                if (prevNode1 != null)
                {
                    prevNode1.Nodes.Clear();
                }

                prevNode1 = e.Node;

                AbstractReader reader = null;
                try
                {
                    reader = new AbstractReader(((VarContents)e.Node.Tag).variable.Definitions()[0].GetFile().FullFileName_Original);
                }
                catch (Exception ex)
                {
                    Monitor.Log.Error(Name, "Не удалось открыть файл " + ((VarContents)e.Node.Tag).variable.Definitions()[0].GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                }

                richTextBox3.Text = reader.getNormalizedText();
                int index = richTextBox3.Lines[(int)((VarContents)e.Node.Tag).variable.Definitions()[0].GetLine() - 1].IndexOf(((VarContents)e.Node.Tag).variable.Name.Substring(((VarContents)e.Node.Tag).variable.Name.LastIndexOf('.') + 1));
                richTextBox3.Select((int)((VarContents)e.Node.Tag).variable.Definitions()[0].GetOffset() + index, ((VarContents)e.Node.Tag).variable.Name.Length - ((VarContents)e.Node.Tag).variable.Name.LastIndexOf('.') - 1);

                richTextBox3.SelectionBackColor = Color.Black;
                richTextBox3.SelectionColor = Results.DefaultBackColor;
                richTextBox3.ScrollToCaret();

                int i = VariablesListBox.Nodes.IndexOf(e.Node);
                foreach (TreeNode nd in ((VarContents)e.Node.Tag).contents.Nodes)
                {
                    VariablesListBox.Nodes[i].Nodes.Add(nd);
                }
                VariablesListBox.Nodes[i].ExpandAll();
            }
            if (e.Node.Level == 2)
            {
                if (e.Node.Parent.Text == "Определения" || e.Node.Parent.Text == "Декларации")
                {
                    AbstractReader reader = null;
                    try
                    {
                        reader = new AbstractReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original);
                    }
                    catch (Exception ex)
                    {
                        Monitor.Log.Error(Name, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                    }

                    richTextBox3.Text = reader.getNormalizedText();
                    int index = richTextBox3.Lines[(int)((Store.Location)e.Node.Tag).GetLine() - 1].IndexOf(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Substring(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') + 1));
                    richTextBox3.Select((int)((Store.Location)e.Node.Tag).GetOffset() + index, ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Length - ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') - 1);

                    richTextBox3.SelectionBackColor = Color.Black;
                    richTextBox3.SelectionColor = Results.DefaultBackColor;
                    richTextBox3.ScrollToCaret();
                }

                if (e.Node.Parent.Text == "Присвоения")
                {
                    AbstractReader reader = null;
                    try
                    {
                        reader = new AbstractReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original);
                    }
                    catch (Exception ex)
                    {
                        Monitor.Log.Error(Name, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                    }

                    richTextBox3.Text = reader.getNormalizedText();
                    int index = richTextBox3.Lines[(int)((Store.Location)e.Node.Tag).GetLine() - 1].IndexOf(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Substring(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') + 1));
                    richTextBox3.Select((int)((Store.Location)e.Node.Tag).GetOffset() + index, ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Length - ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') - 1);

                    richTextBox3.SelectionBackColor = Color.Black;
                    richTextBox3.SelectionColor = Results.DefaultBackColor;
                    richTextBox3.ScrollToCaret();
                }

                if (e.Node.Parent.Text == "Использования")
                {
                    AbstractReader reader = null;
                    try
                    {
                        reader = new AbstractReader(((Store.Location)e.Node.Tag).GetFile().FullFileName_Original);
                    }
                    catch (Exception ex)
                    {
                        Monitor.Log.Error(Name, "Не удалось открыть файл " + ((Store.Location)e.Node.Tag).GetFile().FullFileName_Original + ". Ошибка " + ex.Message);
                    }

                    richTextBox3.Text = reader.getNormalizedText();
                    int index = richTextBox3.Lines[(int)((Store.Location)e.Node.Tag).GetLine() - 1].IndexOf(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Substring(((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') + 1));
                    richTextBox3.Select((int)((Store.Location)e.Node.Tag).GetOffset() + index, ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.Length - ((VarContents)e.Node.Parent.Parent.Tag).variable.Name.LastIndexOf('.') - 1);

                    richTextBox3.SelectionBackColor = Color.Black;
                    richTextBox3.SelectionColor = Results.DefaultBackColor;
                    richTextBox3.ScrollToCaret();
                }
            }
        }

        private void FunctionsListBox_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 0)
            {
                if (e.Node.Checked && !plugin.SelectedFunctions.Contains(((FuncContents)e.Node.Tag).function))
                    plugin.SelectedFunctions.Add(((FuncContents)e.Node.Tag).function);
                else if (!e.Node.Checked)
                    plugin.SelectedFunctions.Remove(((FuncContents)e.Node.Tag).function);


                if (e.Node.Checked)
                {
                    ++counter1;
                }
                else
                {
                    --counter1;
                }
                CounterLabel.Text = "Всего выбрано: " + counter1;
            }

        }

        private void StaticListBox_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 0)
            {
                if (e.Node.Checked && !plugin.SelectedStaticVars.Contains(((VarContents)e.Node.Tag).variable))
                    plugin.SelectedStaticVars.Add(((VarContents)e.Node.Tag).variable);
                else if (!e.Node.Checked)
                    plugin.SelectedStaticVars.Remove(((VarContents)e.Node.Tag).variable);


                if (e.Node.Checked)
                {
                    ++counter2;
                }
                else
                {
                    --counter2;
                }
                label1.Text = "Всего выбрано: " + counter2;
            }
        }

        private void VariablesListBox_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 0)
            {
                if (e.Node.Checked && !plugin.SelectedVars.Contains(((VarContents)e.Node.Tag).variable))
                    plugin.SelectedVars.Add(((VarContents)e.Node.Tag).variable);
                else if (!e.Node.Checked)
                    plugin.SelectedVars.Remove(((VarContents)e.Node.Tag).variable);


                if (e.Node.Checked)
                {
                    ++counter3;
                }
                else
                {
                    --counter3;
                }
                label2.Text = "Всего выбрано: " + counter3;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            LoadFuncs(hideZeroFuncs, textBox1.Text);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            LoadStatic(hideZeroStats, textBox2.Text);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            LoadVars(hideZeroVars, textBox3.Text);
        }

        private void MoveSelectedUp_Click(object sender, EventArgs e)
        {
            TreeNodeCollection tncollection = FunctionsListBox.Nodes;
            foreach (TreeNode node in tncollection)
            {
                if (node.Checked)
                {
                    TreeNode nd = node;
                    FunctionsListBox.Nodes.Remove(node);
                    FunctionsListBox.Nodes.Insert(0, nd);
                }
            }
        }

        private void MoveSelectedInfLinksUp_Click(object sender, EventArgs e)
        {
            TreeNodeCollection tncollection = StaticListBox.Nodes;
            foreach (TreeNode node in tncollection)
            {
                if (node.Checked)
                {
                    TreeNode nd = node;
                    StaticListBox.Nodes.Remove(node);
                    StaticListBox.Nodes.Insert(0, nd);
                }
            }
        }

        private void MoveSelectedInfObjsUp_Click(object sender, EventArgs e)
        {
            TreeNodeCollection tncollection = VariablesListBox.Nodes;
            foreach (TreeNode node in tncollection)
            {
                if (node.Checked)
                {
                    TreeNode nd = node;
                    VariablesListBox.Nodes.Remove(node);
                    VariablesListBox.Nodes.Insert(0, nd);
                }
            }
        }

        void LoadFuncs(bool hideZeroFuncs, string part = "")
        {
            int i = 0;
            FunctionsListBox.Nodes.Clear();
            foreach (Store.IFunction function in plugin.storage.functions.EnumerateFunctions(Store.enFunctionKind.REAL))
            {
                if (function.FullName != "" && function.Definition() != null && !function.FullName.EndsWith("get") && !function.FullName.EndsWith("set")
                    && (function.CalledBy().Count() != 0 || !hideZeroFuncs) && (part == "" || function.FullName.ToLower().Contains(part.ToLower())))
                {
                    ++i;
                    TreeNode nodeFunc = new TreeNode();
                    TreeNode node = new TreeNode();

                    nodeFunc.Text = node.Text = function.FullName + " (" + function.CalledBy().ToArray<Store.IFunctionCall>().Length.ToString() + " вызовов)";
                    TreeNode CatNode = new TreeNode();
                    CatNode.Text = "Определения";
                    TreeNode childNode = new TreeNode();
                    childNode.Tag = function.Definition();
                    childNode.Text = "Define:" + function.Definition().GetFile().FileNameForReports + ", " + function.Definition().GetLine().ToString();
                    CatNode.Nodes.Add(childNode);
                    CatNode.Expand();
                    node.Nodes.Add(CatNode);

                    CatNode = new TreeNode();
                    CatNode.Text = "Декларации";
                    foreach (Store.Location loc in function.Declarations())
                    {
                        childNode = new TreeNode();
                        childNode.Tag = loc;
                        childNode.Text = "Declare:" + loc.GetFile().FileNameForReports + ", " + loc.GetLine().ToString();
                        CatNode.Nodes.Add(childNode);
                    }
                    CatNode.Expand();
                    node.Nodes.Add(CatNode);

                    CatNode = new TreeNode();
                    CatNode.Text = "Вызовы";
                    foreach (Store.IFunctionCall caller in function.CalledBy())
                    {
                        childNode = new TreeNode();
                        childNode.Tag = caller.Caller;
                        childNode.Text = "Call:" + caller.Caller.Definition().GetFile().FileNameForReports + ", " + caller.Caller.Definition().GetLine().ToString();
                        CatNode.Nodes.Add(childNode);
                    }
                    CatNode.Expand();
                    node.Nodes.Add(CatNode);

                    node.Tag = function;
                    FuncContents fc = new FuncContents(function, node);
                    nodeFunc.Tag = fc;
                    if (plugin.SelectedFunctions.Contains(function))
                        nodeFunc.Checked = true;
                    FunctionsListBox.Nodes.Add(nodeFunc);
                }
            }
            AllEntriesFuns.Text = "Всего: " + i.ToString() + " функций";
        }

        
        void LoadStatic(bool hideZeroVars, string part = "")
        {
            StaticListBox.Nodes.Clear();
            int i = 0;
            foreach (Store.Variable variable in plugin.storage.variables.EnumerateVariables())
            {
                if (variable.FullName != "" && variable.isGlobal && variable.Definitions().Length != 0 && 
                    (!hideZeroVars || variable.ValueGetPosition().Count() + variable.ValueSetPosition().Count() + variable.ValueCallPosition().Count() != 0)
                     && (part == "" || variable.FullName.ToLower().Contains(part.ToLower())))
                {
                    TreeNode node = new TreeNode();
                    TreeNode varNode = new TreeNode();
                    node.Text = variable.FullName;
                    TreeNode CatNode = new TreeNode();
                    CatNode.Text = "Определения";
                    foreach (Store.Location loc in variable.Definitions())
                    {
                        TreeNode childNode = new TreeNode();
                        childNode.Tag = loc;
                        childNode.Text = "Define:" + loc.GetFile().FileNameForReports + ", " + loc.GetLine().ToString();
                        CatNode.Nodes.Add(childNode);
                    }
                    CatNode.Expand();
                    varNode.Nodes.Add(CatNode);

                    CatNode = new TreeNode();
                    CatNode.Text = "Присвоения";
                    foreach (Store.Location loc in variable.ValueSetPosition())
                    {
                        TreeNode childNode = new TreeNode();
                        childNode.Tag = loc;
                        childNode.Text = "Set:" + loc.GetFile().FileNameForReports + ", " + loc.GetLine().ToString();
                        CatNode.Nodes.Add(childNode);
                    }
                    CatNode.Expand();
                    varNode.Nodes.Add(CatNode);

                    CatNode = new TreeNode();
                    CatNode.Text = "Использования";
                    foreach (Store.Location loc in variable.ValueGetPosition())
                    {
                        TreeNode childNode = new TreeNode();
                        childNode.Tag = loc;
                        childNode.Text = "Get:" + loc.GetFile().FileNameForReports + ", " + loc.GetLine().ToString();
                        CatNode.Nodes.Add(childNode);
                    }
                    CatNode.Expand();
                    varNode.Nodes.Add(CatNode);

                    node.Tag = new VarContents(variable, varNode);
                    if (plugin.SelectedStaticVars.Contains(variable))
                        node.Checked = true;
                    StaticListBox.Nodes.Add(node);
                    ++i;
                }
            }
            AllEntriesInfLinks.Text = "Всего: " + i.ToString() + " связей";
        }

        private void LoadVars(bool hideZeroVars, string part = "")
        {
            int i = 0;
            VariablesListBox.Nodes.Clear();
            foreach (Store.Variable variable in plugin.storage.variables.EnumerateVariables())
            {
                if (variable.FullName == "")
                    continue;

                if (!variable.isGlobal && (!hideZeroVars || variable.ValueGetPosition().Count() + variable.ValueSetPosition().Count() + variable.ValueCallPosition().Count() != 0)
                     && (part == "" || variable.FullName.ToLower().Contains(part.ToLower())))
                {
                    // вывод информационных объектов для дальнейшего анализа
                    if (variable.FullName != "" && variable.Definitions().Length != 0)
                    {
                        TreeNode node = new TreeNode();
                        TreeNode varNode = new TreeNode();
                        node.Text = variable.FullName;
                        TreeNode CatNode = new TreeNode();
                        CatNode.Text = "Определения";
                        foreach (Store.Location loc in variable.Definitions())
                        {
                            TreeNode childNode = new TreeNode();
                            childNode.Tag = loc;
                            childNode.Text = "Define:" + loc.GetFile().FileNameForReports + ", " + loc.GetLine().ToString();
                            CatNode.Nodes.Add(childNode);
                        }
                        CatNode.Expand();
                        varNode.Nodes.Add(CatNode);

                        CatNode = new TreeNode();
                        CatNode.Text = "Присвоения";
                        foreach (Store.Location loc in variable.ValueSetPosition())
                        {
                            TreeNode childNode = new TreeNode();
                            childNode.Tag = loc;
                            childNode.Text = "Set:" + loc.GetFile().FileNameForReports + ", " + loc.GetLine().ToString();
                            CatNode.Nodes.Add(childNode);
                        }
                        CatNode.Expand();
                        varNode.Nodes.Add(CatNode);

                        CatNode = new TreeNode();
                        CatNode.Text = "Использования";
                        foreach (Store.Location loc in variable.ValueGetPosition())
                        {
                            TreeNode childNode = new TreeNode();
                            childNode.Tag = loc;
                            childNode.Text = "Get:" + loc.GetFile().FileNameForReports + ", " + loc.GetLine().ToString();
                            CatNode.Nodes.Add(childNode);
                        }
                        CatNode.Expand();
                        varNode.Nodes.Add(CatNode);

                        node.Tag = new VarContents(variable, varNode);
                        if (plugin.SelectedVars.Contains(variable))
                            node.Checked = true;
                        VariablesListBox.Nodes.Add(node);
                    }
                }
                ++i;
            }
            AllEntriesInf.Text = "Всего: " + i.ToString() + " переменных.";
        }

        bool hideZeroFuncs = true;
        private void RemoveZeroFuncsButton_Click(object sender, EventArgs e)
        {
            hideZeroFuncs = !hideZeroFuncs;

            if (!hideZeroFuncs)
                RemoveZeroFuncsButton.Text = "Скрыть функции без вызовов";
            else
                RemoveZeroFuncsButton.Text = "Показать функции без вызовов";
            LoadFuncs(hideZeroFuncs);            
        }
        
        bool hideZeroStats = true;
        private void RemoveZeroStatsButton_Click(object sender, System.EventArgs e)
        {
            hideZeroStats = !hideZeroStats;

            if (!hideZeroStats)
                RemoveZeroStatButton.Text = "Скрыть переменные без вызовов";
            else
                RemoveZeroStatButton.Text = "Показать переменные без вызовов";
            LoadStatic(hideZeroStats);            
        }

        bool hideZeroVars = true;
        private void RemoveZeroVarsButton_Click(object sender, EventArgs e)
        {
            hideZeroVars = !hideZeroVars;

            if (!hideZeroVars)
                RemoveZeroVarsButton.Text = "Скрыть переменные без вызовов";
            else
                RemoveZeroVarsButton.Text = "Показать переменные без вызовов";
            LoadVars(hideZeroVars);
        }
    }
}
