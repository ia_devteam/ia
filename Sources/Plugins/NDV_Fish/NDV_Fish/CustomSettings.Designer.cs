﻿namespace IA.Plugins.Analyses.NDV_Fish
{
    partial class CustomSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.variablesExtendedInfoCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // variablesExtendedInfoCheckBox
            // 
            this.variablesExtendedInfoCheckBox.AutoSize = true;
            this.variablesExtendedInfoCheckBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.variablesExtendedInfoCheckBox.Location = new System.Drawing.Point(0, 0);
            this.variablesExtendedInfoCheckBox.Name = "variablesExtendedInfoCheckBox";
            this.variablesExtendedInfoCheckBox.Size = new System.Drawing.Size(528, 17);
            this.variablesExtendedInfoCheckBox.TabIndex = 7;
            this.variablesExtendedInfoCheckBox.Text = "Выводить всю информацию о переменных";
            this.variablesExtendedInfoCheckBox.UseVisualStyleBackColor = true;
            // 
            // CustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.variablesExtendedInfoCheckBox);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(528, 222);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox variablesExtendedInfoCheckBox;
    }
}
