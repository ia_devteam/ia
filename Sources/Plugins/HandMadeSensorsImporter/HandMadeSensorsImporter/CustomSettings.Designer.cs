﻿namespace IA.Plugins.Importers.HandMadeSensorsImporter
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sensorsFiles_remove_button = new System.Windows.Forms.Button();
            this.sensorsFiles_listBox = new System.Windows.Forms.ListBox();
            this.sensorsFiles_add_button = new System.Windows.Forms.Button();
            this.sensorsFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.sensorsFiles_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 2;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.sensorsFiles_remove_button, 1, 1);
            this.settings_tableLayoutPanel.Controls.Add(this.sensorsFiles_listBox, 0, 0);
            this.settings_tableLayoutPanel.Controls.Add(this.sensorsFiles_add_button, 1, 0);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 3;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(565, 217);
            this.settings_tableLayoutPanel.TabIndex = 4;
            // 
            // sensorsFiles_remove_button
            // 
            this.sensorsFiles_remove_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorsFiles_remove_button.Location = new System.Drawing.Point(468, 48);
            this.sensorsFiles_remove_button.Name = "sensorsFiles_remove_button";
            this.sensorsFiles_remove_button.Size = new System.Drawing.Size(94, 39);
            this.sensorsFiles_remove_button.TabIndex = 2;
            this.sensorsFiles_remove_button.Text = "Удалить файл";
            this.sensorsFiles_remove_button.UseVisualStyleBackColor = true;
            this.sensorsFiles_remove_button.Click += new System.EventHandler(this.sensorsFiles_remove_button_Click);
            // 
            // sensorsFiles_listBox
            // 
            this.sensorsFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorsFiles_listBox.FormattingEnabled = true;
            this.sensorsFiles_listBox.Location = new System.Drawing.Point(3, 3);
            this.sensorsFiles_listBox.Name = "sensorsFiles_listBox";
            this.settings_tableLayoutPanel.SetRowSpan(this.sensorsFiles_listBox, 3);
            this.sensorsFiles_listBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.sensorsFiles_listBox.Size = new System.Drawing.Size(459, 211);
            this.sensorsFiles_listBox.TabIndex = 0;
            this.sensorsFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.sensorsFiles_listBox_SelectedIndexChanged);
            // 
            // sensorsFiles_add_button
            // 
            this.sensorsFiles_add_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorsFiles_add_button.Location = new System.Drawing.Point(468, 3);
            this.sensorsFiles_add_button.Name = "sensorsFiles_add_button";
            this.sensorsFiles_add_button.Size = new System.Drawing.Size(94, 39);
            this.sensorsFiles_add_button.TabIndex = 1;
            this.sensorsFiles_add_button.Text = "Добавить файл";
            this.sensorsFiles_add_button.UseVisualStyleBackColor = true;
            this.sensorsFiles_add_button.Click += new System.EventHandler(this.sensorsFiles_add_button_Click);
            // 
            // sensorsFiles_groupBox
            // 
            this.sensorsFiles_groupBox.Controls.Add(this.settings_tableLayoutPanel);
            this.sensorsFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorsFiles_groupBox.Location = new System.Drawing.Point(0, 0);
            this.sensorsFiles_groupBox.Name = "sensorsFiles_groupBox";
            this.sensorsFiles_groupBox.Size = new System.Drawing.Size(571, 236);
            this.sensorsFiles_groupBox.TabIndex = 5;
            this.sensorsFiles_groupBox.TabStop = false;
            this.sensorsFiles_groupBox.Text = "Список файлов с датчиками";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sensorsFiles_groupBox);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(571, 236);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.sensorsFiles_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.Button sensorsFiles_add_button;
        private System.Windows.Forms.Button sensorsFiles_remove_button;
        private System.Windows.Forms.ListBox sensorsFiles_listBox;
        private System.Windows.Forms.GroupBox sensorsFiles_groupBox;
    }
}
