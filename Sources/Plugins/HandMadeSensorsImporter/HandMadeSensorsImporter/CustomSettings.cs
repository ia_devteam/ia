﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace IA.Plugins.Importers.HandMadeSensorsImporter
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        internal Settings()
        {
            InitializeComponent();

            // Изначальная доступность кнопок
            sensorsFiles_add_button.Enabled = true;
            sensorsFiles_remove_button.Enabled = false;

            sensorsFiles_listBox.Items.AddRange(PluginSettings.SensorsFiles.ToArray());
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.SensorsFiles = sensorsFiles_listBox.Items.Cast<string>().ToList();
        }

        /// <summary>
        /// Кнопка добавления файла в список
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sensorsFiles_add_button_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog() { RestoreDirectory = true })
            {
                if (dialog.ShowDialog() != DialogResult.OK)
                    return;

                if (sensorsFiles_listBox.Items.Contains(dialog.FileName))
                    return;

                sensorsFiles_listBox.Items.Add(dialog.FileName);
            }
        }

        /// <summary>
        /// Кнопка удаления файлов из списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sensorsFiles_remove_button_Click(object sender, EventArgs e)
        {
            if (sensorsFiles_listBox.SelectedItems == null || sensorsFiles_listBox.SelectedItems.Count == 0)
                return;

            foreach (object item in new List<object>(sensorsFiles_listBox.SelectedItems.Cast<object>()))
                sensorsFiles_listBox.Items.Remove(item);
        }

        /// <summary>
        /// Обработчик - выбрали файл в списке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sensorsFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            sensorsFiles_remove_button.Enabled = sensorsFiles_listBox.SelectedItems != null && sensorsFiles_listBox.SelectedItems.Count > 0;
        }
    }
}
