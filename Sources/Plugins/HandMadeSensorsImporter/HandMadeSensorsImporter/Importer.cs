﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Store;

namespace IA.Plugins.Importers.HandMadeSensorsImporter
{
    class Importer
    {
        /// <summary>
        /// Список файлов с датчиками
        /// </summary>
        List<string> sensorListFileName;

        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="sensorListFileName">Список файлов с датчиками</param>
        /// <param name="storage">Хранилище</param>
        internal Importer(List<string> sensorListFileName, Storage storage)
        {
            this.storage = storage;
            this.sensorListFileName = sensorListFileName;
        }

        /// <summary>
        /// Импорт датчиков
        /// </summary>
        internal void Import()
        {
            try
            {
                // Достаем имеющие датчики из Хранилища
                Sensors storeSensors = storage.sensors;

                // Количество вставленных датчиков
                ulong insertedSensorsCount = 0;
                
                foreach (string fileName in sensorListFileName)
                {
                    // Строка в файле с датчиками
                    string line;
                    
                    // Номер строки в файле с датчиками
                    ulong count = 0;

                    using (StreamReader reader = new StreamReader(fileName))
                    {
                        // Формат следующий (разделитель только '\t'):
                        // [тип датчика s|i|f]\t[номер датчика]\t[имя функции]\t[имя файла]\t[line definition][\t][col define]
                        // в Хранилище: имя файла начинается с названия папки без '\', например 'BUILD\zabbix-3.2.3\...'
                        // 
                        // Understand:  имя класса - оригинальное(!) регистр учитывается, имя метода приведено к нижнему регистру
                        // Understand: DB::getdbbackend - так для отчета, DB::getDbBackend - так в оригинальных исходниках
                        // Understand: имена функций - приведены к нижнему ригистру
                        // Understand: line и col считаются от единицы

                        // Разбираем строки
                        while ((line = reader.ReadLine()) != null)
                        {
                            count++;

                            // Пропускаем пустую строку
                            if (string.IsNullOrWhiteSpace(line))
                                continue;

                            string[] split = line.Split(new string[] { "\t" }, StringSplitOptions.RemoveEmptyEntries);

                            // Число столбцов отчета равно 6
                            if (split.Length != 6)
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Неправильный формат строки. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }

                            // 1 - Тип датчика
                            string sensorType = split[0].Trim();

                            // Тип датчика для Хранилища
                            Kind sensorKind;

                            // Проверка и установка типа датчика
                            if (sensorType.Equals("s"))
                                sensorKind = Kind.START;
                            else if (sensorType.Equals("i"))
                                sensorKind = Kind.INTERNAL;
                            else if (sensorType.Equals("f"))
                                sensorKind = Kind.FINAL;
                            else
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Тип датчика " + sensorType + " не известен. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }
                                                        
                            // 2 - Номер датчика
                            UInt64 number;

                            // Неправильный формат номера датчика в файле
                            if (!UInt64.TryParse(split[1], out number))
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Неправильный формат строки в файле <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }
                            
                            // Датчик уже есть в Хранилище
                            if (storeSensors.GetSensor(number) != null)
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Датчик " + number + " уже присутствует в Хранилище. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }

                            // Неверный номер датчика
                            if (number == Store.Const.CONSTS.WrongID)
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Неверный номер датчика. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }
                            
                            // 3 - Имя функции
                            string functionName = split[2].Trim().Replace("::", ".");

                            // Список функций из Хранилища
                            List<IFunction> functionList;

                            // Проверка существования функции в Хранилище
                            if ((functionList = storage.functions.FindFunction(functionName.Split('.')).ToList()).Count == 0)
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Функция " + functionName + " в Хранилище отсутствует. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }
                            
                            // 4 - Имя файла
                            string file = split[3].Trim();

                            // Список функций из Хранилища, определенные в файле file
                            List<IFunction> functions = null;

                            // Ищем функции, определнные в файле file
                            foreach (IFunction func in functionList)
                            {
                                Location location = func.Definition();
                                if (location != null && location.GetFile().FileNameForReports.Equals(file))
                                {
                                    if (functions == null)
                                        functions = new List<IFunction>();
                                    functions.Add(func);
                                }
                            }

                            // Если функций не нашлось
                            if (functions == null)
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Функция " + functionName + ", определенная в файле <" + file + ">, в Хранилище отсутствует. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }

                            // 5 - Номер строки в файле, где содержится функция
                            UInt64 row;

                            // Неправильный формат номера строки в файле
                            if (!UInt64.TryParse(split[4], out row))
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Неправильный формат строки в файле <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }

                            // Проверка номера строки
                            if (row == Store.LocationConstants.wrongLine)
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Неверный номер строки. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }

                            functionList = null;

                            // Ищем функции, определнные в файле file, на строке row
                            foreach (IFunction func in functions)
                            {
                                Location location = func.Definition();
                                if (location != null && row == location.GetLine())
                                {
                                    if (functionList == null)
                                        functionList = new List<IFunction>();
                                    functionList.Add(func);
                                }
                            }

                            // Если функций не нашлось
                            if (functionList == null)
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Функция " + functionName + ", определенная в файле <" + file + "> на строке " + row + ", в Хранилище отсутствует. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }
                            
                            // 6 - Номер столбца в файле, где содержится функция
                            UInt64 column;

                            // Неправильный формат номера столбца в файле
                            if (!UInt64.TryParse(split[5], out column))
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Неправильный формат столбца в файле <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }

                            // Проверка номера столбца
                            if (column == Store.LocationConstants.wrongColumn)
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Неверный номер столбца. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }

                            functions = new List<IFunction>();

                            // Ищем функции, определнные в файле file, на строке row в столбце column
                            foreach (IFunction func in functionList)
                            {
                                Location location = func.Definition();
                                if (location != null && column == location.GetColumn())
                                    functions.Add(func);
                            }

                            // Если функций не нашлось
                            if (functions.Count == 0)
                            {
                                IA.Monitor.Log.Error(PluginSettings.Name, "Функция " + functionName + ", определенная в файле <" + file + "> на строке " + row + " в столбце " + column + ", в Хранилище отсутствует. Файл <" + fileName + ">, cтрока " + count + ".");
                                continue;
                            }
                            
                            // Нашлось несколько функций
                            if (functions.Count > 1)
                            {
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Найдены несколько функций в Хранилище, соответсвующих описанию: " + functionName + ", определенная в файле <" + file + "> на строке " + row + " в столбце " + column + ". Файл <" + fileName + ">, cтрока " + count + ".");
                            }

                            // Добавляем датчик в Хранилище
                            storeSensors.AddSensor(number, functions[0].Id, sensorKind);

                            insertedSensorsCount++;
                        }
                    }
                }

                if (insertedSensorsCount == 0)
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Ни один датчик не был импортирован.");
            }
            catch (Exception ex)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, ex.Message);
            }
        }
    }
}
