﻿using System.IO;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using TestUtils;

namespace TestHandMadeSensorsImporter
{
    [TestClass]
    public class TestHandMadeSensorsImporter
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Проверка вывода сообщений, критичных при работе с Хранилищем
        /// </summary>
        [TestMethod]
        public void HandMadeSensorsImporter_MessagesTest()
        {
            const string sourcePrefixPart = "";

            string reportPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, "HandMadeSensorImporter", "Report.txt");

            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>()
            {
                IA.Monitor.Log.Message.enType.ERROR,
                IA.Monitor.Log.Message.enType.WARNING
            };

            //Формируем список ожидаемых сообщений
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>()
            {
                new IA.Monitor.Log.Message(
                    "Хранилище",
                    IA.Monitor.Log.Message.enType.WARNING,
                    "Был добавлен файл с идентификатором 1"),
                new IA.Monitor.Log.Message(
                    "Хранилище",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Файл " + Path.Combine(testUtils.TestMatirialsDirectoryPath.ToLower(), "test.cs") + " отсутствует на жестком диске. Смещение посчитано неверно"),
                new IA.Monitor.Log.Message(
                    "Импорт вручную расставленных датчиков",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Датчик 1 уже присутствует в Хранилище. Файл <" + reportPath + ">, cтрока 2."),
                new IA.Monitor.Log.Message(
                    "Импорт вручную расставленных датчиков",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Функция Test в Хранилище отсутствует. Файл <" + reportPath + ">, cтрока 3."),
                new IA.Monitor.Log.Message(
                    "Импорт вручную расставленных датчиков",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Функция TestFunction, определенная в файле <wrong.cs>, в Хранилище отсутствует. Файл <" + reportPath + ">, cтрока 4."),
                new IA.Monitor.Log.Message(
                    "Импорт вручную расставленных датчиков",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Функция TestFunction, определенная в файле <test.cs> на строке 2, в Хранилище отсутствует. Файл <" + reportPath + ">, cтрока 5."),
                new IA.Monitor.Log.Message(
                    "Импорт вручную расставленных датчиков",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Функция TestFunction, определенная в файле <test.cs> на строке 1 в столбце 2, в Хранилище отсутствует. Файл <" + reportPath + ">, cтрока 6."),
                new IA.Monitor.Log.Message(
                    "Импорт вручную расставленных датчиков",
                    IA.Monitor.Log.Message.enType.WARNING,
                    "Найдены несколько функций в Хранилище, соответсвующих описанию: TestFunction, определенная в файле <test.cs> на строке 10 в столбце 1. Файл <" + reportPath + ">, cтрока 7.")
            };

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    sourcePrefixPart,                                                                   // sourcePrefixPart
                    new IA.Plugins.Importers.HandMadeSensorsImporter.HandMadeSensorsImporter(),         // Plugin
                    false,                                                                              // isUseCachedStorage
                    (storage, testMaterialsPath) => { },                                                // prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        string path = Path.Combine(storage.appliedSettings.FileListPath, "test.cs");

                        // Добавляем функцию
                        IFunction function = storage.functions.AddFunction();
                        function.AddDefinition(path, 1, 1);
                        function.Name = "TestFunction";

                        // Еще одна функция
                        function = storage.functions.AddFunction();
                        function.AddDefinition(path, 10, 1);
                        function.Name = "TestFunction";

                        // Повторное добавление функции.
                        // При вставке датчика должен выдать Warning о повторной функции
                        function = storage.functions.AddFunction();
                        function.AddDefinition(path, 10, 1);
                        function.Name = "TestFunction";

                        // Загружаем настройки
                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                        writer.Add((int)1);
                        writer.Add(reportPath);
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER, writer);
                    },                                                                  // customiseStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },                                                                  // checkStorage
                    (reportsPath, testMaterialsPath) =>
                    {
                        return true;
                    },                                                                  // checkReportBeforeRerun
                    false,                                                              // isUpdateReport
                    (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                    (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                    );
            }
        }

        /// <summary>
        /// Проверка корректности отработки плагина на пустом файле отчета
        /// </summary>
        [TestMethod]
        public void HandMadeSensorsImporter_EmptyFile()
        {
            const string sourcePrefixPart = "";

            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>()
            {
                IA.Monitor.Log.Message.enType.WARNING
            };

            //Формируем список ожидаемых сообщений
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>()
            {
                new IA.Monitor.Log.Message(
                    "Импорт вручную расставленных датчиков",
                    IA.Monitor.Log.Message.enType.WARNING,
                    "Ни один датчик не был импортирован.")
            };

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    sourcePrefixPart,                                                                   // sourcePrefixPart
                    new IA.Plugins.Importers.HandMadeSensorsImporter.HandMadeSensorsImporter(),         // Plugin
                    false,                                                                              // isUseCachedStorage
                    (storage, testMaterialsPath) => { },                                                // prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        // Создаем пустой файл
                        string emptyFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "EmptyFile.txt");
                        using (FileStream file = File.Create(emptyFile)) { }

                        // Загружаем настройки
                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                        writer.Add((int)1);
                        writer.Add(emptyFile);
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER, writer);
                    },                                                                  // customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },                                                                  // checkStorage
                    (reportsPath, testMaterialsPath) =>
                    {
                        return true;
                    },                                                                  // checkReportBeforeRerun
                    false,                                                              // isUpdateReport
                    (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                    (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                    );
            }
        }

        /// <summary>
        /// Проверка корректности вставки датчиков
        /// Функция в Хранилище и входные данные формируются вручную.
        /// </summary>
        [TestMethod]
        public void HandMadeSensorsImporter_CorrectInsert()
        {
            const string sourcePrefixPart = "";
            testUtils.RunTest(
                sourcePrefixPart,                                                                   // sourcePrefixPart
                new IA.Plugins.Importers.HandMadeSensorsImporter.HandMadeSensorsImporter(),         // Plugin
                false,                                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    string path = Path.Combine(storage.appliedSettings.FileListPath, "test.cs");

                    // Добавляем функцию
                    IFunction function = storage.functions.AddFunction();
                    function.AddDefinition(path, 1, 1);
                    function.Name = "TestFunction";

                    // Создаем файл с данными о датчиках
                    string reportFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "Report.txt");
                    using (StreamWriter streamWriter = new StreamWriter(reportFile))
                    {
                        streamWriter.WriteLine("s\t1\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("i\t2\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("f\t3\tTestFunction\ttest.cs\t1\t1");
                    }

                    // Загружаем настройки
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add((int)1);
                    writer.Add(reportFile);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER, writer);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string testFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "Test.txt");

                    // Создаем новый отчет по Хранилищу
                    using (StreamWriter streamWriter = new StreamWriter(testFile))
                    {
                        foreach (Sensor sensor in storage.sensors.EnumerateSensors())
                        {
                            streamWriter.WriteLine(
                                kindToChar(sensor.Kind) + "\t" +
                                sensor.ID + "\t" +
                                sensor.GetFunction().FullNameForReports + "\t" +
                                sensor.GetFunction().Definition().GetFile().FileNameForReports + "\t" +
                                sensor.GetFunction().Definition().GetLine() + "\t" +
                                sensor.GetFunction().Definition().GetColumn());
                        }
                    }

                    string reportFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "Report.txt");

                    // Проверяем только что созданный отчет и отчет, созданный в customizeStorage
                    return TestUtilsClass.Reports_TXT_Compare(testFile, reportFile, true, false);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Преобразуем тип датчика Sensor.Kind в символ для отчета
        /// </summary>
        /// <param name="kind">Тип датчика</param>
        /// <returns>Тип датчика для отчета</returns>
        char? kindToChar(Kind kind)
        {
            switch (kind)
            {
                case Kind.START: 
                    return 's';
                case Kind.INTERNAL: 
                    return 'i';
                case Kind.FINAL: 
                    return 'f';
            }
            return null;
        }

        /// <summary>
        /// Проверка корректности вставки датчиков из нескольких протоколов.
        /// Функция в Хранилище и входные данные формируются вручную.
        /// </summary>
        [TestMethod]
        public void HandMadeSensorsImporter_CorrectMultipleInsert()
        {
            const string sourcePrefixPart = "";
            testUtils.RunTest(
                sourcePrefixPart,                                                                   // sourcePrefixPart
                new IA.Plugins.Importers.HandMadeSensorsImporter.HandMadeSensorsImporter(),         // Plugin
                false,                                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    string path = Path.Combine(storage.appliedSettings.FileListPath, "test.cs");

                    // Добавляем функцию
                    IFunction function = storage.functions.AddFunction();
                    function.AddDefinition(path, 1, 1);
                    function.Name = "TestFunction";

                    // Загружаем настройки
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add((int)2);                   

                    // Создаем первый протокол
                    string reportFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "Report1.txt");
                    using (StreamWriter streamWriter = new StreamWriter(reportFile))
                    {
                        streamWriter.WriteLine("s\t1\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("i\t2\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("f\t3\tTestFunction\ttest.cs\t1\t1");
                    }
                    writer.Add(reportFile);
                    
                    // Создаем второй протокол
                    reportFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "Report2.txt");
                    using (StreamWriter streamWriter = new StreamWriter(reportFile))
                    {
                        streamWriter.WriteLine("s\t4\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("i\t5\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("f\t6\tTestFunction\ttest.cs\t1\t1");
                    }
                    writer.Add(reportFile);

                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER, writer);

                    // Общий протокол
                    reportFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "Report.txt");
                    using (StreamWriter streamWriter = new StreamWriter(reportFile))
                    {
                        streamWriter.WriteLine("s\t1\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("i\t2\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("f\t3\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("s\t4\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("i\t5\tTestFunction\ttest.cs\t1\t1");
                        streamWriter.WriteLine("f\t6\tTestFunction\ttest.cs\t1\t1");
                    }
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string testFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "Test.txt");

                    // Создаем новый отчет по Хранилищу
                    using (StreamWriter streamWriter = new StreamWriter(testFile))
                    {
                        foreach (Sensor sensor in storage.sensors.EnumerateSensors())
                        {
                            streamWriter.WriteLine(
                                kindToChar(sensor.Kind) + "\t" +
                                sensor.ID + "\t" +
                                sensor.GetFunction().FullNameForReports + "\t" +
                                sensor.GetFunction().Definition().GetFile().FileNameForReports + "\t" +
                                sensor.GetFunction().Definition().GetLine() + "\t" +
                                sensor.GetFunction().Definition().GetColumn());
                        }
                    }

                    string reportFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "Report.txt");

                    // Проверяем только что созданный отчет и отчет, созданный в customizeStorage
                    return TestUtilsClass.Reports_TXT_Compare(testFile, reportFile, true, false);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков.
        /// Датчики вставляются с помощью плагина CS_Sensors.
        /// Отчет по датчикам создается с помощью плагина SensorGenerator.
        /// </summary>
        [TestMethod]
        public void HandMadeSensorsImporter_SensorGeneratorReport()
        {
            string sourcePrefixPart = @"CS_Sensors\C++";
            const string understandReportsPrefixPart = @"Understand\C++";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\CS_Sensors\C++\"; 
            string sensorGeneratorReportDirectoryPath = Path.Combine(testUtils.TestRunDirectoryPath, "SensorGeneratorReports");
            string sensorGeneratorReportFilePath = Path.Combine(sensorGeneratorReportDirectoryPath, "SensorGenerator.log");

            testUtils.RunTest(
                sourcePrefixPart,                                                                   // sourcePrefixPart
                new IA.Plugins.Importers.HandMadeSensorsImporter.HandMadeSensorsImporter(),         // Plugin
                false,                                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                    TestUtilsClass.Run_CS_Sensors(storage);

                    //Создаём директорию для формирования отчёта
                    Directory.CreateDirectory(sensorGeneratorReportDirectoryPath);

                    IA.Plugins.Analyses.SensorGenerator.SensorGenerator sensorGenerator = new IA.Plugins.Analyses.SensorGenerator.SensorGenerator();
                    sensorGenerator.Initialize(storage);
                    sensorGenerator.Run();
                    sensorGenerator.GenerateReports(sensorGeneratorReportDirectoryPath);

                    storage.sensors.Clear();

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add((int)1);
                    writer.Add(sensorGeneratorReportFilePath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER, writer);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string testFile = Path.Combine(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.HANDMADE_SENSORS_IMPORTER), "Test.txt");

                    // Создаем новый отчет по Хранилищу
                    using (StreamWriter streamWriter = new StreamWriter(testFile))
                    {
                        foreach (Sensor sensor in storage.sensors.EnumerateSensors())
                        {
                            streamWriter.WriteLine(
                                kindToChar(sensor.Kind) + "\t" +
                                sensor.ID + "\t" +
                                sensor.GetFunction().FullNameForReports + "\t" +
                                sensor.GetFunction().Definition().GetFile().FileNameForReports + "\t" +
                                sensor.GetFunction().Definition().GetLine() + "\t" +
                                sensor.GetFunction().Definition().GetColumn());
                        }
                    }

                    // Проверяем только что созданный отчет и отчет, созданный в customizeStorage
                    return TestUtilsClass.Reports_TXT_Compare(testFile, sensorGeneratorReportFilePath, true, false);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
    }
}
