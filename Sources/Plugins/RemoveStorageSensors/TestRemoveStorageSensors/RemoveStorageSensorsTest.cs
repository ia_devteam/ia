﻿using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using IA.Plugins.Analyses.RemoveStorageSensors;
using TestUtils;

namespace TestRemoveStorageSensors
{
    [TestClass]
    public class RemoveStorageSensorsTest
    {


        /// <summary>
        /// Summary description for MainTest
        /// </summary>
        [TestClass]
        public class TestRemoveStorageSensors
        {
            /// <summary>
            /// Массив шаблонов датчиков, используемых в известных IA языках программирования 
            /// </summary>
            private static string[] SensorsPatterns = new string[] 
                        {
                            "SensorRnt(#);",
                            "sensor(#)",
                            "sensor({#});",
                            "____Din_Go(\" \",#);",
                            "RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(#);",
                            "____Din_Go(' ',#);",
                            "System.Writeln(rnt_sensor,'#');",
                            "fputs($RNThandle,\"#\\n\");",
                            "RNTfileWITHsensors_File.WriteLine(#);"
                        };
            /// <summary>
            /// Экземпляр инфраструктуры тестирования для каждого теста
            /// </summary>
            private TestUtilsClass testUtils;

            private TestContext testContextInstance;
            /// <summary>
            /// Информация о текущем тесте
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            /// <summary>
            /// Делаем то, что необходимо сделать перед запуском каждого теста
            /// </summary>
            [TestInitialize]
            public void EachTest_Initialize()
            {
                //Иницииализация инфраструктуры тестирования для каждого теста
                testUtils = new TestUtilsClass(testContextInstance.TestName);
            }

            /// <summary>
            /// В Хранилище нет датчиков, попытка изъять датчики
            /// Результаты:
            /// Проверка, что количество датчиков = 0
            /// Проверка , что отчет пустой
            /// </summary>
            [TestMethod]
            public void RemoveStorageSensors_NoSensorsInStorage()
            {

                const string sourcePrefixPart = @"Sources\RemoveSensors\";
                List<string> Patterns = new List<string>(SensorsPatterns);

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    new RemoveStorageSensors(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {

                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();

                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"RemoveStorageSensors\Sources\"));
                        settingsBuffer.Add(string.Join("\n", Patterns.ToArray()));

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.REMOVE_STORAGE_SENSORS, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        ulong cnt = storage.sensors.Count();
                        if (cnt != 0)
                            return false;
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"RemoveStorageSensors\ReportsNoSensorsInStorage\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }

            /// <summary>
            /// Попытка изъять датчики из другого диапазона
            /// В Хранилище есть датчики 12 датчиков с номерами 1-12
            /// В текстах исходных файлов 11 датчиков с номерами 101-111 
            /// Результаты:
            /// Проверка, что количество датчиков в Хранилище = 0
            /// Проверка , что отчет об изъятых датчиках включает все датчики
            /// </summary>
            [TestMethod]
            public void RemoveStorageSensors_DifferentSensors()
            {

                const string sourcePrefixPart = @"Sources\RemoveSensors\";
                List<string> Patterns = new List<string>(SensorsPatterns);

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    new RemoveStorageSensors(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {

                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);
                        TestUtils.TestUtilsClass.Run_PhpParser(storage);
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();

                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"RemoveStorageSensors\SourcesDifferent\"));
                        settingsBuffer.Add(string.Join("\n", Patterns.ToArray()));

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.REMOVE_STORAGE_SENSORS, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        ulong cnt = storage.sensors.Count();
                        if (cnt != 0)
                            return false;
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"RemoveStorageSensors\ReportsDifferent\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }

            /// <summary>
            /// В Хранилище есть датчики (12 датчиков), в исходных файлах нет датчиков
            /// Результаты:
            /// Проверка, что количество датчиков в Хранилище = 0
            /// Проверка , что отчет об изъятых датчиков содержит все датчики (12)
            /// </summary>
            [TestMethod]
            public void RemoveStorageSensors_WithoutSensors()
            {

                const string sourcePrefixPart = @"Sources\RemoveSensors\";
                List<string> Patterns = new List<string>(SensorsPatterns);

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    new RemoveStorageSensors(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {

                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);
                        TestUtils.TestUtilsClass.Run_PhpParser(storage);
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();

                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"RemoveStorageSensors\SourcesWithoutSensors\"));
                        settingsBuffer.Add(string.Join("\n", Patterns.ToArray()));

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.REMOVE_STORAGE_SENSORS, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        ulong cnt = storage.sensors.Count();
                        if (cnt != 0)
                            return false;
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"RemoveStorageSensors\ReportsDifferent\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }

            /// <summary>
            /// Проверка того, что если в текстах присутствуют те же датчики, что и в хранилище, то нмчего не изымается
            /// В Хранилище есть датчики 12 датчиков с номерами1-12 
            /// В исходных файлах датчики с номерами 1-12
            /// Результаты:
            /// Проверка, что количество датчиков в Хранилище = 12
            /// Проверка , что отчет об изъятых датчиков не содержит датчиков
            /// </summary>
            [TestMethod]
            public void RemoveStorageSensors_AllSensors()
            {

                const string sourcePrefixPart = @"Sources\RemoveSensors\";
                List<string> Patterns = new List<string>(SensorsPatterns);

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    new RemoveStorageSensors(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {

                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);
                        TestUtils.TestUtilsClass.Run_PhpParser(storage);
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();

                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"RemoveStorageSensors\SourcesAll\"));
                        settingsBuffer.Add(string.Join("\n", Patterns.ToArray()));

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.REMOVE_STORAGE_SENSORS, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        ulong cnt = storage.sensors.Count();
                        if (cnt != 12)
                            return false;
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"RemoveStorageSensors\ReportsNoSensorsInStorage\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }

            /// <summary>
            /// Проверка того, что в текстах распознаются тексты датчиков, сформированных для различных языков программирования, известных IA
            /// В Хранилище есть датчики (12 датчиков), в исходных файлах те же датчики
            /// причем используются шаблоны датчиков различных языков программирования из IA: (C#,C/C++,Delphi,JavaScript,PHP,VB):
            ///                            "SensorRnt(#);",
            ///                            "sensor(#)",
            ///                            "sensor({#});",
            ///                            "____Din_Go(\" \",#);",
            ///                            "RNT_Sensor.RNT_Sensor.RNT_sensor_function_call(#);",
            ///                            "____Din_Go(' ',#);",
            ///                            "System.Writeln(rnt_sensor,'#');",
            ///                            "fputs($RNThandle,\\\"#\\\\n\\\");",
            ///                            "RNTfileWITHsensors_File.WriteLine(#);"
            /// Результаты:
            /// Проверка, что количество датчиков в Хранилище = 12
            /// Проверка , что отчет об изъятых датчиков не содержит датчиков
            /// </summary>
            [TestMethod]
            public void RemoveStorageSensors_AllLanguages()
            {

                const string sourcePrefixPart = @"Sources\RemoveSensors\";
                List<string> Patterns = new List<string>(SensorsPatterns);

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    new RemoveStorageSensors(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {

                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);
                        TestUtils.TestUtilsClass.Run_PhpParser(storage);
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();

                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"RemoveStorageSensors\SourcesAllLanguage\"));
                        settingsBuffer.Add(string.Join("\n", Patterns.ToArray()));

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.REMOVE_STORAGE_SENSORS, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        ulong cnt = storage.sensors.Count();
                        if (cnt != 12)
                            return false;
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"RemoveStorageSensors\ReportsNoSensorsInStorage\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }

            /// <summary>
            /// В Хранилище есть датчики (12 датчиков), в исходных файлах не все датчики (10), 
            /// Результаты:
            /// Проверка, что количество датчиков в Хранилище = 10
            /// Проверка , что отчет об изъятых датчиков содержит 2 датчика, сравнение с эталоном
            /// </summary>
            [TestMethod]
            public void RemoveStorageSensors_OK()
            {

                const string sourcePrefixPart = @"Sources\RemoveSensors\";
                List<string> Patterns = new List<string>(SensorsPatterns);

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    new RemoveStorageSensors(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {

                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);
                        TestUtils.TestUtilsClass.Run_PhpParser(storage);
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();

                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"RemoveStorageSensors\Sources\"));
                        settingsBuffer.Add(string.Join("\n", Patterns.ToArray()));

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.REMOVE_STORAGE_SENSORS, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        ulong cnt = storage.sensors.Count();
                        if (cnt != 10)
                            return false;
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"RemoveStorageSensors\Reports\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }
        }
    }
}
