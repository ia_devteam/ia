﻿namespace IA.Plugins.Analyses.RemoveStorageSensors
{
    partial class Settings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.patterns_groupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.addPattern_button = new System.Windows.Forms.Button();
            this.updatePattern_button = new System.Windows.Forms.Button();
            this.deletePattern_button = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.listBoxPatterns = new System.Windows.Forms.ListBox();
            this.textBox_CurrentPattern = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.labPath_groupBox = new System.Windows.Forms.GroupBox();
            this.textBoxSources = new IA.Controls.TextBoxWithDialogButton();
            this.patterns_groupBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.labPath_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // patterns_groupBox
            // 
            this.patterns_groupBox.Controls.Add(this.tableLayoutPanel1);
            this.patterns_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patterns_groupBox.Location = new System.Drawing.Point(3, 57);
            this.patterns_groupBox.Name = "patterns_groupBox";
            this.patterns_groupBox.Size = new System.Drawing.Size(662, 169);
            this.patterns_groupBox.TabIndex = 0;
            this.patterns_groupBox.TabStop = false;
            this.patterns_groupBox.Text = "Шаблоны текстов датчиков";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(656, 150);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.addPattern_button, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.updatePattern_button, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.deletePattern_button, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(559, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(94, 144);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // addPattern_button
            // 
            this.addPattern_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addPattern_button.Location = new System.Drawing.Point(3, 3);
            this.addPattern_button.Name = "addPattern_button";
            this.addPattern_button.Size = new System.Drawing.Size(88, 24);
            this.addPattern_button.TabIndex = 0;
            this.addPattern_button.Text = "Добавить";
            this.addPattern_button.UseVisualStyleBackColor = true;
            this.addPattern_button.Click += new System.EventHandler(this.buttonAddPattern_Click);
            // 
            // updatePattern_button
            // 
            this.updatePattern_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updatePattern_button.Location = new System.Drawing.Point(3, 33);
            this.updatePattern_button.Name = "updatePattern_button";
            this.updatePattern_button.Size = new System.Drawing.Size(88, 24);
            this.updatePattern_button.TabIndex = 1;
            this.updatePattern_button.Text = "Изменить";
            this.updatePattern_button.UseVisualStyleBackColor = true;
            this.updatePattern_button.Click += new System.EventHandler(this.buttonUpdatePattern_Click);
            // 
            // deletePattern_button
            // 
            this.deletePattern_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deletePattern_button.Location = new System.Drawing.Point(3, 63);
            this.deletePattern_button.Name = "deletePattern_button";
            this.deletePattern_button.Size = new System.Drawing.Size(88, 24);
            this.deletePattern_button.TabIndex = 2;
            this.deletePattern_button.Text = "Удалить";
            this.deletePattern_button.UseVisualStyleBackColor = true;
            this.deletePattern_button.Click += new System.EventHandler(this.buttonDeletePattern_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.listBoxPatterns, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.textBox_CurrentPattern, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(550, 144);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // listBoxPatterns
            // 
            this.listBoxPatterns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxPatterns.FormattingEnabled = true;
            this.listBoxPatterns.Location = new System.Drawing.Point(3, 33);
            this.listBoxPatterns.Name = "listBoxPatterns";
            this.listBoxPatterns.Size = new System.Drawing.Size(544, 108);
            this.listBoxPatterns.TabIndex = 0;
            this.listBoxPatterns.SelectedIndexChanged += new System.EventHandler(this.listBoxPatterns_SelectedIndexChanged);
            // 
            // textBox_CurrentPattern
            // 
            this.textBox_CurrentPattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_CurrentPattern.Location = new System.Drawing.Point(3, 3);
            this.textBox_CurrentPattern.Name = "textBox_CurrentPattern";
            this.textBox_CurrentPattern.Size = new System.Drawing.Size(544, 20);
            this.textBox_CurrentPattern.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.patterns_groupBox, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.labPath_groupBox, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(668, 229);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // labPath_groupBox
            // 
            this.labPath_groupBox.Controls.Add(this.textBoxSources);
            this.labPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labPath_groupBox.Location = new System.Drawing.Point(3, 3);
            this.labPath_groupBox.Name = "labPath_groupBox";
            this.labPath_groupBox.Size = new System.Drawing.Size(662, 48);
            this.labPath_groupBox.TabIndex = 1;
            this.labPath_groupBox.TabStop = false;
            this.labPath_groupBox.Text = "Путь к файлам со вставленными датчиками";
            // 
            // textBoxSources
            // 
            this.textBoxSources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSources.Location = new System.Drawing.Point(3, 16);
            this.textBoxSources.MaximumSize = new System.Drawing.Size(0, 25);
            this.textBoxSources.MinimumSize = new System.Drawing.Size(400, 25);
            this.textBoxSources.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.textBoxSources.Name = "textBoxSources";
            this.textBoxSources.Size = new System.Drawing.Size(656, 25);
            this.textBoxSources.TabIndex = 0;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel4);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(668, 229);
            this.patterns_groupBox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.labPath_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox patterns_groupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button addPattern_button;
        private System.Windows.Forms.Button updatePattern_button;
        private System.Windows.Forms.Button deletePattern_button;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ListBox listBoxPatterns;
        private System.Windows.Forms.TextBox textBox_CurrentPattern;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.GroupBox labPath_groupBox;
        private Controls.TextBoxWithDialogButton textBoxSources;
    }
}
