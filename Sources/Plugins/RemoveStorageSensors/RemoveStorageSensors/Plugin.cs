﻿/*
 * Плагин Изъятие из Хранилища некорректно вставленных датчиков
 * Файл Plugin.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Кокин
 * 
 * Плагин предназначен для удаления из хранилища некорректно вставленных датчиков.
 */
using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Specialized;
using System.Collections.Generic;

using Store;
using Store.Table;
using IA.Extensions;

namespace IA.Plugins.Analyses.RemoveStorageSensors
{

    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Изъятие из Хранилища некорректно вставленных датчиков";

        /// <summary>
        /// Иденитификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.REMOVE_STORAGE_SENSORS;

        #region Settings

        /// <summary>
        /// Путь к лабораторным исходным текстам
        /// </summary>
        internal static string SourcesPath;

        /// <summary>
        /// Список шаблонов датчиков
        /// </summary>
        internal static StringCollection PatternsCollection;

        /// <summary>
        /// Список шаблонов датчиков
        /// </summary>
        internal static List<string> PatternList
        {
            get
            {
                return PluginSettings.PatternsCollection != null ? PluginSettings.PatternsCollection.Cast<string>().ToList() : new List<string>();
            }
            set
            {
                PluginSettings.PatternsCollection = new StringCollection();

                if (value == null)
                    return;

                PluginSettings.PatternsCollection.AddRange(value.ToArray());
            }
        }

        #endregion

        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Формирование списка датчиков из Хранилища")]
            CREATE_SENSORS_LIST,
            [Description("Формирование списка вставленных датчиков по содержимому файлов")]
            CREATE_SENSORS_FILES_LIST,
            [Description("Изъятие из Хранилища некорректно вставленных датчиков")]
            REMOVE_STORAGE_SENSORS
        };
        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum TaskReports
        {
            [Description("Отчет по изъятию из Хранилища некорректно вставленных датчиков")]
            REMOVE_STORAGE_SENSORS_REPORT
        };
    }

    public class RemoveStorageSensors : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Начальные настройки
        /// </summary>
        Settings settings;

        #region Variables

        /// <summary>
        /// Шаблоны, подготовленные для REGEX
        /// </summary>
        internal List<string> listPatterns = new List<string>();

        /// <summary>
        /// Список удаленных из Хранилища датчиков
        /// </summary>
        internal List<ulong> snsRemoved = new List<ulong>();

        internal HashSet<ulong> snsIDsInFiles = new HashSet<ulong>();

        #endregion

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get { return PluginSettings.ID; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get { return PluginSettings.Name; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return
                    Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW |
                    Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW |
                    Plugin.Capabilities.REPORTS |
                    Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get { return settings = new Settings(false); }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get { return settings = new Settings(true); }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                    {
                        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.CREATE_SENSORS_LIST.Description(),0),
                        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.CREATE_SENSORS_FILES_LIST.Description(),0),
                        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.REMOVE_STORAGE_SENSORS.Description(),0)
                    };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                    {
                        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.TaskReports.REMOVE_STORAGE_SENSORS_REPORT.Description(),0)
                    };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.SourcesPath = Properties.Settings.Default.sourcesPath;
            PluginSettings.PatternsCollection = Properties.Settings.Default.Patterns;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.SourcesPath = settingsBuffer.GetString();
            PluginSettings.PatternList = new List<string>(settingsBuffer.GetString().Split('\n'));
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            //Properties.Settings.Default.Patterns = Patterns.Select(x=>x).;
            Properties.Settings.Default.sourcesPath = PluginSettings.SourcesPath;
            Properties.Settings.Default.Patterns = PluginSettings.PatternsCollection;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.SourcesPath);
            settingsBuffer.Add(string.Join("\n", PluginSettings.PatternList.ToArray()));
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (PluginSettings.PatternList.Count == 0)
            {
                message = "Не определены шаблоны текстов вставленных датчиков.";
                return false;
            }

            if (!Directory.Exists(PluginSettings.SourcesPath))
            {
                message = "Путь к файлам со вставленными датчиками не существует.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            // Формирование списка шаблонов для REGEX поиска по тексту
            listPatterns.Clear();
            // Очистка списка для отчета
            snsRemoved.Clear();

            //foreach (string s in PluginSettings.PatternList)
            //{
            //    listPatterns.Add(Regex.Replace(Regex.Replace(s, "\\(|\\)|\\'|\\$", new MatchEvaluator(RegexControls)), "#", new MatchEvaluator(PatNum)));
            //}

            List<Regex> listOfPatterns = new List<Regex>();
            foreach (var s in PluginSettings.PatternList)
            {
                listOfPatterns.Add(new Regex(Regex.Escape(s).Replace(@"\#", @"(?<SENSORID>\d+)"), RegexOptions.ExplicitCapture));
            }
            //List<string> snsFiles = new List<string>();
            //List<ulong> snsIDs = new List<ulong>();

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CREATE_SENSORS_LIST.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)storage.sensors.Count());
            // Собираем все зарегистрированные в Хранилище номера датчиков
            ulong cnt = 1;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CREATE_SENSORS_LIST.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 1);
            var snsIDs = storage.sensors.EnumerateSensors().Select(x => x.ID).ToList();
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CREATE_SENSORS_LIST.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)snsIDs.Count);
            //foreach (Sensor sns in storage.sensors.EnumerateSensors())
            //{
            //    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CREATE_SENSORS_LIST.Description(), Monitor.Tasks.Task.UpdateType.STAGE, cnt++);
            //    snsIDs.Add(sns.ID);
            //}
            //избыточно. Перед запуском выполняется IsSettingsCorrect с такой же проверкой
            var snsFiles = (new DirectoryInfo(PluginSettings.SourcesPath).GetFiles("*.*", SearchOption.AllDirectories)).Select(x => x.FullName).ToArray();

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CREATE_SENSORS_FILES_LIST.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)snsFiles.Length);
            // Для каждого файла ищем все вхождения из списка шаблонов текстов датчиков, найденные номера датчиков сохраняем
            cnt = 1;
            foreach (string filename in snsFiles)
            {
                string sbody = File.ReadAllText(filename);
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CREATE_SENSORS_FILES_LIST.Description(), Monitor.Tasks.Task.UpdateType.STAGE, cnt++);
                //foreach (string reg in listPatterns)
                //{
                //    Regex.Replace(sbody, reg, new MatchEvaluator(ReplacePattern)); 
                //}

                foreach (var reg in listOfPatterns)
                {
                    foreach (Match m in reg.Matches(sbody))
                    {
                        snsIDsInFiles.Add(ulong.Parse(m.Groups["SENSORID"].Value));
                    }
                }
            }

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.REMOVE_STORAGE_SENSORS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)snsIDs.Count);
            cnt = 1;
            // Удаляем из хранилища избыточные датчики, готовим отчет
            foreach (ulong id in snsIDs)
            {
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.REMOVE_STORAGE_SENSORS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, cnt++);
                if (!snsIDsInFiles.Contains(id))
                {
                    storage.sensors.RemoveSensor(id);
                    snsRemoved.Add(id);
                }
            }

            Store.Table.IBufferWriter ibw = Store.Table.WriterPool.Get();
            ibw.Add(ObjectToBytes(snsRemoved));
            storage.pluginResults.SaveResults(PluginSettings.ID, ibw);

        }

        /// <summary>
        /// Шаблоны с текстами датчиков вместо номера датчиков должны содержать символ '#', 
        /// причем этот символ не должен располагаться вначале, либо в конце строки шаблона,
        /// кроме этого в шаблоне не допускаются любые символы из диапазона от '0' до '9'.
        /// Здесь - делегат - замена в REGEX символа '#' на представление числа "[\d{1,9}]".
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        static string PatNum(Match m)
        {
            return @"+[\d{1,9}]+";
        }

        /// <summary>
        /// В тексте шаблона датчика могут встречаться управляющие символы REGEX.
        /// Делегат - Замена управляющих символов 'U' на строки "\U"
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        static string RegexControls(Match m)
        {
            return @"\" + m.ToString();
        }

        /// <summary>
        /// Делегат - После того, как мы нашли через REGEX нужный текст датчика, в нем нужно выделить номер датчика
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        string ReplacePattern(Match m)
        {
            string x = m.ToString();
            x = Regex.Replace(x, @"\d{1,9}", new MatchEvaluator(ReplaceSensor));
            return x;
        }

        /// <summary>
        /// Делегат - Полученный номер датчика сохраняем всписке найденных датчиков
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        string ReplaceSensor(Match m)
        {
            string x = m.ToString();
            ulong val;
            if (ulong.TryParse(x, out val))
                snsIDsInFiles.Add(val);
            return x;
        }


        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            //Проверка на разумность
            if (this.storage != null)
            {
                Store.Table.IBufferReader ibr1 = Store.Table.ReaderPool.Get();
                ibr1 = storage.pluginResults.LoadResult(PluginSettings.ID);
                if (ibr1 != null)
                {
                    snsRemoved = (List<ulong>)BytesToObject(ibr1.GetByteArray());
                }

                File.WriteAllLines(Path.Combine(reportsDirectoryPath, "Изъятые из Хранилища некорректно вставленные датчики.txt"), snsRemoved.Select(x => x.ToString()));
            }
            else
                throw new Exception("Хранилище не определено.");
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }


        /// <summary>
        /// Конвертация сериализуемого объекта в байтовый массив
        /// </summary>
        /// <param name="dataObj"></param>
        /// <returns></returns>
        internal static byte[] ObjectToBytes(object dataObj)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            formatter.Serialize(ms, dataObj);

            return ms.GetBuffer();
        }

        /// <summary>
        /// Конвертация байтового массива в сериализуемый объект 
        /// </summary>
        /// <param name="dataBytes"></param>
        /// <returns></returns>
        internal static object BytesToObject(byte[] dataBytes)
        {
            return new BinaryFormatter().Deserialize(new System.IO.MemoryStream(dataBytes));
        }


    }
}
