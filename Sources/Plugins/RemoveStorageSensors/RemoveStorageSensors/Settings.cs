﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace IA.Plugins.Analyses.RemoveStorageSensors
{
    public partial class Settings : UserControl, IA.Plugin.Settings
    {
        public Settings(bool bCustom)
        {
            InitializeComponent();

            textBoxSources.Text = PluginSettings.SourcesPath;
            if (PluginSettings.PatternList.Count > 0)
                listBoxPatterns.Items.AddRange(PluginSettings.PatternList.ToArray());

            if (!bCustom)
                patterns_groupBox.Visible = false;
            else
                patterns_groupBox.Visible = true;
        }

        /// <summary>
        /// Выделение строки в списке шаблонов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxPatterns_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxPatterns.SelectedIndex != -1)
                textBox_CurrentPattern.Text = listBoxPatterns.Text;
        }

        /// <summary>
        /// Добавление нового шаблона в список
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddPattern_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textBox_CurrentPattern.Text) && !listBoxPatterns.Items.Cast<string>().Contains(textBox_CurrentPattern.Text.Trim()))
            {
                listBoxPatterns.Items.Add(textBox_CurrentPattern.Text.Trim());
            }
            listBoxPatterns.SelectedIndex = listBoxPatterns.Items.Cast<string>().ToList().IndexOf(textBox_CurrentPattern.Text.Trim());
        }

        /// <summary>
        /// Изменение текущего шаблона
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdatePattern_Click(object sender, EventArgs e)
        {
            if (listBoxPatterns.SelectedIndex != -1 && textBox_CurrentPattern.Text.Trim() != listBoxPatterns.Text)
            {
                listBoxPatterns.Items[listBoxPatterns.SelectedIndex] = textBox_CurrentPattern.Text.Trim();
            }
        }

        /// <summary>
        /// Удаление текущего шаблона
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeletePattern_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textBox_CurrentPattern.Text) && listBoxPatterns.Items.Cast<string>().Contains(textBox_CurrentPattern.Text.Trim()))
            {
                listBoxPatterns.Items.RemoveAt(listBoxPatterns.Items.Cast<string>().ToList().IndexOf(textBox_CurrentPattern.Text.Trim()));
            }
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.PatternList = new List<string>(listBoxPatterns.Items.Cast<string>());
            PluginSettings.SourcesPath = textBoxSources.Text;
        }
    }


}
