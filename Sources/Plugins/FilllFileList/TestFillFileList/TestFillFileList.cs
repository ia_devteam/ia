﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;

using TestUtils;

namespace TestFillFileList
{
    /// <summary>
    /// Класс, реализующий тест плагина "Импорт списка файлов"
    /// </summary>
    [TestClass]
    public class TestFillFileList
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Сравнение количества файлов в хранилище и в директирии на диске. Тест проводится на текущей папке (то есть папке, где находится тестирующая dll).
        /// </summary>
        [TestMethod]
        public void FillFileList_Count()
        {
            const string sourcePostfixPart = @"Sources";
            testUtils.RunTest(
                sourcePostfixPart,                                                   // sourcePostfixPart
                new IA.Plugins.Importers.FillFileList.FillFileList(),           // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) => { },                            // customizeStorage
                (storage, testMaterialsPath) => 
                {
                    return (ulong)Directory.EnumerateFiles(Path.Combine(testMaterialsPath, sourcePostfixPart), "*.*", SearchOption.AllDirectories).Count() == storage.files.Count();
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Тест приверяет, что все файлы, занесённые  плагином в Хранилище, присутствуют на диске. Тест проводится на текущей папке (то есть папке, где находится тестирующая dll).
        /// </summary>
        [TestMethod]
        public void FillFileList_FilesExistance()
        {
            const string sourcePostfixPart = @"Sources";
            testUtils.RunTest(
                sourcePostfixPart,                                                   // sourcePostfixPart
                new IA.Plugins.Importers.FillFileList.FillFileList(),           // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) => { },                            // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return storage.files.EnumerateFiles(Store.enFileKind.anyFile).All(f => File.Exists(f.FullFileName_Original));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Тест запуска плагина на пустой папке
        /// </summary>
        [TestMethod]
        public void FillFileList_EmptyDirectory()
        {
            const string sourcePostfixPart = @"FillFileList\EmptyFolder\";
            //git won't store really empty dir.
            //http://stackoverflow.com/questions/115983/how-can-i-add-an-empty-directory-to-a-git-repository
            //Let make it on the fly...
            if (!System.IO.Directory.Exists(System.IO.Path.Combine(testUtils.TestMatirialsDirectoryPath, sourcePostfixPart)))
                System.IO.Directory.CreateDirectory(System.IO.Path.Combine(testUtils.TestMatirialsDirectoryPath, sourcePostfixPart));

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new IA.Plugins.Importers.FillFileList.FillFileList(),           // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) => { },                            // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return storage.files.Count() == 0;
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
                );
        }
    }
}
