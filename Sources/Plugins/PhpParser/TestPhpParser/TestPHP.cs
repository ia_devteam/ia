﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using Store.Table;
using TestUtils;
using IA.Plugins.Parsers.PhpParser;

namespace TestPHP
{
    /// <summary>
    /// Класс для тестов парсера PhpParser
    /// </summary>
    [TestClass]
    public class TestPHP
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Возвращает количество перехваченных сообщений
            /// </summary>
            /// <returns>Количество сообщений</returns>
            public int Count()
            {
                return messages.Count;
            }
        }

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Подпуть до каталога с исходниками, содержащими ошибки
        /// </summary>
        private const string PHP_ERROR_SOURCES_SUBDIRECTORY = @"Sources\PHPerror\";

        /// <summary>
        /// Подпуть до каталога с исходниками, содержащего простые тестовые наборы
        /// </summary>
        private const string PHP_SIMPLE_SOURCES_SUBDIRECTORY = @"Sources\PHPsimple\";

        /// <summary>
        /// Подпуть до каталога с исходниками, содержащего наборы простых конструкций языка
        /// </summary>
        private const string PHP_SOURCES_SUBDIRECTORY = @"Sources\PHP\";

        /// <summary>
        /// Подпуть до каталога с исходниками, содержащего большой набор тестовых файлов
        /// </summary>
        private const string PHP_TEST_SOURCES_SUBDIRECTORY = @"Sources\PHPTests\";

        /// <summary>
        /// Подпуть до каталога с исходниками, содержащего тестовые примеры синтаксический конструкций
        /// </summary>
        private const string PHP_TEST_STATEMENTS_SUBDIRECTORY = @"Sources\PHPStatements\";

        /// <summary>
        /// Подпуть до каталога с эталонными материалами
        /// </summary>
        private const string SAMPLES_SUBDIRECTORY = @"PhpParser\";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        private const ulong pluginID = Store.Const.PluginIdentifiers.PHP_PARSER;

        /// <summary>
        /// Перехватчик сообщений из монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        ///// <summary>
        ///// Проверка получения сообщения об установке стартового датчика по умолчанию
        ///// </summary>
        //[TestMethod]
        //public void PhpParser_DefaultFirstSensor()
        //{
        //    PHPParser testPlugin = new PHPParser();

        //    testUtils.RunTest(
        //        string.Empty,                                               //sourcePostfixPart
        //        testPlugin,                                                 //plugin
        //        false,                                                      //isUseCachedStorage
        //        (storage, testMaterialsPath) => { },                        //prepareStorage
        //        (storage, testMaterialsPath) =>
        //        {
        //            SettingUpPlugin(storage, 0, true);
        //        },                                                          //customizeStorage
        //        (storage, testMaterialsPath) =>
        //        {
        //            CheckMessage("Установлена настройка стартового датчика по умолчанию (ноль). Расстановка датчиков будет начата с датчика <1>.");

        //            return true;
        //        },                                                          //checkStorage
        //        (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
        //        false,                                                      //isUpdateReport
        //        (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
        //        (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
        //        (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
        //        );
        //}

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже присутствует в лабораторных тексах.
        /// </summary>
        [TestMethod]
        public void PhpParser_WrongFirstSensor_InLabs()
        {
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                       //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);

                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <1> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <2>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика не фигурировал в исходных файлах, 
        /// но оказался меньше, чем максимальный идентификатор в Хранилище.
        /// </summary>
        [TestMethod]
        public void PhpParser_WrongFirstSensor_NotInLabs()
        {
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(3, func1.Id, Kind.START);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);

                    SettingUpPlugin(storage, 2, true);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже фигурировал в исходных файлах, но был удален.
        /// </summary>
        [TestMethod]
        public void PhpParser_WrongFirstSensor_NotInLabsDeleted()
        {
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);
                    storage.sensors.AddSensor(2, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(3, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);

                    storage.sensors.RemoveSensor(2);

                    SettingUpPlugin(storage, 2, true);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Простой тест 001. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_001()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "001");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\001");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\001");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(0);

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\001");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }
        /// <summary>
        /// Простой тест 002. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_002()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "002");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\002");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\002");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(10);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\002");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Простой тест 003. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_003()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "003");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\003");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\003");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(0);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\003");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Простой тест 004. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_004()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "004");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\004");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\004");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(3);

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\004");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Простой тест 005. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 3-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_3level1_005()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "005");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, false);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\005");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\005");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(9);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\005");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Простой тест 006. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_006()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "006");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\006");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\006");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(6);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\006");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Простой тест 007. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_007()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "007");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\007");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\007");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(1);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\007");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Простой тест 008. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_008()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "008");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\008");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\008");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(1);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\008");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Простой тест 009. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_009()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "009");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\009");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\009");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(13);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\009");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Простой тест 010. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_010()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "010");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\010");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\010");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(0);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\010");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Прстой тест 011. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_011()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "011");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\011");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\011");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(0);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\011");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Прстой тест 012. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_012()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "012");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\012");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\012");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(1);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\012");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Прстой тест 013. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_013()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "013");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\013");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\013");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(1);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\013");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Прстой тест 014. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_014()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "014");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\014");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\014");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(0);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\014");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Прстой тест 015. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_015()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "015");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\015");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\015");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(0);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\015");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Прстой тест 016. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_016()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "016");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\016");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\016");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(1);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\016");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Прстой тест 017. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_017()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "017");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\017");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\017");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(4);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\017");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Прстой тест 018. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 1 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_018()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "018");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\018");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\018");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(0);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\018");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Прстой тест 019. Результат работы над исправлением ошибок плагина
        /// Тест проверяет работу плагина по 3-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов - 2 штука 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_019()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "019");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, false);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\019");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\019");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(13);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\019");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Исходные тексты PHP имеют вставки на JavaScript и html
        /// Проверяется, что вставлено 29 датчиков в части PHP и ни одного в части JavaScript
        /// количество исходных файлов - 1 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_021()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "021");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\1\021");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1\021");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"1reports\021");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }


        /// <summary>
        /// Тест вложенных конструкций языка PHP
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов менее 40 штук 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level_40()
        {
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                PHP_SOURCES_SUBDIRECTORY,                                   //sourcePostfixPart
                testPlugin,                                            //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, PHP_SOURCES_SUBDIRECTORY));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\2");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"2");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(25);

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"2reports");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Тест вложенных конструкций языка PHP
        /// Тест проверяет работу плагина по 3-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов менее 40 штук 
        /// </summary>
        [TestMethod]
        public void PhpParser_3level_40()
        {
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                PHP_SOURCES_SUBDIRECTORY,                                   //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, PHP_SOURCES_SUBDIRECTORY));
                    SettingUpPlugin(storage, 1, false);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\3");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"3");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(25);

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"3reports");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Тест синтаксиса языка PHP
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов более 500 штук (все конструкции языка PHP)
        /// </summary>
        [TestMethod]
        public void PhpParser_2level_500()
        {
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                PHP_TEST_SOURCES_SUBDIRECTORY,                              //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, PHP_TEST_SOURCES_SUBDIRECTORY));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\500_2");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"500_2");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(1228); //думаю что достаточно глупая и ниочемная проверка при одинаковых результатах

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    //string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"500_2reports");
                    ////testPlugin.GenerateStatements(reportsFullPath);

                    ////CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Тест синтаксиса языка PHP
        /// Тест проверяет работу плагина по 3-му уровню НДВ с расстановкой датчиков с единицы
        /// количество исходных файлов более 500 штук (все конструкции языка PHP)
        /// </summary>
        [TestMethod]
        public void PhpParser_3level_500()
        {
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                PHP_TEST_SOURCES_SUBDIRECTORY,                              //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, PHP_TEST_SOURCES_SUBDIRECTORY));
                    SettingUpPlugin(storage, 1, false);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\500_3");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"500_3");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(1228); //думаю что достаточно глупая и ниочемная проверка при одинаковых результатах

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"500_3reports");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }


        /// <summary>
        /// Тест вложенных конструкций языка PHP
        /// Тест проверяет работу плагина с расстановкой датчиков с 1000000 номера
        /// количество исходных файлов менее 40 штук 
        /// </summary>
        [TestMethod]
        public void PhpParser_number_1000000()
        {
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                PHP_SOURCES_SUBDIRECTORY,                                   //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                         //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, PHP_SOURCES_SUBDIRECTORY));
                    SettingUpPlugin(storage, 1000000, false);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\3_N");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"3_N");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessageCount(25);

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"3_Nreports");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }
        /// <summary>
        /// Тест проверяет работу плагина с ошибочными консрукциями языка PHP
        /// В 4 файлах из 7 есть ошибки PHP, проверяется, что ошибочные файлы изымаются из анализа
        /// </summary>
        [TestMethod]
        public void PhpParser_PHPErrors()
        {
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                PHP_ERROR_SOURCES_SUBDIRECTORY,                             //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                         //prepareStorage                
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, PHP_ERROR_SOURCES_SUBDIRECTORY));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\PHPerror");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"PHPerror");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);
                    CheckMessage("Обнаружены ошибки PHP:");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"PHPerror_Reports");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Граничный тест. Исходные тексты на другом языке (CSharp)
        /// количество исходных файлов - 2 
        /// </summary>
        [TestMethod]
        public void PhpParser_2level1_Other_Lang()
        {
            string sourcePostfixPart = Path.Combine(PHP_SIMPLE_SOURCES_SUBDIRECTORY, "020");

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                new PHPParser(),                                            //plugin
                false,                                                       //isUseCachedStorage                
                (storage, testMaterialsPath) =>
                {

                },                                                          //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, false);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Некоретно заданы настройки - нет стартового файла");

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        #region Test Statements
        /// <summary>
        /// Проверка определения конструкции do-while по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_DoWhileStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "do_while");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\do_while\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\do_while\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\do_while\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции for по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_ForStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "for");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\for\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\for\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\for\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции foreach по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_ForeachStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "foreach");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\foreach\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\foreach\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\foreach\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции goto по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_GotoStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "goto");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\goto\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\goto\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\goto\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции if-then по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_IfThenStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "if_then");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\if_then\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\if_then\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\if_then\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции if-then-else по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_IfThenElseStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "if_then_else");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\if_then_else\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\if_then_else\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\if_then_else\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции return по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_ReturnStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "return");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\return\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\return\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\return\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции switch по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_SwitchStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "switch");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\switch\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\switch\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\switch\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции throw по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_ThrowStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "throw");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\throw\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\throw\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\throw\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        //Ограничение парсера - конструкция try-catch-finally не распознается
        /// <summary>
        /// Проверка определения конструкции try-catch-finally по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_TryCatchFinallyStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "try_catch_finally");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\try_catch_finally\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\try_catch_finally\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\try_catch_finally\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции while по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_WhileStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "while");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\while\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\while\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\while\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции break по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_BreakStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "break");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\break\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\break\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\break\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции continue по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void PhpParser_ContinueStatement()
        {
            string sourcePostfixPart = Path.Combine(PHP_TEST_STATEMENTS_SUBDIRECTORY, "continue");
            PHPParser testPlugin = new PHPParser();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage                
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    SettingUpPlugin(storage, 1, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"Dump\TestStatements\continue\");
                    string labsSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements\continue\");

                    CheckLabs(storage, labsSamplesDirectoryPath);
                    CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                    return true;
                },                                                          //checkStorage                
                (reportsFullPath, testMaterialsPath) =>
                {
                    string reportSamplesDirectoryPath = Path.Combine(testMaterialsPath, SAMPLES_SUBDIRECTORY, @"TestStatements_Reports\continue\");
                    //testPlugin.GenerateStatements(reportsFullPath);

                    //CheckXML(reportSamplesDirectoryPath, reportsFullPath);

                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        #endregion Test Statements

        #region BUGS
        /// <summary>
        /// Баг с вылетом PhpParser при работе по 2 уровню после JSParser.
        /// </summary>
        [TestMethod]
        public void PhpParser_Bug1FailAfterJS()
        {
            string sourcePostfixPart = @"PhpParser\Bugs\1FailAfterJS";

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                new PHPParser(),                                            //plugin
                false,                                                       //isUseCachedStorage                
                null,                                                          //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SettingUpStorage(storage, Path.Combine(testMaterialsPath, Path.Combine(sourcePostfixPart, "src")));
                    TestUtils.TestUtilsClass.Run_JavaScriptParser(storage);
                    SettingUpPlugin(storage, 1000, true);
                },                                                          //customizeStorage                
                (storage, testMaterialsPath) =>
                {
                    return true;
                },                                                          //checkStorage                
                (a,b)=>true,   //checkreports
                false,                                                      //isUpdateReport
                null,
                null,
                null
                );
        }
        #endregion

        /// <summary>
        /// Настройка Хранилища.
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="path">Путь к анализируемой директории. Не может быть пустым.</param>
        private void SettingUpStorage(Storage storage, string path)
        {
            TestUtilsClass.Run_FillFileList(storage, path);
            TestUtilsClass.Run_IdentifyFileTypes(storage);
        }

        /// <summary>
        /// Настройка плагина
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="firstSensorNumber">Номер первого датчика. Не может быть отрицательным.</param>
        /// <param name="isLevel2">true - 2-ой уровень.</param>
        private void SettingUpPlugin(Storage storage, UInt64 firstSensorNumber, bool isLevel2)
        {
            string sensorLine = "$RNThandle = fopen(\"c:\\RNTSens.txt\", \"a\"); fputs($RNThandle,\"#\\n\"); fclose($RNThandle);";

            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(sensorLine);
            buffer.Add(firstSensorNumber);
            buffer.Add((isLevel2) ? (UInt64)2 : (UInt64)3);
            storage.pluginSettings.SaveSettings(pluginID, buffer);
        }

        /// <summary>
        /// Дамп Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <returns>Путь до каталога, куда был произведен дамп Хранилища</returns>
        private string Dump(Storage storage)
        {
            string dumpDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.STORAGE), "Dump");

            if (!Directory.Exists(dumpDirectoryPath))
                Directory.CreateDirectory(dumpDirectoryPath);

            storage.ClassesDump(dumpDirectoryPath);
            storage.FilesDump(dumpDirectoryPath);
            storage.FunctionsDump(dumpDirectoryPath);
            storage.VariablesDump(dumpDirectoryPath);

            return dumpDirectoryPath;
        }

        /// <summary>
        /// Проверка дампа Хранилища
        /// </summary>
        /// <param name="dumpDirectoryPath">Путь до каталога дампа в Хранилище. Не может быть пустым.</param>
        /// <param name="dumpSamplesDirectoryPath">Путь до каталога с эталонным дампом. Не может быть пустым.</param>
        private void CheckDump(string dumpDirectoryPath, string dumpSamplesDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(dumpDirectoryPath, dumpSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Дамп Хранилища не совпадает с эталонными.");
        }

        /// <summary>
        /// Проверка лабораторных исходных файлов
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="labSamplesDirectoryPath">Путь до каталога с эталонными лабораторными исходными текстами. Не может быть пустым.</param>
        private void CheckLabs(Storage storage, string labSamplesDirectoryPath)
        {
            string labsDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "PHP");

            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(labsDirectoryPath, labSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Исходные тексты со вставленными датчиками не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка отчетов
        /// </summary>
        /// <param name="xmlSampleDirectoryPath">Путь до каталога с эталонными отчетами. Не может быть пустым.</param>
        /// <param name="storageReportsDirectoryPath">Путь до каталога с отчетами в Хранилище. Не может быть пустым.</param>
        private void CheckXML(string xmlSampleDirectoryPath, string storageReportsDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_XML_Compare(storageReportsDirectoryPath, xmlSampleDirectoryPath);

            Assert.IsTrue(isEqual, "Отчет по выражениям хранилища (statements.xml) отличается от эталона.");
        }

        /// <summary>
        /// Проверка ожидаемого сообщения в логе монитора
        /// </summary>
        /// <param name="message">Текст сообщения. Не может быть пустым.</param>
        private void CheckMessage(string message)
        {
            bool isContainMessage = listener.ContainsPart(message);

            Assert.IsTrue(isContainMessage, "Сообщение не совпадает с ожидаемым: <" + message + ">.");
        }

        /// <summary>
        /// Проверка количества ожидаемых сообщений в логе монитора
        /// </summary>
        /// <param name="messageCount">Количество ожидаемых сообщений. Не может быть отрицательным.</param>
        private void CheckMessageCount(uint messageCount)
        {
            bool isEqual = (uint)listener.Count() == messageCount;

            Assert.IsTrue(isEqual, "Количество предупреждений (сообщений listener) отлично от " + messageCount.ToString());
        }
    }
}
