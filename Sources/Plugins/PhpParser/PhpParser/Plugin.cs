using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Plugins.Parsers.CommonUtils;

using IA.Extensions;

namespace IA.Plugins.Parsers.PhpParser
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Парсер PHP";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.PHP_PARSER;

        /// <summary>
        /// Наименования тасков плагина (Выполнение)
        /// </summary>
        internal enum Tasks
        {
            [Description("Выделение имён функций и классов")]
            FUNC_CLASS_NAMES,
            [Description("Синтаксический анализ кода")]
            CODE_ANALIZE,
            [Description("Занесение информации о переменных и полях")]
            VARIABLES,
            [Description("Вставка датчиков")]
            SENSORS,
            [Description("Замена в исходниках Extended")]
            REMOVE_EXTENDS,
            [Description("Синтаксический разбор файлов (Фалангер)")]
            PHALANGER,
            [Description("Восстановление в исходниках Extended")]
            RESTORE_EXTENDS
        };

        /// <summary>
        /// Наименования тасков плагина (Генерация отчётов)
        /// </summary>
        internal enum ReportTasks
        {
            [Description("Генерация отчёта")]
            REPORT_GENERATION,
            [Description("Копирование отчёта")]
            REPORT_COPY_LOCAL
        };

        #region Настройки
        /// <summary>
        /// Текст датчика
        /// </summary>
        internal static string SensorText;

        /// <summary>
        /// Номер первого датчика
        /// </summary>
        internal static ulong FirstSensorNumber;

        /// <summary>
        /// Уровень НДВ - допустимые значения - 2 и 3
        /// </summary>
        internal static ulong NdvLevel;

        #endregion

    }

    /// <summary>
    /// Реализация класса плагина
    /// </summary>
    public class PHPParser : IA.Plugin.Interface
    {

        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Окно настроек плагина
        /// </summary>
        IA.Plugin.Settings settings;

        #region PluginInterface Members
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
#if DEBUG001
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS;
#else
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
#endif
            }
        }

        /// <summary>
        /// Метод генерации отчетов для тестов
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до каталога с отчетами плагина</param>
        public void GenerateStatements(string reportsDirectoryPath)
        {            
            (new StatementPrinter(Path.Combine(reportsDirectoryPath, "statements.xml"), storage)).Execute();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
#if DEBUG
            //Проверка на разумность
            if (storage == null)
                throw new Exception("Хранилище не определено.");

            //Формируем отчёт по стейтментам
            //(new StatementPrinter(Path.Combine(reportsDirectoryPath, "statements.xml"), storage)).Execute();

            //Отображаем прогресс
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.ReportTasks.REPORT_GENERATION.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 1);
#endif
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.SensorText = Properties.Settings.Default.SensorText;
            PluginSettings.FirstSensorNumber = Properties.Settings.Default.FirstSensorNumber;
            PluginSettings.NdvLevel = Properties.Settings.Default.NdvLevel;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.SensorText = settingsBuffer.GetString();
            PluginSettings.FirstSensorNumber = settingsBuffer.GetUInt64();
            PluginSettings.NdvLevel = settingsBuffer.GetUInt64();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.FirstSensorNumber = (storage != null && storage.sensors.Count() > 0) ? storage.sensors.EnumerateSensors().Max(s => s.ID) + 1 : 1;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "NDV:3":
                    PluginSettings.NdvLevel = 3;
                    break;
                case "NDV:2":
                    PluginSettings.NdvLevel = 2;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            if (storage != null && storage.sensors.Count() > 0 && storage.sensors.EnumerateSensors().Max(s => s.ID) >= PluginSettings.FirstSensorNumber)
            {
                ulong tmp_FirstSensorNumber = PluginSettings.FirstSensorNumber;
                PluginSettings.FirstSensorNumber = storage.sensors.EnumerateSensors().Max(s => s.ID) + 1;
                Monitor.Log.Warning(PluginSettings.Name, "Расстановка датчиков с номера <" + tmp_FirstSensorNumber + "> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <" + PluginSettings.FirstSensorNumber + ">.");
            }            

            //Получаем количество PHP файлов

            //ulong phpFilesCount = (PluginSettings.bSourceFromOrig) ? (ulong)(storage.files.EnumerateFiles().Where(f => f.fileType.ShortTypeDescription() == "PHP Script Page" && f.fileType.ContainsLanguages().Contains("PHP"))).Count() : (ulong)(storage.files.EnumerateFiles().Where(f => f.fileType.ShortTypeDescription() == "PHP Script Page" && f.fileType.ContainsLanguages().Contains("PHP"))).Count(); //FIXME - когда появятся очищенные файлы в хранилище
            ulong phpFilesCount = (ulong)(storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Where(f => f.fileType.ShortTypeDescription() == "PHP Script Page" && f.fileType.ContainsLanguages().Contains("PHP"))).Count();

            //Обновляем таски
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.REMOVE_EXTENDS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, storage.files.Count());
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.PHALANGER.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.RESTORE_EXTENDS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNC_CLASS_NAMES.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CODE_ANALIZE.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.VARIABLES.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SENSORS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount);

            string sourcePath = storage.appliedSettings.FileListPath;
            string reportFolder = storage.pluginData.GetDataFolder(PluginSettings.ID);
            string outputPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "PHP");
            
            string startFile = LocatePHPStartFile();
            startFile = (storage != null && startFile != null) ? Path.Combine(sourcePath, startFile) : null;

            Phalanger stage0 = new Phalanger(storage, startFile, sourcePath, reportFolder);
            if (stage0.Run(phpFilesCount) == false) // Есть ошибки
                return;  
            string PhalangerReportPath = reportFolder + "\\Understanded.log";

            ulong maxDefinedIdBeforePHP = 0;
            try
            {
                maxDefinedIdBeforePHP = storage.functions.EnumerateFunctions(enFunctionKind.ALL).Max(x => x.Id);
            }
            catch (Exception)
            {
                // no functions
            }
            

            AST_Pass1 stage1 = new AST_Pass1(storage, PhalangerReportPath);
            AST_Pass2 stage2 = new AST_Pass2(storage, PhalangerReportPath);
            AST_Pass3 stage3 = new AST_Pass3(storage, PhalangerReportPath);

            SensorsAndBracesPlacing stage4 = new SensorsAndBracesPlacing(storage, stage2.Braces, maxDefinedIdBeforePHP);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.SensorText = PluginSettings.SensorText;
            Properties.Settings.Default.FirstSensorNumber = PluginSettings.FirstSensorNumber;
            Properties.Settings.Default.NdvLevel = PluginSettings.NdvLevel;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.SensorText);
            settingsBuffer.Add(PluginSettings.FirstSensorNumber);
            settingsBuffer.Add(PluginSettings.NdvLevel);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = String.Empty;

            if (settings != null)
                settings.Save();

            if (String.IsNullOrWhiteSpace(PluginSettings.SensorText))
            {
                message = "Текст датчика не задан.";
                return false;
            }
                        
            if(PluginSettings.NdvLevel != 2 && PluginSettings.NdvLevel != 3)
            {
                message = "Недопустимый уровень НДВ - допустимые значения 2 или 3";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new CustomSettings(
                    storage.sensors.EnumerateSensors().Count() == 0 
                    ? 1 
                    : storage.sensors.EnumerateSensors().Max(s => s.ID) + 1)
                    );
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>() 
                { 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.REMOVE_EXTENDS.Description(), 0), 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.PHALANGER.Description(), 0), 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.RESTORE_EXTENDS.Description(), 0), 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FUNC_CLASS_NAMES.Description(), 0), 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.CODE_ANALIZE.Description(), 0), 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.VARIABLES.Description(), 0), 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.SENSORS.Description(), 0) 
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return new List<Monitor.Tasks.Task>() 
                { 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.ReportTasks.REPORT_GENERATION.Description(), 1), 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.ReportTasks.REPORT_COPY_LOCAL.Description(), 0), 
                };
            }
        }
        #endregion

        /// <summary>
        /// Поиск стартового файла php
        /// </summary>
        /// <returns>искомый файл</returns>
        public string LocatePHPStartFile()
        {
            // 0. Подготовка
            List<string> lsFiles = GetPHPFiles();

            // 1. Ищется index.php в корне
            if (lsFiles.Contains("index.php"))
                return "index.php";
            // 2. Ищется любой в корне
            var ta = lsFiles.Where(x => !x.Contains("\\") && !x.Contains("/"));
            if (ta.Count() > 0)
                return ta.First();
            // 3. Ищется index.php везде
            var tb = lsFiles.Where(x => x.Contains("index.php"));
            if (tb.Count() > 0)
                return tb.First();
            // 4. Ищется любой везде
            return lsFiles.FirstOrDefault();
        }

        /// <summary>
        /// Формирование списка PHP файлов
        /// </summary>
        /// <returns></returns>
        public List<string> GetPHPFiles()
        {
            List<string> ret = new List<string>();
            // Отбираем только те файлы, которые являются исходными файлами php
            if (storage != null)
                foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Where(f => f.fileType.ShortTypeDescription() == "PHP Script Page" && f.fileType.ContainsLanguages().Contains("PHP")))
                    ret.Add(file.RelativeFileName_Original);
            
            return ret;
        }
    }
}
