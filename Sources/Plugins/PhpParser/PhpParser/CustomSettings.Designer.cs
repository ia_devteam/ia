﻿namespace IA.Plugins.Parsers.PhpParser
{
    public partial class CustomSettings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sensorText_groupBox = new System.Windows.Forms.GroupBox();
            this.sensorText_textBox = new System.Windows.Forms.TextBox();
            this.firstSensorNumber_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.firstSensorNumber_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.firstSensorNumber_label = new System.Windows.Forms.Label();
            this.ndvLevel_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ndv3_radioButton = new System.Windows.Forms.RadioButton();
            this.ndv2_radioButton = new System.Windows.Forms.RadioButton();
            this.ndvLevel_groupBox = new System.Windows.Forms.GroupBox();
            this.sensors_groupBox = new System.Windows.Forms.GroupBox();
            this.main_tableLayoutPanel.SuspendLayout();
            this.sensorText_groupBox.SuspendLayout();
            this.firstSensorNumber_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).BeginInit();
            this.ndvLevel_tableLayoutPanel.SuspendLayout();
            this.ndvLevel_groupBox.SuspendLayout();
            this.sensors_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.sensorText_groupBox, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.ndvLevel_groupBox, 0, 0);
            this.main_tableLayoutPanel.Controls.Add(this.sensors_groupBox, 0, 1);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 4;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(213, 210);
            this.main_tableLayoutPanel.TabIndex = 9;
            // 
            // sensorText_groupBox
            // 
            this.sensorText_groupBox.Controls.Add(this.sensorText_textBox);
            this.sensorText_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorText_groupBox.Location = new System.Drawing.Point(3, 113);
            this.sensorText_groupBox.Name = "sensorText_groupBox";
            this.sensorText_groupBox.Size = new System.Drawing.Size(207, 94);
            this.sensorText_groupBox.TabIndex = 1;
            this.sensorText_groupBox.TabStop = false;
            this.sensorText_groupBox.Text = "Текст датчика";
            // 
            // sensorText_textBox
            // 
            this.sensorText_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorText_textBox.Location = new System.Drawing.Point(3, 16);
            this.sensorText_textBox.Multiline = true;
            this.sensorText_textBox.Name = "sensorText_textBox";
            this.sensorText_textBox.Size = new System.Drawing.Size(201, 75);
            this.sensorText_textBox.TabIndex = 0;
            // 
            // firstSensorNumber_tableLayoutPanel
            // 
            this.firstSensorNumber_tableLayoutPanel.ColumnCount = 2;
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.Controls.Add(this.firstSensorNumber_numericUpDown, 1, 0);
            this.firstSensorNumber_tableLayoutPanel.Controls.Add(this.firstSensorNumber_label, 0, 0);
            this.firstSensorNumber_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstSensorNumber_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.firstSensorNumber_tableLayoutPanel.Name = "firstSensorNumber_tableLayoutPanel";
            this.firstSensorNumber_tableLayoutPanel.RowCount = 1;
            this.firstSensorNumber_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.Size = new System.Drawing.Size(201, 30);
            this.firstSensorNumber_tableLayoutPanel.TabIndex = 2;
            // 
            // firstSensorNumber_numericUpDown
            // 
            this.firstSensorNumber_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_numericUpDown.Location = new System.Drawing.Point(140, 5);
            this.firstSensorNumber_numericUpDown.Name = "firstSensorNumber_numericUpDown";
            this.firstSensorNumber_numericUpDown.Size = new System.Drawing.Size(58, 20);
            this.firstSensorNumber_numericUpDown.TabIndex = 0;
            // 
            // firstSensorNumber_label
            // 
            this.firstSensorNumber_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_label.AutoSize = true;
            this.firstSensorNumber_label.Location = new System.Drawing.Point(3, 8);
            this.firstSensorNumber_label.Name = "firstSensorNumber_label";
            this.firstSensorNumber_label.Size = new System.Drawing.Size(131, 13);
            this.firstSensorNumber_label.TabIndex = 1;
            this.firstSensorNumber_label.Text = "Номер первого датчика:";
            // 
            // ndvLevel_tableLayoutPanel
            // 
            this.ndvLevel_tableLayoutPanel.ColumnCount = 3;
            this.ndvLevel_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.ndvLevel_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.ndvLevel_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ndvLevel_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ndvLevel_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.ndvLevel_tableLayoutPanel.Controls.Add(this.ndv2_radioButton, 0, 0);
            this.ndvLevel_tableLayoutPanel.Controls.Add(this.ndv3_radioButton, 1, 0);
            this.ndvLevel_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ndvLevel_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.ndvLevel_tableLayoutPanel.Name = "ndvLevel_tableLayoutPanel";
            this.ndvLevel_tableLayoutPanel.RowCount = 1;
            this.ndvLevel_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ndvLevel_tableLayoutPanel.Size = new System.Drawing.Size(201, 30);
            this.ndvLevel_tableLayoutPanel.TabIndex = 3;
            // 
            // ndv3_radioButton
            // 
            this.ndv3_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ndv3_radioButton.AutoSize = true;
            this.ndv3_radioButton.Location = new System.Drawing.Point(103, 6);
            this.ndv3_radioButton.Name = "ndv3_radioButton";
            this.ndv3_radioButton.Size = new System.Drawing.Size(94, 17);
            this.ndv3_radioButton.TabIndex = 4;
            this.ndv3_radioButton.TabStop = true;
            this.ndv3_radioButton.Text = "3-ий уровень";
            this.ndv3_radioButton.UseVisualStyleBackColor = true;
            this.ndv3_radioButton.Click += new System.EventHandler(this.ndv3_radioButton_Click);
            // 
            // ndv2_radioButton
            // 
            this.ndv2_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ndv2_radioButton.AutoSize = true;
            this.ndv2_radioButton.Location = new System.Drawing.Point(3, 6);
            this.ndv2_radioButton.Name = "ndv2_radioButton";
            this.ndv2_radioButton.Size = new System.Drawing.Size(94, 17);
            this.ndv2_radioButton.TabIndex = 2;
            this.ndv2_radioButton.TabStop = true;
            this.ndv2_radioButton.Text = "2-ой уровень";
            this.ndv2_radioButton.UseVisualStyleBackColor = true;
            this.ndv2_radioButton.Click += new System.EventHandler(this.ndv2_radioButton_Click);
            // 
            // ndvLevel_groupBox
            // 
            this.ndvLevel_groupBox.Controls.Add(this.ndvLevel_tableLayoutPanel);
            this.ndvLevel_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ndvLevel_groupBox.Location = new System.Drawing.Point(3, 3);
            this.ndvLevel_groupBox.Name = "ndvLevel_groupBox";
            this.ndvLevel_groupBox.Size = new System.Drawing.Size(207, 49);
            this.ndvLevel_groupBox.TabIndex = 4;
            this.ndvLevel_groupBox.TabStop = false;
            this.ndvLevel_groupBox.Text = "Уровень НДВ";
            // 
            // sensors_groupBox
            // 
            this.sensors_groupBox.Controls.Add(this.firstSensorNumber_tableLayoutPanel);
            this.sensors_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensors_groupBox.Location = new System.Drawing.Point(3, 58);
            this.sensors_groupBox.Name = "sensors_groupBox";
            this.sensors_groupBox.Size = new System.Drawing.Size(207, 49);
            this.sensors_groupBox.TabIndex = 5;
            this.sensors_groupBox.TabStop = false;
            this.sensors_groupBox.Text = "Расстановка датчиков";
            // 
            // CustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(213, 210);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.sensorText_groupBox.ResumeLayout(false);
            this.sensorText_groupBox.PerformLayout();
            this.firstSensorNumber_tableLayoutPanel.ResumeLayout(false);
            this.firstSensorNumber_tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).EndInit();
            this.ndvLevel_tableLayoutPanel.ResumeLayout(false);
            this.ndvLevel_tableLayoutPanel.PerformLayout();
            this.ndvLevel_groupBox.ResumeLayout(false);
            this.sensors_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.NumericUpDown firstSensorNumber_numericUpDown;
        private System.Windows.Forms.GroupBox sensorText_groupBox;
        private System.Windows.Forms.TextBox sensorText_textBox;
        private System.Windows.Forms.TableLayoutPanel firstSensorNumber_tableLayoutPanel;
        private System.Windows.Forms.Label firstSensorNumber_label;
        private System.Windows.Forms.TableLayoutPanel ndvLevel_tableLayoutPanel;
        private System.Windows.Forms.RadioButton ndv3_radioButton;
        private System.Windows.Forms.RadioButton ndv2_radioButton;
        private System.Windows.Forms.GroupBox ndvLevel_groupBox;
        private System.Windows.Forms.GroupBox sensors_groupBox;
    }
}
