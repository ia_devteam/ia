﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Store;
using IA.Extensions;

namespace IA.Plugins.Parsers.PhpParser
{
    /// <summary>
    /// Класс для парсинга третьего прохода
    /// </summary>
    internal class AST_Pass3
    {
        //Хранилище
        private Storage storage;

        //Текущий файл
        private IFile currentFile;

        private IFunction currentFunction;
        private Class currentClass;

        /// <summary>
        /// Парсинг имен
        /// </summary>
        /// <param name="block"></param>
        private void ParseNames(string block, int linenumber)
        {
            ReportController reportcontroller = new ReportController();

            StringReader reader = new StringReader(block);

            string line, lstr = string.Empty;

            while ((line = reader.ReadLine()) != null)
            {
            beginparse:
                
                
                if (line != null && line != "" && lstr == line)
                {
                    line = reader.ReadLine();
                }
                lstr = line;
                if (line == null)
                    break;
                string[] sa = line.Split('|');
                
                switch (sa[0].Trim())
                {
                    case "Function Decl":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(sa[1].Trim(), linenumber);

                            string name = reportcontroller.ExtractName(sa[1].Trim());
                            IFunction[] search = storage.functions.GetFunction(name);
                            IFunction find = null;
                            foreach(IFunction func in search)
                            {
                                Location funcDefenition = func.Definition();
                                if (funcDefenition != null && funcDefenition.GetFile() == currentFile && funcDefenition.GetLine() == location[0])
                                {
                                    find = func;
                                    break;
                                }
                            }
                            
                            if(find == null)
                            {
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Не найдена функция " + name + " в хранилище из файла " + currentFile.FileNameForReports);
                            }

                            Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            
                            //проверить, что в блоках нет вызовов
                            foreach (string key in retr.Keys)
                            {
                                switch (key.Split('|')[0].Trim())
                                {
                                    case "Function Body":
                                        {
                                            Dictionary<string, string> innerBlocks = reportcontroller.GetInnerBlocks(key, retr[key]);
                                            
                                            foreach (string innerKey in innerBlocks.Keys)
                                            {
                                                //вот оно тело функции
                                                IFunction oldCurr = currentFunction;
                                                currentFunction = find;
                                                ParseNames(innerBlocks[innerKey], linenumber);
                                                currentFunction = oldCurr;  
                                            }
                                            break;
                                        }
                                }
                            }

                            currentFunction = null;
                            goto beginparse;
                        }
                    case "Class/Interface Decl":
                        {
                            string name = reportcontroller.ExtractName(sa[1].Trim());
                            Class[] search = storage.classes.GetClass(name);
                            currentClass = null;
                            if(search.Length == 0)
                            {
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Не найден класс " + name + " в хранилище из файла " + currentFile.FileNameForReports);
                                currentClass = storage.classes.AddClass();
                                currentClass.Name = name;
                            }else
                            {
                                currentClass = search[0];
                            }

                            Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            
                            if (retr.Count == 0)
                                continue;
                            
                            foreach (string key in retr.Keys)
                            {
                                switch (key.Split('|')[0].Trim())
                                {
                                    case "Member":
                                        ParseNames(retr[key], linenumber);
                                        break;
                                }
                            }

                            currentClass = null;
                            
                            goto beginparse;
                        }
                    case "Class Method Decl":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(sa[1].Trim(), linenumber);

                            string name = reportcontroller.ExtractName(sa[1].Trim());
                            IFunction[] search = storage.functions.GetFunction(name);
                            IFunction newFunc = null;
                            foreach(IFunction fun in search)
                            {
                                Location funDefenition = fun.Definition();
                                if (funDefenition != null && funDefenition.GetFile() == currentFile && funDefenition.GetLine() == location[0])
                                    newFunc = fun;
                            }

                            if(newFunc == null)
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Не найдена функция " + name + " в хранилище из файла " + currentFile.FileNameForReports);

                            Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            //реализовать проверку того, что в блоках нет вызовов
                            foreach (string key in retr.Keys)
                            {
                                switch (key.Split('|')[0].Trim())
                                {
                                    case "METHOD Body":
                                        {
                                            Dictionary<string, string> innerBlocks = reportcontroller.GetInnerBlocks(key, retr[key]);
                                            foreach (string innerKey in innerBlocks.Keys)
                                            {
                                                //вот оно тело функции
                                                IFunction oldCurr = currentFunction;
                                                currentFunction = newFunc;
                                                ParseNames(innerBlocks[innerKey], linenumber);
                                                currentFunction = oldCurr;
                                            }
                                            break;
                                        }
                                }
                            }
                            goto beginparse;
                        }
                    case "Use Direct Variable":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(sa[1].Trim(), linenumber);
                            //если переменная - поле класса - сначала пробуем искать ее во внешнем классе
                            //если не найдено - во всех остальных
                            //иначе ищем переменную по всем переменным и если она в этом файле еще не была определена - определяем
                            string name = reportcontroller.ExtractName(sa[1].Trim(), true);
                            bool isClass = false;
                            if(name.Contains("."))
                                isClass = true;
                            if(name.Contains(".") && name.StartsWith("."))
                            {
                                if(currentClass != null)
                                    name = currentClass.FullName + name;
                            }
                            Store.Variable[] search;
                            if(name.Contains(".") == true) search = storage.variables.GetVariable(name.Split('.')); else search = storage.variables.GetVariable(name);

                            Store.Variable vrbl = null;
                            if(search.Length == 0)
                            { 
                                //не определена
                                if (isClass)
                                {
                                    name = name.Substring(name.IndexOf(".") + 1);
                                    Store.Variable[] search2 = storage.variables.GetVariable(name);
                                    if (search2.Length == 0)
                                    {
                                        vrbl = storage.variables.AddVariable();
                                        vrbl.Name = name;
                                        IA.Monitor.Log.Warning(PluginSettings.Name, "Поле " + name + " используемое в файле " + currentFile.FileNameForReports + " не найдено в хранилище");
                                        string access = reportcontroller.ExtractAccessType(sa[1].Trim());
                                        if (access.Contains("read"))
                                            vrbl.AddValueGetPosition(currentFile, location[0], location[1], currentFunction);
                                        if (access.Contains("write"))
                                            vrbl.AddValueSetPosition(currentFile, location[0], location[1], currentFunction);
                                        break;
                                    }
                                    else
                                    {
                                        foreach (Store.Variable vr in search2)
                                        {
                                            //не знаем к какой на самом деле это относится - проставляем всем
                                            string accss = reportcontroller.ExtractAccessType(sa[1].Trim());
                                            if (accss.Contains("read"))
                                                vr.AddValueGetPosition(currentFile, location[0], location[1], currentFunction);
                                            if (accss.Contains("write"))
                                                vr.AddValueSetPosition(currentFile, location[0], location[1], currentFunction);
                                        }
                                        break;
                                    }
                                }
                                else
                                {
                                    vrbl = storage.variables.AddVariable();
                                    vrbl.Name = name;
                                    vrbl.AddDefinition(currentFile, location[0], location[1], currentFunction);
                                    string access = reportcontroller.ExtractAccessType(sa[1].Trim());
                                    if (access.Contains("read"))
                                        vrbl.AddValueGetPosition(currentFile, location[0], location[1], currentFunction);
                                    if (access.Contains("write"))
                                        vrbl.AddValueSetPosition(currentFile, location[0], location[1], currentFunction);
                                    break;
                                }
                                
                            }else
                            {
                                foreach (Store.Variable vr in search)
                                {
                                    vrbl = vr;
                                    if (!isClass)
                                    {
                                        bool find = false;
                                        foreach (Location loc in vrbl.Definitions())
                                        {
                                            if (loc.GetFile() == currentFile)
                                            {
                                                find = true;
                                                break;
                                            }
                                        }
                                        if (!find)
                                            vrbl.AddDefinition(currentFile, location[0], location[1], currentFunction);
                                    }
                                    string access = reportcontroller.ExtractAccessType(sa[1].Trim());
                                    if (access.Contains("read"))
                                        vrbl.AddValueGetPosition(currentFile, location[0], location[1], currentFunction);
                                    if (access.Contains("write"))
                                        vrbl.AddValueSetPosition(currentFile, location[0], location[1], currentFunction);
                                }
                                break;
                            }
                        }
                    case "Use Global Constant":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(sa[1].Trim(), linenumber);

                            //у глобальных переменных нет области определения ;( надо бы с дефайном поработать
                            string name = reportcontroller.ExtractName(sa[1].Trim());
                            Store.Variable[] search = storage.variables.GetVariable(name);//это одно имя без точек
                            Store.Variable vrbl = null;
                            if (search.Length == 0)
                            {
                                vrbl = storage.variables.AddVariable();
                                vrbl.Name = name;
                            }
                            else
                            {
                                vrbl = search[0];//тут опять в нулевой результат идем
                            }
                            string access = reportcontroller.ExtractAccessType(sa[1].Trim());
                            if (access.Contains("read"))
                                vrbl.AddValueGetPosition(currentFile, location[0], location[1], currentFunction);
                            if (access.Contains("write"))
                                vrbl.AddValueSetPosition(currentFile, location[0], location[1], currentFunction);
                            break;
                        }
                    case "Use Class Constant":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(sa[1].Trim(), linenumber);

                            //у глобальных переменных нет области определения ;( надо бы с дефайном поработать
                            string name;
                            try
                            {
                                name = reportcontroller.ExtractName(sa[1].Trim());
                            }catch(Exception e)
                            {
                                name = "_SPECIAL_DYNAMIC_VAR_";
                            }
                            Store.Variable[] search = storage.variables.GetVariable(name);//это одно имя без точек
                            Store.Variable vrbl = null;
                            if (search.Length == 0)
                            {
                                vrbl = storage.variables.AddVariable();
                                vrbl.Name = name;
                            }
                            else
                            {
                                vrbl = search[0];//тут опять в нулевой результат идем
                            }
                            string access = reportcontroller.ExtractAccessType(sa[1].Trim());
                            if (access.Contains("read"))
                                vrbl.AddValueGetPosition(currentFile, location[0], location[1], currentFunction);
                            if (access.Contains("none"))
                                vrbl.AddValueGetPosition(currentFile, location[0], location[1], currentFunction);
                            break;
                        }
                    case "Use Static Field":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(sa[1].Trim(), linenumber);

                            string name;
                            try
                            {
                                name = reportcontroller.ExtractName(sa[1].Trim());
                            }
                            catch (Exception e)
                            {
                                name = "_SPECIAL_DYNAMIC_VAR_";
                            }
                            Store.Variable[] search;
                            if (name.Contains(".") == true) search = storage.variables.GetVariable(name.Split('.')); else search = storage.variables.GetVariable(name);
                            Store.Variable vrbl = null;
                            if (search.Length == 0)
                            {
                                vrbl = storage.variables.AddVariable();
                                vrbl.Name = name;
                            }
                            else
                            {
                                vrbl = search[0];//тут опять в нулевой результат идем
                            }
                            string access = reportcontroller.ExtractAccessType(sa[1].Trim());
                            if (access.Contains("read"))
                                vrbl.AddValueGetPosition(currentFile, location[0], location[1], currentFunction);
                            if (access.Contains("write"))
                                vrbl.AddValueSetPosition(currentFile, location[0], location[1], currentFunction);
                            break;
                        }
                    
                }
            }
        }

        /// <summary>
        /// Третий проход
        /// </summary>
        /// <param name="storage"></param>
        internal AST_Pass3(Storage storage, string PhalangerReportPath)
        {
            this.storage = storage;

            ulong count = 0;
            int linenumber = 0;
            if (File.Exists(PhalangerReportPath))
            using (StreamReader reader = new StreamReader(PhalangerReportPath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    linenumber++;
                beginparse:
                    if (line == null)
                        break;
                    string[] sa = line.Split('|');
                    switch (sa[0].Trim())
                    {
                        case "----- File":
                            {
                                //добавляем файл
                                currentFile = null;

                                //распознаём путь к файлу
                                string filePath = sa[1].Trim();
                                /* FIXED
                                if (PluginSettings.newPrefix != "")
                                    filePath = filePath.Replace(PluginSettings.oldPrefix, PluginSettings.newPrefix);
                                */

                                currentFile = storage.files.Find(filePath); //делаем его текущим
                                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.VARIABLES.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++count);

                                //вычленяем блок
                                StringBuilder block = new StringBuilder();
                                line = reader.ReadLine();
                                block.AppendLine(line);

                                if (line != null && line.Contains(" ----- File|")) //бывает при пустых файлах
                                {
                                    goto beginparse;
                                }

                                currentFunction = storage.functions.GetFunction(filePath)[0];
                                while ((line = reader.ReadLine()) != null && !line.StartsWith(" ----- File|"))
                                    block.AppendLine(line);

                                //currFunc = storage.functions.GetFunction(
                                ParseNames(block.ToString(), linenumber);
                                currentClass = null;



                                goto beginparse;
                            }
                    }
                }
            }
        }
    }
}
