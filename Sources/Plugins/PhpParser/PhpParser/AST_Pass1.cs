﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Store;
using IA.Extensions;

namespace IA.Plugins.Parsers.PhpParser
{
    /// <summary>
    /// Класс для первого прохода парсера
    /// </summary>
    internal class AST_Pass1
    {
        //Хранилище
        private Storage storage;

        private IFile currentFile;
        private IFunction currentFunction;
        private Class currentClass;

        /// <summary>
        /// Парсинг имен
        /// </summary>
        /// <param name="block"></param>
        private void ParseNames(string block, int linenumber)
        {
            ReportController reportcontroller = new ReportController();

            string line, lline = string.Empty;
            StringReader reader = new StringReader(block);
            //using (StringReader reader = new StringReader(block))
            //{
                while ((line = reader.ReadLine()) != null)
                {
                beginparse:
                    
                    if (!String.IsNullOrEmpty(line) && lline == line)
                        line = reader.ReadLine();
                    
                    lline = line;
                    
                    if (line == null)
                        break;

                    string[] split = line.Split('|');

                    switch (split[0].Trim())
                    {
                        case "Function Decl":
                            {
                                
                                IFunction function = storage.functions.AddFunction();
                                function.Name = reportcontroller.ExtractName(split[1].Trim());

                                ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber);
                                function.AddDefinition(currentFile, location[0], location[1]);
                                
                                //if (currFunc != null)
                                //    currFunc.AddInnerFunction(newFunc);

                                Dictionary<string, string> innerBlocks = reportcontroller.GetInnerBlocks(ref reader, ref line);

                                //проверить, что в блоках нет вызовов
                                foreach (string key in innerBlocks.Keys)
                                {
                                    switch (key.Split('|')[0].Trim())
                                    {
                                        case "Function Body":
                                            {
                                                Dictionary<string, string> body_innerBlocks = reportcontroller.GetInnerBlocks(key, innerBlocks[key]);

                                                foreach (string innerKey in body_innerBlocks.Keys)
                                                {
                                                    IFunction previousFunction = currentFunction;
                                                    currentFunction = function;
                                                    ParseNames(body_innerBlocks[innerKey], linenumber);
                                                    currentFunction = previousFunction;
                                                }
                                                break;
                                            }
                                        case "Parameter":
                                            {
                                                if (!key.Split('|')[1].Trim().Contains("Name=["))
                                                    break;

                                                Store.Variable parameter = null;
                                                string name = reportcontroller.ExtractName(key.Split('|')[1].Trim());

                                                if (storage.variables.GetVariable(name).Length == 0)
                                                {
                                                    parameter = storage.variables.AddVariable();
                                                    parameter.Name = name;
                                                }
                                                else
                                                    parameter = storage.variables.GetVariable(name)[0];

                                                ulong[] parameter_location = reportcontroller.ExtractLocation(key.Split('|')[1].Trim(), linenumber);
                                                parameter.AddDefinition(currentFile, parameter_location[0], parameter_location[1], function);
                                                parameter.AddValueSetPosition(currentFile, parameter_location[0], parameter_location[1], function);

                                                break;
                                            }
                                    }
                                }
                                goto beginparse;
                            }
                        case "Class/Interface Decl":
                            {
                                currentClass = storage.classes.AddClass();
                                currentClass.Name = reportcontroller.ExtractName(split[1].Trim());

                                Dictionary<string, string> innerBlocks = reportcontroller.GetInnerBlocks(ref reader, ref line);

                                if (innerBlocks.Count == 0)
                                    continue;

                                foreach (string key in innerBlocks.Keys)
                                {
                                    switch (key.Split('|')[0].Trim())
                                    {
                                        case "Member":
                                            ParseNames(innerBlocks[key], linenumber);
                                            break;
                                    }
                                }
                                goto beginparse;
                            }
                        case "Class Method Decl":
                            {

                                IFunction function = currentClass.AddMethod(false);
                                function.Name = reportcontroller.ExtractName(split[1].Trim());

                                ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber);
                                function.AddDefinition(currentFile, location[0], location[1]);

                                Dictionary<string, string> innerBlocks = reportcontroller.GetInnerBlocks(ref reader, ref line);
                                foreach (string key in innerBlocks.Keys)
                                {
                                    switch (key.Split('|')[0].Trim())
                                    {
                                        case "METHOD Body":
                                            {
                                                Dictionary<string, string> body_innerBlocks = reportcontroller.GetInnerBlocks(key, innerBlocks[key]);
                                                foreach (string innerKey in body_innerBlocks.Keys)
                                                {
                                                    IFunction previousFunction = currentFunction;
                                                    currentFunction = function;
                                                    ParseNames(body_innerBlocks[innerKey], linenumber);
                                                    currentFunction = previousFunction;
                                                }
                                                break;
                                            }
                                        case "Parameter":
                                            {
                                                if (!key.Split('|')[1].Trim().Contains("Name=["))
                                                    break;

                                                Store.Variable parameter = null;
                                                string name = reportcontroller.ExtractName(key.Split('|')[1].Trim());

                                                if (storage.variables.GetVariable(name).Length == 0)
                                                {
                                                    parameter = storage.variables.AddVariable();
                                                    parameter.Name = name;
                                                }
                                                else
                                                    parameter = storage.variables.GetVariable(name)[0];

                                                ulong[] parametr_location = reportcontroller.ExtractLocation(key.Split('|')[1].Trim(), linenumber);
                                                parameter.AddDefinition(currentFile, parametr_location[0], parametr_location[1], function);
                                                parameter.AddValueSetPosition(currentFile, parametr_location[0], parametr_location[1], function);

                                                break;
                                            }
                                    }
                                }
                                goto beginparse;
                            }
                        case "Field":
                            {
                                ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber);

                                Store.Variable field = currentClass.AddField(false);
                                field.AddDefinition(currentFile, location[0], location[1], null);
                                field.Name = reportcontroller.ExtractName(split[1].Trim());
                                break;
                            }
                        case "Parameter":
                            {
                                if (!split[1].Contains("Name=["))
                                    break;

                                Store.Variable parameter = null;
                                string name = reportcontroller.ExtractName(split[1].Trim());
                                if (storage.variables.GetVariable(name).Length == 0)
                                {
                                    parameter = storage.variables.AddVariable();
                                    parameter.Name = name;
                                }
                                else
                                    parameter = storage.variables.GetVariable(name)[0];

                                ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber);
                                parameter.AddDefinition(currentFile, location[0], location[1], currentFunction);
                                parameter.AddValueSetPosition(currentFile, location[0], location[1], currentFunction);

                                break;
                            }
                    }
                }
            //}
        }

        /// <summary>
        /// Первый проход парсера
        /// </summary>
        /// <param name="storage"></param>
        internal AST_Pass1(Storage storage, string PhalangerReportPath)
        {
            this.storage = storage;

            ulong count = 0;
            int linenumber = 0;
            if (File.Exists(PhalangerReportPath))
            using (StreamReader reader = new StreamReader(PhalangerReportPath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    linenumber++;
                beginparse:
                    if (line == null)
                        break;
                    string[] sa = line.Split('|');
                    switch (sa[0].Trim())
                    {
                        case "----- File":
                            {
                                //Выясняем путь к файлу
                                //string currentFilePath = sa[1].Trim().Replace(System.IO.Path.GetDirectoryName(storage.appliedSettings.FileListPath), "").Trim('\\');
                                string currentFilePath = sa[1].Trim();

                                //Отображаем информацию об обрабатываемом файле
                                IA.Monitor.Status.ShowMessage(Monitor.Status.MessageType.PERMANENT,PluginSettings.Tasks.FUNC_CLASS_NAMES.Description()+" Обработка файла: " + currentFilePath);
                                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNC_CLASS_NAMES.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++count);

                                //Задаём новый текущий файл и проверяем, что он есть в Хранилище
                                if ((currentFile = storage.files.Find(currentFilePath)) == null)
                                    throw new Exception("Файл " + currentFilePath + " не найден в Хранилище.");

                                //Вычисляем содержимое файла
                                if ((line = reader.ReadLine()) == null || line.Contains(" ----- File|")) //бывает при пустых файлах
                                {
                                    goto beginparse;
                                }

                                StringBuilder block = new StringBuilder();
                                do
                                    block.AppendLine(line);
                                while ((line = reader.ReadLine()) != null && !line.StartsWith(" ----- File|"));

                                //Обрабатываем блок
                                ParseNames(block.ToString(), linenumber);

                                //Устанавливаем значения текущего класса и функции в неопределённое значение
                                currentClass = null;
                                currentFunction = null;

                                //Отображение прогресса

                                goto beginparse;
                            }
                    }
                }
            }
        }
    }
}
