﻿using System;
using System.Windows.Forms;

namespace IA.Plugins.Parsers.PhpParser
{
    /// <summary>
    /// Класс, реализующий элемент управления настроек плагина
    /// </summary>
    public partial class CustomSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public CustomSettings(ulong minimumStartSensor)
        {
            InitializeComponent();

            firstSensorNumber_numericUpDown.Minimum = minimumStartSensor;
            firstSensorNumber_numericUpDown.Maximum = ulong.MaxValue;
            firstSensorNumber_numericUpDown.Value = 
                (PluginSettings.FirstSensorNumber > minimumStartSensor)
                    ? PluginSettings.FirstSensorNumber
                    : minimumStartSensor;

            ndv2_radioButton.Checked = (PluginSettings.NdvLevel == 2);
            ndv3_radioButton.Checked = !ndv2_radioButton.Checked;

            sensorText_textBox.Text = PluginSettings.SensorText;
        }

        /// <summary>
        /// Сохранение введённых настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.SensorText = sensorText_textBox.Text;
            PluginSettings.FirstSensorNumber = (ulong)firstSensorNumber_numericUpDown.Value;
            PluginSettings.NdvLevel = ndv2_radioButton.Checked ? (ulong)2 : (ulong)3;
        }

        private void ndv2_radioButton_Click(object sender, EventArgs e)
        {
            ndv2_radioButton.Checked = true;
            ndv3_radioButton.Checked = false;
        }

        private void ndv3_radioButton_Click(object sender, EventArgs e)
        {
            ndv3_radioButton.Checked = true;
            ndv2_radioButton.Checked = false;
        }

    }
}
