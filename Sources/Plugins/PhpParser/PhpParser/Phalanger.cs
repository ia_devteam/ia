﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Store;
using PHP.Core;
using System.Text.RegularExpressions;
using System.Threading;
using IA.Extensions;


namespace IA.Plugins.Parsers.PhpParser
{
    /// <summary>
    /// Класс для запуска Фалангера
    /// </summary>
    internal class Phalanger
    {
        const int max_Compiler = 10; // Константа - максимальное количество попыток исключения ошибочных PHP файлов

        private Storage storage;
        SortedList<string, SortedList<int, SortedList<int, int>>> slTransform = new SortedList<string, SortedList<int, SortedList<int, int>>>();
        string startFile;
        string sourceFolder;
        string reportFolder;
        string tempDirectory;
        static bool bRetCode = false;
        StreamWriter swLog = null;
        string G_ReplaceParent = null;
        int Line = 0;
        int Col = 0;
        Encoding encoding;
        static TextWriter errors;
        static TextErrorSink errorSink;
        string FileName = "";

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="startFile_"></param>
        /// <param name="sourceFolder_"></param>
        /// <param name="reportFolder_"></param>
        internal Phalanger(Storage storage, string startFile_, string sourceFolder_, string reportFolder_)
        {
            if (storage != null)
                this.storage = storage;
            if(!string.IsNullOrWhiteSpace(startFile_))
                startFile = startFile_.Trim('\\');
            if(!string.IsNullOrWhiteSpace(sourceFolder_))
                sourceFolder = sourceFolder_.Trim('\\');
            if(!string.IsNullOrWhiteSpace(reportFolder_))
                reportFolder = reportFolder_.Trim('\\');
        }

        /// <summary>
        /// Исполнение
        /// </summary>
        /// <returns>false - некорректные настройки</returns>
        internal bool Run(ulong phpFilesCount)
        {
            ulong phpErrorCount = 0; 
            string[] php_error_files;

            if (!(System.IO.File.Exists(startFile) && System.IO.Directory.Exists(sourceFolder) && System.IO.Directory.Exists(reportFolder)))
            {
                if (!(System.IO.File.Exists(startFile)))
                {
                    Monitor.Log.Error(PluginSettings.Name, "Некоретно заданы настройки - нет стартового файла");
                    return false;
                }
                if (!System.IO.Directory.Exists(sourceFolder))
                {
                    Monitor.Log.Error(PluginSettings.Name, "Некоретно заданы настройки - нет папки с исходниками");
                    return false;
                }
                if (!System.IO.Directory.Exists(reportFolder))
                {
                    Monitor.Log.Error(PluginSettings.Name, "Некоретно заданы настройки - нет папки с отчетами");
                    return false;
                }
            }


            tempDirectory = reportFolder + "\\" + string.Format(@"{0}", System.Guid.NewGuid()).ToLowerInvariant();
            Directory.CreateDirectory(tempDirectory);

            ReplaceExtends();


            // до N раз запускается фалангер для получения безошибочного результата компиляции
            // Файлы с ошибками изымаются из компиляции
            //for (int i_compiler = 0; i_compiler < max_Compiler; i_compiler++) - замена по просьбе Мити
            // Количество повторных запусков ограничено количеством обрабатываемых файлов
            for (int i_compiler = 0; i_compiler < (int)phpFilesCount; i_compiler++)
            {

                if (i_compiler > 0)
                {
                    string td = tempDirectory;
                    tempDirectory = reportFolder + "\\" + string.Format(@"{0}", System.Guid.NewGuid()).ToLowerInvariant();
                    Directory.CreateDirectory(tempDirectory);
                    foreach(FileInfo file in new DirectoryInfo(td).GetFiles("*.*",SearchOption.AllDirectories))
                    {
                        if (Directory.Exists(System.IO.Path.GetDirectoryName(file.FullName.Replace(td, tempDirectory))) == false)
                            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(file.FullName.Replace(td, tempDirectory)));
                        File.Move(file.FullName, file.FullName.Replace(td, tempDirectory));
                    }
                    if (Directory.Exists(td))
                        Directory.Delete(td, true);
                    
                    // Модификация всех счетчиков с учетом ошибочных файлов
                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.PHALANGER.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount - phpErrorCount);
                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.RESTORE_EXTENDS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount - phpErrorCount);
                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNC_CLASS_NAMES.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount - phpErrorCount);
                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CODE_ANALIZE.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount - phpErrorCount);
                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.VARIABLES.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount - phpErrorCount);
                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SENSORS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, phpFilesCount - phpErrorCount);

                }

                PhalangerCompiler.errorFile = reportFolder + "\\phpErrors.log";
                PhalangerCompiler.RootPath = tempDirectory;
                PhalangerCompiler.EntryPoint = ReplaceFirst(startFile, sourceFolder, tempDirectory).Trim('\\');
                PhalangerCompiler.preGo(reportFolder + "\\Understanded_without_extends.log");


                Thread tr = new System.Threading.Thread(PhalangerCompiler.Go);
                tr.Start();

                ulong count = 0;
                int TOTAL = 0;

                while (true)
                {
                    if (!tr.IsAlive)
                        break;
                    Thread.Sleep(500);
                    if (TOTAL == 0)
                    {
                        TOTAL = PhalangerCompiler.Total;
                        if (TOTAL == 0)
                            continue;
                        else
                        {
                        }
                    }

                    if (count != (ulong)PhalangerCompiler.Count)
                    {
                        count = (ulong)PhalangerCompiler.Count;
                        if (count == 0)
                            count = (ulong)TOTAL;
                        IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.PHALANGER.Description(), Monitor.Tasks.Task.UpdateType.STAGE, count);
                    }
                }
                if (count == 0)
                {
                    count = (ulong)TOTAL;
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.PHALANGER.Description(), Monitor.Tasks.Task.UpdateType.STAGE, count);
                }

                PhalangerCompiler.postGo();
                if (bRetCode == false)
                {
                    string serr = "Фалангер завершил работу с ошибками";
                    if (File.Exists(PhalangerCompiler.errorFile))
                    {
                        php_error_files = File.ReadAllLines(PhalangerCompiler.errorFile).Select(x => x.Replace(PhalangerCompiler.RootPath, string.Empty)).Select(x => x.Split('(').FirstOrDefault()).ToArray();
                        phpErrorCount += (ulong)php_error_files.Length;
                        foreach(string s in php_error_files)
                        {
                            if (File.Exists(tempDirectory + s))
                                File.Move(tempDirectory + s, tempDirectory + s + "_");
                        }
                        serr = "Обнаружены ошибки PHP:\n" + File.ReadAllText(PhalangerCompiler.errorFile).Replace(PhalangerCompiler.RootPath, string.Empty);
                    }
                    if (i_compiler + 1 < max_Compiler)
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружены ошибки PHP:\n" + serr);
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Попытка исключить ошибочные файлы (итерация " + (i_compiler + 1).ToString() + ")");
                        continue;
                    }
                    else
                    {
                        IA.Monitor.Log.Error(PluginSettings.Name, "Обнаружены ошибки PHP:\n" + serr);
                        IA.Monitor.Log.Error(PluginSettings.Name, "Попытка исключить ошибочные файлы за "+max_Compiler+" итераций не удалась");
                        return false;
                    }
                }
                break;
            }
            CorrPosExtends();
            LocateAllWrongPositions();

            if (File.Exists(reportFolder + "\\without_extends.log"))
                File.Delete(reportFolder + "\\without_extends.log");
            if (File.Exists(reportFolder + "\\Understanded_without_extends.log"))
                File.Delete(reportFolder + "\\Understanded_without_extends.log");
            if (File.Exists(reportFolder + "\\Understanded_without_extends1.log"))
                File.Delete(reportFolder + "\\Understanded_without_extends1.log");
            if (Directory.Exists(tempDirectory))
                Directory.Delete(tempDirectory, true);
            return true;
        }
        
        /// <summary>
        /// Удаление extends из кода PHP
        /// </summary>
        public void ReplaceExtends()
        {
            DirectoryInfo di = new DirectoryInfo(sourceFolder);
            FileInfo[] files;
            swLog = new StreamWriter(reportFolder+"\\without_extends.log");
            files = di.GetFiles("*.*", SearchOption.AllDirectories);



            ulong count = 0;
            foreach (FileInfo file in files)
            {
                swLog.WriteLine("----File| " + file.FullName);

                if (Directory.Exists(System.IO.Path.GetDirectoryName(file.FullName.Replace(sourceFolder, tempDirectory))) == false)
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(file.FullName.Replace(sourceFolder, tempDirectory)));
                string ReplaceParent = "";
                StreamReader sr = new StreamReader(file.FullName, Encoding.Default);
                string ss = sr.ReadLine();
                encoding = Encoding.Default;
                if (ss != null)
                {
                    if (ss.StartsWith("п»ї"))
                    {
                        encoding = Encoding.UTF8;
                    }
                    else if (ss.StartsWith("яю"))
                    {
                        encoding = Encoding.Unicode;
                    }
                    sr.Close();
                }
                sr = new StreamReader(file.FullName, encoding);
                StreamWriter sw = new StreamWriter(file.FullName.Replace(sourceFolder, tempDirectory), false, encoding);
                Console.WriteLine(file.FullName.Replace(di.FullName, ""));
                Line = 0;
                while (true)
                {
                    string s = sr.ReadLine();
                    if (s == null)
                        break;
                    Col = 0;
                    Line++;
                    string sResult = "";
                    Regex reg1 = new Regex(@"class\s+\w+\s+extends\s+\w+");
                    Match match = reg1.Match(s);
                    Regex reg2 = new Regex(@"class\s+\w+");
                    Regex reg3 = new Regex(@"\w+");
                    Regex reg4 = new Regex(@"parent::");
                    MatchCollection matches1 = reg1.Matches(s);
                    if (matches1.Count > 0)
                    {
                        MatchCollection matches2 = reg2.Matches(matches1[0].Value.ToString());
                        MatchCollection matches3 = reg3.Matches(matches1[0].Value.ToString());
                        ReplaceParent = matches3[matches3.Count - 1].Value.ToString() + "::";
                        G_ReplaceParent = ReplaceParent;
                        sResult = Regex.Replace(s, @"class\s+\w+\s+extends\s+\w+", new MatchEvaluator(ExtendsReplace));
                    }
                    else
                    {
                        
                        sResult = Regex.Replace(s,@"parent::",new MatchEvaluator(ParentReplace));
                    }
                    sw.WriteLine(sResult);
                }
                sr.Close();
                sw.Close();
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.REMOVE_EXTENDS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++count);

            }
            swLog.Close();
        }

        /// <summary>
        /// Функция для Regex
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        string ParentReplace(Match m)
        {
            Col += m.Value.ToString().Length - G_ReplaceParent.Length;
            swLog.WriteLine(Line.ToString() + ">" + (m.Index + 1).ToString() + ":" + (Col).ToString());
            return G_ReplaceParent;
        }

        /// <summary>
        /// Функция для Regex
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        string ExtendsReplace(Match m)
        {
            string s = m.Value.ToString();
            Regex reg2 = new Regex(@"class\s+\w+");
            MatchCollection matches = reg2.Matches(s);
            string s1 = matches[0].Value.ToString();

            Col += s.Length - s1.Length;
            swLog.WriteLine(Line.ToString() + ">" + (m.Index + 1).ToString() + ":" + (Col).ToString());
            return s1;
        }

        /// <summary>
        /// Чтение файла
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;
                buffer = new byte[length];
                if (length != fileStream.Read(buffer, 0, length))
                {
                    //error;
                }
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }

        /// <summary>
        /// Запись файла
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="buffer"></param>
        public void WriteFile(string filePath, byte[] buffer)
        {
            FileStream fileStream = null;
            try
            {
                if (Directory.Exists(System.IO.Path.GetDirectoryName(filePath)) == false)
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(filePath));
                fileStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
            }
            catch (UnauthorizedAccessException ex)
            {
                Monitor.Log.Error(PluginSettings.Name, "Ошибка доступа: " + ex.Message);
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();
            }
        }

        /// <summary>
        /// Кодирование по умолчанию
        /// </summary>
        /// <param name="arrayIn"></param>
        /// <returns></returns>
        public string DefaultEncoding(byte[] arrayIn)
        {
            string txt;
            txt = System.Text.Encoding.Default.GetString(arrayIn);
            encoding = Encoding.Default;
            if (txt.StartsWith("п»ї"))
            {
                txt = System.Text.Encoding.UTF8.GetString(arrayIn);
                encoding = Encoding.UTF8;
            }
            else if (txt.StartsWith("яю"))
            {
                txt = System.Text.Encoding.Unicode.GetString(arrayIn);
                encoding = Encoding.Unicode;
            }

            return txt;
        }

        /// <summary>
        /// Декодирование по умолчанию
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public byte[] DefaultDecoding(string s)
        {
            return encoding.GetBytes(s);
        }

        /// <summary>
        /// Восстановление extends в коде PHP
        /// </summary>
        private void CorrPosExtends()
        {
            string toReplacePath = storage.appliedSettings.FileListPath.Trim('\\');
            string fromReplacePath = tempDirectory;

            slTransform = new SortedList<string, SortedList<int, SortedList<int, int>>>();
            StreamReader sr = new StreamReader(reportFolder + "\\without_extends.log");
            int Maxi = 0;
            int Line = 0;
            int nL, nC, nD;
            while (true)
            {
                string s = sr.ReadLine();
                if (s == null)
                    break;
                Line++;
                if (s.StartsWith("----File"))
                {
                    string[] sa = s.Split('|');
                    if (sa.Length < 2)
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Некорректная структура отчета Extends. Строка " + Line);
                        sr.Close();
                        return;
                    }
                    FileName = sa[1].Trim().Replace(sourceFolder+"\\", "");
                    if (slTransform.Keys.Contains(FileName))
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Некорректная структура отчета Extends. Строка " + Line);
                        sr.Close();
                        return;
                    }
                    else
                    {
                        slTransform.Add(FileName, new SortedList<int, SortedList<int, int>>());
                    }
                }
                else
                {
                    string[] sa = s.Split('>');
                    if (sa.Length < 2)
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Некорректная структура отчета Extends. Строка " + Line);
                        sr.Close();
                        return;
                    }
                    if (!int.TryParse(sa[0], out nL))
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Некорректная структура отчета Extends. Строка " + Line);
                        sr.Close();
                        return;
                    }
                    string[] sb = sa[1].Split(':');
                    if (sb.Length < 2)
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Некорректная структура отчета Extends. Строка " + Line);
                        sr.Close();
                        return;
                    }
                    if (!int.TryParse(sb[0], out nC))
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Некорректная структура отчета Extends. Строка " + Line);
                        sr.Close();
                        return;
                    }
                    if (!int.TryParse(sb[1], out nD))
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Некорректная структура отчета Extends. Строка " + Line); 
                        sr.Close();
                        return;
                    }
                    SortedList<int, int> tmp;
                    if (!slTransform[FileName].TryGetValue(nL, out tmp))
                    {
                        slTransform[FileName].Add(nL, new SortedList<int, int>());
                    }
                    int itmp;
                    if (!slTransform[FileName][nL].TryGetValue(nC, out itmp))
                    {
                        slTransform[FileName][nL].Add(nC, nD);
                    }
                    else
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Некорректная структура отчета Extends. Строка " + Line);
                        sr.Close();
                        return;
                    }
                }
            }
            SortedList<string, SortedList<int, SortedList<int, int>>> slTransform1 = slTransform;
            slTransform = new SortedList<string, SortedList<int, SortedList<int, int>>>();
            foreach (string vvv in slTransform1.Keys)
            {
                if (slTransform1[vvv].Count > 0)
                {
                    slTransform.Add(vvv, new SortedList<int, SortedList<int, int>>());
                    foreach (int nnn in slTransform1[vvv].Keys)
                    {
                        slTransform[vvv].Add(nnn, new SortedList<int, int>());
                        foreach (int kkk in slTransform1[vvv][nnn].Keys)
                        {
                            slTransform[vvv][nnn].Add(kkk, slTransform1[vvv][nnn][kkk]);
                        }
                    }
                }
            }
            sr.Close();
            sr = new StreamReader(reportFolder + "\\Understanded_without_extends.log");
            while (true)
            {
                string s = sr.ReadLine();
                if (s == null)
                    break;
                if (s.StartsWith(" ----- File"))
                    Maxi++;
            }
            sr.Close();
            sr = new StreamReader(reportFolder + "\\Understanded_without_extends.log");
            StreamWriter sw = new StreamWriter(reportFolder + "\\Understanded_without_extends1.log");
            bool bNeedToTransform = false;

            string gs="";
            ulong count = 0;
            while (true)
            {
                try
                {
                    string s = sr.ReadLine();
                    gs = s;
                    if (s == null)
                        break;
                    if (s.StartsWith(" ----- File"))
                    {
                        IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.RESTORE_EXTENDS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++count);
                        string[] sa = s.Split('|');
                        if (sa.Length < 2)
                        {
                            break;
                        }
                        FileName = sa[1].Trim().Replace(fromReplacePath + "\\", "");
                        SortedList<int, SortedList<int, int>> tmp1;
                        if (slTransform.TryGetValue(FileName, out tmp1))
                            bNeedToTransform = true;
                        else
                            bNeedToTransform = false;
                        sw.WriteLine(ReplaceFirst(s, fromReplacePath + "\\", ""));

                    }
                    else
                    {
                        if (!bNeedToTransform)
                        {
                            sw.WriteLine(s);
                            continue;
                        }
                        s = Regex.Replace(s, @"\d+,\d+", new MatchEvaluator(ReplacePosition));
                        sw.WriteLine(s);
                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message+" : "+gs;
                }
            }
            sw.Close();
            sr.Close();
        }

        void LocateWrongPositions()
        {
            string[] ls1 = File.ReadAllLines(reportFolder + "\\Understanded_without_extends1.log");
            List<string> ls2 = new List<string>();
            for (int index1 = 0; index1 < ls1.Length; index1++)
            {
                string s2 = ls1[index1];
                Position r = new Position();
                if (s2.Contains("(-1,-1)-(-1,-1)"))
                {
                    Monitor.Log.Warning(PluginSettings.Name, "Ошибка позиционирования выражения (строка " + index1 + "): " + s2);

                    int level = Level(s2);
                    for (int index2 = index1 + 1; index2 < ls1.Length; index2++)
                    {
                        string s1 = ls1[index2];
                        if (Level(s1) <= level)
                            break;
                        Regex reg = new Regex(@"\(\d+,\d+\)-\(\d+,\d+\)");
                        MatchCollection matches = reg.Matches(s1);
                        foreach (Match match in matches)
                        {
                            if (match.Value == "(-1,-1)-(-1,-1)")
                                continue;
                            int[] iArr = match.Value.Split(new char[] { '(', ',', ')', '-' }).Where(x => x != string.Empty).Select(x => int.Parse(x)).ToArray();
                            r = ExpandPosition(r, new Position(iArr[0], iArr[1], iArr[2], iArr[3]));
                        }
                    }

                }
                if (r.ToString() == "(-1,-1)-(-1,-1)") // FIXME - иначе ошибка PHP Stage_2
                {
                    r = new Position(0, 0, 0, 0);
                    Monitor.Log.Warning(PluginSettings.Name, "Ошибка позиционирования выражения (строка " + index1 + "): " + s2);
                }
                ls2.Add(s2.Replace("(-1,-1)-(-1,-1)", r.ToString()));
            }
            File.WriteAllLines(reportFolder + "\\Understanded.log", ls2);
        }

        class PhalElement
        {
            public Position position;
            public Position maxchildren=new Position();
            public int level;
            public int index;
            public string line;
            public List<PhalElement> Children = new List<PhalElement>();
            public PhalElement Parent;
            public PhalElement(string s, int ind)
            {
                Regex reg = new Regex(@"\(\d+,\d+\)-\(\d+,\d+\)");
                int cnt = 1;
                MatchCollection matches = reg.Matches(s);
                foreach (Match match in matches)
                {
                    if (match.Value == "(-1,-1)-(-1,-1)")
                    {
                        Monitor.Log.Warning(PluginSettings.Name, "Ошибка позиционирования выражения (строка " + ind + "): " + match.Value);
                        position = new Position();
                        continue;
                    }
                    int[] iArr = match.Value.Split(new char[] { '(', ',', ')', '-' }).Where(x => x != string.Empty).Select(x => int.Parse(x)).ToArray();
                    position = new Position(iArr[0], iArr[1], iArr[2], iArr[3]);
                }
                index = ind;
                line = s;
                if (matches.Count > 0)
                    level = Level(s);
                else
                    level = -1;
                Parent = null;
            }
            public Position MaxPosition(PhalElement el)
            {
                if(el.Children.Count==0)
                    return new Position(el.position);
                else
                {
                    Position p = new Position(el.position);
                    foreach (PhalElement chel in el.Children)
                    {
                        p = ExpandPosition(p, MaxPosition(chel));
                    }
                    return p;
                }
            }
        }

        void LocateAllWrongPositions()
        {
            string[] ls1 = File.ReadAllLines(reportFolder + "\\Understanded_without_extends1.log");
            List<PhalElement> phel = new List<PhalElement>();
            for (int i = 0; i < ls1.Length; i++)
            {
                phel.Add(new PhalElement(ls1[i], i));
            }
            List<PhalElement> ast = new List<PhalElement>();
            PhalElement current=null;
            foreach(PhalElement el in phel)
            {
                if (el.level == -1)
                    continue;
                if(ast.Count==0)
                {
                    ast.Add(el);
                    current=el;
                }
                else
                {
                    if (current.level < el.level)
                    {
                        current.Children.Add(el);
                        if (el.Parent == null)
                            el.Parent = current;
                        current = el;
                    }
                    else
                    {
                        while (current != null && current.level >= el.level)
                            current = current.Parent;
                        if (current == null)
                        {
                            ast.Add(el);
                            current = el;
                        }
                        else
                        {
                            current.Children.Add(el);
                            if (el.Parent == null)
                                el.Parent = current;
                            current = el;
                        }
                    }
                }
            }

            foreach(PhalElement el in phel)
            {
                if(el.level!=-1)
                    el.maxchildren = el.MaxPosition(el);
            }

            List<string> ls2 = new List<string>();
            foreach(PhalElement el in phel)
            {
                string s = (el.position != null) ? s = el.line.Replace(el.position.ToString(), el.maxchildren.ToString()) : el.line;
                if (s.Contains("(-1,-1)-(-1,-1)"))
                    Monitor.Log.Warning(PluginSettings.Name, "Ошибка в позиционировании выражения (строка " + el.index + "): " + s);
                ls2.Add(s.Replace("(-1,-1)-(-1,-1)", "(0,0)-(0,0)"));
            }
            File.WriteAllLines(reportFolder + "\\Understanded.log", ls2);
        }

        static int Level(string s)
        {
            int L = 0;
            foreach(char c in s)
            {
                if (c != ' ')
                    break;
                L++;
            }
            return L;
        }

        public class Position
        {
            public int X1, Y1, X2, Y2;
            public Position(int x1 = -1, int y1 = -1, int x2 = -1, int y2 = -1)
            {
                X1 = x1;
                Y1 = y1;
                X2 = x2;
                Y2 = y2;
            }
            public Position(Position pp)
            {
                X1 = pp.X1;
                X2 = pp.X2;
                Y1 = pp.Y1;
                Y2 = pp.Y2;
            }
            public override string ToString()
            {
                return "(" + X1 + "," + Y1 + ")-(" + X2 + "," + Y2 + ")";
            }
        }

        public static Position ExpandPosition(Position r1, Position r2)
        {
            Position R = r1;
            if (R.X1 == -1)
                R.X1 = r2.X1;
            if (R.X2 == -1)
                R.X2 = r2.X2;
            if (R.Y1 == -1)
                R.Y1 = r2.Y1;
            if (R.Y2 == -1)
                R.Y2 = r2.Y2;


            if (R.X1 > r2.X1 && r2.X1 > -1)
            {
                R.X1 = r2.X1;
                R.Y1 = r2.Y1;
            }
            if (R.X1 == r2.X1 && R.Y1 > r2.Y1 && r2.Y1>-1)
            {
                R.Y1 = r2.Y1;
            }
            if (R.X2 < r2.X2)
            {
                R.X2 = r2.X2;
                R.Y2 = r2.Y2;
            }
            if (R.X2 == r2.X2 && R.Y2<r2.Y2)
            {
                R.Y2 = r2.Y2;
            }
            return R;
        }


        /// <summary>
        /// Функция для Regex
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        string ReplacePosition(Match m)
        {
            string[] sa = m.Value.ToString().Split(',');
            int nL, nC;
            if (!int.TryParse(sa[0].Trim('('), out nL))
            {
                return m.Value.ToString();
            }
            if (!int.TryParse(sa[1].Trim(')'), out nC))
            {
                return m.Value.ToString();
            }
            SortedList<int, int> tmp;
            if (!slTransform[FileName].TryGetValue(nL, out tmp))
            {
                return m.Value.ToString();
            }
            int nCol = 0;
            foreach (int vvv in slTransform[FileName][nL].Keys)
            {
                if (nC > vvv)
                    nCol = vvv;
            }
            if (nCol == 0)
            {
                return m.Value.ToString();
            }
            else
            {
                return "" + nL + "," + (nC + slTransform[FileName][nL][nCol]).ToString() + "";
            }
        }

        /// <summary>
        /// Класс для запуска компилятора Фалангера
        /// </summary>
        private class PhalangerCompiler
        {
            public static string RootPath
            {
                set;
                get;
            }
            public static string EntryPoint
            {
                set;
                get;
            }
            public static int Total
            {
                get
                {
                    return Debug.Log.Total;
                }
            }
            public static int Count
            {
                get
                {
                    return Debug.Log.Counter;
                }
            }
            public static string errorFile
            {
                set;
                get;
            }

            static public void Go()
            {
                string[] args = { "/root:" + RootPath, "/recurse:.", "/entrypoint:" + EntryPoint.Replace(RootPath, "").Trim('\\') };
                bRetCode = Compile(new List<string>(args));
            }
            static public void preGo(string filename)
            {
                bRetCode = false;
                Debug.Log.FileName = filename;
                Debug.Log.Refresh();
                Debug.Log.Level = 0;
                Debug.Log.Permission = true;

                errors = new StreamWriter(errorFile);
                errorSink = new TextErrorSink(errors);
            }
            static public void postGo()
            {
                Debug.Log.Flash();
                errors.Flush();
                errors.Close();
            }

            /// <summary>
            /// Компиляция списка файлов
            /// </summary>
            /// <param name="args">список файлов</param>
            /// <returns></returns>
            static public bool Compile(List<string> args)
            {
                //CONTEXT
                PHP.Core.ApplicationContext.DefineDefaultContext(false, true, false);
                PHP.Core.ApplicationContext app_context = PHP.Core.ApplicationContext.Default;

                CommandLineParser/*!*/ commandLineParser = new CommandLineParser();
                try
                {
                    commandLineParser.Parse(args);
                }
                catch (Exception)
                {
                    return false;
                }

                CompilerConfiguration compiler_config;
                try
                {
                    compiler_config = ApplicationCompiler.LoadConfiguration(app_context, commandLineParser.Parameters.ConfigPaths, Console.Out);
                    commandLineParser.Parameters.ApplyToConfiguration(compiler_config);
                }
                catch (Exception)
                {
                    return false;
                }

                try
                {
                    CommandLineParser p = commandLineParser;
                    Statistics.DrawGraph = p.DrawInclusionGraph;

                    errorSink.DisabledGroups = compiler_config.Compiler.DisabledWarnings;
                    errorSink.DisabledWarnings = compiler_config.Compiler.DisabledWarningNumbers;

                    // initializes log:
                    Debug.ConsoleInitialize(Path.GetDirectoryName(p.Parameters.OutPath));
                    
                    //FIXME_PHP - код ниже будет использован для предварительной проверки кода
                    //bool bNoErrors = new ApplicationCompiler().Compile1(app_context, compiler_config, errorSink, p.Parameters);
                    
                    new ApplicationCompiler().Compile(app_context, compiler_config, errorSink, p.Parameters);
                }
                catch (InvalidSourceException ex)
                {
                    ex.Report(errorSink);
                    return false;
                }
                catch (Exception ex)
                {
                    errorSink.AddInternalError(ex);
                    return false;
                }

                if (errorSink.ErrorCount > 0 || errorSink.FatalErrorCount > 0)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Ошибок: обычных -{0}, фатальных - {1}", errorSink.ErrorCount, errorSink.FatalErrorCount);
                    Console.WriteLine("");
                    Console.WriteLine("Работа программы прекращена");
                    return false;
                }
                return true;

            }
        }

        /// <summary>
        /// Замена первой найденной подстроки
        /// </summary>
        /// <param name="src"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        string ReplaceFirst(string src, string from, string to)
        {
            int index = src.ToLowerInvariant().IndexOf(from.ToLowerInvariant());
            if (index == -1)
                return src;
            else
                return src.Substring(0, index) + to + src.Substring(index + from.Length);
        }
    }
}
