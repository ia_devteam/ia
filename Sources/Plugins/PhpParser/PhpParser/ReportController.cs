﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Store;

namespace IA.Plugins.Parsers.PhpParser
{
    /// <summary>
    /// Класс для формирования отчета
    /// </summary>
    internal class ReportController
    {
        private List<IFile> files = null;

        internal int FilesCount(Storage storage, string PhalangerReportPath)
        {
            return files != null ? files.Count : GetFiles(storage, PhalangerReportPath).Count;
        }

        /// <summary>
        /// Получить список файлов
        /// </summary>
        /// <returns></returns>
        internal List<IFile> GetFiles(Storage storage, string PhalangerReportPath)
        {
            if (files != null)
                return files;

            files = new List<IFile>();

            using (StreamReader reader = new StreamReader(PhalangerReportPath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    if (line.Contains("----- File|"))
                    {
                        string file = line.Split('|')[1].Trim();
                        IFile result = storage.files.Find(file);
                        if(result != null)
                            files.Add(result);
                        else
                            IA.Monitor.Log.Error(PluginSettings.Name, "Файл " + file + " не зарегистрирован в Хранилище");
                    }
            }

            return files;
        }

        /// <summary>
        /// Получить глубину вложения из файла Фалангера
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        internal int GetLevel(string line)
        {
            for (int i = 0; i < line.Length; i++)
                if (line[i] != ' ')
                    return i;

            return line.Length;
        }

        /// <summary>
        /// Извлечь имя
        /// </summary>
        /// <param name="line"></param>
        /// <param name="isFullName"></param>
        /// <returns></returns>
        internal string ExtractName(string line, bool isFullName = false)
        {
            string[] keyWords = new string[] { "Function=[", "Method=[", "Name=[", "Jump=[", "Class_Type=[", "Indirect_Class_Type=[", "ArrayKey=[instance of "};

            int index = -1;
            foreach(string word in keyWords)
            {
                index = line.IndexOf(word);
                if(index != -1)
                {
                    index += word.Length;
                    break;
                }
            }

            if (index == -1)
                throw new Exception();

            string name = line.Substring(index);
            name = name.Substring(0, name.IndexOf(']'));

            //Полное имя
            if (isFullName)
                return name.Replace("::", ".").ToLower();

            //Имя
            string[] split = name.Split(':');
            return split[split.Length - 1].ToLower();
        }

        /// <summary>
        /// Извлечь поле
        /// </summary>
        /// <param name="line"></param>
        /// <param name="isFullName"></param>
        /// <returns></returns>
        internal string ExtractFieldName(string line, bool isFullName = false)
        {
            if (!line.Contains("Name=["))
                throw new Exception();

            string name = line.Substring(line.IndexOf("Name=[") + 6);
            name = name.Substring(0, name.IndexOf(']'));

            //Полное имя
            if(isFullName)
                return name.Replace("::", ".").ToLower();

            //Имя
            string[] split = name.Split(':');
            return split[split.Length - 1].ToLower();
        }

        /// <summary>
        /// Извлечь тип доступа
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        internal string ExtractAccessType(string line)
        {
            if (!line.Contains("Access=["))
                //throw new Exception();
                return "write"; // такое случается в случае статических переменных с неопределенным значением

            string name = line.Substring(line.IndexOf("Access=[") + 8);
            name = name.Substring(0, name.IndexOf(']'));
                        
            return name.ToLower();
        }

        /// <summary>
        /// Извлечь позицию
        /// </summary>
        /// <param name="line"></param>
        /// <param name="linenumber"></param>
        /// <param name="isFullLocation"></param>
        /// <returns></returns>
        internal ulong[] ExtractLocation(string line, int linenumber, bool isFullLocation = false)
        {
            
            if (line.Contains("(-1,-1)"))
            {
                Monitor.Log.Warning(PluginSettings.Name, "Ошибка позиционирования выражения (строка " + linenumber + "): " + line);
                IA.Monitor.Log.Error(PluginSettings.Name, "Неверно распознанны координаты ");
                return new ulong[] { 1, 0 };
            }

            string[] split = line.Split(' ');
            string locationString = String.Empty;

            foreach (string subline in split)
                if (subline.Trim().StartsWith("("))
                {
                    locationString = subline;
                    break;
                }

            if (locationString == String.Empty)
                return null;

            //Вычисление координат
            locationString = locationString.Replace("(-", "(").Replace(",-", ",");

            string locationString_start = locationString.Trim().Split('-')[0].Trim();
            string locationString_end = locationString.Trim().Split('-')[1].Trim();

            string[] location_start = locationString_start.Trim(new[] { '(', ')' }).Split(',');
            string[] location_end = locationString_end.Trim(new[] { '(', ')' }).Split(',');

            //Внимение! У фалангера позиции в строке начинаются с 1 а в хранилище с 0
            return isFullLocation ?
                new[] { Convert.ToUInt64(location_start[0]), Convert.ToUInt64(location_start[1]), Convert.ToUInt64(location_end[0]), Convert.ToUInt64(location_end[1])} :
                new[] { Convert.ToUInt64(location_start[0]), Convert.ToUInt64(location_start[1]) }; 
        }

        /// <summary>
        /// Считать блок
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        internal Dictionary<string, string> GetInnerBlocks(ref StringReader reader, ref string line)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();
            string contents = reader.ReadToEnd();
            reader = new StringReader(contents);

            //Узнаём текущий уровень строки, он же минимальный уровень, ниже которого опускаться нельзя
            int minLevel = GetLevel(line);

            line = reader.ReadLine();
            while (line != null && GetLevel(line) > minLevel)
            {
                string key = line;
                StringBuilder value = new StringBuilder();

                int minSubLevel = GetLevel(line);

                while ( (line = reader.ReadLine()) != null && (GetLevel(line) > minSubLevel || (GetLevel(line) <= minSubLevel && !Regex.IsMatch(line, "\\| \\(-?\\d+,-?\\d+\\)") && !line.Contains("----- File| "))))
                    value.AppendLine(line);

                if (ret.ContainsKey(key))
                    key += new string(' ', (new Random(10)).Next(5));

                ret.Add(key, value.ToString());
            }

            if(ret.Count() == 0)
                reader = new StringReader(contents); //если ничего не нашли - сбрасываем

            return ret;
        }

        /// <summary>
        /// Считать блок
        /// </summary>
        /// <param name="key"></param>
        /// <param name="block"></param>
        /// <returns></returns>
        internal Dictionary<string, string> GetInnerBlocks(string key, string block)
        {
            StringReader sr = new StringReader(block);
            return GetInnerBlocks(ref sr, ref key);
        }
    }
}
