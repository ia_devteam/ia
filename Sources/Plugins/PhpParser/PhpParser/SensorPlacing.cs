using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Store;
using System.Text.RegularExpressions;
using IA.Extensions;

namespace IA.Plugins.Parsers.PhpParser
{
    // Структура описывающая один элемент(сенсор) для вставки
    internal struct SensorInfo
    {
        public ulong id;
        public string file;
        public int line;
        public int column;
        public ulong offset;
        public bool isReturn;
    } ;

    /// <summary>
    /// Класс описывающий координаты пары скобок
    /// </summary>
    internal class BracesCoord
    {
        public ulong sRow;
        public ulong sColumn;
        public ulong eRow;
        public ulong eColumn;

        public BracesCoord(ulong sRow, ulong sColumn, ulong eRow, ulong eColumn)
        {
            this.sRow = sRow;
            this.sColumn = sColumn;
            this.eRow = eRow;
            this.eColumn = eColumn;
        }
    };

    /// <summary>
    /// Класс описывающий координаты реторна
    /// </summary>
    internal class ReturnCoord : IComparable<ReturnCoord>
    {
        public ulong sRow, sColumn, eRow, eColumn;
        public bool isEmpty;

        public ReturnCoord(ulong sRow, ulong sColumn, ulong eRow, ulong eColumn, bool isEmpty)
        {
            this.sRow = sRow;
            this.sColumn = sColumn;
            this.eRow = eRow;
            this.eColumn = eColumn;
            this.isEmpty = isEmpty;
        }

        int IComparable<ReturnCoord>.CompareTo(ReturnCoord coord)
        {
            if (sRow < coord.sRow)
                return 1;
            if (sRow > coord.sRow)
                return -1;
            if (sRow == coord.sRow)
            {
                if (sColumn < coord.sColumn)
                    return 1;
                if (sColumn > coord.sColumn)
                    return -1;
            }
            return 0;
        }
    };

    /// <summary>
    /// Класс для размещения скобок
    /// </summary>
    internal class SensorsAndBracesPlacing
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Путь к папке с исходными текстами
        /// </summary>
        private string sourceFolder;

        /// <summary>
        /// Путь к папке с исходными текстами со вставленными датчиками
        /// </summary>
        private string sourceWithSensorsFolder;

        /// <summary>
        /// Словарь скобок
        /// </summary>
        private Dictionary<string, List<BracesCoord>> braces;

        /// <summary>
        /// Список информации о датчиках
        /// </summary>
        private Dictionary<ulong, SensorInfo> sensors = new Dictionary<ulong, SensorInfo>();

        /// <summary>
        /// Список хранит информацию о том, куда нужно что вставить
        /// </summary>
        private SortedList<ulong, string> insertList = new SortedList<ulong, string>();

        /// <summary>
        /// Список файлов для обработки
        /// </summary>
        private List<string> files = new List<string>();

        /// <summary>
        /// Список файлов не содержищих код на PHP
        /// </summary>
        private List<string> nonPhpFiles = new List<string>();

        /// <summary>
        /// Список найденных реторнов
        /// </summary>
        private Dictionary<string, SortedSet<ReturnCoord>> returns = new Dictionary<string, SortedSet<ReturnCoord>>();

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="braces"></param>
        internal SensorsAndBracesPlacing(Storage storage, Dictionary<string, List<BracesCoord>> braces, ulong maxDefinedIdBeforePHP)
        {
            //Хранилище
            this.storage = storage;

            //Словарь скобок (формируется на 3-м проходе)
            this.braces = braces ?? new Dictionary<string, List<BracesCoord>>();

            //Устанавливаем значения папок
            sourceWithSensorsFolder = Store.Files.CanonizeFileName(Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "PHP"));

            sourceFolder = Store.Files.CanonizeFileName(storage.appliedSettings.FileListPath);

            //Заполняем информацию о реторнах
            //IA.Monitor.StatusMessage = " Загрузка информации о реторнах...";
            FillReturnInfo(storage.pluginData.GetDataFolder(PluginSettings.ID).Trim('\\') + "\\returndesc.log");

            storage.sensors.SetSensorStartNumber(PluginSettings.FirstSensorNumber);

            PasteSensors(maxDefinedIdBeforePHP, (int)PluginSettings.NdvLevel);
            //Заполняем информацию о датчиках
            //IA.Monitor.StatusMessage = " Добавление датчиков и скобок в Хранилище...";
            GetSensors(maxDefinedIdBeforePHP);

            //Заполняем список файлов не содержищих кода на PHP
            //IA.Monitor.StatusMessage = " Заполнение списка файлов, не содержащих PHP-код...";
            FillNonPhpFilesInfo(storage.pluginData.GetDataFolder(PluginSettings.ID).Trim('\\') + "\\NoPhpCodeFiles.txt");

            //Вставляем датчики
            Paste();
        }

        private void PasteSensors(ulong maxDefinedIdBeforePHP, int level = 2)
        {
            //формируем список функций
            List<IFunction> functions = new List<IFunction>();
            foreach (LocationFunctionPair lfp in storage.functions.EnumerateFunctionLocationsInSortOrder())
                if (lfp.location.GetFile().fileType.ContainsLanguages().Contains("PHP"))
                    if (lfp.function.Definition() != null && lfp.function.Id > maxDefinedIdBeforePHP)
                        functions.Add(lfp.function);

            foreach (IFunction function in functions)
            {
                if (level == 2 && function.EntryStatement != null)
                    PasterSecondLevel(function.EntryStatement);
                else if (level == 3 && function.EntryStatement != null)
                    PasterThirdLevel(function.EntryStatement);
            }
        }

        private void PasterSecondLevel(IStatement st, int level = 0)
        {
            if (st == null)
                return;

            bool metReturn = st is IStatementReturn || st is IStatementBreak || st is IStatementContinue || st is IStatementYieldReturn || st is IStatementThrow;

            if (metReturn && level > 0)
            {
                storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                return; //больше не надо ничего вставлять - встретили ретурн
            }
            else
                if (level == 0)
                    storage.sensors.AddSensorBeforeStatement(st, Kind.START);
                else
                    storage.sensors.AddSensorBeforeStatement(st, Kind.INTERNAL);

            while (st != null)
            {
                switch (st.Kind)
                {
                    case ENStatementKind.STBreak:
                        metReturn = true;
                        break;
                    case ENStatementKind.STContinue:
                        metReturn = true;
                        break;
                    case ENStatementKind.STDoAndWhile:
                        PasterSecondLevel((st as IStatementDoAndWhile).Body, level + 1);
                        break;
                    case ENStatementKind.STFor:
                        PasterSecondLevel((st as IStatementFor).Body, level + 1);
                        break;
                    case ENStatementKind.STForEach:
                        PasterSecondLevel((st as IStatementForEach).Body, level + 1);
                        break;
                    case ENStatementKind.STGoto:
                        break;
                    case ENStatementKind.STIf:
                        PasterSecondLevel((st as IStatementIf).ThenStatement, level + 1);
                        PasterSecondLevel((st as IStatementIf).ElseStatement, level + 1);
                        if ((st as IStatementIf).ElseStatement is IStatementIf && ((st as IStatementIf).ElseStatement as IStatementIf).SensorBeforeTheStatement != null)
                            storage.sensors.RemoveSensor(((st as IStatementIf).ElseStatement as IStatementIf).SensorBeforeTheStatement.ID);
                        break;
                    case ENStatementKind.STOperational:
                        break;
                    case ENStatementKind.STReturn:
                        metReturn = true;
                        break;
                    case ENStatementKind.STSwitch:
                        foreach (var switchElem in (st as IStatementSwitch).Cases())
                            PasterSecondLevel(switchElem.Value, level + 1);

                        PasterSecondLevel((st as IStatementSwitch).DefaultCase(), level + 1);
                        break;
                    case ENStatementKind.STThrow:
                        metReturn = true;
                        break;
                    case ENStatementKind.STTryCatchFinally:
                        PasterSecondLevel((st as IStatementTryCatchFinally).TryBlock, level + 1);

                        foreach (var catchElem in (st as IStatementTryCatchFinally).Catches())
                            PasterSecondLevel(catchElem, level + 1);

                        PasterSecondLevel((st as IStatementTryCatchFinally).FinallyBlock, level + 1);
                        break;
                    case ENStatementKind.STUsing:
                        PasterSecondLevel((st as IStatementUsing).Body, level + 1);
                        break;
                    case ENStatementKind.STYieldReturn:
                        metReturn = true;
                        break;
                    default:
                        throw new Exception("Неверно задан тип стейтмента.");
                }

                if (st.SensorBeforeTheStatement == null)
                    storage.sensors.AddSensorBeforeStatement(st, metReturn ? Kind.FINAL : Kind.INTERNAL);

                if (metReturn)
                    break;

                if (st.NextInLinearBlock != null)
                    st = st.NextInLinearBlock;
                else
                    break;
            }

            if (!metReturn && level == 0)
            {
                IStatement statement = storage.statements.AddStatementOperational(st.LastSymbolLocation.GetLine(), st.LastSymbolLocation.GetColumn() + 1,
                    st.LastSymbolLocation.GetFile());
                st.NextInLinearBlock = statement;
                storage.sensors.AddSensorBeforeStatement(statement, Kind.FINAL);
            }

        }

        private void PasterThirdLevel(IStatement st, int level = 0)
        {
            if (st == null)
                return;
            //ставим датчики перед каждым ветвлением и в конец, если последний стейтмент - не ретурн
            if (level == 0)
                storage.sensors.AddSensorBeforeStatement(st, Kind.START);

            bool metReturn = st is IStatementReturn || st is IStatementYieldReturn || st is IStatementThrow;

            if (metReturn)
            {
                if (level > 0)
                    storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                return; //больше не надо ничего вставлять - встретили ретурн
            }

            while (st != null)
            {
                switch (st.Kind)
                {
                    case ENStatementKind.STBreak:
                        metReturn = true;
                        break;
                    case ENStatementKind.STContinue:
                        metReturn = true;
                        break;
                    case ENStatementKind.STDoAndWhile:
                        PasterThirdLevel((st as IStatementDoAndWhile).Body, level + 1);
                        break;
                    case ENStatementKind.STFor:
                        PasterThirdLevel((st as IStatementFor).Body, level + 1);
                        break;
                    case ENStatementKind.STForEach:
                        PasterThirdLevel((st as IStatementForEach).Body, level + 1);
                        break;
                    case ENStatementKind.STGoto:
                        break;
                    case ENStatementKind.STIf:
                        PasterThirdLevel((st as IStatementIf).ThenStatement, level + 1);
                        PasterThirdLevel((st as IStatementIf).ElseStatement, level + 1);
                        break;
                    case ENStatementKind.STOperational:
                        break;
                    case ENStatementKind.STReturn:
                        metReturn = true;
                        storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                        break;
                    case ENStatementKind.STSwitch:
                        foreach (var switchElem in (st as IStatementSwitch).Cases())
                            PasterThirdLevel(switchElem.Value, level + 1);

                        PasterThirdLevel((st as IStatementSwitch).DefaultCase(), level + 1);
                        break;
                    case ENStatementKind.STThrow:
                        metReturn = true;
                        storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                        break;
                    case ENStatementKind.STTryCatchFinally:
                        PasterThirdLevel((st as IStatementTryCatchFinally).TryBlock, level + 1);

                        foreach (var catchElem in (st as IStatementTryCatchFinally).Catches())
                            PasterThirdLevel(catchElem, level + 1);

                        PasterThirdLevel((st as IStatementTryCatchFinally).FinallyBlock, level + 1);
                        break;
                    case ENStatementKind.STUsing:
                        PasterThirdLevel((st as IStatementUsing).Body, level + 1);
                        break;
                    case ENStatementKind.STYieldReturn:
                        metReturn = true;
                        storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                        break;
                    default:
                        throw new Exception("Неверно задан тип стейтмента.");
                }

                if (st.NextInLinearBlock != null)
                    st = st.NextInLinearBlock;
                else
                    break;
            }

            if (!metReturn && level == 0)
            {
                IStatement statement = storage.statements.AddStatementOperational(st.LastSymbolLocation.GetLine(), st.LastSymbolLocation.GetColumn() + 1,
                    st.LastSymbolLocation.GetFile());
                st.NextInLinearBlock = statement;
                storage.sensors.AddSensorBeforeStatement(statement, Kind.FINAL);
            }
        }

        /// <summary>
        /// Заполнение информации о датчиках
        /// </summary>
        private void GetSensors(ulong maxDefinedIdBeforePHP)
        {
            // заполняем хранилище для сенсоров
            SensorInfo info = new SensorInfo();
            foreach (Sensor sensor in storage.sensors.EnumerateSensors())
            {
                if(sensor.GetFunction() == null || sensor.GetFunctionID() <= maxDefinedIdBeforePHP) continue;
                info.line = (int)sensor.GetBeforeStatement().FirstSymbolLocation.GetLine();
                info.column = (int)sensor.GetBeforeStatement().FirstSymbolLocation.GetColumn();
                info.offset = sensor.GetBeforeStatement().FirstSymbolLocation.GetOffset();
                info.id = sensor.ID;
                info.file = sensor.GetBeforeStatement().FirstSymbolLocation.GetFile().FullFileName_Canonized;
                info.isReturn = (sensor.GetBeforeStatement() is IStatementReturn || sensor.GetBeforeStatement() is IStatementYieldReturn);
                sensors.Add(sensor.ID, info);
            }
        }

        /// <summary>
        /// Импорт информации о реторнах
        /// </summary>
        /// <param name="report">Путь к файлу с отчётом</param>
        private void FillReturnInfo(string report)
        {
            if (!File.Exists(report))
            {
                IA.Monitor.Log.Warning(PluginSettings.Name, "Файл с информацией о реторнах не был найден.");
                return;
            }

            returns.Clear();

            string sourceFile = string.Empty;
            using (StreamReader reader = new StreamReader(report))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (Regex.IsMatch(line, @"\d+-\d+-\d+-\d+"))
                    {
                        string[] coords = line.Split('-');
                        ReturnCoord rcoord = new ReturnCoord(Convert.ToUInt64(coords[0]), Convert.ToUInt64(coords[1]), Convert.ToUInt64(coords[2]), Convert.ToUInt64(coords[3]), Convert.ToUInt64(coords[4]) != 1);

                        if (!returns.ContainsKey(sourceFile))
                            returns.Add(sourceFile, new SortedSet<ReturnCoord>());

                        returns[sourceFile].Add(rcoord);
                    }
                    else
                        sourceFile = line.Trim().Replace(sourceFolder, sourceWithSensorsFolder);
                }
            }
        }

        /// <summary>
        /// Импорт информации о файлах, не содержащих PHP код
        /// </summary>
        /// <param name="report">Путь к файлу с отчётом</param>
        private void FillNonPhpFilesInfo(string report)
        {
            if (!File.Exists(report))
            {
                IA.Monitor.Log.Warning(PluginSettings.Name, "Файл с информацией о файлах без PHP кода не был найден.");
                return;
            }

            nonPhpFiles.Clear();

            using (StreamReader reader = new StreamReader(report))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    nonPhpFiles.Add(Store.Files.CanonizeFileName(line));
            }
        }

        /// <summary>
        /// Метод возвращет координаты реторна по датчику
        /// </summary>
        /// <param name="sensor">Информация о датчике</param>
        /// <returns>Координаты реторна</returns>
        private ReturnCoord GetReturnCoord(SensorInfo sensor)
        {
            foreach (ReturnCoord coord in returns[sensor.file.Replace(sourceFolder, sourceWithSensorsFolder)])
                if (coord.sRow == (ulong)sensor.line && coord.sColumn == (ulong)sensor.column)
                    return coord;

            //такого быть не должно реторн должен найтись
            throw new Exception("Не найден датчик перед реторном.");
        }

        /// <summary>
        /// вставка датчика в файлы без пхп кода
        /// </summary>
        /// <param name="file"></param>
        private void PasteToNonPhpFile(string file)
        {
            List<ulong> indexes = new List<ulong>();

            //размечаем куда вставить датчики
            foreach (ulong ID in sensors.Keys)
                if (sensors[ID].file == file)
                    indexes.Add(ID);

            if (indexes.Count == 0)
            {
                Monitor.Log.Warning(PluginSettings.Name, "В файл не было вставлено ни одного датчика " + file);
                return;
            }

            FileOperations.AbstractReader reader = null;
            try
            {
                reader = new FileOperations.AbstractReader(file);
            }
            catch (Exception ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    "Не удалось открыть файл " + file + ".",
                    ex.ToFullMultiLineMessage()
                }.ToMultiLineString();

                IA.Monitor.Log.Error(PluginSettings.Name, message);
            }

            //считываем файл целиком
            string contents = reader.getText();

            if (indexes.Count == 0)
            {
                Monitor.Log.Warning(PluginSettings.Name, "В файл не было вставлено ни одного датчика " + file);
            }
            else
            {
                contents = contents.Insert(0, "<?php" + SensorText(sensors[indexes[0]].id) + "?>");
            }

            WriteContents(contents, file);
        }

        /// <summary>
        /// Расставляем 
        /// </summary>
        private void Paste()
        {

            //заполняем полный список файлов куда что либо нужно вставить
            files.AddRange(braces.Keys);

            foreach (ulong ID in sensors.Keys)
                if (!files.Contains(sensors[ID].file))
                    files.Add(sensors[ID].file);

            //Задания

            ulong count = 0;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SENSORS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)files.Count);
            foreach (string file in files)
            {

                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SENSORS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++count);
                if (nonPhpFiles.Contains(file))
                {
                    PasteToNonPhpFile(file);
                    continue;
                }

                insertList.Clear();

                //замены для реторнов
                List<ulong> replaceList = new List<ulong>();

                FileOperations.AbstractReader reader = null;
                try
                {
                    reader = new FileOperations.AbstractReader(file);
                }
                catch (Exception ex)
                {
                    //Формируем сообщение
                    string message = new string[]
                    {
                        "Не удалось открыть файл " + file + ".",
                        ex.ToFullMultiLineMessage()
                    }.ToMultiLineString();

                    IA.Monitor.Log.Error(PluginSettings.Name, message);
                }

                //Cчитываем файл целиком
                string contents = reader.getText();

                //размечаем куда вставить скобки
                if (braces.ContainsKey(file))
                    foreach (BracesCoord pb in braces[file])
                    {
                        ulong firstBraceOffset = (ulong)reader.getLineOffset((int)pb.sRow) + pb.sColumn - 1; // -1;
                        if (!insertList.ContainsKey(firstBraceOffset))
                            insertList.Add(firstBraceOffset, "{");
                        else
                            insertList[firstBraceOffset] += "{";

                        ulong secondBraceOffset = (ulong)reader.getLineOffset((int)pb.eRow) + pb.eColumn;// + 1; //2;
                        if (contents[(int)secondBraceOffset] == ';')
                            secondBraceOffset++;
                        if (!insertList.ContainsKey(secondBraceOffset))
                            insertList.Add(secondBraceOffset, "}");
                        else
                            insertList[secondBraceOffset] += "}";
                    }

                //размечаем куда вставить датчики
                foreach (ulong ID in sensors.Keys)
                    if (sensors[ID].file == file)
                    {
                        //если это датчик, относящийся к реторну
                        ReturnCoord retCoord;
                        if (sensors[ID].isReturn && !(retCoord = GetReturnCoord(sensors[ID])).isEmpty)
                        {
                            replaceList.Add(GetSensorPosition(ref contents, ID));

                            ulong returnEndPos = (ulong)reader.getLineOffset((int)retCoord.eRow) + retCoord.eColumn;
                            if (!insertList.ContainsKey(returnEndPos))
                                insertList.Add(returnEndPos, SensorText(ID));
                            else
                                insertList[returnEndPos] = SensorText(ID) + insertList[returnEndPos];
                        }
                        else
                        {
                            ulong key = GetSensorPosition(ref contents, ID);

                            if (!insertList.ContainsKey(key))
                                insertList.Add(key, SensorText(ID));
                            else
                                insertList[key] += SensorText(ID);
                        }
                    }

                for (int i = insertList.Keys.Count - 1; i >= 0; i--)
                {
                    //ищем замены которые необходимо сделать
                    for (int j = 0; j < replaceList.Count; j++)
                        if (replaceList[j] >= insertList.Keys[i])
                        {
                            contents = contents.Remove((int)replaceList[j], 6).Insert((int)replaceList[j], "$RNTRNTRNT =");
                            replaceList.RemoveAt(j);
                        }

                    contents = contents.Insert((int)insertList.Keys[i], insertList[insertList.Keys[i]]);
                }

                //замещаем то, что осталось
                foreach (int t in replaceList)
                    contents = contents.Remove(t, 6).Insert(t, "$RNTRNTRNT =");

                WriteContents(contents, file);

            }
        }

        /// <summary>
        /// Вставка подстроки
        /// </summary>
        /// <param name="text"></param>
        /// <param name="pos"></param>
        /// <param name="insText"></param>
        /// <returns></returns>
        private string InsertCheck(string text, int pos, string insText)
        {
            return (text.Length > pos) ? text.Insert(pos, insText) : text.Insert(text.Length - 3, insText);
        }

        /// <summary>
        /// Метод записи информации в файл
        /// </summary>
        /// <param name="contents">Содержимое будущего файла</param>
        /// <param name="file">Имя файла</param>
        private void WriteContents(string contents, string file)
        {
            if (sourceWithSensorsFolder == null)
                throw new Exception("Не задана директория для исходные текстов со вставленными датчиками");

            string old_file = file;
            string new_file = file.Replace(sourceFolder, sourceWithSensorsFolder.Trim('\\') + "\\");

            string directory = (new FileInfo(new_file)).Directory.FullName;

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            (new FileOperations.AbstractReader(old_file)).writeTextToFile(new_file, contents);
        }

        /// <summary>
        /// Возвращает смещение строки от начала файла
        /// </summary>
        /// <param name="contents">Содержимое файла</param>
        /// <param name="line">Номер строки</param>
        /// <returns>Смещение строки от начала файла</returns>
        private ulong GetLineOffset(string contents, ulong line)
        {
            int npos = -1;

            for (ulong j = 1; j < line; j++)
                npos = contents.IndexOf('\n', npos + 1);

            if (npos > 0 && contents[npos] == '\r')
                npos++;

            return (ulong)npos + 1;
        }

        /// <summary>
        /// Метод для формирования строки датчика
        /// </summary>
        /// <param name="ID">Номер датчика</param>
        /// <returns>Строка датчика</returns>
        private String SensorText(ulong ID)
        {
            return (sensors[ID].isReturn && !(GetReturnCoord(sensors[ID]).isEmpty)) ?
                " " + PluginSettings.SensorText.Replace(new String('#', 1), sensors[ID].id.ToString()) + " return $RNTRNTRNT;" :
                    " " + PluginSettings.SensorText.Replace(new String('#', 1), sensors[ID].id.ToString()) + ' ';
        }

        /// <summary>
        /// Метод получает позицию датчика в файле, в случае если она отличается от расчетной - меняется смещение в хранилище
        /// </summary>
        /// <param name="contents">Содержимое файла</param>
        /// <param name="ID">Номер датчика</param>
        /// <returns>Позиция датчика в файле</returns>
        private ulong GetSensorPosition(ref string contents, ulong ID)
        {
            var pos = GetSensorPositionWorker(ref contents, ID);
            var sensor = storage.sensors.GetSensor(ID);
            var beforeStatement = sensor.GetBeforeStatement();
            if(pos > 0 && beforeStatement != null && beforeStatement.FirstSymbolLocation != null && beforeStatement.FirstSymbolLocation.GetOffset() != pos)
            {
                beforeStatement.SetFirstSymbolLocation(beforeStatement.FirstSymbolLocation.GetFile(), pos);
            }

            return pos;
        }
        /// <summary>
        /// Метод получает позицию датчика в файле
        /// </summary>
        /// <param name="contents">Содержимое файла</param>
        /// <param name="ID">Номер датчика</param>
        /// <returns>Позиция датчика в файле</returns>
        private ulong GetSensorPositionWorker(ref string contents, ulong ID)
        {
            SensorInfo info = sensors[ID];

            if (info.offset == (ulong)contents.Length || info.offset > (ulong)contents.Length)
            {
                if (contents.IndexOf("?>", (int)info.offset - 4) > 0)
                    return (ulong)contents.IndexOf("?>", (int)info.offset - 4);
                else
                {
                    if (contents.IndexOf("</html>", (int)info.offset - 10) > 0)
                    {
                        //датчик ставится в конец файла - в случае если это крутой микс html и php
                        contents += "<?  ?>";
                        return (ulong)contents.LastIndexOf("?>") - 1;
                    }
                    else
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Смещение датчика посчитано неверно: " + info.file + " - id:" + info.id);
                        return 0;
                    }
                }
            }

            //в случае кетча, там всё нетривиально
            int tempind = contents.IndexOf("\n", (int)info.offset);
            if ((int)info.offset > 4 && contents.IndexOf("?>", (int)info.offset - 4) > 0 && contents.IndexOf("?>", (int)info.offset - 4) < (int)info.offset)
                return (ulong)contents.IndexOf("?>", (int)info.offset - 4);
            int sensorLineOffset = contents.Substring(0, (int)info.offset).LastIndexOf('\n') + 1;
            if (contents.IndexOf("catch", sensorLineOffset, (tempind == -1 ? contents.Length - 1 : tempind) - sensorLineOffset) != -1)
                return (ulong)contents.IndexOf('{', (int)info.offset) + 1;

            // Если хотим вставить на место скобки
            if (contents[(int)info.offset] == '{')
                return info.offset + 1;

            if (contents[(int)info.offset] == ';')
                return info.offset + 1;

            //Если есть особый символ @ - означает что за ним может идти любой неработающий бред
            if (info.offset != 0 && contents[(int)info.offset - 1] == '@')
                return info.offset - 1;



            if ((int)info.offset > 0 && (contents[(int)info.offset] == ';' || contents[(int)info.offset] == ')'))
            {
                int min1 = contents.IndexOf(' ', (int)info.offset - 1);
                int min2 = contents.IndexOf('\r', (int)info.offset - 1);
                int min3 = contents.IndexOf('\n', (int)info.offset - 1);
                if (min1 < 0) min1 = Int32.MaxValue;
                if (min2 < 0) min2 = Int32.MaxValue;
                if (min3 < 0) min3 = Int32.MaxValue;
                return (ulong)Math.Min(min1, Math.Min(min2, min3));
            }

            //Стандартный случай
            return info.offset;
        }
    }
}
