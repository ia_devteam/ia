﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Store;
using IA.Extensions;

namespace IA.Plugins.Parsers.PhpParser
{
    /// <summary>
    /// Класс для второго прохода парсера
    /// </summary>
    internal class AST_Pass2
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Флаги
        /// </summary>
        private bool isSaveLocation, isEnterSensor;

        /// <summary>
        /// Массив для хранения координат начала сегментов
        /// </summary>
        private ulong[] entryLocation;
        
        /// <summary>
        /// Координаты начала PHP кода в текущем файле
        /// </summary>
        private ulong[] currentPHPCodeStartLocation;
        
        /// <summary>
        /// Номер первого датчика
        /// </summary>
        private ulong sensorNumber = PluginSettings.FirstSensorNumber;

        /// <summary>
        /// Текущий файл
        /// </summary>
        private IFile currentFile;

        /// <summary>
        /// Поток для записи списка найденных реторнов
        /// </summary>
        private StreamWriter returnsReportWriter;

        /// <summary>
        /// Поток для записи списка файлов не содержищих PHP код
        /// </summary>
        private StreamWriter noPHPFilesReportWriter;

        /// <summary>
        /// Стек для накопления вызовов функций
        /// </summary>
        private Stack<Dictionary<int, string>> stack = new Stack<Dictionary<int, string>>();

        /// <summary>
        /// Словарь для хранения информации о скобках
        /// </summary>
        private Dictionary<string, List<BracesCoord>> braces = new Dictionary<string, List<BracesCoord>>();
        internal Dictionary<string, List<BracesCoord>> Braces
        {
            get { return braces ?? new Dictionary<string, List<BracesCoord>>(); }
        }

        /// <summary>
        /// Переменная для нумерации неизвестных функций
        /// </summary>
        private int unknownFunctionNumber = 0;

        //функция выдаёт координаты начала пхп кода сразу после тега <?php или <?
        private ulong[] GetPHPCodeStartLocation(string filePath)
        {
            ulong[] coords = new ulong[] {1, 0};
            string str;
            StreamReader originalSourceReader = new StreamReader(filePath);
            
            while ((str = originalSourceReader.ReadLine()) != null)
            {
                str = str.ToLower();
                if (str == "<?php" || str.Contains("<?php ") || str.Trim() == "<?" || str.Contains("<? "))
                {
                    coords[1] = str.Contains("<?php") ? ((ulong)(str.IndexOf("<?php") + 6)) : ((ulong)(str.IndexOf("<?") + 3));
                    break;
                }
                coords[0]++;
            }

            originalSourceReader.Close();

            if (str == null)
            {
                IA.Monitor.Log.Warning(PluginSettings.Name, "Нет PHP кода в файле: " + filePath);
                coords = new ulong[] { 1, 6 }; //вставляем датчик в начало файла.. позже туда вручную добавим тег <?php ?>
                noPHPFilesReportWriter.WriteLine(filePath);
            }

            return coords;
        }

        /// <summary>
        /// Выгрузка элементов из стека
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private List<string> PopCallsFromStack(int level)
        {
            List<string> ret = new List<string>();
            Dictionary<int, string> elem;

            while (stack.Count > 0 && (elem = stack.Peek()) != null && (elem.Keys.Max()) >= level)
            {
                elem = stack.Pop();
                ret.Add(elem[elem.Keys.Max()]);
            }

            return ret;
        }

        private string ExtractValue(string line)
        {
            string val = "";
            string[] split = line.Trim().Split(' ');
            if(split.Length==2)
            {

                if(split[1].Trim().StartsWith("Value=["))
                {
                    val = split[1].Trim().Substring("Value=[".Length).Trim(']');
                }
            }
            return val;
        }

        /// <summary>
        /// Функция для определения имени функции (также обработка инклюдов и дай)
        /// </summary>
        /// <param name="line"></param>
        /// <param name="isFullName"></param>
        /// <returns></returns>
        private string ExtractName(string line, bool isFullName = false)
        {
            ReportController reportcontroller = new ReportController();

            string[] split = line.Split('|');

            if (split.Length == 1)
                return line.Contains('[') && line.Contains(']') ? reportcontroller.ExtractName(line, isFullName) : "UnknownFunction_" + (++unknownFunctionNumber);
            else if (split.Length == 2)
                switch (split[0].Trim())
                {
                    case "Exit Expression":
                        return "die";
                    case "Including Expression":
                        return "UnknownFunction_" + (++unknownFunctionNumber);
                    default:
                        {
                            try
                            {
                                return split[1].Contains('[') && split[1].Contains(']') ? reportcontroller.ExtractName(split[1].Trim(), isFullName) : "UnknownFunction_" + (++unknownFunctionNumber);
                            }catch(Exception e)
                            {
                                Monitor.Log.Warning(PluginSettings.Name, "Не обрабатываемое имя функции конструктора " + split[1].Trim());
                                return "UnknownFunction_" + (++unknownFunctionNumber);
                            }
                        }
                }
            else
                throw new Exception("Ошибка в отчёте Фалангера.");
        }

        /// <summary>
        /// Разбор стейтментов
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="kind"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        private IStatement GetNewStatement(ref IStatement entry, ENStatementKind kind, ulong[] location)
        {
            IStatement statement = null;
            switch (kind)
            {
                case ENStatementKind.STBreak:
                    statement = storage.statements.AddStatementBreak(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STContinue:
                    statement = storage.statements.AddStatementContinue(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STDoAndWhile:
                    statement = storage.statements.AddStatementDoAndWhile(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STFor:
                    statement = storage.statements.AddStatementFor(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STForEach:
                    statement = storage.statements.AddStatementForEach(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STGoto:
                    statement = storage.statements.AddStatementGoto(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STIf:
                    statement = storage.statements.AddStatementIf(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STOperational:
                    statement = storage.statements.AddStatementOperational(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STReturn:
                    statement = storage.statements.AddStatementReturn(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STSwitch:
                    statement = storage.statements.AddStatementSwitch(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STThrow:
                    statement = storage.statements.AddStatementThrow(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STTryCatchFinally:
                    statement = storage.statements.AddStatementTryCatchFinally(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STUsing:
                    statement = storage.statements.AddStatementUsing(location[0], location[1], currentFile, location[2], location[3]);
                    break;
                case ENStatementKind.STYieldReturn:
                    break;
                default:
                    throw new Exception("Неверно задан тип стейтмента.");
            }

            IStatement last = GetLastStatement(entry);
            if (last == null)
            {
                entry = last = statement;
                //storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
            }
            else
            {
                last = last.NextInLinearBlock = statement;
                //storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
            }

            return last;
        }

        /// <summary>
        /// Получить последний стейтмент
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        private IStatement GetLastStatement(IStatement st)
        {
            IStatement tmp = st;
            
            if (st == null)
                return null;
            
            while (tmp.NextInLinearBlock != null)
                tmp = tmp.NextInLinearBlock;
            
            return tmp;
        }

        /// <summary>
        /// Функция для распарсивания блока (string) block. Вызывается рекурсивно. Если флаг (bool) skip опущен, датчики не ставятся. (Дим, наверное наоборот! когда флаг опущен - датчики ставятся!!!!)
        /// флаг isFullFileBlock - если поднят, то мы передали на парсинг весь файл - это нужно чтобы обрабатывать случаи пустых файлов без пхп кода и всякие особые случаи
        /// </summary>
        /// <param name="block"></param>
        /// <param name="isSkip"></param>
        /// <param name="isFullFileBlock"></param>
        /// <returns></returns>
        private IStatement ParseBlock(string block, bool isSkip = false, bool isFullFileBlock = false, int linenumber=0) 
        {
            ReportController reportcontroller = new ReportController();

            //на вход у нас блок, возвращаем по очереди стейтменты                        
            StringReader reader = new StringReader(block);                                      //стрингридер для чтения блока 
            string line, lline = string.Empty; //сюда будем считывать строки из блока
            IStatement entry = null;
            //globalLog.Close();

            //на случай если внутри определение функции или ещё какая шушара
            ulong[] nullLocs = new ulong[] { 1, 1 };
            ulong[] endLocs = null;
            int startLevel = -1;
            string lastStingEntryLevel = "";
            bool saveNullLocs = true;

            while ((line = reader.ReadLine()) != null)
           {
            beginparse:
                linenumber++;
                if (!String.IsNullOrEmpty(line) && lline == line)
                    line = reader.ReadLine();

                lline = line;

                if (line == null)
                    break;
                
                if (!line.Contains("| (") || !line.StartsWith(" "))
                    continue;
                
                string[] split = line.Split('|');

                //подсчитываем левел строки (количесто пробелов перед ней)
                int level = (new ReportController()).GetLevel(line);
                if (startLevel == -1 || level == startLevel)
                {
                    if(lastStingEntryLevel == "" || !line.Contains("Empty Statement") || line.Contains("Empty Statement") && lastStingEntryLevel.Contains("Class/Interface Decl"))
                        lastStingEntryLevel = line;
                    startLevel = level;
                }

                //если флаг возведён запоминаем координаты начала стэйтмента
                if (isSaveLocation)
                {
                    if (split[1].Contains("(-1,-1)"))
                    {
                        Monitor.Log.Warning(PluginSettings.Name, "Ошибка позиционирования выражения в блоке (строка " + linenumber + "): " + line);
                        //если у нас какушка  то в любом случае начало пхп кода мы знаем
                        if (isFullFileBlock)
                        {
                            entryLocation = new[] {currentPHPCodeStartLocation[0], currentPHPCodeStartLocation[1]};
                            isSaveLocation = false;
                            isEnterSensor = true;

                            if(saveNullLocs)
                            {
                                nullLocs = new[] {currentPHPCodeStartLocation[0], currentPHPCodeStartLocation[1]};
                                saveNullLocs = false;
                            }
                        }
                        else
                        { //если мы не на уровне файла
                            saveNullLocs = true;
                            isSaveLocation = true;
                            isEnterSensor = false;
                        }
                    }
                    else
                    {
                        //если всё хорошо сохраняем координаты
                        entryLocation = (isFullFileBlock) ? new[] { currentPHPCodeStartLocation[0], currentPHPCodeStartLocation[1] } : reportcontroller.ExtractLocation(split[1], linenumber);

                        if(saveNullLocs)
                        {
                            nullLocs = (isFullFileBlock) ? new[] { currentPHPCodeStartLocation[0], currentPHPCodeStartLocation[1] } : new[] { entryLocation[0], entryLocation[1] };
                            saveNullLocs = false;
                        }

                        isSaveLocation = false;
                        isEnterSensor = true;
                    }

                    isFullFileBlock = false;
                }

                //выталкиваем из стека все элементы, левел которых выше уровня str и записываем их в результат
                CallsToStore(ref entry, PopCallsFromStack(level), linenumber, isSkip);

                switch (split[0].Trim())
                {
                    #region DFC
                    case "Direct  Method Call":
                    case "Direct Function Call":
                    case "Exit Expression":
                    case "Including Expression":
                    case "New Expression":
                        {
                            Dictionary<int, string> temp = new Dictionary<int, string> {{level, line.Trim()}};
                            stack.Push(temp);
                            break;
                        }
                    #endregion

                    #region IF
                    case "If Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);

                            IStatementIf statementIf = (IStatementIf) GetNewStatement(ref entry, ENStatementKind.STIf, location);

                            IStatementIf currElseIfSt = null;
                            IStatementIf prevElseIfSt = statementIf;

                            Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            foreach (string key in retr.Keys)
                            {
                                switch (key.Split('|')[0].Trim())
                                {
                                    case "IF Condition":
                                        {
                                            isSaveLocation = false;

                                            IStatement cond = ParseBlock(retr[key], true, false, linenumber);
                                            if (cond != null && cond is IStatementOperational)
                                                statementIf.Condition = ((IStatementOperational)cond).Operation;
                                            
                                            break;
                                        }
                                    case "IF Block":
                                        {
                                            isSaveLocation = true;
                                            
                                            //отлов отсутствия скобок
                                            string inner = retr[key];
                                            AddBracesIfNeed(inner, linenumber);

                                            IStatement blo = ParseBlock(inner, false, false, linenumber);
                                            if (blo != null)
                                                statementIf.ThenStatement = blo;

                                            break;
                                        }
                                    case "ELSE Block":
                                        {

                                            isSaveLocation = true;

                                            //проверка на скобки
                                            string inner = retr[key];
                                            AddBracesIfNeed(inner, linenumber);

                                            IStatement elseIfSt = ParseBlock(inner, false, false, linenumber);
                                            if (currElseIfSt == null)
                                            {
                                                if (elseIfSt != null)
                                                    statementIf.ElseStatement = elseIfSt;
                                            }
                                            else
                                                currElseIfSt.ElseStatement = elseIfSt;

                                            break;

                                        }
                                    case "ELSEIF Condition":
                                        {
                                            isSaveLocation = false;

                                            string elseIfCoordString = key.Split('|')[1];

                                            prevElseIfSt.ElseStatement = currElseIfSt = storage.statements.AddStatementIf(reportcontroller.ExtractLocation(elseIfCoordString.Trim(), linenumber)[0], reportcontroller.ExtractLocation(elseIfCoordString.Trim(), linenumber)[1], currentFile); //что здесь делать с датчиками - хз!!!!

                                            IStatement cond = ParseBlock(retr[key],true, false, linenumber);
                                            if (cond != null)
                                                currElseIfSt.Condition = ((IStatementOperational)cond).Operation;

                                            prevElseIfSt = currElseIfSt;

                                            break;
                                        }
                                    case "ELSEIF Block":
                                        {
                                            isSaveLocation = true;
                                            
                                            //проверка на скобки
                                            string inner = retr[key];
                                            AddBracesIfNeed(inner, linenumber);
                                            
                                            IStatement resultSt = ParseBlock(inner,false, false, linenumber);
                                            if (resultSt != null)
                                                currElseIfSt.ThenStatement = resultSt;

                                            break;
                                        }
                                    default:
                                        throw new Exception();
                                }
                            }
                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion

                    #region CONDITIONAL EXPRESSION
                    //case "Conditional Expression":
                    //    {
                    //        IStatement last = getLastStatement(entry);
                    //        if (last == null)
                    //        {
                    //            entry = last = storage.statements.AddStatementIf(currentFile, location[0], location[1]);
                    //            storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                    //            globalLog.WriteLine(currentFile.FullFileName_Canonized + " " + last.Location.GetLine() + " " + last.Location.GetRow() + " " + (sensorNumber - 1).ToString());
                    //        }
                    //        else
                    //        {
                    //            storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.INTERNAL);
                    //            last = last.NextInLinearBlock = storage.statements.AddStatementIf(currentFile, location[0], location[1]);
                    //            globalLog.WriteLine(currentFile.FullFileName_Canonized + " " + last.Location.GetLine() + " " + last.Location.GetRow() + " " + (sensorNumber - 1).ToString());
                    //        }

                    //        Dictionary<string, string> retr = getInnerBlocks(sr, ReportController.GetLevel(str), ref str);
                    //        foreach (string key in retr.Keys)
                    //        {
                    //            switch (key.Split('|')[0].Trim())
                    //            {
                    //                case "Condition":
                    //                    {

                    //                        saveLocs = false;
                    //                        IStatement cond = parseBlock(retr[key], true);
                    //                        if (cond != null && cond is IStatementOperational)
                    //                            ((IStatementIf)last).Condition = ((IStatementOperational)cond).Operation;
                    //                        saveLocs = false;

                    //                        break;
                    //                    }
                    //                case "IF Condition Statement":
                    //                    {

                    //                        saveLocs = true;
                    //                        //отлов отсутствия скобок
                    //                        string inner = retr[key];
                    //                        checkForBlock(inner);

                    //                        IStatement blo = parseBlock(inner);
                    //                        if (blo != null)
                    //                            ((IStatementIf)last).ThenStatement = blo;
                    //                        saveLocs = false;

                    //                        break;
                    //                    }
                    //                case "ELSE Condition Statement":
                    //                    {

                    //                        saveLocs = true;
                    //                        string inner = retr[key];
                    //                        checkForBlock(inner);
                    //                        IStatement ha = parseBlock(inner);
                    //                        if (ha != null)
                    //                            ((IStatementIf)last).ElseStatement = ha;
                    //                        saveLocs = false;
                    //                        break;
                    //                    }
                    //                default:
                    //                    throw new Exception();
                    //            }
                    //        }
                    //        saveLocs = true;
                    //        goto beginparse;
                    //    }
                    #endregion

                    #region LOOPFOR
                    case "Loop For Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);

                            IStatementFor statementFor = (IStatementFor)GetNewStatement(ref entry, ENStatementKind.STFor, location);

                            Dictionary<string, string> forRet = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            Dictionary<string, string> forRetNew = new Dictionary<string, string>();
                            foreach (KeyValuePair<string, string> pair in forRet)
                            {
                                string[] keyParse = pair.Key.Split('|');
                                forRetNew.Add(keyParse[0].Trim(), pair.Value);
                            }

                            isSaveLocation = false;
                            if (forRetNew.ContainsKey("FOR INIT"))
                            {
                                IStatement init = ParseBlock(forRetNew["FOR INIT"],true, false, linenumber);
                                if (init != null)
                                    statementFor.Start = ((IStatementOperational)init).Operation;
                            }
                            isSaveLocation = false;


                            isSaveLocation = false;
                            if (forRetNew.ContainsKey("FOR CONDITION"))
                            {
                                IStatement cond = ParseBlock(forRetNew["FOR CONDITION"],true, false, linenumber);
                                if (cond != null)
                                    statementFor.Condition = ((IStatementOperational)cond).Operation;
                            }
                            isSaveLocation = false;


                            isSaveLocation = false;
                            if (forRetNew.ContainsKey("FOR MOD"))
                            {
                                IStatement mod = ParseBlock(forRetNew["FOR MOD"],true, false, linenumber);
                                if (mod != null)
                                    statementFor.Iteration = ((IStatementOperational)mod).Operation;
                            }
                            isSaveLocation = false;


                            isSaveLocation = true;
                            if (forRetNew.ContainsKey("FOR BODY"))
                            {
                                string inner = forRetNew["FOR BODY"];
                                AddBracesIfNeed(inner, linenumber);
                                IStatement body = ParseBlock(inner,false, false, linenumber);
                                if (body != null)
                                    statementFor.Body = body;
                            }

                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion

                    #region LOOPFOREACH
                    case "Loop Foreach Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);

                            IStatementForEach statementForEach = (IStatementForEach)GetNewStatement(ref entry, ENStatementKind.STForEach, location);
                            bool metBlock = false;
                            Dictionary<string, string> foreachRet = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            foreach (string key in foreachRet.Keys)
                            {
                                string[] keyParse = key.Split('|');
                                switch (keyParse[0].Trim())
                                {
                                    case "Direct Function Call":

                                        isSaveLocation = false;
                                        IStatement cond = ParseBlock(key,true, false, linenumber);
                                        if (cond != null)
                                            statementForEach.Head = ((IStatementOperational)cond).Operation;
                                        isSaveLocation = false;

                                        break;
                                    case "Block Statement":

                                        isSaveLocation = true;
                                        IStatement body = ParseBlock(foreachRet[key],false, false, linenumber);
                                        if (body != null)
                                            statementForEach.Body = body;
                                        isSaveLocation = false;
                                        metBlock = true;
                                        break;
                                }
                            }
                            if (!metBlock)
                            {
                                var virtualBlock = foreachRet.Keys.Last() + "\r\n" + foreachRet[foreachRet.Keys.Last()];
                                isSaveLocation = true;
                                AddBracesIfNeed(virtualBlock, linenumber);
                                IStatement body = ParseBlock(virtualBlock, false, false, linenumber);
                                if (body != null)
                                    statementForEach.Body = body;
                                isSaveLocation = false;
                            }
                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion

                    #region WHILE
                    case "Loop While Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);

                            IStatementDoAndWhile statementDoAndWhile = (IStatementDoAndWhile)GetNewStatement(ref entry, ENStatementKind.STDoAndWhile, location);

                            ulong[] condPos = {1,0}, bodyPos = {1,0};
                            Dictionary<string, string> whileRet = reportcontroller.GetInnerBlocks(ref reader, ref line);

                            foreach (string key in whileRet.Keys)
                            {
                                string[] keyParse = key.Split('|');
                                switch (keyParse[0].Trim())
                                {
                                    case "WHILE BODY":

                                        isSaveLocation = true;
                                        string inner = whileRet[key];
                                        AddBracesIfNeed(inner, linenumber); //не знаю, надо ли?
                                        IStatement body = ParseBlock(inner,false, false, linenumber);
                                        if (body != null)
                                            statementDoAndWhile.Body = body;
                                        isSaveLocation = false;

                                        bodyPos = reportcontroller.ExtractLocation(key, linenumber);
                                        break;
                                    case "WHILE CONDITION":

                                        isSaveLocation = false;
                                        IStatement cond = ParseBlock(whileRet[key],true, false, linenumber);
                                        if (cond != null)
                                            statementDoAndWhile.Condition = ((IStatementOperational)cond).Operation;
                                        isSaveLocation = false;

                                        condPos = reportcontroller.ExtractLocation(key, linenumber);
                                        break;
                                }
                            }
                            statementDoAndWhile.IsCheckConditionBeforeFirstRun = !(condPos[0] > bodyPos[0]);
                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion

                    #region SWITCH
                    case "Switch Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);

                            IStatementSwitch statementSwitch = (IStatementSwitch)GetNewStatement(ref entry, ENStatementKind.STSwitch, location);
                            
                            Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            foreach (string key in retr.Keys)
                            {
                                switch (key.Split('|')[0].Trim())
                                {
                                    case "SWITCH Condition":
                                        {

                                            isSaveLocation = false;
                                            IStatement cond = ParseBlock(retr[key],true, false, linenumber);
                                            if (cond != null)
                                                statementSwitch.Condition = ((IStatementOperational)cond).Operation;
                                            isSaveLocation = false;

                                            break;
                                        }
                                    case "SWITCH Case item":
                                        {
                                            Dictionary<string, string> innerBlocks = reportcontroller.GetInnerBlocks(key, retr[key]);
                                            IStatement blok = null;
                                            IOperation operation = null;
                                            ulong[] blo = { 1, 0, 1, 0 };
                                            foreach (string innerKey in innerBlocks.Keys)
                                            {
                                                switch (innerKey.Split('|')[0].Trim())
                                                {
                                                    case "Case Block":
                                                        isSaveLocation = true; //////// ЭТО ТАК???
                                                        blo = reportcontroller.ExtractLocation(innerKey, linenumber, true);
                                                        blok = ParseBlock(innerBlocks[innerKey],false, false, linenumber);
                                                        isSaveLocation = false;//////// ЭТО ТАК???


                                                        break;
                                                    default:

                                                        isSaveLocation = true;
                                                        reportcontroller.ExtractLocation(innerKey, linenumber);
                                                        IStatement innerSt = ParseBlock(innerKey + '\n' + innerBlocks[innerKey],true, false, linenumber);
                                                        if(innerSt != null)
                                                            operation = ((IStatementOperational)innerSt).Operation;
                                                        isSaveLocation = false;

                                                        break;
                                                }
                                            }

                                            if (blo[1] == 0 && blo[3] == 0)
                                                break;

                                            if (operation == null)
                                                operation = storage.operations.AddOperation();

                                            if (blok == null)
                                                blok = storage.statements.AddStatementOperational(blo[0], blo[1], currentFile, blo[2], blo[3]);

                                            statementSwitch.AddCase(operation, blok);
                                            break;
                                        }
                                    case "SWITCH Default item":
                                        {

                                            isSaveLocation = true;
                                            
                                            IStatement blok = ParseBlock(retr[key], retr[key] == "", false, linenumber);

                                            if (blok != null)
                                                statementSwitch.AddDefaultCase(blok);
                                            isSaveLocation = false;

                                            break;
                                        }
                                }
                            }
                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion

                    #region RETURN
                    case "Jump Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);

                            string type = ExtractName(split[1].Trim());

                            switch (type)
                            {
                                case "continue":
                                    IStatementContinue statementContinue = (IStatementContinue)GetNewStatement(ref entry, ENStatementKind.STContinue, location);
                                    break;
                                case "break":
                                    IStatementBreak statementBreak = (IStatementBreak)GetNewStatement(ref entry, ENStatementKind.STBreak, location);
                                    break;
                                case "return":
                                    IStatementReturn statementReturn = (IStatementReturn)GetNewStatement(ref entry, ENStatementKind.STReturn, location);

                                    Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);
                                    ulong[] retLocs = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);

                                    //пишем в файл с ретурнами - последний параметр 0 если реторн пустой, иначе 1
                                    if(retr.Count == 0)
                                        returnsReportWriter.WriteLine(retLocs[0] + "-" + retLocs[1] + "-" + retLocs[2] + "-" + retLocs[3] + "-" + Convert.ToString(0));
                                    else
                                        returnsReportWriter.WriteLine(retLocs[0] + "-" + retLocs[1] + "-" + retLocs[2] + "-" + retLocs[3] + "-" + Convert.ToString(1));

                                    if (retr.Count == 0)
                                        continue;

                                    foreach (string key in retr.Keys)
                                    {

                                        isSaveLocation = false;
                                        IStatement innerSt = ParseBlock(key + '\n' + retr[key],true, false, linenumber);
                                        if (innerSt != null && innerSt is IStatementOperational)
                                            if (((IStatementOperational)innerSt).Operation != null)
                                                statementReturn.ReturnOperation = ((IStatementOperational)innerSt).Operation;

                                        isSaveLocation = false;
                                        break;
                                    }
                                    isSaveLocation = true;
                                    goto beginparse;
                            }
                            break;
                        }
                    #endregion

                    #region GOTO
                    case "Goto Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);
                            string val = ExtractValue(split[1].Trim());
                            //IStatementGoto statementGoto = (IStatementGoto)GetNewStatement(ref entry, ENStatementKind.STGoto, location);
                        }
                        break;
                    #endregion

                    #region LABEL
                    case "Label Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);
                            string val = ExtractValue(split[1].Trim());
                            //IStatementLabel statementLabel = (IStatement)GetNewStatement(ref entry, ENStatementKind.Label, location);
                        }
                        break;
                    #endregion


                    #region TRY-CATCH
                    case "Try-catch Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);

                            IStatementTryCatchFinally statementTryCatchFinally = (IStatementTryCatchFinally)GetNewStatement(ref entry, ENStatementKind.STTryCatchFinally, location);

                            Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            foreach (string key in retr.Keys)
                            {
                                switch (key.Split('|')[0].Trim())
                                {
                                    case "TRY Block":
                                        {
                                            if (retr[key] == "")
                                                break;
                                            isSaveLocation = true;
                                            IStatement _try = ParseBlock(retr[key], true, false, linenumber);
                                            if (_try != null)
                                                statementTryCatchFinally.TryBlock = _try;
                                            isSaveLocation = false;

                                            break;
                                        }
                                    case "CATCH Block":
                                        {
                                            Dictionary<string, string> inerInerBlocks = reportcontroller.GetInnerBlocks(key, retr[key]);
                                            bool addEmptyCatch = false;

                                            foreach (string subKey in inerInerBlocks.Keys)
                                            {
                                                switch (subKey.Split('|')[0].Trim())
                                                {
                                                    case "Use Direct Variable":
                                                        {
                                                            addEmptyCatch = true;
                                                            break;
                                                        }
                                                    default:
                                                        {
                                                            isSaveLocation = true;
                                                            IStatement _catch = ParseBlock(retr[key], false, false, linenumber);
                                                            if (_catch != null)
                                                                statementTryCatchFinally.AddCatch(_catch);
                                                            isSaveLocation = false;
                                                            break;
                                                        }
                                                }
                                            }

                                            if (addEmptyCatch)
                                            {
                                                ulong[] catchLocs = reportcontroller.ExtractLocation(key.Split('|')[1].Trim(), linenumber, true);
                                                IStatement _emptyCatch = storage.statements.AddStatementOperational(catchLocs[0], catchLocs[1], currentFile, catchLocs[2], catchLocs[3]);
                                                statementTryCatchFinally.AddCatch(_emptyCatch);
                                            }
                                            break;
                                        }
                                    default:
                                        throw new Exception();
                                }
                            }
                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion

                    #region THROW
                    case "Throw Statement":
                        {
                            ulong[] location = reportcontroller.ExtractLocation(split[1].Trim(), linenumber, true);

                            IStatementThrow statementThrow = (IStatementThrow)GetNewStatement(ref entry, ENStatementKind.STThrow, location);

                            Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);

                            if (retr.Count == 0)
                                continue;

                            foreach (string key in retr.Keys)
                            {
                                isSaveLocation = false;
                                IStatement _throw = ParseBlock(key + '\n' + retr[key], true, false, linenumber);
                                if (_throw != null)
                                    if (((IStatementOperational)_throw).Operation != null)
                                        statementThrow.Exception = ((IStatementOperational)_throw).Operation;
                                isSaveLocation = false;

                                break;
                            }
                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion

                    #region FUNCDECL
                    case "Function Decl":
                        {
                            //тело функции
                            Dictionary<string, string> innerBlocks = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            
                            //проверить, что в блоках нет вызовов
                            foreach (string key in innerBlocks.Keys)
                            {
                                switch (key.Split('|')[0].Trim())
                                {
                                    case "Function Body":
                                        {
                                            Dictionary<string, string> body_innerBlocks = reportcontroller.GetInnerBlocks(key, innerBlocks[key]);
                                            foreach (string innerKey in body_innerBlocks.Keys)
                                            {
                                                switch (innerKey.Split('|')[0].Trim())
                                                {
                                                    case "Functions Body":
                                                        {
                                                            isSaveLocation = true;
                                                            IStatement body = ParseBlock(body_innerBlocks[innerKey], false, false, linenumber);
                                                            isSaveLocation = false;

                                                            if (body == null)
                                                                continue;

                                                            //ищем текущую функцию
                                                            string funcName = ExtractName(split[1].Trim());
                                                            IFunction searchFunc = null;

                                                            foreach (IFunction fun in storage.functions.GetFunction(funcName))
                                                            {
                                                                Location funDefenition = fun.Definition();
                                                                if ((funDefenition != null) && (funDefenition.GetFile() == currentFile) && (fun.FullName == funcName))
                                                                {
                                                                    /*if (fun.FullName != funcName)
                                                                    {
                                                                        IA.Monitor.Warning("TROLOLO!!!");
                                                                    }*/
                                                                    searchFunc = fun;
                                                                    break;
                                                                }
                                                            }

                                                            if (searchFunc == null)
                                                            {
                                                                IA.Monitor.Log.Error(PluginSettings.Name, "Не нашел соответствие определению функции: " + funcName);
                                                                break;
                                                            }
                                                            
                                                            if(searchFunc.EntryStatement != null)
                                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Ошибка в определении метода: " + funcName);
                                                            

                                                            searchFunc.EntryStatement = body;
                                                            
                                                            IStatement last = GetLastStatement(searchFunc.EntryStatement);
                                                            last.SetLastSymbolLocation(currentFile, reportcontroller.ExtractLocation(innerKey, linenumber, true)[2], reportcontroller.ExtractLocation(innerKey, linenumber, true)[3]);
                                                            
                                                            break;
                                                        }
                                                    default:
                                                        if (body_innerBlocks[innerKey].Contains(" Call| "))
                                                        {
                                                            //tmpStr = tmpStr;
                                                        }
                                                    break;
                                                }
                                                
                                            }
                                            break;
                                        }
                                }
                            }
                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion

                    #region CLASSDECL
                    case "Class/Interface Decl":
                        {
                            Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            if (retr.Count == 0)
                                continue;
                            
                            foreach (string key in retr.Keys)
                            {
                                switch (key.Split('|')[0].Trim())
                                {
                                    case "Member":
                                        
                                            isSaveLocation = true;
                                            ParseBlock(retr[key], true, false, linenumber);
                                            isSaveLocation = false;
                                        
                                        break;
                                }
                            }
                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion

                    #region METHODDECL
                    case "Class Method Decl":
                        {
                            //тело метода
                            Dictionary<string, string> retr = reportcontroller.GetInnerBlocks(ref reader, ref line);
                            //реализовать проверку того, что в блоках нет вызовов
                            foreach (string key in retr.Keys)
                            {
                                switch (key.Split('|')[0].Trim())
                                {
                                    case "METHOD Body":
                                        {
                                            Dictionary<string, string> innerBlocks = reportcontroller.GetInnerBlocks(key, retr[key]);
                                            foreach (string innerKey in innerBlocks.Keys)
                                            {
                                                switch (innerKey.Split('|')[0].Trim())
                                                {
                                                    case "Functions Body":
                                                        {
                                                            isSaveLocation = true;
                                                            IStatement body = ParseBlock(innerBlocks[innerKey], false, false, linenumber);
                                                            isSaveLocation = false;
                                                            if (body == null)
                                                                continue;
                                                            //ищем текущую функцию
                                                            string funcName = ExtractName(split[1].Trim());
                                                            string funcFullName = ExtractName(split[1].Trim(), true);
                                                            IFunction searchFunc = null;

                                                            foreach (IFunction fun in storage.functions.GetFunction(funcName))
                                                            {
                                                                Location funDefenition = fun.Definition();
                                                                if (funDefenition != null && funDefenition.GetFile() == currentFile && fun.FullName == funcFullName && fun.EntryStatement == null)
                                                                {
                                                                    searchFunc = fun;
                                                                    break;
                                                                }
                                                            }

                                                            if (searchFunc == null)
                                                            {
                                                                IA.Monitor.Log.Error(PluginSettings.Name, "Не нашел соответствие определению метода " + funcName);
                                                                break;
                                                            }
                                                            if (searchFunc.EntryStatement != null)
                                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Ошибка в определении метода " + funcFullName);
                                                            searchFunc.EntryStatement = body;

                                                            IStatement last = GetLastStatement(searchFunc.EntryStatement);
                                                            last.SetLastSymbolLocation(currentFile, reportcontroller.ExtractLocation(innerKey, linenumber, true)[2], reportcontroller.ExtractLocation(innerKey, linenumber, true)[3]);
                                                            
                                                            //body = body;
                                                            break;
                                                        }
                                                }
                                               
                                            }
                                            break;
                                        }
                                }
                            }
                            isSaveLocation = true;
                            goto beginparse;
                        }
                    #endregion
                    case "Empty Statement":
                        {
                            // 1 - сохраняем позицию эмпти, выходим
                            /*
                             *  Empty Statement| (44,1)-(44,4)
                                Echo Statement| (45,1)-(50,2)
                                    Literal Base Class| (45,1)-(50,2) 
                             */
                            // 2
                            /*
                             *  Empty Statement| (44,1)-(44,4) - выходим
                             */
                            // 3 
                            /*
                             *  Empty Statement| (44,1)-(44,4)
                                Echo Statement| (45,1)-(50,2)
                                    Literal Base Class| (45,1)-(50,2) 
                             *  Empty Statement| (44,1)-(44,4)
                             */

                            string old = line;
                            line = reader.ReadLine();
                            if(line == null)
                            {
                                break;
                            }

                            while ((line = reader.ReadLine()) != null)
                                if (line.Contains(")-(") && !line.Contains("Literal Base Class"))
                                {
                                    lastStingEntryLevel = line;
                                    line = reader.ReadLine();
                                    goto beginparse;
                                }else
                                    lastStingEntryLevel = old;

                            break;
                        }
                }
            }

            if (stack.Count > 0)
                CallsToStore(ref entry, PopCallsFromStack(0), linenumber, isSkip);

            if (entry == null)
            {
                if (nullLocs[0] == 0 || nullLocs[1] == 0)
                    nullLocs = currentPHPCodeStartLocation;
                if (block == "")
                    return storage.statements.AddStatementOperational(nullLocs[0], nullLocs[1], currentFile, nullLocs[0], nullLocs[1]+1);

                //извлекаем конец из строки такого же уровня но последней
                endLocs = reportcontroller.ExtractLocation(lastStingEntryLevel.Trim(), linenumber, true);
                IStatementOperational empty = storage.statements.AddStatementOperational(nullLocs[0], nullLocs[1], currentFile, endLocs[2], endLocs[3]);

                entry = empty;
                //if (!isSkip)
                  //  storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
            }

            if(entry != null)
            {
                IStatement last = GetLastStatement(entry);

                endLocs = reportcontroller.ExtractLocation(lastStingEntryLevel.Trim(), linenumber, true);
                if(last.LastSymbolLocation != null && (last.LastSymbolLocation.GetLine() == endLocs[2] && last.LastSymbolLocation.GetColumn() < endLocs[3] || last.LastSymbolLocation.GetLine() < endLocs[2] ))
                {
                    last.SetLastSymbolLocation(currentFile, endLocs[2], endLocs[3]);
                }
            }
            
            return entry;
        }

        /// <summary>
        /// Добавление скобок при необходимости
        /// </summary>
        /// <param name="key"></param>
        private void AddBracesIfNeed(string key, int linenumber)
        {
            ReportController reportcontroller = new ReportController();

            string inner = key.TrimStart(); //сокращаем время, убираем пробелы только в начале
            if (!inner.StartsWith("Block Statement"))
            {
                if (inner.Contains("\n"))
                    inner = inner.Substring(0, inner.IndexOf("\n"));//обрезали ее под \n
                ulong[] locs = reportcontroller.ExtractLocation(inner, linenumber, true);
                
                braces[currentFile.FullFileName_Canonized].Add(new BracesCoord (locs[0], locs[1], locs[2], locs[3])); //Добавляем запись о скобках
            }
        }

        /// <summary>
        /// Добавление вызовов функций в хранилище
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="calls"></param>
        /// <param name="skip"></param>
        private void CallsToStore(ref IStatement entry, List<string> calls, int linenumber, bool skip = false)
        {
            ReportController reportcontroller = new ReportController();

            if (calls.Count > 0)
            { 
                ulong startCol = ulong.MaxValue;
                ulong startRow = ulong.MaxValue;
                ulong endCol = 0;
                ulong endRow = 0;

                foreach (string call in calls)
                {
                    ulong[] location = reportcontroller.ExtractLocation(call, linenumber, true);
                    if (location[0] <= startCol && location[1] <= startRow)
                    {
                        startCol = location[0];
                        startRow = location[1];
                    }
                    if ((location[2] >= endCol) || (location[2] == endCol && location[3] >= endRow))
                    {
                        endCol = location[2];
                        endRow = location[3];
                    }
                }
                
                if (entry == null)
                {
                    //формируем стейтмент с нормальным смещением
                    if (isEnterSensor)
                    {
                        if (entryLocation[0] == 0 || entryLocation[1] == 0)
                            entryLocation = currentPHPCodeStartLocation;
                        entry = storage.statements.AddStatementOperational(entryLocation[0], entryLocation[1], currentFile, endCol, endRow);
                        isEnterSensor = false;
                    }
                    else
                        entry = storage.statements.AddStatementOperational(startCol, startRow, currentFile, endCol, endRow);
                    
                    //здесь в любом случае создаем датчик
                    //if (!skip)
                      //  storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                    
                    IOperation fOp = storage.operations.AddOperation();
                    ((IStatementOperational)entry).Operation = fOp;
                    
                    foreach (string call in calls)
                    {
                        List<IFunction> add = GetFunctionName(call, currentFile);

                        if (add.Count > 0)
                            fOp.AddFunctionCallInSequenceOrder(add.ToArray());  //могут быть и перегрузки
                        
                    }
                }
                else
                {
                    IStatement st = GetLastStatement(entry);

                    if (st is IStatementOperational)
                    {
                        foreach (string call in calls)
                        {
                            List<IFunction> add = GetFunctionName(call, currentFile);
                            if (add.Count > 0)
                                ((IStatementOperational) st).Operation.AddFunctionCallInSequenceOrder(add.ToArray());
                                    //могут быть и перегрузки


                        }
                        if ((st.LastSymbolLocation.GetLine() < endCol) || (st.LastSymbolLocation.GetLine() == endCol && st.LastSymbolLocation.GetColumn() < endRow))
                        st.SetLastSymbolLocation(currentFile, endCol, endRow);
                    }
                    else
                    {
                        IStatement newSt;

                        if (isEnterSensor)
                        {

                            newSt = storage.statements.AddStatementOperational(entryLocation[0], entryLocation[1],
                                currentFile, endCol, endRow);
                            isEnterSensor = false;
                        }
                        else
                            newSt = storage.statements.AddStatementOperational(startCol, startRow, currentFile, endCol, endRow);
                        if (!skip) //это никогда не будет неверным
                        {
                            //storage.sensors.AddSensorBeforeStatement(sensorNumber++, newSt, Kind.INTERNAL);

                        }

                        st.NextInLinearBlock = newSt;
                        IOperation fOp = storage.operations.AddOperation();
                        ((IStatementOperational) newSt).Operation = fOp;

                        foreach (string call in calls)
                        {
                            List<IFunction> add = GetFunctionName(call, currentFile);

                            if (add.Count > 0)
                                fOp.AddFunctionCallInSequenceOrder(add.ToArray()); //могут быть и перегрузки

                        }
                    }
                }
            }
        }

        /// <summary>
        /// Получение списка функций из файла по имени
        /// </summary>
        /// <param name="str"></param>
        /// <param name="currF"></param>
        /// <returns></returns>
        private List<IFunction> GetFunctionName(string str, IFile currF)
        {
            List<IFunction> res = new List<IFunction>();
            string fullName = ExtractName(str, true);
            string shortName = (fullName.StartsWith("UnknownFunction_")) ? fullName : ExtractName(str); //в случае если ф-ия является методом - сначала все равно ищем по шортнейму

            //если это нераспознанный инклюд, возвращаем ничего =)
            if (shortName == "" && fullName == "")
                return res;

            //здесь короткое имя
            //а - имя может быть с глобал
            //б - имя может быть с классом
            //в - имя может быть усеченное

            IFunction[] funcs = storage.functions.GetFunction(shortName);
            foreach (IFunction fun in funcs)
            {
                //проверка - если глобал - то должно быть точное совпадение
                if (fullName.Contains("<global>"))
                {
                    if (fun.FullName == shortName)//имя без глобалов
                        //if (stt.reachable(currentFile, fun.Definition()[0].GetFile()))
                            res.Add(fun);

                }
                else
                {
                    //либо с классом, либо просто почему-то опущено
                    if (shortName == fullName)
                    {
                        //если фалангер не разобрался
                        //то совпадение должно быть до точки или полное
                        if (fun.FullName == shortName || fun.FullName.EndsWith("." + shortName))
                            //if (stt.reachable(currentFile, fun.Definition()[0].GetFile()))
                                res.Add(fun);
                    }
                    else
                    {
                        //в случае если есть класс - совпадение должно быть полным
                        if (fun.FullName == fullName)
                            //if (stt.reachable(currentFile, fun.Definition()[0].GetFile()))
                                res.Add(fun);
                    }
                }
            }

            if (!funcs.Any()) //не нашли такой функции в хранилище, значит она внешняя
            {
                IFunction external_func = storage.functions.AddFunction();
                external_func.Name = fullName;
                external_func.IsExternal = true;
                res.Add(external_func);
            }
            return res;
        }

        /// <summary>
        /// Второй проход
        /// </summary>
        /// <param name="storage"></param>
        internal AST_Pass2(Storage storage, string PhpCompilerReportPath)
        {
            ReportController reportcontroller = new ReportController();
            //Хранилище
            this.storage = storage;
            //Счётчик прогресса
            ulong count = 0;

            if (!File.Exists(PhpCompilerReportPath))
                return;

            //Для каждого найденного файла создаём функцию файла
            List<IFile> files = reportcontroller.GetFiles(storage, PhpCompilerReportPath);
            foreach (IFile file in files)
            {
                //добавляем функцию файла в Хранилище
                IFunction function = storage.functions.AddFunction();
                function.Name = file.FileNameForReports;
                function.AddDefinition(file, 1);
            }


            //Создаём файл отчёта по реторнам
            returnsReportWriter = new StreamWriter(storage.pluginData.GetDataFolder(PluginSettings.ID).Trim('\\') + "\\returndesc.log");
            //Создаём файл отчёта по файлам, не содрежищим PHP код
            //noPHPFilesReportWriter = (PluginSettings.newPrefix != "") ? new StreamWriter(PluginSettings.newPrefix + @"\..\NoPhpCodeFiles.txt") : new StreamWriter(PluginSettings.oldPrefix + @"\..\NoPhpCodeFiles.txt"); //FIXED
            noPHPFilesReportWriter = new StreamWriter(storage.pluginData.GetDataFolder(PluginSettings.ID).Trim('\\') + "\\NoPhpCodeFiles.txt");

            int linenumber = 0;

            using (StreamReader reader = new StreamReader(PhpCompilerReportPath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {

                beginparse2:
                    linenumber++;
                    if (line == null)
                        break;
                    
                    string[] split = line.Split('|');
                    switch (split[0].Trim())
                    {
                        case "----- File":
                            {
                                //Выясняем путь к файлу
                                string currentFileShortName = split[1].Trim();
                                //Задаём новый текущий файл и проверяем, что он есть в Хранилище
                                if ((currentFile = storage.files.Find(currentFileShortName)) == null)
                                    throw new Exception("Файл " + currentFileShortName + " не найден в Хранилище.");
                                
                                string currentFilePath = currentFile.FullFileName_Original;

                                //Отображаем информацию об обрабатываемом файле
                                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CODE_ANALIZE.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++count);

                                //Ищем PHP код в файле
                                currentPHPCodeStartLocation = GetPHPCodeStartLocation(currentFilePath);

                                //Пишем в отчёт о реторнах путь к текущему файлу
                                returnsReportWriter.WriteLine(Store.Files.CanonizeFileName(currentFilePath));

                                

                                //Вычисляем содержимое файла
                                if ((line = reader.ReadLine()) != null && line.Contains(" ----- File|"))
                                {
                                    goto beginparse2;
                                }

                                if (line == null)
                                    break;

                                StringBuilder block = new StringBuilder();
                                do
                                    block.AppendLine(line);
                                while ((line = reader.ReadLine()) != null && !line.StartsWith(" ----- File|"));
                                    
                                //Добавляем запись о файле в словарь скобок
                                braces.Add(currentFile.FullFileName_Canonized, new List<BracesCoord>());
                                
                                //Поднимаем флаги и создаём массив для хранения текущих координат
                                isSaveLocation = true;
                                isEnterSensor = true;
                                entryLocation = new ulong[] { 1, 0 };

                                //Создаём функцию файл
                                IStatement statement = ParseBlock(block.ToString(), false, true, linenumber);
                                if (statement != null)
                                {
                                    IFunction fileFunction = storage.functions.GetFunction(currentFile.FileNameForReports)[0];
                                    fileFunction.EntryStatement = statement;
                                }
                                else
                                    IA.Monitor.Log.Warning(PluginSettings.Name, "Найден пустой файл: " + currentFilePath);

                                //Очищаем стек, если что-то пошло не так
                                if (stack.Count != 0)
                                {
                                    IA.Monitor.Log.Warning(PluginSettings.Name, "При обработке файла " + currentFilePath + " в стеке остались необработанные вызовы функций");
                                    stack.Clear();
                                }

                                //Отображение прогресса
                                
                                goto beginparse2;
                            }
                    }
                }
            }

            //Закрываем открытые потоки
            noPHPFilesReportWriter.Close();
            returnsReportWriter.Close();
        }
    }
}
