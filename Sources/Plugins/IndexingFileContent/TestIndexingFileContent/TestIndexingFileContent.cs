﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;

using TestUtils;

namespace TestIndexingFileContent
{
    [TestClass]
    public class TestIndexingFileContent
    {/// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Проверка индексирования пустого Хранилища.
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_EmptyFolder()
        {
            const string sourcePostfixPart = @"IndexingFileContent\EmptyFolder\";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, @"IndexingFileContent\EmptyFolder\")))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, @"IndexingFileContent\EmptyFolder\"));
                    TestUtilsClass.Run_FillFileList(storage);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return storage.filesIndex.Enumerate().Count() == 0;
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка индексирования одного бинарного файла при стандартных настройках 
        /// (опции по проверке бинарных файлов изначально не установлены).
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_BinaryFileDefaultSettings()
        {
            const string sourcePostfixPart = @"IndexingFileContent\sources\OneBinary";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return storage.filesIndex.Enumerate().Count() == 0;
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка передачи настройки "Дополнительные символы" с повторяющимися символами. 
        /// Пример: результат плагина с настройкой "$|$|s|d" должен совпадать с результатом плагина с настройкой "$|s|d".
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_RepeatingSymbols()
        {
            const string sourcePostfixPart = @"IndexingFileContent\sources\commonSource";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Run_IdentifyFileTypes(storage);

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(true);
                    writer.Add(true);
                    writer.Add(true);
                    writer.Add("$|$|$|s|d");
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.INDEXING_FILE_CONTENT, writer);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    string reportOld = Path.Combine(testMaterialsPath, @"IndexingFileContent\ReportCommonSource.txt");

                    WriteIndexesIntoFile(reportNew, storage);

                    return TestUtilsClass.Reports_TXT_Compare(reportNew, reportOld, true, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                true,                                                                  // isUpdateReport
                (storage, testMaterialsPath) => {
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(true);
                    writer.Add(true);
                    writer.Add(true);
                    writer.Add("$|$|$|s|$|$|$|d");
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.INDEXING_FILE_CONTENT, writer);
                },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => {

                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    string reportOld = Path.Combine(testMaterialsPath, @"IndexingFileContent\ReportCommonSource.txt");

                    WriteIndexesIntoFile(reportNew, storage);

                    return TestUtilsClass.Reports_TXT_Compare(reportNew, reportOld, true, false, testMaterialsPath);
                },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Запись индексов Хранилища в файл.
        /// </summary>
        private void WriteIndexesIntoFile(string reportFile, Store.Storage storage)
        {
            using (StreamWriter writer = new StreamWriter(reportFile, false, System.Text.Encoding.UTF8))
            {
                foreach (var keyPair in storage.filesIndex.Enumerate())
                {
                    writer.WriteLine("{0}\t{1}", keyPair.Key, string.Join("\t", keyPair.Value.File, keyPair.Value.Offset));
                }
            }
        }

        /// <summary>
        /// Проверка корректности индексирования файлов с исполняемым кодом.
        /// В настройках плагина отмечена опция "Файлы с исполняемым кодом" в группе "Типы файлов".
        /// Тип файлов в Хранилище задается вручную.
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_TestExecutables()
        {
            const string sourcePostfixPart = @"IndexingFileContent\sources\OneBinary\";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Storage_Files_SetType(storage, new Store.FileType(@"Windows Dynamic Link Library"));

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(true);
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(string.Empty);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.INDEXING_FILE_CONTENT, writer);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    string reportOld = Path.Combine(testMaterialsPath, @"IndexingFileContent\ReportOneBinary.txt");

                    WriteIndexesIntoFile(reportNew, storage);

                    return TestUtilsClass.Reports_TXT_Compare(reportNew, reportOld, true, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности индексирования бинарных файлов, содержащих исходный код.
        /// В настройках плагина отмечена опция "Бинарные файлы, содержащие исходный код" в группе "Типы файлов".
        /// Тип файлов в Хранилище задается вручную.
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_TestBinarySources()
        {
            const string sourcePostfixPart = @"IndexingFileContent\sources\OneBinary";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Run_IdentifyFileTypes(storage);

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(true);
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(string.Empty);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.INDEXING_FILE_CONTENT, writer);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    string reportOld = Path.Combine(testMaterialsPath, @"IndexingFileContent\ReportOneBinary.txt");

                    WriteIndexesIntoFile(reportNew, storage);

                    return TestUtilsClass.Reports_TXT_Compare(reportNew, reportOld, true, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности индексирования файлов с исходным кодом.
        /// В настройках плагина отмечена опция "Файлы с исходным кодом" в группе "Типы файлов".
        /// Тип файлов в Хранилище задается вручную.
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_TestSources()
        {
            const string sourcePostfixPart = @"IndexingFileContent\sources\commonSource";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Storage_Files_SetType(storage, new Store.FileType(@"Исходные тексты на языке JavaScript"));

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(true);
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(string.Empty);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.INDEXING_FILE_CONTENT, writer);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    string reportOld = Path.Combine(testMaterialsPath, @"IndexingFileContent\ReportCommonSource.txt");

                    WriteIndexesIntoFile(reportNew, storage);

                    return TestUtilsClass.Reports_TXT_Compare(reportNew, reportOld, true, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности индексирования файлов с текстовыми данными.
        /// В настройках плагина отмечена опция "Файлы с данными" в группе "Типы файлов".
        /// Тип файлов в Хранилище задается вручную.
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_TestDataFiles()
        {
            const string sourcePostfixPart = @"IndexingFileContent\Sources\CommonSource";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Storage_Files_SetType(storage, new Store.FileType(@"Текстовый файл"));

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(true);
                    writer.Add(false);
                    writer.Add(string.Empty);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.INDEXING_FILE_CONTENT, writer);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    string reportOld = Path.Combine(testMaterialsPath, @"IndexingFileContent\ReportCommonSourceOnlyLower.txt");

                    WriteIndexesIntoFile(reportNew, storage);

                    return TestUtilsClass.Reports_TXT_Compare(reportNew, reportOld, true, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности индексирования неопределенных файлов.
        /// В настройках плагина отмечена опция "Неопределенные файлы" в группе "Типы файлов".
        /// Тип файлов в Хранилище задается вручную.
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_TestUnknownFiles()
        {
            const string sourcePostfixPart = @"IndexingFileContent\Sources";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Storage_Files_SetType(storage, Store.SpecialFileTypes.UNKNOWN);

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(true);
                    writer.Add(string.Empty);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.INDEXING_FILE_CONTENT, writer);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    string reportOld = Path.Combine(testMaterialsPath, @"IndexingFileContent\ReportUnknown.txt");

                    WriteIndexesIntoFile(reportNew, storage);

                    return TestUtilsClass.Reports_TXT_Compare(reportNew, reportOld, true, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности индексирования файлов, слова в которых не чувствительны к регистру. 
        /// Все слова должны быть проиндексированы в нижнем регистре.
        /// Тип файлов в Хранилище задается вручную.
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_TestWordCase()
        {
            const string sourcePostfixPart = @"IndexingFileContent\Sources\CommonSource";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Storage_Files_SetType(storage, new Store.FileType(@"Текстовый файл"));

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(false);
                    writer.Add(true);
                    writer.Add(true);
                    writer.Add(true);
                    writer.Add(string.Empty);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.INDEXING_FILE_CONTENT, writer);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    string reportOld = Path.Combine(testMaterialsPath, @"IndexingFileContent\ReportCommonSourceOnlyLower.txt");

                    WriteIndexesIntoFile(reportNew, storage);

                    return TestUtilsClass.Reports_TXT_Compare(reportNew, reportOld, true, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка индексирования файлов при стандартных настройках.
        /// Тип файлов в Хранилище задается вручную.
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_TestIndexers()
        {
            const string sourcePostfixPart = @"IndexingFileContent\TestIndexers";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    storage.files.Find(Path.Combine(testMaterialsPath, @"IndexingFileContent\TestIndexers\testBin.txt")).fileType = new Store.FileType(@"Windows Dynamic Link Library");
                    storage.files.Find(Path.Combine(testMaterialsPath, @"IndexingFileContent\TestIndexers\testBinSource.txt")).fileType = new Store.FileType(@"SQL Server Database Primary Data File");
                    storage.files.Find(Path.Combine(testMaterialsPath, @"IndexingFileContent\TestIndexers\testSource.txt")).fileType = new Store.FileType(@"Исходные тексты на языке JavaScript");
                    storage.files.Find(Path.Combine(testMaterialsPath, @"IndexingFileContent\TestIndexers\testData.txt")).fileType = new Store.FileType(@"Текстовый файл");
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    string reportOld = Path.Combine(testMaterialsPath, @"IndexingFileContent\ReportTestIndexers.txt");

                    WriteIndexesIntoFile(reportNew, storage);

                    return TestUtilsClass.Reports_TXT_Compare(reportNew, reportOld, true, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка, что файлы, которые есть в Хранилище, но отсутствуют на жестком диске, не индексируются.
        /// Тип файлов в Хранилище задается вручную.
        /// </summary>
        [TestMethod]
        public void IndexingFileContent_FileNotExists()
        {
            testUtils.RunTest(
                string.Empty,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(),      // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    Store.IFile file = storage.files.Add(Store.enFileKindForAdd.fileWithPrefix);
                    file.FullFileName_Original = Path.Combine(storage.appliedSettings.FileListPath, @"Меня нет на жестком диске.error");
                    file.fileType = new Store.FileType(@"Текстовый файл");
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return storage.filesIndex.Enumerate().Count() == 0;
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }
    }
}
