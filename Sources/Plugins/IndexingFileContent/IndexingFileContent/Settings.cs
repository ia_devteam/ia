﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.IndexingFileContent
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Работаем в экспертном режиме?
        /// </summary>
        bool isCustomSettings;        

        /// <summary>
        /// TextBox с символами
        /// </summary>
        TextBox symbols_textBox;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="isCustomSettings">true - экспертные настройки, false - простые настройки</param>
        internal Settings(bool isCustomSettings = true)
        {
            this.isCustomSettings = isCustomSettings;
            InitializeComponent();

            isProcessBinarySources_checkBox.Checked = PluginSettings.IsProcessBinarySources;
            isProcessExecutable_checkBox.Checked = PluginSettings.IsProcessExecutable;
            isProcessSources_checkBox.Checked = PluginSettings.IsProcessSources;
            isProcessData_checkBox.Checked = PluginSettings.IsProcessData;
            isProcessUnknown_checkBox.Checked = PluginSettings.IsProcessUnknown;

            if (this.isCustomSettings)
            {
                // Создаем TextBox
                this.symbols_textBox = new TextBox()
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Right
                };

                // Создаем TableLayoutPanel
                TableLayoutPanel symbols_tableLayoutPanel = new TableLayoutPanel()
                {
                    Dock = DockStyle.Fill
                };
                // Вкладываем в него TextBox
                symbols_tableLayoutPanel.Controls.Add(this.symbols_textBox);

                // Создаем GroupBox
                GroupBox symbols_groupBox = new GroupBox()
                {
                    Dock = System.Windows.Forms.DockStyle.Fill,
                    Text = "Дополнительные символы"
                };
                // Вкладываем в него TableLayoutPanel
                symbols_groupBox.Controls.Add(symbols_tableLayoutPanel);

                main_tableLayoutPanel.RowCount++;
                main_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 55F));
                main_tableLayoutPanel.Controls.Add(symbols_groupBox, 0, 1);
            }

            main_tableLayoutPanel.RowCount++;
            main_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.IsProcessExecutable = isProcessExecutable_checkBox.Checked;
            PluginSettings.IsProcessBinarySources = isProcessBinarySources_checkBox.Checked;
            PluginSettings.IsProcessSources = isProcessSources_checkBox.Checked;
            PluginSettings.IsProcessData = isProcessData_checkBox.Checked;
            PluginSettings.IsProcessUnknown = isProcessUnknown_checkBox.Checked;
            PluginSettings.Symbols = (this.isCustomSettings) ? symbols_textBox.Text : string.Empty;
        }
    }
}
