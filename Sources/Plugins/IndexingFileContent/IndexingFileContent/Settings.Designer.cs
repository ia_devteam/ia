﻿namespace IA.Plugins.Analyses.IndexingFileContent
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.filesTypes_groupBox = new System.Windows.Forms.GroupBox();
            this.filesTypes_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isProcessUnknown_checkBox = new System.Windows.Forms.CheckBox();
            this.isProcessData_checkBox = new System.Windows.Forms.CheckBox();
            this.isProcessSources_checkBox = new System.Windows.Forms.CheckBox();
            this.isProcessExecutable_checkBox = new System.Windows.Forms.CheckBox();
            this.isProcessBinarySources_checkBox = new System.Windows.Forms.CheckBox();
            this.main_tableLayoutPanel.SuspendLayout();
            this.filesTypes_groupBox.SuspendLayout();
            this.filesTypes_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.filesTypes_groupBox, 0, 0);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 1;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 181F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(276, 179);
            this.main_tableLayoutPanel.TabIndex = 4;
            // 
            // filesTypes_groupBox
            // 
            this.filesTypes_groupBox.Controls.Add(this.filesTypes_tableLayoutPanel);
            this.filesTypes_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filesTypes_groupBox.Location = new System.Drawing.Point(3, 3);
            this.filesTypes_groupBox.Name = "filesTypes_groupBox";
            this.filesTypes_groupBox.Size = new System.Drawing.Size(270, 175);
            this.filesTypes_groupBox.TabIndex = 0;
            this.filesTypes_groupBox.TabStop = false;
            this.filesTypes_groupBox.Text = "Типы файлов";
            // 
            // filesTypes_tableLayoutPanel
            // 
            this.filesTypes_tableLayoutPanel.ColumnCount = 1;
            this.filesTypes_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.filesTypes_tableLayoutPanel.Controls.Add(this.isProcessUnknown_checkBox, 0, 4);
            this.filesTypes_tableLayoutPanel.Controls.Add(this.isProcessData_checkBox, 0, 3);
            this.filesTypes_tableLayoutPanel.Controls.Add(this.isProcessSources_checkBox, 0, 2);
            this.filesTypes_tableLayoutPanel.Controls.Add(this.isProcessExecutable_checkBox, 0, 0);
            this.filesTypes_tableLayoutPanel.Controls.Add(this.isProcessBinarySources_checkBox, 0, 1);
            this.filesTypes_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filesTypes_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.filesTypes_tableLayoutPanel.Name = "filesTypes_tableLayoutPanel";
            this.filesTypes_tableLayoutPanel.RowCount = 5;
            this.filesTypes_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.filesTypes_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.filesTypes_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.filesTypes_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.filesTypes_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.filesTypes_tableLayoutPanel.Size = new System.Drawing.Size(264, 156);
            this.filesTypes_tableLayoutPanel.TabIndex = 0;
            // 
            // isProcessUnknown_checkBox
            // 
            this.isProcessUnknown_checkBox.AutoSize = true;
            this.isProcessUnknown_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isProcessUnknown_checkBox.Location = new System.Drawing.Point(3, 127);
            this.isProcessUnknown_checkBox.Name = "isProcessUnknown_checkBox";
            this.isProcessUnknown_checkBox.Size = new System.Drawing.Size(258, 26);
            this.isProcessUnknown_checkBox.TabIndex = 8;
            this.isProcessUnknown_checkBox.Text = "Неразобранные файлы";
            this.isProcessUnknown_checkBox.UseVisualStyleBackColor = true;
            // 
            // isProcessData_checkBox
            // 
            this.isProcessData_checkBox.AutoSize = true;
            this.isProcessData_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isProcessData_checkBox.Location = new System.Drawing.Point(3, 96);
            this.isProcessData_checkBox.Name = "isProcessData_checkBox";
            this.isProcessData_checkBox.Size = new System.Drawing.Size(258, 25);
            this.isProcessData_checkBox.TabIndex = 7;
            this.isProcessData_checkBox.Text = "Файлы с данными";
            this.isProcessData_checkBox.UseVisualStyleBackColor = true;
            // 
            // isProcessSources_checkBox
            // 
            this.isProcessSources_checkBox.AutoSize = true;
            this.isProcessSources_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isProcessSources_checkBox.Location = new System.Drawing.Point(3, 65);
            this.isProcessSources_checkBox.Name = "isProcessSources_checkBox";
            this.isProcessSources_checkBox.Size = new System.Drawing.Size(258, 25);
            this.isProcessSources_checkBox.TabIndex = 6;
            this.isProcessSources_checkBox.Text = "Файлы с исходным кодом";
            this.isProcessSources_checkBox.UseVisualStyleBackColor = true;
            // 
            // isProcessExecutable_checkBox
            // 
            this.isProcessExecutable_checkBox.AutoSize = true;
            this.isProcessExecutable_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isProcessExecutable_checkBox.Location = new System.Drawing.Point(3, 3);
            this.isProcessExecutable_checkBox.Name = "isProcessExecutable_checkBox";
            this.isProcessExecutable_checkBox.Size = new System.Drawing.Size(258, 25);
            this.isProcessExecutable_checkBox.TabIndex = 5;
            this.isProcessExecutable_checkBox.Text = "Файлы с исполняемым кодом";
            this.isProcessExecutable_checkBox.UseVisualStyleBackColor = true;
            // 
            // isProcessBinarySources_checkBox
            // 
            this.isProcessBinarySources_checkBox.AutoSize = true;
            this.isProcessBinarySources_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isProcessBinarySources_checkBox.Location = new System.Drawing.Point(3, 34);
            this.isProcessBinarySources_checkBox.Name = "isProcessBinarySources_checkBox";
            this.isProcessBinarySources_checkBox.Size = new System.Drawing.Size(258, 25);
            this.isProcessBinarySources_checkBox.TabIndex = 9;
            this.isProcessBinarySources_checkBox.Text = "Бинарные файлы, содержащие исходный код";
            this.isProcessBinarySources_checkBox.UseVisualStyleBackColor = true;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(276, 179);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.filesTypes_groupBox.ResumeLayout(false);
            this.filesTypes_tableLayoutPanel.ResumeLayout(false);
            this.filesTypes_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.GroupBox filesTypes_groupBox;
        private System.Windows.Forms.TableLayoutPanel filesTypes_tableLayoutPanel;
        private System.Windows.Forms.CheckBox isProcessUnknown_checkBox;
        private System.Windows.Forms.CheckBox isProcessData_checkBox;
        private System.Windows.Forms.CheckBox isProcessSources_checkBox;
        private System.Windows.Forms.CheckBox isProcessExecutable_checkBox;
        private System.Windows.Forms.CheckBox isProcessBinarySources_checkBox;

    }
}
