﻿using System;
using System.Collections.Generic;

using FileOperations;
using Store;

using IA.Extensions;

namespace IA.Plugins.Analyses.IndexingFileContent
{
    internal class Logic
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Текущий список символов
        /// </summary>
        List<char> listChar;
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage">Хранилище</param>
        internal Logic(Storage storage)
        {
            this.storage = storage;
        }

        internal void Run()
        {
            storage.filesIndex.Clear();

            //Обновляем максимальный прогресс тасков
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, storage.files.Count());

            storage.appliedSettings.SetIndexChars(PluginSettings.Symbols);
            ulong counter = 0;
            listChar = storage.appliedSettings.IndexChars;

            foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                // Проблемы с типом файла, зависает запрос к SQL и из-за этого падают тесты и дебаг.
                // Но даже это не помогает, т.к. падает один из тестов при пакетном запуске.
                // По одному - все хорошо.
                FileType fType = file.fileType;                
                enTypeOfContents contains = fType.Contains();

                if (PluginSettings.IsProcessExecutable && contains.HasFlag(enTypeOfContents.EXECUTABLE) ||
                    PluginSettings.IsProcessBinarySources && contains.HasFlag(enTypeOfContents.BINARYSOURCES) ||
                    PluginSettings.IsProcessSources && contains.HasFlag(enTypeOfContents.TEXTSOURCES) ||
                    PluginSettings.IsProcessData && contains.HasFlag(enTypeOfContents.DATA) ||
                    PluginSettings.IsProcessUnknown && (fType == SpecialFileTypes.UNKNOWN || contains == 0))                    
                {
                    AbstractReader buffer = null;
                    try
                    {
                        buffer = new AbstractReader(file.FullFileName_Original);//, AbstractReader.OpenMode.SLASH0IGNORE);
                    }
                    catch (Exception ex)
                    {
                        //Формируем сообщение
                        string message = new string[]
                        {
                            "Не удалось открыть файл " + file.FullFileName_Original + ".",
                            ex.ToFullMultiLineMessage()
                        }.ToMultiLineString();

                        IA.Monitor.Log.Warning(PluginSettings.Name, message);
                        continue;
                    }
                    string line;
                    int num = 0;
                    while ((line = buffer.getLine()) != null)
                    {
                        num++;
                        Dictionary<int, string> ret = extractWord(line, listChar);

                        foreach (int offset in ret.Keys)
                        {
                            storage.filesIndex.Add(file, ret[offset], (ulong)(buffer.getLineOffset(num) + offset));
                        }
                    }
                }

                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }
        }

        private Dictionary<int, string> extractWord(string s, List<char> listChar)
        {
            Dictionary<int, string> ret = new Dictionary<int, string>();

            if (s == "")
                return ret;

            bool start = false;
            int begin = 0;
            int count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                char sym = s[i];
                if (((sym >= 'A') && (sym <= 'Z')) || (sym == '_') || ((sym >= 'a') && (sym <= 'z')) || ((sym >= 'А') && (sym <= 'Я')) || ((sym >= 'а') && (sym <= 'я')) || ((sym >= '0') && (sym <= '9')) || (listChar.Count > 0) && (listChar.Contains(sym)))
                {
                    if (!start)
                    {
                        start = true;
                        begin = i;
                    }
                    count++;
                }
                else
                {
                    if (start)
                    {
                        ret.Add(begin, s.Substring(begin, count));
                        start = false;
                        begin = 0;
                        count = 0;
                    }
                }
            }
            if (start)
                ret.Add(begin, s.Substring(begin, count));

            return ret;
        }
    }
}
