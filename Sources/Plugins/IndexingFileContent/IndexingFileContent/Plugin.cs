using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Analyses.IndexingFileContent
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Индексирование файлов";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.INDEXING_FILE_CONTENT;

        #region Настройки

        /// <summary>
        /// Включать файлы с исполняемым кодом?
        /// </summary>
        internal static bool IsProcessExecutable;

        /// <summary>
        /// Включать бинарные файлы, содержащие исходный код?
        /// </summary>
        internal static bool IsProcessBinarySources;

        /// <summary>
        /// Включать файлы с исходным кодом?
        /// </summary>
        internal static bool IsProcessSources;

        /// <summary>
        /// Включать файлы с данными?
        /// </summary>
        internal static bool IsProcessData;

        /// <summary>
        /// Включать неразобранные файлы?
        /// </summary>
        internal static bool IsProcessUnknown;

        /// <summary>
        /// Список символов
        /// </summary>
        internal static string Symbols;

        #endregion

        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка файлов")]
            FILES_PROCESSING
        };
    }

    /// <summary>
    /// Плагин
    /// </summary>
    public class IndexingFileContent : IA.Plugin.Interface
    {	
        /// <summary>
        /// Текущее хранилище
        /// </summary>
		Storage storage;

        /// <summary>
        /// Текущие настройки
        /// </summary>
        IA.Plugin.Settings settings;
                
        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW | Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.IsProcessExecutable = Properties.Settings.Default.IsProcessExecutable;
            PluginSettings.IsProcessBinarySources = Properties.Settings.Default.IsProcessBinarySources;
            PluginSettings.IsProcessSources = Properties.Settings.Default.IsProcessSources;
            PluginSettings.IsProcessData = Properties.Settings.Default.IsProcessData;
            PluginSettings.IsProcessUnknown = Properties.Settings.Default.IsProcessUnknown;
            PluginSettings.Symbols = Properties.Settings.Default.Symbols;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.IsProcessExecutable);
            settingsBuffer.GetBool(ref PluginSettings.IsProcessBinarySources);
            settingsBuffer.GetBool(ref PluginSettings.IsProcessSources);
            settingsBuffer.GetBool(ref PluginSettings.IsProcessData);
            settingsBuffer.GetBool(ref PluginSettings.IsProcessUnknown);
            PluginSettings.Symbols = settingsBuffer.GetString();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return (settings = (new Settings(false)) as IA.Plugin.Settings) as UserControl;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        { 
            get
            {
                return (settings = (new Settings()) as IA.Plugin.Settings) as UserControl;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.IsProcessExecutable = PluginSettings.IsProcessExecutable;
            Properties.Settings.Default.IsProcessBinarySources = PluginSettings.IsProcessBinarySources;
            Properties.Settings.Default.IsProcessSources = PluginSettings.IsProcessSources;
            Properties.Settings.Default.IsProcessData = PluginSettings.IsProcessData;
            Properties.Settings.Default.IsProcessUnknown = PluginSettings.IsProcessUnknown;
            Properties.Settings.Default.Symbols = PluginSettings.Symbols;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.IsProcessExecutable);
            settingsBuffer.Add(PluginSettings.IsProcessBinarySources);
            settingsBuffer.Add(PluginSettings.IsProcessSources);
            settingsBuffer.Add(PluginSettings.IsProcessData);
            settingsBuffer.Add(PluginSettings.IsProcessUnknown);
            settingsBuffer.Add(PluginSettings.Symbols);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (!PluginSettings.IsProcessExecutable && !PluginSettings.IsProcessBinarySources && !PluginSettings.IsProcessData && !PluginSettings.IsProcessSources && !PluginSettings.IsProcessUnknown)
            {
                message = "Не отмечен ни один из типов файлов для индексирования.";
                return false;
            }
            if (!string.IsNullOrEmpty(PluginSettings.Symbols)
                && (PluginSettings.Symbols.Split('|').Where(x => x.Length == 0 || x.Length == 2 || x.Length > 3).Count() != 0
                || PluginSettings.Symbols.Split('|').Where(x => x.Length == 3 && x[1] != '-').Count() != 0))
            {
                message = "Неверный формат строки дополнительных символов.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), 0)
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            (new Logic(storage)).Run();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        #endregion
    }
}
