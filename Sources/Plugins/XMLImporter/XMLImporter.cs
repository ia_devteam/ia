﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IA.Plugin;
using System.Xml;
using Store;

namespace IA.Plugins.Importers.XMLImporter
{
    internal static class PluginSettings
    {
        public static string XMLToLoad { get; set; }
        public static Storage storage;
    }

    public class XmlImporter : IA.Plugin.DefaultPlugin
    {
        public override uint ID { get { return Store.Const.PluginIdentifiers.XMLIMPORTER; } }

        public override string Name { get { return "XML Importer"; } }

        public override Capabilities Capabilities { get { return Capabilities.CUSTOM_SETTINGS_WINDOW | Capabilities.RERUN; } }
        public override void Initialize(Storage storage)
        {
            PluginSettings.storage = storage;
            PluginSettings.XMLToLoad = String.Empty;
        }

        public override UserControl CustomSettingsWindow

        {
            get
            {
                var c = new XMLImporterCustomSettings();
                c.txtXmlToLoad.TextBox.Text = PluginSettings.XMLToLoad;
                c.txtXmlToLoad.TextBox.TextChanged += (o, e) => { PluginSettings.XMLToLoad = c.txtXmlToLoad.TextBox.Text; };
                return c;
            }
        }

        public override void Run()
        {
            StorageImporter.XMLStorageImporterForParser imp = new StorageImporter.XMLStorageImporterForParser();
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(PluginSettings.XMLToLoad);
            imp.ImportXML(xdoc, PluginSettings.storage);
        }
    }
}
