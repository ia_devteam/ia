﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.SourceReducer
{
    /// <summary>
    /// Реализация настроек режима пользователя
    /// </summary>
    public partial class SimpleSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Элемент управления - каталог с измененными исходными текстами
        /// </summary>
        ResultDirectoryPathControl resultDirectoryPathControl = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        public SimpleSettings()
        {
            InitializeComponent();

            resultDirectoryPathControl = new ResultDirectoryPathControl()
            {
                ResultPath = PluginSettings.ResultDirectoryPath,
                Dock = DockStyle.Fill
            };

            settings_tableLayoutPanel.Controls.Add(resultDirectoryPathControl, 0, 0);
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.ResultDirectoryPath = resultDirectoryPathControl.ResultPath;
        }
    }
}
