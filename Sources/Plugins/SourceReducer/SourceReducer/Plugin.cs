using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Analyses.SourceReducer
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.SOURCE_REDUCER;

        /// <summary>
        /// Имя плагина
        /// </summary>
        internal const string Name = "Удаление избыточных функциональных объектов";

        #region Settings

        /// <summary>
        /// Путь к директории с результатом
        /// </summary>
        internal static string ResultDirectoryPath;

        #endregion

        /// <summary>
        /// Задачи плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка функций")]
            FUNCTIONS_PROCESSING,
            [Description("Запись файлов")]
            FILES_WRITING
        }
    }
    
    public class SourceReducer : IA.Plugin.Interface
    {
        Storage storage;
        Plugin.Settings settings;
        Analyse analiser;

        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW | Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
		}

		/// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            if (analiser == null)
                return;

            if (string.IsNullOrWhiteSpace(reportsDirectoryPath))
                return;

            using (StreamWriter deletedFuncionsFile = new StreamWriter(Path.Combine(reportsDirectoryPath, "Закомментированные определения функций.txt"), false, Encoding.Default))
            {
                string tempFileName = "";
                foreach (ReportFunction reportFunction in analiser.deletedFunctionsDefenitions.OrderBy(report => report.Function.Definition().GetFile().FileNameForReports))
                {
                    if (!reportFunction.Function.FullNameForReports.Equals(tempFileName))
                    {
                        deletedFuncionsFile.WriteLine();
                        deletedFuncionsFile.WriteLine("Файл " + reportFunction.Function.Definition().GetFile().FileNameForReports + ":");
                        tempFileName = reportFunction.Function.Definition().GetFile().FileNameForReports;
                    }
                    deletedFuncionsFile.WriteLine("\t" + reportFunction.Function.FullNameForReports + "\t" + reportFunction.Function.Definition().GetLine() + ":" + reportFunction.Function.Definition().GetColumn());
                }
            }

            using (StreamWriter statisticsFile = new StreamWriter(Path.Combine(reportsDirectoryPath, "Статистика.txt"), false, Encoding.Default))
            {
                statisticsFile.WriteLine("Общее количество функций:");
                statisticsFile.WriteLine("\t" + storage.functions.Count);
                statisticsFile.WriteLine("Количество закомментированных определений функций:");
                statisticsFile.WriteLine("\t" + analiser.deletedFunctionsDefenitions.Count);
            }

            using (StreamWriter statisticsFile = new StreamWriter(Path.Combine(reportsDirectoryPath, "По-файловая статистика.txt"), false, Encoding.Default))
            {
                foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
                {
                    statisticsFile.WriteLine("Файл " + file.FileNameForReports);
                    //if (anal.deletedFunctionsDefenitions.Any(func => func.Function.Definition().GetFile().Equals(file)))
                    //{
                        statisticsFile.WriteLine("\tКоличество закомментированных определений функций:");
                        statisticsFile.WriteLine("\t\t" + analiser.deletedFunctionsDefenitions.Where(func => func.Function.Definition().GetFile().Equals(file)).Count());
                        statisticsFile.WriteLine("\tКоличество незакомментированных определений функций:");
                        statisticsFile.WriteLine("\t\t" + (file.FunctionsDefinedInTheFile().Count() - analiser.deletedFunctionsDefenitions.Where(func => func.Function.Definition().GetFile().Equals(file)).Count()));
                    //}
                    //else
                    //{
                    //    statisticsFile.WriteLine("\tКоличество незакомментированных определений функций:");
                    //    statisticsFile.WriteLine("\t\t" + file.FunctionsDefinedInTheFile().Count());
                    //}
                }
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.ResultDirectoryPath = Properties.Settings.Default.ResultDirectoryPath;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.ResultDirectoryPath = settingsBuffer.GetString();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNCTIONS_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)storage.functions.EnumerateFunctionLocationsInSortOrder().ToArray().Length);
            analiser = new Analyse(storage, PluginSettings.ResultDirectoryPath);
            analiser.Run();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.ResultDirectoryPath = PluginSettings.ResultDirectoryPath;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.ResultDirectoryPath);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {    
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FUNCTIONS_PROCESSING.Description()),
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_WRITING.Description())
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

		{
            get
            {
			    return null;
            }
		}

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new CustomSettings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new SimpleSettings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (!Directory.Exists(PluginSettings.ResultDirectoryPath))
            {
                message = "Путь к директории с результатом не найден.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        #endregion
    }
}
