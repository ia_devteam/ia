﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.SourceReducer
{
    public partial class ResultDirectoryPathControl : UserControl
    {
        public string ResultPath
        {
            get
            {
                return resultDirectoryPath_textBox.Text;
            }
            set
            {
                resultDirectoryPath_textBox.Text = value;
            }
        }

        public ResultDirectoryPathControl()
        {
            InitializeComponent();
        }
    }
}
