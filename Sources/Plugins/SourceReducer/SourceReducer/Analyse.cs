using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

using Store;
using FileOperations;
using IA.Extensions;

namespace IA.Plugins.Analyses.SourceReducer
{
    public class ReportFunction
    {
        public IFunction Function { get; private set; }
        public ReportFunction(IFunction function)
        {
            this.Function = function;
        }
    }

    class Analyse
    {
        public HashSet<ReportFunction> deletedFunctionsDefenitions { get; private set; }

        Storage storage;
        Functions functions;
        string newPrefix, oldPrefix;
        
        public Analyse(Storage storage, string newPrefix)
        {
            this.storage = storage;
            this.functions = storage.functions;
            oldPrefix = storage.appliedSettings.FileListPath;
            this.newPrefix = newPrefix + "\\";

            deletedFunctionsDefenitions = new HashSet<ReportFunction>();
        }
        public void Run()
        {
            string fileNameOriginal;
            Dictionary<string, string> cacheContents = new Dictionary<string, string>();
            IEnumerator<LocationFunctionPair> enu = functions.EnumerateFunctionLocationsInSortOrder().GetEnumerator();
            if (enu != null)
                enu.MoveNext(); // Если есть - первая функция
            //еще на одну функцию
            enu.MoveNext(); // Если есть - вторая функция

            int cf = 0; // счетчик функций для task
            Dictionary<string, List<KeyValuePair<int, int>>> comment = new Dictionary<string, List<KeyValuePair<int, int>>>();
            Dictionary<string, List<int>> skip = new Dictionary<string, List<int>>();
            foreach (LocationFunctionPair lfp in functions.EnumerateFunctionLocationsInSortOrder())
            {
                IFunction func = lfp.function;
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNCTIONS_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++cf);
                
                Location loc = func.Definition();
                fileNameOriginal = loc.GetFile().FullFileName_Original;
                if (!cacheContents.Keys.Contains(fileNameOriginal)) // Если файл уже в кэше, обходим проверку
                {
                    string[] langs;
                    try
                    {
                        IFile fl = loc.GetFile();
                        langs = loc.GetFile().fileType.ContainsLanguages();
                    }
                    catch
                    {
                        langs = new string[0];
                    }
                    if (langs.Length == 0 || (!langs.Contains(enLanguage.C.ToString()) && !langs.Contains(enLanguage.C_Sharp.ToString()) && !langs.Contains(enLanguage.Cpp.ToString()) && !langs.Contains(enLanguage.Java.ToString())))
                    {
                        enu.MoveNext(); // Если язык исходного текста не С,С++,С#,JAVA, то функцию пропускаем
                        continue;
                    }
                }
                if ((func.Essential == enFunctionEssentials.REDUNDANT || func.Essential == enFunctionEssentials.UNKNOWN) && loc != null)
                { 
                    // Функция избыточная, либо неизвестная (будет комментироваться)
                    
                    string funcName = func.Name;
                    CachedReader fr = new CachedReader(fileNameOriginal);
                    string fa = fr.getText();
                    string fb;
                    if (cacheContents.Keys.Contains(fileNameOriginal))
                        fb = cacheContents[fileNameOriginal];
                    else
                    {
                        fb = RemoveDefines(RemoveComments(ReplaceQuotesStrings(fa)));
                        cacheContents.Add(fileNameOriginal, fb);
                    }

                    if(fa.Length!=fb.Length)
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Ошибка при разборе исходных текстов");
                    }


                    int startpos = StartFunctionPosition(fb, (int)loc.GetLine(), (int)loc.GetColumn());
                    if (startpos == -1)
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Ошибка при определении начала функции");
                        continue;
                    }
                    int endpos = EndFunction(fb, startpos);
                    if (endpos == -1)
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Ошибка при определении конца функции"); 
                        continue;
                    }
                    int sl = LineFromOffset(fb, startpos);
                    int el = LineFromOffset(fb, endpos);
                    if (sl == -1 || el == -1)
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Ошибка при определении номеров строк функции");
                        continue;
                    }

                    if (!comment.ContainsKey(fileNameOriginal))
                        comment.Add(fileNameOriginal, new List<KeyValuePair<int, int>>());
                    comment[fileNameOriginal].Add(new KeyValuePair<int, int>(sl, el));

                    deletedFunctionsDefenitions.Add(new ReportFunction(func));
                    
                }
            

                enu.MoveNext();
            }

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_WRITING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)comment.Keys.Count);
            //Monitor.Monitor.informNewState(tasks);

            int ckc = 0;

            // Рекурсивная очистка директории и создание корневой директории
            if (Directory.Exists(PluginSettings.ResultDirectoryPath))
                Directory.Delete(PluginSettings.ResultDirectoryPath,true);
            Directory.CreateDirectory(PluginSettings.ResultDirectoryPath);

            foreach (string key in comment.Keys)
            {
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_WRITING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++ckc);

                CachedReader cr = new CachedReader(key);
                string[] source = cr.getText().Split('\n');
                string[] zip = new string[source.Length];
                int start=0;
                foreach(KeyValuePair<int, int> kvp in comment[key])
                {
                    for (; start <= kvp.Key; start++)
                    {
                        zip[start] = string.Empty;
                    }
                    for(;start<=kvp.Value;start++)
                    {
                        zip[start] = "//";
                    }
                }
                for (; start < source.Length; start++)
                {
                    zip[start] = string.Empty;
                }

                string result = zip.Zip(source, (a, b) => a + b).Aggregate((a, b) => a + Environment.NewLine + b);

                if (!Directory.Exists(Path.GetDirectoryName(key.ToLower().Replace(oldPrefix.ToLower(), newPrefix.ToLower()))))
                    Directory.CreateDirectory(Path.GetDirectoryName(key.ToLower().Replace(oldPrefix.ToLower(), newPrefix.ToLower())));
                (new AbstractReader(key)).writeTextToFile(key.ToLower().Replace(oldPrefix.ToLower(), newPrefix.ToLower()), result);
            }
        }

        private static int getLineStart(Location loc, CachedReader fr)
        {
            int linestart = (int)loc.GetLine();
            while (linestart > 0)
            {
                linestart--;
                string pline = fr.readLineN(linestart);// SkipNLinesAndReadNext(linestart);
                if (pline.Trim() == "" || pline.Contains(";") || pline.Contains("}") || pline.Contains("{") || pline.Contains("*/") || pline.Contains("//") || pline.Contains("public:") || pline.Contains("private:") || pline.Contains("protected:"))
                {
                    linestart++;
                    break;
                }
            }
            return linestart;
        }

        private static string deComment(string inner)
        {
            while (inner.Contains("/*"))
                if (inner.Contains("*/"))
                    inner = inner.Substring(0, inner.IndexOf("/*")) + inner.Substring(inner.IndexOf("*/") + 2);
                else
                    break;
            while (inner.Contains("//"))
                if (inner.IndexOf("\n", inner.IndexOf("//")) > 0)
                    inner = inner.Substring(0, inner.IndexOf("//")) + inner.Substring(inner.IndexOf("\n", inner.IndexOf("//")) + 1);
                else
                    inner = inner.Substring(0, inner.IndexOf("//"));
            return inner;
        }

        /// <summary>
        /// Замена #define на пробелы с сохранением позиционирования
        /// </summary>
        /// <param name="fb"></param>
        /// <returns></returns>
        public static string RemoveDefines(string fb)
        {
            List<string> res = new List<string>();
            List<string> lines = fb.Split('\n').ToList();
            
            bool nextdefine = false;
            List<string> definestr = new List<string>();
            foreach(string line in lines)
            {
                if (StartsWithDefine(line) || nextdefine)
                {
                    if (line.Trim(new char[] { '\r', ' ' }).EndsWith("\\"))
                    {
                        nextdefine = true;
                    }
                    else
                        nextdefine = false;
                    StringBuilder sb = new StringBuilder();
                    sb.Append(';');
                    sb.Append(line.Substring(1));
                    res.Add(sb.ToString());
                }
                else
                {
                    res.Add(line);
                }
            }
            if (res.Count > 0)
                return res.Aggregate((a, b) => a + "\n" + b);
            else
                return string.Empty;
        }
        static bool StartsWithDefine(string line)
        {
            string res = line.Trim();
            if (res.StartsWith("#"))
                if (res.Substring(1).Trim().ToLower().StartsWith("define"))
                    return true;
            return false;
        }
        /// <summary>
        /// Замена комментариев в теле файла на пробелы с сохранением позиционирования
        /// </summary>
        /// <param name="fb"></param>
        /// <returns></returns>
        public static string RemoveComments(string fb)
        {
            StringBuilder Ret = new StringBuilder();
            int start = 0;
            int end = -1;
            string xxx = new string('x', 10);
            while (start != -1)
            {
                end = fb.IndexOf("/*", start);
                if (end != -1)
                {
                    int indexbefore = indexBefore(fb, end, '\n');
                    if (fb.Substring(indexbefore + 1, end - indexbefore).Contains("//"))
                    {
                        Ret.Append(fb.Substring(start, end - start) + "  ");
                        start = end + 2;
                        continue;
                    }
                    Ret.Append(fb.Substring(start, end - start) + "  ");
                    start = end + 2;
                    end = fb.IndexOf("*/", start);
                    if (end == -1)
                    {
                        break;
                    }
                    string blanks = fb.Substring(start, end - start);
                    for (int i = 0; i < blanks.Length; i++)
                    {
                        Ret.Append((blanks[i] == '\n') ? "\n" : (blanks[i] == '\r') ? "\r" : " ");
                    }
                    Ret.Append("  ");
                    start = end + 2;
                }
                else
                {
                    Ret.Append(fb.Substring(start));
                    break;
                }
            }
            // //
            StringBuilder nocomm = new StringBuilder();
            string Src = Ret.ToString();
            start = 0;
            end = -1;
            while (true)
            {
                end = Src.IndexOf("//", start);
                if (end != -1)
                {
                    nocomm.Append(Src.Substring(start, end - start) + "  ");
                    start = end + 2;
                    end = Src.IndexOf("\n", start);
                    if (end == -1)
                    {
                        break;
                    }
                    string blanks = fb.Substring(start, end - start);
                    for (int i = 0; i < blanks.Length; i++)
                    {
                        nocomm.Append((blanks[i] == '\n') ? "\n" : (blanks[i] == '\r') ? "\r" : " ");
                    }
                    start = end;
                }
                else
                {
                    nocomm.Append(Src.Substring(start));
                    break;
                }
            }
            return nocomm.ToString();
        }
        /// <summary>
        /// Индекс символа относительно начала текста с текущей позиции строки в обратную сторону
        /// </summary>
        /// <param name="s"></param>
        /// <param name="pos"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        static int indexBefore(string s, int pos, char c)
        {
            if (pos >= s.Length)
                return -1;
            for (; pos >= 0; pos--)
                if (s[pos] == c)
                    break;
            return pos;
        }
        /// <summary>
        /// Замена строковых литералов на литералы из пробелов с сохранением позиционирования
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        public static string ReplaceQuotesStrings(string script)
        {
            const string RegexString = "(\"([^\"\\\\]*\\\\.)*[^\"\\\\]*\")+|@\"[^\"]*\"";
            //const string RegexString ="\"([^\"\\\\]|(\\\\(\")?))*\"|@\"[^\"]*\"";
            Regex reg = new Regex(RegexString);
            string script1 = reg.Replace(script, new MatchEvaluator(MM));
            return script1;
        }

        /// <summary>
        /// Делегат для REGEX
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        static string MM(Match m)
        {
            string ss = m.ToString();
            StringBuilder sb = new StringBuilder();
            sb.Append(ss[0]);
            int i = 1;
            if (ss[0] == '@')
            {
                sb.Append(ss[1]);
                i++;
            }
            for (; i < m.Length; i++)
            {
                if (i != ss.Length - 1)
                    sb.Append(" ");
                else
                    sb.Append(ss[i]);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Вычисляет offset конца функции
        /// </summary>
        /// <param name="fb">тело файла</param>
        /// <param name="startpos">начаьный offset</param>
        /// <returns></returns>
        static int EndFunction(string fb, int startpos)
        {
            int level = 0;
            if (startpos >= fb.Length)
                return -1;
            int pos = fb.IndexOf('{', startpos);
            if (pos == -1)
                return pos;
            startpos = pos + 1;
            level++;
            while(level>0 && startpos<fb.Length)
            {
                switch(fb[startpos])
                {
                    case '{':
                        level++;
                        break;
                    case '}':
                        level--;
                        break;
                }
                startpos++;
            }
            if (level != 0)
                return -1;
            return startpos;
        }

        /// <summary>
        /// Замена фунции на пробелы с сохранением позиционирования
        /// </summary>
        /// <param name="fn"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        static string BlancFunction(string fn, int start, int end)
        {
            if (!(start >= 0 && start < fn.Length) || !(end > 0 && end <= fn.Length))
                return fn;
            StringBuilder sb = new StringBuilder();
            if (start > 0)
                sb.Append(fn.Substring(0, start));
            for (int i = start; i < end; i++)
            {
                switch (fn[i])
                {
                    case '\n':
                        sb.Append('\n');
                        break;
                    case '\r':
                        sb.Append('\r');
                        break;
                    default:
                        sb.Append(' ');
                        break;
                }
            }
            if (fn.Length > end)
                sb.Append(fn.Substring(end, fn.Length-end));
            return sb.ToString();
        }

        /// <summary>
        /// вычисление offset для начала функции по строке и столбцу описания
        /// </summary>
        /// <param name="fb"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        static int StartFunctionPosition(string fb, int row, int col)
        {
            int ret = 0;
            for (int rr=0; ret < fb.Length && rr<row-1; ret++)
            {
                if (fb[ret] == '\n')
                    rr++;
            }

            ret = ret+col-1;
            while(ret>0)
            {
                if (fb[ret] == ';' || fb[ret] == '}' || fb[ret]=='#') //Завершение оператора, блока или команда препроцессора
                    break;
                ret--;
            }
            return ++ret;
        }

        /// <summary>
        /// Оаределение номера строки по offset
        /// </summary>
        /// <param name="fb"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        static int LineFromOffset(string fb, int pos)
        {
            int line = 0;
            for(int i=0; i<pos && i<fb.Length;i++)
            {
                if (fb[i] == '\n')
                    line++;
            }
            return line;
        }
    }
}
