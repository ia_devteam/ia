﻿namespace IA.Plugins.Analyses.SourceReducer
{
    partial class ResultDirectoryPathControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultDirectoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.resultDirectoryPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.resultDirectoryPath_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultDirectoryPath_groupBox
            // 
            this.resultDirectoryPath_groupBox.Controls.Add(this.resultDirectoryPath_textBox);
            this.resultDirectoryPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultDirectoryPath_groupBox.Location = new System.Drawing.Point(0, 0);
            this.resultDirectoryPath_groupBox.Name = "resultDirectoryPath_groupBox";
            this.resultDirectoryPath_groupBox.Size = new System.Drawing.Size(421, 45);
            this.resultDirectoryPath_groupBox.TabIndex = 1;
            this.resultDirectoryPath_groupBox.TabStop = false;
            this.resultDirectoryPath_groupBox.Text = "Путь к директории с результатом";
            // 
            // resultDirectoryPath_textBox
            // 
            this.resultDirectoryPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultDirectoryPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.resultDirectoryPath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.resultDirectoryPath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.resultDirectoryPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.resultDirectoryPath_textBox.Name = "resultDirectoryPath_textBox";
            this.resultDirectoryPath_textBox.Size = new System.Drawing.Size(415, 25);
            this.resultDirectoryPath_textBox.TabIndex = 0;
            // 
            // ResultDirectoryPathControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resultDirectoryPath_groupBox);
            this.Name = "ResultDirectoryPathControl";
            this.Size = new System.Drawing.Size(421, 45);
            this.resultDirectoryPath_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox resultDirectoryPath_groupBox;
        private Controls.TextBoxWithDialogButton resultDirectoryPath_textBox;
    }
}
