﻿using System.IO;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

using TestUtils;
using Store;

namespace TestSourceReducer
{
    [TestClass]
    public class TestSourceReducer
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;
        private static BasicMonitorListener listener;


        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется вручную созданный файл на языке программирования C#. 
        /// В файле присутствуют все заданные функции - избыточные.
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyRemTwo()
        {
            string sourcePrefixPart = @"SourceReducer\CSharp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => 
                { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "Program.cs");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "WriteReport";
                    func.AddDefinition(testFilePath, 93, 29);
                    func.AddDeclaration(testFilePath, 93, 29);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "Main";
                    func.AddDefinition(testFilePath, 20, 21);
                    func.AddDeclaration(testFilePath, 20, 21);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "Test"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestRem2"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется вручную созданный файл на языке программирования C#. 
        /// В файле присутствуют все заданные функции, одна избыточная.
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyRemOne()
        {
            string sourcePrefixPart = @"SourceReducer\CSharp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "Program.cs");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "WriteReport";
                    func.AddDefinition(testFilePath, 93, 29);
                    func.AddDeclaration(testFilePath, 93, 29);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "Main";
                    func.AddDefinition(testFilePath, 20, 21);
                    func.AddDeclaration(testFilePath, 20, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "Test1"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestRem1"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется вручную созданный файл на языке программирования C#. 
        /// В файле присутствуют все заданные функции, все неизбыточные.
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyRemNone()
        {
            string sourcePrefixPart = @"SourceReducer\CSharp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "Program.cs");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "WriteReport";
                    func.AddDefinition(testFilePath, 93, 29);
                    func.AddDeclaration(testFilePath, 93, 29);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "Main";
                    func.AddDefinition(testFilePath, 20, 21);
                    func.AddDeclaration(testFilePath, 20, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(listener.Count() == 0, "Количество предупреждений больше 0");
                    string path = Path.Combine(testMaterialsPath, "SourceReducer", "TestE_1");
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), path);
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestRem0"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }


        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки zlib "crc32.c"
        /// В файле присутствуют 9 функций. Первая - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyC1()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\cpp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "crc32.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "make_crc_table";
                    func.AddDefinition(testFilePath, 106, 12);
                    func.AddDeclaration(testFilePath, 106, 12);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "write_table";
                    func.AddDefinition(testFilePath, 183, 12);
                    func.AddDeclaration(testFilePath, 183, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "get_crc_table";
                    func.AddDefinition(testFilePath, 205, 35);
                    func.AddDeclaration(testFilePath, 205, 35);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32";
                    func.AddDefinition(testFilePath, 219, 23);
                    func.AddDeclaration(testFilePath, 219, 23);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_little";
                    func.AddDefinition(testFilePath, 262, 21);
                    func.AddDeclaration(testFilePath, 262, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_big";
                    func.AddDefinition(testFilePath, 302, 21);
                    func.AddDeclaration(testFilePath, 302, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_times";
                    func.AddDefinition(testFilePath, 342, 21);
                    func.AddDeclaration(testFilePath, 342, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_square";
                    func.AddDefinition(testFilePath, 359, 12);
                    func.AddDeclaration(testFilePath, 359, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_combine";
                    func.AddDefinition(testFilePath, 370, 15);
                    func.AddDeclaration(testFilePath, 370, 15);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "TestC_1"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestC_1"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки zlib "crc32.c"
        /// В файле присутствуют 9 функций. Вторая - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyC2()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\cpp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "crc32.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "make_crc_table";
                    func.AddDefinition(testFilePath, 106, 12);
                    func.AddDeclaration(testFilePath, 106, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "write_table";
                    func.AddDefinition(testFilePath, 183, 12);
                    func.AddDeclaration(testFilePath, 183, 12);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "get_crc_table";
                    func.AddDefinition(testFilePath, 205, 35);
                    func.AddDeclaration(testFilePath, 205, 35);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32";
                    func.AddDefinition(testFilePath, 219, 23);
                    func.AddDeclaration(testFilePath, 219, 23);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_little";
                    func.AddDefinition(testFilePath, 262, 21);
                    func.AddDeclaration(testFilePath, 262, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_big";
                    func.AddDefinition(testFilePath, 302, 21);
                    func.AddDeclaration(testFilePath, 302, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_times";
                    func.AddDefinition(testFilePath, 342, 21);
                    func.AddDeclaration(testFilePath, 342, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_square";
                    func.AddDefinition(testFilePath, 359, 12);
                    func.AddDeclaration(testFilePath, 359, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_combine";
                    func.AddDefinition(testFilePath, 370, 15);
                    func.AddDeclaration(testFilePath, 370, 15);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "TestC_2"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestC_2"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки zlib "crc32.c"
        /// В файле присутствуют 9 функций. Третья - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyC3()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\cpp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "crc32.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "make_crc_table";
                    func.AddDefinition(testFilePath, 106, 12);
                    func.AddDeclaration(testFilePath, 106, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "write_table";
                    func.AddDefinition(testFilePath, 183, 12);
                    func.AddDeclaration(testFilePath, 183, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "get_crc_table";
                    func.AddDefinition(testFilePath, 205, 35);
                    func.AddDeclaration(testFilePath, 205, 35);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32";
                    func.AddDefinition(testFilePath, 219, 23);
                    func.AddDeclaration(testFilePath, 219, 23);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_little";
                    func.AddDefinition(testFilePath, 262, 21);
                    func.AddDeclaration(testFilePath, 262, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_big";
                    func.AddDefinition(testFilePath, 302, 21);
                    func.AddDeclaration(testFilePath, 302, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_times";
                    func.AddDefinition(testFilePath, 342, 21);
                    func.AddDeclaration(testFilePath, 342, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_square";
                    func.AddDefinition(testFilePath, 359, 12);
                    func.AddDeclaration(testFilePath, 359, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_combine";
                    func.AddDefinition(testFilePath, 370, 15);
                    func.AddDeclaration(testFilePath, 370, 15);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "TestC_3"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestC_3"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки zlib "crc32.c"
        /// В файле присутствуют 9 функций. Четвертая - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyC4()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\cpp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "crc32.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "make_crc_table";
                    func.AddDefinition(testFilePath, 106, 12);
                    func.AddDeclaration(testFilePath, 106, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "write_table";
                    func.AddDefinition(testFilePath, 183, 12);
                    func.AddDeclaration(testFilePath, 183, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "get_crc_table";
                    func.AddDefinition(testFilePath, 205, 35);
                    func.AddDeclaration(testFilePath, 205, 35);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32";
                    func.AddDefinition(testFilePath, 219, 23);
                    func.AddDeclaration(testFilePath, 219, 23);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_little";
                    func.AddDefinition(testFilePath, 262, 21);
                    func.AddDeclaration(testFilePath, 262, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_big";
                    func.AddDefinition(testFilePath, 302, 21);
                    func.AddDeclaration(testFilePath, 302, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_times";
                    func.AddDefinition(testFilePath, 342, 21);
                    func.AddDeclaration(testFilePath, 342, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_square";
                    func.AddDefinition(testFilePath, 359, 12);
                    func.AddDeclaration(testFilePath, 359, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_combine";
                    func.AddDefinition(testFilePath, 370, 15);
                    func.AddDeclaration(testFilePath, 370, 15);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "TestC_4"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestC_4"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки zlib "crc32.c"
        /// В файле присутствуют 9 функций. Пятая - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyC5()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\cpp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "crc32.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "make_crc_table";
                    func.AddDefinition(testFilePath, 106, 12);
                    func.AddDeclaration(testFilePath, 106, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "write_table";
                    func.AddDefinition(testFilePath, 183, 12);
                    func.AddDeclaration(testFilePath, 183, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "get_crc_table";
                    func.AddDefinition(testFilePath, 205, 35);
                    func.AddDeclaration(testFilePath, 205, 35);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32";
                    func.AddDefinition(testFilePath, 219, 23);
                    func.AddDeclaration(testFilePath, 219, 23);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_little";
                    func.AddDefinition(testFilePath, 262, 21);
                    func.AddDeclaration(testFilePath, 262, 21);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_big";
                    func.AddDefinition(testFilePath, 302, 21);
                    func.AddDeclaration(testFilePath, 302, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_times";
                    func.AddDefinition(testFilePath, 342, 21);
                    func.AddDeclaration(testFilePath, 342, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_square";
                    func.AddDefinition(testFilePath, 359, 12);
                    func.AddDeclaration(testFilePath, 359, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_combine";
                    func.AddDefinition(testFilePath, 370, 15);
                    func.AddDeclaration(testFilePath, 370, 15);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "TestC_5"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestC_5"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки zlib "crc32.c"
        /// В файле присутствуют 9 функций. Шестая - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyC6()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\cpp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "crc32.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "make_crc_table";
                    func.AddDefinition(testFilePath, 106, 12);
                    func.AddDeclaration(testFilePath, 106, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "write_table";
                    func.AddDefinition(testFilePath, 183, 12);
                    func.AddDeclaration(testFilePath, 183, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "get_crc_table";
                    func.AddDefinition(testFilePath, 205, 35);
                    func.AddDeclaration(testFilePath, 205, 35);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32";
                    func.AddDefinition(testFilePath, 219, 23);
                    func.AddDeclaration(testFilePath, 219, 23);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_little";
                    func.AddDefinition(testFilePath, 262, 21);
                    func.AddDeclaration(testFilePath, 262, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_big";
                    func.AddDefinition(testFilePath, 302, 21);
                    func.AddDeclaration(testFilePath, 302, 21);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_times";
                    func.AddDefinition(testFilePath, 342, 21);
                    func.AddDeclaration(testFilePath, 342, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_square";
                    func.AddDefinition(testFilePath, 359, 12);
                    func.AddDeclaration(testFilePath, 359, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_combine";
                    func.AddDefinition(testFilePath, 370, 15);
                    func.AddDeclaration(testFilePath, 370, 15);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "TestC_6"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestC_6"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки zlib "crc32.c"
        /// В файле присутствуют 9 функций. Седьмая - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyC7()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\cpp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "crc32.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "make_crc_table";
                    func.AddDefinition(testFilePath, 106, 12);
                    func.AddDeclaration(testFilePath, 106, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "write_table";
                    func.AddDefinition(testFilePath, 183, 12);
                    func.AddDeclaration(testFilePath, 183, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "get_crc_table";
                    func.AddDefinition(testFilePath, 205, 35);
                    func.AddDeclaration(testFilePath, 205, 35);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32";
                    func.AddDefinition(testFilePath, 219, 23);
                    func.AddDeclaration(testFilePath, 219, 23);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_little";
                    func.AddDefinition(testFilePath, 262, 21);
                    func.AddDeclaration(testFilePath, 262, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_big";
                    func.AddDefinition(testFilePath, 302, 21);
                    func.AddDeclaration(testFilePath, 302, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_times";
                    func.AddDefinition(testFilePath, 342, 21);
                    func.AddDeclaration(testFilePath, 342, 21);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_square";
                    func.AddDefinition(testFilePath, 359, 12);
                    func.AddDeclaration(testFilePath, 359, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_combine";
                    func.AddDefinition(testFilePath, 370, 15);
                    func.AddDeclaration(testFilePath, 370, 15);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "TestC_7"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestC_7"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки zlib "crc32.c"
        /// В файле присутствуют 9 функций. Восьмая - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyC8()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\cpp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "crc32.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "make_crc_table";
                    func.AddDefinition(testFilePath, 106, 12);
                    func.AddDeclaration(testFilePath, 106, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "write_table";
                    func.AddDefinition(testFilePath, 183, 12);
                    func.AddDeclaration(testFilePath, 183, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "get_crc_table";
                    func.AddDefinition(testFilePath, 205, 35);
                    func.AddDeclaration(testFilePath, 205, 35);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32";
                    func.AddDefinition(testFilePath, 219, 23);
                    func.AddDeclaration(testFilePath, 219, 23);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_little";
                    func.AddDefinition(testFilePath, 262, 21);
                    func.AddDeclaration(testFilePath, 262, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_big";
                    func.AddDefinition(testFilePath, 302, 21);
                    func.AddDeclaration(testFilePath, 302, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_times";
                    func.AddDefinition(testFilePath, 342, 21);
                    func.AddDeclaration(testFilePath, 342, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_square";
                    func.AddDefinition(testFilePath, 359, 12);
                    func.AddDeclaration(testFilePath, 359, 12);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_combine";
                    func.AddDefinition(testFilePath, 370, 15);
                    func.AddDeclaration(testFilePath, 370, 15);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "TestC_8"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestC_8"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки zlib "crc32.c"
        /// В файле присутствуют 9 функций. Девятая - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyC9()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\cpp";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "crc32.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "make_crc_table";
                    func.AddDefinition(testFilePath, 106, 12);
                    func.AddDeclaration(testFilePath, 106, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "write_table";
                    func.AddDefinition(testFilePath, 183, 12);
                    func.AddDeclaration(testFilePath, 183, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "get_crc_table";
                    func.AddDefinition(testFilePath, 205, 35);
                    func.AddDeclaration(testFilePath, 205, 35);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32";
                    func.AddDefinition(testFilePath, 219, 23);
                    func.AddDeclaration(testFilePath, 219, 23);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_little";
                    func.AddDefinition(testFilePath, 262, 21);
                    func.AddDeclaration(testFilePath, 262, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_big";
                    func.AddDefinition(testFilePath, 302, 21);
                    func.AddDeclaration(testFilePath, 302, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_times";
                    func.AddDefinition(testFilePath, 342, 21);
                    func.AddDeclaration(testFilePath, 342, 21);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "gf2_matrix_square";
                    func.AddDefinition(testFilePath, 359, 12);
                    func.AddDeclaration(testFilePath, 359, 12);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "crc32_combine";
                    func.AddDefinition(testFilePath, 370, 15);
                    func.AddDeclaration(testFilePath, 370, 15);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), Path.Combine(testMaterialsPath, "SourceReducer", "TestC_9"));
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestC_9"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется модифицированный файл из библитеки wget "rbuf.c" с добавленными ошибками : нет закрывающей скобки '}' в последней функции
        /// В файле присутствуют 4 функции. Последняя - избыточная
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyE1()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\c";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "rbuf.c");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "rbuf_initialize";
                    func.AddDefinition(testFilePath, 56, 1);
                    func.AddDeclaration(testFilePath, 56, 1);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "rbuf_initialized_p";
                    func.AddDefinition(testFilePath, 68, 1);
                    func.AddDeclaration(testFilePath, 68, 1);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "rbuf_uninitialize";
                    func.AddDefinition(testFilePath, 74, 1);
                    func.AddDeclaration(testFilePath, 74, 1);
                    func.Essential = enFunctionEssentials.ESSENTIAL;

                    func = storage.functions.AddFunction();
                    func.Name = "rbuf_read_bufferful";
                    func.AddDefinition(testFilePath, 80, 1);
                    func.AddDeclaration(testFilePath, 80, 1);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(listener.ContainsPart("Ошибка при определении конца функции"));
                    string path = Path.Combine(testMaterialsPath, "SourceReducer", "TestE_1");
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), path);
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    //Создать папку, если нет. Она должна быть пустой. Git пустые папки "забывает"
                    string path = Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestE_1");
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);

                    return TestUtilsClass.Reports_Directory_TXT_Compare(path, reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина. Настройки задаются вручную.
        /// Функции в Хранилище, а также их избыточность задаются вручную.
        /// Используется пустой файл
        /// В хранилище заводится 1 функция, в файле ее нет.
        /// </summary>
        [TestMethod]
        public void SourceReducer_ManualRedunduncyE2()
        {
            string sourcePrefixPart = @"SourceReducer\Sources\empty";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SourceReducer.SourceReducer(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "empty.txt");
                    string testOutPath = Path.Combine(storage.WorkDirectory.Path, "Test");
                    Directory.CreateDirectory(testOutPath);

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "not_present";
                    func.AddDefinition(testFilePath, 10, 1);
                    func.AddDeclaration(testFilePath, 10, 1);
                    func.Essential = enFunctionEssentials.REDUNDANT;

                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(testOutPath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SOURCE_REDUCER, writer);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(listener.Count() == 0);
                    string path = Path.Combine(testMaterialsPath, "SourceReducer", "TestE_1");
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, "Test"), path);
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    //Создать папку, если нет. Она должна быть пустой. Git пустые папки "забывает"
                    string path = Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestE_2");
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);

                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, "SourceReducer", "Reports", "TestE_2"), reportsPath); 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }


        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            List<string> messages;
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            public void Clear()
            {
                messages.Clear();
            }

            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;
                return messages.Any(s => s.Contains(partialString));
            }
            public int Count()
            {
                return messages.Count;
            }
        }

    }
}
