﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IA.Plugins.Analyses.SensorTypeFixer;

using IOController;
using Store;
using TestUtils;


namespace TestSensorTypeFixer
{
    [TestClass]
    public class TestSensorTypeFixer
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Возвращает количество перехваченных сообщений
            /// </summary>
            /// <returns>Количество сообщений</returns>
            public int Count()
            {
                return messages.Count;
            }
        }

        /// <summary>
        /// Перехватчик сообщений из монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Общий тест на отработку плагина. Заменяет датчики С и С++.
        /// </summary>
        [TestMethod]
        public void SensorTypeFixer_Sample()
        {
            var testPlugin = new SensorTypeFixer();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => {
                    
                },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    var function = storage.functions.AddFunction();
                    function.Name = "Test";
                    var startStatement = storage.statements.AddStatementOperational();
                    var nextStatement = storage.statements.AddStatementOperational();
                    var lastStatement = storage.statements.AddStatementOperational();

                    startStatement.NextInLinearBlock = nextStatement;
                    nextStatement.NextInLinearBlock = lastStatement;
                    function.EntryStatement = startStatement;
                    storage.sensors.AddSensorBeforeStatement(1, startStatement, Kind.INTERNAL);
                    storage.sensors.AddSensorBeforeStatement(2, nextStatement, Kind.INTERNAL);
                    storage.sensors.AddSensorBeforeStatement(3, lastStatement, Kind.INTERNAL);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    var function = storage.functions.EnumerateFunctions(enFunctionKind.ALL).First();
                    Assert.IsTrue(function.EntryStatement.SensorBeforeTheStatement.Kind == Kind.START);
                    Assert.IsTrue(function.EntryStatement.NextInLinearBlock.SensorBeforeTheStatement.Kind == Kind.INTERNAL);
                    Assert.IsTrue(function.EntryStatement.NextInLinearBlock.NextInLinearBlock.SensorBeforeTheStatement.Kind == Kind.FINAL);
                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    return true;
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        

        /// <summary>
        /// Создание копии эталонного отчета в Хранилище по пути "Директория Хранилища\oldReports\Имя директории для копирования\_1_Din_.Rep"
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="sampleReportFilePath">Путь до эталонного файла отчета. Не может быть null.</param>
        /// <param name="copyDirectoryName">Имя директории для копирования. Не может быть null.</param>
        /// <returns>Путь до копии эталонного файла отчета.</returns>
        private string GetSampleReportCopyInStorage(Storage storage, string sampleReportFilePath, string copyDirectoryName)
        {
            string reportsXRFDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.STORAGE), "oldReports", copyDirectoryName);
            string sampleReportCopyFilePath = Path.Combine(reportsXRFDirectoryPath, @"_1_Din_.Rep");

            //Используем System.IO поскольку DirectoryController не способен создавать дерево каталогов
            Directory.CreateDirectory(reportsXRFDirectoryPath);

            FileController.Copy(sampleReportFilePath, sampleReportCopyFilePath);

            return sampleReportCopyFilePath;
        }

        /// <summary>
        /// Создание копии эталонного лабораторных исходных текстов в Хранилище по пути "Директория Хранилища\oldLabs\Имя директории для копирования\"
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="sampleLabSourcesDirectoryPath">Путь до директории с эталонными лабораторными исходными текстами. Не может быть null.</param>
        /// <param name="directoryTo">Имя директории для копирования. Не может быть null.</param>
        /// <returns>Путь до директории с копией эталонных лабораторных исходных текстов.</returns>
        private string GetSampleLabSourcesCopyInStorage(Storage storage, string sampleLabSourcesDirectoryPath, string directoryTo)
        {
            string sampleLabSourcesCopyDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.STORAGE), "oldLabs", directoryTo);

            //Используем System.IO поскольку DirectoryController не способен создавать дерево каталогов
            Directory.CreateDirectory(sampleLabSourcesCopyDirectoryPath);

            DirectoryController.Copy(sampleLabSourcesDirectoryPath, sampleLabSourcesCopyDirectoryPath);

            return sampleLabSourcesCopyDirectoryPath;
        }

        /// <summary>
        /// Получение копии эталонных отчётов с измененёнными путями для тестирования на конкретной машине.
        /// </summary>
        /// <param name="standartReportsDirectoryPath">Путь к директории с эталонными отчетами. Не может быть Null.</param>
        /// <param name="specifiedStandartReportsDirectoryPath">Путь к директории с копией эталонных отчётов с подменёнными путями. Не может быть Null.</param>
        /// <param name="replaceString">Путь в отчетах, который будет изменен. Не может быть Null.</param>
        private void GetSpecifiedStandartReportsCopy(string standartReportsDirectoryPath, string specifiedStandartReportsDirectoryPath, string replaceString)
        {
            foreach (string file in Directory.EnumerateFiles(specifiedStandartReportsDirectoryPath, "*.*", SearchOption.AllDirectories))
            {
                string contents = new StreamReader(file, Encoding.Default).ReadToEnd().Replace(replaceString, testUtils.TestRunDirectoryPath);

                string newFileDirectoryPath = Path.Combine(standartReportsDirectoryPath, Path.GetDirectoryName(file).Substring(specifiedStandartReportsDirectoryPath.Length));

                if (!DirectoryController.IsExists(newFileDirectoryPath))
                    DirectoryController.Create(newFileDirectoryPath);

                string newFileFullPath = Path.Combine(newFileDirectoryPath, Path.GetFileName(file));
                using (StreamWriter writer = new StreamWriter(newFileFullPath, false, Encoding.Default))
                    writer.Write(contents);
            }
        }
    }
}
