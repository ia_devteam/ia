﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

using IA.Extensions;
using Store;

namespace IA.Plugins.Analyses.SensorTypeFixer
{
    /// <summary>
    /// Класс настроек плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Изменение типа датчиков";

        /// <summary>
        /// Иденитификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.SENSOR_TYPE_FIXER;

        #region Settings

        
        

        #endregion

        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Изменение типа датчика")]
            FIX_SENS_TYPE
        };
       


    }

    /// <summary>
    /// Основной класс плагина
    /// </summary>
    public class SensorTypeFixer : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        private Storage storage;

       
        #region plugin interface

        /// <summary>
        ///  <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get { return PluginSettings.ID; }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return 
                    //Plugin.Capabilities.RESULT_WINDOW |
                                   // Plugin.Capabilities.REPORTS |
                                        Plugin.Capabilities.RERUN;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.UserControl CustomSettingsWindow
        {
            get { return null; }
        }
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.UserControl RunWindow
        {
            get { return null; }
        }
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.UserControl ResultWindow
        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(Name, PluginSettings.Tasks.FIX_SENS_TYPE.Description())
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"></param>
        public void Initialize(Store.Storage storage)
        {
            this.storage = storage;
        }

        /// <summary>
        ///  <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"></param>
        public void LoadSettings(Store.Table.IBufferReader settingsBuffer)
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="settingsString"></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="settingsBuffer"></param>
        public void SaveSettings(Store.Table.IBufferWriter settingsBuffer)
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        public void SaveResults()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;
            return true;
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportsDirectoryPath"></param>
        public void GenerateReports(string reportsDirectoryPath)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public void StopRunning()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        public void Run()
        {
            foreach (IFunction f in storage.functions.EnumerateFunctions(enFunctionKind.ALL).Where(g => g.EntryStatement != null))
                ProcessSensors(f.EntryStatement);
        }

        /// <summary>
        /// Модификация типов датчиков
        /// </summary>
        /// <param name="st">ветвление функции</param>
        /// <param name="metFirst">метка начала</param>
        /// <param name="visited">метка обработки</param>
        /// 
        private void ProcessSensors(IStatement st, HashSet<IStatement> visited = null)
        {
            if (visited == null)
                visited = new HashSet<IStatement>();
            if (visited.Contains(st)) return;
            visited.Add(st);
            if(st.AboveStatement == null && st.PreviousInLinearBlock == null)
            {
                if (st.SensorBeforeTheStatement != null)
                {
                    st.SensorBeforeTheStatement.Kind = Kind.START;
                }
            }
            else
            {
                if (st.NextInControl(true).Count(g => !(g is IStatementExit)) == 0)
                {
                    if (st.SensorBeforeTheStatement != null)
                    {
                        st.SensorBeforeTheStatement.Kind = Kind.FINAL;
                    }
                }
                else
                {
                    if (st.SensorBeforeTheStatement != null)
                    {
                        st.SensorBeforeTheStatement.Kind = Kind.INTERNAL;
                    }
                }
            }
            //fix for yieldreturn
            if(st is IStatementYieldReturn)
            {
                if (st.SensorBeforeTheStatement != null)
                {
                    st.SensorBeforeTheStatement.Kind = Kind.FINAL;
                }
            } 
            else
            {
                if(st.PreviousInLinearBlock != null && st.PreviousInLinearBlock is IStatementYieldReturn)
                {
                    if (st.SensorBeforeTheStatement != null)
                    {
                        st.SensorBeforeTheStatement.Kind = Kind.START;
                    }
                }
            }

            foreach(var statement in st.NextInControl(true).Where(g => !(g is IStatementExit)))
            {
                ProcessSensors(statement, visited);
            }
        }
        
        //  private void ProcessSensors(IStatement st, bool metFirst = false, HashSet<IStatement> visited = null)
        //{
        //    //Дважды по одному оператору не ходим. Позволяет не зациклиться
        //    if (visited == null)
        //        visited = new HashSet<IStatement>();
        //    if (visited.Contains(st)) return;
        //    visited.Add(st);

        //    if (!metFirst && st.SensorBeforeTheStatement != null)
        //    {   //Если оператор первый в рассмотрении и имеет датчик
        //        metFirst = true;
        //        st.SensorBeforeTheStatement.Kind = Kind.START;
        //    }
        //    else
        //    {   
        //        if (st.SensorBeforeTheStatement != null)
        //        {   //Если оператор имеет датчик
        //            if (st.NextInControl(false).Count(g => !(g is IStatementExit)) != 0)
        //            {   //Если у оператора есть последующие операторы
        //                if (!(st is IStatementYieldReturn))
        //                    // Если оператор не является YieldReturn - ом
        //                    st.SensorBeforeTheStatement.Kind = Kind.INTERNAL;
        //                else
        //                {   
        //                    st.SensorBeforeTheStatement.Kind = Kind.FINAL;
        //                    metFirst = false;
        //                }
        //            }
        //            else
        //            {   //Если у оператора нет последующиз операторов
        //                st.SensorBeforeTheStatement.Kind = Kind.FINAL;
        //            }
        //        }
        //    }


        //    foreach (IStatement s in st.NextInControl(true))
        //    {
        //        if (s.NextInControl(true).Count(g => !(g is IStatementExit)) == 0)
        //        {
        //            if (s.SensorBeforeTheStatement != null)
        //                s.SensorBeforeTheStatement.Kind = Kind.FINAL;
        //        }
        //        else
        //        {
        //            ProcessSensors(s, metFirst, visited);
        //        }
        //    }
        //}
        #endregion
    }
}
