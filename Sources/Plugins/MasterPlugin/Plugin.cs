﻿using System.Collections.Generic;

namespace MasterPlugin
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Пакетный запуск";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.MASTER_PLUGIN;
    }

    public class MasterPlugin : IA.Plugin.Interface
    {
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        public string Name
        {
            get 
            {
                return PluginSettings.Name; 
            }
        }

        public IA.Plugin.Capabilities Capabilities
        {
            get 
            {
                return IA.Plugin.Capabilities.NONE;
            }
        }

        public System.Windows.Forms.UserControl SimpleSettingsWindow
        {
            get { return null; }
        }

        public System.Windows.Forms.UserControl CustomSettingsWindow
        {
            get { return null; }
        }

        public System.Windows.Forms.UserControl RunWindow
        {
            get { return null; }
        }

        public System.Windows.Forms.UserControl ResultWindow
        {
            get { return null; }
        }

        public List<IA.Monitor.Tasks.Task> Tasks
        {
            get { return null; }
        }

        public List<IA.Monitor.Tasks.Task> TasksReport
        {
            get { return null; }
        }

        public void Initialize(Store.Storage storage)
        {
            
        }

        public void LoadSettings(Store.Table.IBufferReader settingsBuffer)
        {
            
        }

        public void SetSimpleSettings(string settingsString)
        {
            
        }

        public void SaveSettings(Store.Table.IBufferWriter settingsBuffer)
        {
            
        }

        public void SaveResults()
        {
            
        }

        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;
            return true;
        }

        public void Run()
        {
            
        }

        public void GenerateReports(string reportsDirectoryPath)
        {
            
        }

        public void StopRunning()
        {
            
        }
    }
}
