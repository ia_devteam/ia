﻿using System.IO;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;
using Store;

namespace TestCS_Sensors
{
    /// <summary>
    /// Класс для тестов плагина CS_Sensors
    /// </summary>
    [TestClass]
    public class TestCS_Sensors
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }
        }

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Перехватчик сообщений монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Путь до каталога с эталонными исходными текстами
        /// </summary>
        private string labSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонным дампами Хранилища
        /// </summary>
        private string dumpSampleDirectoryPath;

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными лабораторным текстам
        /// </summary>
        private const string LAB_SAMPLES_SUBDIRECTORY = @"CS_Sensors\Reports";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными дампами Хранилища
        /// </summary>
        private const string DUMP_SAMPLES_SUBDIRECTORY = @"CS_Sensors\Dump";

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();

            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            labSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, LAB_SAMPLES_SUBDIRECTORY);
            dumpSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, DUMP_SAMPLES_SUBDIRECTORY);
        }

        /// <summary>
        /// Проверка корректности вставки датчиков в исходные тексты на языке C.
        /// </summary>
        [TestMethod]
        public void CS_Sensors_C()
        {
            const string sourcePostfixPart = @"CS_Sensors\C";
            const string understandReportsPrefixPart = @"Understand\CS_Sensors\C";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\CS_Sensors\C";
            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Analyses.CS_Sensors.CS_Sensors(),                    // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, "C"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "C"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков в исходные тексты на языке C++.
        /// </summary>
        [TestMethod]
        public void CS_Sensors_CPP()
        {
            const string sourcePostfixPart = @"CS_Sensors\C++";
            const string understandReportsPrefixPart = @"Understand\CS_Sensors\C++";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\CS_Sensors\C++";
            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Analyses.CS_Sensors.CS_Sensors(),                    // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, "C++"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "C++"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков в исходные тексты на языке Java.
        /// </summary>
        [TestMethod]
        public void CS_Sensors_Java()
        {
            const string sourcePostfixPart = @"CS_Sensors\Java";
            const string understandReportsPrefixPart = @"Understand\CS_Sensors\Java";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\CS_Sensors\Java";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Analyses.CS_Sensors.CS_Sensors(),                    // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, "Java"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков в исходные тексты на языке Pascal.
        /// </summary>
        [TestMethod]
        public void CS_Sensors_Pascal()
        {
            const string sourcePostfixPart = @"Sources\Pascal";
            const string understandReportsPrefixPart = @"Understand\Pascal";
            const string origSourcePath = @"B:\IA_b\Tests\Materials\Sources\Pascal\";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Analyses.CS_Sensors.CS_Sensors(),                    // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, "Pascal"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки плагина на пустой папке.
        /// </summary>
        [TestMethod]
        public void CS_Sensors_EmptyFolder()
        {
            const string sourcePostfixPart = @"CS_sensors\EmptyFolder";
            const string understandReportsPrefixPart = @"Understand\CS_Sensors\EmptyFolder";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\CS_Sensors\EmptyFolder";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Analyses.CS_Sensors.CS_Sensors(),                    // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, @"CS_sensors\EmptyFolder\")))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, @"CS_sensors\EmptyFolder\"));
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckMessage("Нет файлов для обработки.");
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "EmptyFolder"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков, если файл записан одной строкой.
        /// </summary>
        [TestMethod]
        public void CS_Sensors_OneLineCode()
        {
            const string sourcePostfixPart = @"CS_sensors\OneLine";
            const string understandReportsPrefixPart = @"Understand\CS_Sensors\OneLine";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\CS_Sensors\OneLine";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Analyses.CS_Sensors.CS_Sensors(),                    // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, "OneLine"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "OneLine"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков, максимально близко к концу файла.
        /// </summary>
        [TestMethod]
        public void CS_Sensors_EndLine()
        {
            const string sourcePostfixPart = @"CS_sensors\EndLine";
            const string understandReportsPrefixPart = @"Understand\CS_Sensors\EndLine";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\CS_Sensors\EndLine";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Analyses.CS_Sensors.CS_Sensors(),                    // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, "EndLine"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "EndLine"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков если есть макросы.
        /// </summary>
        [TestMethod, Ignore]
        public void CS_Sensors_Bug60_ArraySensor()
        {
            RunTestBug();
        }

        /// <summary>
        /// Проверка корректности вставки датчиков в исходниках на Java в части enum и конструкторов с вызовами super и this.
        /// </summary>
        [TestMethod]
        public void CS_Sensors_Bug197_JavaSensors()
        {
            //писано в пьяном угаре. Переписать на человеческий язык
            RunTestBug();
        }


        private void RunTestBug()
        {
            const string bugTestNamePrefix = "CS_Sensors_Bug";
            string testDirName = Path.Combine(@"CS_Sensors\bugs\", testUtils.TestName.Substring(bugTestNamePrefix.Length));

            string sourcePostfixPart = Path.Combine(testDirName, "Source");
            string understandReportsPrefixPart = Path.Combine(testDirName, "UnderstandReport");
            string origSourcePath = TestUtilsClass.PathConstant;

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Analyses.CS_Sensors.CS_Sensors(),                    // Plugin
                false,                                                              // isUseCachedStorage
                null,                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckLabs(storage, Path.Combine(testMaterialsPath, testDirName, "Lab"));
                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                null,                                 // changeSettingsBeforeRerun
                null,                   // checkStorageAfterRerun
                null                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Дамп Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <returns>Путь до каталога, куда был произведен дамп Хранилища.</returns>
        private string Dump(Storage storage)
        {
            string dumpDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.STORAGE), "Dump");

            if (!Directory.Exists(dumpDirectoryPath))
                Directory.CreateDirectory(dumpDirectoryPath);

            storage.ClassesDump(dumpDirectoryPath);
            storage.FilesDump(dumpDirectoryPath);
            storage.FunctionsDump(dumpDirectoryPath);
            storage.VariablesDump(dumpDirectoryPath);

            return dumpDirectoryPath;
        }

        /// <summary>
        /// Проверка лабораторных исходных файлов
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="labSamplesDirectoryPath">Путь до каталога с эталонными лабораторными исходными текстами. Не может быть пустым.</param>
        private void CheckLabs(Storage storage, string labSamplesDirectoryPath)
        {
            string labsDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "cs_sensors");

            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(labsDirectoryPath, labSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Исходные тексты со вставленными датчиками не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка дампа Хранилища
        /// </summary>
        /// <param name="dumpDirectoryPath">Путь до каталога с дампом Хранилища. Не может быть пустым.</param>
        /// <param name="dumpSamplesDirectoryPath">Путь до каталога с эталонным дампом. Не может быть пустым.</param>
        private void CheckDump(string dumpDirectoryPath, string dumpSamplesDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(dumpDirectoryPath, dumpSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Дамп Хранилища не совпадает с эталонным.");
        }

        /// <summary>
        /// Проверка ожидаемого сообщения в логе монитора
        /// </summary>
        /// <param name="message">Текст сообщения. Не может быть пустым.</param>
        private void CheckMessage(string message)
        {
            bool isContainMessage = listener.ContainsPart(message);

            Assert.IsTrue(isContainMessage, "Сообщение не совпадает с ожидаемым.");
        }

        /// <summary>
        /// Подготовка Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="testMaterialsPath">Путь до каталога с материалами.</param>
        /// <param name="sourcePostfixPart">Подпуть до каталога с исходными текстами.</param>
        private void CustomizeStorage(Storage storage, string testMaterialsPath, string sourcePostfixPart, string understandReportsPrefixPart = "", string origSourcePath = "")
        {
            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
            TestUtilsClass.Run_IdentifyFileTypes(storage);
            TestUtilsClass.Run_UnderstandImporter(storage, understandReportsPrefixPart, origSourcePath, IsLoadVariables: true);

            Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();

            buffer.Add(1);
            buffer.Add("int rnt_sensorcall = ____Din_Go(\" \",%DAT%);");
            buffer.Add("____Din_Go(\" \",%DAT%);");
            buffer.Add("#include \"c:/sensor.h\"");
            buffer.Add("____Din_Go(' ',%DAT%);");
            buffer.Add("System.AssignFile(rnt_sensor,'c:\\Delphifile.txt'); if not System.FileExists('c:\\Delphifile.txt') then begin System.Rewrite(rnt_sensor); System.CloseFile(rnt_sensor); end; System.Append(rnt_sensor); System.Writeln(rnt_sensor,'%DAT%'); System.Flush(rnt_sensor); System.CloseFile(rnt_sensor);");
            buffer.Add(@"function ____Din_Go(ff: PChar; a: cardinal) : integer; stdcall; external 'c:\\Windows\\system32\\sensor.dll'");
            buffer.Add(false);

            storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.CS_SENSORS, buffer);
        }
    }
}
