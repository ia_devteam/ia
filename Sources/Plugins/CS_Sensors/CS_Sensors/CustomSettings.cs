using System;
using System.Windows.Forms;

namespace IA.Plugins.Analyses.CS_Sensors
{
    /// <summary>
    /// ����� ����������� ������� ���������� ������� ���� �������� �������
    /// </summary>
    public partial class CustomSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// �����������
        /// </summary>
        public CustomSettings()
        {
            InitializeComponent();

            firstSensorNumber_numericUpDown.Minimum = 1;
            firstSensorNumber_numericUpDown.Maximum = UInt64.MaxValue;
            firstSensorNumber_numericUpDown.Value = PluginSettings.FirstSensorNumber;

            SensorsIdentificator_numericUpDown.Minimum = 1;
            SensorsIdentificator_numericUpDown.Maximum = UInt64.MaxValue;
            SensorsIdentificator_numericUpDown.Value = PluginSettings.SensorsIdentificator;

            sensorText_c_textBox.Text = PluginSettings.SensorText_C;
            sensorText_cpp_textBox.Text = PluginSettings.SensorText_Cpp;
            sensorInclude_cpp_textBox.Text = PluginSettings.SensorInclude_Cpp;
            sensorInclude_java_textBox.Text = PluginSettings.SensorInclude_Java;
            
            sensorText_delphi_textBox.Text = PluginSettings.SensorText_Delphi;
            sensorInclude_delphi_textBox.Text = PluginSettings.SensorInclude_Delphi;
            sensorText_delphi_text_textBox.Text = PluginSettings.SensorText_Delphi_Text;            
            isDelphiSensors_text_checkBox.Checked = PluginSettings.IsDelphiSensors_Text;

            sensorText_delphi_text_textBox.Enabled = isDelphiSensors_text_checkBox.Checked;
            sensorText_delphi_textBox.Enabled = sensorInclude_delphi_textBox.Enabled = !isDelphiSensors_text_checkBox.Checked;
        }

        /// <summary>
        /// ���������� �������� ��������
        /// </summary>
        public void Save()
        {
            PluginSettings.FirstSensorNumber = (int)firstSensorNumber_numericUpDown.Value;
            PluginSettings.SensorsIdentificator = (int)SensorsIdentificator_numericUpDown.Value;
            PluginSettings.SensorText_C = sensorText_c_textBox.Text;
            PluginSettings.SensorText_Cpp = sensorText_cpp_textBox.Text;
            PluginSettings.SensorInclude_Cpp = sensorInclude_cpp_textBox.Text;
            PluginSettings.SensorInclude_Java = sensorInclude_java_textBox.Text;
            PluginSettings.SensorText_Delphi = sensorText_delphi_textBox.Text;
            PluginSettings.SensorText_Delphi_Text = sensorText_delphi_text_textBox.Text;
            PluginSettings.SensorInclude_Delphi = sensorInclude_delphi_textBox.Text;
            PluginSettings.IsDelphiSensors_Text = isDelphiSensors_text_checkBox.Checked;
        }

        /// <summary>
        /// ���������� - ������� ������ ��������� �������� � Pascal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isDelphiSensors_text_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            sensorText_delphi_text_textBox.Enabled = isDelphiSensors_text_checkBox.Checked;
            sensorText_delphi_textBox.Enabled = sensorInclude_delphi_textBox.Enabled = !isDelphiSensors_text_checkBox.Checked;
        }
    }
}
