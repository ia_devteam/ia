using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Store;
using Store.Table;
using FileOperations;

using IA.Extensions;

namespace IA.Plugins.Analyses.CS_Sensors
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Вставка датчиков на уровне функций (С, C++, Java, Delphi)";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.CS_SENSORS;

        #region Настройки

        /// <summary>
        /// Номер первого датчика
        /// </summary>
        internal static Int32 FirstSensorNumber = 1;

        /// <summary>
        /// Идентификатор вставки датчиков
        /// </summary>
        internal static Int32 SensorsIdentificator = 1;

        /// <summary>
        /// Датчик C
        /// </summary>
        internal static string SensorText_C;

        /// <summary>
        /// Датчик C++
        /// </summary>
        internal static string SensorText_Cpp;

        /// <summary>
        /// Строка включения заголовочного файла датчика в C/C++
        /// </summary>
        internal static string SensorInclude_Cpp;

        /// <summary>
        /// Строка включения импорта статического метода в Java
        /// </summary>
        internal static string SensorInclude_Java;

        /// <summary>
        /// Датчки Delphi
        /// </summary>
        internal static string SensorText_Delphi;

        /// <summary>
        /// Текстовый датчик Delphi
        /// </summary>
        internal static string SensorText_Delphi_Text;

        /// <summary>
        /// Строка включения библиотеки датчика Delphi
        /// </summary>
        internal static string SensorInclude_Delphi;

        /// <summary>
        /// Вставлять текстовые датчики в Delphi?
        /// </summary>
        internal static bool IsDelphiSensors_Text;

        internal static string SensorNumReplacement = "%DAT%";

        internal static string SensorsIdReplacement = "%SID%";
        #endregion

        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка функций")]
            FUNCTION_PROCESSING,

            [Description("Копирование файлов с датчиками")]
            FILES_WITH_SENSORS_COPYING
        };
    }

    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// TODO:
    /// FIXME: 
    /// 1) переписать плагин, разбив на модули по поддерживаемым языкам. 
    /// 2) устранить костыли заменив на бетонные подпорки.
    /// </remarks>
    public class CS_Sensors : IA.Plugin.Interface
    {
        /// <summary>
        /// переменная для хранения хранилища, которое передается извне при вызове этого плагина и не закрывается при его закрытии
        /// </summary>
        private Storage storage;

        /// <summary>
        /// номер текущего датчика
        /// </summary>
        internal Int32 currentSensor = 1;

        /// <summary>
        /// Текущие настройки
        /// </summary>
        IA.Plugin.Settings settings;

        /// <summary>
        /// хранит смешения в файлах в формате firstSymbolOffset*sensor_text, где firstSymbolOffset - int32, sensor_text - String - датчик или слово HEADER
        /// </summary>
        //List<List<string>> sensorList = new List<List<string>>();

        Dictionary<ulong, List<SensorItem>> sensorList2;// = new Dictionary<ulong,List<SensorItem>>();

        string[] specwords = { "procedure", "function", "constructor", "destructor" };
        string[] structwords = { "class", "struct", "enum", "interface" };
        string[] dlp_specwords = { "interface\n", "unit " };
        string[] dlp_specwords2 = { "\nimplementation\n", "\nprocedure ", "\nconstructor ", "\nfunction " };
        string[] delimiters = { "\n", " ", "\t", "\r" };


        //Получить расположение функции
        private Location GetLocation(IFunction function)
        {
            Location location = function.Definition();

            if (location == null)
                throw new Exception("Не найдено ни одного объявления функции: " + function.FullName);

            return location;
        }

        private void Main(string[] args)
        {
            string fileName = String.Empty;
            string line = String.Empty;

            AbstractReader reader = null;

            string contents = String.Empty;
            int contentsLength = 0;

            int currentLine = 0;
            int counter = 0;

            //инициализация счетчика датчиков
            currentSensor = (PluginSettings.FirstSensorNumber == 0) ? 1 : PluginSettings.FirstSensorNumber;

            //попытка ускорить дохлую лошадь
            IEnumerator<LocationFunctionPair> enu = storage.functions.EnumerateFunctionLocationsInSortOrder().GetEnumerator();
            if (enu != null)
                enu.MoveNext();

            //enu.MoveNext(); // еще 1 сдвиг, чтобы всегда указывало на 1 функцию вперед - сначала указываем на 1ю
            sensorList2 = new Dictionary<ulong, List<SensorItem>>();

            //Проходимся по всем функциям
            foreach (LocationFunctionPair pair in storage.functions.EnumerateFunctionLocationsInSortOrder())
            {
                Location location = pair.location;//GetLocation(pair.function);             // местоположение функции
                IFile file = location.GetFile();                            // файл, в котором расположена функция
                int offset = (int)location.GetOffset() - 1;                      // смещение функции
                int nextoffset = -1;                          // смещение следующей функции

                //Отображение прогресса
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNCTION_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);

                //FIXME: костыль чтобы хоть чуть-чуть работало до исправления общения с БД
                var fileLanguages = file.fileType.ContainsLanguages();

                //Проверка соответствует расширение файла нужному
                if (!fileLanguages.Any(l => l.Equals("C") || l.Equals("Cpp") || l.Equals("Pascal") || l.Equals("Java")))
                {
                    enu.MoveNext();
                    continue;
                }

                if (offset == -1 || pair.function.Name.Contains("(anon_method_") || offset == 0)     //Сережа - не волшебник, еще один воркараунд над его обвесом
                {
                    enu.MoveNext();
                    continue;
                }

                //Инициализация массива файла, а так же определение места вставки заголовка, если это CPP
                if (fileName != file.FullFileName_Canonized)
                {
                    try
                    {
                        reader = new AbstractReader(file.FullFileName_Canonized);
                    }
                    catch (Exception ex)
                    {
                        //Формируем сообщение
                        string message = new string[]
                        {
                            "Не удалось открыть файл " + file.FullFileName_Canonized + ".",
                            ex.ToFullMultiLineMessage()
                        }.ToMultiLineString();

                        IA.Monitor.Log.Error(PluginSettings.Name, message);
                    }


                    contents = reader.getText();
                    contentsLength = contents.Length;

                    if (!sensorList2.ContainsKey(file.Id))
                    {
                        sensorList2.Add(file.Id, new List<SensorItem>());

                        if (fileLanguages.Any(l => l.Equals("Cpp") || l.Equals("C")))
                        {
                            currentLine = 0;
                            do
                            {
                                line = reader.getLine();
                                currentLine++;
                                if (!String.IsNullOrWhiteSpace(line))
                                {
                                    var lineTrimmed = line.Trim().ToLower();
                                    if (lineTrimmed.Contains("#include \"stdafx.h\"") || lineTrimmed.Contains("#include <stdafx.h>"))
                                    {
                                        sensorList2[file.Id].Add(new SensorItem(reader.getLineOffset(currentLine), -1));
                                        currentLine = 0;
                                        break;
                                    }
                                }
                            } while (line != null);

                            if (currentLine != 0)
                            {
                                currentLine = 0;
                                sensorList2[file.Id].Add(new SensorItem(0, -1));
                            }
                        }
                        else if (fileLanguages.Contains("Pascal") && !PluginSettings.IsDelphiSensors_Text)
                        {
                            int impOffset = contents.IndexOf("\nimplementation", StringComparison.OrdinalIgnoreCase);
                            if (impOffset == -1)
                                impOffset = contents.IndexOf("\n implementation", StringComparison.OrdinalIgnoreCase);
                            if (impOffset > -1)
                            {
                                sensorList2[file.Id].Add(new SensorItem(impOffset, -1));
                            }
                        }
                        else if (fileLanguages.Contains("Java"))
                        {
                            currentLine = 0;
                            do
                            {
                                line = reader.getLine();
                                currentLine++;
                                if (!String.IsNullOrWhiteSpace(line))
                                {
                                    var lineTrimmed = line.Trim();
                                    if (lineTrimmed.StartsWith("package ") && lineTrimmed.EndsWith(";"))
                                    {
                                        sensorList2[file.Id].Add(new SensorItem(reader.getLineOffset(currentLine + 1), -1));
                                        currentLine = 0;
                                        break;
                                    }
                                }
                            } while (line != null);

                            if (currentLine != 0)
                            {
                                currentLine = 0;
                                sensorList2[file.Id].Add(new SensorItem(0, -1));
                            }
                        }
                    }
                }
                //FIXME: странный костыль, надо вкурить и переделать
                fileName = file.FullFileName_Canonized;

                //Проверка на баги. Не превосходит ли смещение длину файла?
                if (offset > contentsLength)
                {
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Смещение функции " + pair.function.Name + " неверно offset= " + offset);
                    enu.MoveNext();
                    continue;
                }

                // ---> Вставка датчиков <---

                nextoffset = contents.Length - 1;
                //Определяем смещение
                if (enu.MoveNext()) // сдвиг енумератора на функцию вперед - адский костыль, но придает существенное облегчение
                {
                    //
                    //ПЛОХО СРАБАТЫВАЕТ для ситуации, когда в функцию вложена другая функция - вложенная по location следующая, но не следующая за данной, поэтому в этом месте случается брак вставки :(
                    //нужно научиться отлавливать Anon и lambda при условии, что если текущая функция тоже лямбда, то за ней лямбда - норм.
                    //а вообще, нужно ввести понятие вложенности функций и как-то это определять.
                    //
                    // Таким образом, данный код нормально срабатывает только для функций без вложенных
                    //
                    // Написать быструю функцию поиска баланса скобок с параметром - вид скобки. И реализовать это где-то при определении функции, шоб сразу в стораге были границы функции.
                    var nextMethod = enu.Current.function as IMethod;

                    if ((nextMethod == null
                            || nextMethod.methodOfClass.Name.IndexOf("Anon_", StringComparison.InvariantCulture) == -1)
                        && enu.Current.function.Name.IndexOf("lambda_expr_", StringComparison.InvariantCulture) == -1)
                    {
                        Location loc = enu.Current.location;

                        int offsetNext = (int)loc.GetOffset() - 1;
                        if (loc.GetFile() == file && offsetNext < contentsLength)
                        {
                            int lastCurlBracket = contents.LastIndexOf('}', offsetNext);
                            nextoffset = lastCurlBracket > offset ? lastCurlBracket + 1 : offsetNext;
                        }
                    }
                    else
                    {
                        //поиск по парным скобкам ломает паскаль
                        { //потенциально можем вылететь на случае, когда у функции нихрена нет ни скобок ничего...
                            int localIndexOfParenthesis = contents.IndexOf('(', offset);
                            int localIndexOfBrace = contents.IndexOf('{', offset);
                            if (localIndexOfBrace < localIndexOfParenthesis)
                            { // funcdecl {funcbody} ( some stuff )
                                nextoffset = SkobkaBalance(contents, localIndexOfBrace) + 1;
                            }
                            else
                            { //funcdecl () { funcbody }
                                nextoffset = SkobkaBalance(contents, localIndexOfParenthesis);
                                nextoffset = SkobkaBalance(contents, contents.IndexOf('{', nextoffset)) + 1;
                            }
                        }
                    }
                }

                if (nextoffset >= contents.Length)
                    nextoffset = contents.Length - 1;
                string search_txt = contents.Substring(offset, nextoffset - offset);

                //Поиск баланса скобок
                if (fileLanguages.Any(l => l.Equals("Cpp") || l.Equals("C") || l.Equals("Java")))
                //fileLanguages.Contains("Cpp") || fileLanguages.Contains("C") || fileLanguages.Contains("Java")/* || file.fileType.ContainsLanguages().Contains("C_Sharp")*/)
                {
                    int indexOfOpenCurlBrace = search_txt.IndexOf("{");
                    int indexOfSemicolon = search_txt.IndexOf(";");

                    if (indexOfOpenCurlBrace > 0 && ((indexOfSemicolon > 0 && indexOfOpenCurlBrace < indexOfSemicolon) || indexOfSemicolon < 0))
                    {
                        int indexOfOpenBrace = search_txt.IndexOf("(");
                        if (indexOfOpenBrace > -1 && indexOfOpenBrace < indexOfOpenCurlBrace)
                        {
                            //поиск баланса скобок
                            bool f = false;
                            int ind = 0, pos = 0; // баланс и позиция от смещения
                            for (int i = pair.function.Name.Length; i <= search_txt.LastIndexOf("{"); i++)
                            {
                                //багфикс. считаем баланс скобок - встретили ; - выход и не ставим
                                if (search_txt[i] == ';')
                                    break;

                                //если баланс нулевой и встретили { - выходим и ставим
                                if (search_txt[i] == '{' && ind == 0)
                                {
                                    //так как у нас появляется ошибка, что оно ставит датчики после макросов - если встретили спец слово - датчик не ставим
                                    {
                                        string reduced = search_txt.Substring(0, i);

                                        int indParenthesis = reduced.LastIndexOf(")");
                                        if (indParenthesis > 0)
                                            reduced = reduced.Substring(indParenthesis); //не совсем точное обрезание, но лучше чем ничего

                                        bool fbad = false;

                                        if (reduced.Length > 10)//чтобы не проверять слишком много результатов
                                        {
                                            foreach (string badword in structwords)
                                                foreach (string del in delimiters)
                                                    if (reduced.Contains(del + badword + " "))
                                                    {
                                                        fbad = true;
                                                        break;
                                                    }

                                            if (fbad)
                                                break;
                                        }

                                        //блок обработки закомментированых ковычек обычно вида //{{AFX_....
                                        if (reduced.Contains("\n"))
                                        {
                                            reduced = reduced.Substring(reduced.LastIndexOf("\n"));

                                            if (reduced.Contains("//"))
                                                break;
                                        }
                                    }

                                    f = true;
                                    pos = i + 1;
                                    break;
                                }

                                if (search_txt[i] == '(') ind++;
                                else if (search_txt[i] == ')') ind--;
                            }

                            if (f)
                            {

                                //костыль для бага 197, будет выдавать ложные срабатывания при наличии комментариев с ключевыми словами
                                //добавлено жёсткое условие наличия скобки сразу за ключевым словом
                                if (fileLanguages.Any(l => l.Equals("Java")) && (pair.function as IMethod) != null
                                    && (pair.function as IMethod).IsConstructor())
                                {
                                    var indSuper = search_txt.IndexOf("super", pos);
                                    var indThis = search_txt.IndexOf("this", pos);

                                    if (indSuper != -1)
                                    {
                                        indSuper = indSuper + 5;
                                        while (indSuper < search_txt.Length && search_txt[indSuper] == ' ')
                                            indSuper++;
                                        if (search_txt[indSuper] == '(')
                                            pos = indSuper;
                                    }

                                    if (indThis != -1)
                                    {
                                        indThis = indThis + 4;
                                        while (indThis < search_txt.Length && search_txt[indThis] == ' ')
                                            indThis++;
                                        if (search_txt[indThis] == '(')
                                            pos = indThis;
                                    }

                                    if (search_txt.Length > pos && search_txt[pos] == '(')
                                    {

                                        pos = SkobkaBalance(search_txt, pos) + 1; //+1 - на скобочку, +1 на ;
                                    }


                                }

                                sensorList2[file.Id].Add(new SensorItem(offset + pos, currentSensor++, pair.function));

                            }

                        }
                        else
                        {
                            sensorList2[file.Id].Add(new SensorItem(indexOfOpenCurlBrace + 1 + offset, currentSensor++, pair.function));
                        }
                    }
                }
                else if (fileLanguages.Contains("Pascal"))
                {
                    //Флаг для выхода из внешнего цикла
                    bool break_flag = false;

                    foreach (string start_del in delimiters)
                    {
                        foreach (string end_del in delimiters)
                        {
                            int ind_found = search_txt.IndexOf(start_del + "begin" + end_del, StringComparison.OrdinalIgnoreCase);
                            if (ind_found > 0)
                            {
                                //int ind_found = search_txt.ToLower().IndexOf(start_del + "begin" + end_del);
                                bool tr = true; // нет ли спецслов

                                foreach (string dem in delimiters)
                                    foreach (string spec in specwords)
                                    {
                                        int indexOfSpecDelim = search_txt.IndexOf(spec + dem, StringComparison.OrdinalIgnoreCase);
                                        if (indexOfSpecDelim > 0 && ind_found > indexOfSpecDelim)
                                            tr = false;
                                    }

                                if (tr)
                                    sensorList2[file.Id].Add(new SensorItem(ind_found + (start_del + "begin" + end_del).Length + offset, currentSensor++, pair.function));

                                //Выход из 2го цикла - иначе выйдет только из первого
                                break_flag = true;

                                break;
                            }
                        }
                        if (break_flag)
                            break;
                    }
                }
            }


            //Теперь расставляем датчики в файлы

            //Объявляем задачу
            counter = 0;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_WITH_SENSORS_COPYING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, storage.files.Count());

            //sort offsets for ensure right order.
            sensorList2.Values.AsParallel().ForEach(l => l.Sort());

            foreach (var fileid in sensorList2.Keys)
            {
                IFile inFile = storage.files.GetFile(fileid);

                if (inFile == null)
                    continue;

                string outFile = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "CS_Sensors", inFile.RelativeFileName_Original);

                if (inFile.FullFileName_Canonized.ToLower() == outFile)
                    throw new Exception("Ошибка настроек - некорректно настроенные папки");

                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(outFile));      // кладем каждый файл в "его" папку. Для этого создаем эту папку для файла

                //FIXME - удаляем файл, мегахаком отлавливаем ошибки, чтобы у нас не сыпалось ничего
                try
                {
                    System.IO.File.Delete(outFile);
                }
                catch (Exception) { }

                try
                {
                    reader = new AbstractReader(inFile.FullFileName_Canonized);
                }
                catch (Exception ex)
                {
                    //Формируем сообщение
                    string message = new string[]
                    {
                        "Не удалось открыть файл " + inFile + ".",
                        ex.ToFullMultiLineMessage()
                    }.ToMultiLineString();

                    IA.Monitor.Log.Error(PluginSettings.Name, message);
                }

                string textToRead = reader.getText();
                StringBuilder sbToWrite = new StringBuilder(textToRead);
                //сортировка
                //bool flag;
                //do
                //{
                //    flag = true;
                //    for (int j = 0; j < sensorList[i].Count - 1; j++)
                //    {
                //        if (Int32.Parse(sensorList[i][j].Split('*')[0]) > Int32.Parse(sensorList[i][j + 1].Split('*')[0]))
                //        {
                //            flag = false;
                //            string temp = sensorList[i][j];
                //            sensorList[i][j] = sensorList[i][j + 1];
                //            sensorList[i][j + 1] = temp;
                //        }
                //    }
                //} while (!flag);
                var inFileLanguages = inFile.fileType.ContainsLanguages();

                if (sensorList2[fileid].Count != 0)
                {
                    for (int j = sensorList2[fileid].Count - 1; j >= 0; j--)
                    {
                        //string[] split = sensorList[i][j].Split('*'); // первое - номер строки - второе - датчик
                        //int offset = Int32.Parse(split[0]);
                        //string sensor = split[1];
                        var sensor = sensorList2[fileid][j];
                        string charsToInsert = String.Empty;



                        if (sensor.IsHeader)
                        {
                            if (inFileLanguages.Any(l => l.Equals("Cpp") || l.Equals("C")))
                                charsToInsert = PluginSettings.SensorInclude_Cpp + "\n";
                            else if (inFileLanguages.Contains("Pascal"))
                                charsToInsert = PluginSettings.SensorInclude_Delphi + "\n";  //так надо перед имплементэйшн
                            else if (inFileLanguages.Contains("Java"))
                                charsToInsert = PluginSettings.SensorInclude_Java + "\n";
                        }
                        else
                        {
                            if (inFileLanguages.Contains("C"))
                                charsToInsert = PluginSettings.SensorText_C; //
                            else if (inFileLanguages.Any(l => l.Equals("Cpp") || l.Equals("Java")))
                                charsToInsert = PluginSettings.SensorText_Cpp;
                            else if (inFileLanguages.Contains("Pascal"))
                            {
                                if (!PluginSettings.IsDelphiSensors_Text)
                                    charsToInsert = PluginSettings.SensorText_Delphi;
                                else
                                    charsToInsert = PluginSettings.SensorText_Delphi_Text;
                            }
                            else //default
                                charsToInsert = PluginSettings.SensorText_Cpp;

                            charsToInsert = charsToInsert.Replace(PluginSettings.SensorNumReplacement, sensor.SensorId.ToString());
                            charsToInsert = charsToInsert.Replace(PluginSettings.SensorsIdReplacement, PluginSettings.SensorsIdentificator.ToString());
                        }

                        sbToWrite.Insert(sensor.Offset, charsToInsert);
                    }
                }

                //для текстовых датчиков pascal workaround //FIXME переписать!
                if (sensorList2[fileid].Count != 0 && !sensorList2[fileid][0].IsHeader && inFileLanguages.Contains("Pascal"))
                {
                    int start_search = 0;
                    int end_search = textToRead.Length;

                    //Начало поиска
                    foreach (string specword in dlp_specwords)
                    {
                        int indexOfSpecword = textToRead.IndexOf(specword, StringComparison.OrdinalIgnoreCase);

                        if (indexOfSpecword != -1 && indexOfSpecword >= start_search)
                        {
                            start_search = indexOfSpecword + specword.Length + 1;
                            break;
                        }

                    }

                    //хак для поиска uses
                    int indexOfusesn = textToRead.IndexOf("\nuses\n", start_search, StringComparison.OrdinalIgnoreCase);
                    if (indexOfusesn > -1)
                        start_search = textToRead.IndexOf(";", indexOfusesn) + 1;
                    else
                    {
                        indexOfusesn = textToRead.IndexOf("\r\nuses\r\n", start_search, StringComparison.OrdinalIgnoreCase);
                        if (indexOfusesn > -1)
                            start_search = textToRead.IndexOf(";", indexOfusesn) + 1;
                    }

                    //поиск реализации
                    foreach (string specword2 in dlp_specwords2)
                    {
                        int indexOfSpecword2 = textToRead.IndexOf(specword2, start_search, StringComparison.OrdinalIgnoreCase);
                        if (indexOfSpecword2 != -1 && indexOfSpecword2 < end_search - start_search)
                            end_search = start_search + indexOfSpecword2 + specword2.Length + 1;
                    }

                    if (end_search > 0)
                    {
                        int indexOfVar = textToRead.IndexOf("\nvar\n", start_search, end_search - start_search, StringComparison.OrdinalIgnoreCase);

                        if (indexOfVar != -1)
                            sbToWrite.Insert(indexOfVar + start_search + 5, "\nrnt_sensor : TextFile;\n");
                        else
                        {
                            indexOfVar = textToRead.IndexOf("\r\nvar\r\n", start_search, end_search - start_search, StringComparison.OrdinalIgnoreCase);
                            if (indexOfVar != -1)
                                sbToWrite.Insert(indexOfVar + start_search + 5, "\r\nrnt_sensor : TextFile;\r\n");
                            else
                                sbToWrite.Insert(start_search + 1, "\r\nrnt_sensor : TextFile;\r\n");
                        }

                    }
                }

                reader.writeTextToFile(outFile, sbToWrite.ToString());

                //Отображение прогресса
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_WITH_SENSORS_COPYING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);
            }

            //Сообщаем о количестве вставленных датчиков
            IA.Monitor.Log.Warning(PluginSettings.Name, "Вставлено датчиков: " + (currentSensor - PluginSettings.FirstSensorNumber).ToString());

        }

        #region PluginInterface Members
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            // принимаем в параметрах хранилище
            this.storage = storage;

            PluginSettings.FirstSensorNumber = Properties.Settings.Default.FirstSensorNumber;
            PluginSettings.SensorText_C = Properties.Settings.Default.SensorText_C;
            PluginSettings.SensorText_Cpp = Properties.Settings.Default.SensorText_Cpp;
            PluginSettings.SensorInclude_Cpp = Properties.Settings.Default.SensorInclude_Cpp;
            PluginSettings.SensorText_Delphi = Properties.Settings.Default.SensorText_Delphi;
            PluginSettings.SensorText_Delphi_Text = Properties.Settings.Default.SensorText_Delphi_Text;
            PluginSettings.SensorInclude_Delphi = Properties.Settings.Default.SensorInclude_Delphi;
            PluginSettings.IsDelphiSensors_Text = Properties.Settings.Default.IsDelphiSensors_Text;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.FirstSensorNumber = settingsBuffer.GetInt32();
            PluginSettings.SensorText_C = settingsBuffer.GetString();
            PluginSettings.SensorText_Cpp = settingsBuffer.GetString();
            PluginSettings.SensorInclude_Cpp = settingsBuffer.GetString();
            PluginSettings.SensorText_Delphi = settingsBuffer.GetString();
            PluginSettings.SensorText_Delphi_Text = settingsBuffer.GetString();
            PluginSettings.SensorInclude_Delphi = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.IsDelphiSensors_Text);
            try
            { //backward compatibility
                PluginSettings.SensorsIdentificator = settingsBuffer.GetInt32();
            }
            catch
            {
                PluginSettings.SensorsIdentificator = 1;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.FirstSensorNumber = 1;
            PluginSettings.SensorText_C = "int rnt_sensorcall = ____Din_Go(\" \",%DAT%);";
            PluginSettings.SensorText_Cpp = "____Din_Go(\" \",%DAT%);";
            PluginSettings.SensorInclude_Cpp = "#include \"c:/sensor.h\"";
            PluginSettings.SensorText_Delphi = "____Din_Go(' ',%DAT%);";
            PluginSettings.SensorText_Delphi_Text = "System.AssignFile(rnt_sensor,'c:\\Delphifile.txt'); if not System.FileExists('c:\\Delphifile.txt') then begin System.Rewrite(rnt_sensor); System.CloseFile(rnt_sensor); end; System.Append(rnt_sensor); System.Writeln(rnt_sensor,'%DAT%'); System.Flush(rnt_sensor); System.CloseFile(rnt_sensor);";
            PluginSettings.SensorInclude_Delphi = "function ____Din_Go(ff: PChar; a: cardinal) : integer; stdcall; external 'c:\\Windows\\system32\\sensor.dll'";
            PluginSettings.IsDelphiSensors_Text = false;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new CustomSettings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.FirstSensorNumber = PluginSettings.FirstSensorNumber;
            Properties.Settings.Default.SensorsIdentificator = PluginSettings.SensorsIdentificator;
            Properties.Settings.Default.SensorText_C = PluginSettings.SensorText_C;
            Properties.Settings.Default.SensorText_Cpp = PluginSettings.SensorText_Cpp;
            Properties.Settings.Default.SensorInclude_Cpp = PluginSettings.SensorInclude_Cpp;
            Properties.Settings.Default.SensorText_Delphi = PluginSettings.SensorText_Delphi;
            Properties.Settings.Default.SensorText_Delphi_Text = PluginSettings.SensorText_Delphi_Text;
            Properties.Settings.Default.SensorInclude_Delphi = PluginSettings.SensorInclude_Delphi;
            Properties.Settings.Default.IsDelphiSensors_Text = PluginSettings.IsDelphiSensors_Text;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.FirstSensorNumber);
            settingsBuffer.Add(PluginSettings.SensorText_C);
            settingsBuffer.Add(PluginSettings.SensorText_Cpp);
            settingsBuffer.Add(PluginSettings.SensorInclude_Cpp);
            settingsBuffer.Add(PluginSettings.SensorText_Delphi);
            settingsBuffer.Add(PluginSettings.SensorText_Delphi_Text);
            settingsBuffer.Add(PluginSettings.SensorInclude_Delphi);
            settingsBuffer.Add(PluginSettings.IsDelphiSensors_Text);
            settingsBuffer.Add(PluginSettings.SensorsIdentificator);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (String.IsNullOrWhiteSpace(PluginSettings.SensorText_C))
            {
                message = "Текст датчика (С) пуст.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(PluginSettings.SensorText_Cpp))
            {
                message = "Текст датчика (С++) пуст.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(PluginSettings.SensorInclude_Cpp))
            {
                message = "Текст директивы (С/С++) пуст.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(PluginSettings.SensorText_Delphi))
            {
                message = "Текст датчика (Delphi) пуст.";
                return false;
            }

            if (PluginSettings.IsDelphiSensors_Text && String.IsNullOrWhiteSpace(PluginSettings.SensorText_Delphi_Text))
            {
                message = "Текст текстового датчика (Delphi) пуст.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(PluginSettings.SensorInclude_Delphi))
            {
                message = "Текст директивы датчика (Delphi) пуст.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FUNCTION_PROCESSING.Description()),
                    new Monitor.Tasks.Task(PluginSettings.Name,PluginSettings.Tasks.FILES_WITH_SENSORS_COPYING.Description())
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            if (!storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Any(file =>
                {
                    var fileLanguages = file.fileType.ContainsLanguages();
                    return fileLanguages.Any(l => l.Equals("C") || l.Equals("Cpp") || l.Equals("Pascal") || l.Equals("Java"));
                }))
            //.Contains("C") ||
            //file.fileType.ContainsLanguages().Contains("Cpp") ||
            //file.fileType.ContainsLanguages().Contains("Pascal") ||
            //file.fileType.ContainsLanguages().Contains("Java")))
            {
                Monitor.Log.Error(PluginSettings.Name, "Нет файлов для обработки.");
                return;
            }

            ulong count = 0;
            //foreach (Store.LocationFunctionPair pair in storage.functions.EnumerateFunctionLocationsInSortOrder())
            //    count++;
            count = storage.functions.Count;
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNCTION_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, count);

            // запуск программы по вставлке датчиков
            Main(null);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }
        #endregion

        private static Dictionary<char, char> skobkaPairs = new Dictionary<char, char>()
        { {'(',')' }, { '{','}'}, {'[',']' }, {'<','>' } };

        /// <summary>
        /// Найти ближайшую парную скобку.
        /// </summary>
        /// <param name="source">Строка для поиска.</param>
        /// <param name="startPosition">Значение позиции целевой открывающей скобки.</param>
        /// <returns>Значение позиции соответствующей закрывающей скобки если таковая найдена либо значение длины строки, если стартовая точка -1 либо парная скобка не найдена.</returns>
        private int SkobkaBalance(string source, int startPosition = 0)
        {
            if (startPosition == -1)
                return source.Length - 1;

            if (startPosition >= source.Length)
            {
                throw new ArgumentException("Задан недопустимый параметр начального индекса.");
            }

            char objToFind = source[startPosition];

            if (!skobkaPairs.Keys.Contains(objToFind))
                throw new ArgumentException("Задан недопустимый параметр - стартовый индекс не указывает на скобку.");

            char objPair = skobkaPairs[objToFind];
            int counter = 0;
            int i = startPosition;
            do
            {
                if (source[i] == objToFind)
                    counter++;
                else if (source[i] == objPair)
                    counter--;
                i++;
            }
            while (counter != 0 && i < source.Length);
            return i;
        }
    }

    static class IMethodExtensions
    {
        public static bool IsConstructor(this IMethod method)
        {
            return method.Name.Equals(method.methodOfClass.Name);
        }
    }
}
