namespace IA.Plugins.Analyses.CS_Sensors
{
    partial class CustomSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstSensorNumber_label = new System.Windows.Forms.Label();
            this.sensorText_cpp_textBox = new System.Windows.Forms.TextBox();
            this.sensorText_cpp_label = new System.Windows.Forms.Label();
            this.sensorInclude_cpp_textBox = new System.Windows.Forms.TextBox();
            this.sensorInclude_cpp_label = new System.Windows.Forms.Label();
            this.sensorText_delphi_text_textBox = new System.Windows.Forms.TextBox();
            this.sensorText_delphi_text_label = new System.Windows.Forms.Label();
            this.sensorText_delphi_textBox = new System.Windows.Forms.TextBox();
            this.sensorText_delphi_label = new System.Windows.Forms.Label();
            this.sensorText_c_textBox = new System.Windows.Forms.TextBox();
            this.sensorText_c_label = new System.Windows.Forms.Label();
            this.sensorInclude_delphi_textBox = new System.Windows.Forms.TextBox();
            this.sensorInclude_delphi_label = new System.Windows.Forms.Label();
            this.isDelphiSensors_text_checkBox = new System.Windows.Forms.CheckBox();
            this.mainSettings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.firstSensorNumber_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.c_cpp_groupBox = new System.Windows.Forms.GroupBox();
            this.c_cpp_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sensorInclude_java_textBox = new System.Windows.Forms.TextBox();
            this.sensorInclude_java_label = new System.Windows.Forms.Label();
            this.delphi_groupBox = new System.Windows.Forms.GroupBox();
            this.delphi_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.mainSettings_groupBox = new System.Windows.Forms.GroupBox();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SensorsIdentificator_label = new System.Windows.Forms.Label();
            this.SensorsIdentificator_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.mainSettings_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).BeginInit();
            this.c_cpp_groupBox.SuspendLayout();
            this.c_cpp_tableLayoutPanel.SuspendLayout();
            this.delphi_groupBox.SuspendLayout();
            this.delphi_tableLayoutPanel.SuspendLayout();
            this.mainSettings_groupBox.SuspendLayout();
            this.main_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SensorsIdentificator_numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // firstSensorNumber_label
            // 
            this.firstSensorNumber_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_label.AutoSize = true;
            this.firstSensorNumber_label.Location = new System.Drawing.Point(3, 8);
            this.firstSensorNumber_label.Name = "firstSensorNumber_label";
            this.firstSensorNumber_label.Size = new System.Drawing.Size(179, 13);
            this.firstSensorNumber_label.TabIndex = 0;
            this.firstSensorNumber_label.Text = "����� ������� ������� (%DAT%):";
            this.firstSensorNumber_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sensorText_cpp_textBox
            // 
            this.sensorText_cpp_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_cpp_textBox.Location = new System.Drawing.Point(148, 28);
            this.sensorText_cpp_textBox.Name = "sensorText_cpp_textBox";
            this.sensorText_cpp_textBox.Size = new System.Drawing.Size(356, 20);
            this.sensorText_cpp_textBox.TabIndex = 3;
            // 
            // sensorText_cpp_label
            // 
            this.sensorText_cpp_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_cpp_label.AutoSize = true;
            this.sensorText_cpp_label.Location = new System.Drawing.Point(3, 31);
            this.sensorText_cpp_label.Name = "sensorText_cpp_label";
            this.sensorText_cpp_label.Size = new System.Drawing.Size(139, 13);
            this.sensorText_cpp_label.TabIndex = 0;
            this.sensorText_cpp_label.Text = "����� ������� (C++/Java):";
            this.sensorText_cpp_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sensorInclude_cpp_textBox
            // 
            this.sensorInclude_cpp_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorInclude_cpp_textBox.Location = new System.Drawing.Point(148, 53);
            this.sensorInclude_cpp_textBox.Name = "sensorInclude_cpp_textBox";
            this.sensorInclude_cpp_textBox.Size = new System.Drawing.Size(356, 20);
            this.sensorInclude_cpp_textBox.TabIndex = 4;
            // 
            // sensorInclude_cpp_label
            // 
            this.sensorInclude_cpp_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorInclude_cpp_label.AutoSize = true;
            this.sensorInclude_cpp_label.Location = new System.Drawing.Point(3, 56);
            this.sensorInclude_cpp_label.Name = "sensorInclude_cpp_label";
            this.sensorInclude_cpp_label.Size = new System.Drawing.Size(139, 13);
            this.sensorInclude_cpp_label.TabIndex = 3;
            this.sensorInclude_cpp_label.Text = "��������� (C/C++):";
            this.sensorInclude_cpp_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sensorText_delphi_text_textBox
            // 
            this.sensorText_delphi_text_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_delphi_text_textBox.Location = new System.Drawing.Point(193, 72);
            this.sensorText_delphi_text_textBox.Name = "sensorText_delphi_text_textBox";
            this.sensorText_delphi_text_textBox.Size = new System.Drawing.Size(311, 20);
            this.sensorText_delphi_text_textBox.TabIndex = 7;
            // 
            // sensorText_delphi_text_label
            // 
            this.sensorText_delphi_text_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_delphi_text_label.AutoSize = true;
            this.sensorText_delphi_text_label.Location = new System.Drawing.Point(3, 76);
            this.sensorText_delphi_text_label.Name = "sensorText_delphi_text_label";
            this.sensorText_delphi_text_label.Size = new System.Drawing.Size(184, 13);
            this.sensorText_delphi_text_label.TabIndex = 9;
            this.sensorText_delphi_text_label.Text = "����� ���������� ������� (Delphi):";
            this.sensorText_delphi_text_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sensorText_delphi_textBox
            // 
            this.sensorText_delphi_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_delphi_textBox.Location = new System.Drawing.Point(193, 6);
            this.sensorText_delphi_textBox.Name = "sensorText_delphi_textBox";
            this.sensorText_delphi_textBox.Size = new System.Drawing.Size(311, 20);
            this.sensorText_delphi_textBox.TabIndex = 5;
            // 
            // sensorText_delphi_label
            // 
            this.sensorText_delphi_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_delphi_label.AutoSize = true;
            this.sensorText_delphi_label.Location = new System.Drawing.Point(3, 10);
            this.sensorText_delphi_label.Name = "sensorText_delphi_label";
            this.sensorText_delphi_label.Size = new System.Drawing.Size(184, 13);
            this.sensorText_delphi_label.TabIndex = 8;
            this.sensorText_delphi_label.Text = "����� ������� (Delphi):";
            this.sensorText_delphi_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sensorText_c_textBox
            // 
            this.sensorText_c_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_c_textBox.Location = new System.Drawing.Point(148, 3);
            this.sensorText_c_textBox.Name = "sensorText_c_textBox";
            this.sensorText_c_textBox.Size = new System.Drawing.Size(356, 20);
            this.sensorText_c_textBox.TabIndex = 2;
            // 
            // sensorText_c_label
            // 
            this.sensorText_c_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_c_label.AutoSize = true;
            this.sensorText_c_label.Location = new System.Drawing.Point(3, 6);
            this.sensorText_c_label.Name = "sensorText_c_label";
            this.sensorText_c_label.Size = new System.Drawing.Size(139, 13);
            this.sensorText_c_label.TabIndex = 11;
            this.sensorText_c_label.Text = "����� ������� (C):";
            this.sensorText_c_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sensorInclude_delphi_textBox
            // 
            this.sensorInclude_delphi_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorInclude_delphi_textBox.Location = new System.Drawing.Point(193, 39);
            this.sensorInclude_delphi_textBox.Name = "sensorInclude_delphi_textBox";
            this.sensorInclude_delphi_textBox.Size = new System.Drawing.Size(311, 20);
            this.sensorInclude_delphi_textBox.TabIndex = 6;
            // 
            // sensorInclude_delphi_label
            // 
            this.sensorInclude_delphi_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorInclude_delphi_label.AutoSize = true;
            this.sensorInclude_delphi_label.Location = new System.Drawing.Point(3, 43);
            this.sensorInclude_delphi_label.Name = "sensorInclude_delphi_label";
            this.sensorInclude_delphi_label.Size = new System.Drawing.Size(184, 13);
            this.sensorInclude_delphi_label.TabIndex = 13;
            this.sensorInclude_delphi_label.Text = "��������� (Delphi):";
            this.sensorInclude_delphi_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // isDelphiSensors_text_checkBox
            // 
            this.isDelphiSensors_text_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isDelphiSensors_text_checkBox.AutoSize = true;
            this.delphi_tableLayoutPanel.SetColumnSpan(this.isDelphiSensors_text_checkBox, 2);
            this.isDelphiSensors_text_checkBox.Location = new System.Drawing.Point(3, 108);
            this.isDelphiSensors_text_checkBox.Name = "isDelphiSensors_text_checkBox";
            this.isDelphiSensors_text_checkBox.Size = new System.Drawing.Size(501, 17);
            this.isDelphiSensors_text_checkBox.TabIndex = 8;
            this.isDelphiSensors_text_checkBox.Text = "��������� ������ ��������� ������� � Delphi";
            this.isDelphiSensors_text_checkBox.UseVisualStyleBackColor = true;
            this.isDelphiSensors_text_checkBox.CheckedChanged += new System.EventHandler(this.isDelphiSensors_text_checkBox_CheckedChanged);
            // 
            // mainSettings_tableLayoutPanel
            // 
            this.mainSettings_tableLayoutPanel.ColumnCount = 4;
            this.mainSettings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.mainSettings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainSettings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 142F));
            this.mainSettings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainSettings_tableLayoutPanel.Controls.Add(this.SensorsIdentificator_numericUpDown, 3, 0);
            this.mainSettings_tableLayoutPanel.Controls.Add(this.SensorsIdentificator_label, 2, 0);
            this.mainSettings_tableLayoutPanel.Controls.Add(this.firstSensorNumber_label, 0, 0);
            this.mainSettings_tableLayoutPanel.Controls.Add(this.firstSensorNumber_numericUpDown, 1, 0);
            this.mainSettings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSettings_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.mainSettings_tableLayoutPanel.Name = "mainSettings_tableLayoutPanel";
            this.mainSettings_tableLayoutPanel.RowCount = 1;
            this.mainSettings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainSettings_tableLayoutPanel.Size = new System.Drawing.Size(507, 30);
            this.mainSettings_tableLayoutPanel.TabIndex = 16;
            // 
            // firstSensorNumber_numericUpDown
            // 
            this.firstSensorNumber_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_numericUpDown.Location = new System.Drawing.Point(188, 5);
            this.firstSensorNumber_numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.firstSensorNumber_numericUpDown.Name = "firstSensorNumber_numericUpDown";
            this.firstSensorNumber_numericUpDown.Size = new System.Drawing.Size(84, 20);
            this.firstSensorNumber_numericUpDown.TabIndex = 1;
            this.firstSensorNumber_numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // c_cpp_groupBox
            // 
            this.c_cpp_groupBox.Controls.Add(this.c_cpp_tableLayoutPanel);
            this.c_cpp_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c_cpp_groupBox.Location = new System.Drawing.Point(3, 58);
            this.c_cpp_groupBox.Name = "c_cpp_groupBox";
            this.c_cpp_groupBox.Size = new System.Drawing.Size(513, 119);
            this.c_cpp_groupBox.TabIndex = 17;
            this.c_cpp_groupBox.TabStop = false;
            this.c_cpp_groupBox.Text = "C/C++";
            // 
            // c_cpp_tableLayoutPanel
            // 
            this.c_cpp_tableLayoutPanel.ColumnCount = 2;
            this.c_cpp_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.c_cpp_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.c_cpp_tableLayoutPanel.Controls.Add(this.sensorInclude_java_textBox, 1, 3);
            this.c_cpp_tableLayoutPanel.Controls.Add(this.sensorInclude_java_label, 0, 3);
            this.c_cpp_tableLayoutPanel.Controls.Add(this.sensorText_c_label, 0, 0);
            this.c_cpp_tableLayoutPanel.Controls.Add(this.sensorText_c_textBox, 1, 0);
            this.c_cpp_tableLayoutPanel.Controls.Add(this.sensorText_cpp_label, 0, 1);
            this.c_cpp_tableLayoutPanel.Controls.Add(this.sensorText_cpp_textBox, 1, 1);
            this.c_cpp_tableLayoutPanel.Controls.Add(this.sensorInclude_cpp_label, 0, 2);
            this.c_cpp_tableLayoutPanel.Controls.Add(this.sensorInclude_cpp_textBox, 1, 2);
            this.c_cpp_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c_cpp_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.c_cpp_tableLayoutPanel.Name = "c_cpp_tableLayoutPanel";
            this.c_cpp_tableLayoutPanel.RowCount = 4;
            this.c_cpp_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.c_cpp_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.c_cpp_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.c_cpp_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.c_cpp_tableLayoutPanel.Size = new System.Drawing.Size(507, 100);
            this.c_cpp_tableLayoutPanel.TabIndex = 0;
            // 
            // sensorInclude_java_textBox
            // 
            this.sensorInclude_java_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorInclude_java_textBox.Location = new System.Drawing.Point(148, 78);
            this.sensorInclude_java_textBox.Name = "sensorInclude_java_textBox";
            this.sensorInclude_java_textBox.Size = new System.Drawing.Size(356, 20);
            this.sensorInclude_java_textBox.TabIndex = 13;
            // 
            // sensorInclude_java_label
            // 
            this.sensorInclude_java_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorInclude_java_label.AutoSize = true;
            this.sensorInclude_java_label.Location = new System.Drawing.Point(3, 81);
            this.sensorInclude_java_label.Name = "sensorInclude_java_label";
            this.sensorInclude_java_label.Size = new System.Drawing.Size(139, 13);
            this.sensorInclude_java_label.TabIndex = 12;
            this.sensorInclude_java_label.Text = "��������� (Java):";
            this.sensorInclude_java_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // delphi_groupBox
            // 
            this.delphi_groupBox.Controls.Add(this.delphi_tableLayoutPanel);
            this.delphi_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.delphi_groupBox.Location = new System.Drawing.Point(3, 183);
            this.delphi_groupBox.Name = "delphi_groupBox";
            this.delphi_groupBox.Size = new System.Drawing.Size(513, 154);
            this.delphi_groupBox.TabIndex = 19;
            this.delphi_groupBox.TabStop = false;
            this.delphi_groupBox.Text = "Delphi";
            // 
            // delphi_tableLayoutPanel
            // 
            this.delphi_tableLayoutPanel.ColumnCount = 2;
            this.delphi_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.delphi_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.delphi_tableLayoutPanel.Controls.Add(this.sensorText_delphi_label, 0, 0);
            this.delphi_tableLayoutPanel.Controls.Add(this.sensorText_delphi_textBox, 1, 0);
            this.delphi_tableLayoutPanel.Controls.Add(this.sensorInclude_delphi_label, 0, 1);
            this.delphi_tableLayoutPanel.Controls.Add(this.sensorInclude_delphi_textBox, 1, 1);
            this.delphi_tableLayoutPanel.Controls.Add(this.isDelphiSensors_text_checkBox, 0, 3);
            this.delphi_tableLayoutPanel.Controls.Add(this.sensorText_delphi_text_label, 0, 2);
            this.delphi_tableLayoutPanel.Controls.Add(this.sensorText_delphi_text_textBox, 1, 2);
            this.delphi_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.delphi_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.delphi_tableLayoutPanel.Name = "delphi_tableLayoutPanel";
            this.delphi_tableLayoutPanel.RowCount = 4;
            this.delphi_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.delphi_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.delphi_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.delphi_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.delphi_tableLayoutPanel.Size = new System.Drawing.Size(507, 135);
            this.delphi_tableLayoutPanel.TabIndex = 1;
            // 
            // mainSettings_groupBox
            // 
            this.mainSettings_groupBox.Controls.Add(this.mainSettings_tableLayoutPanel);
            this.mainSettings_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSettings_groupBox.Location = new System.Drawing.Point(3, 3);
            this.mainSettings_groupBox.Name = "mainSettings_groupBox";
            this.mainSettings_groupBox.Size = new System.Drawing.Size(513, 49);
            this.mainSettings_groupBox.TabIndex = 20;
            this.mainSettings_groupBox.TabStop = false;
            this.mainSettings_groupBox.Text = "�������� ���������";
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.mainSettings_groupBox, 0, 0);
            this.main_tableLayoutPanel.Controls.Add(this.delphi_groupBox, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.c_cpp_groupBox, 0, 1);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 4;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(519, 353);
            this.main_tableLayoutPanel.TabIndex = 21;
            // 
            // SensorsIdentificator_label
            // 
            this.SensorsIdentificator_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SensorsIdentificator_label.AutoSize = true;
            this.SensorsIdentificator_label.Location = new System.Drawing.Point(278, 8);
            this.SensorsIdentificator_label.Name = "SensorsIdentificator_label";
            this.SensorsIdentificator_label.Size = new System.Drawing.Size(136, 13);
            this.SensorsIdentificator_label.TabIndex = 2;
            this.SensorsIdentificator_label.Text = "������������� (%SID%):";
            this.SensorsIdentificator_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SensorsIdentificator_numericUpDown
            // 
            this.SensorsIdentificator_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SensorsIdentificator_numericUpDown.Location = new System.Drawing.Point(420, 5);
            this.SensorsIdentificator_numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SensorsIdentificator_numericUpDown.Name = "SensorsIdentificator_numericUpDown";
            this.SensorsIdentificator_numericUpDown.Size = new System.Drawing.Size(84, 20);
            this.SensorsIdentificator_numericUpDown.TabIndex = 3;
            this.SensorsIdentificator_numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // CustomSettings
            // 
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(519, 353);
            this.mainSettings_tableLayoutPanel.ResumeLayout(false);
            this.mainSettings_tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).EndInit();
            this.c_cpp_groupBox.ResumeLayout(false);
            this.c_cpp_tableLayoutPanel.ResumeLayout(false);
            this.c_cpp_tableLayoutPanel.PerformLayout();
            this.delphi_groupBox.ResumeLayout(false);
            this.delphi_tableLayoutPanel.ResumeLayout(false);
            this.delphi_tableLayoutPanel.PerformLayout();
            this.mainSettings_groupBox.ResumeLayout(false);
            this.main_tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SensorsIdentificator_numericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox sensorText_cpp_textBox;
        private System.Windows.Forms.Label firstSensorNumber_label;
        private System.Windows.Forms.Label sensorText_cpp_label;
        private System.Windows.Forms.TextBox sensorInclude_cpp_textBox;
        private System.Windows.Forms.Label sensorInclude_cpp_label;
        private System.Windows.Forms.TextBox sensorText_delphi_text_textBox;
        private System.Windows.Forms.Label sensorText_delphi_text_label;
        private System.Windows.Forms.TextBox sensorText_delphi_textBox;
        private System.Windows.Forms.Label sensorText_delphi_label;
        private System.Windows.Forms.TextBox sensorText_c_textBox;
        private System.Windows.Forms.Label sensorText_c_label;
        private System.Windows.Forms.TextBox sensorInclude_delphi_textBox;
        private System.Windows.Forms.Label sensorInclude_delphi_label;
        private System.Windows.Forms.CheckBox isDelphiSensors_text_checkBox;
        private System.Windows.Forms.TableLayoutPanel mainSettings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox c_cpp_groupBox;
        private System.Windows.Forms.TableLayoutPanel c_cpp_tableLayoutPanel;
        private System.Windows.Forms.GroupBox delphi_groupBox;
        private System.Windows.Forms.TableLayoutPanel delphi_tableLayoutPanel;
        private System.Windows.Forms.GroupBox mainSettings_groupBox;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.NumericUpDown firstSensorNumber_numericUpDown;
        private System.Windows.Forms.Label sensorInclude_java_label;
        private System.Windows.Forms.TextBox sensorInclude_java_textBox;
        private System.Windows.Forms.NumericUpDown SensorsIdentificator_numericUpDown;
        private System.Windows.Forms.Label SensorsIdentificator_label;
    }
}
