﻿using System;

namespace IA.Plugins.Analyses.CS_Sensors
{
    struct SensorItem : IComparable<SensorItem>
    {
        private int _offset;

        /// <summary>
        /// offset in file
        /// </summary>
        public int Offset { get { return _offset; } }

        private int _sensorId;

        /// <summary>
        /// sensor Id to insert on pointed offset. If -1 - insert header.
        /// </summary>
        public int SensorId { get { return _sensorId; } }

        public SensorItem(int offset, int sensId, Store.IFunction func = null)
        {
            this._offset = offset;
            this._sensorId = sensId;
            if (sensId > -1)
                func.AddSensor((ulong)sensId, Store.Kind.START);
        }

        /// <summary>
        /// Признак смещения для заголовка
        /// </summary>
        public bool IsHeader { get { return _sensorId == -1; } }

        public int CompareTo(SensorItem other)
        {
            return this.Offset.CompareTo(other.Offset);
        }
    }
}
