﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXXAstTreeBuilder.AST.Decls;

namespace CXXAstTreeBuilder.AST.Table
{
    public class ReferenceTable
    {
        private static ReferenceTable instance;
        private ReferenceTable()
        {
            Table = new Dictionary<Int64, IDeclaration>();
        }

        public static ReferenceTable Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ReferenceTable();
                }
                return instance;
            }
        }
        public static void Reset()
        {
            instance = new ReferenceTable();
        }

        public Dictionary<Int64, IDeclaration> Table;
    }
}
