﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CXXAstTreeBuilder.AST.Statements
{
    public class CStatement:ICXXStatement
    {
        public override void PrettyPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("CStatement");
            xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            xtw.WriteEndElement();
        }
    }
}
