﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using CXXAstTreeBuilder.AST.Helpers;
using CXXAstTreeBuilder.AST.Location;

namespace CXXAstTreeBuilder.AST.Statements
{
    [XmlInclude(typeof(BStatement))]
    [XmlInclude(typeof(CStatement))]
    [XmlInclude(typeof(DStatement))]
    [XmlInclude(typeof(MBStatement))]
    [XmlInclude(typeof(OStatement))]
    [XmlInclude(typeof(RStatement))]
    public abstract class ICXXStatement
    {
        public ICXXStatement Next { get; set; }
        public List<ICXXStatement> InnerNext { get; set; }
        public SourceFileLocation Start { get; set; }
        public SourceFileLocation End { get; set; }

        public int StaticId { get; set; }

        public ICXXStatement()
        {
            StaticId = ++AstStructureHelper.currentStaticId; //debugging purpose
        }

        public abstract void PrettyPrint(XmlTextWriter xtw);
        //public abstract bool TryCreateStatement(string str);
    }
}
