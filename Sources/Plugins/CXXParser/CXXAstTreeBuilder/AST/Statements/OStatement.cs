﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Decls;

namespace CXXAstTreeBuilder.AST.Statements
{
    public class OStatement:ICXXStatement, IStatementWithReference
    {
        public OStatement()
        {
            ReferencedEntities = new List<Entity>();
            UnknownReferencedEntities = new List<KeyValuePair<string,string>>();
        }
        public List<Entity> ReferencedEntities { get; set; }
        public List<KeyValuePair<string,string>> UnknownReferencedEntities { get; set; }
        public override void PrettyPrint(XmlTextWriter xtw)
        {
            //xtw.WriteStartElement("OStatement");
            //xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            //xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            //if (ReferencedEntities.Any())
            //{
            //    xtw.WriteStartElement("Referenced");
            //    ReferencedEntities.ForEach(x =>
            //    {
            //        xtw.WriteStartElement("Reference");
            //        xtw.WriteAttributeString("Name", x.Name);
            //        xtw.WriteEndElement();
            //    });
            //    xtw.WriteEndElement();
            //}
            //if (UnknownReferencedEntities.Any())
            //{
            //    xtw.WriteStartElement("UnknownReferenced");
            //    UnknownReferencedEntities.ForEach(x =>
            //    {
            //        xtw.WriteStartElement("UnknownReference");
            //        xtw.WriteAttributeString("Name", x.Key);
            //        xtw.WriteAttributeString("Type", x.Value);
            //        xtw.WriteEndElement();
            //    });
            //    xtw.WriteEndElement();
            //}
            //xtw.WriteEndElement();
        }
    }
}
