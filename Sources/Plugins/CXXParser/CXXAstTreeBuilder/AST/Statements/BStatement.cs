﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using CXXAstTreeBuilder.AST.Scope;

namespace CXXAstTreeBuilder.AST.Statements
{
    public class BStatement:ICXXStatement
    {
        public List<OperationalScope> Scopes = new List<OperationalScope>();
        public bool NoBraces = true;
        public override void PrettyPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("BStatement");
            xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            xtw.WriteAttributeString("NoBraces", NoBraces.ToString());
            xtw.WriteEndElement();
        }
    }
}
