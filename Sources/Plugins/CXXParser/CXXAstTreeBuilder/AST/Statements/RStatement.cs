﻿using CXXAstTreeBuilder.AST.Decls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CXXAstTreeBuilder.AST.Statements
{
    public class RStatement:ICXXStatement, IStatementWithReference
    {
        public RStatement()
        {
            ReferencedEntities = new List<Entity>();
            UnknownReferencedEntities = new List<KeyValuePair<string, string>>();
        }
        public OStatement ParameterStatement { get; set; }
        public List<Entity> ReferencedEntities { get; set; }
        public string Type { get; set; }
        public List<KeyValuePair<string, string>> UnknownReferencedEntities { get; set; }
        public override void PrettyPrint(XmlTextWriter xtw)
        {
            //xtw.WriteStartElement("RStatement");
            //xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            //xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            //if (ReferencedEntities.Any())
            //{
            //    xtw.WriteStartElement("Referenced");
            //    ReferencedEntities.ForEach(x => x.Function != null? x.Function.PrettyPrint(xtw) : x.Variable.PrettyPrint(xtw));
            //    xtw.WriteEndElement();
            //}
            //if (UnknownReferencedEntities.Any())
            //{
            //    xtw.WriteStartElement("UnknownReferenced");
            //    UnknownReferencedEntities.ForEach(x =>
            //    {
            //        xtw.WriteStartElement("UnknownReference");
            //        xtw.WriteAttributeString("Name", x.Key);
            //        xtw.WriteAttributeString("Type", x.Value);
            //        xtw.WriteEndElement();
            //    });
            //    xtw.WriteEndElement();
            //}
            //xtw.WriteEndElement();
        }
    }
}
