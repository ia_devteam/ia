﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXXAstTreeBuilder.AST.Decls;

namespace CXXAstTreeBuilder.AST.Statements
{
    public class Entity
    {
        public FunctionDeclaration Function { get; set; }
        public VariableDeclaration Variable { get; set; }

        public Entity(IDeclaration decl)
        {
            if (decl is FunctionDeclaration) Function = (FunctionDeclaration)decl;
            if (decl is VariableDeclaration) Variable = (VariableDeclaration)decl;
        }

        public Entity() { }
    }
}
