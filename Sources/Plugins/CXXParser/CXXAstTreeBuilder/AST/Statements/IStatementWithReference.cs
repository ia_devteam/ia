﻿using CXXAstTreeBuilder.AST.Decls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXXAstTreeBuilder.AST.Statements
{
    public interface IStatementWithReference
    {
        List<Entity> ReferencedEntities { get; set; }
        List<KeyValuePair<string, string>> UnknownReferencedEntities { get; set; }
    }
}
