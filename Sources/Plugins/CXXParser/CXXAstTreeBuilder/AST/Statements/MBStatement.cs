﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Scope;

namespace CXXAstTreeBuilder.AST.Statements
{
    public class MBStatement:ICXXStatement
    {
        public List<OStatement> Conditions { get; set; }
        public List<BStatement> Blocks { get; set; }
        public string Type { get; set; }
        public bool hasError = false;
        public bool isEmpty = false;
        //public MultiBlockStatementScope OwnScope { get; set; }

        public MBStatement()
        {
            Conditions = new List<OStatement>();
            Blocks = new List<BStatement>();
        }

        public override void PrettyPrint(XmlTextWriter xtw)
        {
            //xtw.WriteStartElement("MBStatement");
            //xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            //xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            //if (Blocks.Any())
            //{
            //    xtw.WriteStartElement("Blocks");
            //    Blocks.ForEach(x => x.PrettyPrint(xtw));
            //    xtw.WriteEndElement();
            //}
            //if (Conditions.Any())
            //{
            //    xtw.WriteStartElement("Conditions");
            //    Conditions.ForEach(x => x.PrettyPrint(xtw));
            //    xtw.WriteEndElement();
            //}
            //if(OwnScope != null) OwnScope.PrettryPrint(xtw);
            //xtw.WriteEndElement();
        }
    }
}
