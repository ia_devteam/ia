﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXXAstTreeBuilder.AST.Statements;

namespace CXXAstTreeBuilder.AST.Location
{
    public class SourceFileLocation
    {
        public int Line { get; set; }
        public int Column { get; set; }
        private string _fileName;

        public override bool Equals(object obj)
        {
            var compare = (SourceFileLocation)obj;
            return FileName == compare.FileName && Line == compare.Line && Column == compare.Column;
        }

        public override int GetHashCode()
        {
            return Line * 10 + Column * 10 + FileName.Length;
        }

        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
                if (value.StartsWith("anonymous at "))
                {
                    _fileName = value.Replace("anonymous at ", "");
                }
            }
        }

        public bool IsOurFile { get; set; }

        public override string ToString()
        {
            return string.Format("{0}:{1}:{2}", FileName, Line, Column);
        }
    }
}
