﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using CXXAstTreeBuilder.AST.Statements;
using CXXAstTreeBuilder.AST.Location;

namespace CXXAstTreeBuilder.AST.Helpers
{
    public class AstStructureHelper
    {

        static string mainFile = "";
        static string currentFile = "";

        static int currentLine = -1;

        public static ParseLine ParseLine;
        public static int currentStaticId = 0;
        public static int unknownFile = 0;

        public static void InitStaticVars()
        {
            currentStaticId = 0;
            unknownFile = 0;
            ParseLine = new ParseLine();
        }

        public static void Initialize(string mainFile)
        {
            currentFile = AstStructureHelper.mainFile = mainFile;
        }
        public static bool IsAstTreeLine(string line)
        {
            return line.TrimStart().StartsWith("|") || line.TrimStart().StartsWith("`");
        }

        public static int GetLevel(string s)
        {
            for(int i = 0; i < s.Length; i++)
            {
                if (s[i] != ' ' && s[i] != '-' && s[i] != '|' && s[i] != '`') return i;
            }
            return 0;
        }

        public static List<SourceFileLocation> ExtractLocation(ref string s, Stack<FileNameIdent> nameStack, int clangLevel)
        {
            if (clangLevel == 3) return ExtractLocation3(ref s, nameStack);
            return ExtractLocation4(ref s, nameStack);
        }

        public static List<SourceFileLocation> ExtractLocation4(ref string s, Stack<FileNameIdent> nameStack)
        {
            try
            {
                if (s.Contains("<<<NULL>>>")  || s.IndexOf("<") == -1) return null;
                if(s.Contains("<invalid sloc>")) s = s.Replace("<invalid sloc>", "\0unknownfile" + (++unknownFile) + "\00\00");
                if (s.Contains("<scratch space>")) s = s.Replace("<scratch space>", "\0unknownfile" + (++unknownFile)/* + "\00\00"*/); //nameStack.Peek().FileName); 
                if (s.Contains("<built-in>")) s = s.Replace("<built-in>", "\0unknownfile" + (++unknownFile)/* + "\00\00"*/); //nameStack.Peek().FileName);
                if(s.Contains("<command line>")) s = s.Replace("<command line>", "\0unknownfile" + (++unknownFile)/* + "\00\00"*/);
                // избавляемся от отладочной разметки с 'line:', 'col:' удобно искать в AST через Notepad++ только и всего
                if (s.Contains("line:")) s = s.Replace("line:","");
                if (s.Contains("col:")) s = s.Replace("col:", "");

                string strRegex = @"<\0+(.+?)(?=\0+\d+\0+)\0+(\d+)\0+(\d+)\|?(\0+(.+?)(?=\0+\d+\0+)\0+(\d+)\0+(\d+)\|?)?(\0+(.+?)(?=\0+\d+\0+)\0+(\d+)\0+(\d+))?";
                Regex myRegex = new Regex(strRegex, RegexOptions.None);
                var ret = new List<SourceFileLocation>();
                var split = myRegex.Split(s);
                if (split.Length < 4)
                {
                    return null;
                }
                for (int i = 1; i < split.Length;)
                {
                    currentFile = split[i];
                    if (currentFile.Trim().StartsWith("anonymous ")) currentFile = currentFile.Substring(currentFile.IndexOf(" at ") + " at ".Length).Trim();
                    currentLine = Int32.Parse(split[i + 1]);
                    ret.Add(new SourceFileLocation() {Column = Int32.Parse(split[i + 2]), Line = currentLine, FileName = currentFile, IsOurFile = currentFile == mainFile });
                    if (i + 4 < split.Length) i += 4; else break;
                }

                if(ret.Count() == 1) ret.Add(ret[0]);
                if(ret.Count() == 3) ret.RemoveAt(2);

                return ret;
            }
            catch (Exception e)
            {
                //not a usual line - could be met at c++ decls
                return null;
            }
        }

        public static List<SourceFileLocation> ExtractLocation3(ref string s, Stack<FileNameIdent> nameStack)
        {
            try
            {
                if (s.Contains("<<<NULL>>>") || s.IndexOf("<") == -1) return null;
                if (s.Contains("<invalid sloc>")) s = s.Replace(", <invalid sloc>", "").Replace("<invalid sloc>,", "");
                if (s.Contains("<scratch space>")) s = s.Replace("<scratch space>", "unknownfile" + (++unknownFile)); //nameStack.Peek().FileName); 
                if (s.Contains("<built-in>")) s = s.Replace("<built-in>", "unknownfile" + (++unknownFile)); //nameStack.Peek().FileName);
                if (s.Contains("<command line>")) s = s.Replace("<command line>", "unknownfile" + (++unknownFile));

                var split = s.Split(new char[] { '<', '>' }).Where(g => !string.IsNullOrEmpty(g)).ToList();
                var locationPart = split[1];
                if (locationPart.IndexOf(":") == -1 || locationPart.IndexOf("::") > 0) return null;
                var indexOfDelimiter = locationPart.IndexOf(", ");
                if (indexOfDelimiter > 0)
                {
                    var part1 = locationPart.Substring(0, indexOfDelimiter);
                    var part2 = locationPart.Substring(indexOfDelimiter + 2);
                    var ret = new List<SourceFileLocation>() { ParsePart(part1), ParsePart(part2) };
                    for (int i = 0; i < 2; i++)
                    {
                        if (ret[i] != null && ret[i].FileName.Length > 2 && ret[i].FileName[1] == '#')
                        {
                            ret[i].FileName = ret[i].FileName[0].ToString() + ":" + ret[i].FileName.Substring(2);
                        }
                    }
                    //if(split.Count > 2) GetLastPart(split[2]);
                    if (ret.Count >= 1 && !IsValidPath(ret[0].FileName) || ret.Count == 2 && !IsValidPath(ret[0].FileName))
                    {
                        return null;
                    }
                    return ret;
                }
                var partResult = ParsePart(locationPart);
                if (partResult != null && partResult.FileName.Length > 2 && partResult.FileName[1] == '#')
                {
                    partResult.FileName = partResult.FileName[0].ToString() + ":" + partResult.FileName.Substring(2);
                }
                if (!IsValidPath(partResult.FileName)) return null;
                //if (split.Count > 2) GetLastPart(split[2]);
                return new List<SourceFileLocation>() { partResult, partResult };
            }
            catch (Exception e)
            {
                //not a usual line - could be met at c++ decls
                return null;
            }
        }

        private static bool IsValidPath(string path)
        {
            try
            {
                System.IO.Path.GetFullPath(path);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static SourceFileLocation ParsePart(string s)
        {
            if (s[1] == ':') s = s.Replace(":\\", "#\\");
            var split = s.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (split[0] == "line")
            {
                currentLine = Int32.Parse(split[1]);
                return new SourceFileLocation() { Line = Int32.Parse(split[1]), Column = Int32.Parse(split[2]), FileName = currentFile, IsOurFile = currentFile == mainFile };
            }
            if (split[0] == "col")
            {
                return new SourceFileLocation() { Line = currentLine, Column = Int32.Parse(split[1]), FileName = currentFile, IsOurFile = currentFile == mainFile };
            }
            if (currentFile != split[0] && IsValidPath(split[0]))
            {
                currentFile = split[0];
                if (!string.IsNullOrEmpty(AstParsePhase1._prefix) && currentFile.ToLower().StartsWith(AstParsePhase1._prefix)) currentFile = currentFile.Substring(AstParsePhase1._prefix.Length);
                if (currentFile.Trim().StartsWith("anonymous ")) currentFile = currentFile.Substring(currentFile.IndexOf(" at ") + " at ".Length).Trim();
                if (split.Count == 1)
                {
                    currentLine = 1;
                    return new SourceFileLocation() { Line = currentLine, Column = 1, FileName = currentFile, IsOurFile = currentFile == mainFile };
                }
            }

            currentLine = Int32.Parse(split[1]);
            return new SourceFileLocation() { Line = currentLine, Column = Int32.Parse(split[2]), FileName = currentFile, IsOurFile = currentFile == mainFile };
        }

        public static ICXXStatement GetLastStatement(ICXXStatement entry)
        {
            if (entry == null) return null;
            var last = entry;
            while (last.Next != null)
            {
                last = last.Next;
            }

            return last;
        }
    }

    public class FileNameIdent
    {
        public string FileName { get; set; }
        public int Ident { get; set; }
    }
}
