﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXXAstTreeBuilder.AST.Location;
using CXXAstTreeBuilder.AST.Statements;

namespace CXXAstTreeBuilder.AST.Helpers
{
    public class ParseLine
    {
        private string _type;
        private Int64? _reference;
        private int? _level;
        private string _retType;
        private string _name;

        public string GetType(string str)
        {
            if (_type != null) return _type;
            var split = str.Split(new[] {' ', '|', '`', '-'},StringSplitOptions.RemoveEmptyEntries);
            _type = split[0];
            return _type;
        }

        public int GetLevel(string str)
        {
            if (_level != null) return (int) _level;
            _level = AstStructureHelper.GetLevel(str);
            return (int)_level;
        }

        public Int64 GetReference(string str)
        {
            if (_reference != null) return (Int64)_reference;
            var ind = str.IndexOf("0x");
            if (ind == -1)
            {
                _reference = -1;
                return (Int64)_reference;
            }

            if (str.IndexOf(" prev 0x") != -1) ind = str.IndexOf(" prev 0x") + 6;
            int i = 0;
            for(i = ind+2; i < str.Length; i++) if(str[i] == ' ') break;
            _reference = Int64.Parse(str.Substring(ind + 2, i - ind - 2), System.Globalization.NumberStyles.HexNumber);
            return (Int64)_reference;
        }

        public string GetFunctionType(string str)
        {
            var ret = GetFunctionTypeImpl(str);
            if (ret != null)
            {
                if (ret.StartsWith("class "))
                {
                    ret = ret.Substring("class ".Length);
                } else if (ret.Contains(" class "))
                {
                    ret = ret.Replace(" class ", " ");
                } else if (ret.StartsWith("enum "))
                {
                    ret = ret.Replace("enum", "__IAENUM__");
                } else if (ret.StartsWith("struct "))
                {
                    ret = ret.Replace("struct", "__IASTRUCT__");
                } else if (ret.Contains("::class ")) ret = ret.Replace("::class ", "::");

                if (ret.StartsWith("union "))
                {
                    ret = ret.Replace("union", "__IAUNION__");
                }
            }
            if (ret != null && ret.Contains("type-parameter-"))
            {
                ret = null;
            }
            if (ret != null && _name != null && _name.StartsWith("operator"))
            {
                var len = "operator".Length;
                if (_name.Length == len + 1 || _name.Length == len + 2)
                {
                    ret = null; //no type convertion in that case
                }
            }
            return ret;
        }

        private string FixForType(string str, string ret, string type)
        {
            if (ret == type + " ")
            {
                //find closing brace after 'enum
                var typeInd = str.IndexOf("'" + type + " (");
                var braceInd = str.IndexOf(")", typeInd);
                var lastInd1 = str.IndexOf("'", braceInd);
                var lastInd2 = str.IndexOf("(", braceInd) != -1 ? str.IndexOf("(", braceInd) : Int32.MaxValue;
                ret = str.Substring(braceInd + 1, Math.Min(lastInd2, lastInd1) - braceInd - 1) + " ";
            }
            else
            {
                ret = ret.Substring((type + " ").Length);
            }
            return ret;
        }

        public string GetFunctionTypeImpl(string str)
        {
            if (_retType != null) return _retType;
            if (str.IndexOf("::(anonymous namespace)") != -1) str = str.Replace("::(anonymous namespace)", "");
            var indexOfName = str.IndexOf(" " + GetName(str) + " ");
            if (indexOfName == -1) return null; //TODO: FIXME 
            var indexOfType = str.IndexOf("'", indexOfName);
            if (indexOfType > 0 && indexOfType < str.Length - 2)
            {
                var endingOne = str.IndexOf("'", indexOfType + 1);
                var endingTwo = str.IndexOf("(", indexOfType + 1);
                endingOne = endingOne == -1 ? Int32.MaxValue : endingOne;
                endingTwo = endingTwo == -1 ? Int32.MaxValue : endingTwo;
                
                var type = str.Substring(indexOfType + 1, Math.Min(endingOne, endingTwo) - indexOfType - 1).TrimStart();
                /*if (str.IndexOf(") const'", indexOfType) != -1) //MG asked me to remove it with problems in phoenix-monitoring
                {
                    
                    if (!(type == "void" || 
                        type.StartsWith("class ") || 
                        type.StartsWith("const ") || 
                        type.StartsWith("void ")
                        ))
                    {
                        type = "const " + type;
                    }
                    else
                    {
                if (type.StartsWith("void ") && type != "void ")
                        {
                            type = type + "const ";
                        }
                    }
                }*/
                return _retType = type;
            }
            else
            {
                indexOfType = str.LastIndexOf(" '");
                if(indexOfType > 0)
                {
                    var endingOne = str.IndexOf("'", indexOfType + 2);
                    var endingTwo = str.IndexOf("(", indexOfType + 1);
                    endingOne = endingOne == -1 ? Int32.MaxValue : endingOne;
                    endingTwo = endingTwo == -1 ? Int32.MaxValue : endingTwo;
                    var type = str.Substring(indexOfType + 2, Math.Min(endingOne, endingTwo) - indexOfType - 2).TrimStart();
                    if (str.IndexOf(") const'", indexOfType) != -1 && !(type == "void" || type.StartsWith("class ") || type.StartsWith("const "))) type = "const " + type;
                    return _retType = type;
                }
            }

            return _retType = null;
        }

        public string GetName(string str)
        {
            var ret = GetNameImpl(str);
            if (ret != null)
            {
                if (ret.StartsWith("operator") && str.IndexOf(ret) > -1)
                {
                    var ind = str.IndexOf(ret);
                    var end = str.IndexOf(" ", ind + 1);
                    ret = str.Substring(ind, end - ind).Trim();
                }
                else
                {
                    var badNames = new List<string> { "class", "struct"};
                    foreach (var badName in badNames)
                    {
                        if (ret == badName)
                        {
                            var locator = string.Format(" {0} ", badName);
                            var posOfLocator = str.IndexOf(locator) + locator.Length;
                            var endPos = str.IndexOf(" ", posOfLocator);
                            if (endPos == -1) endPos = str.Length;
                            ret = str.Substring(posOfLocator, endPos - posOfLocator).Trim();
                        }
                    }

                }
                
            }

            return ret;
        }

        private string GetNameImpl(string str)
        {
            if (_name != null) return _name;
            //|-VarDecl 0x1d80370 <strace.c:158:1, col:20> current_tcp 'struct tcb *' static
            //if(GetType(str) == "DeclRefExpr") Debugger.Break();
            var split = str.Split(new[] { '<', '>' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length < 3) return _name = null;
            var namedPart = split[2].Trim() + " "; //so that no hack in 0x extraction
            if (split.Length > 3)
            {
                for (int i = 3; i < split.Length; i++)
                {
                    namedPart += split[i] + " ";
                }
            }
            if (namedPart.Contains(" 0x"))
            {
                if(str.IndexOf(" ", str.LastIndexOf(" 0x") + 1) > 0)
                {
                    //bane is after the hex digit
                    namedPart = str.Substring(str.IndexOf(" ", str.LastIndexOf(" 0x") + 1));
                    var splitAfterHex = namedPart.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    return _name = splitAfterHex[0].Replace("'", "").Trim();
                }
                else
                {
                    //col:7 implicit constexpr Rectangle 'void (const class Rectangle &)' inline noexcept-unevaluated 0xbbb496b2e8
                    //col:7 implicit ~Rectangle 'void (void)' inline noexcept-unevaluated 0xbbb496d8c8 
                    if (!namedPart.StartsWith("'"))
                    {
                        namedPart = namedPart.Substring(0, namedPart.IndexOf("'") - 1);
                        var splitToLast = namedPart.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        return _name = splitToLast[splitToLast.Count() - 1].Trim();
                    } else
                    {
                        //' bound member function type ' .c_str 0xac64d16fa0 
                        var splitToLast = namedPart.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        return _name = splitToLast[splitToLast.Count() - 2].Trim();
                    }


                }
            }


            var namedPartSplit = namedPart.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            if(namedPartSplit.Count > 2 && namedPartSplit[1].Trim() == "used") namedPartSplit.RemoveAt(1);
            if (namedPartSplit[0].Contains("line:") || namedPartSplit[0].Contains("col:")) namedPartSplit.RemoveAt(0);
            namedPart = String.Join(" ", namedPartSplit);
            if (namedPartSplit[0].Contains("'"))
            {
                if (namedPartSplit.Count > 1)
                {
                    //'int (FILE *, const char *, ...)' Function 0x1b30c60 'fprintf' 'int (FILE *, const char *, ...)'
                    var difficult = namedPartSplit[namedPartSplit.Count - 2];
                    var difficultUnmangled = difficult.Replace("'", "");
                    if (difficultUnmangled.Length + 2 == difficult.Length) return difficultUnmangled;
                    return _name = null;
                }

                //unknown
                return _name = null;
            }

            var name = namedPart.Split(' ')[0];
            return _name = name.Trim();
        }
    }
}
