﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Scope;
using CXXAstTreeBuilder.AST.Table;
using Store;

namespace CXXAstTreeBuilder.AST.Decls
{
    public class ClassDeclaration:GlobalScope, IDeclaration
    {
        public string Name { get; set; }
        public void PrettyPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("ClassDeclaration");
            xtw.WriteAttributeString("ReferenceId", ReferenceId.ToString());
            Body.PrettryPrint(xtw);
            xtw.WriteEndElement();
        }

        public Int64 ReferenceId { get; set; }
        public ClassScope Body { get; set; }
        public UInt64? StorageClass { get; set; }
        public ClassDeclaration(Int64 referenceId)
        {
            this.ReferenceId = referenceId;
            ReferenceTable.Instance.Table.Add(referenceId, this);
        }

        public ClassDeclaration() { }
    }
}
