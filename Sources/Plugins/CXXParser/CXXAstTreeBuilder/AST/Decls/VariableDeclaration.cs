﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Table;
using Store;

namespace CXXAstTreeBuilder.AST.Decls
{
    public class VariableDeclaration:IDeclaration
    {
        public Location.SourceFileLocation Start { get; set; }

        public Location.SourceFileLocation End { get; set; }
        public string Name { get; set; }
        public Int64 ReferenceId { get; set; }
        public UInt64? StorageVariable { get; set; }

        public VariableDeclaration(Int64 referenceId)
        {
            this.ReferenceId = referenceId;
            if(!ReferenceTable.Instance.Table.ContainsKey(referenceId))//BAD HACK TO REMOVE
            ReferenceTable.Instance.Table.Add(referenceId, this);
        }

        public VariableDeclaration() { }
        public void PrettyPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("VariableDeclaration");
            xtw.WriteAttributeString("ReferenceId", ReferenceId.ToString());
            xtw.WriteAttributeString("Name", Name.ToString());
            xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            xtw.WriteEndElement();
        }
    }
}
