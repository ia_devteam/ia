﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Location;
namespace CXXAstTreeBuilder.AST.Decls
{
    public interface IDeclaration
    {
        SourceFileLocation Start { get; set; }
        SourceFileLocation End { get; set; }
        string Name { get; set; }

        void PrettyPrint(XmlTextWriter xtw);
    }
}
