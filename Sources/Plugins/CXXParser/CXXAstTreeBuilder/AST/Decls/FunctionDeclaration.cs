﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using CXXAstTreeBuilder.AST.Location;
using CXXAstTreeBuilder.AST.Scope;
using CXXAstTreeBuilder.AST.Table;
using Store;

namespace CXXAstTreeBuilder.AST.Decls
{
    public class FunctionDeclaration:FunctionScope, IDeclaration
    {
        public string Name { get; set; }
        public Int64 ReferenceId { get; set; }
        public UInt64? StorageFunction { get; set; }
        public string ReturnType { get; set; }
        public List<Definition> Definitions { get; set; }
        public bool ToSkip { get; set; }
        public bool IsTemplate { get; set; }
        public FunctionDeclaration(Int64 referenceId, bool toSkip)
        {
            this.ReferenceId = referenceId;
            ToSkip = toSkip;
            IsTemplate = AstParsePhase1.inTemplateDecl;
            Definitions = new List<Definition>();
            ReferenceTable.Instance.Table.Add(referenceId, this);
        }

        public FunctionDeclaration() { }

        public void PrettyPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("FunctionDeclaration");
            xtw.WriteAttributeString("ReferenceId", ReferenceId.ToString());
            xtw.WriteAttributeString("Name", Name.ToString());
            xtw.WriteAttributeString("ReturnType", ReturnType?.ToString() ?? ""); 
            Body.PrettryPrint(xtw);
            xtw.WriteEndElement();
        }
    }

    public class Definition
    {
        public SourceFileLocation Start { get; set; }
        public Definition(List<SourceFileLocation> locs)
        {
            if (locs.Count > 0)
            {
                Start = locs[0];
            }
        }

        public Definition(FunctionDeclaration decl)
        {
            Start = decl.Start;
        }

        public Definition()
        {
        }
    }

}
