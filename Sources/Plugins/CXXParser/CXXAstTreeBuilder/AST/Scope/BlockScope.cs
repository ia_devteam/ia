﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Statements;
using CXXAstTreeBuilder.AST.Helpers;
using CXXAstTreeBuilder.AST.Decls;
using CXXAstTreeBuilder.AST.Table;
using CXXAstTreeBuilder.AST.Location;

namespace CXXAstTreeBuilder.AST.Scope
{
    public class BlockScope : AbstractScope
    {
        int lastLevelOStatement = Int32.MaxValue;
        public override void TryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var level = AstStructureHelper.ParseLine.GetLevel(str);
            var type = AstStructureHelper.ParseLine.GetType(str);
            var lastStatement = AstStructureHelper.GetLastStatement(Entry);
            switch (type)
            {
                case "DeclStmt":
                {
                    //var varDecl = new VariableDeclaration() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null};
                    //Variables.Add(varDecl);
                    lastStatement.Next = new DStatement() {Start = locs[0], End = locs.Count == 2 ? locs[1] : null};
                    break;
                }
                case "VarDecl":
                {
                    var varDecl = new VariableDeclaration(AstStructureHelper.ParseLine.GetReference(str))
                    {
                        Start = locs[0],
                        End = locs.Count == 2 ? locs[1] : null,
                        Name = AstStructureHelper.ParseLine.GetName(str)
                    };
                    Variables.Add(varDecl);
                    if (lastStatement is DStatement)
                    {
                        var dStatement = lastStatement as DStatement;
                        dStatement.Declaration.Add(new Entity(varDecl));
                        //if (locs.Count == 2) dStatement.End = locs[1];
                    }
                    else
                    {
                        var dStatement = new DStatement() {Start = locs[0], End = locs.Count == 2 ? locs[1] : null};
                        lastStatement.Next = dStatement;
                        dStatement.Declaration.Add(new Entity(varDecl));
                    }
                    break;
                }
                case "CXXTryStmt":
                case "SwitchStmt":
                case "CXXForRangeStmt":
                case "ForStmt":
                case "DoStmt":
                case "WhileStmt":
                case "IfStmt":
                {
                    if (locs.Count == 2 && locs[0].FileName != locs[1].FileName) locs.RemoveAt(1);
                    var ifStatement = new MBStatement()
                    {
                        Start = locs[0],
                        End = locs.Count == 2 ? locs[1] : null,
                        Type = type
                    };
                    if (lastStatement == null)
                    {
                        Entry = lastStatement = ifStatement;
                    }
                    else
                    {
                        lastStatement.Next = ifStatement;
                    }

                    var ifScope = new MultiBlockStatementScope()
                    {
                        Start = locs[0],
                        End = locs.Count == 2 ? locs[1] : null,
                        Type = type,
                        Level = AstStructureHelper.ParseLine.GetLevel(str)
                    };
                    ifScope.Statement = ifStatement;
                    //ifStatement.OwnScope = ifScope;

                    stackReference.Push(ifScope);
                    break;
                }
                case "CXXDeleteExpr":
                case "CStyleCastExpr":
                case "CompoundAssignOperator":
                case "BinaryOperator":
                case "UnaryOperator":
                case "CXXOperatorCallExpr":
                case "CXXMemberCallExpr":
                case "CallExpr":
                case "MemberExpr":
                case "DeclRefExpr":
                case "ParenExpr":
                case "CXXNewExpr":
                case "GCCAsmStmt":
                {
                    //if (locs.Count == 1 || locs[0].FileName == locs[1].FileName)
                    //{
                    //    if (lastStatement == null)
                    //    {
                    //        Entry = lastStatement = new OStatement() {Start = locs[0], End = locs.Count == 2 ? locs[1] : null};
                    //        lastLevelOStatement = level;
                    //    }
                    //    else
                    //    {
                    //        if (!(lastStatement is OStatement) || lastStatement.Start.Line > locs[0].Line || lastStatement.Start.FileName != locs[0].FileName)
                    //        {
                    //            if(lastStatement is OStatement && level > lastLevelOStatement) break;
                    //            lastLevelOStatement = level;
                    //            lastStatement.Next = new OStatement()
                    //            {
                    //                Start = locs[0],
                    //                End = locs.Count == 2 ? locs[1] : null
                    //            };
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    //locs[0].FileName != locs[1].FileName
                    //    if (lastStatement == null)
                    //    {
                    //        Entry = lastStatement = new OStatement() { Start = locs[0], End = locs[0] };
                    //        lastLevelOStatement = level;
                    //        lastStatement.Next = new OStatement()
                    //        {
                    //            Start = locs[1],
                    //            End = locs[1]
                    //        };
                    //    }
                    //    else
                    //    {
                    //        if (lastStatement is OStatement && lastStatement.End != null && lastStatement.End.FileName == locs[0].FileName && lastStatement.Start.Line < locs[0].Line)
                    //        {
                    //            lastStatement.End = locs[0];
                    //        }

                    //        if (!(lastStatement is OStatement) || lastStatement.Start.Line > locs[0].Line )
                    //        {
                    //            if (lastStatement is OStatement && level > lastLevelOStatement) break;
                    //            lastLevelOStatement = level;
                    //            lastStatement.Next = new OStatement()
                    //            {
                    //                Start = locs[0],
                    //                End =  locs[0]
                    //            };
                    //            break; //?
                    //        }

                    //        if (lastStatement is OStatement && level > lastLevelOStatement) break;
                    //        lastLevelOStatement = level;
                    //        lastStatement.Next = new OStatement()
                    //        {
                    //            Start = locs[1],
                    //            End = locs[1]
                    //        };
                    //    }
                    //}
                    if (lastStatement == null)
                    {
                        Entry = lastStatement = new OStatement() {Start = locs[0], End = locs.Count == 2 ? locs[1] : null};
                        lastLevelOStatement = level;
                    }
                    else
                    {
                        lastStatement = lastStatement.Next = new OStatement()
                        {
                            Start = locs[0],
                            End = locs.Count == 2 ? locs[1] : null
                        };
                    }
                    var opScop = new OperationalScope { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = level, OperationalStatement = (OStatement)lastStatement };
                    stackReference.Push(opScop);
                    break;
                }
                //case "MemberExpr":
                //case "DeclRefExpr":
                //{
                //    if (!(lastStatement is IStatementWithReference))
                //    {
                //            //throw new Exception("Unknown or wrong context");
                //            if (lastStatement == null)
                //            {
                //                Entry = lastStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                //            }
                //            else
                //            {
                //                if (!(lastStatement is OStatement))
                //                {
                //                    lastStatement.Next = new OStatement()
                //                    {
                //                        Start = locs[0],
                //                        End = locs.Count == 2 ? locs[1] : null
                //                    };
                //                    lastStatement = lastStatement.Next;
                //                }
                //                else
                //                {
                //                    //if (locs.Count == 2)
                //                    //{
                //                    //    lastStatement.End = locs[1];
                //                    //}
                //                }
                //            }
                //        }
                //    var reference = AstStructureHelper.ParseLine.GetReference(str);
                //    if (ReferenceTable.Instance.Table.ContainsKey(reference))
                //    {
                //        (lastStatement as IStatementWithReference).ReferencedEntities.Add(
                //            new Entity(ReferenceTable.Instance.Table[reference]));
                //    }
                //    else
                //    {
                //        var referenceType = str.ToLower().Contains(" function ") ? "Function" : "Var";
                //        (lastStatement as IStatementWithReference).UnknownReferencedEntities.Add(new KeyValuePair<string, string>(AstStructureHelper.ParseLine.GetName(str), referenceType));
                //    }
                //    break;
                //}
                case "CXXThrowExpr":
                case "ContinueStmt":
                case "BreakStmt":
                case "ReturnStmt":
                case "GotoStmt":
                {
                    if (locs.Count == 2 && locs[1].FileName != locs[0].FileName) locs.RemoveAt(1); //need to search for ; manually
                    if (lastStatement == null)
                    {
                        Entry = lastStatement = new RStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Type = type};
                    }
                    else
                    {
                        lastStatement.Next = new RStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Type = type };
                    }
                    if (type == "CXXThrowExpr" || type == "ReturnStmt")
                    {
                        var oStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                        var opScop = new OperationalScope { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = level, OperationalStatement = oStatement };
                        stackReference.Push(opScop);
                        ((lastStatement.Next ?? lastStatement) as RStatement).ParameterStatement = oStatement;
                    }
                    break;
                }
                default:
                {
                    //if(str.Contains("Stmt ")) Console.WriteLine(str);
                    break;
                }
            }

            if (locs.Count == 2 && lastStatement != null)
            {
                if (lastStatement.Next != null) lastStatement = lastStatement.Next;
                if (locs[1].FileName == lastStatement.Start.FileName)
                {
                    //we are talking about the same file
                    if (lastStatement.End == null && CompareLocs(lastStatement.Start, locs[0]) == 1)
                    {
                        lastStatement.End = locs[1];
                    }
                    else
                    {
                        if(lastStatement.End != null && CompareLocs(lastStatement.End, locs[1]) == 1)
                        {
                            lastStatement.End = locs[1];
                        }
                    }
                }

                //if (locs[1].FileName == lastStatement.Start.FileName && (locs[1].Line > lastStatement.Start.Line || locs[1].Line == lastStatement.Start.Line && locs[1].Column > lastStatement.Start.Column)) lastStatement.End = locs[1];
            }
        }

        public override void PrettryPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("BlockScope");
            xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            if (Entry != null)
            {
                var iter = Entry;

                do
                {
                    iter.PrettyPrint(xtw);
                } while ((iter = iter.Next) != null);
            }
            xtw.WriteEndElement();
        }

        private int CompareLocs(SourceFileLocation first, SourceFileLocation second)
        {
            if (first.FileName != second.FileName) return 0;
            if (first.Line > second.Line || first.Line == second.Line && first.Column > second.Column) return 0;
            return 1;
        }
    }
}
