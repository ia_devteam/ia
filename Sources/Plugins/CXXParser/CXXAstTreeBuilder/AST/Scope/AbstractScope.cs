﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Statements;
using CXXAstTreeBuilder.AST.Location;
using CXXAstTreeBuilder.AST.Decls;

namespace CXXAstTreeBuilder.AST.Scope
{
    public abstract class AbstractScope
    {
        public abstract void TryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locations);
        public ICXXStatement Entry { get; set; }
        public int Level { get; set; }
        public SourceFileLocation Start { get; set; }
        public SourceFileLocation End { get; set; }
        public List<FunctionDeclaration> Functions { get; set; }
        public List<VariableDeclaration> Variables { get; set; }
        public List<ClassDeclaration> Classes { get; set; }

        public abstract void PrettryPrint(XmlTextWriter xtw);

        public AbstractScope()
        {
            Functions = new List<FunctionDeclaration>();
            Variables = new List<VariableDeclaration>();
            Classes = new List<ClassDeclaration>();
        }
    }
}
