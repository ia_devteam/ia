﻿using CXXAstTreeBuilder.AST.Decls;
using CXXAstTreeBuilder.AST.Helpers;
using CXXAstTreeBuilder.AST.Location;
using CXXAstTreeBuilder.AST.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CXXAstTreeBuilder.AST.Scope
{
    public class ClassScope:GlobalScope
    {
        public override void TryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            if (locs == null || !locs.Any(g => g.IsOurFile)) return; // we don't need anything that is not mapped to our file
            var type = AstStructureHelper.ParseLine.GetType(str);
            var referenceId = AstStructureHelper.ParseLine.GetReference(str);
            switch (type)
            {
                case "FieldDecl":
                {

                    if (referenceId != -1 && !ReferenceTable.Instance.Table.ContainsKey(referenceId))
                    {
                        var varDecl = new VariableDeclaration(referenceId) {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Name = AstStructureHelper.ParseLine.GetName(str)};
                        Variables.Add(varDecl);
                    }
                    break;
                }
                case "CXXDestructorDecl":
                case "CXXConstructorDecl":
                case "CXXMethodDecl":
                {
                    if (ReferenceTable.Instance.Table.ContainsKey(referenceId))
                    {
                        var decl = (ReferenceTable.Instance.Table[referenceId] as FunctionDeclaration);
                        var body = (ReferenceTable.Instance.Table[referenceId] as FunctionDeclaration).Body;
                        if (body.Entry == null)
                        {
                            decl.Definitions.Add(new Definition(decl));
                            decl.Start = locs[0];
                            decl.End = locs[1] ?? null;
                            var functionScope = new FunctionScope() { Start = locs[0], End = locs[1] ?? null, Level = AstStructureHelper.ParseLine.GetLevel(str) + 1 };
                            (ReferenceTable.Instance.Table[referenceId] as FunctionDeclaration).Body = functionScope.Body;
                            stackReference.Push(functionScope);
                        }
                        else
                        {
                            decl.Definitions.Add(new Definition(locs));
                            var duplicateScope = new DuplicateScope() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = AstStructureHelper.ParseLine.GetLevel(str) + 1 };
                            stackReference.Push(duplicateScope);
                        }
                    }
                    else
                    {
                        // Избавляемся от вставки в struct E:A{ E()=default;Sensor(..); // explicitly defaulted, calls A::A() };
                        bool IsSkip = str.Contains(" inline ") || str.EndsWith(" inline") || str.EndsWith(" default") 
                            || (locs[0].Line == locs[1].Line && locs[0].Column == locs[1].Column);
                        var funDecl = new FunctionDeclaration(AstStructureHelper.ParseLine.GetReference(str), IsSkip)
                        { Start = locs[0], End = locs[1], Name = AstStructureHelper.ParseLine.GetName(str),
                        ReturnType = AstStructureHelper.ParseLine.GetFunctionType(str)};
                        Functions.Add(funDecl);
                        var functionScope = new FunctionScope() {Start = locs[0], End = locs[1], Level = AstStructureHelper.ParseLine.GetLevel(str) + 1};
                        funDecl.Body = functionScope.Body;
                        stackReference.Push(functionScope);
                    }
                    break;
                }
                case "CXXRecordDecl":
                {
                    if (referenceId != -1 && !ReferenceTable.Instance.Table.ContainsKey(referenceId))
                    {
                        var classDecl = new ClassDeclaration(referenceId) {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Name = AstStructureHelper.ParseLine.GetName(str)};
                        Classes.Add(classDecl);
                        var classScope = new ClassScope() {Start = locs[0], End = locs[1], Level = AstStructureHelper.ParseLine.GetLevel(str) + 1};
                        classDecl.Body = classScope;
                        stackReference.Push(classScope);
                    }
                    else
                    {
                        stackReference.Push((ReferenceTable.Instance.Table[referenceId] as ClassDeclaration).Body);
                    }
                    break;
                }
                default:
                    break;
            }
        }
        protected override void PrettyPrintType(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("ClassScope");
        }
    }
}
