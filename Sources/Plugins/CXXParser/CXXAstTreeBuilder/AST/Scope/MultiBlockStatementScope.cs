﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Helpers;
using CXXAstTreeBuilder.AST.Statements;
using CXXAstTreeBuilder.AST.Decls;
using CXXAstTreeBuilder.AST.Table;
using CXXAstTreeBuilder.AST.Location;

namespace CXXAstTreeBuilder.AST.Scope
{
    public class MultiBlockStatementScope: AbstractScope
    {
        public string Type { get; set; }
        public ICXXStatement Statement { get; set; }

        public List<OperationalScope> ConditionScopes { get; set; } 
        public List<BlockScope> BlockScopes { get; set; }
        
        private bool isFirstPass = true;
        private int hitCount = 0;

        public MultiBlockStatementScope()
        {
            ConditionScopes = new List<OperationalScope>();
            BlockScopes = new List<BlockScope>();
        }

        public override void TryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            if (AstStructureHelper.ParseLine.GetType(str) == "LabelStmt") return;
            switch (Type)
            {
                case "WhileStmt":
                {
                    WhileStmtTryPutInScope(str, stackReference, locs);
                    break;
                }
                case "IfStmt":
                {
                    IfStmtTryPutInScope(str, stackReference, locs);
                    break;
                }
                case "DoStmt":
                {
                    DoStmtTryPutInScope(str, stackReference, locs);
                    break;
                }
                case "ForStmt":
                {
                    ForStmtTryPutInScope(str, stackReference, locs);
                    break;
                }
                case "CXXForRangeStmt":
                {
                    BigHeadStmtTryPutInScope(str, stackReference, locs);
                    break;
                }
                case "SwitchStmt":
                {
                    SwitchStmtTryPutInScope(str, stackReference, locs);
                    break;
                }
                case "CXXTryStmt":
                {
                    TryStmtTryPutInScope(str, stackReference, locs);
                    break;
                }
            }
        }

        private void DoStmtTryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var level = AstStructureHelper.ParseLine.GetLevel(str);
            var type = AstStructureHelper.ParseLine.GetType(str);
            var mbStatement = Statement as MBStatement;
            hitCount++;
            if (hitCount == 2)
            {
                var oStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                mbStatement.Conditions = new List<OStatement>() { oStatement };
                var opScop = new OperationalScope { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, OperationalStatement = oStatement };
                stackReference.Push(opScop);
                ConditionScopes.Add(opScop);
                return;
            }
            var bStatement = new BStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, NoBraces = type != "CompoundStmt" };
            if (mbStatement.Blocks == null) mbStatement.Blocks = new List<BStatement>();
            mbStatement.Blocks.Add(bStatement);
            var bScop = new BlockScope() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, Entry = bStatement };
            stackReference.Push(bScop);
            BlockScopes.Add(bScop);
            if (bStatement.NoBraces)
            {
                bScop.TryPutInScope(str, stackReference, locs);
            }
        }

        private void WhileStmtTryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var type = AstStructureHelper.ParseLine.GetType(str);
            var mbStatement = Statement as MBStatement;
            hitCount++;
            if (hitCount == 1)
            {
                var oStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                mbStatement.Conditions = new List<OStatement>() { oStatement };
                var opScop = new OperationalScope { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, OperationalStatement = oStatement };
                stackReference.Push(opScop);
                ConditionScopes.Add(opScop);
            } else if (!str.Contains("`-"))
            {
                var oldCondition = ConditionScopes[0];
                stackReference.Push(oldCondition);
                oldCondition.TryPutInScope(str, stackReference, locs);
            }
            else
            {
                var bStatement = new BStatement() {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, NoBraces = type != "CompoundStmt"};
                if (mbStatement.Blocks == null) mbStatement.Blocks = new List<BStatement>();
                mbStatement.Blocks.Add(bStatement);
                var bScop = new BlockScope() {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, Entry = bStatement};
                stackReference.Push(bScop);
                BlockScopes.Add(bScop);
                if (bStatement.NoBraces)
                {
                    bScop.TryPutInScope(str, stackReference, locs);
                }
            }
        }
        bool firstWasDecl = false;

        private void IfStmtTryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var type = AstStructureHelper.ParseLine.GetType(str);
            var mbStatement = Statement as MBStatement;
            hitCount++;
            if(mbStatement.hasError) return;
            if (hitCount == 4)
            {
                var opFromFirst = mbStatement.Blocks[0].Next as OStatement;
                if (opFromFirst == null)
                {
                    mbStatement.hasError = true;
                    return;
                }
                opFromFirst.ReferencedEntities.ForEach(g => mbStatement.Conditions[0].ReferencedEntities.Add(g));
                opFromFirst.UnknownReferencedEntities.ForEach(g => mbStatement.Conditions[0].UnknownReferencedEntities.Add(g));
                mbStatement.Blocks.RemoveAt(0);
                BlockScopes.RemoveAt(0);
            }
            if (isFirstPass)
            {
                isFirstPass = false;
                firstWasDecl = type == "DeclStmt";
                var oStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                mbStatement.Conditions = new List<OStatement>() { oStatement };
                if (type != "CompoundStmt")
                {
                    var opScop = new OperationalScope {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, OperationalStatement = oStatement};
                    stackReference.Push(opScop);
                    //opScop.TryPutInScope(str, stackReference);
                    ConditionScopes.Add(opScop);
                    return;
                }
            } else if (hitCount == 2 && firstWasDecl)
            {
                var opScop = ConditionScopes.Last();
                opScop.Level++;
                opScop.TryPutInScope(str, stackReference, locs);
                stackReference.Push(opScop);
                hitCount--;
                firstWasDecl = false;
                return;
            }
            var bStatement = new BStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, NoBraces = type != "CompoundStmt" };
            if(mbStatement.Blocks == null) mbStatement.Blocks = new List<BStatement>();
            mbStatement.Blocks.Add(bStatement);
            var bScop = new BlockScope() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, Entry = bStatement};
            stackReference.Push(bScop);
            BlockScopes.Add(bScop);
            if (bStatement.NoBraces)
            {
                bScop.TryPutInScope(str, stackReference, locs);
            }
        }

        private void ForStmtTryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var level = AstStructureHelper.ParseLine.GetLevel(str);
            var type = AstStructureHelper.ParseLine.GetType(str);
            var mbStatement = Statement as MBStatement;
            if (str.IndexOf(" `-") == -1)
            {
                var oStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                mbStatement.Conditions.Add(oStatement);
                var opScop = new OperationalScope { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, OperationalStatement = oStatement };
                stackReference.Push(opScop);
                ConditionScopes.Add(opScop);
            }
            else
            {
                var bStatement = new BStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, NoBraces = type != "CompoundStmt" };
                mbStatement.Blocks.Add(bStatement);
                var bScop = new BlockScope() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, Entry = bStatement };
                stackReference.Push(bScop);
                BlockScopes.Add(bScop);
                if (bStatement.NoBraces)
                {
                    bScop.TryPutInScope(str,stackReference, locs);
                }
            }
        }

        private void BigHeadStmtTryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var level = AstStructureHelper.ParseLine.GetLevel(str);
            var type = AstStructureHelper.ParseLine.GetType(str);
            var mbStatement = Statement as MBStatement;
            if (ConditionScopes.Count == 0)
            {
                var oStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                mbStatement.Conditions.Add(oStatement);
                var opScop = new OperationalScope { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, OperationalStatement = oStatement };
                ConditionScopes.Add(new OperationalScope());
            }
            if (str[Level] == '`')
            {
                var bStatement = new BStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, NoBraces = type != "CompoundStmt" };
                mbStatement.Blocks.Add(bStatement);
                var bScop = new BlockScope() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = this.Level + 2, Entry = bStatement };
                stackReference.Push(bScop);
                BlockScopes.Add(bScop);
                if (bStatement.NoBraces)
                {
                    bScop.TryPutInScope(str, stackReference, locs);
                }
                return;
            }
            switch (type)
            {
                case "VarDecl":
                {
                    var varDecl = new VariableDeclaration(AstStructureHelper.ParseLine.GetReference(str))
                    {
                        Start = locs[0],
                        End = locs.Count == 2 ? locs[1] : null,
                        Name = AstStructureHelper.ParseLine.GetName(str)
                    };
                    Variables.Add(varDecl);
                    
                    break;
                }
                case "DeclRefExpr":
                {
                    var reference = AstStructureHelper.ParseLine.GetReference(str);
                    if (ReferenceTable.Instance.Table.ContainsKey(reference))
                    {
                        mbStatement.Conditions[0].ReferencedEntities.Add(
                            new Entity(ReferenceTable.Instance.Table[reference]));
                    }
                    else
                    {
                            var referenceType = str.ToLower().Contains(" function ") ? "Function" : "Var";
                            mbStatement.Conditions[0].UnknownReferencedEntities.Add(new KeyValuePair<string, string>(AstStructureHelper.ParseLine.GetName(str), referenceType));
                    }
                    break;
                }
            }
        }

        private bool metCase = false;
        private bool metCompound = false;
        private bool inBlock = false;
        private bool metLastCaseAstConstruct = false;
        private bool nextIsCaseOp = false;
        private void SwitchStmtTryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var level = AstStructureHelper.ParseLine.GetLevel(str);
            var type = AstStructureHelper.ParseLine.GetType(str);
            var mbStatement = Statement as MBStatement;
            
            if (isFirstPass)
            {
                mbStatement.isEmpty = true;
                isFirstPass = false;
                var oStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                mbStatement.Conditions = new List<OStatement>() { oStatement };
                var opScop = new OperationalScope { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = level, OperationalStatement = oStatement };
                stackReference.Push(opScop);
                ConditionScopes.Add(opScop);
                return;
            }

            if (type == "CompoundStmt" && !metCompound)
            {
                metCompound = true;
                return;
            }

            if (type == "CaseStmt")
            {
                metCompound = true;
                metCase = true;
                nextIsCaseOp = true;
                return;
            }

            if (nextIsCaseOp && AstParsePhase1.prevWasNull) nextIsCaseOp = false;

            if (nextIsCaseOp || type == "DefaultStmt")
            {
                nextIsCaseOp = false;
                var oStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                var opScop = new OperationalScope { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = level, OperationalStatement = oStatement };
                ConditionScopes.Add(opScop);
                if (type != "DefaultStmt")
                {
                    // next is OP
                    stackReference.Push(opScop);
                }
                else
                {
                    //next is STMT
                    metCase = true;
                }
                return;
            }

            if (!metCompound || !metCase && !BlockScopes.Any() /*Overloaded function test */) //check when it is true????
            {
                stackReference.Push(ConditionScopes[0]);
                stackReference.Last().TryPutInScope(str, stackReference, locs);
                return;
            }

            //everything is a block
            if (metCase)
            {
                metCase = false;
                mbStatement.isEmpty = false;
                var bStatement = new BStatement()
                {
                    Start = locs[0],
                    End = locs.Count == 2 ? locs[1] : null,
                    NoBraces = false//type != "CompoundStmt"
                };
                if (mbStatement.Blocks == null) mbStatement.Blocks = new List<BStatement>();
                mbStatement.Blocks.Add(bStatement);
                var bScop = new BlockScope()
                {
                    Start = locs[0],
                    End = locs.Count == 2 ? locs[1] : null,
                    Level = level,
                    Entry = bStatement
                };
                stackReference.Push(bScop);
                BlockScopes.Add(bScop);
                if (type != "CompoundStmt")
                {
                    bScop.TryPutInScope(str, stackReference, locs);
                }
                return;
            }

            var lastBScope = BlockScopes.Last();
            lastBScope.Level = level;
            if (locs.Count == 2 && lastBScope.End != null
                && lastBScope.End.FileName == locs[1].FileName
                && lastBScope.End.Line <= locs[1].Line)
            {
                lastBScope.End = locs[1];
                lastBScope.Entry.End = locs[1];
            }

            stackReference.Push(lastBScope);
            lastBScope.TryPutInScope(str, stackReference, locs);


            //if (type == "DefaultStmt")
            //{
            //    metCompound = true;
            //    metLastCaseAstConstruct = false;
            //    mbStatement.isEmpty = false;
            //    inBlock = false;
            //    var oStatement = new OStatement() {Start = locs[0], End = locs.Count == 2 ? locs[1] : null};
            //    var opScop = new OperationalScope {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = level + 2, OperationalStatement = oStatement};
            //    ConditionScopes.Add(opScop);
            //    return;
            //}

            //if (!metCompound & !str.Contains(" `-"))
            //{
            //    stackReference.Push(ConditionScopes[0]);
            //    stackReference.Last().TryPutInScope(str, stackReference, locs);
            //    return;
            //}

            //if (str.Contains(" `-") && (level <= ConditionScopes.Last().Level || metLastCaseAstConstruct || type.EndsWith("Stmt") /*|| type.EndsWith("Operator")*/ || AstParsePhase1.prevWasNull) || metCase && level < ConditionScopes.Last().Level)
            //{
            //    inBlock = true;
            //    metCase = false;
            //    mbStatement.isEmpty = false;
            //    var bStatement = new BStatement()
            //    {
            //        Start = locs[0],
            //        End = locs.Count == 2 ? locs[1] : null,
            //        NoBraces = false//type != "CompoundStmt"
            //    };
            //    if (mbStatement.Blocks == null) mbStatement.Blocks = new List<BStatement>();
            //    mbStatement.Blocks.Add(bStatement);
            //    var bScop = new BlockScope()
            //    {
            //        Start = locs[0],
            //        End = locs.Count == 2 ? locs[1] : null,
            //        Level = level,
            //        Entry = bStatement
            //    };
            //    stackReference.Push(bScop);
            //    BlockScopes.Add(bScop);
            //    if (type != "CompoundStmt")
            //    {
            //        bScop.TryPutInScope(str, stackReference, locs);
            //    }
            //}
            //else
            //{
            //    if (!inBlock)
            //    {
            //        ConditionScopes.Last().TryPutInScope(str, stackReference, locs);
            //        metLastCaseAstConstruct = str.Contains(" `-");
            //    }
            //    else
            //    {
            //        metLastCaseAstConstruct = false;
            //        var lastBScope = BlockScopes.Last();
            //        lastBScope.Level = level;
            //        if (locs.Count == 2 && lastBScope.End != null
            //            && lastBScope.End.FileName == locs[1].FileName
            //            && lastBScope.End.Line <= locs[1].Line)
            //        {
            //            lastBScope.End = locs[1];
            //            lastBScope.Entry.End = locs[1];
            //        }

            //        stackReference.Push(lastBScope);
            //        lastBScope.TryPutInScope(str, stackReference, locs);
            //    }
            //}
        }

        private int metCatchAt = -1;
        private OperationalScope currentCatchScope = null; 
        private void TryStmtTryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var level = AstStructureHelper.ParseLine.GetLevel(str);
            var type = AstStructureHelper.ParseLine.GetType(str);
            var mbStatement = Statement as MBStatement;
            if (++hitCount == 1)
            {
                var bStatement = new BStatement()
                {
                    Start = locs[0],
                    End = locs.Count == 2 ? locs[1] : null,
                    NoBraces = type != "CompoundStmt"
                };
                if (mbStatement.Blocks == null) mbStatement.Blocks = new List<BStatement>();
                mbStatement.Blocks.Add(bStatement);
                var bScop = new BlockScope()
                {
                    Start = locs[0],
                    End = locs.Count == 2 ? locs[1] : null,
                    Level = level,
                    Entry = bStatement
                };
                stackReference.Push(bScop);
                BlockScopes.Add(bScop);
                if (bStatement.NoBraces)
                {
                    bScop.TryPutInScope(str, stackReference, locs);
                }
            }
            else
            {
                if (type == "CXXCatchStmt")
                {
                    metCatchAt = hitCount;
                    var oStatement = new OStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null };
                    mbStatement.Conditions = new List<OStatement>() { oStatement };
                    var opScop = currentCatchScope = new OperationalScope { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = level + 2, OperationalStatement = oStatement };
                    ConditionScopes.Add(opScop);
                }
                else
                {
                    if (hitCount - metCatchAt == 1 && !type.Contains("Stmt")) // I am not 100% sure on that logic
                    {
//|     `-CXXCatchStmt 0x792c5bb568 <line:182:2, line:186:2>
//|       |-VarDecl 0x792c5b9b98 <line:182:9, col:13> col:13 used e 'int'
//|       `-CompoundStmt 0x792c5bb540 <line:183:2, line:186:2>
                        currentCatchScope.Level = level;
                        stackReference.Push(currentCatchScope);
                        currentCatchScope.TryPutInScope(str, stackReference, locs);
                    }
                    else
                    {
                        var bStatement = new BStatement()
                        {
                            Start = locs[0],
                            End = locs.Count == 2 ? locs[1] : null,
                            NoBraces = type != "CompoundStmt"
                        };
                        if (mbStatement.Blocks == null) mbStatement.Blocks = new List<BStatement>();
                        mbStatement.Blocks.Add(bStatement);
                        var bScop = new BlockScope()
                        {
                            Start = locs[0],
                            End = locs.Count == 2 ? locs[1] : null,
                            Level = level,
                            Entry = bStatement
                        };
                        stackReference.Push(bScop);
                        BlockScopes.Add(bScop);
                        if (bStatement.NoBraces)
                        {
                            bScop.TryPutInScope(str, stackReference, locs);
                        }
                    }
                }
            }
        }

        public override void PrettryPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("MultiBlockScope");
            xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            xtw.WriteAttributeString("Type", (Statement as MBStatement).Type);
            ConditionScopes.ForEach(x => x.PrettryPrint(xtw));
            BlockScopes.ForEach(x => x.PrettryPrint(xtw));
            xtw.WriteEndElement();
        }
    }
}
