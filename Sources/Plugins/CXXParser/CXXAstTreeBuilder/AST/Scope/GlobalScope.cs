﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Decls;
using CXXAstTreeBuilder.AST.Helpers;
using CXXAstTreeBuilder.AST.Location;
using CXXAstTreeBuilder.AST.Table;

namespace CXXAstTreeBuilder.AST.Scope
{
    public class GlobalScope:AbstractScope
    {
        public override void TryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var referenceId = AstStructureHelper.ParseLine.GetReference(str);
            //if (referenceId != -1 && ReferenceTable.Instance.Table.ContainsKey(referenceId))
            //{
            //    var duplicateScope = new DuplicateScope() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = AstStructureHelper.ParseLine.GetLevel(str) + 1};
            //    stackReference.Push(duplicateScope);
            //    return;
            //}
            var type = AstStructureHelper.ParseLine.GetType(str);
            switch (type)
            {
                case "VarDecl":
                {
                    if (referenceId != -1 && !ReferenceTable.Instance.Table.ContainsKey(referenceId))
                    {
                        var varDecl = new VariableDeclaration(referenceId) {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Name = AstStructureHelper.ParseLine.GetName(str)};
                        Variables.Add(varDecl);
                    }
                    break;
                }
                case "FunctionDecl":
                {
                    if (referenceId != -1 && !ReferenceTable.Instance.Table.ContainsKey(referenceId))
                    {
                        var funDecl = new FunctionDeclaration(referenceId, false) {Start = locs[0], End = locs[1], Name = AstStructureHelper.ParseLine.GetName(str), ReturnType = AstStructureHelper.ParseLine.GetFunctionType(str)};
                        Functions.Add(funDecl);
                        var functionScope = new FunctionScope() {Start = locs[0], End = locs[1], Level = AstStructureHelper.ParseLine.GetLevel(str) + 1};
                        funDecl.Body = functionScope.Body;
                        stackReference.Push(functionScope);
                    }
                    else
                    {
                        var decl = (ReferenceTable.Instance.Table[referenceId] as FunctionDeclaration);
                        var body = (ReferenceTable.Instance.Table[referenceId] as FunctionDeclaration).Body;
                        if (body.Entry == null)
                        {
                            decl.Definitions.Add(new Definition(decl));
                            decl.Start = locs[0];
                            decl.End = locs[1] ?? null;
                            var functionScope = new FunctionScope() { Start = locs[0], End = locs[1], Level = AstStructureHelper.ParseLine.GetLevel(str) + 1 };
                            (ReferenceTable.Instance.Table[referenceId] as FunctionDeclaration).Body = functionScope.Body;
                            stackReference.Push(functionScope);
                        }
                        else
                        {
                            decl.Definitions.Add(new Definition(locs));
                            var duplicateScope = new DuplicateScope() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = AstStructureHelper.ParseLine.GetLevel(str) + 1};
                            stackReference.Push(duplicateScope);
                        }
                    }
                    break;
                }
                case "CXXRecordDecl":
                {
                    if (referenceId != -1 && !ReferenceTable.Instance.Table.ContainsKey(referenceId))
                    {
                        var classDecl = new ClassDeclaration(referenceId) {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Name = AstStructureHelper.ParseLine.GetName(str)};
                        Classes.Add(classDecl);
                        var classScope = new ClassScope() {Start = locs[0], End = locs[1], Level = AstStructureHelper.ParseLine.GetLevel(str) + 1};
                        classDecl.Body = classScope;
                        stackReference.Push(classScope);
                    }
                    else
                    {
                        stackReference.Push((ReferenceTable.Instance.Table[referenceId] as ClassDeclaration).Body);
                    }
                    break;
                }
                case "CXXDestructorDecl":
                case "CXXConstructorDecl":
                case "CXXMethodDecl":
                {
                    if (ReferenceTable.Instance.Table.ContainsKey(referenceId))
                    {
                        var decl = (ReferenceTable.Instance.Table[referenceId] as FunctionDeclaration);
                        var body = (ReferenceTable.Instance.Table[referenceId] as FunctionDeclaration).Body;
                        if (body.Entry == null)
                        {
                            decl.Definitions.Add(new Definition(decl));
                            decl.Start = locs[0];
                            decl.End = locs[1] ?? null;
                            var functionScope = new FunctionScope() { Start = locs[0], End = locs[1] ?? null, Level = AstStructureHelper.ParseLine.GetLevel(str) + 1 };
                            (ReferenceTable.Instance.Table[referenceId] as FunctionDeclaration).Body = functionScope.Body;
                            stackReference.Push(functionScope);
                        }
                        else
                        {
                            decl.Definitions.Add(new Definition(locs));
                            var duplicateScope = new DuplicateScope() {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Level = AstStructureHelper.ParseLine.GetLevel(str) + 1};
                            stackReference.Push(duplicateScope);
                        }
                    }
                    else
                    {
                        var funDecl = new FunctionDeclaration(AstStructureHelper.ParseLine.GetReference(str), str.Contains(" inline ") || str.EndsWith(" inline") || locs[0].Line == locs[1].Line && locs[0].Column == locs[1].Column) {Start = locs[0], End = locs[1], Name = AstStructureHelper.ParseLine.GetName(str), ReturnType = AstStructureHelper.ParseLine.GetFunctionType(str)};
                        Functions.Add(funDecl);
                        var functionScope = new FunctionScope() {Start = locs[0], End = locs[1], Level = AstStructureHelper.ParseLine.GetLevel(str) + 1};
                        funDecl.Body = functionScope.Body;
                        stackReference.Push(functionScope);
                    }
                    break;
                }
                default: break;
            }

        }

        protected virtual void PrettyPrintType(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("GlobalScope");
        }
        public override void PrettryPrint(XmlTextWriter xtw)
        {
            PrettyPrintType(xtw);
            xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            if (Variables != null && Variables.Any())
            {
                xtw.WriteStartElement("Variables");
                foreach (var variableDeclaration in Variables)
                {
                    variableDeclaration.PrettyPrint(xtw);
                }
                xtw.WriteEndElement();
            }
            if (Functions != null && Functions.Any())
            {
                xtw.WriteStartElement("Functions");
                foreach (var variableDeclaration in Functions)
                {
                    variableDeclaration.PrettyPrint(xtw);
                }
                xtw.WriteEndElement();
            }

            if (Classes != null && Classes.Any())
            {
                xtw.WriteStartElement("Classes");
                foreach (var variableDeclaration in Classes)
                {
                    variableDeclaration.PrettyPrint(xtw);
                }
                xtw.WriteEndElement();
            }
            xtw.WriteEndElement();
        }
    }
}
