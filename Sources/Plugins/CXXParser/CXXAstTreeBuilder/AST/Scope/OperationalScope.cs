﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Decls;
using CXXAstTreeBuilder.AST.Helpers;
using CXXAstTreeBuilder.AST.Statements;
using CXXAstTreeBuilder.AST.Table;
using CXXAstTreeBuilder.AST.Location;

namespace CXXAstTreeBuilder.AST.Scope
{
    public class OperationalScope:AbstractScope
    {
        public OStatement OperationalStatement { get; set; }
        public override void TryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var type = AstStructureHelper.ParseLine.GetType(str);
            if (type == "DeclRefExpr" || type == "MemberExpr")
            {
                var reference = AstStructureHelper.ParseLine.GetReference(str);
                if (ReferenceTable.Instance.Table.ContainsKey(reference))
                {
                    OperationalStatement.ReferencedEntities.Add(new Entity(ReferenceTable.Instance.Table[reference]));
                }
                else
                {
                    var referenceType = str.ToLower().Contains(" function ") ? "Function" : "Var";
                    OperationalStatement.UnknownReferencedEntities.Add(new KeyValuePair<string, string>(AstStructureHelper.ParseLine.GetName(str), referenceType));
                }
            }

            for (int i = 0; i < locs.Count; i++)
            {
                if (OperationalStatement.End == null)
                {
                    if (OperationalStatement.Start.FileName == locs[i].FileName)
                    {
                        OperationalStatement.End = locs[i];
                    }
                    else
                    {
                        continue;
                    }
                }
                if (OperationalStatement.End.FileName == locs[i].FileName)
                {
                    if (OperationalStatement.End.Line < locs[i].Line || OperationalStatement.End.Line == locs[i].Line && OperationalStatement.End.Column < locs[i].Column)
                    {
                        OperationalStatement.End = locs[i];
                    }
                }
            }
        }

        public override void PrettryPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("OperationalScope");
            if(OperationalStatement != null) OperationalStatement.PrettyPrint(xtw);
            xtw.WriteEndElement();
        }
    }
}
