﻿using CXXAstTreeBuilder.AST.Location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Statements;
using CXXAstTreeBuilder.AST.Helpers;
using CXXAstTreeBuilder.AST.Decls;

namespace CXXAstTreeBuilder.AST.Scope
{
    public class FunctionScope:AbstractScope
    {
        public SourceFileLocation BodyStart { get; set; }
        public SourceFileLocation BodyEnd { get; set; }

        public List<VariableDeclaration> Parameters = new List<VariableDeclaration>();

        public BlockScope Body { get; set; }

        public FunctionScope()
        {
            Body = new BlockScope();
        }
        public override void TryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            var level = AstStructureHelper.ParseLine.GetLevel(str);
            var type = AstStructureHelper.ParseLine.GetType(str);
            var lastStatement = AstStructureHelper.GetLastStatement(Entry);
            if(locs.Count == 2 && locs[1].FileName != locs[0].FileName) locs.RemoveAt(1);
            switch (type)
            {
                case "CompoundStmt":
                {
                    if (BodyStart == null)
                    {
                        BodyStart = locs[0];
                        BodyEnd = locs.Count == 2 ? locs[1] : null;
                    }
                    if (lastStatement == null)
                    {
                        Entry = lastStatement = new BStatement() {Start = locs[0], End = locs.Count == 2 ? locs[1] : null, NoBraces = false};
                    }
                    else
                    {
                        if (lastStatement is BStatement)
                        {
                            lastStatement.End = locs[1];
                        }
                        else
                        {
                            lastStatement.Next = new BStatement() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, NoBraces = false };
                        }
                    }

                    this.Body.Start = locs[0];
                    this.Body.End = locs.Count == 2 ? locs[1] : null;
                    this.Body.Level = level + 1;
                    this.Body.Entry = Entry;
                    //Body = new BlockScope() { Start = locs[0], End = locs.Count == 2 ? locs[1] : null,  Level = level + 1, Entry = Entry};
                    stackReference.Push(Body);
                    break;
                }
                case "ParmVarDecl":
                {
                    var varDecl = new VariableDeclaration(AstStructureHelper.ParseLine.GetReference(str)) { Start = locs[0], End = locs.Count == 2 ? locs[1] : null, Name = AstStructureHelper.ParseLine.GetName(str) };
                    Parameters.Add(varDecl);
                    break;
                }
            }
        }

        public override void PrettryPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("FunctionScope");
            xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            xtw.WriteAttributeString("BodyStartLocation", BodyStart != null ? BodyStart.ToString() : "NULL");
            xtw.WriteAttributeString("BodyEndLocation", BodyEnd != null ? BodyEnd.ToString() : "NULL");
            if (Body != null) Body.PrettryPrint(xtw);
            xtw.WriteEndElement();
        }
    }
}
