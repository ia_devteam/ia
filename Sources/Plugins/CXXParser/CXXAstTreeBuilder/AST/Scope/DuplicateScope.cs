﻿using CXXAstTreeBuilder.AST.Location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Statements;
using CXXAstTreeBuilder.AST.Helpers;
using CXXAstTreeBuilder.AST.Decls;

namespace CXXAstTreeBuilder.AST.Scope
{
    public class DuplicateScope:AbstractScope
    {
        public SourceFileLocation BodyStart { get; set; }
        public SourceFileLocation BodyEnd { get; set; }

        public List<VariableDeclaration> Parameters = new List<VariableDeclaration>();

        public BlockScope Body { get; set; }
        public override void TryPutInScope(string str, Stack<AbstractScope> stackReference, List<SourceFileLocation> locs)
        {
            
        }

        public override void PrettryPrint(XmlTextWriter xtw)
        {
            xtw.WriteStartElement("FunctionScope");
            xtw.WriteAttributeString("StartLocation", Start != null ? Start.ToString() : "NULL");
            xtw.WriteAttributeString("EndLocation", End != null ? End.ToString() : "NULL");
            xtw.WriteAttributeString("BodyStartLocation", BodyStart != null ? BodyStart.ToString() : "NULL");
            xtw.WriteAttributeString("BodyEndLocation", BodyEnd != null ? BodyEnd.ToString() : "NULL");
            if (Body != null) Body.PrettryPrint(xtw);
            xtw.WriteEndElement();
        }
    }
}
