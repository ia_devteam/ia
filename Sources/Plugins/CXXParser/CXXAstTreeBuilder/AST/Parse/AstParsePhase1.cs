﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CXXAstTreeBuilder.AST.Decls;
using CXXAstTreeBuilder.AST.Helpers;
using CXXAstTreeBuilder.AST.Scope;
using CXXAstTreeBuilder.AST.Table;

namespace CXXAstTreeBuilder
{
    public class AstParsePhase1
    {
        private string _fileName;
        private StreamReader _fileStream;

        private AbstractScope _currentAbstractScope;

        private Stack<AbstractScope> _scopeStack;
        public static string _currentFile; //hack
        public static bool prevWasNull = false;
        public static string _prefix = null;
        public static bool inTemplateDecl = false;
        private int _clangLevel;

        public AstParsePhase1(string fileName, StreamReader fileStream, string prefix, int clangLevel)
        {
            _fileName = fileName;
            _fileStream = fileStream;
            _prefix = prefix != null? prefix.ToLower() : null;
            _scopeStack = new Stack<AbstractScope>();
            _currentFile = Path.GetFileName(fileName).Replace(".ast", "");
            _currentFile = _currentFile.Replace(".gz", "");
            inTemplateDecl = false;
            _clangLevel = clangLevel;
            AstStructureHelper.Initialize(_currentFile);
            ReferenceTable.Reset();
        }

        public AbstractScope Parse()
        {
            var str = string.Empty;
            var namesStack = new Stack<FileNameIdent>();
            int templateLvl = -1;
            _currentAbstractScope = new GlobalScope();
            _currentAbstractScope.Level = 0;
            _scopeStack.Push(_currentAbstractScope);
            namesStack.Push(new FileNameIdent() { FileName = _currentFile, Ident = 0 });
            ReferenceTable.Instance.Table = new Dictionary<long, IDeclaration>();
            while ((str = _fileStream.ReadLine()) != null)
            {
                if (!AstStructureHelper.IsAstTreeLine(str)) continue;
                AstStructureHelper.ParseLine = new ParseLine();
                if(_clangLevel == 3 && AstStructureHelper.ParseLine.GetReference(str) == -1) continue;
                var locations = AstStructureHelper.ExtractLocation(ref str, namesStack, _clangLevel);
                if (locations == null)
                {
                    prevWasNull = true;
                    continue;
                }

                var type = AstStructureHelper.ParseLine.GetType(str);
                var level = AstStructureHelper.ParseLine.GetLevel(str);
                //if (type == "ExprWithCleanups" && locations[0].Column > 5) locations[0].Column -= "emit ".Length; //commented due to #299
                if ((type == "ClassTemplateDecl" || type == "FunctionTemplateDecl") && !inTemplateDecl)
                {
                    inTemplateDecl = true;
                    templateLvl = level;
                } else if (level <= templateLvl && inTemplateDecl)
                {
                    inTemplateDecl = false;
                    templateLvl = -1;
                }

                if (_scopeStack.Peek().Level < level)
                {
                    _scopeStack.Peek().TryPutInScope(str, _scopeStack, locations);
                }
                else
                {
                    do
                    {
                        _scopeStack.Pop();
                    } while (_scopeStack.Peek().Level >= level);
                    _scopeStack.Peek().TryPutInScope(str, _scopeStack, locations);
                }
                prevWasNull = false;
            }
            return _currentAbstractScope;
        }

        public void PrettyPrint(string path)
        {
            var xtw = new XmlTextWriter(path + "\\" + _currentFile.Replace("\\", "#").Replace("/", "#") + ".xml", Encoding.UTF8);

            xtw.WriteStartDocument();

            _currentAbstractScope.PrettryPrint(xtw);

            xtw.WriteEndDocument();

            xtw.Flush();
            xtw.Close();
        }
    }
}
