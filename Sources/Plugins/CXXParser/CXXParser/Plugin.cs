using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using IA.Plugins.Parsers.CXXParser.Properties;
using IA.Plugins.Parsers.CommonUtils;
using Store;
using Store.Table;

namespace IA.Plugins.Parsers.CXXParser
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Парсер СXX";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.CXX_PARSER;

        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Парсинг AST файлов")]
            ASTFILES_PARSING,
            [Description("Занесение функций и переменных")]
            PARSE_PHASE1,
            [Description("Занесение ветвлений")]
            PARSE_PHASE2,
            [Description("Расстановка датчиков")]
            SENSOR_PLACEMENT
        };

        #region Настройки
        /// <summary>
        /// Номер первого датчика
        /// </summary>
        internal static ulong FirstSensorNumber = Settings.Default.FirstSensorNumber;
                
        /// <summary>
        /// Требуется вставлять датчики по 2-му уровню НДВ?
        /// </summary>
        internal static bool IsLevel2 = Settings.Default.IsLevel2;
        
        /// <summary>
        /// Требуется вставлять датчики по 3-му уровню НДВ?
        /// </summary>
        internal static bool IsLevel3 = Settings.Default.IsLevel3;

        internal static bool UnixMode = Settings.Default.UnixMode;

        /// <summary>
        /// Путь к директории с AST файлами
        /// </summary>
        internal static string AstFolderPath = Settings.Default.AstFolderPath;

        /// <summary>
        /// Строка с вызовом датчика
        /// </summary>
        internal static string SensorText = Settings.Default.SensorText;

        /// <summary>
        /// Строка с include
        /// </summary>
        internal static string IncludeText = Settings.Default.IncludeText;

        /// <summary>
        /// Прификс на машине сборки
        /// </summary>
        internal static string Prefix = Settings.Default.Prefix;

        /// <summary>
        /// Версия Clang
        /// </summary>
        internal static int ClangVersion = Settings.Default.ClangVersion;

        /// <summary>
        /// true - сериализация класса AbstractScope в 'zip' архив
        /// </summary>
        internal static bool IsSerilZip = Settings.Default.IsSerilZip;

        /// <summary>
        /// при формировании #include "sensor.h" будет добавляться // версия поставки
        /// </summary>
        internal static string hashVersion = Settings.Default.hashVersion;

        #endregion

        #region Прочие настройки


        /// <summary>
        /// Требуется ли разрешение полных имен?
        /// </summary>
        internal static bool IsNameResolving = true;

        /// <summary>
        /// Требуется ли запустить 1-ю стадию парсинга?
        /// </summary>
        internal static bool IsStage1 = true;

        /// <summary>
        /// Требуется ли запустить 2-ю стадию парсинга?
        /// </summary>
        internal static bool IsStage2 = true;

        /// <summary>
        /// Завершена ли 1-я стадия парсинга?
        /// </summary>
        internal static bool IsStage1Completed = false;

        /// <summary>
        /// Завершена ли 2-я стадия парсинга?
        /// </summary>
        internal static bool IsStage2Completed = false;

        /// <summary>
        /// Путь к будущей директории с исходниками со вставленными датчиками
        /// </summary>
        internal static string SourcesWithSensorsDirectoryPath (Storage storage)
        {
            return Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "C++");
        }

        #endregion  
    }

    public class CXXParser : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Настройки
        /// </summary>
        IA.Plugin.Settings settings;

        #region Члены PluginInterface
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
#if DEBUG
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS;
#else
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
#endif
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
		}

        /// <summary>
        /// Метод генерации отчетов для тестов
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до каталога с отчетами плагина</param>
        public void GenerateStatements(string reportsDirectoryPath)
        {
            (new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage)).Execute();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
#if DEBUG
            // Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            if (string.IsNullOrEmpty(reportsDirectoryPath))
                throw new Exception("Путь к каталогу с отчетами не определен.");

            //StatementPrinter
            //StatementPrinter sprinter = new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage);
            //sprinter.Execute();
#else       
            //RELEASE            
            //оставлено до 2015 года для поддержки старых работ
            return;
#endif
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            PluginSettings.FirstSensorNumber = Properties.Settings.Default.FirstSensorNumber;
            PluginSettings.IsLevel2 = Properties.Settings.Default.IsLevel2;
            PluginSettings.IsLevel3 = Properties.Settings.Default.IsLevel3;
            PluginSettings.SensorText = Properties.Settings.Default.SensorText;
            PluginSettings.IncludeText = Properties.Settings.Default.IncludeText;
            PluginSettings.AstFolderPath = Settings.Default.AstFolderPath;
            PluginSettings.Prefix = Settings.Default.Prefix;
            PluginSettings.ClangVersion = Settings.Default.ClangVersion;
            PluginSettings.IsSerilZip = Settings.Default.IsSerilZip;
            PluginSettings.hashVersion = Settings.Default.hashVersion;
            #if DEBUG
                PluginSettings.hashVersion = "";
            #endif

            this.storage = storage;

            //Выгружаем настройки из данных плагина
            LoadPluginData();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            bool tmpBool = true;
            PluginSettings.FirstSensorNumber = settingsBuffer.GetUInt64();
            settingsBuffer.GetBool(ref tmpBool);
            PluginSettings.IsLevel2 = tmpBool;
            PluginSettings.IsLevel3 = !PluginSettings.IsLevel2;
            PluginSettings.AstFolderPath = settingsBuffer.GetString();
            PluginSettings.SensorText = settingsBuffer.GetString();
            PluginSettings.IncludeText = settingsBuffer.GetString();
            PluginSettings.Prefix = settingsBuffer.GetString();
            PluginSettings.ClangVersion = settingsBuffer.GetInt32();
            settingsBuffer.GetBool(ref tmpBool);
            PluginSettings.IsSerilZip = tmpBool;
            PluginSettings.hashVersion = settingsBuffer.GetString();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            throw new Exception("Cannot be run in simple mode");
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            if (storage != null && storage.sensors.Count() > 0 && storage.sensors.EnumerateSensors().Max(s => s.ID) >= PluginSettings.FirstSensorNumber)
            {
                ulong tmp_FirstSensorNumber = PluginSettings.FirstSensorNumber;
                PluginSettings.FirstSensorNumber = storage.sensors.EnumerateSensors().Max(s => s.ID) + 1;
                Monitor.Log.Warning(PluginSettings.Name, "Расстановка датчиков с номера <" + tmp_FirstSensorNumber + "> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <" + PluginSettings.FirstSensorNumber + ">.");
            }

            CxxParserMain cxxParserMain = new CxxParserMain(storage);
            cxxParserMain.Run();

            SavePluginData();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.FirstSensorNumber = PluginSettings.FirstSensorNumber;
            Properties.Settings.Default.IsLevel2 = PluginSettings.IsLevel2;
            Properties.Settings.Default.IsLevel3 = PluginSettings.IsLevel3 = !PluginSettings.IsLevel2;
            Properties.Settings.Default.SensorText = PluginSettings.SensorText;
            Properties.Settings.Default.IncludeText = PluginSettings.IncludeText;
            Properties.Settings.Default.AstFolderPath = PluginSettings.AstFolderPath;
            Properties.Settings.Default.IsSerilZip = PluginSettings.IsSerilZip;
            Properties.Settings.Default.hashVersion = PluginSettings.hashVersion;
            Settings.Default.Prefix = PluginSettings.Prefix;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.FirstSensorNumber);
            settingsBuffer.Add(PluginSettings.IsLevel2);
            settingsBuffer.Add(PluginSettings.AstFolderPath);
            settingsBuffer.Add(PluginSettings.SensorText);
            settingsBuffer.Add(PluginSettings.IncludeText);
            settingsBuffer.Add(PluginSettings.Prefix);
            settingsBuffer.Add((Int32)PluginSettings.ClangVersion);
            settingsBuffer.Add(PluginSettings.IsSerilZip);
            settingsBuffer.Add(PluginSettings.hashVersion);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    Monitor.Tasks.Task.FromEnum(PluginSettings.Name, PluginSettings.Tasks.ASTFILES_PARSING),
                    Monitor.Tasks.Task.FromEnum(PluginSettings.Name, PluginSettings.Tasks.PARSE_PHASE1),
                    Monitor.Tasks.Task.FromEnum(PluginSettings.Name, PluginSettings.Tasks.PARSE_PHASE2),
                    Monitor.Tasks.Task.FromEnum(PluginSettings.Name, PluginSettings.Tasks.SENSOR_PLACEMENT)
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

		{
            get
            {
			    return null;
            }
		}

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new CustomSettings(
                    storage.sensors.EnumerateSensors().Count() == 0 
                    ? 1 
                    : storage.sensors.EnumerateSensors().Max(s => s.ID) + 1)
                    );
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;
            
            if (settings != null)
                settings.Save();

            if (String.IsNullOrWhiteSpace(PluginSettings.AstFolderPath))
            {
                message = "Директория с AST файлами Clang не задана.";
                return false;
            }
            
            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }             

#endregion

        private void LoadPluginData()
        {
            //////считываем массив байт из файла
            //byte[] data = File.ReadAllBytes(storage.pluginData.GetDataFileName(PluginSettings.ID));

            ////Если файл данных плагина пуст, или содрежит недостаточно информации
            //if (data.Length != 2)
            //{
            //    if (data.Length != 0)
            //        IA.Monitor.Log.Warning(PluginSettings.Name, "Файл данных плагина повреждён. Не удалось загрузить битовый массив.");
            //    return;
            //}

            //PluginSettings.IsStage1Completed = data[0] != 0;
            //PluginSettings.IsStage2Completed = data[1] != 0;
        }

        private void SavePluginData()
        {
            //File.WriteAllBytes(storage.pluginData.GetDataFileName(PluginSettings.ID), new byte[] { (byte)(int)(PluginSettings.IsStage1Completed ? 1 : 0), (byte)(int)(PluginSettings.IsStage2Completed ? 1 : 0) });
        }
    }
}
