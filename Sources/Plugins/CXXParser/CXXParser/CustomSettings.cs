﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using IA.Plugins.Parsers.CXXParser;

namespace IA.Plugins.Parsers.CXXParser
{
    internal partial class CustomSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        internal const string ObjectName = "Окно настроек стадии (плагина) Парсер C#";

        /// <summary>
        /// Конструктор
        /// </summary>
        internal CustomSettings(ulong minimumStartSensor)
        {
            InitializeComponent();

            //Номер первого датчика
            firstSensorNumber_numericUpDown.Minimum = minimumStartSensor;
            firstSensorNumber_numericUpDown.Maximum = ulong.MaxValue;
            firstSensorNumber_numericUpDown.Value = 
                    (PluginSettings.FirstSensorNumber > minimumStartSensor)
                    ? PluginSettings.FirstSensorNumber 
                    : minimumStartSensor;

            //Уровень НДВ
            isLevel2_radioButton.Checked = PluginSettings.IsLevel2;
            isLevel3_radioButton.Checked = PluginSettings.IsLevel3;
            tb_IncludeText.Text = PluginSettings.IncludeText;
            tb_SensorText.Text = PluginSettings.SensorText;
            tb_SourcePrefix.Text = PluginSettings.Prefix;
            //Инициализация элемента управления - Конфигурации проектов
            astFileFolder.Text = PluginSettings.AstFolderPath;
            isClang3_radioButton.Checked = PluginSettings.ClangVersion != 4;
            isClang4_radioButton.Checked = PluginSettings.ClangVersion == 4;
            radioSerilZIP.Checked = PluginSettings.IsSerilZip;
            radioSerilRAW.Checked = !PluginSettings.IsSerilZip;
        }
        

        /// <summary>
        /// Сохранение указанных настроек
        /// </summary>
        public void Save()
        {
            //сохраняем основные настройки
            PluginSettings.FirstSensorNumber = (uint)firstSensorNumber_numericUpDown.Value;
            PluginSettings.IsLevel2 = isLevel2_radioButton.Checked;
            PluginSettings.IsLevel3 = isLevel3_radioButton.Checked;
            PluginSettings.IncludeText = tb_IncludeText.Text;
            PluginSettings.SensorText = tb_SensorText.Text;
            PluginSettings.AstFolderPath = astFileFolder.Text;
            PluginSettings.Prefix = tb_SourcePrefix.Text;
            PluginSettings.ClangVersion = isClang4_radioButton.Checked ? 4 : 3;
            PluginSettings.IsSerilZip = radioSerilZIP.Checked ? true : false;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var ofd = new FolderBrowserDialog();
            var result = ofd.ShowDialog();
            if (result == DialogResult.OK)
            {
                astFileFolder.Text = ofd.SelectedPath;
            }
        }

        private void isClang4_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            isClang3_radioButton.Checked = !isClang4_radioButton.Checked;
        }

        private void isClang3_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            isClang4_radioButton.Checked = !isClang3_radioButton.Checked;
        }

        private void radioSerilRAW_CheckedChanged(object sender, EventArgs e)
        {
            radioSerilZIP.Checked = !radioSerilRAW.Checked;
        }

        private void radioSerilZIP_CheckedChanged(object sender, EventArgs e)
        {
            radioSerilRAW.Checked = !radioSerilZIP.Checked;
        }
    }
}