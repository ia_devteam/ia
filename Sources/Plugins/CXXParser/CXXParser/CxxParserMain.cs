﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using CXXAstTreeBuilder;
using CXXAstTreeBuilder.AST.Scope;
using CXXAstTreeBuilder.AST.Statements;
using Store;
using IA.Extensions;
using System.IO.Compression;
using CXXAstTreeBuilder.AST.Helpers;

namespace IA.Plugins.Parsers.CXXParser
{
    public class CxxParserMain
    {
        private Storage storage;

        private bool IsAstZipped = false;
        private ZipArchive zipArchive = null;
        private string zipAstPath = string.Empty;
        private List<AstFileDecl> mergedFilesList = new List<AstFileDecl>();
        private List<string> astPaths = new List<string>();
        private string tempDir = string.Empty;
        private XmlSerializer xsSubmit = new XmlSerializer(typeof(AbstractScope));
        private bool IsZipSerialize = false;

        public CxxParserMain(Storage storage)
        {
            this.storage = storage;
        }

        public void Run()
        {
            // получаем список 'ast' файлов (или 'zip' архив) из входной строки
            ProcessASTpath();

            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 0).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong) mergedFilesList.Count);
            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 1).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong) mergedFilesList.Count);
            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 2).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong) mergedFilesList.Count);

            tempDir = Path.Combine(storage.WorkDirectory.Path, Path.GetRandomFileName());
            Directory.CreateDirectory(tempDir);
            //for each KVP - generate internal ast tree and store it
            //Directory.CreateDirectory(Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "AST"));

            AstStructureHelper.InitStaticVars(); //FIXME: very bad practice. Need to get rid of all the statics

            // обработка всех имеющихся 'ast' файлов 
            parseRawAST();

            SensorPlacement.SkipSensorPlacement = new List<IStatement>();
            var fileSearcher = new FileSearcher(storage, PluginSettings.Prefix);
            var functionDuplicateChecker = new FunctionDuplicateChecker(storage);
            AbstractScopeParser1 nameParser = null;
            HashSet<ulong> templateFunctionsIds = new HashSet<ulong>();
            for (int i = 0; i < mergedFilesList.Count; i++)
            {
                if (!mergedFilesList[i].AstOk)
                {
                    mergedFilesList[i].FirstPassOk = false;
                    Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 1).Description(),
                        Monitor.Tasks.Task.UpdateType.STAGE, (ulong) i + 1);
                    continue;
                }
                try
                {
                    var ast = DeserializeObject(xsSubmit, mergedFilesList[i].AstDumpPath, IsZipSerialize);
                    nameParser = new AbstractScopeParser1(storage, mergedFilesList[i].IAFile, fileSearcher,
                        functionDuplicateChecker, astPaths[i], templateFunctionsIds);
                    nameParser.Parse(ast);
                    SerializeObject(xsSubmit, ast, mergedFilesList[i].AstDumpPath, IsZipSerialize);
                    mergedFilesList[i].FirstPassOk = true;
                }
                catch (Exception e)
                {
                    Monitor.Log.Error(PluginSettings.Name, "Error resolving names " + mergedFilesList[i].AstFile);
                    mergedFilesList[i].FirstPassOk = false;
                }
                Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 1).Description(),
                    Monitor.Tasks.Task.UpdateType.STAGE, (ulong) i + 1);
            }


            Dictionary<IFile, List<KeyValuePair<ulong, string>>> globalMetBStatements =
                new Dictionary<IFile, List<KeyValuePair<ulong, string>>>();
            //List<KeyValuePair<IFile, List<KeyValuePair<ulong, string>>>> globalMetBStatements = new List<KeyValuePair<IFile, List<KeyValuePair<ulong, string>>>> ();
            for (int i = 0; i < mergedFilesList.Count; i++)
            {
                if (!mergedFilesList[i].FirstPassOk)
                {
                    Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 2).Description(),
                        Monitor.Tasks.Task.UpdateType.STAGE, (ulong) i + 1);
                    continue;
                }
                try
                {
                    var ast = DeserializeObject(xsSubmit, mergedFilesList[i].AstDumpPath, IsZipSerialize);
                    var statementParser = new AbstractScopeParser2(storage, mergedFilesList[i].IAFile, fileSearcher,
                        astPaths[i]);
                    var returnedBraces = statementParser.Parse(ast);
                    foreach (var returnedBracePairs in returnedBraces)
                    {
                        if (!globalMetBStatements.ContainsKey(returnedBracePairs.Key))
                        {
                            globalMetBStatements.Add(returnedBracePairs.Key, returnedBracePairs.Value);
                        }
                    }
                }
                catch (Exception e)
                {
                    Monitor.Log.Error(PluginSettings.Name, "Error resolving statements " + mergedFilesList[i].AstFile);
                }
                Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 2).Description(),
                    Monitor.Tasks.Task.UpdateType.STAGE, (ulong) i + 1);
            }

            storage.sensors.SetSensorStartNumber(PluginSettings.FirstSensorNumber);
            var sensorPlacement = new SensorPlacement(storage, PluginSettings.IsLevel2 ? 2 : 3, globalMetBStatements,
                templateFunctionsIds);
            sensorPlacement.Paste();

            mergedFilesList.ForEach(g =>
            {
                if (!string.IsNullOrEmpty(g.AstDumpPath)) File.Delete(g.AstDumpPath);
            });

            DirectoryInfo dir = new DirectoryInfo(tempDir);
            foreach (FileInfo fi in dir.GetFiles())
                fi.Delete();

            Directory.Delete(tempDir);
        }

        private static AbstractScope DeserializeObject(XmlSerializer xsSubmit, string path, bool IsZipSer)
        {
            if (!IsZipSer)
            {
                var sr = new StreamReader(path);
                var ast = (AbstractScope) xsSubmit.Deserialize(sr);
                sr.Close();
                sr = null;
                return ast;
            }
            else
            {
                var zip = ZipFile.OpenRead(path + ".zip");
                ZipArchiveEntry readmeEntry = zip.GetEntry(Path.GetFileName(path));
                var stream = new StreamReader(readmeEntry.Open());
                var ast = (AbstractScope)xsSubmit.Deserialize(stream);
                stream.Close();
                stream = null;
                readmeEntry = null;
                zip.Dispose();
                zip = null;
                return ast;
            }
        }

        private static void SerializeObject(XmlSerializer xsSubmit, AbstractScope scope, string path, bool IsZipSer)
        {
            var xml = "";
            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, scope);
                    xml = sww.ToString(); // Your XML
                }
            }
            if (!IsZipSer)
            {
                var sw = new StreamWriter(path);
                sw.Write(xml);
                sw.Close();
                sw = null;
            }
            else
            {
                string pathZiped = path + ".zip";
                if (File.Exists(pathZiped))
                    File.Delete(pathZiped);
                
                using (FileStream zipToOpen = new FileStream(pathZiped, FileMode.Create))
                {
                    using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Create))
                    {
                        string t_name = Path.GetFileName(path);
                        ZipArchiveEntry readmeEntry = archive.CreateEntry(t_name, CompressionLevel.Fastest);
                        using (StreamWriter writer = new StreamWriter(readmeEntry.Open()))
                        {
                            writer.WriteLine(xml);
                        }
                    }
                }
            }
            xml = null;
        }

        // получаем список 'ast' файлов (или 'zip' архив) из входной строки
        private void ProcessASTpath()
        {
            string astFolderPath = PluginSettings.AstFolderPath;
            if (!string.IsNullOrEmpty(PluginSettings.Prefix) && !PluginSettings.Prefix.EndsWith("/") &&
                !PluginSettings.Prefix.EndsWith("\\"))
            {
                if (PluginSettings.Prefix.IndexOf("/") != -1)
                {
                    PluginSettings.Prefix += "/";
                }
                else
                {
                    PluginSettings.Prefix += "\\";
                }
            }

            DirectoryInfo directory = new DirectoryInfo(astFolderPath);
            FileInfo[] flist = directory.GetFiles();
            foreach (FileInfo f in flist)
            {
                string ext = Path.GetExtension(f.FullName);
                if (ext.ToLower() == ".zip")
                {
                    zipAstPath = f.FullName; // расчет на честного пользователя, который дает: или
                    IsAstZipped = true; // отдельные файлы (ast могут быть в gz архиве)
                    break; // или папку только с одним zip-архивом
                }
            }
            // find all ast files
            List<string> astFiles;
            List<string> astFilesgz;
            if (!IsAstZipped)
            {
                astFiles = Directory.EnumerateFiles(astFolderPath, "*.ast", SearchOption.AllDirectories).ToList();
                astFilesgz = Directory.EnumerateFiles(astFolderPath, "*.ast.gz", SearchOption.AllDirectories).ToList();
                foreach (var astFile in astFiles)
                {
                    var pathFileName = astFile.Substring(0, astFile.Length - 4) + ".clangastpath";
                    if (File.Exists(pathFileName))
                    {
                        astPaths.Add(File.ReadAllText(pathFileName));
                    }
                    else
                    {
                        astPaths.Add(null);
                    }
                }
                foreach (var astFilegz in astFilesgz) // предполагаем, что ast и ast.gz могут быть в одной папке
                {
                    var pathFileName = astFilegz.Substring(0, astFilegz.Length - 7) + ".clangastpath";
                    astPaths.Add(File.Exists(pathFileName) ? File.ReadAllText(pathFileName) : null);
                }
            }
            else
            {
                // не предполагается, что внутри zip файла будут ast.gz
                astFolderPath = "";
                astFiles = new List<string>();
                astFilesgz = new List<string>();
                try
                {
                    zipArchive = ZipFile.OpenRead(zipAstPath);
                    foreach (ZipArchiveEntry zipArchiveEntry in zipArchive.Entries)
                    {
                        if (Path.GetExtension(zipArchiveEntry.FullName) == ".ast")
                        {
                            string addAstPath = zipArchiveEntry.FullName;
                            //addAstPath = addAstPath.Replace("/", "\\");
                            astFiles.Add(addAstPath);
                        }
                        if (Path.GetExtension(zipArchiveEntry.FullName) == ".clangastpath")
                        {
                            ZipArchiveEntry readmeEntry = zipArchive.GetEntry(zipArchiveEntry.FullName);
                            using (var stream = new StreamReader(readmeEntry.Open()))
                            {
                                astPaths.Add(stream.ReadToEnd());
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    Monitor.Log.Error(PluginSettings.Name, "Zip Archive is not valid");
                    throw;
                }
                if (astFiles.Count != astPaths.Count)
                {
                    Monitor.Log.Error(PluginSettings.Name, "Not all ast files contain path files");
                    astPaths = astFiles.Select(x => "").ToList();
                }
            }

            foreach (var astFile in astFiles)
            {
                var fileToSearch = astFile.Substring(astFolderPath.Length);
                if (fileToSearch.StartsWith("\\")) fileToSearch = fileToSearch.Substring(1);
                var iFile = storage.files.Find(fileToSearch.Substring(0, fileToSearch.Length - 4));
                // cutting off .ast suffix
                if (iFile == null)
                {
                    Monitor.Log.Information(PluginSettings.Name, "Cannot find suitable file for ast " + astFile);
                    continue;
                }
                var kvp = new AstFileDecl() {IAFile = iFile, AstFile = astFile};
                mergedFilesList.Add(kvp);
            }

            foreach (var astFilegz in astFilesgz)
            {
                var fileToSearch = astFilegz.Substring(astFolderPath.Length);
                if (fileToSearch.StartsWith("\\")) fileToSearch = fileToSearch.Substring(1);
                var iFile = storage.files.Find(fileToSearch.Substring(0, fileToSearch.Length - 7));
                // cutting off .ast.gz suffix
                if (iFile == null)
                {
                    Monitor.Log.Information(PluginSettings.Name, "Cannot find suitable file for ast.gz " + astFilegz);
                    continue;
                }
                var kvp = new AstFileDecl() {IAFile = iFile, AstFile = astFilegz};
                mergedFilesList.Add(kvp);
            }
        }

        // обработка всех имеющихся 'ast' файлов 
        private void parseRawAST()
        {
            IsZipSerialize = PluginSettings.IsSerilZip;
            AstParsePhase1 clang = null;
            for (int i = 0; i < mergedFilesList.Count; i++)
            {
                try // сделано так, потомоу использование using требует своего scope,
                {   // в который три разных варианта чтения из stream (сырой ast, большой zip на все, ast.gz)
                    // не запихнешь. По этой причине сделано именно так.
                    StreamReader astStream = null;
                    FileStream reader = null;
                    GZipStream Gzip = null;
                    bool IsGz = false;
                    if (!IsAstZipped)
                    {
                        if (Path.GetExtension(mergedFilesList[i].AstFile) != ".gz")
                        {
                            astStream = new StreamReader(mergedFilesList[i].AstFile);
                        }
                        else
                            IsGz = true;
                    }
                    else
                    {
                        ZipArchiveEntry readmeEntry = zipArchive.GetEntry(mergedFilesList[i].AstFile);
                        astStream = new StreamReader(readmeEntry.Open());
                        mergedFilesList[i].AstFile = Path.Combine(Path.GetDirectoryName(zipAstPath),
                            mergedFilesList[i].AstFile.Replace("/", "\\"));
                    }
                    if (IsGz == false && astStream != null)
                        clang = new AstParsePhase1(mergedFilesList[i].AstFile, astStream, PluginSettings.Prefix,
                            PluginSettings.ClangVersion);
                    else
                    {
                        // читаем из 'ast.gz' файла
                        reader = File.OpenRead(mergedFilesList[i].AstFile);
                        Gzip = new GZipStream(reader, CompressionMode.Decompress, true);
                        astStream = new StreamReader(Gzip);
                        clang = new AstParsePhase1(mergedFilesList[i].AstFile, astStream, PluginSettings.Prefix,
                            PluginSettings.ClangVersion);
                    }
                    var scope = clang.Parse();
                    mergedFilesList[i].AstDumpPath = Path.Combine(tempDir, Path.GetRandomFileName());
                    SerializeObject(xsSubmit, scope, mergedFilesList[i].AstDumpPath, IsZipSerialize);
                    mergedFilesList[i].AstOk = true;
                    astStream.Close();
                    clang = null;
                    astStream = null;
                    if (IsGz)
                    {
                        Gzip.Close();
                        reader.Close();
                        Gzip = null;
                        reader = null;
                    }
                }
                catch (Exception e)
                {
                    Monitor.Log.Error(PluginSettings.Name, "Error parsing " + mergedFilesList[i].AstFile);
                    mergedFilesList[i].AstOk = false;
                }
                Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(),
                    Monitor.Tasks.Task.UpdateType.STAGE, (ulong)i + 1);
            }
            if (zipArchive != null)
            {
                zipArchive.Dispose();
                zipArchive = null;
            }
        }
    }

    public class AstFileDecl
    {
        public IFile IAFile { get; set; } 
        public string AstFile { get; set; }
        public bool AstOk { get; set; }
        public bool FirstPassOk { get; set; }
        public string AstDumpPath { get; set; }
        public string PathToSources { get; set; }
 
    }
}
