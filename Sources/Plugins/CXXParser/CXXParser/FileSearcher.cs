﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Store;

namespace IA.Plugins.Parsers.CXXParser
{
    public class FileSearcher
    {
        private Storage storage;
        private Dictionary<string, IFile> fileParts;
        private Dictionary<string, IFile> fileNameCache;
        private string prefix;
        public FileSearcher(Storage storage, string prefix)
        {
            this.storage = storage;
            this.prefix = prefix.ToLower();
            this.fileParts = new Dictionary<string, IFile>();
            fileNameCache = new Dictionary<string, IFile>();
            var files = storage.files.EnumerateFiles(enFileKind.fileWithPrefix);
            foreach (var file in files)
            {
                var name = file.RelativeFileName_Original;
                while (name.IndexOf("\\") != -1)
                {
                    if (!fileParts.ContainsKey(name))
                    {
                        fileParts.Add(name, file);
                    }
                    else
                    {
                        fileParts.Remove(name); // no duplicates could be allowed
                    }
                    name = name.Substring(name.IndexOf("\\") + 1);
                }
            }
        }

        private IFile Check(string name)
        {
            if (fileParts.ContainsKey(name)) return fileParts[name];
            return null;
        }

        public IFile GetFileForLocation(string fileName, IFile currentFile, string currentPathAst)
        {
            if (fileName == currentFile.NameAndExtension || currentFile.FileNameForReports.EndsWith(fileName.Replace("/","\\")))
                return currentFile; //BUG discovered in postgres
            var originalFileName = fileName;
            if (fileNameCache.ContainsKey(fileName)) return fileNameCache[fileName];
            //calculating path if relative
            if (fileName.StartsWith("."))
            {
                //var oldFileName = fileName;
                var currentPath = currentFile.FullFileName_Original;
                if (!string.IsNullOrEmpty(prefix) && !string.IsNullOrEmpty(currentPathAst) && currentPathAst.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase))
                {
                    currentPath = Path.Combine(storage.appliedSettings.FileListPath,currentPathAst.Substring(prefix.Length));
                }
                var folder = Path.GetDirectoryName(currentPath);
                var possibleFullPath = Path.GetFullPath(Path.Combine(folder, fileName));
                if (possibleFullPath.ToLower().Contains(storage.appliedSettings.FileListPath.ToLower()))
                {
                    fileName = possibleFullPath.Substring(storage.appliedSettings.FileListPath.Length);
                }
                if (fileName.StartsWith("/")) fileName = fileName.Substring(1);
                if (fileNameCache.ContainsKey(fileName)) return fileNameCache[fileName]; //duplicate check
                //fileName = oldFileName;
            }

            var val = GetFileForLocationImpl(fileName, currentFile);
            if (val == null)
            {
                var fixedFileName = originalFileName.Replace("/", "\\");
                if (prefix.Length > 0 && fixedFileName.ToLower().StartsWith(prefix.Replace("/", "\\")))
                {
                    fixedFileName = fixedFileName.Substring(prefix.Length);
                }
                if (fixedFileName.Contains(".."))
                {
                    do
                    {
                        var split = fixedFileName.Split('\\').ToList();
                        var indFirst = split.IndexOf("..");
                        split.RemoveAt(indFirst);
                        if (indFirst != 0)
                        {
                            split.RemoveAt(indFirst - 1);
                        }
                        fixedFileName = string.Join("\\", split);
                    } while (fixedFileName.Contains(".."));
                }
                if (fixedFileName.StartsWith(".\\"))
                {
                    fixedFileName = fixedFileName.Substring(2);
                }
                var sameFiles = Check(fixedFileName) ?? Check("\\" + fixedFileName);
                if (sameFiles != null)
                {
                    val = sameFiles; //FIXME - rare case, need to come up with correct logic
                }
                else
                {

                    val = storage.files.FindOrGenerate(fixedFileName, enFileKindForAdd.externalFile);
                }
            }
            if(!originalFileName.StartsWith(".")) fileNameCache.Add(originalFileName, val);
            return val;
        }

        IFile GetFileForLocationImpl(string fileName, IFile currentFile)
        {
            if (fileName.StartsWith(".")) return null;
            if (fileName == currentFile.NameAndExtension)
                return currentFile;
            if ((prefix.Length == 0 || !fileName.ToLower().StartsWith(prefix)) && (fileName.StartsWith("/") || fileName.Contains(":") || fileName[1] == '#'))
                return storage.files.FindOrGenerate(fileName, enFileKindForAdd.externalFile);
            return storage.files.Find(fileName);
        }
    }
}
