﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CXXAstTreeBuilder.AST.Decls;
using CXXAstTreeBuilder.AST.Location;
using CXXAstTreeBuilder.AST.Scope;
using CXXAstTreeBuilder.AST.Statements;
using FileOperations;
using IA.Extensions;
using Store;

namespace IA.Plugins.Parsers.CXXParser
{
    public class AbstractScopeParser2
    {
        private Storage storage;
        private IFile currentFile;
        private Stack<IStatement> currentLoop = new Stack<IStatement>();
        private FileSearcher fileSearcher;
        private string path;

        public AbstractScopeParser2(Storage storage, IFile currentFile, FileSearcher fileSearcher, string path)
        {
            this.storage = storage;
            this.currentFile = currentFile;
            this.fileSearcher = fileSearcher;
            this.path = path;
            metBObjectStatements = new HashSet<SourceFileLocation>();
            toIgnoreListInCompare = new HashSet<IStatement>();
        }

        public Dictionary<IFile, List<KeyValuePair<ulong, string>>> Parse(AbstractScope scope)
        {
            ParseInStack(scope, null);
            return metBStatements;
            //Debug.Assert(classStack.Count == 0);
        }

        private BStatement firstBStatementForAMethod = null;
        private ulong minOffsetForMethod = 0;
        private ulong maxOffsetForMethod = 0;
        
        private IStatement GetLastStatement(IStatement entry)
        {
            IStatement lastSameBlock = entry;
            while (lastSameBlock.NextInLinearBlock != null)
            {
                lastSameBlock = lastSameBlock.NextInLinearBlock;
            }
            return lastSameBlock;
        }

        private HashSet<IStatement> toIgnoreListInCompare = new HashSet<IStatement>();

        IStatement ParseInStack(AbstractScope scope, IFunction currentFunction)
        {
            IStatement entry = null;
            switch (scope.GetType().Name)
            {
                case "GlobalScope":
                case "ClassScope":
                {
                    var globalScope = scope as GlobalScope;
                    //need to go deeper
                    foreach (var classDeclaration in globalScope.Classes)
                    {
                        ParseInStack(classDeclaration.Body, currentFunction);
                    }
                    var funcsList = globalScope.Functions.Where(g => g.Body != null);
                    foreach (var functionDeclaration in funcsList)
                    {
                        var storageFunction = storage.functions.GetFunction((ulong)functionDeclaration.StorageFunction);
                        if (functionDeclaration.ToSkip) continue;
                        firstBStatementForAMethod = null;
                        minOffsetForMethod = maxOffsetForMethod = 0;
                        currentFunction = storageFunction;
                        Dictionary<IFile, List<KeyValuePair<ulong, string>>> duplicateMetBStatement = null;
                        HashSet<SourceFileLocation> duplicateMetBObjectStatements = null;
                        ulong oldLastStatementId = 0;
                        ulong statementCount = 0;
                            //List<IStatement> oldList = null;
                            //List<IStatement> newList = null;
                            int oldList = 0;
                            int newList = 0;
                        ulong functionStartLocation = 0;
                        if (storageFunction.EntryStatement != null)
                        {
                            functionStartLocation = storageFunction.EntryStatement.FirstSymbolLocation.GetFileID();
                            duplicateMetBStatement = metBStatements.ToDictionary(e => e.Key, e => new List<KeyValuePair<ulong, string>>(e.Value));
                            duplicateMetBObjectStatements = new HashSet<SourceFileLocation>(metBObjectStatements, metBObjectStatements.Comparer);
                            var fakeStatement = storage.statements.AddStatementOperational();
                            oldLastStatementId = fakeStatement.Id;
                            oldList = storageFunction.EnumerateStatements().Count(x => !(x is IStatementExit) && x.FirstSymbolLocation.GetFileID() == functionStartLocation && !toIgnoreListInCompare.Contains(x));
                        }
                        var ret = ParseInStack(functionDeclaration.Body, storageFunction);
                        if (storageFunction.EntryStatement != null)
                        {
                            var fakeStatement = storage.statements.AddStatementOperational();
                            for (var i = oldLastStatementId + 1; i < fakeStatement.Id; i++)
                            {
                                var x = storage.statements.GetStatement(i);
                                if (!(x is IStatementExit) && x.FirstSymbolLocation.GetFileID() == functionStartLocation)
                                {
                                    newList++;
                                }
                            }
                            if (oldList != newList)
                            {
                                //two function definitions ARE NOT the same
                                storageFunction.EnumerateStatements().ForEach(x => SensorPlacement.SkipSensorPlacement.Add(x));
                            }
                            metBStatements = duplicateMetBStatement;
                            metBObjectStatements = duplicateMetBObjectStatements;
                            continue;
                        }

                        //Debug.Assert(storageFunction.EntryStatement == null);
                        if (ret != null)
                        {
                            storageFunction.EntryStatement = ret;
                            var lastStatement = GetLastStatement(ret);
                            var firstLocationFile = fileSearcher.GetFileForLocation(firstBStatementForAMethod.Start.FileName, currentFile, path);
                            IFile lastLocationFile = firstBStatementForAMethod.End != null ? fileSearcher.GetFileForLocation(firstBStatementForAMethod.End.FileName, currentFile, path) : null;

                            if (lastLocationFile != null)
                            {
                                ulong oldOffset = lastStatement.LastSymbolLocation != null && lastStatement.LastSymbolLocation.GetFileID() == lastLocationFile.Id ? lastStatement.LastSymbolLocation.GetOffset() : 0;
                                lastStatement.SetLastSymbolLocation(lastLocationFile, (ulong)firstBStatementForAMethod.End.Line, (ulong)firstBStatementForAMethod.End.Column);
                                lastStatement.SetLastSymbolLocation(lastLocationFile, lastStatement.LastSymbolLocation.GetOffset() - 1);
                                if (oldOffset > lastStatement.LastSymbolLocation.GetOffset())
                                {
                                    lastStatement.SetLastSymbolLocation(lastLocationFile, oldOffset);
                                }
                            }

                            if (ret.FirstSymbolLocation.GetFileID() != storageFunction.Definition().GetFile().Id && firstLocationFile.Id == storageFunction.Definition().GetFile().Id)
                            {
                                ret.SetFirstSymbolLocation(storageFunction.Definition().GetFile(), (ulong)firstBStatementForAMethod.Start.Line, (ulong)firstBStatementForAMethod.Start.Column + 1);
                                SensorPlacement.SkipSensorPlacement.Remove(ret);
                            }

                            if (storageFunction.EntryStatement != null)
                            {
                                //if (storageFunction.EntryStatement.NextInLinearBlock == null)
                                //{
                                    var defFile = storageFunction.Definition();
                                    if (defFile != null)
                                    {
                                        var entryLocation = storageFunction.EntryStatement.FirstSymbolLocation;
                                        if (entryLocation.GetFileID() != defFile.GetFileID() || entryLocation.GetOffset() < minOffsetForMethod || entryLocation.GetOffset() > maxOffsetForMethod)
                                        {
                                            var opSt = storage.statements.AddStatementOperational(minOffsetForMethod + 2, defFile.GetFile());
                                            opSt.NextInLinearBlock = ret;
                                            storageFunction.EntryStatement = opSt;
                                            toIgnoreListInCompare.Add(opSt);
                                        }
                                    }
                                //}
                                //else
                                //{
                                lastStatement = GetLastStatement(storageFunction.EntryStatement);
                                if (!(lastStatement is IStatementReturn || lastStatement is IStatementThrow || lastStatement is IStatementGoto) && !SensorPlacement.SkipSensorPlacement.Contains(lastStatement))
                                {
                                    if (lastLocationFile.Id == storageFunction.Definition().GetFileID() && storageFunction.EntryStatement.FirstSymbolLocation.GetOffset() < lastStatement.LastSymbolLocation.GetOffset())
                                    {
                                        if (maxOffsetForMethod - minOffsetForMethod > 3)
                                        {
                                            var opSt = storage.statements.AddStatementOperational(maxOffsetForMethod - 1, lastLocationFile);
                                            lastStatement.NextInLinearBlock = opSt;
                                            toIgnoreListInCompare.Add(opSt);
                                        }
                                    }
                                }
                                //}
                            }

                        }
                    }
                    break;
                }
                case "BlockScope":
                {
                    var blockScope = scope as BlockScope;
                    if (blockScope.Entry != null) entry = GennyFromTheBlock(blockScope.Entry);
                    break;
                }
            }

            return entry;
        }

        ulong GetRealOffset(IFile location, AbstractReader reader, ulong line, ulong column)
        {
            if (reader == null) return location.ConvertLineColumnToOffset(line, column);
            return (ulong)reader.getOffset((int) line, (int) column, Encoding.ASCII);
        }

        Dictionary<IFile, List<KeyValuePair<ulong, string>>> metBStatements = new Dictionary<IFile, List<KeyValuePair<ulong, string>>>();
        HashSet<SourceFileLocation> metBObjectStatements = new HashSet<SourceFileLocation>(); 
        private int currentBrace = 0;
        private int gennyLevel = 0;
        IStatement GennyFromTheBlock(ICXXStatement entyAstStatement, bool addToSkipSensors = false)
        {
            var ignoredByTheTop = addToSkipSensors == true;
            if (entyAstStatement == null) return null;
            var endLocs = entyAstStatement.End;
            IStatement entry = null;
            IStatement lastStatement = null;
            gennyLevel++;
            ICXXStatement currentIcxxStatement = entyAstStatement;
            
            do
            {
                bool outOfBounds = false;
                var location = fileSearcher.GetFileForLocation(currentIcxxStatement.Start.FileName, currentFile, path);
                ulong startLine = (ulong) currentIcxxStatement.Start.Line;
                ulong startColumn = (ulong) currentIcxxStatement.Start.Column;
                ulong endLine = currentIcxxStatement.End != null ? (ulong) currentIcxxStatement.End.Line : 0;
                ulong endColumn = currentIcxxStatement.End != null ? (ulong) currentIcxxStatement.End.Column : 0;
                if (location.FileExistence != enFileExistence.EXTERNAL && !metBStatements.ContainsKey(location))
                {
                    metBStatements.Add(location, new List<KeyValuePair<ulong, string>>());
                }

                if (firstBStatementForAMethod != null && firstBStatementForAMethod.End != null && firstBStatementForAMethod.End.FileName == firstBStatementForAMethod.Start.FileName)
                {
                    //check if statement is out of bounds in that file
                    if (firstBStatementForAMethod.End.Line != 0 && currentIcxxStatement.Start.FileName == firstBStatementForAMethod.End.FileName && currentIcxxStatement.Start.Line > firstBStatementForAMethod.End.Line)
                    {
                        outOfBounds = true;
                    }

                    if (currentIcxxStatement.Start.FileName == firstBStatementForAMethod.End.FileName && currentIcxxStatement.Start.Line < firstBStatementForAMethod.Start.Line)
                    {
                        outOfBounds = true;
                    }

                    if (currentIcxxStatement.Start.FileName != firstBStatementForAMethod.End.FileName)
                    {
                        outOfBounds = true;
                    }

                    if (currentIcxxStatement.Start.FileName == firstBStatementForAMethod.Start.FileName && !ignoredByTheTop)
                    {
                        addToSkipSensors = outOfBounds;
                    }
                }

                switch (currentIcxxStatement.GetType().Name)
                {
                    case "BStatement":
                    {
                        if(addToSkipSensors) break;

                        AbstractReader locationFileReader = null;
                        if (location.FileExistence != enFileExistence.EXTERNAL)
                        {
                            locationFileReader = new AbstractReader(location.FullFileName_Original);
                        }

                        var bStatement = currentIcxxStatement as BStatement;
                        var openBrace = new KeyValuePair<ulong, string>(0,"");
                        var openBraceLineNum = -1;
                        var closeBrace = new KeyValuePair<ulong, string>(0, "");

                        if (firstBStatementForAMethod == null)
                        {
                            firstBStatementForAMethod = bStatement;
                            if (location.FileExistence != enFileExistence.EXTERNAL && endLine != 0)
                            {
                                minOffsetForMethod = GetRealOffset(location, locationFileReader, startLine, startColumn) - 1;
                                maxOffsetForMethod = GetRealOffset(location, locationFileReader, endLine, endColumn) + 1;
                            }
                            else if (endLine == 0)
                            {
                                if (bStatement.NoBraces)
                                {
                                    addToSkipSensors = true;
                                }
                                break;
                            }
                        }
                        if (location.FileExistence == enFileExistence.EXTERNAL)
                        {
                            if (bStatement.NoBraces)
                            {
                                addToSkipSensors = true;
                            }
                            break;
                        }

                        if (bStatement.NoBraces && (bStatement.Start.FileName == firstBStatementForAMethod.Start.FileName && bStatement.End.FileName == firstBStatementForAMethod.Start.FileName))
                        {
                            if (bStatement.Start.FileName == firstBStatementForAMethod.Start.FileName)
                            {
                                openBrace = new KeyValuePair<ulong, string>(GetRealOffset(location, locationFileReader, startLine, startColumn), "{/*" + ++currentBrace + "*/");
                                openBraceLineNum = (int)startLine;
                            }
                            else
                            {
                                var temp = currentIcxxStatement;
                                while (temp.Start.FileName != firstBStatementForAMethod.Start.FileName && temp.Next != null)
                                {
                                    temp = temp.Next;
                                }
                                if (temp.Start.FileName == firstBStatementForAMethod.Start.FileName)
                                {
                                    openBrace = new KeyValuePair<ulong, string>(GetRealOffset(location, locationFileReader, (ulong) temp.Start.Line, (ulong) temp.Start.Column), "{/*" + ++currentBrace + "*/");
                                    openBraceLineNum = temp.Start.Line;
                                }
                                else
                                {
                                    addToSkipSensors = true;
                                    break;
                                }
                            }

                            if (bStatement.End.FileName == firstBStatementForAMethod.Start.FileName)
                            {
                                closeBrace = new KeyValuePair<ulong, string>(GetRealOffset(location, locationFileReader, endLine, endColumn), "/*" + ++currentBrace + "*/}");

                                //metBStatements.Add(new KeyValuePair<ulong, string>(GetRealOffset(location, locationFileReader, (endLine, endColumn), "/*" + ++currentBrace + "*/}"));
                            }
                            else
                            {
                                if (currentIcxxStatement.Next is MBStatement && ((currentIcxxStatement.Next as MBStatement).Type == "IfStmt" || currentIcxxStatement.Next.End == null))
                                {
                                    addToSkipSensors = true;  // no way to even guess the beginning
                                    break;
                                }

                                if (currentIcxxStatement.Next.Start.FileName == firstBStatementForAMethod.Start.FileName)
                                {
                                    var endOffset = GetEndingOffsetFromStartingPoint(location, openBraceLineNum, startLine, openBrace);
                                    if (endOffset != 0)
                                    {
                                        closeBrace = new KeyValuePair<ulong, string>((ulong)endOffset, "/*" + ++currentBrace + "*/}"); // metBStatements.Add(new KeyValuePair<ulong, string>(GetRealOffset(location, locationFileReader, ((ulong)temp.End.Line, (ulong)temp.End.Column), "/*" + ++currentBrace + "*/}"));
                                    }
                                }
                                else if(currentIcxxStatement.Next.End.FileName == firstBStatementForAMethod.Start.FileName)
                                {
                                    var temp = currentIcxxStatement.Next;
                                    closeBrace = new KeyValuePair<ulong, string>(GetRealOffset(location, locationFileReader, (ulong)temp.End.Line, (ulong)temp.End.Column), "/*" + ++currentBrace + "*/}"); // metBStatements.Add(new KeyValuePair<ulong, string>(GetRealOffset(location, locationFileReader, ((ulong)temp.End.Line, (ulong)temp.End.Column), "/*" + ++currentBrace + "*/}"));
                                }
                            }

                            if (openBrace.Key == 0 || 
                                closeBrace.Key == 0 && currentIcxxStatement.Next is MBStatement && (currentIcxxStatement.Next as MBStatement).Type == "IfStmt" ||
                                closeBrace.Key <= openBrace.Key && currentIcxxStatement.Next is MBStatement && (currentIcxxStatement.Next as MBStatement).Type == "IfStmt")
                            {
                                addToSkipSensors = true;  // no way to even guess the beginning
                                break;
                            }

                            if (closeBrace.Key == 0 || closeBrace.Key <= openBrace.Key)
                            {
                                var endOffset = GetEndingOffsetFromStartingPoint(location, openBraceLineNum, startLine, openBrace);

                                if (endOffset == 0)
                                {
                                    addToSkipSensors = true;  // no way to even guess the beginning
                                    break;
                                }

                                if (closeBrace.Value == string.Empty)
                                {
                                    closeBrace = new KeyValuePair<ulong, string>((ulong)endOffset, "/*" + ++currentBrace + "*/}");//.Value = "/*" + ++currentBrace + "*/}";
                                }

                                closeBrace = new KeyValuePair<ulong, string>((ulong)endOffset, closeBrace.Value);
                            }

                            if (openBrace.Key >= closeBrace.Key || (openBrace.Key < minOffsetForMethod || openBrace.Key > maxOffsetForMethod) || (closeBrace.Key < minOffsetForMethod || closeBrace.Key > maxOffsetForMethod))
                            {
                                addToSkipSensors = true;  // no way to even guess the beginning
                                break;
                            }

                            if (!metBStatements.ContainsKey(location))
                            {
                                metBStatements.Add(location, new List<KeyValuePair<ulong, string>>());
                            }

                            if (!metBObjectStatements.Contains(bStatement.Start))
                            {
                                metBStatements[location].Add(openBrace);
                                metBStatements[location].Add(closeBrace);
                                metBObjectStatements.Add(bStatement.Start);
                            }
                            else
                            {
                                addToSkipSensors = true;  // no way to even guess the beginning
                                break;
                            }
                        } else if (bStatement.NoBraces)
                        {
                            addToSkipSensors = true;
                            ignoredByTheTop = true;
                        }
                        break;
                    }
                    case "OStatement":
                    case "DStatement":
                    case "CStatement":
                    {
                        IStatementOperational opStatement = null;
                        if (!(lastStatement is IStatementOperational) || lastStatement.FirstSymbolLocation.GetFile() != location || lastStatement.FirstSymbolLocation.GetLine() > startLine)
                        {
                            if (currentIcxxStatement.End != null && (currentIcxxStatement.Start.FileName != currentIcxxStatement.End.FileName || currentIcxxStatement.Start.Line > currentIcxxStatement.End.Line ))
                            {
                                endLine = endColumn = 0;
                            }
                            opStatement = storage.statements.AddStatementOperational(startLine, startColumn, location, endLine, endColumn);
                            opStatement.Operation = storage.operations.AddOperation();
                            if (lastStatement == null)
                            {
                                entry = lastStatement = opStatement;
                            }
                            else
                            {
                                lastStatement = lastStatement.NextInLinearBlock = opStatement;
                            }
                        }
                        else
                        {
                            opStatement = (IStatementOperational)lastStatement;
                        }
                        //if (opStatement.Id == 212)
                        //{
                        //    Debugger.Break();
                        //}
                        FillInOperationFromIStatementWithReference(currentIcxxStatement, opStatement.Operation);
                        break;
                    }
                    case "MBStatement":
                    {
                        var mbStatement = currentIcxxStatement as MBStatement;
                        var dumpedLocationCount = location.FileExistence != enFileExistence.EXTERNAL ? metBStatements[location].Count : 0;
                        if (mbStatement.hasError) { Monitor.Log.Warning(PluginSettings.Name, string.Format("Found error in statement in {0} at {1}:{2}", location.FileNameForReports, startLine, startColumn)); continue; }
                        IStatement generatedStatement = null;
                        switch (mbStatement.Type)
                        {
                            case "CXXTryStmt":
                            {
                                var tryCatchSt = storage.statements.AddStatementTryCatchFinally(startLine, startColumn, location, endLine, endColumn);
                                Debug.Assert(mbStatement.Blocks.Count >= 2);
                                tryCatchSt.TryBlock = GennyFromTheBlock(mbStatement.Blocks[0], addToSkipSensors);
                                for (int i = 1; i < mbStatement.Blocks.Count; i++)
                                {
                                    var catchSt = GennyFromTheBlock(mbStatement.Blocks[i], addToSkipSensors);
                                    if (catchSt != null)
                                    {
                                        tryCatchSt.AddCatch(catchSt);
                                    }
                                }
                                generatedStatement = tryCatchSt;
                                break;
                            }
                            case "SwitchStmt":
                            {
                                var switchSt = storage.statements.AddStatementSwitch(startLine, startColumn, location, endLine, endColumn);
                                Debug.Assert(mbStatement.Blocks.Count > 0 && mbStatement.Conditions.Count > 0 || mbStatement.Conditions.Count > 0 && mbStatement.isEmpty);
                                if (mbStatement.isEmpty)
                                {
                                    Monitor.Log.Warning(PluginSettings.Name, string.Format("Found empty switch statement in {0} at {1}:{2}", location.FileNameForReports, startLine, startColumn));
                                }
                                generatedStatement = switchSt;
                                currentLoop.Push(generatedStatement);
                                switchSt.Condition = storage.operations.AddOperation();
                                FillInOperationFromIStatementWithReference(mbStatement.Conditions[0], switchSt.Condition);
                                for (int i = 0; i < mbStatement.Blocks.Count; i++)
                                {
                                    var caseOp = storage.operations.AddOperation();
                                    var caseSt = GennyFromTheBlock(mbStatement.Blocks[i], addToSkipSensors);
                                    if (caseSt != null) switchSt.AddCase(caseOp, caseSt);
                                }
                                currentLoop.Pop();
                                break;
                            }
                            case "CXXForRangeStmt":
                            case "ForStmt":
                            {
                                //for (;;) ;   - in this case - no condition AT ALL!!!!
                                var forSt = storage.statements.AddStatementFor(startLine, startColumn, location, endLine, endColumn);
                                generatedStatement = forSt;
                                currentLoop.Push(generatedStatement);
                                Debug.Assert(mbStatement.Blocks.Count == 1 && mbStatement.Conditions.Count >= 0);
                                forSt.Condition = storage.operations.AddOperation();
                                if (mbStatement.Conditions.Count > 0)
                                {
                                    FillInOperationFromIStatementWithReference(mbStatement.Conditions[0], forSt.Condition);
                                }
                                if(mbStatement.Conditions.Count == 3)
                                        {
                                            forSt.Iteration = storage.operations.AddOperation();
                                            FillInOperationFromIStatementWithReference(mbStatement.Conditions[2], forSt.Iteration);
                                        }
                                        if (mbStatement.Blocks.Count == 1)
                                        {
                                            forSt.Body = GennyFromTheBlock(mbStatement.Blocks[0], addToSkipSensors);
                                        }
                                currentLoop.Pop();
                                break;
                            }
                            case "DoStmt":
                            {
                                var whileSt = storage.statements.AddStatementDoAndWhile(startLine, startColumn, location, endLine, endColumn);
                                whileSt.IsCheckConditionBeforeFirstRun = false;
                                generatedStatement = whileSt;
                                currentLoop.Push(generatedStatement);
                                Debug.Assert(mbStatement.Blocks.Count == mbStatement.Conditions.Count && mbStatement.Blocks.Count == 1);
                                whileSt.Condition = storage.operations.AddOperation();
                                FillInOperationFromIStatementWithReference(mbStatement.Conditions[0], whileSt.Condition);
                                whileSt.Body = GennyFromTheBlock(mbStatement.Blocks[0], addToSkipSensors);
                                currentLoop.Pop();
                                break;
                            }
                            case "WhileStmt":
                            {
                                var whileSt = storage.statements.AddStatementDoAndWhile(startLine, startColumn, location, endLine, endColumn);
                                whileSt.IsCheckConditionBeforeFirstRun = true;
                                generatedStatement = whileSt;
                                currentLoop.Push(generatedStatement);
                                Debug.Assert(mbStatement.Blocks.Count == mbStatement.Conditions.Count && mbStatement.Blocks.Count == 1);
                                whileSt.Condition = storage.operations.AddOperation();
                                FillInOperationFromIStatementWithReference(mbStatement.Conditions[0], whileSt.Condition);
                                whileSt.Body = GennyFromTheBlock(mbStatement.Blocks[0], addToSkipSensors);
                                currentLoop.Pop();
                                break;
                            }
                            case "IfStmt":
                            {
                                var ifSt = storage.statements.AddStatementIf(startLine, startColumn, location, endLine, endColumn);
                                Debug.Assert(mbStatement.Conditions.Count == 1 && (mbStatement.Blocks.Count == 1 || mbStatement.Blocks.Count == 2));
                                ifSt.Condition = storage.operations.AddOperation();
                                FillInOperationFromIStatementWithReference(mbStatement.Conditions[0], ifSt.Condition);
                                ifSt.ThenStatement = GennyFromTheBlock(mbStatement.Blocks[0], addToSkipSensors);
                                if (mbStatement.Blocks.Count == 2)
                                {
                                    ifSt.ElseStatement = GennyFromTheBlock(mbStatement.Blocks[1], addToSkipSensors);
                                }
                                generatedStatement = ifSt;
                                break;
                            }
                        }
                        if (lastStatement == null)
                        {
                            entry = lastStatement = generatedStatement;
                        }
                        else
                        {
                            lastStatement = lastStatement.NextInLinearBlock = generatedStatement;
                        }
                        if (lastStatement != null && !addToSkipSensors && location.FileExistence != enFileExistence.EXTERNAL)
                        {
                            if (checkSkipCond(lastStatement.FirstSymbolLocation, lastStatement.LastSymbolLocation))
                            {
                                SensorPlacement.SkipSensorPlacement.Add(lastStatement);
                                if (dumpedLocationCount > 0)
                                {
                                    metBStatements[location] = metBStatements[location].GetRange(0, dumpedLocationCount);
                                }
                                else
                                {
                                    metBStatements[location].Clear();
                                }
                            }
                        }

                        break;
                    }
                    case "RStatement":
                    {
                        //var retSt = storage.statements.AddStatementReturn(startLine, startColumn, location, endLine, endColumn);
                        var rStatement = currentIcxxStatement as RStatement;
                        IStatement generatedStatement = null;
                        switch (rStatement.Type)
                        {
                            case "CXXThrowExpr":
                            {
                                var thrSt = storage.statements.AddStatementThrow(startLine, startColumn, location, endLine, endColumn);
                                if (rStatement.ParameterStatement != null)
                                {
                                    thrSt.Exception = storage.operations.AddOperation();
                                    FillInOperationFromIStatementWithReference(rStatement.ParameterStatement, thrSt.Exception);
                                }
                                if (rStatement.ParameterStatement.End != null && rStatement.ParameterStatement.End.FileName == rStatement.Start.FileName)
                                {
                                    var endLoc = rStatement.ParameterStatement.End;
                                    if (endLoc.Line > (int)endLine || endLoc.Line == (int)endLine && endLoc.Column > (int)endColumn)
                                    {
                                        thrSt.SetLastSymbolLocation(location, (ulong)endLoc.Line, (ulong)endLoc.Column);
                                    }
                                }
                                generatedStatement = thrSt;
                                break;
                            }
                            case "ContinueStmt":
                            {
                                if (startColumn == endColumn) endColumn = startColumn + (ulong)"continue;".Length;
                                //hack over clang fault
                                if (metBStatements.Count > 0)
                                {
                                    if (metBStatements.ContainsKey(location))
                                    {
                                        if (metBStatements[location].Any())
                                        {
                                            var locReal = location.ConvertLineColumnToOffset(startLine, startColumn);
                                            for (var i = 0; i < metBStatements[location].Count; i++)
                                            {
                                                var metPair = metBStatements[location][i];
                                                if (metPair.Key == locReal && metPair.Value.EndsWith("}"))
                                                {
                                                    metBStatements[location][i] =
                                                        new KeyValuePair<ulong, string>(
                                                            metPair.Key + (ulong) "continue;".Length, metPair.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                                //
                                var cSt = storage.statements.AddStatementContinue(startLine, startColumn, location, endLine, endColumn);
                                Debug.Assert(currentLoop.Count > 0);
                                cSt.ContinuingLoop = currentLoop.Peek();
                                generatedStatement = cSt;
                                break;
                            }
                            case "BreakStmt":
                            {
                                if (startColumn == endColumn) endColumn = startColumn + (ulong)"break;".Length;
                                //hack over clang fault
                                if (metBStatements.Count > 0)
                                {
                                    if (metBStatements.ContainsKey(location))
                                    {
                                        if (metBStatements[location].Any())
                                        {
                                            var locReal = location.ConvertLineColumnToOffset(startLine, startColumn);
                                            for (var i = 0; i < metBStatements[location].Count; i++)
                                            {
                                                var metPair = metBStatements[location][i];
                                                if (metPair.Key == locReal && metPair.Value.EndsWith("}"))
                                                {
                                                    metBStatements[location][i] =
                                                        new KeyValuePair<ulong, string>(
                                                            metPair.Key + (ulong) "break;".Length, metPair.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                                //
                                var bSt = storage.statements.AddStatementBreak(startLine, startColumn, location, endLine, endColumn);
                                Debug.Assert(currentLoop.Count > 0);
                                bSt.BreakingLoop = currentLoop.Peek();
                                generatedStatement = bSt;
                                break;
                            }
                            case "ReturnStmt":
                            {
                                var rSt = storage.statements.AddStatementReturn(startLine, startColumn, location, endLine, endColumn);
                                if (rStatement.ParameterStatement != null)
                                {
                                    rSt.ReturnOperation = storage.operations.AddOperation();
                                    FillInOperationFromIStatementWithReference(rStatement.ParameterStatement, rSt.ReturnOperation);
                                    if (rStatement.ParameterStatement.End != null && rStatement.ParameterStatement.End.FileName == rStatement.Start.FileName)
                                    {
                                        var endLoc = rStatement.ParameterStatement.End;
                                        if (endLoc.Line > (int) endLine || endLoc.Line == (int) endLine && endLoc.Column > (int) endColumn)
                                        {
                                            rSt.SetLastSymbolLocation(location, (ulong)endLoc.Line, (ulong)endLoc.Column);
                                        }
                                    }
                                }
                                generatedStatement = rSt;
                                break;
                            }
                            case "GotoStmt":
                            {
                                var bSt = storage.statements.AddStatementGoto(startLine, startColumn, location, endLine, endColumn);
                                //Debug.Assert(currentLoop != null);
                                //bSt. .BreakingLoop = currentLoop;
                                generatedStatement = bSt;
                                break;
                            }
                        }
                        if (lastStatement == null)
                        {
                            entry = lastStatement = generatedStatement;
                        }
                        else
                        {
                            lastStatement = lastStatement.NextInLinearBlock = generatedStatement;
                        }
                        break;
                    }
                }
                if (lastStatement != null && lastStatement.NextInLinearBlock != null) lastStatement = lastStatement.NextInLinearBlock;
                if (lastStatement != null && (addToSkipSensors || outOfBounds))
                {
                    SensorPlacement.SkipSensorPlacement.Add(lastStatement);
                }

            } while ((currentIcxxStatement = currentIcxxStatement.Next) != null);

            if (lastStatement != null && endLocs != null)
            {
                var endLocation = fileSearcher.GetFileForLocation(endLocs.FileName, currentFile, path);
                if (endLocation.Id == lastStatement.FirstSymbolLocation.GetFileID()) //some bad code - may be bugfix for statements with wrong endings
                {
                    if (lastStatement.LastSymbolLocation == null || 
                        lastStatement.LastSymbolLocation.GetFileID() != lastStatement.FirstSymbolLocation.GetFileID() ||
                        lastStatement.LastSymbolLocation.GetLine() < (uint) endLocs.Line || 
                        lastStatement.LastSymbolLocation.GetLine() == (uint) endLocs.Line && lastStatement.LastSymbolLocation.GetColumn() < (uint) endLocs.Column)
                    {
                        if(!(lastStatement is IStatementReturn))//if (lastStatement.FirstSymbolLocation.GetFileID() == endLocation.Id) //if (lastStatement.LastSymbolLocation.GetFileID() == endLocation.Id)
                        {
                            lastStatement.SetLastSymbolLocation(endLocation, (ulong) endLocs.Line, (ulong) endLocs.Column);
                            var offset = lastStatement.LastSymbolLocation.GetOffset();
                            if (offset != ulong.MaxValue)
                            {
                                lastStatement.SetLastSymbolLocation(endLocation, offset - 1);
                            }
                        } //otherwise - it's a different file!
                    }
                }
            }
            else
            {
                if (lastStatement == null)
                {
                    currentIcxxStatement = entyAstStatement;
                    var location = fileSearcher.GetFileForLocation(currentIcxxStatement.Start.FileName, currentFile, path);
                    ulong startLine = (ulong)currentIcxxStatement.Start.Line != 0 ? (ulong)currentIcxxStatement.Start.Line : 1;
                    ulong startColumn = (ulong)currentIcxxStatement.Start.Column + 1;
                    ulong endLine = currentIcxxStatement.End != null && (ulong)currentIcxxStatement.End.Line != 0 ? (ulong)currentIcxxStatement.End.Line : 1;
                    ulong endColumn = currentIcxxStatement.End != null ? (ulong)currentIcxxStatement.End.Column + 1 : 1;
                    entry = storage.statements.AddStatementOperational(startLine, startColumn, location, endLine, endColumn);
                    if (gennyLevel != 1)
                    {
                        SensorPlacement.SkipSensorPlacement.Add(entry); //we do not put sensors here in any case so that we don;t flood the system with sensors in while(1); loop
                    }
                }
            }

            gennyLevel--;
            return entry;
        }

        private static int GetEndingOffsetFromStartingPoint(IFile location, int openBraceLineNum, ulong startLine, KeyValuePair<ulong, string> openBrace)
        {
            var aReader = new AbstractReader(location.FullFileName_Original);
            var endOffset = 0;
            var lineOfOpenBrace = aReader.getLineN(openBraceLineNum);
            if (lineOfOpenBrace.IndexOf(";") == -1)
            {
                var ident = lineOfOpenBrace.Length - lineOfOpenBrace.TrimStart().Length;
                for (int i = (int) startLine + 1; i <= aReader.getLinesCount(); i++)
                {
                    var currentLine = aReader.getLineN(i);
                    if (currentLine.Length - currentLine.TrimStart().Length <= ident)
                    {
                        endOffset = aReader.getOffset(i, 1) - 2;
                        break;
                    }
                }
            }
            else
            {
                endOffset = aReader.getText().IndexOf(";", (int) openBrace.Key);
            }
            return endOffset;
        }

        private void FillInOperationFromIStatementWithReference(ICXXStatement currentIcxxStatement, IOperation operation)
        {
            var stWithReferences = currentIcxxStatement as IStatementWithReference;
            var calledFunctions = new List<IFunction>();
            var distinctRef = stWithReferences.ReferencedEntities.Distinct();
            foreach (var referencedEntity in distinctRef)
            {
                if (referencedEntity.Function != null)
                {
                    var fDecl = referencedEntity.Function;
                    if (fDecl.StorageFunction != null)
                    {
                        var function = storage.functions.GetFunction((ulong)fDecl.StorageFunction);
                        calledFunctions.Add(function);
                    }
                    else
                    {
                        var funcs = storage.functions.FindFunction(new[] {fDecl.Name}).ToList();
                        calledFunctions.AddRange(funcs);
                    }
                }
                if (referencedEntity.Variable != null)
                {
                    var vDecl = referencedEntity.Variable;
                    if (vDecl.StorageVariable != null)
                    {
                       var vrbl = storage.variables.GetVariable((ulong) vDecl.StorageVariable);
                       operation.AddFunctionCallInSequenceOrder(vrbl);
                    }
                }
            }
            if (calledFunctions.Any()) operation.AddFunctionCallInSequenceOrder(calledFunctions.ToArray());
        }

        private bool checkSkipCond(Location locStmntFirst, Location locStmntLast)
        {
            if (locStmntFirst != null && locStmntLast != null)
            {
                if ((locStmntFirst.GetFile() == currentFile && (locStmntFirst.GetOffset() < minOffsetForMethod || locStmntFirst.GetOffset() > maxOffsetForMethod))
                    || locStmntFirst.GetOffset() > locStmntLast.GetOffset() ||
                    (locStmntLast.GetFile() == currentFile && (locStmntLast.GetOffset() < minOffsetForMethod || locStmntLast.GetOffset() > maxOffsetForMethod)))
                    return true;
            }
            else if (locStmntFirst != null)
            {
                if (locStmntFirst.GetFile() == currentFile && (locStmntFirst.GetOffset() < minOffsetForMethod || locStmntFirst.GetOffset() > maxOffsetForMethod))
                    return true;
            }
            return false;
        }
    }
}
