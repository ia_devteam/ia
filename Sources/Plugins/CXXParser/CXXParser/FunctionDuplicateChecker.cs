﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IA.Extensions;
using Store;

namespace IA.Plugins.Parsers.CXXParser
{
    public class FunctionDuplicateChecker
    {
        private Storage storage;
        private Dictionary<IFile, Dictionary<IFunction, List<Location>>> functionDefines;

        public FunctionDuplicateChecker(Storage storage)
        {
            this.storage = storage;
            var files = storage.files.EnumerateFiles(enFileKind.fileWithPrefix);
            functionDefines = new Dictionary<IFile, Dictionary<IFunction, List<Location>>>();
            foreach (var file in files)
            {
                functionDefines.Add(file, new Dictionary<IFunction, List<Location>>());
                file.FunctionsDefinedInTheFile().ForEach(g =>
                {
                    if (!functionDefines[file].ContainsKey(g))
                    {
                        functionDefines[file].Add(g, new List<Location>());
                    }
                    if (g.Definition() != null)
                    {
                        functionDefines[file][g].Add(g.Definition());
                    }
                });
            }
        }

        public IFunction Check(IFile file, string name, ulong line, ulong column)
        {
            if (!functionDefines.ContainsKey(file) 
                || functionDefines[file].Keys.All(g => g.Name != name)) return null;

            foreach (var function in functionDefines[file].Keys.Where(g => g.Name == name))
            {
                if (functionDefines[file][function].Any(g =>

                    g.GetLine() == line && g.GetColumn() == column
                    )) return function;
            }

            return null;

        }

        public void Add(IFile file, IFunction name)
        {
            if (!functionDefines.ContainsKey(file)) functionDefines.Add(file, new Dictionary<IFunction, List<Location>>());
            if (!functionDefines[file].ContainsKey(name))
            {
                functionDefines[file].Add(name, new List<Location>());
            }
            functionDefines[file][name].Add(name.Definition());
        }
    }
}
