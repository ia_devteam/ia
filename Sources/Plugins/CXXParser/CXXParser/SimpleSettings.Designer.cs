﻿namespace IA.Plugins.Parsers.CXXParser
{
    partial class SimpleSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SuspendLayout();
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 2;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(469, 55);
            this.settings_tableLayoutPanel.TabIndex = 0;
            // 
            // SimpleSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "SimpleSettings";
            this.Size = new System.Drawing.Size(469, 55);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
    }
}
