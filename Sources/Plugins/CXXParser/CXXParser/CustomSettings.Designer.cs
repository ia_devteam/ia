﻿namespace IA.Plugins.Parsers.CXXParser
{
    internal partial class CustomSettings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.sensors_groupBox = new System.Windows.Forms.GroupBox();
            this.firstSensorNumber_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.firstSensorNumber_label = new System.Windows.Forms.Label();
            this.firstSensorNumber_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.ast_groupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelAstFileFolder = new System.Windows.Forms.Label();
            this.astFileFolder = new System.Windows.Forms.TextBox();
            this.browseAstFileFolder = new System.Windows.Forms.Button();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tb_SourcePrefix = new System.Windows.Forms.TextBox();
            this.level_groupBox = new System.Windows.Forms.GroupBox();
            this.level_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isLevel3_radioButton = new System.Windows.Forms.RadioButton();
            this.isLevel2_radioButton = new System.Windows.Forms.RadioButton();
            this.gb_SensorText = new System.Windows.Forms.GroupBox();
            this.tlp_SensorText = new System.Windows.Forms.TableLayoutPanel();
            this.tb_SensorText = new System.Windows.Forms.TextBox();
            this.gb_IncludeText = new System.Windows.Forms.GroupBox();
            this.tlp_IncludeText = new System.Windows.Forms.TableLayoutPanel();
            this.tb_IncludeText = new System.Windows.Forms.TextBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.clangVersion_tlp = new System.Windows.Forms.TableLayoutPanel();
            this.isClang3_radioButton = new System.Windows.Forms.RadioButton();
            this.isClang4_radioButton = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableIsSerilZip = new System.Windows.Forms.TableLayoutPanel();
            this.radioSerilRAW = new System.Windows.Forms.RadioButton();
            this.radioSerilZIP = new System.Windows.Forms.RadioButton();
            this.sensors_groupBox.SuspendLayout();
            this.firstSensorNumber_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).BeginInit();
            this.ast_groupBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.main_tableLayoutPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.level_groupBox.SuspendLayout();
            this.level_tableLayoutPanel.SuspendLayout();
            this.gb_SensorText.SuspendLayout();
            this.tlp_SensorText.SuspendLayout();
            this.gb_IncludeText.SuspendLayout();
            this.tlp_IncludeText.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.clangVersion_tlp.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableIsSerilZip.SuspendLayout();
            this.SuspendLayout();
            // 
            // sensors_groupBox
            // 
            this.sensors_groupBox.Controls.Add(this.firstSensorNumber_tableLayoutPanel);
            this.sensors_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensors_groupBox.Location = new System.Drawing.Point(3, 113);
            this.sensors_groupBox.Name = "sensors_groupBox";
            this.sensors_groupBox.Size = new System.Drawing.Size(684, 49);
            this.sensors_groupBox.TabIndex = 6;
            this.sensors_groupBox.TabStop = false;
            this.sensors_groupBox.Text = "Расстановка датчиков";
            // 
            // firstSensorNumber_tableLayoutPanel
            // 
            this.firstSensorNumber_tableLayoutPanel.ColumnCount = 2;
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.Controls.Add(this.firstSensorNumber_label, 0, 0);
            this.firstSensorNumber_tableLayoutPanel.Controls.Add(this.firstSensorNumber_numericUpDown, 1, 0);
            this.firstSensorNumber_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstSensorNumber_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.firstSensorNumber_tableLayoutPanel.Name = "firstSensorNumber_tableLayoutPanel";
            this.firstSensorNumber_tableLayoutPanel.RowCount = 1;
            this.firstSensorNumber_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.Size = new System.Drawing.Size(678, 30);
            this.firstSensorNumber_tableLayoutPanel.TabIndex = 6;
            // 
            // firstSensorNumber_label
            // 
            this.firstSensorNumber_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_label.AutoSize = true;
            this.firstSensorNumber_label.Location = new System.Drawing.Point(3, 8);
            this.firstSensorNumber_label.Name = "firstSensorNumber_label";
            this.firstSensorNumber_label.Size = new System.Drawing.Size(131, 13);
            this.firstSensorNumber_label.TabIndex = 1;
            this.firstSensorNumber_label.Text = "Номер первого датчика:";
            // 
            // firstSensorNumber_numericUpDown
            // 
            this.firstSensorNumber_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_numericUpDown.Location = new System.Drawing.Point(140, 5);
            this.firstSensorNumber_numericUpDown.Name = "firstSensorNumber_numericUpDown";
            this.firstSensorNumber_numericUpDown.Size = new System.Drawing.Size(535, 20);
            this.firstSensorNumber_numericUpDown.TabIndex = 0;
            // 
            // ast_groupBox
            // 
            this.ast_groupBox.Controls.Add(this.tableLayoutPanel1);
            this.ast_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ast_groupBox.Location = new System.Drawing.Point(3, 168);
            this.ast_groupBox.Name = "ast_groupBox";
            this.ast_groupBox.Size = new System.Drawing.Size(684, 49);
            this.ast_groupBox.TabIndex = 7;
            this.ast_groupBox.TabStop = false;
            this.ast_groupBox.Text = "Путь до папки с AST файлами Clang";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.522124F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 92.47787F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel1.Controls.Add(this.labelAstFileFolder, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.astFileFolder, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.browseAstFileFolder, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(678, 30);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // labelAstFileFolder
            // 
            this.labelAstFileFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAstFileFolder.AutoSize = true;
            this.labelAstFileFolder.Location = new System.Drawing.Point(3, 8);
            this.labelAstFileFolder.Name = "labelAstFileFolder";
            this.labelAstFileFolder.Size = new System.Drawing.Size(37, 13);
            this.labelAstFileFolder.TabIndex = 0;
            this.labelAstFileFolder.Text = "Путь";
            // 
            // astFileFolder
            // 
            this.astFileFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.astFileFolder.Location = new System.Drawing.Point(46, 5);
            this.astFileFolder.Name = "astFileFolder";
            this.astFileFolder.Size = new System.Drawing.Size(524, 20);
            this.astFileFolder.TabIndex = 1;
            // 
            // browseAstFileFolder
            // 
            this.browseAstFileFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.browseAstFileFolder.Location = new System.Drawing.Point(576, 3);
            this.browseAstFileFolder.Name = "browseAstFileFolder";
            this.browseAstFileFolder.Size = new System.Drawing.Size(99, 23);
            this.browseAstFileFolder.TabIndex = 2;
            this.browseAstFileFolder.Text = "Обзор";
            this.browseAstFileFolder.UseVisualStyleBackColor = true;
            this.browseAstFileFolder.Click += new System.EventHandler(this.button1_Click);
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.groupBox1, 0, 6);
            this.main_tableLayoutPanel.Controls.Add(this.ast_groupBox, 0, 3);
            this.main_tableLayoutPanel.Controls.Add(this.sensors_groupBox, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.level_groupBox, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.gb_SensorText, 0, 4);
            this.main_tableLayoutPanel.Controls.Add(this.gb_IncludeText, 0, 5);
            this.main_tableLayoutPanel.Controls.Add(this.lblVersion, 0, 0);
            this.main_tableLayoutPanel.Controls.Add(this.groupBox2, 0, 7);
            this.main_tableLayoutPanel.Controls.Add(this.groupBox3, 0, 8);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 9;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(690, 650);
            this.main_tableLayoutPanel.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 333);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(684, 49);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Префикс исходных текстов";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tb_SourcePrefix, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(678, 30);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tb_SourcePrefix
            // 
            this.tb_SourcePrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_SourcePrefix.Location = new System.Drawing.Point(3, 5);
            this.tb_SourcePrefix.Name = "tb_SourcePrefix";
            this.tb_SourcePrefix.Size = new System.Drawing.Size(672, 20);
            this.tb_SourcePrefix.TabIndex = 0;
            this.tb_SourcePrefix.Text = "/root/rpmbuild/BUILD/";
            // 
            // level_groupBox
            // 
            this.level_groupBox.Controls.Add(this.level_tableLayoutPanel);
            this.level_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_groupBox.Location = new System.Drawing.Point(3, 52);
            this.level_groupBox.Name = "level_groupBox";
            this.level_groupBox.Size = new System.Drawing.Size(684, 55);
            this.level_groupBox.TabIndex = 6;
            this.level_groupBox.TabStop = false;
            this.level_groupBox.Text = "Уровень вставки датчиков";
            // 
            // level_tableLayoutPanel
            // 
            this.level_tableLayoutPanel.ColumnCount = 3;
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Controls.Add(this.isLevel3_radioButton, 1, 0);
            this.level_tableLayoutPanel.Controls.Add(this.isLevel2_radioButton, 0, 0);
            this.level_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.level_tableLayoutPanel.Name = "level_tableLayoutPanel";
            this.level_tableLayoutPanel.RowCount = 1;
            this.level_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Size = new System.Drawing.Size(678, 36);
            this.level_tableLayoutPanel.TabIndex = 2;
            // 
            // isLevel3_radioButton
            // 
            this.isLevel3_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isLevel3_radioButton.AutoSize = true;
            this.isLevel3_radioButton.Location = new System.Drawing.Point(103, 9);
            this.isLevel3_radioButton.Name = "isLevel3_radioButton";
            this.isLevel3_radioButton.Size = new System.Drawing.Size(94, 17);
            this.isLevel3_radioButton.TabIndex = 1;
            this.isLevel3_radioButton.TabStop = true;
            this.isLevel3_radioButton.Text = "3-й уровень";
            this.isLevel3_radioButton.UseVisualStyleBackColor = true;
            // 
            // isLevel2_radioButton
            // 
            this.isLevel2_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isLevel2_radioButton.AutoSize = true;
            this.isLevel2_radioButton.Checked = true;
            this.isLevel2_radioButton.Location = new System.Drawing.Point(3, 9);
            this.isLevel2_radioButton.Name = "isLevel2_radioButton";
            this.isLevel2_radioButton.Size = new System.Drawing.Size(94, 17);
            this.isLevel2_radioButton.TabIndex = 0;
            this.isLevel2_radioButton.TabStop = true;
            this.isLevel2_radioButton.Text = "2-й уровень";
            this.isLevel2_radioButton.UseVisualStyleBackColor = true;
            // 
            // gb_SensorText
            // 
            this.gb_SensorText.Controls.Add(this.tlp_SensorText);
            this.gb_SensorText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_SensorText.Location = new System.Drawing.Point(3, 223);
            this.gb_SensorText.Name = "gb_SensorText";
            this.gb_SensorText.Size = new System.Drawing.Size(684, 49);
            this.gb_SensorText.TabIndex = 8;
            this.gb_SensorText.TabStop = false;
            this.gb_SensorText.Text = "Текст датчика";
            // 
            // tlp_SensorText
            // 
            this.tlp_SensorText.ColumnCount = 1;
            this.tlp_SensorText.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_SensorText.Controls.Add(this.tb_SensorText, 0, 0);
            this.tlp_SensorText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_SensorText.Location = new System.Drawing.Point(3, 16);
            this.tlp_SensorText.Name = "tlp_SensorText";
            this.tlp_SensorText.RowCount = 1;
            this.tlp_SensorText.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_SensorText.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlp_SensorText.Size = new System.Drawing.Size(678, 30);
            this.tlp_SensorText.TabIndex = 0;
            // 
            // tb_SensorText
            // 
            this.tb_SensorText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_SensorText.Location = new System.Drawing.Point(3, 5);
            this.tb_SensorText.Name = "tb_SensorText";
            this.tb_SensorText.Size = new System.Drawing.Size(672, 20);
            this.tb_SensorText.TabIndex = 0;
            this.tb_SensorText.Text = "Din_Go(#,2048);";
            // 
            // gb_IncludeText
            // 
            this.gb_IncludeText.Controls.Add(this.tlp_IncludeText);
            this.gb_IncludeText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_IncludeText.Location = new System.Drawing.Point(3, 278);
            this.gb_IncludeText.Name = "gb_IncludeText";
            this.gb_IncludeText.Size = new System.Drawing.Size(684, 49);
            this.gb_IncludeText.TabIndex = 9;
            this.gb_IncludeText.TabStop = false;
            this.gb_IncludeText.Text = "Текст include строки";
            // 
            // tlp_IncludeText
            // 
            this.tlp_IncludeText.ColumnCount = 1;
            this.tlp_IncludeText.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_IncludeText.Controls.Add(this.tb_IncludeText, 0, 0);
            this.tlp_IncludeText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_IncludeText.Location = new System.Drawing.Point(3, 16);
            this.tlp_IncludeText.Name = "tlp_IncludeText";
            this.tlp_IncludeText.RowCount = 1;
            this.tlp_IncludeText.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_IncludeText.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlp_IncludeText.Size = new System.Drawing.Size(678, 30);
            this.tlp_IncludeText.TabIndex = 0;
            // 
            // tb_IncludeText
            // 
            this.tb_IncludeText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_IncludeText.Location = new System.Drawing.Point(3, 5);
            this.tb_IncludeText.Name = "tb_IncludeText";
            this.tb_IncludeText.Size = new System.Drawing.Size(672, 20);
            this.tb_IncludeText.TabIndex = 0;
            this.tb_IncludeText.Text = "#include \"/var/tmp/sensor.h\"";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(3, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(94, 13);
            this.lblVersion.TabIndex = 11;
            this.lblVersion.Text = "10.02.12.05 /ninja";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.clangVersion_tlp);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 388);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(684, 55);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Версия Clang";
            // 
            // clangVersion_tlp
            // 
            this.clangVersion_tlp.ColumnCount = 2;
            this.clangVersion_tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.clangVersion_tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.clangVersion_tlp.Controls.Add(this.isClang3_radioButton, 1, 0);
            this.clangVersion_tlp.Controls.Add(this.isClang4_radioButton, 0, 0);
            this.clangVersion_tlp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clangVersion_tlp.Location = new System.Drawing.Point(3, 16);
            this.clangVersion_tlp.Name = "clangVersion_tlp";
            this.clangVersion_tlp.RowCount = 1;
            this.clangVersion_tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.clangVersion_tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.clangVersion_tlp.Size = new System.Drawing.Size(678, 36);
            this.clangVersion_tlp.TabIndex = 0;
            // 
            // isClang3_radioButton
            // 
            this.isClang3_radioButton.AutoSize = true;
            this.isClang3_radioButton.Location = new System.Drawing.Point(342, 3);
            this.isClang3_radioButton.Name = "isClang3_radioButton";
            this.isClang3_radioButton.Size = new System.Drawing.Size(58, 17);
            this.isClang3_radioButton.TabIndex = 1;
            this.isClang3_radioButton.Text = "Clang3";
            this.isClang3_radioButton.UseVisualStyleBackColor = true;
            this.isClang3_radioButton.CheckedChanged += new System.EventHandler(this.isClang3_radioButton_CheckedChanged);
            // 
            // isClang4_radioButton
            // 
            this.isClang4_radioButton.AutoSize = true;
            this.isClang4_radioButton.Checked = true;
            this.isClang4_radioButton.Location = new System.Drawing.Point(3, 3);
            this.isClang4_radioButton.Name = "isClang4_radioButton";
            this.isClang4_radioButton.Size = new System.Drawing.Size(58, 17);
            this.isClang4_radioButton.TabIndex = 0;
            this.isClang4_radioButton.TabStop = true;
            this.isClang4_radioButton.Text = "Clang4";
            this.isClang4_radioButton.UseVisualStyleBackColor = true;
            this.isClang4_radioButton.CheckedChanged += new System.EventHandler(this.isClang4_radioButton_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableIsSerilZip);
            this.groupBox3.Location = new System.Drawing.Point(3, 449);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(684, 55);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Формат сохранения временных файлов";
            // 
            // tableIsSerilZip
            // 
            this.tableIsSerilZip.ColumnCount = 2;
            this.tableIsSerilZip.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableIsSerilZip.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableIsSerilZip.Controls.Add(this.radioSerilRAW, 1, 0);
            this.tableIsSerilZip.Controls.Add(this.radioSerilZIP, 0, 0);
            this.tableIsSerilZip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableIsSerilZip.Location = new System.Drawing.Point(3, 16);
            this.tableIsSerilZip.Name = "tableIsSerilZip";
            this.tableIsSerilZip.RowCount = 1;
            this.tableIsSerilZip.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableIsSerilZip.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableIsSerilZip.Size = new System.Drawing.Size(678, 36);
            this.tableIsSerilZip.TabIndex = 1;
            // 
            // radioSerilRAW
            // 
            this.radioSerilRAW.AutoSize = true;
            this.radioSerilRAW.Checked = true;
            this.radioSerilRAW.Location = new System.Drawing.Point(342, 3);
            this.radioSerilRAW.Name = "radioSerilRAW";
            this.radioSerilRAW.Size = new System.Drawing.Size(51, 17);
            this.radioSerilRAW.TabIndex = 1;
            this.radioSerilRAW.TabStop = true;
            this.radioSerilRAW.Text = "RAW";
            this.radioSerilRAW.UseVisualStyleBackColor = true;
            this.radioSerilRAW.CheckedChanged += new System.EventHandler(this.radioSerilRAW_CheckedChanged);
            // 
            // radioSerilZIP
            // 
            this.radioSerilZIP.AutoSize = true;
            this.radioSerilZIP.Location = new System.Drawing.Point(3, 3);
            this.radioSerilZIP.Name = "radioSerilZIP";
            this.radioSerilZIP.Size = new System.Drawing.Size(42, 17);
            this.radioSerilZIP.TabIndex = 0;
            this.radioSerilZIP.Text = "ZIP";
            this.radioSerilZIP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioSerilZIP.UseVisualStyleBackColor = true;
            this.radioSerilZIP.CheckedChanged += new System.EventHandler(this.radioSerilZIP_CheckedChanged);
            // 
            // CustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(690, 650);
            this.sensors_groupBox.ResumeLayout(false);
            this.firstSensorNumber_tableLayoutPanel.ResumeLayout(false);
            this.firstSensorNumber_tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).EndInit();
            this.ast_groupBox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.main_tableLayoutPanel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.level_groupBox.ResumeLayout(false);
            this.level_tableLayoutPanel.ResumeLayout(false);
            this.level_tableLayoutPanel.PerformLayout();
            this.gb_SensorText.ResumeLayout(false);
            this.tlp_SensorText.ResumeLayout(false);
            this.tlp_SensorText.PerformLayout();
            this.gb_IncludeText.ResumeLayout(false);
            this.tlp_IncludeText.ResumeLayout(false);
            this.tlp_IncludeText.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.clangVersion_tlp.ResumeLayout(false);
            this.clangVersion_tlp.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableIsSerilZip.ResumeLayout(false);
            this.tableIsSerilZip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox sensors_groupBox;
        private System.Windows.Forms.Label firstSensorNumber_label;
        private System.Windows.Forms.NumericUpDown firstSensorNumber_numericUpDown;
        private System.Windows.Forms.GroupBox ast_groupBox;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.GroupBox level_groupBox;
        private System.Windows.Forms.RadioButton isLevel3_radioButton;
        private System.Windows.Forms.RadioButton isLevel2_radioButton;
        private System.Windows.Forms.TableLayoutPanel firstSensorNumber_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel level_tableLayoutPanel;
        private System.Windows.Forms.Label labelAstFileFolder;
        private System.Windows.Forms.TextBox astFileFolder;
        private System.Windows.Forms.Button browseAstFileFolder;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gb_SensorText;
        private System.Windows.Forms.TableLayoutPanel tlp_SensorText;
        private System.Windows.Forms.TextBox tb_SensorText;
        private System.Windows.Forms.GroupBox gb_IncludeText;
        private System.Windows.Forms.TableLayoutPanel tlp_IncludeText;
        private System.Windows.Forms.TextBox tb_IncludeText;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox tb_SourcePrefix;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel clangVersion_tlp;
        private System.Windows.Forms.RadioButton isClang3_radioButton;
        private System.Windows.Forms.RadioButton isClang4_radioButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableIsSerilZip;
        private System.Windows.Forms.RadioButton radioSerilRAW;
        private System.Windows.Forms.RadioButton radioSerilZIP;
    }
}
