﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXXAstTreeBuilder.AST.Scope;
using Store;

namespace IA.Plugins.Parsers.CXXParser
{
    public class AbstractScopeParser1
    {
        private Storage storage;
        private Stack<Class> classStack;
        private IFile currentFile;
        private FileSearcher fileSearcher;
        private FunctionDuplicateChecker funcDuplicateChecker;
        private string path;
        private HashSet<ulong> templateFunctionsIds; 

        public AbstractScopeParser1(Storage storage, IFile currentFile, FileSearcher fileSearcher, FunctionDuplicateChecker functionDuplicateChecker, string path, HashSet<ulong> templateFunctionsIds)
        {
            this.storage = storage;
            this.currentFile = currentFile;
            this.fileSearcher = fileSearcher;
            this.funcDuplicateChecker = functionDuplicateChecker;
            this.path = path;
            this.templateFunctionsIds = templateFunctionsIds;
            classStack = new Stack<Class>();
        }

        public void Parse(AbstractScope scope)
        {
            ParseInStack(scope);
            Debug.Assert(classStack.Count == 0);
        }

        void ParseInStack(AbstractScope scope)
        {
            if (scope is GlobalScope)
            {
                var globalScope = scope as GlobalScope;
#region Functions/Methods
                foreach (var functionDeclaration in globalScope.Functions)
                {
                    if (functionDeclaration.Body != null 
                        && functionDeclaration.Body.Start != null && functionDeclaration.Body.End != null &&
                        functionDeclaration.End != null && functionDeclaration.Start.FileName != functionDeclaration.End.FileName
                        && functionDeclaration.Body.Start.FileName == functionDeclaration.Body.End.FileName)
                    {
                        functionDeclaration.Start = functionDeclaration.Body.Start;
                    }
                    var location = fileSearcher.GetFileForLocation(functionDeclaration.Start.FileName, currentFile, path);
                    if (classStack.Count == 0)
                    {
                        var match = funcDuplicateChecker.Check(location, functionDeclaration.Name, (ulong) functionDeclaration.Start.Line, (ulong) functionDeclaration.Start.Column);
                        
                        if (match != null)
                        {
                            functionDeclaration.StorageFunction = match.Id;
                            if (functionDeclaration.Body != null && functionDeclaration.IsTemplate)
                            {
                                if (!templateFunctionsIds.Contains(match.Id)) templateFunctionsIds.Add(match.Id);
                            }
                            continue;
                        }

                        var func = storage.functions.AddFunction();
                        func.Name = functionDeclaration.Name;
                        if (functionDeclaration.ReturnType != null) func.FunctionReturnType = functionDeclaration.ReturnType;
                        if (functionDeclaration.Body == null)
                        {
                            func.AddDeclaration(location, (ulong) functionDeclaration.Start.Line, (ulong) functionDeclaration.Start.Column);
                        }
                        else
                        {
                            func.AddDefinition(location, (ulong) functionDeclaration.Start.Line, (ulong) functionDeclaration.Start.Column);
                            funcDuplicateChecker.Add(location, func);
                            if (functionDeclaration.IsTemplate && !templateFunctionsIds.Contains(func.Id)) templateFunctionsIds.Add(func.Id);
                        }
                        foreach (var definition in functionDeclaration.Definitions.Distinct())
                        {
                            var defLocation = fileSearcher.GetFileForLocation(definition.Start.FileName, currentFile, path);
                            func.AddDeclaration(defLocation, (ulong)definition.Start.Line, (ulong)definition.Start.Column);
                        }

                        functionDeclaration.StorageFunction = func.Id;
                    }
                    else
                    {
                        var func = classStack.Peek().AddMethod(false /*всем пофиг на этот флаг*/);
                        func.Name = functionDeclaration.Name;
                        if (functionDeclaration.ReturnType != null) func.FunctionReturnType = functionDeclaration.ReturnType;
                        if (functionDeclaration.Body == null)
                        {
                            func.AddDeclaration(location, (ulong)functionDeclaration.Start.Line, (ulong)functionDeclaration.Start.Column);
                        }
                        else
                        {
                            func.AddDefinition(location, (ulong)functionDeclaration.Start.Line, (ulong)functionDeclaration.Start.Column);
                            if (functionDeclaration.IsTemplate && !templateFunctionsIds.Contains(func.Id)) templateFunctionsIds.Add(func.Id);
                        }
                        functionDeclaration.StorageFunction = func.Id;
                    }
                }
                #endregion
#region Variables/Parameters
                foreach (var variableDeclaration in globalScope.Variables)
                {
                    var location = fileSearcher.GetFileForLocation(variableDeclaration.Start.FileName, currentFile, path);
                    if (classStack.Count == 0)
                    {
                        var vrbl = storage.variables.AddVariable();
                        vrbl.Name = variableDeclaration.Name;
                        vrbl.AddDefinition(location, (ulong) variableDeclaration.Start.Line, (ulong) variableDeclaration.Start.Column, null);
                        variableDeclaration.StorageVariable = vrbl.Id;
                    }
                    else
                    {
                        var vrbl = classStack.Peek().AddField(false /*всем на это тоже пофиг*/);
                        vrbl.Name = variableDeclaration.Name;
                        vrbl.AddDefinition(location, (ulong)variableDeclaration.Start.Line, (ulong)variableDeclaration.Start.Column, null);
                        variableDeclaration.StorageVariable = vrbl.Id;
                    }
                }
                #endregion
#region Classes
                foreach (var classDeclaration in globalScope.Classes)
                {
                    var classDecl = storage.classes.AddClass();
                    classDeclaration.StorageClass = classDecl.Id;
                    classDecl.Name = classDeclaration.Name;
                    if(classStack.Count != 0) classStack.Peek().AddInnerClass(classDecl);
                    classStack.Push(classDecl);
                    ParseInStack(classDeclaration.Body);
                    classStack.Pop();
                }
#endregion
            }
        }
    }
}
