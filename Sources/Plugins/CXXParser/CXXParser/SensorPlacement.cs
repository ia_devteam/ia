﻿using Store;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CXXAstTreeBuilder.AST.Location;
using CXXAstTreeBuilder.AST.Statements;
using FileOperations;
using IA.Extensions;

namespace IA.Plugins.Parsers.CXXParser
{
    public class SensorPlacement
    {
        public static List<IStatement> SkipSensorPlacement = new List<IStatement>(); 
        private Storage storage;
        private int insertionLevel;
        private int RetVarNumber = 0;
        private List<KeyValuePair<IFile, List<KeyValuePair<ulong, string>>>> globalMetBStatements;
        private HashSet<ulong> templateFunctionsIds;

        public SensorPlacement(Storage storage, int insertionLevel, Dictionary<IFile, List<KeyValuePair<ulong, string>>> globalMetBStatementsDictionary, HashSet<ulong> templateFunctionsIds)
        {
            this.storage = storage;
            this.insertionLevel = insertionLevel;
            pasteInfoList = new List<PasteInfo>();
            this.globalMetBStatements = new List<KeyValuePair<IFile, List<KeyValuePair<ulong, string>>>>();
            this.templateFunctionsIds = templateFunctionsIds;
            metStatements = new HashSet<SourceFileLocation>();
            //if(insertionLevel == 3) return; //no braces on 3rd level
            foreach (var ifileDictionary in globalMetBStatementsDictionary)
            {
                globalMetBStatements.Add(new KeyValuePair<IFile, List<KeyValuePair<ulong, string>>>(ifileDictionary.Key, ifileDictionary.Value));
            }
        }

        public void Paste()
        {
            PasteSensors(insertionLevel);
            FillInSensorsInfo();

            var groupedPaste = pasteInfoList.GroupBy(g => g.File).Where(g => g.Key.isPlacedInSources());
            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)3).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)groupedPaste.Count());
            int i = 0;
            foreach (var fileWithSensorsAndBraces in groupedPaste)
            {
                PasteStuff(fileWithSensorsAndBraces);
                Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)3).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++i);
            }

        }

        List<PasteInfo> pasteInfoList;
        private IFile currentPastingFile = null;

        private ulong currentMinOffset = 0;

        private void PasteSensors(int level = 2)
        {
            //формируем список функций
            List<KeyValuePair<IFunction, IFile>> functions = new List<KeyValuePair<IFunction, IFile>>();

            foreach (LocationFunctionPair lfp in storage.functions.EnumerateFunctionLocationsInSortOrder())
                if (lfp.location.GetFile().fileType.ContainsLanguages().Contains("C") || lfp.location.GetFile().fileType.ContainsLanguages().Contains("Cpp"))
                    if (lfp.function.Definition() != null)
                        functions.Add(new KeyValuePair<IFunction, IFile>(lfp.function, lfp.location.GetFile()));

            foreach (KeyValuePair<IFunction, IFile> pair in functions)
            {
                currentPastingFile = pair.Value;
                currentMinOffset = pair.Key.Definition().GetOffset();
                var function = pair.Key;
                if (level == 2 && function.EntryStatement != null)
                    PasterSecondLevel(function.EntryStatement);
                else if (level == 3 && function.EntryStatement != null)
                    PasterThirdLevel(function.EntryStatement);

                //FixEndings(function.EntryStatement, currentPastingFile);
            }
        }

        private HashSet<SourceFileLocation> metStatements = new HashSet<SourceFileLocation>(); 

        private void PasterSecondLevel(IStatement st, int level = 0)
        {
            if (st == null || st.SensorBeforeTheStatement != null)
                return;

            bool metReturn = st is IStatementReturn || st is IStatementBreak || st is IStatementContinue || st is IStatementYieldReturn || st is IStatementThrow;
            var checkLocation = st.FirstSymbolLocation != null ? new SourceFileLocation() { Column = (int)st.FirstSymbolLocation.GetColumn(), FileName = st.FirstSymbolLocation.GetFileID().ToString(), Line = (int)st.FirstSymbolLocation.GetLine() } : null;

            if (!SkipSensorPlacement.Contains(st) && st.FirstSymbolLocation.GetFile() == currentPastingFile && st.FirstSymbolLocation.GetOffset() >= currentMinOffset && !(st.FirstSymbolLocation != null && metStatements.Contains(checkLocation)))
            {
                if (metReturn && level > 0)
                {
                    storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                    metStatements.Add(checkLocation);
                    return; //больше не надо ничего вставлять - встретили ретурн
                }

                if (level == 0)
                {
                    storage.sensors.AddSensorBeforeStatement(st, Kind.START);
                }
                else
                {
                    storage.sensors.AddSensorBeforeStatement(st, Kind.INTERNAL);
                }
            }

            while (st != null)
            {
                checkLocation = st.FirstSymbolLocation != null ? new SourceFileLocation() {Column = (int)st.FirstSymbolLocation.GetColumn(), FileName = st.FirstSymbolLocation.GetFileID().ToString(), Line = (int)st.FirstSymbolLocation.GetLine() } : null;
                if (st.FirstSymbolLocation.GetFile() != currentPastingFile 
                    || SkipSensorPlacement.Contains(st) 
                    || st.FirstSymbolLocation.GetOffset() < currentMinOffset
                    || st.FirstSymbolLocation != null && metStatements.Contains(checkLocation))
                {
                    if (st.NextInLinearBlock != null)
                    {
                        st = st.NextInLinearBlock;
                        continue;
                    }
                    break;
                }

                

                if (st is IStatementOperational)
                {
                    var seekPreviousStatementInSameFile = st;
                    while ((seekPreviousStatementInSameFile = seekPreviousStatementInSameFile.PreviousInLinearBlock) != null)
                    {
                        if (seekPreviousStatementInSameFile.FirstSymbolLocation.GetFile() == st.FirstSymbolLocation.GetFile())
                        {
                            break;
                        }
                    }

                    if (seekPreviousStatementInSameFile != null && seekPreviousStatementInSameFile is IStatementOperational)
                    {
                        if (st.NextInLinearBlock != null)
                        {
                            st = st.NextInLinearBlock;
                            continue;
                        }
                        else
                        {
                            break;
                        }
                        //continue; //fixing bug when sensor is in the function call
                    }
                }

                switch (st.Kind)
                {
                    case ENStatementKind.STBreak:
                        metReturn = true;
                        break;
                    case ENStatementKind.STContinue:
                        metReturn = true;
                        break;
                    case ENStatementKind.STDoAndWhile:
                        PasterSecondLevel((st as IStatementDoAndWhile).Body, level + 1);
                        break;
                    case ENStatementKind.STFor:
                        PasterSecondLevel((st as IStatementFor).Body, level + 1);
                        break;
                    case ENStatementKind.STForEach:
                        PasterSecondLevel((st as IStatementForEach).Body, level + 1);
                        break;
                    case ENStatementKind.STGoto:
                        break;
                    case ENStatementKind.STIf:
                        PasterSecondLevel((st as IStatementIf).ThenStatement, level + 1);
                        PasterSecondLevel((st as IStatementIf).ElseStatement, level + 1);
                        //if ((st as IStatementIf).ElseStatement is IStatementIf && ((st as IStatementIf).ElseStatement as IStatementIf).SensorBeforeTheStatement != null)
                        //    storage.sensors.RemoveSensor(((st as IStatementIf).ElseStatement as IStatementIf).SensorBeforeTheStatement.ID);
                        break;
                    case ENStatementKind.STOperational:
                        break;
                    case ENStatementKind.STReturn:
                        metReturn = true;
                        break;
                    case ENStatementKind.STSwitch:
                        foreach (var switchElem in (st as IStatementSwitch).Cases())
                            PasterSecondLevel(switchElem.Value, level + 1);

                        PasterSecondLevel((st as IStatementSwitch).DefaultCase(), level + 1);
                        break;
                    case ENStatementKind.STThrow:
                        metReturn = true;
                        break;
                    case ENStatementKind.STTryCatchFinally:
                        PasterSecondLevel((st as IStatementTryCatchFinally).TryBlock, level + 1);

                        foreach (var catchElem in (st as IStatementTryCatchFinally).Catches())
                            PasterSecondLevel(catchElem, level + 1);

                        PasterSecondLevel((st as IStatementTryCatchFinally).FinallyBlock, level + 1);
                        break;
                    case ENStatementKind.STUsing:
                        PasterSecondLevel((st as IStatementUsing).Body, level + 1);
                        break;
                    case ENStatementKind.STYieldReturn:
                        metReturn = true;
                        break;
                    default:
                        throw new Exception("Неверно задан тип стейтмента.");
                }

                checkLocation = st.FirstSymbolLocation != null ? new SourceFileLocation() { Column = (int)st.FirstSymbolLocation.GetColumn(), FileName = st.FirstSymbolLocation.GetFileID().ToString(), Line = (int)st.FirstSymbolLocation.GetLine() } : null;

                if (st.SensorBeforeTheStatement == null && !(st.FirstSymbolLocation != null && metStatements.Contains(checkLocation)))
                    storage.sensors.AddSensorBeforeStatement(st, metReturn ? Kind.FINAL : Kind.INTERNAL);

                metStatements.Add(checkLocation);

                if (metReturn)
                    break;

                if (st.NextInLinearBlock != null)
                    st = st.NextInLinearBlock;
                else
                    break;
            }

            if (!metReturn && level == 0 && st.FirstSymbolLocation.GetFile() == currentPastingFile && !SkipSensorPlacement.Contains(st) && st.SensorBeforeTheStatement == null)
            {
                checkLocation = st.FirstSymbolLocation != null ? new SourceFileLocation() { Column = (int)st.FirstSymbolLocation.GetColumn(), FileName = st.FirstSymbolLocation.GetFileID().ToString(), Line = (int)st.FirstSymbolLocation.GetLine() } : null;
                if (!metStatements.Contains(checkLocation))
                {
                    storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                    metStatements.Add(checkLocation);
                }
            }

            /*if (!metReturn && level == 0 && st.LastSymbolLocation != null && st.LastSymbolLocation.GetFile() == currentPastingFile)
            {
                if (!SkipSensorPlacement.Contains(st))
                {
                    checkLocation = st.LastSymbolLocation != null ? new SourceFileLocation() { Column = (int)st.LastSymbolLocation.GetColumn(), FileName = st.LastSymbolLocation.GetFileID().ToString(), Line = (int)st.LastSymbolLocation.GetLine() } : null;
                    if (!(st.PreviousInLinearBlock == null && st.FirstSymbolLocation.GetOffset() == st.LastSymbolLocation.GetOffset()) && !metStatements.Contains(checkLocation))
                    {
                        if (!(st.PreviousInLinearBlock == null && st.FirstSymbolLocation.GetLine() != st.LastSymbolLocation.GetLine()))
                        {
                            IStatement statement = storage.statements.AddStatementOperational(st.LastSymbolLocation.GetLine(), st.LastSymbolLocation.GetColumn(),
                                st.LastSymbolLocation.GetFile());
                            st.NextInLinearBlock = statement;
                            storage.sensors.AddSensorBeforeStatement(statement, Kind.FINAL);
                            metStatements.Add(checkLocation);
                            //metStatements.Add(new KeyValuePair<Location, Location>(statement.FirstSymbolLocation, statement.LastSymbolLocation));
                        }
                    }
                }
                else
                {
                    
                }
            }*/

        }

        private void PasterThirdLevel(IStatement st, int level = 0)
        {
            if (st == null || st.SensorBeforeTheStatement != null)
                return;
            var checkLocation = st.FirstSymbolLocation != null ? new SourceFileLocation() { Column = (int)st.FirstSymbolLocation.GetColumn(), FileName = st.FirstSymbolLocation.GetFileID().ToString(), Line = (int)st.FirstSymbolLocation.GetLine() } : null;
            //ставим датчики перед каждым ветвлением и в конец, если последний стейтмент - не ретурн
            if (level == 0 && st.FirstSymbolLocation.GetFile() == currentPastingFile && !SkipSensorPlacement.Contains(st))
                storage.sensors.AddSensorBeforeStatement(st, Kind.START);

            bool metReturn = st is IStatementReturn || st is IStatementYieldReturn || st is IStatementThrow;

            if (metReturn)
            {
                if (level > 0 && st.FirstSymbolLocation.GetFile() == currentPastingFile && !SkipSensorPlacement.Contains(st))
                {
                    storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                    metStatements.Add(checkLocation);
                }
                return; //больше не надо ничего вставлять - встретили ретурн
            }

            while (st != null)
            {
                checkLocation = st.FirstSymbolLocation != null ? new SourceFileLocation() { Column = (int)st.FirstSymbolLocation.GetColumn(), FileName = st.FirstSymbolLocation.GetFileID().ToString(), Line = (int)st.FirstSymbolLocation.GetLine() } : null;
                if (st.FirstSymbolLocation.GetFile() != currentPastingFile || SkipSensorPlacement.Contains(st) || st.FirstSymbolLocation.GetOffset() < currentMinOffset)
                {
                    if (st.NextInLinearBlock != null)
                    {
                        st = st.NextInLinearBlock;
                        continue;
                    }
                    break;
                }

                switch (st.Kind)
                {
                    case ENStatementKind.STBreak:
                        metReturn = true;
                        break;
                    case ENStatementKind.STContinue:
                        metReturn = true;
                        break;
                    case ENStatementKind.STDoAndWhile:
                        PasterThirdLevel((st as IStatementDoAndWhile).Body, level + 1);
                        break;
                    case ENStatementKind.STFor:
                        PasterThirdLevel((st as IStatementFor).Body, level + 1);
                        break;
                    case ENStatementKind.STForEach:
                        PasterThirdLevel((st as IStatementForEach).Body, level + 1);
                        break;
                    case ENStatementKind.STGoto:
                        break;
                    case ENStatementKind.STIf:
                        PasterThirdLevel((st as IStatementIf).ThenStatement, level + 1);
                        PasterThirdLevel((st as IStatementIf).ElseStatement, level + 1);
                        break;
                    case ENStatementKind.STOperational:
                        break;
                    case ENStatementKind.STReturn:
                        metReturn = true;
                        storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                        break;
                    case ENStatementKind.STSwitch:
                        foreach (var switchElem in (st as IStatementSwitch).Cases())
                            PasterThirdLevel(switchElem.Value, level + 1);

                        PasterThirdLevel((st as IStatementSwitch).DefaultCase(), level + 1);
                        break;
                    case ENStatementKind.STThrow:
                        metReturn = true;
                        if (!metStatements.Contains(checkLocation))
                        {
                            storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                            metStatements.Add(checkLocation);
                        }
                        break;
                    case ENStatementKind.STTryCatchFinally:
                        PasterThirdLevel((st as IStatementTryCatchFinally).TryBlock, level + 1);

                        foreach (var catchElem in (st as IStatementTryCatchFinally).Catches())
                            PasterThirdLevel(catchElem, level + 1);

                        PasterThirdLevel((st as IStatementTryCatchFinally).FinallyBlock, level + 1);
                        break;
                    case ENStatementKind.STUsing:
                        PasterThirdLevel((st as IStatementUsing).Body, level + 1);
                        break;
                    case ENStatementKind.STYieldReturn:
                        metReturn = true;
                        if (!metStatements.Contains(checkLocation))
                        {
                            storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                            metStatements.Add(checkLocation);
                        }
                    break;
                    default:
                        throw new Exception("Неверно задан тип стейтмента.");
                }

                if (st.NextInLinearBlock != null)
                    st = st.NextInLinearBlock;
                else
                    break;
            }

            if (!metReturn && level == 0 && st.FirstSymbolLocation.GetFile() == currentPastingFile && !SkipSensorPlacement.Contains(st) && st.SensorBeforeTheStatement == null)
            {
                checkLocation = st.FirstSymbolLocation != null ? new SourceFileLocation() { Column = (int)st.FirstSymbolLocation.GetColumn(), FileName = st.FirstSymbolLocation.GetFileID().ToString(), Line = (int)st.FirstSymbolLocation.GetLine() } : null;
                if (!metStatements.Contains(checkLocation))
                {
                    storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                    metStatements.Add(checkLocation);
                }
            }

            /*if (!metReturn && level == 0 && st.FirstSymbolLocation.GetFile() == currentPastingFile && !SkipSensorPlacement.Contains(st))
            {
                if (st.LastSymbolLocation != null)
                {
                    IStatement statement = storage.statements.AddStatementOperational(st.LastSymbolLocation.GetLine(),
                        st.LastSymbolLocation.GetColumn(),
                        st.LastSymbolLocation.GetFile());
                    st.NextInLinearBlock = statement;
                    storage.sensors.AddSensorBeforeStatement(statement, Kind.FINAL);
                }
            }*/
        }
        
        private void FillInSensorsInfo()
        {
            var sensorsList = storage.sensors.EnumerateSensors().Where(g => g.GetBeforeStatement() != null && g.GetBeforeStatement().FirstSymbolLocation != null && (g.GetBeforeStatement().FirstSymbolLocation.GetFile().fileType.ContainsLanguages().Contains("C") || g.GetBeforeStatement().FirstSymbolLocation.GetFile().fileType.ContainsLanguages().Contains("Cpp"))).ToList();
            foreach (var sensor in sensorsList)
            {
                if (sensor.GetBeforeStatement() != null)
                {
                    var offset = sensor.GetBeforeStatement().FirstSymbolLocation.GetOffset();
                    var file = sensor.GetBeforeStatement().FirstSymbolLocation.GetFile();
                    if (file.FileExistence != enFileExistence.EXTERNAL)
                    {
                        var reader = new AbstractReader(file.FullFileName_Original);
                        var testOffset = reader.getOffset((int)sensor.GetBeforeStatement().FirstSymbolLocation.GetLine(), (int)sensor.GetBeforeStatement().FirstSymbolLocation.GetColumn(), Encoding.ASCII);
                        if (testOffset != -1)
                        {
                            offset = (ulong) testOffset;
                        }
                        else
                        {
                            offset = (ulong)reader.getOffset((int)sensor.GetBeforeStatement().FirstSymbolLocation.GetLine(), (int)sensor.GetBeforeStatement().FirstSymbolLocation.GetColumn() - 1, Encoding.ASCII);
                        }
                    }

                    var sensorInfo = new PasteInfo()
                    {
                        File = sensor.GetBeforeStatement().FirstSymbolLocation.GetFile(),
                        Row = sensor.GetBeforeStatement().FirstSymbolLocation.GetLine(),
                        Column = sensor.GetBeforeStatement().FirstSymbolLocation.GetColumn(),
                        Offset = offset,
                        isReturn = sensor.GetBeforeStatement() is IStatementReturn,
                        Paste = PluginSettings.SensorText.Replace("#", sensor.ID.ToString()),
                        function = sensor.GetFunction(),
                        isSensor = true,
                        iaSensor = sensor
                    };
                    pasteInfoList.Add(sensorInfo);
                }
            }

            foreach (var globalMetBStatementFile in globalMetBStatements)
            {
                if (insertionLevel == 3)
                {
                    for (var i = 0; i < globalMetBStatementFile.Value.Count; i += 2)
                    {
                        var first = globalMetBStatementFile.Value[i].Key;
                        var second = globalMetBStatementFile.Value[i + 1].Key;
                        if (pasteInfoList.Any(g => g.Offset >= first && g.Offset <= second && g.Paste.Contains("(")))
                            if (pasteInfoList.Any(g => g.Offset >= first && g.Offset <= second && g.Paste.Contains("(")))
                            {
                                var braceInfo = new PasteInfo()
                                {
                                    File = globalMetBStatementFile.Key,
                                    Row = 0,
                                    Column = 0,
                                    Offset = globalMetBStatementFile.Value[i].Key,
                                    isReturn = false,
                                    Paste = globalMetBStatementFile.Value[i].Value,
                                    function = null,
                                    isSensor = false
                                };
                                pasteInfoList.Add(braceInfo);

                                braceInfo = new PasteInfo()
                                {
                                    File = globalMetBStatementFile.Key,
                                    Row = 0,
                                    Column = 0,
                                    Offset = globalMetBStatementFile.Value[i + 1].Key,
                                    isReturn = false,
                                    Paste = globalMetBStatementFile.Value[i + 1].Value,
                                    function = null,
                                    isSensor = false
                                };
                                pasteInfoList.Add(braceInfo);
                            }
                    }
                }
                else
                {
                    foreach (var braces in globalMetBStatementFile.Value)
                    {
                        var braceInfo = new PasteInfo()
                        {
                            File = globalMetBStatementFile.Key,
                            Row = 0,
                            Column = 0,
                            Offset = braces.Key,
                            isReturn = false,
                            Paste = braces.Value,
                            function = null,
                            isSensor = false
                        };
                        pasteInfoList.Add(braceInfo);
                    }
                }
            }
            pasteInfoList = pasteInfoList.OrderByDescending(g => g.Offset).ToList();
        }

        

        private string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        private IStatement GetLastStatement(IStatement entry)
        {
            IStatement lastSameBlock = entry;
            while (entry.NextInLinearBlock != null)
            {
                entry = entry.NextInLinearBlock;
                if (entry.LastSymbolLocation.GetFileID() == lastSameBlock.FirstSymbolLocation.GetFileID()) lastSameBlock = entry;
            }
            return lastSameBlock;
        }

        private void PasteStuff(IGrouping<IFile, PasteInfo> toPaste)
        {
            try
            {
                var sourceWithSensorsFolder = Store.Files.CanonizeFileName(Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "CXX"));
                var originalPath = Store.Files.CanonizeFileName(storage.appliedSettings.FileListPath); //storage.WorkDirectory.GetSubDirectory(WorkDirectory.enSubDirectories.SOURCES_ORIGINAL);
                var oldFile = Path.Combine(originalPath, toPaste.Key.FileNameForReports);
                var aReaderObject = new FileOperations.AbstractReader(oldFile);

                var fileContents = aReaderObject.getText();

                foreach (var pasteInfo in toPaste)
                {
                    bool IsPasteBlocked = false;    // блокируем вставку в неправильное место
                    bool IsDefine = false;          // признак нахождения датчика внутри макроса
                    var paste = pasteInfo;
                    if (paste.Offset > (ulong) fileContents.Length) continue;
                    var endOfPrevLine = fileContents.Substring(0, (int) paste.Offset - 1).LastIndexOf('\n');
                    var currentLine = fileContents.Substring(endOfPrevLine, (int) paste.Offset - endOfPrevLine);
                    if(Regex.Match(currentLine, "#\\s*define").Success || fileContents[endOfPrevLine - 1] == '\\') continue;
                    if (!paste.isReturn)
                    {
                        //ищем перенос строки или //
                        bool beginWord = false;
                        var word = "";
                        if (paste.Paste.EndsWith("}"))
                        {
                            if (fileContents[(int) paste.Offset] == '"')
                            {
                                paste.Offset = SearchForEnd(paste, fileContents, '"');
                            }
                            else
                            {
                                var arrayOfNormal = new List<char>() {'\t', '\n', '\r', ' ', '{', '}'};
                                var arrayOfEndings = new List<char>() {'\n', '\r', '{', '}'};
                                for (int j = (int) paste.Offset; j < fileContents.Length; j++)
                                {
                                    var ch = fileContents[j];
                                    if (beginWord) word = word + ch;
                                    if (ch == ';')
                                    {
                                        if (!beginWord)
                                        {
                                            paste.Offset = (ulong) j + 1;
                                            break;
                                        }
                                        var testWord = word.Replace("\\\"", "#").Replace("\\'", "#");
                                        var testWord2 = testWord.Replace("\"", "").Replace("\'", "");
                                        var diff = testWord.Length - testWord2.Length;
                                        if (diff == 0 || diff == 2)
                                        {
                                            paste.Offset = (ulong) j + 1;
                                            break;
                                        }
                                    }
                                    if (arrayOfEndings.Contains(ch))
                                    {
                                        //дошли до начала строки
                                        if (!beginWord) break; // все хорошо
                                        paste.Offset = (ulong) j;
                                        break;
                                    }

                                    if (arrayOfNormal.Contains(ch))
                                    {
                                        continue;
                                    }

                                    if (!beginWord)
                                    {
                                        beginWord = true;
                                        word = ch.ToString();
                                    }
                                }
                            }
                            if (fileContents[(int) paste.Offset - 1] != ';' && fileContents[(int) paste.Offset - 1] != '}')
                            {
                                paste.Paste = ";" + paste.Paste;
                            }

                        }
                        else
                        {
                            if (!paste.Paste.StartsWith("{"))
                            {
                                if (fileContents[(int) paste.Offset] == '\n')
                                {
                                    paste.Offset++;
                                } else if (fileContents[(int) paste.Offset] == '\'' || fileContents[(int) paste.Offset] == '"')
                                {
                                    paste.Offset = SearchForEnd(paste, fileContents, fileContents[(int) paste.Offset]);
                                }
                                else
                                {
                                    var arrayOfNormal = new List<char>() {'\t', '\n', '\r', ' ', '{', '}', ';', '/'};
                                    // ':' - чтобы вставлять внутрь блока switch-case, иначе вне его
                                    var arrayOfEndings = new List<char>() {'\n', '\r', ';', '{', '}', ';', '/', ':', ')'};

                                    for (int j = (int) paste.Offset - 1; j >= 0; j--)
                                    {
                                        var ch = fileContents[j];

                                        if (!(j >= 1 && ch == ':' && (fileContents[j - 1] == ':' || fileContents[j + 1] == ':')))
                                        {

                                            if (arrayOfEndings.Contains(ch))
                                            {
                                                //дошли до начала строки
                                                if (!beginWord) break; // все хорошо
                                                if (beginWord && word.Trim() == "#endif")
                                                {
                                                    paste.Offset++;
                                                    break;
                                                }
                                                if (beginWord && (word.Trim() == "do" || word.Trim() == "else")) break;

                                                if (paste.Paste.StartsWith("{") && toPaste.Any(x => x.Offset == paste.Offset && x.Paste != paste.Paste))
                                                {
                                                    paste.Offset = (ulong) j - 1;
                                                }
                                                else
                                                {
                                                    paste.Offset = (ulong) j + 1;
                                                }
                                                break;
                                            }
                                        }
                                        if (beginWord) word = ch.ToString() + word;
                                        if (arrayOfNormal.Contains(ch))
                                        {
                                            continue;
                                        }

                                        if (!beginWord)
                                        {
                                            beginWord = true;
                                            word = ch.ToString();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // блокировка не вставки датчика перед do / ... while(0) в макросе
                    int k = (int)paste.Offset;
                    if (fileContents[k] == 'd' && fileContents[k + 1] == 'o')
                        do
                        {
                            if (fileContents[--k] == '\n')
                                if (fileContents[k - 1] == '\\')
                                    IsPasteBlocked =
                                        true; // конец строки, но мы в многострочном макросе
                                else
                                    break;
                        } while (!IsPasteBlocked);
                    // здесь будем пытаться понять что мы внутри макроса:
                    // если рядом со следующим '\n' есть символ '\'
                    // если в подстроке от '\n' до '\n' содержится #define 
                    int l = (int)paste.Offset;
                    k = (int)paste.Offset;
                    while (fileContents[--k] != '\n');
                    while (char.IsWhiteSpace(fileContents[--l]));
                    if (fileContents[k - 1] == '\\' && fileContents[l] == ')')
                        IsPasteBlocked = true;
                    else
                    {
                        int e = (int)paste.Offset;
                        while (fileContents[e++] != '\n' && e < fileContents.Length);
                        string sdef = fileContents.Substring(k, e - k);
                        IsDefine = sdef.Contains("#define");
                        if (IsDefine && sdef.Contains("\\\n") && sdef.Contains("do "))
                            IsPasteBlocked = true;
                        sdef = null;
                    }

                    if (!paste.isReturn || paste.function.FunctionReturnType.Trim() == "void" || paste.function.FunctionReturnType.Trim() == "" || templateFunctionsIds.Contains(paste.function.Id)) // bug trim
                    {
                        if (paste.isReturn && paste.function.FunctionReturnType.Trim() == "")
                        {
                            Monitor.Log.Warning(PluginSettings.Name, string.Format("Missing return type for function {0} at file {1} at offset {2}", paste.function.FullName, toPaste.Key.FileNameForReports, paste.Offset));
                        }
                        if(insertionLevel == 3 && paste.isReturn && paste.iaSensor.GetBeforeStatement().AboveStatement != null) continue;

                        if (!IsPasteBlocked)
                        {
                            if (fileContents[(int) paste.Offset] == '#')
                                paste.Paste = paste.Paste + "\n";
                            if (fileContents[(int) paste.Offset] == ';')
                                paste.Offset++;
                            if (paste.Paste.Contains("}") && IsDefine) // FIXME: добавить признак в PasteInfo датчик или скобка
                            {
                                int u = (int) paste.Offset;
                                while (Char.IsWhiteSpace(fileContents[--u]));
                                if (fileContents[u] == ')')
                                    paste.Paste = ";" + paste.Paste;
                            }
                            fileContents = fileContents.Insert((int) paste.Offset, paste.Paste);
                        }
                    }
                    else
                    {
                        if (fileContents.IndexOf("return;", (int) paste.Offset - 1) == (int)paste.Offset)
                        {
                            fileContents = fileContents.Insert((int)paste.Offset, paste.Paste);
                            continue;
                        }
                        var retEndIndex = fileContents.IndexOf(";", (int) paste.Offset);
                        var retCommand = fileContents.Substring((int) paste.Offset, retEndIndex - (int) paste.Offset + 1);
                        if (retCommand.Contains("\"") && !(retCommand.Contains("\\\"") || retCommand.Contains("'\""))
                            || retCommand.Contains("\'")
                            /*&& ((retCommand.Length - retCommand.Replace("\"", "").Length) %2 != 0)*/) // TODO create normal algorythm
                        {
                            var pattern = PluginSettings.SensorText.Substring(0, PluginSettings.SensorText.IndexOf("(") + 1);
                            var end = fileContents.IndexOf(pattern, (int) paste.Offset);
                            if (end == -1) end = fileContents.Length;
                            bool inString = false;
                            bool inChar = false;
                            bool inComment = false;
                            bool inMultilineComment = false;
                            retEndIndex = -1;
                            for (var i = (int)paste.Offset + 1; i < end; i++)
                            {
                                if (fileContents[i] == ';' && !inString && !inChar && !inComment && !inMultilineComment)
                                {
                                    retEndIndex = i;
                                    retCommand = fileContents.Substring((int)paste.Offset, retEndIndex - (int)paste.Offset + 1);
                                    break;
                                }
                                if (inString || inChar)
                                {
                                    if (fileContents[i] == '\\')
                                    {
                                        i++;
                                        continue;
                                    }
                                    if(fileContents[i] != '"' && fileContents[i] != '\'') continue;
                                }
                                if (fileContents[i] == '"' && !inComment && !inMultilineComment)
                                {
                                    if (inString)
                                    {
                                        inString = false;
                                        continue;
                                    }
                                    if (!inChar)
                                    {
                                        inString = true;
                                        continue;
                                    }
                                }
                                if (fileContents[i] == '\'' && !inComment && !inMultilineComment)
                                {
                                    if (inChar)
                                    {
                                        inChar = false;
                                        continue;
                                    }
                                    if (!inString)
                                    {
                                        inChar = true;
                                        continue;
                                    }
                                }
                                if (fileContents[i] == '/' && (i + 1 < fileContents.Length - 1) && !inComment && !inMultilineComment)
                                {
                                    if (fileContents[i + 1] == '/')
                                    {
                                        inComment = true;
                                        continue;
                                    }
                                    if (fileContents[i + 1] == '*')
                                    {
                                        inMultilineComment = true;
                                    }
                                }
                                if (inComment && fileContents[i] == '\n')
                                {
                                    inComment = false;
                                    continue;
                                }
                                if (inMultilineComment && fileContents[i] == '*' && (i + 1 < fileContents.Length - 1) && fileContents[i + 1] == '/')
                                {
                                    inMultilineComment = false;
                                    continue;
                                }
                            }

                            if (retEndIndex == -1)
                            {
                                Debugger.Break();
                                continue;
                            }
                        }
                        if (retCommand.IndexOf("return ") == -1)
                        {
                            fileContents = fileContents.Insert((int)paste.Offset, paste.Paste);
                            continue;
                        }
                        var retOperand = ReplaceFirst(retCommand.Trim(), "return", "").Trim();
                        var newRetCommand = string.Format("{{{0} ReplaceReturn{1} = {2} {3} return ReplaceReturn{1};}}", paste.function.FunctionReturnType, RetVarNumber++, retOperand, paste.Paste);
                        fileContents = fileContents.Substring(0, (int) paste.Offset) + newRetCommand + fileContents.Substring((int) paste.Offset + retCommand.Length);
                    }
                }

                string IncludeText = PluginSettings.IncludeText + PluginSettings.hashVersion;

                if (toPaste.Count() > 0)
                {
                    var firstOffset = toPaste.Last().Offset;
                    var source = fileContents.Substring(0, (int)firstOffset);
                    var lastIncludeOffset = source.LastIndexOf("#include");
                    if (lastIncludeOffset == -1)
                    {
                        fileContents = IncludeText + "\r\n" + fileContents;
                    }
                    else
                    {
                        var lines = source.Split(new char[] {'\n'});
                        var counter = 0;
                        var index = 0;
                        var found = false;
                        foreach (var line in lines)
                        {
                            if (line.Trim().StartsWith("#include") && counter == 0)
                            {
                                index += line.Length + 1;
                                found = true;
                                break;
                            }
                            index += line.Length + 1;
                            if (line.Trim().StartsWith("#ifdef") || line.Trim().StartsWith("#ifndef"))
                            {
                                counter++;
                                continue;
                            }
                            if (line.Trim().StartsWith("#endif"))
                            {
                                counter--;
                            }
                        }
                        if (found)
                        {
                            fileContents = fileContents.Insert(index, IncludeText + "\r\n");
                        }
                        else
                        {
                            fileContents = IncludeText + "\r\n" + fileContents;
                        }
                    }
                }

                var newFile = Path.Combine(sourceWithSensorsFolder, toPaste.Key.FileNameForReports);

                var directory = (new FileInfo(newFile)).Directory.FullName;

                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                aReaderObject.writeTextToFile(newFile, fileContents);
            }

            catch (Exception)
            {
                
            }
        }

        private static ulong SearchForEnd(PasteInfo paste, string fileContents, char delim)
        {
            bool searchForEnd = false;
            for (int i = (int) paste.Offset + 1; i < fileContents.Length; i++)
            {
                if (!searchForEnd)
                {
                    if (fileContents[i] == '\\')
                    {
                        i ++;
                        continue;
                    }
                    if (fileContents[i] == delim)
                    {
                        searchForEnd = true;
                    }
                }
                if (searchForEnd && (fileContents[i] == ';' || fileContents[i] == '\n'))
                {
                    return (ulong) i + 1;
                }
            }
            return paste.Offset;
        }
    }

    internal struct PasteInfo
    {
        public string Paste;
        public ulong Offset;
        public ulong Row;
        public ulong Column;
        public IFile File;
        public bool isReturn;
        public IFunction function;
        public bool isSensor;
        public Sensor iaSensor;
    }

    //// Структура описывающая один элемент(сенсор) для вставки
    //internal struct SensorInfo
    //{
    //    public ulong id;
    //    public string file;
    //    public int line;
    //    public int column;
    //    public ulong offset;
    //    public bool isReturn;
    //};

    /// <summary>
    /// Класс описывающий координаты пары скобок
    /// </summary>
    internal class BracesCoord
    {
        public ulong sRow;
        public ulong sColumn;
        public ulong eRow;
        public ulong eColumn;
        public ulong sOffset;
        public ulong eOffset;

        public BracesCoord(ulong sRow, ulong sColumn, ulong sOffset, ulong eRow, ulong eColumn, ulong eOffset)
        {
            this.sRow = sRow;
            this.sColumn = sColumn;
            this.sOffset = sOffset;
            this.eRow = eRow;
            this.eColumn = eColumn;
            this.eOffset = eOffset;
        }
    };

    /// <summary>
    /// Класс описывающий координаты реторна
    /// </summary>
    internal class ReturnCoord : IComparable<ReturnCoord>
    {
        public ulong sRow, sColumn, eRow, eColumn;
        public bool isEmpty;

        public ReturnCoord(ulong sRow, ulong sColumn, ulong eRow, ulong eColumn, bool isEmpty)
        {
            this.sRow = sRow;
            this.sColumn = sColumn;
            this.eRow = eRow;
            this.eColumn = eColumn;
            this.isEmpty = isEmpty;
        }

        int IComparable<ReturnCoord>.CompareTo(ReturnCoord coord)
        {
            if (sRow < coord.sRow)
                return 1;
            if (sRow > coord.sRow)
                return -1;
            if (sRow == coord.sRow)
            {
                if (sColumn < coord.sColumn)
                    return 1;
                if (sColumn > coord.sColumn)
                    return -1;
            }
            return 0;
        }
    };
}
