﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IA.Plugins.Parsers.CommonUtils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using IA.Plugins.Parsers.CXXParser;
using IOController;
using Store;
using Store.Table;
using TestUtils;
using FileOperations;
using System.Text.RegularExpressions;

namespace TestCXXParser
{
    /// <summary>
    /// Класс для тестов парсера CXX
    /// </summary>
    [TestClass]
    public class TestCXXParser
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            #region TestUtilsCXX
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Список сообщений
            /// </summary>
            public List<string> Messages
            {
                get { return messages; }
            }
        }

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Путь до каталога с эталонными исходными текстами
        /// </summary>
        private string labSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонным дампами Хранилища
        /// </summary>
        private string dumpSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонными отчетами
        /// </summary>
        private string reportSampleDirectoryPath;

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными лабораторным текстам
        /// </summary>
        private const string LAB_SAMPLES_SUBDIRECTORY = @"CXXParser\Labs";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными дампами Хранилища
        /// </summary>
        private const string DUMP_SAMPLES_SUBDIRECTORY = @"CXXParser\Dump";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными отчетами
        /// </summary>
        private const string REPORT_SAMPLES_SUBDIRECTORY = @"CXXParser\Reports";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        private const ulong pluginID = Store.Const.PluginIdentifiers.CXX_PARSER;

        /// <summary>
        /// Перехватчик сообщений монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            labSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, LAB_SAMPLES_SUBDIRECTORY);
            dumpSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, DUMP_SAMPLES_SUBDIRECTORY);
            reportSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, REPORT_SAMPLES_SUBDIRECTORY);
        }

        /// <summary>
        /// Подготовка Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="testMaterialsPath">Путь до каталога с материалами.</param>
        /// <param name="sourcePostfixPart">Подпуть до каталога с исходными файлами.</param>
        /// <param name="firstSensorNumber">Номер первого датчика. Не может быть отрицательным.</param>
        /// <param name="astFilesPath">Глубина пути до 'ast' файлов должна совпадать с каталогом очищенных исходников.</param>
        /// <param name="sensorText">Текст датчика: SensorCall(#), Din_Go(#,№_pkg).</param>
        /// <param name="includeText">Путь до файла 'sensor.h', содержит реализацию датчика.</param>
        /// <param name="prefix">Префикс пути, по которому собирался пакет.'root/rpmbuild/BUILD/'</param>
        /// <param name="ClangVer">Версия Clang, с которой происходит работа. Актуальная - '4'.</param>
        /// <param name="IsSerilZip">true - сериализация класса AbstractScope в 'zip' архив.</param>
        /// <param name="hashVersion">для тестов задавать пустую строку, только через GUI для пользователей</param> 
        private static void CustomizeStorage(Storage storage, string testMaterialsPath, string sourcePostfixPart, 
            bool isLevel2, UInt64 firstSensorNumber, string astFilesPath, 
            string sensorText = "SensorCall(#);", string includeText = "#include \"/opt/sensor.h\"", 
            string prefix = "", int ClangVer = 4, bool IsSerilZip = false, string hashVersion = "" )
        {
            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
            TestUtilsClass.Run_IdentifyFileTypes(storage);

            SettingUpPlugin(storage, firstSensorNumber, isLevel2, Path.Combine(testMaterialsPath, astFilesPath), 
                sensorText, includeText, prefix, ClangVer, IsSerilZip, hashVersion);
        }

        /// <summary>
        /// Настройка плагина
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="firstSensorNumber">Номер первого датчика. Не может быть отрицательным.</param>
        /// <param name="isLevel2">true - 2-ой уровень.</param>
        /// <param name="astFilesPath">Глубина пути до 'ast' файлов должна совпадать с каталогом очищенных исходников.</param>
        /// <param name="sensorText">Текст датчика: SensorCall(#), Din_Go(#,№_pkg).</param>
        /// <param name="includeText">Путь до файла 'sensor.h', содержит реализацию датчика.</param>
        /// <param name="prefix">Префикс пути, по которому собирался пакет.'root/rpmbuild/BUILD/'</param>
        /// <param name="ClangVer">Версия Clang, с которой происходит работа. Актуальная - '4'.</param>
        /// <param name="IsSerilZip">true - сериализация класса AbstractScope в 'zip' архив.</param>
        /// <param name="hashVersion">для тестов задавать пустую строку, только через GUI для пользователей</param> 
        private static void SettingUpPlugin(Storage storage, UInt64 firstSensorNumber, 
            bool isLevel2, string astFilesPath, string sensorText, string includeText, 
            string prefix, int ClangVer = 4, bool IsSerilZip = false, string hashVersion = "")
        {
            IBufferWriter writer = WriterPool.Get();

            writer.Add(firstSensorNumber);
            writer.Add(isLevel2);
            writer.Add(astFilesPath);
            writer.Add(sensorText);
            writer.Add(includeText);
            writer.Add(prefix);
            writer.Add(ClangVer);
            writer.Add(IsSerilZip);
            writer.Add(hashVersion);
            storage.pluginSettings.SaveSettings(pluginID, writer);
        }

        /// <summary>
        /// Дамп Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <returns>Путь до каталога, куда был произведен дамп Хранилища</returns>
        private string Dump(Storage storage)
        {
            string dumpDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Dump");

            if (!DirectoryController.IsExists(dumpDirectoryPath))
                DirectoryController.Create(dumpDirectoryPath);

            storage.ClassesDump(dumpDirectoryPath);
            storage.FilesDump(dumpDirectoryPath);
            storage.FunctionsDump(dumpDirectoryPath);
            storage.VariablesDump(dumpDirectoryPath);

            return dumpDirectoryPath;
        }

        /// <summary>
        /// Проверка дампа Хранилища
        /// </summary>
        /// <param name="dumpDirectoryPath">Путь до каталога дампа в Хранилище. Не может быть пустым.</param>
        /// <param name="dumpSamplesDirectoryPath">Путь до каталога с эталонным дампом. Не может быть пустым.</param>
        private void CheckDump(string dumpDirectoryPath, string dumpSamplesDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(dumpDirectoryPath, dumpSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Дамп Хранилища не совпадает с эталонными.");
        }

        /// <summary>
        /// Проверка лабораторных исходных файлов
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="labSamplesDirectoryPath">Путь до каталога с эталонными лабораторными исходными текстами. Не может быть пустым.</param>
        private void CheckLabs(Storage storage, string labSamplesDirectoryPath)
        {
            string labsDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");

            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(labsDirectoryPath, labSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Исходные тексты со вставленными датчиками не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка ожидаемого сообщения в логе монитора
        /// </summary>
        /// <param name="message">Текст сообщения. Не может быть пустым.</param>
        private void CheckMessage(string message)
        {
            bool isContainMessage = listener.ContainsPart(message);

            Assert.IsTrue(isContainMessage, "Сообщение не совпадает с ожидаемым.");
        }

        /// <summary>
        /// Проверка отчетов
        /// </summary>
        /// <param name="xmlSampleDirectoryPath">Путь до каталога с эталонными отчетами. Не может быть пустым.</param>
        /// <param name="storageReportsDirectoryPath">Путь до каталога с отчетами в Хранилище. Не может быть пустым.</param>
        private void CheckXML(string xmlSampleDirectoryPath, string storageReportsDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_XML_Compare(storageReportsDirectoryPath, xmlSampleDirectoryPath);

            Assert.IsTrue(isEqual, "Отчеты не совпадают с эталонными.");
        }

        private void WashLabSources(string dirWithLabFiles, 
            string srgx_I = "SensorCall\\(\\d+\\)", string srgx_II = "ReplaceReturn\\d+",
            string rrgx_I = "SensorCall()", string rrgx_II = "ReplaceReturn")
        {
            string[] aLabFiles = Directory.GetFiles(dirWithLabFiles, "*", SearchOption.AllDirectories);
            foreach (string cFile in aLabFiles)
            {
                var aReaderObject = new AbstractReader(cFile);
                string fileContents = aReaderObject.getText();
                Regex rgx_I = new Regex(srgx_I);
                fileContents = rgx_I.Replace(fileContents, rrgx_I);
                Regex rgx_II = new Regex(srgx_II);
                fileContents = rgx_II.Replace(fileContents, rrgx_II);
                aReaderObject.writeTextToFile(cFile, fileContents);
            }
        }

        #endregion

        //
        // Ниже идут непосредственно тесты
        //

        [Ignore]
        [TestMethod]
        public void CXXParser_SimpleTest()
        {
            string sourcePostfixPart = @"Sources\CXX\TestOne";

            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                new CXXParser(), // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        @"Sources\CXX\TestOneAst");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"SimpleModeTestSources"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "SimpleModeTestSources"));

                    return true;
                }, // checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        [Ignore]
        [TestMethod]
        public void CXXParser_BrazzersTest()
        {
            string sourcePostfixPart = @"Sources\CXX\BSrc";

            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                new CXXParser(), // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        false,
                        1,
                        @"Sources\CXX\BAst", prefix: "/root/rpmbuild/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return false;
                    TestUtilsClass.Run_SensorTypeFixer(storage);

                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);

                    //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"SimpleModeTestSources"));
                    //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "SimpleModeTestSources"));

                    return false;
                }, // checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        [Ignore]
        [TestMethod]
        public void CXXParser_MacrosTest()
        {
            string sourcePostfixPart = @"Sources\CXX\BugsEvgeny";

            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                new CXXParser(), // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        @"Sources\CXX\BugsEvgenyAst");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);

                    //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"SimpleModeTestSources"));
                    //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "SimpleModeTestSources"));

                    return true;
                }, // checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        [Ignore]
        [TestMethod]
        public void CXXParser_TypeTest()
        {
            string sourcePostfixPart = @"Sources\CXX\TypeTest";

            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                new CXXParser(), // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        @"Sources\CXX\TypeTestAst");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);

                    //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"SimpleModeTestSources"));
                    //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "SimpleModeTestSources"));

                    return true;
                }, // checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }





        /// <summary>
        /// Регрессионный тест на пакете 'tiff' по 2-ому уровню
        /// </summary>

        [TestMethod]
        public void CXXParser_tiffRegressionTest2Level()
        {
            string sourcePostfixPart = @"Sources\CXX\tiffRegression";
            string astPath = @"Sources\CXX\tiffRegressionAst";
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                new CXXParser(), // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"/var/tmp/sensor.h\"",
                        "/root/rpmbuild/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"tiffRegressionLabs_II"));
                    string samplesPath = dumpSampleDirectoryPath + @"\tiffRegressionLabs_II\";
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\tiffRegression_II.xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\tiffRegression_II.xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, //checkreports
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Регрессионный тест на пакете 'tiff' по 3-ому уровню
        /// </summary>

        [TestMethod]
        public void CXXParser_tiffRegressionTest3Level()
        {
            string sourcePostfixPart = @"Sources\CXX\tiffRegression";
            string astPath = @"Sources\CXX\tiffRegressionAst";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        false,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"/var/tmp/sensor.h\"",
                        "/root/rpmbuild/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"tiffRegressionLabs_III"));
                    string samplesPath = dumpSampleDirectoryPath + @"\tiffRegressionLabs_III\";
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\tiffRegression_III.xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, // checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\tiffRegression_III.xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'IfElseStatements'
        /// </summary>

        [TestMethod]
        public void CXXParser_IfElseStatementsTest()
        {
            string short_name = "IfElseStatements";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'ForStatements'
        /// </summary>

        [TestMethod]
        public void CXXParser_ForStatementsTest()
        {
            string short_name = "ForStatements";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'SwitchStatement'
        /// </summary>

        [TestMethod]
        public void CXXParser_SwitchStatementTest()
        {
            string short_name = "SwitchStatement";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'DoStatement' ('WhileStatement')
        /// </summary>

        [TestMethod]
        public void CXXParser_DoStatementTest()
        {
            string short_name = "DoStatement";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }


        /// <summary>
        /// 'DoWhile'
        /// </summary>

        [TestMethod]
        public void CXXParser_DoWhileStatementTest()
        {
            string short_name = "DoWhileStatement";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Return'
        /// </summary>

        [TestMethod]
        public void CXXParser_ReturnStatementTest()
        {
            string short_name = "ReturnStatement";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'ReturnVariants' - только синтаксис языка, без h-файлов;
        /// для проверки корректного формирования 'ReplaceReturn'
        /// </summary>

        [TestMethod]
        public void CXXParser_ReturnVariantsTest()
        {
            string short_name = "ReturnVariants";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Goto'
        /// </summary>

        [TestMethod]
        public void CXXParser_GotoStatementTest()
        {
            string short_name = "GotoStatement";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Break'
        /// </summary>

        [TestMethod]
        public void CXXParser_BreakStatementTest()
        {
            string short_name = "BreakStatement";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Continue'
        /// </summary>

        [TestMethod]
        public void CXXParser_ContinueStatementTest()
        {
            string short_name = "ContinueStatement";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }


        /// <summary>
        /// 'Conditional inclusion'
        /// </summary>

        [TestMethod]
        public void CXXParser_CondIncludeMacrosTest()
        {
            string short_name = "CondIncludeMacros";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Filename and line information'
        /// </summary>

        [TestMethod]
        public void CXXParser_FileLineMacrosTest()
        {
            string short_name = "FileLineMacros";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }


        /// <summary>
        /// 'Replacing text macros'
        /// </summary>

        [TestMethod]
        public void CXXParser_ReplacingTextMacrosTest()
        {
            string short_name = "ReplacingTextMacros";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'StringConstants' - проверка НЕ вставки датчиков внутрь строковой константы 
        /// </summary>

        [TestMethod]
        public void CXXParser_StringConstantsTest()
        {
            string short_name = "StringConstants";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Define' - только синтаксис языка, без h-файлов
        /// </summary>

        [TestMethod]
        public void CXXParser_DefineTest()
        {
            string short_name = "Define";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }


        /// <summary>
        /// 'ThrowExpression'
        /// </summary>

        [TestMethod]
        public void CXXParser_ThrowExpressionTest()
        {
            string short_name = "ThrowExpression";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'NewDeleteOperator'
        /// </summary>

        [TestMethod]
        public void CXXParser_NewDeleteOperatorTest()
        {
            string short_name = "NewDeleteOperator";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'SizeofOperator'
        /// </summary>

        [TestMethod]
        public void CXXParser_SizeofOperatorTest()
        {
            string short_name = "SizeofOperator";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'ExplicitConversion'
        /// </summary>

        [TestMethod]
        public void CXXParser_ExplicitConversionTest()
        {
            string short_name = "ExplicitConversion";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'OperatorOverloading'
        /// </summary>

        [TestMethod]
        public void CXXParser_OperatorOverloadingTest()
        {
            string short_name = "OperatorOverloading";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'DeclaringFunctions'
        /// </summary>

        [TestMethod]
        public void CXXParser_DeclaringFunctionsTest()
        {
            string short_name = "DeclaringFunctions";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'VariadicArguments'
        /// </summary>

        [TestMethod]
        public void CXXParser_VariadicArgumentsTest()
        {
            string short_name = "VariadicArguments";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'InlineFunction'
        /// </summary>

        [TestMethod]
        public void CXXParser_InlineFunctionTest()
        {
            string short_name = "InlineFunction";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'OverloadedFunction'
        /// </summary>

        [TestMethod]
        public void CXXParser_OverloadedFunctionTest()
        {
            string short_name = "OverloadedFunction";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Namespace'
        /// </summary>

        [TestMethod]
        public void CXXParser_NamespaceTest()
        {
            string short_name = "Namespace";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'NamespaceAlias'
        /// </summary>

        [TestMethod]
        public void CXXParser_NamespaceAliasTest()
        {
            string short_name = "NamespaceAlias";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'RValueReference'
        /// </summary>

        [TestMethod]
        public void CXXParser_RValueReferenceTest()
        {
            string short_name = "RValueReference";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }


        /// <summary>
        /// 'LValueReference'
        /// </summary>

        [TestMethod]
        public void CXXParser_LValueReferenceTest()
        {
            string short_name = "LValueReference";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'LvaluesRvalueReference'
        /// </summary>
        [Ignore]
        [TestMethod]
        public void CXXParser_LvaluesRvalueReferenceTest()
        {
            string short_name = "LvaluesRvalueReference";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }


        /// <summary>
        /// 'EnumDeclaration'
        /// </summary>

        [TestMethod]
        public void CXXParser_EnumDeclarationTest()
        {
            string short_name = "EnumDeclaration";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'ArrayDeclaration'
        /// </summary>

        [TestMethod]
        public void CXXParser_ArrayDeclarationTest()
        {
            string short_name = "ArrayDeclaration";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'PointerDeclaration'
        /// </summary>

        [TestMethod]
        public void CXXParser_PointerDeclarationTest()
        {
            string short_name = "PointerDeclaration";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Class'
        /// </summary>

        [TestMethod]
        public void CXXParser_ClassTest()
        {
            string short_name = "Class";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'DefaultConstructors'
        /// </summary>

        [TestMethod]
        public void CXXParser_DefaultConstructorsTest()
        {
            string short_name = "DefaultConstructors";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Destructors'
        /// </summary>

        [TestMethod]
        public void CXXParser_DestructorsTest()
        {
            string short_name = "Destructors";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'BitField'
        /// </summary>

        [TestMethod]
        public void CXXParser_BitFieldTest()
        {
            string short_name = "BitField";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Abstract'
        /// </summary>

        [TestMethod]
        public void CXXParser_AbstractTest()
        {
            string short_name = "Abstract";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'UnionlikeClasses'
        /// </summary>

        [TestMethod]
        public void CXXParser_UnionlikeClassesTest()
        {
            string short_name = "UnionlikeClasses";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'TemplateFunction'
        /// </summary>

        [TestMethod]
        public void CXXParser_TemplateFunctionTest()
        {
            string short_name = "TemplateFunction";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'TemplateMember'
        /// </summary>

        [TestMethod]
        public void CXXParser_TemplateMemberTest()
        {
            string short_name = "TemplateMember";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'NestedClassTemplate'
        /// </summary>

        [TestMethod]
        public void CXXParser_NestedClassTemplateTest()
        {
            string short_name = "NestedClassTemplate";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'TemplateReturn' - только синтаксис языка, без h-файлов
        /// </summary>

        [TestMethod]
        public void CXXParser_TemplateReturnTest()
        {
            string short_name = "TemplateReturn";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'TemplateMemberShort' - только синтаксис языка, без h-файлов
        /// </summary>

        [TestMethod]
        public void CXXParser_TemplateMemberShortTest()
        {
            string short_name = "TemplateMemberShort";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'TemplateMember' - только синтаксис языка, без h-файлов
        /// </summary>

        [TestMethod]
        public void CXXParser_TemplateFunctionShortTest()
        {
            string short_name = "TemplateFunctionShort";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'FunctionPointer'
        /// </summary>

        [TestMethod]
        public void CXXParser_FunctionPointerTest()
        {
            string short_name = "FunctionPointer";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }


        /// <summary>
        /// 'RussianEncoding'
        /// </summary>

        [TestMethod]
        public void CXXParser_RussianEncodingTest()
        {
            string short_name = "RussianEncoding";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'RussianEncoding'
        /// </summary>

        [TestMethod]
        public void CXXParser_ChineseEncodingTest()
        {
            string short_name = "ChineseEncoding";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'EnglishEncoding'
        /// </summary>

        [TestMethod]
        public void CXXParser_EnglishEncodingTest()
        {
            string short_name = "EnglishEncoding";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'AsmInline'
        /// </summary>

        [TestMethod]
        public void CXXParser_AsmInlineTest()
        {
            string short_name = "AsmInline";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ConstructCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\ConstructCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\ConstructCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Перехват сообщений при работе с пустой папкой
        /// </summary>

        [Ignore]
        [TestMethod]
        public void CXXParser_EmptyFolderTest()
        {
            string short_name = "EmptyFolder";
            string sourcePostfixPart = @"Sources\CXX\InterfaceCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\InterfaceCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //TestUtilsClass.Run_SensorTypeFixer(storage); 
                    string dumpDirectoryPath = Dump(storage);

                    //CheckMessage("Additional information: Работа без КЭШа. Исключение при выполнении теста: Исключение при вызове checkStorage:");
                    //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "EmptyFolder"));

                    return true;
                }, // checkStorage
                (reportsPath, testMaterialsPath) => { return true; }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Недостаточное количество исходных тестов в папке
        /// </summary>

        [TestMethod]
        public void CXXParser_InsufficientSourcesTest()
        {
            string short_name = "InsufficientSources";
            string sourcePostfixPart = @"Sources\CXX\InterfaceCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\InterfaceCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckMessage("Cannot find suitable file for ast lloyd-yajl-fee1ebe/build/CMakeFiles/2.8.12.2/CompilerIdC/CMakeCCompilerId.c.ast");
                    CheckMessage("Cannot find suitable file for ast lloyd-yajl-fee1ebe/src/yajl_encode.c.ast");
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"InterfaceCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\InterfaceCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\InterfaceCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже присутствует в лабораторных тексах.
        /// </summary>
        [TestMethod]
        public void CXXParser_WrongFirstSensorInLabsTest()
        {
            string short_name = "AsmInline";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <1> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <2>.");
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже присутствует в лабораторных тексах.
        /// </summary>
        [TestMethod]
        public void CXXParser_WrongFirstSensorNotInLabsTest()
        {
            string short_name = "AsmInline";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(3, func1.Id, Kind.START);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <1> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже фигурировал в исходных файлах, но был удален.
        /// </summary>
        [TestMethod]
        public void CXXParser_WrongFirstSensorNotInLabsDeletedTest()
        {
            string short_name = "AsmInline";
            string sourcePostfixPart = @"Sources\CXX\ConstructCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\ConstructCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/tmp/CasesCXX/");
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);
                    storage.sensors.AddSensor(2, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(3, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);
                    storage.sensors.RemoveSensor(2);
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <1> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Мультилингвальный пакет 'perl' - исходники на Си и на самом Perl
        /// </summary>
        [Ignore]
        [TestMethod]
        public void CXXParser_MultiLingvoTest()
        {
            string short_name = "MultiLingvo";
            string sourcePostfixPart = @"Sources\CXX\InterfaceCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\InterfaceCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"InterfaceCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\InterfaceCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    //reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    //StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    //printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    //string etalonPath = testMaterialsPath + @"\CXXParser\XML\InterfaceCXX\" + short_name + ".xml";
                    ////bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    //return true;
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }
        #region SpecialCases
        // особые случае, которые были выявлены при эксплуатации парсера

        /// <summary>
        /// 'NameViaNamespace' - Issue 288 : https://bitbucket.org/ia_devteam/ia/issues/288/namespace
        /// проверка на корректную вставку датчиков в конструкции
        /// new (d.s) std::Din_Go(91872,30002);string(*GetStringPtr(v.data_));
        /// </summary>

        [TestMethod]
        public void CXXParser_NameViaNamespaceTest()
        {
            string short_name = "NameViaNamespace";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/", 3);
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'NameViaNamespace1' - Issue 288 : https://bitbucket.org/ia_devteam/ia/issues/288/namespace
        /// проверка на корректную вставку датчиков в конструкции
        /// {ucbhelper::class InterceptedInteraction::EInterceptionState  ReplaceReturn64502 = ::ucbhelper::InterceptedInteraction::E_INTERCEPTED; Din_Go(61088,60020); return ReplaceReturn64502;};
        /// </summary>

        [TestMethod]
        public void CXXParser_NameViaNamespace1Test()
        {
            string short_name = "NameViaNamespace1";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'RepalceReturnEpicFail' - Issue 293 : https://bitbucket.org/ia_devteam/ia/issues/293/replacereturn
        /// ReplaceReturn - начал вставляться в одной функции, закончил в другой
        /// </summary>

        [TestMethod]
        public void CXXParser_RepalceReturnEpicFailTest()
        {
            string short_name = "RepalceReturnEpicFail";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'CurlyBraceBproblem' - Issue 294 : https://bitbucket.org/ia_devteam/ia/issues/294/curly-brace-problem
        /// urihelper.cxx:69:5: error: 'else' without a previous 'if'
        /// window.cxx:3983:17: error: expected ';' before '{' token COMPAT_BODY({/403/GetFocus,())Din_Go(688826,60020);/404/}
        /// датчик в теле вызова макроса
        /// </summary>
        [Ignore]
        [TestMethod]
        public void CXXParser_CurlyBraceBproblemTest()
        {
            string short_name = "CurlyBraceBproblem";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    //StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    //printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'SensInComment' - Issue 286 : https://bitbucket.org/ia_devteam/ia/issues/286/cxx
        /// dgt_mtrk.cpp : 874 '///*148*/}'
        /// dgt_mtrk.cpp : 890 '///*156*/}' 
        /// </summary>

        [TestMethod]
        public void CXXParser_SensInCommentTest()
        {
            string short_name = "SensInComment";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/", ClangVer:3);
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'ForStatementFail' - датчик вставлен в тело конструкции for(...)
        /// https://bitbucket.org/ia_devteam/ia/issues/299/for
        /// </summary>
        // Din_Go(653390,60020);for(i=0;i<nPoints;{/*15*/i++) Din_Go(653389,60020);aPoly.SetPoint(Point(EckP[i].x,EckP[i].y),i);/*16*/}

        [TestMethod]
        public void CXXParser_ForStatementFailTest()
        {
            string short_name = "ForStatementFail";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'SensInMacros' - датчик НЕ должен вставляться в макрос
        /// </summary>
        // ENTER1( rPtSource, {/*211*/pMapModeSource, {/*213*/pMapModeDest );/*214*/}/*212*/}

        [TestMethod]
        public void CXXParser_SensInMacrosTest()
        {
            string short_name = "SensInMacros";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'RepalceReturnVoid' -  https://bitbucket.org/ia_devteam/ia/issues/298/replacereturn
        /// /BUILD/libreoffice-5.0.6.2/svl/source/numbers/zforscan.cxx:171:79: error: 'ReplaceReturn6114' was not declared in this scope
        /// </summary>
        // {/*1*/{const void  ReplaceReturn6114 = ; Din_Go(392518,60020); return ReplaceReturn6114;};/*2*/}

        [TestMethod]
        public void CXXParser_RepalceReturnVoidTest()
        {
            string short_name = "RepalceReturnVoid";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'RepalceReturnVoid' -  https://bitbucket.org/ia_devteam/ia/issues/259/too-many-sensors-in-define-macros
        /// множественная вставка датчиков в макрос
        /// </summary>

        [TestMethod]
        public void CXXParser_ManySensInMacrosTest()
        {
            string short_name = "ManySensInMacros";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'ManySensInMacros2' - множество датчиков в макросе
        /// выявлено на пакете 'postgresql-9.5.5-1.el7~60062'
        /// </summary>

        [TestMethod]
        public void CXXParser_ManySensInMacros2Test()
        {
            string short_name = "ManySensInMacros2";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'TryCatchFail' - датчик некорректно вставлен в try-catch
        /// </summary>

        [TestMethod]
        public void CXXParser_TryCatchFailTest()
        {
            string short_name = "TryCatchFail";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "SensorCall(#);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'PlainCsyntax' - проверка на return у функций в стиле чистого Си:
        /// для struct, enum, union. Также тесты CXXParser_ManySensInMacros2Test,
        /// часть файлов в tiff, часть файлов в MultiLingvo (perl).
        /// </summary>

        [TestMethod]
        public void CXXParser_PlainCsyntaxTest()
        {
            string short_name = "PlainCsyntax";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'PlainCunion' - проверка на return у функций в стиле чистого Си:
        /// для union.
        /// </summary>

        [TestMethod]
        public void CXXParser_PlainCunionTest()
        {
            string short_name = "PlainCunion";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'SensInMultiLineMacro' - https://bitbucket.org/ia_devteam/ia/issues/302/wrong-sensor-placement-in-multiline-macro
        /// #define ALG_APPEND(to, from) \
        /// {/*31*/Din_Go(23238,33);do { \
        /// если навешивать датчики вне их do while(0) будет компилироваться как statement, а не как вызов функции
        /// в итоге будет ошибка компиляции 'else without previous if'
        ///} while (0)/*32*/}
        /// </summary>

        [TestMethod]
        public void CXXParser_SensInMultiLineMacroTest()
        {
            string short_name = "SensInMultiLineMacro";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    string dirLabs = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "cxx");
                    WashLabSources(dirLabs);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'SensInStrLiteral' - https://bitbucket.org/ia_devteam/ia/issues/303/------------------------------------------
        /// </summary>

        [TestMethod]
        public void CXXParser_SensInStrLiteralTest()
        {
            string short_name = "SensInStrLiteral";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'WrongPlaceInMacroOneMore' #307 - https://bitbucket.org/ia_devteam/ia/issues/307/wrong-sensor-placement-in-multiline-macro
        /// </summary>

        [TestMethod]
        public void CXXParser_WrongPlaceInMacroOneMoreTest()
        {
            string short_name = "WrongPlaceInMacroOneMore";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'WrongPlaceInMacroCall' #308 - https://bitbucket.org/ia_devteam/ia/issues/308/wrong-sensor-placement-in-macro-call-one
        /// </summary>

        [TestMethod]
        public void CXXParser_WrongPlaceInMacroCallTest()
        {
            string short_name = "WrongPlaceInMacroCall";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Incorrect template dereferencing' #305 - https://bitbucket.org/ia_devteam/ia/issues/305/incorrect-dereferencing
        /// </summary>
        [TestMethod]
        public void CXXParser_IncorrectTemplateDereferencingTest()
        {
            string short_name = "IncorrectTemplateDereferencing";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                    testMaterialsPath,
                    sourcePostfixPart,
                    true,
                    1,
                    astPath,
                    "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                    "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'Curly brace problem - after one line comment' #309 - https://bitbucket.org/ia_devteam/ia/issues/309/curly-brace-problem-after-one-line-comment
        /// </summary>
        [Ignore]
        [TestMethod]
        public void CXXParser_CurlyBraceUnoCommentTest()
        {
            string short_name = "CurlyBraceUnoComment";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                    testMaterialsPath,
                    sourcePostfixPart,
                    true,
                    1,
                    astPath,
                    "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                    "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    //StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    //printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'CurlyBraceMacrosAndIf' Issue #314 https://bitbucket.org/ia_devteam/ia/issues/314/wrong-curly-brace-position-in-macros-and
        ///  </summary>
        [TestMethod]
        public void CXXParser_CurlyBraceInMacrosTest()
        {
            string short_name = "CurlyBraceMacrosAndIf";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                    testMaterialsPath,
                    sourcePostfixPart,
                    true,
                    1,
                    astPath,
                    "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                    "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'WrongSensorPostion' #310 - https://bitbucket.org/ia_devteam/ia/issues/310/sensor-between-variable-and-namespace
        /// </summary>
        [TestMethod]
        public void CXXParser_WrongSensorPostionTest()
        {
            string short_name = "WrongSensorPostion";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                    testMaterialsPath,
                    sourcePostfixPart,
                    true,
                    1,
                    astPath,
                    "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                    "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'SensorOutFunction' #311 - https://bitbucket.org/ia_devteam/ia/issues/311/sensor-outside-function
        /// </summary>
        [TestMethod]
        public void CXXParser_SensorOutFunctionTest()
        {
            string short_name = "SensorOutFunction";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                    testMaterialsPath,
                    sourcePostfixPart,
                    true,
                    1,
                    astPath,
                    "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                    "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'IfElseManyNesting' - не повторяется проблема с цланг, когда 
        /// проглатываются if-else при большой вложенности
        /// </summary>
        [TestMethod]
        public void CXXParser_IfElseManyNestingTest()
        {
            string short_name = "IfElseManyNesting";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                    testMaterialsPath,
                    sourcePostfixPart,
                    true,
                    1,
                    astPath,
                    "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                    "/tmp/CasesCXX/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// 'WrongPlaceInMacroOneMore' #307 - https://bitbucket.org/ia_devteam/ia/issues/307/wrong-sensor-placement-in-multiline-macro
        /// </summary>

        [TestMethod]
        public void CXXParser_WrongTypeClassTest()
        {
            string short_name = "WrongTypeClass";
            string sourcePostfixPart = @"Sources\CXX\sCasesCXX\" + short_name + @"\src\";
            string astPath = @"Sources\CXX\sCasesCXX\" + short_name + @"\ast\";
            CXXParser testPlugin = new CXXParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1,
                        astPath,
                        "Din_Go(#,2048);", "#include \"var/tmp/sensor.h\"",
                        "/root/rpmbuild/BUILD/");
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"sCasesCXX\" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\sCasesCXX\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\CXXParser\XML\sCasesCXX\" + short_name + ".xml";
                    //bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return true;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        #endregion
    }
}
