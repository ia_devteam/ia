namespace IA.Plugins.Importers.UnderstandImporter
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.isIgnoreMacroPage_checkBox = new System.Windows.Forms.CheckBox();
            this.isLoadVariables_checkBox = new System.Windows.Forms.CheckBox();
            this.isRunUnderstand_checkBox = new System.Windows.Forms.CheckBox();
            this.reportsDirectoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.reportsDirectoryPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.understandPath_groupBox = new System.Windows.Forms.GroupBox();
            this.understandPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.sourcesDirectoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.sourcesPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.languages_groupBox = new System.Windows.Forms.GroupBox();
            this.languages_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isCSharp_checkbox = new System.Windows.Forms.CheckBox();
            this.isJava_checkBox = new System.Windows.Forms.CheckBox();
            this.isWeb_checkBox = new System.Windows.Forms.CheckBox();
            this.isPascal_checkBox = new System.Windows.Forms.CheckBox();
            this.isCPP_checkBox = new System.Windows.Forms.CheckBox();
            this.reportsDirectoryPath_groupBox.SuspendLayout();
            this.understandPath_groupBox.SuspendLayout();
            this.sourcesDirectoryPath_groupBox.SuspendLayout();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.languages_groupBox.SuspendLayout();
            this.languages_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // isIgnoreMacroPage_checkBox
            // 
            this.isIgnoreMacroPage_checkBox.AutoSize = true;
            this.isIgnoreMacroPage_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isIgnoreMacroPage_checkBox.Location = new System.Drawing.Point(3, 83);
            this.isIgnoreMacroPage_checkBox.Name = "isIgnoreMacroPage_checkBox";
            this.isIgnoreMacroPage_checkBox.Size = new System.Drawing.Size(717, 24);
            this.isIgnoreMacroPage_checkBox.TabIndex = 8;
            this.isIgnoreMacroPage_checkBox.Text = "������������ ������, ���������� �������";
            this.isIgnoreMacroPage_checkBox.UseVisualStyleBackColor = true;
            // 
            // isLoadVariables_checkBox
            // 
            this.isLoadVariables_checkBox.AutoSize = true;
            this.isLoadVariables_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isLoadVariables_checkBox.Location = new System.Drawing.Point(3, 53);
            this.isLoadVariables_checkBox.Name = "isLoadVariables_checkBox";
            this.isLoadVariables_checkBox.Size = new System.Drawing.Size(717, 24);
            this.isLoadVariables_checkBox.TabIndex = 9;
            this.isLoadVariables_checkBox.Text = "��������� ����� �� ����������";
            this.isLoadVariables_checkBox.UseVisualStyleBackColor = true;
            // 
            // isRunUnderstand_checkBox
            // 
            this.isRunUnderstand_checkBox.AutoSize = true;
            this.isRunUnderstand_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isRunUnderstand_checkBox.Location = new System.Drawing.Point(3, 113);
            this.isRunUnderstand_checkBox.Name = "isRunUnderstand_checkBox";
            this.isRunUnderstand_checkBox.Size = new System.Drawing.Size(717, 24);
            this.isRunUnderstand_checkBox.TabIndex = 4;
            this.isRunUnderstand_checkBox.Text = "������������� ������ Understand �������������";
            this.isRunUnderstand_checkBox.UseVisualStyleBackColor = true;
            this.isRunUnderstand_checkBox.CheckedChanged += new System.EventHandler(this.isRunUnderstand_checkBox_CheckedChanged);
            // 
            // reportsDirectoryPath_groupBox
            // 
            this.reportsDirectoryPath_groupBox.Controls.Add(this.reportsDirectoryPath_textBox);
            this.reportsDirectoryPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportsDirectoryPath_groupBox.Location = new System.Drawing.Point(3, 3);
            this.reportsDirectoryPath_groupBox.Name = "reportsDirectoryPath_groupBox";
            this.reportsDirectoryPath_groupBox.Size = new System.Drawing.Size(717, 44);
            this.reportsDirectoryPath_groupBox.TabIndex = 7;
            this.reportsDirectoryPath_groupBox.TabStop = false;
            this.reportsDirectoryPath_groupBox.Text = "����� � ��������";
            // 
            // reportsDirectoryPath_textBox
            // 
            this.reportsDirectoryPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportsDirectoryPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.reportsDirectoryPath_textBox.MaximumSize = new System.Drawing.Size(0, 24);
            this.reportsDirectoryPath_textBox.MinimumSize = new System.Drawing.Size(470, 24);
            this.reportsDirectoryPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.reportsDirectoryPath_textBox.Name = "reportsDirectoryPath_textBox";
            this.reportsDirectoryPath_textBox.Size = new System.Drawing.Size(711, 24);
            this.reportsDirectoryPath_textBox.TabIndex = 0;
            // 
            // understandPath_groupBox
            // 
            this.understandPath_groupBox.Controls.Add(this.understandPath_textBox);
            this.understandPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.understandPath_groupBox.Location = new System.Drawing.Point(3, 288);
            this.understandPath_groupBox.Name = "understandPath_groupBox";
            this.understandPath_groupBox.Size = new System.Drawing.Size(717, 43);
            this.understandPath_groupBox.TabIndex = 6;
            this.understandPath_groupBox.TabStop = false;
            this.understandPath_groupBox.Text = "���� � ����� Und.exe (�� �������� Understand)";
            // 
            // understandPath_textBox
            // 
            this.understandPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.understandPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.understandPath_textBox.MaximumSize = new System.Drawing.Size(0, 24);
            this.understandPath_textBox.MinimumSize = new System.Drawing.Size(470, 24);
            this.understandPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.understandPath_textBox.Name = "understandPath_textBox";
            this.understandPath_textBox.Size = new System.Drawing.Size(711, 24);
            this.understandPath_textBox.TabIndex = 0;
            // 
            // sourcesDirectoryPath_groupBox
            // 
            this.sourcesDirectoryPath_groupBox.Controls.Add(this.sourcesPath_textBox);
            this.sourcesDirectoryPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourcesDirectoryPath_groupBox.Location = new System.Drawing.Point(3, 337);
            this.sourcesDirectoryPath_groupBox.Name = "sourcesDirectoryPath_groupBox";
            this.sourcesDirectoryPath_groupBox.Size = new System.Drawing.Size(717, 43);
            this.sourcesDirectoryPath_groupBox.TabIndex = 10;
            this.sourcesDirectoryPath_groupBox.TabStop = false;
            this.sourcesDirectoryPath_groupBox.Text = "����� � �������������� �������";
            // 
            // sourcesPath_textBox
            // 
            this.sourcesPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourcesPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.sourcesPath_textBox.MaximumSize = new System.Drawing.Size(0, 24);
            this.sourcesPath_textBox.MinimumSize = new System.Drawing.Size(470, 24);
            this.sourcesPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.sourcesPath_textBox.Name = "sourcesPath_textBox";
            this.sourcesPath_textBox.Size = new System.Drawing.Size(711, 24);
            this.sourcesPath_textBox.TabIndex = 0;
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.reportsDirectoryPath_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Controls.Add(this.isLoadVariables_checkBox, 0, 1);
            this.settings_tableLayoutPanel.Controls.Add(this.isIgnoreMacroPage_checkBox, 0, 2);
            this.settings_tableLayoutPanel.Controls.Add(this.isRunUnderstand_checkBox, 0, 3);
            this.settings_tableLayoutPanel.Controls.Add(this.sourcesDirectoryPath_groupBox, 0, 6);
            this.settings_tableLayoutPanel.Controls.Add(this.understandPath_groupBox, 0, 5);
            this.settings_tableLayoutPanel.Controls.Add(this.languages_groupBox, 0, 4);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 7;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(723, 383);
            this.settings_tableLayoutPanel.TabIndex = 3;
            // 
            // languages_groupBox
            // 
            this.languages_groupBox.Controls.Add(this.languages_tableLayoutPanel);
            this.languages_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.languages_groupBox.Location = new System.Drawing.Point(3, 143);
            this.languages_groupBox.Name = "languages_groupBox";
            this.languages_groupBox.Size = new System.Drawing.Size(717, 139);
            this.languages_groupBox.TabIndex = 12;
            this.languages_groupBox.TabStop = false;
            this.languages_groupBox.Text = "������������� ����� ����������������";
            // 
            // languages_tableLayoutPanel
            // 
            this.languages_tableLayoutPanel.ColumnCount = 1;
            this.languages_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.languages_tableLayoutPanel.Controls.Add(this.isCSharp_checkbox, 0, 4);
            this.languages_tableLayoutPanel.Controls.Add(this.isJava_checkBox, 0, 3);
            this.languages_tableLayoutPanel.Controls.Add(this.isWeb_checkBox, 0, 2);
            this.languages_tableLayoutPanel.Controls.Add(this.isPascal_checkBox, 0, 1);
            this.languages_tableLayoutPanel.Controls.Add(this.isCPP_checkBox, 0, 0);
            this.languages_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.languages_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.languages_tableLayoutPanel.Name = "languages_tableLayoutPanel";
            this.languages_tableLayoutPanel.RowCount = 5;
            this.languages_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.languages_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.languages_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.languages_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.languages_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.languages_tableLayoutPanel.Size = new System.Drawing.Size(711, 120);
            this.languages_tableLayoutPanel.TabIndex = 0;
            // 
            // isCSharp_checkbox
            // 
            this.isCSharp_checkbox.AutoSize = true;
            this.isCSharp_checkbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isCSharp_checkbox.Location = new System.Drawing.Point(3, 99);
            this.isCSharp_checkbox.Name = "isCSharp_checkbox";
            this.isCSharp_checkbox.Size = new System.Drawing.Size(705, 18);
            this.isCSharp_checkbox.TabIndex = 5;
            this.isCSharp_checkbox.Text = "C#";
            this.isCSharp_checkbox.UseVisualStyleBackColor = true;
            // 
            // isJava_checkBox
            // 
            this.isJava_checkBox.AutoSize = true;
            this.isJava_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isJava_checkBox.Location = new System.Drawing.Point(3, 75);
            this.isJava_checkBox.Name = "isJava_checkBox";
            this.isJava_checkBox.Size = new System.Drawing.Size(705, 18);
            this.isJava_checkBox.TabIndex = 4;
            this.isJava_checkBox.Text = "Java";
            this.isJava_checkBox.UseVisualStyleBackColor = true;
            // 
            // isWeb_checkBox
            // 
            this.isWeb_checkBox.AutoSize = true;
            this.isWeb_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isWeb_checkBox.Location = new System.Drawing.Point(3, 51);
            this.isWeb_checkBox.Name = "isWeb_checkBox";
            this.isWeb_checkBox.Size = new System.Drawing.Size(705, 18);
            this.isWeb_checkBox.TabIndex = 3;
            this.isWeb_checkBox.Text = "Web (PHP, JavaScript)";
            this.isWeb_checkBox.UseVisualStyleBackColor = true;
            // 
            // isPascal_checkBox
            // 
            this.isPascal_checkBox.AutoSize = true;
            this.isPascal_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isPascal_checkBox.Location = new System.Drawing.Point(3, 27);
            this.isPascal_checkBox.Name = "isPascal_checkBox";
            this.isPascal_checkBox.Size = new System.Drawing.Size(705, 18);
            this.isPascal_checkBox.TabIndex = 2;
            this.isPascal_checkBox.Text = "Pascal";
            this.isPascal_checkBox.UseVisualStyleBackColor = true;
            // 
            // isCPP_checkBox
            // 
            this.isCPP_checkBox.AutoSize = true;
            this.isCPP_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isCPP_checkBox.Location = new System.Drawing.Point(3, 3);
            this.isCPP_checkBox.Name = "isCPP_checkBox";
            this.isCPP_checkBox.Size = new System.Drawing.Size(705, 18);
            this.isCPP_checkBox.TabIndex = 0;
            this.isCPP_checkBox.Text = "C++";
            this.isCPP_checkBox.UseVisualStyleBackColor = true;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(723, 383);
            this.reportsDirectoryPath_groupBox.ResumeLayout(false);
            this.understandPath_groupBox.ResumeLayout(false);
            this.sourcesDirectoryPath_groupBox.ResumeLayout(false);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.settings_tableLayoutPanel.PerformLayout();
            this.languages_groupBox.ResumeLayout(false);
            this.languages_tableLayoutPanel.ResumeLayout(false);
            this.languages_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox isRunUnderstand_checkBox;
        private System.Windows.Forms.GroupBox understandPath_groupBox;
        private System.Windows.Forms.GroupBox reportsDirectoryPath_groupBox;
        private System.Windows.Forms.CheckBox isIgnoreMacroPage_checkBox;
        private System.Windows.Forms.CheckBox isLoadVariables_checkBox;
        private System.Windows.Forms.GroupBox sourcesDirectoryPath_groupBox;
        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private Controls.TextBoxWithDialogButton reportsDirectoryPath_textBox;
        private Controls.TextBoxWithDialogButton sourcesPath_textBox;
        private Controls.TextBoxWithDialogButton understandPath_textBox;
        private System.Windows.Forms.GroupBox languages_groupBox;
        private System.Windows.Forms.TableLayoutPanel languages_tableLayoutPanel;
        private System.Windows.Forms.CheckBox isJava_checkBox;
        private System.Windows.Forms.CheckBox isWeb_checkBox;
        private System.Windows.Forms.CheckBox isPascal_checkBox;
        private System.Windows.Forms.CheckBox isCPP_checkBox;
        private System.Windows.Forms.CheckBox isCSharp_checkbox;
    }
}
