﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;
using Microsoft.Win32;

namespace IA.Plugins.Importers.UnderstandImporter
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER;

        /// <summary>
        /// Имя плагина
        /// </summary>
        internal const string Name = "Импорт протоколов Understand";

        #region Settings

        /// <summary>
        /// Папка с отчетами
        /// если пусто - генерировалось на выкачанном с сервера (вручную или автоматом). Разместить во временной папке плагина
        /// </summary>
        internal static string ReportsDirectoryPath;

        /// <summary>
        /// Папка с исходниками, для которых генерируются отчеты Understand
        /// если пусто - генерировалось на выкачанном с сервера (вручную или автоматом)
        /// </summary>
        internal static string SourcesDirectoryPath;

        /// <summary>
        /// Запустить Understand автоматически
        /// </summary>
        internal static bool IsRunUnderstand;

        /// <summary>
        /// Загрузить переменные
        /// </summary>
        internal static bool IsLoadVariables;

        /// <summary>
        /// Игнорировать строки с макровызовами
        /// </summary>
        internal static bool IsIgnoreMacroLines;

        /// <summary>
        /// Путь к Understand
        /// </summary>
        internal static string UnderstandRealPath;

        /// <summary>
        /// Анализировать языки программирования С/С++
        /// </summary>
        internal static bool IsCPP;

        /// <summary>
        /// Анализировать язык программирования Pascal
        /// </summary>
        internal static bool IsPascal;

        /// <summary>
        /// Анализировать языки Web-программирования (PHP и JavaScript)
        /// </summary>
        internal static bool IsWeb;

        /// <summary>
        /// Анализировать язык программирования Java
        /// </summary>
        internal static bool IsJava;

        /// <summary>
        /// Анализировать язык программирования C#
        /// </summary>
        internal static bool IsCS;

        #endregion

        internal static string UnderstandPath
        {
            get
            {
                return PluginSettings.UnderstandRealPath;
            }
            set
            {
                if (value != null && File.Exists(value))
                {
                    PluginSettings.UnderstandRealPath = value;
                    return;
                }

                PluginSettings.UnderstandRealPath = string.Empty;

                try
                {
                    RegistryKey currRegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\SciTools");
                    if (currRegistryKey != null)
                    {
                        foreach (string version in currRegistryKey.GetSubKeyNames())
                        {
                            RegistryKey key = currRegistryKey.OpenSubKey(version);
                            if (key != null)
                            {
                                string keyValue = (string)key.GetValue("");
                                PluginSettings.UnderstandRealPath = File.Exists(keyValue + @"\bin\pc-win32\und.exe") ? keyValue + @"\bin\pc-win32\und.exe" : keyValue + @"\bin\pc-win64\und.exe";
                            }
                            else
                                Monitor.Log.Error(PluginSettings.Name, "Неверная информация в реестре в ключе <" + currRegistryKey.Name + ">.");
                            key.Close();
                        }
                        currRegistryKey.Close();
                    }
                    else
                    {
                        currRegistryKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\SciTools\");
                        if (currRegistryKey != null)
                        {
                            foreach (string version in currRegistryKey.GetSubKeyNames())
                            {
                                RegistryKey key = currRegistryKey.OpenSubKey(version);
                                if (key != null)
                                {
                                    string keyValue = (string)key.GetValue("");
                                    PluginSettings.UnderstandRealPath = File.Exists(keyValue + @"\bin\pc-win32\und.exe") ? keyValue + @"\bin\pc-win32\und.exe" : keyValue + @"\bin\pc-win64\und.exe";
                                }
                                else
                                    Monitor.Log.Error(PluginSettings.Name, "Неверная информация в реестре в ключе <" + currRegistryKey.Name + ">.");
                                key.Close();
                            }
                            currRegistryKey.Close();
                        }
                        else
                        {
                            Monitor.Log.Warning(PluginSettings.Name, "На АРМ не установлен Understand.");
                        }
                    }
                }
                catch
                {
                    Monitor.Log.Warning(PluginSettings.Name, "На АРМ не установлен Understand.");
                }
                
                if (!File.Exists(PluginSettings.UnderstandRealPath))
                {
                    PluginSettings.UnderstandRealPath = string.Empty;
                }
            }
        }

        /// <summary>
        /// Задачи плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Чтение перечня классов")]
            CLASS_LIST_READING,
            [Description("Чтение перечня функций")]
            FUNCTION_LIST_READING,
            [Description("Чтение обращений функций")]
            FUNCTION_REFERENCE_READING,
            [Description("Чтение информации о переменных")]
            VARIABLES_INFORMATION_READING,
            [Description("Чтение информации об избыточных функциях")]
            REDUNDANT_FUCTIONS_INFORMATION_READING
        }

        /// <summary>
        /// Рекомендуемая версия Understand
        /// </summary>
        internal const int UnderstandBuild = 715;
    }

    public class UnderstandImporter : IA.Plugin.Interface
    {
        /// <summary>
        /// Текущее хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Текущие настройки
        /// </summary>
        Settings settings;

        bool stopper = false;
        Functions functions;

        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW |
                Plugin.Capabilities.STOP_SMOOTHLY;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
            stopper = true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.ReportsDirectoryPath = Properties.Settings.Default.ReportsFolder;
            PluginSettings.SourcesDirectoryPath = Properties.Settings.Default.SourcesPath;
            PluginSettings.IsRunUnderstand = Properties.Settings.Default.IsRunUnderstand;
            PluginSettings.IsLoadVariables = Properties.Settings.Default.IsLoadVariables;
            PluginSettings.IsIgnoreMacroLines = Properties.Settings.Default.IsIgnoreMacroLines;
            PluginSettings.UnderstandRealPath = Properties.Settings.Default.UnderstandPath;
            PluginSettings.IsCPP = Properties.Settings.Default.IsCPP;
            PluginSettings.IsPascal = Properties.Settings.Default.IsPascal;
            PluginSettings.IsWeb = Properties.Settings.Default.IsWeb;
            PluginSettings.IsJava = Properties.Settings.Default.IsJava;
            PluginSettings.IsCS = Properties.Settings.Default.IsCS;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.ReportsDirectoryPath = settingsBuffer.GetString();
            PluginSettings.SourcesDirectoryPath = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.IsRunUnderstand);
            settingsBuffer.GetBool(ref PluginSettings.IsIgnoreMacroLines);
            settingsBuffer.GetBool(ref PluginSettings.IsLoadVariables);
            PluginSettings.UnderstandPath = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.IsCPP);
            settingsBuffer.GetBool(ref PluginSettings.IsPascal);
            settingsBuffer.GetBool(ref PluginSettings.IsWeb);
            settingsBuffer.GetBool(ref PluginSettings.IsJava);
            settingsBuffer.GetBool(ref PluginSettings.IsCS);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.ReportsDirectoryPath = string.Empty;
            PluginSettings.IsRunUnderstand = true;
            PluginSettings.IsIgnoreMacroLines = false;
            PluginSettings.IsLoadVariables = true;
            PluginSettings.UnderstandPath = string.Empty;
            PluginSettings.SourcesDirectoryPath = string.Empty;
            PluginSettings.IsCPP = true;
            PluginSettings.IsPascal = true;
            PluginSettings.IsWeb = false;
            PluginSettings.IsJava = true;      
            PluginSettings.IsCS = false;      

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
            throw new NotImplementedException();
        }
        }

        /// <summary>
        /// генерация отчетов Understand 
        /// </summary>
        void GenerateUnderstandProtocols()
        {
            IA.Monitor.Log.Warning(PluginSettings.Name, "Начата генерация отчетов Understand");
            
            //сначала проверим версию Understand
            string result = runCommand(PluginSettings.UnderstandPath, "version", true).ReadToEnd();
            result = result.Substring(7, result.IndexOf(")") - 7);

            if (int.Parse(result) < PluginSettings.UnderstandBuild)
            {
                Monitor.Log.Error(Name, "Рекомендуемая версия Understand - Build " + PluginSettings.UnderstandBuild.ToString() + ". Установлена " + result);
            }

            string languages = string.Empty;
            if (PluginSettings.IsCPP)
                languages += " C++";
            if (PluginSettings.IsPascal)
                languages += " Pascal";
            if (PluginSettings.IsWeb)
                languages += " Web";
            if (PluginSettings.IsJava)
                languages += " Java";
            if (PluginSettings.IsCS)
                languages += " C#";

            string undProject = Path.Combine(storage.WorkDirectory.Path, Path.GetRandomFileName()) + ".udb";

            runCommand(PluginSettings.UnderstandPath, "create -db " + undProject + " -languages" + languages, false);

            Directory.CreateDirectory(PluginSettings.ReportsDirectoryPath);
            IA.Monitor.Log.Warning(PluginSettings.Name, "Создан проект Understand");

            runCommand(PluginSettings.UnderstandPath, "-db " + undProject + " add " + storage.appliedSettings.FileListPath, false);
            IA.Monitor.Log.Warning(PluginSettings.Name, "Добавлены файлы проект Understand");

            IA.Monitor.Log.Warning(PluginSettings.Name, "Запущен анализ при помощи Understand");
            runCommand(PluginSettings.UnderstandPath, "-db " + undProject + " analyze", false);
            IA.Monitor.Log.Warning(PluginSettings.Name, "Завершен анализ при помощи Understand");

            IA.Monitor.Log.Warning(PluginSettings.Name, "Запущено формирование отчетов Understand");

            //runCommand(undPath, "-db " + undProject + " settings -ReportReports \"None\"");

            runCommand(PluginSettings.UnderstandPath, "-db " + undProject + " settings -ReportReports \"Data Dictionary\" \"Program Unit Cross Reference\" \"Object Cross Reference\" \"Unused Program Units\" \"Macro Cross Reference\"", false);

            runCommand(PluginSettings.UnderstandPath, "-db " + undProject + " settings -ReportFileNameDisplayMode full", false);

            runCommand(PluginSettings.UnderstandPath, "-db " + undProject + " settings -ReportOutputDirectorySeparate \"" + PluginSettings.ReportsDirectoryPath + "\"", false);

            runCommand(PluginSettings.UnderstandPath, "-db " + undProject + " settings -ReportNumberOfFiles Separate", false);

            runCommand(PluginSettings.UnderstandPath, "-db " + undProject + " settings -ReportGenerateTextReport on", false);

            // Add 21.05.2014 - отключено создание html-файлов
            runCommand(PluginSettings.UnderstandPath, "-db " + undProject + " settings -ReportGenerateHtmlReport off", false);
            // 21.05.2014

            runCommand(PluginSettings.UnderstandPath, "-db " + undProject + " report", false);

            IA.Monitor.Log.Warning(PluginSettings.Name, "Завершено формирование отчетов Understand");

            System.IO.File.Delete(undProject);
            IA.Monitor.Log.Warning(PluginSettings.Name, "Завершена генерация отчетов Understand");
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        public void Run()
        {
            stopper = false;

            functions = storage.functions;

            if (PluginSettings.IsRunUnderstand)
            {
                if (PluginSettings.UnderstandPath == String.Empty)
                {
                    Monitor.Log.Error(Name,"Не установлен Understand. Анализ, выполняемый с его помощью выполнен не будет.");
                    return;
                }
                if (string.IsNullOrEmpty(PluginSettings.ReportsDirectoryPath))
                {
                    PluginSettings.ReportsDirectoryPath = Path.Combine(storage.pluginData.GetDataFolder(PluginSettings.ID), "Understand");                
                }
                Directory.CreateDirectory(PluginSettings.ReportsDirectoryPath);
                GenerateUnderstandProtocols();
            }

            #region заполнение тасков
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CLASS_LIST_READING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)(new FileInfo(Path.Combine(PluginSettings.ReportsDirectoryPath, "DataDictionary.txt"))).Length);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNCTION_LIST_READING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)(new FileInfo(Path.Combine(PluginSettings.ReportsDirectoryPath, "DataDictionary.txt"))).Length);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNCTION_REFERENCE_READING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)(new FileInfo(Path.Combine(PluginSettings.ReportsDirectoryPath, "ProgramUnits.txt"))).Length);
            if (PluginSettings.IsLoadVariables)
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.VARIABLES_INFORMATION_READING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)(new FileInfo(Path.Combine(PluginSettings.ReportsDirectoryPath, "Objects.txt"))).Length);
            try
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.REDUNDANT_FUCTIONS_INFORMATION_READING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)(new FileInfo(Path.Combine(PluginSettings.ReportsDirectoryPath, "UnusedProgramUnits.txt"))).Length);
            }
            catch
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.REDUNDANT_FUCTIONS_INFORMATION_READING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, 0);
            }
            #endregion заполнение тасков

            if (!Analyzer.Run(storage, stopper))
                throw new Exception("Разработчик не описал эту ошибку.");
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.ReportsFolder = PluginSettings.ReportsDirectoryPath;
            Properties.Settings.Default.IsRunUnderstand = PluginSettings.IsRunUnderstand;
            Properties.Settings.Default.IsIgnoreMacroLines = PluginSettings.IsIgnoreMacroLines;
            Properties.Settings.Default.IsLoadVariables = PluginSettings.IsLoadVariables;
            Properties.Settings.Default.UnderstandPath = PluginSettings.UnderstandPath;
            Properties.Settings.Default.SourcesPath = PluginSettings.SourcesDirectoryPath;
            Properties.Settings.Default.IsCPP = PluginSettings.IsCPP;
            Properties.Settings.Default.IsPascal = PluginSettings.IsPascal;
            Properties.Settings.Default.IsWeb = PluginSettings.IsWeb;
            Properties.Settings.Default.IsJava = PluginSettings.IsJava;
            Properties.Settings.Default.IsCS = PluginSettings.IsCS;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.ReportsDirectoryPath);
            settingsBuffer.Add(PluginSettings.SourcesDirectoryPath);
            settingsBuffer.Add(PluginSettings.IsRunUnderstand);
            settingsBuffer.Add(PluginSettings.IsIgnoreMacroLines);
            settingsBuffer.Add(PluginSettings.IsLoadVariables);
            settingsBuffer.Add(PluginSettings.UnderstandPath);
            settingsBuffer.Add(PluginSettings.IsCPP);
            settingsBuffer.Add(PluginSettings.IsPascal);
            settingsBuffer.Add(PluginSettings.IsWeb);
            settingsBuffer.Add(PluginSettings.IsJava);
            settingsBuffer.Add(PluginSettings.IsCS);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (settings = new Settings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (PluginSettings.IsRunUnderstand)
            {
                char[] rusSym = {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т',
                             'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'};

                if (storage.appliedSettings.FileListPathNoUnpack.ToLower().Any(c => rusSym.Contains(c)))
                {
                    message = "Для Understand недопустим путь с русскими символами.";
                    return false;
                }

                if (!File.Exists(PluginSettings.UnderstandPath))
                {
                    message = "Не найден исполнимый файл Understand - Und.exe.";
                    PluginSettings.UnderstandPath = String.Empty;
                    return true;
                }

                if (!PluginSettings.IsCPP && !PluginSettings.IsPascal && !PluginSettings.IsWeb && !PluginSettings.IsJava && !PluginSettings.IsCS)
                {
                    message = "Не выбраны языки программирования для анализа.";
                    return false;
                }
            }
            else
            {
                string dataDictionaryPath = Path.Combine(PluginSettings.ReportsDirectoryPath, "DataDictionary.txt");
                if (!PluginSettings.IsRunUnderstand && !File.Exists(dataDictionaryPath))
                {
                    message = "Файла " + dataDictionaryPath + " не существует.";
                    return false;
                }

                string programUnitsPath = Path.Combine(PluginSettings.ReportsDirectoryPath, "ProgramUnits.txt");
                if (!PluginSettings.IsRunUnderstand && !File.Exists(programUnitsPath))
                {
                    message = "Файла " + programUnitsPath + " не существует.";
                    return false;
                }

                string ObjectsPath = Path.Combine(PluginSettings.ReportsDirectoryPath, "Objects.txt");
                if (!PluginSettings.IsRunUnderstand && !File.Exists(ObjectsPath) && PluginSettings.IsLoadVariables)
                {
                    message = "Файла " + ObjectsPath + " не существует.";
                    return false;
                }

                string MacrosPath = Path.Combine(PluginSettings.ReportsDirectoryPath, "Macros.txt");
                if (!PluginSettings.IsRunUnderstand && !File.Exists(MacrosPath) && PluginSettings.IsIgnoreMacroLines)
                {
                    message = "Файла " + MacrosPath + " не существует.";
                    return false;
                }
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                List<Monitor.Tasks.Task> Tasks = new List<Monitor.Tasks.Task>();

                Tasks.Add(new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.CLASS_LIST_READING.Description()));
                Tasks.Add(new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FUNCTION_LIST_READING.Description()));
                Tasks.Add(new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FUNCTION_REFERENCE_READING.Description()));
                Tasks.Add(new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.VARIABLES_INFORMATION_READING.Description()));
                Tasks.Add(new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.REDUNDANT_FUCTIONS_INFORMATION_READING.Description()));

                return Tasks;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return null;
            }
        }

        #endregion

        /// <summary>
        /// Запустить процесс
        /// </summary>
        /// <param name="path">Путь к файлу.</param>
        /// <param name="Arguments">Аргументы запуска.</param>
        /// <param name="RedirectOutput">Перенаправить в стандартный поток ввода/вывода?</param>
        /// <returns>Стандартный поток ввода/вывода с результатом выполнения команды.</returns>
        static StreamReader runCommand(string path, string Arguments, bool RedirectOutput)
        {
            //Проверка на разумность
            if (!File.Exists(path) || string.IsNullOrEmpty(path))
            {
                Monitor.Log.Error(PluginSettings.Name, "Не задан путь к файлу для запуска команды");
                return null;
            }

            Process undProcess = new Process
            {
                StartInfo =
                {
                    FileName = path,
                    Arguments = Arguments,
                    RedirectStandardOutput = RedirectOutput,
                    RedirectStandardError = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    UseShellExecute = false
                }
            };
            undProcess.Start();
            undProcess.WaitForExit();
            
            if (undProcess.ExitCode != 0)
            {
                Monitor.Log.Error("Understand Importer", "Не выполнена команда " + path + "  " + Arguments);
            }

            return RedirectOutput ? undProcess.StandardOutput : null;
        }

    }
}
