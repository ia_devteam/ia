using System;
using FileOperations;
using Store;

using IA.Extensions;

namespace IA.Plugins.Importers.UnderstandImporter
{
    /// <summary>
    /// ��������� ������� � ���������
    /// </summary>
    class Function
    {
        internal Store.IFunction function;

        public Function(Functions functions)
        {
            function = functions.AddFunction();
        }

        public Function()
        {
            function = null;
        }

        public Function(Store.IFunction function)
        {
            this.function = function;
        }

        /// <summary>
        /// ������ ��� �������
        /// </summary>
        public string Name
        {
            get { return function.Name; }
            set
            {
                function.Name = value;
            }
        }

        /// <summary>
        /// ������ �������������� ����������� �������
        /// </summary>
        /// <param name="fileName">��� �����</param>
        /// <param name="Line">������ �����������</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public void Definition(string fileName, UInt64 Line)
        {
            char[] arrF = { '.', ':' };
            char[] arrL = { ' ', '+', '=', '-', '*', '/', '[', '(' };
            char[] arrB = { ')', '{', '}' };
            char[] arrMethodSeparator = { ' ', ';', '\t' };

            try
            {
                AbstractReader sr = null;

                sr = new AbstractReader(fileName);

                string line;
                line = sr.getLineN((int)Line);

                if (line == null)
                    return;

                string nameToFind = Name;
                foreach (char sym in arrF)
                    if (nameToFind.LastIndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(nameToFind.LastIndexOf(sym) + 1);

                foreach (char sym in arrL)
                    if (nameToFind.IndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(0, nameToFind.IndexOf(sym));

                //� function.AddDefinition � ������� ���������� 1, ��� ��� ������� ���������� � �������, � �� � ��������
                if (nameToFind.Length == 0)
                {
                    function.AddDefinition(fileName, Line, 1);
                }
                else
                {
                    int lastInd = line.LastIndexOf(nameToFind);
                    
                    if (lastInd != -1)
                    { // ���� ��������� �����

                        int secondSearchInd = lastInd;
                        if (secondSearchInd > 0)
                        do
                            secondSearchInd = line.LastIndexOf(nameToFind, secondSearchInd - 1);
                        while (secondSearchInd > 0 && (!Char.IsPunctuation(line, secondSearchInd - 1) && !Char.IsSeparator(line, secondSearchInd - 1)));
                        if (secondSearchInd > 0)
                            lastInd = secondSearchInd;

                        int lastInd2 = line.LastIndexOf(nameToFind, lastInd);
                        string betweenTwoName = null;

                        if (lastInd2 > -1)
                        {
                            betweenTwoName = line.Substring(lastInd2, lastInd - lastInd2);
                        }

                        if (!String.IsNullOrWhiteSpace(betweenTwoName) //���� ���� ��� ���� ��������� �����
                            && betweenTwoName.IndexOf('(') > -1  //� ����� ����� ����������� ����� (
                            && betweenTwoName.IndexOfAny(arrB) == -1) // � ��� ������ ������
                        {
                            function.AddDefinition(fileName, Line, (uint)lastInd2 + 1); //�� � ������� ������������ ������ ��� - �������� ������������
                        }
                        else
                        {
                            function.AddDefinition(fileName, Line, (uint)lastInd + 1);
                        }

                    }
                    else
                    { //������� ����� ��������� ����� � ������ ��������
                        if (line.ToLower().LastIndexOf(nameToFind.ToLower()) != -1)
                            function.AddDefinition(fileName, Line, (uint)line.ToLower().LastIndexOf(nameToFind.ToLower()) + 1);
                        else
                            function.AddDefinition(fileName, Line, 1);
                    }
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                function.AddDefinition(fileName, Line, 0);
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                function.AddDefinition(fileName, Line, 0);
            }
            catch (Exception ex)
            {
                //��������� ���������
                string message = new string[]
                {
                        "�� ������� ������� ���� " + fileName + ".",
                        ex.ToFullMultiLineMessage()
                }.ToMultiLineString();

                IA.Monitor.Log.Error(PluginSettings.Name, message);
            }
        }
    }
}
