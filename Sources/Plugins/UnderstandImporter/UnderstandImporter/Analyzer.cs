using System;
using System.Collections.Generic;
using Store;
using System.Linq;
using IA.Extensions;
using System.IO;

using IOController;
using FileOperations;

namespace IA.Plugins.Importers.UnderstandImporter
{
    /// <summary>
    /// �����-�������
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
    class Reader
    {
        readonly System.IO.StreamReader reader;
        readonly int taskNumber;

        internal Reader(string fileName, int taskNumber)
        {
            reader = new System.IO.StreamReader(fileName);
            this.taskNumber = taskNumber;
        }

        internal bool EndOfStream
        {
            get { return reader.EndOfStream; }
        }

        internal string ReadLine()
        {
            string line = reader.ReadLine();

            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)taskNumber).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)reader.BaseStream.Position);

            return line;
        }

        internal void Close()
        {
            reader.Close();
        }

        internal bool isOpened()
        {
            return reader == null;
        }
    }

    class ReportEnumJavaEntity
    {
        public string name;
        public string filename;
        public int line;
    }

    /// <summary>
    /// ������� ����� ��� ������� ����������
    /// </summary>
    public static class Analyzer
    {
        static Storage storage;

        //������� ��� ���� 197
        //static Dictionary<string, ReportEnumJavaEntity> enums;
        static List<ReportEnumJavaEntity> enums;
        /// <summary>
        /// ������ ����� � ���������� - DataDictionary.txt
        /// </summary>
        /// <param name="storage">���������</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        static void ReadEntityFile(Storage storage)
        {
            Analyzer.storage = storage;
            Reader entityFileStream = null;

            #region �������� �����
            HashSet<string> NonClassKeyWords = new HashSet<string> {"Method", "Function", "Constructor", "Constuctor", "Finalizer", "Indexer", "Unit", "Procedure", "Destructor", "Variable",
                                                  "Sql", "Property", "Parameter","Enum","Enumerator","Routine","Field","Type","Delegate","Const","Struct","Interface","Event",
                                                  "Macro","Object","Environment","Param","Record","Typedef","Union","Resourcestring","Program","Tag","Id","Package","Constant",
                                                  "TypeSelector","Media","Frame","Anchor","Fragment","Page", "EnumConstant"};
            HashSet<string> NonFunctionKeyWords = new HashSet<string>{"Class", "Variable", "Sql", "Property", "Parameter","Enum","Enumerator","Routine","Field","Type","Delegate","Const",
                                                    "Struct","Interface","Event","Macro","Object","Environment","Param","Record","Typedef","Union","Resourcestring","Program",
                                                    "Tag","Id","Package","Constant","TypeSelector","Media","Frame","Anchor","Fragment","Page", "EnumConstant"};
            HashSet<string> FunctionKeyWords = new HashSet<string> { "Method", "Function", "Constructor", "Constuctor", "Finalizer", "Indexer", "Unit", "Procedure", "Destructor" };
            #endregion

            #region ������ ������
            try
            {
                entityFileStream = new Reader(Path.Combine(PluginSettings.ReportsDirectoryPath, "DataDictionary.txt"), 0);
                if (entityFileStream.isOpened())
                    throw new Exception("���� �� ������� ��������� ������� �� ������");

                string line;

                #region ���������� ��������� �����
                for (int i = 0; i < 3; ++i)
                {
                    entityFileStream.ReadLine(); //���������� ��������� �����
                }
                #endregion

                while ((line = entityFileStream.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(line)) continue;
                    if (!line.Contains("(")) continue;
                    if (line[3] == '[') continue; //������� ���������� ������

                    int idx1 = line.LastIndexOf('(');
                    int idx2 = line.LastIndexOf(')');
                    string type = line.Substring(idx1 + 1, idx2 - idx1 - 1);
                    string[] array = type.Split(' ');
                    bool found = false;
                    foreach (string word in array)
                    {
                        if (word == "Class")
                        {
                            GenClass(line);
                            found = true;
                            break;
                        }
                        if (NonClassKeyWords.Contains(word))
                        {
                            //������� ��� ���� 197
                            if (PluginSettings.IsJava && word.Equals("enum", StringComparison.InvariantCultureIgnoreCase))
                            {
                                //��������� �������� ��� ���� ������: � ������� ������ ��� ����� ������
                                string line1 = entityFileStream.ReadLine();
                                int pos1 = line1.LastIndexOf(',') + 1;
                                int pos2 = line1.LastIndexOf(']');
                                if (pos2 > pos1 && pos1 != -1)
                                {
                                    string numStr = line1.Substring(pos1, pos2 - pos1);
                                    var entry = new ReportEnumJavaEntity()
                                    {
                                        name = line.Substring(0, line.IndexOf(' ')),
                                        line = int.Parse(numStr),
                                        filename = line1.Substring(line1.IndexOf('[') + 1, pos1 - 2)
                                    };
                                    enums.Add(entry);
                                    GenClass(line);
                                }
                            }
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        IA.Monitor.Log.Error(PluginSettings.Name, "����������� ��� �������� " + type);
                }
            }
            finally
            {
                if (entityFileStream != null) entityFileStream.Close();
            }
            #endregion

            #region ������ �������
            try
            {
                string dataDictionaryPath = Path.Combine(PluginSettings.ReportsDirectoryPath, "DataDictionary.txt");
                entityFileStream = new Reader(dataDictionaryPath, 1);
                if (entityFileStream.isOpened())
                    throw new Exception("���� �� ������� ��������� ������� �� ������");

                string line;

                #region ���������� ��������� �����
                for (int i = 0; i < 3; ++i)
                {
                    entityFileStream.ReadLine(); //���������� ��������� �����
                }
                #endregion

                while ((line = entityFileStream.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(line)) continue;
                    if (!line.Contains("(")) continue;
                    if (line[3] == '[') continue; //������� ���������� ������

                    int idx1 = line.LastIndexOf('(');
                    int idx2 = line.LastIndexOf(')');
                    string type = line.Substring(idx1 + 1, idx2 - idx1 - 1);
                    string[] array = type.Split(' ');
                    bool found = false;
                    foreach (string word in array)
                    {
                        if (FunctionKeyWords.Contains(word))
                        {
                            found = true;

                            GenFunction(line, entityFileStream.ReadLine(), storage.functions);
                            break;
                        }
                        if (NonFunctionKeyWords.Contains(word))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        IA.Monitor.Log.Error(PluginSettings.Name, "����������� ��� �������� " + type);
                }
            }
            finally
            {
                entityFileStream.Close();
            }
            #endregion
        }

        /// <summary>
        /// ���������� ������ �������
        /// </summary>
        /// <param name="fn">���������� �������</param>
        /// <param name="CallLine">���������� �������</param>
        /// <param name="storage">���������</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        static void AddCall(Function fn, string CallLine, Storage storage)
        {
            uint DeclOff = 0;
            string path;
            string DeclPath;
            string CallerName;
            IFile fl;
            Store.IFunction Caller;
            ulong fnl;
            ulong ln;
            uint DeclLine;
            bool found;

            if (CallLine.StartsWith("    Call [") || CallLine.StartsWith("    Virtual Call ["))
            {
                if (fn != null)
                {
                    path = CallLine.Substring(CallLine.IndexOf("[") + 1,
                                              CallLine.IndexOf("]   ") - CallLine.IndexOf("[") - 1);
                    DeclPath = path.Substring(0, path.LastIndexOf(','));
                    if (!string.IsNullOrEmpty(PluginSettings.SourcesDirectoryPath))
                    {
                        DeclPath = DirectoryController.CanonizePath(DeclPath).Replace(DirectoryController.CanonizePath(PluginSettings.SourcesDirectoryPath), DirectoryController.CanonizePath(storage.appliedSettings.FileListPath));
                    }
                    DeclLine = Convert.ToUInt32(path.Substring(path.LastIndexOf(" ") + 1));

                    CallerName = CallLine.Substring(CallLine.IndexOf("]   ") + 4).Replace("::", ".");

                    while (CallerName.StartsWith(" "))
                        CallerName = CallerName.Substring(1);

                    fl = storage.files.FindOrGenerate(DeclPath, enFileKindForAdd.fileWithPrefix);
                    Caller = null;
                    ln = 0;

                    foreach (Store.IFunction fnc in fl.FunctionsDefinedInTheFile())
                    {
                        if (CallerName == fnc.FullName)
                        {
                            Location def = fnc.Definition(); //������ ����� �� ������ ����� �����������, ��� ��� fnc ���������� ������  �� �������, ������� �����������
                            if (def == null)
                                throw new ApplicationException("� UnderstandImporter ��������� �������������� ��������. ���� �������� ���������");

                            fnl = def.GetLine();
                            if (fnl > ln && fnl < DeclLine)
                            {
                                ln = fnl;
                                Caller = fnc;
                            }
                        }
                    }

                    if (Caller != null)
                    {
                        found = false;
                        foreach (IFunctionCall called in Caller.Calles())
                        {
                            if (called.Calles == fn.function)
                                found = true;
                        }

                        if (!found)
                        {
                            try
                            {
                                FileOperations.AbstractReader sr = null;
                                try
                                {
                                    sr = new FileOperations.AbstractReader(DeclPath);
                                }
                                catch (Exception ex)
                                {
                                    //��������� ���������
                                    string message = new string[]
                                    {
                                        "�� ������� ������� ���� " + DeclPath + ".",
                                        ex.ToFullMultiLineMessage()
                                    }.ToMultiLineString();

                                    IA.Monitor.Log.Error(PluginSettings.Name, message);
                                }


                                string line;
                                line = sr.getLineN((int)DeclLine);
                                DeclOff = (uint)sr.getLineOffset((int)DeclLine);

                                DeclOff += (uint)line.IndexOf(fn.function.Name);
                            }
                            catch
                            {
                            }

                            Caller.AddCall(fn.function, DeclPath, DeclOff);
                        }
                    }
                }
            }
            else if (CallLine.StartsWith("    Pointer ["))
            {
                if (fn != null)
                {
                    path = CallLine.Substring(CallLine.IndexOf("[") + 1, CallLine.LastIndexOf("]   ") - CallLine.IndexOf("[") - 1);
                    DeclPath = path.Substring(0, path.LastIndexOf(','));
                    DeclLine = Convert.ToUInt32(path.Substring(path.LastIndexOf(" ") + 1));
                    if (!string.IsNullOrEmpty(PluginSettings.SourcesDirectoryPath))
                    {
                        DeclPath = DeclPath.ToLowerInvariant().Replace(DirectoryController.CanonizePath(PluginSettings.SourcesDirectoryPath), DirectoryController.CanonizePath(storage.appliedSettings.FileListPath));
                    }
                    CallerName = CallLine.Substring(CallLine.LastIndexOf("]   ") + 4).Replace("::", ".");

                    fl = storage.files.Find(DeclPath);
                    Caller = null;
                    ln = 0;

                    foreach (Store.IFunction fnc in fl.FunctionsDefinedInTheFile())
                    {
                        if (CallerName == fnc.FullName)
                        {
                            fnl = fnc.Definition().GetLine();
                            if (fnl > ln && fnl < DeclLine)
                            {
                                ln = fnl;
                                Caller = fnc;
                            }
                        }
                    }

                    if (Caller != null)
                    {
                        found = false;
                        foreach (IFunctionCall called in Caller.Calles())
                            if (called.Calles == fn.function)
                                found = true;

                        foreach (PointsToFunction pointer in Caller.OperatorsGetPointerToFunction())
                            if (fn.function.Id == pointer.function)
                                found = true;

                        if (!found)
                        {
                            try
                            {
                                FileOperations.AbstractReader sr = null;
                                try
                                {
                                    sr = new FileOperations.AbstractReader(DeclPath);
                                }
                                catch (Exception ex)
                                {
                                    //��������� ���������
                                    string message = new string[]
                                    {
                                        "�� ������� ������� ���� " + DeclPath + ".",
                                        ex.ToFullMultiLineMessage()
                                    }.ToMultiLineString();

                                    IA.Monitor.Log.Error(PluginSettings.Name, message);
                                }

                                string line;
                                line = sr.getLineN((int)DeclLine);
                                DeclOff = (uint)sr.getLineOffset((int)DeclLine);

                                DeclOff += (uint)line.IndexOf(fn.function.Name);
                            }
                            catch { }

                            Store.Variable var = storage.variables.AddVariable();
                            PointsToFunction ptf1 = var.AddPointsToFunctions();
                            ptf1.function = fn.function.Id;
                            ptf1.AddPosition(fl, DeclOff, Caller);
                            ptf1.isGetAddress = true;
                            ptf1.Save();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// ������ ����� � ������� �������
        /// </summary>
        /// <param name="functions">������� � ���������</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.UInt32.ToString"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        static void ReadReferencesFile(Functions functions)
        {
            Reader referencesFileStream = null;

            try
            {
                string programUnitsPath = Path.Combine(PluginSettings.ReportsDirectoryPath, "ProgramUnits.txt");

                // Add 25.04.2014 - ��������� ��������
                File.Copy(programUnitsPath, storage.GetSharedFilePath(Store.SharedFiles.enSharedFiles.UnderstandProtocolProgramUnits), true);

                referencesFileStream = new Reader(programUnitsPath, 2);
                string buffer;

                for (int i = 0; i < 3; ++i)
                    referencesFileStream.ReadLine(); //���������� ��������� �����


                List<Function> fn = new List<Function>();

                List<string> Collector = new List<string>();

                List<KeyValuePair<string, uint>> DeclCollector = new List<KeyValuePair<string, uint>>();

                while ((buffer = referencesFileStream.ReadLine()) != null)
                {
                    if (buffer.EndsWith("Method)") || buffer.EndsWith("Constructor)") || buffer.EndsWith("Destructor)") || buffer.EndsWith("Function)") || buffer.EndsWith("Finalizer)") ||
                        buffer.EndsWith("Constuctor)") || buffer.EndsWith("Unit)") || buffer.EndsWith("Procedure)"))
                    {
                        fn.Clear();
                        Collector.Clear();
                        DeclCollector.Clear();

                        string name = buffer.Substring(0, buffer.LastIndexOf('(') - 3);

                        while (!string.IsNullOrEmpty(buffer = referencesFileStream.ReadLine())) // ������ ��� ���������
                        {
                            if (buffer.StartsWith("  Declared as:")) // ���� ���� ������ � ����� ����������
                                continue; // ������ ����� ��������                            

                            if (buffer.StartsWith("    Define ["))
                            {
                                // ���� ��������
                                string path = buffer.Substring(buffer.IndexOf("[") + 1, buffer.IndexOf("]   ") - buffer.IndexOf("[") - 1);
                                if (!string.IsNullOrEmpty(PluginSettings.SourcesDirectoryPath))
                                {
                                    path = path.ToLowerInvariant().Replace(DirectoryController.CanonizePath(PluginSettings.SourcesDirectoryPath), DirectoryController.CanonizePath(storage.appliedSettings.FileListPath));
                                }

                                string DeclPath = path.Substring(0, path.LastIndexOf(','));
                                if (!string.IsNullOrEmpty(PluginSettings.SourcesDirectoryPath))
                                {
                                    DeclPath = DeclPath.ToLowerInvariant().Replace(DirectoryController.CanonizePath(PluginSettings.SourcesDirectoryPath), DirectoryController.CanonizePath(storage.appliedSettings.FileListPath));
                                }
                                uint DeclLine = Convert.ToUInt32(path.Substring(path.LastIndexOf(" ") + 1));

                                bool found = false;
                                foreach (Store.IFunction Func in storage.functions.GetFunction(name.Replace("::", ".").Split('.')))
                                {
                                    Location funcDefenition = Func.Definition();
                                    if (funcDefenition != null && funcDefenition.GetFile().FullFileName_Original.ToLower() == DeclPath.ToLower() && funcDefenition.GetLine() == DeclLine)
                                    {
                                        fn.Add(new Function(Func));
                                        found = true;
                                        break;
                                    }
                                }

                                if (!found)
                                {
                                    GenFunction(name.Replace("::", ".") + "   (Public Method)", "   [" + DeclPath + ", " + DeclLine.ToString() + "]", functions);
                                    foreach (Store.IFunction Func in storage.functions.GetFunction(name.Replace("::", ".").Split('.')))
                                    {
                                        Location funcDefenition = Func.Definition();
                                        if (funcDefenition != null && funcDefenition.GetFile().FullFileName_Original.ToLower() == DeclPath.ToLower() && funcDefenition.GetLine() == DeclLine)
                                        {
                                            fn.Add(new Function(Func));
                                            found = true;
                                            break;
                                        }
                                    }
                                }

                                foreach (Function f in fn)
                                    foreach (string line in Collector)
                                        AddCall(f, line, storage);
                            }
                            else if (buffer.StartsWith("    Declare ["))
                            {
                                // ���� ����������
                                string path = buffer.Substring(buffer.IndexOf("[") + 1, buffer.IndexOf("]   ") - buffer.IndexOf("[") - 1);
                                if (!string.IsNullOrEmpty(PluginSettings.SourcesDirectoryPath))
                                {
                                    path = path.ToLowerInvariant().Replace(DirectoryController.CanonizePath(PluginSettings.SourcesDirectoryPath), DirectoryController.CanonizePath(storage.appliedSettings.FileListPath));
                                }
                                if (Convert.ToUInt32(path.Substring(path.LastIndexOf(" ") + 1)) != 0)
                                    DeclCollector.Add(new KeyValuePair<string, uint>(path.Substring(0, path.LastIndexOf(',')), Convert.ToUInt32(path.Substring(path.LastIndexOf(" ") + 1))));
                            }
                            else if (buffer.StartsWith("    Call [") || buffer.StartsWith("    Virtual Call [") || buffer.StartsWith("    Pointer ["))
                            {
                                foreach (Function f in fn)
                                    AddCall(f, buffer, storage);

                                if (fn.Count == 0)
                                    Collector.Add(buffer);
                            }
                        }

                        // �� ����� ����� �����������
                        if (fn.Count == 0)
                        {
                            foreach (Store.IFunction Func in storage.functions.GetFunction(name.Replace("::", ".").Split('.')))
                            {
                                foreach (string line in Collector)
                                    AddCall(new Function(Func), line, storage);
                            }
                        }

                        foreach (KeyValuePair<string, uint> kvp in DeclCollector)
                            foreach (Function f in fn)
                                f.function.AddDeclaration(kvp.Key, kvp.Value);
                    }
                    else
                    {
                        while (!string.IsNullOrEmpty(buffer))
                        {
                            buffer = referencesFileStream.ReadLine();
                        }
                    }

                }
            }
            finally
            {
                if (referencesFileStream != null) referencesFileStream.Close();
            }
        }

        /// <summary>
        /// ������ ����������
        /// </summary>
        static void ReadVariablesFile()
        {
            string objectsPath = Path.Combine(PluginSettings.ReportsDirectoryPath, "Objects.txt");
            if (!System.IO.File.Exists(objectsPath))
                return;
            Reader variablesFileStream = null;
            try
            {
                variablesFileStream = new Reader(objectsPath, 3);
                string buffer;

                for (int i = 0; i < 3; ++i)
                    variablesFileStream.ReadLine(); //���������� ��������� �����

                List<string> funcList = new List<string>();

                HashSet<string> lineEndings = new HashSet<string>()
                {
                    "(Global Object)",
                    "(Local Object)",
                    "(Param)",
                    "(Parameter)",
                    "(Private Object)",
                    "(Private Static Object)",
                    "(Protected Object)",
                    "(Protected Static Object)",
                    "(Public Object)",
                    "(Public Static Object)",
                    "(Static Global Object)",
                    "(Static Local Object)",
                    "(Unknown Member Object)",
                    "(Unknown Object)",
                    "(Unknown Variable)",
                    "(Public Static Field)",
                    "(Private Static Field)",
                    "(Protected Static Field)",
                    "(Private Field)",
                    "(Public Field)",
                    "(Protected Field)",
                    "(Variable)"
                };

                while ((buffer = variablesFileStream.ReadLine()) != null)
                {
                    int bracketPos = buffer.LastIndexOf('(');
                    if (bracketPos != -1 && lineEndings.Contains(buffer.Substring(bracketPos)))
                    {
                        string name; //��� ����������
                        string type; //��� ����������
                        try
                        {
                            //����� ��� ����������
                            name = buffer.Substring(0, bracketPos - 3).Replace("::", ".");//name = buffer.Substring(0, buffer.LastIndexOf('(') - 3).Replace("::", ".");

                            //����� ��� ����������
                            type = buffer.Substring(bracketPos + 1, buffer.LastIndexOf(')') - bracketPos - 1);//type = buffer.Substring(buffer.LastIndexOf('(') + 1, buffer.LastIndexOf(')') - buffer.LastIndexOf('(') - 1);
                        }
                        catch
                        {
                            IA.Monitor.Log.Warning(PluginSettings.Name, buffer);
                            continue;
                        }

                        Store.Variable variable = null; //= GenVar(name, type.Contains("Static"));
                        string[] variableWords = {"Global Object","Private Static Object","Protected Static Object","Public Static Object","Static Global Object",
                                                  "Public Static Field","Private Static Field", "Protected Static Field"};
                        string[] variableWordsUnknownVars = { "Unknown Object", "Unknown Variable", "Variable" };

                        variable = GenVar(name, variableWords.Contains(type) || variableWordsUnknownVars.Contains(type) && funcList.Count > 1);

                        //��� ������ ���������� � ��� ����� ���� ������ �������, ��� ��� ������������, ���� ������ ������� �� ����� ��� ����� �������
                        //��� ����� (Unknown Object, Unknown Variable, Variable), �� ���������� ��������� ����������
                        funcList.Clear();

                        while (!string.IsNullOrEmpty(buffer = variablesFileStream.ReadLine())) // ������ ��� ���������
                        {
                            if (buffer.StartsWith("  Declared as:"))
                                continue; // ����� ������ ����������                         

                            string functionName; // ������� ������ ����, ����� ���� ��� �� �����
                            uint linePath; //����� ����, ��� ���������� � path - ������
                            string filePath; //����� ����, ��� ���������� � path - ����

                            int posOfOpenSquareBracket = buffer.IndexOf("[");

                            try
                            {
                                //string fullPath = buffer.Substring(buffer.IndexOf("[") + 1, buffer.IndexOf("]   ") - buffer.IndexOf("[") - 1); //����� �������� ������ ���� � �������� ��� ���������� (�����������, �������������, set, get)
                                string fullPath = buffer.Substring(posOfOpenSquareBracket + 1, buffer.IndexOf("]   ") - posOfOpenSquareBracket - 1);
                                filePath = fullPath.Substring(0, fullPath.LastIndexOf(','));
                                if (!string.IsNullOrEmpty(PluginSettings.SourcesDirectoryPath))
                                {
                                    filePath = filePath.ToLowerInvariant().Replace(DirectoryController.CanonizePath(PluginSettings.SourcesDirectoryPath), DirectoryController.CanonizePath(storage.appliedSettings.FileListPath));
                                }
                                linePath = Convert.ToUInt32(fullPath.Substring(fullPath.LastIndexOf(" ") + 1));

                                //��� ��������� �� ��� �������, � ������� ��� ����� ���������
                                functionName = buffer.Substring(buffer.IndexOf("]") + 1).Trim().Replace("::", "."); //����� ���� ����� ���� ��������!!!!!!!!!!!!!!
                            }
                            catch
                            {
                                IA.Monitor.Log.Warning(PluginSettings.Name, buffer);
                                continue;
                            }

                            //���� �� ��������� �� �������
                            Store.IFunction searchFunc = null;
                            IFile searchFile = storage.files.FindOrGenerate(filePath, enFileKindForAdd.fileWithPrefix);
                            foreach (Store.IFunction Func in storage.functions.GetFunction(functionName.Split('.')))
                            {
                                //��� ���� ���������, ��� � ������� ���� ����������� � ���� �����
                                if (Func.Definition() != null)
                                {
                                    //����� ������� ��� ���������� ���� ����������
                                    searchFunc = Func;

                                    //��������� ������� � ������
                                    if (!funcList.Contains(functionName))
                                        funcList.Add(functionName);
                                }
                            }
                            //if (functionName.Contains("\\"))
                            //{
                            //    //���� ���������� ���������� ��� �������

                            //}

                            //��������� ��� �� �������� � ������ ��������������� ���������� � ���������

                            string opType; // ������� ������ ����, ����� ���� ��� �� �����

                            try
                            {
                                opType = buffer.Substring(0, posOfOpenSquareBracket - 1).Trim();

                            }
                            catch
                            {
                                IA.Monitor.Log.Warning(PluginSettings.Name, buffer);
                                continue;
                            }


                            switch (opType)
                            {
                                case "Define":
                                    {
                                        //if (searchFunc == null)
                                        //{
                                        //    IA.Monitor.Log.Warning(PluginSettings.Name, "�� ������� ������� " + functionName + ", ��� ���������� ���������� " + variable.Name);
                                        //    continue;
                                        //}

                                        try
                                        {
                                            GenVariableDefinition(variable, searchFile, linePath, searchFunc);
                                        }
                                        catch
                                        {
                                            IA.Monitor.Log.Warning(PluginSettings.Name, buffer);
                                        }

                                        break;
                                    }
                                case "Use":
                                case "Deref Use":
                                case "Return":
                                case "Cast":
                                case "Macro":
                                case "Raise":
                                case "Handle":
                                    {
                                        //if (searchFunc == null)
                                        //{
                                        //    IA.Monitor.Log.Warning(PluginSettings.Name, "�� ������� ������� " + functionName + ", ��� ������������ �������� ���������� " + variable.Name);
                                        //    continue;
                                        //}

                                        try
                                        {
                                            GenVariableGet(variable, searchFile, linePath, searchFunc);
                                        }
                                        catch
                                        {
                                            IA.Monitor.Log.Warning(PluginSettings.Name, buffer);
                                        }

                                        break;
                                    }
                                case "Set":
                                case "Deref Set":
                                case "Modify":
                                case "Init":
                                case "Declare":
                                case "Alloc":
                                case "Deref Modify":
                                    {
                                        //if (searchFunc == null)
                                        //{
                                        //    IA.Monitor.Log.Warning(PluginSettings.Name, "�� ������� ������� " + functionName + ", ��� ������������� �������� ���������� " + variable.Name);
                                        //    continue;
                                        //}

                                        try
                                        {
                                            GenVariableSet(variable, searchFile, linePath, searchFunc);
                                        }
                                        catch
                                        {
                                            IA.Monitor.Log.Warning(PluginSettings.Name, buffer);
                                        }

                                        break;
                                    }
                                case "Call":
                                case "Deref Call":
                                case "Addr Use":
                                    {
                                        //if (searchFunc == null)
                                        //{
                                        //    //IA.Monitor.Log.Warning(PluginSettings.Name, "�� ������� ������� " + functionName + ", ��� ���������� �������� ���������� " + variable.Name);
                                        //    continue;
                                        //}

                                        try
                                        {
                                            GenVariableCall(variable, searchFile, linePath, searchFunc);
                                        }
                                        catch
                                        {
                                            IA.Monitor.Log.Warning(PluginSettings.Name, buffer);
                                        }

                                        break;
                                    }
                                default:
                                    {
                                        //��� ��� ����������� ������ ����
                                        //IA.Monitor.Log.Warning(PluginSettings.Name, "�� ���������� ����� " + buffer.Substring(0, buffer.IndexOf("[") - 1).Trim());
                                        break;
                                    }
                            }
                        }
                    }
                    else
                    {
                        while (!string.IsNullOrEmpty(buffer))
                            buffer = variablesFileStream.ReadLine();
                    }

                }
            }
            finally
            {
                if (variablesFileStream != null) variablesFileStream.Close();
            }
        }

        private static void GenVariableCall(Variable variable, IFile searchFile, uint linePath, Store.IFunction searchFunc)
        {
            char[] arrF = { '.', ':' };
            char[] arrL = { ' ', '+', '=', '-', '*', '/', '[', '(' };
            bool isOpened = true;
            AbstractReader sr = null;
            try
            {
                sr = new AbstractReader(searchFile.FullFileName_Original);
            }
            catch (Exception ex)
            {
                //��������� ���������
                string message = new string[]
                    {
                        "�� ������� ������� ���� " + searchFile + ".",
                        ex.ToFullMultiLineMessage()
                    }.ToMultiLineString();

                IA.Monitor.Log.Error(PluginSettings.Name, message);
                isOpened = false;
            }

            if (isOpened)
            {
                string line;
                line = sr.getLineN((int)linePath);

                if (line == null)
                    return;

                string nameToFind = variable.Name;
                foreach (char sym in arrF)
                    if (nameToFind.LastIndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(nameToFind.LastIndexOf(sym) + 1);

                foreach (char sym in arrL)
                    if (nameToFind.IndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(0, nameToFind.IndexOf(sym));

                //� function.AddDefinition � ������� ���������� 1, ��� ��� ������� ���������� � �������, � �� � ��������
                if (nameToFind.Length == 0)
                {
                    variable.AddValueCallPosition(searchFile, linePath, 1, searchFunc); ;
                }
                else
                {
                    if (line.LastIndexOf(nameToFind) != -1)
                        variable.AddValueCallPosition(searchFile, linePath, (uint)line.LastIndexOf(nameToFind) + 1, searchFunc);
                    else
                    {
                        if (line.ToLower().LastIndexOf(nameToFind.ToLower()) != -1)
                            variable.AddValueCallPosition(searchFile, linePath, (uint)line.ToLower().LastIndexOf(nameToFind.ToLower()) + 1, searchFunc);
                        else
                            variable.AddValueCallPosition(searchFile, linePath, 1, searchFunc);
                    }
                }
            }
        }

        private static void GenVariableGet(Variable variable, IFile searchFile, uint linePath, Store.IFunction searchFunc)
        {
            char[] arrF = { '.', ':' };
            char[] arrL = { ' ', '+', '=', '-', '*', '/', '[', '(' };
            bool isOpened = true;
            AbstractReader sr = null;
            try
            {
                sr = new AbstractReader(searchFile.FullFileName_Original);
            }
            catch (Exception ex)
            {
                //��������� ���������
                string message = new string[]
                    {
                        "�� ������� ������� ���� " + searchFile + ".",
                        ex.ToFullMultiLineMessage()
                    }.ToMultiLineString();

                IA.Monitor.Log.Error(PluginSettings.Name, message);
                isOpened = false;
            }

            if (isOpened)
            {
                string line;
                line = sr.getLineN((int)linePath);

                if (line == null)
                    return;

                string nameToFind = variable.Name;
                foreach (char sym in arrF)
                    if (nameToFind.LastIndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(nameToFind.LastIndexOf(sym) + 1);

                foreach (char sym in arrL)
                    if (nameToFind.IndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(0, nameToFind.IndexOf(sym));

                //� function.AddDefinition � ������� ���������� 1, ��� ��� ������� ���������� � �������, � �� � ��������
                if (nameToFind.Length == 0)
                {
                    variable.AddValueGetPosition(searchFile, linePath, 1, searchFunc); ;
                }
                else
                {
                    if (line.LastIndexOf(nameToFind) != -1)
                        variable.AddValueGetPosition(searchFile, linePath, (uint)line.LastIndexOf(nameToFind) + 1, searchFunc);
                    else
                    {
                        if (line.ToLower().LastIndexOf(nameToFind.ToLower()) != -1)
                            variable.AddValueGetPosition(searchFile, linePath, (uint)line.ToLower().LastIndexOf(nameToFind.ToLower()) + 1, searchFunc);
                        else
                            variable.AddValueGetPosition(searchFile, linePath, 1, searchFunc);
                    }
                }
            }
        }

        /// <summary>
        /// ������� ��� ����������� ���������� 
        /// </summary>
        /// <param name="variable">����������</param>
        /// <param name="searchFile">����</param>
        /// <param name="linePath">������</param>
        /// <param name="searchFunc">�������, � ������� ���������� ����������</param>
        private static void GenVariableSet(Variable variable, IFile searchFile, uint linePath, Store.IFunction searchFunc)
        {
            char[] arrF = { '.', ':' };
            char[] arrL = { ' ', '+', '=', '-', '*', '/', '[', '(' };
            bool isOpened = true;
            AbstractReader sr = null;
            try
            {
                sr = new AbstractReader(searchFile.FullFileName_Original);
            }
            catch (Exception ex)
            {
                //��������� ���������
                string message = new string[]
                    {
                        "�� ������� ������� ���� " + searchFile + ".",
                        ex.ToFullMultiLineMessage()
                    }.ToMultiLineString();

                IA.Monitor.Log.Error(PluginSettings.Name, message);
                isOpened = false;
            }

            if (isOpened)
            {
                string line;
                line = sr.getLineN((int)linePath);

                if (line == null)
                    return;

                string nameToFind = variable.Name;
                foreach (char sym in arrF)
                    if (nameToFind.LastIndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(nameToFind.LastIndexOf(sym) + 1);

                foreach (char sym in arrL)
                    if (nameToFind.IndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(0, nameToFind.IndexOf(sym));

                //� function.AddDefinition � ������� ���������� 1, ��� ��� ������� ���������� � �������, � �� � ��������
                if (nameToFind.Length == 0)
                {
                    variable.AddValueSetPosition(searchFile, linePath, 1, searchFunc); ;
                }
                else
                {
                    if (line.LastIndexOf(nameToFind) != -1)
                        variable.AddValueSetPosition(searchFile, linePath, (uint)line.LastIndexOf(nameToFind) + 1, searchFunc);
                    else
                    {
                        if (line.ToLower().LastIndexOf(nameToFind.ToLower()) != -1)
                            variable.AddValueSetPosition(searchFile, linePath, (uint)line.ToLower().LastIndexOf(nameToFind.ToLower()) + 1, searchFunc);
                        else
                            variable.AddValueSetPosition(searchFile, linePath, 1, searchFunc);
                    }
                }
            }
        }

        /// <summary>
        /// ������� ��� ����������� ���������� 
        /// </summary>
        /// <param name="variable">����������</param>
        /// <param name="searchFile">����</param>
        /// <param name="linePath">������</param>
        /// <param name="searchFunc">�������, � ������� ���������� ����������</param>
        private static void GenVariableDefinition(Variable variable, IFile searchFile, uint linePath, Store.IFunction searchFunc)
        {
            char[] arrF = { '.', ':' };
            char[] arrL = { ' ', '+', '=', '-', '*', '/', '[', '(' };
            bool isOpened = true;
            AbstractReader sr = null;
            try
            {
                sr = new AbstractReader(searchFile.FullFileName_Original);
            }
            catch (Exception ex)
            {
                //��������� ���������
                string message = new string[]
                    {
                        "�� ������� ������� ���� " + searchFile + ".",
                        ex.ToFullMultiLineMessage()
                    }.ToMultiLineString();

                IA.Monitor.Log.Error(PluginSettings.Name, message);
                isOpened = false;
            }

            if (isOpened)
            {
                string line;
                line = sr.getLineN((int)linePath);

                if (line == null)
                    return;

                string nameToFind = variable.Name;
                foreach (char sym in arrF)
                    if (nameToFind.LastIndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(nameToFind.LastIndexOf(sym) + 1);

                foreach (char sym in arrL)
                    if (nameToFind.IndexOf(sym) >= 0)
                        nameToFind = nameToFind.Substring(0, nameToFind.IndexOf(sym));

                //� function.AddDefinition � ������� ���������� 1, ��� ��� ������� ���������� � �������, � �� � ��������
                if (nameToFind.Length == 0)
                {
                    variable.AddDefinition(searchFile, linePath, 1, searchFunc); ;
                }
                else
                {
                    if (line.LastIndexOf(nameToFind) != -1)
                        variable.AddDefinition(searchFile, linePath, (uint)line.LastIndexOf(nameToFind) + 1, searchFunc);
                    else
                    {
                        if (line.ToLower().LastIndexOf(nameToFind.ToLower()) != -1)
                            variable.AddDefinition(searchFile, linePath, (uint)line.ToLower().LastIndexOf(nameToFind.ToLower()) + 1, searchFunc);
                        else
                            variable.AddDefinition(searchFile, linePath, 1, searchFunc);
                    }
                }
            }
        }

        delegate void MethodDelegate(Store.Functions fns);


        /// <summary>
        /// ������ �������
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="stopper"></param>
        /// <returns></returns>
        public static bool Run(Storage storage, bool stopper)
        {
            //enums = new Dictionary<string, ReportEnumJavaEntity>();
            enums = new List<ReportEnumJavaEntity>();

            if (PluginSettings.IsIgnoreMacroLines)
            {
                RunIgnoreMacroLines();
            }
            ReadEntityFile(storage);
            if (stopper)
            {
                return false;
            }

            ReadReferencesFile(storage.functions);
            if (stopper)
            {
                return false;
            }

            if (PluginSettings.IsLoadVariables)
            {
                ReadVariablesFile();
                if (stopper)
                {
                    return false;
                }
            }



            ReadRedundantFunctions();
            if (stopper)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// ��������� ������ ���������� �������
        /// </summary>
        private static void ReadRedundantFunctions()
        {
            try
            {
                string unusedProgramUnitsPath = Path.Combine(PluginSettings.ReportsDirectoryPath, "UnusedProgramUnits.txt");
                Reader entityFileStream = new Reader(unusedProgramUnitsPath, 4);
                if (entityFileStream.isOpened())
                {
                    return;
                }
                //���������� ���������
                for (int i = 0; i < 3; ++i)
                {
                    entityFileStream.ReadLine();
                }
                string file, functionLine, line, functionName;
                int functionline;

                while ((line = entityFileStream.ReadLine()) != null)
                {
                    if (!string.IsNullOrEmpty(PluginSettings.SourcesDirectoryPath))
                    {
                        file = line.ToLowerInvariant().Replace(DirectoryController.CanonizePath(PluginSettings.SourcesDirectoryPath), DirectoryController.CanonizePath(storage.appliedSettings.FileListPath)); ;
                    }
                    else
                    {
                        file = line;
                    }
                    while (!string.IsNullOrEmpty(functionLine = entityFileStream.ReadLine()) && !string.IsNullOrEmpty(line))
                    {
                        functionName = functionLine.Substring(4);
                        functionName = functionName.Substring(0, functionName.IndexOf(" "));
                        functionline = int.Parse(functionLine.Substring(functionLine.LastIndexOf(" ")));
                        foreach (Store.IFunction Func in storage.functions.GetFunction(functionName.Replace("::", ".").Split('.')))
                        {
                            Location funcDefenition = Func.Definition();
                            if (funcDefenition != null && funcDefenition.GetFile().FullFileName_Original.ToLower() == file.ToLower() && funcDefenition.GetLine() == (ulong)functionline)
                            {
                                Func.EssentialUnderstand = enFunctionEssentialsUnderstand.REDUNDANT;
                                break;
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        static readonly List<string> ignoredMacros = new List<string>();

        /// <summary>
        /// ��������� �������
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.StartsWith(System.String)"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.IndexOf(System.String)")]
        public static void RunIgnoreMacroLines()
        {
            Reader ignoreMacroFileStream = null;

            try
            {
                string macrosPath = Path.Combine(PluginSettings.ReportsDirectoryPath, "Macros.txt");
                ignoreMacroFileStream = new Reader(macrosPath, 2);
                string line;
                while ((line = ignoreMacroFileStream.ReadLine()) != null)
                {
                    if (line.StartsWith("    Use ["))
                    {
                        string fullPath = line.Substring(9);
                        fullPath = fullPath.Substring(0, fullPath.IndexOf("]"));
                        ignoredMacros.Add(fullPath.Replace("::", "."));
                    }
                }
            }
            finally
            {
                if (ignoreMacroFileStream != null) ignoreMacroFileStream.Close();
            }
        }

        /// <summary>
        /// ������� �����
        /// </summary>
        /// <param name="line">������ � ��������� ������</param>
        private static void GenClass(string line)
        {
            Class cl = storage.classes.AddClass();

            int idx2 = line.LastIndexOf('(') - 1;
            while (line[idx2] == ' ')
                idx2--;
            string Name = line.Substring(0, idx2 + 1).Replace("::", "."), nspace = null;

            if (Name.Contains("."))
            {
                nspace = Name.Substring(0, Name.LastIndexOf('.'));
                Name = Name.Substring(Name.LastIndexOf('.') + 1);
            }

            Namespace Nspace = null;
            if (!string.IsNullOrEmpty(nspace))
            {
                Nspace = storage.namespaces.FindOrCreate(nspace.Split('.'));
            }

            cl.Name = Name;


            if (Nspace != null)
            {
                Nspace.AddClassToNamespace(cl);
            }
        }

        /// <summary>
        /// ������� �������
        /// </summary>
        /// <param name="line">������ � ���������</param>
        /// <param name="line2">������ � ��������� �����</param>
        /// <param name="functions">������� � ���������</param>
        private static void GenFunction(string line, string line2, Functions functions)
        {
            Function func;

            int idx2 = line.LastIndexOf('(') - 1;
            while (line[idx2] == ' ')
                idx2--;
            string Name = line.Substring(0, idx2 + 1).Replace("::", "."), nspace = null;

            if (Name.Contains("."))
            {
                nspace = Name.Substring(0, Name.LastIndexOf('.'));
                Name = Name.Substring(Name.LastIndexOf('.') + 1);
            }

            if (nspace != null && PluginSettings.IsJava && !String.IsNullOrWhiteSpace(line2))
            {
                int line2pos1 = line2.LastIndexOf(',');
                int line2pos2 = line2.LastIndexOf(']');
                if (line2pos1 != -1 && line2pos2 != -1)
                {
                    int numLine = int.Parse(line2.Substring(line2pos1 + 1, line2pos2 - line2pos1 - 1));

                    //������� ��� ���� 197 - �������, ����������� � ��� �� ������, ��� � ����� ����� ������� ������������
                    if (enums.Any(x => x.name == nspace && x.line == numLine))
                         return;                        
                    //if (enums.ContainsKey(nspace) && enums[nspace].line == numLine)
                    //    return;
                }
            }
            Namespace Nspace = null;
            Class Cl = null;


            if (!string.IsNullOrEmpty(nspace))
            {
                if (storage.classes.GetClass(nspace.Split('.')) == null || storage.classes.GetClass(nspace.Split('.')).Length == 0 || (Cl = storage.classes.GetClass(nspace.Split('.'))[0]) == null)
                {
                    Nspace = storage.namespaces.FindOrCreate(nspace.Split('.'));
                }
            }

            if (Cl != null)
            {
                func = new Function();
                func.function = Cl.AddMethod(false);
            }
            else
            {
                func = new Function(functions);
            }

            func.Name = Name;

            if (!string.IsNullOrEmpty(line2))
            {
                const int idx = 4;
                int idx1 = line2.LastIndexOf(',');
                if (idx1 != -1) //���� ���� ����� �����������
                {
                    string fileName = line2.Substring(idx, idx1 - idx);

                    if (!string.IsNullOrEmpty(PluginSettings.SourcesDirectoryPath))
                    {
                        fileName = fileName.ToLowerInvariant().Replace(DirectoryController.CanonizePath(PluginSettings.SourcesDirectoryPath), DirectoryController.CanonizePath(storage.appliedSettings.FileListPath));
                    }
                    string lineNum = line2.Substring(idx1 + 1, line2.LastIndexOf(']') - idx1 - 1);
                    if (!PluginSettings.IsIgnoreMacroLines || !ignoredMacros.Contains(fileName + ", " + lineNum))
                    {
                        func.Definition(fileName, Convert.ToUInt64(lineNum));
                    }
                }
            }
            if (Nspace != null)
            {
                Nspace.AddFunctionToNamespace(func.function);
            }
        }

        /// <summary>
        /// ������� ����������
        /// </summary>
        /// <param name="Name">���</param>
        /// <param name="isStatic">����������� ��</param>
        /// <returns></returns>
        private static Store.Variable GenVar(string Name, bool isStatic)
        {
            Store.Variable vrbl = null;

            string nspace = null;

            if (Name.Contains("."))
            {
                nspace = Name.Substring(0, Name.LastIndexOf('.'));
                Name = Name.Substring(Name.LastIndexOf('.') + 1);
            }

            Namespace Nspace = null;
            Class Cl = null;


            if (!string.IsNullOrEmpty(nspace))
            {
                if (storage.classes.GetClass(nspace.Split('.')) == null || storage.classes.GetClass(nspace.Split('.')).Length == 0 || (Cl = storage.classes.GetClass(nspace.Split('.'))[0]) == null)
                {
                    Nspace = storage.namespaces.FindOrCreate(nspace.Split('.'));
                }
            }

            if (Cl != null)
            {
                vrbl = Cl.AddField(isStatic);

            }
            else
                vrbl = storage.variables.AddVariable();

            vrbl.Name = Name;


            if (Nspace != null)
            {
                Nspace.AddVariableToNamespace(vrbl);
            }

            return vrbl;
        }
    }
}
