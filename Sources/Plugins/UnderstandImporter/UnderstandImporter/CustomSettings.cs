using System;
using System.Windows.Forms;

namespace IA.Plugins.Importers.UnderstandImporter
{
    /// <summary>
    /// �������� ����� �������
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// �����������
        /// </summary>
        internal Settings()
        {
            InitializeComponent();

            reportsDirectoryPath_textBox.Text = PluginSettings.ReportsDirectoryPath;
            sourcesPath_textBox.Text = PluginSettings.SourcesDirectoryPath;

            isCPP_checkBox.Checked = PluginSettings.IsCPP;
            isPascal_checkBox.Checked = PluginSettings.IsPascal;
            isWeb_checkBox.Checked = PluginSettings.IsWeb;
            isJava_checkBox.Checked = PluginSettings.IsJava;
            isCSharp_checkbox.Checked = PluginSettings.IsCS;

            if (string.IsNullOrEmpty(PluginSettings.UnderstandPath))
            {
                // ������������� �������� PluginSettings.UnderstandPath.set, � ������� ����� � ������
                PluginSettings.UnderstandPath = "";
            }
            understandPath_textBox.Text = PluginSettings.UnderstandPath;

            understandPath_groupBox.Visible = languages_groupBox.Visible = isRunUnderstand_checkBox.Checked;
            (understandPath_textBox.Dialog as OpenFileDialog).Filter = "���� ����������� ���������� Understand | und.exe"; 
            if (isRunUnderstand_checkBox.Checked)
            {
                settings_tableLayoutPanel.RowStyles[4].Height = 175;
                settings_tableLayoutPanel.RowStyles[5].Height = 55;
            }
            else
            {
                settings_tableLayoutPanel.RowStyles[4].Height = 0;
                settings_tableLayoutPanel.RowStyles[5].Height = 0;
            }

            sourcesDirectoryPath_groupBox.Visible = !isRunUnderstand_checkBox.Checked;
            
            isRunUnderstand_checkBox.Checked = PluginSettings.IsRunUnderstand;
            isIgnoreMacroPage_checkBox.Checked = PluginSettings.IsIgnoreMacroLines;
            isLoadVariables_checkBox.Checked = PluginSettings.IsLoadVariables;
        }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        public void Save()
        {
            PluginSettings.ReportsDirectoryPath = reportsDirectoryPath_textBox.Text;
            PluginSettings.IsRunUnderstand = isRunUnderstand_checkBox.Checked;
            PluginSettings.IsIgnoreMacroLines = isIgnoreMacroPage_checkBox.Checked;
            PluginSettings.IsLoadVariables = isLoadVariables_checkBox.Checked;
            PluginSettings.UnderstandPath = understandPath_textBox.Text;
            PluginSettings.SourcesDirectoryPath = sourcesPath_textBox.Text;
            PluginSettings.IsCPP = isCPP_checkBox.Checked;
            PluginSettings.IsPascal = isPascal_checkBox.Checked;
            PluginSettings.IsWeb = isWeb_checkBox.Checked;
            PluginSettings.IsJava = isJava_checkBox.Checked;
            PluginSettings.IsCS = isCSharp_checkbox.Checked;
        }

        private void isRunUnderstand_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (isRunUnderstand_checkBox.Checked)
            {
                settings_tableLayoutPanel.RowStyles[4].Height = 175;
                settings_tableLayoutPanel.RowStyles[5].Height = 55;
            }
            else
            {
                settings_tableLayoutPanel.RowStyles[4].Height = 0;
                settings_tableLayoutPanel.RowStyles[5].Height = 0;
            }

            sourcesDirectoryPath_groupBox.Visible = !isRunUnderstand_checkBox.Checked;
            understandPath_groupBox.Visible = languages_groupBox.Visible = isRunUnderstand_checkBox.Checked;            
        }

        ///// <summary>
        ///// �������� ���� � ������������ ����� Understand. 
        ///// ���� ����, ���������� � �������� ���������, ����������, �� ������ ������ �� ����. 
        ///// ����� - ���������� ����� �� ����� � �������.
        ///// </summary>
        ///// <param name="understandPath">���� � ������������ ����� Understand.</param>
        ///// <returns>���� � ������������ ����� Understand.</returns>
        //private string RepairUnderstandPath(string understandPath)
        //{
        //    if (File.Exists(understandPath))
        //        return understandPath;

        //    string ret = string.Empty;

        //    try
        //    {
        //        using (RegistryKey currRegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\SciTools"))
        //        {
        //            foreach (string version in currRegistryKey.GetSubKeyNames())
        //            {
        //                try
        //                {
        //                    string key = (string)currRegistryKey.OpenSubKey(version).GetValue("");

        //                    if (key != null)
        //                        ret = File.Exists(key + @"\bin\pc-win32\und.exe") ? key + @"\bin\pc-win32\und.exe" : key + @"\bin\pc-win64\und.exe";
        //                    else
        //                        Monitor.Log.Error(PluginSettings.Name, "�������� ���������� � ������� � ����� <" + key + ">.");
        //                }
        //                catch
        //                {
        //                    Monitor.Log.Error(PluginSettings.Name, "������ ��� �������� ����� ������� <" + currRegistryKey.Name + ">.");
        //                }
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        try
        //        {
        //            using (RegistryKey currRegistryKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\SciTools\"))
        //            {
        //                foreach (string version in currRegistryKey.GetSubKeyNames())
        //                {
        //                    try
        //                    {
        //                        string key = (string)currRegistryKey.OpenSubKey(version).GetValue("");

        //                        if (key != null)
        //                            ret = File.Exists(key + @"\bin\pc-win32\und.exe") ? key + @"\bin\pc-win32\und.exe" : key + @"\bin\pc-win64\und.exe";
        //                        else
        //                            Monitor.Log.Error(PluginSettings.Name, "�������� ���������� � ������� � ����� <" + key + ">.");
        //                    }
        //                    catch 
        //                    {
        //                        Monitor.Log.Error(PluginSettings.Name, "������ ��� �������� ����� ������� <" + currRegistryKey.Name + ">.");
        //                    }
        //                }
        //            }
        //        }
        //        catch
        //        {
        //            Monitor.Log.Error(PluginSettings.Name, "�� ��� �� ���������� Understand.");
        //        }
        //    }

        //    return ret;
        //}
    }
}
