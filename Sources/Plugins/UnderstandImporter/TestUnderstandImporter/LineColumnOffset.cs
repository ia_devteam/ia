﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using TestUtils;

namespace TestUnderstandImporter
{
    [TestClass]
    public class TestLineColumnOffset
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Проверка правильности смещения
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_LineColumnOffset()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                 string.Empty,

                 // plugin 
                 new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(),

                 // isUseCachedStorage
                 false,

                 // prepareStorage                                         
                 (storage, testMaterialsPath) => { },

                 // customizeStorage
                 (storage, testMaterialsPath) =>
                 {
                     //выбор настроек
                     Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                     writer.Add(Path.Combine(testMaterialsPath, @"UnderstandImporter\LineColOffset\Understand\")); // путь к протоколам
                     writer.Add(@"@#$%^&*");  //перфикс путей в отчете
                     writer.Add(false); // запустить ли Understand 
                     writer.Add(false); // игнорировать макросы
                     writer.Add(false); //загрузить отчет по переменным
                     writer.Add("");
                     writer.Add(true); //C/C++
                     writer.Add(true); //C#
                     writer.Add(true); //Pascal
                     writer.Add(true); //Web
                     writer.Add(true); //Java
                     storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER, writer);

                     TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\LineColOffset\Source"));
                 },

                 // checkStorage
                 // В отдельный файл помещаем для каждой из добавленных в хранилище функций место определения и обращений, затем сверяем файл с эталонным
                 (storage, testMaterialsPath) =>
                 {
                     ulong i = storage.functions.GetFunction(3).Definition().GetOffset();
                     Assert.IsTrue(i == 94);
                     return true;
                 },

                 // checkReportBeforeRerun                       
                 (reportsPath, testMaterialsPath) =>
                 {
                     return true;
                 },

                 // isUpdateReport
                 false,

                 // changeSettingsBeforeRerun                     
                 (plugin, testMaterialsPath) => { },

                 // checkStorageAfterRerun
                 (storage, testMaterialsPath) => { return true; },

                 // checkReportAfterRerun
                 (reportsPath, testMaterialsPath) => { return true; }
                 );
        }
    }
}
