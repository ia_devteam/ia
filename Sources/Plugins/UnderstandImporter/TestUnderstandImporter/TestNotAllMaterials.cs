﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestUtils;
using System.IO;
using System.Collections.Generic;
using IOController;

namespace TestUnderstandImporter
{
    [TestClass]
    public class TestNotAllMaterials
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Проверка поведения плагина при отсутствии отчёта Understand "ProgramUnits.txt"
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_NotAllMaterialsProgramUnits()
        {
            //Формируем список типов отлавливаемых сообщений
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>()
            {
                IA.Monitor.Log.Message.enType.ERROR
            };

            //Формируем список ожидаемых сообщений
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>()
            {
                new IA.Monitor.Log.Message(
                    "Импорт протоколов Understand",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Файла " + Path.Combine(testUtils.TestMatirialsDirectoryPath, @"UnderstandImporter\Incorrect\Understand2\ProgramUnits.txt") + " не существует."
                    )
            };

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                    string.Empty,

                    // plugin 
                    new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(),

                    // isUseCachedStorage
                    false,

                    // prepareStorage                                         
                    (storage, testMaterialsPath) => { },

                    // customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        //выбор настроек
                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                        writer.Add(System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\Incorrect\Understand2\")); // путь к протоколам
                        writer.Add("");  //перфикс путей в отчете
                        writer.Add(false); // запустить ли Understand 
                        writer.Add(false); // игнорировать макросы
                        writer.Add(false); //загрузить отчет по переменным
                        writer.Add("");
                        writer.Add(true); //C/C++
                        writer.Add(true); //C#
                        writer.Add(true); //Pascal
                        writer.Add(true); //Web
                        writer.Add(true); //Java
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER, writer);

                        TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                    },

                    // checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },

                    // checkReportBeforeRerun                       
                    (reportsPath, testMaterialsPath) =>
                    {
                        return true;
                    },

                    // isUpdateReport
                    false,

                    // changeSettingsBeforeRerun                     
                    (plugin, testMaterialsPath) => { },

                    // checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    // checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; },
                    
                    true);
            }
        }

        /// <summary>
        /// Проверка поведения плагина при отсутствии отчёта Understand "DataDictionary.txt"
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_NotAllMaterialsDataDictionary()
        {
            //Формируем список типов отлавливаемых сообщений
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>()
            {
                IA.Monitor.Log.Message.enType.ERROR
            };

            //Формируем список ожидаемых сообщений
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>()
            {
                new IA.Monitor.Log.Message(
                    "Импорт протоколов Understand",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Файла " + Path.Combine(testUtils.TestMatirialsDirectoryPath, @"UnderstandImporter\Incorrect\Understand1\DataDictionary.txt") + " не существует."
                    )
            };

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                         string.Empty,

                         // plugin 
                         new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(),

                         // isUseCachedStorage
                         false,

                         // prepareStorage                                         
                         (storage, testMaterialsPath) => { },

                         // customizeStorage
                         (storage, testMaterialsPath) =>
                         {
                             //выбор настроек
                             Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                             writer.Add(System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\Incorrect\Understand1\")); // путь к протоколам
                             writer.Add("");  //перфикс путей в отчете
                             writer.Add(false); // запустить ли Understand 
                             writer.Add(false); // игнорировать макросы
                             writer.Add(false); //загрузить отчет по переменным
                             writer.Add("");
                             writer.Add(true); //C/C++
                             writer.Add(true); //C#
                             writer.Add(true); //Pascal
                             writer.Add(true); //Web
                             writer.Add(true); //Java
                             storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER, writer);

                             TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                         },

                         // checkStorage
                         (storage, testMaterialsPath) =>
                         {
                             return true;
                         },

                         // checkReportBeforeRerun                       
                         (reportsPath, testMaterialsPath) =>
                         {
                             return true;
                         },

                         // isUpdateReport
                         false,

                         // changeSettingsBeforeRerun                     
                         (plugin, testMaterialsPath) => { },

                         // checkStorageAfterRerun
                         (storage, testMaterialsPath) => { return true; },

                          // checkReportAfterRerun
                         (reportsPath, testMaterialsPath) => { return true; }, 
                         true);
            }
        }
                
        /// <summary>
        /// Проверка поведения плагина при отсутствии отчёта Understand "Objects.txt"
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_NotAllMaterialsVariables()
        {
            //Формируем список типов отлавливаемых сообщений
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>()
            {
                IA.Monitor.Log.Message.enType.ERROR
            };

            //Формируем список ожидаемых сообщений
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>()
            {
                new IA.Monitor.Log.Message(
                    "Импорт протоколов Understand",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Файла " + Path.Combine(testUtils.TestMatirialsDirectoryPath, @"UnderstandImporter\Incorrect\Understand3\Objects.txt") + " не существует."
                    )
            };

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                         string.Empty,

                         // plugin 
                         new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(),

                         // isUseCachedStorage
                         false,

                         // prepareStorage                                         
                         (storage, testMaterialsPath) => { },

                         // customizeStorage
                         (storage, testMaterialsPath) =>
                         {
                             //выбор настроек
                             Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                             writer.Add(System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\Incorrect\Understand3\")); // путь к протоколам
                             writer.Add("");  //перфикс путей в отчете
                             writer.Add(false); // запустить ли Understand 
                             writer.Add(false); // игнорировать макросы
                             writer.Add(true); //загрузить отчет по переменным
                             writer.Add("");
                             writer.Add(true); //C/C++
                             writer.Add(true); //C#
                             writer.Add(true); //Pascal
                             writer.Add(true); //Web
                             writer.Add(true); //Java
                             storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER, writer);

                             TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                         },

                         // checkStorage
                         (storage, testMaterialsPath) =>
                         {
                             return true;
                         },

                         // checkReportBeforeRerun                       
                         (reportsPath, testMaterialsPath) =>
                         {
                             return true;
                         },

                         // isUpdateReport
                         false,

                         // changeSettingsBeforeRerun                     
                         (plugin, testMaterialsPath) => { },

                         // checkStorageAfterRerun
                         (storage, testMaterialsPath) => { return true; },

                          // checkReportAfterRerun
                         (reportsPath, testMaterialsPath) => { return true; },
                         true);
            }
        }

        /// <summary>
        /// Проверка поведения плагина при отсутствии отчёта Understand "Macros.txt"
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_NotAllMaterialsMacros()
        {
            //Формируем список типов отлавливаемых сообщений
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>()
            {
                IA.Monitor.Log.Message.enType.ERROR
            };

            //Формируем список ожидаемых сообщений
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>()
            {
                new IA.Monitor.Log.Message(
                    "Импорт протоколов Understand",
                    IA.Monitor.Log.Message.enType.ERROR,
                    "Файла " + Path.Combine(testUtils.TestMatirialsDirectoryPath, @"UnderstandImporter\Incorrect\Understand4\Macros.txt") + " не существует."
                    )
            };

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                    string.Empty,

                    // plugin 
                    new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(),

                    // isUseCachedStorage
                    false,

                    // prepareStorage                                         
                    (storage, testMaterialsPath) => { },

                    // customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        //выбор настроек
                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                        writer.Add(System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\Incorrect\Understand4\")); // путь к протоколам
                        writer.Add("");  //перфикс путей в отчете
                        writer.Add(false); // запустить ли Understand 
                        writer.Add(true); // игнорировать макросы
                        writer.Add(true); //загрузить отчет по переменным
                        writer.Add("");
                        writer.Add(true); //C/C++
                        writer.Add(true); //C#
                        writer.Add(true); //Pascal
                        writer.Add(true); //Web
                        writer.Add(true); //Java
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER, writer);

                        TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                    },

                    // checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },

                    // checkReportBeforeRerun                       
                    (reportsPath, testMaterialsPath) =>
                    {
                        return true;
                    },

                    // isUpdateReport
                    false,

                    // changeSettingsBeforeRerun                     
                    (plugin, testMaterialsPath) => { },

                    // checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    // checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; },
                    true);
            }
        }

        /// <summary>
        /// Проверка поведения плагина в случае присутствия некорретных префиксов путей до файлов в отчётах Understand
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_NotAllMaterialsIncorrectSource()
        {
            //Создаём Хранилище
            Store.Storage storage = testUtils.Storage_CreateAndOpen();

            //Запускаем Fill File List
            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testUtils.TestMatirialsDirectoryPath, @"UnderstandImporter\FunctionReferences\Source\"));

            //Создаём экземпляр плагина
            IA.Plugins.Importers.UnderstandImporter.UnderstandImporter plugin = new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter();

            //Подготавливаем буффер с настройками
            Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
            writer.Add(System.IO.Path.Combine(testUtils.TestMatirialsDirectoryPath, @"UnderstandImporter\Incorrect\Understand5\Understand")); // путь к протоколам
            writer.Add(@"@#$%^&*");  //перфикс путей в отчете
            writer.Add(false); // запустить ли Understand 
            writer.Add(true); // игнорировать макросы
            writer.Add(true); //загрузить отчет по переменным
            writer.Add("");
            writer.Add(true); //C/C++
            writer.Add(true); //C#
            writer.Add(true); //Pascal
            writer.Add(true); //Web
            writer.Add(true); //Java
            storage.pluginSettings.SaveSettings(plugin.ID, writer);

            //Инициализируем плагин
            plugin.Initialize(storage);

            //Задаём настройки
            plugin.LoadSettings(storage.pluginSettings.LoadSettings(plugin.ID));

            try
            {
                plugin.Run();
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message == @"От имени файла невозможно отрезать префикс: @#$%^&*\Plugin.cs");
            }               
            //Тестирование Хранилища закончилось. Закрываем его
            storage.Close();
            DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }
    }
}
