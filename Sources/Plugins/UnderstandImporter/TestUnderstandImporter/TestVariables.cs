﻿using System;
using System.IO;
using System.Reflection;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;
using Store;


namespace TestUnderstandImporterVariables
{
    [TestClass]
    public class UnderstandImporterTestVariables
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Проверка корректности импорта отчёта Understand по переменным.
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_UnderstandImporterCheckVariables()
        {
            testUtils.RunTest(
             // sourcePrefixPart
             string.Empty,

             // plugin 
             new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(),

             // isUseCachedStorage
             false,

             // prepareStorage                                         
             (storage, testMaterialsPath) => { },

             // customizeStorage
             (storage, testMaterialsPath) =>
             {
                 //выбор настроек
                 Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                 writer.Add(System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\")); // путь к протоколам
                 writer.Add("@#$%^&*");  //перфикс путей в отчете
                 writer.Add(false); // запустить ли Understand 
                 writer.Add(false); // игнорировать макросы
                 writer.Add(true); //загрузить отчет по переменным
                 writer.Add("");
                 writer.Add(true); //C/C++
                 writer.Add(true); //C#
                 writer.Add(true); //Pascal
                 writer.Add(true); //Web
                 writer.Add(true); //Java
                 storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER, writer);

                 TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
             },

             // checkStorage
             // В отдельный файл помещаем для каждой из добавленных в хранилище переменных место определения и обращений, затем сверяем файл с эталонным
             (storage, testMaterialsPath) =>
             {
                 int i = 0;
                 using (StreamWriter writer = new StreamWriter(System.IO.Path.Combine(testUtils.TestRunDirectoryPath, "VariableReferences.txt")))
                 {
                     foreach (Variable var in storage.variables.EnumerateVariables())
                     {
                         writer.WriteLine(var.FullName);
                         
                         foreach (Location loc in var.Definitions())
                             writer.WriteLine("\tDef: " + loc.GetFile().FileNameForReports + ", " + loc.GetOffset().ToString());

                         foreach (IAssignment ass in var.Enumerate_AssignmentsFrom())
                             writer.WriteLine("\tAssignment: " + ass.From.FullName + ", " + ass.Position.GetFile().FileNameForReports + ", " + ass.Position.GetOffset().ToString());
                         
                         foreach (PointsToFunction poi in var.Enumerate_PointsToFunctions())
                             writer.WriteLine("\tPoints: " + storage.functions.GetFunction(poi.function).FullNameForReports + ", " + poi.position.GetFile().FileNameForReports + ", " + poi.position.GetOffset().ToString());
                         
                         foreach (Location loc in var.ValueGetPosition())
                             writer.WriteLine("\tGet: " + loc.GetFile().FileNameForReports + ", " + loc.GetOffset().ToString());
                         
                         foreach (Location loc in var.ValueCallPosition())
                             writer.WriteLine("\tCall: " + loc.GetFile().FileNameForReports + ", " + loc.GetOffset().ToString());
                         
                         foreach (Location loc in var.ValueSetPosition())
                             writer.WriteLine("\tSet: " + loc.GetFile().FileNameForReports + ", " + loc.GetOffset().ToString());
                         
                         if (var.ImplementedInModule != null)
                             writer.WriteLine("\tModule " + var.ImplementedInModule.Name);

                         ++i;
                     }
                 }

                 Assert.IsTrue(i == 162);

                 return TestUtilsClass.Reports_TXT_Compare(Path.Combine(testUtils.TestRunDirectoryPath, "VariableReferences.txt"), Path.Combine(testMaterialsPath, @"UnderstandImporter\Variables\VariableReferences.txt"));
             },

             // checkReportBeforeRerun                       
             (reportsPath, testMaterialsPath) =>
             {
                 return true;
             },

             // isUpdateReport
             false,

             // changeSettingsBeforeRerun                     
             (plugin, testMaterialsPath) => { },

             // checkStorageAfterRerun
             (storage, testMaterialsPath) => { return true; },

              // checkReportAfterRerun
             (reportsPath, testMaterialsPath) => { return true; }
             );
        }
    }
}
