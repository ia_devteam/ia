﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestUtils;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace TestUnderstandImporterAutomaticGenetation
{
    [TestClass]
    public class TestAutomaticGenetationUnderstand
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Проверка автоматической генерации отчётов Understand
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_AutomaticGenetationUnderstand()
        {
            testUtils.RunTest(
             // sourcePrefixPart
             string.Empty,

             // plugin 
             new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(),

             // isUseCachedStorage
             false,

             // prepareStorage                                         
             (storage, testMaterialsPath) => { },

             // customizeStorage
             (storage, testMaterialsPath) =>
             {
                 //выбор настроек
                 Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                 writer.Add(""); // путь к протоколам
                 writer.Add("");  //перфикс путей в отчете
                 writer.Add(true); // запустить ли Understand 
                 writer.Add(false); // игнорировать макросы
                 writer.Add(false); //загрузить отчет по переменным
                 writer.Add("");
                 writer.Add(true); //C/C++
                 writer.Add(true); //C#
                 writer.Add(true); //Pascal
                 writer.Add(true); //Web
                 writer.Add(true); //Java
                 storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER, writer);

                 TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\Understand\Source\"));
             },

             // checkStorage
             // сверяем результат генерации протоколов Understand c эталонным
             (storage, testMaterialsPath) =>
             {
                 //Получаем путь к директории с данными плагина
                 string pluginDataFolderPath = storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER);

                 foreach (string reportFilePath in Directory.EnumerateFiles(Path.Combine(pluginDataFolderPath, "Understand")))
                 {
                     string contents;

                     //Считываем содержимое файла в нижнем регистре
                     using (StreamReader reader = new StreamReader(reportFilePath))
                         contents = reader.ReadToEnd().ToLower();

                     //Заменяем префиксы путей в файле
                     contents = contents.Replace(Path.Combine(testMaterialsPath, @"UnderstandImporter\Understand\Source").ToLower(),
                                      @"b:\IA_b\Tests\Materials\UnderstandImporter\Understand\Source").ToLower();

                     string testFileName = reportFilePath.Replace(Path.Combine(pluginDataFolderPath, "Understand"), testUtils.TestRunDirectoryPath) + "1";
                     
                     using (StreamWriter writer = new StreamWriter(testFileName))
                        writer.Write(contents);

                     if (!TestUtilsClass.Reports_TXT_Compare(reportFilePath.Replace(pluginDataFolderPath, Path.Combine(testMaterialsPath, @"UnderstandImporter\Understand")), testFileName, true, true))
                         return false;
                 }
                 return true;
             },

             // checkReportBeforeRerun                       
             (reportsPath, testMaterialsPath) =>
             {
                 return true;
             },

             // isUpdateReport
             false,

             // changeSettingsBeforeRerun                     
             (plugin, testMaterialsPath) => { },

             // checkStorageAfterRerun
             (storage, testMaterialsPath) => { return true; },

              // checkReportAfterRerun
             (reportsPath, testMaterialsPath) => { return true; }
             );

        }
    }
}
