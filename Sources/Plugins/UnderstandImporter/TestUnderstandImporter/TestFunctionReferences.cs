﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Reflection;
using TestUtils;
using Store;
using System.Collections.Generic;

namespace TestUnderstandImporterFunctions
{
    [TestClass]
    public class TestFunctionReferences
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }
        
        /// <summary>
        /// Проверка корректности импорта отчёта Understand по функциям.
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_FunctionReferenceTestUnderstandImporter()
        {
            testUtils.RunTest(
             // sourcePrefixPart
             string.Empty,

             // plugin 
             new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(),

             // isUseCachedStorage
             false,

             // prepareStorage                                         
             (storage, testMaterialsPath) => { },

             // customizeStorage
             (storage, testMaterialsPath) =>
             {
                 //выбор настроек
                 Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                 writer.Add(System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\")); // путь к протоколам
                 writer.Add("@#$%^&*");  //перфикс путей в отчете
                 writer.Add(false); // запустить ли Understand 
                 writer.Add(false); // игнорировать макросы
                 writer.Add(false); //загрузить отчет по переменным
                 writer.Add("");
                 writer.Add(true); //C/C++
                 writer.Add(true); //C#
                 writer.Add(true); //Pascal
                 writer.Add(true); //Web
                 writer.Add(true); //Java
                 storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER, writer);

                 TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
             },

             // checkStorage
             // В отдельный файл помещаем для каждой из добавленных в хранилище функций место определения и обращений, затем сверяем файл с эталонным
             (storage, testMaterialsPath) =>
             {
                 Assert.IsTrue(storage.functions.Count == 269);

                 using (StreamWriter writer = new StreamWriter(System.IO.Path.Combine(testUtils.TestRunDirectoryPath, "FunctionReferences.txt")))
                 {
                     foreach (IFunction function in storage.functions.EnumerateFunctions(enFunctionKind.ALL))
                     {
                         writer.WriteLine(function.NameForReports);

                         Location functionDefinition = function.Definition();
                         if (functionDefinition != null)
                             writer.WriteLine(functionDefinition.GetFile() + " " + functionDefinition.GetColumn() + " " + functionDefinition.GetLine() + " " + functionDefinition.GetOffset());

                         foreach (IFunctionCall called in function.Calles())
                             writer.WriteLine("\t" + called.Calles.NameForReports + "[" + called.CallLocation.GetFile().FileNameForReports + ", " + called.CallLocation.GetOffset() + "]");
                     }
                 }

                 if (!TestUtilsClass.Reports_TXT_Compare(System.IO.Path.Combine(testUtils.TestRunDirectoryPath, "FunctionReferences.txt"), 
                                                         System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\FunctionReferences.txt")))
                     return false;

                 return true;
             },

             // checkReportBeforeRerun                       
             (reportsPath, testMaterialsPath) =>
             {
                 return true;
             },

             // isUpdateReport
             false,

             // changeSettingsBeforeRerun                     
             (plugin, testMaterialsPath) => { },

             // checkStorageAfterRerun
             (storage, testMaterialsPath) => { return true; },

             // checkReportAfterRerun
             (reportsPath, testMaterialsPath) => { return true; }
             );
        }

        /// <summary>
        /// Проверка корректности импорта отчёта Understand по избыточности функций.
        /// </summary>
        [TestMethod]
        public void UnderstandImporter_UnusedFunction()
        {
            testUtils.RunTest(
                // sourcePrefixPart
             string.Empty,

             // plugin 
             new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(),

             // isUseCachedStorage
             false,

             // prepareStorage                                         
             (storage, testMaterialsPath) => { },

             // customizeStorage
             (storage, testMaterialsPath) =>
             {
                 //выбор настроек
                 Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                 writer.Add(System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\")); // путь к протоколам
                 writer.Add("@#$%^&*");  //перфикс путей в отчете
                 writer.Add(false); // запустить ли Understand 
                 writer.Add(false); // игнорировать макросы
                 writer.Add(false); //загрузить отчет по переменным
                 writer.Add("");
                 writer.Add(true); //C/C++
                 writer.Add(true); //C#
                 writer.Add(true); //Pascal
                 writer.Add(true); //Web
                 writer.Add(true); //Java
                 storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.UNDERSTAND_IMPORTER, writer);

                 TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
             },

             // checkStorage
             // В отдельный файл помещаем для каждой из добавленных в хранилище функций место определения и обращений, затем сверяем файл с эталонным
             (storage, testMaterialsPath) =>
             {
                 Assert.IsTrue(storage.functions.Count == 269);

                 using (StreamWriter writer = new StreamWriter(System.IO.Path.Combine(testUtils.TestRunDirectoryPath, "FunctionUnused.txt")))
                 {
                     foreach (IFunction function in storage.functions.EnumerateFunctions(enFunctionKind.ALL))
                     {
                         writer.WriteLine(function.NameForReports);
                         writer.WriteLine(function.EssentialUnderstand.ToString());
                     }
                 }

                 if (!TestUtilsClass.Reports_TXT_Compare(System.IO.Path.Combine(testUtils.TestRunDirectoryPath, "FunctionUnused.txt"),
                                                         System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionUnused.txt")))
                     return false;

                 return true;
             },

             // checkReportBeforeRerun                       
             (reportsPath, testMaterialsPath) =>
             {
                 return true;
             },

             // isUpdateReport
             false,

             // changeSettingsBeforeRerun                     
             (plugin, testMaterialsPath) => { },

             // checkStorageAfterRerun
             (storage, testMaterialsPath) => { return true; },

             // checkReportAfterRerun
             (reportsPath, testMaterialsPath) => { return true; }
             );
        }
        
    }
}
