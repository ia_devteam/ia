using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Linq;

using Store;
using Store.Table;

using IA.Extensions;
using IOController;

namespace IA.Plugins.Analyses.FileSaver
{
    /// <summary>
    /// Класс настроек плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.FILE_SAVER;

        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Сохранение очищенных исходных текстов";

        /// <summary>
        /// Наименования задач плагина при выполнении
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка файлов")]
            FILES_PROCESSING
        }

        /// <summary>
        /// Наименования задач плагина при генерации отчётов
        /// </summary>
        internal enum TasksReport
        {
            [Description("Формирование отчёта")]
            REPORT_GENERATION
        }
    }

    /// <summary>
    /// Основной класс плагина
    /// </summary>
    public class FileSaver : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        private Storage storage;

        #region PluginInterface Members
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.REPORTS | Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {

        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>
                { 
                    new Monitor.Tasks.Task(PluginSettings.Name,PluginSettings.Tasks.FILES_PROCESSING.Description()) 
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return new List<Monitor.Tasks.Task>
                { 
                    new Monitor.Tasks.Task(PluginSettings.Name,PluginSettings.TasksReport.REPORT_GENERATION.Description(), 1) 
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            IFile[] filesToProcess = storage.files.EnumerateFiles(enFileKind.fileWithPrefix).ToArray();

            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)filesToProcess.Length);

            //Получаем путь до директории с актуальными исходными текстами из Хранилища (могут быть как original, так и clear в зависимости от момента запуска плагина)
            string originalSourcesDirectoryPath = storage.appliedSettings.FileListPath;

            string clearSourcesDirectoryPath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_CLEAR);

            if (!PathController.IsPathsEqualAsInvariants(originalSourcesDirectoryPath, clearSourcesDirectoryPath))
            {
                DirectoryController.Clear(clearSourcesDirectoryPath);

                Clone(originalSourcesDirectoryPath, clearSourcesDirectoryPath);

                ulong counter = 0;
                foreach (IFile file in filesToProcess)
                {
                    if (file.FileEssential == enFileEssential.ESSENTIAL)
                    {
                        string essentialFileCopyPath =
                            PathController.ChangePrefix(
                                file.FullFileName_Original,
                                originalSourcesDirectoryPath,
                                clearSourcesDirectoryPath
                            );

                        FileController.Copy(file.FullFileName_Original, essentialFileCopyPath);
                    }

                    //Обновляем прогресс
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                }
            }
            //Очистка производится в ту же директорию
            else
            {
                //Обрабатываем существенный файлы
                ulong counter = 0;
                foreach (IFile file in filesToProcess)
                {
                    if (file.FileEssential != enFileEssential.ESSENTIAL)
                        FileController.Remove(file.FullFileName_Original, true);

                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                }
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            if (storage == null)
                throw new Exception("Хранилище не определено.");

            (new ReportsGenerator(storage, reportsDirectoryPath)).Run();

            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.REPORT_GENERATION.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 1);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }
        #endregion

        /// <summary>
        /// Клонирование директории. В процессе клонирования копируется только дерево каталогов.
        /// </summary>
        /// <param name="fromDirectoryPath">Директория для клонирования</param>
        /// <param name="toDirectoryPath">Директория куда неоходимо произвести клонирование</param>
        /// <returns></returns>
        private void Clone(string fromDirectoryPath, string toDirectoryPath)
        {
            if (!DirectoryController.IsExists(fromDirectoryPath))
                throw new Exception("Директория для клонирования не существует.");

            if (!DirectoryController.IsExists(toDirectoryPath))
                DirectoryController.Create(toDirectoryPath);

            foreach (string fromSubDirectoryPath in DirectoryController.GetSubDirectories(fromDirectoryPath))
            {
                string toSubDirectoryPath = PathController.ChangePrefix(fromSubDirectoryPath, fromDirectoryPath, toDirectoryPath);

                Clone(fromSubDirectoryPath, toSubDirectoryPath);
            }
        }
    }
}
