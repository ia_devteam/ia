﻿using System;
using System.IO;
using System.Linq;

using Store;

namespace IA.Plugins.Analyses.FileSaver
{
    /// <summary>
    /// Класс, реализующий логику работы плагина при генерации отчётов
    /// </summary>
    internal class ReportsGenerator
    {
        private const string ReportFilePath = "Избыточные файлы.log";

        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Директория для формирования отчётов
        /// </summary>
        string reportsDirectoryPath;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="reportsDirectoryPath">Путь до директории для формирования отчётов</param>
        internal ReportsGenerator(Storage storage, string reportsDirectoryPath)
        {
            if (storage == null)
                throw new Exception("Хранилище не определено.");

            if (reportsDirectoryPath == null)
                throw new Exception("Директория для формирования отчётов не определена.");

            if (!Directory.Exists(reportsDirectoryPath))
                throw new Exception("Директория для формирования отчётов не существует.");

            this.storage = storage;
            this.reportsDirectoryPath = reportsDirectoryPath;
        }

        /// <summary>
        /// Запуск генерации отчёта
        /// </summary>
        internal void Run()
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(this.reportsDirectoryPath, ReportFilePath)))
                foreach (string filePath in this.storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Where(f => f.FileEssential != enFileEssential.ESSENTIAL).Select(f => f.FileNameForReports))
                    writer.WriteLine(filePath);
        }
    }
}
