﻿using System;
using System.IO;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;

using IOController;

namespace TestFileSaver
{
    [TestClass]
    public class TestFileSaver
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// В Хранилище нет файлов.
        /// Выходная директория пуста.
        /// </summary>
        [TestMethod]
        public void FileSaver_NoFiles_OutputDirectoryIsEmpty()
        {
            const string sourcePostfixPart = @"FileSaver\EmptyFolder";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new IA.Plugins.Analyses.FileSaver.FileSaver(),                  // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) => 
                {
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, @"FileSaver\EmptyFolder\")))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, @"FileSaver\EmptyFolder\"));
                },                            // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //Получаем путь до поддиректории рабочей директории Хранилища с очищенными исходными текстами
                    string clearWorkSubDirectoryPath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR);

                    //Проверяем что поддиректория пустая
                    if (!DirectoryController.IsEmpty(clearWorkSubDirectoryPath))
                        Assert.Fail("Директория с очищенными исходными текстами не пуста");

                    return true;
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                null,                             // changeSettingsBeforeRerun
                null,               // checkStorageAfterRerun
                null            // checkReportAfterRerun
                );
        }

        /// <summary>
        /// В Хранилище нет файлов.
        /// Выходная директория не пуста.
        /// </summary>
        [TestMethod]
        public void FileSaver_NoFiles_OutputDirectoryIsNotEmpty()
        {
            const string sourcePostfixPart = @"FileSaver\EmptyFolder";
            const string tempFileName = "tmp.txt";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new IA.Plugins.Analyses.FileSaver.FileSaver(),                  // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) => 
                {
                    //Получаем путь до поддиректории рабочей директории Хранилища с очищенными исходными текстами
                    string clearWorkSubDirectoryPath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR);

                    //Получать путь до временного файла
                    string tempFilePath = Path.Combine(clearWorkSubDirectoryPath, tempFileName);

                    //Формируем временный файл
                    File.WriteAllText(tempFilePath, String.Empty);
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, @"FileSaver\EmptyFolder\")))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, @"FileSaver\EmptyFolder\"));
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //Получаем путь до поддиректории рабочей директории Хранилища с очищенными исходными текстами
                    string clearWorkSubDirectoryPath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR);

                    //Проверяем что поддиректория пустая
                    if (!DirectoryController.IsEmpty(clearWorkSubDirectoryPath))
                        Assert.Fail("Директория с очищенными исходными текстами не пуста");

                    return true;
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
                );
        }

        /// <summary>
        /// В Хранилище нет файлов.
        /// Входная директория содержит только пустые подкаталоги.
        /// </summary>
        [TestMethod]
        public void FileSaver_NoFiles_InputDirectoryHasSubDirectoriesOnly()
        {
            const string sourcePostfixPart = @"FileSaver\SubDirectoriesOnly";

            //Набор имён поддиректорий
            string[] subDirectoryNames = new string[]
            {
                "Directory1",
                "Directory2",
                "Directory3"
            };

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new IA.Plugins.Analyses.FileSaver.FileSaver(),                  // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) => 
                {
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, @"FileSaver\SubDirectoriesOnly\")))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, @"FileSaver\SubDirectoriesOnly\"));
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, @"FileSaver\SubDirectoriesOnly\Directory1")))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, @"FileSaver\SubDirectoriesOnly\Directory1"));
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, @"FileSaver\SubDirectoriesOnly\Directory2")))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, @"FileSaver\SubDirectoriesOnly\Directory2"));
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, @"FileSaver\SubDirectoriesOnly\Directory3")))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, @"FileSaver\SubDirectoriesOnly\Directory3"));

                },                            // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //Получаем путь до поддиректории рабочей директории Хранилища с очищенными исходными текстами
                    string clearWorkSubDirectoryPath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR);

                    foreach(string subDirectoryName in subDirectoryNames)
                    {
                        //Получаем путь до поддиректории
                        string subDirectoryPath = Path.Combine(clearWorkSubDirectoryPath, subDirectoryName);

                        //Проверяем существование поддиректории
                        if (!DirectoryController.IsExists(subDirectoryPath))
                            Assert.Fail("Дерево подкаталогов было клонировано неверно.");
                    }

                    return true;
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Главный тест на логику плагина. Исходная директория и директория с очищенными исходными текстами не совпадают.
        /// Хранилище содержит все типы фалов (в рамках избыточности)
        /// </summary>
        [TestMethod]
        public void FileSaver_Main_InputNotEqualsOutput()
        {
            const string sourcePostfixPart = @"Sources\IdentifyFileTypes";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new IA.Plugins.Analyses.FileSaver.FileSaver(),                  // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) => 
                {
                    //FillFileList
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));

                    //Размечаем файлы произвольно так, чтобы в Хранилище гарантированно был хотя бы один файл одного типа (в рамках избыточности)
                    this.RandomMarkEssential(storage);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //Проверка что все существенные файлы были скопированы, а несущественные нет.
                    this.CheckEssential(storage);

                    return true;
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Главный тест на логику плагина. Исходная директория и директория с очищенными исходными текстами совпадают.
        /// Хранилище содержит все типы фалов (в рамках избыточности)
        /// </summary>
        [TestMethod]
        public void FileSaver_Main_InputEqualsOutput()
        {
            const string sourcePostfixPart = @"Sources\IdentifyFileTypes";

            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new IA.Plugins.Analyses.FileSaver.FileSaver(),                  // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    //Получаем путь до поддиректории рабочей директории Хранилища с очищенными исходными текстами
                    string clearWorkSubDirectoryPath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR);

                    //Копируем содержимое уже в поддиректорию рабочей директории Хранилища с очищенными исходными текстами
                    DirectoryController.Copy(
                        Path.Combine(testMaterialsPath, sourcePostfixPart),
                        clearWorkSubDirectoryPath
                    );

                    //Подменяем префикс в Хранилище
                    storage.appliedSettings.SetFileListPath(Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR);

                    //FillFileList
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));

                    //Размечаем файлы произвольно так, чтобы в Хранилище гарантированно был хотя бы один файл одного типа (в рамках избыточности)
                    this.RandomMarkEssential(storage);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //Проверка что все существенные файлы были скопированы, а несущественные нет.
                    this.CheckEssential(storage);

                    return true;
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Произвольно разметить файлы в Хранилище (в рамках избыточности).
        /// После выполнения метода в Хранилище будет хотя бы один файл одного типа (в рамках избыточности).
        /// </summary>
        /// <param name="storage">Хранилище</param>
        private void RandomMarkEssential(Store.Storage storage)
        {
            //Получаем набор файлов в Хранилище
            Store.IFile[] files = storage.files.EnumerateFiles(Store.enFileKind.anyFile).ToArray();

            //Получаем число типов файлов (в рамках избыточности)
            int essentialTypesCount = Enum.GetValues(typeof(Store.enFileEssential)).Length;

            //Если в Хранилище нет файлов
            if(files.Length < essentialTypesCount)
                Assert.Fail("В Хранилище недостаточно файлов для разметки (в рамках избыточности).");

            //Первые файлы размечаем разными типами (в рамках избыточности)
            for (int i = 0; i < essentialTypesCount; i++)
                files[i].FileEssential = (Store.enFileEssential)i;

            Random random = new Random();
            //Тип (в рамках избыточности) остальных файлов определяется случайным образом
            for (int j = essentialTypesCount; j < files.Length; j++)
                files[j].FileEssential = (Store.enFileEssential)(random.Next() % essentialTypesCount);
        }

        /// <summary>
        /// Проверка на то, что все существенные файлы были сохранены, а несущественные нет.
        /// </summary>
        /// <param name="storage">Хранилище</param>
        private void CheckEssential(Store.Storage storage)
        {
            //Получаем путь до поддиректории рабочей директории Хранилища с очищенными исходными текстами
            string clearWorkSubDirectoryPath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR);

            //Проходим по всем файлам
            foreach (Store.IFile file in storage.files.EnumerateFiles(Store.enFileKind.anyFile))
            {
                //Получаем путь до файла
                string filePath = file.FullFileName_Original;

                //Получаем путь до файла в директории с очищенными исходными текстами
                string fileCopyPath = PathController.ChangePrefix(
                    filePath,
                    storage.appliedSettings.FileListPath,
                    clearWorkSubDirectoryPath
                 );

                //Существенный файл должен существовать
                if (file.FileEssential == Store.enFileEssential.ESSENTIAL)
                {
                    if (!FileController.IsExists(fileCopyPath))
                        Assert.Fail(
                            "Существенный файл <" + filePath + ">" +
                            "не найден в поддиректории рабочей директории Хранилища с очищенными исходными текстами <" + clearWorkSubDirectoryPath + ">.");
                }
                //Несущественный - нет
                else
                {
                    if (FileController.IsExists(fileCopyPath))
                        Assert.Fail(
                            "Несущественный файл <" + filePath + ">" +
                            "найден в поддиректории рабочей директории Хранилища с очищенными исходными текстами <" + clearWorkSubDirectoryPath + ">.");
                }
            }
        }
    }
}
