﻿using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;

namespace TestBinaryRightsChecker
{
    /// <summary>
    /// Сводное описание для BinariesRightsCheckerTestDriver
    /// </summary>
    [TestClass]
    public class TestBinariesRightsCheckerDriver
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// тест обработки драйвера ОС. Генерируется по файлу отчет и сравнивается с эталонным
        /// </summary>
        [TestMethod]
        public void BinaryRightsChecker_Driver()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                // plugin                                     
                new IA.Plugins.Analyses.BinaryRightsChecker.BinaryRightsChecker(),

                // isUseCachedStorage
                false,

                // prepareStorage                                      
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Source\Driver\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun                           
                (reportsPath, testMaterialsPath) =>
                {
                    string original_reportFilePath = Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Res\Driver\Report.txt");
                    string new_reportFilePath = Path.Combine(reportsPath, @"Report.txt");

                    //Сравниваем отчёты
                    return TestUtilsClass.Reports_TXT_Compare(original_reportFilePath, new_reportFilePath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun   
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun         
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            
                );
        }
    }
}
