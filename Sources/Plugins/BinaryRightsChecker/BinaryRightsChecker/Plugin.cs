using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Analyses.BinaryRightsChecker
{
    /// <summary>
    /// Статический класс плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Проверка настроек доступа к сегментам кода и данных";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.BINARY_RIGHTS_CHECKER;

        /// <summary>
        /// Наименования задач плагина при выполнении
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка файлов")]
            FILES_PROCESSING
        };

        /// <summary>
        /// Наименования задач плагина при генерации отчётов
        /// </summary>
        internal enum ReportTasks
        {
            [Description("Формирование отчёта")]
            REPORT_GENERATION
        };
    }

    /// <summary>
    /// Основной класс плагина
    /// </summary>
    public class BinaryRightsChecker : IA.Plugin.Interface
    {
        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Список файлов для обработки
        /// </summary>
        private List<File> files = new List<File>();

        #region PluginInterface Members
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.RERUN | Plugin.Capabilities.REPORTS;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            #region Получаем результаты из хранилища
            Store.Table.IBufferReader ibr = storage.pluginResults.LoadResult(PluginSettings.ID);
            if(ibr != null)
            {
                files = new List<File>();
                int n = ibr.GetInt32();
                for (int i = 0; i < n; ++i)
                {
                    File newFile = new File(ibr.GetString());
                    newFile.Contents = ibr.GetString();
                    int nSects = ibr.GetInt32();
                    for (int j = 0; j < nSects; ++j)
                    {
                        Section sect = new Section();
                        ibr.GetBool(ref sect.canExecute);
                        ibr.GetBool(ref sect.canRead);
                        ibr.GetBool(ref sect.canWrite);
                        sect.ends = ibr.GetString();
                        sect.name = ibr.GetString();
                        sect.starts = ibr.GetString();
                        sect.type = ibr.GetString();
                        newFile.Sections.Add(sect);
                    }
                    files.Add(newFile);
                }
            }
            #endregion
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = String.Empty;
            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description())
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.ReportTasks.REPORT_GENERATION.Description(), 1)
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            if (!System.IO.File.Exists("Common\\dumpbin.exe"))
                throw new Exception("Файл dumpbin.exe не найден");

            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, storage.files.Count());
            
            Process dumpBin;
            string line;
            File current;
            Section currSec;
            ulong j = 0;

            foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                if (file.fileType.Contains().HasFlag(enTypeOfContents.EXECUTABLE))
                {
                    current = new File(file.FileNameForReports);
                    dumpBin = new Process();
                    dumpBin.StartInfo.FileName = "Common\\dumpbin.exe";
                    dumpBin.StartInfo.Arguments = "/headers \"" + file.FullFileName_Original + "\"";
                    dumpBin.StartInfo.RedirectStandardOutput = true;
                    dumpBin.StartInfo.UseShellExecute = false;
                    dumpBin.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    dumpBin.StartInfo.CreateNoWindow = true;
                    dumpBin.Start();

                    current.Contents = dumpBin.StandardOutput.ReadToEnd();
                    dumpBin.StandardOutput.Close();

                    MemoryStream stringInMemoryStream = new MemoryStream(ASCIIEncoding.Default.GetBytes(current.Contents));
                    StreamReader sr = new StreamReader(stringInMemoryStream);

                    while ((line = sr.ReadLine()) != null)
                        if (line.StartsWith("SECTION HEADER #"))
                            break;

                    if (line == null)
                    {
                        IA.Monitor.Log.Error(PluginSettings.Name, "Файл " + file.FileNameForReports + " не обработан при помощи DumpBin");
                        IA.Monitor.Tasks.Update(PluginSettings.Name,PluginSettings.Tasks.FILES_PROCESSING.Description(),Monitor.Tasks.Task.UpdateType.STAGE, ++j);
                        continue;
                    }

                    do
                    {
                        try
                        {
                            line = sr.ReadLine(); // название секции
                            currSec = new Section();
                            currSec.name = line.Substring(0, line.IndexOf(" name")).Substring(line.Substring(0, line.IndexOf(" name")).LastIndexOf(" ") + 1);
                            line = sr.ReadLine(); // пропускаем размер секции

                            line = sr.ReadLine(); // Читаем адреса секции
                            line = line.Substring(line.IndexOf("(") + 1);
                            currSec.starts = line.Substring(0, line.IndexOf(" "));
                            line = line.Substring(line.IndexOf(" ") + 4);
                            currSec.ends = line.Substring(0, line.IndexOf(")"));
                            
                            for (int i = 0; i < 7; ++i)
                                line = sr.ReadLine(); // пропускаем неинтересное                

                            line = sr.ReadLine(); // тип секции
                            if (!(line.Contains("Read") || line.Contains("Write") || line.Contains("Execute")))
                            {
                                currSec.type = line.Substring(9);

                                line = sr.ReadLine(); // права секции
                            }

                            if (line.Contains("Shared"))
                                line = sr.ReadLine(); // права секции

                            if (!line.Contains("Execute") && !line.Contains("Read") && !line.Contains("Write"))
                                line = sr.ReadLine(); // права секции

                            currSec.canExecute = (line.Contains("Execute"));
                            currSec.canRead = (line.Contains("Read"));
                            currSec.canWrite = (line.Contains("Write"));

                            current.Sections.Add(currSec);
                        }
                        catch 
                        {
                        }

                        while (!(line == null || line.StartsWith("SECTION HEADER #") || line.StartsWith("  Summary")))
                            line = sr.ReadLine();
                    }
                    while (line != null && !line.StartsWith("  Summary"));

                    files.Add(current);
                }
                if (files.Count == 0)
                {
                    Monitor.Log.Warning(Name, "Нет бинарных файлов для анализа");
                }
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++j);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
            Store.Table.IBufferWriter ibw = Store.Table.WriterPool.Get();
            ibw.Add(files.Count);
            foreach (File file in files)
            {
                ibw.Add(file.name);
                ibw.Add(file.Contents);
                ibw.Add(file.Sections.Count);
                foreach (Section sects in file.Sections)
                {
                    ibw.Add(sects.canExecute);
                    ibw.Add(sects.canRead);
                    ibw.Add(sects.canWrite);
                    ibw.Add(sects.ends);
                    ibw.Add(sects.name);
                    ibw.Add(sects.starts);
                    ibw.Add(sects.type);
                }
            }

            storage.pluginResults.SaveResults(PluginSettings.ID, ibw);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            if (storage == null)
                throw new Exception("Хранилище не определено.");

            (new ReportsGenerator(reportsDirectoryPath)).Run(files);

            //Обновляем прогресс
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.ReportTasks.REPORT_GENERATION.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 1);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {

        }
       
        #endregion
        
    }

    #region Other code

    /// <summary>
    /// файл
    /// </summary>
    public class File
    {
        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="Name">имя файла</param>
        public File(string Name)
        {
            Sections = new List<Section>();
            name = Name;
            Contents = string.Empty;
        }
        /// <summary>
        /// имя
        /// </summary>
        public string name;
        /// <summary>
        /// сегмент
        /// </summary>
        public List<Section> Sections;
        /// <summary>
        /// содержимое
        /// </summary>
        public string Contents;
    }

    /// <summary>
    /// сегмент
    /// </summary>
    public class Section
    {
        /// <summary>
        /// имя
        /// </summary>
        public string name;
        /// <summary>
        /// координаты начала
        /// </summary>
        public string starts;
        /// <summary>
        /// координаты окончания
        /// </summary>
        public string ends;
        /// <summary>
        /// тип
        /// </summary>
        public string type;
        /// <summary>
        /// возможна запись?
        /// </summary>
        public bool canRead;
        /// <summary>
        /// возможно чтение? 
        /// </summary>
        public bool canWrite;
        /// <summary>
        /// возможно исполнение? 
        /// </summary>
        public bool canExecute;
    }

    #endregion

}
