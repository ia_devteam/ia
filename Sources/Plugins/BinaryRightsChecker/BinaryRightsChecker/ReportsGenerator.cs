﻿using System;
using System.Collections.Generic;
using System.IO;

namespace IA.Plugins.Analyses.BinaryRightsChecker
{
    /// <summary>
    /// Класс, реализующий логику работы плагина при генерации отчётов
    /// </summary>
    internal class ReportsGenerator
    {
        /// <summary>
        /// Имя файла отчёта
        /// </summary>
        const string ReportFileName = "Report.txt";

        /// <summary>
        /// Путь до директории для формирования отчётов
        /// </summary>
        string reportsDirectoryPath;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до директориии для формирования отчётов</param>
        internal ReportsGenerator(string reportsDirectoryPath)
        {
            //Проверка на разумность
            if (reportsDirectoryPath == null)
                throw new Exception("Директория для формирования отчётов не определена.");

            //Проверка на разумность
            if (!Directory.Exists(reportsDirectoryPath))
                throw new Exception("Директория для формирования отчётов не существует.");

            this.reportsDirectoryPath = reportsDirectoryPath;
        }
        
        /// <summary>
        /// Запуск генерации отчёта
        /// </summary>
        /// <param name="files">Список файлов</param>
        /// <returns>Успешно ли сгенерирован отчёт?</returns>
        internal void Run(List<File> files)
        {            
            //Проверка на разумность
            if (files == null)
                throw new Exception("Список файлов не определён.");

            // генерируем протоколы DumpBin
            foreach (File file in files)
            {
                string path = Path.Combine(this.reportsDirectoryPath, "DumpBin", file.name);
                Directory.CreateDirectory(path.Substring(0, path.LastIndexOf("\\")));
                StreamWriter sw = new StreamWriter(path + ".dump");
                sw.Write(file.Contents);
                sw.Close();
            }

            using (StreamWriter writer = new StreamWriter(Path.Combine(this.reportsDirectoryPath, ReportsGenerator.ReportFileName)))
            {
                if (files.Count == 0)
                {
                    writer.WriteLine("Бинарных файлов в составе исходных текстов не обнаружено.");
                    return;
                }

                writer.WriteLine("Исследуемые файлы:");

                foreach (File file in files)
                    writer.WriteLine("\t" + file.name);

                List<File> bad = new List<File>();
                foreach (File file in files)
                    foreach (Section section in file.Sections)
                        if (section.canExecute && section.canWrite)
                        {
                            bad.Add(file);
                            break;
                        }

                if (bad.Count == 0)
                {
                    writer.WriteLine("Для всех файлов редактирование сегментов кода и исполнение сегментов данных невозможно.");
                }
                else
                {
                    writer.WriteLine("Для следующих файлов возможно редактирование сегментов кода и исполнение сегментов данных:");

                    foreach (File file in bad)
                        writer.WriteLine("\t" + file.name);
                }

                writer.WriteLine("Ниже для каждого файла приведены настроек всех входящих в него сегментов. ");

                foreach (File file in files)
                {
                    writer.WriteLine(file.name);

                    foreach (Section section in file.Sections)
                    {
                        writer.WriteLine("\t Имя: " + section.name);

                        if (!string.IsNullOrEmpty(section.type))
                            writer.WriteLine("\t\t Тип: " + section.type);

                        writer.WriteLine("\t\t Адрес начала: " + section.starts);
                        writer.WriteLine("\t\t Адрес окончания: " + section.ends);

                        writer.WriteLine("\t\t Права:");
                        writer.WriteLine("\t\t\t Выполнение: " + ((section.canExecute) ? "Да" : "Нет"));
                        writer.WriteLine("\t\t\t Чтение: " + ((section.canRead) ? "Да" : "Нет"));
                        writer.WriteLine("\t\t\t Запись: " + ((section.canWrite) ? "Да" : "Нет"));
                    }
                }
            }
        }
    }
}
