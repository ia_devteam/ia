﻿using System.IO;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;
using Store;
using IA.Plugins.Analyses.WayGenerator;

namespace TestWayGenerator
{
    [TestClass]
    public class TestWayGenerator
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Список сообщений
            /// </summary>
            public List<string> Messages
            {
                get
                {
                    return messages;
                }
            }
        }

        /// <summary>
        /// Путь до папки с эталонными отчетами плагина
        /// </summary>
        private string samplesDirectoryPath;

        /// <summary>
        /// Подпапка с эталонными отчетами
        /// </summary>
        private const string SAMPLES_SUBDIRECTORY = @"WayGenerator\Reports";

        /// <summary>
        /// Архив с отчетами (НДВ2)
        /// </summary>
        private const string ARCHIVE_REPORT_NDV2 = "Traces_NDV2.7z";

        /// <summary>
        /// Архив с отчетами (НДВ3)
        /// </summary>
        private const string ARCHIVE_REPORT_NDV3 = "Traces_NDV3.7z";

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        private Storage currentStorage;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        
        /// <summary>
        /// Перехватчик сообщений монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();

            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            samplesDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, SAMPLES_SUBDIRECTORY, testUtils.TestName);
        }

        /// <summary>
        /// После каждого теста очищаем текущие поля.
        /// </summary>
        [TestCleanup]
        public void EachTest_Cleanup()
        {
            //Сбрасываем информацию о текущем Хранилище
            currentStorage = null;
        }

        #region Fast Tests
        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Конструкции в Хранилище создаются вручную. Используется конструкции if-then-else и return.
        /// </summary>
        [TestMethod]
        public void WayGenerator_Simple_NDV2()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFile file = storage.files.Add(enFileKindForAdd.externalFile);
                    file.FullFileName_Original = "test.cs";

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "f";

                    storage.sensors.SetSensorStartNumber(1);
                    func.EntryStatement = storage.statements.AddStatementIf();
                    storage.sensors.AddSensorBeforeStatement(func.EntryStatement, Kind.START);
                    
                    (func.EntryStatement as IStatementIf).ThenStatement = storage.statements.AddStatementOperational();
                    storage.sensors.AddSensorBeforeStatement((func.EntryStatement as IStatementIf).ThenStatement, Kind.INTERNAL);

                    (func.EntryStatement as IStatementIf).ElseStatement = storage.statements.AddStatementOperational();
                    storage.sensors.AddSensorBeforeStatement((func.EntryStatement as IStatementIf).ElseStatement, Kind.INTERNAL);

                    func.EntryStatement.NextInLinearBlock = storage.statements.AddStatementReturn();
                    storage.sensors.AddSensorBeforeStatement(func.EntryStatement.NextInLinearBlock, Kind.FINAL);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true; 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Конструкции в Хранилище создаются вручную. Используется конструкции for и return.
        /// </summary>
        [TestMethod]
        public void WayGenerator_Hard_NDV2()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFile file = storage.files.Add(enFileKindForAdd.externalFile);
                    file.FullFileName_Original = "test.cs";

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "f";

                    storage.sensors.SetSensorStartNumber(1);
                    func.EntryStatement = storage.statements.AddStatementFor();
                    storage.sensors.AddSensorBeforeStatement(func.EntryStatement, Kind.START);

                    (func.EntryStatement as IStatementFor).Body = storage.statements.AddStatementOperational();
                    storage.sensors.AddSensorBeforeStatement((func.EntryStatement as IStatementFor).Body, Kind.INTERNAL);

                    func.EntryStatement.NextInLinearBlock = storage.statements.AddStatementReturn();
                    storage.sensors.AddSensorBeforeStatement(func.EntryStatement.NextInLinearBlock, Kind.FINAL);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true; 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 3 уровню НДВ.
        /// Функции и связь между ними в Хранилище задаются вручную.
        /// </summary>
        [TestMethod]
        public void WayGenerator_Simple_NDV3()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFile file = storage.files.Add(enFileKindForAdd.externalFile);
                    file.FullFileName_Original = "test.cs";

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "f";

                    IFunction func2 = storage.functions.AddFunction();
                    func2.Name = "f2";

                    IFunction func3 = storage.functions.AddFunction();
                    func3.Name = "f3";

                    func.AddCall(func2);
                    func2.AddCall(func3);

                    SetSettings(storage, false);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) => 
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, @"NDV3", ARCHIVE_REPORT_NDV3));

                    return true; 
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 3 уровню НДВ. 
        /// Исходные тексты представлены на языке С++
        /// </summary>
        [TestMethod]
        public void WayGenerator_Hard_NDV3()
        {
            string sourcePostfixPart = @"Sources\WayGenerator\C\Zlib\";
            const string understandReportsPrefixPart = @"Understand\Zlib";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\Sources\WayGenerator\C\Zlib\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);

                    SetSettings(storage, false);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, @"NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 3 уровню НДВ. 
        /// Исходные тексты представлены на языке C# с рекурсивными вызовами функций.
        /// </summary>
        [TestMethod]
        public void WayGenerator_SimpleFuncCalls_NDV3()
        {
            string sourcePostfixPart = @"Sources\CSharp\SimpleFuncCalls\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\SimpleFuncCalls\"));

                    SetSettings(storage, false);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, @"NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }
        #endregion Fast Tests
        #region Statements Test
        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Исходные файлы - простые конструкции на языке C#.
        /// Проверяется адеквадность составления путей при простых конструкциях языка.
        /// </summary>
        [TestMethod]
        public void WayGenerator_CSharpStatements_NDV2()
        {
            string sourcePostfixPart = @"Sources\CSharp\TestStatements\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestStatements\"), level2: true);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Исходные файлы - простые конструкции на языке C#.
        /// Проверяется адеквадность составления путей конструкций try-catch и try-catch-finally.
        /// </summary>
        [TestMethod]
        public void WayGenerator_TryCatchCSharpStatements_NDV2()
        {
            string sourcePostfixPart = @"Sources\CSharp\TestStatements\TryCatchFinallyStatement\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestStatements\TryCatchFinallyStatement\"), level2: true);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Исходные файлы - простые конструкции на языке C#.
        /// Проверяется адеквадность составления неповторяющихся путей конструкции continue.
        /// </summary>
        [TestMethod]
        public void WayGenerator_ContinueCSharpStatements_NDV2()
        {
            string sourcePostfixPart = @"Sources\CSharp\TestStatements\ContinueStatement\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestStatements\ContinueStatement\"), level2: true);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Исходные файлы - простые конструкции на языке C#.
        /// Проверяется адеквадность составления неповторяющихся путей конструкции switch.
        /// </summary>
        [TestMethod]
        public void WayGenerator_SwitchCSharpStatements_NDV2()
        {
            string sourcePostfixPart = @"Sources\CSharp\TestStatements\SwitchStatement\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestStatements\SwitchStatement\"), level2: true);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Исходные файлы - простые конструкции на языке PHP.
        /// Проверяется адеквадность составления путей при простых конструкциях языка.
        /// </summary>
        [TestMethod]
        public void WayGenerator_PHPStatements_NDV2()
        {
            string sourcePostfixPart = @"Sources\PHPStatements\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_PhpParser(storage);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        //FIXME - раскомментировать, когда поправят парсер JavaScript из-за ошибок стейтментов.
        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Исходные файлы - простые конструкции на языке JavaScript.
        /// Проверяется адеквадность составления путей при простых конструкциях языка.
        /// </summary>
        [TestMethod]
        public void WayGenerator_JSStatements_NDV2()
        {
            string sourcePostfixPart = @"Sources\JS\TestStatements\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_JavaScriptParser(storage);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Исходные файлы - простые конструкции на языке C#.
        /// Проверяется адеквадность составления путей при простых конструкциях языка.
        /// </summary>
        [TestMethod]
        public void WayGenerator_ComplexStatements_NDV2()
        {
            string sourcePostfixPart = @"Sources\CSharp\ComplexStatements\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\ComplexStatements\"), level2: true);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ.
        /// Исходные файлы - простые конструкции на языке C#.
        /// Проверяется адеквадность составления путей при простых конструкциях языка.
        /// </summary>
        [TestMethod]
        public void WayGenerator_ComplexStatements_DoubleLoop()
        {
            string sourcePostfixPart = @"Sources\JS\TestStatements\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    storage.sensors.SetSensorStartNumber(1);
                    var func = storage.functions.AddFunction();
                    func.EntryStatement = storage.statements.AddStatementFor();
                    storage.sensors.AddSensorBeforeStatement(1, func.EntryStatement, Kind.START);
                    var op = (func.EntryStatement as IStatementFor).Body = storage.statements.AddStatementOperational();
                    storage.sensors.AddSensorBeforeStatement(2, op, Kind.INTERNAL);
                    op.NextInLinearBlock = storage.statements.AddStatementFor();
                    storage.sensors.AddSensorBeforeStatement(3, op.NextInLinearBlock, Kind.INTERNAL);
                    var op2 = (op.NextInLinearBlock as IStatementFor).Body = storage.statements.AddStatementOperational();
                    storage.sensors.AddSensorBeforeStatement(4, op2, Kind.INTERNAL);
                    SetSettings(storage, true);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }


        #endregion Statements Test
        #region Simple Tests
        /// <summary>
        /// Тест на проверку вывода сообщения о не найденых датчиках. 
        /// Плагин запускается на генерацию путей по 2-му уровню.
        /// </summary>
        [TestMethod]
        public void WayGenerator_NoSensors_NDV2()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SetSettings(storage, true);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => 
                {
                    CheckMessage("В Хранилище не найдено ни одного датчика.");

                    return true; 
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку вывода сообщения о не найденых функциях. 
        /// Плагин запускается на генерацию путей по 3-му уровню.
        /// </summary>
        [TestMethod]
        public void WayGenerator_NoFunctions_NDV3()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    SetSettings(storage, false);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("В Хранилище не найдено ни одной функции.");

                    return true;
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 3 уровню НДВ, если между функциями связей нет.
        /// Функции в Хранилище создаются вручную.
        /// </summary>
        [TestMethod]
        public void WayGenerator_NoFunctionsConnections_NDV3()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFile file = storage.files.Add(enFileKindForAdd.externalFile);
                    file.FullFileName_Original = "test.cs";
                    
                    IFunction func = storage.functions.AddFunction();
                    func.Name = "f1";

                    IFunction func2 = storage.functions.AddFunction();
                    func2.Name = "f2";

                    IFunction func3 = storage.functions.AddFunction();
                    func3.Name = "f3";

                    SetSettings(storage, false);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, @"NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 3 уровню НДВ, если присутствуют одноименные функции.
        /// Функции в Хранилище создаются вручную.
        /// </summary>
        [TestMethod]
        public void WayGenerator_SameNameFunctions_NDV3()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFile file = storage.files.Add(enFileKindForAdd.externalFile);
                    file.FullFileName_Original = "test.cs";

                    IFunction func = storage.functions.AddFunction();
                    func.Name = "f";
                                        
                    IFunction func2 = storage.functions.AddFunction();
                    func2.Name = "f";

                    IFunction func3 = storage.functions.AddFunction();
                    func3.Name = "f1";

                    IFunction func4 = storage.functions.AddFunction();
                    func4.Name = "f2";

                    func.AddCall(func3);
                    func.AddCall(func4);
                    func2.AddCall(func4);

                    SetSettings(storage, false);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, @"NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 2 уровню НДВ. Связей между конструкциями нет.
        /// Функции и связь между ними в Хранилище задаются вручную.
        /// </summary>
        [TestMethod]
        public void WayGenerator_NoStatements_NDV2()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFile file = storage.files.Add(enFileKindForAdd.externalFile);
                    file.FullFileName_Original = "test.cs";
                    
                    storage.sensors.SetSensorStartNumber(1);

                    IFunction func1 = storage.functions.AddFunction();
                    func1.Name = "f1";
                    storage.sensors.AddSensor(func1.Id, Kind.START);
                    storage.sensors.AddSensor(func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(func1.Id, Kind.FINAL);

                    IFunction func2 = storage.functions.AddFunction();
                    func2.Name = "f2";
                    storage.sensors.AddSensor(func2.Id, Kind.START);
                    storage.sensors.AddSensor(func2.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(func2.Id, Kind.FINAL);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку генерации путей по 3 уровню НДВ. Внутри функции датчиков 3, а не 2, как положено.
        /// Функции и связь между ними в Хранилище задаются вручную.
        /// </summary>
        [TestMethod]
        public void WayGenerator_WrongSensorsCount_NDV3()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFile file = storage.files.Add(enFileKindForAdd.externalFile);
                    file.FullFileName_Original = "test.cs";

                    storage.sensors.SetSensorStartNumber(1);

                    IFunction func1 = storage.functions.AddFunction();
                    func1.Name = "f1";
                    storage.sensors.AddSensor(func1.Id, Kind.START);
                    storage.sensors.AddSensor(func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(func1.Id, Kind.FINAL);

                    IFunction func2 = storage.functions.AddFunction();
                    func2.Name = "f2";
                    storage.sensors.AddSensor(func2.Id, Kind.START);
                    storage.sensors.AddSensor(func2.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(func2.Id, Kind.FINAL);

                    func1.AddCall(func2);

                    SetSettings(storage, false);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }
        #endregion Simple Tests
        #region Test .NetZip functiions
        /// <summary>
        /// Проверка работоспособности плагина на некоторых "трудных" функциях .NetZip.
        /// Функция _SaveSfxStub.
        /// </summary>
        [TestMethod]
        public void WayGenerator_DotNetZip1_NDV2()
        {
            string sourcePostfixPart = @"Sources\CSharp\TestDotNetZip1\";

            testUtils.RunTest(
                sourcePostfixPart,                                              // sourcePostfixPart
                new WayGenerator(),                                             // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestDotNetZip1\"), level2: true);

                    SetSettings(storage, true);
                    currentStorage = storage;
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV2", ARCHIVE_REPORT_NDV2), Path.Combine(reportsPath, "NDV2", ARCHIVE_REPORT_NDV2));
                    CheckReports(Path.Combine(samplesDirectoryPath, "NDV3", ARCHIVE_REPORT_NDV3), Path.Combine(reportsPath, "NDV3", ARCHIVE_REPORT_NDV3));

                    return true;
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }
        #endregion Test .NetZip functiions


        /// <summary>
        /// Установка настроек.
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="isNDV2">true - 2-ой уровень; false - 3-ий уровень.</param>
        private void SetSettings(Storage storage, bool isNDV2)
        {            
            Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();

            writer.Add(isNDV2);

            storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.WAY_GENERATOR, writer);
            
            currentStorage = storage;
        }

        /// <summary>
        /// Проверка получившихся отчетов плагина с эталонными
        /// </summary>
        /// <param name="sample7zReportPath">Путь до архива в эталонных отчетах. Не может быть пустым или null.</param>
        /// <param name="plugin7zReportPath">Путь до архива в отчетах плагина. Не может быть пустым или null.</param>
        private void CheckReports(string sample7zReportPath, string plugin7zReportPath)
        {
            TestUtilsClass.Report_7z_Compare(sample7zReportPath, plugin7zReportPath, currentStorage, Store.Const.PluginIdentifiers.WAY_GENERATOR,
                (a, b) => TestUtilsClass.Report_7z_Compare(a, b, currentStorage, Store.Const.PluginIdentifiers.WAY_GENERATOR,
                    (f1, f2) => TestUtilsClass.Reports_TXT_Compare(f1, f2, isIgnoreLinesOrder: true)));
        }

        /// <summary>
        /// Проверка полученного из монитора сообщения
        /// </summary>
        /// <param name="message">Сообщение. Не может быть пустым или null.</param>
        private void CheckMessage(string message)
        {
            Assert.IsTrue(listener.ContainsPart(message), "Сообщение не было получено: <" + message + ">.");
        }
    }
}
