﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

using Store;
using IA.Extensions;
using FileOperations;

namespace IA.Plugins.Analyses.WayGenerator
{
    /// <summary>
    /// Класс, хранящий все список вызовов функций вместе с количеством переходов до функции
    /// </summary>
    public class Calls
    {
        /// <summary>
        /// Словарь вложенности функции
        /// </summary>
        Dictionary<IFunction, int> intern;

        /// <summary>
        /// Конструктор
        /// </summary>
        public Calls()
        {
            intern = new Dictionary<IFunction, int>();
        }

        /// <summary>
        /// Добавляем функцию в список, если уровень ее вложенности по вызовам
        /// меньше, чем имещийся - обновляем его
        /// </summary>
        /// <param name="f">Функция</param>
        /// <param name="level">Уровень вложенности</param>
        /// <returns></returns>
        public bool Add(IFunction f, int level = Int32.MaxValue)
        {
            if (!intern.ContainsKey(f))
                intern.Add(f, level);

            if (intern[f] > level)
                intern[f] = level;

            return true;
        }

        /// <summary>
        /// Содержит ли список функцию
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public bool Contains(IFunction f)
        {
            return intern.ContainsKey(f);
        }

        /// <summary>
        /// Возвращает список функций в списке вызовов
        /// </summary>
        /// <returns></returns>
        public List<IFunction> ToList()
        {
            return intern.Keys.ToList();
        }

        /// <summary>
        /// Проверяет, пуст ли список функций
        /// </summary>
        /// <returns></returns>
        public bool isEmpty()
        {
            return intern.Keys.Count() == 0;
        }

        /// <summary>
        /// Вовзвращает список пар "функция - уровень вложенности"
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<IFunction, int>> ToKeyValueList()
        {
            return intern;
        }

        /// <summary>
        /// Возвращает prettyprint списка функций и их вызовов
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            HashSet<string> result = new HashSet<string>();

            foreach (KeyValuePair<IFunction, int> pair in intern.OrderBy(key => key.Value))
                result.Add(" функция " + pair.Key.FullNameForReports + " по цепочке вызовов длины " + pair.Value);

            return string.Join(",", result.ToArray());
        }
    }

    /// <summary>
    /// Класс, отвечающий за построение маршрутов по 3му уровню НДВ
    /// </summary>
    public class WayGeneratorLevel3
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Генерируемый путь. Содержит функцию и все ее вызовы.
        /// </summary>
        Dictionary<IFunction, Calls> way;

        /// <summary>
        /// 
        /// </summary>
        HashSet<IFunction> full;

        /// <summary>
        /// Архив с отчетом
        /// </summary>
        ArchiveWriter archive;

        /// <summary>
        /// Список всех просмотренных функций
        /// </summary>
        HashSet<IFunction> visited;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="archive">Путь, куда будет складываться отчет</param>
        public WayGeneratorLevel3(Storage storage, ArchiveWriter archive)
        {
            if (storage == null)
                throw new Exception("Хранилище не определено.");

            if (archive == null)
                throw new Exception("Не указан архив для отчета.");

            this.storage = storage;
            this.archive = archive;

            this.way = new Dictionary<IFunction, Calls>();
            this.full = new HashSet<IFunction>();
            this.visited = new HashSet<IFunction>();
        }

        /// <summary>
        /// Функция, отвечающая за построение и генерацию маршрутов (всех)
        /// </summary>
        public void Run()
        {
            //Обновляем задачу
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WAY_GENERATOR_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)storage.functions.EnumerateFunctions(enFunctionKind.REAL).Count());

            //Создаем счетчик обработанных функций
            ulong counter = 0;

            //инициализация
            foreach (IFunction function in storage.functions.EnumerateFunctions(enFunctionKind.REAL))
            {
                Calls cs = new Calls();
                function.Calles().All(g => cs.Add(g.Calles, 1));
                way.Add(function, cs);
                
                //Переходим к следующей функции
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WAY_GENERATOR_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }

            foreach (IFunction func in way.Keys)
            {
                visited = new HashSet<IFunction>();
                Visiter(func);
            }

            ArchivePrinter();
        }

        /// <summary>
        /// Функция-визитер, отвечающая за рекурсивный проход по всем стейтментам
        /// </summary>
        /// <param name="function">Функция, по которой осуществляется проход</param>
        private void Visiter(IFunction function)
        {            
            bool fullFind = true;
            do
            {
                fullFind = true;
                List<IFunction> iter = way[function].ToList();

                //visited = new HashSet<Function>();
                for (int i = 0; i < iter.Count; i++)
                {
                    if (visited.Contains(iter[i]))
                        continue;

                    if (full.Contains(iter[i]))
                    {
                        way[iter[i]].ToKeyValueList().All(g => way[function].Add(g.Key, g.Value + 1));
                        visited.Add(iter[i]);
                    }
                    else
                    {
                        fullFind = false;
                        visited.Add(iter[i]);
                        Visiter(iter[i]);                        
                    }
                }
            }
            while (!fullFind);

            full.Add(function);
        }

        /// <summary>
        /// Функция вывода полученной информации в архив
        /// </summary>
        private void ArchivePrinter()
        {
            //Обновляем задачу
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WAY_GENERATOR_WAY_PRINTER_NDV3.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)way.Count());

            //Создаем счетчик сохраненных вызовов функций
            ulong counter = 0;

            //Путь до архива с маршрутами
            string gzFullFilePath = Path.Combine(storage.GetTempDirectory(PluginSettings.ID).Path, "Маршруты.gz");

            using (StreamWriter writer = new StreamWriter(new GZipStream(File.OpenWrite(gzFullFilePath), CompressionMode.Compress)))
            {
                foreach (IFunction func in way.Keys)
                {
                    if (!way[func].isEmpty())
                        writer.WriteLine("Из функции " + func.FullNameForReports + " достижима" + way[func].ToString());
                    else
                        writer.WriteLine("Функция " + func.FullNameForReports + " не вызывает функций");

                    //Переходим к следующей функции
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WAY_GENERATOR_WAY_PRINTER_NDV3.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                }
            }

            archive.AddFile(gzFullFilePath);
        }
    }
}
