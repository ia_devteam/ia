﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

using Store;
using IOController;
using IA.Extensions;
using FileOperations;

namespace IA.Plugins.Analyses.WayGenerator
{
    /// <summary>
    /// Класс, хранящий информацию о достижимых стейтментах
    /// </summary>
    class ReachSequence
    {
        /// <summary>
        /// Те стейтменты, которые достижимы из данного
        /// </summary>
        public Dictionary<IStatement, int> sequence;

        /// <summary>
        /// Конструктор
        /// </summary>
        public ReachSequence()
        {
            sequence = new Dictionary<IStatement, int>();
        }

        /// <summary>
        /// Добавить стейтмент, с увеличением счетчика на один (по дефолту добавляет с 1цей)
        /// </summary>
        /// <param name="statement"></param>
        /// <param name="hopCount"></param>
        public void Push(IStatement statement, int hopCount = 0)
        {
            if(sequence.ContainsKey(statement))
            {
                if (sequence[statement] > hopCount + 1) 
                    sequence[statement] = hopCount + 1;
                return;
            }

            sequence.Add(statement, hopCount + 1);
        }

        /// <summary>
        /// Обработать список достижимости (добавить все как +1)
        /// </summary>
        /// <param name="sequence"></param>
        public void MergeAndInc(ReachSequence sequence)
        {
            foreach (var key in sequence.sequence.Keys)
                Push(key, sequence.sequence[key]);
        }

        /// <summary>
        /// Обработать список достижимости дочернего узла (добавить все как +1) и сам узел
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="currentStatement"></param>
        public void MergeAndInc(ReachSequence sequence, IStatement currentStatement)
        {
            Push(currentStatement);
            foreach (var key in sequence.sequence.Keys) 
                Push(key, sequence.sequence[key]);
        }

        /// <summary>
        /// Обработать список достижимости (добавить все как +1) кроме указанного узла
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="currentStatement"></param>
        public void MergeAndIncExclude(ReachSequence sequence, IStatement currentStatement)
        {
            foreach (var key in sequence.sequence.Keys) 
                if(key != currentStatement) 
                    Push(key, sequence.sequence[key]);
        }                
    }
    
    /// <summary>
    /// Класс по НДВ 2
    /// </summary>
    public class WayGeneratorLevel2
    {
        /// <summary>
        /// Словарь достижимых узлов
        /// </summary>
        Dictionary<IStatement, ReachSequence> reachDictionary;

        /// <summary>
        /// Уже обработанные узлы
        /// </summary>
        HashSet<IStatement> built;

        /// <summary>
        /// Очередь циклов
        /// </summary>
        Queue<KeyValuePair<IStatement, List<IStatement>>> loopQueue;

        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Архив с отчетом
        /// </summary>
        ArchiveWriter archive;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="storage">Хранилище.</param>
        /// <param name="archive">Архив, куда будет складываться отчет.</param>
        public WayGeneratorLevel2(Storage storage, ArchiveWriter archive)
        {
            if (storage == null)
                throw new Exception("Хранилище не определено.");

            if (archive == null)
                throw new Exception("Не указан архив для отчетов.");

            this.storage = storage;
            this.archive = archive;
        }

        /// <summary>
        /// Отвечает за построение маршрутов
        /// </summary>
        public void Run()
        {
            //Проверка существования переходов
            var storageFunctions = storage.functions.EnumerateFunctions(enFunctionKind.REAL).Where(g => g.EntryStatement != null);
            if (storageFunctions.Count() == 0)
            {
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WAY_GENERATOR_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, 1);

                //Путь до архива с маршрутами
                string gzFullFilePath = Path.Combine(storage.GetTempDirectory(PluginSettings.ID).Path, "Trace_NDV2.gz");

                using (StreamWriter writer = new StreamWriter(new GZipStream(File.OpenWrite(gzFullFilePath), CompressionMode.Compress)))
                    writer.WriteLine("Переходов не обнаружено.");

                archive.AddFile(gzFullFilePath);
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WAY_GENERATOR_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 1);
                return;
            }

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WAY_GENERATOR_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)storageFunctions.Count());
            ulong counter = 0;

            foreach (IFunction func in storageFunctions)
            {
                loopQueue = new Queue<KeyValuePair<IStatement, List<IStatement>>>();
                reachDictionary = new Dictionary<IStatement, ReachSequence>();
                built = new HashSet<IStatement>();

                //обрабатываем часть без циклов
                ProcessWays(func.EntryStatement, new HashSet<IStatement>(), new List<IStatement>());

                //теперь у нас есть в loopQueue циклы - один за одним обрабатываем их
                while (loopQueue.Count > 0)
                {
                    loopQueue = new Queue<KeyValuePair<IStatement, List<IStatement>>>(loopQueue.Reverse());
                    var elem = loopQueue.Dequeue();
                    if (elem.Value.Count == 0)
                    {
                        //переход на самого себя
                        reachDictionary[elem.Key].Push(elem.Key);
                        continue;
                    }
                    elem.Value.Reverse();

                    if (elem.Value[0] == elem.Key)
                        continue;

                    IStatement prev = elem.Key;
                    for (int i = 0; elem.Value[i] != elem.Key; i++)
                    {
                        reachDictionary[elem.Value[i]].MergeAndIncExclude(reachDictionary[prev], elem.Value[i]);
                        prev = elem.Value[i];
                    }

                    //добавляем этот цикл в него
                    reachDictionary[elem.Key].Push(elem.Value[0]);
                }

                //Временная папка в Хранилище
                string tempDirectoryPath = storage.GetTempDirectory(PluginSettings.ID).Path;
                //FIXME - проверка на логичность
                //if (tempDirectoryPath.Length > 240)
                //{
                //    Monitor.Log.Error(PluginSettings.Name, "Слишком длинный путь рабочей директории. Пожалуйста, выберите другую директорию и перезапустите плагин.");
                //    return;
                //}

                //Формируем имя архива с маршрутами
                Location funcDef = func.Definition();

                ulong funcOffset = funcDef != null ? funcDef.GetOffset() : 0;
                string funcFileName = (funcDef != null && funcDef.GetFile() != null) ? funcDef.GetFile().FileNameForReports.Replace(Path.DirectorySeparatorChar, '.') : "";                
                string gzFullFileName = func.FullNameForFileName;
                
                //Хэшируем путь, если вдруг перейдем черту в 256 символов - обрезаем имя файла до 200 символов
                string hashedFilePath = Path.Combine(tempDirectoryPath, (gzFullFileName.Length + tempDirectoryPath.Length > 455 ? gzFullFileName.Substring(0, 200) + gzFullFileName.GetHashCode().ToString("X8") : gzFullFileName) + ".gz");

                //Привинтивная мера создания каталогов - полный путь к функции может состоять из подкаталогов
                DirectoryController.Create(Path.GetDirectoryName(hashedFilePath));

                using (StreamWriter writer = new StreamWriter(new GZipStream(File.OpenWrite(hashedFilePath), CompressionMode.Compress)))
                {
                    writer.WriteLine("Достижимые переходы внутри функции " + func.FullNameForReports);

                    if (reachDictionary.Count == 0)
                        writer.WriteLine("Переходов не найдено.");
                    else
                        foreach (var stPair in reachDictionary.Where(pair => pair.Key.SensorBeforeTheStatement != null).OrderBy(pair => pair.Key.SensorBeforeTheStatement.ID))
                        {
                            if (reachDictionary[stPair.Key].sequence != null && reachDictionary[stPair.Key].sequence.Count(pair => pair.Key.SensorBeforeTheStatement != null) == 0)
                            {
                                writer.WriteLine("Из датчика " + stPair.Key.SensorBeforeTheStatement.ID + " достижимых датчиков не найдено");
                                continue;
                            }

                            writer.Write("Из датчика " + stPair.Key.SensorBeforeTheStatement.ID + " достижимы датчики");

                            HashSet<string> result = new HashSet<string>();

                            foreach (var pair in reachDictionary[stPair.Key].sequence.Where(pair => pair.Key.SensorBeforeTheStatement != null && pair.Key.SensorBeforeTheStatement != stPair.Key.SensorBeforeTheStatement).OrderBy(pair => pair.Value))
                                    result.Add(" " + pair.Key.SensorBeforeTheStatement.ID + "(" + reachDictionary[stPair.Key].sequence[pair.Key] + " переходов)");

                            writer.WriteLine(string.Join(",", result.ToArray()));
                        }
                }

                archive.AddFile(hashedFilePath);
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WAY_GENERATOR_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }
        }

        /// <summary>
        /// Строит маршруты, на вход - текущий стейтмент, список предыдущих стейтментов и лист этих стейтментов (для того, чтобы корректно записывать в очередь циклы)
        /// </summary>
        /// <param name="currentStatement"></param>
        /// <param name="previousStatements"></param>
        /// <param name="listPrevious"></param>
        /// <returns></returns>
        private ReachSequence ProcessWays(IStatement currentStatement, HashSet<IStatement> previousStatements, List<IStatement> listPrevious)
        {
            if (currentStatement.SensorBeforeTheStatement != null && currentStatement.SensorBeforeTheStatement.ID == 15)
            {
                currentStatement = currentStatement;
            }
            //Добавляем в словарь достижимости текущий элемент
            if (!reachDictionary.ContainsKey(currentStatement)) 
                reachDictionary.Add(currentStatement, new ReachSequence());

            previousStatements.Add(currentStatement);
            listPrevious.Add(currentStatement);

            if (!built.Contains(currentStatement))
            {
                foreach (IStatement statement in currentStatement.NextInControl(true))
                {
                    if (previousStatements.Contains(statement))
                    {
                        //loopStack.Push(new LoopInfo(statement, currentStatement, currentSequence.ReturnPath()));
                        List<IStatement> prev = new List<IStatement>();
                        prev.AddRange(listPrevious);

                        loopQueue.Enqueue(new KeyValuePair<IStatement, List<IStatement>>(statement, prev));
                        if (!reachDictionary[currentStatement].sequence.ContainsKey(statement))
                        {
                            reachDictionary[currentStatement].sequence.Add(statement, 1);
                        }
                        else
                        {
                            reachDictionary[currentStatement].sequence[statement] = 1;
                        }

                        continue;
                    }

                    ReachSequence returnedSequence = ProcessWays(statement, previousStatements, listPrevious);
                    reachDictionary[currentStatement].MergeAndInc(returnedSequence, statement);
                }
                built.Add(currentStatement);
            }
            
            listPrevious.Remove(currentStatement);
            previousStatements.Remove(currentStatement);
            return reachDictionary[currentStatement];
        }
    }
}


