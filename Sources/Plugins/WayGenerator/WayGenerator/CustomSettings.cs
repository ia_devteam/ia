﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.WayGenerator
{
    /// <summary>
    /// Основной класс работы с настройками
    /// </summary>
    internal partial class CustomSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        internal CustomSettings()
        {
            InitializeComponent();

            isLevel2_checkBox.Checked = PluginSettings.IsLevel2;
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.IsLevel2 = isLevel2_checkBox.Checked;
        }
    }
}
