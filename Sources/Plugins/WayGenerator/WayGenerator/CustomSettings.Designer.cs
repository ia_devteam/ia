﻿namespace IA.Plugins.Analyses.WayGenerator
{
    partial class CustomSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.isLevel2_checkBox = new System.Windows.Forms.CheckBox();
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // isLevel2_checkBox
            // 
            this.isLevel2_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isLevel2_checkBox.AutoSize = true;
            this.isLevel2_checkBox.Location = new System.Drawing.Point(3, 6);
            this.isLevel2_checkBox.Name = "isLevel2_checkBox";
            this.isLevel2_checkBox.Size = new System.Drawing.Size(349, 17);
            this.isLevel2_checkBox.TabIndex = 14;
            this.isLevel2_checkBox.Text = "Уровень НДВ 2";
            this.isLevel2_checkBox.UseVisualStyleBackColor = true;
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.isLevel2_checkBox, 0, 0);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 2;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(355, 29);
            this.settings_tableLayoutPanel.TabIndex = 11;
            // 
            // CustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(355, 29);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.settings_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox isLevel2_checkBox;
        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
    }
}
