﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.ComponentModel;

using Store;
using Store.Table;

using IA.Extensions;
using IOController;

namespace IA.Plugins.Analyses.WayGenerator
{
    /// <summary>
    /// Стандартный класс констант и установок плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.WAY_GENERATOR;

        /// <summary>
        /// Имя плагина
        /// </summary>
        internal const string Name = "Генератор статических маршрутов";

        /// <summary>
        /// Наименования тасков плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка функций")]
            WAY_GENERATOR_FUNCTIONS,
            [Description("Сохранение путей")]
            WAY_GENERATOR_WAY_PRINTER_NDV3
        };

        /// <summary>
        /// Второй уровень НДВ?
        /// </summary>
        internal static bool IsLevel2;
    }

    /// <summary>
    /// Класс-плагин. Генерирует все ациклические маршруты в графе вызовов и записывает результат в файл.
    /// </summary>
    public class WayGenerator : IA.Plugin.Interface
    {
        /// <summary>
        /// Текущее хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Текущие настройки
        /// </summary>
        CustomSettings settings;

        HashSet<string> phpFunctionsHash = new HashSet<string>();

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS | Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            // Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            if (string.IsNullOrWhiteSpace(reportsDirectoryPath))
                throw new Exception("Путь к отчетам не определен.");

            //Скопировать отчеты
            DirectoryController.Copy(storage.pluginData.GetDataFolder(ID), reportsDirectoryPath, true);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;
            PluginSettings.IsLevel2 = Properties.Settings.Default.IsLevel2;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.IsLevel2);
        }


        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            FileOperations.ArchiveWriter archive;
            #region НДВ 2
            if (PluginSettings.IsLevel2)
            {
                //Если в Хранилище нет датчиков - прекращаем работу
                if (storage.sensors.EnumerateSensors().Count() == 0)
                {
                    Monitor.Log.Error(PluginSettings.Name, "В Хранилище не найдено ни одного датчика.");
                    return;
                }

                using (archive = new FileOperations.ArchiveWriter(Path.Combine(storage.pluginData.GetDataFolder(ID), "NDV2", "Traces_NDV2.7z"), true))
                {
                    DirectoryController.Create(Path.Combine(storage.pluginData.GetDataFolder(ID), "NDV2"));
                    (new WayGeneratorLevel2(storage, archive)).Run();
                }
            }
            #endregion

            #region НДВ 3
            //Если в Хранилище нет функций - прекращаем работу
            if (storage.functions.EnumerateFunctions(enFunctionKind.REAL).Count() == 0)
            {
                Monitor.Log.Error(PluginSettings.Name, "В Хранилище не найдено ни одной функции.");
                return;
            }

            using (archive = new FileOperations.ArchiveWriter(Path.Combine(storage.pluginData.GetDataFolder(ID), "NDV3", "Traces_NDV3.7z"), true))
            {
                DirectoryController.Create(Path.Combine(storage.pluginData.GetDataFolder(ID), "NDV3"));
                (new WayGeneratorLevel3(storage, archive)).Run();
            }

            #endregion
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.IsLevel2 = PluginSettings.IsLevel2;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.IsLevel2);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (settings = new CustomSettings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            return true;
        }

        /// <returns></returns>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    PluginSettings.Tasks.WAY_GENERATOR_FUNCTIONS.ToMonitorTask(Name),
                    PluginSettings.Tasks.WAY_GENERATOR_WAY_PRINTER_NDV3.ToMonitorTask(Name)
                };
            }
        }


        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "NDV:3":
                    PluginSettings.IsLevel2 = false;
                    break;
                case "NDV:2":
                    PluginSettings.IsLevel2 = true;
                    break;
                default:
                    PluginSettings.IsLevel2 = false;
                    break;
            }
        }
    }
}