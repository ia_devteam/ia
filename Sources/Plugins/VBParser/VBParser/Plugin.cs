/*
 * Плагин Парсер VB-VBS
 * Файл Plugin.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Юхтин
 * 
 * Плагин предназначен для вставки датчиков в исходные очищенные тексты по 2-ому и 3-ему уровню.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

using FileOperations;
using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Parsers.VBParser
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.VB_PARSER;

        /// <summary>
        /// Имя плагина
        /// </summary>
        internal const string Name = "Парсер VB(S)";

        internal enum Tasks
        {
            [Description("Обработка файлов. Этап 1")]
            FIRST_STAGE_FILES_PROCESSING,
            [Description("Обработка файлов. Этап 2")]
            SECOND_STAGE_FILES_PROCESSING 
        };

        internal enum TasksReports
        {
            [Description("Генерация отчета")]
            REPORT_GENERATION            
        };

        #region Настройки

        /// <summary>
        /// Идентификатор уровня вставки датчиков
        /// </summary>
        internal static uint LevelID;

        /// <summary>
        /// Номер первого датчика
        /// </summary>
        internal static ulong FirstSensorNumber;

        /// <summary>
        /// Текст датчика
        /// </summary>
        internal static string SensorText;

        /// <summary>
        /// Обрабатывать исходные тексты на языке VisaulBasic?
        /// </summary>
        internal static bool IsVB;

        /// <summary>
        /// Текст датчика на языке VisualBasic в виде коллекции строк
        /// </summary>
        internal static StringCollection VBSensorFunctionTextCollection;

        /// <summary>
        /// Обрабатывать исходные тексты на языке VisaulBasicScript?
        /// </summary>
        internal static bool IsVBS;

        /// <summary>
        /// Текст датчика на языке VisualBasicScript в виде коллекции строк
        /// </summary>
        internal static StringCollection VBSSensorFunctionTextCollection;

        /// <summary>
        /// Путь к файлу со списком системных функций задаётся по умолчанию?
        /// </summary>
        internal static bool IsDefault_SystemFunctionsFilePath;

        /// <summary>
        /// Путь к файлу со списком системных функций
        /// </summary>
        internal static string SystemFunctionsFilePath;

        /// <summary>
        /// Путь к файлу со списком системных переменных задаётся по умолчанию?
        /// </summary>
        internal static bool IsDefault_SystemVariablesFilePath;

        /// <summary>
        /// Путь к файлу со списком системных переменных
        /// </summary>
        internal static string SystemVariablesFilePath;

        #endregion

        /// <summary>
        /// Уровень вставки датчиков
        /// </summary>
        internal static uint Level
        {
            get { return PluginSettings.LevelID; }

            set
            {
                switch (value)
                {
                    case 0:
                    case 2:
                    case 3:
                        Properties.Settings.Default.LevelID = PluginSettings.LevelID = value;
                        return;
                    default:
                        throw new Exception("Уровень вставки датчиков может принимать значения 0 (не вставлять датчики), 2 (2-й уровень), 3 (3-й уровень).");
                }
            }
        }

        /// <summary>
        /// Текст датчика на языке VisualBasic в виде списка строк
        /// </summary>
        internal static List<string> VBSensorFunctionText
        {
            get 
            {
                return PluginSettings.VBSensorFunctionTextCollection != null ? PluginSettings.VBSensorFunctionTextCollection.Cast<string>().ToList() : new List<string>();
            }

            set 
            {
                PluginSettings.VBSensorFunctionTextCollection = new StringCollection();

                if (value == null)
                    return;

                PluginSettings.VBSensorFunctionTextCollection.AddRange(value.ToArray());
            }
        }

        /// <summary>
        /// Текст датчика на языке VisualBasicScript в виде списка строк
        /// </summary>
        internal static List<string> VBSSensorFunctionText
        {
            get
            {
                return PluginSettings.VBSSensorFunctionTextCollection != null ? PluginSettings.VBSSensorFunctionTextCollection.Cast<string>().ToList() : new List<string>();
            }

            set
            {
                PluginSettings.VBSSensorFunctionTextCollection = new StringCollection();

                if (value == null)
                    return;

                PluginSettings.VBSSensorFunctionTextCollection.AddRange(value.ToArray());
            }
        }
    }

    /// <summary>
    /// Возможные типы обрабатываемого файла
    /// </summary>
    internal enum FileType
    {
        VB,
        VBScript
    }

    /// <summary>
    /// Структура, для описания свойств обрабатываемого файла
    /// </summary>
    internal class FileToProcess
    {
        /// <summary>
        /// Полный путь к файлу до обработки
        /// </summary>
        internal string InPath { get; private set; }

        /// <summary>
        /// Полный путь к файлу после обработки
        /// </summary>
        internal string OutPath { get; private set; }

        /// <summary>
        /// Тип файла
        /// </summary>
        internal FileType Type { get; private set; }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="inPath">Путь к файлу.</param>
        /// <param name="outPath">Выходная директория для файла.</param>
        /// <param name="type">Тип обрабатываемого файла.</param>
        internal FileToProcess(string inPath, string outPath, FileType type)
        {
            this.InPath = inPath;
            this.OutPath = outPath;
            this.Type = type;
        }
    }

    public class VBParser : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Настройки
        /// </summary>
        Settings settings;

        /// <summary>
        /// Постфикс пути до файла со списком системных функций
        /// </summary>
        internal const string SystemFunctionsFilePathPostfix = @"\Data\Functions.txt";
        
        /// <summary>
        /// Постфикс пути до файла со списком системных переменных
        /// </summary>
        internal const string SystemVariablesFilePathPostfix = @"\Data\Variables.txt";

        //Разделитель между строками по умолчанию
        internal static string separator = "\r\n";

        //Парсер
        VBParserMain parser = null;

        //Расстановщик датчиков
        SensorInserter inserter = null;

        //Временные директории для обработки XML документов
        string tempDirectoryIn = null;
        string tempDirectoryOut = null;

        //Счётчик временных файлов при обработке XML докуметов
        int tempFileCounter;

        #region PluginInterface Members
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
#if DEBUG
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS;
#else
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
#endif
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.LevelID = Properties.Settings.Default.LevelID;
            PluginSettings.FirstSensorNumber = Properties.Settings.Default.FirstSensorNumber;
            PluginSettings.SensorText = Properties.Settings.Default.SensorText;
            PluginSettings.IsVB = Properties.Settings.Default.IsVB;
            PluginSettings.VBSensorFunctionTextCollection = Properties.Settings.Default.VBSensorFunctionTextCollection;
            PluginSettings.IsVBS = Properties.Settings.Default.IsVBS;
            PluginSettings.VBSSensorFunctionTextCollection = Properties.Settings.Default.VBSSensorFunctionTextCollection;
            PluginSettings.IsDefault_SystemFunctionsFilePath = Properties.Settings.Default.IsDefault_SystemFunctionsFilePath;
            PluginSettings.SystemFunctionsFilePath = Properties.Settings.Default.SystemFunctionsFilePath;
            PluginSettings.IsDefault_SystemVariablesFilePath = Properties.Settings.Default.IsDefault_SystemVariablesFilePath;
            PluginSettings.SystemVariablesFilePath = Properties.Settings.Default.SystemVariablesFilePath;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            bool b = false;

            PluginSettings.Level = settingsBuffer.GetUInt32();

            PluginSettings.FirstSensorNumber = settingsBuffer.GetUInt64();
            PluginSettings.SensorText = settingsBuffer.GetString();

            settingsBuffer.GetBool(ref b);
            PluginSettings.IsVB = b;
            PluginSettings.VBSensorFunctionText = new List<string>(settingsBuffer.GetString().Split('\n'));

            settingsBuffer.GetBool(ref b);
            PluginSettings.IsVBS = b;
            PluginSettings.VBSSensorFunctionText = new List<string>(settingsBuffer.GetString().Split('\n'));

            settingsBuffer.GetBool(ref b);
            PluginSettings.IsDefault_SystemFunctionsFilePath = b;
            PluginSettings.SystemFunctionsFilePath = settingsBuffer.GetString();

            settingsBuffer.GetBool(ref b);
            PluginSettings.IsDefault_SystemVariablesFilePath = b;
            PluginSettings.SystemVariablesFilePath = settingsBuffer.GetString();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.FirstSensorNumber = (storage != null && storage.sensors.Count() > 0) ? storage.sensors.EnumerateSensors().Max(s => s.ID) + 1 : 1;
            PluginSettings.SensorText = "sensor(#)";
            PluginSettings.IsVB = true;
            PluginSettings.VBSensorFunctionText = new List<string>()
                    {
                        "Private Sub sensor(n)",
                        "Dim MyFile",
                        "MyFile = FreeFile",
                        "Open (\"C:\\vb_log_\\\" & App.ThreadID & \".log\") For Append As #MyFile",
                        "Write #MyFile, n",
                        "Close #MyFile",
                        "End Sub"
                    };
            PluginSettings.IsVBS = true;
            PluginSettings.VBSSensorFunctionText = new List<string>()
        {
                        "Private Sub sensor(n)",
                        "Dim WinAPI: Set WinAPI = CreateObject(&quot;DynamicWrapperX&quot;)",
                        "WinAPI.Register &quot;KERNEL32.DLL&quot;, &quot;GetCurrentThreadId&quot;, &quot;r=u&quot;",
                        "Dim fso: Set fso = CreateObject(&quot;Scripting.FileSystemObject&quot;)",
                        "Dim tf: Set tf = fso.OpenTextFile(&quot;c:\vbs_traces_&quot; &amp; WinAPI.GetCurrentThreadId &amp; &quot;.txt&quot;,8,true)",
                        "tf.WriteLine(n)",
                        "tf.Close",
                        "End Sub"
                    };
            PluginSettings.IsDefault_SystemFunctionsFilePath = true;
            PluginSettings.SystemFunctionsFilePath = Path.GetFullPath(Environment.CurrentDirectory + VBParser.SystemFunctionsFilePathPostfix);
            PluginSettings.IsDefault_SystemVariablesFilePath = true;
            PluginSettings.SystemVariablesFilePath = Path.GetFullPath(Environment.CurrentDirectory + VBParser.SystemVariablesFilePathPostfix);

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "NDV:3":
                    PluginSettings.Level = (uint)3;  
                    break;
                case "NDV:2":
                    PluginSettings.Level = (uint)2;  
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
#if DEBUG
            //Проверка на разумность
            if (storage == null)
                throw new Exception("Хранилище не определено.");

            //Формируем отчёт по стейтментам
            (new StatementPrinter(Path.Combine(reportsDirectoryPath, "statements.xml"), storage)).Execute();
#endif
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return settings = new Settings(
                    storage.sensors.EnumerateSensors().Count() == 0 
                    ? 1 
                    : storage.sensors.EnumerateSensors().Max(s => s.ID) + 1
                    );
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FIRST_STAGE_FILES_PROCESSING.Description(), 0),
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.SECOND_STAGE_FILES_PROCESSING.Description(), 0)
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.TasksReports.REPORT_GENERATION.Description(), 0)
                };
            }
		}

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();
            
            if (!PluginSettings.SensorText.Contains("#"))
            {
                message = "Текст датчика должен содержать символ '#'.";
                return false;
            }

            if (PluginSettings.IsVB && String.IsNullOrWhiteSpace(string.Join("\n", PluginSettings.VBSensorFunctionText.ToArray())))
            {
                message = "Текст функции датчика для файлов на языке Visual Basic не может быть пустым.";
                return false;
            }

            if (PluginSettings.IsVBS && String.IsNullOrWhiteSpace(string.Join("\n", PluginSettings.VBSSensorFunctionText.ToArray())))
            {
                message = "Текст функции датчика для файлов на языке Visual Basic Script не может быть пустым.";
                return false;
            }

            if (!PluginSettings.IsDefault_SystemFunctionsFilePath && !String.IsNullOrWhiteSpace(PluginSettings.SystemFunctionsFilePath) && !Directory.Exists(PluginSettings.SystemFunctionsFilePath))
            {
                message = "Указанный файл, содержащий список системных функций, не найден.";
                return false;
            }

            if (!PluginSettings.IsDefault_SystemVariablesFilePath && !String.IsNullOrWhiteSpace(PluginSettings.SystemVariablesFilePath) && !Directory.Exists(PluginSettings.SystemVariablesFilePath))
            {
                message = "Указанный файл, содержащий список системных переменных, не найден.";
                return false;
            }

            return true;
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.LevelID = PluginSettings.LevelID;
            Properties.Settings.Default.FirstSensorNumber = PluginSettings.FirstSensorNumber;
            Properties.Settings.Default.SensorText = PluginSettings.SensorText;
            Properties.Settings.Default.IsVB = PluginSettings.IsVB;
            Properties.Settings.Default.VBSensorFunctionTextCollection = PluginSettings.VBSensorFunctionTextCollection;
            Properties.Settings.Default.IsVBS = PluginSettings.IsVBS;
            Properties.Settings.Default.VBSSensorFunctionTextCollection = PluginSettings.VBSSensorFunctionTextCollection;
            Properties.Settings.Default.IsDefault_SystemFunctionsFilePath = PluginSettings.IsDefault_SystemFunctionsFilePath;
            Properties.Settings.Default.SystemFunctionsFilePath = PluginSettings.SystemFunctionsFilePath;
            Properties.Settings.Default.IsDefault_SystemVariablesFilePath = PluginSettings.IsDefault_SystemVariablesFilePath;
            Properties.Settings.Default.SystemVariablesFilePath = PluginSettings.SystemVariablesFilePath;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.Level);
            settingsBuffer.Add(PluginSettings.FirstSensorNumber);
            settingsBuffer.Add(PluginSettings.SensorText);
            settingsBuffer.Add(PluginSettings.IsVB);
            settingsBuffer.Add(string.Join("\n", PluginSettings.VBSensorFunctionText.ToArray()));
            settingsBuffer.Add(PluginSettings.IsVBS);
            settingsBuffer.Add(string.Join("\n", PluginSettings.VBSSensorFunctionText.ToArray()));
            settingsBuffer.Add(PluginSettings.IsDefault_SystemFunctionsFilePath);
            settingsBuffer.Add(PluginSettings.SystemFunctionsFilePath);
            settingsBuffer.Add(PluginSettings.IsDefault_SystemVariablesFilePath);
            settingsBuffer.Add(PluginSettings.SystemVariablesFilePath);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            if (storage != null && storage.sensors.Count() > 0 && storage.sensors.EnumerateSensors().Max(s => s.ID) >= PluginSettings.FirstSensorNumber)
            {
                ulong tmp_FirstSensorNumber = PluginSettings.FirstSensorNumber;
                PluginSettings.FirstSensorNumber = storage.sensors.EnumerateSensors().Max(s => s.ID) + 1;
                Monitor.Log.Warning(PluginSettings.Name, "Расстановка датчиков с номера <" + tmp_FirstSensorNumber + "> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <" + PluginSettings.FirstSensorNumber + ">.");
            }

            //Временная директория для обработки XML документов
            TempDirectory tempDirectory = storage.GetTempDirectory((ulong)PluginSettings.ID);

            //Генерируем имена для временных каталогов
            string datetime = DateTime.Now.ToString("yyyy-MMM-dd_HH-mm-ss");
            tempDirectoryIn = Path.Combine(tempDirectory.Path, "in_" + datetime);
            tempDirectoryOut = Path.Combine(tempDirectory.Path, "out_" + datetime);
            
            //Счётчик колчисества обработанных файлов
            int fileCounter;

            //Парсер
            parser = new VBParserMain(storage);

            //Расстановщик датчиков
            inserter = PluginSettings.Level != 0 ? new SensorInserter(storage) : null;

            //Формируем список файлов для разбора
            List<FileToProcess> files = GetFilesToProcess();

            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)files.Count);
            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)1).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)files.Count);

            //Импортируем список системных функций
            ImportSystemFunctions();
            //Импортируем список системных переменных
            ImportSystemVariables();

            //-------------------------> Обработка файлов (1-й этап) <--------------------------

            //Обнуляем счётчик обработанных файлов
            fileCounter = 0;

            //Обнуляем счётчик временных файлов
            tempFileCounter = 0;

            //Создаём необходимые временные каталоги
            Directory.CreateDirectory(tempDirectoryIn);
            Directory.CreateDirectory(tempDirectoryOut);

            foreach (FileToProcess file in files)
            {
                // FIXME
                //Monitor.Monitor.WriteToTempInfo = "Обработка файла:" + file.InPath;

                XmlTextReader xmlReader = new XmlTextReader(file.InPath) { Normalization = false };
                
                try
                {
                    //Загружаем XML документ
                    XDocument document = XDocument.Load(xmlReader);

                    //Обрабатываем XML документ
                    foreach (XElement element in document.Elements())
                        ProcessXmlElement_1st_Stage(element, file.Type);

                    xmlReader.Close();
                }
                catch (System.Xml.XmlException)
                {
                    xmlReader.Close();

                    //Обрабатываем обычный файл
                    if (!ProcessFile_1st_Stage(file))
                        IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось обработать файл: " + file.InPath);
                }

                IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++fileCounter);
            }
            
            //-------------------------> Обработка файлов (2-й этап) <--------------------------

            //Обнуляем счётчик обработанных файлов
            fileCounter = 0;

            //Обнуляем счётчик временных файлов, посокольку они должны нумероваться так же как и на первом этапе
            tempFileCounter = 0;

            foreach (FileToProcess file in files)
            {
                // FIXME
                //Monitor.Monitor.WriteToTempInfo = "Обработка файла:" + file.InPath;

                XmlTextReader xmlReader = new XmlTextReader(file.InPath) { Normalization = false };

                try
                {
                    //Загружаем XML документ
                    XDocument document = XDocument.Load(xmlReader);

                    //Обрабатываем XML документ
                    foreach (XElement element in document.Elements())
                        ProcessXmlElement_2nd_Stage(element, file.Type);

                    //Создаём директорию для обработанного файла
                    FileInfo fileInfo = new FileInfo(file.OutPath);
                    if (!Directory.Exists(fileInfo.Directory.FullName))
                        Directory.CreateDirectory(fileInfo.Directory.FullName);

                    //Сохраняем файл
                    document.Save(file.OutPath);

                    xmlReader.Close();
                }
                catch (System.Xml.XmlException)
                {
                    xmlReader.Close();

                    //Обрабатываем обычный файл
                    if (!ProcessFile_2nd_Stage(file))
                        IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось обработать файл: " + file.InPath);
                }

                IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)1).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++fileCounter);
            }

            ////Удаляем временные директории и всё содрежимое
            if (Directory.Exists(tempDirectoryIn))
                (new DirectoryInfo(tempDirectoryIn)).Delete(true);
            if (Directory.Exists(tempDirectoryOut))
                (new DirectoryInfo(tempDirectoryOut)).Delete(true);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        #endregion

        /// <summary>
        /// Метод отсеивает только те файлы, которые нужны для обработки
        /// </summary>
        /// <returns>Список файлов для обработки</returns>
        private List<FileToProcess> GetFilesToProcess()
        {
            List<FileToProcess> ret = new List<FileToProcess>();

            foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                //Отбираем исходные тексты на языке VisualBasic
                if(file.fileType.ContainsLanguages().Contains("VisualBasic"))
                    ret.Add(new FileToProcess(file.FullFileName_Original, Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic", file.RelativeFileName_Original), FileType.VB));

                //Отбираем исходные тексты на языке VisualBasicScript
                if(file.fileType.ContainsLanguages().Contains("VisualBasicScript"))
                    ret.Add(new FileToProcess(file.FullFileName_Original, Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasicScript", file.RelativeFileName_Original), FileType.VBScript));
            }

            return ret;
        }

        /// <summary>
        /// Функция обработки отдельного файла (1-й этап)
        /// </summary>
        /// <param name="file">Файл для обработки</param>
        private bool ProcessFile_1st_Stage(FileToProcess file)
        {
            //Разбираем файлы. 1-й этап. Занесение имён переменных и функций.
            if (!parser.Parse_1st_Stage(file.InPath))
                return false;

            return true;
        }

        /// <summary>
        /// Функция обработки отдельного файла (2-й этап)
        /// </summary>
        /// <param name="file">Файл для обработки</param>
        private bool ProcessFile_2nd_Stage(FileToProcess file)
        {
            //Разбираем файлы. 2-й этап. Занесение стейтментов и датчиков в Хранилище.
            if (!parser.Parse_2nd_Stage(file.InPath))
                return false;

            //Расстановка датчиков.
            if (PluginSettings.Level != 0)
                inserter.Run(file);

            return true;
        }

        /// <summary>
        /// Функция рекурсивной обработки XML документа (1-й этап)
        /// </summary>
        /// <param name="root">Корневой узел</param>
        private void ProcessXmlElement_1st_Stage(XElement root, FileType type)
        {
            XAttribute attribute = root.Attribute("Script");

            //Если нашли скрипт
            if (attribute != null)
            {
                //создаём временный файл для обработки
                FileToProcess tempFile = new FileToProcess(Path.Combine(tempDirectoryIn, "script_" + ++tempFileCounter), Path.Combine(tempDirectoryOut, "script_" + tempFileCounter), type);
                File.Create(tempFile.InPath).Close();

                //Добавляем вновь созданный файл в Хранилище
                IFile tempFileobj = storage.files.Add(enFileKindForAdd.fileWithPrefix);
                tempFileobj.FullFileName_Original = tempFile.InPath;

                //Открываем файл
                AbstractReader writer = new AbstractReader(tempFile.InPath);

                //Записыаем в него содержимое скрипта
                writer.writeTextToFile(tempFile.InPath, attribute.Value);

                //Обрабатываем файл
                if (!ProcessFile_1st_Stage(tempFile))
                    IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось обработать временный файл: " + tempFile.InPath);
            }

            foreach (XElement element in root.Elements())
                ProcessXmlElement_1st_Stage(element, type);
        }

        /// <summary>
        /// Функция рекурсивной обработки XML документа (2-й этап)
        /// </summary>
        /// <param name="root">Корневой узел</param>
        private void ProcessXmlElement_2nd_Stage(XElement root, FileType type)
        {
            XAttribute attribute = root.Attribute("Script");

            //Если нашли скрипт
            if (attribute != null)
            {
                //создаём временный файл для обработки
                FileToProcess tempFile = new FileToProcess(Path.Combine(tempDirectoryIn, "script_" + ++tempFileCounter), Path.Combine(tempDirectoryOut, "script_" + tempFileCounter), type);

                //Обрабатываем файл
                if (ProcessFile_2nd_Stage(tempFile))
                {
                    //Записываем обработанную информацию обрабтно в атрибут
                    attribute.Value = (new AbstractReader(tempFile.OutPath)).getText();
                }
                else
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось обработать временный файл: " + tempFile.InPath);

                    //FIXME - удалить
                    MessageBox.Show(tempFile.InPath);
                }
            }

            foreach (XElement element in root.Elements())
                ProcessXmlElement_2nd_Stage(element, type);
        }

        /// <summary>
        /// Метод импортирует в Хранилище список системных функций заданный пользователем
        /// </summary>
        private void ImportSystemFunctions()
        {
            if (string.IsNullOrWhiteSpace(PluginSettings.SystemFunctionsFilePath))
                return;

            // FIXME
            //Monitor.Monitor.AddLogMessageToWriter("Импорт списка системных функций запущен.");

            foreach (string line in (new CachedReader(PluginSettings.SystemFunctionsFilePath)).getText().Split('\n'))
            {
                if (string.IsNullOrWhiteSpace(line) || line.Trim().Contains(" ") || line.Trim().Contains("\t"))
                {
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Строка \"" + line + "\" списка системных функций проигнорирована.");
                    continue;
                }

                IFunction function = storage.functions.AddFunction();
                function.Name = line.Trim();
                function.IsExternal = true;
            }

            // FIXME
            //Monitor.Monitor.AddLogMessageToWriter("Импорт списка системных функций завершён.");
        }

        /// <summary>
        /// Метод импортирует в Хранилище список системных переменных заданный пользователем
        /// </summary>
        private void ImportSystemVariables()
        {
            if (string.IsNullOrWhiteSpace(PluginSettings.SystemVariablesFilePath))
                return;
            // FIXME
            //Monitor.Monitor.AddLogMessageToWriter("Импорт списка системных переменных запущен.");

            foreach (string line in (new CachedReader(PluginSettings.SystemVariablesFilePath)).getText().Split('\n'))
            {
                if (string.IsNullOrWhiteSpace(line) || line.Trim().Contains(" ") || line.Trim().Contains("\t"))
                {
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Строка \"" + line + "\" списка системных переменных проигнорирована.");
                    continue;
                }

                Store.Variable variable = storage.variables.AddVariable();
                variable.Name = line.Trim();
            }

            // FIXME
            //Monitor.Monitor.AddLogMessageToWriter("Импорт списка системных переменных завершён.");
        }
    }
}
