﻿/*
 * Плагин Парсер VB-VBS
 * Файл SensorInserter.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Юхтин
 * 
 * Плагин предназначен для вставки датчиков в исходные очищенные тексты по 2-ому и 3-ему уровню.
 */

using System;
using System.Collections.Generic;
using FileOperations;
using Store;
using System.IO;

namespace IA.Plugins.Parsers.VBParser
{
    class SensorInserter
    {
        /// <summary>
        /// Класс, созданный для того чтобы формировать список датчиков для дальнейшей вставки
        /// </summary>
        private class SensorToInsert : IComparable<SensorToInsert>
        {
            /// <summary>
            /// Идентификатор датчика
            /// </summary>
            internal ulong ID { get; private set; }

            /// <summary>
            /// Строка, где расположен датчик
            /// </summary>
            internal ulong Line { get; private set; }

            /// <summary>
            /// Столбец, где расположен датчик
            /// </summary>
            internal ulong Column { get; private set; }

            /// <summary>
            /// Конструктор класса
            /// </summary>
            /// <param name="sensor">Датчик в Хранилище</param>
            internal SensorToInsert(Sensor sensor)
            {
                this.ID = sensor.ID;
                this.Line = sensor.GetBeforeStatement().FirstSymbolLocation.GetLine();
                this.Column = sensor.GetBeforeStatement().FirstSymbolLocation.GetColumn();
            }

            /// <summary>
            /// Перегруженный метод для сравнения двух элементов класса друг с другом
            /// </summary>
            /// <param name="sensor">Датчик для сравнения</param>
            /// <returns> 
            /// меньше 0 - текущий датчик расположен позже переданного
            /// равно 0 - расположение датчиков совпадает
            /// больше 0 - текущий датчик расположен раньше переданного
            /// </returns>
            public int CompareTo(SensorToInsert sensor)
            {
                if (this.Line != sensor.Line)
                    return -this.Line.CompareTo(sensor.Line);

                return -this.Column.CompareTo(sensor.Column);
            }

            /// <summary>
            /// Метод для формирования строки с датчиком для вставки
            /// </summary>
            /// <returns>Строка датчика</returns>
            public override string ToString()
            {
                return PluginSettings.SensorText.Replace("#", this.ID.ToString());
            }

        }

        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="storage">Текущее Хранилище</param>
        internal SensorInserter(Storage storage)
        {
            this.storage = storage;
        }

        /// <summary>
        /// Метод вставляет датчики в файл
        /// </summary>
        /// <param name="file">Файл для обработки</param>
        internal void Run(FileToProcess file)
        {
            IFile fileObj = storage.files.Find(file.InPath);
            if (fileObj == null)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, "Файл не найден в Хранилище.");
                return;
            }

            //Считываем содержимое файла
            AbstractReader reader = new AbstractReader(file.InPath);
            string contents = reader.getText();

            //Раставляем датчики начиная с конца файла
            foreach (SensorToInsert sensor in GetSensors(fileObj))
            {
                int offset = (int)fileObj.ConvertLineColumnToOffset(sensor.Line, 1);

                if (offset != -1)
                    contents = contents.Insert(offset, sensor.ToString() + VBParser.separator);
                else
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Не удалось получить смещение датчика " + sensor.ToString() + ".");
            }

            //Добавляем функцию датчика в файл
            AddSensorFucntion(ref contents, file.Type);
            
            FileInfo outFileInfo = new FileInfo(file.OutPath);
            if (!Directory.Exists(outFileInfo.Directory.FullName))
                Directory.CreateDirectory(outFileInfo.Directory.FullName);

            //Записываем то, что получилось в новый файл
            reader.writeTextToFile(file.OutPath, contents);
        }

        /// <summary>
        /// Метод для получения списка датчиков для заданного файла.
        /// Список отсортирован для последнующей вставки. Первым в списке идёт последний датчик в файле.
        /// </summary>
        /// <param name="file">Файл, для короторого нужно найти датчики</param>
        /// <returns>Список датчиков для заданного файла</returns>
        private List<SensorToInsert> GetSensors(IFile file)
        {
            List<SensorToInsert> ret = new List<SensorToInsert>();

            foreach(Sensor sensor in storage.sensors.EnumerateSensors())
                if(sensor.GetBeforeStatement().FirstSymbolLocation.GetFileID() == file.Id)
                    ret.Add(new SensorToInsert(sensor));

            ret.Sort();

            return ret;
        }

        /// <summary>
        /// Метод добавляем в файл функцию датчика
        /// </summary>
        /// <param name="contents">Содержимое файла</param>
        /// <param name="type">Тип файла</param>
        private void AddSensorFucntion(ref string contents, FileType type)
        {
            switch (type)
            {
                case FileType.VB:
                    {
                        contents += VBParser.separator + String.Join(VBParser.separator, PluginSettings.VBSensorFunctionText.ToArray()) + VBParser.separator;
                        break;
                    }
                case FileType.VBScript:
                    {
                        int scriptIndex = contents.IndexOf("Script=\"");

                        if (scriptIndex == -1)
                            contents += VBParser.separator + String.Join(VBParser.separator, PluginSettings.VBSSensorFunctionText.ToArray()) + VBParser.separator;
                        else
                            contents = contents.Insert(scriptIndex + 8, VBParser.separator + String.Join(VBParser.separator, PluginSettings.VBSSensorFunctionText.ToArray()) + VBParser.separator);

                        break;
                    }
                default:
                    throw new Exception("Неизвестный тип файла.");
            }
        }
    }
}
