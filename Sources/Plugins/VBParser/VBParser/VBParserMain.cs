﻿/*
 * Плагин Парсер VB-VBS
 * Файл VBParserMain.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Юхтин
 * 
 * Плагин предназначен для вставки датчиков в исходные очищенные тексты по 2-ому и 3-ему уровню.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Store;
using System.IO;
using FileOperations;
using GOLD;

namespace IA.Plugins.Parsers.VBParser
{
    class VBParserMain
    {
        private class StorePosition
        {
            internal ulong Line { get; private set; }

            internal ulong Column { get; private set; }

            internal StorePosition(GOLD.Position position)
            {
                //Проверка на разумность
                if (position == null)
                    throw new Exception("Ошибка при создании экземпляра позиции. Позиция не определена.");

                this.Line = (ulong)position.Line + 1;
                this.Column = (ulong)position.Column + 1;
            }
        }
        private class TokenInfo
        {
            internal StorePosition Position { get; private set; }

            internal string Name { get; private set; }

            internal TokenInfo (StorePosition position, string name)
            {
                this.Position = position;
                this.Name = name;
            }

        }
        private class FunctionInfo
        {
            private TokenInfo info;
            private int level;

            internal TokenInfo Info
            {
                get { return info; }
            }
            internal int Level
            {
                get { return level; } 
            }

            internal FunctionInfo(TokenInfo info, int level = 0)
            {
                this.info = info;
                this.level = level;
            }
        }
        private class FunctionCallsStack
        {
            private Stack<FunctionInfo> stack;
            private bool inCall;
            private int level;

            /// <summary>
            /// Конструктор класса
            /// </summary>
            internal FunctionCallsStack()
            {
                stack = new Stack<FunctionInfo>();
                inCall = false;
                level = 0;
            }

            /// <summary>
            /// Метод выталкивает из стека элементы, уровень которых выше указанного
            /// </summary>
            /// <param name="level">Уровень в дереве разбора</param>
            /// <returns>Список вытолкнутых элементов</returns>
            internal List<TokenInfo> Pop(int level)
            {
                if (!IsPop(level))
                    return null;

                List<TokenInfo> ret = new List<TokenInfo>();
                while (stack.Count > 0 && stack.Count() != 0 && stack.Peek().Level >= level)
                    ret.Add(stack.Pop().Info);

                return ret;
            }

            /// <summary>
            /// Методо доабавления элемента в стек
            /// </summary>
            /// <param name="function">Функция, чей вызов мы хоти добавить в стек</param>
            internal void Push(FunctionInfo function)
            {
                stack.Push(function);
                InCall(function.Level);
            }

            /// <summary>
            /// Метод вызывается в тех случаях, когда нужно показать стеку, что мы находимся внутри внешнего вызова и пока ничего выталкивать нельзя
            /// </summary>
            /// <param name="level"></param>
            private void InCall(int level)
            {
                if (inCall)
                    return;
                    
                inCall = true;
                this.level = level;
            }

            /// <summary>
            /// Запрашиваем возможность вытолкнуть что-то из стека
            /// </summary>
            /// <param name="level">Текущий уровень токена</param>
            /// <returns>Можно или нельзя что-то вытолкнуть</returns>
            private bool IsPop(int level)
            {
                if (!inCall || this.level != level)
                    return false;

                inCall = false;
                return true;
            }

            /// <summary>
            /// Метод для перехода в начальное состояние (контролирует ошибки)
            /// </summary>
            internal void Clear()
            {
                if (stack.Count > 0 || inCall)
                    IA.Monitor.Log.Error(PluginSettings.Name, "Ошибка при работе со стеком");

                stack.Clear();
                inCall = false;
                level = 0;
            }
        }
        
        /// <summary>
        /// Класс, реализующий цепочку от корня дерева до текущего элемента. Ключ словаря - уровень в дереве. Значение - токен.
        /// </summary>
        private class TokenChain
        {
            /// <summary>
            /// Непосредственно сама цепочка
            /// </summary>
            private Dictionary<int, Token> chain;

            /// <summary>
            /// Конструктор класса
            /// </summary>
            internal TokenChain()
            {
                chain = new Dictionary<int, Token>();
            }

            /// <summary>
            /// Добавление токена в цепочку
            /// </summary>
            /// <param name="level">Уровень токена в дерева</param>
            /// <param name="token">Токен</param>
            internal void Add(int level, Token token)
            {
                //Проверяем все элементы до этого уже есть в цепочке
                for (int i = 1; i < level; i++)
                    if (!chain.ContainsKey(i))
                        throw new Exception("Обнаружена попытка при добавлении элемента нарушить целостность цепочки токенов.");

                chain.Add(level, token);
            }

            /// <summary>
            /// Удаление токена из цепочки
            /// </summary>
            /// <param name="level">Уровено токена</param>
            internal void Remove(int level)
            {
                //Удалять можно только последний элемент цепочки (элемент с максимальным клуючом)
                int max = 0;
                foreach (int key in chain.Keys)
                    if (key > max)
                        max = key;

                if (level != max)
                    throw new Exception("Обнаружена попытка при удалении элемента нарушить целостность цепочки токенов.");

                chain.Remove(level);
                    
            }

            /// <summary>
            /// Проверка есть ли в цепочке токен с заданным именем
            /// </summary>
            /// <param name="name">Имя токена</param>
            /// <returns>true - есть, иначе нет</returns>
            internal bool Contains(string name)
            {
                foreach (int level in chain.Keys)
                    if (chain[level].Parent.Text() == name)
                        return true;

                return false;
            }

            /// <summary>
            /// Проверка есть ли в цепочке токен с заданным именем на заданном уровне
            /// </summary>
            /// <param name="name">Имя токена</param>
            /// <param name="level">Уровень токена</param>
            /// <returns>true - есть, иначе нет</returns>
            internal bool Contains(string name, int level)
            {
                return chain[level].Parent.Text() == name;
            }

            /// <summary>
            /// Начиная с заданного уровня метод проходит вверх по цепочке в поисках токена с заданным именем.
            /// При этом игнорируются только токены с именем, указанном в 3-м параметре.
            /// </summary>
            /// <param name="name">Имя токена</param>
            /// <param name="level">Уровень токена</param>
            /// <param name="skip">Имя игнорируемого токена</param>
            /// <returns>true - есть, иначе нет</returns>
            internal bool Contains(string name, int level, string skip)
            {
                for (; level > 0; level--)
                {
                    if (chain[level].Parent.Text() == skip)
                        continue;

                    return chain[level].Parent.Text() == name;
                }

                return false;
            }

            /// <summary>
            /// Метод удаления всех элементов цепочки
            /// </summary>
            internal void Clear()
            {
                if (chain.Count != 0)
                    throw new Exception("Ошибка при формировании текущей цепочки токенов.");

                chain.Clear();
            }
        }

        /// <summary>
        /// Класс, реализующий флаг, контролирующий вставку датчиков
        /// </summary>
        private class SensorsFlag
        {
            private bool flag;
            private Stack<bool> stack;

            /// <summary>
            /// Свойство. Возвращает текущее значение флага
            /// </summary>
            internal bool Value
            {
                get
                {
                    if (PluginSettings.Level == 3)
                    {
                        if (!enter)
                            return false;

                        enter = false;
                    }

                    return flag;
                }
            }

            /// <summary>
            /// Конструктор класса
            /// </summary>
            /// <param name="flag">Начальное значение флага</param>
            internal SensorsFlag(bool flag)
            {
                this.flag = flag;
                stack = new Stack<bool>();
            }

            /// <summary>
            /// Метод опускает флаг и сохраняет его предыдущее значение
            /// </summary>
            internal void Down()
            {
                stack.Push(flag);
                flag = false;
            }

            /// <summary>
            /// Метод получает старое значение флага из стека
            /// </summary>
            internal void Raise()
            {
                if (!stack.Any())
                    throw new Exception("Ошибка при работе со стеком флага.");

                flag = stack.Pop();
            }

            //Флаг для третьего уровня контроля - создан чтобы контролировать вставку датчиков только в начале функции.
            private bool enter = false;

            /// <summary>
            /// Метод вызывается при 3-м уровене контроля, когда мы вохдим в функцию
            /// </summary>
            internal void enterFunction()
            {
                if (PluginSettings.Level != 3)
                    return;

                if(enter)
                    throw new Exception("Ошибка при работе с флагом расстановки датчиков №1.");

                enter = true;
            }

            /// <summary>
            /// Метод вызывается при 3-м уровене контроля, когда мы выходим из функции
            /// </summary>
            internal void leaveFunction()
            {
                if (PluginSettings.Level != 3)
                    return;

                if (enter)
                    throw new Exception("Ошибка при работе с флагом расстановки датчиков №2.");
            }
        }

        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Путь к файлу с грамматикой
        /// </summary>
        private string grammar = @"Grammar\VB + VBScript.egt";

        /// <summary>
        /// Объект голд парсера
        /// </summary>
        private GOLD.Parser goldParser = new GOLD.Parser();

        /// <summary>
        /// Стек для хранения вызовов функций
        /// </summary>
        private FunctionCallsStack stack = new FunctionCallsStack();

        /// <summary>
        /// Текущий файл
        /// </summary>
        private IFile currentFile = null;

        /// <summary>
        /// Текущее пространство имён
        /// </summary>
        private Namespace currentNamespace = null;

        /// <summary>
        /// Текущий класс
        /// </summary>
        private Class currentClass = null;

        /// <summary>
        /// Текущая функция
        /// </summary>
        private IFunction currentFunction = null;

        /// <summary>
        /// "Глобальная" функция - соедержит то, что находится в файле вне классов, пространств имён и т.д.
        /// </summary>
        private IFunction globalFunction = null;

        /// <summary>
        /// Счётчик глобальных функций
        /// </summary>
        private int globalFunctionsCounter = 0;

        /// <summary>
        /// Корень дерева разбора
        /// </summary>
        private Reduction root;

        /// <summary>
        /// Цепочка от корня дерева до текущего элемента. Ключ словаря - уровень в дереве. Значение - токен.
        /// </summary>
        TokenChain chain = new TokenChain();

        /// <summary>
        /// Уровень текущего токена в дереве разбора
        /// </summary>
        private int level;

        /// <summary>
        /// Список необработанных токенов
        /// </summary>
        private List<string> failList = new List<string>();

        /// <summary>
        /// Флаг, если поднят то следует вставлять датчики, иначе - вставка датчиков не осуществляется
        /// </summary>
        private SensorsFlag sensorFlag = new SensorsFlag(PluginSettings.Level != 0);

        /// <summary>
        /// Текущий номер датчика
        /// </summary>
        private ulong currentSensor = PluginSettings.FirstSensorNumber;

        /// <summary>
        /// Все типы датчиков
        /// </summary>
        private enum StatementType
        {
            Break,
            Continue,
            DoAndWhile,
            For,
            ForEach,
            Goto,
            If,
            Operational,
            Return,
            Switch,
            Throw,
            TryCatchFinally,
            Using,
            YieldReturn
        }

        /// <summary>
        /// Функция возвращает последний стейтмент в цепочки стейтментов, начиная с указанного
        /// </summary>
        /// <param name="statement">Стейтмент, с которого следует начать поиск</param>
        /// <returns>Последний стейтмент в цепочки стейтментов</returns>
        private IStatement GetLastStatement(IStatement statement)
        {
            IStatement tmp = statement;

            if (statement == null)
                return null;

            while (tmp.NextInLinearBlock != null)
                tmp = tmp.NextInLinearBlock;

            return tmp;
        }

        /// <summary>
        /// Униферсальный метод для добавления нового стейтмента
        /// </summary>
        /// <param name="entry">Цепочка стейтментов для добавления в её конец нового стейтмента</param>
        /// <param name="type">Тип нового стейтмента</param>
        /// <param name="line">строка нового стейтмента</param>
        /// <param name="column">столбец нового стейтмента</param>
        /// <returns>Новый стейтмент заданного типа</returns>
        private IStatement GetNewStatement(ref IStatement entry, StatementType type, ulong line, ulong column)
        {
            IStatement statement = null;

            switch(type)
            {
                case StatementType.Break:
                    statement = storage.statements.AddStatementBreak(line, column, currentFile);
                    break;
                case StatementType.Continue:
                    statement = storage.statements.AddStatementContinue(line, column, currentFile);
                    break;
                case StatementType.DoAndWhile:
                    statement = storage.statements.AddStatementDoAndWhile(line, column, currentFile);
                    break;
                case StatementType.For:
                    statement = storage.statements.AddStatementFor(line, column, currentFile);
                    break;
                case StatementType.ForEach:
                    statement = storage.statements.AddStatementForEach(line, column, currentFile);
                    break;
                case StatementType.Goto:
                    statement = storage.statements.AddStatementGoto(line, column, currentFile);
                    break;
                case StatementType.If:
                    statement = storage.statements.AddStatementIf(line, column, currentFile);
                    break;
                case StatementType.Operational:
                    statement = storage.statements.AddStatementOperational(line, column, currentFile);
                    break;
                case StatementType.Return:
                    statement = storage.statements.AddStatementReturn(line, column, currentFile);
                    break;
                case StatementType.Switch:
                    statement = storage.statements.AddStatementSwitch(line, column, currentFile);
                    break;
                case StatementType.Throw:
                    statement = storage.statements.AddStatementThrow(line, column, currentFile);
                    break;
                case StatementType.TryCatchFinally:
                    statement = storage.statements.AddStatementTryCatchFinally(line, column, currentFile);
                    break;
                case StatementType.Using:
                    statement = storage.statements.AddStatementUsing(line, column, currentFile);
                    break;
                case StatementType.YieldReturn:
                    statement = storage.statements.AddStatementYieldReturn(line, column, currentFile);
                    break;
                default:
                    throw new Exception("Неправильно передан тип датчика");
            }
            
            IStatement last = GetLastStatement(entry);
            
            if (last == null)
            {
                entry = last = statement;
                                            
                if(sensorFlag.Value)
                    storage.sensors.AddSensorBeforeStatement(currentSensor++, entry, Kind.START);
            }
            else
            {
                last = last.NextInLinearBlock = statement;
                
                if(sensorFlag.Value)
                    storage.sensors.AddSensorBeforeStatement(currentSensor++, last, Kind.INTERNAL);
            }

            return statement;
        }

        /// <summary>
        /// Метод возращает пустой стейтмент-оперэшнл со вставленным датчиком
        /// Иногда внутри токена нет ничего интересного, однако датчик в этом месте всё равно нужен.
        /// </summary>
        /// <param name="position">координаты стейтмента</param>
        /// <returns>Пустой стейтмент с датчиком</returns>
        private IStatementOperational GetEmptyStatement(StorePosition position)
        {
            IStatementOperational statement = position == null ? 
                                                storage.statements.AddStatementOperational() :
                                                storage.statements.AddStatementOperational((ulong)position.Line, (ulong)position.Column, currentFile);

            if (position != null && sensorFlag.Value)
                storage.sensors.AddSensorBeforeStatement(currentSensor++, statement, Kind.START);

            return statement;
        }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="storage">Текущее Хранилище</param>
        internal VBParserMain(Storage storage) 
        {
            this.storage = storage;

            //Выгрузка таблиц из грамматики
            goldParser.LoadTables(grammar);

            //Перед тем как включать ЭТО разработчики рекомендуют прочитать иснтрукцию
            goldParser.TrimReductions = false;
        }

        /// <summary>
        /// Метод разбора файла (1-й этап)
        /// </summary>
        /// <param name="file">Файл для обработки</param>
        internal bool Parse_1st_Stage(string file)
        {
            //Задаём значение текущего файла
            if ((currentFile = storage.files.Find(file)) == null)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, "Файл " + file + " не найден в Хранилище.");
                return false;
            }

            //Задаём значение текущей функции
            currentFunction = globalFunction;

            //Обнуляем уровень
            level = 0;

            //Обнуление текущей цепочки
            chain.Clear();

            //Получаем дерево разбора
            if (!GetTree(file))
                return false;

            //Обходим дерево
            TreeWalker_1st_Stage(root);

            return true;
        }

        /// <summary>
        /// Метод разбора файла (2-й этап)
        /// </summary>
        /// <param name="file">Файл для обработки</param>
        internal bool Parse_2nd_Stage(string file)
        {
            if ((currentFile = storage.files.Find(file)) == null)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, "Файл " + file + " не найден в Хранилище.");
                return false;
            }

            //Получаем дерево разбора
            if (!GetTree(file))
                return false;
            
            //Добавляем в Хранилище "глобальную" функцию для хранения всего того, что лежит напрямую в файле
            globalFunction = storage.functions.AddFunction();
            globalFunction.Name = "Global_RNT_Function_" + ++globalFunctionsCounter;
            globalFunction.AddDefinition(currentFile, 0);

            //Обнуляем уровень
            level = 0;

            //Очистка стека
            stack.Clear();

            //Обнуление текущей цепочки
            chain.Clear();

            //Очищаем список необработанных токенов
            failList.Clear();

            //Обходим дерево
            TreeWalker_2nd_Stage(root);

            //Выводим необработанные токены на экран
            // FIXME
            //foreach (string fail in failList)
            //    Monitor.Monitor.AddLogMessageToWriter("Не обработан токен: " + fail);

            return true;
        }

        /// <summary>
        /// Функция создания дерева разбора для заданного файла
        /// </summary>
        /// <param name="file">Файл для обработки</param>
        /// <returns>true - разбор файла прошёл успешно, false - иначе</returns>
        private bool GetTree(string file)
        {
            //Считываем содержимое файла
            string contents = new CachedReader(file).getText();

            //Принудительно добавляем перевод строки. Это нужно для правильного парсинга.
            contents += VBParser.separator;

            //Открываем файл на чтение и готовимся к разбору
            goldParser.Open(new StringReader(contents));

            //Крутимся в бесконечном цикле пока не закончим удачно либо с ошибкой
            while (true)
            {
                switch (goldParser.Parse())
                {
                    case GOLD.ParseMessage.LexicalError:
                        IA.Monitor.Log.Error(PluginSettings.Name, "Lexical Error: " + 
                                                "File:" + file + " " + 
                                                "Position: " + goldParser.CurrentPosition().Line + ", " + goldParser.CurrentPosition().Column + " " + 
                                                "Read: " + goldParser.CurrentToken().Data);
                        return false;

                    case GOLD.ParseMessage.SyntaxError:
                        IA.Monitor.Log.Error(PluginSettings.Name, "Syntax Error: " +
                                                "File:" + file + " " +
                                                "Position: " + goldParser.CurrentPosition().Line + ", " + goldParser.CurrentPosition().Column + " " +
                                                "Read: " + goldParser.CurrentToken().Data + " " +
                                                "Expecting: " + goldParser.ExpectedSymbols().Text());
                        return false;

                    case GOLD.ParseMessage.Reduction:
                        break;

                    case GOLD.ParseMessage.Accept:
                        root = (Reduction)goldParser.CurrentReduction;                          
                        return true;

                    case GOLD.ParseMessage.TokenRead:
                        break;

                    case GOLD.ParseMessage.InternalError:
                        return false;

                    case GOLD.ParseMessage.NotLoadedError:
                        IA.Monitor.Log.Error(PluginSettings.Name, "File:" + file + " " + 
                                                "Parsing Error : Tables not loaded");
                        return false;

                    case GOLD.ParseMessage.GroupError:
                        IA.Monitor.Log.Error(PluginSettings.Name, "File:" + file + " " + 
                                                "Runaway group");
                        return false;
                }
            }
        }

        /// <summary>
        /// В случае, если какой-то токен нас не интересует вызывается данный метод для продолжения обхода дерева с сохранением цепочки стейтментов
        /// </summary>
        /// <param name="reduction">Вершина дерева, откуда следует начать разбор</param>
        /// <param name="entry">Цепочка стейтментов до обхода</param>
        private void WalkAhead(Reduction reduction, ref IStatement entry)
        {
            IStatement statement = TreeWalker_2nd_Stage(reduction);

            if (statement == null)
                return;

            IStatement last = GetLastStatement(entry);
            if (last == null)
                entry = last = statement;
            else
            {
                //Если предыдущий стейтмент был StatementOperational то добавляем содержимое нового в него
                if (statement is IStatementOperational && last is IStatementOperational)
                    foreach(IFunction[] mass in ((IStatementOperational)statement).Operation.SequentiallyCallsFunctions())
                        ((IStatementOperational)last).Operation.AddFunctionCallInSequenceOrder(mass);
                else
                    last = last.NextInLinearBlock = statement;
            }

        }

        /// <summary>
        /// Метод для рекурсивного обхода дерева (1-й этап)
        /// </summary>
        /// <param name="root_reduction">Корневой элемент для начала обхода</param>
        private void TreeWalker_1st_Stage(Reduction root_reduction)
        {
            //С каждым новым вызовом увеличиваем уровень
            level++;

            for (int i = 0; i < root_reduction.Count(); i++)
            {
                Token token = root_reduction[i];

                //Добаляем токен в текущую цепочку
                chain.Add(level, token);

                switch (token.Type())
                {
                    case SymbolType.Nonterminal:
                        {
                            Reduction reduction = (Reduction)token.Data;
                            Production production = reduction.Parent;

                            switch (token.Parent.Text())
                            {
                                #region Namespace Decl
                                case "<NamespaceDecl>":
                                    {
                                        //Запоминаем пространство имён, в котором мы находимся
                                        Namespace parentNamespace = currentNamespace;

                                        //Заносим найденное пространство имён в Хранилище
                                        TokenInfo info = Get_QualifiedID_Info(reduction[1]);
                                        Namespace @namespace = (parentNamespace != null) ? 
                                                                parentNamespace.FindOrCreateSubNamespace(info.Name.Split('.')) : 
                                                                storage.namespaces.FindOrCreate(info.Name.Split('.'));

                                        //Делаем найденное пространство имён текущим
                                        currentNamespace = @namespace;

                                        //Продолжаем обработку
                                        TreeWalker_1st_Stage((Reduction)reduction[3].Data);

                                        //Выходя из пространства имён вспоминам где были раньше
                                        currentNamespace = parentNamespace;

                                        break;
                                    }
                                #endregion
                                #region Class Decl
                                case "<ClassDecl>":
                                    {
                                        //Запоминаем класс, в котором мы находимся
                                        Class parentClass = currentClass;

                                        //Заносим найденный класс в Хранилище
                                        TokenInfo info = Get_ExtendedID_Info(reduction[2]);
                                        Class @class = storage.classes.AddClass();
                                        @class.Name = info.Name;

                                        //Если имеет место вложение, обрабатываем его
                                        if (parentClass != null)
                                            parentClass.AddInnerClass(@class);

                                        //Если имеет место наследование обрабатываем его
                                        if (((Reduction)reduction[5].Data).Parent.Text() != "<InheritsListOpt> ::= ")
                                            foreach (string parentName in Process_QualifiedIDList(((Reduction)reduction[5].Data)[1]))
                                            {
                                                //Ищем наследуемый класс в Храниилще
                                                Class[] parents = storage.classes.FindClass(parentName.Split('.'));

                                                if (parents.Length == 0)
                                                {
                                                    Class parent = storage.classes.AddClass();
                                                    parent.Name = parentName;
                                                    @class.AddParent(parent);
                                                    IA.Monitor.Log.Error(PluginSettings.Name, "Класс " + parentName + " принудительно добавлен в Хранилище");
                                                }
                                                else
                                                    foreach (Class parent in parents)
                                                        @class.AddParent(parent); //FIXME - пока мы добавлям в наследованные классы всё, что находим
                                            }

                                        //Добавляем класс в текущее пространство имён
                                        if (currentNamespace != null)
                                            currentNamespace.AddClassToNamespace(@class);

                                        //Делаем найденный класс текущим
                                        currentClass = @class;

                                        //Продолжаем обработку
                                        TreeWalker_1st_Stage((Reduction)reduction[7].Data);

                                        //Выходя из класса вспоминам где были раньше
                                        currentClass = parentClass;

                                        break;
                                    }
                                #endregion
                                #region Module Decl
                                case "<ModuleDecl>":
                                    {
                                        if (currentClass != null)
                                            throw new Exception("Найдено объявление модуля внутри класса. Модуль может быть объявлен в файле или в пространстве имён.");

                                        //Заносим найденный класс в Хранилище
                                        TokenInfo info = Get_ExtendedID_Info(reduction[2]);
                                        Class @class = storage.classes.AddClass();
                                        @class.Name = info.Name;

                                        //Добавляем класс в текущее пространство имён
                                        if (currentNamespace != null)
                                            currentNamespace.AddClassToNamespace(@class);

                                        //Делаем найденный класс текущим
                                        currentClass = @class;

                                        //Продолжаем обработку
                                        TreeWalker_1st_Stage((Reduction)reduction[4].Data);
                                        
                                        //Выходя из класса вспоминам где были раньше
                                        currentClass = null;

                                        break;
                                    }
                                #endregion
                                #region Property Decl
                                case "<PropertyDecl>":
                                    {
                                        //Получаем имя свойства
                                        Token propertyNameToken = Get_SubToken_By_Name(token, "<ExtendedID>");
                                        if(propertyNameToken == null)
                                            throw new Exception("Ошибка при разборе токена <PropertyDecl>");

                                        TokenInfo info = Get_ExtendedID_Info(propertyNameToken);

                                        //Ищем функции set и get
                                        Token propertyFunctionsOpt_token = Get_SubToken_By_Name(token, "<PropertyFunctionsOpt>");
                                        if(propertyFunctionsOpt_token == null)
                                            throw new Exception("Ошибка при разборе токена <PropertyDecl>");

                                        Reduction propertyFunctionsOpt_reduction = (Reduction)propertyFunctionsOpt_token.Data;
                                        switch (propertyFunctionsOpt_reduction.Parent.Text())
                                        {
                                            case "<PropertyFunctionsOpt> ::= <AccessModifierOpt> <GetOrSetFunction> <AccessModifierOpt> <GetOrSetFunction>":
                                                Process_GetOrSetFunction(info.Name, propertyFunctionsOpt_reduction[1]);
                                                Process_GetOrSetFunction(info.Name, propertyFunctionsOpt_reduction[3]);
                                                break;
                                            case "<PropertyFunctionsOpt> ::= <AccessModifierOpt> <GetOrSetFunction>":
                                                Process_GetOrSetFunction(info.Name, propertyFunctionsOpt_reduction[1]);
                                                break;
                                            case "<PropertyFunctionsOpt> ::= ":
                                                break;
                                            default:
                                                throw new Exception("Ошибка при разборе токена <PropertyFunctionsOpt>");
                                        }


                                        break;
                                    }
                                #endregion
                                #region Var/Field Decl
                                //Объявление переменной
                                case "<VarName>":
                                    {
                                        //Field
                                        if (chain.Contains("<FieldDecl>", level - 2, "<OtherVarsOpt>"))
                                        {
                                            //Заносим найденное поле в Хранилище
                                            TokenInfo info = Get_ExtendedID_Info(reduction[0]);
                                            if (currentClass != null)
                                            {
                                                Field field = currentClass.AddField(false);
                                                field.Name = info.Name;
                                                field.AddDefinition(currentFile, (ulong)info.Position.Line, (ulong)info.Position.Column, null);
                                            }
                                            else
                                            {
                                                Variable field = storage.variables.AddVariable();
                                                field.Name = info.Name;
                                                field.AddDefinition(currentFile, (ulong)info.Position.Line, (ulong)info.Position.Column, globalFunction);
                                            }
                                        }
                                        //Variable
                                        else if (chain.Contains("<VarDecl>", level - 1, "<OtherVarsOpt>"))
                                        {
                                            if (currentFunction == globalFunction)
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Найдено объявление переменной вне функции.");
                                            else
                                            {
                                                //Заносим найденную переменную в Хранилище
                                                TokenInfo info = Get_ExtendedID_Info(reduction[0]);
                                                Variable variable = storage.variables.AddVariable();
                                                variable.Name = info.Name;
                                                variable.AddDefinition(currentFile, (ulong)info.Position.Line, (ulong)info.Position.Column, currentFunction);
                                            }
                                        }
                                        else
                                            throw new Exception("Ошибка при обработке токена <VarName>");

                                        break;
                                    }
                                case "<FieldName>":
                                    {
                                        //Заносим найденное поле в Хранилище
                                        TokenInfo info = Get_FieldID_Info(reduction[0]);

                                        if (currentClass != null)
                                        {
                                            Field field = currentClass.AddField(false);
                                            field.Name = info.Name;
                                            field.AddDefinition(currentFile, (ulong)info.Position.Line, (ulong)info.Position.Column, null);
                                        }
                                        else
                                        {
                                            Variable field = storage.variables.AddVariable();
                                            field.Name = info.Name;
                                            field.AddDefinition(currentFile, (ulong)info.Position.Line, (ulong)info.Position.Column, globalFunction);
                                        }

                                        break;
                                    }
                                #endregion
                                #region Function/Sub Decl
                                //Объявление функций и процедур
                                case "<SubDecl>":
                                case "<FunctionDecl>":
                                    {
                                        //Добавляем найденную функцию/метод в Хранилище
                                        TokenInfo info = Get_ExtendedID_Info(reduction[2]);

                                        IFunction function;
                                        if (currentClass != null)
                                            function = currentClass.AddMethod(false);
                                        else
                                            function = storage.functions.AddFunction();
                                        
                                        function.Name = info.Name;

                                        // FIXME - line != 0
                                        function.AddDefinition(currentFile, (ulong)info.Position.Line, (ulong)info.Position.Column);

                                        //Ищем нужный нам токен, описывающий содержание функции
                                        Token body_token = Get_SubToken_By_Name(token, "<MethodStmtList>");
                                        if (body_token == null)
                                            body_token = Get_SubToken_By_Name(token, "<InlineStmt>");
                                        if (body_token == null)
                                            throw new Exception("Ошибка разбора правила");

                                        //Обрабатываем содержимое функции
                                        currentFunction = function;
                                        TreeWalker_1st_Stage((Reduction)body_token.Data);
                                        currentFunction = globalFunction;

                                        break;
                                    }
                                #endregion
                                default:
                                    {
                                        TreeWalker_1st_Stage(reduction);
                                        break;
                                    }
                            }
                        }
                        break;
                    default:
                        break;
                }

                //Удаляем токен из текущей цепочки
                chain.Remove(level);
            }

            //При каждом выходе уменьшаем уровень
            level--;
        }

        /// <summary>
        /// Метод для рекурсивного обхода дерева (2-й этап)
        /// </summary>
        /// <param name="root_reduction">Корневой элемент для начала обхода</param>
        /// <param name="isNoSensorsSubTree">Флаг указывает находимся ли мы в поддереве, в котором не требуется вставлять датчики</param>
        /// <returns>Цепочка стейтментов полученная из дерева разбора</returns>
        private IStatement TreeWalker_2nd_Stage(Reduction root_reduction, bool isNoSensorsSubTree = false)
        {
            IStatement entry = null;

            //С каждым новым вызовом увеличиваем уровень
            level++;

            //Если в на данном этапе датчики не нужны (например, мы находимся в условии конструкции "IF") опускаем флаг
            if (isNoSensorsSubTree)
                sensorFlag.Down();

            for(int i = 0; i < root_reduction.Count(); i++)
            {
                Token token = root_reduction[i];

                //Добаляем токен в текущую цепочку
                chain.Add(level, token);

                switch(token.Type())
                {
                    case SymbolType.Nonterminal:
                        {
                            Reduction reduction = (Reduction)token.Data;
                            Production production = reduction.Parent;

                            switch (token.Parent.Text())
                            {
                                #region Function/Sub Decl
                                //Объявление функций и процедур
                                case "<SubDecl>":
                                case "<FunctionDecl>":
                                    {
                                        //Ищем нужный нам токен, описывающий содержание функции
                                        Token body_token = Get_SubToken_By_Name(token, "<MethodStmtList>");
                                        if (body_token == null)
                                            body_token = Get_SubToken_By_Name(token, "<InlineStmt>");
                                        if (body_token == null)
                                            throw new Exception("Ошибка разбора правила");

                                        //Ищем функцию, занесённую на первом проходе в Хранилище
                                        TokenInfo info = Get_ExtendedID_Info(reduction[2]);
                                        IFunction function = GetFunctionFromStorageByCurrentFile(info.Name);
                                        if (function == null)
                                        {
                                            IA.Monitor.Log.Warning(PluginSettings.Name, "Критическая ошибка: Функция " + info.Name + " не найдена в Хранилище." + "Ошибка на втором проходе.");
                                            break;
                                        }
                                        
                                        //Приписываем найденной функции цепочку стейтментов
                                        sensorFlag.enterFunction();
                                        IStatement entryStatement = TreeWalker_2nd_Stage((Reduction)body_token.Data);
                                        function.EntryStatement = entryStatement ?? GetEmptyStatement(Find_Token_Position(reduction, body_token));
                                        sensorFlag.leaveFunction();

                                        break;
                                    }
                                #endregion
                                #region Function/Sub Call
                                //Вызов функции
                                case "<SubCallStmt>":
                                    {
                                        TokenInfo info = Get_QualifiedID_Info(reduction[0]);
                                       
                                        //Добавляем функцию в стек
                                        stack.Push(new FunctionInfo(info, level));

                                        //Обходим дерево дальше
                                        WalkAhead(reduction, ref entry);

                                        break;
                                    }
                                case "<LeftExpr>":
                                    {
                                        //Отсекаем случаи когда <LeftExpr> - это не вызов
                                        if (chain.Contains("<AssignStmt>", level - 1)) // || chain.Contains("<SubSafeValue>", level - 1) || chain.Contains("<Value>", level - 1))
                                            break;

                                        TokenInfo info = null;

                                        switch (production.Text())
                                        {
                                            case "<LeftExpr> ::= <QualifiedID> <IndexOrParamsList> '.' <LeftExprTail>":
                                            case "<LeftExpr> ::= <QualifiedID> <IndexOrParamsListDot> <LeftExprTail>":
                                            case "<LeftExpr> ::= <QualifiedID> <IndexOrParamsList>":
                                            case "<LeftExpr> ::= <QualifiedID>":
                                                {
                                                    info = Get_QualifiedID_Info(reduction[0]);
                                                    break;
                                                }
                                            case "<LeftExpr> ::= <SafeKeywordID>":
                                                {
                                                    info = Get_SafeKeywordID_Info(reduction[0]);
                                                    break;
                                                }
                                            case "<LeftExpr> ::= <ComplexType>":
                                                break;
                                            default:
                                                throw new Exception("Ошибка в описании токена <LeftExpr>");
                                        }

                                        //Добавляем функцию в стек
                                        if (info != null)
                                            //Добавляем функцию в стек
                                            stack.Push(new FunctionInfo(info, level));

                                        //Обходим дерево дальше
                                        WalkAhead(reduction, ref entry);

                                        break;
                                    }
                                #endregion
                                #region If/Else
                                case "<IfStmt>":
                                    {
                                        StorePosition position = Find_Terminal_Position(token, "If");
                                        IStatementIf statement = (IStatementIf) GetNewStatement(ref entry, StatementType.If, (ulong)position.Line, (ulong)position.Column);

                                        //If Condition
                                        IStatement condition = TreeWalker_2nd_Stage((Reduction)reduction[1].Data, true);
                                        if (condition != null && condition is IStatementOperational)
                                            ((IStatementIf)statement).Condition = ((IStatementOperational)condition).Operation;

                                        //Ищем нужный нам токен, описывающий then блок
                                        Token then_token = null;
                                        // 'If' <Expr> 'Then' <NL> <BlockStmtList> <ElseStmtList> 'End' 'If' <NL>
                                        if ((then_token = Get_SubToken_By_Name(token, "<BlockStmtList>")) != null) 
                                        {
                                            //Then Block
                                            IStatement then_block = TreeWalker_2nd_Stage((Reduction)then_token.Data);
                                            statement.ThenStatement = then_block ?? GetEmptyStatement(Find_Token_Position(reduction, then_token));
                                            
                                            //Else Block
                                            IStatement else_block = Process_ElseStmtList(reduction[5]);
                                            statement.ElseStatement = else_block ?? GetEmptyStatement(Find_Token_Position(reduction, 5));
                                        }
                                        //'If' <Expr> 'Then' <InlineStmt> <ElseOpt> <EndIfOpt> <NL>
                                        else if ((then_token = Get_SubToken_By_Name(token, "<InlineStmt>")) != null)
                                        {
                                            //Then Block
                                            IStatement then_block = TreeWalker_2nd_Stage((Reduction)then_token.Data);
                                            statement.ThenStatement = then_block ?? GetEmptyStatement(Find_Token_Position(reduction, then_token));

                                            //Else Block
                                            IStatement else_block = TreeWalker_2nd_Stage((Reduction)reduction[4].Data);
                                            statement.ElseStatement = else_block ?? GetEmptyStatement(Find_Token_Position(reduction, 4));
                                        }
                                        else
                                            throw new Exception("Ошибка разбора правила");

                                        break;
                                    }
                                #endregion
                                #region For/Foreach
                                case "<ForStmt>":
                                    {

                                        //Ищем позицию
                                        StorePosition position = Find_Terminal_Position(token, "For");

                                        if (production.Text().Contains("Each")) //Foreach
                                        {
                                            IStatementForEach statement = (IStatementForEach)GetNewStatement(ref entry, StatementType.ForEach, (ulong)position.Line, (ulong)position.Column);

                                            //Head
                                            IStatement head = TreeWalker_2nd_Stage((Reduction)reduction[2].Data, true);
                                            if (head != null)
                                                statement.Head = ((IStatementOperational)head).Operation;

                                            //Body
                                            IStatement body = TreeWalker_2nd_Stage((Reduction)reduction[4].Data);
                                            statement.Body = body ?? GetEmptyStatement(Find_Token_Position(reduction, 4));
                                        }
                                        else //For
                                        {
                                            IStatementFor statement = (IStatementFor)GetNewStatement(ref entry, StatementType.For, (ulong)position.Line, (ulong)position.Column);

                                            //Start
                                            IStatement start = TreeWalker_2nd_Stage((Reduction)reduction[1].Data, true);
                                            if (start != null)
                                                statement.Start = ((IStatementOperational)start).Operation;

                                            //Condition
                                            IStatement cond = TreeWalker_2nd_Stage((Reduction)reduction[3].Data, true);
                                            if (cond != null)
                                                statement.Condition = ((IStatementOperational)cond).Operation;

                                            //Iteration
                                            IStatement iter = TreeWalker_2nd_Stage((Reduction)reduction[4].Data, true);
                                            if (iter != null)
                                                statement.Iteration = ((IStatementOperational)iter).Operation;

                                            //Body
                                            IStatement body = TreeWalker_2nd_Stage((Reduction)reduction[6].Data);
                                            statement.Body = body ?? GetEmptyStatement(Find_Token_Position(reduction, 6));
                                        }
                                        break;
                                    }
                                #endregion
                                #region Do/While
                                case "<LoopStmt>":
                                    {
                                        //Ищем позицию цикла
                                        StorePosition position;
                                        if ((position = Find_Terminal_Position(token, "Do")) == null)
                                            if((position = Find_Terminal_Position(token, "While")) == null)
                                                throw new Exception("Найдено необрабатываемое правило для токена <LoopStmt>");

                                        //Заводим стейтмент
                                        IStatementDoAndWhile statement = (IStatementDoAndWhile)GetNewStatement(ref entry, StatementType.DoAndWhile, (ulong)position.Line, (ulong)position.Column);

                                        Token condition_token = Get_SubToken_By_Name(token, "<Expr>");
                                        Token body_token = Get_SubToken_By_Name(token, "<BlockStmtList>");

                                        if (condition_token != null)
                                        {
                                            IStatement condition = TreeWalker_2nd_Stage((Reduction)condition_token.Data, true);
                                            if (condition != null && condition is IStatementOperational)
                                                statement.Condition = ((IStatementOperational)condition).Operation;
                                        }

                                        if (body_token != null)
                                        {
                                            IStatement body = TreeWalker_2nd_Stage((Reduction)body_token.Data);
                                            statement.Body = body ?? GetEmptyStatement(Find_Token_Position(reduction, body_token));
                                        }

                                        break;
                                    }
                                #endregion
                                #region Continue
                                case "<ContinueStmt>":
                                    {
                                        StorePosition position = Find_Terminal_Position(token, "Continue");

                                        IStatementContinue statement = (IStatementContinue)GetNewStatement(ref entry, StatementType.Continue, (ulong)position.Line, (ulong)position.Column);

                                        IStatement continuingLoop = TreeWalker_2nd_Stage(reduction, true);
                                        if (continuingLoop != null)
                                            statement.ContinuingLoop = continuingLoop;

                                        break;
                                    }
                                #endregion
                                #region Break
                                case "<BreakStmt>":
                                    {
                                        StorePosition position = Find_Terminal_Position(token, "Exit");

                                        IStatementBreak statement = (IStatementBreak)GetNewStatement(ref entry, StatementType.Break, (ulong)position.Line, (ulong)position.Column);

                                        IStatement breakingLoop = TreeWalker_2nd_Stage(reduction, true);
                                        if (breakingLoop != null)
                                            statement.BreakingLoop = breakingLoop;

                                        break;
                                    }
                                #endregion
                                #region Select
                                case "<SelectStmt>":
                                    {
                                        StorePosition position = Find_Terminal_Position(token, "Select");

                                        IStatementSwitch statement = (IStatementSwitch) GetNewStatement(ref entry, StatementType.Switch, (ulong)position.Line, (ulong)position.Column);

                                        //Condition
                                        IStatement condition = TreeWalker_2nd_Stage((Reduction)reduction[2].Data, true);
                                        if (condition != null)
                                            statement.Condition = ((IStatementOperational)condition).Operation;

                                        //Перед набором кейсов могут идти объявления переменных, обрабатываем их - FIXME (пока не точно)
                                        TreeWalker_2nd_Stage((Reduction)reduction[4].Data, true);

                                        //Cases & Default
                                        Process_CaseStmtList(reduction[5], ref statement);

                                        break;
                                    }
                                #endregion
                                #region TryCatchFinally
                                case "<TryStmt>":
                                    {
                                        StorePosition position = Find_Terminal_Position(token, "Try");

                                        IStatementTryCatchFinally statement = (IStatementTryCatchFinally)GetNewStatement(ref entry, StatementType.TryCatchFinally, (ulong)position.Line, (ulong)position.Column);

                                        //Try
                                        IStatement try_block = TreeWalker_2nd_Stage((Reduction)reduction[2].Data);
                                        statement.TryBlock = try_block ?? GetEmptyStatement(Find_Token_Position(reduction, 2));

                                        //Catches
                                        Process_CatchStmtList(reduction[3], ref statement);

                                        //Finally
                                        Token finally_token = reduction[4];
                                        Reduction finally_reduction = (Reduction)finally_token.Data;

                                        Token finally_block_token = Get_SubToken_By_Name(finally_token, "<BlockStmtList>");
                                        if(finally_block_token == null)
                                            break;

                                        IStatement finally_block = TreeWalker_2nd_Stage((Reduction)finally_block_token.Data);
                                        statement.FinallyBlock = finally_block ?? GetEmptyStatement(Find_Token_Position(finally_reduction, finally_block_token));

                                        break;
                                    }
                                #endregion
                                #region Throw
                                case "<ThrowStmt>":
                                    {
                                        StorePosition position = Find_Terminal_Position(token, "Throw");

                                        IStatementThrow statement = (IStatementThrow)GetNewStatement(ref entry, StatementType.Throw, (ulong)position.Line, (ulong)position.Column);

                                        IStatement exception = TreeWalker_2nd_Stage((Reduction)reduction[1].Data, true);
                                        if (exception != null && exception is IStatementOperational)
                                            statement.Exception = ((IStatementOperational)exception).Operation;

                                        break;
                                    }
                                #endregion
                                #region Goto
                                case "<GotoStmt>":
                                    {
                                        StorePosition position = Find_Terminal_Position(token, "GoTo");

                                        IStatementGoto statement = (IStatementGoto)GetNewStatement(ref entry, StatementType.Goto, (ulong)position.Line, (ulong)position.Column);

                                        IStatement destination = TreeWalker_2nd_Stage(reduction, true);
                                        if (destination != null)
                                            statement.Destination = destination;

                                        break;
                                    }
                                case "<ErrorStmt>":
                                    {
                                        Token goto_token = Get_SubToken_By_Name(token, "<GotoStmt>");
                                        if (goto_token == null)
                                            break;

                                        StorePosition position = Find_Terminal_Position(token, "On");

                                        IStatementGoto statement = (IStatementGoto)GetNewStatement(ref entry, StatementType.Goto, (ulong)position.Line, (ulong)position.Column);

                                        IStatement destination = TreeWalker_2nd_Stage((Reduction)goto_token.Data);
                                        if (destination != null)
                                            statement.Destination = destination;

                                        break;
                                    }
                                #endregion
                                #region Using
                                case "<UsingStmt>":
                                    {
                                        StorePosition position = Find_Terminal_Position(token, "Using");

                                        IStatementUsing statement = (IStatementUsing)GetNewStatement(ref entry, StatementType.Using, (ulong)position.Line, (ulong)position.Column);

                                        //Head
                                        IStatement head = TreeWalker_2nd_Stage((Reduction)reduction[1].Data, true);
                                        if (head != null && head is IStatementOperational)
                                            statement.Head = ((IStatementOperational)head).Operation;
                                        
                                        //Body
                                        IStatement body = TreeWalker_2nd_Stage((Reduction)reduction[3].Data);
                                        statement.Body = body ?? GetEmptyStatement(Find_Token_Position(reduction, 3));

                                        break;
                                    }
                                #endregion
                                #region Return
                                case "<ReturnStmt>":
                                    {
                                        StorePosition position = Find_Terminal_Position(token, "Return");
                                        IStatementReturn statement = (IStatementReturn)GetNewStatement(ref entry, StatementType.Return, (ulong)position.Line, (ulong)position.Column);

                                        //Мы обрабатываем такие конструкции, но выдаём предупреждение (в теории такого быть не должно)
                                        Token expr_token = Get_SubToken_By_Name(token, "<Expr>");
                                        if(expr_token != null)
                                        {
                                            IA.Monitor.Log.Warning(PluginSettings.Name, "Внимание! Обнаружена конструкция Return, возвращающая значение. Файл: " + currentFile.FullFileName_Original + " (" + position.Line + "," + (position.Column) + ")");

                                            IStatement returnExpr = TreeWalker_2nd_Stage((Reduction)expr_token.Data, true);
                                            if (returnExpr != null && returnExpr is IStatementOperational)
                                                statement.ReturnOperation = ((IStatementOperational)returnExpr).Operation;
                                        }

                                        break;
                                    }
                                #endregion
                                #region Shit & Go
                                //Следующие токены нас не интересуют, но мы продолжаем обработку дерева дальше
                                case "<AddExpr>":
                                case "<AndAlsoExpr>":
                                case "<AndExpr>":
                                case "<Arg>":
                                case "<ArgList>":
                                case "<ArgOptional>":
                                case "<AssignStmt>":
                                case "<AssignmentList>":
                                case "<BlockStmt>":
                                case "<BlockStmtList>":
                                case "<CallStmt>":
                                case "<CaseStmtList>":
                                case "<ClassBefore>":
                                case "<ClassDecl>":
                                case "<CommaExprList>":
                                case "<CompareExpr>":
                                case "<ConcatExpr>":
                                case "<DeclareStmt>":
                                case "<ElseOpt>":
                                case "<ElseSharpStmtList>":
                                case "<ElseStmtList>":
                                case "<EqvExpr>":
                                case "<ExpExpr>":
                                case "<Expr>":
                                case "<ExprList>":
                                case "<ForStart>":
                                case "<ForHead>":
                                case "<GetOrSetFunction>":
                                case "<GlobalStmt>":
                                case "<GlobalStmtList>":
                                case "<ID_CommasList>":
                                case "<IfSharpStmt>":
                                case "<ImpExpr>":
                                case "<ImplementsListOpt>":
                                case "<IndexOrParams>":
                                case "<IndexOrParamsDash>":
                                case "<IndexOrParamsDot>":
                                case "<IndexOrParamsList>":
                                case "<IndexOrParamsListDash>":
                                case "<IndexOrParamsListDot>":
                                case "<InheritsListOpt>":
                                case "<InlineStmt>":
                                case "<IntDivExpr>":
                                case "<IsNotExpr>":
                                case "<LeftExprTail>":
                                case "<MemberDecl>":
                                case "<MemberDeclList>":
                                case "<MethodArgList>":
                                case "<MethodStmt>":
                                case "<MethodStmtList>":
                                case "<ModExpr>":
                                case "<ModuleDecl>":
                                case "<MultExpr>":
                                case "<NamespaceDecl>":
                                case "<NamespaceMember>":
                                case "<NamespaceMembersListOpt>":
                                case "<NotExpr>":
                                case "<OfClassTypeListOpt>":
                                case "<OfClassTypeList>":
                                case "<OfClassType>":
                                case "<OpenStmt>":
                                case "<OrExpr>":
                                case "<ProcedureModifier>":
                                case "<PropertyBefore>":
                                case "<PropertyDecl>":
                                case "<PropertyTypeOpt>":
                                case "<PropertyFunctionsOpt>":
                                case "<QualifiedID>":
                                case "<QualifiedIDTail>":
                                case "<QualifiedID_CommasList>":
                                case "<StepOpt>":
                                case "<SubSafeAddExpr>":
                                case "<SubSafeAndAlsoExpr>":
                                case "<SubSafeAndExpr>":
                                case "<SubSafeCompareExpr>":
                                case "<SubSafeConcatExpr>":
                                case "<SubSafeEqvExpr>":
                                case "<SubSafeExpExpr>":
                                case "<SubSafeExpr>":
                                case "<SubSafeExprOpt>":
                                case "<SubSafeImpExpr>":
                                case "<SubSafeIntDivExpr>":
                                case "<SubSafeIsNotExpr>":
                                case "<SubSafeModExpr>":
                                case "<SubSafeMultExpr>":
                                case "<SubSafeNotExpr>":
                                case "<SubSafeOrExpr>":
                                case "<SubSafeUnaryExpr>":
                                case "<SubSafeValue>":
                                case "<SubSafeXorExpr>":
                                case "<Type>":
                                case "<Types>":
                                case "<UnaryExpr>":
                                case "<UsingResource>":
                                case "<UsingResourceList>":
                                case "<UsingResourceArgList>":
                                case "<Value>":
                                case "<WhenExprOpt>":
                                case "<WithStmt>":
                                case "<XorExpr>":
                                    {
                                        WalkAhead(reduction, ref entry);
                                        break;
                                    }
                                #endregion
                                #region Shit & Stop
                                //Следующие токены нас не интересуют и мы останавливаем обработку дерева (далее нет ничего интересного)
                                case "<AccessModifier>":
                                case "<AccessModifierOpt>":
                                case "<AliasOpt>":
                                case "<ArgModifierOpt>":
                                case "<ArrayRankList>":
                                case "<AssignOpt>":
                                case "<AsType>":
                                case "<AsTypeOpt>":
                                case "<BoolLiteral>":
                                case "<ClassAccessOpt>":
                                case "<CloseStmt>":
                                case "<ComplexType>":
                                case "<ConstDecl>":
                                case "<ConstExpr>":
                                case "<ConstList>":
                                case "<ElementaryType>":
                                case "<EndIfOpt>":
                                case "<EndIfSharpOpt>":
                                case "<ExitStmt>":
                                case "<ExtAccessModifierOpt>":
                                case "<ExtendedID>":
                                case "<FieldDecl>":
                                case "<FieldID>":
                                case "<FieldName>":
                                case "<FileNum>":
                                case "<FileNumList>":
                                case "<FileOpenAccess>":
                                case "<FileOpenAccessTypes>":
                                case "<FileOpenLock>":
                                case "<FileOpenLockTypes>":
                                case "<FileOpenMode>":
                                case "<FileOpenModeTypes>":
                                case "<HandleOpt>":
                                case "<InheritOptionOpt>":
                                case "<IntLiteral>":
                                case "<InputStmt>":
                                case "<KeywordID>":
                                case "<LineInputStmt>":
                                case "<LoopType>":
                                case "<MethodAccessOpt>":
                                case "<Next>":
                                case "<Nothing>":
                                case "<NL>":
                                case "<NLOpt>":
                                case "<Option>":
                                case "<OtherVarsOpt>":
                                case "<PartialOpt>":
                                case "<PropertyAccessType>":
                                case "<ReadWriteOnly>":
                                case "<SafeKeywordID>":
                                case "<ShadowsOpt>":
                                case "<UserType>":
                                case "<VarDecl>":
                                case "<VarName>":
                                case "<VersionDecl>":
                                    {
                                        break;
                                    }
                                #endregion
                                default:
                                    {
                                        if (!failList.Contains(token.Parent.Text()))
                                            failList.Add(token.Parent.Text());

                                        WalkAhead(reduction, ref entry);

                                        break;
                                    }
                            }
                        }
                        break;
                    default:
                        break;

                } // End of Switch

                //Удаляем токен из текущей цепочки
                chain.Remove(level);

            } //End Of For

            //Выгружаем из стека всё что накопилось
            AddFunctionCallesToStore(ref entry, stack.Pop(level));

            //Если завершился этап, на котором датчики не нужны (например, мы вышли из условия конструкции "IF") возвращаем предыдущее значение флага
            if (isNoSensorsSubTree)
                sensorFlag.Raise();

            //При каждом выходе уменьшаем уровень
            level--;
            
            return entry;
        }

        /// <summary>
        /// Метод для получения имени и позиции токена QualifiedID
        /// </summary>
        /// <param name="qualifiedID_token">Токен для анализа</param>
        /// <returns>Имя и позиция токена QualifiedID</returns>
        private TokenInfo Get_QualifiedID_Info(Token qualifiedID_token)
        {
            Reduction qualifiedID_reduction = (Reduction)qualifiedID_token.Data;
            Production qualifiedID_production = qualifiedID_reduction.Parent;

            switch (qualifiedID_production.Text())
            {
                case "<QualifiedID> ::= IDDot <QualifiedIDTail>":
                case "<QualifiedID> ::= DotIDDot <QualifiedIDTail>":
                    {
                        TokenInfo id_info = Get_Terminal_Info(qualifiedID_reduction[0]);
                        TokenInfo qualufiedID_info = Get_QualifiedIDTail_Info(qualifiedID_reduction[1]);
                        return new TokenInfo(id_info.Position, id_info.Name + qualufiedID_info.Name);
                    }
                case "<QualifiedID> ::= ID":
                        return Get_Terminal_Info(qualifiedID_reduction[0]);
                case "<QualifiedID> ::= DotID":
                        return Get_Terminal_Info(qualifiedID_reduction[0]);
                default:
                        throw new Exception("Ошибка в описании токена <QualifiedID>");
            }
        }

        /// <summary>
        /// Метод для получения имени и позиции токена QualifiedIDTail
        /// </summary>
        /// <param name="qualifiedIDTail_token">Токен для анализа</param>
        /// <returns>Имя и позиция токена QualifiedIDTail</returns>
        private TokenInfo Get_QualifiedIDTail_Info(Token qualifiedIDTail_token)
        {
            TokenInfo qualifiedIDTail_info = new TokenInfo(null, "");
            
            Reduction qualifiedIDTail_reduction = (Reduction)qualifiedIDTail_token.Data;
            Production qualifiedIDTail_production = qualifiedIDTail_reduction.Parent;

            switch (qualifiedIDTail_production.Text())
            {
                case "<QualifiedIDTail> ::= IDDot <QualifiedIDTail>":
                    {
                        TokenInfo idDot_info = Get_Terminal_Info(qualifiedIDTail_reduction[0]);
                        TokenInfo qualifiedIDTail_info_2 = Get_QualifiedIDTail_Info(qualifiedIDTail_reduction[1]);
                        qualifiedIDTail_info = new TokenInfo(idDot_info.Position, qualifiedIDTail_info.Name + idDot_info.Name + qualifiedIDTail_info_2.Name);
                        break;
                    }
                case "<QualifiedIDTail> ::= ID":
                    {
                        TokenInfo id_info = Get_Terminal_Info(qualifiedIDTail_reduction[0]);
                        qualifiedIDTail_info = new TokenInfo(id_info.Position, qualifiedIDTail_info.Name + id_info.Name);
                        break;
                    }
                case "<QualifiedIDTail> ::= <KeywordID>":
                    {
                        TokenInfo keywordID_info = Get_KeywordID_Info(qualifiedIDTail_reduction[0]);
                        qualifiedIDTail_info = new TokenInfo(keywordID_info.Position, qualifiedIDTail_info.Name + keywordID_info.Name);
                        break;
                    }
                default:
                    throw new Exception("Ошибка в описании токена <QualifiedIDTail>");
            }

            return qualifiedIDTail_info;
        }

        /// <summary>
        /// Метод для получения имени и позиции токена KeywordID
        /// </summary>
        /// <param name="keywordID_token">Токен для анализа</param>
        /// <returns>Имя и позиция токена KeywordID</returns>
        private TokenInfo Get_KeywordID_Info(Token keywordID_token)
        {
            Reduction keywordID_reduction = (Reduction)keywordID_token.Data;
            Production keywordID_production = keywordID_reduction.Parent;

            switch (keywordID_production.Text())
            {
                case "<KeywordID> ::= <SafeKeywordID>":
                        return Get_SafeKeywordID_Info(keywordID_reduction[0]);
                default:
                        return Get_Terminal_Info(keywordID_reduction[0]);
            }
        }

        /// <summary>
        /// Метод для получения имени и позиции токена SafeKeywordID
        /// </summary>
        /// <param name="safeKeywordID_token">Токен для анализа</param>
        /// <returns>Имя и позиция токена SafeKeywordID</returns>
        private TokenInfo Get_SafeKeywordID_Info(Token safeKeywordID_token)
        {
            Reduction safeKeywordID_reduction = (Reduction)safeKeywordID_token.Data;
            return Get_Terminal_Info(safeKeywordID_reduction[0]);
        }

        /// <summary>
        /// Метод для получения имени и позиции токена ExtendedID
        /// </summary>
        /// <param name="extendedID_token">Токен для анализа</param>
        /// <returns>Имя и позиция токена ExtendedID</returns>
        private TokenInfo Get_ExtendedID_Info(Token extendedID_token)
        {
            Reduction extendedID_reduction = (Reduction)extendedID_token.Data;
            Production extendedID_production = extendedID_reduction.Parent;

            switch (extendedID_production.Text())
            {
                case "<ExtendedID> ::= ID":
                        return Get_Terminal_Info(extendedID_reduction[0]);
                case "<ExtendedID> ::= <SafeKeywordID>":
                        return Get_SafeKeywordID_Info(extendedID_reduction[0]);
                default:
                    throw new Exception("Ошибка в описании токена <ExtendedID>");
            }
        }

        /// <summary>
        /// Метод для получения имени и позиции токена FieldID
        /// </summary>
        /// <param name="fieldID_token">Токен для анализа</param>
        /// <returns>Имя и позиция токена FieldID</returns>
        private TokenInfo Get_FieldID_Info(Token fieldID_token)
        {
            Reduction fieldID_reduction = (Reduction)fieldID_token.Data;
            return Get_Terminal_Info(fieldID_reduction[0]);
        }

        /// <summary>
        /// Метод для получения имени и позиции терминального токена
        /// </summary>
        /// <param name="token">Токен для анализа</param>
        /// <returns>Имя и позиция терминального токена</returns>
        private TokenInfo Get_Terminal_Info(Token token)
        {
            if (token.Type() == SymbolType.Nonterminal)
                throw new Exception("Обнаружен нетерминальный символ " + token.Parent.Text());

            return new TokenInfo(new StorePosition(token.Position()) ,(string)token.Data);
        }

        /// <summary>
        /// Функция поиска позиции заданного терминала. Поиск производится по дереву производится с заданного токена.
        /// </summary>
        /// <param name="root_token">Токен, с которого следует начать поиск</param>
        /// <param name="terminal">Терминальный символ, позицию которого мы ищем</param>
        /// <returns>Позиция терминального символа в тексте</returns>
        private StorePosition Find_Terminal_Position(Token root_token, string terminal)
        {
            Reduction root_reduction = (Reduction) root_token.Data;

            for(int i = 0; i < root_reduction.Count(); i++)
            {
                Token token = root_reduction[i];

                if (token.Type() == SymbolType.Nonterminal)
                {
                    StorePosition position = Find_Terminal_Position(token, terminal);
                    if(position == null)
                        continue;
                    else
                        return position;
                }
                else
                {
                    TokenInfo info = Get_Terminal_Info(token);
                    if (String.Equals(info.Name, terminal, StringComparison.OrdinalIgnoreCase))
                        return info.Position;
                    else
                        return null;
                }
            }

            return null;
        }

        /// <summary>
        /// Рекурсивный метод поиска позиции заданного токена (преимущественно нетерминального)
        /// </summary>
        /// <param name="token">Токен</param>
        /// <returns>Позиция токена</returns>
        private StorePosition Find_Token_Position(Token token)
        {
            //Если это терминал, то у него есть координаты
            if (token.Type() != SymbolType.Nonterminal)
                return new StorePosition(token.Position());

            //Если мы имеем дело с нетерминалом
            Reduction reduction = (Reduction)token.Data;
            for (int i = 0; i < reduction.Count(); i++)
            {
                StorePosition position = Find_Token_Position(reduction[i]);

                if (position == null)
                    continue;

                return position;
            }

            return null;
        }

        /// <summary>
        /// Метод поиска позиции токена
        /// </summary>
        /// <param name="reduction">Редакция, содержащая токен чью позицию мы ищем</param>
        /// <param name="start">Порядковый номер токена в редакции</param>
        /// <returns>Позиция токена</returns>
        private StorePosition Find_Token_Position(Reduction reduction, int start)
        {
            for(int i = start; i < reduction.Count(); i++)
            {
                StorePosition position = Find_Token_Position(reduction[i]);

                if (position != null)
                    return position;
            }

            IA.Monitor.Log.Warning(PluginSettings.Name, "Ошибка распознания координат №1. Токен: " + reduction[start].Parent.Text());
            return null;
        }

        /// <summary>
        /// Метод поиска позиции токена
        /// </summary>
        /// <param name="reduction">Редакция, содержащая токен чью позицию мы ищем</param>
        /// <param name="token">Токен</param>
        /// <returns>Позиция токена</returns>
        private StorePosition Find_Token_Position(Reduction reduction, Token token)
        {
            for (int i = 0; i < reduction.Count(); i++)
                if (reduction[i].Parent.Text() == token.Parent.Text())
                    return Find_Token_Position(reduction, i);

            throw new Exception("Ошибка распознания координат №2. Токен: " + token.Parent.Text());
        }

        /// <summary>
        /// Функция предназначена для поиска токена в правой части правила
        /// </summary>
        /// <param name="token">Токен в левой части правила</param>
        /// <param name="name">Имя нужного нам токена</param>
        /// <returns>Токен в правой части правила</returns>
        private Token Get_SubToken_By_Name(Token token, string name)
        {
            Reduction reduction = (Reduction)token.Data;

            string text = reduction.Parent.Text();

            if (!text.Contains("::="))
                return null;

            text = text.Substring(text.IndexOf("::=") + 3).Trim();
            
            string[] splitText = text.Split(' ');

            for (int i = 0; i < splitText.Length; i++)
                if (splitText[i] == name)
                    return reduction[i];

            return null;
        }

        /// <summary>
        /// Метод для обработки токена CaseStmtList для формирования IStatementSwitch 
        /// </summary>
        /// <param name="token">Токен CaseStmtList</param>
        /// <param name="statement">Стейтмент для формирования</param>
        private void Process_CaseStmtList(Token token, ref IStatementSwitch statement)
        {
            Reduction reduction = (Reduction)token.Data;
            Production production = reduction.Parent;

            //'Case' <ExprList> <NLOpt> <BlockStmtList> <CaseStmtList>
            if (production.Text() == "<CaseStmtList> ::= Case <ExprList> <NLOpt> <BlockStmtList> <CaseStmtList>")
            {
                IStatement case_label = TreeWalker_2nd_Stage((Reduction)reduction[1].Data, true);

                IOperation case_operation = (case_label != null && ((IStatementOperational)case_label).Operation != null) ? ((IStatementOperational)case_label).Operation : storage.operations.AddOperation();
                IStatement case_body = TreeWalker_2nd_Stage((Reduction)reduction[3].Data);

                statement.AddCase(case_operation, case_body ?? GetEmptyStatement(Find_Token_Position(reduction, 3)));

                Process_CaseStmtList(reduction[4], ref statement);
            }
            //'Case' 'Else' <NLOpt> <BlockStmtList>
            else if (production.Text() == "<CaseStmtList> ::= Case Else <NLOpt> <BlockStmtList>")
            {
                IStatement default_body = TreeWalker_2nd_Stage((Reduction)reduction[3].Data);
               
                statement.AddDefaultCase(default_body ?? GetEmptyStatement(Find_Token_Position(reduction, 3)));
            }
        }

        /// <summary>
        /// Метод для обработки токена CatchStmtList для формирования IStatementTryCatchFinally 
        /// </summary>
        /// <param name="token">Токен CatchStmtList</param>
        /// <param name="statement">Стейтмент для формирования</param>
        private void Process_CatchStmtList(Token token, ref IStatementTryCatchFinally statement)
        {
            Token catchStmt_token = Get_SubToken_By_Name(token, "<CatchStmt>");

            if (catchStmt_token == null)
                return;

            Token body_token = Get_SubToken_By_Name(catchStmt_token, "<BlockStmtList>");
            Reduction body_reduction = (Reduction)body_token.Data;

            //Добавляем новый кэтч
            IStatement catch_body = TreeWalker_2nd_Stage(body_reduction);
            statement.AddCatch(catch_body ?? GetEmptyStatement(Find_Token_Position((Reduction)catchStmt_token.Data, body_token)));

            //Уходим в рекурсию
            Process_CatchStmtList(((Reduction)token.Data)[1], ref statement);
        }

        /// <summary>
        /// Метод для обработки токена ElseStmtList
        /// </summary>
        /// <param name="token">Токен ElseStmtList</param>
        /// <returns>Стейтмент, соответствующий данному токену</returns>
        private IStatement Process_ElseStmtList(Token token)
        {
            Reduction reduction = (Reduction)token.Data;

            // 'ElseIf' <Expr> 'Then' <NL> <BlockStmtList> <ElseStmtList>
            // 'ElseIf' <Expr> 'Then' <InlineStmt> <NL> <ElseStmtList>
            if (Get_SubToken_By_Name(token, "ElseIf") != null)
            {
                //Ищем позицию
                StorePosition position = Find_Terminal_Position(token, "ElseIf");
                IStatementIf statementIf = storage.statements.AddStatementIf((ulong)position.Line, (ulong)position.Column, currentFile);

                Token then_token = null;
                // 'ElseIf' <Expr> 'Then' <NL> <BlockStmtList> <ElseStmtList>
                // 'ElseIf' <Expr> 'Then' <InlineStmt> <NL> <ElseStmtList>
                if ((then_token = Get_SubToken_By_Name(token, "<BlockStmtList>")) != null || (then_token = Get_SubToken_By_Name(token, "<InlineStmt>")) != null)
                {
                    //Condition
                    IStatement condition = TreeWalker_2nd_Stage((Reduction)reduction[1].Data, true);
                    if (condition != null && condition is IStatementOperational)
                        statementIf.Condition = ((IStatementOperational)condition).Operation;
                    
                    //Then Block
                    IStatement then_block = TreeWalker_2nd_Stage((Reduction)then_token.Data);
                    statementIf.ThenStatement = then_block ?? GetEmptyStatement(Find_Token_Position(reduction, then_token));

                    //Else Block
                    IStatement else_block = Process_ElseStmtList(reduction[5]);
                    statementIf.ElseStatement = else_block ?? GetEmptyStatement(Find_Token_Position(reduction, 5));
                }
                else
                {
                    if (then_token == null)
                        throw new Exception("Ошибка разбора правила");
                }

                return statementIf;
            }
            //'Else' <InlineStmt> <NL>
            //'Else' <NL> <BlockStmtList>
            else if (Get_SubToken_By_Name(token, "Else") != null)
            {
                Token else_token = null;
                if ((else_token = Get_SubToken_By_Name(token, "<BlockStmtList>")) != null || (else_token = Get_SubToken_By_Name(token, "<InlineStmt>")) != null)
                {
                    IStatement else_block = TreeWalker_2nd_Stage((Reduction)else_token.Data);
                    return else_block ?? GetEmptyStatement(Find_Token_Position(reduction, else_token));
                }
                else
                    if (else_token == null)
                        throw new Exception("Ошибка разбора правила");
            }

            return null;
        }

        /// <summary>
        /// Метод для обработки любого токена, который описывет любой список ID через какой-либо разделитель
        /// </summary>
        /// <param name="token">Токен для обработки</param>
        /// <returns>Список найденных QualifiedID</returns>
        private List<string> Process_QualifiedIDList(Token token)
        {
            List<string> ret = new List<string>();

            Reduction reduction = (Reduction)token.Data;

            if (reduction.Parent.Text().EndsWith("::= <QualifiedID>"))
                ret.Add(Get_QualifiedID_Info(reduction[0]).Name);
            else
            {
                ret.Add(Get_QualifiedID_Info(reduction[0]).Name);
                ret.AddRange(Process_QualifiedIDList(reduction[2]));
            }

            return ret;
        }

        /// <summary>
        /// Метод для обработки токена GetOrSetFunction.
        /// Заносит информацию о геттерах и сеттерах в Хранилище
        /// </summary>
        /// <param name="name">Имя свойства</param>
        /// <param name="token">Токен</param>
        private void Process_GetOrSetFunction(string name, Token token)
        {
            TokenInfo info = Get_Terminal_Info(((Reduction)token.Data)[0]);

            //Заносим найденную функцию в Хранлище
            IFunction function;
            if (currentClass != null)
                function = currentClass.AddMethod(false);
            else
                function = storage.functions.AddFunction();

            function.Name = name + "." + info.Name;
            function.AddDefinition(currentFile, (ulong)info.Position.Line, (ulong)info.Position.Column);

            //Ищем нужный нам токен, описывающий содержание функции
            Token body_token = Get_SubToken_By_Name(token, "<MethodStmtList>");
            if (body_token == null)
                throw new Exception("Ошибка при разборе токена <GetOrSetFunction>");

            //Обрабатываем содержимое функции
            currentFunction = function;
            TreeWalker_1st_Stage((Reduction)body_token.Data);
            currentFunction = globalFunction;
        }

        /// <summary>
        /// Создаём или дополняем StatementOperational
        /// </summary>
        /// <param name="entry">Текущая цепочка стейтментов</param>
        /// <param name="functions">Список найденных вызовов функций</param>
        private void AddFunctionCallesToStore(ref IStatement entry, List<TokenInfo> functions)
        {
            if (functions == null || functions.Count == 0)
                return;

            //Ищем каждую функцию в Хранилище, если ничего не находим, стейтмент не создаём
            List<List<IFunction>> functionsFromStorage = new List<List<IFunction>>();
            foreach (TokenInfo function in functions)
            {
                List<IFunction> result = GetFunctionOverloadsFromStorage(function.Name);
                if(result.Any())
                    functionsFromStorage.Add(result);
            }
            if(!functionsFromStorage.Any())
                return;

            //Добавляем новый стейтмент или наращиваем предыдущий StatementOperational
            IStatement last = GetLastStatement(entry);
            if (entry == null || !(last is IStatementOperational))
            {
                //Поиск координат для будущего стейтмента
                ulong column = ulong.MaxValue;
                ulong line = ulong.MaxValue;
                foreach (TokenInfo function in functions)
                    if ((ulong)function.Position.Line <= line && (ulong)function.Position.Column <= column)
                    {
                        line = (ulong)function.Position.Line;
                        column = (ulong)function.Position.Column;
                    }

                IStatementOperational statement = (IStatementOperational)GetNewStatement(ref entry, StatementType.Operational, line, column);
                statement.Operation = storage.operations.AddOperation();
                last = statement;
            }

            foreach (TokenInfo function in functions)
                ((IStatementOperational)last).Operation.AddFunctionCallInSequenceOrder(GetFunctionOverloadsFromStorage(function.Name).ToArray());
        }

        /// <summary>
        /// Поиск всех перегрузок функции в Хранилище по текущему файлу
        /// </summary>
        /// <param name="fullName">Имя функции для поиска</param>
        /// <returns>Список всевозможных перегрузок заданной функции в текущем файле</returns>
        private List<IFunction> GetFunctionOverloadsFromStorage(string fullName)
        {
            IFunction[] functions;
            List<IFunction> ret = new List<IFunction>();
            
            //Сначала ищем функцию по полному имени
            functions = storage.functions.GetFunction(fullName.Split('.'));

            //Если нашли что-то по полному имени вовращаем это
            if (functions.Any())
            {
                foreach (IFunction function in functions)
                    if (!function.IsExternal) //системные функции не учитываем
                        ret.Add(function);
                
                return ret;
            }

            //Если по полному имени ничего не нашли ищем по частичному имени в текущем файле
            string shortName = fullName.LastIndexOf('.') != -1 ? fullName.Substring(fullName.LastIndexOf('.') + 1) : fullName;
            functions = storage.functions.GetFunction(shortName);
            foreach (IFunction function in functions)
                if (!function.IsExternal) //системные функции не учитываем
                {
                    Location location = function.Definition();
                    if (location != null && location.GetFileID() == currentFile.Id)
                        ret.Add(function);
                }

            if (!ret.Any())
                IA.Monitor.Log.Error(PluginSettings.Name, "Функция " + fullName + " не найдена в Хранилище.");

            return ret;
        }

        /// <summary>
        /// Поиск функции в Хранилище определённой в текущем файле
        /// </summary>
        /// <param name="name">Имя функции</param>
        /// <returns>Объект функции в Хранилище</returns>
        private IFunction GetFunctionFromStorageByCurrentFile(string name)
        {
            IFunction[] functions = storage.functions.GetFunction(name);

            foreach (IFunction function in functions)
            {
                Location location = function.Definition();
                if (location !=  null && location.GetFileID() == currentFile.Id)
                    return function;
            }

            return null;
        }
    }
}
