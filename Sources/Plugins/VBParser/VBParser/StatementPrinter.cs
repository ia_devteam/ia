﻿/*
 * Плагин Парсер VB-VBS
 * Файл StatementPrinter.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Юхтин
 * 
 * Плагин предназначен для вставки датчиков в исходные очищенные тексты по 2-ому и 3-ему уровню.
 */

using System;
using System.Text;
using Store;
using System.Xml;
using System.Collections.Generic;
using IA.Extensions;

namespace IA.Plugins.Parsers.VBParser
{
    public class StatementPrinter
    {
        private XmlTextWriter xw;
        private Storage storage;
        
        public StatementPrinter(String resultXmlFileName, Storage storage)
        {
            //Задаём текущее Хранилище
            this.storage = storage;

            //Задаём файл для формирования отчёта
            xw = new XmlTextWriter(resultXmlFileName, Encoding.UTF8);
        }

        public void Execute(bool isWithNamespacesAndClasses = true)
        {
            xw.WriteStartDocument();
            xw.WriteStartElement("Files");

            if (isWithNamespacesAndClasses)
                GenReportWithNamespacesAndClasses(xw);
            else
                GenReport(xw);

            xw.WriteEndElement();
            xw.WriteEndDocument();

            xw.Flush();
            xw.Close();
        }

        private void GenReport(XmlTextWriter xmlWriter, IStatement st = null)
        {
            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.TasksReports)0).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)storage.files.Count());
            int counter = 0;

            // Если st = null, значит мы не в функции
            if (st == null)
            {
                foreach (IFile f in storage.files.EnumerateFiles(enFileKind.fileWithPrefix)) // ходим по файлам  
                {
                    xmlWriter.WriteStartElement("file");
                    xmlWriter.WriteAttributeString("Name", f.FullFileName_Canonized);

                    //ходим по функциям внутри класса
                    foreach (IFunction fun in f.FunctionsDefinedInTheFile()) // ходим по функциям в файле
                    {
                        xmlWriter.WriteStartElement("FunctionDecl");
                        xmlWriter.WriteAttributeString("Name", fun.FullNameForReports);

                        Location loc = fun.Definition();
                        if(loc != null && loc.GetFileID() == f.Id)
                            xmlWriter.WriteAttributeString("Position", '(' + loc.GetLine().ToString() + ',' + loc.GetColumn().ToString() + ')');

                        //переменные объявленные в функции
                        PrintVariables(fun);

                        if (fun.EntryStatement != null)
                            GenReport(xmlWriter, fun.EntryStatement);
                        xmlWriter.WriteEndElement();
                    }

                    xmlWriter.WriteEndElement();

                    IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.TasksReports)0).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);                    
                }
            }
            else
            {
                //есть стейтмент - значит мы внутри функции
                //цикл
                IStatement iter = st;
                do
                {
                    switch (iter.Kind)
                    {
                        #region STOperational 
                        case ENStatementKind.STOperational:
                            {
                                PrintOperational(iter);
                                
                            }
                            break;
                        #endregion
                        #region STDoAndWhile
                        case ENStatementKind.STDoAndWhile:
                            {
                                IStatementDoAndWhile sta = (IStatementDoAndWhile)iter;
                                if (sta.IsCheckConditionBeforeFirstRun) // это DO WHILE или WHILE DO
                                {//WHILE DO
                                    xmlWriter.WriteStartElement("while_do");
                                    xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                    PrintOperation(sta.Condition);

                                    if (sta.Body != null)
                                    {
                                        GenReport(xmlWriter, sta.Body);
                                    }

                                    xmlWriter.WriteEndElement();
                                }
                                else
                                {//DO WHILE
                                    xmlWriter.WriteStartElement("do_while");
                                    xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');
                                
                                    if (sta.Body != null)
                                    {
                                        GenReport(xmlWriter, sta.Body);
                                    }

                                    PrintOperation(sta.Condition);
                                    
                                    xmlWriter.WriteEndElement();
                                }
                                
                            }
                            break;
                        #endregion
                        #region STReturn
                        case ENStatementKind.STReturn:
                            {
                                IStatementReturn sta = (IStatementReturn)iter;
                                IOperation op = sta.ReturnOperation;

                                PrintOperation(op);
  
                                xmlWriter.WriteStartElement("return");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');
                                xmlWriter.WriteEndElement();
                                
                            }
                            break;
                        #endregion 
                        #region STBreak
                        case ENStatementKind.STBreak:
                            {
                                xmlWriter.WriteStartElement("break");
                                xmlWriter.WriteAttributeString("Position", '(' + iter.FirstSymbolLocation.GetLine().ToString() + ',' + iter.FirstSymbolLocation.GetColumn().ToString() + ')');
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STGoto
                        case ENStatementKind.STGoto:
                            {  
                                xmlWriter.WriteStartElement("goto");
                                xmlWriter.WriteAttributeString("Position", '(' + iter.FirstSymbolLocation.GetLine().ToString() + ',' + iter.FirstSymbolLocation.GetColumn().ToString() + ')');
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STContinue
                        case ENStatementKind.STContinue:
                            {
                                IStatementContinue sta = (IStatementContinue)iter;
  
                                xmlWriter.WriteStartElement("continue");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STIf
                        case ENStatementKind.STIf:
                            {
                                IStatementIf sta = (IStatementIf)iter;

                                if (sta.FirstSymbolLocation != null) // Если вдруг локейшн NULL
                                {
                                    xmlWriter.WriteStartElement("if");
                                    xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');
                                }
                                else
                                {
                                    xmlWriter.WriteStartElement("if");
                                    xmlWriter.WriteAttributeString("Position", "NUll_Location");
                                }

                                if (sta.Condition != null)
                                {
                                    PrintOperation(sta.Condition);
                                }
                                if (sta.ThenStatement != null)
                                {
                                    xmlWriter.WriteStartElement("then");

                                    GenReport(xmlWriter, sta.ThenStatement);

                                    xmlWriter.WriteEndElement();
                                }
                                if (sta.ElseStatement != null)
                                {
                                    xmlWriter.WriteStartElement("else");

                                    GenReport(xmlWriter, sta.ElseStatement);
                                    
                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STFor
                        case ENStatementKind.STFor:
                            {
                                IStatementFor sta = (IStatementFor)iter;
                                xmlWriter.WriteStartElement("for");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                PrintOperation(sta.Start);
                                PrintOperation(sta.Condition);
                                PrintOperation(sta.Iteration);
                                if (sta.Body != null)
                                {
                                    xmlWriter.WriteStartElement("body");
                                    GenReport(xmlWriter, sta.Body);
                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STForEach
                        case ENStatementKind.STForEach:
                            {

                                IStatementForEach sta = (IStatementForEach)iter;
                                xmlWriter.WriteStartElement("foreach");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                PrintOperation(sta.Head);
                                if (sta.Body != null)
                                {
                                    xmlWriter.WriteStartElement("body");
                                    GenReport(xmlWriter, sta.Body);
                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STSwitch
                        case ENStatementKind.STSwitch:
                            {
                                IStatementSwitch sta = (IStatementSwitch)iter;

                                xmlWriter.WriteStartElement("switch");
                                xmlWriter.WriteAttributeString("position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                PrintOperation(sta.Condition);

                                foreach (KeyValuePair<IOperation, IStatement> casies in sta.Cases())
                                {
                                    xmlWriter.WriteStartElement("case");

                                    PrintOperation(casies.Key);

                                    if (casies.Value != null) GenReport(xmlWriter, casies.Value);

                                    xmlWriter.WriteEndElement();
                                }
                                if (sta.DefaultCase() != null)
                                {
                                    xmlWriter.WriteStartElement("default");

                                    GenReport(xmlWriter, sta.DefaultCase());

                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STThrow
                        case ENStatementKind.STThrow:
                            {
                                IStatementThrow sta = (IStatementThrow)iter;

                                xmlWriter.WriteStartElement("throw");

                                PrintOperation(sta.Exception);
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STTryCatchFinally
                        case ENStatementKind.STTryCatchFinally:
                            {
                                IStatementTryCatchFinally sta = (IStatementTryCatchFinally)iter;

                                if (sta.TryBlock != null)
                                {
                                    xmlWriter.WriteStartElement("try");

                                    GenReport(xmlWriter, sta.TryBlock);

                                    xmlWriter.WriteEndElement();
                                }
                                else break;

                                foreach (IStatement catches in sta.Catches())
                                {
                                    xmlWriter.WriteStartElement("catch");

                                    GenReport(xmlWriter, catches);

                                    xmlWriter.WriteEndElement();
                                }

                                if (sta.FinallyBlock != null)
                                {
                                    xmlWriter.WriteStartElement("finally");

                                    GenReport(xmlWriter, sta.FinallyBlock);

                                    xmlWriter.WriteEndElement();
                                }
                            }
                            break;
                        #endregion
                        #region STUsing
                        case ENStatementKind.STUsing:
                            {
                                IStatementUsing sta = (IStatementUsing)iter;
                                xmlWriter.WriteStartElement("using");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                PrintOperation(sta.Head);
                                if (sta.Body != null)
                                {
                                    xmlWriter.WriteStartElement("body");
                                    GenReport(xmlWriter, sta.Body);
                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region DEFAULT
                        default:
                            xmlWriter.WriteStartElement(iter.Kind.ToString());
                            xmlWriter.WriteEndElement();
                         break;
                        #endregion
                    }

                } while ((iter = iter.NextInLinearBlock) != null);
            }
        }

        private void GenReportWithNamespacesAndClasses(XmlTextWriter xmlWriter, IStatement st = null)
        {
            List<Namespaces> namespaces = new List<Namespaces>();
            List<Class> classes = new List<Class>();
            List<IFunction> noClassFunctions = new List<IFunction>(); 

            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.TasksReports)0).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)storage.files.Count());
            int counter = 0;

            // Если st = null, значит мы не в функции
            if (st == null)
            {
                foreach (IFile f in storage.files.EnumerateFiles(enFileKind.fileWithPrefix)) // ходим по файлам  
                {
                    xmlWriter.WriteStartElement("file");
                    xmlWriter.WriteAttributeString("Name", f.FullFileName_Canonized);

                    //узнаём классы в файле
                    classes.Clear();
                    foreach (IFunction fun in f.FunctionsDefinedInTheFile())
                    {
                        string classFullName;
                        if (fun.FullName.LastIndexOf('.') != -1)
                            classFullName = fun.FullName.Substring(0, fun.FullName.LastIndexOf('.'));
                        else
                            continue;

                        Class[] searchClass = storage.classes.GetClass(classFullName.Split('.'));
                        if (searchClass.Length > 0)
                            if (!classes.Contains(searchClass[0]))
                                classes.Add(searchClass[0]);
                    }

                    //сначала предполагаем что все функции вне классов
                    noClassFunctions.Clear();
                    foreach (IFunction fun in f.FunctionsDefinedInTheFile())
                        noClassFunctions.Add(fun);
                    
                    //ходим по классам их методам и полям
                    foreach (Class cls in classes)
                    {
                        xmlWriter.WriteStartElement("Class");
                        xmlWriter.WriteAttributeString("Name", cls.FullName);

                        List<string> inherits = new List<string>();
                        foreach (Class parent in cls.EnumerateParents())
                            inherits.Add(parent.FullName);
                        if (inherits.Count != 0)
                            xmlWriter.WriteAttributeString("Inherits", string.Join(", ", inherits.ToArray()));

                        //обработка полей
                        PrintFields(cls);

                        //обработка методов
                        foreach (IFunction fun in cls.EnumerateMethods()) // ходим по функциям в данном файле, данном классе
                        {
                            xmlWriter.WriteStartElement("Function");
                            xmlWriter.WriteAttributeString("Name", fun.FullNameForReports);

                            //ходим по переменным внутри функции
                            PrintVariables(fun);

                            if (fun.EntryStatement != null)
                                GenReportWithNamespacesAndClasses(xmlWriter, fun.EntryStatement);
                            xmlWriter.WriteEndElement();

                            noClassFunctions.Remove(fun);
                        }

                        xmlWriter.WriteEndElement();
                    }

                    //добиваем функции вне классов
                    foreach (IFunction fun in noClassFunctions) // ходим по функциям в данном файле, данном классе
                    {
                        xmlWriter.WriteStartElement("Function");
                        xmlWriter.WriteAttributeString("Name", fun.FullNameForReports);

                        //ходим по переменным внутри функции
                        PrintVariables(fun);

                        if (fun.EntryStatement != null)
                            GenReportWithNamespacesAndClasses(xmlWriter, fun.EntryStatement);
                        xmlWriter.WriteEndElement();
                    }

                    xmlWriter.WriteEndElement();

                    IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.TasksReports)0).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);
                }
            }
            else
            {
                //есть стейтмент - значит мы внутри функции
                //цикл
                IStatement iter = st;
                do
                {
                    switch (iter.Kind)
                    {
                        #region STOperational
                        case ENStatementKind.STOperational:
                            {
                                PrintOperational(iter);

                            }
                            break;
                        #endregion
                        #region STDoAndWhile
                        case ENStatementKind.STDoAndWhile:
                            {
                                IStatementDoAndWhile sta = (IStatementDoAndWhile)iter;
                                if (sta.IsCheckConditionBeforeFirstRun) // это DO WHILE или WHILE DO
                                {//WHILE DO
                                    xmlWriter.WriteStartElement("while_do");
                                    xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                    PrintOperation(sta.Condition);

                                    if (sta.Body != null)
                                    {
                                        GenReportWithNamespacesAndClasses(xmlWriter, sta.Body);
                                    }

                                    xmlWriter.WriteEndElement();
                                }
                                else
                                {//DO WHILE
                                    xmlWriter.WriteStartElement("do_while");
                                    xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                    if (sta.Body != null)
                                    {
                                        GenReportWithNamespacesAndClasses(xmlWriter, sta.Body);
                                    }

                                    PrintOperation(sta.Condition);

                                    xmlWriter.WriteEndElement();
                                }

                            }
                            break;
                        #endregion
                        #region STReturn
                        case ENStatementKind.STReturn:
                            {
                                IStatementReturn sta = (IStatementReturn)iter;
                                IOperation op = sta.ReturnOperation;

                                PrintOperation(op);

                                xmlWriter.WriteStartElement("return");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');
                                xmlWriter.WriteEndElement();

                            }
                            break;
                        #endregion
                        #region STBreak
                        case ENStatementKind.STBreak:
                            {
                                xmlWriter.WriteStartElement("break");
                                xmlWriter.WriteAttributeString("Position", '(' + iter.FirstSymbolLocation.GetLine().ToString() + ',' + iter.FirstSymbolLocation.GetColumn().ToString() + ')');
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STGoto
                        case ENStatementKind.STGoto:
                            {
                                xmlWriter.WriteStartElement("goto");
                                xmlWriter.WriteAttributeString("Position", '(' + iter.FirstSymbolLocation.GetLine().ToString() + ',' + iter.FirstSymbolLocation.GetColumn().ToString() + ')');
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STContinue
                        case ENStatementKind.STContinue:
                            {
                                IStatementContinue sta = (IStatementContinue)iter;

                                xmlWriter.WriteStartElement("continue");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STIf
                        case ENStatementKind.STIf:
                            {
                                IStatementIf sta = (IStatementIf)iter;

                                if (sta.FirstSymbolLocation != null) // Если вдруг локейшн NULL
                                {
                                    xmlWriter.WriteStartElement("if");
                                    xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');
                                }
                                else
                                {
                                    xmlWriter.WriteStartElement("if");
                                    xmlWriter.WriteAttributeString("Position", "NUll_Location");
                                }

                                if (sta.Condition != null)
                                {
                                    PrintOperation(sta.Condition);
                                }
                                if (sta.ThenStatement != null)
                                {
                                    xmlWriter.WriteStartElement("then");

                                    GenReportWithNamespacesAndClasses(xmlWriter, sta.ThenStatement);

                                    xmlWriter.WriteEndElement();
                                }
                                if (sta.ElseStatement != null)
                                {
                                    xmlWriter.WriteStartElement("else");

                                    GenReportWithNamespacesAndClasses(xmlWriter, sta.ElseStatement);

                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STFor
                        case ENStatementKind.STFor:
                            {
                                IStatementFor sta = (IStatementFor)iter;
                                xmlWriter.WriteStartElement("for");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                PrintOperation(sta.Start);
                                PrintOperation(sta.Condition);
                                PrintOperation(sta.Iteration);
                                if (sta.Body != null)
                                {
                                    xmlWriter.WriteStartElement("body");
                                    GenReportWithNamespacesAndClasses(xmlWriter, sta.Body);
                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STForEach
                        case ENStatementKind.STForEach:
                            {

                                IStatementForEach sta = (IStatementForEach)iter;
                                xmlWriter.WriteStartElement("foreach");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                PrintOperation(sta.Head);
                                if (sta.Body != null)
                                {
                                    xmlWriter.WriteStartElement("body");
                                    GenReportWithNamespacesAndClasses(xmlWriter, sta.Body);
                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STSwitch
                        case ENStatementKind.STSwitch:
                            {
                                IStatementSwitch sta = (IStatementSwitch)iter;

                                xmlWriter.WriteStartElement("switch");
                                xmlWriter.WriteAttributeString("position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                PrintOperation(sta.Condition);

                                foreach (KeyValuePair<IOperation, IStatement> casies in sta.Cases())
                                {
                                    xmlWriter.WriteStartElement("case");

                                    PrintOperation(casies.Key);

                                    if (casies.Value != null) GenReportWithNamespacesAndClasses(xmlWriter, casies.Value);

                                    xmlWriter.WriteEndElement();
                                }
                                if (sta.DefaultCase() != null)
                                {
                                    xmlWriter.WriteStartElement("default");

                                    GenReportWithNamespacesAndClasses(xmlWriter, sta.DefaultCase());

                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STThrow
                        case ENStatementKind.STThrow:
                            {
                                IStatementThrow sta = (IStatementThrow)iter;

                                xmlWriter.WriteStartElement("throw");

                                PrintOperation(sta.Exception);
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STTryCatchFinally
                        case ENStatementKind.STTryCatchFinally:
                            {
                                IStatementTryCatchFinally sta = (IStatementTryCatchFinally)iter;

                                if (sta.TryBlock != null)
                                {
                                    xmlWriter.WriteStartElement("try");

                                    GenReportWithNamespacesAndClasses(xmlWriter, sta.TryBlock);

                                    xmlWriter.WriteEndElement();
                                }
                                else break;

                                foreach (IStatement catches in sta.Catches())
                                {
                                    xmlWriter.WriteStartElement("catch");

                                    GenReportWithNamespacesAndClasses(xmlWriter, catches);

                                    xmlWriter.WriteEndElement();
                                }

                                if (sta.FinallyBlock != null)
                                {
                                    xmlWriter.WriteStartElement("finally");

                                    GenReportWithNamespacesAndClasses(xmlWriter, sta.FinallyBlock);

                                    xmlWriter.WriteEndElement();
                                }
                            }
                            break;
                        #endregion
                        #region STUsing
                        case ENStatementKind.STUsing:
                            {
                                IStatementUsing sta = (IStatementUsing)iter;
                                xmlWriter.WriteStartElement("using");
                                xmlWriter.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                                PrintOperation(sta.Head);
                                if (sta.Body != null)
                                {
                                    xmlWriter.WriteStartElement("body");
                                    GenReportWithNamespacesAndClasses(xmlWriter, sta.Body);
                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region DEFAULT
                        default:
                            xmlWriter.WriteStartElement(iter.Kind.ToString());
                            xmlWriter.WriteEndElement();
                            break;
                        #endregion
                    }

                } while ((iter = iter.NextInLinearBlock) != null);
            }
        }

        void PrintOperational(IStatement iter)
        {
            //xmlWriter.Flush();
            //xmlWriter.Close();
            xw.WriteStartElement("IOperational");
            if (iter != null)
            {
                IStatementOperational sta = (IStatementOperational)iter;
                IOperation op = sta.Operation; // получаем инкапсулированный Operation

                xw.WriteAttributeString("Position", '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')');

                PrintOperation(op);
            }
            xw.WriteEndElement();
        }

        void PrintOperation(IOperation op)
        {
            xw.WriteStartElement("IOperation");
            if (op != null)
            {


                foreach (IFunction[] fun in op.SequentiallyCallsFunctions())// если несколько в одну линию в квадратных скобках и через запятые
                {
                    String tmps = "";
                    if (fun.Length != 0)
                    {

                        if (fun.Length > 1) // если точно неизвестно какая функция вызывается вывести все варианты
                        {
                            foreach (IFunction fun1 in fun) // но сначала их надо собрать в кучу
                                tmps += "[" + fun1.FullNameForReports + "],";

                            tmps = tmps.TrimEnd(','); // и убрать лишнюю запятую
                            xw.WriteStartElement("FunctionCall");
                            xw.WriteAttributeString("Name", tmps);
                            xw.WriteEndElement();
                        }
                        else
                        {//Если вариант один, выводим его 
                            xw.WriteStartElement("FunctionCall");
                            xw.WriteAttributeString("Name", fun[0].FullNameForReports);
                            xw.WriteEndElement();
                        }
                    }
                }
              }
            xw.WriteEndElement();
          }

        void PrintVariables(IFunction fun)
        {
            foreach (Store.Variable var in storage.variables.EnumerateVariables())
            {
                if (var.Definitions().Length > 0 && var.Definitions()[0].GetFunctionId() == fun.Id)
                {
                    xw.WriteStartElement("VariableDecl");
                    xw.WriteAttributeString("Name", var.FullName);
                    xw.WriteAttributeString("Definition", '(' + var.Definitions()[0].GetLine().ToString() + ',' + var.Definitions()[0].GetColumn().ToString() + ')');

                    //геты
                    foreach (Location getLoc in var.ValueGetPosition())
                    {
                        xw.WriteStartElement("Get");
                        xw.WriteAttributeString("Position", '(' + getLoc.GetLine().ToString() + ',' + getLoc.GetColumn().ToString() + ')');
                        xw.WriteEndElement();
                    }

                    //сеты
                    foreach (Location setLoc in var.ValueSetPosition())
                    {
                        xw.WriteStartElement("Set");
                        xw.WriteAttributeString("Position", '(' + setLoc.GetLine().ToString() + ',' + setLoc.GetColumn().ToString() + ')');
                        xw.WriteEndElement();
                    }

                    //колы
                    foreach (Location callLoc in var.ValueCallPosition())
                    {
                        xw.WriteStartElement("Call");
                        xw.WriteAttributeString("Position", '(' + callLoc.GetLine().ToString() + ',' + callLoc.GetColumn().ToString() + ')');
                        xw.WriteEndElement();
                    }

                    xw.WriteEndElement();
                }
            }
        }

        void PrintFields(Class cls)
        {
            foreach (Field field in cls.EnumerateFields())
            {
                xw.WriteStartElement("Field");
                xw.WriteAttributeString("Name", field.FullName);
                xw.WriteAttributeString("Static", field.isStatic.ToString());

                //геты
                foreach (Location getLoc in field.ValueGetPosition())
                {
                    xw.WriteStartElement("Get");
                    xw.WriteAttributeString("Position", '(' + getLoc.GetLine().ToString() + ',' + getLoc.GetColumn().ToString() + ')');
                    xw.WriteEndElement();
                }

                //сеты
                foreach (Location setLoc in field.ValueSetPosition())
                {
                    xw.WriteStartElement("Set");
                    xw.WriteAttributeString("Position", '(' + setLoc.GetLine().ToString() + ',' + setLoc.GetColumn().ToString() + ')');
                    xw.WriteEndElement();
                }

                //колы
                foreach (Location callLoc in field.ValueCallPosition())
                {
                    xw.WriteStartElement("Call");
                    xw.WriteAttributeString("Position", '(' + callLoc.GetLine().ToString() + ',' + callLoc.GetColumn().ToString() + ')');
                    xw.WriteEndElement();
                }

                xw.WriteEndElement();
            }
        }
        
    }
}
