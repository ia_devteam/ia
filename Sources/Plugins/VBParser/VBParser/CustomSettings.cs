﻿/*
 * Плагин Парсер VB-VBS
 * Файл Settings.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Юхтин
 * 
 * Плагин предназначен для вставки датчиков в исходные очищенные тексты по 2-ому и 3-ему уровню.
 */

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace IA.Plugins.Parsers.VBParser
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        private string systemFunctionsFilePath = Path.GetFullPath(Environment.CurrentDirectory + VBParser.SystemFunctionsFilePathPostfix);
        private string systemVariablesFilePath = Path.GetFullPath(Environment.CurrentDirectory + VBParser.SystemVariablesFilePathPostfix);

        /// <summary>
        /// Конструктор
        /// </summary>
        internal Settings(ulong minimumStartSensor)
        {
            InitializeComponent();
            
            //Уровень вставки датчиков
            switch (PluginSettings.Level)
            {
                case 0:
                    level0_radioButton.Checked = true;
                    break;
                case 2:
                    level2_radioButton.Checked = true;
                    break;
                case 3:
                    level3_radioButton.Checked = true;
                    break;
                default:
                    break;
            }

            //Первый номер датчика
            firstSensorNumber_numericUpDown.Minimum = minimumStartSensor;
            firstSensorNumber_numericUpDown.Maximum = ulong.MaxValue;
            firstSensorNumber_numericUpDown.Value =
                (PluginSettings.FirstSensorNumber > minimumStartSensor)
                    ? PluginSettings.FirstSensorNumber
                    : minimumStartSensor;

            //Текст датчика
            sensorText_textBox.Text = PluginSettings.SensorText;

            //Настройки для VB
            isVB_checkBox.Checked = PluginSettings.IsVB;
            vbSensorFunctionText_groupBox.Enabled = PluginSettings.IsVB && PluginSettings.Level != 0;
            vbSensorFunctionText_textBox.Text = string.Join("\r\n", PluginSettings.VBSensorFunctionText.ToArray());

            //Настройки для VBS
            isVBS_checkBox.Checked = PluginSettings.IsVBS;
            vbsSensorFunctionText_groupBox.Enabled = PluginSettings.IsVBS && PluginSettings.Level != 0;
            vbsSensorFunctionText_textBox.Text = string.Join("\r\n", PluginSettings.VBSSensorFunctionText.ToArray());

            //Путь к файлу со списокм системных функций
            isDefault_systemFunctionsFilePath_checkBox.Checked = PluginSettings.IsDefault_SystemFunctionsFilePath;
            systemFunctionsFilePath_textBox.Enabled = !isDefault_systemFunctionsFilePath_checkBox.Checked;
            systemFunctionsFilePath_textBox.Text = PluginSettings.IsDefault_SystemFunctionsFilePath ? systemFunctionsFilePath : PluginSettings.SystemFunctionsFilePath;

            //Путь к файлу со списокм системных переменных
            isDefault_systemVariablesFilePath_checkBox.Checked = PluginSettings.IsDefault_SystemVariablesFilePath;
            systemVariablesFilePath_textBox.Enabled = !isDefault_systemVariablesFilePath_checkBox.Checked;
            systemVariablesFilePath_textBox.Text = PluginSettings.IsDefault_SystemVariablesFilePath ? systemVariablesFilePath : PluginSettings.SystemVariablesFilePath;
        }
        
        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.Level = level0_radioButton.Checked ? (uint)0 : level2_radioButton.Checked ? (uint)2 : level3_radioButton.Checked ? (uint)3 : (uint)1;  

            PluginSettings.FirstSensorNumber = (ulong)firstSensorNumber_numericUpDown.Value;
            PluginSettings.SensorText = sensorText_textBox.Text;

            PluginSettings.IsVB = isVB_checkBox.Checked;
            PluginSettings.VBSensorFunctionText = new List<string>(vbSensorFunctionText_textBox.Text.Split(new string[] {"\r\n"}, StringSplitOptions.None));

            PluginSettings.IsVBS = isVBS_checkBox.Checked;
            PluginSettings.VBSSensorFunctionText = new List<string>(vbsSensorFunctionText_textBox.Text.Split(new string[] { "\r\n" }, StringSplitOptions.None));

            PluginSettings.IsDefault_SystemFunctionsFilePath = isDefault_systemFunctionsFilePath_checkBox.Checked;
            PluginSettings.SystemFunctionsFilePath = systemFunctionsFilePath_textBox.Text;

            PluginSettings.IsDefault_SystemVariablesFilePath = isDefault_systemVariablesFilePath_checkBox.Checked;
            PluginSettings.SystemVariablesFilePath = systemVariablesFilePath_textBox.Text;
        }
        
        /// <summary>
        /// Кнопка выбора файла со списком системных функций
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void systemFunctionsFilePath_button_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { FileName = systemFunctionsFilePath_textBox.Text })
            {
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;

                systemFunctionsFilePath_textBox.Text = ofd.FileName;
            }
        }

        /// <summary>
        /// Кнопка выбора файла со списком системных переменных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void systemVariablesFilePath_button_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { FileName = systemVariablesFilePath_textBox.Text })
            {
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;

                systemVariablesFilePath_textBox.Text = ofd.FileName;
            }
        }
        
        /// <summary>
        /// Обработчик - работа с VB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isVB_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            vbSensorFunctionText_groupBox.Enabled = isVB_checkBox.Checked && !level0_radioButton.Checked;
        }

        /// <summary>
        /// Обработчик - работа с VBS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isVBS_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            vbsSensorFunctionText_groupBox.Enabled = isVBS_checkBox.Checked && !level0_radioButton.Checked;
        }

        /// <summary>
        /// Обработчик - файл со списком системных функций по умолчанию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isDefault_systemFunctionsFilePath_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            systemFunctionsFilePath_textBox.Enabled = !isDefault_systemFunctionsFilePath_checkBox.Checked;

            systemFunctionsFilePath_textBox.Text = isDefault_systemFunctionsFilePath_checkBox.Checked ? systemFunctionsFilePath : String.Empty;
        }

        /// <summary>
        /// Обработчик - файл со списком системных переменных по умолчанию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isDefault_systemVariablesFilePath_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            systemVariablesFilePath_textBox.Enabled = !isDefault_systemVariablesFilePath_checkBox.Checked;

            systemVariablesFilePath_textBox.Text = isDefault_systemVariablesFilePath_checkBox.Checked ? systemVariablesFilePath : String.Empty;
        }
                
        /// <summary>
        /// Обработчик - не вставлять датчики
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void level0_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            sensors_groupBox.Enabled = vb_groupBox.Enabled = vbs_groupBox.Enabled = !level0_radioButton.Checked;
        }
    }
}
