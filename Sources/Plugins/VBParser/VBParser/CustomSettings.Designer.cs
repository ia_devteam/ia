﻿namespace IA.Plugins.Parsers.VBParser
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sensors_groupBox = new System.Windows.Forms.GroupBox();
            this.sensors_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sensorText_textBox = new System.Windows.Forms.TextBox();
            this.firstSensorNumber_label = new System.Windows.Forms.Label();
            this.firstSensorNumber_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.sensorText_label = new System.Windows.Forms.Label();
            this.systemFunctionsFilePath_groupBox = new System.Windows.Forms.GroupBox();
            this.systemFunctionsFilePath_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isDefault_systemFunctionsFilePath_checkBox = new System.Windows.Forms.CheckBox();
            this.systemFunctionsFilePath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.systemVariablesFilePath_groupBox = new System.Windows.Forms.GroupBox();
            this.systemVariablesFilePath_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isDefault_systemVariablesFilePath_checkBox = new System.Windows.Forms.CheckBox();
            this.systemVariablesFilePath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.vb_groupBox = new System.Windows.Forms.GroupBox();
            this.vb_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.vbSensorFunctionText_groupBox = new System.Windows.Forms.GroupBox();
            this.vbSensorFunctionText_textBox = new System.Windows.Forms.TextBox();
            this.isVB_checkBox = new System.Windows.Forms.CheckBox();
            this.vbs_groupBox = new System.Windows.Forms.GroupBox();
            this.vbs_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isVBS_checkBox = new System.Windows.Forms.CheckBox();
            this.vbsSensorFunctionText_groupBox = new System.Windows.Forms.GroupBox();
            this.vbsSensorFunctionText_textBox = new System.Windows.Forms.TextBox();
            this.level_groupBox = new System.Windows.Forms.GroupBox();
            this.level_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.level0_radioButton = new System.Windows.Forms.RadioButton();
            this.level3_radioButton = new System.Windows.Forms.RadioButton();
            this.level2_radioButton = new System.Windows.Forms.RadioButton();
            this.sensors_groupBox.SuspendLayout();
            this.sensors_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).BeginInit();
            this.systemFunctionsFilePath_groupBox.SuspendLayout();
            this.systemFunctionsFilePath_tableLayoutPanel.SuspendLayout();
            this.systemVariablesFilePath_groupBox.SuspendLayout();
            this.systemVariablesFilePath_tableLayoutPanel.SuspendLayout();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.vb_groupBox.SuspendLayout();
            this.vb_tableLayoutPanel.SuspendLayout();
            this.vbSensorFunctionText_groupBox.SuspendLayout();
            this.vbs_groupBox.SuspendLayout();
            this.vbs_tableLayoutPanel.SuspendLayout();
            this.vbsSensorFunctionText_groupBox.SuspendLayout();
            this.level_groupBox.SuspendLayout();
            this.level_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sensors_groupBox
            // 
            this.sensors_groupBox.Controls.Add(this.sensors_tableLayoutPanel);
            this.sensors_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensors_groupBox.Location = new System.Drawing.Point(3, 58);
            this.sensors_groupBox.Name = "sensors_groupBox";
            this.sensors_groupBox.Size = new System.Drawing.Size(436, 79);
            this.sensors_groupBox.TabIndex = 1;
            this.sensors_groupBox.TabStop = false;
            this.sensors_groupBox.Text = "Настройка датчиков";
            // 
            // sensors_tableLayoutPanel
            // 
            this.sensors_tableLayoutPanel.ColumnCount = 2;
            this.sensors_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.sensors_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.sensors_tableLayoutPanel.Controls.Add(this.sensorText_textBox, 1, 1);
            this.sensors_tableLayoutPanel.Controls.Add(this.firstSensorNumber_label, 0, 0);
            this.sensors_tableLayoutPanel.Controls.Add(this.firstSensorNumber_numericUpDown, 1, 0);
            this.sensors_tableLayoutPanel.Controls.Add(this.sensorText_label, 0, 1);
            this.sensors_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensors_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.sensors_tableLayoutPanel.Name = "sensors_tableLayoutPanel";
            this.sensors_tableLayoutPanel.RowCount = 2;
            this.sensors_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.sensors_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.sensors_tableLayoutPanel.Size = new System.Drawing.Size(430, 60);
            this.sensors_tableLayoutPanel.TabIndex = 0;
            // 
            // sensorText_textBox
            // 
            this.sensorText_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_textBox.Location = new System.Drawing.Point(141, 35);
            this.sensorText_textBox.Name = "sensorText_textBox";
            this.sensorText_textBox.Size = new System.Drawing.Size(286, 20);
            this.sensorText_textBox.TabIndex = 2;
            // 
            // firstSensorNumber_label
            // 
            this.firstSensorNumber_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_label.AutoSize = true;
            this.firstSensorNumber_label.Location = new System.Drawing.Point(3, 8);
            this.firstSensorNumber_label.Name = "firstSensorNumber_label";
            this.firstSensorNumber_label.Size = new System.Drawing.Size(132, 13);
            this.firstSensorNumber_label.TabIndex = 1;
            this.firstSensorNumber_label.Text = "Номер первого датчика:";
            // 
            // firstSensorNumber_numericUpDown
            // 
            this.firstSensorNumber_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_numericUpDown.Location = new System.Drawing.Point(141, 5);
            this.firstSensorNumber_numericUpDown.Name = "firstSensorNumber_numericUpDown";
            this.firstSensorNumber_numericUpDown.Size = new System.Drawing.Size(286, 20);
            this.firstSensorNumber_numericUpDown.TabIndex = 0;
            // 
            // sensorText_label
            // 
            this.sensorText_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_label.AutoSize = true;
            this.sensorText_label.Location = new System.Drawing.Point(3, 38);
            this.sensorText_label.Name = "sensorText_label";
            this.sensorText_label.Size = new System.Drawing.Size(132, 13);
            this.sensorText_label.TabIndex = 3;
            this.sensorText_label.Text = "Текст датчика:";
            // 
            // systemFunctionsFilePath_groupBox
            // 
            this.systemFunctionsFilePath_groupBox.Controls.Add(this.systemFunctionsFilePath_tableLayoutPanel);
            this.systemFunctionsFilePath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.systemFunctionsFilePath_groupBox.Location = new System.Drawing.Point(3, 383);
            this.systemFunctionsFilePath_groupBox.Name = "systemFunctionsFilePath_groupBox";
            this.systemFunctionsFilePath_groupBox.Size = new System.Drawing.Size(436, 79);
            this.systemFunctionsFilePath_groupBox.TabIndex = 2;
            this.systemFunctionsFilePath_groupBox.TabStop = false;
            this.systemFunctionsFilePath_groupBox.Text = "Файл со списком системных функций";
            // 
            // systemFunctionsFilePath_tableLayoutPanel
            // 
            this.systemFunctionsFilePath_tableLayoutPanel.ColumnCount = 1;
            this.systemFunctionsFilePath_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.systemFunctionsFilePath_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.systemFunctionsFilePath_tableLayoutPanel.Controls.Add(this.isDefault_systemFunctionsFilePath_checkBox, 0, 0);
            this.systemFunctionsFilePath_tableLayoutPanel.Controls.Add(this.systemFunctionsFilePath_textBox, 0, 1);
            this.systemFunctionsFilePath_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.systemFunctionsFilePath_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.systemFunctionsFilePath_tableLayoutPanel.Name = "systemFunctionsFilePath_tableLayoutPanel";
            this.systemFunctionsFilePath_tableLayoutPanel.RowCount = 2;
            this.systemFunctionsFilePath_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.systemFunctionsFilePath_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.systemFunctionsFilePath_tableLayoutPanel.Size = new System.Drawing.Size(430, 60);
            this.systemFunctionsFilePath_tableLayoutPanel.TabIndex = 3;
            // 
            // isDefault_systemFunctionsFilePath_checkBox
            // 
            this.isDefault_systemFunctionsFilePath_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isDefault_systemFunctionsFilePath_checkBox.AutoSize = true;
            this.systemFunctionsFilePath_tableLayoutPanel.SetColumnSpan(this.isDefault_systemFunctionsFilePath_checkBox, 2);
            this.isDefault_systemFunctionsFilePath_checkBox.Location = new System.Drawing.Point(3, 6);
            this.isDefault_systemFunctionsFilePath_checkBox.Name = "isDefault_systemFunctionsFilePath_checkBox";
            this.isDefault_systemFunctionsFilePath_checkBox.Size = new System.Drawing.Size(424, 17);
            this.isDefault_systemFunctionsFilePath_checkBox.TabIndex = 2;
            this.isDefault_systemFunctionsFilePath_checkBox.Text = "По умолчанию";
            this.isDefault_systemFunctionsFilePath_checkBox.UseVisualStyleBackColor = true;
            this.isDefault_systemFunctionsFilePath_checkBox.CheckedChanged += new System.EventHandler(this.isDefault_systemFunctionsFilePath_checkBox_CheckedChanged);
            // 
            // systemFunctionsFilePath_textBox
            // 
            this.systemFunctionsFilePath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.systemFunctionsFilePath_textBox.Location = new System.Drawing.Point(3, 33);
            this.systemFunctionsFilePath_textBox.MaximumSize = new System.Drawing.Size(0, 24);
            this.systemFunctionsFilePath_textBox.MinimumSize = new System.Drawing.Size(0, 24);
            this.systemFunctionsFilePath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.systemFunctionsFilePath_textBox.Name = "systemFunctionsFilePath_textBox";
            this.systemFunctionsFilePath_textBox.Size = new System.Drawing.Size(424, 24);
            this.systemFunctionsFilePath_textBox.TabIndex = 3;
            // 
            // systemVariablesFilePath_groupBox
            // 
            this.systemVariablesFilePath_groupBox.Controls.Add(this.systemVariablesFilePath_tableLayoutPanel);
            this.systemVariablesFilePath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.systemVariablesFilePath_groupBox.Location = new System.Drawing.Point(3, 468);
            this.systemVariablesFilePath_groupBox.Name = "systemVariablesFilePath_groupBox";
            this.systemVariablesFilePath_groupBox.Size = new System.Drawing.Size(436, 79);
            this.systemVariablesFilePath_groupBox.TabIndex = 3;
            this.systemVariablesFilePath_groupBox.TabStop = false;
            this.systemVariablesFilePath_groupBox.Text = "Файл со списком системных переменных";
            // 
            // systemVariablesFilePath_tableLayoutPanel
            // 
            this.systemVariablesFilePath_tableLayoutPanel.ColumnCount = 1;
            this.systemVariablesFilePath_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.systemVariablesFilePath_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.systemVariablesFilePath_tableLayoutPanel.Controls.Add(this.isDefault_systemVariablesFilePath_checkBox, 0, 0);
            this.systemVariablesFilePath_tableLayoutPanel.Controls.Add(this.systemVariablesFilePath_textBox, 0, 1);
            this.systemVariablesFilePath_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.systemVariablesFilePath_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.systemVariablesFilePath_tableLayoutPanel.Name = "systemVariablesFilePath_tableLayoutPanel";
            this.systemVariablesFilePath_tableLayoutPanel.RowCount = 2;
            this.systemVariablesFilePath_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.systemVariablesFilePath_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.systemVariablesFilePath_tableLayoutPanel.Size = new System.Drawing.Size(430, 60);
            this.systemVariablesFilePath_tableLayoutPanel.TabIndex = 4;
            // 
            // isDefault_systemVariablesFilePath_checkBox
            // 
            this.isDefault_systemVariablesFilePath_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isDefault_systemVariablesFilePath_checkBox.AutoSize = true;
            this.systemVariablesFilePath_tableLayoutPanel.SetColumnSpan(this.isDefault_systemVariablesFilePath_checkBox, 2);
            this.isDefault_systemVariablesFilePath_checkBox.Location = new System.Drawing.Point(3, 6);
            this.isDefault_systemVariablesFilePath_checkBox.Name = "isDefault_systemVariablesFilePath_checkBox";
            this.isDefault_systemVariablesFilePath_checkBox.Size = new System.Drawing.Size(424, 17);
            this.isDefault_systemVariablesFilePath_checkBox.TabIndex = 3;
            this.isDefault_systemVariablesFilePath_checkBox.Text = "По умолчанию";
            this.isDefault_systemVariablesFilePath_checkBox.UseVisualStyleBackColor = true;
            this.isDefault_systemVariablesFilePath_checkBox.CheckedChanged += new System.EventHandler(this.isDefault_systemVariablesFilePath_checkBox_CheckedChanged);
            // 
            // systemVariablesFilePath_textBox
            // 
            this.systemVariablesFilePath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.systemVariablesFilePath_textBox.Location = new System.Drawing.Point(3, 33);
            this.systemVariablesFilePath_textBox.MaximumSize = new System.Drawing.Size(0, 24);
            this.systemVariablesFilePath_textBox.MinimumSize = new System.Drawing.Size(0, 24);
            this.systemVariablesFilePath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.systemVariablesFilePath_textBox.Name = "systemVariablesFilePath_textBox";
            this.systemVariablesFilePath_textBox.Size = new System.Drawing.Size(424, 24);
            this.systemVariablesFilePath_textBox.TabIndex = 4;
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.systemVariablesFilePath_groupBox, 0, 5);
            this.settings_tableLayoutPanel.Controls.Add(this.sensors_groupBox, 0, 1);
            this.settings_tableLayoutPanel.Controls.Add(this.systemFunctionsFilePath_groupBox, 0, 4);
            this.settings_tableLayoutPanel.Controls.Add(this.vb_groupBox, 0, 2);
            this.settings_tableLayoutPanel.Controls.Add(this.vbs_groupBox, 0, 3);
            this.settings_tableLayoutPanel.Controls.Add(this.level_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 6;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(442, 550);
            this.settings_tableLayoutPanel.TabIndex = 4;
            // 
            // vb_groupBox
            // 
            this.vb_groupBox.Controls.Add(this.vb_tableLayoutPanel);
            this.vb_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vb_groupBox.Location = new System.Drawing.Point(3, 143);
            this.vb_groupBox.Name = "vb_groupBox";
            this.vb_groupBox.Size = new System.Drawing.Size(436, 114);
            this.vb_groupBox.TabIndex = 19;
            this.vb_groupBox.TabStop = false;
            this.vb_groupBox.Text = "Visual Basic";
            // 
            // vb_tableLayoutPanel
            // 
            this.vb_tableLayoutPanel.ColumnCount = 1;
            this.vb_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.vb_tableLayoutPanel.Controls.Add(this.vbSensorFunctionText_groupBox, 0, 1);
            this.vb_tableLayoutPanel.Controls.Add(this.isVB_checkBox, 0, 0);
            this.vb_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vb_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.vb_tableLayoutPanel.Name = "vb_tableLayoutPanel";
            this.vb_tableLayoutPanel.RowCount = 2;
            this.vb_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.vb_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.vb_tableLayoutPanel.Size = new System.Drawing.Size(430, 95);
            this.vb_tableLayoutPanel.TabIndex = 0;
            // 
            // vbSensorFunctionText_groupBox
            // 
            this.vbSensorFunctionText_groupBox.Controls.Add(this.vbSensorFunctionText_textBox);
            this.vbSensorFunctionText_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vbSensorFunctionText_groupBox.Location = new System.Drawing.Point(3, 33);
            this.vbSensorFunctionText_groupBox.Name = "vbSensorFunctionText_groupBox";
            this.vbSensorFunctionText_groupBox.Size = new System.Drawing.Size(424, 59);
            this.vbSensorFunctionText_groupBox.TabIndex = 4;
            this.vbSensorFunctionText_groupBox.TabStop = false;
            this.vbSensorFunctionText_groupBox.Text = "Текст функции датчика";
            // 
            // vbSensorFunctionText_textBox
            // 
            this.vbSensorFunctionText_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vbSensorFunctionText_textBox.Location = new System.Drawing.Point(3, 16);
            this.vbSensorFunctionText_textBox.Multiline = true;
            this.vbSensorFunctionText_textBox.Name = "vbSensorFunctionText_textBox";
            this.vbSensorFunctionText_textBox.Size = new System.Drawing.Size(418, 40);
            this.vbSensorFunctionText_textBox.TabIndex = 0;
            // 
            // isVB_checkBox
            // 
            this.isVB_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isVB_checkBox.AutoSize = true;
            this.isVB_checkBox.Location = new System.Drawing.Point(3, 6);
            this.isVB_checkBox.Name = "isVB_checkBox";
            this.isVB_checkBox.Size = new System.Drawing.Size(424, 17);
            this.isVB_checkBox.TabIndex = 3;
            this.isVB_checkBox.Text = "Включить обработку файлов Visual Basic";
            this.isVB_checkBox.UseVisualStyleBackColor = true;
            this.isVB_checkBox.CheckedChanged += new System.EventHandler(this.isVB_checkBox_CheckedChanged);
            // 
            // vbs_groupBox
            // 
            this.vbs_groupBox.Controls.Add(this.vbs_tableLayoutPanel);
            this.vbs_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vbs_groupBox.Location = new System.Drawing.Point(3, 263);
            this.vbs_groupBox.Name = "vbs_groupBox";
            this.vbs_groupBox.Size = new System.Drawing.Size(436, 114);
            this.vbs_groupBox.TabIndex = 20;
            this.vbs_groupBox.TabStop = false;
            this.vbs_groupBox.Text = "Visual Basic Script";
            // 
            // vbs_tableLayoutPanel
            // 
            this.vbs_tableLayoutPanel.ColumnCount = 1;
            this.vbs_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.vbs_tableLayoutPanel.Controls.Add(this.isVBS_checkBox, 0, 0);
            this.vbs_tableLayoutPanel.Controls.Add(this.vbsSensorFunctionText_groupBox, 0, 1);
            this.vbs_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vbs_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.vbs_tableLayoutPanel.Name = "vbs_tableLayoutPanel";
            this.vbs_tableLayoutPanel.RowCount = 2;
            this.vbs_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.vbs_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.vbs_tableLayoutPanel.Size = new System.Drawing.Size(430, 95);
            this.vbs_tableLayoutPanel.TabIndex = 5;
            // 
            // isVBS_checkBox
            // 
            this.isVBS_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isVBS_checkBox.AutoSize = true;
            this.isVBS_checkBox.Location = new System.Drawing.Point(3, 6);
            this.isVBS_checkBox.Name = "isVBS_checkBox";
            this.isVBS_checkBox.Size = new System.Drawing.Size(424, 17);
            this.isVBS_checkBox.TabIndex = 3;
            this.isVBS_checkBox.Text = "Включить обработку файлов Visual Basic Script";
            this.isVBS_checkBox.UseVisualStyleBackColor = true;
            this.isVBS_checkBox.CheckedChanged += new System.EventHandler(this.isVBS_checkBox_CheckedChanged);
            // 
            // vbsSensorFunctionText_groupBox
            // 
            this.vbsSensorFunctionText_groupBox.Controls.Add(this.vbsSensorFunctionText_textBox);
            this.vbsSensorFunctionText_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vbsSensorFunctionText_groupBox.Location = new System.Drawing.Point(3, 33);
            this.vbsSensorFunctionText_groupBox.Name = "vbsSensorFunctionText_groupBox";
            this.vbsSensorFunctionText_groupBox.Size = new System.Drawing.Size(424, 59);
            this.vbsSensorFunctionText_groupBox.TabIndex = 4;
            this.vbsSensorFunctionText_groupBox.TabStop = false;
            this.vbsSensorFunctionText_groupBox.Text = "Текст функции датчика";
            // 
            // vbsSensorFunctionText_textBox
            // 
            this.vbsSensorFunctionText_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vbsSensorFunctionText_textBox.Location = new System.Drawing.Point(3, 16);
            this.vbsSensorFunctionText_textBox.Multiline = true;
            this.vbsSensorFunctionText_textBox.Name = "vbsSensorFunctionText_textBox";
            this.vbsSensorFunctionText_textBox.Size = new System.Drawing.Size(418, 40);
            this.vbsSensorFunctionText_textBox.TabIndex = 0;
            // 
            // level_groupBox
            // 
            this.level_groupBox.Controls.Add(this.level_tableLayoutPanel);
            this.level_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_groupBox.Location = new System.Drawing.Point(3, 3);
            this.level_groupBox.Name = "level_groupBox";
            this.level_groupBox.Size = new System.Drawing.Size(436, 49);
            this.level_groupBox.TabIndex = 21;
            this.level_groupBox.TabStop = false;
            this.level_groupBox.Text = "Уровень вставки датчиков";
            // 
            // level_tableLayoutPanel
            // 
            this.level_tableLayoutPanel.ColumnCount = 4;
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Controls.Add(this.level0_radioButton, 2, 0);
            this.level_tableLayoutPanel.Controls.Add(this.level3_radioButton, 1, 0);
            this.level_tableLayoutPanel.Controls.Add(this.level2_radioButton, 0, 0);
            this.level_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.level_tableLayoutPanel.Name = "level_tableLayoutPanel";
            this.level_tableLayoutPanel.RowCount = 1;
            this.level_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Size = new System.Drawing.Size(430, 30);
            this.level_tableLayoutPanel.TabIndex = 0;
            // 
            // level0_radioButton
            // 
            this.level0_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.level0_radioButton.AutoSize = true;
            this.level0_radioButton.Location = new System.Drawing.Point(289, 6);
            this.level0_radioButton.Name = "level0_radioButton";
            this.level0_radioButton.Size = new System.Drawing.Size(137, 17);
            this.level0_radioButton.TabIndex = 2;
            this.level0_radioButton.TabStop = true;
            this.level0_radioButton.Text = "Не вставлять датчики";
            this.level0_radioButton.UseVisualStyleBackColor = true;
            this.level0_radioButton.CheckedChanged += new System.EventHandler(this.level0_radioButton_CheckedChanged);
            // 
            // level3_radioButton
            // 
            this.level3_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.level3_radioButton.AutoSize = true;
            this.level3_radioButton.Location = new System.Drawing.Point(146, 6);
            this.level3_radioButton.Name = "level3_radioButton";
            this.level3_radioButton.Size = new System.Drawing.Size(137, 17);
            this.level3_radioButton.TabIndex = 1;
            this.level3_radioButton.TabStop = true;
            this.level3_radioButton.Text = "3-й уровень";
            this.level3_radioButton.UseVisualStyleBackColor = true;
            // 
            // level2_radioButton
            // 
            this.level2_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.level2_radioButton.AutoSize = true;
            this.level2_radioButton.Location = new System.Drawing.Point(3, 6);
            this.level2_radioButton.Name = "level2_radioButton";
            this.level2_radioButton.Size = new System.Drawing.Size(137, 17);
            this.level2_radioButton.TabIndex = 0;
            this.level2_radioButton.TabStop = true;
            this.level2_radioButton.Text = "2-й уровень";
            this.level2_radioButton.UseVisualStyleBackColor = true;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(442, 550);
            this.sensors_groupBox.ResumeLayout(false);
            this.sensors_tableLayoutPanel.ResumeLayout(false);
            this.sensors_tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).EndInit();
            this.systemFunctionsFilePath_groupBox.ResumeLayout(false);
            this.systemFunctionsFilePath_tableLayoutPanel.ResumeLayout(false);
            this.systemFunctionsFilePath_tableLayoutPanel.PerformLayout();
            this.systemVariablesFilePath_groupBox.ResumeLayout(false);
            this.systemVariablesFilePath_tableLayoutPanel.ResumeLayout(false);
            this.systemVariablesFilePath_tableLayoutPanel.PerformLayout();
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.vb_groupBox.ResumeLayout(false);
            this.vb_tableLayoutPanel.ResumeLayout(false);
            this.vb_tableLayoutPanel.PerformLayout();
            this.vbSensorFunctionText_groupBox.ResumeLayout(false);
            this.vbSensorFunctionText_groupBox.PerformLayout();
            this.vbs_groupBox.ResumeLayout(false);
            this.vbs_tableLayoutPanel.ResumeLayout(false);
            this.vbs_tableLayoutPanel.PerformLayout();
            this.vbsSensorFunctionText_groupBox.ResumeLayout(false);
            this.vbsSensorFunctionText_groupBox.PerformLayout();
            this.level_groupBox.ResumeLayout(false);
            this.level_tableLayoutPanel.ResumeLayout(false);
            this.level_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox sensors_groupBox;
        private System.Windows.Forms.Label sensorText_label;
        private System.Windows.Forms.TextBox sensorText_textBox;
        private System.Windows.Forms.Label firstSensorNumber_label;
        private System.Windows.Forms.NumericUpDown firstSensorNumber_numericUpDown;
        private System.Windows.Forms.GroupBox systemFunctionsFilePath_groupBox;
        private System.Windows.Forms.GroupBox systemVariablesFilePath_groupBox;
        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox vb_groupBox;
        private System.Windows.Forms.GroupBox vbSensorFunctionText_groupBox;
        private System.Windows.Forms.TextBox vbSensorFunctionText_textBox;
        private System.Windows.Forms.CheckBox isVB_checkBox;
        private System.Windows.Forms.GroupBox vbs_groupBox;
        private System.Windows.Forms.GroupBox vbsSensorFunctionText_groupBox;
        private System.Windows.Forms.TextBox vbsSensorFunctionText_textBox;
        private System.Windows.Forms.CheckBox isVBS_checkBox;
        private System.Windows.Forms.CheckBox isDefault_systemFunctionsFilePath_checkBox;
        private System.Windows.Forms.CheckBox isDefault_systemVariablesFilePath_checkBox;
        private System.Windows.Forms.GroupBox level_groupBox;
        private System.Windows.Forms.RadioButton level0_radioButton;
        private System.Windows.Forms.RadioButton level3_radioButton;
        private System.Windows.Forms.RadioButton level2_radioButton;
        private System.Windows.Forms.TableLayoutPanel level_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel sensors_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel systemFunctionsFilePath_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel systemVariablesFilePath_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel vb_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel vbs_tableLayoutPanel;
        private Controls.TextBoxWithDialogButton systemFunctionsFilePath_textBox;
        private Controls.TextBoxWithDialogButton systemVariablesFilePath_textBox;
    }
}
