﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestUtils;

namespace TestVBSParser
{
    [TestClass]
    public class TestVBSParser
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Возвращает количество перехваченных сообщений
            /// </summary>
            /// <returns>Количество сообщений</returns>
            public int Count()
            {
                return messages.Count;
            }
        }

        /// <summary>
        /// Перехватчик сообщений из монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        private const ulong pluginID = Store.Const.PluginIdentifiers.VB_PARSER;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

/*       
        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже присутствует в лабораторных тексах.
        /// </summary>
        [TestMethod]
        public void PhpParser_WrongFirstSensor_InLabs()
        {
            VBParser testPlugin = new VBParser();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                       //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    Function func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);

                    SettingUpPlugin(storage, firstSensorNumber: 1);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <1> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <2>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика не фигурировал в исходных файлах, 
        /// но оказался меньше, чем максимальный идентификатор в Хранилище.
        /// </summary>
        [TestMethod]
        public void PhpParser_WrongFirstSensor_NotInLabs()
        {
            VBParser testPlugin = new VBParser();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    Function func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(3, func1.Id, Kind.START);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);

                    SettingUpPlugin(storage, firstSensorNumber: 2);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже фигурировал в исходных файлах, но был удален.
        /// </summary>
        [TestMethod]
        public void PhpParser_WrongFirstSensor_NotInLabsDeleted()
        {
            VBParser testPlugin = new VBParser();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    Function func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);
                    storage.sensors.AddSensor(2, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(3, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);

                    storage.sensors.RemoveSensor(2);

                    SettingUpPlugin(storage, firstSensorNumber: 2);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        #region VBParser_EuphratVBS
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах Евфрата, написанных на языке Visual Basic Script.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Датчики вставляются не во все файлы
        /// </summary>
        [TestMethod]
        public void VBParser_EuphratVBS()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\Euphrat\VBS\";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {                   
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasicScript");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "EuphratVBS");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_EuphratVB
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах Евфрата, написанных на языке Visual Basic.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Тест вылетает с системной ошибкой vshost
        /// </summary>
        //[TestMethod]
        //public void VBParser_EuphratVB()
        //{
        //    const string sourcePrefixPart = @"Sources\VB(S)\Euphrat\VB\";
        //    testUtils.RunTest(
        //        sourcePrefixPart,                                                   // sourcePrefixPart
        //        new IA.Plugins.Parsers.VBParser.VBParser(),            // Plugin
        //        false,                                                              // isUseCachedStorage
        //        (storage, testMaterialsPath) => { },                                // prepareStorage
        //        (storage, testMaterialsPath) =>
        //        {
        //            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
        //            TestUtilsClass.Run_IdentifyFileTypes(storage);
        //        },                                                                  // customizeStorage
        //        (storage, testMaterialsPath) =>
        //        {
        //            string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
        //            string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "EuphratVB");

        //            return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
        //        },                                                                  // checkStorage
        //        (reportsPath, testMaterialsPath) =>
        //        {
        //            return true;
        //        },                                                                  // checkReportBeforeRerun
        //        false,                                                              // isUpdateReport
        //        (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
        //        (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
        //        (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
        //        );
        //}
        #endregion

        #region VBParser_EuphratVBS3lvl
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах Евфрата, написанных на языке Visual Basic Script.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются вручную. Датчики вставляются по 3 уровню контроля.
        /// Датчики вставляются не во все файлы.
        /// </summary>
        [TestMethod]
        public void VBParser_EuphratVBS3lvl()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\Euphrat\VBS\";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);

                    //выбор настроек
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();

                    writer.Add(3); // Level
                    writer.Add(1);  // FirstSensorNumber
                    writer.Add("sensor(#)"); // SensorText 
                    writer.Add(false); // IsVB                   
                    writer.Add(""); // VBSensorFunctionText
                    writer.Add(true); // IsVBS
                    string[] vbsText = 
                    {
                        "<?xml version=\"1.0\" encoding=\"utf-16\"?>",
                        "<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">",
                        "<string>Private Sub sensor(n)</string>",
                        "<string>Dim WinAPI: Set WinAPI = CreateObject(&amp;quot;DynamicWrapperX&amp;quot;)</string>",
                        "<string>WinAPI.Register &amp;quot;KERNEL32.DLL&amp;quot;, &amp;quot;GetCurrentThreadId&amp;quot;, &amp;quot;r=u&amp;quot;</string>",
                        "<string>Dim fso: Set fso = CreateObject(&amp;quot;Scripting.FileSystemObject&amp;quot;)</string>",
                        "<string>Dim tf: Set tf = fso.OpenTextFile(&amp;quot;c:\vbs_traces_&amp;quot; &amp;amp; WinAPI.GetCurrentThreadId &amp;amp; &amp;quot;.txt&amp;quot;,8,true)</string>",
                        "<string>tf.WriteLine(n)</string>",
                        "<string>tf.Close</string>",
                        "<string>End Sub</string>",
                        "</ArrayOfString>"
                    };
                    writer.Add(string.Join("\n", vbsText)); // VBSSensorFunctionText
                    writer.Add(true); // IsDefault_SystemFunctionsFilePath
                    writer.Add(""); // SystemFunctionsFilePath
                    writer.Add(true); //IsDefault_SystemVariablesFilePath
                    writer.Add(""); // SystemVariablesFilePath

                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.p_VB_VBS, writer);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasicScript");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "EuphratVBS3lvl");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_EuphratVB3lvl
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах Евфрата, написанных на языке Visual Basic.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются вручную. Датчики вставляются по 3 уровню контроля.
        /// Тест вылетает с системной ошибкой vshost
        /// </summary>
        //[TestMethod]
        //public void VBParser_EuphratVB3lvl()
        //{
        //    const string sourcePrefixPart = @"Sources\VB(S)\Euphrat\VB\";
        //    testUtils.RunTest(
        //        sourcePrefixPart,                                                   // sourcePrefixPart
        //        new IA.Plugins.Parsers.VBParser.VBParser(),            // Plugin
        //        false,                                                              // isUseCachedStorage
        //        (storage, testMaterialsPath) => { },                                // prepareStorage
        //        (storage, testMaterialsPath) =>
        //        {
        //            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
        //            TestUtilsClass.Run_IdentifyFileTypes(storage);

        //            //выбор настроек
        //            Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();

        //            writer.Add(3); // Level
        //            writer.Add(1);  // FirstSensorNumber
        //            writer.Add("sensor(#)"); // SensorText 
        //            writer.Add(true); // IsVB
        //            string[] vbText = 
        //            {
        //                "<?xml version=\"1.0\" encoding=\"utf-16\"?>",
        //                "<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">",
        //                "<string>Private Sub sensor(n)</string>",
        //                "<string>Dim MyFile</string>",
        //                "<string>MyFile = FreeFile</string>",
        //                "<string>Open (\"C:\vb_log_\" &amp; App.ThreadID &amp; \".log\") For Append As #MyFile</string>",
        //                "<string>Write #MyFile, n</string>",
        //                "<string>Close #MyFile</string>",
        //                "<string>End Sub</string>",
        //                "</ArrayOfString>"
        //            };
        //            writer.Add(string.Join("\n", vbText)); // VBSensorFunctionText
        //            writer.Add(false); // IsVB
        //            string[] vbsText = 
        //            {
        //                "<?xml version=\"1.0\" encoding=\"utf-16\"?>",
        //                "<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">",
        //                "<string>Private Sub sensor(n)</string>",
        //                "<string>Dim WinAPI: Set WinAPI = CreateObject(&amp;quot;DynamicWrapperX&amp;quot;)</string>",
        //                "<string>WinAPI.Register &amp;quot;KERNEL32.DLL&amp;quot;, &amp;quot;GetCurrentThreadId&amp;quot;, &amp;quot;r=u&amp;quot;</string>",
        //                "<string>Dim fso: Set fso = CreateObject(&amp;quot;Scripting.FileSystemObject&amp;quot;)</string>",
        //                "<string>Dim tf: Set tf = fso.OpenTextFile(&amp;quot;c:\vbs_traces_&amp;quot; &amp;amp; WinAPI.GetCurrentThreadId &amp;amp; &amp;quot;.txt&amp;quot;,8,true)</string>",
        //                "<string>tf.WriteLine(n)</string>",
        //                "<string>tf.Close</string>",
        //                "<string>End Sub</string>",
        //                "</ArrayOfString>"
        //            };
        //            writer.Add(string.Join("\n", vbsText)); // VBSSensorFunctionText
        //            writer.Add(true); // IsDefault_SystemFunctionsFilePath
        //            writer.Add(""); // SystemFunctionsFilePath
        //            writer.Add(true); //IsDefault_SystemVariablesFilePath
        //            writer.Add(""); // SystemVariablesFilePath

        //            storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.p_VB_VBS, writer);
        //        },                                                                  // customizeStorage
        //        (storage, testMaterialsPath) =>
        //        {
        //            string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
        //            string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "EuphratVB3lvl");

        //            return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
        //        },                                                                  // checkStorage
        //        (reportsPath, testMaterialsPath) =>
        //        {
        //            return true;
        //        },                                                                  // checkReportBeforeRerun
        //        false,                                                              // isUpdateReport
        //        (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
        //        (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
        //        (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
        //        );
        //}
        #endregion

        #region VBParser_InternetProject_1
        /// <summary>
        /// Проверка корректности отработки плагина на текстах программ, взятых из Интернет (Project_1) и написанных на языке Visual Basic.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Датчики вставляются не во все файлы
        /// </summary>
        [TestMethod]
        public void VBParser_InternetProject_1()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\InternetProjects\Project_1";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "Internet\\Project_1");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_InternetProject_2
        /// <summary>
        /// Проверка корректности отработки плагина на текстах программ, взятых из Интернет (Project_2) и написанных на языке Visual Basic.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Датчики вставляются не во все файлы
        /// </summary>
        [TestMethod]
        public void VBParser_InternetProject_2()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\InternetProjects\Project_2";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "Internet\\Project_2");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion

        #region VBParser_TestVB_calls
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic с вызовами процедур и функций.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_calls()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\calls";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\calls");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_classes
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой Class.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_classes()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\classes";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\classes");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_do_while
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой do_while.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_do_while()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\do_while";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\do_while");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_for_foreach
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой for_foreach.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_for_foreach()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\for_foreach";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\for_foreach");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_goto
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой goto.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_goto()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\goto";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\goto");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_if_then_else
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой if_then_else.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_if_then_else()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\if_then_else";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\if_then_else");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_module
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой Module.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_module()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\module";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\module");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_namespaces
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой Namespace.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_namespaces()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\namespaces";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\namespaces");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_property
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой Property.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_property()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\property";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\property");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_return
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой Return.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_return()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\return";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\return");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_select_case
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой Select_Case.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_select_case()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\select";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\select");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_throw
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой Throw.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_throw()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\throw";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\throw");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_try_catch_finally
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой Try_Catch_Finally.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_try_catch_finally()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\try";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\try");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVB_using
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах языка Visual Basic со структурой Using.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVB_using()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVB\using";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasic");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVB\\using");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion

        #region VBParser_TestVBS_Kaspersky
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах, написанных на языке Visual Basic Script.
        /// Файлы были взяты из скриптов Kaspersky Internet Security.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVBS_Kaspersky()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVBS\Kaspersky";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasicScript");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVBS\\Kaspersky");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVBS_Printer
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах, написанных на языке Visual Basic Script.
        /// Файлы были взяты из скриптов настройки принтера.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Тест вылетает с системной ошибкой vshost.
        /// </summary>
        //[TestMethod]
        //public void VBParser_TestVBS_Printer()
        //{
        //    const string sourcePrefixPart = @"Sources\VB(S)\TestVBS\Printer";
        //    testUtils.RunTest(
        //        sourcePrefixPart,                                                   // sourcePrefixPart
        //        new IA.Plugins.Parsers.VBParser.VBParser(),            // Plugin
        //        false,                                                              // isUseCachedStorage
        //        (storage, testMaterialsPath) => { },                                // prepareStorage
        //        (storage, testMaterialsPath) =>
        //        {
        //            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
        //            TestUtilsClass.Run_IdentifyFileTypes(storage);
        //        },                                                                  // customizeStorage
        //        (storage, testMaterialsPath) =>
        //        {
        //            string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasicScript");
        //            string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVBS\\Printer");

        //            return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
        //        },                                                                  // checkStorage
        //        (reportsPath, testMaterialsPath) =>
        //        {
        //            return true;
        //        },                                                                  // checkReportBeforeRerun
        //        false,                                                              // isUpdateReport
        //        (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
        //        (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
        //        (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
        //        );
        //}
        #endregion
        #region VBParser_TestVBS_SVN
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах, написанных на языке Visual Basic Script.
        /// Файлы были взяты из скриптов Turtoise SVN.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Не ставятся датчики перед концом функции или процедуры.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVBS_SVN()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVBS\SVN";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasicScript");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVBS\\SVN");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region VBParser_TestVBS_Windows
        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах, написанных на языке Visual Basic Script.
        /// Файлы были взяты из скриптов Windows.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// Настройки плагина задаются автоматически по умолчанию. Датчики вставляются по 2 уровню контроля.
        /// Ошибки парсера. Файлы с датчиками не формируются.
        /// </summary>
        [TestMethod]
        public void VBParser_TestVBS_Windows()
        {
            const string sourcePrefixPart = @"Sources\VB(S)\TestVBS\Windows";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Parsers.VBParser.VBParser(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "VisualBasicScript");
                    string reportsPath = Path.Combine(testMaterialsPath, "VBSParser", "TestVBS\\Windows");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
 * 
        /// <summary>
        /// Настройка плагина
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="firstSensorNumber">Номер первого датчика. Не может быть отрицательным.</param>
        /// <param name="isLevel2">true - 2-ой уровень.</param>
        private void SettingUpPlugin(
            Storage storage,
            uint level = 2,
            ulong firstSensorNumber = 1,
            string sensorText = "sensor(#)",
            bool isVB = true,
            string vbSensorFunctionText = "Private Sub sensor(n)\nDim MyFile\nMyFile = FreeFile\nOpen (\"C:\\vb_log_\\\" & App.ThreadID & \".log\") For Append As #MyFile\nWrite #MyFile, n\nClose #MyFile\nEnd Sub",
            bool isVBS = true,
            string vbsSensorFunctionText = "Private Sub sensor(n)\nDim WinAPI: Set WinAPI = CreateObject(&quot;DynamicWrapperX&quot;)\nWinAPI.Register &quot;KERNEL32.DLL&quot;, &quot;GetCurrentThreadId&quot;, &quot;r=u&quot;\nDim fso: Set fso = CreateObject(&quot;Scripting.FileSystemObject&quot;)\nDim tf: Set tf = fso.OpenTextFile(&quot;c:\vbs_traces_&quot; &amp; WinAPI.GetCurrentThreadId &amp; &quot;.txt&quot;,8,true)\ntf.WriteLine(n)\ntf.Close\nEnd Sub",
            bool isDefault_systemFunctionsFilePath = true,
            string systemFunctionsFilePath = null,
            bool isDefault_systemVariablesFilePath = true,
            string systemVariablesFilePath = null
            )
        {
            //Формируем буфер настроек плагина
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(level);
            buffer.Add(firstSensorNumber);
            buffer.Add(sensorText);
            buffer.Add(isVB);
            buffer.Add(vbSensorFunctionText);
            buffer.Add(isVBS);
            buffer.Add(vbsSensorFunctionText);
            buffer.Add(isDefault_systemFunctionsFilePath);
            buffer.Add(systemFunctionsFilePath);
            buffer.Add(isDefault_systemVariablesFilePath);
            buffer.Add(systemVariablesFilePath);

            storage.pluginSettings.SaveSettings(pluginID, buffer);
        }

        /// <summary>
        /// Проверка ожидаемого сообщения в логе монитора
        /// </summary>
        /// <param name="message">Текст сообщения. Не может быть пустым.</param>
        private void CheckMessage(string message)
        {
            bool isContainMessage = listener.ContainsPart(message);

            Assert.IsTrue(isContainMessage, "Сообщение не совпадает с ожидаемым: <" + message + ">.");
        }
 */
    }
}
