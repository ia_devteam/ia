﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using IA.Extensions;

using IOController;

using Store;
using Store.Table;

namespace IA.Plugins.Analyses.GraphGenerator
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Формирование блок-схем и диаграмм";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.GRAPH_GENERATOR;

        /// <summary>
        /// Наименования тасков плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Формирование графов по функциям")]
            FUNCTION_GRAPHS_GENERATION,
            [Description("Формирование графов по операциям")]
            STATEMENT_GRAPHS_GENERATION,
        };

        /// <summary>
        /// Наименования тасков плагина при генерации отчётов
        /// </summary>
        internal enum TasksReport
        {
            [Description("Формирование легенды")]
            LEGEND_GENERATION,
            [Description("Формирование изображений графов по функциям")]
            FUNCTION_GRAPHS_IMAGES_GENERATION,
            [Description("Формирование изображений графов по операциям")]
            STATEMENT_GRAPHS_IMAGES_GENERATION,
        };

        internal static bool generateAllDot;
    }

    /// <summary>
    /// Плагин - Формирование блок-схем и диаграмм
    /// </summary>
    public class GraphGenerator : IA.Plugin.Interface
    {
        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        private Storage storage = null;

        #region Константы
        /// <summary>
        /// Имя директории, содержащей файлы локальных и глобального графов управления по функциям
        /// </summary>
        public const string FunctionGraphsDirectoryName = "Функции";

        /// <summary>
        /// Имя директории, содержащей файлы графов упралвения по операциям
        /// </summary>
        public const string StatementGraphsDirectoryName = "Операции";

        /// <summary>
        /// Расширение файла, содержащего граф
        /// </summary>
        public const string GraphFileExtension = ".dot";

        /// <summary>
        /// Расширение файла, содержащего изображение графа
        /// </summary>
        public const string GraphImageFileExtension = ".png";
        #endregion

        /// <summary>
        /// Путь до директории, содержащей файлы локальных и глобального графов управления по функциям
        /// </summary>
        private string functionGraphsDirectoryPath = null;
        
        /// <summary>
        /// Путь до директории, содержащей файлы графов упралвения по операциям
        /// </summary>
        private string statementGraphsDirectoryPath = null;

        #region IA.Plugin.Interface
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public IA.Plugin.Capabilities Capabilities
        {
            get
            {
                return IA.Plugin.Capabilities.RERUN | IA.Plugin.Capabilities.REPORTS | Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        Settings settingsForm;

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                if (settingsForm == null)
                    settingsForm = new Settings(PluginSettings.generateAllDot);
                return settingsForm;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(
                        PluginSettings.Name,
                        PluginSettings.Tasks.FUNCTION_GRAPHS_GENERATION.Description()
                    ),
                    new Monitor.Tasks.Task(
                        PluginSettings.Name,
                        PluginSettings.Tasks.STATEMENT_GRAPHS_GENERATION.Description()
                    )
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(
                        PluginSettings.Name,
                        PluginSettings.TasksReport.LEGEND_GENERATION.Description()
                    ),
                    new Monitor.Tasks.Task(
                        PluginSettings.Name,
                        PluginSettings.TasksReport.FUNCTION_GRAPHS_IMAGES_GENERATION.Description()
                    ),
                    new Monitor.Tasks.Task(
                        PluginSettings.Name,
                        PluginSettings.TasksReport.STATEMENT_GRAPHS_IMAGES_GENERATION.Description()
                    )
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        public void Initialize(Store.Storage storage)
        {
            //Запоминаем Храниилище для работы
            this.storage = storage;

            //Получаем путь до директории с данными плагина
            string dataDirectoryPath = storage.pluginData.GetDataFolder(PluginSettings.ID);

            //Получаем необходимые пути
            this.functionGraphsDirectoryPath = Path.Combine(dataDirectoryPath, FunctionGraphsDirectoryName);
            this.statementGraphsDirectoryPath = Path.Combine(dataDirectoryPath, StatementGraphsDirectoryName);
            PluginSettings.generateAllDot = Properties.Settings.Default.generateAllDot;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            if(settingsBuffer != null)
                settingsBuffer.GetBool(ref PluginSettings.generateAllDot);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        public void SetSimpleSettings(string settingsString)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            if (settingsForm != null)
            {
                PluginSettings.generateAllDot = settingsForm.generateAllDot;
            }
            Properties.Settings.Default.generateAllDot = PluginSettings.generateAllDot;
            Properties.Settings.Default.Save();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        public bool IsSettingsCorrect(out string message)
        {
            message = String.Empty;
            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            //Получаем набор функций из Хранилища
            IFunction[] functions = storage.functions.EnumerateFunctions(enFunctionKind.REAL).ToArray();

            //Счётчик прогресса
            ulong counter;

            //Задаём максимальный прогресс
            Monitor.Tasks.Update(
                PluginSettings.Name,
                PluginSettings.Tasks.FUNCTION_GRAPHS_GENERATION.Description(),
                Monitor.Tasks.Task.UpdateType.STAGES,
                (ulong)functions.Length + 1
            );


            //Задаём максимальный прогресс
            Monitor.Tasks.Update(
                PluginSettings.Name,
                PluginSettings.Tasks.STATEMENT_GRAPHS_GENERATION.Description(),
                Monitor.Tasks.Task.UpdateType.STAGES,
                (ulong)functions.Length
            );

            #region Формирование графов по функциям

            //Создаём директорию для хранения глобального и локальных графов по функциям
            if (!DirectoryController.IsExists(this.functionGraphsDirectoryPath))
                DirectoryController.Create(this.functionGraphsDirectoryPath);

            //Строим локальные графы управления для каждой функции в Хранилище
            counter = 0;
            foreach (IFunction function in functions)
            {
                //Если функция определена
                if (function != null && function.kind != enFunctionKind.PSEUDO)
                {
                    //Формируем локальный граф управления по функции
                    DiGraphFunctionsLocal diGraphFunctionsLocal = new DiGraphFunctionsLocal(function);

                    //Если граф управления не пуст
                    if (!diGraphFunctionsLocal.IsEmpty)
                    {
                        if (function.FullNameForFileName == "ibs_dSig.Common.Misc.Cache<,>.Add_311")
                        {
                            int i = 0;
                        }
                        //Формируем путь до локального файла графа управления по функции
                        string funcName = function.FullNameForFileName;
                        string functionLocalGraphFilePath = Path.Combine(
                            this.functionGraphsDirectoryPath,
                            function.FullNameForFileName + GraphFileExtension
                        );

                        //Формируем файл локального графа управления по функции
                        File.WriteAllText(functionLocalGraphFilePath, diGraphFunctionsLocal.ToDotLanguage(), Encoding.UTF8);
                    }
                }

                //Обновляем текущий прогресс
                Monitor.Tasks.Update(
                    PluginSettings.Name,
                    PluginSettings.Tasks.FUNCTION_GRAPHS_GENERATION.Description(),
                    Monitor.Tasks.Task.UpdateType.STAGE,
                    ++counter
                );
            }

            if (PluginSettings.generateAllDot)
            {
                //Получаем путь до файла глобального графа управления по функциям
                string functionsGlobalGraphFilePath = Path.Combine(this.functionGraphsDirectoryPath, "All" + GraphFileExtension);

                //Формируем глобальный граф управления по функциям
                DiGraphFunctionsGlobal diGraphFunctionsGlobal = new DiGraphFunctionsGlobal(functions);

                //Формируем файл глобального графа управления по функциям
                File.WriteAllText(functionsGlobalGraphFilePath, diGraphFunctionsGlobal.ToDotLanguage(), Encoding.UTF8);
            }
            //Обновляем текущий прогресс
            Monitor.Tasks.Update(
                PluginSettings.Name,
                PluginSettings.Tasks.FUNCTION_GRAPHS_GENERATION.Description(),
                Monitor.Tasks.Task.UpdateType.STAGE,
                ++counter
            );
            #endregion

            #region Формирование графов по операциям
            //Создаём директорию для хранения графов управления по операциям
            if (!DirectoryController.IsExists(this.statementGraphsDirectoryPath))
                DirectoryController.Create(this.statementGraphsDirectoryPath);
            
            //Проходим по всем функциям в Хранилище
            counter = 0;
            foreach (IFunction function in functions)
            {
                //Если функция определена
                if (function != null)
                {
                    //Формируем граф управления
                    DiGraphStatements diGraphStatements = new DiGraphStatements(function);

                    //Если граф управления не пуст
                    if (!diGraphStatements.IsEmpty)
                    {
                        //Формируем путь до файла графа управления по опирациям для заданной функции
                        string statementsGraphFilePath = Path.Combine(
                            this.statementGraphsDirectoryPath,
                            function.FullNameForFileName + GraphFileExtension
                        );

                        //Фомрируем файл графа управления по операциям для заданной функции
                        File.WriteAllText(statementsGraphFilePath, diGraphStatements.ToDotLanguage());
                    }
                }

                //Обновляем текущий прогресс
                Monitor.Tasks.Update(
                    PluginSettings.Name,
                    PluginSettings.Tasks.STATEMENT_GRAPHS_GENERATION.Description(),
                    Monitor.Tasks.Task.UpdateType.STAGE,
                    ++counter
                );
            } 
            #endregion
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        public void GenerateReports(string reportsDirectoryPath)
        {
            //Получаем директорию для формирования изображений графов по функциям
            string functionsGraphImagesDirectoryPath = Path.Combine(reportsDirectoryPath, FunctionGraphsDirectoryName);

            //Создаём директорию для формирования изображений графов по функциям
            if (!DirectoryController.IsExists(functionsGraphImagesDirectoryPath))
                DirectoryController.Create(functionsGraphImagesDirectoryPath);

            //Получаем директорию для формирования изображений графов по операциям
            string statementsGraphImagesDirectoryPath = Path.Combine(reportsDirectoryPath, StatementGraphsDirectoryName);

            //Создаём директорию для формирования изображений графов по операциям
            if (!DirectoryController.IsExists(statementsGraphImagesDirectoryPath))
                DirectoryController.Create(statementsGraphImagesDirectoryPath);

            //Счётчик прогресса
            ulong counter;

            //Список файлов графов
            string[] graphFiles;

            IFunction[] functions = storage.functions.EnumerateFunctions(enFunctionKind.REAL).ToArray();

            //Задаём максимальный прогресс
            Monitor.Tasks.Update(
                PluginSettings.Name,
                PluginSettings.TasksReport.LEGEND_GENERATION.Description(),
                Monitor.Tasks.Task.UpdateType.STAGES,
                (ulong)functions.Length
            );

            //Получаем список файлов графов по функциям
            graphFiles = DirectoryController.GetFiles(this.functionGraphsDirectoryPath).Where(f => Path.GetExtension(f) == GraphFileExtension).ToArray();

            //Задаём максимальный прогресс
            Monitor.Tasks.Update(
                PluginSettings.Name,
                PluginSettings.TasksReport.FUNCTION_GRAPHS_IMAGES_GENERATION.Description(),
                Monitor.Tasks.Task.UpdateType.STAGES,
                (ulong)graphFiles.Length
            );

            //Задаём максимальный прогресс
            Monitor.Tasks.Update(
                PluginSettings.Name,
                PluginSettings.TasksReport.STATEMENT_GRAPHS_IMAGES_GENERATION.Description(),
                Monitor.Tasks.Task.UpdateType.STAGES,
                (ulong)graphFiles.Length
            );

            #region Формирование легенды
            //Получаем все функции в Хранилище

            //Задаём имя файла легенды
            const string legendFileName = "Functions.txt";

            //Получаем путь до файла легенды
            string legendFilePath = Path.Combine(functionsGraphImagesDirectoryPath, legendFileName);

            //Формируем легенду по функциям
            counter = 0;
            using (StreamWriter writer = new StreamWriter(legendFilePath))
            {
                //Проходим по всем функциям
                foreach (IFunction function in functions)
                {
                    if (function != null)
                    {
                        //Накапливаем информацию о функции. Начинаем с идентификатора.
                        List<string> functionInfo = new List<string>() { function.Id.ToString() };

                        //Добавляем в информацию о функции имя
                        functionInfo.Add(function.FullNameForReports ?? "<Имя неизвестно>");

                        //Получаем место определения функции
                        Location functionDefinition = function.Definition();

                        //Добавляем в информацию о функции место определения
                        if (functionDefinition == null)
                            functionInfo.Add("<Место определения неизвестно>");
                        else
                        {
                            functionInfo.Add(functionDefinition.GetFile().FileNameForReports);
                            functionInfo.Add(functionDefinition.GetLine().ToString());
                            functionInfo.Add(functionDefinition.GetColumn().ToString());
                        }

                        //Записываем накопленную информацию в файл
                        writer.WriteLine(String.Join("\t", functionInfo.ToArray()));
                    }

                    //Обновляем текущий прогресс
                    Monitor.Tasks.Update(
                        PluginSettings.Name,
                        PluginSettings.TasksReport.LEGEND_GENERATION.Description(),
                        Monitor.Tasks.Task.UpdateType.STAGE,
                        ++counter
                    );
                }
            }
            #endregion

            #region Формирование графов по функциям
            //Создаём директорию для формирования изображений графов по функциям
            if (!DirectoryController.IsExists(functionsGraphImagesDirectoryPath))
                DirectoryController.Create(functionsGraphImagesDirectoryPath);

            //Строим изображения всех графов
            counter = 0;
            foreach (string graphFile in graphFiles)
            {
                //Генерируем изображение графа
                DotGraphImageGenerator.Generate(
                    graphFile,Path.Combine(functionsGraphImagesDirectoryPath, Path.GetFileNameWithoutExtension(graphFile) + GraphImageFileExtension                    )
                );

                //Обновляем текущий прогресс
                Monitor.Tasks.Update(
                    PluginSettings.Name,
                    PluginSettings.TasksReport.FUNCTION_GRAPHS_IMAGES_GENERATION.Description(),
                    Monitor.Tasks.Task.UpdateType.STAGE,
                    ++counter
                );
            }
            #endregion

            #region Формирование графов по операциям
            //Строим изображения всех графов
            counter = 0;
            foreach (string graphFile in graphFiles)
            {
                //Генерируем изображение графа
                DotGraphImageGenerator.Generate(
                    graphFile,
                    Path.Combine(
                        statementsGraphImagesDirectoryPath,
                        Path.GetFileNameWithoutExtension(graphFile) + GraphImageFileExtension
                    )
                );

                //Обновляем текущий прогресс
                Monitor.Tasks.Update(
                    PluginSettings.Name,
                    PluginSettings.TasksReport.STATEMENT_GRAPHS_IMAGES_GENERATION.Description(),
                    Monitor.Tasks.Task.UpdateType.STAGE,
                    ++counter
                );
            }
            #endregion
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        } 
        #endregion


    }
}
