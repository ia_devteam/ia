﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IA.Plugins.Analyses.GraphGenerator
{
    /// <summary>
    /// Класс, реализующий узел графа
    /// </summary>
    /// <typeparam name="T">Тип узла графа</typeparam>
    internal class GraphNode<T> where T : IComparable<T>
    {
        /// <summary>
        /// Ключ узла
        /// </summary>
        internal T Key { get; private set; }

        /// <summary>
        /// Метка узла (отображается в графе)
        /// </summary>
        internal string Label { get; set; }

        /// <summary>
        /// Цвет узла
        /// </summary>
        internal string Color { get; set; }

        private HashSet<T> children = null;
        /// <summary>
        /// Дочерние узлы
        /// </summary>
        internal IEnumerable<T> Children
        {
            get
            {
                return this.children ?? Enumerable.Empty<T>();
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="key">Ключ узла</param>
        internal GraphNode(T key)
        {
            //Проверка на разумность
            if (key.CompareTo(default(T)) == 0)
                throw new Exception("Недопустимое значение ключа узла графа.");

            this.Key = key;
        }

        /// <summary>
        /// Добавить дочерний узел
        /// </summary>
        /// <param name="key">Ключ дочернего узла</param>
        internal void AddChild(T key)
        {
            //Проверка на разумнтсть
            if (key.CompareTo(default(T)) == 0)
                throw new Exception("Недопустимое значение ключа узла графа.");

            if(this.children == null)
                this.children = new HashSet<T>();

            this.children.Add(key);
        }

        /// <summary>
        /// Получть описание узла на языке DOT
        /// </summary>
        /// <returns>Описание узла на языке DOT. Не может быть Null.</returns>
        internal string ToDotLanguage()
        {
            string ret = String.Empty;

            //Добавляем описание узла
            ret += this.Key;

            if (!String.IsNullOrEmpty(this.Label))
                ret += "[label=\"" + this.Label + "\"]";

            if (!String.IsNullOrEmpty(this.Color))
                ret += "[color=\"" + this.Color + "\"]";

            ret += ";";

            //Добавляем описание переходов
            if(this.children != null)
                ret += this.children.Aggregate(
                    String.Empty,
                    (total, childKey) => total += this.Key + " -> " + childKey + ";"
                );

            return ret;
        }
    }
}
