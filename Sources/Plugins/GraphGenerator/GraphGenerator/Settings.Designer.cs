﻿namespace IA.Plugins.Analyses.GraphGenerator
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.generateAllDotCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // generateAllDotCheckBox
            // 
            this.generateAllDotCheckBox.AutoSize = true;
            this.generateAllDotCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.generateAllDotCheckBox.Location = new System.Drawing.Point(0, 0);
            this.generateAllDotCheckBox.Name = "generateAllDotCheckBox";
            this.generateAllDotCheckBox.Size = new System.Drawing.Size(469, 26);
            this.generateAllDotCheckBox.TabIndex = 0;
            this.generateAllDotCheckBox.Text = "Сгенерировать файл с полным графом? ";
            this.generateAllDotCheckBox.UseVisualStyleBackColor = true;
            this.generateAllDotCheckBox.CheckedChanged += new System.EventHandler(this.generateAllDotCheckBox_CheckedChanged);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.generateAllDotCheckBox);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(469, 26);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox generateAllDotCheckBox;
    }
}
