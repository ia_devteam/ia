﻿using System;
using System.Windows.Forms;

namespace IA.Plugins.Analyses.GraphGenerator
{
    public partial class Settings : UserControl
    {
        public bool generateAllDot;

        public Settings(bool GenerateAllDot)
        {
            InitializeComponent();
            generateAllDot = GenerateAllDot;
            generateAllDotCheckBox.Checked = generateAllDot;
        }

        private void generateAllDotCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            generateAllDot = generateAllDotCheckBox.Checked;
        }
    }
}
