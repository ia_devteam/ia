﻿using System;

using Store;

namespace IA.Plugins.Analyses.GraphGenerator
{
    /// <summary>
    /// Класс, реализующий граф управления между функциями
    /// </summary>
    class DiGraphFunctions : DiGraph<ulong>
    {
        /// <summary>
        /// Получить имя узла, соответствующего функции
        /// </summary>
        /// <param name="function">Функция</param>
        /// <returns></returns>
        protected string GetFunctionNodeLabel(IFunction function)
        {
            //Проверка на разумность
            if (function == null)
                throw new ArgumentNullException();

            return "(" + function.Id + ") " + function.NameForReports ?? "<Имя неизвестно>";
        }
    }
}
