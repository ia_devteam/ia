﻿using System;
using System.Linq;
using Store;

namespace IA.Plugins.Analyses.GraphGenerator
{
    /// <summary>
    /// Класс, реализующий локальный граф управления между функциями в рамках указанного Хранилища
    /// </summary>
    internal class DiGraphFunctionsLocal : DiGraphFunctions
    {
        /// <summary>
        /// Максимальный уроверь построения графа (сколько функций выше и ниже указанной будет отображаться в графе)
        /// </summary>
        const int maxLevel = 3;

        /// <summary>
        /// Текущий уровень построения графа
        /// </summary>
        private int currentLevel = 1;

        /// <summary>
        /// Функция, по которой строится граф
        /// </summary>
        internal IFunction Function { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="function">Функция</param>
        internal DiGraphFunctionsLocal(IFunction function)
        {
            //Проверка на разумность
            if (function == null)
                throw new Exception("Функция для генерации локального графа управления не определена.");

            //Сохраняем переданное значение
            this.Function = function;

            //Получаем узeл функции
            GraphNode<ulong> functionNode = new GraphNode<ulong>(function.Id)
            {
                Label = this.GetFunctionNodeLabel(function),
                Color = "red"
            };
            
            //Добавляем функцию в граф
            this.AddNode(functionNode);

            //Добавляем информацию о вызывающих функциях
            this.UpdateCallerFunctions(function);

            //Добавляем информацию о вызываемых функциях
            this.UpdateCalledFunctions(function);
        }

        /// <summary>
        /// Занести информацию о вызывающих функциях в граф управления по указанной функции
        /// </summary>
        /// <param name="function"></param>
        private void UpdateCallerFunctions(IFunction function)
        {
            //Если функция не задана, ничего не делаем
            if (function == null)
                throw new ArgumentNullException("function");

            //Проверяем есть ли такая фукнция в графе
            if (!this.IsContains(function.Id))
                throw new Exception("Функция с идентификатором <" + function.Id + "> отсутствует в графе.");

            //Если текущий уровень построения графа больше максимального, ничего не делаем
            if (this.currentLevel > maxLevel)
                return;

            //Увеличиваем текущий уровень построения графа
            this.currentLevel++;

            //Проходим по всем вызывающим функциям
            foreach (IFunctionCall functionCall in function.CalledBy().Where(c => c != null && c.Caller != null))
            {
                if (functionCall.Caller.kind == enFunctionKind.PSEUDO)
                    continue;

                //Получаем узeл вызывающей функции
                GraphNode<ulong> callerFunctionNode = new GraphNode<ulong>(functionCall.Caller.Id)
                {
                    Label = this.GetFunctionNodeLabel(functionCall.Caller)
                };

                //Если такой узел уже есть в графе
                if (this.IsContains(callerFunctionNode.Key))
                {
                    //Добавляем связь
                    this[callerFunctionNode.Key].AddChild(function.Id);
                    
                    continue;
                }

                //Добавляем узел в граф
                this.AddNode(callerFunctionNode);
                
                //Добавляем связь
                this[callerFunctionNode.Key].AddChild(function.Id);

                //Заносим информацию о вызывающей функции в граф
                this.UpdateCallerFunctions(functionCall.Caller);
            }

            //Уменьшаем текущий уровень построения графа
            this.currentLevel--;
        }

        /// <summary>
        /// Занести информацию о вызываемых функциях в граф управления по указанной функции
        /// </summary>
        /// <param name="function"></param>
        private void UpdateCalledFunctions(IFunction function)
        {
            //Если функция не задана, ничего не делаем
            if (function == null)
                throw new ArgumentNullException("function");

            //Проверяем есть ли такая фукнция в графе
            if (!this.IsContains(function.Id))
                throw new Exception("Функция с идентификатором <" + function.Id + "> отсутствует в графе.");

            //Если текущий уровень построения графа больше максимального, ничего не делаем
            if (this.currentLevel > maxLevel)
                return;

            //Увеличиваем текущий уровень построения графа
            this.currentLevel++;

            //Проходим по всем вызываемым функциям
            foreach (IFunctionCall functionCall in function.Calles().Where(c => c != null && c.Calles != null))
            {
                if (functionCall.Calles.kind == enFunctionKind.PSEUDO)
                    continue;

                //Получаем узeл вызывающей функции
                GraphNode<ulong> calledFunctionNode = new GraphNode<ulong>(functionCall.Calles.Id)
                {
                    Label = this.GetFunctionNodeLabel(functionCall.Calles)
                };

                //Если такой узел уже есть в графе
                if (this.IsContains(calledFunctionNode.Key))
                {
                    //Добавляем связь
                    this[function.Id].AddChild(calledFunctionNode.Key);
                    
                    continue;
                }

                //Добавляем узел
                this.AddNode(calledFunctionNode);

                //Добавляем связь
                this[function.Id].AddChild(calledFunctionNode.Key);

                //Заносим информацию о вызываемой функции в граф
                this.UpdateCalledFunctions(functionCall.Calles);
            }

            //Уменьшаем текущий уровень построения графа
            this.currentLevel--;
        }
    }
}
