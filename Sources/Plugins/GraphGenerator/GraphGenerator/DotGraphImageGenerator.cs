﻿using System;
using System.IO;

using IA.Extensions;
using System.Diagnostics;

namespace IA.Plugins.Analyses.GraphGenerator
{
    /// <summary>
    /// Класс, реализующий генерацию изображений графов, описанных на 
    /// 
    /// </summary>
    internal static class DotGraphImageGenerator
    {
        /// <summary>
        /// Генерация изображения графа, описанного на языке DOT
        /// </summary>
        /// <param name="dotGraphPath">Описание графа на языке DOT</param>
        /// <param name="graphImageFilePath">Путь до файла изображения графа</param>
        internal static void Generate(string dotGraphPath, string graphImageFilePath)
        {
            try//
            {
                if (String.IsNullOrWhiteSpace(dotGraphPath))
                    throw new Exception("Текст описания графа не определён.");

                if (String.IsNullOrWhiteSpace(graphImageFilePath))
                    throw new Exception("Путь до файла изображения графа не определён.");

                Process wrapperProcess = new Process();
                ProcessStartInfo cmdStartInfo = new ProcessStartInfo();

                //путь пока только для тестов
                cmdStartInfo.FileName = "GraphViz\\GraphVizWrapper.exe";
                if (Directory.Exists("Plugins"))
                {
                    // живем не в тесте
                    cmdStartInfo.FileName = "Plugins\\" + cmdStartInfo.FileName;
                }
                cmdStartInfo.Arguments = "\"" + dotGraphPath + "\" \"" + graphImageFilePath + "\"";
                cmdStartInfo.UseShellExecute = false;
                cmdStartInfo.CreateNoWindow = true;
                wrapperProcess.StartInfo = cmdStartInfo;

                wrapperProcess.Start();
                wrapperProcess.WaitForExit();
                if (wrapperProcess.ExitCode != 0)
                    throw new Exception("Запуск программы-обертки для GraphViz завершился неудачей.");
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Не удалось сформировать файл изображения графа на языке DOT.");
            }
        } 
    }
}
