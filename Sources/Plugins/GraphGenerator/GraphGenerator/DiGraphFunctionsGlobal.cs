﻿using System;
using System.Collections.Generic;
using System.Linq;

using Store;

namespace IA.Plugins.Analyses.GraphGenerator
{
    /// <summary>
    /// Класс, реализующий глобальный граф управления между функциями
    /// </summary>
    internal class DiGraphFunctionsGlobal : DiGraphFunctions
    {
        /// <summary>
        /// Набор функций, по которому строится граф
        /// </summary>
        internal IEnumerable<IFunction> Functions { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="functions">Набор функций</param>
        internal DiGraphFunctionsGlobal(IEnumerable<IFunction> functions)
        {
            //Проверка на разумность
            if (functions == null)
                throw new Exception("Набор функций для генерации глобального графа управления не определен.");

            //Сохраняем переданное значение
            this.Functions = functions;

            //Генерируем граф управления
            foreach(IFunction function in functions)
                this.Update(function);
        }

        /// <summary>
        /// Занести информацию в граф управления по указанной функции
        /// </summary>
        /// <param name="function">Функция</param>
        private void Update(IFunction function)
        {
            //Если функция не задана, ничего не делаем
            if (function == null)
                return;

            //Получаем узeл, соответствующий функции
            GraphNode<ulong> functionNode = new GraphNode<ulong>(function.Id)
            {
                Label = this.GetFunctionNodeLabel(function)
            };

            //Если такой узел уже есть в графе, то ничего не делаем
            if (this.IsContains(functionNode.Key))
                return;

            //Добавляем узел в граф
            this.AddNode(functionNode);

            //Проходим по всем вызываемым фунциям
            foreach (IFunctionCall functionCall in function.Calles().Where(c => c != null && c.Calles != null))
            {
                if (functionCall.Calles.kind == enFunctionKind.PSEUDO)
                    continue;

                //Получаем узeл вызываемой функции
                GraphNode<ulong> callesFunctionNode = new GraphNode<ulong>(functionCall.Calles.Id)
                {
                    Label = this.GetFunctionNodeLabel(functionCall.Calles)
                };

                //Добавляем новый переход
                this[functionNode.Key].AddChild(callesFunctionNode.Key);

                //Заносим информацию о вызываемой функции
                this.Update(functionCall.Calles);
            }
        }
    }
}
