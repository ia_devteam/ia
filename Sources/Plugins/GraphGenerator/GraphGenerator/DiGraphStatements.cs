﻿using System;
using System.Linq;

using Store;

namespace IA.Plugins.Analyses.GraphGenerator
{
    /// <summary>
    /// Класс, реализующий граф управления между операциями в рамках указанной функции
    /// </summary>
    internal class DiGraphStatements : DiGraph<ulong>
    {
        /// <summary>
        /// Функция, по которой строится граф
        /// </summary>
        internal IFunction Function { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="function">Функция</param>
        internal DiGraphStatements(IFunction function)
        {
            //Проверка на разумность
            if (function == null)
                throw new Exception("Функция для генерации графа управления не определена.");

            //Сохраняем переданное значение
            this.Function = function;

            //Если функция пуста, ничего не делаем
            if (this.Function.EntryStatement == null)
                return;

            //Генерируем граф управления
            this.Update(this.Function.EntryStatement);
        }

        /// <summary>
        /// Занести информацию в граф управления по указанной операции
        /// </summary>
        /// <param name="statement"></param>
        private void Update(IStatement statement)
        {
            //Если операция не задана, ничего не делаем
            if (statement == null)
                return;

            //Получаем датчик, соответствующий операции
            Sensor statementSensor = statement.SensorBeforeTheStatement;

            //Если датчик, соответствующий операции не задан, ничего не делаем
            if (statementSensor == null)
                return;

            //Получаем узел соответствующий операции
            GraphNode<ulong> statementSensorNode = new GraphNode<ulong>(statementSensor.ID);

            //Если такой узел уже есть в графе, то ничего не делаем
            if (this.IsContains(statementSensorNode.Key))
                return;

            //Добавляем узел в граф
            this.AddNode(statementSensorNode);

            //Проходим по всем дочерним операциям
            foreach (IStatement subStatement in statement.NextInControl(false).Where(s => s != null && s.SensorBeforeTheStatement != null))
            {
                //Получаем узел, соответствующий дочерней операции
                GraphNode<ulong> subStatementSensorNode = new GraphNode<ulong>(subStatement.SensorBeforeTheStatement.ID);

                //Добавляем новый переход
                this[statementSensorNode.Key].AddChild(subStatementSensorNode.Key);

                //Заносим информацию о дочерней операции в граф
                this.Update(subStatement);
            }
        }
    }
}
