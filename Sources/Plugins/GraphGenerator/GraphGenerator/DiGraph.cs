﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IA.Plugins.Analyses.GraphGenerator
{
    /// <summary>
    /// Абстракный класс, определяющий направленный граф
    /// </summary>
    /// <typeparam name="T">Тип элементов</typeparam>
    internal abstract class DiGraph<T> where T : IComparable<T>
    {
        /// <summary>
        /// Набор узлов графа
        /// </summary>
        private Dictionary<T, GraphNode<T>> nodes = null;

        protected GraphNode<T> this[T key]
        {
            get
            {
                //Проверка на разумнтсть
                if (key.CompareTo(default(T)) == 0)
                    throw new Exception("Недопустимое значение ключа узла графа.");

                GraphNode<T> node;

                if (this.nodes == null || !this.nodes.TryGetValue(key, out node))
                    throw new Exception("Узел с ключём <" + key + "> отсутствует в графе.");

                return node;
            }
        }

        /// <summary>
        /// Содержит ли граф, хотя бы один узел?
        /// </summary>
        internal bool IsEmpty
        {
            get
            {
                return this.nodes == null || this.nodes.Count == 0;
            }
        }

        /// <summary>
        /// Содержится ли узел с указанным ключём в графе?
        /// </summary>
        /// <param name="nodeKey">Ключ узла</param>
        /// <returns></returns>
        internal bool IsContains(T nodeKey)
        {
            //Проверка на разумнтсть
            if (nodeKey.CompareTo(default(T)) == 0)
                throw new Exception("Недопустимое значение ключа узла графа.");

            return this.nodes != null && this.nodes.ContainsKey(nodeKey);
        }

        /// <summary>
        /// Добавить узел в граф
        /// </summary>
        /// <param name="node">Узел</param>
        internal void AddNode(GraphNode<T> node)
        {
            //Проверка на разумность
            if (node == null)
                throw new ArgumentNullException("node");

            //Если такой узел, уже есть в графе, ничего не делаем
            if (this.IsContains(node.Key))
                return;

            //Если граф не определён
            if(this.nodes == null)
                //Создаём новый граф
                this.nodes = new Dictionary<T,GraphNode<T>>();

            //Добавялем узел в граф
            this.nodes.Add(node.Key, node); 
        }

        /// <summary>
        /// Получть описание графа на языке DOT
        /// </summary>
        /// <returns>Описание графа на языке DOT. Не может быть Null.</returns>
        internal string ToDotLanguage()
        {
            string ret = "digraph {";

            if(this.nodes != null)
                ret += this.nodes.Keys.Aggregate(
                    String.Empty,
                    (total, key) => total += this.nodes[key].ToDotLanguage()
                );

            return ret += "}";
        }
    }
}
