﻿using System;
using System.IO;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using TestUtils;
using IA.Plugins.Analyses.GraphGenerator;
using Store.Table;

namespace TestGraphGenerator
{
    [TestClass]
    public class TestGraphGenerator
    {
        /// <summary>
        /// Эпсилон-окрестность размера файла от эталонного размера
        /// 2 мб = 2 * 1024 * 1024 (мб*кб*б)
        /// </summary>
        private const uint DELTA_EPSILON = 0;

        /// <summary>
        /// Подпапка с эталонными отчетами.
        /// </summary>
        private string SAMPLES_MATERIALS_PATH = @"GraphGenerator";

        /// <summary>
        /// Полный путь к эталонным файлам
        /// </summary>
        static string sampleMaterialsFullPath;

        /// <summary>
        /// Имя отчета по функциям
        /// </summary>
        static string functionGraphsReportName = "FunctionGraphsReport.txt";

        /// <summary>
        /// Имя отчета по операторам
        /// </summary>
        static string statementGraphsReportName = "StatementGraphsReport.txt";

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        static TestUtilsClass testUtils;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            sampleMaterialsFullPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, SAMPLES_MATERIALS_PATH, testContextInstance.TestName);
        }

        /// <summary>
        /// Постройка графа по второму уровню НДВ. 
        /// Граф строится по путям, сгенерированных плагином WayGenerator, для языка C#.
        /// </summary>
        [TestMethod]
        public void GraphGenerator_StatementsNDV2()
        {
            const string sourcePrefixPart = @"Sources\CSharp\TestProject\";
            GraphGenerator graphGenerator = new GraphGenerator();

            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                graphGenerator,                                                     // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestProject\"), level2: true);
                    TestUtilsClass.Run_PointsToAnalysesSimple(storage);
                    TestUtilsClass.Run_WayGenerator(storage, true);

                    IBufferWriter buffer = WriterPool.Get();
                    buffer.Add(true);
                    ulong pluginID = Store.Const.PluginIdentifiers.GRAPH_GENERATOR;
                    storage.pluginSettings.SaveSettings(pluginID, buffer);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {        
                    //Проверка Хранилища            
                    CheckStorage(storage.pluginData.GetDataFolder(graphGenerator.ID));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Постройка графа по третьему уровню НДВ. 
        /// Граф строится по путям, сгенерированных плагином WayGenerator, для языка C#.
        /// </summary>
        [TestMethod]
        public void GraphGenerator_FunctionsNDV3()
        {
            const string sourcePrefixPart = @"Sources\CSharp\TestProject\";
            GraphGenerator graphGenerator = new GraphGenerator();

            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                graphGenerator,                                                     // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestProject\"), level2: false);
                    TestUtilsClass.Run_PointsToAnalysesSimple(storage);
                    TestUtilsClass.Run_WayGenerator(storage, false);

                    IBufferWriter buffer = WriterPool.Get();
                    buffer.Add(true);
                    ulong pluginID = Store.Const.PluginIdentifiers.GRAPH_GENERATOR;
                    storage.pluginSettings.SaveSettings(pluginID, buffer);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //Проверка Хранилища
                    CheckStorage(storage.pluginData.GetDataFolder(graphGenerator.ID));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        internal static bool CheckReportsCommon(string reportsDirectoryPath)
        {
            //Сверяем отчет с эталонным
            Assert.IsTrue(TestUtilsClass.Reports_Directory_TXT_Compare(reportsDirectoryPath, TestGraphGenerator.sampleMaterialsFullPath), "Отчет не совпадает с эталонным.");
            
            return true;
        }

        class RussianLettersTest : TestUtilsClass.RunTestConfig
        {
            string sourcePrefixPart = @"Sources\Russian\";
            public override string Stage01SourcesPostfix
            {
                get
                {
                    return sourcePrefixPart;
                }
            }

            public override void Stage11PrepareStorageDelegate(Store.Storage storage, string testMaterialsDirectoryPath)
            {
                TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsDirectoryPath, sourcePrefixPart));
                TestUtilsClass.Run_IdentifyFileTypes(storage);
                TestUtilsClass.Run_UnderstandImporter(storage, reportsDirectoryPath: Path.Combine(testMaterialsDirectoryPath, @"Understand\Russian"), sourcesDirectoryPath: @"B:\IA_b\Tests\Materials\Sources\Russian\");
                TestUtilsClass.Run_PointsToAnalysesSimple(storage);
                TestUtilsClass.Run_WayGenerator(storage, false);

                IBufferWriter buffer = WriterPool.Get();
                buffer.Add(true);
                ulong pluginID = Store.Const.PluginIdentifiers.GRAPH_GENERATOR;
                storage.pluginSettings.SaveSettings(pluginID, buffer);
            }

            public override bool Stage12CheckStorageDelegate(Storage storage, string testMaterialsDirectoryPath)
            {
                return CheckReportsCommon(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.GRAPH_GENERATOR));
            }
        }

        class AnonimousFuncsTest : TestUtilsClass.RunTestConfig
        {
            string sourcePrefixPart = @"Sources\JavaScript\";
            public override string Stage01SourcesPostfix
            {
                get
                {
                    return sourcePrefixPart;
                }
            }

            public override void Stage11PrepareStorageDelegate(Store.Storage storage, string testMaterialsDirectoryPath)
            {
                TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsDirectoryPath, sourcePrefixPart));
                TestUtilsClass.Run_IdentifyFileTypes(storage);
                TestUtilsClass.Run_JavaScriptParser(storage);
                TestUtilsClass.Run_PointsToAnalysesSimple(storage);
                TestUtilsClass.Run_WayGenerator(storage, false);

                IBufferWriter buffer = WriterPool.Get();
                buffer.Add(true);
                ulong pluginID = Store.Const.PluginIdentifiers.GRAPH_GENERATOR;
                storage.pluginSettings.SaveSettings(pluginID, buffer);
            }

            public override bool Stage12CheckStorageDelegate(Storage storage, string testMaterialsDirectoryPath)
            {
                return CheckReportsCommon(storage.pluginData.GetDataFolder(Store.Const.PluginIdentifiers.GRAPH_GENERATOR));
            }
        }

        /// <summary>
        /// попытка обработать функции, имена которых содержат русские симполы
        /// </summary>
        [TestMethod]
        public void GraphGenerator_RussianLettersTest()
        {
            testUtils.RunTest<GraphGenerator, RussianLettersTest>();
        }

        /// <summary>
        /// попытка обработать анонимную функцию
        /// </summary>
        [TestMethod]
        public void GraphGenerator_AnonimousFuncsTest()
        {
            testUtils.RunTest<GraphGenerator, AnonimousFuncsTest>();
        }

        /// <summary>
        /// Проверка точек графов в Хранилище.
        /// </summary>
        /// <param name="pluginStorageDataFolder">Каталог с данными плагина в Хранилище. Не может быть null.</param>
        private void CheckStorage(string pluginStorageDataFolder)
        {
            //Пути к папкам плагина в Хранилище
            string functionGraphsDirectoryNameInReports = Path.Combine(pluginStorageDataFolder, GraphGenerator.FunctionGraphsDirectoryName);
            string statementGraphsDirectoryNameInReports = Path.Combine(pluginStorageDataFolder, GraphGenerator.StatementGraphsDirectoryName);

            //Пути к папкам плагина в эталонах
            string functionGraphsDirectoryNameInSamples = Path.Combine(sampleMaterialsFullPath, "Storage", GraphGenerator.FunctionGraphsDirectoryName);
            string statementGraphsDirectoryNameInSamples = Path.Combine(sampleMaterialsFullPath, "Storage", GraphGenerator.StatementGraphsDirectoryName);

            //Проверяем содержимое папок
            Assert.IsTrue(TestUtilsClass.Reports_Directory_TXT_Compare(functionGraphsDirectoryNameInReports, functionGraphsDirectoryNameInSamples), "Точки графов по функциям не совпадают с эталонными.");
            Assert.IsTrue(TestUtilsClass.Reports_Directory_TXT_Compare(statementGraphsDirectoryNameInReports, statementGraphsDirectoryNameInSamples), "Точки графов по операторам не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка отчетов.
        /// </summary>
        /// <param name="files">Словарь файлов с их размерами. Не может быть null.</param>
        /// <param name="reportFileName">Файл отчета по размерам файлов. Не может быть пустым.</param>
        static void CheckReports(Dictionary<string, long> files, string reportFileName)
        {
            //Создаем отчет о файлах с их размерами (используется только как эталонный файл при существенных изменениях)
            using (StreamWriter writer = new StreamWriter(Path.Combine(testUtils.TestRunDirectoryPath, reportFileName)))
                foreach (KeyValuePair<string, long> file in files)
                    writer.WriteLine(string.Join("\t", new string[] { file.Key, file.Value.ToString() }));

            //Открываем ранее созданный и проверенный вручную эталонный файл
            using (StreamReader reader = new StreamReader(Path.Combine(sampleMaterialsFullPath, "Reports", reportFileName)))
            {
                //Строка отчета
                string readerLine;
                //Номер строки в файле
                uint counter = 0;

                //Пока не конец файла
                while ((readerLine = reader.ReadLine()) != null)
                {
                    //Увеличиваем счетчик 
                    counter++;
                    //Считываем строку
                    string[] content = readerLine.Split('\t');

                    //Проверяем строку
                    Assert.IsTrue(content.Length == 2, "Неверная строка (" + counter + ") в эталонном файле. Количество элементов не равно 2.");

                    //Пробуем получить размер файла из строки отчета
                    long result;
                    Assert.IsTrue(long.TryParse(content[1], out result), "Неверная строка (" + counter + ") в эталонном файле. Невозможно получить размер файла.");

                    //Ищем в словаре размер файла по его имени
                    long value;
                    Assert.IsTrue(files.TryGetValue(content[0], out value), "Отсутствует файл <" + content[0] + "> для сравнения.");

                    //Проверяем разброс размера файла с эталоном
                    Assert.IsTrue((uint)Math.Abs(result - value) <= DELTA_EPSILON, "Файл <" + content[0] + "> слишком сильно различается от эталонного.");
                }

                //Проверка на то. что были проверены все файлы
                Assert.IsTrue(counter == files.Count, "Количество файлов не совпадает с эталонным.");
            }
        }
    }
}
