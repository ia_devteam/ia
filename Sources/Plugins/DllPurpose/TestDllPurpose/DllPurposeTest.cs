﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;
using IA.Plugins.Utils.DllPurpose;

namespace TestDllPurpose
{
    [TestClass]
    public class DllPurposeTest
    {
        /// <summary>
        /// Summary description for MainTest
        /// </summary>
        [TestClass]
        public class TestDllPurpose
        {
            /// <summary>
            /// Экземпляр инфраструктуры тестирования для каждого теста
            /// </summary>
            private TestUtilsClass testUtils;

            private TestContext testContextInstance;
            /// <summary>
            /// Информация о текущем тесте
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            /// <summary>
            /// Делаем то, что необходимо сделать перед запуском каждого теста
            /// </summary>
            [TestInitialize]
            public void EachTest_Initialize()
            {
                //Иницииализация инфраструктуры тестирования для каждого теста
                testUtils = new TestUtilsClass(testContextInstance.TestName);
            }

            /// <summary>
            /// Файл с данными о библиотеках и функциях пустой
            /// Результат - пустые отчеты
            /// </summary>
            [TestMethod]
            public void DllPurpose_Empty()
            {

                TestUtils.TestUtilsClass.SQL_SetupTestConnection();
                TestUtils.TestUtilsClass.RemoveSqlTestData();
                TestUtils.TestUtilsClass.SetSqlTestData();

                testUtils.RunTest(
                    //sourcePrefixPart
                    string.Empty,

                    //plugin
                    new DllPurpose(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"DllPurpose\Settings\ExtFuncEmpty.il"));
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.DLL_PURPOSE, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"DllPurpose\Reports\Empty\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

                TestUtils.TestUtilsClass.RemoveSqlTestData();


            }

            /// <summary>
            /// В файле данных о библиотеках и функциях нет ни одной библиотеки, ни одной функции из базы данных
            /// </summary>
            [TestMethod]
            public void DllPurpose_NotInBase()
            {

                TestUtils.TestUtilsClass.SQL_SetupTestConnection();
                TestUtils.TestUtilsClass.RemoveSqlTestData();
                TestUtils.TestUtilsClass.SetSqlTestData();

                testUtils.RunTest(
                    //sourcePrefixPart
                    string.Empty,

                    //plugin
                    new DllPurpose(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        //TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"DllPurpose\Settings\ExtFuncNotInBase.il"));
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.DLL_PURPOSE, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"DllPurpose\Reports\NotInBase\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

                TestUtils.TestUtilsClass.RemoveSqlTestData();


            }
            /// <summary>
            /// В файле данных о библиотеках и функциях все библиотеки и все функции есть в базе данных
            /// </summary>
            [TestMethod]
            public void DllPurpose_InBase()
            {

                TestUtils.TestUtilsClass.SQL_SetupTestConnection();
                TestUtils.TestUtilsClass.RemoveSqlTestData();
                TestUtils.TestUtilsClass.SetSqlTestData();

                testUtils.RunTest(
                    //sourcePrefixPart
                    string.Empty,

                    //plugin
                    new DllPurpose(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        //TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"DllPurpose\Settings\ExtFuncInBase.il"));
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.DLL_PURPOSE, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"DllPurpose\Reports\InBase\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

                TestUtils.TestUtilsClass.RemoveSqlTestData();


            }

            /// <summary>
            /// В файле данных о библиотеках и функциях часть данных по функциям присутствует в базе, часть отсутствует; аналогично - по библиотекам
            /// </summary>
            [TestMethod]
            public void DllPurpose_Normal()
            {

                TestUtils.TestUtilsClass.SQL_SetupTestConnection();
                TestUtils.TestUtilsClass.RemoveSqlTestData();
                TestUtils.TestUtilsClass.SetSqlTestData();

                testUtils.RunTest(
                    //sourcePrefixPart
                    string.Empty,

                    //plugin
                    new DllPurpose(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        //TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"DllPurpose\Settings\ExtFuncNormal.il"));
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.DLL_PURPOSE, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"DllPurpose\Reports\Normal\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

                TestUtils.TestUtilsClass.RemoveSqlTestData();


            }

            /// <summary>
            /// Файл данных содержит сведения только о библиотеках, имена функций берутся из базы данных
            /// </summary>
            [TestMethod]
            public void DllPurpose_Settigs()
            {

                TestUtils.TestUtilsClass.SQL_SetupTestConnection();
                TestUtils.TestUtilsClass.RemoveSqlTestData();
                TestUtils.TestUtilsClass.SetSqlTestData();

                testUtils.RunTest(
                    //sourcePrefixPart
                    string.Empty,

                    //plugin
                    new DllPurpose(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        
                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                        settingsBuffer.Add(System.IO.Path.Combine(testMaterialsPath, @"DllPurpose\Settings\ExtDllNormal.il"));
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.DLL_PURPOSE, settingsBuffer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"DllPurpose\Reports\Settings\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

                TestUtils.TestUtilsClass.RemoveSqlTestData();


            }

        }
    }
}
