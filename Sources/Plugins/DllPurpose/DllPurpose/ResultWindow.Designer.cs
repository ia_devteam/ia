﻿namespace IA.Plugins.Utils.DllPurpose
{
    partial class ResultWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.needAnalize_tabControl = new System.Windows.Forms.TabControl();
            this.libraries_tabPage = new System.Windows.Forms.TabPage();
            this.libraries_dataGridView = new System.Windows.Forms.DataGridView();
            this.functions_tabPage = new System.Windows.Forms.TabPage();
            this.functions_dataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.needAnalize_groupBox = new System.Windows.Forms.GroupBox();
            this.resultWindow_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.saveToDatabaseИutton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Column11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.needAnalize_tabControl.SuspendLayout();
            this.libraries_tabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.libraries_dataGridView)).BeginInit();
            this.functions_tabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.functions_dataGridView)).BeginInit();
            this.needAnalize_groupBox.SuspendLayout();
            this.resultWindow_tableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // needAnalize_tabControl
            // 
            this.needAnalize_tabControl.Controls.Add(this.libraries_tabPage);
            this.needAnalize_tabControl.Controls.Add(this.functions_tabPage);
            this.needAnalize_tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.needAnalize_tabControl.Location = new System.Drawing.Point(3, 16);
            this.needAnalize_tabControl.Name = "needAnalize_tabControl";
            this.needAnalize_tabControl.SelectedIndex = 0;
            this.needAnalize_tabControl.Size = new System.Drawing.Size(824, 348);
            this.needAnalize_tabControl.TabIndex = 0;
            // 
            // libraries_tabPage
            // 
            this.libraries_tabPage.Controls.Add(this.libraries_dataGridView);
            this.libraries_tabPage.Location = new System.Drawing.Point(4, 22);
            this.libraries_tabPage.Name = "libraries_tabPage";
            this.libraries_tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.libraries_tabPage.Size = new System.Drawing.Size(816, 322);
            this.libraries_tabPage.TabIndex = 0;
            this.libraries_tabPage.Text = "Библиотеки";
            this.libraries_tabPage.UseVisualStyleBackColor = true;
            // 
            // libraries_dataGridView
            // 
            this.libraries_dataGridView.AllowUserToAddRows = false;
            this.libraries_dataGridView.AllowUserToDeleteRows = false;
            this.libraries_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.libraries_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column15,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column12,
            this.Column10,
            this.Column11});
            this.libraries_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.libraries_dataGridView.Location = new System.Drawing.Point(3, 3);
            this.libraries_dataGridView.Name = "libraries_dataGridView";
            this.libraries_dataGridView.RowHeadersVisible = false;
            this.libraries_dataGridView.Size = new System.Drawing.Size(810, 316);
            this.libraries_dataGridView.TabIndex = 0;
            this.libraries_dataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.libraries_dataGridView_CellEndEdit);
            // 
            // functions_tabPage
            // 
            this.functions_tabPage.Controls.Add(this.functions_dataGridView);
            this.functions_tabPage.Location = new System.Drawing.Point(4, 22);
            this.functions_tabPage.Name = "functions_tabPage";
            this.functions_tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.functions_tabPage.Size = new System.Drawing.Size(816, 322);
            this.functions_tabPage.TabIndex = 1;
            this.functions_tabPage.Text = "Функции";
            this.functions_tabPage.UseVisualStyleBackColor = true;
            // 
            // functions_dataGridView
            // 
            this.functions_dataGridView.AllowUserToAddRows = false;
            this.functions_dataGridView.AllowUserToDeleteRows = false;
            this.functions_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.functions_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column16,
            this.Column14,
            this.Column13,
            this.Column2,
            this.Column3,
            this.Column5,
            this.Column6});
            this.functions_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.functions_dataGridView.Location = new System.Drawing.Point(3, 3);
            this.functions_dataGridView.Name = "functions_dataGridView";
            this.functions_dataGridView.RowHeadersVisible = false;
            this.functions_dataGridView.Size = new System.Drawing.Size(810, 316);
            this.functions_dataGridView.TabIndex = 0;
            this.functions_dataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.functions_dataGridView_CellEndEdit);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "№";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 40;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "Библиотека";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "ID";
            this.Column14.Name = "Column14";
            this.Column14.Visible = false;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "DllID";
            this.Column13.Name = "Column13";
            this.Column13.Visible = false;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Наименование";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Описание";
            this.Column3.Name = "Column3";
            this.Column3.Width = 400;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Ordinal";
            this.Column5.Name = "Column5";
            this.Column5.Width = 60;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Требует исследования";
            this.Column6.Name = "Column6";
            this.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column6.Width = 80;
            // 
            // needAnalize_groupBox
            // 
            this.needAnalize_groupBox.Controls.Add(this.needAnalize_tabControl);
            this.needAnalize_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.needAnalize_groupBox.Location = new System.Drawing.Point(3, 33);
            this.needAnalize_groupBox.Name = "needAnalize_groupBox";
            this.needAnalize_groupBox.Size = new System.Drawing.Size(830, 367);
            this.needAnalize_groupBox.TabIndex = 1;
            this.needAnalize_groupBox.TabStop = false;
            this.needAnalize_groupBox.Text = "Укажите необходимость исследования";
            // 
            // resultWindow_tableLayoutPanel
            // 
            this.resultWindow_tableLayoutPanel.ColumnCount = 1;
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.resultWindow_tableLayoutPanel.Controls.Add(this.needAnalize_groupBox, 0, 1);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.resultWindow_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultWindow_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.resultWindow_tableLayoutPanel.Name = "resultWindow_tableLayoutPanel";
            this.resultWindow_tableLayoutPanel.RowCount = 2;
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.resultWindow_tableLayoutPanel.Size = new System.Drawing.Size(836, 403);
            this.resultWindow_tableLayoutPanel.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel1.Controls.Add(this.saveToDatabaseИutton, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(836, 30);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // saveToDatabaseИutton
            // 
            this.saveToDatabaseИutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saveToDatabaseИutton.Location = new System.Drawing.Point(659, 3);
            this.saveToDatabaseИutton.Name = "saveToDatabaseИutton";
            this.saveToDatabaseИutton.Size = new System.Drawing.Size(174, 24);
            this.saveToDatabaseИutton.TabIndex = 3;
            this.saveToDatabaseИutton.Text = "Сохранить изменения в базе";
            this.saveToDatabaseИutton.UseVisualStyleBackColor = true;
            this.saveToDatabaseИutton.Click += new System.EventHandler(this.saveToDatabaseButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(450, 30);
            this.label1.TabIndex = 5;
            this.label1.Text = "* Желтым цветом выделены ячейки с данными, отличными от данных в базе";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Требует исследования";
            this.Column11.Name = "Column11";
            this.Column11.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Версия";
            this.Column10.Name = "Column10";
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Контрольная сумма";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Visible = false;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Описание";
            this.Column9.Name = "Column9";
            this.Column9.Width = 300;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Автор";
            this.Column8.Name = "Column8";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Наименование";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "ID";
            this.Column15.Name = "Column15";
            this.Column15.Visible = false;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "№";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 40;
            // 
            // ResultWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resultWindow_tableLayoutPanel);
            this.Name = "ResultWindow";
            this.Size = new System.Drawing.Size(836, 403);
            this.needAnalize_tabControl.ResumeLayout(false);
            this.libraries_tabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.libraries_dataGridView)).EndInit();
            this.functions_tabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.functions_dataGridView)).EndInit();
            this.needAnalize_groupBox.ResumeLayout(false);
            this.resultWindow_tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl needAnalize_tabControl;
        private System.Windows.Forms.TabPage libraries_tabPage;
        private System.Windows.Forms.DataGridView libraries_dataGridView;
        private System.Windows.Forms.TabPage functions_tabPage;
        private System.Windows.Forms.DataGridView functions_dataGridView;
        private System.Windows.Forms.GroupBox needAnalize_groupBox;
        private System.Windows.Forms.TableLayoutPanel resultWindow_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button saveToDatabaseИutton;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column11;
    }
}
