﻿namespace IA.Plugins.Utils.DllPurpose
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.filePath_groupBox = new System.Windows.Forms.GroupBox();
            this.filePath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.filePath_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // filePath_groupBox
            // 
            this.filePath_groupBox.Controls.Add(this.filePath_textBox);
            this.filePath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filePath_groupBox.Location = new System.Drawing.Point(0, 0);
            this.filePath_groupBox.Name = "filePath_groupBox";
            this.filePath_groupBox.Size = new System.Drawing.Size(420, 45);
            this.filePath_groupBox.TabIndex = 0;
            this.filePath_groupBox.TabStop = false;
            this.filePath_groupBox.Text = "Укажите файл со списком внешних библиотек и функций";
            // 
            // filePath_textBox
            // 
            this.filePath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filePath_textBox.Location = new System.Drawing.Point(3, 16);
            this.filePath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.filePath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.filePath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.filePath_textBox.Name = "filePath_textBox";
            this.filePath_textBox.Size = new System.Drawing.Size(414, 25);
            this.filePath_textBox.TabIndex = 0;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.filePath_groupBox);
            this.MaximumSize = new System.Drawing.Size(0, 45);
            this.MinimumSize = new System.Drawing.Size(420, 45);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(420, 45);
            this.filePath_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox filePath_groupBox;
        private Controls.TextBoxWithDialogButton filePath_textBox;
    }
}
