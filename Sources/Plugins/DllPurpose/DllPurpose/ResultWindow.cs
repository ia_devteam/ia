﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace IA.Plugins.Utils.DllPurpose
{
    /// <summary>
    /// Результирующее окно для принятия решения об исследовании
    /// </summary>
    public partial class ResultWindow : UserControl
    {
        DllPurpose plugin;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="plug">вызвавший плагин</param>
        public ResultWindow(DllPurpose plug)
        {
            plugin = plug;
            InitializeComponent();
            plugin.dllSavedData.Clear();
            plugin.funcSavedData.Clear();
            
            int cnt = 1;
            // Заполнение данных по библиотекам
            foreach (string[] dll in plugin.clearDllData) 
            {
                dll[6] = dll[6] == "1" ? "false" : dll[6] == "Не требует исследования" ? "false" : "true";
                
                string[] resDll = (new string[] { cnt++.ToString() }).Concat(dll).ToArray();
                libraries_dataGridView.Rows.Add(resDll);
                plugin.dllSavedData.Add(resDll);
                
                if (int.Parse(dll[0]) < 0)
                    libraries_dataGridView[0, cnt-2].Style.BackColor = Color.Yellow;
            }

            cnt = 1;
            // Заполнение данных по функциям
            foreach (string[] func in plugin.clearFuncData)
            {
                string[] dll = plugin.clearDllData.Where(x => x[0] == func[1]).FirstOrDefault();
                func[5] = (func[5] == "1" || func[5] == "Не требует исследования" || dll[6] == "false") ? "false" : "true";
                
                string[] resFunc = (new string[] { cnt++.ToString(), dll[1] }).Concat(func).ToArray();
                functions_dataGridView.Rows.Add(resFunc);
                plugin.funcSavedData.Add(resFunc);

                if (int.Parse(func[0]) < 0)
                    functions_dataGridView[0, cnt - 2].Style.BackColor = Color.Yellow;
            }
        }

        /// <summary>
        /// Чтение данных из строки DataGridView
        /// </summary>
        /// <param name="targetDataGridView">DataGridView</param>
        /// <param name="row">индекс строки</param>
        /// <returns>массив строк</returns>
        string[] GetRowData(DataGridView targetDataGridView, int row)
        {
            int colCount = targetDataGridView.Rows[row].Cells.Count;
            
            string[] result = new string[colCount];
            
            for (int i = 0; i < colCount; i++)
                result[i] = targetDataGridView.Rows[row].Cells[i].Value == null ? "" : targetDataGridView.Rows[row].Cells[i].Value.ToString();

            return result;
        }

        private void saveToDatabaseButton_Click(object sender, EventArgs e)
        {
            List<string[]> toSaveDlls = GetSelectedFieldsFromTable(libraries_dataGridView);
            List<string[]> toSaveFuncs = GetSelectedFieldsFromTable(functions_dataGridView);

            foreach (string[] dll in toSaveDlls)
                dll[7] = dll[7].ToLower() == "false" ? "1" : "0";

            foreach (string[] func in toSaveFuncs)
                func[7] = func[7].ToLower() == "false" ? "1" : "0";

            IA.Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection SQLConnection = new IA.Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection();

            if (needAnalize_tabControl.SelectedTab == libraries_tabPage)
            {
                if (toSaveDlls.Count != 0)
                {
                    List<string[]> dllsFromBase = SQLConnection.DllBase;
                    foreach (string[] dll in toSaveDlls)
                    {
                        dll[2] = dll[2].ToLower();

                        var temp = dllsFromBase.Where(x => (x[1].ToLower() == dll[2] || x[1].ToLower() == (dll[2] + ".dll")));

                        if (temp.Count() > 0)
                        {
                            SQLConnection.UpdateDll(dll.Skip(1).ToArray());
                        }
                        else
                        {
                            SQLConnection.InsertDllToBase(dll.Skip(2).ToArray(), null, null, null);
                        }
                    }
                    setSelectedFildsNormal(libraries_dataGridView);
                }
            }
            else
            {
                if (toSaveFuncs.Count != 0)
                {
                    if (plugin.funcsFromBase == null)
                        plugin.funcsFromBase = SQLConnection.Functions;
                    foreach (string[] func in toSaveFuncs)
                    {
                        var temp = plugin.funcsFromBase.Where(x => (x[1].ToLower() == func[3] && x[2].ToLower() == func[4]));

                        if (temp.Count() > 0)
                        {
                            SQLConnection.UpdateFunction(new string[] { func[2], func[4], func[5], func[7], "" });
                        }
                        else
                        {
                            SQLConnection.InsertFunction(new string[] { func[3], func[4], func[5], func[7], "" });
                        }
                    }
                    setSelectedFildsNormal(functions_dataGridView);
                }
            }
        }

        /// <summary>
        /// раскрасить всю таблицу обычным цветом (убрать желтый)
        /// </summary>
        /// <param name="dataGrid"></param>
        void setSelectedFildsNormal(DataGridView dataGrid)
        {

            for (int i = 0; i < dataGrid.Rows.Count; i++)
            {
                for (int j = 0; j < dataGrid.Columns.Count; j++)
                {
                    dataGrid[j, i].Style.BackColor = Color.White;
                }
            }
        }

        List<string[]> GetSelectedFieldsFromTable(DataGridView dataGrid)
        {
            int rowCount = dataGrid.Rows.Count;
            int columnCount = dataGrid.Columns.Count;

            List<string[]> result = new List<string[]>();
            
            for (int i = 0; i < rowCount; i++)
            {
                string[] row = new string[columnCount];
                bool found = false;

                for (int j = 0; j < columnCount; j++)
                {
                    if (dataGrid[j, i].Style.BackColor == Color.Yellow)
                        found = true;
                    row[j] = CellValue(dataGrid, j, i);
                }

                if (found)
                    result.Add(row);
            }

            return result;
        }

        string CellValue(DataGridView dataGrid, int column, int row)
        {
            return dataGrid[column, row].Value != null ? dataGrid[column, row].Value.ToString() : "";
        }

        private void libraries_dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 1)
                return;

            string cellValu = CellValue(libraries_dataGridView,e.ColumnIndex, e.RowIndex);

            plugin.dllSavedData[e.RowIndex][e.ColumnIndex] = cellValu;

            if (e.ColumnIndex == 7)
            {
                // поменяли "Требует исследования" - надо обновить вторую страничку со списком функций.
                for (int i = 0; i < functions_dataGridView.RowCount; ++i)
                {
                    if (CellValue(functions_dataGridView, 3, i) == CellValue(libraries_dataGridView, 1, e.RowIndex))
                    {
                        if ((string)functions_dataGridView.Rows[i].Cells[7].Value != cellValu)
                        {
                            functions_dataGridView.Rows[i].Cells[7].Value = cellValu;
                            functions_dataGridView[7, i].Style.BackColor = Color.Yellow;
                        }
                    }
                }
            }

            if (cellValu.ToLower() != plugin.clearDllData[e.RowIndex][e.ColumnIndex - 1])
                libraries_dataGridView[e.ColumnIndex, e.RowIndex].Style.BackColor = Color.Yellow;
            else
                libraries_dataGridView[e.ColumnIndex, e.RowIndex].Style.BackColor = SystemColors.Window;

        }

        private void functions_dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 2)
                return;

            string cellValue = CellValue(functions_dataGridView, e.ColumnIndex, e.RowIndex);

            plugin.funcSavedData[e.RowIndex][e.ColumnIndex - 1] = cellValue;

            if (cellValue.ToLower() != plugin.clearFuncData[e.RowIndex][e.ColumnIndex - 2])
                functions_dataGridView[e.ColumnIndex, e.RowIndex].Style.BackColor = Color.Yellow;
            else
                functions_dataGridView[e.ColumnIndex, e.RowIndex].Style.BackColor = SystemColors.Window;
        }
        
    }
}
