﻿using System.Windows.Forms;

namespace IA.Plugins.Utils.DllPurpose
{
    /// <summary>
    /// Основной класс настроек плагина
    /// </summary>
    public partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public Settings()
        {
            InitializeComponent();

            filePath_textBox.Text = PluginSettings.FilePath;
        }

        /// <summary>
        /// Сохранение настроек плагина
        /// </summary>
        public void Save()
        {
            PluginSettings.FilePath = filePath_textBox.Text;
        }
    }
}
