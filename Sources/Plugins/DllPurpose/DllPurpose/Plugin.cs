﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Utils.DllPurpose
{
    /// <summary>
    /// Класс настрое плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Назначение библиотек";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.DLL_PURPOSE;

        /// <summary>
        /// Наименования тасков плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Назначение библиотек")]
            DLL_PURPOSE,
            [Description("Назначение функций")]
            FUNC_PURPOSE
        };

        /// <summary>
        /// Наименования тасков отчетов плагина
        /// </summary>
        internal enum TasksReports
        {
            [Description("Формирование отчета")]
            COPY_REPORT
        };

        /// <summary>
        /// Имена файлов отчетов
        /// </summary>
        internal enum reportNames
        {
            [Description("Список библиотек")]
            DLL_REPORT,
            [Description("Список функций")]
            FUNC_REPORT
        }

        //Настройки
        internal static string FilePath;

    }

    /// <summary>
    /// Реализация плагина
    /// </summary>
    public class DllPurpose : IA.Plugin.Interface
    {
        /// <summary>
        /// Настройки плагина
        /// </summary>
        Settings settings;

        /// <summary>
        /// Данные о библиотеках, полученные из БД
        /// </summary>
        internal List<string[]> clearDllData = new List<string[]>();

        /// <summary>
        /// Данные о библиотеках после редактирования пользователем
        /// </summary>
        internal List<string[]> dllSavedData = new List<string[]>();
        
        /// <summary>
        /// Данные о функциях, полученные из БД
        /// </summary>
        internal List<string[]> clearFuncData = new List<string[]>();

        /// <summary>
        /// Данные о функцияхпосле редактирования пользователем
        /// </summary>
        internal List<string[]> funcSavedData = new List<string[]>();
        
        /// <summary>
        /// Отчет по библиотекам
        /// </summary>
        internal List<string[]> dllReport = new List<string[]>();

        /// <summary>
        /// Отчет по функциям
        /// </summary>
        internal List<string[]> funcReport = new List<string[]>();

        /// <summary>
        /// полный список функций, полученные из БД
        /// </summary>
        internal List<string[]> funcsFromBase = null;

        #region PluginSettings Members
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW | Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.RESULT_WINDOW | Plugin.Capabilities.REPORTS | Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            PluginSettings.FilePath = Properties.Settings.Default.FilePath;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.FilePath = settingsBuffer.GetString();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return this.CustomSettingsWindow;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return settings = new Settings();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return new ResultWindow(this);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.FilePath = PluginSettings.FilePath;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.FilePath);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (!File.Exists(PluginSettings.FilePath))
            {
                message = "Файл со списком внешних библиотек не найден.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>() 
                { 
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.DLL_PURPOSE.Description(), 0),
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FUNC_PURPOSE.Description(), 0) 
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return new List<Monitor.Tasks.Task>() { new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.TasksReports.COPY_REPORT.Description(), 2) };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            List<string> dllList = new List<string>();
            Dictionary<string, List<string>> functionsListForDlls = new Dictionary<string, List<string>>();

            // Допустимы два вида входных файлов:
            // файл ExternalFunctions.il - формата <имя библиотеки>\t<имя функции>\t<номер функции> в каждой строке
            // файл ExternalDlls.il - формата <имя библиотеки> в каждой строке

            //Читаем из файла настроек список библиотек и функций для анализа (файл ExternalFunctions.il), либо список библиотек (файл ExternalDlls.il)
            int fileFormat = -1;
            using (StreamReader reader = new StreamReader(PluginSettings.FilePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] split = line.Split('\t');
                    string dll, fun;
                    if (fileFormat == -1)
                        fileFormat = split.Length;
                    if (fileFormat != split.Length)
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Ошибка в формате исходного файла: " + line);
                        return;
                    }
                    switch (split.Length)
                    {
                        case 1:
                            // файл ExternalDlls.il
                            dll = split[0].Trim(new char[] { '[', ']' }).ToLower();
                            if (!dllList.Contains(dll))
                            {
                                dllList.Add(dll);
                            }
                            if (!functionsListForDlls.Keys.Contains(dll))
                            {
                                functionsListForDlls.Add(dll, new List<string>());
                            }
                            break;
                        case 3:
                            // файл ExternalFunctions.il
                            dll = split[0].Trim(new char[] { '[', ']' }).ToLower();
                            fun = split[1].ToLower();
                            if (!dllList.Contains(dll) && !dllList.Contains(dll))
                            {
                                dllList.Add(dll);
                            }
                            if (!functionsListForDlls.Keys.Contains(dll))
                            {
                                functionsListForDlls.Add(dll, new List<string>());
                            }
                            if (!functionsListForDlls[dll].Contains(fun))
                            {
                                functionsListForDlls[dll].Add(fun);
                            }
                            break;
                        default:
                            Monitor.Log.Error(PluginSettings.Name, "Ошибка в формате исходного файла: " + line);
                            return;
                    }
                }
            }

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.DLL_PURPOSE.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)dllList.Count());

            // Заполняем данные по библиотекам
            IA.Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection SQLConnection = new IA.Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection();
            List<string[]> dllsFromBase = SQLConnection.DllBase;
            clearDllData.Clear();
            clearFuncData.Clear();
            ulong cnt = 1;
            if (dllList.Count > 0)
            {
                int id = -1;
                foreach (string dll in dllList)
                {
                    var found = dllsFromBase.Where(x => (x[1].ToLower() == dll || x[1].ToLower() == (dll+".dll")));
                    if (found.Count() > 0)
                    {
                        string[] firstFound = found.First();
                        firstFound[1] = dll;
                        clearDllData.Add(found.First());
                    }
                    else
                        clearDllData.Add(new string[] { id--.ToString(), dll, "", "", "", "", "0" });
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.DLL_PURPOSE.Description(), Monitor.Tasks.Task.UpdateType.STAGE, cnt++);
                }
            }

            clearDllData = clearDllData.Distinct().ToList();

            if (fileFormat == 3)
            {
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNC_PURPOSE.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)clearDllData.Count());
                cnt = 1;
                // Заполняем данные по функциям
                funcsFromBase = SQLConnection.Functions;
                int negatideID = -1;

                // файл ExternalFunctions.il
                foreach (string[] dll in clearDllData)
                {
                    List<string> dllIDs = new List<string>();
                    var found = dllsFromBase.Where(x => x[1].ToLower() == dll[1].ToLower()).Select(x => x[0]);
                    if (found.Count() > 0)
                    {
                        dllIDs.Add(found.FirstOrDefault());
                        var func = funcsFromBase.Where(x => dllIDs.Contains(x[1]) && functionsListForDlls[dll[1].ToLower()].Contains(x[2].ToLower()));
                        if (func.Count() > 0)
                            clearFuncData.AddRange(func); // функция есть в базе
                        else
                        {
                            // функции нет в базе
                            clearFuncData.AddRange(functionsListForDlls[dll[1].ToLower()].Select(x => new string[] { negatideID--.ToString(), dll[0], x, "", "", "" }));
                        }
                    }
                    else
                    {
                        // Найденных библиотек нет в базе
                        clearFuncData.AddRange(functionsListForDlls[dll[1].ToLower()].Select(x => new string[] { negatideID--.ToString(), dll[0], x, "", "", "" }));
                    }
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FUNC_PURPOSE.Description(), Monitor.Tasks.Task.UpdateType.STAGE, cnt++);
                }
            }
        }

        /// <summary>
        /// Формирование выходных данных на основе таблиц библиотек и функций
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
            PrepareReports();
        }

        /// <summary>
        /// Формирование отчетов на основе данных таблиц библиотек и функций
        /// </summary>
        private void PrepareReports()
        {
            dllReport = new List<string[]>()
            {
                new string[] { "№", "Наименование", "Автор", "Описание", "Версия", "Исследование" }
            };

            funcReport = new List<string[]>()
            {
                new string[] { "№", "Библиотека", "Функция", "Описание", "Ordinal", "Исследование" }
            };

            foreach (string[] dll in dllSavedData)
            {
                dll[7] = dll[7] == "false" ? "Не требует исследования" : dll[7] == "Не требует исследования" ? "Не требует исследования" : "Требует исследования";
                dllReport.Add(new string[] { dll[0], dll[2], dll[3], dll[4], dll[6], dll[7] });
            }

            foreach (string[] func in funcSavedData)
            {
                func[7] = func[7] == "false" ? "Не требует исследования" : func[7] == "Не требует исследования" ? "Не требует исследования" : "Требует исследования";
                funcReport.Add(new string[] { func[0], func[1], func[4], func[5], func[6], func[7] });
            }

        }


        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            string repDllPath = Path.Combine(reportsDirectoryPath, PluginSettings.reportNames.DLL_REPORT.Description() + ".xls");
            string repFuncPath = Path.Combine(reportsDirectoryPath, PluginSettings.reportNames.FUNC_REPORT.Description() + ".xls");

            XMLReport.XMLReport.Report(repDllPath, "Отчет по библиотекам", dllReport);
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReports.COPY_REPORT.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 1);
            XMLReport.XMLReport.Report(repFuncPath, "Отчет по Функциям", funcReport);
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReports.COPY_REPORT.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 2);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }
        #endregion
    }
}
