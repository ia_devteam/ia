﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Store;
using FileOperations;

namespace IA.Plugins.Analyses.Reporter
{
    /// <summary>
    /// Класс, реализующий логику работы плагина при генерации отчётов
    /// </summary>
    internal static class ReportsGenerator
    {
        /// <summary>
        /// Генерация отчёта связей по управлению
        /// </summary>
        internal static void GenerateFunctionByControl_Calles(Storage storage, string reportsDirectoryPath)
        {

            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Связи по управлению - вызовы.txt")))
            {
                if (!storage.functions.EnumerateFunctions(enFunctionKind.REAL).Any())
                {// Если ни одной функции нет
                    writer.WriteLine("Функций не выявлено");
                    return;
                }

                //функции есть

                writer.WriteLine("Связи по управлению. Функция, представленная слева от стрелки, вызывает функцию, представленную справа от стрелки");
                writer.WriteLine();

                foreach (IFunction func_caller_1 in storage.functions.EnumerateFunctions(enFunctionKind.REAL))
                {
                    var funcCalles = func_caller_1.CallesUniq().Where(f => f.Name != "Глобальное окружение");
                    if (funcCalles.Any())
                    {
                        writer.WriteLine(func_caller_1.ToShortStringForReports());
                        foreach (IFunction func_caller_2 in funcCalles)
                            writer.WriteLine("   ->" + func_caller_2.ToShortStringForReports());
                    }
                }
            }
        }

        internal static void GenerateFunctionByControl_CalledBy(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Связи по управлению - вызывающие функции.txt")))
            {
                if (!storage.functions.EnumerateFunctions(enFunctionKind.REAL).Any())
                {// Если ни одной функции нет
                    writer.WriteLine("Функций не выявлено");
                    return;
                }

                //функции есть

                writer.WriteLine("Связи по управлению. Функция, представленная слева от стрелки, вызывается функцией, представленной справа от стрелки");
                writer.WriteLine();

                foreach (IFunction func_caller_1 in storage.functions.EnumerateFunctions(enFunctionKind.REAL))
                {
                    var funcCalled = func_caller_1.CalledByUniq().Where(f => f.Name != "Глобальное окружение");
                    if (funcCalled.Any())
                    {
                        writer.WriteLine(func_caller_1.ToShortStringForReports());
                        foreach (IFunction func_caller_2 in funcCalled)
                            writer.WriteLine("   <-" + func_caller_2.ToShortStringForReports());
                    }
                }
            }
        }

        internal static void GenerateFunctionByControl_Forward(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Связи по управлению - прямая зависимость.txt")))
            {
                if (!storage.functions.EnumerateFunctions(enFunctionKind.REAL).Any())
                {// Если ни одной функции нет
                    writer.WriteLine("Функций не выявлено");
                    return;
                }

                //функции есть

                writer.WriteLine("В данном файле для каждой функции перечислены все вызываемые ей функции.");
                writer.WriteLine();

                foreach (IFunction func in storage.functions.EnumerateFunctions(enFunctionKind.REAL))
                {
                    List<IFunction> calls = new List<IFunction>();
                    foreach (IFunction funct in func.CallesUniq().Where(f => f.kind == enFunctionKind.REAL))
                        calls.Add(funct);

                    writer.Write(func.ToShortStringForReports());
                    writer.WriteLine("   Всего вызовов: " + calls.Count());
                    foreach (IFunction funct in calls)
                    {
                        writer.Write(funct.ToStringForReports(1));
                    }
                }
            }
        }

        internal static void GenerateFunctionByControl_Backwards(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Связи по управлению - обратная зависимость.txt")))
            {
                if (!storage.functions.EnumerateFunctions(enFunctionKind.REAL).Any())
                {// Если ни одной функции нет
                    writer.WriteLine("Функций не выявлено");
                    return;
                }

                //функции есть

                writer.WriteLine("В данном файле для каждой функции перечислены все вызывающие ее функции.");
                writer.WriteLine();

                foreach (IFunction func in storage.functions.EnumerateFunctions(enFunctionKind.REAL))
                {
                    List<IFunction> called = new List<IFunction>();
                    foreach (IFunction funct in func.CalledByUniq())
                        if (funct.kind == enFunctionKind.REAL)
                            called.Add(funct);

                    writer.WriteLine(func.ToShortStringForReports() + "   Всего вызывается: " + called.Count());
                    foreach (IFunction funct in called)
                    {
                        writer.Write(funct.ToStringForReports(1));
                    }
                }
            }
        }

        /// <summary>
        /// Генерация отчета о функциях, не имеющих вызовов.
        /// </summary>
        internal static void GenerateNotCalledFunction(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Не вызываемые функции.txt")))
            {
                if (!storage.functions.EnumerateFunctions(enFunctionKind.REAL).Any())
                {// Если ни одной функции нет
                    writer.WriteLine("Функций не выявлено");
                    return;
                }

                //Есть хотя бы одна функция

                writer.WriteLine("В данном файле перечислены функции, не имеющие вызовов");
                writer.WriteLine();

                bool isUsed = false;

                foreach (IFunction func in storage.functions.EnumerateFunctions(enFunctionKind.REAL))
                {
                    if (func.Name == "Глобальное окружение") //Прячем "Глобальное окружение".
                        continue;

                    if (!func.CalledBy().Any())
                    {
                        isUsed = true;

                        writer.WriteLine(func.ToStringForReports());
                    }
                }

                if (!isUsed)
                    writer.WriteLine(" Ни одной функции, не имеющей вызовов, не выявлено");
            }
        }


        /// <summary>
        /// Генерация отчета о висячих функциях.
        /// </summary>
        internal static void GenerateFlyingFunction(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Висячие функциональные объекты.txt")))
            {
                if (!storage.functions.EnumerateFunctions(enFunctionKind.REAL).Any())
                {// Если ни одной функции нет
                    writer.WriteLine("Функций не выявлено");
                    return;
                }

                //Есть хотя бы одна функция

                writer.WriteLine("В данном файле перечислены функции, одновременно не имеющие вызовов и не вызывающие другие функции");
                writer.WriteLine();

                bool isUsed = false;

                foreach (IFunction func in storage.functions.EnumerateFunctions(enFunctionKind.REAL))
                {
                    if (func.Name == "Глобальное окружение") //Прячем "Глобальное окружение".
                        continue;

                    if (!func.CalledBy().Any() && !func.Calles().Any())
                    {
                        isUsed = true;

                        writer.WriteLine(func.ToStringForReports());
                    }
                }

                if (!isUsed)
                    writer.WriteLine(" Ни одной висячей функции не выявлено");
            }
        }



        /// <summary>
        /// Генерация отчета о функциях, не вызывающих другие функции.
        /// </summary>
        internal static void GenerateFunctionsNotCallingAnyFunction(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Не вызывающие функции.txt")))
            {
                if (!storage.functions.EnumerateFunctions(enFunctionKind.REAL).Any())
                {// Если ни одной функции нет
                    writer.WriteLine("Функций не выявлено");
                    return;
                }

                writer.WriteLine("В данном файле перечислены функции, не вызовающие ни одной из зарегистрированных в Хранилище функций");
                writer.WriteLine();

                bool isUsed = false;

                foreach (IFunction func in storage.functions.EnumerateFunctions(enFunctionKind.REAL))
                {
                    if (!func.Calles().Any())
                    {
                        isUsed = true;
                        writer.WriteLine(func.ToStringForReports());
                    }
                }

                if (!isUsed)
                    writer.WriteLine(" Ни одной функции, не вызовающие ни одной из зарегистрированных в Хранилище функций, не выявлено");
            }
        }



        /// <summary>
        /// Генерация отчёта связей по информации.
        /// </summary>
        internal static void GenerateFunctionByInformation(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Связи по информации.txt")))
            {
                writer.WriteLine("В данном файле представлены функции, использующие одну и ту же переменную");
                writer.WriteLine();

                //Для каждой переменной строим список использующих её функций
                var functions = new Dictionary<ulong, HashSet<ulong>>();

                foreach (Variable var in storage.variables.EnumerateVariables())
                {
                    foreach (Location loc in var.ValueCallPosition().Union(var.ValueGetPosition()).Union(var.ValueSetPosition()))
                    {
                        var funcId = loc.GetFunctionId();
                        if (funcId == Store.Const.CONSTS.WrongID)
                            continue;

                        var varId = var.Id;
                        if (!functions.ContainsKey(varId))
                            functions.Add(varId, new HashSet<ulong>());
                        functions[varId].Add(funcId);
                    }
                }

                //Для каждой переменной, которая используется более чем одной функцией, добавляем запись в протокол (они связаны по информации)
                foreach (var pair in functions)
                {
                    if (pair.Value.Count > 1)
                    {
                        var funcs = pair.Value.Select(id => storage.functions.GetFunction(id)).Where(f => f.kind == enFunctionKind.REAL).ToArray();

                        if (funcs.Length > 1)
                        {

                            writer.WriteLine("Связь по переменной " + storage.variables.GetVariable(pair.Key).ToShortStringForReports() + " между функциями ");

                            foreach (IFunction func in funcs)
                                writer.WriteLine("   " + func.ToShortStringForReports());

                            writer.WriteLine();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Генерация отчёта, в котором выписывается перечень мест использования переменных.
        /// </summary>
        internal static void GenerateVariableUses(Storage storage, string reportsDirectoryPath)
        {
            #region Связи по информации.txt
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Информационные объекты.txt")))
            {
                if (!storage.variables.EnumerateVariables().Any())
                { //Если пеерменных нет
                    writer.WriteLine("Ни одной переменной не распознано");
                    return;
                }

                //Хотя бы одна переменная имеется

                writer.WriteLine("В данном файле для каждой переменной указываются места её использования");
                writer.WriteLine();

                foreach (Variable var in storage.variables.EnumerateVariables())
                {
                    writer.Write(var.ToStringForReports());

                    bool isUsed = false;

                    Location[] locs;
                    if ((locs = var.ValueCallPosition()).Length > 0)
                    {
                        writer.Write(locs.ToStringWithNewLines("Вызовы по указателю"));
                        isUsed = true;
                    }

                    if ((locs = var.ValueGetPosition()).Length > 0)
                    {
                        writer.Write(locs.ToStringWithNewLines("Чтения значения переменной"));
                        isUsed = true;
                    }

                    if ((locs = var.ValueSetPosition()).Length > 0)
                    {
                        writer.Write(locs.ToStringWithNewLines("Изменения значения переменной"));
                        isUsed = true;
                    }

                    if (!isUsed)
                        writer.Write("   Переменная не используется ");

                    writer.WriteLine();
                }
            }
            #endregion
        }

        /// <summary>
        /// Генерация отчёта, в котором выписывается перечень чтения всех переменных.
        /// </summary>
        internal static void GenerateVariableReades(Storage storage, string reportsDirectoryPath)
        {
            #region Код с чтением инф. объектов.txt
            using (StreamWriter writer_read = new StreamWriter(Path.Combine(reportsDirectoryPath, "Код с чтением информационных объектов.txt")))
            {
                if (!storage.variables.EnumerateVariables().Any())
                { //Если пеерменных нет
                    writer_read.WriteLine("Ни одной переменной не распознано");
                    return;
                }

                //Хотя бы одна переменная имеется

                writer_read.WriteLine("В данном файле для каждой переменной указывается исходный код, в котором осуществляется доступ по чтению до этой переменной");
                writer_read.WriteLine();

                foreach (Variable var in storage.variables.EnumerateVariables())
                {
                    writer_read.Write(var.ToStringForReports());

                    Location[] locs;
                    if ((locs = var.ValueGetPosition()).Length > 0)
                    {
                        writer_read.Write(locs.ToStringWithNewLines("Чтения значения переменной", InsertSourceCodeLine: true));
                    }
                    else
                    {
                        writer_read.WriteLine("   Чтения значения переменной не обнаружено");
                    }
                }
            }
            #endregion
        }

        /// <summary>
        /// Генерация отчёта, в котором выписывается перечень изменения всех переменных.
        /// </summary>
        internal static void GenerateVariableWrites(Storage storage, string reportsDirectoryPath)
        {
            #region Код с изменением инф. объектов.txt
            using (StreamWriter writer_write = new StreamWriter(Path.Combine(reportsDirectoryPath, "Код с изменением информационных объектов.txt")))
            {
                if (!storage.variables.EnumerateVariables().Any())
                { //Если пеерменных нет
                    writer_write.WriteLine("Ни одной переменной не распознано");
                    return;
                }

                //Хотя бы одна переменная имеется

                writer_write.WriteLine("В данном файле для каждой переменной указывается исходный код, в котором осуществляется доступ по записи до этой переменной");
                writer_write.WriteLine();

                foreach (Variable var in storage.variables.EnumerateVariables())
                {
                    writer_write.Write(var.ToStringForReports());

                    Location[] locs;
                    if ((locs = var.ValueSetPosition()).Length > 0)
                    {
                        writer_write.Write(locs.ToStringWithNewLines("Изменения значения переменной", InsertSourceCodeLine: true));
                    }
                    else
                    {
                        writer_write.WriteLine("   Изменение значения переменной не обнаружено");
                    }
                }
            }
            #endregion
        }

        /// <summary>
        /// Генерация отчёта, в котором выписывается перечень вызовов по указателю всех переменных.
        /// </summary>
        internal static void GenerateVariablePointerCalles(Storage storage, string reportsDirectoryPath)
        {
            #region Код с вызовом по указателю инф. объектов.txt
            using (StreamWriter writer_ref = new StreamWriter(Path.Combine(reportsDirectoryPath, "Код с вызовом по указателю информационных объектов.txt")))
            {
                if (!storage.variables.EnumerateVariables().Any())
                { //Если пеерменных нет
                    writer_ref.WriteLine("Ни одной переменной не распознано");
                    return;
                }

                //Хотя бы одна переменная имеется

                writer_ref.WriteLine("В данном файле для каждой переменной указывается исходный код, в котором осуществляется вызов по указателю из этой переменной");
                writer_ref.WriteLine();

                foreach (Variable var in storage.variables.EnumerateVariables())
                {
                    writer_ref.Write(var.ToStringForReports());

                    Location[] locs;
                    if ((locs = var.ValueCallPosition()).Length > 0)
                    {
                        writer_ref.Write(locs.ToStringWithNewLines("Вызовы по указателю", InsertSourceCodeLine: true));
                    }
                    else
                    {
                        writer_ref.WriteLine("   Вызовов по указателю не обнаружено");
                    }
                }
            }
            #endregion
        }

        /// <summary>
        /// Генерация отчета о переменных, которые не используются. При этом переменным могут присваиваться значения.
        /// </summary>
        internal static void GenerateNeverUsedVariables(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Переменные, значения которых не используются.txt")))
            {
                if (!storage.variables.EnumerateVariables().Any())
                { //Если переменных нет
                    writer.WriteLine("Ни одной переменной не распознано");
                    return;
                }

                //Хотя бы одна переменная имеется

                writer.WriteLine("В данном файле приводится перечень переменных, значения которых могут изменяться, но никогда не читаются или не используются как вызов по указателю");
                writer.WriteLine();

                bool isHave = false;

                foreach (Variable var in storage.variables.EnumerateVariables())
                {
                    bool isUsed = var.ValueCallPosition().Any() || var.ValueGetPosition().Any();

                    if (!isUsed)
                    {
                        isHave = true;
                        writer.Write(var.ToStringForReports());

                        Location[] locs;
                        if ((locs = var.ValueSetPosition()).Length > 0)
                            writer.Write(locs.ToStringWithNewLines("Изменения значения переменной"));
                        else
                            writer.WriteLine("   Значения переменной не присваиваются ");

                        writer.WriteLine();
                    }
                }

                if (!isHave)
                    writer.WriteLine("Переменных, значения которых могут изменяться, но никогда не читаются или не используются как вызов по указателю, не обнаружено");
            }
        }

        /// <summary>
        /// Генерация отчета о переменных, которым не присваиваются значения.
        /// </summary>
        internal static void GenerateNeverSetVariables(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Переменные, не имеющие присвоений.txt")))
            {
                if (!storage.variables.EnumerateVariables().Any())
                { //Если переменных нет
                    writer.WriteLine("Ни одной переменной не распознано");
                    return;
                }

                //Хотя бы одна переменная имеется

                writer.WriteLine("В данном файле приводится перечень переменных, которым никогда не присваиваются значения");
                writer.WriteLine();

                bool isHave = false;

                foreach (Variable var in storage.variables.EnumerateVariables())
                {
                    bool isUsed = var.ValueSetPosition().Any();

                    if (!isUsed)
                    {
                        isHave = true;

                        writer.Write(var.ToStringForReports());

                        bool haveUses = false;

                        Location[] locs;

                        if ((locs = var.ValueGetPosition()).Length > 0)
                        {
                            writer.Write(locs.ToStringWithNewLines("Чтения значения переменной"));
                            haveUses = true;
                        }

                        if ((locs = var.ValueCallPosition()).Length > 0)
                        {
                            writer.Write(locs.ToStringWithNewLines("Вызовы по указателю"));
                            haveUses = true;
                        }

                        if (haveUses == false)
                            writer.WriteLine("   Значение переменной не читается ");

                        writer.WriteLine();
                    }
                }

                if (!isHave)
                    writer.WriteLine("Переменных, которым никогда не присваиваются значения, не обнаружено");
            }
        }

        internal static void GenerateVariablesNeverSetNeverRead(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Переменные без записи и чтения.txt")))
            {
                if (!storage.variables.EnumerateVariables().Any())
                {
                    writer.WriteLine("Ни одной переменной не распознано");
                    return;
                }

                writer.WriteLine("В данном файле приводится перечень переменных, которым никогда не присваиваются значения и никогда не производится чтения значния.");
                writer.WriteLine();

                foreach (var var in storage.variables.EnumerateVariables()
                        .Where(var => !string.IsNullOrWhiteSpace(var.FullName)
                                    && var.ValueCallPosition().Length == 0
                                    && var.ValueGetPosition().Length == 0
                                    && var.ValueSetPosition().Length == 0))
                {
                    writer.WriteLine(var.ToStringForReports());
                }
            }
        }

        internal static void GenerateVariablesNeverSetNeverReadFUllTextSearch(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Переменные без записи и чтения (полнотекстовый поиск).txt")))
            {
                if (!storage.variables.EnumerateVariables().Any())
                {
                    writer.WriteLine("Ни одной переменной не распознано");
                    return;
                }

                writer.WriteLine("В данном файле приводится перечень переменных, которым никогда не присваиваются значения и никогда не производится чтения значния.");
                writer.WriteLine();

                HashSet<string> usedNames = new HashSet<string>();

                StringBuilder sb = new StringBuilder(100);
                foreach (var var in storage.variables.EnumerateVariables()
                        .Where(var => !string.IsNullOrWhiteSpace(var.FullName)
                                    && var.ValueCallPosition().Length == 0
                                    && var.ValueGetPosition().Length == 0
                                    && var.ValueSetPosition().Length == 0))
                {

                    string name = var.Name;

                    writer.WriteLine(var.ToStringForReports());

                    if (usedNames.Contains(name)) //незачем выводить полнотекстовые вхождения переменной, для одноимённой копии которой уже всё выведено
                        continue;

                    var occurencies = storage.filesIndex.Find(name);

                    writer.WriteLine("    Перечень строк полнотекстовых вхождений:");
                    foreach (var occ in occurencies)
                    {
                        sb.Append("        ");
                        sb.Append(occ.File.FileNameForReports);
                        sb.Append("\t");
                        sb.Append(occ.Line);
                        writer.WriteLine(sb.ToString());
                        sb.Clear();
                    }

                    usedNames.Add(name);
                }
            }
        }

        /// <summary>
        /// Генерация отчета о функциях.
        /// </summary>
        internal static void GenerateFunctionsList(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Перечень функций.txt")))
            {
                if (!storage.functions.EnumerateFunctions(enFunctionKind.REAL).Any())
                { //Если переменных нет
                    writer.WriteLine("Ни одной функции не распознано");
                    return;
                }

                //Хотя бы одна переменная имеется                

                writer.WriteLine("В данном файле представлен перечень всех функций, упоминаемых в исходных текстах.");
                writer.WriteLine();

                foreach (IFunction func in storage.functions.EnumerateFunctions(enFunctionKind.REAL))
                {
                    writer.WriteLine(func.FullNameForReports);

                    Location funcDefenition = func.Definition();
                    if (funcDefenition != null)
                        writer.WriteLine("  определена " + funcDefenition.ToStringWithDot());

                    foreach (Location loc in func.Declarations())
                        writer.WriteLine("  объявлена " + loc.ToStringWithDot());

                    foreach (IFunctionCall call in func.CalledBy())
                        if (call.CallLocation != null)
                            writer.WriteLine("  вызывается " + call.CallLocation.ToStringWithDot());
                        else
                            IA.Monitor.Log.Error(PluginSettings.Name, "Для вызова функцией " + call.Caller.FullName + " функции " + call.Calles.FullName + " не заполнен Location ");

                    foreach (PointsToFunction points in func.OperatorsGetPointerToFunction())
                        if (points.position != null)
                        {
                            if (points.isGetAddress)
                                writer.WriteLine("  берётся указатель " + points.position.ToStringWithDot());
                        }
                        else
                            IA.Monitor.Log.Error(PluginSettings.Name, "Для взятия указателя на функцию " + func.FullName + "для переменной " + points.variable.FullName + " не заполнен Location ");

                    writer.WriteLine();
                }
            }
        }

        /// <summary>
        /// Генерация отчета о переменных.
        /// </summary>
        internal static void GenerateVariablesList(Storage storage, string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Перечень переменных.txt")))
            {
                if (!storage.variables.EnumerateVariables().GetEnumerator().MoveNext())
                { //Если переменных нет
                    writer.WriteLine("Ни одной переменной не распознано");
                    return;
                }

                //Хотя бы одна переменная имеется

                writer.WriteLine("В данном файле представлен перечень всех переменных, упоминаемых в исходных текстах.");
                writer.WriteLine();

                foreach (Variable var in storage.variables.EnumerateVariables())
                {
                    writer.WriteLine(var.FullName);

                    foreach (Location loc in var.Definitions())
                        writer.WriteLine("  определена " + loc.ToStringWithDot());

                    foreach (Location loc in var.ValueGetPosition())
                        writer.WriteLine("  читается " + loc.ToStringWithDot());

                    foreach (Location loc in var.ValueSetPosition())
                        writer.WriteLine("  изменяется " + loc.ToStringWithDot());

                    foreach (Location loc in var.ValueCallPosition())
                        writer.WriteLine("  вызов по указателю " + loc.ToStringWithDot());

                    writer.WriteLine();
                }
            }
        }

        internal static void GenerateFileMD5Doc(Storage storage, string reportsDirectoryPath)
        {
            using (Novacode.DocX doc = Novacode.DocX.Create(Path.Combine(reportsDirectoryPath, "FilesMD5.docx"), Novacode.DocumentTypes.Document))
            {
                var files = storage.files.EnumerateFiles(enFileKind.fileWithPrefix).ToList();
                Novacode.Table table = doc.AddTable(files.Count, 4);

                var row = table.InsertRow(0);
                row.Cells[0].Paragraphs.First().Append("№ п/п").Alignment = Novacode.Alignment.center; ;
                row.Cells[1].Paragraphs.First().Append("Имя файла").Alignment = Novacode.Alignment.center; ;
                row.Cells[2].Paragraphs.First().Append("Размер файла, байт").Alignment = Novacode.Alignment.center; ;
                row.Cells[3].Paragraphs.First().Append("Контрольная сумма содержимого файла, рассчитанная по алгоритму MD5").Alignment = Novacode.Alignment.center; ;

                Dictionary<string, List<IFile>> filesByCatalogs = new Dictionary<string, List<IFile>>();

                foreach (var file in files)
                {
                    string parentDir = Path.GetDirectoryName(file.RelativeFileName_Original);
                    if (!filesByCatalogs.ContainsKey(parentDir))
                        filesByCatalogs.Add(parentDir, new List<IFile>());

                    filesByCatalogs[parentDir].Add(file);
                }

                int ind = 1;
                int fileind = 1;
                foreach (var dir in filesByCatalogs.Keys)
                {
                    row = table.InsertRow(ind++);

                    row.MergeCells(0, 3);

                    row.Cells[0].Paragraphs.First().Append("Каталог " + dir).Alignment = Novacode.Alignment.center;
                    foreach (var p in row.Cells[0].Paragraphs.Skip(1))
                        p.Remove(false);

                    foreach (var file in filesByCatalogs[dir])
                    {
                        var cells = table.Rows[ind++].Cells;
                        cells[0].Paragraphs.First().Append(fileind++.ToString());
                        cells[1].Paragraphs.First().Append(file.NameAndExtension);
                        cells[2].Paragraphs.First().Append(new FileInfo(file.FullFileName_Original).Length.ToString());
                        cells[3].Paragraphs.First().Append(BitConverter.ToString(file.CheckSum).Replace("-", ""));
                    }
                }

                row = table.InsertRow(ind++);
                row.MergeCells(0, 3);
                row.Cells[0].Paragraphs.First().Append("КОНЕЦ").Bold().Alignment = Novacode.Alignment.center;
                foreach (var p in row.Cells[0].Paragraphs.Skip(1))
                    p.Remove(false);

                doc.InsertTable(table);
                doc.Save();
            }
        }

        #region Вспомогательные методы
        /// <summary>
        /// Получить строку из исходников с соответствующим размещением.
        /// </summary>
        /// <param name="loc"></param>
        private static string GetLocationSourceLine(this Location loc)
        {
            try
            {
                AbstractReader fr = new AbstractReader(loc.GetFile().FullFileName_Original);
                return fr.getLineN((int)loc.GetLine());
            }
            catch { }
            return string.Empty;
        }


        //оптимизация построения мириад строк
        static StringBuilder sbStatic = new StringBuilder(2048);
        static String[] nestings = new string[]
        {
            new string(' ', 3 * 0),
            new string(' ', 3 * 1),
            new string(' ', 3 * 2),
        };

        /// <summary>
        /// Сформировать строку из размещения с точкой на конце с учётом желаемого отступа.
        /// </summary>
        /// <param name="loc"></param>
        /// <param name="nestingLevel"></param>
        /// <returns></returns>
        private static string ToStringWithDot(this Location loc, byte nestingLevel = 0)
        {
            nestingLevel = Math.Min(nestingLevel, (byte)(nestings.Length - 1));
            return nestings[nestingLevel] + loc.ToString() + ".";
        }

        /// <summary>
        /// Сформировать строку с переносами строк из указанных размещений. С отступом перед prefix и двойным отступом перед размещениями.
        /// </summary>
        /// <param name="locs"></param>
        /// <param name="prefix"></param>
        /// <param name="InsertSourceCodeLine"></param>
        /// <returns></returns>
        private static string ToStringWithNewLines(this Location[] locs, string prefix, bool InsertSourceCodeLine = false)
        {
            sbStatic.Clear();

            sbStatic.Append(nestings[1]);
            sbStatic.AppendLine(prefix);
            foreach (var loc in locs)
            {
                sbStatic.AppendLine(loc.ToStringWithDot(2));
                if (InsertSourceCodeLine)
                {
                    string sourceLine = loc.GetLocationSourceLine();
                    if (!String.IsNullOrWhiteSpace(sourceLine))
                        sbStatic.AppendLine(sourceLine);
                }
            }

            return sbStatic.ToString();
        }

        /// <summary>
        /// Сформировать строку из имени и мест определений (объявлений), разделённых переносами строк. Для построения используется StringBuilder. Учитывается уровень вложенности путём добевления кратного количества пробелов.
        /// </summary>
        /// <param name="func"></param>
        /// <param name="nestingLevel">Уровень вложенности. 0-based. Max = 2.</param>
        /// <returns></returns>
        private static string ToStringForReports(this IFunction func, byte nestingLevel = 0)
        {
            sbStatic.Clear();
            nestingLevel = Math.Min(nestingLevel, (byte)(nestings.Length - 1));

            string nestingString = nestings[nestingLevel];

            sbStatic.Append(nestingString);
            sbStatic.Append("Функция ");
            sbStatic.AppendLine(func.FullNameForReports);

            if (func.Definition() != null)
            {
                sbStatic.Append(nestingString);
                sbStatic.Append("   Определена ");
                sbStatic.Append(func.Definition().ToString());
                sbStatic.AppendLine(".");
            }

            foreach (Location loc in func.Declarations())
            {
                sbStatic.Append(nestingString);
                sbStatic.Append("   Объявление ");
                sbStatic.Append(loc.ToString());
                sbStatic.AppendLine(".");
            }

            return sbStatic.ToString();
        }

        /// <summary>
        /// Сформировать строку из имени и места определения.
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        private static string ToShortStringForReports(this IFunction func)
        {
            var loc = func.Definition();
            string location = loc == null ? "<не задано>" : loc.ToString();

            return func.FullNameForReports + " (Определена " + location + ")";
        }

        /// <summary>
        /// Сформировать строку из имени и мест определений, разделённых переносами строк. Для построения используется StringBuilder.
        /// </summary>
        /// <param name="var"></param>
        /// <returns></returns>
        private static string ToStringForReports(this Variable var)
        {
            sbStatic.Clear();
            sbStatic.Append("Переменная ");
            sbStatic.AppendLine(var.FullName);

            foreach (Location loc in var.Definitions())
            {
                sbStatic.Append("   Определена ");
                sbStatic.Append(loc.ToString());
                sbStatic.AppendLine(".");
            }

            return sbStatic.ToString();
        }

        /// <summary>
        /// Сформировать строку из имени и места определения.
        /// </summary>
        /// <param name="var"></param>
        /// <returns></returns>
        private static string ToShortStringForReports(this Variable var)
        {
            var loc = var.Definitions().FirstOrDefault();
            string location = loc == null ? "<не задано>" : loc.ToString();
            return var.FullName + " (Определена " + location + ")";
        }

        #endregion

    }
}
