using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Analyses.Reporter
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.REPORTER;

        /// <summary>
        /// Имя плагина
        /// </summary>
        internal const string Name = "Генератор статических отчётов";

        /// <summary>
        /// Наименования задач плагина при генерации отчётов
        /// </summary>
        internal enum ReportTasks
        {
            [Description("Формирование отчётов")]
            REPORTS_GENERATION
        };

        internal delegate void CreateReport(Storage storage, string reportsDirectoryPath);

        /// <summary>
        /// Описатель отчёта
        /// </summary>
        internal sealed class Report
        {
            /// <summary>
            /// Функция, при помощи которой строится отчёт
            /// </summary>
            internal CreateReport createReport;
            
            /// <summary>
            /// Битовый флаг, соответствующий данному отчету
            /// </summary>
            internal int reportType; 

            /// <summary>
            /// Имя отчета, отображаемое в настройках
            /// </summary>
            internal string reportName;

            public override string ToString()
            {
                return reportName;
            }

        }


        internal static Report[] reports = {
                                        new Report {createReport = ReportsGenerator.GenerateFileMD5Doc,
                                            reportName = "Список файлов с контрольными суммами в таблице MS Word"},

                                        new Report {createReport = ReportsGenerator.GenerateVariablesNeverSetNeverRead,
                                            reportName = "Перечень переменных, значения которых никогда не читаются, не записываются или не используются как вызов по указателю"},
                                        
                                        new Report {createReport = ReportsGenerator.GenerateVariablesNeverSetNeverReadFUllTextSearch,
                                            reportName = "Перечень переменных, значения которых никогда не читаются, не записываются или не используются как вызов по указателю (Полнотекстовые вхождения)"},

                                        new Report {createReport = ReportsGenerator.GenerateFunctionByControl_Calles,  
                                            reportName = "Связи по управлению - вызовы"},

                                        new Report {createReport = ReportsGenerator.GenerateFunctionByControl_CalledBy,  
                                            reportName = "Связи по управлению - вызывающие функции"},

                                        new Report {createReport = ReportsGenerator.GenerateFunctionByControl_Forward,  
                                            reportName = "Связи по управлению - прямая зависимость"},

                                        new Report {createReport = ReportsGenerator.GenerateFunctionByControl_Backwards,  
                                            reportName = "Связи по управлению - Обратная зависимость"},
                                        
                                        new Report {createReport = ReportsGenerator.GenerateNotCalledFunction, 
                                            reportName = "Функции, не имеющие вызовов"},

                                        new Report {createReport = ReportsGenerator.GenerateFlyingFunction, 
                                            reportName = "Висячие функции"},

                                        new Report {createReport = ReportsGenerator.GenerateFunctionsNotCallingAnyFunction, 
                                            reportName = "Функции, не вызовающие ни одной из зарегистрированных в Хранилище функций"},

                                        new Report {createReport = ReportsGenerator.GenerateFunctionByInformation, 
                                            reportName = "Функции, использующие одну и ту же переменную"},

                                        new Report {createReport = ReportsGenerator.GenerateVariableUses, 
                                            reportName = "Связи по информации"},

                                        new Report {createReport = ReportsGenerator.GenerateVariableReades, 
                                            reportName = "Для каждой переменной указывается исходный код, в котором осуществляется доступ по чтению до этой переменной"},

                                        new Report {createReport = ReportsGenerator.GenerateVariableWrites, 
                                            reportName = "Для каждой переменной указывается исходный код, в котором осуществляется доступ по записи до этой переменной"},

                                        new Report {createReport = ReportsGenerator.GenerateVariablePointerCalles, 
                                            reportName = "Для каждой переменной указывается исходный код, в котором осуществляется вызов по указателю из этой переменной"},

                                        new Report {createReport = ReportsGenerator.GenerateNeverUsedVariables, 
                                            reportName = "Перечень переменных, значения которых могут изменяться, но никогда не читаются или не используются как вызов по указателю"},

                                        new Report {createReport = ReportsGenerator.GenerateNeverSetVariables, 
                                            reportName = "Перечень переменных, которым никогда не присваиваются значения"},

                                        new Report {createReport = ReportsGenerator.GenerateFunctionsList, 
                                            reportName = "Перечень всех функций, упоминаемых в исходных текстах"},

                                        new Report {createReport = ReportsGenerator.GenerateVariablesList, 
                                            reportName = "Перечень всех переменных, упоминаемых в исходных текстах"},

                                    };

        /// <summary>
        /// В reports задаём значение поля reportType.
        /// </summary>
        static PluginSettings()
        {
            int num = 0;
            foreach (Report rep in reports)
            {
                rep.reportType = 1 << num;
                num += 1;
            }
        }

        /// <summary>
        /// Настройка плагина, указывающая, какие отчёты выводить
        /// </summary>
        internal static uint reportTypes;
    }


    /// <summary>
    /// Основной класс плагина
    /// </summary>
    public sealed class Reporter : IA.Plugin.Interface
    {       		
        /// <summary>
        /// Хранилище
        /// </summary>
		Storage storage;


        SettingsWindows settings;

        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.REPORTS |
                        Plugin.Capabilities.RERUN |
                        Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW |
                        Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }    
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            //Проверка на разумность
            if (storage == null)
                throw new Exception("Хранилище не определено.");

            //Проверка на разумность
            if (reportsDirectoryPath == null)
                throw new Exception("Директория для формирования отчётов не определена.");

            //Проверка на разумность
            if (!System.IO.Directory.Exists(reportsDirectoryPath))
                throw new Exception("Директория для формирования отчётов не существует.");


            //считаем число запускаемых отчетов
            uint r = PluginSettings.reportTypes;
            uint count = 0;
            while (r != 0)
            {
                count += 1;
                r = r >> 1;
            }
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.ReportTasks.REPORTS_GENERATION.Description(), Monitor.Tasks.Task.UpdateType.STAGES, count);


            uint stage = 0;
            foreach (var rep in PluginSettings.reports)
            {
                if ((PluginSettings.reportTypes & rep.reportType) == rep.reportType) 
                { 
                    rep.createReport(storage, reportsDirectoryPath);
                    stage+=1;
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.ReportTasks.REPORTS_GENERATION.Description(), Monitor.Tasks.Task.UpdateType.STAGE, stage);
                }
            };
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.reportTypes = Properties.Settings.Default.Reports;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.reportTypes = settingsBuffer.GetUInt32();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            uint num = 1;
            foreach (var item in PluginSettings.reports)
            {
                PluginSettings.reportTypes = PluginSettings.reportTypes | num;

                num = num << 1;
            }
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            Properties.Settings.Default.Reports = PluginSettings.reportTypes;
            Properties.Settings.Default.Save();

            settingsBuffer.Add((int)PluginSettings.reportTypes);
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

		{
            get
            {
                //считаем число запускаемых отчетов
                uint r = PluginSettings.reportTypes;
                uint count = 0;
                while(r != 0)
                {
                    count += 1;
                    r = r >> 1;
                }

                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.ReportTasks.REPORTS_GENERATION.Description(), count)
                };
            }
		}

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return settings = new SettingsWindows(PluginSettings.reports, PluginSettings.reportTypes);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return CustomSettingsWindow;
            }
        }
      
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            if(settings != null)
            {
                PluginSettings.reportTypes = 0;
                uint num = 1;
                foreach(var item in settings.cbList.Items)
                {
                    if (settings.cbList.CheckedItems.Contains(item))
                        PluginSettings.reportTypes = PluginSettings.reportTypes | num;

                    num = num << 1;
                }
            }

            message = string.Empty;

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        #endregion

    }
}
