﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.Reporter
{
    public partial class SettingsWindows : UserControl
    {

        internal SettingsWindows(PluginSettings.Report[] report, uint reportTypes)
        {
            InitializeComponent();

            //Заполняем checklist всеми имеющимися отчетами
            for(int i=0;i<report.Length; i++)
            {
                int rptType = report[i].reportType;
                cbList.Items.Add(report[i], (reportTypes & rptType) == rptType);
            }
        }
    }
}
