﻿using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using Store.Table;
using TestUtils;

using IA.Plugins.Analyses.Reporter;

namespace TestReporter
{
    /// <summary>
    ///This is a test class for ReporterTest and is intended
    ///to contain all ReporterTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ReporterTest
    {
        /// <summary>
        /// Путь к папке, в которой находятся материалы теста
        /// </summary>
        public static string testsFolder;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        private const ulong pluginID = Store.Const.PluginIdentifiers.REPORTER;


        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Тест на проверку составления отчетов связей в Хранилище.
        /// Функции и переменные, а также связи между ними, задаются вручную.
        /// Проверяются составленные отчеты с эталонными.
        /// </summary>
        [TestMethod()]
        public void Reporter_ManualRelations()
        {
            const string testMaterialsSubpath = @"Reporter\ManualRelations\";

            testUtils.RunTest(
                string.Empty,                                           //sourcePrefixPart
                new Reporter(),                                         //plugin
                false,                                                  //isUseCachedStorage
                (storage, testMaterialsPath) => { },                    //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    GenerateRelations(storage);

                    int reportsToDisable = 0;
                    reportsToDisable |= 1 << 0; //FilesMD5
                    reportsToDisable |= 1 << 1; //Переменные без записи и чтения
                    reportsToDisable |= 1 << 2; //Переменные без записи и чтения (полнотекстовый поиск)

                    CustomizePlugin(storage, reportsToDisable);  //Выключаем отчет, содержащий список файлов. Так как живых файлов у нас нет.
                },                                                      //customizeStorage
                (storage, testMaterialsPath) => { return true; },       //checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(testMaterialsPath, testMaterialsSubpath), reportsPath);

                    return true;
                },                                                      //checkReportBeforRerun
                false,                                                  //isUpdateReport
                (plugin, testMaterialsPath) => { },                     //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },       //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }    //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест на проверку составления отчетов связей в Хранилище после работы парсера JavaScript.
        /// Для корректировки связей после парсера запускается плагин "Анализ связей".
        /// Проверяются составленные отчеты с эталонными.
        /// </summary>
        [TestMethod]
        public void Reporter_JavaScriptParser()
        {
            const string sourcePostfixPart = @"Sources\JS\UnexpectedInsertion\";
            const string testMaterialsSubpath = @"Reporter\JavaScriptParser\";

            testUtils.RunTest(
                sourcePostfixPart,                                       //sourcePostfixPart
                new Reporter(),                                         //plugin
                false,                                                   //isUseCachedStorage
                (storage, testMaterialsPath) =>
                {

                },                                                      //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
                    TestUtilsClass.Storage_Files_SetType(storage, Store.SpecialFileTypes.JAVA_SCRIPT_SOURCE);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_JavaScriptParser(storage);
                    TestUtilsClass.Run_PointsToAnalysesSimple(storage);

                    CustomizePlugin(storage, 0);  //Генерируем все отчеты
                },                    //customizeStorage
                (storage, testMaterialsPath) => { return true; },       //checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckReports(Path.Combine(testMaterialsPath, testMaterialsSubpath), reportsPath);

                    return true;
                },                                                      //checkReportBeforRerun
                false,                                                  //isUpdateReport
                (plugin, testMaterialsPath) => { },                     //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },       //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }    //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Настроить выполнение плагина
        /// </summary>
        /// <param name="storage">Маска отчётов, которые печатать НЕ надо.</param>
        private static void CustomizePlugin(Storage storage, int settings)
        {
            int reportsToPrint = -1; //все отчёты

            reportsToPrint ^= settings;

            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(reportsToPrint);
            storage.pluginSettings.SaveSettings(pluginID, buffer);
        }

        /// <summary>
        /// Сравнение сгенерированных отчетов с эталонными
        /// </summary>
        /// <param name="testMaterialsPath">Путь до каталога с эталонными отчетами. Не может быть пустым.</param>
        /// <param name="reportsPath">Путь до каталога с сгенерированными отчетами. Не может быть пустым.</param>
        private void CheckReports(string testMaterialsPath, string reportsPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(testMaterialsPath, reportsPath, true, true);

            Assert.IsTrue(isEqual, "Отчеты плагина не совпадают с эталонными.");
        }

        /// <summary>
        /// Ручное создание связей в Хранилище.
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        private void GenerateRelations(Storage storage)
        {
            Namespace ns = storage.namespaces.FindOrCreate("ns");

            IFunction func1 = storage.functions.AddFunction();
            func1.Name = "f1";
            ns.AddFunctionToNamespace(func1);

            IFunction func2 = storage.functions.AddFunction();
            func2.Name = "f2";
            ns.AddFunctionToNamespace(func2);

            func1.AddCall(func2);

            IFunction func3 = storage.functions.AddFunction();
            func3.Name = "f3";
            ns.AddFunctionToNamespace(func3);

            IFunction func4 = storage.functions.AddFunction();
            func4.Name = "f4";
            ns.AddFunctionToNamespace(func4);

            func2.AddCall(func4);

            IFunction func5 = storage.functions.AddFunction();
            func5.Name = "f5";
            ns.AddFunctionToNamespace(func5);

            func1.AddCall(func5);

            IFile f1 = storage.files.Add(enFileKindForAdd.fileWithPrefix);

            Variable var1 = storage.variables.AddVariable();
            var1.Name = "var1";
            var1.AddValueGetPosition(f1, 1, func1);
            var1.AddValueCallPosition(f1, 2, func2);

            Variable var2 = storage.variables.AddVariable();
            var2.Name = "var2";
            ns.AddVariableToNamespace(var2);
            var2.AddValueSetPosition(f1, 1, func3);

            Variable var3 = storage.variables.AddVariable();
            var3.Name = "var3";
        }
    }
}
