﻿using System;
using System.IO;
using System.Linq;
using System.Text;

using Store;
using IA.Extensions;

namespace IA.Plugins.Analyses.SensorGenerator
{
    /// <summary>
    /// Класс, реализующий логику работы плагина при генерации отчётов
    /// </summary>
    internal class ReportsGenerator
    {
        /// <summary>
        /// Имя файла отчёта
        /// </summary>
        internal const string ReportFileName = "SensorGenerator.log";

        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Директория для формирования отчётов
        /// </summary>
        string reportsDirectoryPath;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="reportsDirectoryPath">Путь до директории для формирования отчётов</param>
        internal ReportsGenerator(Storage storage, string reportsDirectoryPath)
        {
            //Проверка на разумность
            if (storage == null)
                throw new Exception("Хранилище не определено.");

            //Проверка на разумность
            if (reportsDirectoryPath == null)
                throw new Exception("Директория для формирования отчётов не определена.");

            //Проверка на разумность
            if (!Directory.Exists(reportsDirectoryPath))
                throw new Exception("Директория для формирования отчётов не существует.");

            this.storage = storage;
            this.reportsDirectoryPath = reportsDirectoryPath;
        }


        /// <summary>
        /// Запуск генерации отчёта
        /// </summary>
        /// <returns>Успешно ли сгенерирован отчёт?</returns>
        internal bool Run()
        {
          
            try
            {
                //Задаём максимальный прогресс
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReports.SENSORS_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, this.storage.sensors.Count());

                using (StreamWriter writer = new StreamWriter(Path.Combine(this.reportsDirectoryPath, ReportFileName), false, Encoding.UTF8))
                {
                    ulong counter = 0;

                    foreach (Sensor sensor in this.storage.sensors.EnumerateSensors())
                    {
                        IStatement statement = sensor.GetBeforeStatement();
                        IFunction function = this.storage.functions.GetFunction(sensor.GetFunctionID());

                        // Добавляем тип датчика в протокол
                        string sensorKind = "s";

                        switch (sensor.Kind)
                        {
                            case Kind.START:
                                sensorKind = "s"; break;
                            case Kind.FINAL:
                                sensorKind = "f"; break;
                            case Kind.INTERNAL:
                                sensorKind = "i"; break;
                        }                        
                        
                        if (statement != null)
                        {
                            if (function != null)
                            {
                                // Проверяем, что имя функции не пустая строка и не равна имени файла, чтобы в протоколе не было путаницы
                                string functionName = statement.FirstSymbolLocation.GetFile().FullFileName_Original.Equals(function.FullNameForReports) ? "Вне функции" : function.FullNameForReports;
                                writer.WriteLine(sensorKind + "\t" + sensor.ID + "\t" + functionName + "\t" + statement.FirstSymbolLocation.GetFile() + "\t" + statement.FirstSymbolLocation.GetLine().ToString() + "\t" + statement.FirstSymbolLocation.GetColumn().ToString());
                            }
                            else
                                IA.Monitor.Log.Error(PluginSettings.Name, "Датчику " + sensor.ID + " не присвоена функция. Файл: " + statement.FirstSymbolLocation.GetFile() + " Строка: " + statement.FirstSymbolLocation.GetLine());
                        }
                        else
                        {
                            if (function != null)
                            {
                                Location functionDefinitions = function.Definition();
                                if (functionDefinitions != null)
                                {
                                    // Проверяем, что имя функции не пустая строка и не равна имени файла, чтобы в протоколе не было путаницы
                                    string functionName = functionDefinitions.GetFile().FullFileName_Original.Equals(function.FullNameForReports) ? "Вне функции" : function.FullNameForReports;
                                    writer.WriteLine(sensorKind + "\t" + sensor.ID + "\t" + functionName + "\t" + functionDefinitions.GetFile() + "\t" + functionDefinitions.GetLine().ToString() + "\t" + functionDefinitions.GetColumn().ToString());
                                }
                                else
                                    IA.Monitor.Log.Error(PluginSettings.Name, "Датчику " + sensor.ID + " присвоена функция " + function.FullNameForReports + " у которой не найдено ни одного определения.");
                            }
                            else
                                IA.Monitor.Log.Error(PluginSettings.Name, "Датчику " + sensor.ID + " не присвоена функция.");
                        }

                        //Задаём текущий прогресс
                        IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReports.SENSORS_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                    }

                    // Файл протокола не может быть пустым, если датчиков в хранилище не обнаружилось
                    if (this.storage.sensors.EnumerateSensors().Count() == 0)
                        writer.WriteLine("Датчики не найдены.");
                }
            }
            catch
            {
                //Выставляем максимальный прогресс
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReports.SENSORS_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, this.storage.sensors.Count());

                return false;
            }

            return true;
        }
    }
}
