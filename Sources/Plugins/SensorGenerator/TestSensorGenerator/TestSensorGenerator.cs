﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Store;
using System.Text.RegularExpressions;
using System.Linq;

using TestUtils;

namespace TestSensorGenerator
{
    [TestClass]
    public class TestSensorGenerator
    { 
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        const string ReportFileName = "SensorGenerator.log";

        /// <summary>
        /// Проверка на корректное создание файла отчета SensorGenerator.log при пустом хранилище.
        /// </summary>
        [TestMethod]
        public void SensorGenerator_IsLogExistAndHasNoSensors()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) => { },                            // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    string reportPath = Path.Combine(reportsPath, ReportFileName);
                    string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\NoSensors", ReportFileName);

                    return TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath);
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка на корректное создание протокола после ручной вставки датчиков в Хранилище
        /// </summary>
        [TestMethod]
        public void SensorGenerator_HandMadeSensorsLog()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    string path = Path.Combine(storage.appliedSettings.FileListPath, "test.cs");

                    // Добавление обычной функции
                    IFunction function = storage.functions.AddFunction();
                    function.AddDefinition(path, 1, 1);
                    function.Name = "TestFunction";
                    function.AddSensor(1, Kind.START);
                    function.AddSensor(2, Kind.INTERNAL);
                    function.AddSensor(3, Kind.FINAL);

                    // Добавление анонимной функции
                    function = storage.functions.AddFunction();
                    function.AddDefinition(path, 41, 1);
                    function.AddSensor(4, Kind.START);
                    function.AddSensor(5, Kind.INTERNAL);
                    function.AddSensor(6, Kind.FINAL);

                    // Добавление внешней функции
                    function = storage.functions.AddFunction();
                    function.AddDefinition(path, 81, 1);
                    function.Name = path;
                    function.AddSensor(7, Kind.START);
                    function.AddSensor(8, Kind.INTERNAL);
                    function.AddSensor(9, Kind.FINAL);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) => { return true; },               // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    string buffer;
                    ulong i = 0;
                    string reportPath = Path.Combine(reportsPath, ReportFileName);
                    string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\HandMade", ReportFileName);
                    using (StreamReader reader = new StreamReader(reportPath))
                    {
                        while ((buffer = reader.ReadLine()) != null)
                        {
                            buffer = buffer.Replace("\n", string.Empty);

                            // Проверка, что строка составлена правильно
                            if (!Regex.IsMatch(buffer, @".+?\t.+?\t.+?\t.+?\t.+?\t.+?"))
                                return false;

                            // Проверяем столбцы отчета
                            Match match = Regex.Match(buffer, @"(.+?)\t(.+?)\t(.+?)\t(.+?)\t(.+?)\t(.+?)");
                            if (!CheckColumns(match.Groups))
                                return false;

                            i++;
                        }
                    }

                    // Проверка, что количество строк в отчете равно количеству датчиков, и сравнение с эталонным отчетом 
                    return (i == 9 && TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath));
                },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректного содержимого в отчете
        /// </summary>
        /// <param name="groupCollection">Коллекция найденных значений регулярным выражением.</param>
        /// <returns>true - корректно составленный отчет</returns>
        private bool CheckColumns(GroupCollection groupCollection)
        {
            // <Число столбцов = 6> == <количеству групп найденных значений> - 1
            if (groupCollection.Count - 1 != 6)
                return false;

            // Первый столбец - тип датчика
            switch (groupCollection[1].ToString())
            {
                case "s":
                case "i":
                case "f":
                    break;
                default:
                    return false;
            }

            // Второй столбец - номер датчика
            UInt64 sensorID = Convert.ToUInt64(groupCollection[2].ToString());
            if (sensorID == 0 || sensorID == Store.Const.CONSTS.WrongID)
                return false;

            // Третий столбец - существующая функция, вне функции или анонимная функция
            string functionName = groupCollection[3].ToString();
            if (functionName != @"TestFunction"
                && functionName != @"Вне функции"
                && functionName != @"<Анонимная функция>")
                return false;

            // Четвертый столбец - имя файла для отчета
            if (groupCollection[4].ToString() != @"test.cs")
                return false;

            // Пятый столбец - строка не может быть 0
            if (Convert.ToUInt64(groupCollection[5].ToString()) == Store.LocationConstants.wrongLine)
                return false;

            // Шестой столбец - столбец не может быть 0
            if (Convert.ToUInt64(groupCollection[6].ToString()) == Store.LocationConstants.wrongColumn)
                return false;

            return true;
        }

        /// <summary>
        /// Тест корректного создания отчета после выполнения парсера JavaScript
        /// </summary>
        [TestMethod]
        public void SensorGenerator_JavaScriptParser()
        {
            string sourcePrefixPart = @"Sources\JavaScript";
            testUtils.RunTest(
               sourcePrefixPart,                                               // sourcePrefixPart
               new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
               false,                                                          // isUseCachedStorage
               (storage, testMaterialsPath) => { },                            // prepareStorage
               (storage, testMaterialsPath) =>
               {
                   TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                   TestUtilsClass.Run_IdentifyFileTypes(storage);
                   TestUtilsClass.Run_JavaScriptParser(storage);
                   TestUtilsClass.Run_SensorTypeFixer(storage);
               },                                                              // customizeStorage
               (storage, testMaterialsPath) => { return true; },               // checkStorage
               (reportsPath, testMaterialsPath) =>
               {
                   string reportPath = Path.Combine(reportsPath, ReportFileName);
                   string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\JavaScript", ReportFileName);

                   return TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath);
               },                                                              // checkReportBeforeRerun
               false,                                                          // isUpdateReport
               (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
               (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест корректного создания отчета после выполнения парсера VB(S)
        /// </summary>
        [TestMethod, Ignore]
        public void SensorGenerator_VBParser()
        {
            string sourcePrefixPart = @"Sources\VB(S)\TestVB";
            testUtils.RunTest(
               sourcePrefixPart,                                               // sourcePrefixPart
               new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
               false,                                                          // isUseCachedStorage
               (storage, testMaterialsPath) => { },                            // prepareStorage
               (storage, testMaterialsPath) =>
               {
                   TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                   TestUtilsClass.Run_IdentifyFileTypes(storage);
                   TestUtilsClass.Run_VBParser(storage);
                   TestUtilsClass.Run_SensorTypeFixer(storage);
               },                                                              // customizeStorage
               (storage, testMaterialsPath) => { return true; },               // checkStorage
               (reportsPath, testMaterialsPath) =>
               {
                   string reportPath = Path.Combine(reportsPath, ReportFileName);
                   string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\VB(S)", ReportFileName);

                   return TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath);
               },                                                              // checkReportBeforeRerun
               false,                                                          // isUpdateReport
               (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
               (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест корректного создания отчета после выполнения парсера PHP
        /// </summary>
        [TestMethod]
        public void SensorGenerator_PhpParser()
        {
            string sourcePrefixPart = @"Sources\PHP";
            testUtils.RunTest(
               sourcePrefixPart,                                               // sourcePrefixPart
               new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
               false,                                                          // isUseCachedStorage
               (storage, testMaterialsPath) => { },                            // prepareStorage
               (storage, testMaterialsPath) =>
               {
                   TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                   TestUtilsClass.Run_IdentifyFileTypes(storage);
                   TestUtilsClass.Run_PhpParser(storage);
                   TestUtilsClass.Run_SensorTypeFixer(storage);
               },                                                              // customizeStorage
               (storage, testMaterialsPath) => { return true; },               // checkStorage
               (reportsPath, testMaterialsPath) =>
               {
                   string reportPath = Path.Combine(reportsPath, ReportFileName);
                   string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\PHP", ReportFileName);

                   return TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath);
               },                                                              // checkReportBeforeRerun
               false,                                                          // isUpdateReport
               (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
               (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест корректного создания отчета после выполнения плагина вставки датчиков в исходные тексты Pascal по второму уровню.
        /// </summary>
        [TestMethod]
        public void SensorGenerator_DelphiSensors()
        {
            string sourcePrefixPart = @"Sources\Pascal";
            const string understandReportsPrefixPart = @"Understand\Pascal";
            const string origSourcePath = @"B:\IA_b\Tests\Materials\Sources\Pascal"; 
            
            testUtils.RunTest(
               sourcePrefixPart,                                               // sourcePrefixPart
               new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
               false,                                                          // isUseCachedStorage
               (storage, testMaterialsPath) => { },                            // prepareStorage
               (storage, testMaterialsPath) =>
               {
                   TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                   TestUtilsClass.Run_IdentifyFileTypes(storage);
                   TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                   TestUtilsClass.Run_DelphiSensors(storage, origSourcePath);
               },                                                              // customizeStorage
               (storage, testMaterialsPath) => { return true; },               // checkStorage
               (reportsPath, testMaterialsPath) =>
               {
                   string reportPath = Path.Combine(reportsPath, ReportFileName);
                   string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\Pascal", ReportFileName);

                   return TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath);
               },                                                              // checkReportBeforeRerun
               false,                                                          // isUpdateReport
               (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
               (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест корректного создания отчета после выполнения плагина - Вставка датчиков в исходники С C++ C# Delphi
        /// </summary>
        [TestMethod]
        public void SensorGenerator_CS_Sensors()
        {
            string sourcePrefixPart = @"Sources\Pascal";
            const string understandReportsPrefixPart = @"Understand\Pascal";
            const string origSourcePath = @"B:\IA_b\Tests\Materials\Sources\Pascal"; testUtils.RunTest(
               sourcePrefixPart,                                               // sourcePrefixPart
               new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
               false,                                                          // isUseCachedStorage
               (storage, testMaterialsPath) => { },                            // prepareStorage
               (storage, testMaterialsPath) =>
               {
                   TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                   TestUtilsClass.Run_IdentifyFileTypes(storage);
                   TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                   TestUtilsClass.Run_CS_Sensors(storage);
               },                                                              // customizeStorage
               (storage, testMaterialsPath) => { return true; },               // checkStorage
               (reportsPath, testMaterialsPath) =>
               {
                   string reportPath = Path.Combine(reportsPath, ReportFileName);
                   string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\CS_Sensors", ReportFileName);

                   return TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath);
               },                                                              // checkReportBeforeRerun
               false,                                                          // isUpdateReport
               (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
               (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест корректного создания отчета после выполнения плагина - Парсер C# (3 уровень)
        /// </summary>
        [TestMethod]
        public void SensorGenerator_CSharpParserNDV3()
        {
            string sourcePrefixPart = @"Sources\CSharp\TestStatements\";
            testUtils.RunTest(
               sourcePrefixPart,                                               // sourcePrefixPart
               new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
               false,                                                          // isUseCachedStorage
               (storage, testMaterialsPath) => { },                            // prepareStorage
               (storage, testMaterialsPath) =>
               {
                   TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                   TestUtilsClass.Run_IdentifyFileTypes(storage);
                   TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestStatements\"), level2: false);
                   TestUtilsClass.Run_PointsToAnalysesSimple(storage);
                   TestUtilsClass.Run_SensorTypeFixer(storage);
               },                                                              // customizeStorage
               (storage, testMaterialsPath) => { return true; },               // checkStorage
               (reportsPath, testMaterialsPath) =>
               {
                   string reportPath = Path.Combine(reportsPath, ReportFileName);
                   string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\C#\NDV3", ReportFileName);

                   return TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath);
               },                                                              // checkReportBeforeRerun
               false,                                                          // isUpdateReport
               (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
               (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест корректного создания отчета после выполнения плагина - Парсер C# (3 уровень)
        /// </summary>
        [TestMethod]
        public void SensorGenerator_CSharpParserNDV3_StackOverflowBug()
        {
            string sourcePrefixPart = @"Sources\CSharp\TestStatements\ContinueStatement\";
            testUtils.RunTest(
               sourcePrefixPart,                                               // sourcePrefixPart
               new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
               false,                                                          // isUseCachedStorage
               (storage, testMaterialsPath) => { },                            // prepareStorage
               (storage, testMaterialsPath) =>
               {
                   TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                   TestUtilsClass.Run_IdentifyFileTypes(storage);
                   TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestStatements\ContinueStatement\"), level2: false);
                   TestUtilsClass.Run_PointsToAnalysesSimple(storage);
               },                                                              // customizeStorage
               (storage, testMaterialsPath) => { return true; },               // checkStorage
               (reportsPath, testMaterialsPath) =>
               {
                   //string reportPath = Path.Combine(reportsPath, ReportFileName);
                   //string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\C#\NDV3", ReportFileName);

                   //return TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath);
                   return true;
               },                                                              // checkReportBeforeRerun
               false,                                                          // isUpdateReport
               (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
               (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест корректного создания отчета после выполнения плагина - Парсер C# (2 уровень)
        /// </summary>
        [TestMethod]
        public void SensorGenerator_CSharpParserNDV2()
        {
            string sourcePrefixPart = @"Sources\CSharp\TestStatements\";
            testUtils.RunTest(
               sourcePrefixPart,                                               // sourcePrefixPart
               new IA.Plugins.Analyses.SensorGenerator.SensorGenerator(),      // Plugin
               false,                                                          // isUseCachedStorage
               (storage, testMaterialsPath) => { },                            // prepareStorage
               (storage, testMaterialsPath) =>
               {
                   TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                   TestUtilsClass.Run_IdentifyFileTypes(storage);
                   TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\TestStatements\"), level2: true);
                   TestUtilsClass.Run_PointsToAnalysesSimple(storage);
                   TestUtilsClass.Run_SensorTypeFixer(storage);
               },                                                              // customizeStorage
               (storage, testMaterialsPath) => { return true; },               // checkStorage
               (reportsPath, testMaterialsPath) =>
               {
                   string reportPath = Path.Combine(reportsPath, ReportFileName);
                   string mainRep = Path.Combine(testMaterialsPath, @"SensorsGenerator\C#\NDV2", ReportFileName);

                   return TestUtilsClass.Reports_TXT_Compare(reportPath, mainRep, true, false, testMaterialsPath);
               },                                                              // checkReportBeforeRerun
               false,                                                          // isUpdateReport
               (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
               (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
               );
        }
    }
}
