﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

using Store;


namespace IA.Plugins.Analyses.DynamicReports
{
    /// <summary>
    /// Элемент кеша файлов
    /// </summary>
    public class CacheElement
    {
        /// <summary>
        /// Время загрузки файла
        /// </summary>
        public ulong LT;
        
        /// <summary>
        /// Суммарное время загрузок файла
        /// </summary>
        ulong TT;
        /// <summary>
        /// Текст файла
        /// </summary>
        public string FileBody = string.Empty;
        /// <summary>
        /// Список позиций начал строк файла
        /// </summary>
        public List<ulong> LinePositions = new List<ulong>();

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="filename">имя файла</param>
        public CacheElement(string filename)
        {
            LT = LoadFile(filename, true);
            TT = 0;
        }
        /// <summary>
        /// Модификация суммарного времени загрузки
        /// </summary>
        /// <param name="plus">true - увеличение, false - уменьшение</param>
        public void Update(bool plus)
        {
            TT = (plus) ? TT + LT : (TT > 20) ? TT - 20 : 0;

        }
        /// <summary>
        /// Весовая функция для элемента кэша
        /// </summary>
        /// <returns></returns>
        public ulong Weight()
        {
            return TT;
        }
        /// <summary>
        /// Загрузка файла
        /// </summary>
        /// <param name="filename">имя файла</param>
        /// <param name="compressed">true - пропускать пустые строки</param>
        /// <returns></returns>
        ulong LoadFile(string filename, bool compressed)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();


            DateTime dt = DateTime.Now;
            StreamReader sr = new StreamReader(filename, Encoding.Default);
            string s;
            FileBody = string.Empty;
            LinePositions.Add((ulong)FileBody.Length);
            while ((s = sr.ReadLine()) != null)
            {
                if (compressed && s.Trim() == "")
                {
                }
                else
                    FileBody += s + '\n';
                LinePositions.Add((ulong)FileBody.Length);
            }
            DateTime d1 = DateTime.Now;
            sw.Stop();
            long ticks = sw.ElapsedTicks;

            return (ulong)ticks;
        }
    }
    /// <summary>
    /// Класс кэша файлов
    /// </summary>
    public class CacheFiles
    {
        /// <summary>
        /// Начальное значение размера кэша
        /// </summary>
        const int dim = 100;
        
        int _dimension;
        /// <summary>
        /// Размер кэша (максимальное количество файлов, хранимых в памяти)
        /// </summary>
        public int Dimension
        {
            set
            {
                if (value > 0)
                    _dimension = value;
                else
                    _dimension = dim;
            }
            get
            {
                return _dimension;
            }
        }

        /// <summary>
        /// Кэш
        /// </summary>
        public Dictionary<string, CacheElement> array = new Dictionary<string, CacheElement>();

        /// <summary>
        /// Конструктор
        /// </summary>
        public CacheFiles()
        {
            Dimension = dim;
        }

        /// <summary>
        /// Получение элемента кэша по имени файла
        /// </summary>
        /// <param name="filename">имя файла</param>
        /// <returns>элемент кэша</returns>
        public CacheElement GetElement(string filename)
        {
            CacheElement ret;
            if (array.TryGetValue(filename, out ret))
            {
                foreach (KeyValuePair<string, CacheElement> pair in array)
                    pair.Value.Update(ret == pair.Value);
                return ret;
            }
            else
            {
                while (true)
                {
                    if (array.Count >= _dimension)
                    {
                        string toRemove = array.OrderBy(x => x.Value.Weight()).Select(x => x.Key).FirstOrDefault();
                        if (toRemove != string.Empty)
                            array.Remove(toRemove);
                    }
                    else
                        break;
                }
                ret = new CacheElement(filename);
                array.Add(filename, ret);
                foreach (KeyValuePair<string, CacheElement> pair in array)
                    pair.Value.Update(ret == pair.Value);
                return ret;
            }
        }

    }

    /// <summary>
    /// Вспомогательный класс для анализа датчиков
    /// </summary>
    internal class ViewSensors
    {
        /// <summary>
        /// Битовый массив покрытия
        /// </summary>
        public BitArray bitArray;

        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage; 

        /// <summary>
        /// Кэш файлов
        /// </summary>
        public CacheFiles CF;

        /// <summary>
        /// Максимальная позиция по оси Х графика покрытия
        /// </summary>
        int nMaxPos;

        /// <summary>
        /// Количество датчиков в одной позиции оси Х
        /// </summary>
        int step;

        /// <summary>
        /// Количество точек по оси X для одного датчика
        /// </summary>
        int massshtab=1; 

        /// <summary>
        /// Преобразование номера датчика в позицию панели на графике покрытия
        /// </summary>
        public Dictionary<ulong, int> SensorToArray;

        /// <summary>
        /// Преобразование позиции панели на графике покрытия в номер датчика
        /// </summary>
        public Dictionary<int, ulong> ArrayToSensor;

        public Dictionary<ulong, ulong> SensorsPositions;

        /// <summary>
        /// Коструктор
        /// </summary>
        /// <param name="storage">хранилище</param>
        public ViewSensors(Storage storage)
        {
            this.storage = storage;
            CF = new CacheFiles();
            SensorToArray = new Dictionary<ulong, int>();
            ArrayToSensor = new Dictionary<int, ulong>();
            SensorsPositions = new Dictionary<ulong, ulong>();
        }

        /// <summary>
        /// Заполнение таблиц соответствия bitArray и датчиков
        /// </summary>
        public void UseSensors()
        {
            SensorToArray = new Dictionary<ulong, int>();
            ArrayToSensor = new Dictionary<int, ulong>();
            SensorsPositions.Clear();
            int index = 0;
            foreach (Sensor sensor in storage.sensors.EnumerateSensors())
            {
                SensorToArray.Add(sensor.ID, index);
                ArrayToSensor.Add(index, sensor.ID);
                index++;
                SensorsPositions.Add(sensor.ID, GetPosition(sensor));
            }

        }

        ulong GetPosition(Sensor sensor)
        {
            IStatement statement = sensor.GetBeforeStatement();
            IFunction function = this.storage.functions.GetFunction(sensor.GetFunctionID());
            string filename = function.Definition().GetFile().FullFileName_Original;
            ulong row=0;
            ulong col=0;
            if (statement != null)
            {
                row = statement.FirstSymbolLocation.GetLine();
                col = statement.FirstSymbolLocation.GetColumn();
            }
            else
            {
                if (function != null)
                {
                    Location functionDefinitions = function.Definition();
                    if (functionDefinitions != null)
                    {
                        row = functionDefinitions.GetLine();
                        col = functionDefinitions.GetColumn();
                    }
                }
            }
            CacheElement el = CF.GetElement(filename);
            if (el.LinePositions.Count > (int)row && (int)row >= 0)
                return el.LinePositions[(int)row];
            else
                return 0;
        }

        /// <summary>
        /// Загрузка битового массива
        /// </summary>
        /// <param name="filePath">имя файла битового массива</param>
        internal void LoadBitArray(string filePath)
        {
            if (File.Exists(filePath)) //Если файл уже есть
                bitArray = new BitArray(File.ReadAllBytes(filePath));
            else //Создаем минимальный
                bitArray = new BitArray(1, false);

            //общее число датчиков в хранилище
            bitArray.Length = (int)storage.sensors.Count();
        }

        /// <summary>
        /// Конвертация битового массива в байтовый
        /// </summary>
        /// <param name="bits">битовый массив</param>
        /// <returns>байтовый массив</returns>
        byte[] FromBits(BitArray bits)
        {
            byte[] ret = new byte[bits.Length / 8 + ((bits.Length % 8 > 0) ? 1 : 0)];
            bits.CopyTo(ret, 0);
            return ret;
        }

        /// <summary>
        /// Конвертация байтового массива в битовый
        /// </summary>
        /// <param name="bytes">байтовый массив</param>
        /// <returns>битовый массив</returns>
        BitArray FromBytes(byte[] bytes)
        {
            return new BitArray(bytes);
        }

        /// <summary>
        /// отображение на панели графика покрытия датчиков
        /// </summary>
        /// <param name="panel1">панель</param>
        /// <param name="factor">множитель</param>
        /// <param name="offset">смещение</param>
        public void DrawPanel(Panel panel1, int factor, int offset)
        {
            int slength = bitArray.Length;
            int pwidth = panel1.Width;
            int pheight = panel1.Height;

            if (slength == 0)
                return;

            step = slength / (pwidth * factor) + 1;

            if (step == 1)
                massshtab = pwidth * factor / slength;
            if (massshtab == 0)
                massshtab = 1;

            int[] arrB = new int[pwidth];

            int size = pwidth / factor;
            int startpos = (pwidth - size) * offset / (massshtab * 100);

            int startsensorpos = step * startpos;

            nMaxPos = startsensorpos + pwidth * step + 1;

            for (int i = 0; i < pwidth; i++)
            {
                for (int j = 0; j < step; j++)
                {
                    if (((i + startsensorpos) * step + j < slength) && bitArray[(i + startsensorpos) * step + j])
                        arrB[i]++;
                }
            }

            Graphics dc = panel1.CreateGraphics();
            dc.FillRectangle(new SolidBrush(SystemColors.Control), 0, 0, panel1.Width, panel1.Height);
            Pen BluePen = new Pen(Color.Blue, 1);
            Brush BlackBrush = Brushes.Black;
            dc.FillRectangle(BlackBrush, 0, 0, panel1.Size.Width, panel1.Height * 6 / 7 + 1);
            Brush GrayBrush = Brushes.Gray;
            SolidBrush ControlBrush = new SolidBrush(SystemColors.Control);
            dc.FillRectangle(ControlBrush, nMaxPos * massshtab * factor, 0, panel1.Size.Width, panel1.Height);
            dc.FillRectangle(GrayBrush, 0, panel1.Height * 6 / 7 + 1, nMaxPos * massshtab * factor - 1, panel1.Height);
            if (arrB != null)
            {

                for (int i = 0; i * massshtab < pwidth; i++)
                {
                    dc.DrawLine(BluePen, i * massshtab, pheight * 6 / 7, i * massshtab, pheight * 6 / 7 - arrB[i] * pheight * 6 / 7 / step);
                    if (massshtab > 1)
                    {
                        for (int j = 1; j < massshtab; j++)
                        {
                            Pen BluePen1 = new Pen(Color.FromArgb(0, 0, 255 - j * 127 / (massshtab)), 1);
                            dc.DrawLine(BluePen1, i * massshtab + j, pheight * 6 / 7, i * massshtab + j, pheight * 6 / 7 - arrB[i] * pheight * 6 / 7 / step);
                        }
                    }
                }
            }

            //BlackBrush.Dispose();
            //GrayBrush.Dispose();
            //ControlBrush.Dispose();
            //BluePen.Dispose();
            //dc.Dispose();

        }
        /// <summary>
        /// Отображение на панели указателя (треугольника) в соответвующей позиции
        /// </summary>
        /// <param name="panel1">панель</param>
        /// <param name="pos">позиция</param>
        /// <param name="factor">множитель</param>
        /// <param name="offset">смещение</param>
        public void PaintTriangle(Panel panel1, int pos, int factor, int offset)
        {

            int nX = pos;
            Graphics dc = panel1.CreateGraphics();
            Brush GrayBrush = Brushes.Gray;
            dc.FillRectangle(GrayBrush, 0, panel1.Height * 6 / 7 + 1, nMaxPos * massshtab - 1, panel1.Height);
            SolidBrush ControlBrush = new SolidBrush(SystemColors.Control);
            dc.FillRectangle(ControlBrush, nMaxPos * massshtab, 0, panel1.Size.Width, panel1.Height);

            Pen RedPen = new Pen(Color.Red, 1);
            Point[] poligone = new Point[3];
            poligone[0] = new Point(nX, panel1.Height * 6 / 7 + 1);
            poligone[1] = new Point(nX + 3, panel1.Height * 6 / 7 + 10);
            poligone[2] = new Point(nX - 3, panel1.Height * 6 / 7 + 10);
            Brush WhiteBrush = Brushes.White;
            dc.FillPolygon(WhiteBrush, poligone);
            dc.DrawPolygon(RedPen, poligone);

            //GrayBrush.Dispose();
            //ControlBrush.Dispose();
            //WhiteBrush.Dispose();
            //RedPen.Dispose();
            //dc.Dispose();
        }
        public void PaintTriangle1(Panel panel1, int pos, int factor, int offset)
        {

            int slength = bitArray.Length;
            int pwidth = panel1.Width;
            int pheight = panel1.Height;

            if (slength == 0)
                return;

            int step = slength / (pwidth * factor) + 1;

            int massshtab = 1;

            if (step == 1)
                massshtab = pwidth * factor / slength;
            if (massshtab == 0)
                massshtab = 1;


            int size = pwidth / factor;
            int startpos = (pwidth - size) * offset / (massshtab * 100);

            int startsensorpos = step * startpos;



            int nX = (pos - startpos) / step;
            Graphics dc = panel1.CreateGraphics();
            Brush GrayBrush = Brushes.Gray;
            dc.FillRectangle(GrayBrush, 0, panel1.Height * 6 / 7 + 1, nMaxPos * massshtab - 1, panel1.Height);
            SolidBrush ControlBrush = new SolidBrush(SystemColors.Control);
            dc.FillRectangle(ControlBrush, nMaxPos * massshtab, 0, panel1.Size.Width, panel1.Height);

            Pen RedPen = new Pen(Color.Red, 1);
            Point[] poligone = new Point[3];
            poligone[0] = new Point(nX, panel1.Height * 6 / 7 + 1);
            poligone[1] = new Point(nX + 3, panel1.Height * 6 / 7 + 10);
            poligone[2] = new Point(nX - 3, panel1.Height * 6 / 7 + 10);
            Brush WhiteBrush = Brushes.White;
            dc.FillPolygon(WhiteBrush, poligone);
            dc.DrawPolygon(RedPen, poligone);

            //GrayBrush.Dispose();
            //ControlBrush.Dispose();
            //WhiteBrush.Dispose();
            //RedPen.Dispose();
            //dc.Dispose();
        }

        public void DrawPanel1(Panel panel1, int factor, int offset)
        {
            Graphics dc = panel1.CreateGraphics();
            dc.FillRectangle(new SolidBrush(SystemColors.Control), 0, 0, panel1.Width, panel1.Height);
            Brush LightBlue = Brushes.LightBlue;
            dc.FillRectangle(LightBlue, 0, panel1.Height / 3, panel1.Width, panel1.Height / 3);
            Pen BluePen = new Pen(Color.Blue, 1);
            int size = panel1.Width / factor;
            int startpos = (panel1.Width - size) * offset / 100;
            dc.DrawRectangle(BluePen, startpos, 2, size-1, panel1.Height - 4);

            //LightBlue.Dispose();
            //BluePen.Dispose();
            //dc.Dispose();
        }
    }
}
