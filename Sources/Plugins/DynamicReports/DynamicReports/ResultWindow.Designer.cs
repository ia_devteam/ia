﻿namespace IA.Plugins.Analyses.DynamicReports
{
    partial class ResultWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.graphics_panel = new System.Windows.Forms.Panel();
            this.resultWindow_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.covered_groupBox = new System.Windows.Forms.GroupBox();
            this.covered_listBox = new System.Windows.Forms.ListBox();
            this.uncovered_groupBox = new System.Windows.Forms.GroupBox();
            this.uncovered_listBox = new System.Windows.Forms.ListBox();
            this.source_groupBox = new System.Windows.Forms.GroupBox();
            this.source_richTextBox = new System.Windows.Forms.RichTextBox();
            this.coveredIndex_label = new System.Windows.Forms.Label();
            this.uncoveredIndex_label = new System.Windows.Forms.Label();
            this.currentSensor_label = new System.Windows.Forms.Label();
            this.labelFactor = new System.Windows.Forms.Label();
            this.trackBarFactor = new System.Windows.Forms.TrackBar();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelOffset = new System.Windows.Forms.Label();
            this.trackBarOffset = new System.Windows.Forms.TrackBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.resultWindow_tableLayoutPanel.SuspendLayout();
            this.covered_groupBox.SuspendLayout();
            this.uncovered_groupBox.SuspendLayout();
            this.source_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFactor)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarOffset)).BeginInit();
            this.SuspendLayout();
            // 
            // graphics_panel
            // 
            this.resultWindow_tableLayoutPanel.SetColumnSpan(this.graphics_panel, 3);
            this.graphics_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphics_panel.Location = new System.Drawing.Point(3, 303);
            this.graphics_panel.Name = "graphics_panel";
            this.graphics_panel.Size = new System.Drawing.Size(1029, 264);
            this.graphics_panel.TabIndex = 0;
            this.graphics_panel.Click += new System.EventHandler(this.graphics_panel_Click);
            this.graphics_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.graphics_panel_Paint);
            // 
            // resultWindow_tableLayoutPanel
            // 
            this.resultWindow_tableLayoutPanel.ColumnCount = 3;
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.resultWindow_tableLayoutPanel.Controls.Add(this.graphics_panel, 0, 2);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.covered_groupBox, 0, 0);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.uncovered_groupBox, 1, 0);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.source_groupBox, 2, 0);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.coveredIndex_label, 0, 1);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.uncoveredIndex_label, 1, 1);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.currentSensor_label, 2, 1);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.labelFactor, 0, 3);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.trackBarFactor, 1, 3);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 2, 3);
            this.resultWindow_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultWindow_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.resultWindow_tableLayoutPanel.Name = "resultWindow_tableLayoutPanel";
            this.resultWindow_tableLayoutPanel.RowCount = 4;
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.resultWindow_tableLayoutPanel.Size = new System.Drawing.Size(1035, 601);
            this.resultWindow_tableLayoutPanel.TabIndex = 1;
            // 
            // covered_groupBox
            // 
            this.covered_groupBox.Controls.Add(this.covered_listBox);
            this.covered_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.covered_groupBox.Location = new System.Drawing.Point(3, 3);
            this.covered_groupBox.Name = "covered_groupBox";
            this.covered_groupBox.Size = new System.Drawing.Size(144, 264);
            this.covered_groupBox.TabIndex = 0;
            this.covered_groupBox.TabStop = false;
            this.covered_groupBox.Text = "Покрытые";
            // 
            // covered_listBox
            // 
            this.covered_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.covered_listBox.ForeColor = System.Drawing.Color.Blue;
            this.covered_listBox.FormattingEnabled = true;
            this.covered_listBox.Location = new System.Drawing.Point(3, 16);
            this.covered_listBox.Name = "covered_listBox";
            this.covered_listBox.Size = new System.Drawing.Size(138, 245);
            this.covered_listBox.TabIndex = 0;
            this.covered_listBox.SelectedIndexChanged += new System.EventHandler(this.covered_listBox_SelectedIndexChanged);
            // 
            // uncovered_groupBox
            // 
            this.uncovered_groupBox.Controls.Add(this.uncovered_listBox);
            this.uncovered_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uncovered_groupBox.Location = new System.Drawing.Point(153, 3);
            this.uncovered_groupBox.Name = "uncovered_groupBox";
            this.uncovered_groupBox.Size = new System.Drawing.Size(144, 264);
            this.uncovered_groupBox.TabIndex = 1;
            this.uncovered_groupBox.TabStop = false;
            this.uncovered_groupBox.Text = "Непокрытые";
            // 
            // uncovered_listBox
            // 
            this.uncovered_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uncovered_listBox.ForeColor = System.Drawing.Color.Red;
            this.uncovered_listBox.FormattingEnabled = true;
            this.uncovered_listBox.Location = new System.Drawing.Point(3, 16);
            this.uncovered_listBox.Name = "uncovered_listBox";
            this.uncovered_listBox.Size = new System.Drawing.Size(138, 245);
            this.uncovered_listBox.TabIndex = 0;
            this.uncovered_listBox.SelectedIndexChanged += new System.EventHandler(this.uncovered_listBox_SelectedIndexChanged);
            // 
            // source_groupBox
            // 
            this.source_groupBox.Controls.Add(this.source_richTextBox);
            this.source_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.source_groupBox.Location = new System.Drawing.Point(303, 3);
            this.source_groupBox.Name = "source_groupBox";
            this.source_groupBox.Size = new System.Drawing.Size(729, 264);
            this.source_groupBox.TabIndex = 2;
            this.source_groupBox.TabStop = false;
            this.source_groupBox.Text = "Исходный код";
            // 
            // source_richTextBox
            // 
            this.source_richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.source_richTextBox.Location = new System.Drawing.Point(3, 16);
            this.source_richTextBox.Name = "source_richTextBox";
            this.source_richTextBox.Size = new System.Drawing.Size(723, 245);
            this.source_richTextBox.TabIndex = 0;
            this.source_richTextBox.Text = "";
            // 
            // coveredIndex_label
            // 
            this.coveredIndex_label.AutoSize = true;
            this.coveredIndex_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.coveredIndex_label.ForeColor = System.Drawing.Color.Blue;
            this.coveredIndex_label.Location = new System.Drawing.Point(3, 270);
            this.coveredIndex_label.Name = "coveredIndex_label";
            this.coveredIndex_label.Size = new System.Drawing.Size(144, 30);
            this.coveredIndex_label.TabIndex = 3;
            this.coveredIndex_label.Text = ".";
            this.coveredIndex_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uncoveredIndex_label
            // 
            this.uncoveredIndex_label.AutoSize = true;
            this.uncoveredIndex_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uncoveredIndex_label.ForeColor = System.Drawing.Color.Red;
            this.uncoveredIndex_label.Location = new System.Drawing.Point(153, 270);
            this.uncoveredIndex_label.Name = "uncoveredIndex_label";
            this.uncoveredIndex_label.Size = new System.Drawing.Size(144, 30);
            this.uncoveredIndex_label.TabIndex = 4;
            this.uncoveredIndex_label.Text = ".";
            this.uncoveredIndex_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // currentSensor_label
            // 
            this.currentSensor_label.AutoSize = true;
            this.currentSensor_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentSensor_label.Location = new System.Drawing.Point(303, 270);
            this.currentSensor_label.Name = "currentSensor_label";
            this.currentSensor_label.Size = new System.Drawing.Size(729, 30);
            this.currentSensor_label.TabIndex = 7;
            this.currentSensor_label.Text = ".";
            this.currentSensor_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelFactor
            // 
            this.labelFactor.AutoSize = true;
            this.labelFactor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelFactor.Location = new System.Drawing.Point(3, 570);
            this.labelFactor.Name = "labelFactor";
            this.labelFactor.Size = new System.Drawing.Size(144, 31);
            this.labelFactor.TabIndex = 8;
            this.labelFactor.Text = "Масштаб 1:1";
            this.labelFactor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackBarFactor
            // 
            this.trackBarFactor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarFactor.Location = new System.Drawing.Point(153, 573);
            this.trackBarFactor.Name = "trackBarFactor";
            this.trackBarFactor.Size = new System.Drawing.Size(144, 25);
            this.trackBarFactor.TabIndex = 9;
            this.trackBarFactor.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.labelOffset, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.trackBarOffset, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(300, 570);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(735, 31);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // labelOffset
            // 
            this.labelOffset.AutoSize = true;
            this.labelOffset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelOffset.Location = new System.Drawing.Point(3, 0);
            this.labelOffset.Name = "labelOffset";
            this.labelOffset.Size = new System.Drawing.Size(114, 31);
            this.labelOffset.TabIndex = 0;
            this.labelOffset.Text = "Позиция";
            this.labelOffset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackBarOffset
            // 
            this.trackBarOffset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarOffset.Location = new System.Drawing.Point(273, 3);
            this.trackBarOffset.Name = "trackBarOffset";
            this.trackBarOffset.Size = new System.Drawing.Size(459, 25);
            this.trackBarOffset.TabIndex = 1;
            this.trackBarOffset.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(123, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(144, 25);
            this.panel1.TabIndex = 2;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // ResultWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resultWindow_tableLayoutPanel);
            this.Name = "ResultWindow";
            this.Size = new System.Drawing.Size(1035, 601);
            this.resultWindow_tableLayoutPanel.ResumeLayout(false);
            this.resultWindow_tableLayoutPanel.PerformLayout();
            this.covered_groupBox.ResumeLayout(false);
            this.uncovered_groupBox.ResumeLayout(false);
            this.source_groupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFactor)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarOffset)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel graphics_panel;
        private System.Windows.Forms.TableLayoutPanel resultWindow_tableLayoutPanel;
        private System.Windows.Forms.GroupBox covered_groupBox;
        private System.Windows.Forms.ListBox covered_listBox;
        private System.Windows.Forms.GroupBox uncovered_groupBox;
        private System.Windows.Forms.ListBox uncovered_listBox;
        private System.Windows.Forms.GroupBox source_groupBox;
        private System.Windows.Forms.RichTextBox source_richTextBox;
        private System.Windows.Forms.Label coveredIndex_label;
        private System.Windows.Forms.Label uncoveredIndex_label;
        private System.Windows.Forms.Label currentSensor_label;
        private System.Windows.Forms.Label labelFactor;
        private System.Windows.Forms.TrackBar trackBarFactor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelOffset;
        private System.Windows.Forms.TrackBar trackBarOffset;
        private System.Windows.Forms.Panel panel1;
    }
}
