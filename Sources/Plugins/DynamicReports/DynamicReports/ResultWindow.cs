﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using Store;

namespace IA.Plugins.Analyses.DynamicReports
{


    /// <summary>
    /// Окно результатов исполнения плагина
    /// </summary>
    internal partial class ResultWindow : UserControl
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage">Хранилище</param>
        internal ResultWindow(Storage storage)
        {
            this.storage = storage;
            InitializeComponent();

            //this.panel1 = new BufferedPanel();
            trackBarFactor.Minimum = 1;
            trackBarFactor.Maximum = 10;
            trackBarOffset.Minimum = 0;
            trackBarOffset.Maximum = 100;



            int index=0; 
            foreach (bool bit in PluginSettings.viewSensors.bitArray)
            {
                if (bit)
                    covered_listBox.Items.Add(PluginSettings.viewSensors.ArrayToSensor[index]);
                else
                    uncovered_listBox.Items.Add(PluginSettings.viewSensors.ArrayToSensor[index]);
                index++;
            }
            PluginSettings.viewSensors.DrawPanel(graphics_panel, trackBarFactor.Value, trackBarOffset.Value);
        }

        /// <summary>
        /// Событие, связанное с прорисовкой панели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void graphics_panel_Paint(object sender, PaintEventArgs e)
        {
            PluginSettings.viewSensors.DrawPanel(graphics_panel, trackBarFactor.Value, trackBarOffset.Value);
        }

        /// <summary>
        /// Выделение датчика из списка покрытых
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void covered_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = covered_listBox.SelectedIndex;
            ulong sensor;
            if (index != -1)
                if (ulong.TryParse(covered_listBox.Text, out sensor))
                {
                    coveredIndex_label.Text = "Индекс: " + index;
                    try
                    {
                        IFunction func = storage.sensors.GetSensor(sensor).GetFunction(); //Функция, в которую вставлен датчик
                        Location funcDefinitions = func.Definition(); //не должен быть пустым, так как в функцию вставлен датчик
                        string funcname = func.Name;
                        string filename = funcDefinitions.GetFile().FileNameForReports; 
                        string fullfilename = funcDefinitions.GetFile().FullFileName_Original;
                        source_groupBox.Text = "Исходный код: " + filename;
                        source_richTextBox.ForeColor = covered_listBox.ForeColor;
                        source_richTextBox.Text = PluginSettings.viewSensors.CF.GetElement(fullfilename).FileBody;
                        source_richTextBox.SelectionStart = (int)PluginSettings.viewSensors.SensorsPositions[sensor];
                        source_richTextBox.ScrollToCaret();
                    }
                    catch (Exception ex)
                    {
                        Monitor.Log.Warning(PluginSettings.Name, "Ошибка при определении параметров датчика в хранилище: " + ex.Message);
                        source_groupBox.Text = "Исходный код";
                        source_richTextBox.Text = "";
                    }

                    currentSensor_label.Text = "Текущий датчик: " + sensor;
                    PluginSettings.viewSensors.PaintTriangle1(graphics_panel, PluginSettings.viewSensors.SensorToArray[sensor], trackBarFactor.Value, trackBarOffset.Value);
                }
        }

        /// <summary>
        /// Выделение датчика из списка непокрытых
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uncovered_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = uncovered_listBox.SelectedIndex;
            ulong sensor;
            if (index != -1)
                if (ulong.TryParse(uncovered_listBox.Text, out sensor))
                {
                    uncoveredIndex_label.Text = "Индекс: " + index;
                    try
                    {
                        IFunction func = storage.sensors.GetSensor(sensor).GetFunction(); //Функция, в которую вставлен датчик
                        Location funcDefinitions = func.Definition(); //не должен быть пустым, так как в функцию вставлен датчик
                        string funcname = func.Name;
                        string filename = funcDefinitions.GetFile().FileNameForReports;
                        string fullfilename = funcDefinitions.GetFile().FullFileName_Original;
                        source_groupBox.Text = "Исходный код: " + filename;
                        source_richTextBox.ForeColor = uncovered_listBox.ForeColor;
                        source_richTextBox.Text = PluginSettings.viewSensors.CF.GetElement(fullfilename).FileBody;
                        source_richTextBox.SelectionStart = (int)PluginSettings.viewSensors.SensorsPositions[sensor];
                        source_richTextBox.ScrollToCaret();
                    }
                    catch(Exception ex)
                    {
                        Monitor.Log.Warning(PluginSettings.Name, "Ошибка при определении параметров датчика в хранилище: " + ex.Message);
                        source_groupBox.Text = "Исходный код";
                        source_richTextBox.Text = "";
                    }

                    currentSensor_label.Text = "Текущий датчик: " + sensor;
                    PluginSettings.viewSensors.PaintTriangle1(graphics_panel, PluginSettings.viewSensors.SensorToArray[sensor], trackBarFactor.Value, trackBarOffset.Value); 
                }
        }

        /// <summary>
        /// Отработка собития щелчка мышью на панели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void graphics_panel_Click(object sender, EventArgs e)
        {
            Point pScreen = new Point(MousePosition.X, MousePosition.Y), pClient;
            pClient = graphics_panel.PointToClient(pScreen);
            Int32 CurPosition = pClient.X;

            if (CurPosition < 0 || CurPosition > graphics_panel.Width)
                return;

            int factor = trackBarFactor.Value;

            int slength = PluginSettings.viewSensors.bitArray.Length;
            int pwidth = graphics_panel.Width;
            int pheight = graphics_panel.Height;

            if (slength == 0)
                return;

            int step = slength / (pwidth * factor) + 1;

            int massshtab = 1;

            if (step == 1)
                massshtab = pwidth * factor / slength;
            if (massshtab == 0)
                massshtab = 1;


            int size = pwidth / factor;
            int startpos = (pwidth - size) * trackBarOffset.Value / (massshtab * 100);

            int startsensorpos = step * startpos * factor;



            if (((CurPosition) * step / (massshtab) + startsensorpos) >= (PluginSettings.viewSensors.bitArray.Length))
                currentSensor_label.Text = "Текущий датчик: " + PluginSettings.viewSensors.ArrayToSensor.Values.Max().ToString();
            else
                currentSensor_label.Text = "Текущий датчик: " + PluginSettings.viewSensors.ArrayToSensor[(CurPosition) * step / (massshtab) + startsensorpos].ToString();
            ulong sensor;
            if (ulong.TryParse(currentSensor_label.Text.Replace("Текущий датчик: ", ""), out sensor))
            {
                try
                {
                    IFunction func = storage.sensors.GetSensor(sensor).GetFunction();
                    Location funcDefenition = func.Definition();
                    string funcname = func.Name;
                    string filename = funcDefenition.GetFile().FileNameForReports;
                    string fullfilename = funcDefenition.GetFile().FullFileName_Original;
                    source_groupBox.Text = "Исходный код: " + filename;
                    if(covered_listBox.Items.Contains(sensor))
                        source_richTextBox.ForeColor = covered_listBox.ForeColor;
                    else
                        source_richTextBox.ForeColor = uncovered_listBox.ForeColor;
                    //source_richTextBox.ForeColor = Color.Black;
                    source_richTextBox.Text = PluginSettings.viewSensors.CF.GetElement(fullfilename).FileBody;
                    source_richTextBox.SelectionStart = (int)PluginSettings.viewSensors.SensorsPositions[sensor];
                    source_richTextBox.ScrollToCaret();
                }
                catch (Exception ex)
                {
                    Monitor.Log.Warning(PluginSettings.Name, "Ошибка при определении параметров датчика в хранилище: " + ex.Message);
                    source_groupBox.Text = "Исходный код";
                    source_richTextBox.Text = "";
                }
                //label5.Text = "Текущий датчик: " + sensorId;
                PluginSettings.viewSensors.PaintTriangle(graphics_panel, CurPosition, trackBarFactor.Value, trackBarOffset.Value); 
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            graphics_panel.Invalidate();
            panel1.Invalidate();
            labelFactor.Text = "Масштаб " + trackBarFactor.Value + ":1";

        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            graphics_panel.Invalidate();
            panel1.Invalidate();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            PluginSettings.viewSensors.DrawPanel1(panel1, trackBarFactor.Value, trackBarOffset.Value);
        }
    }
}
