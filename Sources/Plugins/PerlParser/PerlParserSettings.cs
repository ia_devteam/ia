﻿using System.Windows.Forms;

namespace IA.Plugins.Parsers.PerlParser
{
    public partial class PerlParserSettings : UserControl, IA.Plugin.Settings
    {
        public PerlParserSettings()
        {
            InitializeComponent();
            chkNDV2.Checked = PerlParser.PluginSettings.NDVLevel == 2;
            chkNDV2.CheckStateChanged += (o,e) => PerlParser.PluginSettings.NDVLevel = chkNDV2.Checked ? 2 : 3;
        }

        public void Save()
        {
            PerlParser.PluginSettings.NDVLevel = chkNDV2.Checked ? 2 : 3;
        }
    }
}
