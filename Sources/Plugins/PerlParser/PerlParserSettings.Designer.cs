﻿namespace IA.Plugins.Parsers.PerlParser
{
    partial class PerlParserSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpNDVSelection = new System.Windows.Forms.GroupBox();
            this.chkNDV2 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.grpNDVSelection.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpNDVSelection
            // 
            this.grpNDVSelection.Controls.Add(this.tableLayoutPanel1);
            this.grpNDVSelection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpNDVSelection.Location = new System.Drawing.Point(0, 0);
            this.grpNDVSelection.Name = "grpNDVSelection";
            this.grpNDVSelection.Size = new System.Drawing.Size(315, 45);
            this.grpNDVSelection.TabIndex = 0;
            this.grpNDVSelection.TabStop = false;
            this.grpNDVSelection.Text = "Уровень НДВ";
            // 
            // chkNDV2
            // 
            this.chkNDV2.AutoSize = true;
            this.chkNDV2.Checked = true;
            this.chkNDV2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNDV2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkNDV2.Location = new System.Drawing.Point(3, 3);
            this.chkNDV2.Name = "chkNDV2";
            this.chkNDV2.Size = new System.Drawing.Size(148, 20);
            this.chkNDV2.TabIndex = 0;
            this.chkNDV2.Text = "НДВ2";
            this.chkNDV2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.chkNDV2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(309, 26);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // PerlParserSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpNDVSelection);
            this.Name = "PerlParserSettings";
            this.Size = new System.Drawing.Size(315, 45);
            this.grpNDVSelection.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpNDVSelection;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox chkNDV2;
    }
}
