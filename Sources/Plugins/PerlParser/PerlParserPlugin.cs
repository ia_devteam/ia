﻿#pragma warning disable 1591
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using IA.Plugin;
using IA.Plugins.Parsers.CommonUtils;
using Store;
using Store.Table;


namespace IA.Plugins.Parsers.PerlParser
{
    /// <summary>
    /// Класс с реализацией парсинга исходных текстов на языке Perl 5.
    /// </summary>
    public class PerlParser : IA.Plugin.DefaultPlugin
    {
        internal static class PluginSettings
        {
            static PluginSettings()
            {
                NDVLevel = Properties.Settings.Default.NDVLevel;
                startId = Properties.Settings.Default.startSendorId;
                moduleId = Properties.Settings.Default.moduleId;
                sensorText = Properties.Settings.Default.SensorText;
            }

            public static uint ID = Store.Const.PluginIdentifiers.PERL_PARSER;
            public static string Name = "Парсер Perl";
            private static int ndvLevel = 3;
            public static int NDVLevel
            {
                get { return ndvLevel; }
                set
                {
                    if (value == 2 || value == 3)
                        ndvLevel = value;
                }
            }
            public static int startId;
            public static int moduleId;
            public static string sensorText;

            public static Storage storage;
        }

        public override uint ID { get { return PluginSettings.ID; } }

        public override string Name { get { return PluginSettings.Name; } }

        public override Capabilities Capabilities { get { return Capabilities.CUSTOM_SETTINGS_WINDOW; } }

        public override void Initialize(Storage storage)
        {
            PluginSettings.storage = storage;
        }

        public override UserControl CustomSettingsWindow
        {
            get
            {
                ulong maxSensId = PluginSettings.storage.sensors.SetSensorStartNumberAsMaximum();
                PluginSettings.startId = maxSensId == 1 ? PluginSettings.startId : (int)maxSensId;
                var control = new IA.Controls.ParsersLevelDATSID(typeof(PluginSettings),
                    new Dictionary<Controls.ParsersLevelDATSID.Ctrls, string>()
                    {
                        { Controls.ParsersLevelDATSID.Ctrls.DAT, "startId" },
                        { Controls.ParsersLevelDATSID.Ctrls.SID, "moduleId"  },
                        { Controls.ParsersLevelDATSID.Ctrls.Level, "ndvLevel"  },
                        { Controls.ParsersLevelDATSID.Ctrls.Text, "sensorText"  }
                    });

                return control;
            }
        }

        public override void Run()
        {
            var storage = PluginSettings.storage;

            var filesWithPerl = PluginSettings.storage.files.EnumerateFiles(enFileKind.fileWithPrefix)
                .Where(f => new List<string>() { ".t", ".pm", ".pl" }.IndexOf(Path.GetExtension(f.FileNameForReports).ToLower()) != -1)
                .Select(f => f.FullFileName_Original)
                .ToArray();

            if (filesWithPerl.Length == 0)
            {
                Monitor.Log.Warning(PluginSettings.Name, "Не обнаружено файлов для разбора.");
                return;
            }
            var startSensorCount = storage.sensors.Count();

            var currentExeDir = System.AppDomain.CurrentDomain.BaseDirectory; //bueee TODO

            RunConfiguration config = new RunConfiguration();

            config.fileWithFilelist = Path.Combine(storage.GetTempDirectory(ID).Path, "perlFilelist.txt");
            File.WriteAllLines(config.fileWithFilelist, filesWithPerl);

            config.sensorStartId = (ulong)PluginSettings.startId;
            config.dirWithClearFiles = storage.appliedSettings.FileListPath;
            config.dirWithLabFiles = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "Perl");
            Directory.CreateDirectory(config.dirWithLabFiles);
            config.dirWithReports = storage.pluginData.GetDataFolder(ID);
            config.level = PluginSettings.NDVLevel;
            config.pathToXSD = Path.Combine(currentExeDir, @"Common\ParserToStorageData.xsd"); //bueee
            config.sensorPlacementMode = "safe";
            config.sensorTemplate = PluginSettings.sensorText;
            config.moduleId = PluginSettings.moduleId;

            //если существовали уже вставленные датчики - надо IA проинформировать, что мы собираемя добавлять, а так как  файлы порождаются сторонней программой, то каталог надо подготовить заранее
            storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB);

            ProcessStartInfo psi = new ProcessStartInfo(Path.Combine(currentExeDir, @"Plugins\Perl\Strawberry\portableshell.bat"));
            psi.Arguments = "PerlHandler/lib/PerlHandler.pm " + config;
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            psi.WorkingDirectory = Path.Combine(currentExeDir, @"Plugins\Perl\"); //bueee

            Directory.CreateDirectory(config.dirWithLabFiles);

            var proc = Process.Start(psi);
            var resultO = proc.StandardOutput.ReadToEndAsync();
            var resultE = proc.StandardError.ReadToEndAsync();
            proc.WaitForExit();

            if (!string.IsNullOrWhiteSpace(resultE.Result))
            {
                Monitor.Log.Warning(Name, "При обработке возникли следующие ошибки (возможно, не критичные): " + resultE.Result);
            }

            if (!string.IsNullOrWhiteSpace(resultO.Result))
            {
                string strRegex = @"^ERROR\s\d{2}:\d{2}\s.+?(?= file: )\sfile:\s(.*)";
                Regex myRegex = new Regex(strRegex, RegexOptions.Multiline);
                var prefix = storage.appliedSettings.FileListPath != null? 
                    storage.appliedSettings.FileListPath.ToLower() : 
                    storage.appliedSettings.FileListPathFromUser.ToLower();
                foreach (Match myMatch in myRegex.Matches(resultO.Result))
                {
                    if (myMatch.Success)
                    {
                        foreach (var group in myMatch.Groups)
                        {
                            var errLine = group.ToString().ToLower();
                            if (errLine.StartsWith(prefix))
                            {
                                Monitor.Log.Error(Name, "Файл не распознан парсером: " + errLine.Substring(prefix.Length));
                            }
                        }
                    }
                }
            }
            try
            {
                StorageImporter.XMLStorageImporterForParser importer = new StorageImporter.XMLStorageImporterForParser();
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(Path.Combine(config.dirWithReports, "Perl.xml"));
                importer.ImportXML(xdoc, storage);
            }
            catch (System.Exception ex)
            {
                Monitor.Log.Error(Name, "Критическая ошибка при выполнении импорта обработанных данных: " + ex.Message);
#if DEBUG
                Monitor.Log.Error(Name, ex.StackTrace);
#endif
                //throw;
            }

            Monitor.Log.Warning(PluginSettings.Name, $"Вставлено датчиков: {(storage.sensors.Count() - startSensorCount)}");
        }

        public override void GenerateReports(string reportsDirectoryPath)
        {
            // Проверка на разумность
            if (PluginSettings.storage == null)
                throw new Exception("Хранилище не определено.");

            if (string.IsNullOrEmpty(reportsDirectoryPath))
                throw new Exception("Путь к каталогу с отчетами не определен.");

            //StatementPrinter
            StatementPrinter sprinter = new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), PluginSettings.storage);
            sprinter.Execute();
        }

        public override void SaveSettings(IBufferWriter settingsBuffer)
        {
            settingsBuffer.Add(PluginSettings.NDVLevel);

            Properties.Settings.Default.NDVLevel = PluginSettings.NDVLevel;
            Properties.Settings.Default.Save();
        }

        public override void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.NDVLevel = settingsBuffer.GetInt32();
        }

        [DataContract]
        internal class RunConfiguration
        {
            //на основе ParserRun.pm
            [DataMember(Name = "sources")]
            public string fileWithFilelist;
            [DataMember(Name = "start-sensor")]
            public ulong sensorStartId;
            [DataMember(Name = "basepath")]
            public string dirWithClearFiles;
            [DataMember(Name = "with-sensor-dir")]
            public string dirWithLabFiles;
            [DataMember(Name = "xml-log-dir")]
            public string dirWithReports;
            [DataMember(Name = "level")]
            public int level;
            [DataMember(Name = "xsd-path")]
            public string pathToXSD;
            [DataMember(Name = "return-mode")]
            public string sensorPlacementMode;
            [DataMember(Name = "sensor-template")]
            public string sensorTemplate;
            [DataMember(Name = "module-id")]
            public int moduleId;

            /*    
    my $filename = $params->{"sources"};
    my $withoutSensorsDir= $params->{"basepath"};
    my $withSensorsDir= $params->{"with-sensor-dir"};
    my $xmlAndLogDir= $params->{"xml-log-dir"};
    my $level= $params->{"level"};
    my $sensorTemplate = $params->{"sensor-template"};
    my $sensorStartId = $params->{"start-sensor"};
    my $moduleId = $params->{"module-id"};
    my $xsdPath= $params->{"xsd-path"};
    my $return_mode = $params->{"return-mode"};
             */

            public override string ToString()
            {
                return string.Join(" ",
                    fileWithFilelist,
                    dirWithClearFiles,
                    dirWithLabFiles,
                    dirWithReports,
                    level.ToString(),
                    sensorTemplate,
                    sensorStartId.ToString(),
                    moduleId.ToString(),
                    pathToXSD,
                    sensorPlacementMode);
            }
        }

    }
}
#pragma warning restore 1591
