using System;
using System.Linq;
using FileOperations;
using System.IO;
using HtmlAgilityPack;
using Store;
using IA.Extensions;
using System.Collections.Generic;

using ES3Lexer = Xebic.Parsers.ES3.ES3Lexer;
using ES3Parser = Xebic.Parsers.ES3.ES3Parser;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    class JavaScriptMain
    {
        private readonly Storage storage;

        private int fileCounter;

        public JavaScriptMain(Storage storage)
        {
            this.storage = storage;

            //счётчик обработанных файлов
            fileCounter = 0;
        }

        private bool isFileHasJavaScriptAndSomethingElse(IFile f)
        {
            return f.fileType.ContainsLanguages().Contains("JavaScript") &&
                   f.fileType != SpecialFileTypes.JAVA_SCRIPT_SOURCE;
        }

        /// <summary>
        /// Перечисляет все файлы, которые плагин должен анализировать
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<IFile> EnumerateFilesToAnalyze()
        {
            return storage.files.EnumerateUniqFiles(enFileKind.fileWithPrefix).Where(f => f.fileType.ContainsLanguages(enLanguage.JavaScript) || isFileHasJavaScriptAndSomethingElse(f));
        }


        private bool GenerateJSFromHTML(IFile htmlFile)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.Load(htmlFile.FullFileName_Original, System.Text.Encoding.UTF8);

            // HTML без javascript
            if (doc.DocumentNode.SelectNodes("//script") == null)
                return false;

            if (PluginSettings.IsLevel2)
            {
                bool nojs = true;

                using (FileStream jsparts = new FileStream(htmlFile.FullFileName_Original + ".js", FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter htmlscript = new StreamWriter(jsparts, System.Text.Encoding.UTF8))
                    {
                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//script").Where(link => link.Attributes["src"] == null))
                        {
                            htmlscript.Write(link.InnerText);
                            htmlscript.Write("\nvar rntcutforjsparts = 1\n");
                            nojs = false;
                        }

                        htmlscript.Flush();
                    }
                }

                if (nojs) // если скрипты есть, но там не JavaScript то удаляем пустой файл.
                {
                    File.Delete(htmlFile.FullFileName_Original + ".js");
                    return false;
                }
            }

            if (PluginSettings.IsLevel3 || PluginSettings.IsNoSensors)
            {
                using (FileStream jstream = new FileStream(htmlFile.FullFileName_Original + ".js", FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter htmlscript = new StreamWriter(jstream))
                    {
                        int prevline = 1;

                        foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//script").Where(link => link.Attributes["src"] == null))
                        {
                            for (int i = 0; i < link.Line - prevline; i++)
                                htmlscript.Write(Environment.NewLine);

                            //отступ от начала строки
                            for (int i = 0; i < link.LinePosition + (link.OuterHtml.Length - link.InnerHtml.Length - 10); i++)
                                htmlscript.Write(" ");

                            htmlscript.Write(link.InnerHtml);
                            prevline = link.Line;

                            //количество строк в тексте
                            int pos = link.InnerText.IndexOf("\n");
                            while (pos > -1)
                            {
                                prevline++;
                                pos = link.InnerText.IndexOf("\n", pos + 1);
                            }
                        }
                        htmlscript.Flush();
                    }
                }
            }

            return true;
        }

        private bool RestoreHTMLFromJS(IFile file)
        {
            //если мы имели дело с HTML то удаляем временные JS файлы, теперь нам нужны только такие файлы из папки с результатами
            File.Delete(file.FullFileName_Original + ".js");

            if (PluginSettings.IsNoSensors)
                return true;

            if (!PluginSettings.IsLevel2)
                return false;

            String name = Files.CanonizeFileName(file.FullFileName_Original + ".js").Replace(Files.CanonizeFileName(storage.appliedSettings.FileListPath), Files.CanonizeFileName(Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "JavaScript")));
            if (!File.Exists(name))
                return false;

            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.Load(file.FullFileName_Original, System.Text.Encoding.UTF8);

            AbstractReader html_script = null;
            try
            {
                html_script = new AbstractReader(name, OpenMode.SLASH0IGNORE);
            }
            catch (Exception ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    "Не удалось открыть файл " + html_script + ".",
                    ex.ToFullMultiLineMessage()
                }.ToMultiLineString();

                IA.Monitor.Log.Error(PluginSettings.Name, message);
            }

            String text = html_script.getText();
            int startindex = 0;

            foreach (HtmlNode link in htmlDoc.DocumentNode.SelectNodes("//script"))
            {
                if (link.Attributes["src"] != null) continue;

                int lastindex = text.IndexOf("\nvar rntcutforjsparts = 1", startindex);
                HtmlNode node = htmlDoc.CreateTextNode(text.Substring(startindex, lastindex - startindex));
                link.RemoveAllChildren();
                link.AppendChild(node);
                startindex = lastindex + 26;
            }
            htmlDoc.Save(name.Substring(0, name.Length - 3), System.Text.Encoding.UTF8);
            File.Delete(name);

            return true;
        }

        bool extract(string fName, bool add = false)
        {
            AbstractReader sr = new AbstractReader(fName);
            bool err = false;
            string s = sr.getText();
            string js = "";
            int currRep = 0, oldRep = 0;
            if (s.ToLower().Contains("<script"))
            {
                int pos = s.ToLower().IndexOf("<script");
                while (pos > 0)
                {
                    bool gotErr = false;
                    oldRep = currRep;
                    if (!s.ToLower().Substring(pos, s.IndexOf(">", pos) - pos).Contains("src=") && s.ToLower().IndexOf("<script\"", pos) != pos)
                    {

                        if (s.ToLower().IndexOf("</script>", pos) > 0)
                        {
                            int endpos = s.ToLower().IndexOf("</script>", pos);
                            int startpos = s.IndexOf(">", pos) + 1;
                            if (startpos > endpos)
                            {
                                pos = s.ToLower().IndexOf("<script", pos + 1);
                                continue;
                            }
                            //корректировка всвязи с бредом в виде <![CDATA[ и ]]>
                            int cdata_begin = s.IndexOf("<![CDATA[", startpos);
                            int cdata_end = s.IndexOf("]]>", (cdata_begin > 0) ? cdata_begin : 0);
                            if (cdata_begin >= startpos && cdata_begin < endpos && cdata_end > startpos && cdata_end < endpos)
                            {
                                startpos = cdata_begin + "<![CDATA[".Length;
                                endpos = cdata_end;
                            }
                            js = s.Substring(startpos, endpos - startpos);
                            bool find = true;
                            Dictionary<string, string> replaced = new Dictionary<string, string>();
                            while (find)
                            {
                                find = false;
                                int ind = js.IndexOf("<?");
                                if (ind > 0 && js.IndexOf("?>", ind) > 0)
                                {
                                    find = true;
                                    replaced.Add("PHPREPL" + (++currRep).ToString(), js.Substring(ind, js.IndexOf("?>", ind) + 2 - ind));
                                    js = js.Substring(0, ind) + "RNTPHPREPL(" + currRep.ToString() + ")" + js.Substring(js.IndexOf("?>", ind) + 2);
                                }
                            }
                            //для того чтобы плагин жаваскрипт правильно рассчитывал строки
                            if (add)
                            {
                                int lTotal = sr.countLineByOffset(startpos);
                                int cTotal = sr.countColumnByOffset(startpos);
                                string toAdd = string.Empty;
                                for (int i = 0; i < lTotal; i++)
                                    toAdd += '\n';
                                for (int i = 0; i < cTotal; i++)
                                    toAdd += ' ';
                                js = toAdd + js;
                            }
                            //

                            //
                            Antlr.Runtime.ANTLRStringStream stream = new Antlr.Runtime.ANTLRStringStream(js);
                            Xebic.Parsers.ES3.ES3Lexer lexer = new Xebic.Parsers.ES3.ES3Lexer(stream);
                            Xebic.Parsers.ES3.ES3Parser parser = new Xebic.Parsers.ES3.ES3Parser(new Antlr.Runtime.CommonTokenStream(lexer));
                            Xebic.Parsers.ES3.ES3Parser.program_return ret = null;

                            try
                            {
                                ret = parser.program();
                                if (parser.NumberOfSyntaxErrors > 0)
                                    gotErr = true;
                            }
                            catch (Exception ex)
                            {
                                err = gotErr = true;
                            }
                            if (!gotErr)
                            {
                                sr.writeTextToFile(fName + "(" + startpos + "," + endpos + ").js", js);

                                foreach (string key in replaced.Keys)
                                {
                                    sr.writeTextToFile(fName + "(" + startpos + "," + endpos + ").js." + key, replaced[key]);
                                }
                            }
                            else
                                currRep = oldRep;
                        }
                    }
                    pos = s.ToLower().IndexOf("<script", pos + 1);
                }
            }

            return err;
        }

        class Part
        {
            public int startPos, endPos;
            public string contents;
        }
        class PhpRpl
        {
            public string replace;
            public string value;
            public string origin;
        }

        void join(string fName)
        {
            List<string> cutJs = new List<string>();
            List<string> phpRepl = new List<string>();
            if (System.IO.File.Exists(fName))
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(fName);
                string path = fileInfo.DirectoryName;

                foreach (string file in System.IO.Directory.EnumerateFiles(path))
                {
                    if (file.Contains(fName) && file != fName && file.Contains(").js"))
                    {
                        if (!file.Contains("PHPREPL"))
                            cutJs.Add(file);
                        else
                            phpRepl.Add(file);
                    }
                }
                if (cutJs.Count == 0)
                    return;
            }

            bool sorted;
            do
            {
                sorted = true;
                for (int i = 0; i < cutJs.Count - 1; i++)
                {
                    int num1 = Int32.Parse(cutJs[i].Substring(cutJs[i].IndexOf("(") + 1, cutJs[i].IndexOf(",", cutJs[i].IndexOf("(")) - cutJs[i].IndexOf("(") - 1));
                    int num2 = Int32.Parse(cutJs[i + 1].Substring(cutJs[i + 1].IndexOf("(") + 1, cutJs[i + 1].IndexOf(",", cutJs[i + 1].IndexOf("(")) - cutJs[i + 1].IndexOf("(") - 1));
                    if (num1 < num2)
                    {
                        string temp = cutJs[i];
                        cutJs[i] = cutJs[i + 1];
                        cutJs[i + 1] = temp;
                        sorted = false;
                    }
                }
            } while (!sorted);

            List<PhpRpl> phpRpl = new List<PhpRpl>();
            foreach (string rp in phpRepl)
            {
                string repKey = rp.Substring(rp.IndexOf("PHPREPL"));
                AbstractReader frd = new AbstractReader(rp);
                string repValue = frd.getText().Trim();
                PhpRpl rpl = new PhpRpl();
                rpl.replace = repKey.Replace("PHPREPL", "RNTPHPREPL(") + ")";
                rpl.value = repValue;
                rpl.origin = rp.Substring(0, rp.IndexOf("PHPREPL") - 1);
                phpRpl.Add(rpl);
            }


            List<Part> repl = new List<Part>();
            foreach (string fCut in cutJs)
            {
                Part prt = new Part();
                prt.startPos = Int32.Parse(fCut.Substring(fCut.LastIndexOf("(") + 1, fCut.IndexOf(",", fCut.LastIndexOf("(")) - fCut.LastIndexOf("(") - 1));
                prt.endPos = Int32.Parse(fCut.Substring(fCut.LastIndexOf(",") + 1, fCut.IndexOf(")", fCut.LastIndexOf(",")) - fCut.LastIndexOf(",") - 1));
                AbstractReader frd = new AbstractReader(fCut);
                prt.contents = frd.getText().Trim();
                foreach (PhpRpl rpl in phpRpl)
                    if (fCut == rpl.origin)
                    {
                        if (prt.contents.Contains(rpl.replace))
                        {
                            prt.contents = prt.contents.Replace(rpl.replace, rpl.value);
                        }
                        else
                        {
                            /*err.Add(rpl.replace + " " + rpl.origin);*/
                        }

                    }
                repl.Add(prt);
            }
            AbstractReader fr = new AbstractReader(fName);
            string contents = fr.getText();
            foreach (Part r in repl)
            {
                contents = contents.Substring(0, r.startPos) + r.contents + contents.Substring(r.endPos - 1);
            }
            System.IO.File.Copy(fName, fName + ".OLDRNT");

            //если была хотя бы одна вставка, вставляем тело датчика в самое начало
            if (cutJs.Count > 0)
            {
                if ((fName.ToLower().EndsWith(".htm") || fName.ToLower().EndsWith(".html")) && contents.IndexOf("</body>") > 0)
                    contents = contents.Insert(contents.IndexOf("</body>"), "<script>" + (PluginSettings.IsLevel2 ? PluginSettings.SensorFunctionText_Level2 : PluginSettings.SensorFunctionText_Level3) + "</script>");
                else
                    contents = "<script>" + (PluginSettings.IsLevel2 ? PluginSettings.SensorFunctionText_Level2 : PluginSettings.SensorFunctionText_Level3) + "</script>" + contents;
            }

            fr.writeTextToFile(fName, contents);
        }

        public void Run()
        {
            var filesToAnalyze = EnumerateFilesToAnalyze().ToArray();//приходится делать ToArray для получения общего количества...
            IFile fileGlobalEnvironment = storage.files.Find("Глобальное окружение");

            //Нет файлов в Хранилище
            if (filesToAnalyze.Length == 0)
            {
                Monitor.Log.Error(PluginSettings.Name, "Файлы для обработки не найдены.");
                return;
            }

            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)filesToAnalyze.Length);

            //Выделяем внутреннюю структуру
            SemanticTables semantic = new SemanticTables(storage, PluginSettings.IsLevel3);
            string tempDirectoryClean = storage.GetTempDirectory(PluginSettings.ID).Path;
            string tempDirectoryLab = storage.GetTempDirectory(PluginSettings.ID).Path;

            //Разбираем файлы
            foreach (IFile fileNum in filesToAnalyze)
            {
                IFile file = fileNum;
                bool jsExtracted = false;


                if (file == fileGlobalEnvironment)
                    throw new ApplicationException("Такого быть не может. file.RelativeFileName_Original == Глобальное окружение");

                fileCounter++;

                //Заботимся о разделении файла на части, когда это нужно
                List<string> jsFilesToProcess = new List<string>();
                try
                {
                    if (isFileHasJavaScriptAndSomethingElse(file)) //Эти файлы надо делить. Выполняем деление.
                    {
                        //JS in PHP - do a split, baby
                        jsExtracted = true;
                        //cleaning previous results
                        foreach (string toDel in System.IO.Directory.EnumerateFiles(tempDirectoryClean, "*.*", SearchOption.AllDirectories))
                        {
                            System.IO.File.Delete(toDel);
                        }

                        string filename = Path.GetFileName(file.FullFileName_Original);
                        string newPath = Path.Combine(tempDirectoryClean, filename);
                        string newPathLab = Path.Combine(tempDirectoryLab, filename);

                        System.IO.File.Copy(file.FullFileName_Original, newPath);

                        try
                        {
                            if (extract(newPath, true))
                            {
                                Monitor.Log.Warning(PluginSettings.Name, "Не все части JS файла были извлечены корректно из " + file.FileNameForReports);
                            }
                        }
                        catch (Exception e)
                        {
                            Monitor.Log.Warning(PluginSettings.Name, "Ошибки в верстке файла " + file.FileNameForReports + " извлечение не удалось");
                            continue;
                        }

                        foreach (string newP in Directory.EnumerateFiles(tempDirectoryClean, "*.*", SearchOption.AllDirectories))
                        {
                            File.Copy(newP, newP.Replace(tempDirectoryClean, tempDirectoryLab), true);
                        }

                        jsFilesToProcess.AddRange(System.IO.Directory.EnumerateFiles(tempDirectoryClean, "*.js"));

                        if (jsFilesToProcess.Count == 0) continue;
                    }
                    else //Значит мы имеем дело с чистым JavaScrpit. Делить не надо
                    {
                        //vanilla script 
                        jsExtracted = false;
                        jsFilesToProcess.Add(file.FullFileName_Original);
                    }

                }
                catch (Exception e)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось открыть файл " + ". Ошибка " + e.Message);
                }

                //Выполняем основные работы
                foreach (string jsFile in jsFilesToProcess)
                {
                    AbstractReader fileReader = null;
                    fileReader = new AbstractReader(jsFile);

                    string contents = fileReader.getText();

                    Antlr.Runtime.ANTLRStringStream stream = new Antlr.Runtime.ANTLRStringStream(contents);
                    ES3Lexer lexer = new ES3Lexer(stream);
                    ES3Parser parser = new ES3Parser(new Antlr.Runtime.CommonTokenStream(lexer));

                    ES3Parser.program_return ret = null;

                    try
                    {
                        ret = parser.program();

                        if (parser.NumberOfSyntaxErrors > 0)
                        {
                            if (PluginSettings.IsSkipErrors)
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Не удалось разобрать файл " + file.FullFileName_Original);
                            else
                                throw new Exception("Не удалось разобрать файл " + file.FullFileName_Original);
                        }
                    }
                    catch (Exception ex)
                    {
                        //Формируем сообщение
                        string message = new string[]
                        {
                            "Не удалось разобрать файл " + file.FullFileName_Original + ".",
                            ex.ToFullMultiLineMessage()
                        }.ToMultiLineString();

                        if (PluginSettings.IsSkipErrors)
                            IA.Monitor.Log.Warning(PluginSettings.Name, message);
                        else
                            throw new Exception(message);
                    }

                    //Переходим к разбору дерева. Семанитический анализ.
                    //Нулевой проход - собираем определения VAR и function. При этом создаем в памяти собственную структуру данных.
                    //Первый проход - разбираемся с использованием функций как переменных
                    //Второй проход - укладываем информацию                            
                    //Третий проход - добавление датчиков в Хранилище

                    if (ret != null && ret.Tree == null)
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "В файле " + file.FullFileName_Original + " отсутствует код");
                    }
                    else
                    {
                        semantic.Setup(file);
                        if (ret != null)
                        {

                            ES3Tree treeParser = new ES3Tree(new Antlr.Runtime.Tree.CommonTreeNodeStream(ret.Tree));
                            treeParser.SetSemanticTables(semantic);

                            //Проход 0
                            //Собираем информацию о var и function. Заодно строим уменьшенное представление.
                            EntityCode result = null;
                            try
                            {
                                result = treeParser.program();

                                if (treeParser.NumberOfSyntaxErrors > 0)
                                {
                                    if (PluginSettings.IsSkipErrors)
                                        IA.Monitor.Log.Warning(PluginSettings.Name, "Не удалось разобрать грамматику " + file.FullFileName_Original);
                                    else
                                        throw new Exception("Не удалось разобрать грамматику " + file.FullFileName_Original);
                                }

                                
                                if (result != null)
                                {
                                    //Проход 1
                                    //Статус функций-переменных
                                    result.Pass1(semantic);

                                    //Проход 2
                                    //Собираем информацию 
                                    result.Pass2(semantic);

                                    //Проход 3
                                    //Добавление датчиков в Хранилище
                                    result.Pass3(semantic);
                                    if (semantic.currentScope.Statement != null)
                                        semantic.globalScope.Function.EntryStatement = semantic.currentScope.Statement;
                                }
                            }
                            catch (Exception ex)
                            {
                                //Формируем сообщение
                                string message = new string[]
                                {
                                    "Не удалось разобрать грамматику " + file.FullFileName_Original + ".",
                                    ex.ToFullMultiLineMessage()
                                }.ToMultiLineString();

                                if (PluginSettings.IsSkipErrors)
                                    IA.Monitor.Log.Warning(PluginSettings.Name, message);
                                else
                                    throw new Exception(message);
                            }
                        }
                        
                        SensorInserter inserter = new SensorInserter(storage, Files.CanonizeFileName(storage.appliedSettings.FileListPath), Files.CanonizeFileName(Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "JavaScript")), jsExtracted, PluginSettings.IsLevel2, parser.begendPlace);

                        var funcsToProcess = storage.files.GetFile(semantic.fileID).FunctionsDefinedInTheFile();
                        fileGlobalEnvironment = fileGlobalEnvironment ?? storage.files.Find("Глобальное окружение");
                        if (fileGlobalEnvironment != null)
                        {
                            funcsToProcess = funcsToProcess.Union(fileGlobalEnvironment.FunctionsDefinedInTheFile());
                        }

                        foreach (var func in funcsToProcess)
                        {
                            IStatement st = func.EntryStatement;

                            ulong beginloc = 0;
                            if (jsFilesToProcess.Count > 1)
                            {
                                beginloc = ulong.Parse(jsFile.Substring(jsFile.LastIndexOf("(") + 1, jsFile.LastIndexOf(",") - jsFile.LastIndexOf("(") - 1));
                            }

                            if (st != null && st.FirstSymbolLocation != null && st.FirstSymbolLocation.GetOffset() < beginloc)
                            {
                                IStatement found = null;
                                while (st.NextInLinearBlock != null)
                                {
                                    st = st.NextInLinearBlock;
                                    if (st.FirstSymbolLocation != null && st.FirstSymbolLocation.GetOffset() >= beginloc && st.FirstSymbolLocation.GetFileID() == file.Id)
                                    {
                                        found = st;
                                        break;
                                    }
                                }
                                if (found == null)
                                    continue;
                                st = found;
                            }

                            if (st != null)
                            {
                                inserter.StatementWalk(st, semantic.fileID);
                            }
                        }
                                                
                        if (ret != null)
                        {
                            if (!jsExtracted)
                                inserter.SensorWalk(file, ref ret.ele, parser.sensPlace, fileReader);
                            else
                                inserter.SensorWalk(file, ref ret.ele, parser.sensPlace, fileReader, tempDirectoryLab);
                            parser.sensPlace.Clear();
                        }
                    }
                }

                string filePathnameInTempdir = Path.Combine(tempDirectoryLab, file.FullFileName_Original.Substring(file.FullFileName_Original.LastIndexOf("\\") + 1));
                string filePathnameInLabdir = file.FullFileName_Original.Replace(storage.appliedSettings.FileListPath, Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "javascript") + "\\");

                //Собираем разделенные файлы
                if (jsExtracted)
                {
                    string directoryPathInLabdir = filePathnameInLabdir.Substring(0, filePathnameInLabdir.LastIndexOf("\\"));

                    join(filePathnameInTempdir);
                    Directory.CreateDirectory(directoryPathInLabdir);
                    System.IO.File.Copy(filePathnameInTempdir, filePathnameInLabdir);
                }

                //Заботимся о дублирующих файлах. Мы их не разбирали. Однако датчики в них должны быть вставлены. Поэтому берём проанализированный файл и кладём его вместо копий
                foreach (IFile fileCopy in file.GetCopies())
                {
                    try
                    {
                        string fileCopyPathnameInLabdir = fileCopy.FullFileName_Original.Replace(storage.appliedSettings.FileListPath, Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "javascript") + "\\");
                        string directoryCopyPathInLabdir = fileCopyPathnameInLabdir.Substring(0, fileCopyPathnameInLabdir.LastIndexOf("\\"));

                        Directory.CreateDirectory(directoryCopyPathInLabdir);
                        System.IO.File.Copy(filePathnameInLabdir, fileCopyPathnameInLabdir);
                    }
                    catch (Exception e)
                    {
                        Monitor.Log.Warning(PluginSettings.Name, "Ошибка копирования дубликатов: " + e.Message);
                    }
                }

                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)fileCounter);
            }
        }
    }
}
