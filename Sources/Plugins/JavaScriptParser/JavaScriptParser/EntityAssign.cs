﻿using System;
using System.Collections.Generic;
using Store;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    /// <summary>
    /// Вершина промежуточного представления, соответствующая присваиванию (==)
    /// </summary>
    public class EntityAssign : Entity
    {
        /// <summary>
        /// Левый операнд
        /// </summary>
        Entity left;
        /// <summary>
        /// Правый операнд
        /// </summary>
        Entity right;

        UInt64 Line;
        UInt64 Column;

        internal override int[] getPos()
        {
            return new int[] { (int)Line, (int)Column };
        }

        internal EntityAssign(Entity st1, Entity st2, Antlr.Runtime.IToken token)
        {
            left = st1;
            right = st2;
            Line = (UInt64) token.Line;
            Column = (UInt64) token.CharPositionInLine + 1;
        }

        internal override void Pass1(SemanticTables semantic)
        {
            //Смотрим, что у нас стоит слева
            List<string> left_name = left.Identifier();

            //Слева стоит переменная. Обрабатываем ее встречу в текущем простаранстве имен
            Variable var = null;

            //Обрабатываем левую часть
            if (left_name != null)  //==null например случается, когда слева стоит a[b]
            {
                EntityScope.ScopeFound el = semantic.currentScope.Get(left_name, semantic);
                if (el.variable != null)   //Если в некотором пространстве имен была объевлена переменная
                                          //с таким именем
                    var = el.variable;
                else      //Если это либо впервые встретившаяся глобальная переменная, 
                //либо встроенная в стреду исполнения
                //либо определенная в другом скрипте
                {
                    var = semantic.variables.AddVariable();
                    string name1;
                    bool isGlobal; //Значение переменной игнорируем
                    semantic.ListToName(left_name, out name1, out isGlobal, semantic);
                    var.Name = name1;
                    var.AddValueSetPosition(semantic.fileName, Line, Column, semantic.currentScope.Function);
                    semantic.globalScope.Add_Step1(var);
                }

                //Если переменная стоит слева, то она меняется
                VariableChange(semantic, var);
            }

            //Переходим к правой части
            if (right is EntityFunction)    //Если справа встретилась функция, то добавляем в перечню функций,
            {
                //на которые может ссылаться переменная, эту функцию.
                //Запоминаем, что новая переменная ссылалась на функцию.
                if (var != null)
                    VariableGetPointer(semantic, var, (right as EntityFunction).Function, Line, Column, true);
            }

            List<string> name = right.Identifier();

            if (name != null) //Справа стоит идентификатор. То есть присвоение одного индетификатора другому
            {
                EntityScope.ScopeFound found = semantic.currentScope.Get(name, semantic);
                if (found.variable != null)    //Присваиваем значение одной переменной другой переменной.
                {
                    VariableRead(semantic, found.variable );
                    if (var != null)
                        VariableAssignment(semantic, found.variable, var);
                }
                else                                        //Значит, такого идентификатора мы еще не встречали.
                {
                    Variable var1 = semantic.variables.AddVariable();
                    string name2;
                    bool isGlobal; //Значение переменной игнорируем
                    semantic.ListToName(name, out name2, out isGlobal, semantic);
                    var1.Name = name2;
                    var1.AddValueSetPosition(semantic.fileName, Line, Column, semantic.currentScope.Function);
                    semantic.globalScope.Add_Step1(var1);
                }
            }

            //В любом случае продолжаем pass1
            right.Pass1(semantic);
        }

        internal override void Pass2(SemanticTables semantic)
        {
            left.Pass2(semantic);
            right.Pass2(semantic);
        }
        internal override void Pass3(SemanticTables semantic)
        {
            left.Pass3(semantic);
            right.Pass3(semantic);
        }
        private void VariableRead(SemanticTables semantic, Variable variable)
        {
            variable.AddValueGetPosition(semantic.fileName, Line, Column, semantic.currentScope.Function);
        }

        private void VariableChange(SemanticTables semantic, Variable var)
        {
            var.AddValueSetPosition(semantic.fileName, Line, Column, semantic.currentScope.Function);
        }

        private void VariableAssignment(SemanticTables semantic, Variable from, Variable to)
        {
            IAssignment ass = from.AddAssignmentsFrom();
            ass.To = to;
            ass.AddPosition(semantic.fileName, Line, Column, semantic.currentScope.Function);
            ass.Save();
        }

        static internal void VariableGetPointer(SemanticTables semantic, Variable var, IFunction function, UInt64 line, UInt64 column, bool isUseFunctionDef)
        {
            Store.Location def = function.Definition();
            if (def == null || !isUseFunctionDef)
                var.AddValueSetPosition(semantic.fileName, line, column, semantic.currentScope.Function);
            else
                var.AddValueSetPosition(semantic.fileName, def.GetLine(),
                                                def.GetColumn(), semantic.currentScope.Function);
            PointsToFunction points = var.AddPointsToFunctions();
            points.function = function.Id;
            if (def == null || !isUseFunctionDef)
                points.AddPosition(semantic.fileName, line, column, semantic.currentScope.Function);
            else
                points.AddPosition(semantic.fileName, def.GetLine(),
                                def.GetColumn(), semantic.currentScope.Function);
            points.isGetAddress = true;
            points.Save();
        }
        
    }
}
