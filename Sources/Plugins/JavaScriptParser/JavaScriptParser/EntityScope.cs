﻿using System.Collections.Generic;
using Store;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    /// <summary>
    /// Вершина, которая организует собственную область видимости
    /// </summary>
    abstract public class EntityScope : Entity
    {
        /// <summary>
        /// Область видимости, в которую вложена эта.
        /// null, если эта область глобальная.
        /// </summary>
        EntityScope parent = null;
        
        /// <summary>
        /// Елементы, находящиеся в данной области видимости.
        /// Так как функция является одновременно переменной, то хранятся только переменные
        /// </summary>
        Dictionary<string, Variable> varsInScope = new Dictionary<string, Variable>();

        internal override int[] getPos()
        {
            return new int[] { (int)0, (int)0 };
        }

        internal void Add_Step0(Variable var)
        {
          if(var != null)
                if(var.Name != null)
                    if(!varsInScope.ContainsKey(var.Name))
                        varsInScope.Add(var.Name, var);
        }

        /// <summary>
        /// Используется при обходе. Входим в эту область видимости
        /// </summary>
        /// <param name="semantic"></param>
        protected void EnterScope(SemanticTables semantic)
        {
            parent = semantic.currentScope;
            semantic.currentScope = (EntityFunction)this;
        }

        /// <summary>
        /// Используется при обходе. Выйти из этой области видимости.
        /// </summary>
        /// <param name="semantic"></param>
        protected void LeaveScope(SemanticTables semantic)
        {
            semantic.currentScope = (EntityFunction)parent;
        }

        internal void Add_Step1(Variable var)
        {
            if(var.Name != null)
                varsInScope[var.Name] = var;
        }

        /// <summary>
        /// Тип, возвращаемый методом Get
        /// </summary>
        internal struct ScopeFound
        {
            public EntityScope scope;
            public Variable variable;
        }

        internal ScopeFound Get(List<string> name, SemanticTables semantic)
        {
            string name1;
            bool isGlobal;
            semantic.ListToName(name, out name1, out isGlobal,semantic);
            if (isGlobal)
                return semantic.globalScope.Get(name1);
            else
                return Get(name1);
        }

        internal ScopeFound Get(string name)
        {
            Variable el;
            if (varsInScope.TryGetValue(name, out el))
                return new ScopeFound { scope = this, variable = el };

            if (parent != null)
                return parent.Get(name);

            return new ScopeFound { scope = null, variable = null };
        }

        internal Variable ContainsVariable(string name)
        {
            Variable var;
            if (varsInScope.TryGetValue(name, out var))
                return var;
            if (varsInScope.ContainsKey(name))
                return varsInScope[name];

            return null;
        }

    }
}
