﻿using System;
using Store;


namespace IA.Plugins.Parsers.JavaScriptParser
{
    /// <summary>
    /// Вершина промежуточного представления, соответствующая явному определению переменной через var.
    /// </summary>
    public class EntityVariable : Entity
    {
        /// <summary>
        /// Запись в Хранилище, соответствующая этой переменной
        /// </summary>
        Variable var;

        /// <summary>
        /// Значением, присваиваемое переменной в конструкции var a = ...
        /// </summary>
        Entity assign;

        internal override int[] getPos()
        {
            if(assign != null)
                return assign.getPos();
            else
                return new int[] { (int)0, (int)0 };
        }

        static internal EntityVariable AddEntityVariable(string name, Antlr.Runtime.IToken token, bool isLocal, SemanticTables semantic)
        {
            Variable var = semantic.currentScope.ContainsVariable(name);
            
            return var == null ? new EntityVariable(name, token, isLocal, semantic) : new EntityVariable(var);
        }

        static internal EntityVariable AddEntityVariable(string name, Entity assign, Antlr.Runtime.IToken token, bool isLocal, SemanticTables semantic)
        {
            Variable var = semantic.currentScope.ContainsVariable(name);

            return var == null ? new EntityVariable(name, assign, token, isLocal, semantic) : new EntityVariable(var, assign, token, isLocal, semantic);
        }

        private EntityVariable(Variable var)
        {
            this.var = var;
        }

        private EntityVariable(Variable var, Entity assign, Antlr.Runtime.IToken token, bool isLocal, SemanticTables semantic)
        {
            this.var = var;

            this.assign = assign;
            //step0 и step1 Добавлять не надо - уже добавлены
            var.AddDefinition(semantic.fileName, (UInt64)token.Line, (UInt64)token.CharPositionInLine + 1, semantic.currentScope.Function);
            if (assign is EntityFunction)
            {
                (assign as EntityFunction).AssignName(var.Name);
                ReportReference(assign as EntityFunction, token, semantic);
            }

            var.AddValueSetPosition(semantic.fileName, (UInt64)token.Line, (UInt64)token.CharPositionInLine + 1, semantic.currentScope.Function);
        }

        private EntityVariable(string name, Antlr.Runtime.IToken token, bool isLocal, SemanticTables semantic)
        {
            CreateVariable(name, token, isLocal, semantic);
        }

        private void CreateVariable(string name, Antlr.Runtime.IToken token, bool isLocal, SemanticTables semantic)
        {
            ReportVariable(name, token, semantic);

            if (isLocal)
                semantic.currentScope.Add_Step0(var);
            else
                semantic.globalScope.Add_Step1(var);
        }

        private EntityVariable(string name, Entity assign, Antlr.Runtime.IToken token, bool isLocal, SemanticTables semantic)
        {
            this.assign = assign;

            CreateVariable(name, token, isLocal, semantic);

            if (assign is EntityFunction)
            {
                (assign as EntityFunction).AssignName(name);
                ReportReference(assign as EntityFunction, token, semantic);
            }

            var.AddValueSetPosition(semantic.fileName, (UInt64)token.Line, (UInt64)token.CharPositionInLine + 1, semantic.currentScope.Function);
        }

        /// <summary>
        /// Указываем присвоение переменной адреса функции
        /// </summary>
        /// <param name="entityFunction"></param>
        private void ReportReference(EntityFunction entityFunction, Antlr.Runtime.IToken token, SemanticTables semantic)
        {
            PointsToFunction points = var.AddPointsToFunctions();
            points.function = entityFunction.Function.Id;
            points.AddPosition(semantic.fileName, (UInt64)token.Line, (UInt64)token.CharPositionInLine + 1, semantic.currentScope.Function);
            points.isGetAddress = true;
            points.Save();
        }

        private void ReportVariable(string name, Antlr.Runtime.IToken token, SemanticTables semantic)
        {
            var = semantic.variables.AddVariable();
            var.Name = name;
            var.AddDefinition(semantic.fileName, (UInt64)token.Line, (UInt64)token.CharPositionInLine + 1, semantic.currentScope.Function);
        }

        internal override void Pass1(SemanticTables semantic)
        {
            if(assign!= null)
                assign.Pass1(semantic);
        }

        internal override void Pass2(SemanticTables semantic)
        {
            if (assign != null)
                assign.Pass2(semantic);
        }

        internal override void Pass3(SemanticTables semantic)
        {
            if (assign != null)
                assign.Pass3(semantic);
        }
    }
}
