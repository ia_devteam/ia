tree grammar ES3Tree;


options {
  language = CSharp2 ;
	
  // We're going to process an AST whose nodes are of type CommonTree.
  ASTLabelType = CommonTree;

  // We're going to use the tokens defined in
  // both our MathLexer and MathParser grammars.
  // The MathParser grammar already includes
  // the tokens defined in the MathLexer grammar.
  tokenVocab = ES3;
  
  backtrack=true; 
}


@namespace { IA.Plugins.Parsers.JavaScriptParser }
@members {
IToken close = null;
}


program	returns [EntityCode res]
@init
{
res = new EntityCode();
IToken tok = null;
}
	:	(
			{tok = CurrentToken();} 
			sourceElement	
			{$res.Add($sourceElement.res, tok);}
		)*
	;

sourceElement returns [Entity res]
options
{
	k = 1 ;
}
	:	statement	{$res = $statement.res;}
	;
	
functionExpression returns [EntityFunction res]
@init
{
	IToken tok = null;
}
	:	^(	FUNCTION 
			(	name=Identifier
			)?						
			formalParameterList 	{$res = new EntityFunction($name == null ? null : $name.Text, $name == null ? $FUNCTION.Token : $name.Token, $formalParameterList.res, semantic);}
			{tok = CurrentToken();}
			functionBody			{$res.Body($functionBody.res, semantic, tok, close);}
		) 
	;
	
formalParameterList returns [System.Collections.Generic.List<EntityFunction.Parameter> res]
@init
{
	$res = new System.Collections.Generic.List<EntityFunction.Parameter>();
}
	:	^(ARGS (name=Identifier   {$res.Add(new EntityFunction.Parameter(){name = $name.Text, token = $name.Token});})*)
	;
	
functionBody returns [Entity res]
	:	block 	{$res = $block.res;}
	;

block	returns [EntityCode res]
@init
{
res = new EntityCode();
IToken tok = null;
}
	:	^(BLOCK {ReportSensor($BLOCK.Token);} ( {tok = CurrentToken(); } sourceElement  {$res.Add($sourceElement.res, tok);}) * (RBRACE {ReportSensor($RBRACE.Token); close = $RBRACE.Token;} )? )
	;

statement returns [Entity res]
	:	block			{$res = $block.res;}
	|	functionExpression	{$res = $functionExpression.res;}
	|	variable		{$res = $variable.res;}
	|	ifStatement		{$res = $ifStatement.res;}
	|	iterationStatement	{$res = $iterationStatement.res;}
	|	continueStatement	{$res = $continueStatement.res;}
	|	breakStatement		{$res = $breakStatement.res;}
	|	returnStatement		{$res = $returnStatement.res;}
	|	withStatement		{$res = $withStatement.res;}
	|	labelledStatement	{$res = $labelledStatement.res;}
	|	switchStatement		{$res = $switchStatement.res;}
	|	throwStatement		{$res = $throwStatement.res;}
	|	tryStatement		{$res = $tryStatement.res;}
	|	expressionStatement	{$res = $expressionStatement.res;}
	;
	
variable returns [EntityVariable res]
	:	^(VAR variableDeclaration)	{$res = $variableDeclaration.res;}
	|	^(CONST variableDeclaration)	{$res = $variableDeclaration.res;}
	;
	
variableDeclaration returns [EntityVariable res]
	:(	name=Identifier 				{$res = EntityVariable.AddEntityVariable($name.Text, $name.Token, true, semantic);}
	 | 	^(ASSIGN name=Identifier assignmentExpression) 	{$res = EntityVariable.AddEntityVariable($name.Text, $assignmentExpression.res, $name.Token, true, semantic);}
	 )+
	;
	
expressionStatement returns [Entity res]
	:	expression	{$res = $expression.res;}	
	;	
	
ifStatement  returns [EntityCode res]
	:	^(	IF 
			expression 
			(	st1=statement		{$res = new EntityCode($expression.res, $st1.res, $IF.Token, $IF.Token);} 
			)?	//if (); will not produce the statement
			(	st2=statement	{$res.Add($st2.res, $IF.Token);}
			)?
		)
	;
	
iterationStatement returns [EntityCode res]
	:	^(	DO 
			st1=statement 		{$res = new EntityCode($st1.res, $DO.Token);}
			(	expression	{$res.Add($expression.res, $DO.Token);}
			)? 
		)
	|	^(	WHILE 
			expression 		{$res = new EntityCode($expression.res, $WHILE.Token);}
			(	st2=statement	{$res.Add($st2.res, $WHILE.Token);}
			)?
		)
	|	^(	FOR 			
			forControl 		{$res = new EntityCode($forControl.res, $FOR.Token);}
			(	statement	{$res.Add($statement.res, $FOR.Token);}
			)?
		)
	;


continueStatement	returns [EntityCode res]
	:	^(CONTINUE {$res = new EntityCode($CONTINUE.Token);} Identifier? )
	;
	
forControl returns [EntityCode res]
@init
{
$res = new EntityCode();
bool first = false, second = false, third = false;
}
	:	^( 	FORITER 
			^( 	VAR 
				variableDeclaration	{$res.Add($variableDeclaration.res, $VAR.Token);}
			) 
			^( 	ee1=EXPR 
				expression 		{$res.Add($expression.res, $ee1.Token);}
			) 
		)
	|	^( 	FORITER 
			^( 	ee2=EXPR 
				ex2=expression 		{$res.Add($ex2.res, $ee2.Token);}
			) 
			^( 	ee3=EXPR 
				ex3=expression 		{$res.Add($ex3.res, $ee3.Token);}
			) 
		)
	|	^( 	FORSTEP 
			^( 	VAR 
				variableDeclaration 	{$res.Add($variableDeclaration.res, $VAR.Token);}
			) 
			^( 	ee4=EXPR 
				(	ex4=expression	{second = true; $res.Add($ex4.res, $ee4.Token);}
				)?
			)  {if(!second) $res.Add(null, $ee4.Token);}
			^( 	ee5=EXPR 
				(	ex5=expression	{third = true; $res.Add($ex5.res, $ee5.Token);}
				)? 
			) {if(!third) $res.Add(null, $ee5.Token);}
		)
	|	^( 	FORSTEP 
			^( 	ee6=EXPR
				(	ex6=expression	{first = true; $res.Add($ex6.res, $ee6.Token);}
				)? 
			) {if(!first) $res.Add(null, $ee6.Token);}
			^( 	ee7=EXPR 
				(	ex7=expression	{second = true; $res.Add($ex7.res, $ee7.Token);}
				)? 
			){if(!second) $res.Add(null, $ee7.Token);}
			^( 	ee8=EXPR 
				(	ex8=expression	{third = true; $res.Add($ex8.res, $ee8.Token);}
				)? 
			){if(!third) $res.Add(null, $ee8.Token);}
		) 
	;

	
breakStatement returns [EntityCode res]
	:	^(BREAK { $res = new EntityCode($BREAK.Token); } Identifier? )
	;

returnStatement returns [EntityCode res]
	:	 ^(	RETURN { $res = new EntityCode($RETURN.Token); }
			(	expression {$res.Add($expression.res, $RETURN.Token);}
			)?	
		 )
	;
	
withStatement returns [EntityCode res]
	:	^(WITH expression statement)	{$res = new EntityCode($expression.res, $statement.res, $WITH.Token, $WITH.Token);}
	;
	
labelledStatement returns [EntityCode res]
	:	^( LABELLED Identifier statement )	{$res = new EntityCode($statement.res, $Identifier.Token);}
	;	
	
switchStatement returns [EntityCode res]
	:	^( 	SWITCH 
			ex1=expression 				{$res = new EntityCode($ex1.res, $SWITCH.Token);}
			(	^(
					DEFAULT 
					(	st1=statement	{$res.Add($st1.res, $DEFAULT.Token);}
					)*
				)
			)? 
			(
				^(	CASE 
					ex2=expression 		{$res.Add($ex2.res, $CASE.Token);}
					^( CASEST
						(	st2=statement 	{$res.Add($st2.res, $CASEST.Token);}
						)*
					)
				)
			)* 
		)
	;
	
throwStatement returns [EntityCode res]
	:	^(THROW expression)		{$res = new EntityCode($expression.res, $THROW.Token);}
	;
	
tryStatement returns [EntityCode res]
	:	^(	TRY 
			b1=block 					{$res = new EntityCode($b1.res, $TRY.Token);}
			(	^(	CATCH 
					Identifier 	
					b2=block		{$res.Add($b2.res, $CATCH.Token);}
				)
			)? 
			(	^(	FINALLY 
					b3=block			{$res.Add($b3.res, $FINALLY.Token);}
				)
			)?
		)
	;	



expression returns [Entity res]
	: 	^( 	CEXPR 
			(	assignmentExpression	{$res= new EntityCode($assignmentExpression.res, $CEXPR.Token);}
			)+ 
		)
	| 	assignmentExpression	{$res = $assignmentExpression.res;}
	;

assignmentExpression returns [Entity res]
@init
{IToken tok = null;
}
	:	conditionalExpression		{$res = $conditionalExpression.res;}
	|	{tok = CurrentToken();}
		^(	(MULASS | DIVASS | MODASS | ADDASS | SUBASS | SHLASS | SHRASS | SHUASS | ANDASS | XORASS | ORASS) 
			conditionalExpression 	
			as1=assignmentExpression	{$res = new EntityCode($conditionalExpression.res, $as1.res, tok, tok);}
		)
	|	^(	ASSIGN
			conditionalExpression 	
			as1=assignmentExpression	{$res = new EntityAssign($conditionalExpression.res, $as1.res, $ASSIGN.Token);}
		)
	;	

conditionalExpression returns [Entity res]
	:	logicalORExpression			{$res = $logicalORExpression.res;}
	|	^(	QUE 
			logicalORExpression 		
			as1=assignmentExpression 	
			as2=assignmentExpression	{$res= new EntityCode($logicalORExpression.res, $as1.res, $as2.res, $QUE.Token, $QUE.Token, $QUE.Token);}
		)
	;
	
logicalORExpression returns [Entity res]
	:	logicalANDExpression			{$res = $logicalANDExpression.res;}
	|	^(	LOR	
			l1=logicalORExpression 		
			l2=logicalANDExpression		{$res= new EntityCode($l1.res, $l2.res, $LOR.Token, $LOR.Token);}
		)
	;	
	
logicalANDExpression returns [Entity res]
	:	bitwiseORExpression					{$res = $bitwiseORExpression.res;}
	|	^(LAND	l1=logicalANDExpression bitwiseORExpression)	{$res= new EntityCode($l1.res, $bitwiseORExpression.res, $LAND.Token, $LAND.Token);}
	;
	
bitwiseORExpression returns [Entity res]
	:	bitwiseXORExpression			{$res = $bitwiseXORExpression.res;}
	|	^(	OR 
			b1=bitwiseORExpression 		
			bitwiseXORExpression		{$res= new EntityCode($b1.res, $bitwiseXORExpression.res, $OR.Token, $OR.Token);}
		)
	;
	
bitwiseXORExpression returns [Entity res]
	:	bitwiseANDExpression			{$res = $bitwiseANDExpression.res;}
	|	^(	XOR 
			b1=bitwiseXORExpression 	
			bitwiseANDExpression		{$res= new EntityCode($b1.res, $bitwiseANDExpression.res, $XOR.Token, $XOR.Token);}
		)
	;

bitwiseANDExpression returns [Entity res]
	:	equalityExpression			{$res = $equalityExpression.res;}
	|	^(	AND 
			b1=bitwiseANDExpression 	
			equalityExpression		{$res= new EntityCode($b1.res, $equalityExpression.res, $AND.Token, $AND.Token);}
		)
	;

equalityExpression returns [Entity res]
@init{
IToken tok = null;
}
	:	relationalExpression			{$res = $relationalExpression.res;}
	|	{tok = CurrentToken();} 
		^(	( EQ | NEQ | SAME | NSAME ) 
			e1=equalityExpression 		
			relationalExpression		{$res= new EntityCode($e1.res, $relationalExpression.res, tok, tok);}
		)
	;

relationalExpression returns [Entity res]
@init{
IToken tok = null;
}
	:	shiftExpression						{$res = $shiftExpression.res;}
	|	{tok = CurrentToken();} 
		^(( LT | GT | LTE | GTE | INSTANCEOF | IN )
			r1=relationalExpression 			
			shiftExpression					{$res= new EntityCode($r1.res, $shiftExpression.res, tok, tok);}
		)
	;

shiftExpression returns [Entity res]
@init{
IToken tok = null;
}
	:	additiveExpression			{$res = $additiveExpression.res;}
	|	{tok = CurrentToken();} 
		^(	( SHL | SHR | SHU ) 
			s1=shiftExpression 		
			additiveExpression		{$res= new EntityCode($s1.res, $additiveExpression.res, tok, tok);}
		)
	;

additiveExpression returns [Entity res]
@init{
IToken tok = null;
}
	:	multiplicativeExpression		{$res = $multiplicativeExpression.res;}
	|	{tok =  CurrentToken();}
		^(	( ADD | SUB ) 
			a1=additiveExpression 		
			multiplicativeExpression	{$res= new EntityCode($a1.res, $multiplicativeExpression.res, tok, tok);}
		)
	;

multiplicativeExpression returns [Entity res]
@init{
IToken tok = null;
}
	:	unaryExpression				{$res = $unaryExpression.res;}
	|	{tok =  CurrentToken();}
		^(	( MUL | DIV | MOD ) 
			m1=multiplicativeExpression 	
			unaryExpression			{$res= new EntityCode($m1.res, $unaryExpression.res, tok, tok);}
		)
	;
	
unaryExpression returns [Entity res]
@init{
IToken tok = null;
}
	:	postfixExpression								{$res = $postfixExpression.res;}
	| 	{tok =  CurrentToken();}
		^( 	(DELETE | VOID | TYPEOF | INC | DEC | POS | NEG | INV | NOT) 
			u1=unaryExpression							{$res= new EntityCode($u1.res, tok);}
		)
	;

postfixExpression returns [Entity res]
@init{
IToken tok = null;
}
	:	leftHandSideExpression				{$res = $leftHandSideExpression.res;}
	|	{tok =  CurrentToken();}
		^((PINC | PDEC) leftHandSideExpression)		{$res= new EntityCode($leftHandSideExpression.res, tok);}
	;

leftHandSideExpression returns [Entity res]

	:	memberExpression					{$res = $memberExpression.res;}
	|	^(CALL l1=leftHandSideExpression arguments )		{$res = new EntityCall($l1.res, $arguments.res, $CALL.Token);}
	| 	^(BYINDEX l2=leftHandSideExpression expression )	{$res = new EntityCode($l2.res, $expression.res, $BYINDEX.Token, $BYINDEX.Token);}
	| 	^(BYFIELD l3=leftHandSideExpression Identifier )	{$res = new EntityCode($l3.res, $Identifier.Text);}
	;
	
arguments returns [EntityCode res]
@init
{
$res= new EntityCode();
IToken tok = null;
}
	: ^( 	ARGS_CALL 
		(	{tok = CurrentToken();} assignmentExpression			{$res.Add($assignmentExpression.res, tok);}
		)* 
	   )
	;

memberExpression returns [Entity res]
	: primaryExpression	{$res = $primaryExpression.res;}
	| functionExpression	{$res = $functionExpression.res;}
	| newExpression		{$res = $newExpression.res;}
	;

newExpression returns [EntityNew res]
	: ^(	NEW 
		(	primaryExpression 	{$res = new EntityNew($primaryExpression.res, $primaryExpression.start);}
		| 	functionExpression	{$res = new EntityNew($functionExpression.res, $primaryExpression.start);}
		)
	)
	;

primaryExpression returns [Entity res]
	: THIS				{$res= new EntityCode(0);}
	| Identifier			{$res= new EntityCode($Identifier.Text);}
	| literal			{$res= new EntityCode();}
	| arrayLiteral			{$res=$arrayLiteral.res;}
	| objectLiteral			{$res=$objectLiteral.res;}
	| ^( PAREXPR expression )	{$res=$expression.res;}
	;
	
literal
	: NULL
	| booleanLiteral
	| numericLiteral
	| StringLiteral
	| RegularExpressionLiteral
	;
	
booleanLiteral
	: TRUE
	| FALSE
	;
	
numericLiteral	returns [string res]
	: DecimalLiteral		{$res = $DecimalLiteral.Text;}
	| OctalIntegerLiteral		{$res = $OctalIntegerLiteral.Text;}
	| HexIntegerLiteral		{$res = $HexIntegerLiteral.Text;}
	;
	
arrayLiteral returns [EntityCode res]
@init
{
$res= new EntityCode();
}
	: ^( 	ARRAY 
		(	arrayItem	{$res.Add($arrayItem.res, $ARRAY.Token);}
		)* 
	)
	;
	
arrayItem returns [EntityCode res]
@init
{
$res= new EntityCode();
}
	: ^( 	ITEM 
		(	assignmentExpression	{$res.Add($assignmentExpression.res, $ITEM.Token);}
		)? 
	)
	;
	
objectLiteral returns [EntityObject res]
@init
{

}
	:^( 	OBJECT {$res= new EntityObject($OBJECT.Token);}
		//(	nameValuePair		{$res.Add($nameValuePair.res, $OBJECT.Token);} {$res = EntityVariable.AddEntityVariable($name.Text, $assignmentExpression.res, $name.Token, true, semantic);}
		(^( NAMEDVALUE propertyName COLON assignmentExpression ) {$res.Add(EntityVariable.AddEntityVariable($propertyName.res, $assignmentExpression.res, $COLON.Token, false, semantic));}
		)* 
	)
	;
	
//nameValuePair returns [Entity res]
//	: ^( NAMEDVALUE propertyName assignmentExpression )		{$res = new EntityObject($propertyName.res, $assignmentExpression.res);}
//	;
	
propertyName returns	[string res]
	: Identifier		{$res = $Identifier.Text;}
	| StringLiteral		{$res = $StringLiteral.Text;}
	| numericLiteral	{$res = $numericLiteral.res;}
	;
	
		