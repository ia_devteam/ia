using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.Windows.Forms;

using IA.Plugins.Parsers.CommonUtils;

using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.JAVASCRIPT_PARSER;

        /// <summary>
        /// имя плагина
        /// </summary>
        internal const string Name = "Парсер JavaScript";

        /// <summary>
        /// Задачи плагинов
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка файлов")]
            FILES_PROCESSING
        };

        #region Settings
        /// <summary>
        /// 2-й уровень вставки датчиков
        /// </summary>
        internal static bool IsLevel2;

        /// <summary>
        /// 3-й уровень вставки датчиков
        /// </summary>
        internal static bool IsLevel3;

        /// <summary>
        /// Не вставлять датчики
        /// </summary>
        internal static bool IsNoSensors;

        /// <summary>
        /// Текст функции датчика для 2-го уровня
        /// </summary>
        internal static string SensorFunctionText_Level2;

        /// <summary>
        /// Текст датчика для 2-го уровня
        /// </summary>
        internal static string SensorText_Level2;

        /// <summary>
        /// Текст функции датчика для 3-го уровня
        /// </summary>
        internal static string SensorFunctionText_Level3;

        /// <summary>
        /// Текст датчика для 3-го уровня
        /// </summary>
        internal static string SensorText_Level3;

        /// <summary>
        /// Номер первого датчика
        /// </summary>
        internal static ulong FirstSensorNumber;

        /// <summary>
        /// Пропуск ошибок
        /// </summary>
        internal static bool IsSkipErrors;
        #endregion

    }

    public class JavaScriptParser : IA.Plugin.Interface
    {	
        /// <summary>
        /// Текущее хранилище
        /// </summary>
		Storage storage;   

        /// <summary>
        /// Текущие настройки
        /// </summary>
        Settings settings;

        #region Члены PluginInterface
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS;
            }
        }

        /// <summary>
        /// Метод генерации отчетов для тестов
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до каталога с отчетами плагина</param>
        public void GenerateStatements(string reportsDirectoryPath)
        {
            (new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage)).Execute();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {            
            return;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            PluginSettings.IsLevel2 = Properties.Settings.Default.IsLevel2;
            PluginSettings.IsLevel3 = Properties.Settings.Default.IsLevel3;
            PluginSettings.IsNoSensors = Properties.Settings.Default.IsNoSensors;
            PluginSettings.SensorFunctionText_Level2 = Properties.Settings.Default.SensorFunctionText_Level2;
            PluginSettings.SensorText_Level2 = Properties.Settings.Default.SensorText_Level2;
            PluginSettings.SensorFunctionText_Level3 = Properties.Settings.Default.SensorFunctionText_Level3;
            PluginSettings.SensorText_Level3 = Properties.Settings.Default.SensorText_Level3;
            PluginSettings.FirstSensorNumber = Properties.Settings.Default.FirstSensorNumber;
            PluginSettings.IsSkipErrors = Properties.Settings.Default.IsSkipErrors;

            this.storage = storage;

        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.IsLevel2, ref PluginSettings.IsLevel3, ref PluginSettings.IsNoSensors);
            PluginSettings.FirstSensorNumber = settingsBuffer.GetUInt64();
            PluginSettings.SensorFunctionText_Level2 = settingsBuffer.GetString();
            PluginSettings.SensorText_Level2 = settingsBuffer.GetString();
            PluginSettings.SensorFunctionText_Level3 = settingsBuffer.GetString();
            PluginSettings.SensorText_Level3 = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.IsSkipErrors);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.IsNoSensors = false;
            PluginSettings.SensorFunctionText_Level2 = "function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open(\"GET\", \"http://localhost:8080/\"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} ";
            PluginSettings.SensorText_Level2 = "SensorRnt(#);";
            PluginSettings.SensorFunctionText_Level3 = String.Empty;
            PluginSettings.SensorText_Level3 = "var RNTfileWITHsensors_FileSystemObject, RNTfileWITHsensors_File; RNTfileWITHsensors_FileSystemObject = new ActiveXObject(\"Scripting.FileSystemObject\"); RNTfileWITHsensors_File = RNTfileWITHsensors_FileSystemObject.OpenTextFile(\"c:\\\\RNTSensorlog_JavaScript.log\", 8, true); RNTfileWITHsensors_File.WriteLine(#);RNTfileWITHsensors_File.Close();";
            PluginSettings.FirstSensorNumber = (storage != null && storage.sensors.EnumerateSensors().Any()) ? storage.sensors.EnumerateSensors().Max(s => s.ID) + 1 : 1;
            PluginSettings.IsSkipErrors = false;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "NDV:3":
                    PluginSettings.IsLevel3 = true;
                    PluginSettings.IsLevel2 = false;
                    break;
                case "NDV:2":
                    PluginSettings.IsLevel2 = true;
                    PluginSettings.IsLevel3 = false;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            JavaScriptMain javaScriptMain = new JavaScriptMain(storage);

            try
            {
                storage.sensors.SetSensorStartNumber(PluginSettings.FirstSensorNumber);
            }
            catch
            {
                ulong tmp_FirstSensorNumber = PluginSettings.FirstSensorNumber;
                PluginSettings.FirstSensorNumber = storage.sensors.SetSensorStartNumberAsMaximum();
                
                Monitor.Log.Warning(PluginSettings.Name, "Расстановка датчиков с номера <" + tmp_FirstSensorNumber + "> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <" + PluginSettings.FirstSensorNumber + ">.");
            }           

            javaScriptMain.Run();
        }


        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.IsLevel2 = PluginSettings.IsLevel2;
            Properties.Settings.Default.IsLevel3 = PluginSettings.IsLevel3;
            Properties.Settings.Default.IsNoSensors = PluginSettings.IsNoSensors;
            Properties.Settings.Default.FirstSensorNumber = PluginSettings.FirstSensorNumber;
            Properties.Settings.Default.IsSkipErrors = PluginSettings.IsSkipErrors;
            Properties.Settings.Default.SensorFunctionText_Level2 = PluginSettings.SensorFunctionText_Level2;
            Properties.Settings.Default.SensorText_Level2 = PluginSettings.SensorText_Level2;
            Properties.Settings.Default.SensorFunctionText_Level3 = PluginSettings.SensorFunctionText_Level3;
            Properties.Settings.Default.SensorText_Level3 = PluginSettings.SensorText_Level3;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.IsLevel2, PluginSettings.IsLevel3, PluginSettings.IsNoSensors);
            settingsBuffer.Add(PluginSettings.FirstSensorNumber);
            settingsBuffer.Add(PluginSettings.SensorFunctionText_Level2);
            settingsBuffer.Add(PluginSettings.SensorText_Level2);
            settingsBuffer.Add(PluginSettings.SensorFunctionText_Level3);
            settingsBuffer.Add(PluginSettings.SensorText_Level3);
            settingsBuffer.Add(PluginSettings.IsSkipErrors);
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description())
                };
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

		{
            get
            {
			    return null;
            }
		}		
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return settings = new Settings(
                    storage.sensors.EnumerateSensors().Count() == 0 
                    ? 1 
                    : storage.sensors.EnumerateSensors().Max(s => s.ID) + 1
                    );
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }
       
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();
            
            if (!(PluginSettings.IsLevel2 ? PluginSettings.SensorText_Level2 : PluginSettings.SensorText_Level3).Contains("#") && !PluginSettings.IsNoSensors)
            {
                message = "Текст датчика должен содержать символ #.";
                return false;
            }
                        
            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }
 
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        #endregion

    }
}
