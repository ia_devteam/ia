﻿namespace IA.Plugins.Parsers.JavaScriptParser
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sensorFunctionText_groupBox = new System.Windows.Forms.GroupBox();
            this.sensorFunctionText_textBox = new System.Windows.Forms.TextBox();
            this.sensorText_groupBox = new System.Windows.Forms.GroupBox();
            this.sensorText_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sensorText_textBox = new System.Windows.Forms.TextBox();
            this.level_groupBox = new System.Windows.Forms.GroupBox();
            this.level_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.level2_radioButton = new System.Windows.Forms.RadioButton();
            this.level0_radioButton = new System.Windows.Forms.RadioButton();
            this.level3_radioButton = new System.Windows.Forms.RadioButton();
            this.additionalSettings_groupBox = new System.Windows.Forms.GroupBox();
            this.additionalSettings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isSkipError_checkBox = new System.Windows.Forms.CheckBox();
            this.firstSensorNumber_groupBox = new System.Windows.Forms.GroupBox();
            this.firstSensorNumber_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.firstSensorNumber_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.sensorFunctionText_groupBox.SuspendLayout();
            this.sensorText_groupBox.SuspendLayout();
            this.sensorText_tableLayoutPanel.SuspendLayout();
            this.level_groupBox.SuspendLayout();
            this.level_tableLayoutPanel.SuspendLayout();
            this.additionalSettings_groupBox.SuspendLayout();
            this.additionalSettings_tableLayoutPanel.SuspendLayout();
            this.firstSensorNumber_groupBox.SuspendLayout();
            this.firstSensorNumber_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.sensorFunctionText_groupBox, 0, 3);
            this.settings_tableLayoutPanel.Controls.Add(this.sensorText_groupBox, 0, 2);
            this.settings_tableLayoutPanel.Controls.Add(this.level_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Controls.Add(this.additionalSettings_groupBox, 0, 4);
            this.settings_tableLayoutPanel.Controls.Add(this.firstSensorNumber_groupBox, 0, 1);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 5;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(445, 379);
            this.settings_tableLayoutPanel.TabIndex = 0;
            // 
            // sensorFunctionText_groupBox
            // 
            this.sensorFunctionText_groupBox.Controls.Add(this.sensorFunctionText_textBox);
            this.sensorFunctionText_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorFunctionText_groupBox.Location = new System.Drawing.Point(3, 163);
            this.sensorFunctionText_groupBox.Name = "sensorFunctionText_groupBox";
            this.sensorFunctionText_groupBox.Size = new System.Drawing.Size(439, 163);
            this.sensorFunctionText_groupBox.TabIndex = 9;
            this.sensorFunctionText_groupBox.TabStop = false;
            this.sensorFunctionText_groupBox.Text = "Текст функции датчика (вставляется в начало каждого JavaScript-файла):";
            // 
            // sensorFunctionText_textBox
            // 
            this.sensorFunctionText_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorFunctionText_textBox.Location = new System.Drawing.Point(3, 16);
            this.sensorFunctionText_textBox.Multiline = true;
            this.sensorFunctionText_textBox.Name = "sensorFunctionText_textBox";
            this.sensorFunctionText_textBox.Size = new System.Drawing.Size(433, 144);
            this.sensorFunctionText_textBox.TabIndex = 2;
            // 
            // sensorText_groupBox
            // 
            this.sensorText_groupBox.Controls.Add(this.sensorText_tableLayoutPanel);
            this.sensorText_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorText_groupBox.Location = new System.Drawing.Point(3, 108);
            this.sensorText_groupBox.Name = "sensorText_groupBox";
            this.sensorText_groupBox.Size = new System.Drawing.Size(439, 49);
            this.sensorText_groupBox.TabIndex = 8;
            this.sensorText_groupBox.TabStop = false;
            this.sensorText_groupBox.Text = "Текст датчика (вставляется в текст программы): ";
            // 
            // sensorText_tableLayoutPanel
            // 
            this.sensorText_tableLayoutPanel.ColumnCount = 1;
            this.sensorText_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.sensorText_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.sensorText_tableLayoutPanel.Controls.Add(this.sensorText_textBox, 0, 0);
            this.sensorText_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorText_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.sensorText_tableLayoutPanel.Name = "sensorText_tableLayoutPanel";
            this.sensorText_tableLayoutPanel.RowCount = 1;
            this.sensorText_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.sensorText_tableLayoutPanel.Size = new System.Drawing.Size(433, 30);
            this.sensorText_tableLayoutPanel.TabIndex = 1;
            // 
            // sensorText_textBox
            // 
            this.sensorText_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorText_textBox.Location = new System.Drawing.Point(3, 5);
            this.sensorText_textBox.Name = "sensorText_textBox";
            this.sensorText_textBox.Size = new System.Drawing.Size(427, 20);
            this.sensorText_textBox.TabIndex = 3;
            // 
            // level_groupBox
            // 
            this.level_groupBox.Controls.Add(this.level_tableLayoutPanel);
            this.level_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_groupBox.Location = new System.Drawing.Point(3, 3);
            this.level_groupBox.Name = "level_groupBox";
            this.level_groupBox.Size = new System.Drawing.Size(439, 44);
            this.level_groupBox.TabIndex = 3;
            this.level_groupBox.TabStop = false;
            this.level_groupBox.Text = "Расстановка датчиков";
            // 
            // level_tableLayoutPanel
            // 
            this.level_tableLayoutPanel.ColumnCount = 4;
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Controls.Add(this.level2_radioButton, 0, 0);
            this.level_tableLayoutPanel.Controls.Add(this.level0_radioButton, 2, 0);
            this.level_tableLayoutPanel.Controls.Add(this.level3_radioButton, 1, 0);
            this.level_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.level_tableLayoutPanel.Name = "level_tableLayoutPanel";
            this.level_tableLayoutPanel.RowCount = 1;
            this.level_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Size = new System.Drawing.Size(433, 25);
            this.level_tableLayoutPanel.TabIndex = 3;
            // 
            // level2_radioButton
            // 
            this.level2_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.level2_radioButton.AutoSize = true;
            this.level2_radioButton.Location = new System.Drawing.Point(3, 4);
            this.level2_radioButton.Name = "level2_radioButton";
            this.level2_radioButton.Size = new System.Drawing.Size(138, 17);
            this.level2_radioButton.TabIndex = 0;
            this.level2_radioButton.Text = "Второй уровень";
            this.level2_radioButton.UseVisualStyleBackColor = true;
            this.level2_radioButton.CheckedChanged += new System.EventHandler(this.level2_radioButton_CheckedChanged);
            // 
            // level0_radioButton
            // 
            this.level0_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.level0_radioButton.AutoSize = true;
            this.level0_radioButton.Location = new System.Drawing.Point(291, 4);
            this.level0_radioButton.Name = "level0_radioButton";
            this.level0_radioButton.Size = new System.Drawing.Size(138, 17);
            this.level0_radioButton.TabIndex = 2;
            this.level0_radioButton.Text = "Не вставлять датчики";
            this.level0_radioButton.UseVisualStyleBackColor = true;
            this.level0_radioButton.CheckedChanged += new System.EventHandler(this.level0_radioButton_CheckedChanged);
            // 
            // level3_radioButton
            // 
            this.level3_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.level3_radioButton.AutoSize = true;
            this.level3_radioButton.Location = new System.Drawing.Point(147, 4);
            this.level3_radioButton.Name = "level3_radioButton";
            this.level3_radioButton.Size = new System.Drawing.Size(138, 17);
            this.level3_radioButton.TabIndex = 1;
            this.level3_radioButton.Text = "Третий уровень";
            this.level3_radioButton.UseVisualStyleBackColor = true;
            this.level3_radioButton.CheckedChanged += new System.EventHandler(this.level3_radioButton_CheckedChanged);
            // 
            // additionalSettings_groupBox
            // 
            this.additionalSettings_groupBox.Controls.Add(this.additionalSettings_tableLayoutPanel);
            this.additionalSettings_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.additionalSettings_groupBox.Location = new System.Drawing.Point(3, 332);
            this.additionalSettings_groupBox.Name = "additionalSettings_groupBox";
            this.additionalSettings_groupBox.Size = new System.Drawing.Size(439, 44);
            this.additionalSettings_groupBox.TabIndex = 6;
            this.additionalSettings_groupBox.TabStop = false;
            this.additionalSettings_groupBox.Text = "Дополнительные параметры";
            // 
            // additionalSettings_tableLayoutPanel
            // 
            this.additionalSettings_tableLayoutPanel.ColumnCount = 1;
            this.additionalSettings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.additionalSettings_tableLayoutPanel.Controls.Add(this.isSkipError_checkBox, 0, 0);
            this.additionalSettings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.additionalSettings_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.additionalSettings_tableLayoutPanel.Name = "additionalSettings_tableLayoutPanel";
            this.additionalSettings_tableLayoutPanel.RowCount = 1;
            this.additionalSettings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.additionalSettings_tableLayoutPanel.Size = new System.Drawing.Size(433, 25);
            this.additionalSettings_tableLayoutPanel.TabIndex = 1;
            // 
            // isSkipError_checkBox
            // 
            this.isSkipError_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isSkipError_checkBox.AutoSize = true;
            this.isSkipError_checkBox.Location = new System.Drawing.Point(3, 4);
            this.isSkipError_checkBox.Name = "isSkipError_checkBox";
            this.isSkipError_checkBox.Size = new System.Drawing.Size(427, 17);
            this.isSkipError_checkBox.TabIndex = 0;
            this.isSkipError_checkBox.Text = "Продолжать работу при критических ошибках";
            this.isSkipError_checkBox.UseVisualStyleBackColor = true;
            // 
            // firstSensorNumber_groupBox
            // 
            this.firstSensorNumber_groupBox.Controls.Add(this.firstSensorNumber_tableLayoutPanel);
            this.firstSensorNumber_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstSensorNumber_groupBox.Location = new System.Drawing.Point(3, 53);
            this.firstSensorNumber_groupBox.Name = "firstSensorNumber_groupBox";
            this.firstSensorNumber_groupBox.Size = new System.Drawing.Size(439, 49);
            this.firstSensorNumber_groupBox.TabIndex = 7;
            this.firstSensorNumber_groupBox.TabStop = false;
            this.firstSensorNumber_groupBox.Text = "Номер первого датчика";
            // 
            // firstSensorNumber_tableLayoutPanel
            // 
            this.firstSensorNumber_tableLayoutPanel.ColumnCount = 1;
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.firstSensorNumber_tableLayoutPanel.Controls.Add(this.firstSensorNumber_numericUpDown, 0, 0);
            this.firstSensorNumber_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstSensorNumber_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.firstSensorNumber_tableLayoutPanel.Name = "firstSensorNumber_tableLayoutPanel";
            this.firstSensorNumber_tableLayoutPanel.RowCount = 1;
            this.firstSensorNumber_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.Size = new System.Drawing.Size(433, 30);
            this.firstSensorNumber_tableLayoutPanel.TabIndex = 0;
            // 
            // firstSensorNumber_numericUpDown
            // 
            this.firstSensorNumber_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_numericUpDown.Location = new System.Drawing.Point(3, 5);
            this.firstSensorNumber_numericUpDown.Name = "firstSensorNumber_numericUpDown";
            this.firstSensorNumber_numericUpDown.Size = new System.Drawing.Size(427, 20);
            this.firstSensorNumber_numericUpDown.TabIndex = 12;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(445, 379);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.sensorFunctionText_groupBox.ResumeLayout(false);
            this.sensorFunctionText_groupBox.PerformLayout();
            this.sensorText_groupBox.ResumeLayout(false);
            this.sensorText_tableLayoutPanel.ResumeLayout(false);
            this.sensorText_tableLayoutPanel.PerformLayout();
            this.level_groupBox.ResumeLayout(false);
            this.level_tableLayoutPanel.ResumeLayout(false);
            this.level_tableLayoutPanel.PerformLayout();
            this.additionalSettings_groupBox.ResumeLayout(false);
            this.additionalSettings_tableLayoutPanel.ResumeLayout(false);
            this.additionalSettings_tableLayoutPanel.PerformLayout();
            this.firstSensorNumber_groupBox.ResumeLayout(false);
            this.firstSensorNumber_tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox level_groupBox;
        private System.Windows.Forms.RadioButton level3_radioButton;
        private System.Windows.Forms.RadioButton level2_radioButton;
        private System.Windows.Forms.NumericUpDown firstSensorNumber_numericUpDown;
        private System.Windows.Forms.TextBox sensorFunctionText_textBox;
        private System.Windows.Forms.TextBox sensorText_textBox;
        private System.Windows.Forms.RadioButton level0_radioButton;
        private System.Windows.Forms.GroupBox additionalSettings_groupBox;
        private System.Windows.Forms.CheckBox isSkipError_checkBox;
        private System.Windows.Forms.TableLayoutPanel level_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel additionalSettings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox sensorFunctionText_groupBox;
        private System.Windows.Forms.GroupBox sensorText_groupBox;
        private System.Windows.Forms.GroupBox firstSensorNumber_groupBox;
        private System.Windows.Forms.TableLayoutPanel firstSensorNumber_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel sensorText_tableLayoutPanel;
    }
}
