﻿using System;
using System.Collections.Generic;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    /// <summary>
    /// Вершина промежуточного представления, соответствующая оператору new.
    /// </summary>
    public class EntityNew : Entity
    {
        /// <summary>
        /// Аргумент оператора new
        /// </summary>
        internal Entity ent;

        internal override int[] getPos()
        {
            if (ent != null)
                return ent.getPos();
            else
                return new int[] { (int)0, (int)0 };
        }

        public EntityNew(Entity ent, Antlr.Runtime.Tree.CommonTree token)
            : base(token)
        {
            this.ent = ent;

            //if (ent is EntityFunction)
            //{
            //    ReportUseConstructor(ent as EntityFunction);
            //    return;
            //}

            //List<string> name = ent.Identifier();
            //if (name != null)
            //    ReportCreateForName(name);

            //if (ent is EntityCode)
            //{
            //    List<EntityObject> list = (ent as EntityCode).ObjectList();
            //    if(list != null)
            //        ReportCreateObjects(list);
            //}
        }

        //private void ReportCreateObjects(List<EntityObject> list)
        //{
        //    throw new NotImplementedException();
        //}

        private void ReportCreateForName(List<string> name, SemanticTables semantic)
        {
            if (name.Count == 1)
                if (name[0] == "Function")  //Функцию можно определить при помощи строк.
                                            //Например, var square = new Function("x", "return x*x;");
                                            //Такие ситуации пропускаем, и сообщаем пользователю о пропуске.
                {
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружено определение функции через конструктор new Function(). Определение пропущено. " + entityPlaceInCode(semantic));
                    return;
                }


            throw new NotImplementedException();
        }

        private void ReportUseConstructor(EntityFunction entityFunction)
        {
            throw new NotImplementedException();
        }

        internal override void Pass1(SemanticTables semantic)
        {
            ent.Pass1(semantic);
        }

        internal override void Pass2(SemanticTables semantic)
        {
            ent.Pass2(semantic);
        }

        internal override void Pass3(SemanticTables semantic)
        {
            ent.Pass3(semantic);
        }
    }
}
