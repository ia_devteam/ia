﻿using System;
using System.Collections.Generic;
using Store;

namespace IA.Plugins.Parsers.JavaScriptParser
{

    internal class SensorPlace
    {
        internal UInt64 function;
        internal UInt64 file;
        internal UInt64 Line;
        internal UInt64 Column;
    }

    /// <summary>
    /// Хранит и обрабатывает таблицы, использующиеся при семантическом анализе.
    /// </summary>
    public class SemanticTables
    {
        /// <summary>
        /// Полное имя файла, анализ которого в настоящий момент идет.
        /// </summary>
        internal string fileName;

        internal UInt64 fileID;
        internal IFile file;

        /// <summary>
        /// Хранилище для записи результатов
        /// </summary>
        internal Storage storage;

        /// <summary>
        /// Будем ли мы добавлять датчики
        /// </summary>
        internal bool isAddSensors;
        /// <summary>
        /// Раздел storage
        /// </summary>
        internal Functions functions;
        /// <summary>
        /// Раздел storage
        /// </summary>
        internal Variables variables;
        /// <summary>
        /// Раздел storage
        /// </summary>
        internal Statements statements;
        /// <summary>
        /// Раздел storage
        /// </summary>
        internal Operations operations;

        /// <summary>
        /// Текущее пространство имен. Используется в процессе обхода.
        /// </summary>
        internal EntityFunction currentScope;
        /// <summary>
        /// Глобальное пространство имен
        /// </summary>
        internal EntityFunction globalScope;

        /// <summary>
        /// Перечень мест, в которые следует вставить датчики
        /// </summary>
        internal List<SensorPlace> sensorPlaces = new List<SensorPlace>();

        /// <summary>
        /// Указывает на функцию при нулевом проходе в момент, когда мы уже прошли ее заголовок,
        /// но не прошли открывающую скобку. Используется для сбора информации о местах,
        /// в которые следует расставлять датчики.
        /// </summary>
        internal IFunction functionForSensor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="isAddSensors">Будем ли мы добавлять датчики</param>
        internal SemanticTables(Storage storage, bool isAddSensors)
        {
            this.storage = storage;
            this.isAddSensors = isAddSensors;

            functions = storage.functions;
            variables = storage.variables;
            statements = storage.statements;
            operations = storage.operations;

            globalScope = new EntityFunction(this);
            currentScope = globalScope;
        }


        internal void Setup(IFile file)
        {
            this.fileName = file.FullFileName_Original;
            this.fileID = file.Id;
            this.file = file;
        }

        /// <summary>
        /// Переводит полное имя, запомненное в виде списка, в единую строку с разделителем-точкой.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        internal void ListToName(List<string> list, out string name, out bool isGlobal, SemanticTables semantic)
        {
            //System.Text.StringBuilder builder = new System.Text.StringBuilder();
            //bool is_dot = false;
            //foreach (string str in list)
            //{
            //    if (is_dot)
            //        builder.Append('.');
            //    else
            //        is_dot = true;

            //    builder.Append(str);
            //}

            //return builder.ToString();
            if (list.Contains("this"))
                list.Remove("this");
            if(list.Contains("prototype"))
                list.Remove("prototype");
            name = "";
            isGlobal = false;
            if (list.Count != 0)
            {
                //Уничтожаем всю работу с составными именами
                name = list[list.Count - 1];
                if(semantic.currentScope == semantic.globalScope || semantic.currentScope.ContainsVariable(name) == null)
                    isGlobal = true; //на самом деле мы не знаем так, глобальная или нет! ибо объекты!
                //isGlobal = list.Count != 1;
                
            }
        }

    }
}
