﻿using System.Collections.Generic;
using System;
using Store;
using System.IO;
using FileOperations;
using System.Linq;
using IOController;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    class SensorInserter
    {
        readonly Storage storage;
        readonly string oldPrefix;
        readonly string newPrefix;
        readonly List<Xebic.Parsers.ES3.ES3Parser.begend> begend;
        readonly string sensorText;
        private bool jsExtracted;
        private int level = 2;

        public SensorInserter(Storage storage, string oldPrefix, string newPrefix, bool jsExtracted, bool isSecond, List<Xebic.Parsers.ES3.ES3Parser.begend> begend)
        {
            this.storage = storage;
            this.oldPrefix = DirectoryController.CanonizePath(oldPrefix);
            this.newPrefix = DirectoryController.CanonizePath(newPrefix);
            this.begend = begend;
            this.jsExtracted = jsExtracted;
            this.level = isSecond ? 2 : 3;
            this.sensorText = ((PluginSettings.IsLevel2 ? PluginSettings.SensorText_Level2 : PluginSettings.SensorText_Level3) + ";").Replace(";;", ";");
        }

        /// <summary>
        /// Метод проверяет семантическую корректность лабораторных исходных текстов
        /// </summary>
        /// <param name="contents">Текст с лабораторными исходниками</param>
        /// <param name="fs">Файловый дескриптор</param>
        void checkResult(string contents, AbstractReader fs)
        {
            Xebic.Parsers.ES3.ES3Parser.program_return ret = null;
            Xebic.Parsers.ES3.ES3Parser parser = null;
            try
            {
                Antlr.Runtime.ANTLRStringStream stream = new Antlr.Runtime.ANTLRStringStream(contents);
                Xebic.Parsers.ES3.ES3Lexer lexer = new Xebic.Parsers.ES3.ES3Lexer(stream);
                parser = new Xebic.Parsers.ES3.ES3Parser(new Antlr.Runtime.CommonTokenStream(lexer));
                ret = parser.program();
            }
            catch (Exception ex)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось разобрать файл с датчиками " + Files.CanonizeFileName(fs.getPath()).Replace(oldPrefix, newPrefix));
            }
            finally
            {
                if(parser != null && parser.NumberOfSyntaxErrors > 0)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, "Обнаружены ошибки в лабораторных исходных текстах" + Files.CanonizeFileName(fs.getPath()).Replace(oldPrefix, newPrefix));
                }
            }
        }

        void processFile(string contents, AbstractReader fs, string directoryLab = "")
        {
            string newName = Files.CanonizeFileName(fs.getPath()).Replace(oldPrefix, newPrefix);

            //создаем необходимую папку
            string directory = newName.Substring(0, newName.LastIndexOf("\\"));
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            if(!jsExtracted)
                fs.writeTextToFile(newName, (PluginSettings.IsLevel2 ? PluginSettings.SensorFunctionText_Level2 : PluginSettings.SensorFunctionText_Level3) + contents);
            else
                fs.writeTextToFile(Path.Combine(directoryLab, newName.Substring(newName.LastIndexOf("\\")+1)), contents);

            checkResult(contents, fs);
        }

        public void StatementWalk(IStatement st, ulong id)
        {
            if (level == 2)
                StatementWalk2(st, id);
            else
                StatementWalk3(st, id, 1);
        }
        public void StatementWalk2(IStatement st, ulong id)
        {
            int i = 0;
            if (st.AboveStatement != null) i = 1;
            do
            {
                Location stLoc = st.FirstSymbolLocation;
                if (begend.Where(g => g.startLine == (int)stLoc.GetLine() && g.startCol == (int)stLoc.GetColumn()).Count() == 1)
                {
                    Xebic.Parsers.ES3.ES3Parser.begend loc = begend.First(g => g.startLine == (int)stLoc.GetLine() && g.startCol == (int)stLoc.GetColumn());
                    st.SetLastSymbolLocation(stLoc.GetFile(), (ulong)loc.endLine, (ulong)loc.endCol);
                }
                if (stLoc.GetFileID() != id)
                    continue;

                switch (st.Kind)
                {
                    case ENStatementKind.STDoAndWhile:
                        {
                            IStatementDoAndWhile s = (IStatementDoAndWhile)st;
                            //ставим датчик перед ним
                            if (s.SensorBeforeTheStatement == null)
                            {
                                storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                if (s.Body != null)
                                    StatementWalk2(s.Body, id);
                            }
                            break;
                        }
                    case ENStatementKind.STFor:
                        {
                            IStatementFor s = (IStatementFor)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                if (s.Body != null)
                                    StatementWalk2(s.Body, id);
                            }
                            break;
                        }
                    case ENStatementKind.STIf:
                        {
                            IStatementIf s = (IStatementIf)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                if (s.ThenStatement != null)
                                    StatementWalk2(s.ThenStatement, id);
                                if (s.ElseStatement != null)
                                    StatementWalk2(s.ElseStatement, id);
                            }
                            break;
                        }
                    case ENStatementKind.STOperational:
                        {
                            IStatementOperational s = (IStatementOperational)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                //if (s.Operation == null && s.NextInLinearBlock == null)
                                //{
                                    if (s.AboveStatement == null && s.NextInLinearBlock == null)
                                        storage.sensors.AddSensorBeforeStatement(s, Kind.FINAL);
                                    else
                                        storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                    if (s.NextInLinearBlock == null)
                                        return;
                                //}
                                //else
                                //{
                                //    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                //}                                    
                            }                                
                            break;
                        }
                    case ENStatementKind.STSwitch:
                        {
                            IStatementSwitch s = (IStatementSwitch)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                foreach (KeyValuePair<IOperation, IStatement> kvp in s.Cases())
                                    if (kvp.Value != null && !(kvp.Value is IStatementOperational && (kvp.Value as IStatementOperational).Operation == null))
                                        StatementWalk2(kvp.Value, id);
                                if (s.DefaultCase() != null)
                                    StatementWalk2(s.DefaultCase(), id);
                            }
                            break;
                        }
                    case ENStatementKind.STTryCatchFinally:
                        {
                            IStatementTryCatchFinally s = (IStatementTryCatchFinally)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                if (s.TryBlock != null)
                                    StatementWalk2(s.TryBlock, id);
                                foreach (IStatement cat in s.Catches())
                                    if (cat != null)
                                        StatementWalk2(cat, id);
                                if (s.FinallyBlock != null)
                                    StatementWalk2(s.FinallyBlock, id);
                            }
                            break;
                        }
                    case ENStatementKind.STUsing:
                        {
                            IStatementUsing s = (IStatementUsing)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                if (s.Body != null)
                                    StatementWalk2(s.Body, id);
                            }
                            break;
                        }
                    case ENStatementKind.STContinue:
                        {
                            IStatementContinue s = (IStatementContinue)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                s.ContinuingLoop = resolveLoop(s);
                                storage.sensors.AddSensorBeforeStatement(st, (i == 0) ? Kind.START : Kind.INTERNAL);
                                return;
                            }
                            break;
                        }
                    case ENStatementKind.STBreak:
                        {
                            IStatementBreak s = (IStatementBreak)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                s.BreakingLoop = resolveLoop(s);
                                storage.sensors.AddSensorBeforeStatement(st, (i == 0) ? Kind.START : Kind.INTERNAL);
                                return;
                            }
                            break;
                        }
                    case ENStatementKind.STReturn:
                    case ENStatementKind.STThrow:
                        {
                            if (st.SensorBeforeTheStatement == null)
                            {
                                storage.sensors.AddSensorBeforeStatement(st, (i == 0) ? Kind.START : Kind.FINAL);
                                return;
                            }
                            break;
                        }
                }
                i++;
                if(st.NextInLinearBlock == null && st.FirstSymbolLocation.GetFileID() == id)
                {
                    if(st.LastSymbolLocation != null)
                        st.NextInLinearBlock = storage.statements.AddStatementOperational(st.LastSymbolLocation.GetLine(), st.LastSymbolLocation.GetColumn() + 100500, st.FirstSymbolLocation.GetFile());
                    else
                        st.NextInLinearBlock = storage.statements.AddStatementOperational(st.FirstSymbolLocation.GetLine(), st.FirstSymbolLocation.GetColumn() + 100500, st.FirstSymbolLocation.GetFile());

                    if(st.AboveStatement != null)
                        storage.sensors.AddSensorBeforeStatement(st.NextInLinearBlock, Kind.INTERNAL);
                    else
                        storage.sensors.AddSensorBeforeStatement(st.NextInLinearBlock, Kind.FINAL);
                    return;
                }
            } while ((st = st.NextInLinearBlock) != null);
        }

        public void StatementWalk3(IStatement st, ulong id, int level)
        {
            int i = 0;
            if (st.AboveStatement != null) i = 1;
            bool metSens = !(level == 1);
            do
            {
                Location stLoc = st.FirstSymbolLocation;
                if (begend.Where(g => g.startLine == (int)stLoc.GetLine() && g.startCol == (int)stLoc.GetColumn()).Count() == 1)
                {
                    Xebic.Parsers.ES3.ES3Parser.begend loc = begend.First(g => g.startLine == (int)stLoc.GetLine() && g.startCol == (int)stLoc.GetColumn());
                    st.SetLastSymbolLocation(stLoc.GetFile(), (ulong)loc.endLine, (ulong)loc.endCol);
                }

                if (stLoc.GetFileID() != id)
                    continue;

                switch (st.Kind)
                {
                    case ENStatementKind.STDoAndWhile:
                        {
                            IStatementDoAndWhile s = (IStatementDoAndWhile)st;
                            //ставим датчик перед ним
                            if (s.SensorBeforeTheStatement == null)
                            {
                                if(!metSens)
                                    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                metSens = true;
                                if (s.Body != null)
                                    StatementWalk3(s.Body, id, level + 1);
                            }
                            break;
                        }
                    case ENStatementKind.STFor:
                        {
                            IStatementFor s = (IStatementFor)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                if (!metSens)
                                    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                metSens = true;
                                if (s.Body != null)
                                    StatementWalk3(s.Body, id, level + 1);
                            }
                            break;
                        }
                    case ENStatementKind.STIf:
                        {
                            IStatementIf s = (IStatementIf)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                if (!metSens)
                                    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                metSens = true;
                                if (s.ThenStatement != null)
                                    StatementWalk3(s.ThenStatement, id, level + 1);
                                if (s.ElseStatement != null)
                                    StatementWalk3(s.ElseStatement, id, level + 1);
                            }
                            break;
                        }
                    case ENStatementKind.STOperational:
                        {
                            IStatementOperational s = (IStatementOperational)st;
                            if (s.SensorBeforeTheStatement == null)
                            {

                                if (/*s.Operation == null &&*/ s.NextInLinearBlock == null)
                                {
                                    if (s.AboveStatement == null)
                                        storage.sensors.AddSensorBeforeStatement(s, Kind.FINAL);
                                    return;
                                }
                                else
                                {
                                    if (!metSens)
                                        storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                }   


                                //if (!metSens)
                                //    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                //else if(level == 1 && s.NextInLinearBlock == null)
                                //{
                                //    storage.sensors.AddSensorBeforeStatement(s, Kind.FINAL);
                                //}
                                metSens = true;
                            }
                            break;
                        }
                    case ENStatementKind.STSwitch:
                        {
                            IStatementSwitch s = (IStatementSwitch)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                if (!metSens)
                                    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                metSens = true;
                                foreach (KeyValuePair<IOperation, IStatement> kvp in s.Cases())
                                    if (kvp.Value != null)
                                        StatementWalk3(kvp.Value, id, level + 1);
                                if (s.DefaultCase() != null)
                                    StatementWalk3(s.DefaultCase(), id, level + 1);
                            }
                            break;
                        }
                    case ENStatementKind.STTryCatchFinally:
                        {
                            IStatementTryCatchFinally s = (IStatementTryCatchFinally)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                if (!metSens)
                                    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                metSens = true;
                                if (s.TryBlock != null)
                                    StatementWalk3(s.TryBlock, id, level + 1);
                                foreach (IStatement cat in s.Catches())
                                    if (cat != null)
                                        StatementWalk3(cat, id, level + 1);
                                if (s.FinallyBlock != null)
                                    StatementWalk3(s.FinallyBlock, id, level + 1);
                            }
                            break;
                        }
                    case ENStatementKind.STUsing:
                        {
                            IStatementUsing s = (IStatementUsing)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                if (!metSens)
                                    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                metSens = true;
                                if (s.Body != null)
                                    StatementWalk3(s.Body, id, level + 1);
                            }
                            break;
                        }
                    case ENStatementKind.STContinue:
                        {
                            IStatementContinue s = (IStatementContinue)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                s.ContinuingLoop = resolveLoop(s);
                                if (!metSens)
                                    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                metSens = true;
                            }
                            return;
                            break;

                        }
                    case ENStatementKind.STBreak:
                        {
                            IStatementBreak s = (IStatementBreak)st;
                            if (s.SensorBeforeTheStatement == null)
                            {
                                s.BreakingLoop = resolveLoop(s);
                                if (!metSens)
                                    storage.sensors.AddSensorBeforeStatement(s, (i == 0) ? Kind.START : Kind.INTERNAL);
                                metSens = true;
                            }
                            return;
                            break;
                        }
                    case ENStatementKind.STReturn:
                    case ENStatementKind.STThrow:
                        {
                            if (st.SensorBeforeTheStatement == null)
                            {
                                storage.sensors.AddSensorBeforeStatement(st, Kind.FINAL);
                            }
                            return;
                            break;
                        }
                }
                if (st.NextInLinearBlock == null && st.AboveStatement == null && st.FirstSymbolLocation.GetFileID() == id)
                {
                    if (st.LastSymbolLocation != null)
                        st.NextInLinearBlock = storage.statements.AddStatementOperational(st.LastSymbolLocation.GetLine(), st.LastSymbolLocation.GetColumn() + 100500, st.FirstSymbolLocation.GetFile());
                    else
                        st.NextInLinearBlock = storage.statements.AddStatementOperational(st.FirstSymbolLocation.GetLine(), st.FirstSymbolLocation.GetColumn() + 100500, st.FirstSymbolLocation.GetFile());

                    storage.sensors.AddSensorBeforeStatement(st.NextInLinearBlock, Kind.FINAL);
                    return;
                }
                i++;
            } while ((st = st.NextInLinearBlock) != null);
        }

        static List<Sensor> skip = new List<Sensor>();

        public void SensorWalk(IFile currentFile, ref string contents, Dictionary<Xebic.Parsers.ES3.ES3Parser.sensor, string> places, AbstractReader fs, string directoryLab = "")
        {
            //Оптимизация. индексируем ключи places.
            Dictionary<int, List<Xebic.Parsers.ES3.ES3Parser.sensor>> placesIndex = new Dictionary<int, List<Xebic.Parsers.ES3.ES3Parser.sensor>>();
            foreach(Xebic.Parsers.ES3.ES3Parser.sensor sensor in places.Keys)
            {
                //Удостоверяемся в инициализированности ячейки
                List<Xebic.Parsers.ES3.ES3Parser.sensor> list;
                if (!placesIndex.TryGetValue(sensor.line, out list))
                {
                    list = new List<Xebic.Parsers.ES3.ES3Parser.sensor>();
                    placesIndex.Add(sensor.line, list);
                }

                //Добавляем элемент в индекс
                list.Add(sensor);
            }

            //места для датчиков, которые не были использованы
            HashSet<string> toRemove = new HashSet<string>();
            toRemove.UnionWith(places.Values);
            contents = contents.Replace("RNTRETRET;;", "RNTRETRET;");
            foreach (Sensor sen in storage.sensors.EnumerateSensors())
            {
                if (skip.Contains(sen))
                    continue;

                if (sen.GetBeforeStatement() != null && sen.GetBeforeStatement().FirstSymbolLocation != null && sen.GetBeforeStatement().FirstSymbolLocation.GetFileID() == currentFile.Id)
                {
                    //обрабатываем этот датчик - он нам интересен
                    int sensLine = (int)sen.GetBeforeStatement().FirstSymbolLocation.GetLine();
                    int sensCol = (int)sen.GetBeforeStatement().FirstSymbolLocation.GetColumn();

                    //когда нам не известно реальное положение поставленной скобки - ее положение равно 100500+положение последнего известного символа
                    if (sensCol > 100500 && sen.GetBeforeStatement().AboveStatement != null && sen.GetBeforeStatement().AboveStatement.LastSymbolLocation != null && sen.GetBeforeStatement().AboveStatement.LastSymbolLocation.GetLine() == (ulong)sensLine)
                    {
                        sensCol = (int)sen.GetBeforeStatement().AboveStatement.LastSymbolLocation.GetColumn() - 1;
                    }
                    //когда вся иерархия стетментов известна - мы выравниваем смещение датчика по верхнему или предыдущему стейтменту
                    if (sensCol > 100500 && sen.GetBeforeStatement().PreviousInLinearBlock != null && sen.GetBeforeStatement().PreviousInLinearBlock.LastSymbolLocation != null && sen.GetBeforeStatement().PreviousInLinearBlock.LastSymbolLocation.GetLine() == (ulong)sensLine)
                    {
                        sensCol = (int)sen.GetBeforeStatement().PreviousInLinearBlock.LastSymbolLocation.GetColumn() + 1;
                    }
                    //если ни одно из этих правил не сработало - оставляем как последний символ + 1
                    if (sensCol > 100500) sensCol -= 100499;
                    Xebic.Parsers.ES3.ES3Parser.sensor bestKey = null;
                    List<Xebic.Parsers.ES3.ES3Parser.sensor> ar;
                    if (placesIndex.TryGetValue(sensLine, out ar)) //Если есть значение, находящее ся в данной строке
                    {
                        if (ar.Count == 1)
                            bestKey = ar[0];
                        else if (ar.Count > 1)
                            bestKey = ar.Aggregate((b, plc) => (plc.col <= sensCol + 1 && (b.col < plc.col || b.col > sensCol + 1 ) ) ? plc : b); // ar.FirstOrDefault(g => { int col = g.col; return (col == sensCol + 1 || col == sensCol - 1 || col == sensCol); });
                    }
                    else
                    //if (bestKey == null)
                    {
                        List<Xebic.Parsers.ES3.ES3Parser.sensor> list = null;
                        //if (placesIndex.TryGetValue(sensLine, out list)) //Если есть хоть одно указание ставить датчик на этой строке
                        //    bestKey = ;
                        //else //Если для данной строки датчиков не нашлось
                        //{ //Пытаемся найти датчик на предыдущих строках
                            for(int i=sensLine+1; i>=0; i--) 
                                if(placesIndex.TryGetValue(i, out list))
                                    break;

                            if (list != null)
                                bestKey = list.Aggregate((b, plc) => (b.col < plc.col) ?  b : plc);
                        //}
                    }

                    if (bestKey != null)
                    {
                        string st = places[bestKey];
//                        places.Remove(bestKey);
                        toRemove.Remove(st);

                        //Очищаем индекс
                        var list = placesIndex[bestKey.line];
                        list.Remove(bestKey);
                        if (list.Count == 0)
                            placesIndex.Remove(bestKey.line);
                        //проверяем символ до вставки датчика
                        var insertIndex = contents.IndexOf(st);
                        if (insertIndex > 0) 
                        {
                            char ch = contents[insertIndex - 1];
                            if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || ch == '(' || ch == ')' || ch == '}')
                            {
                                contents = contents.Insert(insertIndex, ";");
                            }
                        }
                        
                        //Обрабатываем датчик
                        contents = contents.Replace(st, sensorText.Replace("#", sen.ID.ToString()));
                        skip.Add(sen);
                    }
                    else
                    {
                        throw new System.Exception("Sensor placement not found");
                    }
                }
            }

            foreach (string st in toRemove)
                contents = contents.Replace(st, "");
            if(directoryLab == "")
                processFile(contents, fs);
            else
                processFile(contents, fs, directoryLab);
        }

        IStatement resolveLoop(IStatement s)
        {
            if (s == null)
                return null;
            switch (s.Kind)
            {
                case ENStatementKind.STDoAndWhile:
                case ENStatementKind.STFor:
                case ENStatementKind.STForEach:
                    return s;
                default:
                    return resolveLoop(s.AboveStatement);
            }
        }

    }
}
