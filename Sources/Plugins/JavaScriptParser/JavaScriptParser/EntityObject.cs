﻿using System;
using System.Collections.Generic;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    /// <summary>
    /// Вершина промежуточного представления, соответствующая явному определению объекта.
    /// </summary>
    public class EntityObject : Entity
    {
        List<Entity> fields;
        /// <summary>
        /// Координаты кода соответствующего данной вершине
        /// </summary>
        UInt64 Line;
        UInt64 Column;

        internal override int[] getPos()
        {
            /*if (asignment != null)
                return asignment.getPos();
            else*/
                return new int[] { (int)Line, (int)Column };
        }
        /*
        public EntityObject(string name, Entity asignment)
        {
            this.name = name;
            this.asignment = asignment;
        }*/

        public EntityObject(Antlr.Runtime.IToken token)
        {
            Line = (ulong)token.Line;
            Column = (ulong)token.CharPositionInLine + 1;
            fields = new List<Entity>();
        }

        public void Add(Entity en)
        {
            fields.Add(en);
        }

        /*internal override void Pass0(SemanticTables semantic)
        {
            foreach (Entity en in fields.Values)
                en.Pass0(semantic);
        }*/

        internal override void Pass1(SemanticTables semantic)
        {
            foreach (Entity en in fields)
                en.Pass1(semantic);
            //asignment.Pass1(semantic);
        }

        internal override void Pass2(SemanticTables semantic)
        {
            foreach (Entity en in fields)
                en.Pass2(semantic);
            //asignment.Pass2(semantic);
        }

        internal override void Pass3(SemanticTables semantic)
        {
            foreach (Entity en in fields)
                en.Pass3(semantic);
            //asignment.Pass3(semantic);
        }
    }
}
