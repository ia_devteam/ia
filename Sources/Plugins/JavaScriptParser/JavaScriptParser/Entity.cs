﻿using System.Collections.Generic;

namespace IA.Plugins.Parsers.JavaScriptParser 
{
    /// <summary>
    /// Бызовый класс для строящегося по AST-дереву собственного представления.
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Номер строки в исходнике, соответствующий данной вершине.
        /// </summary>
        int line;

        protected Entity()
        {
        }

        protected Entity(Antlr.Runtime.Tree.CommonTree token)
        {
            if(token != null)
                line = token.Line;
        }

        protected string entityPlaceInCode(SemanticTables semantic)
        {
            return "Файл " + semantic.fileName + " позиция " + line;
        }

        /// <summary>
        /// Накапливаемое полное имя вида n1.n2.n3
        /// Все, что содержится в этой вершине и ее подчиненных. 
        /// Если это поле != null, то в вершине содержится полный идентификатор
        /// </summary>
        virtual internal List<string> Identifier()
        {
            return null;
        }

        abstract internal void Pass1(SemanticTables semantic);
        abstract internal void Pass2(SemanticTables semantic);
        abstract internal void Pass3(SemanticTables semantic);
        abstract internal int[] getPos();
    }
}
