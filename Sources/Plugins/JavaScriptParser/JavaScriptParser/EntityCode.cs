﻿using System;
using System.Collections.Generic;
using System.Linq;
using Store;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    public enum TypeCode
    {
        //if
        IF = ES3Tree.IF,
        //for
        FORITER = ES3Tree.FORITER,
        FORSTEP = ES3Tree.FORSTEP,
        //do_while
        DO = ES3Tree.DO,
        WHILE = ES3Tree.WHILE,
        FOR = ES3Tree.FOR,
        //break
        BREAK = ES3Tree.BREAK,
        //continue
        CONTINUE = ES3Tree.CONTINUE,
        //return
        RETURN = ES3Tree.RETURN,
        //with
        WITH = ES3Tree.WITH,
        //switch
        SWITCH = ES3Tree.SWITCH,
        CASE = ES3Tree.CASE,
        CASEST = ES3Tree.CASEST,
        DEFAULT = ES3Tree.DEFAULT,
        //try-catch
        TRY = ES3Tree.TRY,
        CATCH = ES3Tree.CATCH,
        FINALLY = ES3Tree.FINALLY,
        //throw
        THROW = ES3Tree.THROW
    };

    /// <summary>
    /// Вершина промежуточного представления, соответствующая некоторому оператору внутри функции.
    /// Оператор для анализа значения не имеет.
    /// </summary>
    public class EntityCode : Entity
    {
        /// <summary>
        /// Подчиненные вершины.
        /// </summary>
        Entity[] childs;

        /// <summary>
        /// Типы подчиненных вершин.
        /// </summary>
        TypeCode[] childsType;

        /// <summary>
        /// Массивы строк и столбцов расположения кода.
        /// </summary>
        UInt64[] lines;
        UInt64[] columns;

        /// <summary>
        /// Накапливаемое полное имя вида n1.n2.n3
        /// Все, что содержится в этой вершине и ее подчиненных. 
        /// Если это поле != null, то childs == null
        /// </summary>
        readonly List<string> name;

        /// <summary>
        /// Тип текущей вершины EntityCode
        /// </summary>
        TypeCode thisType;
            
        /// <summary>
        /// true в случае, если данная вершина является this. Все остальные поля класса будут null.
        /// </summary>
        readonly bool isThis;

        /// <summary>
        /// Координаты кода соответствующего данной вершине
        /// </summary>
        UInt64 Line;
        UInt64 Column;

        internal override int[] getPos()
        {
            return lines != null ? new[] { (int)lines[0], (int)columns[0] } : new[] { (int)Line, (int)Column };
        }
        
        internal EntityCode(Antlr.Runtime.IToken token)
        {
            Line = (UInt64)token.Line;
            Column = (UInt64)token.CharPositionInLine + 1;
            thisType = (TypeCode)token.Type;
        }

        internal EntityCode(Entity st, Antlr.Runtime.IToken token)
        {
            NewEntityCode(st, token);
        }

        private void NewEntityCode(Entity st, Antlr.Runtime.IToken token)
        {
            if (st == null)
                return;

            Line = (UInt64)token.Line;
            Column = (UInt64)token.CharPositionInLine + 1;
            childs = new[] { st };
            childsType = new[] { (TypeCode)token.Type };
            lines = new[] { (UInt64)token.Line };
            columns = new[] { (UInt64)token.CharPositionInLine + 1 };
            thisType = (TypeCode)token.Type;
        }
        
        internal EntityCode(Entity st1, Entity st2, Antlr.Runtime.IToken token1, Antlr.Runtime.IToken token2 )
        {
            NewEntityCode(st1, st2, token1, token2);
        }

        private void NewEntityCode(Entity st1, Entity st2, Antlr.Runtime.IToken token1, Antlr.Runtime.IToken token2)
        {
            if (st1 == null)
                NewEntityCode(st2, token2);
            else
                if (st2 == null)
                    NewEntityCode(st1, token1);
                else
                {
                    Line = (UInt64)token1.Line;
                    Column = (UInt64)token1.CharPositionInLine + 1;
                    childs = new[] { st1, st2 };
                    childsType = new[] { (TypeCode)token1.Type, (TypeCode)token2.Type };
                    lines = new[] { (UInt64)token1.Line, (UInt64)token2.Line };
                    columns = new[] { (UInt64)token1.CharPositionInLine + 1, (UInt64)token2.CharPositionInLine + 1 };
                    thisType = (TypeCode)token1.Type;
                }
        }

        internal EntityCode(Entity st1, Entity st2, Entity st3, Antlr.Runtime.IToken token1, Antlr.Runtime.IToken token2, Antlr.Runtime.IToken token3)
        {
            if (st1 == null)
                NewEntityCode(st2, st3, token2, token3);
            else
                if (st2 == null)
                    NewEntityCode(st1, st3, token1, token3);
                else
                    if (st3 == null)
                        NewEntityCode(st1, st2, token1, token2);
                    else
                    {
                        Line = (UInt64)token1.Line;
                        Column = (UInt64)token1.CharPositionInLine + 1;
                        childs = new[] { st1, st2, st3 };
                        childsType = new[] { (TypeCode)token1.Type, (TypeCode)token2.Type, (TypeCode)token3.Type };
                        lines = new[] { (UInt64)token1.Line, (UInt64)token2.Line, (UInt64)token3.Line };
                        columns = new[] { (UInt64)token1.CharPositionInLine + 1, (UInt64)token2.CharPositionInLine + 1, (UInt64)token3.CharPositionInLine + 1 };
                        thisType = (TypeCode)token1.Type;
                    }
        }

        internal EntityCode(Entity st1, string name)
        {
            List<string> sub = st1.Identifier();
            
            this.name = sub != null ? new List<string>(sub) : new List<string>();

            this.name.Add(name);
        }

        internal EntityCode(int num)
        {
            isThis = true;
        }

        internal EntityCode()
        {
        }

        internal EntityCode(string name)
        {
            this.name = new List<string> {name};
        }

        internal void Add(Entity st, Antlr.Runtime.IToken token)
        {
                if (st == null && token.Text != "EXPR") //добавлено для корректной обработки FOR
                    return;

                Increase(1);

                childs[childs.Length - 1] = st;
                childsType[childsType.Length - 1] = (TypeCode)token.Type;
                if (token.Line == 0 && st != null)
                {
                    token.Line = st.getPos()[0];
                    token.CharPositionInLine = st.getPos()[1];
                }
                lines[lines.Length - 1] = (UInt64)token.Line;
                columns[columns.Length - 1] = (UInt64)token.CharPositionInLine + 1;
        }

        /// <summary>
        /// Увеличивает массив childs на требуемую величину count
        /// </summary>
        /// <param name="count"></param>
        private void Increase(int count)
        {
            if (childs == null)
            {
                childs = new Entity[count];
                childsType = new TypeCode[count];
                lines = new UInt64[count];
                columns = new UInt64[count];
            }
            else
            {
                Entity[] childs_new = new Entity[childs.Length + count];
                TypeCode[] childsType_new = new TypeCode[childsType.Length + count];
                UInt64[] lines_new = new UInt64[lines.Length + count];
                UInt64[] columns_new = new UInt64[columns.Length + count];
                for (int i = 0; i < childs.Length; i++)
                {
                    childs_new[i] = childs[i];
                    childsType_new[i] = childsType[i];
                    lines_new[i] = lines[i];
                    columns_new[i] = columns[i];
                }

                childs = childs_new;
                childsType = childsType_new;
                lines = lines_new;
                columns = columns_new;
            }
        }

        internal void Add(Entity st1, Entity st2, Antlr.Runtime.IToken token1, Antlr.Runtime.IToken token2)
        {
            if (st1 == null)
                Add(st2, token2);
            else if (st2 == null)
                Add(st1, token1);
            else
            {
                Increase(2);
                childs[childs.Length - 2] = st1;
                childs[childs.Length - 1] = st2;
                childsType[childsType.Length - 2] = (TypeCode)token1.Type;
                childsType[childsType.Length - 1] = (TypeCode)token2.Type;
                lines[lines.Length - 2] = (UInt64)token1.Line;
                lines[lines.Length - 1] = (UInt64)token2.Line;
                columns[columns.Length - 2] = (UInt64)token1.CharPositionInLine + 1;
                columns[columns.Length - 1] = (UInt64)token2.CharPositionInLine + 1;
            }
        }

        internal override List<string> Identifier()
        {
            if (isThis)
                return new List<string>{"this"}; 
            if (name != null)
                return name;
            if (childs == null || childs.Length != 1)
                return null;
            return childs[0].Identifier();
        }

        internal List<EntityObject> ObjectList()
        {
            throw new NotImplementedException();
        }

        internal override void Pass1(SemanticTables semantic)
        {
            if(childs != null)
                foreach (Entity entity in childs)
                    if (entity != null)
                        entity.Pass1(semantic);
        }

        internal override void Pass2(SemanticTables semantic)
        {
            if(childs != null)
                for(int i=0; i<childs.Length; ++i)
                {
                    Entity ent;
                    if (childs[i] != null)
                        ent = childs[i];
                    else
                        continue;

                    List<string> entityName = ent.Identifier();
                    if(entityName == null)
                        ent.Pass2(semantic);
                    else   //Нашли использование идентификатора
                    {
                        EntityScope.ScopeFound found = semantic.currentScope.Get(entityName, semantic);
                        if (found.scope == null) //Мы нашли использование еще не известного идентификатора
                                                //Это либо внешний идентификатор, либо еще не известный внутренний
                        {
                            Variable var = semantic.variables.AddVariable();
                            string name1;
                            bool isGlobal; //Значение переменной игнорируем
                            semantic.ListToName(entityName, out name1, out isGlobal, semantic);
                            var.Name = name1;
                            var.AddValueGetPosition(semantic.fileName, lines[i], columns[i], semantic.currentScope.Function);
                            semantic.globalScope.Add_Step1(var);
                        }
                        else     //В противном случае ничего делать не надо. Это использование в арифметике уже знакомого 
                            //идентификатора
                            if (found.variable != null)
                                found.variable.AddValueGetPosition(semantic.fileName, lines[i], columns[i], semantic.currentScope.Function);
                            else throw new Exception();
                    }
                }
        }
        internal IStatement getLastStatement(IStatement st)
        {
            IStatement tmp = st;


            if (st == null)
                return null;

            while (tmp.NextInLinearBlock != null)
                tmp = tmp.NextInLinearBlock;

            return tmp;
        }

        internal bool isZero(int[] a)
        {
            if(a[0] == 0 && a[1] == 0)
                return true;
            return false;
        }


        /// <summary>
        /// Осуществляется рекурсивных проход построенного дерева с занесением в хранилище вызовов функций.
        /// </summary>
        internal override void Pass3(SemanticTables semantic)
        {
            IStatement last;
            IStatement dump;

            //порождаем стейтмент
            switch (thisType)
            {
                #region SWITCH
                case TypeCode.SWITCH:
                    {
                        IStatementSwitch switchSt = semantic.statements.AddStatementSwitch(Line, Column, semantic.file);
                        last = getLastStatement(semantic.currentScope.Statement);
                        if (last == null)
                            semantic.currentScope.Statement = switchSt;
                        else
                            last.NextInLinearBlock = switchSt;

                        //condition
                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент
                        IStatement caseSt = null;
                        IOperation caseOp = null;
                        IStatement defaultSt = null;
                        for (int j = 0; j < childs.Count(); j++)
                        {
                            switch (childsType[j])
                            {
                                case TypeCode.DEFAULT:
                                    {
                                        semantic.currentScope.Statement = defaultSt; //формируем 1 большой дефолт стейтмент
                                        if (caseOp != null)
                                        {
                                            //такой сброс всегда надо производить, если у нас пошел новый кейс или дефолт
                                            if (caseSt == null)
                                                caseSt = semantic.statements.AddStatementOperational(lines[j - 1], columns[j - 1], semantic.file);
                                            
                                            switchSt.AddCase(caseOp, caseSt);
                                            caseOp = null;
                                            caseSt = null;
                                        }

                                        childs[j].Pass3(semantic); // сохраняем статус вставки датчиков
                                        defaultSt = semantic.currentScope.Statement; //формируем 1 большой дефолт стейтмент - добавляем полученные результаты
                                        break;
                                    }
                                case TypeCode.CASE:
                                    {
                                        if (caseOp != null)
                                        {
                                            //такой сброс всегда надо производить, если у нас пошел новый кейс или дефолт
                                            if (caseSt == null)
                                                if (lines[j - 1] == 0)
                                                    caseSt = semantic.statements.AddStatementOperational((ulong)childs[j - 1].getPos()[0], (ulong)childs[j - 1].getPos()[1], semantic.file);//может так надо делать всегда?
                                                else
                                                    caseSt = semantic.statements.AddStatementOperational(lines[j - 1], columns[j - 1], semantic.file);

                                            switchSt.AddCase(caseOp, caseSt);
                                            caseOp = null;
                                            caseSt = null;
                                        }
                                        semantic.currentScope.Statement = null;     //один большой кейс оператор делать не надо
                                        childs[j].Pass3(semantic);                  //здесь датчик не ставим никогда - это условие в кейсе
                                        if (semantic.currentScope.Statement != null)
                                        {
                                            if (semantic.currentScope.Statement is IStatementOperational)
                                                caseOp = ((IStatementOperational)semantic.currentScope.Statement).Operation ?? semantic.operations.AddOperation();
                                            else
                                                throw new Exception("Wring case expression");
                                        }
                                        else
                                            caseOp = semantic.operations.AddOperation();

                                        semantic.currentScope.Statement = null;     //один большой кейс оператор делать не надо
                                        
                                        break;
                                    }
                                case TypeCode.CASEST:
                                    {
                                        semantic.currentScope.Statement = caseSt;
                                        childs[j].Pass3(semantic);      //сохраняем статус вставки датчиков
                                        caseSt = semantic.currentScope.Statement;
                                        break;
                                    }
                                case TypeCode.SWITCH:
                                    {
                                        semantic.currentScope.Statement = null;
                                        childs[j].Pass3(semantic); //здесь датчик не ставим никогда - это условие в голове свитча
                                        if (semantic.currentScope.Statement is IStatementOperational)
                                        {
                                            if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                                switchSt.Condition = ((IStatementOperational)semantic.currentScope.Statement).Operation;
                                        }
                                        else
                                        {
                                            switchSt.Condition = semantic.operations.AddOperation();
                                        }
                                        semantic.currentScope.Statement = null;
                                        break;
                                    }
                            }
                        }
                        if (defaultSt != null)
                            switchSt.AddDefaultCase(defaultSt);

                        if (caseOp != null)
                        {
                            //такой сброс всегда надо производить, если у нас пошел новый кейс или дефолт
                            if (caseSt == null)
                                if(lines[lines.Count() - 1] == 0)
                                    caseSt = semantic.statements.AddStatementOperational((ulong)childs[lines.Count() - 1].getPos()[0], (ulong)childs[lines.Count() - 1].getPos()[1], semantic.file);//может так надо делать всегда?
                                else
                                    caseSt = semantic.statements.AddStatementOperational(lines[lines.Count() - 1], columns[lines.Count() - 1], semantic.file);
                            switchSt.AddCase(caseOp, caseSt);
                            caseOp = null;
                            caseSt = null;
                        }
                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент
                        break;
                    }
                #endregion
                #region IF
                case TypeCode.IF:
                    {
                        IStatementIf ifSt = semantic.statements.AddStatementIf(Line, Column, semantic.file);
                        last = getLastStatement(semantic.currentScope.Statement);
                        if (last == null)
                            semantic.currentScope.Statement = ifSt;
                        else
                            last.NextInLinearBlock = ifSt;

                        //condition
                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                        childs[0].Pass3(semantic);
                        if (semantic.currentScope.Statement != null)                //добавляем условие только тогда, когда породился хотя бы один стейтмент при разборе ребенка
                        {
                            if (!(semantic.currentScope.Statement is IStatementOperational))
                                throw new Exception();
                            if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                ifSt.Condition = ((IStatementOperational)semantic.currentScope.Statement).Operation;
                        }

                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент

                        if (childs.Count() > 1) //then
                        {
                            dump = DumpCurrentStatement(semantic);                  //сохраняем текущий стэйтмент

                            childs[1].Pass3(semantic);
                            if (!isZero(childs[1].getPos()))
                            {
                                ifSt.ThenStatement = semantic.currentScope.Statement ??
                                                     semantic.statements.AddStatementOperational(
                                                         (ulong) childs[1].getPos()[0],
                                                         (ulong) childs[1].getPos()[1] > 1
                                                             ? (ulong) childs[1].getPos()[1] - 1
                                                             : (ulong) childs[1].getPos()[1], semantic.file);
                            }
                            else
                            {
                                var code = (childs[0] as EntityCode);
                                ifSt.ThenStatement = semantic.currentScope.Statement ??
                                             semantic.statements.AddStatementOperational(
                                                 (ulong)code.lines[code.lines.Length - 1],
                                                 (ulong)code.columns[code.columns.Length - 1], semantic.file);
                            }

                            semantic.currentScope.Statement = dump;                 //восстанавливаем текущий стэйтмент
                            if (childs.Count() > 2) // else
                            {
                                dump = DumpCurrentStatement(semantic);              //сохраняем текущий стэйтмент

                                childs[2].Pass3(semantic);
                                if(!isZero(childs[2].getPos()))
                                    ifSt.ElseStatement = semantic.currentScope.Statement ?? semantic.statements.AddStatementOperational((ulong)childs[2].getPos()[0], (ulong)childs[2].getPos()[1] > 1 ? (ulong)childs[2].getPos()[1] - 1 : (ulong)childs[2].getPos()[1], semantic.file);

                                semantic.currentScope.Statement = dump;             //восстанавливаем текущий стэйтмент
                            }
                        }
                        break;
                    }
                #endregion
                #region FOR
                //FOR
                case TypeCode.FOR:
                    {
                        IStatementFor forSt = semantic.statements.AddStatementFor(Line, Column, semantic.storage.files.Find(semantic.fileName));
                        last = getLastStatement(semantic.currentScope.Statement);
                        if (last == null)
                            semantic.currentScope.Statement = forSt;
                        else
                            last.NextInLinearBlock = forSt;

                        ////проверка на разумность
                        //if (childsType[0] == TypeCode.FORITER)
                        //    if (((EntityCode)childs[0]).childs.Count() != 2)
                        //        throw new Exception();
                        //if (childsType[0] == TypeCode.FORSTEP)
                        //    if (((EntityCode)childs[0]).childs.Count() != 3)
                        //        throw new Exception();

                        //start
                        if (((EntityCode)childs[0]).childs[0] != null && ((EntityCode)childs[0]).childs[0] is EntityCode)
                        {
                            dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент
                            
                            ((EntityCode)childs[0]).childs[0].Pass3(semantic);
                            if (semantic.currentScope.Statement != null)
                            {
                                if (!(semantic.currentScope.Statement is IStatementOperational))
                                    throw new Exception();
                                if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                    forSt.Start = ((IStatementOperational)semantic.currentScope.Statement).Operation;
                            }
                            
                            semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент
                        }

                        //condition
                        if (((EntityCode)childs[0]).childs[1] != null)
                        {
                            dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент
                            
                            ((EntityCode)childs[0]).childs[1].Pass3(semantic);
                            if (semantic.currentScope.Statement != null)
                                if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                    forSt.Condition = ((IStatementOperational)semantic.currentScope.Statement).Operation;

                            semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент
                        }


                        //iteration
                        if (((EntityCode)childs[0]).childs.Count() > 2 && ((EntityCode)childs[0]).childs[2] != null)
                        {
                            dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                            ((EntityCode)childs[0]).childs[2].Pass3(semantic);
                            if (semantic.currentScope.Statement != null)
                                if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                    forSt.Iteration = ((IStatementOperational)semantic.currentScope.Statement).Operation;

                            semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент
                        }

                        //body
                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент
                        if (childs.Length >= 2) //пустой фор!!!
                        {
                            childs[1].Pass3(semantic);
                            if (!isZero(childs[1].getPos()))
                            {
                                forSt.Body = semantic.currentScope.Statement ??
                                             semantic.statements.AddStatementOperational((ulong) childs[1].getPos()[0],
                                                 (ulong) childs[1].getPos()[1], semantic.file);
                            }
                            else
                            {
                                var code = (childs[0] as EntityCode);
                                forSt.Body = semantic.currentScope.Statement ??
                                             semantic.statements.AddStatementOperational(
                                                 (ulong) code.lines[code.lines.Length - 1],
                                                 (ulong) code.columns[code.columns.Length - 1], semantic.file);
                            }
                        }

                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент

                        break;
                    }
                #endregion
                #region DO
                //DO-WHILE
                case TypeCode.DO:
                    {
                        IStatementDoAndWhile doWhileSt = semantic.statements.AddStatementDoAndWhile(Line, Column, semantic.storage.files.Find(semantic.fileName));
                        doWhileSt.IsCheckConditionBeforeFirstRun = false;
                        last = getLastStatement(semantic.currentScope.Statement);
                        if (last == null)
                            semantic.currentScope.Statement = doWhileSt;
                        else
                            last.NextInLinearBlock = doWhileSt;

                        //body
                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                        childs[0].Pass3(semantic);
                        if (!isZero(childs[0].getPos()))
                            doWhileSt.Body = semantic.currentScope.Statement ?? semantic.statements.AddStatementOperational((ulong)childs[0].getPos()[0], (ulong)childs[0].getPos()[1], semantic.file);

                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент

                        //condition
                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                        childs[1].Pass3(semantic);
                        if (semantic.currentScope.Statement != null)
                        {
                            if (!(semantic.currentScope.Statement is IStatementOperational))
                                throw new Exception();
                            if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                doWhileSt.Condition = ((IStatementOperational)semantic.currentScope.Statement).Operation;
                        }
                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент

                        break;
                    }
                #endregion
                #region WHILE
                //WHILE
                case TypeCode.WHILE:
                    {
                        IStatementDoAndWhile whileSt = semantic.statements.AddStatementDoAndWhile(Line, Column, semantic.storage.files.Find(semantic.fileName));
                        whileSt.IsCheckConditionBeforeFirstRun = true;
                        last = getLastStatement(semantic.currentScope.Statement);
                        if (last == null)
                            semantic.currentScope.Statement = whileSt;
                        else
                            last.NextInLinearBlock = whileSt;

                        //condition
                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                        childs[0].Pass3(semantic);
                        if (semantic.currentScope.Statement != null)
                        {
                            if (!(semantic.currentScope.Statement is IStatementOperational))
                                throw new Exception();
                            if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                whileSt.Condition = ((IStatementOperational)semantic.currentScope.Statement).Operation;
                        }

                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент

                        //body
                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                        childs[1].Pass3(semantic);
                        if (!isZero(childs[1].getPos()))
                        {
                            whileSt.Body = semantic.currentScope.Statement ??
                                           semantic.statements.AddStatementOperational((ulong) childs[1].getPos()[0],
                                               (ulong) childs[1].getPos()[1], semantic.file);
                        }
                        else
                        {
                            whileSt.Body = semantic.currentScope.Statement ??
                                           semantic.statements.AddStatementOperational((ulong)childs[0].getPos()[0],
                                               (ulong)childs[0].getPos()[1], semantic.file);
                        }

                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент

                        break;
                    }
                #endregion
                #region TRY
                //TRY-CATCH-FINALLY
                case TypeCode.TRY:
                    {
                        if (childs.Count() < 2 && childs.Count() > 3)
                            throw new Exception();

                        IStatementTryCatchFinally tryCatchFinallySt = semantic.statements.AddStatementTryCatchFinally(Line, Column, semantic.storage.files.Find(semantic.fileName));
                        last = getLastStatement(semantic.currentScope.Statement);
                        if (last == null)
                            semantic.currentScope.Statement = tryCatchFinallySt;
                        else
                            last.NextInLinearBlock = tryCatchFinallySt;

                        //try
                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                        childs[0].Pass3(semantic);
                        if (!isZero(childs[0].getPos()))
                            tryCatchFinallySt.TryBlock = semantic.currentScope.Statement ?? semantic.statements.AddStatementOperational((ulong)childs[0].getPos()[0], (ulong)childs[0].getPos()[1], semantic.file);

                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент

                        //catch
                        if (childs.Count() == 3 || (childs.Count() == 2 && childsType[1] == TypeCode.CATCH))
                        {
                            dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                            childs[1].Pass3(semantic);
                            if (!isZero(childs[1].getPos()))
                                tryCatchFinallySt.AddCatch(semantic.currentScope.Statement ?? semantic.statements.AddStatementOperational((ulong)childs[1].getPos()[0], (ulong)childs[1].getPos()[1], semantic.file));

                            semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент
                        }

                        //finally
                        if (childs.Count() == 3 || (childs.Count() == 2 && childsType[1] == TypeCode.FINALLY))
                        {
                            dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                            if (childs.Count() == 2)
                                childs[1].Pass3(semantic);
                            else
                                childs[2].Pass3(semantic);
                            if (semantic.currentScope.Statement != null)
                                tryCatchFinallySt.FinallyBlock = semantic.currentScope.Statement;
                            else
                                if (childs.Count() == 2)
                                {
                                    if (!isZero(childs[1].getPos()))
                                        tryCatchFinallySt.FinallyBlock = semantic.statements.AddStatementOperational((ulong)childs[1].getPos()[0], (ulong)childs[1].getPos()[1], semantic.file);
                                }
                                else
                                    if (!isZero(childs[2].getPos()))
                                        tryCatchFinallySt.FinallyBlock = semantic.statements.AddStatementOperational((ulong)childs[2].getPos()[0], (ulong)childs[2].getPos()[1], semantic.file);


                            semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент
                        }

                        break;
                    }
                #endregion
                #region THROW
                //THROW
                case TypeCode.THROW:
                    {
                        IStatementThrow throwSt = semantic.statements.AddStatementThrow(Line, Column, semantic.storage.files.Find(semantic.fileName));
                        last = getLastStatement(semantic.currentScope.Statement);
                        if (last == null)
                            semantic.currentScope.Statement = throwSt;
                        else
                            last.NextInLinearBlock = throwSt;

                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент

                        childs[0].Pass3(semantic);
                        if (semantic.currentScope.Statement != null)
                        {
                            if (!(semantic.currentScope.Statement is IStatementOperational))
                                throw new Exception();
                            if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                throwSt.Exception = ((IStatementOperational)semantic.currentScope.Statement).Operation;
                        }
                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент
                        break;
                    }
                #endregion
                #region BREAK
                //BREAK
                case TypeCode.BREAK:
                    {
                        IStatementBreak breakSt = semantic.statements.AddStatementBreak( Line, Column, semantic.storage.files.Find(semantic.fileName));
                        last = getLastStatement(semantic.currentScope.Statement);
                        
                        if (last == null)
                            semantic.currentScope.Statement = breakSt;
                        else
                            last.NextInLinearBlock = breakSt;
                        
                        break;
                    }
                #endregion
                #region CONTINUE
                //CONTINUE
                case TypeCode.CONTINUE:
                    {
                        IStatementContinue continueSt = semantic.statements.AddStatementContinue(Line, Column, semantic.storage.files.Find(semantic.fileName));
                        last = getLastStatement(semantic.currentScope.Statement);
                        
                        if (last == null)
                            semantic.currentScope.Statement = continueSt;
                        else
                            last.NextInLinearBlock = continueSt;
                        
                        break;
                    }
                #endregion
                #region RETURN
                //RETURN
                case TypeCode.RETURN:
                    {
                        IStatementReturn returnSt = semantic.statements.AddStatementReturn(Line, Column, semantic.storage.files.Find(semantic.fileName));

                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент
                        
                        if (childs != null)
                            foreach (Entity entity in childs)
                                entity.Pass3(semantic);

                        if (semantic.currentScope.Statement is IStatementOperational)
                            if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                returnSt.ReturnOperation = ((IStatementOperational)semantic.currentScope.Statement).Operation;

                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент
                        
                        last = getLastStatement(semantic.currentScope.Statement);
                        
                        if (last == null)
                            semantic.currentScope.Statement = returnSt;
                        else
                            last.NextInLinearBlock = returnSt;
                        
                        break;
                    }
                #endregion
                #region WITH
                case TypeCode.WITH:
                    {
                        //добавляем стейтмент
                        IStatementUsing usingSt = semantic.statements.AddStatementUsing(Line, Column, semantic.file);
                        
                        last = getLastStatement(semantic.currentScope.Statement);
                        if (last == null)
                            semantic.currentScope.Statement = usingSt;
                        else
                            last.NextInLinearBlock = usingSt;

                        dump = DumpCurrentStatement(semantic);                      //сохраняем текущий стэйтмент
                        if (childs != null)
                        {
                            //должно быть всегда только 2 ребенка
                            childs[0].Pass3(semantic);
                            //надо получить айоперейшн
                            if (semantic.currentScope.Statement is IStatementOperational)
                            {
                                if (((IStatementOperational)semantic.currentScope.Statement).Operation != null)
                                {
                                    usingSt.Head = ((IStatementOperational)semantic.currentScope.Statement).Operation;
                                }
                            }
                            semantic.currentScope.Statement = null;
                            childs[1].Pass3(semantic);
                            if (!isZero(childs[1].getPos()))
                                usingSt.Body = semantic.currentScope.Statement ?? semantic.statements.AddStatementOperational((ulong)childs[1].getPos()[0], (ulong)childs[1].getPos()[1], semantic.file);
                        }
                        semantic.currentScope.Statement = dump;                     //восстанавливаем текущий стэйтмент
                        break;
                    }
                #endregion
                default:
                    if (childs != null)
                    {
                        foreach (Entity entity in childs)
                            entity.Pass3(semantic);
                    }
                    //else
                    //{
                    //    Entity ent = this; //ТАК И НЕ ПОНЯЛ, ЗАЧЕМ ЭТО БЫЛО ВВЕДЕНО ?!

                    //    List<string> name = ent.Identifier();
                    //    if (name != null)
                    //    {
                    //        EntityScope.ScopeFound found = semantic.currentScope.Get(name, semantic);
                    //        if (found.scope != null)        //Мы нашли использование еще не известного идентификатора
                    //            if (found.variable != null)
                    //            {
                    //                last = getLastStatement(semantic.currentScope.Statement);
                                    
                    //                if (last is IStatementOperational)
                    //                    ((IStatementOperational)last).Operation.AddFunctionCallInSequenceOrder(found.variable);
                    //                else
                    //                {
                    //                    IStatementOperational stOp = semantic.statements.AddStatementOperational(semantic.file, Line, Column);
                    //                    IOperation op = semantic.operations.AddOperation();
                    //                    op.AddFunctionCallInSequenceOrder(found.variable);
                    //                    stOp.Operation = op;
                                        
                    //                    if (semantic.currentScope.Statement == null)
                    //                        semantic.currentScope.Statement = stOp;
                    //                    else
                    //                        last.NextInLinearBlock = stOp;
                    //                }
                    //            }
                    //            else throw new Exception();
                    //    }
                    //}
                    break;
            }
        }
            
        private static IStatement DumpCurrentStatement(SemanticTables semantic)
        {
            IStatement dump = semantic.currentScope.Statement;
            semantic.currentScope.Statement = null;
            return dump;
        }
    }
}
