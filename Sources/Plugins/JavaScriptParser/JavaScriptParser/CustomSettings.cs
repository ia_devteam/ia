﻿using System;
using System.Windows.Forms;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class Settings : UserControl
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        internal Settings(ulong minimumStartSensor)
        {
            InitializeComponent();
            
            //Основные настройки для элементов форм
            firstSensorNumber_numericUpDown.Minimum = minimumStartSensor;
            firstSensorNumber_numericUpDown.Maximum = ulong.MaxValue;

            //Выбор уровня НДВ
            level2_radioButton.Checked = PluginSettings.IsLevel2;
            level3_radioButton.Checked = PluginSettings.IsLevel3;
            level0_radioButton.Checked = PluginSettings.IsNoSensors;
            
            //Номер первого датчика
            firstSensorNumber_numericUpDown.Value = 
                    (PluginSettings.FirstSensorNumber > minimumStartSensor)
                    ? PluginSettings.FirstSensorNumber
                    : minimumStartSensor;

            //Текст функции датчика
            sensorFunctionText_textBox.Text = PluginSettings.IsLevel2 ? PluginSettings.SensorFunctionText_Level2 : PluginSettings.SensorFunctionText_Level3 ;

            //Текст датчика
            sensorText_textBox.Text = PluginSettings.IsLevel2 ? PluginSettings.SensorText_Level2 : PluginSettings.SensorText_Level3;
   
            //Дополниетельные параметры
            isSkipError_checkBox.Checked = PluginSettings.IsSkipErrors;

            // Изначальная видимость элементов управления
            firstSensorNumber_groupBox.Enabled = sensorText_groupBox.Enabled = sensorFunctionText_groupBox.Enabled = !PluginSettings.IsNoSensors;
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.IsLevel2 = level2_radioButton.Checked;
            PluginSettings.IsLevel3 = level3_radioButton.Checked;
            PluginSettings.IsNoSensors = level0_radioButton.Checked;
            PluginSettings.FirstSensorNumber = (ulong)firstSensorNumber_numericUpDown.Value;
            PluginSettings.IsSkipErrors = isSkipError_checkBox.Checked;

            if (level2_radioButton.Checked)
            {
                PluginSettings.SensorFunctionText_Level2 = sensorFunctionText_textBox.Text;
                PluginSettings.SensorText_Level2 = sensorText_textBox.Text;
            }
            else if (level3_radioButton.Checked)
            {
                PluginSettings.SensorFunctionText_Level3 = sensorFunctionText_textBox.Text;
                PluginSettings.SensorText_Level3 = sensorText_textBox.Text;
            }
        }

        /// <summary>
        /// Обработчик - используем 2-й уровень вставки датчиков
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void level2_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!level2_radioButton.Checked)
                return;

            sensorFunctionText_textBox.Text = PluginSettings.SensorFunctionText_Level2;
            sensorText_textBox.Text = PluginSettings.SensorText_Level2;
        }

        /// <summary>
        /// Обработчик - используем 3-й уровень вставки датчиков
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void level3_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!level3_radioButton.Checked)
                return;

            sensorFunctionText_textBox.Text = PluginSettings.SensorFunctionText_Level3;
            sensorText_textBox.Text = PluginSettings.SensorText_Level3;
        }

        /// <summary>
        /// Обработчик - не вставляем датчики
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void level0_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            sensorFunctionText_textBox.Text = string.Empty;
            sensorText_textBox.Text = string.Empty;

            firstSensorNumber_groupBox.Enabled = sensorText_groupBox.Enabled = sensorFunctionText_groupBox.Enabled = !level0_radioButton.Checked;
        }
    }
}
