﻿using System;
using Antlr.Runtime.Tree;

namespace IA.Plugins.Parsers.JavaScriptParser
{

    public partial class ES3Tree
    {
        SemanticTables semantic;

        public override void EmitErrorMessage(string msg)
        {
            throw new Exception(msg);
        }

        internal void SetSemanticTables(SemanticTables semantic)
        {
            this.semantic = semantic;
        }

        internal Antlr.Runtime.IToken CurrentToken()
        {
            CommonTreeNodeStream tree = (CommonTreeNodeStream)this.TreeNodeStream;
            CommonTree node = (CommonTree)tree.CurrentSymbol;

            Antlr.Runtime.IToken token = node.Token;
            if (token.CharPositionInLine == -1)
                throw new Exception("Использован токен с неприсвоенным номером строки");
            return token;
        }

        internal void ReportSensor(Antlr.Runtime.IToken token)
        {
            if(semantic.isAddSensors)
                if (semantic.functionForSensor != null)
                {
                    SensorPlace place = new SensorPlace 
                            { Column = (UInt64) token.CharPositionInLine + 1, 
                                file = semantic.fileID, 
                                function = semantic.functionForSensor.Id, 
                                Line = (UInt64) token.Line 
                            };

                    semantic.sensorPlaces.Add(place);

                    semantic.functionForSensor = null;
                }
        }
    }

}