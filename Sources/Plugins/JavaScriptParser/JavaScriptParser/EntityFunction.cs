﻿using System;
using System.Collections.Generic;
using Store;

namespace IA.Plugins.Parsers.JavaScriptParser
{
    /// <summary>
    /// Вершина промежуточного представления, соответствующая определению функции.
    /// Может встретиться внутри тела другой функции.
    /// </summary>
    public class EntityFunction : EntityScope
    {
        /// <summary>
        /// Тело функции
        /// </summary>
        Entity body;
        /// <summary>
        /// Имя функции
        /// </summary>
        string name;
		/// <summary>
        /// Позиция тела функции в файле
        /// </summary>
        ulong[] location;
        /// <summary>
        /// Елемент Хранилища, созданный для этой функции.
        /// </summary>
        IFunction func;

        /// <summary>
        /// Закрывающая скобка в функции
        /// </summary>
        Antlr.Runtime.IToken close = null;

        /// <summary>
        /// Генерирует default-функцию, в которую помещается весь код скрипта
        /// </summary>
        internal EntityFunction( SemanticTables semantic)
        {
            this.name = "Глобальное окружение";
        
            func = semantic.functions.AddFunction();
            func.Name = name;
            func.AddDefinition(name, enFileKindForAdd.externalFile);
        }

        public struct Parameter
        {
            internal string name;
            internal Antlr.Runtime.IToken token;
        }

        internal EntityFunction(string Name, Antlr.Runtime.IToken token, List<Parameter> parameters, SemanticTables semantic)
        {
            this.name = Name;
            ReportFunction(Name, token, parameters, semantic);

            //Всякая функция является переменной.
            Variable var = ReportVariableFromFunction(func, semantic, token);

            semantic.currentScope.Add_Step0(var);

            EnterScope(semantic);

            ReportParameters(parameters, semantic);

            PrepareForSensor(semantic);
        }

        /// <summary>
        /// Ранее была объфвлена функция с именем left_name в пространстве found.
        /// Теперь мы встретили использование переменной с таким именем.
        /// Вспоминаем что в JavaScript при определении функции с именем на самом деле
        /// создается переменная, которой присваивается указатель на функцию.
        /// Создаем эту самую переменную
        /// </summary>
        /// <param name="func"></param>
        /// <param name="semantic"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private Variable ReportVariableFromFunction(IFunction func, SemanticTables semantic,  Antlr.Runtime.IToken token)
        {
            if (func.Name == "")
                return null;

            Variable var = semantic.variables.AddVariable();
            var.Name = func.Name;

            Store.Location loc = func.Definition();
            if(loc != null)
                var.AddDefinition(semantic.storage.files.GetFile(loc.GetFileID()), loc.GetLine(), loc.GetColumn(), semantic.currentScope.Function);

            //Запоминаем, что новая переменная ссылалась на функцию.
            EntityAssign.VariableGetPointer(semantic, var, func, (UInt64)token.Line, (UInt64)token.CharPositionInLine + 1, true);

            return var;
        }


        private void PrepareForSensor(SemanticTables semantic)
        {
            semantic.functionForSensor = func;
        }

        internal void Body(Entity body, SemanticTables semantic, Antlr.Runtime.IToken token, Antlr.Runtime.IToken close)
        {
            this.body = body;
            location = new[] { (UInt64)token.Line, (UInt64)token.CharPositionInLine + 1 };
            this.close = close;
            LeaveScope(semantic);
        }

        private void ReportFunction(string Name, Antlr.Runtime.IToken token, List<Parameter> parameters, SemanticTables semantic)
        {
            func = semantic.functions.AddFunction();
            func.Name = Name;
            if (token != null)
                func.AddDefinition(semantic.fileName, (UInt64)token.Line, (UInt64)token.CharPositionInLine + 1);
            else
                func.AddDefinition(semantic.fileName, 0, 0);
        }

        private void ReportParameters(List<Parameter> parameters, SemanticTables semantic)
        {
            foreach (Parameter par in parameters)
            {
                Variable var = semantic.variables.AddVariable();
                var.Name = par.name;
                var.AddDefinition(semantic.fileName, (UInt64)par.token.Line, (UInt64)par.token.CharPositionInLine + 1, semantic.currentScope.Function);

                semantic.currentScope.Add_Step0(var);
            }
        }

        internal override int[] getPos()
        {
            if (location != null)
                return new[] { (int)location[0], (int)location[1] };
            
            return new[] { 0, 0 };
        }

        /// <summary>
        /// Обрабатываем констукцию, когда в определении переменной ей присваивается функция.
        /// 
        /// В JavaScript часто используется конструкция вида
        /// var square = function(x) { return x*x; }
        /// В этом случае функции присваеваем имя square и не создаем переменной.
        /// В противном случае создаем переменную и добавлем ей указатель.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>true, если конструкция указанного вида</returns>
        internal bool AssignName(string name)
        {
            if (name == null || func.Name == "")
            {
                func.Name = name;
                this.name = name;
                return true;
            }
            
            return false;
        }

        internal IFunction Function
        {
            get { return func; }
        }

        internal IStatement Statement { get; set; }

        internal override void Pass1(SemanticTables semantic)
        {
            EnterScope(semantic);

            body.Pass1(semantic);

            LeaveScope(semantic);
        }

        internal override void Pass2(SemanticTables semantic)
        {
            EnterScope(semantic);

            body.Pass2(semantic);

            LeaveScope(semantic);
        }

        internal override void Pass3(SemanticTables semantic)
        {
            EnterScope(semantic);

            body.Pass3(semantic);
            semantic.currentScope.func.EntryStatement = semantic.currentScope.Statement ?? semantic.statements.AddStatementOperational(location[0], location[1], semantic.file);
            IStatement st = semantic.currentScope.func.EntryStatement;
            if(st != null && (!(st is IStatementOperational && (st as IStatementOperational).Operation == null) || st.NextInLinearBlock != null))
            {
                while ((st.NextInLinearBlock != null))
                    st = st.NextInLinearBlock;
                st.NextInLinearBlock = semantic.storage.statements.AddStatementOperational((ulong)close.Line, (ulong)close.CharPositionInLine + 1, semantic.file);
            }
            semantic.currentScope.Statement = null;
            LeaveScope(semantic);
        }
    }
}
