﻿using System;
using System.Collections.Generic;
using Store;


namespace IA.Plugins.Parsers.JavaScriptParser
{
    /// <summary>
    /// Вершина промежуточного представления, соответствующая вызову функции
    /// </summary>
    public class EntityCall : Entity
    {
        /// <summary>
        /// Функция, которая вызывается. Не обязательно идентификатор.
        /// </summary>
        Entity what;
        /// <summary>
        /// Аргументы вызова
        /// </summary>
        Entity Arguments;

        UInt64 Line;
        UInt64 Column;

        internal override int[] getPos()
        {
            return new int[] { (int)Line, (int)Column };
        }

        internal IStatement getLastStatement(IStatement st)
        {
            IStatement tmp = st;


            if (st == null)
                return null;

            while (tmp.NextInLinearBlock != null)
                tmp = tmp.NextInLinearBlock;

            return tmp;
        }

        internal EntityCall(Entity what, Entity Arguments, Antlr.Runtime.IToken token)
        {
            this.what = what;
            this.Arguments = Arguments;

            Line = (UInt64)token.Line;
            Column = (UInt64)token.CharPositionInLine + 1;
        }

        internal override void Pass1(SemanticTables semantic)
        {
            what.Pass1(semantic);
            Arguments.Pass1(semantic);
        }

        internal override void Pass2(SemanticTables semantic)
        {
            List<string> name;
            if(what is EntityNew)
                name = (what as EntityNew).ent.Identifier();
            else
                name = what.Identifier();
            if (name != null)   //Если имени нет, то мы упускаем вызов
                ReportCall(name, semantic);

            if (Arguments != null)
                Arguments.Pass2(semantic);
        }
        internal override void Pass3(SemanticTables semantic)
        {
            List<string> name;
            if (what is EntityNew)
                name = (what as EntityNew).ent.Identifier();
            else
                name = what.Identifier();
             
            if (Arguments != null)
                Arguments.Pass3(semantic); //уходим рекурсивно внутрь - сначала вынимаем самые внутренние вызовы функций
            
            if (name != null)   //Если имени нет, то мы упускаем вызов
            {
                EntityScope.ScopeFound called = semantic.currentScope.Get(name, semantic);
                if (called.variable != null)
                {
                    
                    IStatement last = getLastStatement(semantic.currentScope.Statement);
                    if (last is IStatementOperational && last.FirstSymbolLocation.GetFileID() == semantic.fileID)
                    {
                        ((IStatementOperational)last).Operation.AddFunctionCallInSequenceOrder(called.variable);
                        if (last.FirstSymbolLocation.GetLine() > Line || last.FirstSymbolLocation.GetLine() == Line && last.FirstSymbolLocation.GetColumn() > Column)
                            last.SetFirstSymbolLocation(semantic.file, Line, Column); //корректировка положение statement в тексте, так как сначала мы обрабатываем самые внутренние функции
                        if (Line > last.FirstSymbolLocation.GetLine())
                            last.SetLastSymbolLocation(semantic.file, Line, 100500);
                        
                    }
                    else
                    {
                        IStatementOperational stOp = semantic.statements.AddStatementOperational(Line, Column, semantic.file);
                        IOperation op = semantic.operations.AddOperation();
                        op.AddFunctionCallInSequenceOrder(called.variable);
                        stOp.Operation = op;
                        if (semantic.currentScope.Statement == null)
                            semantic.currentScope.Statement = stOp;
                        else
                            last.NextInLinearBlock = stOp;

                    }
                }
            }
            if (what is EntityFunction)
                what.Pass3(semantic);

            
        }

        /// <summary>
        /// Информируем о том, что в текущем месте была вызвана функция с именем name
        /// </summary>
        /// <param name="name"></param>
        private void ReportCall(List<string> name, SemanticTables semantic)
        {
            EntityScope.ScopeFound called = semantic.currentScope.Get(name, semantic);

            if (called.variable != null) //FIXME!!! тут явно какой-то бред
            {
                if (called.variable != null)
                    called.variable.AddValueCallPosition(semantic.fileName, Line, Column, semantic.currentScope.Function);
                else
                    throw new NotImplementedException();
            }
            else   //Если такой идентификатор нам до этого не встречался, он либо встроенный в среду, либо определялся в другом скрипте
            {
                string name1;
                bool isGlobal; //Значение переменной игнорируем
                semantic.ListToName(name, out name1, out isGlobal, semantic);

                Variable var = semantic.variables.AddVariable();
                var.Name = name1;

                semantic.globalScope.Add_Step1(var);
                var.AddValueCallPosition(semantic.fileName, Line, Column, semantic.currentScope.Function);
            }
        }

    }
}
