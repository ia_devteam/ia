﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using IA.Plugins.Parsers.CSharpParser;

using IOController;
using Store;
using Store.Table;
using TestUtils;

using IA.Plugins.Analyses.SensorGenerator;

namespace TestCSharpParser
{
    /// <summary>
    /// Класс для тестов парсера C#
    /// </summary>
    [TestClass]
    public class TestCSharpParser
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Список сообщений
            /// </summary>
            public List<string> Messages
            {
                get
                {
                    return messages;
                }
            }
        }

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Путь до каталога с эталонными исходными текстами
        /// </summary>
        private string labSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонным дампами Хранилища
        /// </summary>
        private string dumpSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонными отчетами
        /// </summary>
        private string reportSampleDirectoryPath;

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными лабораторным текстам
        /// </summary>
        private const string LAB_SAMPLES_SUBDIRECTORY = @"CSParser\Labs";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными дампами Хранилища
        /// </summary>
        private const string DUMP_SAMPLES_SUBDIRECTORY = @"CSParser\Dump";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными отчетами
        /// </summary>
        private const string REPORT_SAMPLES_SUBDIRECTORY = @"CSParser\Reports";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        private const ulong pluginID = Store.Const.PluginIdentifiers.CSHARP_PARSER;

        /// <summary>
        /// Перехватчик сообщений монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            labSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, LAB_SAMPLES_SUBDIRECTORY);
            dumpSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, DUMP_SAMPLES_SUBDIRECTORY);
            reportSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, REPORT_SAMPLES_SUBDIRECTORY);
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже присутствует в лабораторных тексах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_WrongFirstSensor_InLabs()
        {
            CSharpParser testPlugin = new CSharpParser();
            string sourcePostfixPart = @"Sources\CSharp\EmptyFolder";

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                       //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, sourcePostfixPart)))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, sourcePostfixPart));
                    CustomizeStorage(storage,
                         testMaterialsPath,
                         sourcePostfixPart,
                         sourcePostfixPart,
                         true,
                         1);

                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <1> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <2>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика не фигурировал в исходных файлах, 
        /// но оказался меньше, чем максимальный идентификатор в Хранилище.
        /// </summary>
        [TestMethod]
        public void CSharpParser_WrongFirstSensor_NotInLabs()
        {
            CSharpParser testPlugin = new CSharpParser();
            string sourcePostfixPart = @"Sources\CSharp\EmptyFolder";

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, sourcePostfixPart)))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, sourcePostfixPart));
                    CustomizeStorage(storage,
                           testMaterialsPath,
                           sourcePostfixPart,
                           sourcePostfixPart,
                           true,
                           2);

                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(3, func1.Id, Kind.START);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже фигурировал в исходных файлах, но был удален.
        /// </summary>
        [TestMethod]
        public void CSharpParser_WrongFirstSensor_NotInLabsDeleted()
        {
            CSharpParser testPlugin = new CSharpParser();
            string sourcePostfixPart = @"Sources\CSharp\EmptyFolder";

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, sourcePostfixPart)))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, sourcePostfixPart));
                    CustomizeStorage(storage,
                           testMaterialsPath,
                           sourcePostfixPart,
                           sourcePostfixPart,
                           true,
                           2);

                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);
                    storage.sensors.AddSensor(2, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(3, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);

                    storage.sensors.RemoveSensor(2);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        #region CommonTests
        /// <summary>
        /// Тест материалов для простого режима. Датчики вставляются по 2-му уровню контроля.
        /// </summary>
        [TestMethod]
        public void CSharpParser_SimpleModeTestSources()
        {
            string sourcePostfixPart = @"Sources\CSharp\TestSources\SimpleModeTestSources";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        @"Sources\CSharpBuilt\SimpleModeTestSources\built",
                        true,
                        1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"SimpleModeTestSources"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "SimpleModeTestSources"));

                    return true;
                },                                                                  // checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Перехват сообщений при работе с пустой папкой
        /// </summary>
        [TestMethod]
        public void CSharpParser_EmptyFolder()
        {
            string sourcePostfixPart = @"Sources\CSharp\EmptyFolder";


            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, sourcePostfixPart)))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, sourcePostfixPart));

                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        sourcePostfixPart,
                        true,
                        1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckMessage("Файлов проектов не обнаружено.");
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "EmptyFolder"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Перехват сообщений при отсутсвтвии файлов с расширением .cs, но присутсвует проектный файл с ссылками на отсутствующие файлы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_NoCS()
        {
            string sourcePostfixPart = @"CSParser\NoCS";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        sourcePostfixPart,
                        true,
                        1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckMessage("Исходных файлов на языке C# не обнаружено.");
                    CheckMessage("Файлы для обработки не найдены.");
                    CheckMessage("Файл <" + Path.Combine(testMaterialsPath.ToLower(), sourcePostfixPart.ToLower(), "Program.cs") + "> не найден в Хранилище.");
                    CheckMessage("Файл <" + Path.Combine(testMaterialsPath.ToLower(), sourcePostfixPart.ToLower(), @"Properties\AssemblyInfo.cs") + "> не найден в Хранилище.");
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "NoCS"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности выдачи сообщения о неправильных собранных исходных текстах.
        /// То, что передали в качестве очищенных, обязательно должно присутствовать в собранных.
        /// </summary>
        [TestMethod]
        public void CSharpParser_UnprocessedFiles()
        {
            string sourcePostfixPart = @"Sources\CSharp\TestSources\UnprocessedFiles\sources";
            string builtPostfixPart = @"Sources\CSharpBuilt\UnprocessedFiles\built";
            try
            {
                testUtils.RunTest(
                    sourcePostfixPart,                                                  // sourcePostfixPart
                    new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                    false,                                                              // isUseCachedStorage
                    (storage, testMaterialsPath) => { },                                // prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            builtPostfixPart,
                            true,
                            1);
                    },                                                                  // customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },                                                                  // checkStorage
                    (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                    false,                                                              // isUpdateReport
                    (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                    (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                    );
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message.Contains("Директория с собранными исходными текстами <" + Path.Combine(testUtils.TestMatirialsDirectoryPath, builtPostfixPart) + "> не корректна."));

                //Удаляем каталог
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
            }
        }

        /// <summary>
        /// Перехват сообщений при присутствии необработанных исходных файлов
        /// </summary>
        [TestMethod]
        public void CSharpParser_NoFilesInStorage()
        {
            string sourcePostfixPart = @"Sources\CSharp\TestSources\NoFilesInStorage\sources";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        @"Sources\CSharpBuilt\NoFilesInStorage\built",
                        true,
                        1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckMessage("Файл <" + Path.Combine(testMaterialsPath.ToLower(), sourcePostfixPart.ToLower(), @"ConsoleApplication1\ConsoleApplication1\Program.cs") + "> не найден в Хранилище.");
                    CheckMessage("Файл <" + Path.Combine(testMaterialsPath.ToLower(), sourcePostfixPart.ToLower(), @"ConsoleApplication1\ConsoleApplication1\Properties\AssemblyInfo.cs") + "> не найден в Хранилище.");
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "NoFilesInStorage"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Перехват сообщений при отсутствии проектных файлов с расширением .csproj
        /// </summary>
        [TestMethod]
        public void CSharpParser_NoCSProj()
        {
            string sourcePostfixPart = @"CSParser\NoCSProj";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        sourcePostfixPart,
                        true,
                        1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckMessage("Файлов проектов не обнаружено.");
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "NoCSProj"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера с внешними сборками. Датчики вставляются по 2-му уровню контроля.
        /// </summary>
        [TestMethod]
        public void CSharpParser_UnknownAssemblies()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\UnknownAssemblies";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\UnknownAssemblies",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"UnknownAssemblies"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "UnknownAssemblies"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion CommonTests
        #region lvl2-TestSolutions
        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution1()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution1";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution1\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution1\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution1"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution2()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution2";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution2\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution2\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution2"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution3()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution3";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution3\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution3\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution3"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution4()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution4";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution4\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution4\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution4"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution5()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution5";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution5\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution5\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution5"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution6()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution6";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution6\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution6\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution6"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution7()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution7";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution7\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution7\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution7"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution8()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution8";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution8\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution8\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution8"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution9()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution9";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution9\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution9\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution9"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution10()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution10";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution10\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution10\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution10"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution11()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution11";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution11\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution11\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution11"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution12()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution12";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution12\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution12\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution12"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution13()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution13";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution13\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution13\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution13"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution14()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution14";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution14\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution14\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution14"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 2-уровню контроля, начиная с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files2ndLevelTestSolution15()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution15";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution15\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution15\sensored-lvl2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV2\TestSolution15"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion lvl2-TestSolutions
        #region lvl3-TestSolutions
        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution1()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution1";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution1\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution1\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution1"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution2()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution2";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution2\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution2\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution2"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution3()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution3";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution3\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution3\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution3"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution4()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution4";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution4\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution4\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution4"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution5()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution5";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution5\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution5\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution5"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution6()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution6";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution6\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution6\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution6"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution7()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution7";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution7\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution7\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution7"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution8()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution8";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution8\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution8\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution8"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution9()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution9";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution9\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution9\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution9"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution10()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution10";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution10\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution10\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution10"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution11()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution11";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution11\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution11\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution11"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution12()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution12";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution12\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution12\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution12"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution13()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution13";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution13\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution13\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution13"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution14()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution14";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution14\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution14\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution14"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки парсера. Датчики вставлются по 3-уровню контроля, начиная с единицы.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Files3rdLevelTestSolution15()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\TestSolution15";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestSolution15\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestSolution15\sensored-lvl3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"LV3\TestSolution15"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion lvl2-TestSolutions
        #region From TestCS
        /// <summary>
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_2level()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\Test_Simple";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\Test_Simple\built",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"Test_Simple\2"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "Test_Simple"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Тест проверяет работу плагина по 3-му уровню НДВ с расстановкой датчиков с единицы
        /// </summary>
        [TestMethod]
        public void CSharpParser_3level()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestSources\Test_Simple";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\Test_Simple\built",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"Test_Simple\3"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "Test_Simple"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        #endregion From TestCS
        #region Test4Bugs
        /// <summary>
        /// Баг с занесением функций из ядра фреймворка в список функций проекта при обработке парсером.
        /// </summary>
        [TestMethod]
        public void CSharpParser_CoreAssembliesInStorage()
        {
            const string sourcePostfixPart = @"Sources\CSharp\FindStringsInFiles";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\FindStringsInFiles",
                            false,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    //FIXME - эталонный лог нуждается в обсуждении: вносить ли в список вызываемых функций системные
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "CoreAssembliesInStorage"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion
        #region Test Simple Statements
        /// <summary>
        /// Проверка определения конструкции break по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_BreakStatements()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\BreakStatements";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\BreakStatements",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\BreakStatements"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\BreakStatements"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\BreakStatements"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции continue по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_ContinueStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\ContinueStatement";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\ContinueStatement",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\ContinueStatement"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\ContinueStatement"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\ContinueStatement"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции do-while по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_DoWhileStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\DoWhileStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\DoWhileStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\DoWhileStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\DoWhileStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\DoWhileStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции foreach по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_ForeachStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\ForeachStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\ForeachStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\ForeachStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\ForeachStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\ForeachStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции for по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_ForStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\ForStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\ForStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\ForStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\ForStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\ForStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции goto по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_GotoStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\GotoStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\GotoStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\GotoStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\GotoStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\GotoStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции if-then-else по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_IfThenElseStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\IfThenElseStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\IfThenElseStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\IfThenElseStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\IfThenElseStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\IfThenElseStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции if-then по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_IfThenStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\IfThenStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\IfThenStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\IfThenStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\IfThenStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\IfThenStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции return по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_ReturnStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\ReturnStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\ReturnStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\ReturnStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\ReturnStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\ReturnStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции switch по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_SwitchStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\SwitchStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\SwitchStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\SwitchStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\SwitchStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\SwitchStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции throw по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_ThrowStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\ThrowStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\ThrowStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\ThrowStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\ThrowStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\ThrowStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции try-catch-finally по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_TryCatchFinallyStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\TryCatchFinallyStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\TryCatchFinallyStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\TryCatchFinallyStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\TryCatchFinallyStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\TryCatchFinallyStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции using по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_UsingStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\UsingStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\UsingStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\UsingStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\UsingStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\UsingStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции while-do по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_WhileDoStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\WhileDoStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\WhileDoStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\WhileDoStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\WhileDoStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\WhileDoStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка определения конструкции yield-return по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void CSharpParser_YieldReturnStatement()
        {
            const string sourcePostfixPart = @"Sources\CSharp\TestStatements\YieldReturnStatement\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\TestStatements\YieldReturnStatement\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TestStatements\YieldReturnStatement\"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"TestStatements\YieldReturnStatement\"));

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsPath);

                    CheckXML(Path.Combine(reportSampleDirectoryPath, @"TestStatements\YieldReturnStatement\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        #endregion Test Simple Statements

        /// <summary>
        /// Тест материалов для простого режима. Датчики вставляются по 2-му уровню контроля.
        /// </summary>
        [TestMethod]
        public void CSharpParser_InsufficientSources()
        {
            string sourcePostfixPart = @"Sources\CSharp\TestSources\InsufficientSources";

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                new IA.Plugins.Parsers.CSharpParser.CSharpParser(),                 // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        @"Sources\CSharpBuilt\InsufficientSources",
                        true,
                        1, false, true, "");
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"InsufficientSources"));
                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "InsufficientSources"));

                    return true;
                },                                                                  // checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },           // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        #region Bugs
        sealed class RunTest_Bugs : TestUtilsClass.RunTestConfig
        {
            string sourcePostfixPart;
            public static RunTest_Bugs Get([System.Runtime.CompilerServices.CallerMemberName] string testname = null)
            {
                return new RunTest_Bugs(testname);
            }
            private RunTest_Bugs(string testname)
            {
                string bugName = testname.Substring("CSharpParser_Bug".Length);

                sourcePostfixPart = @"CSParser\Bugs\" + bugName;
            }

            public override string Stage01SourcesPostfix
            {
                get
                {
                    return sourcePostfixPart;
                }
            }
            public override IA.Plugin.Interface Stage02Plugin
            {
                get
                {
                    return new CSharpParser();
                }
            }

            public override void Stage11PrepareStorageDelegate(Storage storage, string testMaterialsDirectoryPath)
            {
                string sources = Path.Combine(sourcePostfixPart, "source");
                CustomizeStorage(storage,
                       testMaterialsDirectoryPath,
                       sources,
                       sources,
                       false,
                       1,
                       true);
            }

            public override bool Stage12CheckStorageDelegate(Storage storage, string testMaterialsDirectoryPath)
            {
                string labsEthalon = Path.Combine(testMaterialsDirectoryPath, sourcePostfixPart, "lab");
                string labsTest = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "C#");
                Assert.IsTrue(TestUtils.TestUtilsClass.Reports_Directory_TXT_Compare(labsEthalon, labsTest),
                    "Лабораторные исходные тексты не совпадают с эталонными.");
                return true;
            }
        }

        /// <summary>
        /// Проверка корректной обработки ситуаций вроде <code>case '\u2028'</code>. Символ юникода не должен быть преобразован.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Bug12_UnicodeChars()
        {
            testUtils.RunTest(RunTest_Bugs.Get());
        }

        /// <summary>
        /// Проверка корректной замены ReturnValue для случая <code>yield return new KeyValuePair<TKey, TValue>(kvp.Key, kvp.Value);</code>.
        /// После вставки датчиков бывает такое: <code>KeyValuePair<TKey,TValue RNTRNTRNT_5037 = new KeyValuePair<TKey, TValue>(kvp.Key, kvp.Value);</code> (отсутствует закрывающая угловая скобка)
        /// </summary>
        [TestMethod]
        public void CSharpParser_Bug13_ReturnValueReplace()
        {
            testUtils.RunTest(RunTest_Bugs.Get());
        }

        /// <summary>
        /// Проверка корректной обработки случая <code> S IQueryProvider.Execute<S>(Expression expression)</code>.
        /// </summary>
        [TestMethod, Ignore] //тест выключен в связи с принципиальным багом базового парсера. Излечение возможно только при глубокой доработке либо смене базового парсера.
        public void CSharpParser_Bug127_GenericMethod()
        {
            testUtils.RunTest(RunTest_Bugs.Get());
        }

        /// <summary>
        /// Проверка корректной обработки случая <code> public override System.Collections.Generic.IEnumerable<Expression> Operands</code>.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Bug128_GenericIEnumerableFullyQuallifiedName()
        {
            testUtils.RunTest(RunTest_Bugs.Get());
        }

        /// <summary>
        /// Проверка корректной обработки switch..case.
        /// </summary>
        [TestMethod]
        public void CSharpParser_Bug119_SwitchCaseBrackets()
        {
            testUtils.RunTest(RunTest_Bugs.Get());
        }

        /// <summary>
        /// Баг с обрезанием части файла
        /// </summary>
        [TestMethod]
        public void CSharpParser_Bug156_FileCrop()
        {
            testUtils.RunTest(RunTest_Bugs.Get());
        }

        /// <summary>
        /// Баг с System.Void (недопустимый тип в C#)
        /// </summary>
        [TestMethod]
        public void CSharpParser_Bug157_SystemVoid()
        {
            testUtils.RunTest(RunTest_Bugs.Get());
        }

        /// <summary>
        /// Баг с падением парсера на вложенных пустых скобочках
        /// </summary>
        [TestMethod]
        public void CSharpParser_Bug159_InnerBracketsFail()
        {
            testUtils.RunTest(RunTest_Bugs.Get());
        }

        #endregion

        /// <summary>
        /// Подготовка Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="builtSourcesPart">Подпуть до каталога с собранными исходными текстами.</param>
        /// <param name="testMaterialsPath">Путь до каталога с материалами.</param>
        /// <param name="sourcePostfixPart">Подпуть до каталога с исходными файлами.</param>
        /// <param name="isLevel2">Уровень НДВ. true - 2-ой уровень</param>
        private static void CustomizeStorage(Storage storage, string testMaterialsPath, string sourcePostfixPart, string builtSourcesPart, bool isLevel2, UInt64 firstSensorNumber, bool isUnixMode = false, bool insufficientProcess = false, string insufficientDefines = "")
        {
            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
            TestUtilsClass.Run_IdentifyFileTypes(storage);

            SettingUpPlugin(storage, Path.Combine(testMaterialsPath, builtSourcesPart), firstSensorNumber, isLevel2, isUnixMode, insufficientProcess, insufficientDefines);
        }

        /// <summary>
        /// Настройка плагина
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="buildSourcesDirectoryPath">Путь до собранных исходных текстов. Не может быть null.</param>
        /// <param name="firstSensorNumber">Номер первого датчика. Не может быть отрицательным.</param>
        /// <param name="isLevel2">true - 2-ой уровень.</param>
        /// <param name="isUnixMode">true - вставляем датчики для Mono под Linux.</param>
        private static void SettingUpPlugin(Storage storage, string buildSourcesDirectoryPath, UInt64 firstSensorNumber, bool isLevel2, bool isUnixMode, bool insufficientProcess, string insufficientDefines)
        {
            IBufferWriter writer = WriterPool.Get();

            writer.Add(buildSourcesDirectoryPath);
            writer.Add(firstSensorNumber);
            writer.Add(isLevel2);
            writer.Add(isUnixMode);
            writer.Add(insufficientProcess);
            writer.Add(insufficientDefines);
            storage.pluginSettings.SaveSettings(pluginID, writer);
        }

        /// <summary>
        /// Дамп Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <returns>Путь до каталога, куда был произведен дамп Хранилища</returns>
        private string Dump(Storage storage)
        {
            string dumpDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Dump");

            if (!DirectoryController.IsExists(dumpDirectoryPath))
                DirectoryController.Create(dumpDirectoryPath);

            storage.ClassesDump(dumpDirectoryPath);
            storage.FilesDump(dumpDirectoryPath);
            storage.FunctionsDump(dumpDirectoryPath);
            storage.VariablesDump(dumpDirectoryPath);

            return dumpDirectoryPath;
        }

        /// <summary>
        /// Проверка дампа Хранилища
        /// </summary>
        /// <param name="dumpDirectoryPath">Путь до каталога дампа в Хранилище. Не может быть пустым.</param>
        /// <param name="dumpSamplesDirectoryPath">Путь до каталога с эталонным дампом. Не может быть пустым.</param>
        private void CheckDump(string dumpDirectoryPath, string dumpSamplesDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(dumpDirectoryPath, dumpSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Дамп Хранилища не совпадает с эталонными.");
        }

        /// <summary>
        /// Проверка лабораторных исходных файлов
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="labSamplesDirectoryPath">Путь до каталога с эталонными лабораторными исходными текстами. Не может быть пустым.</param>
        private void CheckLabs(Storage storage, string labSamplesDirectoryPath)
        {
            string labsDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB), "C#");

            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(labsDirectoryPath, labSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Исходные тексты со вставленными датчиками не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка ожидаемого сообщения в логе монитора
        /// </summary>
        /// <param name="message">Текст сообщения. Не может быть пустым.</param>
        private void CheckMessage(string message)
        {
            bool isContainMessage = listener.ContainsPart(message);

            Assert.IsTrue(isContainMessage, "Сообщение не совпадает с ожидаемым.");
        }

        /// <summary>
        /// Проверка отчетов
        /// </summary>
        /// <param name="xmlSampleDirectoryPath">Путь до каталога с эталонными отчетами. Не может быть пустым.</param>
        /// <param name="storageReportsDirectoryPath">Путь до каталога с отчетами в Хранилище. Не может быть пустым.</param>
        private void CheckXML(string xmlSampleDirectoryPath, string storageReportsDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_XML_Compare(storageReportsDirectoryPath, xmlSampleDirectoryPath);

            Assert.IsTrue(isEqual, "Отчеты не совпадают с эталонными.");
        }

        /// <summary>
        /// Issue #207 'Датчик в unsafe не присваивается функции'
        /// </summary>
        [TestMethod]
        public void CSharpParser_UnsafeCode()
        {
            const string sourcePostfixPart = @"Sources\CSharp\UnsafeCode\";
            CSharpParser testPlugin = new CSharpParser();

            testUtils.RunTest(
                sourcePostfixPart,                                                  // sourcePostfixPart
                testPlugin,                                                         // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                            testMaterialsPath,
                            sourcePostfixPart,
                            @"Sources\CSharpBuilt\UnsafeCode\build\",
                            true,
                            1);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"UnsafeCode\"));
                    //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"UnsafeCode\"));

                    SensorGenerator SenGen = new SensorGenerator();
                    SenGen.Initialize(storage);
                    SenGen.GenerateReports(dumpDirectoryPath);
                    Assert.IsTrue(listener.Messages.Count == 1);
                    //CheckMessage(@"Датчику 2 не присвоена функция. Файл: ConsoleApplication1\Program.cs Строка: 23");

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    //testPlugin.GenerateStatements(reportsPath);

                    //CheckXML(Path.Combine(reportSampleDirectoryPath, @"UnsafeCode\"), reportsPath);

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }


    }
}
