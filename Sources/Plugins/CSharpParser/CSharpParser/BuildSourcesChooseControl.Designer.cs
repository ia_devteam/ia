﻿namespace IA.Plugins.Parsers.CSharpParser
{
    partial class BuildSourcesChooseControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_groupBox = new System.Windows.Forms.GroupBox();
            this.main_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.main_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_groupBox
            // 
            this.main_groupBox.Controls.Add(this.main_textBox);
            this.main_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_groupBox.Location = new System.Drawing.Point(0, 0);
            this.main_groupBox.Name = "main_groupBox";
            this.main_groupBox.Size = new System.Drawing.Size(615, 45);
            this.main_groupBox.TabIndex = 11;
            this.main_groupBox.TabStop = false;
            this.main_groupBox.Text = "Путь до директории с собранными исходными текстами на языке C#";
            // 
            // main_textBox
            // 
            this.main_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_textBox.Location = new System.Drawing.Point(3, 16);
            this.main_textBox.MaximumSize = new System.Drawing.Size(0, 24);
            this.main_textBox.MinimumSize = new System.Drawing.Size(470, 24);
            this.main_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.main_textBox.Name = "main_textBox";
            this.main_textBox.Size = new System.Drawing.Size(609, 24);
            this.main_textBox.TabIndex = 0;
            // 
            // BuildSourcesChooseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_groupBox);
            this.Name = "BuildSourcesChooseControl";
            this.Size = new System.Drawing.Size(615, 45);
            this.main_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox main_groupBox;
        private Controls.TextBoxWithDialogButton main_textBox;
    }
}
