﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using IA.SlnResolver;
using IA.SlnResolver.CS;

namespace IA.Plugins.Parsers.CSharpParser
{
    internal partial class CustomSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        internal const string ObjectName = "Окно настроек стадии (плагина) Парсер C#";

        /// <summary>
        /// Элемент управления выбора пути до директории с собранными исходными текстами
        /// </summary>
        private BuildSourcesChooseControl buildSourcesChooseControl = null;

        /// <summary>
        /// Элемент управления - Конфигурации проектов
        /// </summary>
        private ProjectsConfigurationsControl projectsConfigurationsControl = null;

        /// <summary>
        /// Список проектов
        /// </summary>
        private List<IA.SlnResolver.CS.ProjectBase> CSProjects { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        internal CustomSettings(ulong minimumStartSensor)
        {
            InitializeComponent();

            //Изначально список проектов не определён
            this.CSProjects = null;

            //Инициализация элемента управления выбора пути до директории с собранными исходными текстами
            buildSourcesChooseControl = new BuildSourcesChooseControl()
            {
                BuildSourcesDirectoryPath = PluginSettings.BuiltSourcesDirectoryPath,
                Dock = DockStyle.Fill
            };

            main_tableLayoutPanel.Controls.Add(buildSourcesChooseControl, 0, 0);

            //Номер первого датчика
            firstSensorNumber_numericUpDown.Minimum = minimumStartSensor;
            firstSensorNumber_numericUpDown.Maximum = ulong.MaxValue;
            firstSensorNumber_numericUpDown.Value = 
                    (PluginSettings.FirstSensorNumber > minimumStartSensor)
                    ? PluginSettings.FirstSensorNumber 
                    : minimumStartSensor;

            //Уровень НДВ
            isLevel2_radioButton.Checked = PluginSettings.IsLevel2;
            isLevel3_radioButton.Checked = PluginSettings.IsLevel3;
            chkUnixMode.Checked = PluginSettings.UnixMode;
            insufficientFiles_checkBox.Checked = PluginSettings.ProcessInsufficient;
            defines_textBox.Text = PluginSettings.InsufficientDefines;
            //Инициализация элемента управления - Конфигурации проектов
            projectsConfigurationsControl = new ProjectsConfigurationsControl()
            {
                Dock = DockStyle.Fill,
                Enabled = false
            };

            projectsConfigurations_tableLayoutPanel.Controls.Add(projectsConfigurationsControl, 0, 1);
        }

        /// <summary>
        /// Обработчик - Дважды нажали на таблицу префиксов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void prefixes_dataGridView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            prefixes_dataGridView.Rows.Add();
            prefixes_dataGridView.Rows[prefixes_dataGridView.RowCount - 1].SetValues(String.Empty, buildSourcesChooseControl.BuildSourcesDirectoryPath);
        }

        /// <summary>
        /// Обработчик - Закончили редактирование ячейки таблицы префиксов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void prefixes_dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewRow row in prefixes_dataGridView.Rows)
                if(((string)row.Cells[0].Value) != String.Empty)
                    if (!PluginSettings.Prefixes.ContainsKey((string)row.Cells[0].Value) && ((string)row.Cells[1].Value) != String.Empty)
                        PluginSettings.Prefixes.Add(((string)row.Cells[0].Value).ToLower(), ((string)row.Cells[1].Value).ToLower());
        }

        /// <summary>
        /// Сохранение указанных настроек
        /// </summary>
        public void Save()
        {
            //сохраняем основные настройки
            PluginSettings.BuiltSourcesDirectoryPath = buildSourcesChooseControl.BuildSourcesDirectoryPath;
            PluginSettings.FirstSensorNumber = (uint)firstSensorNumber_numericUpDown.Value;
            PluginSettings.IsLevel2 = isLevel2_radioButton.Checked;
            PluginSettings.IsLevel3 = isLevel3_radioButton.Checked;
            PluginSettings.UnixMode = chkUnixMode.Checked;
            PluginSettings.ProcessInsufficient = insufficientFiles_checkBox.Checked;
            PluginSettings.InsufficientDefines = defines_textBox.Text;
            //Сохраняем настройки конфигураций проектов
            PluginSettings.CsProjects = this.CSProjects;
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку "Редактировать"\"Сохранить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void edit_save_button_Click(object sender, EventArgs e)
        {
            //Сохранить
            if (Convert.ToBoolean(edit_save_button.Tag))
            {
                //Сохраняем конфигурацию проектов
                this.CSProjects = projectsConfigurationsControl.CSProjects;

                //Очищаем список конфигураций проектов
                projectsConfigurationsControl.CSProjects = null;

                //Делаем список конфигураций проектов недоступным
                projectsConfigurationsControl.Enabled = false;

                //Меняем режим кнопки
                edit_save_button.Text = "Редактировать";
                edit_save_button.Tag = false;
            }
            //Редактировать
            else
            {
                //Проверка на разумность
                if (String.IsNullOrWhiteSpace(buildSourcesChooseControl.BuildSourcesDirectoryPath) || !Directory.Exists(buildSourcesChooseControl.BuildSourcesDirectoryPath))
                {
                    //Отображаем предупреждение
                    Monitor.Log.Warning(CustomSettings.ObjectName, "Путь к директории с собранными исходными текстами не задан или не существует.", true);

                    return;
                }

                //Если уже редактировали конфигурации, то достаем их из памяти
                //Иначе - смотрим файл проекта
                if (this.CSProjects == null || this.CSProjects.Count == 0)
                {
                    //Создаём список проектов (парсим проекты)
                    List<SlnResolver.CS.ProjectBase> projects = new List<ProjectBase>();
                    foreach (string file in Directory.EnumerateFiles(buildSourcesChooseControl.BuildSourcesDirectoryPath, "*.csproj", SearchOption.AllDirectories))
                        projects.Add(ProjectVS.ParseCSProject(file));

                    //Передаём список проектов для отображения
                    projectsConfigurationsControl.CSProjects = projects;
                }
                else
                    projectsConfigurationsControl.CSProjects = this.CSProjects;

                //Делаем список конфигураций проектов доступным
                projectsConfigurationsControl.Enabled = true;

                //Меняем режим кнопки
                edit_save_button.Text = "Сохранить";
                edit_save_button.Tag = true;
            }
        }

    }
}