﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using IA.SlnResolver;
using IA.SlnResolver.CS;

namespace IA.Plugins.Parsers.CSharpParser
{
    internal partial class ProjectsConfigurationsControl : UserControl
    {
        private List<SlnResolver.CS.ProjectBase> csProjects;
        /// <summary>
        /// Список проектов для отображения
        /// </summary>
        internal List<SlnResolver.CS.ProjectBase> CSProjects
        {
            get
            {
                //Сохраняем настройки конфигураций проектов
                this.Save();

                return this.csProjects;
            }

            set
            {
                //Сохраняем переданное значение
                this.csProjects = value;

                //Формируем настройки конфигураций проектов
                this.Generate();
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        internal ProjectsConfigurationsControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обработчик - Выбрали конфигурацию в списке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void configurations_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int row = 0; row < main_dataGridView.Rows.Count - 1; row++)
                if (((DataGridViewComboBoxCell)main_dataGridView[1, row]).Items.Contains(configurations_comboBox.SelectedItem.ToString()))
                    main_dataGridView[1, row].Value = configurations_comboBox.SelectedItem.ToString();
        }        

        /// <summary>
        /// Обработчик - Нажали на кнопку "Проверить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void check_button_Click(object sender, EventArgs e)
        {
            //Проверка на разумность
            if (this.csProjects == null)
                throw new Exception("Список проектов не определён.");

            //смотрим установленные конфигурации и проверяем есть ли dll'ки для них
            foreach (ProjectBase project in this.csProjects)
                for (int row = 0; row < main_dataGridView.Rows.Count - 1; row++)
                    if (main_dataGridView[0, row].Value.ToString() == project.Name)
                    {
                        for (int m = 0; m < main_dataGridView.ColumnCount; m++)
                            main_dataGridView.Rows[row].Cells[m].Style.BackColor = Color.White;

                        ProjectConfiguration correctConfiguration = null;
                        bool foundCorrect = false;

                        foreach (ProjectConfiguration conf in project.Settings.ConfigurationArray)
                        {
                            string assemblyPath = conf.OutputPath.Contains("..")
                                                      ? Path.GetDirectoryName(project.Path) + conf.OutputPath
                                                      : conf.OutputPath;

                            if (project.Settings.OutputType == "Library")
                            {
                                if (File.Exists(assemblyPath + "\\" + project.Settings.AssemblyName + ".dll"))
                                {
                                    correctConfiguration = conf;
                                    if (conf.ConfigurationName == main_dataGridView[1, row].Value.ToString())
                                    {

                                        foundCorrect = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                if (File.Exists(assemblyPath + "\\" + project.Settings.AssemblyName + ".exe"))
                                {
                                    correctConfiguration = conf;
                                    if (conf.ConfigurationName == main_dataGridView[1, row].Value.ToString())
                                    {
                                        foundCorrect = true;
                                        break;
                                    }
                                }
                            }
                        }

                        //вышли из цикла, тут либо наша конфигурация крута и всё в ней есть, либо нет
                        if (correctConfiguration == null) //ни для одной конфигурации нет собранного бинарника
                        {
                            for (int m = 0; m < main_dataGridView.ColumnCount; m++)
                                main_dataGridView.Rows[row].Cells[m].Style.BackColor = Color.Red;
                        }
                        else if (!foundCorrect) //нашли но не в указанной конфигурации
                        {
                            for (int m = 0; m < main_dataGridView.ColumnCount; m++)
                                main_dataGridView.Rows[row].Cells[m].Style.BackColor = Color.Yellow;

                            main_dataGridView[1, row].Value = correctConfiguration.ConfigurationName;
                        }
                    }

            MessageBox.Show("Проверка настроек проектов завершена.", "Выполнено", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }

        /// <summary>
        /// Формирование настроек конфигураций проектов
        /// </summary>
        private void Generate()
        {
            //Очищаем список всевозможных конфигураций
            configurations_comboBox.Items.Clear();

            //Очищаем таблицу конфигураций проектов
            main_dataGridView.Rows.Clear();

            //Определяем доступность элементов управления
            configurations_comboBox.Enabled = this.csProjects != null;

            //Если ничего не передали, ничего не делаем
            if (this.csProjects == null || this.csProjects.Count == 0)
                return;

            //Заполняем таблицу
            int row = 0;
            foreach (SlnResolver.CS.ProjectBase project in this.csProjects)
            {
                main_dataGridView.Rows.Add();
                main_dataGridView[0, row].Value = project.Name;

                SetBuiltConfigurations(project, row);

                //foreach (SlnResolver.CS.ProjectConfiguration projectConfiguration in project.Settings.ConfigurationArray)
                //{
                //    ((DataGridViewComboBoxCell)main_dataGridView[1, row]).Items.Add(projectConfiguration.ConfigurationName);

                //    //Если такой конфигурации ещё не было в списке, добавляем её
                //    if (!configurations_comboBox.Items.Contains(projectConfiguration.ConfigurationName))
                //        configurations_comboBox.Items.Add(projectConfiguration.ConfigurationName);
                //}

                //SlnResolver.CS.ProjectConfiguration currentConfiguration = ProjectVS.GetCurrentConfiguration(project);                
                //if (currentConfiguration != null)
                //    main_dataGridView[1, row].Value = currentConfiguration.ConfigurationName;
                
                row++;
            }
        }

        /// <summary>
        /// Формирование списка собранных конфигураций проекта.
        /// </summary>
        /// <param name="project">Проект.</param>
        /// <param name="row">Строка таблицы конфигурации.</param>
        private void SetBuiltConfigurations(ProjectBase project, int row)
        {
            ProjectConfiguration correctConfiguration = null;

            foreach (ProjectConfiguration projectConfiguration in project.Settings.ConfigurationArray)
            {
                string assemblyPath = projectConfiguration.OutputPath.Contains("..")
                                          ? Path.GetDirectoryName(project.Path) + projectConfiguration.OutputPath
                                          : projectConfiguration.OutputPath;
                
                if (project.Settings.OutputType == "Library")
                {
                    if (File.Exists(assemblyPath + "\\" + project.Settings.AssemblyName + ".dll"))
                    {
                        correctConfiguration = projectConfiguration;
                        ((DataGridViewComboBoxCell)main_dataGridView[1, row]).Items.Add(projectConfiguration.ConfigurationName);

                        //Если такой конфигурации ещё не было в списке, добавляем её
                        if (!configurations_comboBox.Items.Contains(projectConfiguration.ConfigurationName))
                            configurations_comboBox.Items.Add(projectConfiguration.ConfigurationName);
                    }
                }
                else
                {
                    if (File.Exists(assemblyPath + "\\" + project.Settings.AssemblyName + ".exe"))
                    {
                        correctConfiguration = projectConfiguration;
                        ((DataGridViewComboBoxCell)main_dataGridView[1, row]).Items.Add(projectConfiguration.ConfigurationName);

                        //Если такой конфигурации ещё не было в списке, добавляем её
                        if (!configurations_comboBox.Items.Contains(projectConfiguration.ConfigurationName))
                            configurations_comboBox.Items.Add(projectConfiguration.ConfigurationName);
                    }
                }
            }

            if (correctConfiguration == null) //ни для одной конфигурации нет собранного бинарника
            {
                for (int m = 0; m < main_dataGridView.ColumnCount; m++)
                    main_dataGridView.Rows[row].Cells[m].Style.BackColor = Color.Red;

                foreach (string projConf in configurations_comboBox.Items)
                    ((DataGridViewComboBoxCell)main_dataGridView[1, row]).Items.Add(projConf);
            }

            ProjectConfiguration currentConfiguration = ProjectVS.GetCurrentConfiguration(project);
            if (currentConfiguration != null && ((DataGridViewComboBoxCell)main_dataGridView[1, row]).Items.Count != 0)
            {
                if (((DataGridViewComboBoxCell)main_dataGridView[1, row]).Items.Contains(currentConfiguration.ConfigurationName))
                    main_dataGridView[1, row].Value = currentConfiguration.ConfigurationName;
                else
                    main_dataGridView[1, row].Value = ((DataGridViewComboBoxCell)main_dataGridView[1, row]).Items[0];
            }
        }

        /// <summary>
        /// Сохранение введённых настроек
        /// </summary>
        private void Save()
        {
            //Если нечего сохранять, ничего не делаем
            if (this.csProjects == null)
                return;

            //Сохраняем настройки проектов
            foreach (ProjectBase project in this.csProjects)
                for (int row = 0; row < main_dataGridView.Rows.Count - 1; row++)
                    if (main_dataGridView[0, row].Value.ToString() == project.Name)
                        if (main_dataGridView[1, row].Value != null)
                            ProjectVS.SetCurrentConfiguration(project, main_dataGridView[1, row].Value.ToString());
        }
    }
}
