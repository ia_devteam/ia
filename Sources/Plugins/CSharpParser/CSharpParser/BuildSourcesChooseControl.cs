﻿using System;
using System.Windows.Forms;

namespace IA.Plugins.Parsers.CSharpParser
{
    /// <summary>
    /// Класс, реализующий элемент управления выбора путь до директории с собранными исходными текстами
    /// </summary>
    internal partial class BuildSourcesChooseControl : UserControl
    {
        /// <summary>
        /// Путь до директории с собранными исходными текстами
        /// </summary>
        internal string BuildSourcesDirectoryPath
        {
            get
            {
                return this.main_textBox.Text;
            }
            set
            {
                this.main_textBox.Text = value ?? String.Empty;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        internal BuildSourcesChooseControl()
        {
            InitializeComponent();
        }
    }
}
