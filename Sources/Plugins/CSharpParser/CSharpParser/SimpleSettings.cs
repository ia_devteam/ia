﻿using System.Windows.Forms;

namespace IA.Plugins.Parsers.CSharpParser
{
    internal partial class SimpleSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Элемент управления выбора пути до директории с собранными исходными текстами
        /// </summary>
        private BuildSourcesChooseControl buildSourcesChooseControl = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        internal SimpleSettings()
        {
            InitializeComponent();

            //Инициализация элемента управления выбора пути до директории с собранными исходными текстами
            buildSourcesChooseControl = new BuildSourcesChooseControl()
            {
                BuildSourcesDirectoryPath = PluginSettings.BuiltSourcesDirectoryPath,
                Dock = DockStyle.Fill
            };

            settings_tableLayoutPanel.Controls.Add(buildSourcesChooseControl, 0, 0);
        }

        /// <summary>
        /// Сохранить введённые настройки
        /// </summary>
        public void Save()
        {
            PluginSettings.BuiltSourcesDirectoryPath = buildSourcesChooseControl.BuildSourcesDirectoryPath;
        }
    }
}
