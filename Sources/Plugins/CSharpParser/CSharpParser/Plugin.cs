using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using IA.SlnResolver;
using IA.SlnResolver.CS;

using IA.Plugins.Parsers.CommonUtils;

using Store;
using Store.Table;

namespace IA.Plugins.Parsers.CSharpParser
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Парсер С#";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.CSHARP_PARSER;

        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Парсинг файлов")]
            FILES_PARSING,
            [Description("Первая стадия - Обработано проектов")]
            FIRST_STAGE_PROJECT_PROCESSING,
            [Description("Первая стадия - Обработано файлов")]
            FIRST_STAGE_FILES_PROCESSING,
            [Description("Вторая стадия - Обработано проектов")]
            SECOND_STAGE_PROJECT_PROCESSING,
            [Description("Вторая стадия - Обработано файлов")]
            SECOND_STAGE_FILES_PROCESSING,
        };

        #region Настройки
        /// <summary>
        /// Путь к директории с очищенными исходными текстами
        /// </summary>
        internal static string BuiltSourcesDirectoryPath = Properties.Settings.Default.BuiltSourcesDirectoryPath;

        /// <summary>
        /// Номер первого датчика
        /// </summary>
        internal static ulong FirstSensorNumber = Properties.Settings.Default.FirstSensorNumber;
                
        /// <summary>
        /// Требуется вставлять датчики по 2-му уровню НДВ?
        /// </summary>
        internal static bool IsLevel2 = Properties.Settings.Default.IsLevel2;
        
        /// <summary>
        /// Требуется вставлять датчики по 3-му уровню НДВ?
        /// </summary>
        internal static bool IsLevel3 = Properties.Settings.Default.IsLevel3;

        internal static bool UnixMode = Properties.Settings.Default.UnixMode;


        /// <summary>
        /// Обработка файлов вне проектов
        /// </summary>
        internal static bool ProcessInsufficient = Properties.Settings.Default.InsufficientProcessing;

        /// <summary>
        /// Дефайны для файлов вне проетктов
        /// </summary>
        internal static string InsufficientDefines = Properties.Settings.Default.InsufficietnDefines;
        #endregion

        #region Прочие настройки


        /// <summary>
        /// Требуется ли разрешение полных имен?
        /// </summary>
        internal static bool IsNameResolving = true;

        /// <summary>
        /// Требуется ли запустить 1-ю стадию парсинга?
        /// </summary>
        internal static bool IsStage1 = true;

        /// <summary>
        /// Требуется ли запустить 2-ю стадию парсинга?
        /// </summary>
        internal static bool IsStage2 = true;

        /// <summary>
        /// Завершена ли 1-я стадия парсинга?
        /// </summary>
        internal static bool IsStage1Completed = false;

        /// <summary>
        /// Завершена ли 2-я стадия парсинга?
        /// </summary>
        internal static bool IsStage2Completed = false;

        /// <summary>
        /// Путь к будущей директории с исходниками со вставленными датчиками
        /// </summary>
        internal static string SourcesWithSensorsDirectoryPath (Storage storage)
        {
            return Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "C#");
        }

        /// <summary>
        /// Набор префиксов
        /// </summary>
        internal static Dictionary<string, string> Prefixes = new Dictionary<string, string>();

        /// <summary>
        /// Проекты C#
        /// </summary>
        internal static List<SlnResolver.CS.ProjectBase> CsProjects = null;
        #endregion  
    }

    public class CSharpParser : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Настройки
        /// </summary>
        IA.Plugin.Settings settings;

        #region Члены PluginInterface
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
#if DEBUG
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW | Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS;
#else
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW | Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
#endif
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
		}

        /// <summary>
        /// Метод генерации отчетов для тестов
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до каталога с отчетами плагина</param>
        public void GenerateStatements(string reportsDirectoryPath)
        {
            (new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage)).Execute();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
#if DEBUG
            // Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            if (string.IsNullOrEmpty(reportsDirectoryPath))
                throw new Exception("Путь к каталогу с отчетами не определен.");

            //StatementPrinter
            StatementPrinter sprinter = new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage);
            sprinter.Execute();
#else       
            //RELEASE            
            //оставлено до 2015 года для поддержки старых работ
            return;
#endif
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            
            PluginSettings.BuiltSourcesDirectoryPath = Properties.Settings.Default.BuiltSourcesDirectoryPath;
            PluginSettings.FirstSensorNumber = Properties.Settings.Default.FirstSensorNumber;
            PluginSettings.IsLevel2 = Properties.Settings.Default.IsLevel2;
            PluginSettings.IsLevel3 = Properties.Settings.Default.IsLevel3;
            PluginSettings.UnixMode = Properties.Settings.Default.UnixMode;
            PluginSettings.IsNameResolving = true;
            PluginSettings.IsStage1 = true;
            PluginSettings.IsStage2 = true;
            PluginSettings.IsStage1Completed = false;
            PluginSettings.IsStage2Completed = false;
            PluginSettings.Prefixes = new Dictionary<string, string>();
            PluginSettings.CsProjects = null;

            PluginSettings.ProcessInsufficient = Properties.Settings.Default.InsufficientProcessing;
            PluginSettings.InsufficientDefines = Properties.Settings.Default.InsufficietnDefines;
            this.storage = storage;

            //Выгружаем настройки из данных плагина
            LoadPluginData();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            bool tmpBool = true;

            PluginSettings.BuiltSourcesDirectoryPath = settingsBuffer.GetString();
            PluginSettings.FirstSensorNumber = settingsBuffer.GetUInt64();
            
            settingsBuffer.GetBool(ref tmpBool);
            PluginSettings.IsLevel2 = tmpBool;

            PluginSettings.IsLevel3 = PluginSettings.IsLevel2 ? false : true;

            settingsBuffer.GetBool(ref tmpBool);
            PluginSettings.UnixMode = tmpBool;
            settingsBuffer.GetBool(ref tmpBool);
            PluginSettings.ProcessInsufficient = tmpBool;
            PluginSettings.InsufficientDefines = settingsBuffer.GetString();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.FirstSensorNumber = (storage != null && storage.sensors.Count() > 0) ? (uint)storage.sensors.EnumerateSensors().Max(s => s.ID) + 1 : 1;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "NDV:3":
                    PluginSettings.IsLevel3 = true;
                    PluginSettings.IsLevel2 = false;
                    break;
                case "NDV:2":
                    PluginSettings.IsLevel2 = true;
                    PluginSettings.IsLevel3 = false;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            if (storage != null && storage.sensors.Count() > 0 && storage.sensors.EnumerateSensors().Max(s => s.ID) >= PluginSettings.FirstSensorNumber)
            {
                ulong tmp_FirstSensorNumber = PluginSettings.FirstSensorNumber;
                PluginSettings.FirstSensorNumber = storage.sensors.EnumerateSensors().Max(s => s.ID) + 1;
                Monitor.Log.Warning(PluginSettings.Name, "Расстановка датчиков с номера <" + tmp_FirstSensorNumber + "> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <" + PluginSettings.FirstSensorNumber + ">.");
            }

            //Проверка существования проектных файлов в собранных исходных текстах
            if (!PluginSettings.ProcessInsufficient && Directory.EnumerateFiles(PluginSettings.BuiltSourcesDirectoryPath, "*.csproj", SearchOption.AllDirectories).Count() == 0)
            {
                Monitor.Log.Error(PluginSettings.Name, "Файлов проектов не обнаружено.");
                return;
            }

            //Проверка существования исходных файлов C# в собранных исходных текстах
            if (Directory.EnumerateFiles(PluginSettings.BuiltSourcesDirectoryPath, "*.cs", SearchOption.AllDirectories).Count() == 0)
            {
                Monitor.Log.Error(PluginSettings.Name, "Исходных файлов на языке C# не обнаружено.");
            }

            //Если настройки проектов не были заданы, проставляем всем проектам конфигурацию Release
            if (PluginSettings.CsProjects == null)
            {
                //Инициализируем список проектов
                PluginSettings.CsProjects = new List<ProjectBase>();

                //Парсим все проекты
                foreach (string file in Directory.EnumerateFiles(PluginSettings.BuiltSourcesDirectoryPath, "*.csproj", SearchOption.AllDirectories))
                    PluginSettings.CsProjects.Add(ProjectVS.ParseCSProject(file));

                //фиксим все пути до сорцов
                foreach (ProjectBase prj in PluginSettings.CsProjects)
                {
                    foreach (var file in prj.SourceFiles)
                    {
                        if (PluginSettings.BuiltSourcesDirectoryPath.EndsWith("\\"))
                            file.Location = file.Location.Replace(PluginSettings.BuiltSourcesDirectoryPath, storage.appliedSettings.FileListPath);
                        else
                        {
                            file.Location = file.Location.Replace(PluginSettings.BuiltSourcesDirectoryPath, storage.appliedSettings.FileListPath.TrimEnd('\\'));
                        }
                    }
                }

                //Проставляем всем Release в качестве дефолта
                foreach (ProjectBase project in PluginSettings.CsProjects)
                    ProjectVS.SetCurrentConfiguration(project, "Release");
            }

            //Запускаем парсер
            CSharpMain csParser = new CSharpMain(storage);
            csParser.Run();

            //Помечаем уже отработанные стадии и сохраняем настройки
            PluginSettings.IsStage1Completed = PluginSettings.IsStage1;
            PluginSettings.IsStage2Completed = PluginSettings.IsStage2;
            SavePluginData();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.BuiltSourcesDirectoryPath = PluginSettings.BuiltSourcesDirectoryPath;
            Properties.Settings.Default.FirstSensorNumber = PluginSettings.FirstSensorNumber;
            Properties.Settings.Default.IsLevel2 = PluginSettings.IsLevel2;
            Properties.Settings.Default.IsLevel3 = PluginSettings.IsLevel3;
            Properties.Settings.Default.UnixMode = PluginSettings.UnixMode;
            Properties.Settings.Default.InsufficientProcessing = PluginSettings.ProcessInsufficient;
            Properties.Settings.Default.InsufficietnDefines = PluginSettings.InsufficientDefines;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.BuiltSourcesDirectoryPath);
            settingsBuffer.Add(PluginSettings.FirstSensorNumber);
            settingsBuffer.Add(PluginSettings.IsLevel2);
            settingsBuffer.Add(PluginSettings.UnixMode);
            settingsBuffer.Add(PluginSettings.ProcessInsufficient);
            settingsBuffer.Add(PluginSettings.InsufficientDefines);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    Monitor.Tasks.Task.FromEnum(PluginSettings.Name, PluginSettings.Tasks.FILES_PARSING),
                    Monitor.Tasks.Task.FromEnum(PluginSettings.Name, PluginSettings.Tasks.FIRST_STAGE_PROJECT_PROCESSING),
                    Monitor.Tasks.Task.FromEnum(PluginSettings.Name, PluginSettings.Tasks.FIRST_STAGE_FILES_PROCESSING),
                    Monitor.Tasks.Task.FromEnum(PluginSettings.Name, PluginSettings.Tasks.SECOND_STAGE_PROJECT_PROCESSING),
                    Monitor.Tasks.Task.FromEnum(PluginSettings.Name, PluginSettings.Tasks.SECOND_STAGE_FILES_PROCESSING)
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

		{
            get
            {
			    return null;
            }
		}

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new CustomSettings(
                    storage.sensors.EnumerateSensors().Count() == 0 
                    ? 1 
                    : storage.sensors.EnumerateSensors().Max(s => s.ID) + 1)
                    );
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new SimpleSettings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;
            
            if (settings != null)
                settings.Save();

            if (String.IsNullOrWhiteSpace(PluginSettings.BuiltSourcesDirectoryPath))
            {
                message = "Директория с собранными исходными текстами не задана.";
                return false;
            }

            if (!Directory.Exists(PluginSettings.BuiltSourcesDirectoryPath))
            {
                message = "Директория с собранными исходными текстами <" + PluginSettings.BuiltSourcesDirectoryPath + "> не существует.";
                return false;
            }

            var builtDir = PluginSettings.BuiltSourcesDirectoryPath[PluginSettings.BuiltSourcesDirectoryPath.Length - 1] == '\\' ? PluginSettings.BuiltSourcesDirectoryPath : PluginSettings.BuiltSourcesDirectoryPath + "\\";
            List<string> srcProj = Directory.EnumerateFiles(storage.appliedSettings.FileListPath, "*.csproj", SearchOption.AllDirectories).ToList();
            Dictionary<string, bool> dirtyProj = Directory.EnumerateFiles(PluginSettings.BuiltSourcesDirectoryPath, "*.csproj", SearchOption.AllDirectories).ToDictionary(g => g.ToLower(),g => true);
            if (srcProj.FirstOrDefault(g => !dirtyProj.ContainsKey(g.Replace(storage.appliedSettings.FileListPath, builtDir).ToLower())) != null)
            {
                message = "Директория с собранными исходными текстами <" + PluginSettings.BuiltSourcesDirectoryPath + "> не корректна.";
                return false;
            }
            
            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }             

        #endregion

        private void LoadPluginData()
        {
            ////считываем массив байт из файла
            byte[] data = File.ReadAllBytes(storage.pluginData.GetDataFileName(PluginSettings.ID));

            //Если файл данных плагина пуст, или содрежит недостаточно информации
            if (data.Length != 2)
            {
                if (data.Length != 0)
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Файл данных плагина повреждён. Не удалось загрузить битовый массив.");
                return;
            }

            PluginSettings.IsStage1Completed = data[0] != 0;
            PluginSettings.IsStage2Completed = data[1] != 0;
        }

        private void SavePluginData()
        {
            File.WriteAllBytes(storage.pluginData.GetDataFileName(PluginSettings.ID), new byte[] { (byte)(int)(PluginSettings.IsStage1Completed ? 1 : 0), (byte)(int)(PluginSettings.IsStage2Completed ? 1 : 0) });
        }
    }
}
