﻿namespace IA.Plugins.Parsers.CSharpParser
{
    internal partial class CustomSettings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.sensors_groupBox = new System.Windows.Forms.GroupBox();
            this.firstSensorNumber_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.firstSensorNumber_label = new System.Windows.Forms.Label();
            this.firstSensorNumber_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.prefixes_groupBox = new System.Windows.Forms.GroupBox();
            this.prefixes_dataGridView = new System.Windows.Forms.DataGridView();
            this.oldPrefix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.newPrefix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.projectsConfigurations_groupBox = new System.Windows.Forms.GroupBox();
            this.projectsConfigurations_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.edit_save_button = new System.Windows.Forms.Button();
            this.level_groupBox = new System.Windows.Forms.GroupBox();
            this.level_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isLevel3_radioButton = new System.Windows.Forms.RadioButton();
            this.isLevel2_radioButton = new System.Windows.Forms.RadioButton();
            this.chkUnixMode = new System.Windows.Forms.CheckBox();
            this.insufficientGroupBox = new System.Windows.Forms.GroupBox();
            this.insufficientFiles_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.insufficientFiles_checkBox = new System.Windows.Forms.CheckBox();
            this.insufficientFiles_label = new System.Windows.Forms.Label();
            this.defines_textBox = new System.Windows.Forms.TextBox();
            this.sensors_groupBox.SuspendLayout();
            this.firstSensorNumber_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).BeginInit();
            this.prefixes_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prefixes_dataGridView)).BeginInit();
            this.main_tableLayoutPanel.SuspendLayout();
            this.projectsConfigurations_groupBox.SuspendLayout();
            this.projectsConfigurations_tableLayoutPanel.SuspendLayout();
            this.level_groupBox.SuspendLayout();
            this.level_tableLayoutPanel.SuspendLayout();
            this.insufficientGroupBox.SuspendLayout();
            this.insufficientFiles_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sensors_groupBox
            // 
            this.sensors_groupBox.Controls.Add(this.firstSensorNumber_tableLayoutPanel);
            this.sensors_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensors_groupBox.Location = new System.Drawing.Point(3, 113);
            this.sensors_groupBox.Name = "sensors_groupBox";
            this.sensors_groupBox.Size = new System.Drawing.Size(684, 49);
            this.sensors_groupBox.TabIndex = 6;
            this.sensors_groupBox.TabStop = false;
            this.sensors_groupBox.Text = "Расстановка датчиков";
            // 
            // firstSensorNumber_tableLayoutPanel
            // 
            this.firstSensorNumber_tableLayoutPanel.ColumnCount = 2;
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.Controls.Add(this.firstSensorNumber_label, 0, 0);
            this.firstSensorNumber_tableLayoutPanel.Controls.Add(this.firstSensorNumber_numericUpDown, 1, 0);
            this.firstSensorNumber_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstSensorNumber_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.firstSensorNumber_tableLayoutPanel.Name = "firstSensorNumber_tableLayoutPanel";
            this.firstSensorNumber_tableLayoutPanel.RowCount = 1;
            this.firstSensorNumber_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.Size = new System.Drawing.Size(678, 30);
            this.firstSensorNumber_tableLayoutPanel.TabIndex = 6;
            // 
            // firstSensorNumber_label
            // 
            this.firstSensorNumber_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_label.AutoSize = true;
            this.firstSensorNumber_label.Location = new System.Drawing.Point(3, 8);
            this.firstSensorNumber_label.Name = "firstSensorNumber_label";
            this.firstSensorNumber_label.Size = new System.Drawing.Size(131, 13);
            this.firstSensorNumber_label.TabIndex = 1;
            this.firstSensorNumber_label.Text = "Номер первого датчика:";
            // 
            // firstSensorNumber_numericUpDown
            // 
            this.firstSensorNumber_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_numericUpDown.Location = new System.Drawing.Point(140, 5);
            this.firstSensorNumber_numericUpDown.Name = "firstSensorNumber_numericUpDown";
            this.firstSensorNumber_numericUpDown.Size = new System.Drawing.Size(535, 20);
            this.firstSensorNumber_numericUpDown.TabIndex = 0;
            // 
            // prefixes_groupBox
            // 
            this.prefixes_groupBox.Controls.Add(this.prefixes_dataGridView);
            this.prefixes_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prefixes_groupBox.Location = new System.Drawing.Point(3, 168);
            this.prefixes_groupBox.Name = "prefixes_groupBox";
            this.prefixes_groupBox.Size = new System.Drawing.Size(684, 155);
            this.prefixes_groupBox.TabIndex = 7;
            this.prefixes_groupBox.TabStop = false;
            this.prefixes_groupBox.Text = "Замена префиксов путей, указанных в проекте";
            // 
            // prefixes_dataGridView
            // 
            this.prefixes_dataGridView.AllowUserToAddRows = false;
            this.prefixes_dataGridView.AllowUserToResizeColumns = false;
            this.prefixes_dataGridView.AllowUserToResizeRows = false;
            this.prefixes_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.prefixes_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.prefixes_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.oldPrefix,
            this.newPrefix});
            this.prefixes_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prefixes_dataGridView.Location = new System.Drawing.Point(3, 16);
            this.prefixes_dataGridView.Name = "prefixes_dataGridView";
            this.prefixes_dataGridView.Size = new System.Drawing.Size(678, 136);
            this.prefixes_dataGridView.TabIndex = 0;
            this.prefixes_dataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.prefixes_dataGridView_CellEndEdit);
            this.prefixes_dataGridView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.prefixes_dataGridView_MouseDoubleClick);
            // 
            // oldPrefix
            // 
            this.oldPrefix.HeaderText = "Старый префикс";
            this.oldPrefix.Name = "oldPrefix";
            // 
            // newPrefix
            // 
            this.newPrefix.HeaderText = "Новый префикс";
            this.newPrefix.Name = "newPrefix";
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.prefixes_groupBox, 0, 3);
            this.main_tableLayoutPanel.Controls.Add(this.sensors_groupBox, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.projectsConfigurations_groupBox, 0, 4);
            this.main_tableLayoutPanel.Controls.Add(this.level_groupBox, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.insufficientGroupBox, 0, 5);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 6;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(690, 650);
            this.main_tableLayoutPanel.TabIndex = 8;
            // 
            // projectsConfigurations_groupBox
            // 
            this.projectsConfigurations_groupBox.Controls.Add(this.projectsConfigurations_tableLayoutPanel);
            this.projectsConfigurations_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectsConfigurations_groupBox.Location = new System.Drawing.Point(3, 329);
            this.projectsConfigurations_groupBox.Name = "projectsConfigurations_groupBox";
            this.projectsConfigurations_groupBox.Size = new System.Drawing.Size(684, 236);
            this.projectsConfigurations_groupBox.TabIndex = 9;
            this.projectsConfigurations_groupBox.TabStop = false;
            this.projectsConfigurations_groupBox.Text = "Настройка конфигураций проектов";
            // 
            // projectsConfigurations_tableLayoutPanel
            // 
            this.projectsConfigurations_tableLayoutPanel.ColumnCount = 1;
            this.projectsConfigurations_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.projectsConfigurations_tableLayoutPanel.Controls.Add(this.edit_save_button, 0, 0);
            this.projectsConfigurations_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectsConfigurations_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.projectsConfigurations_tableLayoutPanel.Name = "projectsConfigurations_tableLayoutPanel";
            this.projectsConfigurations_tableLayoutPanel.RowCount = 2;
            this.projectsConfigurations_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.projectsConfigurations_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.projectsConfigurations_tableLayoutPanel.Size = new System.Drawing.Size(678, 217);
            this.projectsConfigurations_tableLayoutPanel.TabIndex = 0;
            // 
            // edit_save_button
            // 
            this.edit_save_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.edit_save_button.Location = new System.Drawing.Point(3, 6);
            this.edit_save_button.Name = "edit_save_button";
            this.edit_save_button.Size = new System.Drawing.Size(672, 23);
            this.edit_save_button.TabIndex = 0;
            this.edit_save_button.Tag = "FALSE";
            this.edit_save_button.Text = "Редактировать";
            this.edit_save_button.UseVisualStyleBackColor = true;
            this.edit_save_button.Click += new System.EventHandler(this.edit_save_button_Click);
            // 
            // level_groupBox
            // 
            this.level_groupBox.Controls.Add(this.level_tableLayoutPanel);
            this.level_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_groupBox.Location = new System.Drawing.Point(3, 58);
            this.level_groupBox.Name = "level_groupBox";
            this.level_groupBox.Size = new System.Drawing.Size(684, 49);
            this.level_groupBox.TabIndex = 6;
            this.level_groupBox.TabStop = false;
            this.level_groupBox.Text = "Уровень вставки датчиков";
            // 
            // level_tableLayoutPanel
            // 
            this.level_tableLayoutPanel.ColumnCount = 3;
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Controls.Add(this.isLevel3_radioButton, 1, 0);
            this.level_tableLayoutPanel.Controls.Add(this.isLevel2_radioButton, 0, 0);
            this.level_tableLayoutPanel.Controls.Add(this.chkUnixMode, 2, 0);
            this.level_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.level_tableLayoutPanel.Name = "level_tableLayoutPanel";
            this.level_tableLayoutPanel.RowCount = 1;
            this.level_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Size = new System.Drawing.Size(678, 30);
            this.level_tableLayoutPanel.TabIndex = 2;
            // 
            // isLevel3_radioButton
            // 
            this.isLevel3_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isLevel3_radioButton.AutoSize = true;
            this.isLevel3_radioButton.Location = new System.Drawing.Point(103, 6);
            this.isLevel3_radioButton.Name = "isLevel3_radioButton";
            this.isLevel3_radioButton.Size = new System.Drawing.Size(94, 17);
            this.isLevel3_radioButton.TabIndex = 1;
            this.isLevel3_radioButton.TabStop = true;
            this.isLevel3_radioButton.Text = "3-й уровень";
            this.isLevel3_radioButton.UseVisualStyleBackColor = true;
            // 
            // isLevel2_radioButton
            // 
            this.isLevel2_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isLevel2_radioButton.AutoSize = true;
            this.isLevel2_radioButton.Location = new System.Drawing.Point(3, 6);
            this.isLevel2_radioButton.Name = "isLevel2_radioButton";
            this.isLevel2_radioButton.Size = new System.Drawing.Size(94, 17);
            this.isLevel2_radioButton.TabIndex = 0;
            this.isLevel2_radioButton.TabStop = true;
            this.isLevel2_radioButton.Text = "2-й уровень";
            this.isLevel2_radioButton.UseVisualStyleBackColor = true;
            // 
            // chkUnixMode
            // 
            this.chkUnixMode.AutoSize = true;
            this.chkUnixMode.Location = new System.Drawing.Point(203, 3);
            this.chkUnixMode.Name = "chkUnixMode";
            this.chkUnixMode.Size = new System.Drawing.Size(72, 17);
            this.chkUnixMode.TabIndex = 2;
            this.chkUnixMode.Text = "*nix mode";
            this.chkUnixMode.UseVisualStyleBackColor = true;
            // 
            // insufficientGroupBox
            // 
            this.insufficientGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.insufficientGroupBox.Controls.Add(this.insufficientFiles_tableLayoutPanel);
            this.insufficientGroupBox.Location = new System.Drawing.Point(3, 571);
            this.insufficientGroupBox.Name = "insufficientGroupBox";
            this.insufficientGroupBox.Size = new System.Drawing.Size(684, 76);
            this.insufficientGroupBox.TabIndex = 10;
            this.insufficientGroupBox.TabStop = false;
            this.insufficientGroupBox.Text = "Файлы без проектов";
            // 
            // insufficientFiles_tableLayoutPanel
            // 
            this.insufficientFiles_tableLayoutPanel.ColumnCount = 3;
            this.insufficientFiles_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.insufficientFiles_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.insufficientFiles_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.insufficientFiles_tableLayoutPanel.Controls.Add(this.insufficientFiles_checkBox, 0, 0);
            this.insufficientFiles_tableLayoutPanel.Controls.Add(this.insufficientFiles_label, 1, 0);
            this.insufficientFiles_tableLayoutPanel.Controls.Add(this.defines_textBox, 2, 0);
            this.insufficientFiles_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.insufficientFiles_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.insufficientFiles_tableLayoutPanel.Name = "insufficientFiles_tableLayoutPanel";
            this.insufficientFiles_tableLayoutPanel.RowCount = 1;
            this.insufficientFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.insufficientFiles_tableLayoutPanel.Size = new System.Drawing.Size(678, 57);
            this.insufficientFiles_tableLayoutPanel.TabIndex = 0;
            // 
            // insufficientFiles_checkBox
            // 
            this.insufficientFiles_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.insufficientFiles_checkBox.AutoSize = true;
            this.insufficientFiles_checkBox.Location = new System.Drawing.Point(3, 20);
            this.insufficientFiles_checkBox.Name = "insufficientFiles_checkBox";
            this.insufficientFiles_checkBox.Size = new System.Drawing.Size(94, 17);
            this.insufficientFiles_checkBox.TabIndex = 0;
            this.insufficientFiles_checkBox.Text = "Обработка";
            this.insufficientFiles_checkBox.UseVisualStyleBackColor = true;
            // 
            // insufficientFiles_label
            // 
            this.insufficientFiles_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.insufficientFiles_label.AutoSize = true;
            this.insufficientFiles_label.Location = new System.Drawing.Point(103, 22);
            this.insufficientFiles_label.Name = "insufficientFiles_label";
            this.insufficientFiles_label.Size = new System.Drawing.Size(94, 13);
            this.insufficientFiles_label.TabIndex = 1;
            this.insufficientFiles_label.Text = "Константы";
            // 
            // defines_textBox
            // 
            this.defines_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.defines_textBox.Location = new System.Drawing.Point(203, 18);
            this.defines_textBox.Name = "defines_textBox";
            this.defines_textBox.Size = new System.Drawing.Size(472, 20);
            this.defines_textBox.TabIndex = 2;
            // 
            // CustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(690, 650);
            this.sensors_groupBox.ResumeLayout(false);
            this.firstSensorNumber_tableLayoutPanel.ResumeLayout(false);
            this.firstSensorNumber_tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstSensorNumber_numericUpDown)).EndInit();
            this.prefixes_groupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.prefixes_dataGridView)).EndInit();
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.projectsConfigurations_groupBox.ResumeLayout(false);
            this.projectsConfigurations_tableLayoutPanel.ResumeLayout(false);
            this.level_groupBox.ResumeLayout(false);
            this.level_tableLayoutPanel.ResumeLayout(false);
            this.level_tableLayoutPanel.PerformLayout();
            this.insufficientGroupBox.ResumeLayout(false);
            this.insufficientFiles_tableLayoutPanel.ResumeLayout(false);
            this.insufficientFiles_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox sensors_groupBox;
        private System.Windows.Forms.Label firstSensorNumber_label;
        private System.Windows.Forms.NumericUpDown firstSensorNumber_numericUpDown;
        private System.Windows.Forms.GroupBox prefixes_groupBox;
        private System.Windows.Forms.DataGridView prefixes_dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldPrefix;
        private System.Windows.Forms.DataGridViewTextBoxColumn newPrefix;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.GroupBox projectsConfigurations_groupBox;
        private System.Windows.Forms.GroupBox level_groupBox;
        private System.Windows.Forms.RadioButton isLevel3_radioButton;
        private System.Windows.Forms.RadioButton isLevel2_radioButton;
        private System.Windows.Forms.TableLayoutPanel firstSensorNumber_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel projectsConfigurations_tableLayoutPanel;
        private System.Windows.Forms.Button edit_save_button;
        private System.Windows.Forms.TableLayoutPanel level_tableLayoutPanel;
        private System.Windows.Forms.CheckBox chkUnixMode;
        private System.Windows.Forms.GroupBox insufficientGroupBox;
        private System.Windows.Forms.TableLayoutPanel insufficientFiles_tableLayoutPanel;
        private System.Windows.Forms.CheckBox insufficientFiles_checkBox;
        private System.Windows.Forms.Label insufficientFiles_label;
        private System.Windows.Forms.TextBox defines_textBox;
    }
}
