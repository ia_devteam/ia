﻿/*
 * Плагин Парсер C#
 * Файл Parser.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Головинов, Юхтин
 * 
 * Плагин предназначен для разбора файлов на языке C# с целью сбора информации о функциональных и информационных объектах,
 * ветвлениях и связях между отдельными файлами с последующим занесением в Хранилище. После сбора всей необходимой информации
 * плагином производится расстановка датчиков для последующего сбора трасс.
 */

using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Reflection;
using ICSharpCode.SharpDevelop.Dom;
using ICSharpCode.SharpDevelop.Dom.CSharp;
using ICSharpCode.SharpDevelop.Dom.NRefactoryResolver;
using Store;
using ICSharpCode.NRefactory.Ast;
using ICSharpCode.NRefactory.PrettyPrinter;

using IMethod = Store.IMethod;
using Location = ICSharpCode.NRefactory.Location;

namespace IA.Plugins.Parsers.CSharpParser
{
    /// <summary>
    /// Класс парсера
    /// </summary>
    public class Parser
    {
        #region Fields

        readonly Storage storage; //Хранилище
        private readonly TreeView tree; //дерево разбора
        ulong sensorNumber; //номер текущего датчика
        /// <summary>
        /// Вставка датчиков по 2му уровню
        /// </summary>
        public bool SensorFlag;
        /// <summary>
        /// Вставка датчиков по 3му уровню
        /// </summary>
        public bool ThirdSensor;

        IFile currentFile;          //путь к текущему обрабатываемому файлу
        Namespace currNamespace;    //текущий нэймспэйс
        Class currClass;            //текущий класс
        IMethod currMethod;         //текущий метод

        int isGetOrSetorSetGet; // 0 == get, 1 == set, 2 == setget - данное поле указывает на то что происходит с переменной в текущем выражении

        ulong lastDingo; //FIXME временная переменная для проверки корректности вставки датчивов - потом выпилить

        IOperation currentOperation; //данное поле служит для формирования операции перед внесением её в стэйтмент

        Location currLoc;

        /// <summary>
        /// Список всех встретившихся в коде делегатов
        /// </summary>
        static private List<string> delegateDeclList = new List<string>();

        /// <summary>
        /// Переменная для автомтической нумерации сгенерированных переменных
        /// </summary>
        private static ulong generatedVariableNumber = 0;

        private NRefactoryResolver resolver;

        private bool inInvocation; //флаг, который показывает находимся ли мы внутри вызова функции
        private readonly Stack<ResolvedElem> stack = new Stack<ResolvedElem>(); //стек для хранения вызовов функция напрямую или по ссылке (в стеке лежат только функции или указатели)

        private string IACSharpSensorCallText = null;

        #endregion

        public static void clearStatic()
        {
            delegateDeclList = new List<string>();
            generatedVariableNumber = 0;
        }


        #region Properties

        /// <summary>
        /// Свойство. Номер текущего датчика для вставки
        /// </summary>
        public ulong SensorNumber
        {
            get { return sensorNumber; }
            set
            {
                sensorNumber = value;
                lastDingo = value + 1;
            }
        }

        CompilationUnit unit;
        /// <summary>
        /// Свойство.
        /// </summary>
        public CompilationUnit Unit
        {
            get
            {
                return unit;
            }
            set
            {
                unit = value;
                UpdateTree();
            }
        }

        ParseInformation parseInformation;
        /// <summary>
        /// Свойство.
        /// </summary>
        public ParseInformation ParseInformation
        {
            get
            {
                return parseInformation;
            }
            set
            {
                parseInformation = value;
            }
        }

        string contents;
        /// <summary>
        /// Свойство. Содержимое обрабатываемого файла.
        /// </summary>
        public string Contents
        {
            get
            {
                return contents;
            }
            set
            {
                contents = value;
            }
        }

        string filePath;
        /// <summary>
        /// Свойство. Полный путь обрабатываемого файла
        /// </summary>
        public string FilePath
        {
            get
            {
                return filePath;
            }
            set
            {
                filePath = value;
            }
        }
        #endregion

        #region Classes
        class ResolvedElem
        {
            private readonly bool isDelegate; //делегат
            private readonly bool isLocalVariable; //локальная переменная или параметр
            private readonly bool isDom; //неймспейс или класс
            private readonly IMember memberType; //тип члена
            private readonly string fullDotNetName; //полное имя
            private readonly Location exprLoc;

            /// <summary>
            /// Свойство. Определяет, является ли элемент делагатом.
            /// </summary>
            public bool IsDelegate
            {
                get { return isDelegate; }
            }

            /// <summary>
            /// Свойство. Определяет какую роль в классе играет элемент (поле, метод, и т.д.)
            /// </summary>
            public IMember MemberType
            {
                get { return memberType; }
            }

            /// <summary>
            /// Свойство. Определяет полное разрешённое имя элемента
            /// </summary>
            public string FullDotNetName
            {
                get { return fullDotNetName; }
            }

            /// <summary>
            /// Свойство. Определяет, является ли элемент локальной переменной.
            /// </summary>
            public bool IsLocalVariable
            {
                get { return isLocalVariable; }
            }
            /// <summary>
            /// Свойство.
            /// </summary>
            public bool IsDom
            {
                get { return isDom; }
            }

            /// <summary>
            /// Свойство. Определяет месторасположение элемента в коде.
            /// </summary>
            public Location ExprLoc
            {
                get { return exprLoc; }
            }

            /// <summary>
            /// Конструктор класса
            /// </summary>
            /// <param name="memberType">Параметр определяет роль элемента</param>
            /// <param name="fullDotNetName">Параметр определяет полное имя создаваемого элемента</param>
            /// <param name="exprLoc">Параметро определяет месторасположение создаваемого элемента в коде</param>
            /// <param name="isLocalVariable">Параметр определяет, является ли создаваемый элемент локальной переменной.</param>
            /// <param name="isDom">Параметр определяет </param>
            /// <param name="isDelegate">Параметр определяет, является ли создаваемый элемент делегатом.</param>
            public ResolvedElem(IMember memberType, string fullDotNetName, Location exprLoc, bool isLocalVariable = false, bool isDom = false, bool isDelegate = false)
            {
                this.memberType = memberType;
                this.fullDotNetName = fullDotNetName;
                this.exprLoc = exprLoc;
                this.isLocalVariable = isLocalVariable;
                this.isDom = isDom;
                this.isDelegate = isDelegate;
            }
        }
        #endregion

        ~Parser()
        {
            stack.Clear();
            resolver = null;
            unit = null;
            tree.Dispose();
            parseInformation = null;
            contents = null;
            filePath = null;
        }

        /// <summary>
        /// Вставлять датчики по 2му уровню
        /// </summary>
        public void setSecond()
        {
            SensorFlag = true;
            ThirdSensor = false;
        }

        /// <summary>
        /// Вставлять датчики по 3му уровню
        /// </summary>
        public void setThird()
        {
            SensorFlag = false;
            ThirdSensor = true;
        }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="storage">Параметро определяет Хранилище с которым ведётся работа</param>
        public Parser(Storage storage)
        {
            tree = new TreeView();
            this.storage = storage;
            SensorFlag = false;
            ThirdSensor = false;

            isGetOrSetorSetGet = 0; //стандартный случай - гет
            currentFile = null;
            currNamespace = null;
            currClass = null;
            currMethod = null;
            currentOperation = null;

            IACSharpSensorCallText = "IACSharpSensor.IACSharpSensor.SensorReached";

            resolver = new NRefactoryResolver(LanguageProperties.CSharp);
        }

        /// <summary>
        /// Функция обновляет содержимое дерева.
        /// </summary>
        private void UpdateTree()
        {
            tree.Nodes.Clear();
            tree.Nodes.Add(new CollectionNode("CompilationUnit", unit.Children));
            tree.SelectedNode = tree.Nodes[0];
        }

        /// <summary>
        /// Функция создана для получения возвращаемого значения функции, в которой находится указанный в качестве параметра элемент дерева.
        /// Данная функция используется в частности для разрешения ситуаций вида "yield return;", чтобы понять тип возвращаемого значения для добавления в перечисление.
        /// </summary>
        /// <param name="node">Произвольная вершина дерева кода.</param>
        /// <returns>Тип возвращаемого значения функции в которой находится вершина дерева, переданная в качестве параметра</returns>
        private string GetParentType(INode node)
        {
            string ret;

            while (!(node is MethodDeclaration || node is PropertyDeclaration || node is DelegateDeclaration))
            {
                if (node is ConstructorDeclaration || node is DestructorDeclaration)
                {
                    ret = "var";
                    return ret;
                }

                node = node.Parent;
                if (node == null) return "var";
            }
            if (node is MethodDeclaration)
            {
                MethodDeclaration md = (MethodDeclaration)node;
                ret = md.TypeReference.ToString();
            }
            else
            {
                if (node is PropertyDeclaration)
                {
                    ret = (node as PropertyDeclaration).TypeReference.ToString();
                }
                else
                {
                    ret = (node as DelegateDeclaration).ReturnType.ToString();
                }
            }
            if (ret == "" || ret == " ") ret = "var";
            return ret;
        }

        //функция формирует узел дерева, соответстующий вхождению датчика
        private ExpressionStatement dingo()
        {
            if (lastDingo == sensorNumber)
                throw new Exception("Рассинхронизация при вставке датчиков в дерево и Хранилище");

            ExpressionStatement st = new ExpressionStatement(new InvocationExpression(new IdentifierExpression(IACSharpSensorCallText)));
            ((InvocationExpression)(st.Expression)).Arguments.Add(new PrimitiveExpression((int)sensorNumber));

            lastDingo = sensorNumber;
            return st;
        }

        //функция получает первый элемент цепочки стейтментов и выдаёт последний
        private IStatement GetLastStatement(IStatement statement)
        {
            IStatement tmp = statement;

            if (statement == null)
                return null;

            while (tmp.NextInLinearBlock != null)
                tmp = tmp.NextInLinearBlock;

            return tmp;
        }

        /// <summary>
        /// Функция для подсчёта смещения относительно начала файла.
        /// </summary>
        /// <param name="fileContents">Содержимое файла</param>
        /// <param name="line">Строка для подсчёта смещения</param>
        /// <param name="column">Столбец для подсчёта смещения</param>
        /// <returns>Смещение относительно начала файла</returns>
        public int GetOffset(ref string fileContents, int line, int column)
        {
            if (fileContents == null) throw new ArgumentNullException("fileContents");

            //так как в тексте строки считаются с 1, а в массиве они лежат с нуля
            int _line = line - 1;
            int _column = column - 1;

            int result = -1;
            int fileLength = fileContents.Length;

            for (int i = 0; i < _line; i++)
            {
                result = fileContents.IndexOf('\n', result + 1);
                if (result == -1 || result + 1 >= fileLength)
                    return -1;
            }

            result += _column + 1;

            return result;
        }

        /// <summary>
        /// Функция с которой начинается первый проход, её задача запустить рекурсивный обход кода 1-го этапа
        /// </summary>
        public void firstStep()
        {
            currentFile = storage.files.FindOrGenerate(filePath, enFileKindForAdd.fileWithPrefix);
            Walker_1stage(tree.Nodes);
        }

        /// <summary>
        /// Функция с которой начинается первый проход, её задача запустить рекурсивный обход кода 2-го этапа
        /// </summary>
        /// <returns>Сгенерированный по дереву код со вставленными датчиками.</returns>
        public string secondStep(Dictionary<string, string> unicode)
        {
            currentFile = storage.files.FindOrGenerate(filePath, enFileKindForAdd.fileWithPrefix);
            Walker_2stage(tree.Nodes);
            var code = new StringBuilder(GenerateCode(new CSharpOutputVisitor()));
            if (unicode.Keys.Count > 0)
            {
                foreach (var key in unicode.Keys)
                    code = code.Replace(key, unicode[key]);
                //страшно, ужас, 2 строки!
            }
            return code.ToString();
        }

        /// <summary>
        /// Функция реализующая рекурсивный обходчик 1-го этапа, использует рекурсивный обход узлов дерева 1-го этапа
        /// </summary>
        /// <param name="nodes">Указатель на вершину дерева, сгенерированного по коду.</param>
        public void Walker_1stage(TreeNodeCollection nodes)
        {
            //на входе у нас дерево - ходим по верзним уровням
            foreach (TreeNode node in nodes)
            {
                if (node is ElementNode)
                {
                    walkNode_1stage(node);
                }
                else if (node is CollectionNode)
                {
                    Walker_1stage(node.Nodes);
                }
                else
                {
                    throw new Exception("В дереве присутствует нераспознанный элемент.");
                }
            }
        }

        /// <summary>
        /// Функция реализующая рекурсивный обходчик 2-го этапа, использует рекурсивный обход узлов дерева 2-го этапа
        /// </summary>
        /// <param name="nodes">Указатель на вершину дерева, сгенерированного по коду.</param>
        public void Walker_2stage(TreeNodeCollection nodes)
        {
            //на входе у нас дерево - ходим по верзним уровням
            foreach (TreeNode node in nodes)
            {
                if (node is ElementNode)
                {
                    walkNode_2stage(node);
                }
                else if (node is CollectionNode)
                {
                    Walker_2stage(node.Nodes);
                }
                else
                {
                    throw new Exception("В дереве присутствует нераспознанный элемент.");
                }
            }
        }

        /// <summary>
        /// Функция реализует рекурсивный обход узлов дерева 1-го этапа.
        /// </summary>
        /// <param name="node">Указатель на вершину дерева, сгенерированного по коду.</param>
        public void walkNode_1stage(TreeNode node)
        {
            //ELEMENT NODE only
            ElementNode nod = (ElementNode)node;
            switch (nod.element.GetType().UnderlyingSystemType.FullName)
            {
                case "ICSharpCode.NRefactory.Ast.NamespaceDeclaration":
                    {
                        //объявление пространства имён
                        NamespaceDeclaration elem = (NamespaceDeclaration)(nod.element);

                        //задаём предыдущий нэймспейс
                        Namespace prevNamespace = currNamespace;

                        //разбиваем неймспейс, на отдельные имена вложенных неймспейсов
                        string[] namespaces = elem.Name.Split('.');

                        Namespace innerNamespace = prevNamespace;
                        foreach (string ns in namespaces)
                            innerNamespace = innerNamespace != null ? innerNamespace.FindOrCreateSubNamespace(ns) : storage.namespaces.FindOrCreate(ns);

                        currNamespace = innerNamespace;

                        for (int i = 0; i < nod.Nodes.Count; i++)
                            if (nod.Nodes[i] is CollectionNode)
                                foreach (TreeNode n in nod.Nodes[i].Nodes)
                                    walkNode_1stage(n);

                        currNamespace = prevNamespace;

                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.TypeDeclaration":
                    {
                        //объявление класса, интерфейса, структуры, модуля или перечислимого типа

                        TypeDeclaration elem = (TypeDeclaration)(nod.element);

                        Class prevClass = currClass;
                        currClass = null;

                        foreach (Class search in storage.classes.GetClass(elem.Name))
                        {
                            if (currNamespace != null && (search.FullName.StartsWith(currNamespace.FullName + ".") || search.FullName.Contains("." + currNamespace.FullName + ".")))
                            {
                                if (prevClass == null || search.FullName.Contains(prevClass.FullName))
                                {
                                    currClass = search;
                                    break;
                                }
                            }
                            else
                            {
                                if (search.FullName == elem.Name)
                                {
                                    currClass = search;
                                    break;
                                }
                            }
                        }
                        if (currClass == null) //а то мы дико множим одни и те же классы FIXME ПРОВЕРИТЬ РАБОТОСПОСОБНОСТЬ!
                        {
                            currClass = storage.classes.AddClass();
                            currClass.Name = elem.Name;


                            //добавляем класс к неймспейсу
                            if (currNamespace != null)
                                currNamespace.AddClassToNamespace(currClass);
                            //else
                            //    IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружен класс вне какого либо пространства имён."); //sith happens

                            //если текущий класс вложен в предыдущий
                            if (prevClass != null)
                                prevClass.AddInnerClass(currClass);
                        }

                        for (int i = 0; i < nod.Nodes.Count; i++)
                            if (nod.Nodes[i] is CollectionNode)
                                foreach (TreeNode n in nod.Nodes[i].Nodes)
                                    walkNode_1stage(n);

                        currClass = prevClass;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.MethodDeclaration":
                    {
                        //метод
                        MethodDeclaration elem = (MethodDeclaration)(nod.element);
                        IMethod prev = currMethod;
                        if (currClass != null)
                        {
                            currMethod = currClass.AddMethod(false);
                            currMethod.Name = elem.Name;
                            if (prev != null)
                                prev.AddInnerFunction(currMethod);
                            currMethod.AddDefinition(currentFile, (ulong)elem.StartLocation.Line, (ulong)elem.StartLocation.Column);
                        }
                        else
                        {
                            if (currNamespace != null)
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружен метод вне какого либо класса, пренадлежащий пространству имен: " + currNamespace.FullName + ".");
                            else
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружен метод вне какого либо класса или пространства имён.");
                        }

                        currMethod = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.ConstructorDeclaration":
                    {
                        //конструктор
                        ConstructorDeclaration elem = (ConstructorDeclaration)(nod.element);
                        IMethod prev = currMethod;
                        if (currClass != null)
                        {
                            currMethod = currClass.AddMethod(false);
                            currMethod.Name = elem.Name;
                            if (prev != null)
                                prev.AddInnerFunction(currMethod);
                            currMethod.AddDefinition(currentFile, (ulong)elem.StartLocation.Line, (ulong)elem.StartLocation.Column);
                        }
                        else
                        {
                            IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружен конструктор вне какого либо класса.");
                        }

                        currMethod = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.DestructorDeclaration":
                    {
                        //деструктор
                        DestructorDeclaration elem = (DestructorDeclaration)(nod.element);
                        IMethod prev = currMethod;
                        if (currClass != null)
                        {
                            currMethod = currClass.AddMethod(false);
                            currMethod.Name = "~" + elem.Name;
                            if (prev != null)
                                prev.AddInnerFunction(currMethod);
                            currMethod.AddDefinition(currentFile, (ulong)elem.StartLocation.Line, (ulong)elem.StartLocation.Column);
                        }
                        else
                        {
                            IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружен диструктор вне какого либо класса.");
                        }
                        currMethod = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.FieldDeclaration":
                    {
                        FieldDeclaration elem = (FieldDeclaration)(nod.element);


                        if (currClass != null)
                        {
                            foreach (VariableDeclaration varDecl in elem.Fields)
                            {
                                if (varDecl.StartLocation.Column != 0) //не заходим внутрь перечислений
                                {
                                    Field field = currClass.AddField(false); //FIX ME!!! вместо фолс правильно указать какое поле статическое а какое нет
                                    field.Name = varDecl.Name;
                                    field.AddDefinition(currentFile, (ulong)varDecl.StartLocation.Line, (ulong)varDecl.StartLocation.Column, null);
                                    field = field;
                                }
                            }
                        }
                        else if (currNamespace != null)
                        {
                            foreach (VariableDeclaration varDecl in elem.Fields)
                            {
                                Variable variable = storage.variables.AddVariable();
                                variable.Name = varDecl.Name;
                                variable.AddDefinition(currentFile, (ulong)varDecl.StartLocation.Line, (ulong)varDecl.StartLocation.Column, null);
                                currNamespace.AddVariableToNamespace(variable);
                            }
                        }
                        else
                        {
                            IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружено объявление поля вне какого либо класса или пространства имён.");//ох ты ж, кто-то использует букву ё...
                        }
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.DelegateDeclaration":
                    {
                        //здесь был убитый код
                        //на первом этапе мы не делаем ничего

                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.EventDeclaration":
                    {
                        EventDeclaration elem = (EventDeclaration)(nod.element);
                        IMethod prevMethod = currMethod;

                        if (currClass != null)
                        {
                            currMethod = currClass.AddMethod(false);
                            currMethod.Name = elem.Name;
                            if (prevMethod != null)
                                prevMethod.AddInnerFunction(currMethod);
                            currMethod.AddDefinition(currentFile, (ulong)elem.StartLocation.Line, (ulong)elem.StartLocation.Column);
                        }
                        else
                        {
                            IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружено объявление эвента вне какого либо класса.");
                        }

                        currMethod = prevMethod;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.OperatorDeclaration": //так же как и метод класса
                    {
                        OperatorDeclaration elem = (OperatorDeclaration)(nod.element);
                        IMethod prev = currMethod;
                        if (currClass != null)
                        {
                            currMethod = currClass.AddMethod(false);
                            currMethod.Name = elem.Name;
                            if (prev != null)
                                prev.AddInnerFunction(currMethod);
                            currMethod.AddDefinition(currentFile, (ulong)elem.StartLocation.Line, (ulong)elem.StartLocation.Column);
                        }
                        else
                        {
                            IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружена перегрузка оператора вне какого либо класса.");
                        }

                        currMethod = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.PropertyDeclaration":
                    {
                        PropertyDeclaration elem = (PropertyDeclaration)(nod.element);

                        if (currClass != null)
                        {
                            //поле класса в данном случае не добавляется

                            if (elem.HasGetRegion)
                            {
                                IMethod methodGet = currClass.AddMethod(false);
                                methodGet.Name = elem.Name + "_get";
                                methodGet.AddDefinition(currentFile, (ulong)elem.StartLocation.Line, (ulong)elem.StartLocation.Column);
                            }
                            if (elem.HasSetRegion)
                            {
                                IMethod methodSet = currClass.AddMethod(false);
                                methodSet.Name = elem.Name + "_set";
                                methodSet.AddDefinition(currentFile, (ulong)elem.StartLocation.Line, (ulong)elem.StartLocation.Column);
                            }
                        }
                        else
                        {
                            IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружено объявление свойства вне какого либо класса.");
                        }
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.UsingDeclaration": break; // с этим просто ничего не делаем
                case "ICSharpCode.NRefactory.Ast.OptionDeclaration":
                    {
                        //nod = nod;
                        break; //этого в дереве нет
                    }
                case "ICSharpCode.NRefactory.Ast.VariableDeclaration":
                    {
                        //nod = nod;
                        break; //этого в дереве нет
                    }
                case "ICSharpCode.NRefactory.Ast.DeclareDeclaration":
                    {
                        //nod = nod;
                        break; //этого в дереве нет
                    }
                default:
                    {
                        var fullName = nod.element.GetType().UnderlyingSystemType.FullName;
                        if (fullName != null && fullName.EndsWith("Declaration"))
                            IA.Monitor.Log.Warning(PluginSettings.Name, "Не обработано объявление: " + nod.element.GetType().UnderlyingSystemType.FullName);
                        break;
                    }

            }
        }

        /// <summary>
        /// Функция реализует рекурсивный обход узлов дерева 2-го этапа.
        /// </summary>
        /// <param name="node">Указатель на вершину дерева, сгенерированного по коду.</param>
        public void walkNode_2stage(TreeNode node)
        {
            //ELEMENT NODE only
            ElementNode nod = (ElementNode)node;
            switch (nod.element.GetType().UnderlyingSystemType.FullName)
            {
                case "ICSharpCode.NRefactory.Ast.NamespaceDeclaration":
                    {
                        //класс
                        //тип
                        NamespaceDeclaration elem = (NamespaceDeclaration)(nod.element);
                        Namespace prev = currNamespace;
                        currNamespace = storage.namespaces.FindOrCreate(elem.Name);
                        for (int i = 0; i < nod.Nodes.Count; i++)
                            if (nod.Nodes[i] is CollectionNode)
                                foreach (TreeNode n in nod.Nodes[i].Nodes)
                                    walkNode_2stage(n);
                        currNamespace = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.TypeDeclaration":
                    {
                        //класс
                        //тип
                        TypeDeclaration elem = (TypeDeclaration)(nod.element);
                        Class prev = currClass;
                        currClass = null;
                        foreach (Class search in storage.classes.GetClass(elem.Name))
                        {
                            if (currNamespace != null && (search.FullName.StartsWith(currNamespace.FullName + ".") || search.FullName.Contains("." + currNamespace.FullName + ".")))
                            {
                                if (prev == null || search.FullName.Contains(prev.FullName))
                                {
                                    currClass = search;
                                    break;
                                }
                            }
                            else
                            {
                                if (search.FullName == elem.Name || prev != null && search.FullName.Contains(prev.FullName))
                                {
                                    currClass = search;
                                    break;
                                }
                            }
                        }
                        if (currClass == null)
                            throw new Exception("Не найден класс, ранее помещённый в Хранилище.");
                        for (int i = 0; i < nod.Nodes.Count; i++)
                            if (nod.Nodes[i] is CollectionNode)
                                foreach (TreeNode n in nod.Nodes[i].Nodes)
                                    walkNode_2stage(n);
                        currClass = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.MethodDeclaration":
                    {
                        //метод
                        MethodDeclaration elem = (MethodDeclaration)(nod.element);
                        IMethod prev = currMethod;
                        currMethod = currMethod = FindMethodInStorage(elem.Name, elem.StartLocation);

                        IStatement ret = walkBody(elem.Body, true);
                        if (ret != null)
                            if (currMethod != null)
                            {
                                currMethod.EntryStatement = ret;
                            }
                        currMethod = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.ConstructorDeclaration":
                    {
                        //конструктор
                        ConstructorDeclaration elem = (ConstructorDeclaration)(nod.element);
                        IMethod prev = currMethod;
                        currMethod = FindMethodInStorage(elem.Name, elem.StartLocation);

                        IStatement ret = walkBody(elem.Body, true);
                        if (ret != null)
                            if (currMethod != null)
                            {
                                currMethod.EntryStatement = ret;
                            }
                        currMethod = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.DestructorDeclaration":
                    {
                        //деструктор
                        DestructorDeclaration elem = (DestructorDeclaration)(nod.element);
                        IMethod prev = currMethod;
                        currMethod = FindMethodInStorage("~" + elem.Name, elem.StartLocation);

                        IStatement ret = walkBody(elem.Body, true);
                        if (ret != null)
                            if (currMethod != null)
                            {
                                currMethod.EntryStatement = ret;
                            }
                        currMethod = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.FieldDeclaration":
                    {
                        FieldDeclaration elem = (FieldDeclaration)(nod.element);
                        foreach (VariableDeclaration fld in elem.Fields)
                        {
                            if (fld.Initializer == null) continue;
                            if (fld.StartLocation.Column == 0) continue; //enum

                            Variable find = null;
                            foreach (Variable search in storage.variables.GetVariable(fld.Name))
                            {
                                if (search.FullName.StartsWith(currClass.FullName))
                                {
                                    find = search;
                                    break;
                                }
                                if (currClass == null && search.FullName.StartsWith(currNamespace.FullName))
                                {
                                    find = search;
                                    break;
                                }
                            }
                            if (find != null)
                            {
                                if (delegateDeclList.Contains(fld.TypeReference.Type))
                                {
                                    //мы в объявлении делегата, которому сразу же присваевается функция
                                    //на самом деле стейтмент тут и не надо заводить, но и так пойдет
                                    if (fld.Initializer is ObjectCreateExpression)
                                    {
                                        ObjectCreateExpression ocrt = fld.Initializer as ObjectCreateExpression;
                                        if (ocrt.CreateType.Type == fld.TypeReference.Type)
                                        {
                                            if (ocrt.Parameters.Count == 1 && ocrt.Parameters[0] is IdentifierExpression)
                                            {
                                                IdentifierExpression tempExp = ocrt.Parameters[0] as IdentifierExpression;

                                                var ret = walkExpr(tempExp);

                                                if (ret.Count == 0)
                                                    IA.Monitor.Log.Warning(PluginSettings.Name, "Не была найдена функция для делегата 1го типа в файле " +
                                                                    currentFile.FileNameForReports + " в позиции " +
                                                                    fld.StartLocation.ToString());
                                                //теперь tempOp должно содержать функции
                                                foreach (Object o in ret)
                                                    if (o is IFunction[])
                                                        foreach (IFunction fu in (o as IFunction[]))
                                                        {
                                                            PointsToFunction ptf = find.AddPointsToFunctions();
                                                            ptf.function = fu.Id;
                                                            ptf.AddPosition(currentFile,
                                                                            (ulong)
                                                                            fld.StartLocation.Line,
                                                                            (ulong)
                                                                            fld.StartLocation.Column,
                                                                            currMethod);
                                                            ptf.Save();
                                                        }

                                                //functionsCallList.Clear();
                                            }
                                            else
                                            {
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Новая разновидность делегата 1го типа в файле " +
                                                                currentFile.FileNameForReports + " в позиции " +
                                                                fld.StartLocation.ToString());
                                            }
                                        }
                                        else
                                        {
                                            IA.Monitor.Log.Warning(PluginSettings.Name, "Новая разновидность делегата 1го типа в файле " +
                                                            currentFile.FileNameForReports + " в позиции " +
                                                            fld.StartLocation.ToString());
                                        }
                                    }
                                    if (fld.Initializer is AnonymousMethodExpression)
                                    {
                                        AnonymousMethodExpression ame = fld.Initializer as AnonymousMethodExpression;
                                        IFunction amFunc = storage.functions.AddFunction();
                                        IStatement ret = walkBody(ame.Body);
                                        if (ret != null)
                                        {
                                            /*if (ThirdSensor)
                                            {
                                                ame.Body.Children = AddFirstSensor(ame.Body.Children);
                                                storage.sensors.AddSensorBeforeStatement(sensorNumber++, ret, Kind.START);
                                                IStatement lst = GetLastStatement(ret);
                                                if (!(lst is IStatementReturn || lst is IStatementYieldReturn))
                                                {
                                                    ame.Body.Children = AddLastSensor(ame.Body.Children);
                                                    IStatementOperational tmp = storage.statements.AddStatementOperational(lst.LastSymbolLocation.GetOffset() + (ulong)1, currentFile);
                                                    lst.NextInLinearBlock = tmp;
                                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, lst, Kind.FINAL);
                                                }
                                            }*/
                                        }
                                        amFunc.EntryStatement = ret;

                                        PointsToFunction ptf = find.AddPointsToFunctions();
                                        ptf.function = amFunc.Id;
                                        ptf.AddPosition(currentFile, (ulong)fld.StartLocation.Line, (ulong)fld.StartLocation.Column, currMethod);
                                        ptf.Save();
                                    }
                                }
                                else
                                {
                                    walkExpr(fld.Initializer); //просто обрабатываем гетсеты всех переменных
                                }
                            }
                            else
                            {
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Не нашел поле " + fld.Name);
                            }
                        }
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.DelegateDeclaration":
                    {
                        DelegateDeclaration elem = (DelegateDeclaration)(nod.element);

                        delegateDeclList.Add(elem.Name); //добавляем в список делегатов еще один кандидат, чтобы потом если что в нём его найти
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.EventDeclaration":
                    {
                        //разбираем как метод
                        EventDeclaration elem = (EventDeclaration)(nod.element);

                        FindMethodInStorage(elem.Name, elem.StartLocation);

                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.OperatorDeclaration":
                    {
                        //разбираем как метод
                        OperatorDeclaration elem = (OperatorDeclaration)(nod.element);
                        IMethod prev = currMethod;
                        currMethod = FindMethodInStorage(elem.Name, elem.StartLocation);

                        IStatement ret = walkBody(elem.Body, true);
                        if (ret != null)
                            if (currMethod != null)
                            {
                                currMethod.EntryStatement = ret;
                            }
                        currMethod = prev;
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.PropertyDeclaration":
                    {
                        //так же как метод
                        PropertyDeclaration elem = (PropertyDeclaration)(nod.element);

                        if (elem.HasGetRegion)
                        {
                            IMethod prev = currMethod;
                            currMethod = FindMethodInStorage(elem.Name + "_get", elem.StartLocation);

                            IStatement ret = walkBody(elem.GetRegion.Block, true);
                            if (ret != null)
                                if (currMethod != null)
                                {
                                    currMethod.EntryStatement = ret;
                                }
                            currMethod = prev;
                        }

                        if (elem.HasSetRegion)
                        {
                            IMethod prev = currMethod;
                            currMethod = FindMethodInStorage(elem.Name + "_set", elem.StartLocation);

                            IStatement ret = walkBody(elem.SetRegion.Block, true);
                            if (ret != null)
                                if (currMethod != null)
                                {
                                    currMethod.EntryStatement = ret;
                                }
                            currMethod = prev;
                        }
                        break;
                    }
                case "ICSharpCode.NRefactory.Ast.UsingDeclaration": break; //с этим ничего не делаем
                case "ICSharpCode.NRefactory.Ast.VariableDeclaration":
                    {
                        //nod = nod;
                        break; //этого в дереве нет
                    }
                case "ICSharpCode.NRefactory.Ast.DeclareDeclaration":
                    {
                        //nod = nod;
                        break; //этого в дереве нет
                    }
                case "ICSharpCode.NRefactory.Ast.OptionDeclaration":
                    {
                        //nod = nod;
                        break; //этого в дереве нет
                    }
                default:
                    {
                        break;
                    }

            }
        }

        /// <summary>
        /// Найти в Хранилище соответствующий данному элементу метод: у него должно быть такое же место определения. При ошибке ругается в Monitor.Warning.
        /// </summary>
        /// <param name="elemName">Имя метода для поиска в Хранилище.</param>
        /// <param name="elemStartLocation">Место определения, которым должен обладать искомый метод.</param>
        /// <returns></returns>
        private IMethod FindMethodInStorage(string elemName, Location elemStartLocation)
        {
            foreach (IFunction search in storage.functions.GetFunction(elemName))
            {
                //Если до парсера C# работал другой парсер и внес функции, не являющиеся методами, и они по имени совпали с нашим методом, 
                //то их надо пропустить
                var searched = search as IMethod;
                if (searched == null)
                    continue;

                if (searched != null && searched.Definition() != null)
                {
                    Store.Location loc = searched.Definition();
                    if (loc.GetFile() == currentFile)
                        if (loc.GetLine() == ((ulong)elemStartLocation.Line))
                            if (loc.GetColumn() == (ulong)elemStartLocation.Column)
                            {
                                return searched;
                            }
                }
            }

            IA.Monitor.Log.Warning(PluginSettings.Name, "Не удалось найти функцию : " + elemName);
            return null;
        }

        private List<INode> AddFirstSensor(List<INode> elems)
        {
            List<INode> old = elems;
            elems = new List<INode>();
            elems.Add(dingo());
            elems.AddRange(old);
            return elems;
        }

        private List<INode> AddLastSensor(List<INode> elems)
        {
            elems.Add(dingo());
            return elems;
        }

        //функция обхода сложных узлов на обоих проходах
        private IStatement walkBody(BlockStatement block, bool endSensor = false, int levelWalk = 1)
        {
            IStatement entry = null;

            BlockStatement blNew = new BlockStatement();

            //обработка детей
            //bool 
            Location endLoc = new Location(-1, -1);
            foreach (INode node in block.Children)
            {
                IStatement last;
                endLoc = node.EndLocation;
                switch (node.GetType().UnderlyingSystemType.Name)
                {
                    #region Expression
                    case "ExpressionStatement":
                        {

                            ExpressionStatement expressionNode = (ExpressionStatement)node;

                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                if (!(last is IStatementOperational))
                                {
                                    last = last.NextInLinearBlock = storage.statements.AddStatementOperational((ulong)expressionNode.Expression.StartLocation.Line, (ulong)expressionNode.Expression.StartLocation.Column, currentFile);
                                    currentOperation = storage.operations.AddOperation();
                                    if (SensorFlag)
                                    {
                                        blNew.Children.Add(dingo());
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                    }
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementOperational((ulong)expressionNode.Expression.StartLocation.Line, (ulong)expressionNode.Expression.StartLocation.Column, currentFile);
                                currentOperation = storage.operations.AddOperation();
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }

                            blNew.Children.Add(node);

                            var ret = walkExpr(expressionNode.Expression);
                            foreach (object o in ret)
                            {
                                if (o is IFunction[])
                                    currentOperation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                else
                                    if (o is Variable[] && (o as Variable[]).Count() == 1)
                                        currentOperation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                    else
                                        if (o is Variable[] && (o as Variable[]).Count() > 1)
                                            IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                            }
                            ((IStatementOperational)last).Operation = currentOperation;
                            ((IStatementOperational)last).SetLastSymbolLocation(currentFile, (ulong)expressionNode.Expression.EndLocation.Line, (ulong)expressionNode.Expression.EndLocation.Column);
                            break;
                        }
                    #endregion
                    #region Block
                    case "BlockStatement":
                        {
                            IStatement ret = walkBody((BlockStatement)node, false, levelWalk + 1);

                            //
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = ret;
                                if (SensorFlag && ret != null)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                if (ret == null)
                                {
                                    ret = storage.statements.AddStatementOperational((ulong)node.StartLocation.Line, (ulong)node.StartLocation.Line, currentFile);
                                }
                                entry = last = ret;
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.START);
                                }
                            }
                            blNew.Children.Add(node);
                            //
                            //last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);
                            break;
                        }
                    #endregion
                    #region LocalVariable
                    case "LocalVariableDeclaration":
                        {
                            isGetOrSetorSetGet = 1;
                            LocalVariableDeclaration localVariableNode = (LocalVariableDeclaration)node;


                            foreach (VariableDeclaration varDecl in localVariableNode.Variables)
                            {
                                Variable inVar = storage.variables.AddVariable();
                                inVar.Name = varDecl.Name;
                                inVar.AddDefinition(currentFile, (ulong)varDecl.StartLocation.Line, (ulong)varDecl.StartLocation.Column, currMethod);
                                if (varDecl.Initializer != null)
                                    inVar.AddValueSetPosition(currentFile, (ulong)varDecl.StartLocation.Line, (ulong)varDecl.StartLocation.Column, currMethod);

                                last = GetLastStatement(entry);
                                if (last != null)
                                {
                                    if (!(last is IStatementOperational))
                                    {
                                        last = last.NextInLinearBlock = storage.statements.AddStatementOperational((ulong)localVariableNode.StartLocation.Line, (ulong)localVariableNode.StartLocation.Column, currentFile);
                                        currentOperation = storage.operations.AddOperation();
                                        if (SensorFlag)
                                        {
                                            blNew.Children.Add(dingo());
                                            storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                        }
                                    }
                                }
                                else
                                {
                                    entry = last = storage.statements.AddStatementOperational((ulong)localVariableNode.StartLocation.Line, (ulong)localVariableNode.StartLocation.Column, currentFile);
                                    currentOperation = storage.operations.AddOperation();
                                    if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                    {
                                        blNew.Children.Add(dingo());
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                    }
                                }


                                // если это делегат то обрабатываем как делегат иначе как обычную локальную переменную
                                if (delegateDeclList.Contains(localVariableNode.TypeReference.Type))
                                {
                                    //на самом деле стейтмент тут и не надо заводить, но и так пойдет
                                    if (varDecl.Initializer is ObjectCreateExpression)
                                    {
                                        ObjectCreateExpression ocrt = varDecl.Initializer as ObjectCreateExpression;
                                        if (ocrt.CreateType.Type == localVariableNode.TypeReference.Type)
                                        {
                                            if (ocrt.Parameters.Count == 1 && ocrt.Parameters[0] is IdentifierExpression)
                                            {
                                                IdentifierExpression tempExp = ocrt.Parameters[0] as IdentifierExpression;

                                                var ret = walkExpr(tempExp);

                                                if (ret.Count == 0)
                                                    IA.Monitor.Log.Warning(PluginSettings.Name, "Не была найдена функция для делегата 1го типа в файле " +
                                                            currentFile.FileNameForReports + " в позиции " +
                                                            varDecl.StartLocation.ToString());
                                                //теперь tempOp должно содержать функции
                                                foreach (Object o in ret)
                                                    if (o is IFunction[])
                                                        foreach (IFunction fu in (o as IFunction[]))
                                                        {
                                                            PointsToFunction ptf = inVar.AddPointsToFunctions();
                                                            ptf.function = fu.Id;
                                                            ptf.AddPosition(currentFile,
                                                                            (ulong)
                                                                            localVariableNode.StartLocation.Line,
                                                                            (ulong)
                                                                            localVariableNode.StartLocation.Column,
                                                                            currMethod);
                                                            ptf.Save();
                                                        }

                                                //functionsCallList.Clear();
                                            }
                                            else
                                            {
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Новая разновидность делегата 1го типа в файле " +
                                                            currentFile.FileNameForReports + " в позиции " +
                                                            varDecl.StartLocation.ToString());
                                            }
                                        }
                                        else
                                        {
                                            IA.Monitor.Log.Warning(PluginSettings.Name, "Новая разновидность делегата 1го типа в файле " +
                                                            currentFile.FileNameForReports + " в позиции " +
                                                            varDecl.StartLocation.ToString());
                                        }
                                    }
                                    if (varDecl.Initializer is AnonymousMethodExpression)
                                    {
                                        AnonymousMethodExpression ame = varDecl.Initializer as AnonymousMethodExpression;
                                        IFunction amFunc = storage.functions.AddFunction();
                                        IStatement ret = walkBody(ame.Body, false, levelWalk + 1);
                                        if (ret != null)
                                        {
                                            amFunc.EntryStatement = ret;
                                        }

                                        PointsToFunction ptf = inVar.AddPointsToFunctions();
                                        ptf.function = amFunc.Id;
                                        ptf.AddPosition(currentFile, (ulong)localVariableNode.StartLocation.Line, (ulong)localVariableNode.StartLocation.Column, currMethod);
                                        ptf.Save();
                                    }
                                }
                                else
                                {
                                    var ret = walkExpr(varDecl.Initializer);
                                    foreach (object o in ret)
                                    {
                                        if (o is IFunction[])
                                            currentOperation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                        else
                                            if (o is Variable[] && (o as Variable[]).Count() == 1)
                                                currentOperation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                            else
                                                if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                    IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                    }
                                    ((IStatementOperational)last).Operation = currentOperation;
                                }

                            }
                            blNew.Children.Add(node);
                            //
                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);
                            isGetOrSetorSetGet = 0;
                            break;
                        }
                    #endregion
                    #region Empty
                    case "EmptyStatement":
                        {
                            blNew.Children.Add(node);
                            EmptyStatement emSt = (EmptyStatement)node;
                            //заполнение хранилища
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementOperational((ulong)emSt.StartLocation.Line, (ulong)emSt.StartLocation.Column, currentFile);
                                currentOperation = storage.operations.AddOperation();
                                ((IStatementOperational)last).Operation = currentOperation;
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementOperational((ulong)emSt.StartLocation.Line, (ulong)emSt.StartLocation.Column, currentFile);
                                currentOperation = storage.operations.AddOperation();
                                ((IStatementOperational)last).Operation = currentOperation;
                            }

                            //Головинов: у стейтментов, наверное, надо проставить смещения, но пока не до этого
                            //end
                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);
                            break;
                        }
                    #endregion
                    #region Label
                    case "LabelStatement":
                        {
                            blNew.Children.Add(node);
                            break;
                        }
                    #endregion
                    #region Break
                    case "BreakStatement":
                        {
                            BreakStatement breakNode = (BreakStatement)node;


                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementBreak((ulong)breakNode.StartLocation.Line, (ulong)breakNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = storage.statements.AddStatementBreak((ulong)breakNode.StartLocation.Line, (ulong)breakNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }
                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Continue
                    case "ContinueStatement":
                        {
                            ContinueStatement continueNode = (ContinueStatement)node;


                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementContinue((ulong)continueNode.StartLocation.Line, (ulong)continueNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = storage.statements.AddStatementContinue((ulong)continueNode.StartLocation.Line, (ulong)continueNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }
                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Goto
                    case "GotoStatement":
                        {
                            GotoStatement gotoNode = (GotoStatement)node;

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementGoto((ulong)gotoNode.StartLocation.Line, (ulong)gotoNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = storage.statements.AddStatementGoto((ulong)gotoNode.StartLocation.Line, (ulong)gotoNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Goto Case
                    case "GotoCaseStatement":
                        {
                            //пока пропускаем - тут со стейтментами какая-то жесткотека намечается

                            GotoCaseStatement gotoNode = (GotoCaseStatement)node;

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementGoto((ulong)gotoNode.StartLocation.Line, (ulong)gotoNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = storage.statements.AddStatementGoto((ulong)gotoNode.StartLocation.Line, (ulong)gotoNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }
                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Throw
                    case "ThrowStatement":
                        {

                            ThrowStatement throwNode = (ThrowStatement)node;

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementThrow((ulong)throwNode.StartLocation.Line, (ulong)throwNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.FINAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementThrow((ulong)throwNode.StartLocation.Line, (ulong)throwNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }

                            //Exception
                            if (!throwNode.Expression.IsNull)
                            {
                                IOperation operation = storage.operations.AddOperation();
                                var ret = walkExpr(throwNode.Expression);
                                foreach (object o in ret)
                                {
                                    if (o is IFunction[])
                                        operation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                    else
                                        if (o is Variable[] && (o as Variable[]).Count() == 1)
                                            operation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                        else
                                            if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                }

                                if (operation != null)
                                    ((IStatementThrow)last).Exception = operation;
                            }

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region If-Else
                    case "IfElseStatement":
                        {


                            IfElseStatement ifNode = (IfElseStatement)node;

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementIf((ulong)ifNode.StartLocation.Line, (ulong)ifNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementIf((ulong)ifNode.StartLocation.Line, (ulong)ifNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }

                            //Condition
                            if (!ifNode.Condition.IsNull)
                            {
                                IOperation operation = storage.operations.AddOperation();

                                var ret = walkExpr(ifNode.Condition);
                                foreach (object o in ret)
                                {
                                    if (o is IFunction[])
                                        operation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                    else
                                        if (o is Variable[] && (o as Variable[]).Count() == 1)
                                            operation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                        else
                                            if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                }
                                if (operation != null)
                                    ((IStatementIf)last).Condition = operation;
                            }

                            //Then Block
                            if (ifNode.TrueStatement != null)
                            {
                                if (ifNode.TrueStatement != null && ifNode.TrueStatement.Count > 0)
                                {
                                    if (!(ifNode.TrueStatement[0] is BlockStatement))
                                    {
                                        BlockStatement tempBlc = new BlockStatement();
                                        tempBlc.Children.Add(ifNode.TrueStatement[0]);
                                        tempBlc.Parent = ifNode.TrueStatement[0].Parent;
                                        ifNode.TrueStatement[0] = tempBlc;
                                    }

                                    IStatement ifTrueStatement = walkBody((BlockStatement)ifNode.TrueStatement[0], false, levelWalk + 1);
                                    if (ifTrueStatement != null)
                                        ((IStatementIf)last).ThenStatement = ifTrueStatement;
                                }
                            }


                            //Else Block
                            IStatement ifFalseStatement = null;
                            if (ifNode.FalseStatement != null && ifNode.FalseStatement.Count > 0)
                            {
                                if (ifNode.FalseStatement != null)
                                {
                                    if (!(ifNode.FalseStatement[0] is BlockStatement))
                                    {
                                        BlockStatement tempBlc = new BlockStatement();
                                        tempBlc.Children.Add(ifNode.FalseStatement[0]);
                                        tempBlc.Parent = ifNode.FalseStatement[0].Parent;
                                        ifNode.FalseStatement[0] = tempBlc;
                                    }
                                    ifFalseStatement = walkBody((BlockStatement)ifNode.FalseStatement[0], false, levelWalk + 1);
                                }
                            }
                            // если простой иф то тут уже определено что иф что элс.. иначе сначала мутим мутки с элсифами

                            //Else If
                            //а вот тут вот начинается жесть со стейтментами
                            //cюда мы не заходим если элсифов нету
                            List<ElseIfSection> elseIfFull = new List<ElseIfSection>(); // те же секции элсифов, только с блоками внутри
                            if (ifNode.HasElseIfSections)
                            {
                                //цикл ниже нужен для того чтобы внутри элсифов были только блокстэйтменты
                                foreach (ElseIfSection elseIf in ifNode.ElseIfSections)
                                {
                                    if (!(elseIf.EmbeddedStatement is BlockStatement))
                                    {
                                        BlockStatement bodyBlc = new BlockStatement();
                                        bodyBlc.Children.Add(elseIf.EmbeddedStatement);
                                        bodyBlc.Parent = elseIf.EmbeddedStatement.Parent;
                                        elseIf.EmbeddedStatement = bodyBlc;
                                    }
                                    elseIfFull.Add(elseIf);
                                }


                                //начиная с последнего элсифа начинаем разворачивать дерево ифов
                                IStatementIf previousIfStatement = null;

                                for (int i = elseIfFull.Count - 1; i >= 0; i--)
                                {
                                    //создаём новый стейтмент
                                    IStatementIf currentIfStatement = storage.statements.AddStatementIf((ulong)elseIfFull[i].StartLocation.Line, (ulong)elseIfFull[i].StartLocation.Column, currentFile);

                                    //Condition
                                    if (!elseIfFull[i].Condition.IsNull)
                                    {
                                        IOperation operation = storage.operations.AddOperation();
                                        var ret = walkExpr(elseIfFull[i].Condition);
                                        foreach (object o in ret)
                                        {
                                            if (o is IFunction[])
                                                operation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                            else
                                                if (o is Variable[] && (o as Variable[]).Count() == 1)
                                                    operation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                                else
                                                    if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                        IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                        }

                                        if (operation != null)
                                            ((IStatementIf)last).Condition = operation;
                                    }

                                    //Then Block
                                    IStatement thenBlockStatement =
                                        walkBody((BlockStatement)elseIfFull[i].EmbeddedStatement, false, levelWalk + 1);
                                    if (thenBlockStatement != null)
                                        currentIfStatement.ThenStatement = thenBlockStatement;

                                    //Else Block
                                    if (i != elseIfFull.Count - 1) // это не самый последний элсиф 
                                    {
                                        if (previousIfStatement != null)
                                            currentIfStatement.ElseStatement = previousIfStatement;
                                    }
                                    else //это последний иф
                                    {
                                        if (ifFalseStatement != null)
                                            currentIfStatement.ElseStatement = ifFalseStatement;
                                    }

                                    previousIfStatement = currentIfStatement;
                                }
                                if (previousIfStatement != null) // если элсифи таки были
                                    ((IStatementIf)last).ElseStatement = previousIfStatement;
                            }
                            else //если элсифов нету
                                if (ifFalseStatement != null)
                                    ((IStatementIf)last).ElseStatement = ifFalseStatement;

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region For
                    case "ForStatement":
                        {


                            ForStatement forNode = (ForStatement)node;

                            //добавляем стейтмент и датчик(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementFor((ulong)forNode.StartLocation.Line, (ulong)forNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementFor((ulong)forNode.StartLocation.Line, (ulong)forNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }

                            //Init
                            if (forNode.Initializers.Count != 0)
                            {
                                BlockStatement forInitBlock = new BlockStatement();
                                foreach (Statement child in forNode.Initializers)
                                {
                                    forInitBlock.Children.Add(child);
                                    forInitBlock.Parent = forNode;
                                }

                                Boolean oldFlag = SensorFlag;
                                SensorFlag = false;
                                IStatementOperational iso = (IStatementOperational)walkBody(forInitBlock, false, levelWalk + 1);
                                SensorFlag = oldFlag;

                                if (iso != null && iso.Operation != null)
                                    ((IStatementFor)last).Start = iso.Operation;
                            }

                            //Condition
                            if (!forNode.Condition.IsNull)
                            {
                                IOperation operation = storage.operations.AddOperation();
                                var ret = walkExpr(forNode.Condition);
                                foreach (object o in ret)
                                {
                                    if (o is IFunction[])
                                        operation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                    else
                                        if (o is Variable[] && (o as Variable[]).Count() == 1)
                                            operation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                        else
                                            if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                }

                                if (operation != null)
                                    ((IStatementFor)last).Condition = operation;
                            }

                            //Init
                            if (forNode.Iterator.Count != 0)
                            {
                                BlockStatement forIterBlock = new BlockStatement();
                                foreach (Statement child in forNode.Iterator)
                                {
                                    forIterBlock.Children.Add(child);
                                    forIterBlock.Parent = forNode;
                                }

                                Boolean oldFlag = SensorFlag;
                                SensorFlag = false;
                                IStatementOperational iso = (IStatementOperational)walkBody(forIterBlock, false, levelWalk + 1);
                                SensorFlag = oldFlag;

                                if (iso != null && iso.Operation != null)
                                    ((IStatementFor)last).Iteration = iso.Operation;
                            }

                            //Body
                            if (forNode.EmbeddedStatement != null)
                            {
                                if (!(forNode.EmbeddedStatement is BlockStatement))
                                {
                                    BlockStatement tempBlc = new BlockStatement();
                                    tempBlc.Children.Add(forNode.EmbeddedStatement);
                                    tempBlc.Parent = forNode.EmbeddedStatement.Parent;
                                    forNode.EmbeddedStatement = tempBlc;
                                }

                                IStatement forBodyStatement = walkBody((BlockStatement)forNode.EmbeddedStatement, false, levelWalk + 1);
                                if (forBodyStatement != null)
                                    ((IStatementFor)last).Body = forBodyStatement;
                            }

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Foreach
                    case "ForeachStatement":
                        {


                            ForeachStatement forEachNode = (ForeachStatement)node;

                            //добавляем стейтмент и датчик(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementForEach((ulong)forEachNode.StartLocation.Line, (ulong)forEachNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementForEach((ulong)forEachNode.StartLocation.Line, (ulong)forEachNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }

                            //Head
                            if (!forEachNode.Expression.IsNull)
                            {
                                IOperation operation = storage.operations.AddOperation();
                                var ret = walkExpr(forEachNode.Expression);
                                foreach (object o in ret)
                                {
                                    if (o is IFunction[])
                                        operation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                    else
                                        if (o is Variable[] && (o as Variable[]).Count() == 1)
                                            operation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                        else
                                            if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                }

                                if (operation != null)
                                    ((IStatementForEach)last).Head = operation;
                            }

                            //Body
                            if (forEachNode.EmbeddedStatement != null)
                            {
                                if (!(forEachNode.EmbeddedStatement is BlockStatement))
                                {
                                    BlockStatement tempBlc = new BlockStatement();
                                    tempBlc.Children.Add(forEachNode.EmbeddedStatement);
                                    tempBlc.Parent = forEachNode.EmbeddedStatement.Parent;
                                    forEachNode.EmbeddedStatement = tempBlc;
                                }

                                IStatement forEachBodyStatement = walkBody((BlockStatement)forEachNode.EmbeddedStatement, false, levelWalk + 1);
                                if (forEachBodyStatement != null)
                                    ((IStatementForEach)last).Body = forEachBodyStatement;
                            }
                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Do-While
                    case "DoLoopStatement":
                        {


                            DoLoopStatement doWhileNode = (DoLoopStatement)node;

                            //добавляем стейтмент и датчик(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementDoAndWhile((ulong)doWhileNode.StartLocation.Line, (ulong)doWhileNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementDoAndWhile((ulong)doWhileNode.StartLocation.Line, (ulong)doWhileNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }

                            ((IStatementDoAndWhile)last).IsCheckConditionBeforeFirstRun = (doWhileNode.ConditionPosition == ConditionPosition.Start);

                            //Condition
                            if (!doWhileNode.Condition.IsNull)
                            {
                                IOperation operation = storage.operations.AddOperation();
                                var ret = walkExpr(doWhileNode.Condition);
                                foreach (object o in ret)
                                {
                                    if (o is IFunction[])
                                        operation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                    else
                                        if (o is Variable[] && (o as Variable[]).Count() == 1)
                                            operation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                        else
                                            if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                }
                                if (operation != null)
                                    ((IStatementDoAndWhile)last).Condition = operation;
                            }

                            //Body
                            if (doWhileNode.EmbeddedStatement != null)
                            {
                                if (!(doWhileNode.EmbeddedStatement is BlockStatement))
                                {
                                    BlockStatement tempBlc = new BlockStatement();
                                    tempBlc.Children.Add(doWhileNode.EmbeddedStatement);
                                    tempBlc.Parent = doWhileNode.EmbeddedStatement.Parent;
                                    doWhileNode.EmbeddedStatement = tempBlc;
                                }

                                IStatement doWhileBodyStatement = walkBody((BlockStatement)doWhileNode.EmbeddedStatement, false, levelWalk + 1);
                                if (doWhileBodyStatement != null)
                                    ((IStatementDoAndWhile)last).Body = doWhileBodyStatement;
                            }
                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Return
                    case "ReturnStatement":
                        {


                            ReturnStatement returnNode = (ReturnStatement)node;
                            ReturnStatement saveReturnNode = returnNode;

                            string type = GetParentType(node);

                            //FIXME - айццкий хак, но вот такое может быть в дереве, я тут не при чём
                            type = type.Replace(">+", ">.");
                            if (type.Contains("System.Void")) type = type.Replace("System.Void", "void ");

                            //if (type.Contains("<T>")) //VS это дико не нравится
                            //    type = "var"; 

                            if (!(returnNode.Expression.IsNull) && (!(returnNode.Expression is PrimitiveExpression && (returnNode.Expression as PrimitiveExpression).StringValue == "null"))) //если непустой реторн
                            {
                                if (SensorFlag || ThirdSensor)
                                {
                                    LocalVariableDeclaration lvd = new LocalVariableDeclaration(new VariableDeclaration("RNTRNTRNT_" + (++generatedVariableNumber).ToString(), returnNode.Expression, new TypeReference(type)));
                                    blNew.Children.Add(lvd);
                                    blNew.Children.Add(dingo());

                                    returnNode = new ReturnStatement(new IdentifierExpression("RNTRNTRNT_" + generatedVariableNumber.ToString())) { Parent = saveReturnNode.Parent };
                                }

                                blNew.Children.Add(returnNode);
                            }
                            else //если пустой реторн
                            {
                                if (SensorFlag || ThirdSensor)
                                    blNew.Children.Add(dingo());
                                blNew.Children.Add(node);
                            }

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementReturn((ulong)saveReturnNode.StartLocation.Line, (ulong)saveReturnNode.StartLocation.Column, currentFile, (ulong)saveReturnNode.EndLocation.Line, (ulong)saveReturnNode.EndLocation.Column);
                                if (SensorFlag || ThirdSensor)
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.FINAL);
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementReturn((ulong)saveReturnNode.StartLocation.Line, (ulong)saveReturnNode.StartLocation.Column, currentFile, (ulong)saveReturnNode.EndLocation.Line, (ulong)saveReturnNode.EndLocation.Column);
                                if (SensorFlag || ThirdSensor)
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.START);
                            }

                            //Return Operation
                            if (!saveReturnNode.Expression.IsNull)
                            {
                                IOperation operation = storage.operations.AddOperation();
                                var ret = walkExpr(saveReturnNode.Expression);
                                foreach (object o in ret)
                                {
                                    if (o is IFunction[])
                                        operation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                    else
                                        if (o is Variable[] && (o as Variable[]).Count() == 1)
                                            operation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                        else
                                            if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                }

                                if (operation != null)
                                    ((IStatementReturn)last).ReturnOperation = operation;
                            }

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Yield
                    case "YieldStatement":
                        {


                            string type = GetParentType(node).Trim();

                            if (type.StartsWith("IEnumerable<") || type.StartsWith("IEnumerator<") || type.StartsWith("System.Collections.Generic.IEnumerable<"))
                            {
                                type = type.Substring(type.IndexOf("<") + 1, type.IndexOf(">") - type.IndexOf("<"));
                                if (type.EndsWith(">") && type.IndexOf("<") == -1)
                                    type = type.Replace(">", "");
                            }
                            if (type == "IEnumerator") type = "Object";
                            // SHIT HAPPENS FIXME throw new Exception("Yield не возвращаем перечисление."); //данное исключение показывает что функция, использующая yield не возвращает перечисление

                            //вычленяем тип перечисления


                            YieldStatement yieldNode = (YieldStatement)node;
                            YieldStatement saveYieldNode = yieldNode;

                            if (yieldNode.Statement is ReturnStatement) //если yield return <expression>;
                            {
                                if (SensorFlag || ThirdSensor)
                                {
                                    LocalVariableDeclaration lvd = new LocalVariableDeclaration(new VariableDeclaration("RNTRNTRNT_" + (++generatedVariableNumber).ToString(), ((ReturnStatement)(yieldNode.Statement)).Expression, new TypeReference(type)));
                                    blNew.Children.Add(lvd);
                                    blNew.Children.Add(dingo());

                                    yieldNode = new YieldStatement(new ReturnStatement(new IdentifierExpression("RNTRNTRNT_" + generatedVariableNumber.ToString()))) { Parent = node.Parent };
                                }

                                blNew.Children.Add(yieldNode);

                                if (SensorFlag || ThirdSensor)
                                {
                                    sensorNumber++;
                                    blNew.Children.Add(dingo());
                                    sensorNumber--;
                                }

                                //добавление стейтмента и датчика(если нужен) в хранилище
                                last = GetLastStatement(entry);
                                if (last != null)
                                {
                                    last = last.NextInLinearBlock = storage.statements.AddStatementYieldReturn((ulong)saveYieldNode.StartLocation.Line, (ulong)saveYieldNode.StartLocation.Column, currentFile);
                                    if (SensorFlag || ThirdSensor)
                                    {
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.FINAL);
                                        last = last.NextInLinearBlock = storage.statements.AddStatementOperational((ulong)saveYieldNode.StartLocation.Line, (ulong)saveYieldNode.StartLocation.Column, currentFile);
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.START);
                                    }
                                }
                                else
                                {
                                    entry = last = storage.statements.AddStatementYieldReturn((ulong)saveYieldNode.StartLocation.Line, (ulong)saveYieldNode.StartLocation.Column, currentFile);
                                    if (SensorFlag || ThirdSensor)
                                    {
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.START);
                                        last = last.NextInLinearBlock = storage.statements.AddStatementOperational((ulong)saveYieldNode.StartLocation.Line, (ulong)saveYieldNode.StartLocation.Column, currentFile);
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.START);
                                    }
                                }
                            }
                            else //если yield break;
                            {
                                if (SensorFlag || ThirdSensor)
                                    blNew.Children.Add(dingo());
                                blNew.Children.Add(node);

                                //добавление стейтмента и датчика(если нужен) в хранилище
                                last = GetLastStatement(entry);
                                if (last != null)
                                {
                                    last = last.NextInLinearBlock = storage.statements.AddStatementReturn((ulong)saveYieldNode.StartLocation.Line, (ulong)saveYieldNode.StartLocation.Column, currentFile);
                                    if (SensorFlag || ThirdSensor)
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.FINAL);
                                }
                                else
                                {
                                    entry = last = storage.statements.AddStatementReturn((ulong)saveYieldNode.StartLocation.Line, (ulong)saveYieldNode.StartLocation.Column, currentFile);
                                    if (SensorFlag || ThirdSensor)
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.START);
                                }
                            }

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Switch
                    case "SwitchStatement":
                        {


                            SwitchStatement switchNode = (SwitchStatement)node;

                            //добавляем стейтмент и датчик(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementSwitch((ulong)switchNode.StartLocation.Line, (ulong)switchNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementSwitch((ulong)switchNode.StartLocation.Line, (ulong)switchNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }


                            //Condition
                            if (!switchNode.SwitchExpression.IsNull)
                            {
                                IOperation operation = storage.operations.AddOperation();
                                var ret = walkExpr(switchNode.SwitchExpression);
                                foreach (object o in ret)
                                {
                                    if (o is IFunction[])
                                        operation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                    else
                                        if (o is Variable[] && (o as Variable[]).Count() == 1)
                                            operation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                        else
                                            if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                }

                                if (operation != null)
                                    ((IStatementSwitch)last).Condition = operation;
                            }

                            //Body
                            //список блок стэйтментов что сидят в каждом кейсе
                            List<BlockStatement> tempBlcList = new List<BlockStatement>();

                            foreach (SwitchSection t in switchNode.SwitchSections)
                            {
                                if (t != null)
                                {
                                    var innerBlock = new BlockStatement();
                                    //fix for strange case
                                    if (t.Children.Count > 1 && t.Children[0] is BlockStatement)
                                    {
                                        //добавляем все в него, чтобы не было лишних датчиков
                                        var dumpStatement = t.Children[0];
                                        for (int i = 1; i < t.Children.Count; i++)
                                        {
                                            dumpStatement.Children.Add(t.Children[i]);
                                        }
                                        t.Children.Clear();
                                        t.Children.Add(dumpStatement);
                                    }
                                    var onlyBlock = t.Children.Count == 1 && t.Children[0] is BlockStatement;
                                    if (!onlyBlock)
                                    {
                                        t.Children.ForEach(g => innerBlock.Children.Add(g));
                                        innerBlock.StartLocation = t.StartLocation;
                                    }
                                    else
                                    {
                                        innerBlock = t.Children[0] as BlockStatement;
                                    }

                                    //bool startNewBlSt = true;
                                    //tempBlcList.Clear();

                                    //foreach (INode child in t.Children)
                                    //{
                                    //    if (child != null)
                                    //    {
                                    //        if (!(child is BlockStatement))
                                    //        {
                                    //            if (startNewBlSt)
                                    //            {
                                    //                tempBlcList.Add(new BlockStatement());
                                    //                startNewBlSt = false;
                                    //            }
                                    //            tempBlcList[tempBlcList.Count - 1].Children.Add(child);
                                    //            tempBlcList[tempBlcList.Count - 1].Parent = child.Parent;
                                    //        }
                                    //        else
                                    //        {
                                    //            startNewBlSt = true;
                                    //            tempBlcList.Add((BlockStatement)child);
                                    //        }
                                    //    }
                                    //}

                                    //очищаем список детей у текущей секции
                                    t.Children.Clear();

                                    //обрабатываем метку секции
                                    IOperation caseLabel = null;
                                    if (t.SwitchLabels.Count > 0 && !t.SwitchLabels[0].Label.IsNull) //если дефолт то тут же вылетаем
                                    {
                                        IOperation operation = storage.operations.AddOperation();
                                        var ret = walkExpr(t.SwitchLabels[0].Label);
                                        foreach (object o in ret)
                                        {
                                            if (o is IFunction[])
                                                operation.AddFunctionCallInSequenceOrder(o as IFunction[]);
                                            else
                                                if (o is Variable[] && (o as Variable[]).Count() == 1)
                                                    operation.AddFunctionCallInSequenceOrder((o as Variable[])[0]); //ад
                                                else
                                                    if (o is Variable[] && (o as Variable[]).Count() > 1)
                                                        IA.Monitor.Log.Warning(PluginSettings.Name, "Неоднозначность в полученных переменных!");
                                        }

                                        if (operation != null)
                                            caseLabel = operation;
                                    }

                                    //if (tempBlcList.Count >= 1)
                                    {
                                        //обрабатываем тело секции у которой теперь все дети имеют тип BlockStatement
                                        IStatement caseOrDefaultStatement = walkBody(innerBlock, false, levelWalk + 1);
                                        if (caseOrDefaultStatement != null)
                                        {
                                            if (caseLabel != null) //case
                                                ((IStatementSwitch)last).AddCase(caseLabel, caseOrDefaultStatement);
                                            else //default
                                                ((IStatementSwitch)last).AddDefaultCase(caseOrDefaultStatement);
                                        }
                                        if (!onlyBlock)
                                        {
                                            innerBlock.Children.ForEach(g => t.Children.Add(g));
                                        }
                                        else
                                        {
                                            t.Children.Add(innerBlock);
                                        }
                                        ////заполняем новый - обновлённый список детей
                                        //if (!startNewBlSt)
                                        //{
                                        //    //пишем поддетей (создавали новый стейтмент)
                                        //    foreach (BlockStatement blockS in tempBlcList)
                                        //        foreach(var innerNode in blockS.Children)
                                        //            t.Children.Add(innerNode);
                                        //}
                                        //else
                                        //{
                                        //    //дети так и были блоками
                                        //    foreach (BlockStatement blockS in tempBlcList)
                                        //        t.Children.Add(blockS);
                                        //}
                                    }
                                }
                            }
                            blNew.Children.Add(node);
                            //end

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Try-Catch-Finally
                    case "TryCatchStatement":
                        {


                            TryCatchStatement tryCatchNode = (TryCatchStatement)node;

                            //доабавление стейтмента в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementTryCatchFinally((ulong)tryCatchNode.StartLocation.Line, (ulong)tryCatchNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementTryCatchFinally((ulong)tryCatchNode.StartLocation.Line, (ulong)tryCatchNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }


                            //Try
                            IStatement tryBlockStatement = walkBody((BlockStatement)tryCatchNode.StatementBlock, false, levelWalk + 1);
                            if (tryBlockStatement != null)
                                ((IStatementTryCatchFinally)last).TryBlock = tryBlockStatement;

                            //Сatches
                            foreach (CatchClause _catch in tryCatchNode.CatchClauses)
                            {
                                IStatement catchBlockStatement = walkBody((BlockStatement)_catch.StatementBlock, false, levelWalk + 1);
                                if (catchBlockStatement != null)
                                {
                                    if (SensorFlag)
                                    {
                                        IStatement lastInner = GetLastStatement(catchBlockStatement);
                                        Store.Location innerLoc = lastInner.FirstSymbolLocation;
                                        lastInner = lastInner.NextInLinearBlock = storage.statements.AddStatementOperational(innerLoc.GetLine(), innerLoc.GetColumn(), currentFile);
                                        ((BlockStatement)_catch.StatementBlock).Children.Add(dingo());
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, lastInner, Kind.INTERNAL);
                                    }
                                    ((IStatementTryCatchFinally)last).AddCatch(catchBlockStatement);
                                }
                            }

                            //если присутствует блок файнали то и его обрабатываем
                            var blockStatement = tryCatchNode.FinallyBlock as BlockStatement;
                            if (blockStatement != null)
                            {
                                IStatement finallyBlockStatement = walkBody(blockStatement, false, levelWalk + 1);
                                if (finallyBlockStatement != null)
                                {
                                    if (SensorFlag)
                                    {
                                        IStatement lastInner = GetLastStatement(finallyBlockStatement);
                                        Store.Location innerLoc = lastInner.FirstSymbolLocation;
                                        lastInner = lastInner.NextInLinearBlock = storage.statements.AddStatementOperational(innerLoc.GetLine(), innerLoc.GetColumn(), currentFile);
                                        blockStatement.Children.Add(dingo());
                                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, lastInner, Kind.INTERNAL);
                                    }
                                    ((IStatementTryCatchFinally)last).FinallyBlock = finallyBlockStatement;
                                }
                            }

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Using
                    case "UsingStatement":
                        {


                            UsingStatement usingNode = (UsingStatement)node;

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementUsing((ulong)usingNode.StartLocation.Line, (ulong)usingNode.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementUsing((ulong)usingNode.StartLocation.Line, (ulong)usingNode.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }


                            //Head
                            if (!usingNode.ResourceAcquisition.IsNull)
                            {
                                BlockStatement usingBlock = new BlockStatement();
                                usingBlock.Children.Add(usingNode.ResourceAcquisition);
                                usingBlock.Parent = usingNode.ResourceAcquisition.Parent;

                                Boolean oldFlag = SensorFlag;
                                SensorFlag = false;
                                IStatementOperational iso = (IStatementOperational)walkBody(usingBlock, false, levelWalk + 1);
                                SensorFlag = oldFlag;

                                if (iso != null && iso.Operation != null)
                                    ((IStatementUsing)last).Head = iso.Operation;
                            }

                            //Body

                            if (!(usingNode.EmbeddedStatement is BlockStatement))
                            {
                                BlockStatement tempBlc = new BlockStatement { Parent = usingNode.Parent }; //FIXME проверить, что не генерится бредовых датчиков
                                tempBlc.Children.Add(usingNode.EmbeddedStatement);
                                usingNode.EmbeddedStatement = tempBlc;
                            }
                            IStatement usingBlockStatement = walkBody((BlockStatement)(usingNode.EmbeddedStatement), false, levelWalk + 1);
                            if (usingBlockStatement != null)
                                ((IStatementUsing)last).Body = usingBlockStatement;


                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Unsafe
                    case "UnsafeStatement":
                        {
                            //FIXME какой стейтмент тут добавлять не знаю



                            UnsafeStatement unsafeSt = (UnsafeStatement)node;

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            /*if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementOperational((ulong)unsafeSt.StartLocation.Line, (ulong)unsafeSt.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementOperational((ulong)unsafeSt.StartLocation.Line, (ulong)unsafeSt.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }*/

                            //обрабатываем содержимое
                            IStatement ret = walkBody((BlockStatement)(unsafeSt.Block), false, levelWalk + 1);
                            /*if (ret != null && ret is IStatementOperational)
                                ((IStatementOperational)last).Operation = ((IStatementOperational)ret).Operation;*/
                            if (ret != null)
                            {
                                if (last != null)
                                {
                                    last.NextInLinearBlock = ret;
                                }
                                else
                                {
                                    entry = ret;
                                }
                            }

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Checked
                    case "CheckedStatement":
                        {
                            //FIXME какой стейтмент тут добавлять не знаю



                            CheckedStatement checkedSt = (CheckedStatement)node;

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementOperational((ulong)checkedSt.StartLocation.Line, (ulong)checkedSt.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementOperational((ulong)checkedSt.StartLocation.Line, (ulong)checkedSt.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }

                            //обрабатываем содержимое чектом
                            IStatement ret = walkBody((BlockStatement)(checkedSt.Block), false, levelWalk + 1);
                            if (ret != null && ret is IStatementOperational)
                                ((IStatementOperational)last).Operation = ((IStatementOperational)ret).Operation;

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Unchecked
                    case "UncheckedStatement":
                        {
                            //FIXME какой стейтмент тут добавлять не знаю



                            UncheckedStatement uncheckedSt = (UncheckedStatement)node;

                            //вставили датчик перед анчектом
                            //if (SensorFlag)
                            //    blNew.Children.Add(dingo());

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            //if (last != null)
                            //{
                            //    last = last.NextInLinearBlock = storage.statements.AddStatementOperational(currentFile, (ulong)uncheckedSt.StartLocation.Line, (ulong)uncheckedSt.StartLocation.Column);
                            //    if (SensorFlag)
                            //        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                            //}
                            //else
                            //{
                            //    entry = last = storage.statements.AddStatementOperational(currentFile, (ulong)uncheckedSt.StartLocation.Line, (ulong)uncheckedSt.StartLocation.Column);
                            //    if (SensorFlag)
                            //        storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                            //}

                            //обрабатываем содержимое анчекта
                            IStatement ret = walkBody((BlockStatement)(uncheckedSt.Block), false, levelWalk + 1);
                            if (ret != null)
                            {
                                //FIXME - пока просто тупо подцепляем содержимое как линейный блок
                                if (last != null)
                                    last.NextInLinearBlock = ret;
                                else
                                    entry = ret;
                            }

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Fixed
                    case "FixedStatement":
                        {
                            //FIXME какой стейтмент тут добавлять не знаю



                            FixedStatement fixedSt = (FixedStatement)node;

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            if (last != null)
                            {
                                last = last.NextInLinearBlock = storage.statements.AddStatementOperational((ulong)fixedSt.StartLocation.Line, (ulong)fixedSt.StartLocation.Column, currentFile);
                                if (SensorFlag)
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                                }
                            }
                            else
                            {
                                entry = last = storage.statements.AddStatementOperational((ulong)fixedSt.StartLocation.Line, (ulong)fixedSt.StartLocation.Column, currentFile);
                                if (SensorFlag || (levelWalk == 1 && ThirdSensor))
                                {
                                    blNew.Children.Add(dingo());
                                    storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                                }
                            }

                            //обрабатываем содержимое юзинга
                            if (fixedSt.EmbeddedStatement != null)
                            {
                                if (!(fixedSt.EmbeddedStatement is BlockStatement))
                                {
                                    BlockStatement tempBlc = new BlockStatement();
                                    tempBlc.Children.Add(fixedSt.EmbeddedStatement);
                                    tempBlc.Parent = fixedSt.EmbeddedStatement.Parent;
                                    fixedSt.EmbeddedStatement = tempBlc;
                                }
                                IStatement ret = walkBody((BlockStatement)fixedSt.EmbeddedStatement, false, levelWalk + 1);
                                if (ret != null && ret is IStatementOperational)
                                    ((IStatementOperational)last).Operation = ((IStatementOperational)ret).Operation;
                            }

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    #region Lock
                    case "LockStatement":
                        {


                            LockStatement lockSt = (LockStatement)node;


                            //вставили датчик перед локом
                            //if (SensorFlag)
                            //    blNew.Children.Add(dingo());

                            //добавление стейтмента и датчика(если нужен) в хранилище
                            last = GetLastStatement(entry);
                            //if (last != null)
                            //{
                            //    last = last.NextInLinearBlock = storage.statements.AddStatementOperational(currentFile, (ulong)lockSt.StartLocation.Line, (ulong)lockSt.StartLocation.Column);
                            //    if (SensorFlag)
                            //        storage.sensors.AddSensorBeforeStatement(sensorNumber++, last, Kind.INTERNAL);
                            //}
                            //else
                            //{
                            //    entry = last = storage.statements.AddStatementOperational(currentFile, (ulong)lockSt.StartLocation.Line, (ulong)lockSt.StartLocation.Column);
                            //    if (SensorFlag)
                            //        storage.sensors.AddSensorBeforeStatement(sensorNumber++, entry, Kind.START);
                            //}

                            if (lockSt.EmbeddedStatement != null)
                            {
                                if (!(lockSt.EmbeddedStatement is BlockStatement))
                                {
                                    BlockStatement tempBlc = new BlockStatement();
                                    tempBlc.Children.Add(lockSt.EmbeddedStatement);
                                    tempBlc.Parent = lockSt.EmbeddedStatement.Parent;
                                    lockSt.EmbeddedStatement = tempBlc;
                                }
                                IStatement ret = walkBody((BlockStatement)lockSt.EmbeddedStatement, false, levelWalk + 1);
                                if (ret != null)
                                {
                                    //FIXME - пока просто тупо подцепляем содержимое как линейный блок
                                    if (last != null)
                                        last.NextInLinearBlock = ret;
                                    else
                                        entry = ret;
                                    //if(ret is IStatementOperational)
                                    //((IStatementOperational)last).Operation = ((IStatementOperational)ret).Operation;
                                }
                            }

                            blNew.Children.Add(node);

                            last = GetLastStatement(entry);
                            if (last != null)
                                last.SetLastSymbolLocation(currentFile, (ulong)node.EndLocation.Line, (ulong)node.EndLocation.Column);

                            break;
                        }
                    #endregion
                    default:
                        {
                            IA.Monitor.Log.Warning(PluginSettings.Name, "Обнаружен необработанный стейтмент: " + node.GetType().UnderlyingSystemType.Name);

                            //blNew.Children.Add(st);
                            blNew.Children.Add(node);
                            break;
                        }
                }
            }
            IStatement lastSt = GetLastStatement(entry);
            if ((SensorFlag || ThirdSensor) && endSensor && lastSt != null && !checkIfBrk(lastSt) && checkLastStatement(lastSt))
            {
                lastSt =
                    lastSt.NextInLinearBlock = storage.statements.AddStatementOperational((ulong)endLoc.Line, (ulong)endLoc.Column, currentFile, (ulong)endLoc.Line, (ulong)endLoc.Column);
                blNew.Children.Add(dingo());
                storage.sensors.AddSensorBeforeStatement(sensorNumber++, lastSt, Kind.FINAL);
            }
            else
            {
                if (lastSt == null && SensorFlag && endSensor)
                {
                    if (endLoc.Line == -1)
                        endLoc = new Location(block.EndLocation.Line, block.EndLocation.Column + 1);

                    if (block.StartLocation.Line != 0 || block.StartLocation.Column != 0)
                    {
                        entry = lastSt = storage.statements.AddStatementOperational((ulong)block.StartLocation.Line, (ulong)block.StartLocation.Column + 1, currentFile, (ulong)endLoc.Line, (ulong)endLoc.Column);
                        blNew.Children.Add(dingo());
                        storage.sensors.AddSensorBeforeStatement(sensorNumber++, lastSt, Kind.FINAL);
                    }
                }
            }
            block.Children = blNew.Children;
            return entry;
        }

        private bool checkLastStatement(IStatement st)
        {
            if (st == null || st is IStatementOperational)
                return true;
            if (checkIfRet(st))
                return false;
            if (checkIfBrk(st))
                return true; //надо обдумать

            if (st is IStatementFor)
            {
                if (!checkLastStatement(GetLastStatement((st as IStatementFor).Body)))
                    return false; //нельзя ставить датчик
            }
            if (st is IStatementForEach)
            {
                if (!checkLastStatement(GetLastStatement((st as IStatementForEach).Body)))
                    return false; //нельзя ставить датчик
            }
            if (st is IStatementDoAndWhile)
            {
                if (!checkLastStatement(GetLastStatement((st as IStatementDoAndWhile).Body)))
                    return false; //нельзя ставить датчик
            }
            if (st is IStatementTryCatchFinally)
            {
                return true;
                if (!checkLastStatement(GetLastStatement((st as IStatementTryCatchFinally).TryBlock)))
                    //if ((st as IStatementTryCatchFinally).FinallyBlock == null || !checkLastStatement(getLastStatement((st as IStatementTryCatchFinally).FinallyBlock)))
                    return false; //нельзя ставить датчик
            }
            if (st is IStatementUsing)
            {
                if (!checkLastStatement(GetLastStatement((st as IStatementUsing).Body)))
                    return false; //нельзя ставить датчик
            }
            if (st is IStatementSwitch)
            {
                IStatementSwitch s = st as IStatementSwitch;
                bool notRet = false;
                foreach (KeyValuePair<IOperation, IStatement> kvp in s.Cases().Where(kvp => checkLastStatement(kvp.Value)))
                    notRet = true;
                if (checkLastStatement(s.DefaultCase()))
                    notRet = true;
                return notRet;
            }
            if (st is IStatementIf)
            {
                IStatementIf s = st as IStatementIf;
                return checkLastStatement(s.ThenStatement) || checkLastStatement(s.ElseStatement);
            }
            return true;
        }

        private bool checkIfBrk(IStatement st)
        {
            if (st == null)
                return false;
            return st is IStatementBreak || st is IStatementContinue || st is IStatementGoto || st is IStatementReturn || st is IStatementThrow; //|| st is IStatementYieldReturn;
        }
        private bool checkIfRet(IStatement st)
        {
            if (st == null)
                return false;
            return st is IStatementGoto || st is IStatementReturn || st is IStatementThrow; //|| st is IStatementYieldReturn;
        }

        private ResolvedElem getElem(ResolveResult result)
        {
            if (result == null)
                return null;

            switch (result.GetType().UnderlyingSystemType.Name)
            {
                case "MixedResolveResult":  //expression inside
                    {
                        MixedResolveResult elem = (MixedResolveResult)result;
                        return getElem(elem.PrimaryResult);
                    }
                case "MemberResolveResult": //member
                    {
                        MemberResolveResult elem = (MemberResolveResult)result;
                        IMember el = elem.ResolvedMember;

                        if (el is IField)
                        {
                            IField ele = (IField)el;
                            return new ResolvedElem(el, ele.FullyQualifiedName, currLoc);
                        }

                        if (el is IProperty)
                        {
                            IProperty ele = (IProperty)el;
                            return new ResolvedElem(el, ele.FullyQualifiedName, currLoc);
                        }
                        if (el is IEvent)
                        {
                            IEvent ele = (IEvent)el;
                            return new ResolvedElem(el, ele.FullyQualifiedName, currLoc);
                        }
                        if (el is ICSharpCode.SharpDevelop.Dom.IMethod)
                        {
                            ICSharpCode.SharpDevelop.Dom.IMethod ele = (ICSharpCode.SharpDevelop.Dom.IMethod)el;
                            return new ResolvedElem(el, ele.FullyQualifiedName, currLoc);
                        }
                        if (el is IClass)
                        {
                            IClass ele = (IClass)el; //ну и что ? что тут возвращать мне интересно?
                            return new ResolvedElem(el, ele.FullyQualifiedName, currLoc);
                        }
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Неприятная ситуация: " + elem.ResolvedMember.GetType().UnderlyingSystemType.Name);
                        return null;
                    }
                case "LocalResolveResult": //variable or parameter
                    {
                        LocalResolveResult elem = (LocalResolveResult)result;
                        return new ResolvedElem(elem.Field, elem.Field.FullyQualifiedName, currLoc, true);
                    }
                case "NamespaceResolveResult": //namespace
                    {
                        //зачем это может понадобиться - ума не приложу
                        NamespaceResolveResult elem = (NamespaceResolveResult)result;
                        return new ResolvedElem(null, elem.Name, currLoc, false, true);
                    }
                case "TypeResolveResult": //type name
                    {
                        IClass c = ((TypeResolveResult)result).ResolvedClass; // может быть нулом, FIXME!!!!
                        if (c != null)
                            return new ResolvedElem(null, c.FullyQualifiedName, currLoc, false, true);

                        throw new Exception("вот такая бня, надо понять, когда она у нас возникает");
                    }
                case "MethodGroupResolveResult": //method without parameters, overload
                    {
                        MethodGroupResolveResult mrr = result as MethodGroupResolveResult;
                        if (mrr != null)
                        {
                            if (mrr.GetMethodIfSingleOverload() != null)
                                return new ResolvedElem(mrr.GetMethodIfSingleOverload(), mrr.GetMethodIfSingleOverload().FullyQualifiedName, currLoc);

                            if (mrr.GetMethodWithEmptyParameterList() != null)
                                return new ResolvedElem(mrr.GetMethodWithEmptyParameterList(), mrr.GetMethodWithEmptyParameterList().FullyQualifiedName, currLoc);

                            ResolvedElem rr = new ResolvedElem(null, mrr.ContainingType.FullyQualifiedName + "." + mrr.Name, currLoc);
                            if (mrr.ContainingType.Namespace == "?")
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Не определено пространство имён для функции " + mrr.ContainingType.FullyQualifiedName + "." + mrr.Name);

                            if (rr.FullDotNetName.StartsWith("."))
                                IA.Monitor.Log.Warning(PluginSettings.Name, "Неверное разрешение имени для функции: " + rr.FullDotNetName);
                            return rr;
                        }
                        break;
                    }
                case "DelegateCallResolveResult": //новенькое
                    {
                        DelegateCallResolveResult elem = (DelegateCallResolveResult)result;
                        if (elem.Target != null)
                        {
                            ResolvedElem ret = getElem(elem.Target);
                            if (ret != null)
                                return new ResolvedElem(null, ret.FullDotNetName, currLoc, false, false, true);
                            return null;
                        }
                        break;
                    }
                case "UnknownMethodResolveResult": //новенькое
                    {
                        UnknownMethodResolveResult elem = (UnknownMethodResolveResult)result;
                        return new ResolvedElem(null, elem.Target.FullyQualifiedName, currLoc); //точно не уверен
                    }
                case "UnknownIdentifierResolveResult": //новенькое
                    {
                        return null;
                    }
                default:
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Нераспознанный результат разрешения имён: " + result.GetType().UnderlyingSystemType.Name + ".");
                        break;
                    }
            }
            return null;
        }

        //функция, которая возвращает распознанную последовательность вызовов функция (напрямую - Function[] или по указателю - Variable)
        private IEnumerable<object> getVarFunc(ResolvedElem elem)
        {
            List<Object> ret = new List<object>();
            if (elem.MemberType != null) //член класса или локальная переменная
            {
                #region Field
                if (elem.MemberType is IField)
                {
                    IEnumerable<object> retList = processFieldorVariable(elem);
                    if (retList != null)
                        ret.AddRange(retList);
                }
                #endregion
                #region Property
                if (elem.MemberType is IProperty)
                {
                    string searchName;
                    switch (isGetOrSetorSetGet)
                    {
                        case 0:
                            {
                                searchName = elem.FullDotNetName + "_get";
                                ret.Add(storage.functions.GetFunction(searchName.Split('.')));
                                break;
                            }
                        case 1:
                            {
                                searchName = elem.FullDotNetName + "_set";
                                ret.Add(storage.functions.GetFunction(searchName.Split('.')));
                                break;
                            }
                        case 2:
                            {
                                //тут полный щит, так как надо вернуть 2 штуки
                                searchName = elem.FullDotNetName + "_get";
                                ret.Add(storage.functions.GetFunction(searchName.Split('.')));
                                searchName = elem.FullDotNetName + "_set";
                                ret.Add(storage.functions.GetFunction(searchName.Split('.')));
                                break;
                            }
                    }
                    isGetOrSetorSetGet = 0; //сразу после обоаботки возвращаемся к стандартному случаю

                    //if (inInvocation)
                    //    IA.Monitor.Log.Warning(PluginSettings.Name, "Iproperty call???");
                }
                #endregion
                #region Event
                else if (elem.MemberType is IEvent)
                {
                    //не понятно что именно тут делать
                }
                #endregion
                #region Method
                else if (elem.MemberType is ICSharpCode.SharpDevelop.Dom.IMethod)
                {
                    IFunction[] search = storage.functions.GetFunction(elem.FullDotNetName.Split('.'));
                    if (search.Any())
                        ret.Add(search);
                    else
                    {
                        search = storage.functions.GetFunction(elem.FullDotNetName);
                        if (search.Any())
                            ret.Add(search);//внешняя функция
                        else
                        {
                            IFunction newFunc = storage.functions.AddFunction();
                            newFunc.Name = elem.FullDotNetName;
                            newFunc.AddDefinition(elem.MemberType.ProjectContent.AssemblyName != null ? storage.files.FindOrGenerate(elem.MemberType.ProjectContent.AssemblyName, enFileKindForAdd.externalFile) : storage.files.FindOrGenerate("Unknown SourceFile", enFileKindForAdd.externalFile), 0);
                            newFunc.IsExternal = true;
                            ret.Add(new[] { newFunc });
                        }

                    }
                }
                #endregion
                #region Class
                if (elem.MemberType is IClass)
                {
                    //тут делать ничего не надо
                }
                #endregion

                return ret;
            }
            if (elem.IsDom) //класс или пространство имён
            {
                return ret;
            }
            if (elem.IsDelegate) //делегат
            {
                Variable[] searchDelegate = storage.variables.GetVariable(elem.FullDotNetName.Split('.'));

                if ((searchDelegate != null && !searchDelegate.Any()) || searchDelegate == null)
                    return ret;

                if (searchDelegate.Count() > 1)
                    throw new Exception("Найден неоднозначный вызов по указателю.");

                foreach (Variable variable in searchDelegate)
                    variable.AddValueCallPosition(currentFile, (ulong)elem.ExprLoc.Line, (ulong)elem.ExprLoc.Column, currMethod);
                ret.Add(searchDelegate);

                return ret;
            }

            // елси никуда выше не зашли то мы имее дело с перегрузкой метода или неизвестным методом или неизвестным полем
            IFunction[] searchOverload = storage.functions.GetFunction(elem.FullDotNetName.Split('.'));

            if (searchOverload == null || !searchOverload.Any())
            {
                //пытаемся найти метод подсовывая ему всё имя
                IFunction[] searchFunction = storage.functions.GetFunction(elem.FullDotNetName);
                if (searchFunction.Any())
                {
                    ret.Add(searchFunction); //внешняя функция
                    return ret;
                }

                //пытаемся найти метод, подсовывая ему только последнюю часть идентификатора
                IFunction[] searchFunction2 = storage.functions.GetFunction(elem.FullDotNetName.Substring(elem.FullDotNetName.LastIndexOf(".") + 1));
                if (searchFunction2.Any())
                {
                    List<IFunction> list = new List<IFunction>();
                    foreach (IFunction fun in searchFunction2)
                        if (fun.FullName.ToLower().Contains(elem.FullDotNetName.ToLower()))
                            list.Add(fun);
                    if (list.Any())
                    {
                        ret.Add(list.ToArray()); //внешняя функция
                        return ret;
                    }

                }

                //пытаемся распознать поле
                IEnumerable<object> retList = processFieldorVariable(elem);
                if (retList != null)
                {
                    ret.AddRange(retList);
                    return ret;
                }

                //не удалось распознать метод поэтому просто добавляем его в неведомый файл
                IFunction newFunc = storage.functions.AddFunction();
                newFunc.Name = elem.FullDotNetName;
                newFunc.AddDefinition(storage.files.FindOrGenerate("Unknown SourceFile", enFileKindForAdd.externalFile), 0);
                newFunc.IsExternal = true;
                ret.Add(new[] { newFunc });
            }
            else
            {
                ret.Add(searchOverload);
            }

            return ret;
        }

        private IEnumerable<object> processFieldorVariable(ResolvedElem elem)
        {
            List<Object> ret = new List<object>();

            Variable[] searchField = storage.variables.GetVariable(elem.FullDotNetName.Substring(elem.FullDotNetName.LastIndexOf('.') + 1));
            if (searchField == null || !searchField.Any())
                return null;

            switch (isGetOrSetorSetGet)
            {
                case 0: //get
                    {
                        foreach (Variable variable in searchField)
                        {
                            if (elem.IsLocalVariable)
                                if (!variable.Definitions().Any() || variable.Definitions()[0].GetFile() != currentFile || !elem.MemberType.Region.IsInside((int)variable.Definitions()[0].GetLine(), (int)variable.Definitions()[0].GetColumn()))
                                    continue;
                            variable.AddValueGetPosition(currentFile, (ulong)elem.ExprLoc.Line, (ulong)elem.ExprLoc.Column, currMethod);
                        }
                        break;
                    }
                case 1: //set
                    {
                        foreach (Variable variable in searchField)
                        {
                            if (elem.IsLocalVariable)
                                if (!variable.Definitions().Any() || variable.Definitions()[0].GetFile() != currentFile || !elem.MemberType.Region.IsInside((int)variable.Definitions()[0].GetLine(), (int)variable.Definitions()[0].GetColumn()))
                                    continue;
                            variable.AddValueSetPosition(currentFile, (ulong)elem.ExprLoc.Line, (ulong)elem.ExprLoc.Column, currMethod);
                        }
                        break;
                    }
                case 2: //setget
                    {
                        foreach (Variable variable in searchField)
                        {
                            if (elem.IsLocalVariable)
                                if (!variable.Definitions().Any() || variable.Definitions()[0].GetFile() != currentFile || !elem.MemberType.Region.IsInside((int)variable.Definitions()[0].GetLine(), (int)variable.Definitions()[0].GetColumn()))
                                    continue;
                            variable.AddValueSetPosition(currentFile, (ulong)elem.ExprLoc.Line, (ulong)elem.ExprLoc.Column, currMethod);
                            variable.AddValueGetPosition(currentFile, (ulong)elem.ExprLoc.Line, (ulong)elem.ExprLoc.Column, currMethod);
                        }
                        break;
                    }
            }
            isGetOrSetorSetGet = 0; //сразу после обоаботки возвращаемся к стандартному случаю

            if (inInvocation)
            {
                foreach (Variable variable in searchField)
                {
                    if (elem.IsLocalVariable)
                        if (!variable.Definitions().Any() || variable.Definitions()[0].GetFile() != currentFile || !elem.MemberType.Region.IsInside((int)variable.Definitions()[0].GetLine(), (int)variable.Definitions()[0].GetColumn()))
                            continue;
                    variable.AddValueCallPosition(currentFile, (ulong)elem.ExprLoc.Line, (ulong)elem.ExprLoc.Column, currMethod);
                    ret.Add(variable);
                }
            }

            return ret;
        }

        private int toStack;
        //обходчик всех выражений
        private List<Object> walkExpr(Expression expr)
        {
            // toStack:
            // 0 - нам тут неинтересно ничего не кладём стек
            // 1 - мы находимся в главной ветке вызова - кладём всё в стек
            // 2 - мы обрабатываем аргументы вызова, пока флаг опускаем, если даже внутри будет вызов он сам поднимет свой флаг

            toStack = (toStack != 2 && (expr.Parent is InvocationExpression || expr.Parent is ObjectCreateExpression || expr.Parent is ArrayCreateExpression)) ? 1 : 0;

            List<Object> res = new List<Object>(); //возвращаемые операции

            switch (expr.GetType().UnderlyingSystemType.Name)
            {
                case "IdentifierExpression":
                    {
                        IdentifierExpression exp = (IdentifierExpression)expr;
                        ResolvedElem elem = expResolver(exp);
                        if (elem == null)
                            break;
                        if (toStack == 1)
                            stack.Push(elem);
                        else
                            res.AddRange(getVarFunc(elem)); //если тут не null - то это хреново, это присваивание функции, бывает в делегатах и эвентах, надо отлавливать эти случаи
                        break;
                    }
                case "MemberReferenceExpression":
                    {
                        MemberReferenceExpression exp = (MemberReferenceExpression)expr;
                        res.AddRange(walkExpr(exp.TargetObject));
                        ResolvedElem elem = expResolver(exp);
                        if (elem == null)
                            break;
                        if (toStack == 1)
                            stack.Push(elem);
                        else
                            res.AddRange(getVarFunc(elem));
                        break;
                    }
                case "InvocationExpression":
                    {
                        InvocationExpression exp = (InvocationExpression)expr;
                        int oldstack = stack.Count;

                        toStack = 1;
                        res.AddRange(walkExpr(exp.TargetObject));

                        //arguments
                        foreach (Expression e in exp.Arguments)
                        {
                            toStack = 2;
                            res.AddRange(walkExpr(e));
                        }


                        inInvocation = true;
                        //if (stack.Count > oldstack) //выпихиваем только свои члены стека, это важно!
                        while (stack.Count > oldstack) //Диман, разве тут не так должно быть? 
                            res.AddRange(getVarFunc(stack.Pop()));
                        inInvocation = false;
                        break;
                    }
                case "AssignmentExpression":
                    {
                        AssignmentExpression exp = (AssignmentExpression)expr;

                        //обрабатываем правую часть присваивания
                        res.AddRange(walkExpr(exp.Right));

                        //обрабатываем левую часть присваивания 
                        if (exp.Parent is AssignmentExpression) //точно ли проверка правильная???
                            isGetOrSetorSetGet = 2; //getset
                        else
                            isGetOrSetorSetGet = 1; //set
                        res.AddRange(walkExpr(exp.Left));
                        isGetOrSetorSetGet = 0; //get
                        break;
                    }
                case "UnaryOperatorExpression":
                    {
                        UnaryOperatorExpression exp = (UnaryOperatorExpression)expr;

                        if (exp.Op == UnaryOperatorType.Increment || exp.Op == UnaryOperatorType.PostIncrement || exp.Op == UnaryOperatorType.Decrement || exp.Op == UnaryOperatorType.PostDecrement)
                            isGetOrSetorSetGet = 2;
                        res.AddRange(walkExpr(exp.Expression));

                        isGetOrSetorSetGet = 0;
                        break;
                    }
                case "BinaryOperatorExpression":
                    {
                        BinaryOperatorExpression exp = (BinaryOperatorExpression)expr;
                        res.AddRange(walkExpr(exp.Left));
                        res.AddRange(walkExpr(exp.Right));
                        break;
                    }
                case "AnonymousMethodExpression": // вот это вот вообще жесть надо много думать
                case "QueryExpression": // вот тут вот вообще жара, вроде внутри експрешенф есть и вроде как тока шушара там
                case "NullCollectionInitializerExpression":
                case "NullExpression":
                case "TypeOfExpression":
                case "PrimitiveExpression":
                case "ThisReferenceExpression":
                case "SizeOfExpression":
                case "DefaultValueExpression":
                case "BaseReferenceExpression":
                case "TypeReferenceExpression":
                    break;
                case "ObjectCreateExpression":
                    {
                        ObjectCreateExpression exp = (ObjectCreateExpression)expr;
                        int oldstack = stack.Count;

                        toStack = 1;
                        res.AddRange(walkExpr(exp.ObjectInitializer));

                        //arguments
                        foreach (Expression e in exp.Parameters)
                        {
                            toStack = 2;
                            res.AddRange(walkExpr(e));
                        }

                        inInvocation = true;
                        while (stack.Count > oldstack)
                            res.AddRange(getVarFunc(stack.Pop()));
                        inInvocation = false;
                        break;
                    }
                case "CheckedExpression":
                    {
                        CheckedExpression exp = (CheckedExpression)expr;
                        res.AddRange(walkExpr(exp.Expression));
                        break;
                    }
                case "UncheckedExpression":
                    {
                        UncheckedExpression exp = (UncheckedExpression)expr;
                        res.AddRange(walkExpr(exp.Expression));
                        break;
                    }
                case "CastExpression":
                    {
                        CastExpression exp = (CastExpression)expr;
                        res.AddRange(walkExpr(exp.Expression));
                        break;
                    }
                case "ParenthesizedExpression":
                    {
                        ParenthesizedExpression exp = (ParenthesizedExpression)expr;
                        res.AddRange(walkExpr(exp.Expression));
                        break;
                    }
                case "TypeOfIsExpression":
                    {
                        TypeOfIsExpression exp = (TypeOfIsExpression)expr;
                        res.AddRange(walkExpr(exp.Expression));
                        break;
                    }
                case "IndexerExpression":
                    {
                        IndexerExpression exp = (IndexerExpression)expr;
                        //i++(0) будем обрабатывать неправильно!
                        res.AddRange(walkExpr(exp.TargetObject));
                        foreach (Expression e in exp.Indexes)
                            res.AddRange(walkExpr(e));
                        break;
                    }
                case "LambdaExpression":
                    {
                        LambdaExpression exp = (LambdaExpression)expr;
                        res.AddRange(walkExpr(exp.ExpressionBody));
                        break;
                    }
                case "ParameterDeclarationExpression":
                    {
                        ParameterDeclarationExpression exp = (ParameterDeclarationExpression)expr;
                        res.AddRange(walkExpr(exp.DefaultValue));
                        break;
                    }
                case "StackAllocExpression":
                    {
                        StackAllocExpression exp = (StackAllocExpression)expr;
                        res.AddRange(walkExpr(exp.Expression));
                        break;
                    }
                case "ArrayCreateExpression":
                    {
                        ArrayCreateExpression exp = (ArrayCreateExpression)expr;
                        int oldstack = stack.Count;

                        toStack = 1;
                        res.AddRange(walkExpr(exp.ArrayInitializer));

                        //arguments
                        foreach (Expression e in exp.Arguments)
                        {
                            toStack = 2;
                            res.AddRange(walkExpr(e));
                        }

                        inInvocation = true;
                        while (stack.Count > oldstack)
                            res.AddRange(getVarFunc(stack.Pop()));
                        inInvocation = false;
                        break;
                    }
                case "CollectionInitializerExpression":
                    {
                        CollectionInitializerExpression exp = (CollectionInitializerExpression)expr;
                        foreach (Expression e in exp.CreateExpressions)
                            res.AddRange(walkExpr(e));

                        break;
                    }
                case "NamedArgumentExpression":
                    {
                        NamedArgumentExpression exp = (NamedArgumentExpression)expr;
                        res.AddRange(walkExpr(exp.Expression));
                        break;
                    }
                case "MemberInitializerExpression":
                    {
                        MemberInitializerExpression exp = (MemberInitializerExpression)expr;
                        res.AddRange(walkExpr(exp.Expression));
                        break;
                    }
                case "DirectionExpression":
                    {
                        DirectionExpression exp = (DirectionExpression)expr;
                        res.AddRange(walkExpr(exp.Expression));
                        break;
                    }
                case "ConditionalExpression": //тринарный оператор
                    {
                        ConditionalExpression exp = (ConditionalExpression)expr;
                        res.AddRange(walkExpr(exp.Condition));
                        res.AddRange(walkExpr(exp.TrueExpression));
                        res.AddRange(walkExpr(exp.FalseExpression));
                        break;
                    }
                case "PointerReferenceExpression":
                    {
                        PointerReferenceExpression exp = (PointerReferenceExpression)expr;
                        res.AddRange(walkExpr(exp.TargetObject));
                        break;
                    }
                default:
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Найден необработанное выражение: " + expr.GetType().UnderlyingSystemType.Name);
                        break;
                    }
            }
            return res;
        }

        //фуннкция, разрешающая переданное в него выражение, элемент на выходе можно положить в стек
        private ResolvedElem expResolver(Expression exp)
        {
            if (!PluginSettings.IsNameResolving)
                return null;

            IExpressionFinder expressionFinder = new CSharpExpressionFinder(parseInformation);

            if (exp.StartLocation.Line == 0 && exp.StartLocation.Column == 0)
                return null;

            ExpressionResult expression = expressionFinder.FindFullExpression(ref contents, (int)currentFile.ConvertLineColumnToOffset((ulong)exp.StartLocation.Line, (ulong)exp.StartLocation.Column));
            //ExpressionResult expression = expressionFinder.FindFullExpression(ref contents,GetOffset(ref contents,exp.StartLocation.Line,exp.StartLocation.Column));
            if (expression.Region.IsEmpty)
                expression.Region = new DomRegion(exp.StartLocation.Line - 1, exp.StartLocation.Column - 1);

            ResolveResult rr = resolver.ResolveSimple(expression, parseInformation, ref contents, exp);
            currLoc = exp.StartLocation;
            ResolvedElem resolvedElem = getElem(rr);
            return resolvedElem;
        }

        //генерация кода по построенному дереву
        private string GenerateCode(IOutputAstVisitor outputVisitor)
        {
            unit.AcceptVisitor(outputVisitor, null);
            return outputVisitor.Text.Replace("\t", "  ");
        }

        /*public void ApplyTransformation(IAstVisitor visitor)
        {
            if (tree.SelectedNode == tree.Nodes[0]) {
                unit.AcceptVisitor(visitor, null);
                UpdateTree();
            } else {
                string name = visitor.GetType().Name;
                ElementNode elementNode = tree.SelectedNode as ElementNode;
                CollectionNode collectionNode = tree.SelectedNode as CollectionNode;
                if (elementNode != null) {
                    if (IA.Monitor.Log.Warning(PluginSettings.Name, ("Apply " + name + " to selected element '" + elementNode.Text + "'?"),
                                        "Apply transformation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        elementNode.element.AcceptVisitor(visitor, null);
                        elementNode.Update();
                    }
                } else if (collectionNode != null) {
                    if (IA.Monitor.Log.Warning(PluginSettings.Name, ("Apply " + name + " to all elements in selected collection '" + collectionNode.Text + "'?"),
                                        "Apply transformation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        foreach (TreeNode subNode in collectionNode.Nodes) {
                            if (subNode is ElementNode) {
                                (subNode as ElementNode).element.AcceptVisitor(visitor, null);
                            }
                        }
                        collectionNode.Update();
                    }
                }
            }
        }*/


        /// <summary>
        /// Функция создаёт вершину дерева на основе объека.
        /// </summary>
        /// <param name="child">Объект, на основе которого требуется создать вершину дерева.</param>
        /// <returns>Указатель на созданную вершину дерева.</returns>
        private static TreeNode CreateNode(object child)
        {
            if (child == null)
            {
                return new TreeNode("*null reference*");
            }
            if (child is INode)
            {
                return new ElementNode(child as INode);
            }
            return new TreeNode(child.ToString());
        }

        /*public void DeleteSelectedNode()
        {
            if (tree.SelectedNode is ElementNode)
            {
                INode element = (tree.SelectedNode as ElementNode).element;
                if (tree.SelectedNode.Parent is CollectionNode)
                {
                    if (IA.Monitor.Log.Warning(PluginSettings.Name, "Remove selected node from parent collection?", "Remove node", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        IList col = (tree.SelectedNode.Parent as CollectionNode).collection;
                        col.Remove(element);
                        (tree.SelectedNode.Parent as CollectionNode).Update();
                    }
                }
                else if (tree.SelectedNode.Parent is ElementNode)
                {
                    if (IA.Monitor.Log.Warning(PluginSettings.Name, "Set selected property to null?", "Remove node", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        // get parent element
                        element = (tree.SelectedNode.Parent as ElementNode).element;
                        string propertyName = (string) tree.SelectedNode.Tag;
                        element.GetType().GetProperty(propertyName).SetValue(element, null, null);
                        (tree.SelectedNode.Parent as ElementNode).Update();
                    }
                }
            }
            else if (tree.SelectedNode is CollectionNode)
            {
                if (IA.Monitor.Log.Warning(PluginSettings.Name, "Remove all elements from selected collection?", "Clear collection", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    == DialogResult.Yes)
                {
                    IList col = (tree.SelectedNode as CollectionNode).collection;
                    col.Clear();
                    (tree.SelectedNode as CollectionNode).Update();
                }
            }
        }
        */

        /// <summary>
        /// Функция редактирования выбранного элемента дерева.
        /// </summary>
        public void EditSelectedNode()
        {
            TreeNode node = tree.SelectedNode;
            while (!(node is ElementNode))
            {
                if (node == null)
                {
                    return;
                }
                node = node.Parent;
            }
            //INode element = ((ElementNode)node).element;
            //using (EditDialog dlg = new EditDialog(element)) {
            //	dlg.ShowDialog();
            //}
            ((ElementNode)node).Update();
        }

        class CollectionNode : TreeNode
        {
            internal readonly IList collection;
            readonly string baseName;

            /// <summary>
            /// 
            /// </summary>
            /// <param name="text"></param>
            /// <param name="children"></param>
            public CollectionNode(string text, IList children)
                : base(text)
            {
                baseName = text;
                collection = children;
                Update();
            }

            /// <summary>
            /// 
            /// </summary>
            public void Update()
            {
                if (collection.Count == 0)
                {
                    Text = baseName + " (empty collection)";
                }
                else if (collection.Count == 1)
                {
                    Text = baseName + " (collection with 1 element)";
                }
                else
                {
                    Text = baseName + " (collection with " + collection.Count + " elements)";
                }
                Nodes.Clear();
                foreach (object child in collection)
                {
                    Nodes.Add(CreateNode(child));
                }
                Expand();
            }
        }

        class ElementNode : TreeNode
        {
            internal readonly INode element;

            /// <summary>
            /// 
            /// </summary>
            /// <param name="node"></param>
            public ElementNode(INode node)
            {
                element = node;
                Update();
            }

            /// <summary>
            /// 
            /// </summary>
            public void Update()
            {
                Nodes.Clear();
                Type type = element.GetType();
                Text = type.Name;
                if (Tag != null)
                { // HACK: after editing property element
                    Text = Tag + " = " + Text;
                }
                if (!(element is INullable && (element as INullable).IsNull))
                {
                    AddProperties(type, element);
                    if (element.Children.Count > 0)
                    {
                        Nodes.Add(new CollectionNode("Children", element.Children));
                    }
                }
            }

            void AddProperties(Type type, INode node)
            {
                if (type == typeof(AbstractNode))
                    return;
                foreach (PropertyInfo pi in type.GetProperties(BindingFlags.Instance | BindingFlags.Public))
                {
                    if (pi.DeclaringType != type) // don't add derived properties
                        continue;
                    if (pi.Name == "IsNull")
                        continue;
                    object value = pi.GetValue(node, null);
                    if (value is IList)
                    {
                        Nodes.Add(new CollectionNode(pi.Name, (IList)value));
                    }
                    else if (value is string)
                    {
                        Text += " " + pi.Name + "='" + value + "'";
                    }
                    else
                    {
                        TreeNode treeNode = CreateNode(value);
                        treeNode.Text = pi.Name + " = " + treeNode.Text;
                        treeNode.Tag = pi.Name;
                        Nodes.Add(treeNode);
                    }
                }
                AddProperties(type.BaseType, node);
            }
        }
        /*
                void TreeKeyDown(object sender, KeyEventArgs e)
                {
                    if (e.KeyData == Keys.Delete) {
                        DeleteSelectedNode();
                    } else if (e.KeyData == Keys.Space || e.KeyData == Keys.Enter) {
                        EditSelectedNode();
                    }
                }
        */
    }
}