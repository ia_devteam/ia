﻿namespace IA.Plugins.Parsers.CSharpParser
{
    partial class ProjectsConfigurationsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.main_dataGridView = new System.Windows.Forms.DataGridView();
            this.projectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.projectConfig = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.configurations_comboBox = new System.Windows.Forms.ComboBox();
            this.main_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.main_dataGridView, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.configurations_comboBox, 0, 0);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 2;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(360, 231);
            this.main_tableLayoutPanel.TabIndex = 2;
            // 
            // main_dataGridView
            // 
            this.main_dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.main_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.main_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.projectName,
            this.projectConfig});
            this.main_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_dataGridView.Location = new System.Drawing.Point(3, 38);
            this.main_dataGridView.Name = "main_dataGridView";
            this.main_dataGridView.Size = new System.Drawing.Size(354, 190);
            this.main_dataGridView.TabIndex = 0;
            // 
            // projectName
            // 
            this.projectName.HeaderText = "Имя";
            this.projectName.Name = "projectName";
            // 
            // projectConfig
            // 
            this.projectConfig.HeaderText = "Конфигурация";
            this.projectConfig.Name = "projectConfig";
            this.projectConfig.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // configurations_comboBox
            // 
            this.configurations_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.configurations_comboBox.Enabled = false;
            this.configurations_comboBox.FormattingEnabled = true;
            this.configurations_comboBox.Location = new System.Drawing.Point(3, 7);
            this.configurations_comboBox.Name = "configurations_comboBox";
            this.configurations_comboBox.Size = new System.Drawing.Size(354, 21);
            this.configurations_comboBox.TabIndex = 2;
            this.configurations_comboBox.Text = "Выберите конфигурацию для всех проектов...";
            this.configurations_comboBox.SelectedIndexChanged += new System.EventHandler(this.configurations_comboBox_SelectedIndexChanged);
            // 
            // ProjectsConfigurationsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "ProjectsConfigurationsControl";
            this.Size = new System.Drawing.Size(360, 231);
            this.main_tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.main_dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.DataGridView main_dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectName;
        private System.Windows.Forms.DataGridViewComboBoxColumn projectConfig;
        private System.Windows.Forms.ComboBox configurations_comboBox;
    }
}
