﻿/*
 * Плагин Парсер C#
 * Файл CSharpMain.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Головинов, Юхтин
 * 
 * Плагин предназначен для разбора файлов на языке C# с целью сбора информации о функциональных и информационных объектах,
 * ветвлениях и связях между отдельными файлами с последующим занесением в Хранилище. После сбора всей необходимой информации
 * плагином производится расстановка датчиков для последующего сбора трасс.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using ICSharpCode.NRefactory.Ast;
using ICSharpCode.SharpDevelop.Dom;
using ICSharpCode.SharpDevelop.Dom.NRefactoryResolver;
using IA.SlnResolver.CS;
using NRefactory = ICSharpCode.NRefactory;
using Dom = ICSharpCode.SharpDevelop.Dom;
using FileOperations;
using IA.SlnResolver;
using IA.Extensions;
using System.Text.RegularExpressions;
using System.Text;

namespace IA.Plugins.Parsers.CSharpParser
{
    internal class ProjectInternals
    {
        /// <summary>
        /// Cписок ссылок на сторонние библиотеки
        /// </summary>
        public List<string[]> refs;

        /// <summary>
        /// Cписок файлов проекта
        /// </summary>
        public List<string> files;

        /// <summary>
        /// Cписок переменных конфигурации проекта
        /// </summary>
        public string configVariables;

        /// <summary>
        /// 
        /// </summary>
        public Dom.DefaultProjectContent myProjectContent;
        //public Dom.ICompilationUnit[] lastCompilationUnit;

        /// <summary>
        /// 
        /// </summary>
        public CompilationUnit[] compilationUnit;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, Dictionary<string, string>> unicodeReplacements;

        /// <summary>
        /// 
        /// </summary>
        public Dom.ParseInformation[] parseInformation;

        /// <summary>
        /// 
        /// </summary>
        public string[] contents;

        /// <summary>
        /// 
        /// </summary>
        public AbstractReader[] frs;

        /// <summary>
        /// Счётчик количества обработанных файлов
        /// </summary>
        private static int filesCounter = 1;

        internal Dom.ProjectContentRegistry pcRegistry;

        /// <summary>
        /// Функция обнуления счётчика количества обработанных файлов
        /// </summary>
        public void resetCounter()
        {
            filesCounter = 1;
        }

        ~ProjectInternals()
        {
            refs.Clear();
            files.Clear();
            contents = new string[1];
            contents = null;
            frs = null;
            if (myProjectContent != null)
                myProjectContent.Dispose();
            if (pcRegistry != null)
                pcRegistry.Dispose();
            compilationUnit = null;
            parseInformation = null;
        }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="project">Проект для обработки</param>
        public ProjectInternals(SlnResolver.CS.ProjectBase project)
        {
            refs = new List<string[]>();
            files = new List<string>();

            //файлы
            List<SlnResolver.CS.ProjectSourceFile> fileLst = project.SourceFiles;
            List<string> csList = fileLst.Select(file => file.Location).ToList();
            //reference - прямые библиотечки
            List<SlnResolver.CS.ProjectReferencedDll> dllList = project.ReferencedDlls;
            int dllCount = dllList.Count;
            //reference - проекты
            List<SlnResolver.CS.ProjectReferencedProject> refprjList = project.ReferencedProjects;
            int projCount = refprjList.Count;
            //framework
            string framework = project.Settings.TargetFrameworkVersion;
            List<SlnResolver.CS.ProjectConfiguration> confArr = project.Settings.ConfigurationArray;

            refs.AddRange(CSharpMain.ResolveDeps(dllList, refprjList, framework, ProjectVS.GetCurrentConfiguration(project)));
            if (refs.Count != dllCount + projCount)
            {
                IA.Monitor.Log.Warning(PluginSettings.Name, "Ошибка в определении библиотек в проекте " + project.Name);
            }


            files.AddRange(csList); //заполняем список файлов с исходными текстами
            try
            {
                configVariables = ProjectVS.GetCurrentConfiguration(project).CompilationConstants; //заполняем переменные конфигурации проекта
            }
            catch
            {
                configVariables = String.Empty;
            }

            myProjectContent = new Dom.DefaultProjectContent { Language = Dom.LanguageProperties.CSharp };
            compilationUnit = new CompilationUnit[files.Count];
            parseInformation = new ParseInformation[files.Count];
            contents = new string[files.Count];
            unicodeReplacements = new Dictionary<int, Dictionary<string, string>>();
            frs = new AbstractReader[files.Count];

            for (int i = 0; i < files.Count; i++)
                if (System.IO.File.Exists(files[i]))
                {
                    try
                    {
                        frs[i] = new AbstractReader(files[i]);
                    }
                    catch (Exception ex)
                    {
                        //Формируем сообщение
                        string message = new string[]
                        {
                            "Не удалось открыть файл " + files[i] + ".",
                            ex.ToFullMultiLineMessage()
                        }.ToMultiLineString();

                        IA.Monitor.Log.Error(PluginSettings.Name, message);
                    }

                    contents[i] = frs[i].getText();
                }

            pcRegistry = new Dom.ProjectContentRegistry();
            pcRegistry.ActivatePersistence(Path.Combine(Path.GetTempPath(), "CSharpParser"));

            //инициализация ссылок
            myProjectContent.AddReferencedContent(pcRegistry.Mscorlib);
            foreach (string[] assembly in refs)
            {
                //string assemblyNameCopy = assembly[0]; // copy for anonymous method
                if (assembly[1] == "")
                {
                    //FIXME_YUKHTIN - потом убрать отсюда старое гумно
                    //Debug.Assert(project_old.ProjectName == project.Path);
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Не найдена сборка " + assembly[0] + " в проекте " + project.Path);
                    continue;
                }
                Dom.IProjectContent referenceProjectContent = pcRegistry.GetProjectContentForReference(assembly[0], assembly[1]);

                myProjectContent.AddReferencedContent(referenceProjectContent);

                if (referenceProjectContent is Dom.ReflectionProjectContent)
                {
                    (referenceProjectContent as Dom.ReflectionProjectContent).InitializeReferences();
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public void ParseInit()
        {
            for (int fileNum = 0; fileNum < files.Count; fileNum++)
            {

                if (!System.IO.File.Exists(files[fileNum]))
                {
                    IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)filesCounter++);
                    continue;
                }

                Dom.ICompilationUnit newCompilationUnit;
                //fixing unicode substrings by replacing them
                string strRegex = @"('\\u[0-9a-fA-F]+')";
                var myRegex = new Regex(strRegex);
                unicodeReplacements.Add(fileNum, new Dictionary<string, string>());
                var unicodeReplacement = unicodeReplacements[fileNum];
                var newContents = new StringBuilder();
                using (StringReader sr = new StringReader(contents[fileNum]))
                {
                    var line = string.Empty;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.IndexOf("'\\u") > 0)
                        {
                            var split = myRegex.Split(line);
                            foreach (var unicode in split)
                            {
                                if (unicode.StartsWith("'\\u"))
                                {
                                    if (!line.Contains('"') || line.IndexOf('"') == line.LastIndexOf('"') || line.IndexOf("'\\u") < line.IndexOf('"') || line.IndexOf("'\\u") > line.LastIndexOf('"'))
                                    {
                                        var replacement = "\"UNICODE_REPLACEMENT" + unicodeReplacement.Keys.Count.ToString() + "\"";
                                        line = line.Replace(unicode, replacement);
                                        unicodeReplacement.Add(replacement, unicode);
                                    }
                                    else
                                    {
                                        var crop = line.Substring(line.IndexOf('"'), line.LastIndexOf('"') - line.IndexOf('"')+1);
                                        var replacement = "\"UNICODE_REPLACEMENT" + unicodeReplacement.Keys.Count.ToString() + "\"";
                                        line = line.Replace(crop, replacement);
                                        unicodeReplacement.Add(replacement, crop);
                                        break;
                                    }
                                }
                            }
                        }
                        newContents.AppendLine(line);
                    }
                }

                TextReader textReader = new StringReader(newContents.ToString());

                using (NRefactory.IParser parser = NRefactory.ParserFactory.CreateParser(NRefactory.SupportedLanguage.CSharp, textReader))
                {
                    parser.Lexer.SetConditionalCompilationSymbols(configVariables);
                    parser.Lexer.EvaluateConditionalCompilation = true;
                    parser.Parse();
                    compilationUnit[fileNum] = parser.CompilationUnit;
                    NRefactoryASTConvertVisitor converter = new NRefactoryASTConvertVisitor(myProjectContent, NRefactory.SupportedLanguage.CSharp);
                    parser.CompilationUnit.AcceptVisitor(converter, null);
                    newCompilationUnit = converter.Cu;
                    if (parser.Errors.Count > 0)
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Ошибка парсинга " + files[fileNum] + " " + parser.Errors.ErrorOutput);
                }
                myProjectContent.UpdateCompilationUnit(null, newCompilationUnit, files[fileNum]);
                parseInformation[fileNum] = new Dom.ParseInformation(newCompilationUnit);
            }
        }
    }

    class CSharpMain
    {
        //Проект
        internal Dictionary<string, ProjectInternals> projectInternals; //внутренности распарсенных проектов

        Parser _parser;
        readonly Store.Storage _storage;

        //Списки Dll'ек
        /// <summary>
        /// Список библиотек, найденных во всех подпапках c:\Windows\assembly\GAC\
        /// </summary>
        private static Dictionary<string, List<string>> gac_DLLs;

        /// <summary>
        /// Список библиотек, найденных во всех подпапках c:\Windows\assembly\GAC_MSIL\
        /// </summary>
        private static Dictionary<string, List<string>> gac_msil_DLLs;

        /// <summary>
        /// Список библиотек, найденных во всех подпапках c:\Windows\Microsoft.NET\Framework\
        /// </summary>
        private static Dictionary<string, List<string>> framework_DLLs;

        /// <summary>
        /// Список библиотек, найденных во всех подпапках C:\Windows\Microsoft.NET\assembly\GAC_MSIL\
        /// </summary>
        private static Dictionary<string, List<string>> gac_dotnet_DLLs;

        /// <summary>
        /// Список библиотек, найденных во всех подпапках C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0\
        /// </summary>
        private static Dictionary<string, List<string>> framework_v40;

        /// <summary>
        /// Список библиотек, найденных во всех подпапках C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5\
        /// </summary>
        private static Dictionary<string, List<string>> framework_v45;

        /// <summary>
        /// Список библиотек, найденных во всех подпапках C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5.1\
        /// </summary>
        private static Dictionary<string, List<string>> framework_v451;

        /// <summary>
        /// Список библиотек, найденных во всех подпапках внутри папки с исходными текстами
        /// </summary>
        private static Dictionary<string, List<string>> source_folder__DLLs;

        /// <summary>
        /// 
        /// </summary>
        private static List<SlnResolver.CS.ProjectBase> CSProjects;

        /// <summary>
        /// 
        /// </summary>
        static internal string chr_include = null;

        /// <summary>
        /// Функция заполняет списки библиотек gac_DLLs, gac_msil_DLLs, framework_DLLs, gac_dotnet_DLLs, source_folder__DLLs.
        /// </summary>
        private static void GetDLLLists()
        {
            // FIXME
            //Monitor.Monitor.WriteToTempInfo = "Создание списков библиотек...";

            //GAC
            gac_DLLs = new Dictionary<string, List<string>>();
            foreach (string dllPath in Directory.EnumerateFiles(@"c:\Windows\assembly\GAC\", "*.dll", SearchOption.AllDirectories))
            {
                string dllName = dllPath.Substring(dllPath.LastIndexOf("\\") + 1).ToLower();
                if (!gac_DLLs.ContainsKey(dllName))
                    gac_DLLs.Add(dllName, new List<string> { dllPath });
                else
                    gac_DLLs[dllName].Add(dllPath);
            }

            //GAC_MSIL
            gac_msil_DLLs = new Dictionary<string, List<string>>();
            foreach (string dllPath in Directory.EnumerateFiles(@"c:\Windows\assembly\GAC_MSIL\", "*.dll", SearchOption.AllDirectories))
            {
                string dllName = dllPath.Substring(dllPath.LastIndexOf("\\") + 1).ToLower();
                if (!gac_msil_DLLs.ContainsKey(dllName))
                    gac_msil_DLLs.Add(dllName, new List<string> { dllPath });
                else
                    gac_msil_DLLs[dllName].Add(dllPath);
            }

            //Framework
            framework_DLLs = new Dictionary<string, List<string>>();
            foreach (string dllPath in Directory.EnumerateFiles(@"c:\Windows\Microsoft.NET\Framework\", "*.dll", SearchOption.AllDirectories))
            {
                string dllName = dllPath.Substring(dllPath.LastIndexOf("\\") + 1).ToLower();
                if (!framework_DLLs.ContainsKey(dllName))
                    framework_DLLs.Add(dllName, new List<string> { dllPath });
                else
                    framework_DLLs[dllName].Add(dllPath);
            }

            //Framework64
            //string dotNet64 = @"c:\Windows\Microsoft.NET\Framework64\"; FIXME

            //GAC.NET
            gac_dotnet_DLLs = new Dictionary<string, List<string>>();
            foreach (string dllPath in Directory.EnumerateFiles(@"C:\Windows\Microsoft.NET\assembly\GAC_MSIL\", "*.dll", SearchOption.AllDirectories))
            {
                string dllName = dllPath.Substring(dllPath.LastIndexOf("\\") + 1).ToLower();
                if (!gac_dotnet_DLLs.ContainsKey(dllName))
                    gac_dotnet_DLLs.Add(dllName, new List<string> { dllPath });
                else
                    gac_dotnet_DLLs[dllName].Add(dllPath);
            }

            //Framework v4.0
            if (Directory.Exists(@"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0\"))
            {
                framework_v40 = new Dictionary<string, List<string>>();
                foreach (string dllPath in Directory.EnumerateFiles(@"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0\", "*.dll", SearchOption.AllDirectories))
                {
                    string dllName = dllPath.Substring(dllPath.LastIndexOf("\\") + 1).ToLower();
                    if (!framework_v40.ContainsKey(dllName))
                        framework_v40.Add(dllName, new List<string> { dllPath });
                    else
                        framework_v40[dllName].Add(dllPath);
                }
            }

            //Framework v4.5
            if (framework_v40 != null && framework_v40.Count != 0 && !Directory.Exists(@"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5\"))
            {
                framework_v45 = new Dictionary<string, List<string>>(framework_v40);
            }
            else
                if (Directory.Exists(@"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5\"))
                {
                    framework_v45 = new Dictionary<string, List<string>>();
                    foreach (string dllPath in Directory.EnumerateFiles(@"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5\", "*.dll", SearchOption.AllDirectories))
                    {
                        string dllName = dllPath.Substring(dllPath.LastIndexOf("\\") + 1).ToLower();
                        if (!framework_v45.ContainsKey(dllName))
                            framework_v45.Add(dllName, new List<string> { dllPath });
                        else
                            framework_v45[dllName].Add(dllPath);
                    }
                }

            //Framework v4.5.1
            if (framework_v40 != null && framework_v40.Count != 0 && !Directory.Exists(@"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5.1\"))
            {
                framework_v451 = new Dictionary<string, List<string>>(framework_v40);
            }
            else
                if (Directory.Exists(@"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5.1\"))
                {
                    framework_v451 = new Dictionary<string, List<string>>();
                    foreach (string dllPath in Directory.EnumerateFiles(@"c:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5.1\", "*.dll", SearchOption.AllDirectories))
                    {
                        string dllName = dllPath.Substring(dllPath.LastIndexOf("\\") + 1).ToLower();
                        if (!framework_v451.ContainsKey(dllName))
                            framework_v451.Add(dllName, new List<string> { dllPath });
                        else
                            framework_v451[dllName].Add(dllPath);
                    }
                }

            // Source Folder from FillFileList
            source_folder__DLLs = new Dictionary<string, List<string>>();
            foreach (string dllPath in Directory.EnumerateFiles(PluginSettings.BuiltSourcesDirectoryPath, "*.dll", SearchOption.AllDirectories))
            {
                string dllName = dllPath.Substring(dllPath.LastIndexOf("\\") + 1).ToLower();
                if (!source_folder__DLLs.ContainsKey(dllName))
                    source_folder__DLLs.Add(dllName, new List<string> { dllPath });
                else
                    source_folder__DLLs[dllName].Add(dllPath);
            }

            // FIXME
            //Monitor.Monitor.WriteToTempInfo = "Создание списков библиотек успешно завершено.";
        }

        /// <summary>
        /// Функция разрерашет пути до всех внешних библиотек, присоединённых к проекту
        /// </summary>
        /// <param name="dllList">Список внешних библиотек присоединённых к проекту</param>
        /// <param name="refprjList">Список зависимых проектов</param>
        /// <param name="framework">Текущая версия Framework'a</param>
        /// <param name="mainConf">Текущая конфигурация проекта</param>
        /// <returns>Список, в котором каждый элемент есть массив строк типа(название библиотеки, полный разрешённый путь до неё)</returns>
        static public List<string[]> ResolveDeps(List<SlnResolver.CS.ProjectReferencedDll> dllList, List<SlnResolver.CS.ProjectReferencedProject> refprjList, string framework, SlnResolver.CS.ProjectConfiguration mainConf)
        {
            List<string[]> retLibs = new List<string[]>();
            //сначала ищем все проекты
            foreach (SlnResolver.CS.ProjectReferencedProject prj in refprjList)
            {
                if (prj.Location.ToLower().EndsWith(".csproj"))
                {
                    //сишарп
                    SlnResolver.CS.ProjectBase project = ProjectVS.GetProjectByPath(PluginSettings.CsProjects, prj.Location);
                    if (project == null)
                        continue; //что делать если проект не нашли??

                    SlnResolver.CS.ProjectConfiguration conf = ProjectVS.GetCurrentConfiguration(project);
                    if (conf != null)
                        if (System.IO.File.Exists(conf.OutputPath.Replace("/", "\\") + "\\" + project.Settings.AssemblyName + ".dll"))
                        {
                            dllList.Add(new ProjectReferencedDll() { Name = project.Settings.AssemblyName, FullName = project.Settings.AssemblyName, Location = Path.GetFullPath(conf.OutputPath.Replace("/", "\\") + "\\" + project.Settings.AssemblyName + ".dll") });
                        }
                        else
                        {
                            dllList.Add(new ProjectReferencedDll() { Name = project.Settings.AssemblyName, FullName = project.Settings.AssemblyName });
                        }
                }
                else
                {
                    if (prj.Location.ToLower().EndsWith(".vcproj"))
                    {
                        SlnResolver.VC.ProjectBase project = ProjectVS.ParseVCProject(prj.Location);
                        SlnResolver.VC.ProjectConfiguration conf = ProjectVS.SetCurrentConfiguration(project, mainConf.ConfigurationName);
                        if (System.IO.File.Exists(conf.OutputDirectory.Replace("/", "\\") + "\\" + project.Settings.AssemblyName + ".dll"))
                        {
                            dllList.Add(new ProjectReferencedDll() { Name = project.Settings.AssemblyName, FullName = project.Settings.AssemblyName, Location = Path.GetFullPath(conf.OutputDirectory.Replace("/", "\\") + "\\" + project.Settings.AssemblyName + ".dll") });
                        }
                        else
                        {
                            dllList.Add(new ProjectReferencedDll() { Name = project.Settings.AssemblyName, FullName = project.Settings.AssemblyName });
                        }
                    }
                    else
                    {
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Файлы проектов типа " + prj.Location.Substring(prj.Name.LastIndexOf(".") + 1) + " не обрабатываются");
                    }
                }
            }
            foreach (SlnResolver.CS.ProjectReferencedDll dll in dllList)
            {
                string libPath = dll.Location;
                if (libPath != "")
                {
                    foreach (string key in PluginSettings.Prefixes.Keys)
                    {
                        if (libPath.Contains(key.ToLower()))
                            libPath = libPath.Replace(key.ToLower(), PluginSettings.Prefixes[key].ToLower());
                    }
                    if (File.Exists(libPath))
                    {
                        if (!retLibs.Contains(new string[2] { dll.Name, libPath }))
                            retLibs.Add(new string[2] { dll.Name, libPath });
                        continue;
                    }
                }

                string searchName = dll.Name.ToLower() + ".dll";
                #region Начинаем поиск по ранее созданным спискам
                //GAC
                if (gac_DLLs.ContainsKey(searchName))
                    libPath = gac_DLLs[searchName][0];

                //GAC_MSIL
                if (libPath == "")
                    if (gac_msil_DLLs.ContainsKey(searchName))
                        libPath = gac_msil_DLLs[searchName][0];

                //Framework
                if (libPath == "")
                    if (framework_DLLs.ContainsKey(searchName))
                        foreach (string path in framework_DLLs[searchName])
                            if (path.Contains(framework))
                                libPath = path;

                ////Framework64
                ////string dotNet32 = @"c:\Windows\Microsoft.NET\Framework64\"; FIXME

                //GAC.NET
                if (libPath == "")
                    if (gac_dotnet_DLLs.ContainsKey(searchName))
                        foreach (string path in gac_dotnet_DLLs[searchName])
                            if (path.Contains(framework))
                                libPath = path;

                //v.4.5
                if (libPath == "")
                    if (framework_v45.ContainsKey(searchName))
                        foreach (string path in framework_v45[searchName])
                            if (path.Contains(framework))
                                libPath = path;

                //v.4.5.1
                if (libPath == "")
                    if (framework_v451.ContainsKey(searchName))
                        foreach (string path in framework_v451[searchName])
                            if (path.Contains(framework))
                                libPath = path;

                //ищем по всем исходникам
                if (libPath == "")
                    if (source_folder__DLLs.ContainsKey(searchName))
                        libPath = source_folder__DLLs[searchName][0];

                #endregion
                if (libPath != string.Empty)
                {
                    if (!retLibs.Contains(new string[2] { dll.Name, libPath }))
                        retLibs.Add(new string[2] { dll.Name, libPath });
                }
                else
                {
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Не найдена библиотека " + dll.Name);
                }

            }
            return retLibs;
        }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="storage">Хранилище, с которым необходимо работать</param>
        public CSharpMain(Store.Storage storage)
        {
            gac_DLLs = new Dictionary<string, List<string>>();
            gac_msil_DLLs = new Dictionary<string, List<string>>();
            framework_DLLs = new Dictionary<string, List<string>>();
            gac_dotnet_DLLs = new Dictionary<string, List<string>>();
            framework_v45 = new Dictionary<string, List<string>>();
            framework_v451 = new Dictionary<string, List<string>>();
            source_folder__DLLs = new Dictionary<string, List<string>>();
            Parser.clearStatic();
            if (PluginSettings.UnixMode)
                chr_include = "<Reference Include=\"IACSharpSensor\"><HintPath>IACSharpSensor.dll</HintPath></Reference>";
            else
                chr_include = "<Reference Include=\"IACSharpSensor\"><HintPath>C:\\Windows\\system32\\IACSharpSensor.dll</HintPath></Reference>"; //C# include string
            GetDLLLists();

            _storage = storage;

            projectInternals = new Dictionary<string, ProjectInternals>();
            CSProjects = new List<SlnResolver.CS.ProjectBase>();
        }

        /// <summary>
        /// Основаная функция, реализующая работу плагина
        /// </summary>
        public void Run()
        {
            //ищем все файлы решений
            List<string> solutions = new List<string>(Directory.EnumerateFiles(PluginSettings.BuiltSourcesDirectoryPath, "*.sln", SearchOption.AllDirectories));

            //ищём все файлы проектов
            //этот список в дальнейшем будет использоваться для того чтобы понять какие проекты входят в решения а какие просто лежат отдельно и должны обрабатываеться также отдельно
            List<string> projects = new List<string>(Directory.EnumerateFiles(PluginSettings.BuiltSourcesDirectoryPath, "*.csproj", SearchOption.AllDirectories));

            List<string> projList = new List<string>();
            foreach (string currentProject in from currentSolution in solutions let solution = new SolutionVS() select solution.GetProjects(currentSolution) into solutionProjects from currentProject in solutionProjects.Where(currentProject => !projList.Contains(currentProject)) select currentProject)
            {
                projList.Add(currentProject);
                projects.Remove(currentProject);
            }
            projList.AddRange(projects);

            //узнаём количество файлов для обработки
            int filesAmount = 0; //общее количество файлов для обработки

            //Отбираем все С#-ые файлы
            List<string> files = _storage.files.EnumerateFiles(Store.enFileKind.fileWithPrefix).Where(file => file.fileType.ContainsLanguages().Contains("C_Sharp")).Select(file => file.FullFileName_Original).ToList();

            List<string> filesMet = new List<string>();
            //Проверка на разумность
            if (files == null || files.Count == 0)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, "Файлы для обработки не найдены.");
            }
            List<string> toDelProj = new List<string>();
            foreach (string project in projList)
            {
                /*ProjectVS_Old projectOBJ = new ProjectVS_Old(project);

                //FIXME_YUKHTIN - потом убрать отсюда старое гумно
                //а я воспользуюсь этим циклом, чтобы распарсить проекты новым методом и заполнить массив
                ProjectBase csProject = ProjectVS.ParseCSProject(project);
                CSProjects.Add(csProject);

                Debug.Assert(csProject.SourceFiles.Count == projectOBJ.GetModules.Count);

                filesAmount += projectOBJ.GetModules.Count;*/
                var proj = ProjectVS.GetProjectByPath(PluginSettings.CsProjects, project);
                if (proj == null) { toDelProj.Add(project); continue; }
                filesAmount += proj.SourceFiles.Count;

                //Проходим по всем файлам проекта
                foreach (ProjectSourceFile projectSourceFile in proj.SourceFiles)
                {
                    //Меняем префикс
                    string storageFileName = projectSourceFile.Location.Replace(PluginSettings.BuiltSourcesDirectoryPath, _storage.appliedSettings.FileListPath);

                    //Смотрим, есть ли файл
                    if (!files.Contains(storageFileName) && !filesMet.Contains(storageFileName.ToLower()))
                    {
                        //Если нет - выводим ошибку, что файл не найден в Хранилище. Скорее всего - это автосгенерированный файл
                        IA.Monitor.Log.Error(PluginSettings.Name, "Файл <" + storageFileName + "> не найден в Хранилище.");
                    }
                    else
                    {
                        //Если есть - забываем про него
                        files.Remove(storageFileName);
                        filesMet.Add(storageFileName.ToLower());
                    }
                }
            }

            toDelProj.ForEach(g => projList.Remove(g));

            //gathering all unused CS files in one project
            if (PluginSettings.ProcessInsufficient)
            {
                var proj = new ProjectBase();
                proj.Settings = new ProjectSettings()
                {
                    AssemblyName = "InsufficientFilesIA",
                    ConfigurationArray = new List<ProjectConfiguration>() { new ProjectConfiguration() {CompilationConstants = PluginSettings.InsufficientDefines, ConfigurationName = "Release", isCurrent = true, OutputPath = PluginSettings.BuiltSourcesDirectoryPath} },
                };
                proj.SourceFiles = new List<ProjectSourceFile>();
                files.ForEach(g => proj.SourceFiles.Add(new ProjectSourceFile() {Name = g.Substring(g.LastIndexOf("\\") + 1), Location = g}));
                proj.Name = "InsufficientFilesIA.csproj";
                proj.Path = Path.Combine(_storage.appliedSettings.FileListPath, "InsufficientFilesIA.csproj");
                projList.Add(proj.Path);
                PluginSettings.CsProjects.Add(proj);
            }


            //FIXME - морально устаревшая проверка
            //Проверяем, что обработаны все файлы
            //if (files.Count != 0)
            //    foreach (string file in files)
            //        IA.Monitor.Log.Warning(PluginSettings.Name, "Файл <" + file + "> не обработан.");

            //обновляем задания            
            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)filesAmount);
            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)1).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)projList.Count);
            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)2).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)filesAmount);
            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)3).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)projList.Count);
            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)4).Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)filesAmount);

            List<string> processedFilesFirst = new List<string>();
            List<string> processedFilesSecond = new List<string>();

            //обрабатываем все проекты в 2 стадии
            for (int i = 0; i < 2; i++)
            {
                //если первую стадию проходить не надо
                if (i == 0 && (!PluginSettings.IsStage1 || PluginSettings.IsStage1Completed))
                {
                    //FIXME - запилить отображение распарсенных файлов
                    IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)1).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)projList.Count);
                    IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)2).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)filesAmount);

                    IA.Monitor.Log.Warning(PluginSettings.Name, "Стадия 1: пропущена.");
                    continue;
                }

                if (i == 1 && (!PluginSettings.IsStage2 || PluginSettings.IsStage2Completed))
                {
                    //FIXME - запилить отображение распарсенных файлов
                    IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)3).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)projList.Count);
                    IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)4).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)filesAmount);
                    IA.Monitor.Log.Warning(PluginSettings.Name, "Стадия 2: пропущена.");
                    continue;
                }
                int projectCounter = 1;
                int filesCounter = 1;

                foreach (string project in projList)
                {
                    ProjectInternals prj;
                    _parser = new Parser(_storage)
                                  {
                                      SensorNumber =
                                          (_parser == null)
                                              ? PluginSettings.FirstSensorNumber
                                              : _parser.SensorNumber
                                  };

                    _parser.SensorFlag = PluginSettings.IsLevel2;
                    _parser.ThirdSensor = PluginSettings.IsLevel3;

                    if (!projectInternals.ContainsKey(project))
                    {
                        //первый проход
                        //FIXME_YUKHTIN - потом убрать отсюда старое гумно
                        prj = new ProjectInternals(ProjectVS.GetProjectByPath(PluginSettings.CsProjects, project));
                        prj.ParseInit();
                        //projectInternals.Add(_project, prj);
                    }
                    else
                        prj = projectInternals[project];
                    for (int j = 0; j < prj.files.Count; j++)
                    {
                        if (prj.contents[j] == null)
                        {
                            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)(i == 0 ? 2 : 4)).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)filesCounter++);

                            continue; // файл не был считан
                        }

                        string fileFolder = prj.files[j].Substring(0,
                                                                   prj.files[j].LastIndexOf("\\",
                                                                                            StringComparison.Ordinal) +
                                                                   1);

                        _parser.Unit = prj.compilationUnit[j];
                        _parser.Contents = prj.contents[j];
                        _parser.FilePath = prj.files[j];
                        _parser.ParseInformation = prj.parseInformation[j];
                        HostCallback.GetCurrentProjectContent = () => prj.myProjectContent;


                        if (i == 0)
                        {
                            if (!processedFilesFirst.Contains(prj.files[j].ToLower()))
                            {
                                _parser.firstStep();
                                processedFilesFirst.Add(prj.files[j].ToLower());
                            }
                        }
                        else
                        {
                            if (!processedFilesSecond.Contains(prj.files[j].ToLower()))
                            {
                                if (
                                    !Directory.Exists(fileFolder.Replace(_storage.appliedSettings.FileListPath,
                                                                         PluginSettings.SourcesWithSensorsDirectoryPath(_storage) + "\\")))
                                    Directory.CreateDirectory(fileFolder.Replace(_storage.appliedSettings.FileListPath,
                                                                                 PluginSettings.SourcesWithSensorsDirectoryPath(_storage) + "\\"));
                                prj.frs[j].writeTextToFile(prj.files[j].Replace(_storage.appliedSettings.FileListPath, PluginSettings.SourcesWithSensorsDirectoryPath(_storage) + "\\"), _parser.secondStep(prj.unicodeReplacements[j]));
                                processedFilesSecond.Add(prj.files[j].ToLower());
                            }
                        }


                        if (i == 1)
                            IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)filesCounter);

                        IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)(i == 0 ? 2 : 4)).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)filesCounter++);
                    }
                    IA.Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)(i == 0 ? 1 : 3)).Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)projectCounter++);

                    if (i == 1 && !project.EndsWith("\\InsufficientFilesIA.csproj")) //пропатчим проектные файлы
                    {
                        AbstractReader buffer = null;
                        try
                        {
                            buffer = new AbstractReader(project);
                        }
                        catch (Exception ex)
                        {
                            //Формируем сообщение
                            string message = new string[]
                            {
                                "Не удалось открыть файл " + project + ".",
                                ex.ToFullMultiLineMessage()
                            }.ToMultiLineString();

                            IA.Monitor.Log.Error(PluginSettings.Name, message);
                        }

                        if (buffer.getText().Contains("<Reference Include="))
                        {
                            string new_csproj = buffer.getText().Insert(buffer.getText().IndexOf("<Reference Include="), chr_include);
                            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(project.Replace(PluginSettings.BuiltSourcesDirectoryPath, PluginSettings.SourcesWithSensorsDirectoryPath(_storage) + "\\")));
                            buffer.writeTextToFile(project.Replace(PluginSettings.BuiltSourcesDirectoryPath, PluginSettings.SourcesWithSensorsDirectoryPath(_storage) + "\\"), new_csproj);
                        }
                        else
                        {
                            //Если нет атрибута ссылок в проекте, то создаем новый
                            if (buffer.getText().Contains("<ItemGroup>"))
                            {
                                string new_csproj = buffer.getText().Insert(buffer.getText().IndexOf("<ItemGroup>"),
                                    "\t<ItemGroup>\n\t\t" + chr_include + "\n\t</ItemGroup>\n");
                                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(project.Replace(PluginSettings.BuiltSourcesDirectoryPath, PluginSettings.SourcesWithSensorsDirectoryPath(_storage) + "\\")));
                                buffer.writeTextToFile(project.Replace(PluginSettings.BuiltSourcesDirectoryPath, PluginSettings.SourcesWithSensorsDirectoryPath(_storage) + "\\"), new_csproj);
                            }
                        }
                    }
                }
            }
        }
    }
}

