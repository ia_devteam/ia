using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;


namespace IA.Plugins.Utils.ReplaceSensors
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.REPLACE_SENSORS;

        /// <summary>
        /// Имя плагина
        /// </summary>
        internal const string Name = "Замена датчиков";

        #region Settings

        /// <summary>
        /// Папка с входными данными
        /// </summary>
        internal static string OldSourcesDirectoryPath;

        /// <summary>
        /// Папка с выходными данными
        /// </summary>
        internal static string NewSourcesDirectoryPath;

        /// <summary>
        /// Правила замены датчиков
        /// </summary>
        internal static string SensorsReplaceRules;

        /// <summary>
        /// Файл замены датчиков
        /// </summary>
        internal static string SensorsReplaceFilePath;

        /// <summary>
        /// Список шаблонов
        /// </summary>
        internal static StringCollection PatternsCollection;

        /// <summary>
        /// Список шаблонов
        /// </summary>
        internal static List<string> PatternsList
        {
            get
            {
                List<string> ret = new List<string>();

                if (PluginSettings.PatternsCollection != null)
                    ret.AddRange(PluginSettings.PatternsCollection.Cast<string>().ToList());

                return ret;
            }
            set
            {
                PluginSettings.PatternsCollection = new StringCollection();

                if (value != null)
                    PluginSettings.PatternsCollection.AddRange(value.ToArray());
            }
        }

        #endregion

        /// <summary>
        /// Задачи плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Просмотрено файлов при замене датчиков")]
            FILES_PROCESSING
        }
    }

    public class ReplaceSensors : IA.Plugin.Interface
    {
        /// <summary>
        /// Текущие настройки
        /// </summary>
        Settings settings;
                
        List<string[]> arraySensors;
        List<int> errorSensors;
        Dictionary<int, int> dictSensors;        
        List<string> FileList;
        
        int CountReplace, CountError, TCountReplace, TCountError;
        bool bCanRun;
        Encoding encoding;

        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW | Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS | Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
		}

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "ReplaceTable.log")))
            {
                for (int i = 0; i < arraySensors.Count; i++)
                    writer.WriteLine("{0}->{1}", arraySensors[i][0], arraySensors[i][1]);
            }
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "ErrorReplace.log")))
            {
                for (int i = 0; i < errorSensors.Count; i++)
                    writer.WriteLine("{0}", errorSensors[i]);
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            arraySensors = new List<string[]>();
            errorSensors = new List<int>();
            dictSensors = new Dictionary<int, int>();            
            FileList = new List<string>();
            CountReplace = 0;
            CountError = 0;
            TCountReplace = 0;
            TCountError = 0;

            PluginSettings.OldSourcesDirectoryPath = Properties.Settings.Default.OldSourcesDirectoryPath;
            PluginSettings.NewSourcesDirectoryPath = Properties.Settings.Default.NewSourcesDirectoryPath;
            PluginSettings.SensorsReplaceRules = Properties.Settings.Default.SensorsReplaceRules;
            PluginSettings.SensorsReplaceFilePath = Properties.Settings.Default.SensorsReplaceFilePath;
            PluginSettings.PatternsCollection = Properties.Settings.Default.PatternsCollection;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.OldSourcesDirectoryPath = settingsBuffer.GetString();
            PluginSettings.NewSourcesDirectoryPath = settingsBuffer.GetString();
            PluginSettings.SensorsReplaceRules = settingsBuffer.GetString();
            PluginSettings.SensorsReplaceFilePath = settingsBuffer.GetString();

            int patternsCount = settingsBuffer.GetInt32();
            PluginSettings.PatternsCollection = new StringCollection();
            for (int i = 0; i < patternsCount; i++)
                PluginSettings.PatternsCollection.Add(settingsBuffer.GetString());
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            arraySensors = new List<string[]>();
            errorSensors = new List<int>();
            dictSensors = new Dictionary<int, int>();
            FileList = new List<string>();
            FileList = new List<string>();
            CountReplace = 0;
            CountError = 0;
            TCountReplace = 0;
            TCountError = 0;

            if (String.IsNullOrEmpty(PluginSettings.SensorsReplaceFilePath))
            {
                List<string[]> lsh = new List<string[]>();
                string[] ssi = PluginSettings.SensorsReplaceRules.Split(';');
                for (int i = 0; i < ssi.Length; i++)
                {
                    string[] st = ssi[i].Split(new char[2] { '-', ',' });
                    lsh.Add(st);
                }
                for (int k = 0; k < lsh.Count; k++)
                {
                    int i0 = Convert.ToInt32(lsh[k][0]);
                    int imax = Convert.ToInt32(lsh[k][1]);
                    int j = Convert.ToInt32(lsh[k][2]);

                    for (int i = i0; i <= imax; i++)
                    {
                        dictSensors.Add(i, j);
                        j++;
                    }
                }
            }
            else
            {
                // Чтение из файла
                TextReader tr=null;
                try
                {
                    tr = new StreamReader(PluginSettings.SensorsReplaceFilePath);
                    while (true)
                    {
                        string s = tr.ReadLine();
                        if (s == null)
                            break;
                        string[] s1 = s.Split('-');
                        if (s1.Length != 2)
                            break;
                        dictSensors.Add(int.Parse(s1[0]), int.Parse(s1[1]));
                    }
                }
                finally
                {
                    if(tr!=null)
                        tr.Close();
                }

            }

            DirectoryInfo di = new DirectoryInfo(PluginSettings.OldSourcesDirectoryPath);
            FileInfo[] files;
            files = di.GetFiles("*.*", SearchOption.AllDirectories);

            FileList.Clear();
            foreach (FileInfo file in files)
            {
                if(!file.FullName.Contains(".svn"))
                    FileList.Add(file.FullName);
            }

            List<string> PatternList1 = new List<string>();
            foreach (string pattern in PluginSettings.PatternsList)
            {
                string s = pattern;
                s = Regex.Replace(s, @"\(|\)|\'", new MatchEvaluator(RegexControls));
                s = Regex.Replace(s, "#", new MatchEvaluator(PatNum));
                PatternList1.Add(s);
            }
//            PatternList = PatternList1;
            TCountReplace = 0;
            TCountError = 0;

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)FileList.Count);
            ulong counter = 0;
            for (int i = 0; i < FileList.Count; i++)
            {
                CountReplace = 0;
                CountError = 0;
                byte[] arrb = ReadFile(FileList[i]);
                string s = DefaultEncoding(arrb);
                for (int j = 0; j < PluginSettings.PatternsList.Count; j++)
                {
                    s = Regex.Replace(s, PatternList1[j], new MatchEvaluator(ReplacePattern));
                }
                byte[] arrc = DefaultDecoding(s);
                WriteFile(FileList[i].Replace(PluginSettings.OldSourcesDirectoryPath, PluginSettings.NewSourcesDirectoryPath), arrc);
                TCountReplace += CountReplace;
                TCountError += CountError;
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.OldSourcesDirectoryPath = PluginSettings.OldSourcesDirectoryPath;
            Properties.Settings.Default.NewSourcesDirectoryPath = PluginSettings.NewSourcesDirectoryPath;
            Properties.Settings.Default.SensorsReplaceRules = PluginSettings.SensorsReplaceRules;
            Properties.Settings.Default.SensorsReplaceFilePath = PluginSettings.SensorsReplaceFilePath;
            Properties.Settings.Default.PatternsCollection = PluginSettings.PatternsCollection;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.OldSourcesDirectoryPath);
            settingsBuffer.Add(PluginSettings.NewSourcesDirectoryPath);
            settingsBuffer.Add(PluginSettings.SensorsReplaceRules);
            settingsBuffer.Add(PluginSettings.SensorsReplaceFilePath);
        
            settingsBuffer.Add(PluginSettings.PatternsList.Count);
            foreach (string pattern in PluginSettings.PatternsList)
                settingsBuffer.Add(pattern);
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), 0)
                };
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
			    return null;
            }
		}

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                settings = new Settings();
                return settings;
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return this.CustomSettingsWindow;
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (!Directory.Exists(PluginSettings.OldSourcesDirectoryPath))
            {
                message = "Каталог с входными данными не найден.";
                return false;
            }

            if (!Directory.Exists(PluginSettings.NewSourcesDirectoryPath))
            {
                message = "Каталог для выходных данных не найден.";
                return false;
            }

            if (string.IsNullOrWhiteSpace(PluginSettings.SensorsReplaceRules) && !File.Exists(PluginSettings.SensorsReplaceFilePath))
            {
                message = "Не настроены правила замены датчиков или не выбран файл замены датчиков.";
                return false;
            }

            if (PluginSettings.PatternsList.Count == 0)
            {
                message = "Нет шаблонов функций датчиков.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        #endregion

        static string PatNum(Match m)
        {
            return @"+[\d{1,9}]+";
        }
        static string RegexControls(Match m)
        {
            return @"\" + m.ToString();
        }
        string ReplacePattern(Match m)
        {
            string x = m.ToString();
            x = Regex.Replace(x, @"\d{1,9}", new MatchEvaluator(ReplaceSensor));
            return x;
        }
        string ReplaceSensor(Match m)
        {
            string x = m.ToString();
            int val;
            if (dictSensors.TryGetValue(int.Parse(x), out val))
            {
                CountReplace++;
                arraySensors.Add(new string[] { x, val.ToString() });
                return (val.ToString());
            }
            else
            {
                CountError++;
                errorSensors.Add(int.Parse(x));
                return x;
            }
            /*
            bool bFound = false;
            for (int i = 0; i < arraySensors.Count; i++)
            {
                if (arraySensors[i][0] == x)
                {
                    bFound = true;
                    CountReplace++;
                    x = arraySensors[i][1];
                    break;
                }
            }
            if (!bFound)
                CountError++;
            return x;
             */ 
        }
        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;
                buffer = new byte[length];
                if (length != fileStream.Read(buffer, 0, length))
                {
                    //error;
                }
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }
        public static void WriteFile(string filePath, byte[] buffer)
        {
            FileStream fileStream = null;
            try
            {
                if(!Directory.Exists(System.IO.Path.GetDirectoryName(filePath)))
                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(filePath));
                fileStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                fileStream.Write(buffer, 0, buffer.Length);
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Ошибка доступа: " + ex.Message);
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();
            }
        }
        public string DefaultEncoding(byte[] arrayIn)
        {
            string txt;
            txt = System.Text.Encoding.Default.GetString(arrayIn);
            encoding = Encoding.Default;
            if (txt.StartsWith("п»ї"))
            {
                txt = System.Text.Encoding.UTF8.GetString(arrayIn);
                encoding = Encoding.UTF8;
            }
            else if (txt.StartsWith("яю"))
            {
                txt = System.Text.Encoding.Unicode.GetString(arrayIn);
                encoding = Encoding.Unicode;
            }

            return txt;
        }
        public byte[] DefaultDecoding(string s)
        {
            return encoding.GetBytes(s); 
        }

    }
}
