﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace IA.Plugins.Utils.ReplaceSensors
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        internal Settings()
        {
            InitializeComponent();                      

            // Изначальная видимость кнопок
            patternsList_remove_button.Enabled = false;

            oldSourcesDirectoryPath_textBox.Text = PluginSettings.OldSourcesDirectoryPath;
            newSourcesDirectoryPath_textBox.Text = PluginSettings.NewSourcesDirectoryPath;
            sensorsReplaceRules_textBox.Text = PluginSettings.SensorsReplaceRules;
            sensorsReplaceFilePath_textBox.Text = PluginSettings.SensorsReplaceFilePath;
            patternsList_listBox.Items.AddRange(PluginSettings.PatternsList.ToArray());
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.OldSourcesDirectoryPath = oldSourcesDirectoryPath_textBox.Text;
            PluginSettings.NewSourcesDirectoryPath = newSourcesDirectoryPath_textBox.Text;
            PluginSettings.SensorsReplaceRules = sensorsReplaceRules_textBox.Text;
            PluginSettings.SensorsReplaceFilePath = sensorsReplaceFilePath_textBox.Text;
            PluginSettings.PatternsList = patternsList_listBox.Items.Cast<string>().ToList();
        }

        /// <summary>
        /// Обработчик - выбрали шаблон датчика в списке шаблонов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void patternsList_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Задаем видимость кнопок
            patternsList_remove_button.Enabled = patternsList_listBox.SelectedItem != null;

            //Отображаем строку шаблона
            patternsList_textBox.Text = patternsList_listBox.SelectedItem != null ? patternsList_listBox.SelectedItem.ToString() : string.Empty;
            patternsList_textBox.Refresh();
        }

        /// <summary>
        /// Кнопка "Удалить шаблон"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void patternsList_remove_button_Click(object sender, EventArgs e)
        {
            //Удаляем шаблон из списка
            if (patternsList_listBox.SelectedItem != null)
                patternsList_listBox.Items.Remove(patternsList_listBox.SelectedItem);

            //Очищаем строку шаблона
            patternsList_textBox.Text = string.Empty;
            patternsList_textBox.Refresh();
        }

        /// <summary>
        /// Кнопка "Добавить шаблон"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void patternsList_add_button_Click(object sender, EventArgs e)
        {
            //Сохраняем заданное значение
            string pattern = patternsList_textBox.Text;

            //Очищаем строку шаблона
            patternsList_textBox.Text = string.Empty;
            patternsList_textBox.Refresh();

            //Если ничего не задано, ничего не делаем
            if (string.IsNullOrWhiteSpace(pattern))
                return;

            //Если такой шаблон уже есть в списке, ничего не делаем
            if (patternsList_listBox.Items.Contains(pattern))
                return;

            //Добавляем шаблон в список
            patternsList_listBox.Items.Add(pattern);       
        }
    }
}
