﻿namespace IA.Plugins.Utils.ReplaceSensors
{
    partial class Settings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sensorsReplaceRules_textBox = new System.Windows.Forms.TextBox();
            this.patternsList_listBox = new System.Windows.Forms.ListBox();
            this.patternsList_add_button = new System.Windows.Forms.Button();
            this.patternsList_remove_button = new System.Windows.Forms.Button();
            this.patternsList_textBox = new System.Windows.Forms.TextBox();
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.oldSourcesDirectoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.oldSourcesDirectoryPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.sensorsReplaceRules_groupBox = new System.Windows.Forms.GroupBox();
            this.sensorsReplaceRules_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sensorsReplaceFilePath_groupBox = new System.Windows.Forms.GroupBox();
            this.sensorsReplaceFilePath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.patternsList_groupBox = new System.Windows.Forms.GroupBox();
            this.patternsList_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.newSourcesDirectoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.newSourcesDirectoryPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.custom_toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.settings_tableLayoutPanel.SuspendLayout();
            this.oldSourcesDirectoryPath_groupBox.SuspendLayout();
            this.sensorsReplaceRules_groupBox.SuspendLayout();
            this.sensorsReplaceRules_tableLayoutPanel.SuspendLayout();
            this.sensorsReplaceFilePath_groupBox.SuspendLayout();
            this.patternsList_groupBox.SuspendLayout();
            this.patternsList_tableLayoutPanel.SuspendLayout();
            this.newSourcesDirectoryPath_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // sensorsReplaceRules_textBox
            // 
            this.sensorsReplaceRules_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.sensorsReplaceRules_textBox.Location = new System.Drawing.Point(3, 5);
            this.sensorsReplaceRules_textBox.Name = "sensorsReplaceRules_textBox";
            this.sensorsReplaceRules_textBox.Size = new System.Drawing.Size(663, 20);
            this.sensorsReplaceRules_textBox.TabIndex = 3;
            this.custom_toolTip.SetToolTip(this.sensorsReplaceRules_textBox, "{\\\\d-\\\\d,\\\\d}[;\\\\d-\\\\d,\\\\d]\\n(Нач. номер-Кон. номер, Новый нач. номер)");
            // 
            // patternsList_listBox
            // 
            this.patternsList_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patternsList_listBox.FormattingEnabled = true;
            this.patternsList_listBox.Location = new System.Drawing.Point(203, 3);
            this.patternsList_listBox.Name = "patternsList_listBox";
            this.patternsList_tableLayoutPanel.SetRowSpan(this.patternsList_listBox, 3);
            this.patternsList_listBox.Size = new System.Drawing.Size(463, 80);
            this.patternsList_listBox.TabIndex = 7;
            this.patternsList_listBox.SelectedIndexChanged += new System.EventHandler(this.patternsList_listBox_SelectedIndexChanged);
            // 
            // patternsList_add_button
            // 
            this.patternsList_add_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.patternsList_add_button.Location = new System.Drawing.Point(3, 4);
            this.patternsList_add_button.Name = "patternsList_add_button";
            this.patternsList_add_button.Size = new System.Drawing.Size(94, 42);
            this.patternsList_add_button.TabIndex = 8;
            this.patternsList_add_button.Text = "Добавить шаблон";
            this.patternsList_add_button.UseVisualStyleBackColor = true;
            this.patternsList_add_button.Click += new System.EventHandler(this.patternsList_add_button_Click);
            // 
            // patternsList_remove_button
            // 
            this.patternsList_remove_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.patternsList_remove_button.Location = new System.Drawing.Point(103, 3);
            this.patternsList_remove_button.Name = "patternsList_remove_button";
            this.patternsList_remove_button.Size = new System.Drawing.Size(94, 44);
            this.patternsList_remove_button.TabIndex = 10;
            this.patternsList_remove_button.Text = "Удалить шаблон";
            this.patternsList_remove_button.UseVisualStyleBackColor = true;
            this.patternsList_remove_button.Click += new System.EventHandler(this.patternsList_remove_button_Click);
            // 
            // patternsList_textBox
            // 
            this.patternsList_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.patternsList_tableLayoutPanel.SetColumnSpan(this.patternsList_textBox, 2);
            this.patternsList_textBox.Location = new System.Drawing.Point(3, 57);
            this.patternsList_textBox.Name = "patternsList_textBox";
            this.patternsList_textBox.Size = new System.Drawing.Size(194, 20);
            this.patternsList_textBox.TabIndex = 11;
            this.custom_toolTip.SetToolTip(this.patternsList_textBox, "Функция с датчиком, где вместо номера символ #");
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.oldSourcesDirectoryPath_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Controls.Add(this.sensorsReplaceRules_groupBox, 0, 2);
            this.settings_tableLayoutPanel.Controls.Add(this.sensorsReplaceFilePath_groupBox, 0, 3);
            this.settings_tableLayoutPanel.Controls.Add(this.patternsList_groupBox, 0, 4);
            this.settings_tableLayoutPanel.Controls.Add(this.newSourcesDirectoryPath_groupBox, 0, 1);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 5;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 146F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(681, 331);
            this.settings_tableLayoutPanel.TabIndex = 23;
            // 
            // oldSourcesDirectoryPath_groupBox
            // 
            this.oldSourcesDirectoryPath_groupBox.Controls.Add(this.oldSourcesDirectoryPath_textBox);
            this.oldSourcesDirectoryPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oldSourcesDirectoryPath_groupBox.Location = new System.Drawing.Point(3, 3);
            this.oldSourcesDirectoryPath_groupBox.Name = "oldSourcesDirectoryPath_groupBox";
            this.oldSourcesDirectoryPath_groupBox.Size = new System.Drawing.Size(675, 49);
            this.oldSourcesDirectoryPath_groupBox.TabIndex = 0;
            this.oldSourcesDirectoryPath_groupBox.TabStop = false;
            this.oldSourcesDirectoryPath_groupBox.Text = "Папка с исходными текстами для замены";
            // 
            // oldSourcesDirectoryPath_textBox
            // 
            this.oldSourcesDirectoryPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oldSourcesDirectoryPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.oldSourcesDirectoryPath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.oldSourcesDirectoryPath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.oldSourcesDirectoryPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.oldSourcesDirectoryPath_textBox.Name = "oldSourcesDirectoryPath_textBox";
            this.oldSourcesDirectoryPath_textBox.Size = new System.Drawing.Size(669, 25);
            this.oldSourcesDirectoryPath_textBox.TabIndex = 0;
            // 
            // sensorsReplaceRules_groupBox
            // 
            this.sensorsReplaceRules_groupBox.Controls.Add(this.sensorsReplaceRules_tableLayoutPanel);
            this.sensorsReplaceRules_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorsReplaceRules_groupBox.Location = new System.Drawing.Point(3, 113);
            this.sensorsReplaceRules_groupBox.Name = "sensorsReplaceRules_groupBox";
            this.sensorsReplaceRules_groupBox.Size = new System.Drawing.Size(675, 49);
            this.sensorsReplaceRules_groupBox.TabIndex = 2;
            this.sensorsReplaceRules_groupBox.TabStop = false;
            this.sensorsReplaceRules_groupBox.Text = "Правила замены датчиков";
            // 
            // sensorsReplaceRules_tableLayoutPanel
            // 
            this.sensorsReplaceRules_tableLayoutPanel.ColumnCount = 1;
            this.sensorsReplaceRules_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.sensorsReplaceRules_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.sensorsReplaceRules_tableLayoutPanel.Controls.Add(this.sensorsReplaceRules_textBox, 0, 0);
            this.sensorsReplaceRules_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorsReplaceRules_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.sensorsReplaceRules_tableLayoutPanel.Name = "sensorsReplaceRules_tableLayoutPanel";
            this.sensorsReplaceRules_tableLayoutPanel.RowCount = 1;
            this.sensorsReplaceRules_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.sensorsReplaceRules_tableLayoutPanel.Size = new System.Drawing.Size(669, 30);
            this.sensorsReplaceRules_tableLayoutPanel.TabIndex = 0;
            // 
            // sensorsReplaceFilePath_groupBox
            // 
            this.sensorsReplaceFilePath_groupBox.Controls.Add(this.sensorsReplaceFilePath_textBox);
            this.sensorsReplaceFilePath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorsReplaceFilePath_groupBox.Location = new System.Drawing.Point(3, 168);
            this.sensorsReplaceFilePath_groupBox.Name = "sensorsReplaceFilePath_groupBox";
            this.sensorsReplaceFilePath_groupBox.Size = new System.Drawing.Size(675, 49);
            this.sensorsReplaceFilePath_groupBox.TabIndex = 3;
            this.sensorsReplaceFilePath_groupBox.TabStop = false;
            this.sensorsReplaceFilePath_groupBox.Text = "Файл замены датчиков";
            // 
            // sensorsReplaceFilePath_textBox
            // 
            this.sensorsReplaceFilePath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorsReplaceFilePath_textBox.Location = new System.Drawing.Point(3, 16);
            this.sensorsReplaceFilePath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.sensorsReplaceFilePath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.sensorsReplaceFilePath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.sensorsReplaceFilePath_textBox.Name = "sensorsReplaceFilePath_textBox";
            this.sensorsReplaceFilePath_textBox.Size = new System.Drawing.Size(669, 25);
            this.sensorsReplaceFilePath_textBox.TabIndex = 0;
            // 
            // patternsList_groupBox
            // 
            this.patternsList_groupBox.Controls.Add(this.patternsList_tableLayoutPanel);
            this.patternsList_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patternsList_groupBox.Location = new System.Drawing.Point(3, 223);
            this.patternsList_groupBox.Name = "patternsList_groupBox";
            this.patternsList_groupBox.Size = new System.Drawing.Size(675, 105);
            this.patternsList_groupBox.TabIndex = 4;
            this.patternsList_groupBox.TabStop = false;
            this.patternsList_groupBox.Text = "Список шаблонов функций датчиков";
            // 
            // patternsList_tableLayoutPanel
            // 
            this.patternsList_tableLayoutPanel.ColumnCount = 3;
            this.patternsList_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.patternsList_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.patternsList_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.patternsList_tableLayoutPanel.Controls.Add(this.patternsList_listBox, 2, 0);
            this.patternsList_tableLayoutPanel.Controls.Add(this.patternsList_add_button, 0, 0);
            this.patternsList_tableLayoutPanel.Controls.Add(this.patternsList_remove_button, 1, 0);
            this.patternsList_tableLayoutPanel.Controls.Add(this.patternsList_textBox, 0, 1);
            this.patternsList_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patternsList_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.patternsList_tableLayoutPanel.Name = "patternsList_tableLayoutPanel";
            this.patternsList_tableLayoutPanel.RowCount = 3;
            this.patternsList_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.patternsList_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.patternsList_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.patternsList_tableLayoutPanel.Size = new System.Drawing.Size(669, 86);
            this.patternsList_tableLayoutPanel.TabIndex = 0;
            // 
            // newSourcesDirectoryPath_groupBox
            // 
            this.newSourcesDirectoryPath_groupBox.Controls.Add(this.newSourcesDirectoryPath_textBox);
            this.newSourcesDirectoryPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newSourcesDirectoryPath_groupBox.Location = new System.Drawing.Point(3, 58);
            this.newSourcesDirectoryPath_groupBox.Name = "newSourcesDirectoryPath_groupBox";
            this.newSourcesDirectoryPath_groupBox.Size = new System.Drawing.Size(675, 49);
            this.newSourcesDirectoryPath_groupBox.TabIndex = 5;
            this.newSourcesDirectoryPath_groupBox.TabStop = false;
            this.newSourcesDirectoryPath_groupBox.Text = "Путь к выходному каталогу с замененными датчиками";
            // 
            // newSourcesDirectoryPath_textBox
            // 
            this.newSourcesDirectoryPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newSourcesDirectoryPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.newSourcesDirectoryPath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.newSourcesDirectoryPath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.newSourcesDirectoryPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.newSourcesDirectoryPath_textBox.Name = "newSourcesDirectoryPath_textBox";
            this.newSourcesDirectoryPath_textBox.Size = new System.Drawing.Size(669, 25);
            this.newSourcesDirectoryPath_textBox.TabIndex = 1;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(681, 331);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.oldSourcesDirectoryPath_groupBox.ResumeLayout(false);
            this.sensorsReplaceRules_groupBox.ResumeLayout(false);
            this.sensorsReplaceRules_tableLayoutPanel.ResumeLayout(false);
            this.sensorsReplaceRules_tableLayoutPanel.PerformLayout();
            this.sensorsReplaceFilePath_groupBox.ResumeLayout(false);
            this.patternsList_groupBox.ResumeLayout(false);
            this.patternsList_tableLayoutPanel.ResumeLayout(false);
            this.patternsList_tableLayoutPanel.PerformLayout();
            this.newSourcesDirectoryPath_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TextBox sensorsReplaceRules_textBox;
        public System.Windows.Forms.ListBox patternsList_listBox;
        private System.Windows.Forms.Button patternsList_add_button;
        private System.Windows.Forms.Button patternsList_remove_button;
        private System.Windows.Forms.TextBox patternsList_textBox;
        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox oldSourcesDirectoryPath_groupBox;
        private System.Windows.Forms.GroupBox sensorsReplaceRules_groupBox;
        private System.Windows.Forms.TableLayoutPanel sensorsReplaceRules_tableLayoutPanel;
        private System.Windows.Forms.GroupBox sensorsReplaceFilePath_groupBox;
        private System.Windows.Forms.TableLayoutPanel patternsList_tableLayoutPanel;
        private System.Windows.Forms.GroupBox patternsList_groupBox;
        private Controls.TextBoxWithDialogButton oldSourcesDirectoryPath_textBox;
        private Controls.TextBoxWithDialogButton sensorsReplaceFilePath_textBox;
        private System.Windows.Forms.GroupBox newSourcesDirectoryPath_groupBox;
        private Controls.TextBoxWithDialogButton newSourcesDirectoryPath_textBox;
        private System.Windows.Forms.ToolTip custom_toolTip;
    }
}
