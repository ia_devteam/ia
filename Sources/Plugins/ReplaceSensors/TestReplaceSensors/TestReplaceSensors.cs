﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;

namespace TestReplaceSensors
{
    [TestClass]
    public class TestReplaceSensors
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        [TestMethod]
        public void ReplaceSensors_Test()
        {            
            string replacedDirectoryPath = string.Empty;
            testUtils.RunTest(
                string.Empty,                                                   // sourcePrefixPart
                new IA.Plugins.Utils.ReplaceSensors.ReplaceSensors(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(Path.Combine(testMaterialsPath, @"ReplaceSensors\Clear-withsensors"));
                    writer.Add(replacedDirectoryPath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.SOURCES_LAB));
                    writer.Add("1-100000,500001");
                    writer.Add(string.Empty);
                    writer.Add(1);
                    writer.Add("RNT_sensor_function_call(#u");
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.REPLACE_SENSORS, writer);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    bool isEqualSources = TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"ReplaceSensors\Clear-withsensors-new"), replacedDirectoryPath);
                    bool isEqualReports = TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"ReplaceSensors\Reports"), reportsPath);

                    Assert.IsTrue(isEqualSources, "Исходные тексты с измененными идентификаторами датчиков не совпадают с эталонными");
                    Assert.IsTrue(isEqualReports, "Отчеты плагина не совпадают с эталонными");

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
    }
}
