﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using TestUtils;

namespace TestDelphiSensors
{
    [TestClass]
    public class TestDelphiSensors
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Проверка корректности отработки плагина на исходных текстах Pascal.
        /// Проверяются эталонные исходные тексты со вставленными датчиками и текущие сгенерированные
        /// </summary>
        [TestMethod]
        public void DelphiSensors_Files()
        { 
            const string sourcePrefixPart = @"Sources\Pascal";
            const string understandReportsPrefixPart = @"Understand\Pascal";
            const string origSourcePath = @"B:\IA_b\Tests\Materials\Sources\Pascal\";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.DelphiSensors.DelphiSensors(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);

                    var bw = Store.Table.WriterPool.Get();
                    bw.Add(origSourcePath);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.DELPHI_SENSORS, bw);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "Pascal");
                    string reportsPath = Path.Combine(testMaterialsPath, "DelphiSensors");

                    return TestUtilsClass.Reports_Directory_TXT_Compare(labPath, reportsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {                    
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
    }
}
