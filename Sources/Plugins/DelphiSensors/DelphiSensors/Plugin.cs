/*
 * Плагин DelphiSensors
 * Файл Plugin.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Громилов
 * 
 * Плагин предназначен для вставки датчиков в исходные очищенные тексты языка Pascal по 2-ому уровню. 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Analyses.DelphiSensors
{
    /// <summary>
    /// Класс внутренних статических настроек
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Вставка датчиков на уровне ветвей (Pascal)";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.DELPHI_SENSORS;

        internal static string understandSourcePath = String.Empty;

        /// <summary>
        /// Список задач работы плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка файлов")]
            FILES_PROCESSING,
            [Description("Сохранение файлов с датчиками")]
            FILES_WITH_SENSORS_SAVING
        }
    }

    /// <summary>
    /// Плагин DelphiSensors
    /// </summary>
    public class DelphiSensors : IA.Plugin.Interface
    {
        /// <summary>
        /// Текущее хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Класс вставки датчиков
        /// </summary>
        DelphiSensors_Logic delphiSensors;
        
        #region PluginInterface Members
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.NONE;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            // Сохраняем текущее хранилище
            this.storage = storage;

            PluginSettings.understandSourcePath = Properties.Settings.Default.UnderstandSourcePath;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {            
            try
            {
                PluginSettings.understandSourcePath = settingsBuffer.GetString();
            }
            catch
            {

            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {      
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            settingsBuffer.Add(PluginSettings.understandSourcePath);

            Properties.Settings.Default.UnderstandSourcePath = PluginSettings.understandSourcePath;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            // Изначально сообщение пустое и нет ошибок
            message = String.Empty;

            // Все хорошо, ошибок нет
            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    // Задача обработки файлов
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description()),
                    // Задача сохранения исходных текстов со вставленными датчиками
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_WITH_SENSORS_SAVING.Description())
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return null;
            }
        }  
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            // Вызываем конструктор класса DelphiSensors_Logic
            // Передаем ему:
            // storage - текущее хранилище
            delphiSensors = new DelphiSensors_Logic(storage);

            // Запускаем алгоритм поиска мест вставки датчиков
            if (!delphiSensors.Run())
                return;

            // Вставляем датчики и сохраняем результаты
            delphiSensors.InsertAndSave();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }
        
        #endregion             
    }
}
