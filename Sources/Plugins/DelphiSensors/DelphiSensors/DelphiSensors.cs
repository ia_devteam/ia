﻿/*
 * Плагин DelphiSensors
 * Файл DelphiSensors.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Громилов
 * 
 * Плагин предназначен для вставки датчиков в исходные очищенные тексты по 2-ому уровню. 
 * В процессе выполнения плагина генерируются отчеты о вставленных датчиках.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using IA.Extensions;
using IOController;
using Store;
using FileOperations;

namespace IA.Plugins.Analyses.DelphiSensors
{
    /// <summary>
    /// Основной класс по вставке датчиков в Pascal
    /// </summary>
    internal class DelphiSensors_Logic
    {
        /// <summary>
        /// Класс информации о датчике
        /// </summary> 
        class Sensor
        {
            /// <summary>
            /// Тип датчиков для отчета
            /// </summary>
            internal Kind kind { get; set; }
            /// <summary>
            /// Тип датчика для вставки в исходники
            /// </summary> 
            internal string Type { get; set; }
            /// <summary>
            /// Номер датчика
            /// </summary> 
            internal ulong Number { get; set; }
            /// <summary>
            /// Сущность, куда вставлен датчик
            /// </summary> 
            internal string UnitName { get; set; }
            /// <summary>
            /// Файл, куда вставлен датчик
            /// </summary> 
            internal string FilePath { get; set; }
            /// <summary>
            /// Смещение, относительно начала файла
            /// </summary> 
            internal int Offset { get; set; }
            /// <summary>
            /// Смещение функции, относительно начала файла
            /// </summary> 
            internal ulong Line { get; set; }
        }

        /// <summary>
        /// Класс информации о сущности (функция, процедура и т.п.)
        /// </summary>
        class Unit
        {
            /// <summary>
            /// Тип сущности
            /// </summary>
            internal string UnitType;
            /// <summary>
            /// Имя сущности
            /// </summary> 
            internal string UnitName { get; set; }
            /// <summary>
            /// Файл, где содержится сущность
            /// </summary> 
            internal string FilePath { get; set; }
            /// <summary>
            /// Строка, относительно начала файла
            /// </summary> 
            internal ulong Line { get; set; }
            /// <summary>
            /// Столбец, относительно начала строки
            /// </summary>
            internal ulong Column { get; set; }
        }               

        #region Private Fields

        /// <summary>
        /// Хранилище
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Текущий номер датчика
        /// </summary>
        private ulong currentSensorNamuber;

        /// <summary>
        /// Список всех датчиков
        /// </summary> 
        private HashSet<Sensor> allSensors;

        /// <summary>
        /// Список всех сущностей
        /// </summary> 
        private HashSet<Unit> pascalUnits;

        /// <summary>
        /// Список файлов, написанных на языке программирования Pascal
        /// </summary> 
        private HashSet<IFile> pascalFiles;

        /// <summary>
        /// Каталог с исходными текстами
        /// </summary> 
        private string inputDirectoryPath;

        /// <summary>
        /// Каталог с исходными текстами со вставленными датчиками
        /// </summary>
        private string labsDirectoryPath;
        
        #endregion

        /// <summary>
        /// Основной конструктор класса DelphiSensors
        /// </summary>
        /// <param name="storage">Хранилище.</param>
        internal DelphiSensors_Logic(Storage storage)
        {
            this.storage = storage;
            this.pascalFiles = new HashSet<IFile>(storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Where(file => file.fileType.ContainsLanguages().Contains("Pascal")));
            this.inputDirectoryPath = storage.appliedSettings.FileListPath;
            this.labsDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "Pascal");
            //Текущий датчик - последний датчик в Хранилище
            this.currentSensorNamuber = (storage.sensors.EnumerateSensors().Count() != 0) ? storage.sensors.EnumerateSensors().Last().ID : 1;
            this.pascalUnits = new HashSet<Unit>();
            this.allSensors = new HashSet<Sensor>();

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)pascalFiles.Count());
        }

        /// <summary>
        /// Поиск сущностей в отчете Understand'а "ProgramUnits.txt"
        /// </summary>
        private bool ReadUnits()
        {
            //Получаем путь к файлу Understand
            string programUnitsFilePath = storage.GetSharedFilePath(Store.SharedFiles.enSharedFiles.UnderstandProtocolProgramUnits);
            
            //Проверка существования протокола
            if (!FileController.IsExists(programUnitsFilePath))
            {
                IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось найти файл протоколов Understand.");
                return false;
            }

            //Подмена путей в протоколе
            string contents;
            using (StreamReader reader = new StreamReader(programUnitsFilePath))
            {
                contents = reader.ReadToEnd();

                string str = contents.Split(new char[] { '[', ',', ']' }).ElementAtOrDefault(1);

                if (str != null)
                {
                    string understandSourcePath = PluginSettings.understandSourcePath;
                    if (!String.IsNullOrWhiteSpace(understandSourcePath) && str.IndexOf(understandSourcePath, StringComparison.InvariantCultureIgnoreCase) > -1)
                    {
                        contents = contents.Replace(understandSourcePath, storage.appliedSettings.FileListPath).Replace("\\\\", "\\");
                    }
                    else
                    {
                        int index = str.IndexOf("src_clear");
                        if (index == -1)
                            index = str.IndexOf("src_original");

                        if (index != -1)
                        {
                            str = str.Substring(0, index);
                            contents = contents.Replace(str, storage.WorkDirectory.Path + "\\");
                        }
                        else
                        {
                            IA.Monitor.Log.Warning(PluginSettings.Name, "Протоколы были сгенерированы не из папки с исходными файлами в Хранилище");
                        }
                    }
                }                
            }                        
            using (StreamWriter writer = new StreamWriter(programUnitsFilePath))
            {
                writer.Write(contents);
            }

            //Поиск сущностей
            string[] patterns = { "(Procedure)", "(Function)", "(Virtual Function)", "(Constructor)", "(Virtual Destructor)", "(Destructor)" };
            string DefPattern = "Define ";

            using (StreamReader reader = new StreamReader(programUnitsFilePath))
            {
                string buffer;
                while ((buffer = reader.ReadLine()) != null)
                {
                    if (patterns.Any(s => buffer.Contains(s)))
                    {
                        string[] str = buffer.Trim().Split(new char[] { '(', ')' }).Select(x => x.Trim()).ToArray();

                        Unit new_Unit = new Unit()
                        {
                            UnitName = str[0],
                            UnitType = str[1]
                        };

                        buffer = reader.ReadLine();
                        while (!string.IsNullOrWhiteSpace(buffer))
                        {
                            if (buffer.IndexOf(DefPattern) != -1)
                            {
                                str = buffer.Trim().Split(new char[] { '[', ',', ']' }).Select(x => x.Trim()).ToArray();

                                new_Unit.FilePath = str[1];
                                new_Unit.Line = ulong.Parse(str[2]);
                                pascalUnits.Add(new_Unit);

                                break;
                            }
                            buffer = reader.ReadLine();
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Основная процедура запуска плагина
        /// </summary>
        internal bool Run()
        {
            // Чтение протокола
            if (!ReadUnits())
                return false;

            ulong counter = 0;
            //Формирование мест для вставки датчиков
            foreach (IFile file in pascalFiles)
            {
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                FormingSensorList(file.FullFileName_Original);              
            }

            return true;
        }

        /// <summary>
        /// Формирование списка датчиков
        /// </summary>
        /// <param name="path">Путь до файла</param>
        private void FormingSensorList(string path)
        {
            //Читаем файл
            string buffer = new AbstractReader(path).getText();

            //Переменная, проверяющая вложенность функций. Содержит правильное смещение начала функции.
            int beginIndex = -2;
            //Переменная содержит смещение конца функции
            int postendIndex = 0;
            HashSet<Unit> unitsInFile = new HashSet<Unit>(pascalUnits.Where(x => x.FilePath.ToLower() == path.ToLower()).OrderBy(x => x.Line));

            // Формирование списка датчиков
            while (unitsInFile.Count > 0)
            {
                int funcIndex = FindFunctionDefinition(unitsInFile.ElementAt(0), buffer);
                if (unitsInFile.Count > 1)
                {
                    int tmp = FindFunctionDefinition(unitsInFile.ElementAt(1), buffer);
                    if (beginIndex == -2)
                    {
                        beginIndex = BeginStatement(funcIndex, tmp, buffer);
                        if (beginIndex == -2)
                        {
                            ParsingFunction(funcIndex, buffer, unitsInFile.ElementAt(0));
                            unitsInFile.Remove(unitsInFile.ElementAt(0));
                        }
                        else
                        {
                            postendIndex = ParsingFunction(tmp, buffer, unitsInFile.ElementAt(1));
                            postendIndex += buffer.Substring(postendIndex).IndexOf("begin\r\n");
                            unitsInFile.Remove(unitsInFile.ElementAt(1));
                        }
                    }
                    else
                    {
                        if (postendIndex != tmp)
                        {
                            ParsingFunction(postendIndex, buffer, unitsInFile.ElementAt(0));
                            unitsInFile.Remove(unitsInFile.ElementAt(0));
                            beginIndex = -2;
                        }
                        else
                            unitsInFile.Remove(unitsInFile.ElementAt(0));
                    }
                }
                else
                {
                    ParsingFunction(funcIndex, buffer, unitsInFile.ElementAt(0));
                    unitsInFile.Remove(unitsInFile.ElementAt(0));
                }
            }
        }

        /// <summary>
        /// Меняем позицию в файле и буфер остатка текста файла, передвигаясь на следующую строку
        /// </summary>
        /// <param name="nextIndex">Смещение</param>
        /// <param name="buffer">Остаток буфера файла</param>
        private void NextLine(ref int nextIndex, ref string buffer)
        {
            //Изменяем смещение для вставки датчика
            nextIndex += buffer.IndexOf('\n') + 1;
            //Удаляем из буфера лишние символы
            buffer = buffer.Substring(buffer.IndexOf('\n') + 1);
        }

        /// <summary>
        /// Меняем позицию в файле и буфер остатка текста файла по ключевому слову, передвигаясь на следующую строку
        /// </summary>
        /// <param name="nextIndex">Смещение</param>
        /// <param name="buffer">Остаток буфера файла</param>
        /// <param name="keyword">Ключевое слово</param>
        private void NextLine(ref int nextIndex, ref string buffer, string keyword)
        {
            //Изменяем смещение для вставки датчика
            nextIndex += buffer.IndexOf(keyword) + keyword.Length;
            //Удаляем из буфера лишние символы
            buffer = buffer.Substring(buffer.IndexOf(keyword) + keyword.Length);
        }

        /// <summary>
        /// Смещаемся и отрезаем строку из буфера и сохраняем ее
        /// </summary>
        /// <param name="currentLine">Отрезанная строка</param>
        /// <param name="nextIndex">Смещение</param>
        /// <param name="buffer">Остаток буфера файла</param>
        private void NextLineWithCurrentLine(ref string currentLine, ref int nextIndex, ref string buffer)
        {
            //Смещаемся
            NextLine(ref nextIndex, ref buffer);
            //Отрезаем строку
            currentLine = buffer.Substring(0, buffer.IndexOf('\n') + 1);
        }

        /// <summary>
        /// Смещаемся и отрезаем строку из буфера и сохраняем ее.
        /// Смещение происходит, если встретилось ключевое слово.
        /// </summary>
        /// <param name="currentLine">Отрезанная строка</param>
        /// <param name="nextIndex">Смещение</param>
        /// <param name="buffer">Остаток буфера файла</param>
        /// <param name="keyWord">Ключевое слово</param>
        private void NextLineWithCurrentLine(ref string currentLine, ref int nextIndex, ref string buffer, string keyWord)
        {
            //Смещаемся
            NextLine(ref nextIndex, ref buffer, keyWord);
            //Отрезаем строку
            currentLine = buffer.Substring(0, buffer.IndexOf('\n') + 1);
        }

        /// <summary>
        /// Определение смещения в файле для последующей вставки датчиков в функцию
        /// </summary>
        /// <param name="beginIndex">Смещение после объявления функции в файле</param>
        /// <param name="buffer">Текст функции</param>
        /// <param name="unit">Сущность</param>
        /// <returns>Возвращает смещения окончания функции</returns>
        private int ParsingFunction(int beginIndex, string buffer, Unit unit)
        {
            //Строка функции
            string currentLine;

            //Буфер, который содержит код программы, начиная со смещения анализируемой функции
            buffer = buffer.Substring(beginIndex);

            //Смещение для вставки датчика
            int nextIndex = beginIndex;
            
            //Счетчик вложенности begin'ов
            List<int> beginCount = new List<int>();
            //Так как уже стоим перед именем функции, то вложенность нулевая
            beginCount.Add(0);
            
            //Счетчик ветвей
            List<int> stemSt = new List<int>();

            //Счетчик ветвей выбора
            List<int> caseSt = new List<int>();

            bool label = false;
            bool stDeleted = false;

            //i - это глубина вложенности ветвей, включая препроцессорные вставки
            //s - 
            //c - 
            int i = 0, s = -1, c = -1;
            
            //были ли препроцессорные вставки. Со вложенностью - не работает. На FIXME поставлен.
            //Индекс IFDEF
            int indexIFDEF = -1;
            //Индекс ELSEDEF
            int indexELSEDEF = -1;
            //Индекс ENDIFDEF
            int indexENDIFDEF = -1;

            //
            bool NotEnd = true;

            //Циклимся 
            while (true)
            {
                currentLine = buffer.Substring(0, buffer.IndexOf('\n') + 1);
                // Если открывающаяся фигурная скобка
                if (isInCurlyBracket(currentLine))
                {
                    // Ищем закрывающуюся фигурную скобку, т.к. всё, что между ними - комментарии
                    while (!isOutCurlyBracket(currentLine))
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }

                    //Нашли закрывающуюся фигурную скобку
                    //Смещаемся
                    NextLine(ref nextIndex, ref buffer);
                }
                // Если двойная косая черта
                else if (isDoubleSlash(currentLine))
                {
                    //Смещаемся
                    NextLine(ref nextIndex, ref buffer);
                }
                // Если препроцессорная вставка IFDEF
                else if (isIFDEF(currentLine))
                {
                    //Новая ветка 
                    //Считаем эту ветку как некую новую функцию
                    i++; 
                    beginCount.Add(0);
                    
                    //Ставим текущее смещение для IFDEF, чтобы знать, что мы в IFDEF'е
                    indexIFDEF = nextIndex;

                    //Смещаемся
                    NextLine(ref nextIndex, ref buffer);
                }
                else if (isELSEDEF(currentLine))
                {
                    // Если препроцессорная вставка ELSEDEF
                    //Говорим, что покинули IFDEF
                    indexIFDEF = -1;

                    //Ставим текущее смещение для ELSEDEF, чтобы знать, что мы в ELSEDEF'е
                    indexELSEDEF = nextIndex;

                    //Спускаемся по веткам обратно
                    beginCount.RemoveAt(i--);

                    //Смещаемся
                    NextLine(ref nextIndex, ref buffer);
                }
                else if (isENDIFDEF(currentLine))
                {
                    // Если препроцессорная вставка ENDIFDEF
                    //Выходим из IFDEF и ELSEDEF
                    if (indexELSEDEF > -1)
                        indexELSEDEF = -1;
                    else indexIFDEF = -1;

                    //Ставим текущее смещение для ENDIFDEF, чтобы знать, что мы ушли из предпроцессорных вставок
                    indexENDIFDEF = nextIndex;

                    //Смещаемся
                    NextLine(ref nextIndex, ref buffer);
                }
                else if (isIFStatement(currentLine) || isELSEStatement(currentLine))
                {
                    // Если if
                    if (isIFStatement(currentLine))
                    {
                        //Нужно сначала проверить, что раньше были вставлено слово 'begin', чтобы вставить 'end' 
                        //(случай, когда один был оператор в ветке, а после вставки функции датчика не нарушить синтаксис)

                        //Каждый блок следует закончить верно
                        while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                        {
                            //Пишем в файл 'end'
                            AddSenor(nextIndex, unit, "END");

                            //А после - ставим датчик после конца ветки
                            AddSenor(nextIndex, unit, "INTERNAL");

                            //Уходим из текущей ветки
                            stemSt.RemoveAt(s--);
                        }
                    }
                    // Если else
                    if (isELSEStatement(currentLine))
                    {
                        //Проверяем, что
                        if (stemSt.Count > 0 && stemSt[s] > 0 && NotEnd)
                        {
                            AddSenor(nextIndex - 2, unit, "ENDELSE");
                            stemSt.RemoveAt(s--);
                        }

                        stemSt.Add(0); 
                        stemSt[++s]++;

                        if (currentLine.ToLower().IndexOf(" else ") != -1)
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer, " else ");
                        }
                        else
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                        }
                    }
                    else
                    {
                        //AddSenor(nextIndex, unit, "INTERNAL");
                        stemSt.Add(0);
                        stemSt[++s]++;

                        // Пока не дойдем до then
                        while (!isThenStatement(currentLine))
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                        }
                        if (currentLine.ToLower().IndexOf(" then ") != -1)
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer, " then ");
                        }
                        else if (currentLine.ToLower().IndexOf(")then ") != -1)
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer, ")then ");
                        }
                        else
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                        }
                    }
                    // Пока двойная косая черта (пока комментарии)
                    while (isDoubleSlash(currentLine))
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    // Если begin
                    if (isBeginStatement(currentLine))
                    {
                        stemSt[s]--;
                        NotEnd = false;
                    }
                    else
                    {
                        AddSenor(nextIndex, unit, "BEGIN");
                        AddSenor(nextIndex, unit, "INTERNAL");
                        NotEnd = true;
                    }
                    stDeleted = false;
                }
                else if (isRepeatStatement(currentLine))
                {
                    // Если repeat
                    while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                    {
                        AddSenor(nextIndex, unit, "END");
                        AddSenor(nextIndex, unit, "INTERNAL");
                        stemSt.RemoveAt(s--);
                    }

                    //AddSenor(nextIndex, unit, "INTERNAL");
                    stemSt.Add(0); 
                    s++;

                    if (currentLine.ToLower().IndexOf(" repeat ") != -1)
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer, " repeat ");
                    }
                    else
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    // Пока комментарии
                    while (isDoubleSlash(currentLine))
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    AddSenor(nextIndex, unit, "INTERNAL");
                    stDeleted = false;
                }
                else if (isOnDoStatement(currentLine))
                {
                    // Если on или do
                    stemSt.Add(0);
                    stemSt[++s]++;

                    // пока не дойдем до do
                    while (!isDoStatement(currentLine))
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    if (currentLine.ToLower().IndexOf(" do ") != -1)
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer, " do ");
                    }
                    else
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    // Пока комментарии
                    while (isDoubleSlash(currentLine))
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    // Если begin
                    if (isBeginStatement(currentLine))
                    {
                        stemSt[s]--;
                    }
                    else
                    {
                        AddSenor(nextIndex, unit, "BEGIN");
                        AddSenor(nextIndex, unit, "INTERNAL");
                    }
                    stDeleted = false;
                }
                else if (isUntilStatement(currentLine))
                {
                    // Если until
                    while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                    {
                        AddSenor(nextIndex, unit, "END");
                        AddSenor(nextIndex, unit, "INTERNAL");

                        stemSt.RemoveAt(s--);
                    }

                    stemSt.RemoveAt(s--);

                    while (currentLine.IndexOf(";\r\n") == -1)
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    //Смещаемся
                    NextLine(ref nextIndex, ref buffer);
                    AddSenor(nextIndex, unit, "INTERNAL");
                    stDeleted = false;
                }
                // Если case или except
                else if (isCaseStatement(currentLine) || isExceptStatement(currentLine))
                {                    
                    while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                    {
                        AddSenor(nextIndex, unit, "END");
                        AddSenor(nextIndex, unit, "INTERNAL");

                        stemSt.RemoveAt(s--); 
                    }

                    // Если case
                    if (isCaseStatement(currentLine))
                    {
                        //AddSenor(nextIndex, unit, "INTERNAL");
                        // Работаем как с begin
                        beginCount[i]++;
                    }

                    caseSt.Add(0); 
                    caseSt[++c] = beginCount[i];

                    // Если case
                    if (isCaseStatement(currentLine))
                    {
                        // Ищем of
                        while (currentLine.ToLower().IndexOf(" of\r\n") == -1)
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                        }
                        //Смещаемся
                        NextLine(ref nextIndex, ref buffer);
                    }
                    else
                    {
                        // Если except, 
                        if (currentLine.ToLower().IndexOf("except\r\n") != -1)
                        {
                            // то переходим на другум строку
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                        }
                        else
                        {
                            // или перемещаемся на 8 символов
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer, " except ");
                        }
                        // Пропускаем комментарии
                        while (isDoubleSlash(currentLine))
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                        }
                        // Если on или do
                        if (!isOnDoStatement(currentLine))
                        {
                            // Нужно вставить датчик
                            AddSenor(nextIndex, unit, "INTERNAL");
                        }
                    }
                    stDeleted = false;
                }
                // Если метка
                else if (isLabelStatement(currentLine))
                {
                    if (beginCount[0] > 0)
                    {
                        if (caseSt.Count > 0 && caseSt[c] == beginCount[i])
                        {
                            while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                            {
                                AddSenor(nextIndex, unit, "END");

                                stemSt.RemoveAt(s--); 
                            }

                            label = true;

                            stemSt.Add(0);
                            stemSt[++s]++;

                            if (currentLine.ToLower().IndexOf(":\r\n") != -1)
                            {
                                //Смещаемся и сохраняем следующую строку
                                NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);

                                if (isBeginStatement(currentLine))
                                {
                                    stemSt[s]--;
                                    label = false;
                                }
                                else
                                {
                                    AddSenor(nextIndex, unit, "BEGIN");
                                    AddSenor(nextIndex, unit, "INTERNAL");
                                }
                            }
                            else
                            {
                                //Смещаемся
                                NextLine(ref nextIndex, ref buffer, ":");

                                AddSenor(nextIndex, unit, "BEGIN");
                                AddSenor(nextIndex, unit, "INTERNAL");
                            }
                        }
                        else
                        {
                            while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                            {
                                AddSenor(nextIndex, unit, "END");
                                AddSenor(nextIndex, unit, "INTERNAL");

                                stemSt.RemoveAt(s--);
                            }
                            //Смещаемся
                            NextLine(ref nextIndex, ref buffer);

                            AddSenor(nextIndex, unit, "INTERNAL");
                        }
                    }
                    else
                    {
                        //Смещаемся
                        NextLine(ref nextIndex, ref buffer);
                    }
                    stDeleted = false;
                }
                // Если циклы for, while или with
                else if (isForStatement(currentLine) || isWhileStatement(currentLine) || isWithStatement(currentLine))
                {
                    while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                    {
                        AddSenor(nextIndex, unit, "END");
                        AddSenor(nextIndex, unit, "INTERNAL");

                        stemSt.RemoveAt(s--);
                    }

                    stemSt.Add(0);
                    stemSt[++s]++;

                    while (!isDoStatement(currentLine))
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    if (currentLine.ToLower().IndexOf(" do ") != -1)
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer, " do ");
                    }
                    else
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    while (isDoubleSlash(currentLine))
                    {
                        //Смещаемся и сохраняем следующую строку
                        NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                    }
                    if (isBeginStatement(currentLine))
                    {
                        stemSt[s]--;
                    }
                    else
                    {
                        AddSenor(nextIndex, unit, "BEGIN");
                        AddSenor(nextIndex, unit, "INTERNAL");
                    }
                    stDeleted = false;
                }
                // Если begin или try
                else if (isBeginStatement(currentLine) || isTryStatement(currentLine))
                {
                    if (isTryStatement(currentLine))
                        while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                        {
                            AddSenor(nextIndex, unit, "END");
                            AddSenor(nextIndex, unit, "INTERNAL");

                            stemSt.RemoveAt(s--);
                        }

                    //Смещаемся
                    NextLine(ref nextIndex, ref buffer);

                    if (nextIndex > indexIFDEF || indexIFDEF == -1)
                        beginCount[i]++;

                    if (beginCount[0] > 1)
                        AddSenor(nextIndex, unit, "INTERNAL");
                    else 
                        AddSenor(nextIndex, unit, "ENTER");
                }
                // Если finally
                else if (isFinallyStatement(currentLine))
                {
                    while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                    {
                        AddSenor(nextIndex, unit, "END");
                        AddSenor(nextIndex, unit, "INTERNAL");

                        stemSt.RemoveAt(s--);
                    }

                    //Смещаемся
                    NextLine(ref nextIndex, ref buffer);

                    AddSenor(nextIndex, unit, "INTERNAL");

                    stDeleted = false;
                }
                // Если exit
                else if (isExitStatement(currentLine))
                {
                    AddSenor(nextIndex, unit, "EXIT");

                    //Смещаемся
                    NextLine(ref nextIndex, ref buffer);
                }
                // Если end
                else if (isEndStatement(currentLine))
                {
                    while (stemSt.Count > 0 && stemSt[s] > 0 && stDeleted)
                    {
                        AddSenor(nextIndex, unit, "END");
                        AddSenor(nextIndex, unit, "INTERNAL");

                        stemSt.RemoveAt(s--);
                    }

                    if (stemSt.Count > 0)
                    {
                        stemSt.RemoveAt(s--);

                        stDeleted = true;
                    }

                    if (caseSt.Count > 0 && caseSt[c] == beginCount[i])
                    {
                        caseSt.RemoveAt(c--);
                    }

                    if (nextIndex > indexELSEDEF || indexELSEDEF == -1)
                        beginCount[i]--;

                    if (beginCount[0] != 0)
                    {
                        if (currentLine.ToLower().IndexOf(" end ") != -1)
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer, " end ");

                            if (currentLine.TrimStart().IndexOf(";") == 0)
                            {
                                //Смещаемся и сохраняем следующую строку
                                NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer, ";");
                            }
                        }
                        else
                        {
                            //Смещаемся и сохраняем следующую строку
                            NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                            while (currentLine.IndexOf("\r\n") == 0)
                            {
                                //Смещаемся и сохраняем следующую строку
                                NextLineWithCurrentLine(ref currentLine, ref nextIndex, ref buffer);
                            }
                        }

                        if (!isELSEStatement(currentLine) && !isUntilStatement(currentLine))
                        {
                            AddSenor(nextIndex, unit, "INTERNAL");

                            NotEnd = true;
                        }
                        else 
                            NotEnd = false;
                    }
                    else
                    {
                        //Конец функции
                        AddSenor(nextIndex, unit, "EXIT");

                        //Смещаемся
                        NextLine(ref nextIndex, ref buffer);
                        break;
                    }
                }
                // Если ничего не встретили
                else
                {
                    if (stemSt.Count > 0 && stemSt[s] > 0 && (currentLine.IndexOf(";\r\n") != -1 || currentLine.IndexOf("; ") != -1))
                    {
                        //Смещаемся
                        NextLine(ref nextIndex, ref buffer);

                        if (label)
                        {
                            AddSenor(nextIndex, unit, "END");

                            label = false;
                        }
                        else
                        {
                            AddSenor(nextIndex, unit, "END");
                            AddSenor(nextIndex, unit, "INTERNAL");
                        }

                        stemSt.RemoveAt(s--);

                        stDeleted = true;
                    }
                    else
                    {
                        //Смещаемся
                        NextLine(ref nextIndex, ref buffer);
                        if (buffer == "")
                            break;
                    }
                }
            }
            return nextIndex;
        }

        #region Проверка ключевых слов

        private bool isUntilStatement(string Line)
        {
            if (Line.ToLower().IndexOf(" until ") != -1)
                return true;
            return false;
        }
        private bool isRepeatStatement(string Line)
        {
            if (Line.ToLower().IndexOf(" repeat ") != -1 || Line.ToLower().IndexOf(" repeat\r\n") != -1)
                return true;
            return false;
        }
        private bool isWithStatement(string Line)
        {
            if (Line.ToLower().IndexOf(" with ") != -1)
                return true;
            return false;
        }
        private bool isExitStatement(string Line)
        {
            if (Line.ToLower().IndexOf("exit;\r\n") != -1)
                return true;
            return false;
        }
        private bool isOnDoStatement(string Line)
        {
            if (Line.TrimStart().ToLower().IndexOf("on ") == 0)
                return true;
            return false;
        }
        private bool isExceptStatement(string Line)
        {
            if (Line.ToLower().IndexOf(" except\r\n") != -1 || Line.ToLower().IndexOf(" except ") != -1)
                return true;
            return false;
        }
        private bool isDoubleSlash(string Line)
        {
            if (Line.TrimStart().IndexOf("//") == 0)
                return true;
            return false;
        }
        private bool isLabelStatement(string Line)
        {
            if (Line.ToLower().IndexOf(":\r\n") != -1 || Line.ToLower().IndexOf(": ") != -1)
                return true;
            return false;
        }
        private bool isCaseStatement(string Line)
        {
            if (Line.ToLower().IndexOf(" case ") != -1)
                return true;
            return false;
        }
        private bool isOutCurlyBracket(string Line)
        {
            if (Line.ToLower().IndexOf("}") != -1)
            {
                if (Line.IndexOf("$IFDEF") != -1 || Line.IndexOf("$ELSE") != -1 || Line.IndexOf("$ENDIF") != -1)
                    return false;
                return true;
            }
            return false;
        }
        private bool isInCurlyBracket(string Line)
        {
            if (Line.TrimStart().ToLower().IndexOf("{") == 0)
            {
                if (Line.IndexOf("$IFDEF") != -1 || Line.IndexOf("$ELSE") != -1 || Line.IndexOf("$ENDIF") != -1)
                    return false;
                return true;
            }
            return false;
        }
        private bool isDoStatement(string Line)
        {
            if (Line.ToLower().IndexOf(" do\r\n") != -1 || Line.ToLower().IndexOf(" do ") != -1)
                return true;
            return false;
        }
        private bool isWhileStatement(string Line)
        {
            if (Line.ToLower().IndexOf(" while ") != -1)
                return true;
            return false;
        }
        private bool isForStatement(string Line)
        {
            if (Line.ToLower().IndexOf(" for ") != -1)
                return true;
            return false;
        }
        private bool isELSEStatement(string Line)
        {
            if (Line.TrimStart().ToLower().IndexOf("else\r\n") == 0 || Line.TrimStart().ToLower().IndexOf("else ") == 0)
                return true;
            return false;
        }
        private bool isThenStatement(string Line)
        {
            if (Line.TrimStart().ToLower().IndexOf("then\r\n") != -1 || Line.TrimStart().ToLower().IndexOf(")then ") != -1 || Line.TrimStart().ToLower().IndexOf("then ") != -1)
                return true;
            return false;
        }
        private bool isIFStatement(string Line)
        {
            if (Line.TrimStart().ToLower().IndexOf("if ") == 0)
                return true;
            return false;
        }
        private bool isFinallyStatement(string Line)
        {
            if (Line.ToLower().IndexOf("finally\r\n") != -1)
                return true;
            return false;
        }
        private bool isTryStatement(string Line)
        {
            if (Line.ToLower().IndexOf("try\r\n") != -1)
                return true;
            return false;
        }
        private bool isENDIFDEF(string Line)
        {
            if (Line.IndexOf("$ENDIF") != -1)
                return true;
            return false;
        }
        private bool isELSEDEF(string Line)
        {
            if (Line.IndexOf("$ELSE") != -1)
                return true;
            return false;
        }
        private bool isIFDEF(string Line)
        {
            if (Line.IndexOf("$IFDEF") != -1)
                return true;
            return false;
        }
        private bool isEndStatement(string Line)
        {
            if (Line.ToLower().IndexOf("end\r\n") != -1 || Line.ToLower().IndexOf("end;\r\n") != -1 ||
                Line.ToLower().IndexOf(" end ") != -1 || Line.TrimStart().ToLower().IndexOf("end; ") != -1)
                return true;
            return false;
        }
        private bool isBeginStatement(string Line)
        {
            if (Line.ToLower().TrimStart().IndexOf("begin\r\n") == 0)
                return true;
            return false;
        }

        #endregion

        /// <summary>
        /// Добавление сенсора или структуры BEGIN-END
        /// </summary>
        /// <param name="nextIndex">Смещение в файле</param>
        /// <param name="unit">Данные о функции</param>
        /// <param name="type">Тип датчика</param>
        private void AddSenor(int nextIndex, Unit unit, string type)
        {
            Sensor sensor = new Sensor();
            sensor.Number = currentSensorNamuber;
            sensor.Type = type;
            sensor.UnitName = unit.UnitName;
            sensor.FilePath = unit.FilePath;
            sensor.Offset = nextIndex;
            sensor.Line = unit.Line;
            if (type != "BEGIN" && type != "END" && type != "ENDELSE")
            {
                currentSensorNamuber++;
                sensor.kind = (type == "ENTER") ? Kind.START : (type == "EXIT") ? Kind.FINAL : Kind.INTERNAL;
            }
            allSensors.Add(sensor);
        }

        /// <summary>
        /// Сравнение смещений начал 1-ой и 2-ой функции. Ищем вложенные функции.
        /// </summary>
        /// <param name="funcIndex">Смещение 1-ой функции</param>
        /// <param name="fileOffset">Смещение 2-ой функции</param>
        /// <param name="buffer">Текст файла</param>
        /// <returns>Возвращает индекс первого найденного BEGIN'а после определения 1-ой функции</returns>
        private int BeginStatement(int funcIndex, int fileOffset, string buffer)
        {
            // Становимся на старт i-ой функции
            buffer = buffer.Substring(funcIndex);
            // Если смещение begin'а в i+1-ой функции
            if (fileOffset - funcIndex < buffer.IndexOf("begin\r\n"))
            {
                // то возвращаем индекс данного begin'а,
                return funcIndex + buffer.IndexOf("begin\r\n");
            }
            // иначе все хорошо
            else return -2;
        }

        /// <summary>
        /// Поиск смещения функции в файле
        /// </summary>
        /// <param name="unit">Сущность</param>
        /// <param name="buffer">Буфер файла</param>
        /// <returns>Возвращает смещение функции</returns>
        private int FindFunctionDefinition(Unit unit, string buffer)
        {
            int ind = 0;
            for (ulong i = 0; i < unit.Line; i++)
            {
                ind += buffer.IndexOf('\n') + 1;
                buffer = buffer.Substring(buffer.IndexOf('\n') + 1);
            }
            return ind;
        }
                
        /// <summary>
        /// Вставка датчиков и сохранение лабораторных версий файлов
        /// </summary>
        internal void InsertAndSave()
        {
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_WITH_SENSORS_SAVING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)pascalFiles.Count());
            ulong counter = 0;
            foreach (IFile file in pascalFiles)
            {
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_WITH_SENSORS_SAVING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);

                StringBuilder builder = new StringBuilder(new AbstractReader(file.FullFileName_Original).getText());
                
                foreach (Sensor sensor in allSensors.Where(x => x.FilePath.ToLower() == file.FullFileName_Original.ToLower()).Reverse())
                    if (sensor.FilePath.ToLower() == file.FullFileName_Original.ToLower())
                    {
                        switch (sensor.Type)
                        {
                            case "BEGIN":
                                //Доставляем недостающий BEGIN
                                builder.Insert(sensor.Offset, "begin\r\n");
                                break;
                            case "END":
                                //Доставляем недостающий ЕND
                                builder.Insert(sensor.Offset, "end;\r\n");
                                break;
                            case "ENDELSE":
                                //Доставляем недостающую ';' к составному блоку BEGIN-END внутри IF перед ELSE
                                builder.Insert(sensor.Offset, ";\r\nend");
                                break;
                            default:
                                //Встраиваем вызов функции датчика
                                builder.Insert(sensor.Offset, string.Format("sensor({0});\r\n", sensor.Number));

                                //Ищем функцию, которая определена в той же строке, что и датчик
                                IFunction sensorFunction = storage.functions.GetFunction(sensor.UnitName.Split('.'))
                                    .FirstOrDefault(function =>
                                        {
                                            Location functionLocation = function.Definition();
                                            return functionLocation != null && functionLocation.GetLine() == sensor.Line;
                                        });

                                //В данном плагине пока так: места определения всех датчиков в данной функции = месту определения данной функции
                                //Если нашли функцию, то добавляем датчик в Хранилище
                                if (sensorFunction != null)
                                    storage.sensors.AddSensor(sensor.Number, sensorFunction.Id, sensor.kind);
                                break;
                        }
                    }

                //Вставка кода функции датчика
                int index = builder.ToString().LastIndexOf("end.");
                // Проверка на разумность (файл не пуст)
                if (index > 5)
                    builder.Insert(index, "procedure sensor(sensorID : integer);\r\nvar\r\n\trnt_sensor : System.TextFile;\r\nbegin\r\n\tSystem.AssignFile(rnt_sensor,'c:\\Delphifile.txt');\r\n\tif not System.FileExists('c:\\Delphifile.txt') then\r\n\tbegin\r\n\t\tSystem.Rewrite(rnt_sensor);\r\n\t\tSystem.CloseFile(rnt_sensor);\r\n\tend;\r\n\tSystem.Append(rnt_sensor);\r\n\tSystem.Writeln(rnt_sensor, sensorID);\r\n\tSystem.Flush(rnt_sensor);\r\n\tSystem.CloseFile(rnt_sensor);\r\nend;\r\n");

                //Путь к папке с файлом с датчиками
                string outputDirectoryPath = Path.Combine(labsDirectoryPath, (Path.GetDirectoryName(file.FullFileName_Original)+"\\").Substring(inputDirectoryPath.Length));

                //Создаем папку
                DirectoryController.Create(outputDirectoryPath);

                //Создаем файл с датчиками
                using (StreamWriter writer = new StreamWriter(Path.Combine(outputDirectoryPath, Path.GetFileName(file.FullFileName_Original)), false, Encoding.Default))
                {
                    writer.Write(builder.ToString());
                }

                builder.Clear();
            }
        }
    }
}
