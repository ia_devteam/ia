﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FileOperations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using Store.Table;
using TestUtils;
using IA.Plugins.Parsers.JavaScriptParser_NodeJS;

namespace TestNodeJSParser
{
    /// <summary>
    /// Класс для тестов парсера JavaScript (NodeJS)
    /// </summary>
    [TestClass]
    public class TestNodeJSParser
    {

        #region ServiceRegion
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Список сообщений
            /// </summary>
            public List<string> Messages
            {
                get 
                {
                    return messages;
                }
            }
        }

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Путь до каталога с эталонными исходными текстами
        /// </summary>
        private string labSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонным дампами Хранилища
        /// </summary>
        private string dumpSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонными отчетами
        /// </summary>
        private string reportSampleDirectoryPath;

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными лабораторным текстам
        /// </summary>
        private const string LAB_SAMPLES_SUBDIRECTORY = @"NodeJS\Labs";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными дампами Хранилища
        /// </summary>
        private const string DUMP_SAMPLES_SUBDIRECTORY = @"NodeJS\Dump";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными отчетами
        /// </summary>
        private const string REPORT_SAMPLES_SUBDIRECTORY = @"NodeJS\Reports";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        private const ulong pluginID = Store.Const.PluginIdentifiers.JAVASCRIPT_NODEJS_PARSER;

        /// <summary>
        /// Перехватчик сообщений монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName); 
            labSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, LAB_SAMPLES_SUBDIRECTORY);
            dumpSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, DUMP_SAMPLES_SUBDIRECTORY);
            reportSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, REPORT_SAMPLES_SUBDIRECTORY);
        }

        /// <summary>
        /// Подготовка Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="testMaterialsPath">Путь до каталога с материалами.</param>
        /// <param name="sourcePostfixPart">Подпуть до каталога с исходными файлами.</param>
        /// <param name="isLevel2">Уровень НДВ. true - 2-ой уровень</param>
        private void CustomizeStorage(Storage storage, string testMaterialsPath, string sourcePostfixPart, bool isLevel2, ulong firstSensorNumber)
        {
            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
            TestUtilsClass.Run_IdentifyFileTypes(storage);
            TestUtilsClass.Run_CheckSum(storage);

            SettingUpPlugin(storage, firstSensorNumber, isLevel2);
        }

        /// <summary>
        /// Настройка плагина
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="firstSensorNumber">Номер первого датчика. Не может быть отрицательным.</param>
        /// <param name="isLevel2">true - 2-ой уровень.</param>
        private void SettingUpPlugin(Storage storage, UInt64 firstSensorNumber, bool isLevel2)
        {
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(isLevel2, !isLevel2, false);
            buffer.Add(firstSensorNumber);
            buffer.Add("function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open(\"GET\", \"http://localhost:8080/\"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} ");
            buffer.Add("SensorRnt(#);");
            buffer.Add("function SensorRnt(a) { var RNTfileWITHsensors_FileSystemObject, RNTfileWITHsensors_File; RNTfileWITHsensors_FileSystemObject = new ActiveXObject(\"Scripting.FileSystemObject\"); RNTfileWITHsensors_File = RNTfileWITHsensors_FileSystemObject.OpenTextFile(\"c:\\\\RNTSensorlog_JavaScript.log\", 8, true); RNTfileWITHsensors_File.WriteLine(a);RNTfileWITHsensors_File.Close();}");
            buffer.Add("SensorRnt(#);");
            buffer.Add(true);

            storage.pluginSettings.SaveSettings(pluginID, buffer);
        }

        /// <summary>
        /// Дамп Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <returns>Путь до каталога, куда был произведен дамп Хранилища</returns>
        private string Dump(Storage storage)
        {
            string dumpDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.STORAGE), "Dump");

            if (!Directory.Exists(dumpDirectoryPath))
                Directory.CreateDirectory(dumpDirectoryPath);

            storage.ClassesDump(dumpDirectoryPath);
            storage.FilesDump(dumpDirectoryPath);
            storage.FunctionsDump(dumpDirectoryPath);
            storage.VariablesDump(dumpDirectoryPath);

            return dumpDirectoryPath;
        }

        /// <summary>
        /// Проверка дампа Хранилища
        /// </summary>
        /// <param name="dumpDirectoryPath">Путь до каталога дампа в Хранилище. Не может быть пустым.</param>
        /// <param name="dumpSamplesDirectoryPath">Путь до каталога с эталонным дампом. Не может быть пустым.</param>
        private void CheckDump(string dumpDirectoryPath, string dumpSamplesDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(dumpDirectoryPath, dumpSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Дамп Хранилища не совпадает с эталонными.");
        }

        /// <summary>
        /// Проверка лабораторных исходных файлов
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="labSamplesDirectoryPath">Путь до каталога с эталонными лабораторными исходными текстами. Не может быть пустым.</param>
        private void CheckLabs(Storage storage, string labSamplesDirectoryPath)
        {
            string labsDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "javascript");

            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(labsDirectoryPath, labSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Исходные тексты со вставленными датчиками не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка отчетов
        /// </summary>
        /// <param name="xmlSampleDirectoryPath">Путь до каталога с эталонными отчетами. Не может быть пустым.</param>
        /// <param name="storageReportsDirectoryPath">Путь до каталога с отчетами в Хранилище. Не может быть пустым.</param>
        private void CheckXML(string xmlSampleDirectoryPath, string storageReportsDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_XML_Compare(storageReportsDirectoryPath, xmlSampleDirectoryPath);

            Assert.IsTrue(isEqual, "Отчеты не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка ожидаемого сообщения в логе монитора
        /// </summary>
        /// <param name="message">Текст сообщения. Не может быть пустым.</param>
        private void CheckMessage(string message)
        {
            bool isContainMessage = listener.ContainsPart(message);

            Assert.IsTrue(isContainMessage, "Сообщение не совпадает с ожидаемым.");
        }

        #endregion

        #region IACommon
        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже присутствует в лабораторных тексах.
        /// </summary>
        [TestMethod]
        public void NodeJSParser_WrongFirstSensor_InLabsTest()
        {
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();
            var sourcePostfixPart = @"Sources\NodeJS\firstsensor";
            testUtils.RunTest(
                sourcePostfixPart,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                       //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);

                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <1> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <2>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика не фигурировал в исходных файлах, 
        /// но оказался меньше, чем максимальный идентификатор в Хранилище.
        /// </summary>
        [TestMethod]
        public void NodeJS_WrongFirstSensor_NotInLabsTest()
        {
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();
            var sourcePostfixPart = @"Sources\NodeJS\firstsensor";
            testUtils.RunTest(
                sourcePostfixPart,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(3, func1.Id, Kind.START);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);

                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 2);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже фигурировал в исходных файлах, но был удален.
        /// </summary>
        [TestMethod]
        public void NodeJS_WrongFirstSensor_NotInLabsDeletedTest()
        {
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();
            var sourcePostfixPart = @"Sources\NodeJS\firstsensor";
            testUtils.RunTest(
                sourcePostfixPart,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);
                    storage.sensors.AddSensor(2, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(3, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);

                    storage.sensors.RemoveSensor(2);

                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 2);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки плагина на пустой папке.
        /// </summary>
        [TestMethod]
        public void NodeJS_EmptyFolderTest()
        {
            string sourcePostfixPart = @"NodeJS\EmptyFolder";

            testUtils.RunTest(
               sourcePostfixPart,                                           //sourcePostFixPart
               new JavaScriptParser_NodeJS(),                                      //plugin
               false,                                                       //isUseCachedStorage
               (storage, testMaterialsPath) => { },                         //prepareStorage
               (storage, testMaterialsPath) =>
               {
                   if (!Directory.Exists(Path.Combine(testMaterialsPath, sourcePostfixPart)))
                       Directory.CreateDirectory(Path.Combine(testMaterialsPath, sourcePostfixPart));
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                           //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckMessage("Исходных файлов js не обнаружено.");
                   ////CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "EmptyFolder"));

                   return true;
               },                                                           //checkStorage
               (reportsFullPath, testMaterialsPath) => { return true; },    //checkreports
               false,                                                       //isUpdateReport
               (plugin, testMaterialsPath) => { },                          //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },            //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }         //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка выдачи сообщений об ошибках разбора файлов.
        /// </summary>
        [TestMethod]
        public void NodeJS_BadFilesTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\ForeachStatement";

            testUtils.RunTest(
               sourcePostfixPart,                                           //sourcePostFixPart
               new JavaScriptParser_NodeJS(),                                      //plugin
               false,                                                       //isUseCachedStorage
               (storage, testMaterialsPath) => { },                         //prepareStorage
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                           //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   CheckMessage("{\"index\":24,\"lineNumber\":4,\"description\":\"Unexpected identifier\"}");

                   return true;
               },                                                           //checkStorage
               (reportsFullPath, testMaterialsPath) => { return true; },    //checkreports
               false,                                                       //isUpdateReport
               (plugin, testMaterialsPath) => { },                          //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },            //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }         //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка отработки плагина по 2 уровню на предмет невставленных датчиков в простые функции.
        /// </summary>
        [TestMethod]
        public void NodeJS_UnexpectedInsertion2lvlTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\UnexpectedInsertion";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                               //sourcePostFixPart
               testPlugin,                                                      //plugin
               false,                                                           //isUseCachedStorage
               (storage, testMaterialsPath) => { },                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, "UnexpectedInsertion"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "UnexpectedInsertion"));

                   return true;
               },                                                               //checkStorage               
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "UnexpectedInsertion"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка корректного чтения файла js с кириллицей и записи корректного лабораторного варианта
        /// </summary>
        [TestMethod]
        public void NodeJS_RussianCp1251Test()
        {
            string sourcePostfixPart = @"Sources\NodeJS\RussianCp1251";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                               //sourcePostFixPart
               testPlugin,                                                      //plugin
               false,                                                           //isUseCachedStorage
               (storage, testMaterialsPath) => { },                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);
                   string labPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "javascript");
                   labPath = labPath + @"\aggregateAreaModels.js";
                   var reader = new AbstractReader(labPath);
                   bool iSequal = reader.getLineN(5).Contains("'Неверное количествово аргументов [loadAggregateTerrainModelsView(...)]'");    
                   Assert.IsTrue(iSequal, "NodeJS: тест RussianCp1251 не пройден.");
                   return true;
               },                                                               //checkStorage               
               (reportsFullPath, testMaterialsPath) =>
               {
                   //testPlugin.GenerateStatements(reportsFullPath);
                   //CheckXML(Path.Combine(reportSampleDirectoryPath, "RussianCp1251"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка корректности отработки плагина на папке, в которой нет js и html файлов.
        /// </summary>
        [Ignore]
        [TestMethod]
        public void NodeJS_NoJSTest()
        {
            string sourcePostfixPart = @"NodeJS\NoJS";

            testUtils.RunTest(
               sourcePostfixPart,                                                       //sourcePostFixPart
               new JavaScriptParser_NodeJS(),                                                  //plugin
               false,                                                                   //isUseCachedStorage
               (storage, testMaterialsPath) => { },                                     //prepareStorage
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                       //customizeStorage
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckMessage("Исходных файлов js не обнаружено.");
                   ////CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "NoJS"));

                   return true;
               },                                                                       //checkStorage
               (reportsFullPath, testMaterialsPath) => { return true; },                //checkreports
               false,                                                                   //isUpdateReport
               (plugin, testMaterialsPath) => { },                                      //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                        //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                     //checkReportAfterRerun
               );
        }
        #endregion


        /// <summary>
        /// Заглушка для тестирования парсера. DebugDummy - при загрузке в репозиторий должна быть пустой
        /// </summary>
        [Ignore]
        [TestMethod]
        public void NodeJS_DebugDummyTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\DebugDummy";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   //string dumpDirectoryPath = Dump(storage);

                   //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"simple\sensored-lvl3"));
                   ////CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"simple\sensored-lvl3"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   //testPlugin.GenerateStatements(reportsFullPath);

                   //CheckXML(Path.Combine(reportSampleDirectoryPath, @"simple\sensored-lvl3"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }


        /// <summary>
        /// Проверка корректности вставки датчиков по 2 уровню контроля в HTML-файл с javascript-вставкой.
        /// </summary>
        [Ignore]
        [TestMethod]
        public void NodeJS_HTML2lvlTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\HTML";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"HTML\sensored2-lvl"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "HTML2"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "HTML2"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков по 3 уровню контроля в HTML-файл с javascript-вставкой.
        /// </summary>
        [Ignore]
        [TestMethod]
        public void NodeJS_HTML3lvlTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\HTML";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"HTML\sensored3-lvl"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "HTML3"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "HTML3"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// </summary>
        [TestMethod]
        public void NodeJS_2levelTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\simple";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"simple\sensored-lvl2"));
                   ////CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"simple\sensored-lvl2"));

                   return true;                   
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   //CheckXML(Path.Combine(reportSampleDirectoryPath, @"simple\sensored-lvl2"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест проверяет работу плагина по 3-му уровню НДВ с расстановкой датчиков с единицы
        /// </summary>
        [TestMethod]
        public void NodeJS_3levelTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\simple";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"simple\sensored-lvl3"));
                   ////CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"simple\sensored-lvl3"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   //CheckXML(Path.Combine(reportSampleDirectoryPath, @"simple\sensored-lvl3"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест проверяет работу плагина по 2-му уровню НДВ с расстановкой датчиков с единицы
        /// </summary>
        [TestMethod]
        public void NodeJS_2levelBadEncodingTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\simplebadencoding";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"simple2\sensored-lvl2"));
                   ////CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"simple2\sensored-lvl2"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   //CheckXML(Path.Combine(reportSampleDirectoryPath, @"simple2\sensored-lvl2"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Тест проверяет работу плагина по 3-му уровню НДВ с расстановкой датчиков с единицы
        /// </summary>
        [TestMethod]
        public void NodeJS_3levelBadEncodingTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\simplebadencoding";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"simple2\sensored-lvl3"));
                   ////CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"simple2\sensored-lvl3"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   //CheckXML(Path.Combine(reportSampleDirectoryPath, @"simple2\sensored-lvl3"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков по 2 уровню контроля в PHP-файл с javascript-вставкой.
        /// </summary>
        [Ignore]
        [TestMethod]
        public void NodeJS_PHP2lvlTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\PHP";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"PHP\sensored2-lvl"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"PHP\sensored2-lvl"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, @"PHP\sensored2-lvl"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка корректности вставки датчиков по 3 уровню контроля в PHP-файл с javascript-вставкой.
        /// </summary>
        [Ignore]
        [TestMethod]
        public void NodeJS_PHP3lvlTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\PHP";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 4096);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"PHP\sensored3-lvl"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"PHP\sensored3-lvl"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, @"PHP\sensored3-lvl"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }


        /// <summary>
        /// Проверка корректности работы с дубликатами файлов.
        /// </summary>
        [TestMethod]
        public void NodeJS_DublicatesTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\Dublicate";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"Dublicates"));
                   ////CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "Dublicates"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   //CheckXML(Path.Combine(reportSampleDirectoryPath, "Dublicates"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        #region Test Statements
        /// <summary>
        /// Проверка определения конструкции try-catch-finally по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_TryCatchFinallyStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\TryCatchFinallyStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"TryCatchFinallyStatement"));
                   ////CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "TryCatchFinallyStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   //CheckXML(Path.Combine(reportSampleDirectoryPath, "TryCatchFinallyStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка определения конструкции switch по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_SwitchStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\SwitchStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {                   
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"SwitchStatement"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "SwitchStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "SwitchStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        //Закомментировано. Спецификация ECMAScript 5.1 не используется.
        // <summary>
        // Проверка определения конструкции for...in по 2-уровню. Строится statement.xml в отчетах.
        // </summary>
        //[TestMethod]
        //public void JSParser_ForInStatement()
        //{
        //    string sourcePostfixPart = @"Sources\JS\ForInStatement";

        //    testUtils.RunTest(
        //       sourcePostfixPart,                                                               //sourcePostFixPart
        //       testPlugin,                                                                      //plugin
        //       false,                                                                           //isUseCachedStorage               
        //       (storage, testMaterialsPath) => { },                                             //prepareStorage               
        //       (storage, testMaterialsPath) =>
        //       {
        //           CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true);
        //       },                                                                               //customizeStorage               
        //       (storage, testMaterialsPath) =>
        //       {
        //           string dumpDirectoryPath = Dump(storage);

        //           CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ForInStatement"));
        //           //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "ForInStatement"));

        //           return true;
        //       },                                                                               //checkStorage
        //       (reportsFullPath, testMaterialsPath) =>
        //       {
        //          CheckXML(Path.Combine(reportSampleDirectoryPath, "ForInStatement"), reportsFullPath);

        //           return true;
        //       },                                                                               //checkreports
        //       false,                                                                           //isUpdateReport
        //       (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
        //       (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
        //       (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
        //       );
        //}

        //Закомментировано. Спецификация ECMAScript 6 не используется.
        // <summary>
        // Проверка определения конструкций for...of и for each...in по 2-уровню. Строится statement.xml в отчетах.
        // </summary>
        //[TestMethod]
        //public void JSParser_ForEachStatement()
        //{
        //    string sourcePostfixPart = @"Sources\JS\ForEachStatement";

        //    testUtils.RunTest(
        //       sourcePostfixPart,                                                               //sourcePostFixPart
        //       testPlugin,                                                                      //plugin
        //       false,                                                                           //isUseCachedStorage               
        //       (storage, testMaterialsPath) => { },                                             //prepareStorage               
        //       (storage, testMaterialsPath) =>
        //       {
        //           CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true);
        //       },                                                                               //customizeStorage               
        //       (storage, testMaterialsPath) =>
        //       {
        //           string dumpDirectoryPath = Dump(storage);

        //           CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ForEachStatement"));
        //           //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "ForEachStatement"));

        //           return true;
        //       },                                                                               //checkStorage
        //       (reportsFullPath, testMaterialsPath) =>
        //       {
        //          CheckXML(Path.Combine(reportSampleDirectoryPath, "ForEachStatement"), reportsFullPath);

        //           return true;
        //       },                                                                               //checkreports
        //       false,                                                                           //isUpdateReport
        //       (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
        //       (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
        //       (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
        //       );
        //}

        /// <summary>
        /// Проверка глобального окружения по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_GlobalEnvironmentTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\GlobalEnvironment";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"GlobalEnvironment"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "GlobalEnvironment"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "GlobalEnvironment"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка определения конструкции label-continue (аналог goto) по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_LabelContinueStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\LabelContinueStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"LabelContinueStatement"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "LabelContinueStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "LabelContinueStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка определения конструкции break по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_BreakStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\BreakStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"BreakStatement"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "BreakStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "BreakStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка определения конструкции return по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_ReturnStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\ReturnStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ReturnStatement"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "ReturnStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "ReturnStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка определения конструкции while-do по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_WhileDoStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\WhileDoStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"WhileDoStatement"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "WhileDoStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "WhileDoStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка определения конструкции do-while по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_DoWhileStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\DoWhileStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"DoWhileStatement"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "DoWhileStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "DoWhileStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка определения конструкции if-then-else по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_IfThenElseStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\IfThenElseStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"IfThenElseStatement"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "IfThenElseStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "IfThenElseStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка определения конструкции if-then по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_IfThenStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\IfThenStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"IfThenStatement"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "IfThenStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "IfThenStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        /// <summary>
        /// Проверка определения конструкции for по 2-уровню. Строится statement.xml в отчетах.
        /// </summary>
        [TestMethod]
        public void NodeJS_ForStatementTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\TestStatements\ForStatement";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ForStatement"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "ForStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "ForStatement"), reportsFullPath);

                   return true; 
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }


        /// <summary>
        /// Проверка для файла, на котором была ошибка
        /// </summary>
        [TestMethod]
        public void NodeJS_Bug1()
        {
            string sourcePostfixPart = @"Sources\NodeJS\bug1";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();
            
            testUtils.RunTest(
                sourcePostfixPart,                                                               //sourcePostFixPart
                testPlugin,                                                                      //plugin
                false,                                                                           //isUseCachedStorage               
                (storage, testMaterialsPath) => { },                                             //prepareStorage               
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
                },                                                                               //customizeStorage               
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("exception {\"index\":349,\"lineNumber\":18,\"description\":\"Unexpected end of input\"}");

                    return true;
                },                                                                               //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                               //checkreports
                false,                                                                           //isUpdateReport
                (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
            );
        }

        #endregion Test Statement

        //Парсеру недостаточно оперативной памяти в 8 гб для обработки такого количества информации. Тест закомментирован.
        ///// <summary>
        ///// Проверка отработки плагина на исходниках, общим весом в 10 мб.
        ///// </summary>
        //[TestMethod]
        //public void JSParser_StressTest()
        //{
        //    string sourcePostfixPart = @"Sources\JS\StressTest\";
        //    JavaScriptParser testPlugin = new JavaScriptParser();

        //    testUtils.RunTest(
        //       sourcePostfixPart,                                                               //sourcePostFixPart
        //       testPlugin,                                                                      //plugin
        //       false,                                                                           //isUseCachedStorage               
        //       (storage, testMaterialsPath) => { },                                             //prepareStorage               
        //       (storage, testMaterialsPath) =>
        //       {
        //           CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true);
        //       },                                                                               //customizeStorage               
        //       (storage, testMaterialsPath) =>
        //       {
        //           string dumpDirectoryPath = Dump(storage);

        //           CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"StressTest"));
        //           //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "StressTest"));

        //           return true;
        //       },                                                                               //checkStorage
        //       (reportsFullPath, testMaterialsPath) =>
        //       {
        //           //testPlugin.GenerateStatements(reportsFullPath);

        //           //CheckXML(Path.Combine(reportSampleDirectoryPath, "StressTest"), reportsFullPath);

        //           return true;
        //       },                                                                               //checkreports
        //       false,                                                                           //isUpdateReport
        //       (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
        //       (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
        //       (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
        //       );
        //}

        //Парсеру недостаточно оперативной памяти в 8 гб для обработки такого количества информации. Тест закомментирован.
        ///// <summary>
        ///// Проверка отработки плагина на исходниках, общим весом в 10 мб.
        ///// </summary>
        [Ignore]
        [TestMethod]
        public void NodeJS_StressTest1()
        {
            string sourcePostfixPart = @"Sources\NodeJS\StressTest\001\";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {

                   string dumpDirectoryPath = Dump(storage);
                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"StressTest1"));
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "StressTest1"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                  CheckXML(Path.Combine(reportSampleDirectoryPath, "StressTest1"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        #region bugs
        /// <summary>
        /// Проверка для файла, на котором была ошибка
        /// </summary>
        [TestMethod]
        public void NodeJS_ASPXCodeTest()
        {
            string sourcePostfixPart = @"Sources\NodeJS\ASPX\";
            JavaScriptParser_NodeJS testPlugin = new JavaScriptParser_NodeJS();

            testUtils.RunTest(
                sourcePostfixPart,                                                               //sourcePostFixPart
                testPlugin,                                                                      //plugin
                false,                                                                           //isUseCachedStorage               
                null,                                             //prepareStorage               
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
                },                                                                               //customizeStorage               
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);
                    //CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"ASPX"));
                    //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "ASPX"));
                    return true;
                },                                                                               //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    testPlugin.GenerateStatements(reportsFullPath);
                   CheckXML(Path.Combine(reportSampleDirectoryPath, "ASPX"), reportsFullPath);
                    return true;
                },                                                                               //checkreports
                false,                                                                           //isUpdateReport
                null,                                              //changeSettingsBeforRerun
                null,                                //checkStorageAfterRerun
                null                             //checkReportAfterRerun
            );
        }
        #endregion

    //
    }
}
