﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

using IA.Plugin;
using Store;
using Store.Table;

using IA.Extensions;
using IA.Plugins.Parsers.CommonUtils;
using FileOperations;

namespace IA.Plugins.Parsers.JavaScriptParser_NodeJS
{
    internal static class PluginSettings
    {
        public const uint ID = Store.Const.PluginIdentifiers.JAVASCRIPT_NODEJS_PARSER;
        public const string Name = "Парсер JavaScript (NodeJS)";
       
        /// <summary>
        /// Задачи плагинов
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка файлов")]
            FILES_PROCESSING,
            [Description("Расстановка датчиков")]
            SENSOR_PLACEMENT
        };

        #region Settings
        /// <summary>
        /// 2-й уровень вставки датчиков
        /// </summary>
        internal static bool IsLevel2;
        /// <summary>
        /// 3-й уровень вставки датчиков
        /// </summary>
        internal static bool IsLevel3;

        /// <summary>
        /// Не вставлять датчики
        /// </summary>
        internal static bool IsNoSensors;
        /// <summary>
        /// Текст функции датчика для 2-го уровня
        /// </summary>
        internal static string SensorFunctionText_Level2;
        /// <summary>
        /// Текст датчика для 2-го уровня
        /// </summary>
        internal static string SensorText_Level2;
        /// <summary>
        /// Текст функции датчика для 3-го уровня
        /// </summary>
        internal static string SensorFunctionText_Level3;
        /// <summary>
        /// Текст датчика для 3-го уровня
        /// </summary>
        internal static string SensorText_Level3;
        /// <summary>
        /// Номер первого датчика
        /// </summary>
		internal static ulong FirstSensorNumber;
		
		/// <summary>
        /// Пропуск ошибок
        /// </summary>
        internal static bool IsSkipErrors;
        #endregion
        
    }

    public class JavaScriptParser_NodeJS : IA.Plugin.Interface
    {

        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Настройки
        /// </summary>
        IA.Plugin.Settings settings;
        
        /// <summary>
        /// Признак был создан парсером XML или же нет
        /// </summary>
        public bool IsXML = false;

        /// <summary>
        /// Число файлов *.js на обработку парсеру
        /// </summary>
        public long NumJS = 0;

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public IA.Plugin.Capabilities Capabilities
        {
            get
            {
#if DEBUG
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS;
#else
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
#endif
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
		}

        /// <summary>
        /// Метод генерации отчетов для тестов
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до каталога с отчетами плагина</param>
        public void GenerateStatements(string reportsDirectoryPath)
        {
            (new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage)).Execute();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            #if DEBUG
            // Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            if (string.IsNullOrEmpty(reportsDirectoryPath))
                throw new Exception("Путь к каталогу с отчетами не определен.");

            //StatementPrinter
            //StatementPrinter sprinter = new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage);
            //sprinter.Execute();
            #endif
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID {get { return PluginSettings.ID; } }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            PluginSettings.IsLevel2 = true;
            PluginSettings.IsLevel3 = false;
            PluginSettings.IsNoSensors = false;
            PluginSettings.SensorFunctionText_Level2 = "function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open(\"GET\", \"http://localhost:8080/\"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} ";
            PluginSettings.SensorText_Level2 = "Sensor(#)";
            PluginSettings.SensorFunctionText_Level3 = "function SensorRnt(a) { var RNTfileWITHsensors_FileSystemObject, RNTfileWITHsensors_File; RNTfileWITHsensors_FileSystemObject = new ActiveXObject(\"Scripting.FileSystemObject\"); RNTfileWITHsensors_File = RNTfileWITHsensors_FileSystemObject.OpenTextFile(\"c:\\\\RNTSensorlog_JavaScript.log\", 8, true); RNTfileWITHsensors_File.WriteLine(a);RNTfileWITHsensors_File.Close();}";
            PluginSettings.SensorText_Level3 = "Sensor(#)";
            PluginSettings.FirstSensorNumber = 1;
            PluginSettings.IsSkipErrors = true;
            this.storage = storage;

            //Выгружаем настройки из данных плагина
            LoadPluginData();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.IsLevel2, ref PluginSettings.IsLevel3, ref PluginSettings.IsNoSensors);
            PluginSettings.FirstSensorNumber = settingsBuffer.GetUInt64();
            PluginSettings.SensorFunctionText_Level2 = settingsBuffer.GetString();
            PluginSettings.SensorText_Level2 = settingsBuffer.GetString();
            PluginSettings.SensorFunctionText_Level3 = settingsBuffer.GetString();
            PluginSettings.SensorText_Level3 = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.IsSkipErrors);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.IsNoSensors = false;
            PluginSettings.SensorFunctionText_Level2 =
                "function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open(\"GET\", \"http://localhost:8080/\"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} ";
            PluginSettings.SensorText_Level2 = "SensorRnt(#);";
            PluginSettings.SensorFunctionText_Level3 = String.Empty;
            PluginSettings.SensorText_Level3 =
                "var RNTfileWITHsensors_FileSystemObject, RNTfileWITHsensors_File; RNTfileWITHsensors_FileSystemObject = new ActiveXObject(\"Scripting.FileSystemObject\"); RNTfileWITHsensors_File = RNTfileWITHsensors_FileSystemObject.OpenTextFile(\"c:\\\\RNTSensorlog_JavaScript.log\", 8, true); RNTfileWITHsensors_File.WriteLine(#);RNTfileWITHsensors_File.Close();";
            PluginSettings.FirstSensorNumber = (storage != null && storage.sensors.EnumerateSensors().Any())
                ? storage.sensors.EnumerateSensors().Max(s => s.ID) + 1
                : 1;
            PluginSettings.IsSkipErrors = false;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "NDV:3":
                    PluginSettings.IsLevel3 = true;
                    PluginSettings.IsLevel2 = false;
                    break;
                case "NDV:2":
                    PluginSettings.IsLevel2 = true;
                    PluginSettings.IsLevel3 = false;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
        
        public UserControl SimpleSettingsWindow
        { get { return null; } }

        private string MapEncodingName(Encoding encoding)
        {
            if (encoding.HeaderName.ToUpper().StartsWith("iso-8859-"))
            {
                return encoding.HeaderName.ToUpper();
            }

            if (encoding.HeaderName.ToUpper().StartsWith("IBM"))
            {
                return encoding.HeaderName.ToUpper().Replace("IBM","CP");
            }

            if (encoding.HeaderName.ToUpper().StartsWith("WINDOWS-"))
            {
                return encoding.HeaderName.ToUpper().Replace("WINDOWS-", "CP");
            }

            if (encoding.HeaderName.ToUpper().StartsWith("UTF-"))
            {
                return encoding.HeaderName.ToUpper();
            }

            if (encoding.HeaderName == "macintosh")
            {
                return "Macintosh";
            }
            
            switch (encoding.HeaderName)
            {
                case "us-ascii":
                    return "ASCII";
                case "koi8-u":
                    return "KOI8-U";
                case "koi8-r":
                    return "KOI8-R";
            }

            return "UTF-8";
        }

        /// <summary> 
        /// 
        /// </summary>
        /// <param name="IsTestEnv">При тестовой среде удаляются номера дтчиков и # у ReplaceReturn</param>
        public void Run()
        {
            if (storage != null && storage.sensors.Count() > 0 && storage.sensors.EnumerateSensors().Max(s => s.ID) >=
                PluginSettings.FirstSensorNumber)
            {
                ulong tmp_FirstSensorNumber = PluginSettings.FirstSensorNumber;
                PluginSettings.FirstSensorNumber = storage.sensors.EnumerateSensors().Max(s => s.ID) + 1;
                Monitor.Log.Warning(PluginSettings.Name,
                    "Расстановка датчиков с номера <" + tmp_FirstSensorNumber +
                    "> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <" +
                    PluginSettings.FirstSensorNumber + ">.");
            }
            // все что возвращает 'IA'
            string[] filesWithJS = storage.files.EnumerateFiles(enFileKind.fileWithPrefix)
                .Where(f => f.fileType.ContainsLanguages(enLanguage.JavaScript))
                .Select(f => f.FullFileName_Original)
                .ToArray();
            // здесь выбираем только *.js файлы
            string[] filesOnlyJS = filesWithJS.Where(f => f.EndsWith(".js")).ToArray();
            NumJS = filesOnlyJS.LongLength;
            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong)NumJS);
            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)1).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong)NumJS);
            if (filesOnlyJS.Length == 0)
            {
                Monitor.Log.Error(PluginSettings.Name, "Исходных файлов js не обнаружено.");
            }
            else
            {
                RunConfiguration config = new RunConfiguration();
                config.fileWithFilelist = Path.Combine(storage.GetTempDirectory(ID).Path, "JSFilelist.txt");
                //build dictionary of encodings
                StreamWriter sw = new StreamWriter(config.fileWithFilelist);
                foreach (var file in filesOnlyJS)
                {
                    var aReader = new AbstractReader(file);
                    sw.WriteLine(file + "|" + MapEncodingName(aReader.getEncoding()));
                }
                sw.Close();

                string currentExeDir = IA.RuntimeDirectories.IARuntimeDirs.Plugins.ToString();
                config.pathRunner = Path.Combine(Directory.GetCurrentDirectory(), currentExeDir, "Runner.exe");

                config.sensorStartId = PluginSettings.FirstSensorNumber; //storage.sensors.GetMaxSensorNumber() + 1;
                config.dirWithClearFiles = storage.appliedSettings.FileListPath;

                //config.dirWithLabFiles = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "JavaScript");
                //config.dirWithLabFiles = storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB);                
                config.dirWithLabFiles = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "JavaScript") + "\\";
                if (Directory.Exists(config.dirWithLabFiles))
                    Directory.Delete(config.dirWithLabFiles, true);
                Directory.CreateDirectory(config.dirWithLabFiles);
                
                
                config.dirWithReports = storage.pluginData.GetDataFolder(ID) + "\\";
                if (PluginSettings.IsLevel2)
                    config.level = 2;
                else
                    config.level = 3;
                
                string pathXML = Path.Combine(config.dirWithReports, "javascript.xml");
                string pathERROR = Path.Combine(config.dirWithReports, "error.log");
                ThreadRunNodeJS(config, pathXML, pathERROR);
#if DEBUG
                if (config.dirWithLabFiles.Contains("runtemp"))
                    WashLabSources(config.dirWithLabFiles);
#endif
                if (IsXML)
                {
                    // а вдруг xml окажется невалидным
                    try
                    {
                        var startSensorCount = storage.sensors.Count();
                        StorageImporter.XMLStorageImporterForParser
                            importer = new StorageImporter.XMLStorageImporterForParser();
                        XmlDocument xdoc = new XmlDocument();
                        xdoc.Load(pathXML);
                        Monitor.Log.Warning(PluginSettings.Name, "Запуск импортера xml файла от парсера.");
                        importer.ImportXML(xdoc, storage);
                        Monitor.Log.Warning(PluginSettings.Name, $"Вставлено датчиков: {(storage.sensors.Count() - startSensorCount)}");
                    }
                    catch (System.Exception ex)
                    {
                        Monitor.Log.Error(PluginSettings.Name,
                            "Критическая ошибка при выполнении импорта данных из xml: " + ex.Message);
                    }
                }
                else
                {
                    if (File.Exists(pathERROR))
                    {
                        string[] aErrors = File.ReadAllLines(pathERROR);
                        foreach (string cError in aErrors)
                            Monitor.Log.Error(PluginSettings.Name, cError);
                    }
                }
            }
        }

        // для запуска парсера в отдельном потоке
        // чисто так на всякий случай, чтобы вдруг стек не испортило или еще что-нибудь
        private void ThreadRunNodeJS(RunConfiguration config, string pathXML, string pathERROR)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;

            string currentExeDir = IA.RuntimeDirectories.IARuntimeDirs.Plugins.ToString();
            string currentDir = Path.Combine(Directory.GetCurrentDirectory(), currentExeDir);
            string nodejsEngine = Path.Combine(currentDir, "jsparser\\nodejs-portable.exe");

            startInfo.FileName = nodejsEngine;
            startInfo.Arguments = "--max_old_space_size=4096 main.js " + config.fileWithFilelist + " " + config.sensorStartId + " " + config.level + " " +
                                  config.dirWithLabFiles + " " + config.dirWithReports + " " + config.dirWithClearFiles;
            startInfo.UseShellExecute = false;
            startInfo.WorkingDirectory = Path.Combine(currentDir, "jsparser");
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            //startInfo.EnvironmentVariables.Add("EDGE_NODE_PARAMS", "--max_old_space_size=4096");
            var pr = Process.Start(startInfo);
            ulong prev = 0;
            bool IsSecondStage = false;
            while (!pr.StandardOutput.EndOfStream)
            {
                string line = pr.StandardOutput.ReadLine();
                ulong i;
                bool IsParsed = ulong.TryParse(line, out i);
                if (IsParsed)
                {
                    if (prev < i && !IsSecondStage)
                        prev = i;
                    else
                        IsSecondStage = true;
                    if (!IsSecondStage)
                        Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(),
                        Monitor.Tasks.Task.UpdateType.STAGE, i);
                    else
                        Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)1).Description(),
                        Monitor.Tasks.Task.UpdateType.STAGE, (ulong)NumJS - i);
                }
                Monitor.Log.Information(PluginSettings.Name, line);
            }
            pr.WaitForExit();
            // ждем появления от парсера или 'javascript.xml' или 'error.log'
            IsXML = File.Exists(pathXML);
        }

        private void WashLabSources(string dirWithLabFiles)
        {
            string[] aLabFiles = Directory.GetFiles(dirWithLabFiles, "*", SearchOption.AllDirectories);
            foreach (string cFile in aLabFiles)
            {
                var aReaderObject = new AbstractReader(cFile);
                string fileContents = aReaderObject.getText();
                
                string replace = "Sensor()";
                Regex rgx_I = new Regex("Sensor\\(\\d+\\)");
                fileContents = rgx_I.Replace(fileContents, replace);

                replace = "IARETREPLACE";
                Regex rgx_II = new Regex("IARETREPLACE\\d+");
                fileContents = rgx_II.Replace(fileContents, replace);

                aReaderObject.writeTextToFile(cFile, fileContents);
            }
        }

#region Misc

        public string Name { get { return PluginSettings.Name; } }


        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (!(PluginSettings.IsLevel2 ? PluginSettings.SensorText_Level2 : 
                PluginSettings.SensorText_Level3).Contains("#") && !PluginSettings.IsNoSensors)
            {
                message = "Текст датчика должен содержать символ #.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.IsLevel2 = PluginSettings.IsLevel2;
            Properties.Settings.Default.IsLevel3 = PluginSettings.IsLevel3;
            Properties.Settings.Default.IsNoSensors = PluginSettings.IsNoSensors;
            Properties.Settings.Default.FirstSensorNumber = PluginSettings.FirstSensorNumber;
            Properties.Settings.Default.SensorFunctionText_Level2 = PluginSettings.SensorFunctionText_Level2;
            Properties.Settings.Default.SensorText_Level2 = PluginSettings.SensorText_Level2;
            Properties.Settings.Default.SensorFunctionText_Level3 = PluginSettings.SensorFunctionText_Level3;
            Properties.Settings.Default.SensorText_Level3 = PluginSettings.SensorText_Level3;
            Properties.Settings.Default.IsSkipErrors = PluginSettings.IsSkipErrors;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.IsLevel2, PluginSettings.IsLevel3, PluginSettings.IsNoSensors);
            settingsBuffer.Add(PluginSettings.FirstSensorNumber);
            settingsBuffer.Add(PluginSettings.SensorFunctionText_Level2);
            settingsBuffer.Add(PluginSettings.SensorText_Level2);
            settingsBuffer.Add(PluginSettings.SensorFunctionText_Level3);
            settingsBuffer.Add(PluginSettings.SensorText_Level3);
            settingsBuffer.Add(PluginSettings.IsSkipErrors);
        }

        private void LoadPluginData()
        {
            //////считываем массив байт из файла
            //byte[] data = File.ReadAllBytes(storage.pluginData.GetDataFileName(PluginSettings.ID));

            ////Если файл данных плагина пуст, или содрежит недостаточно информации
            //if (data.Length != 2)
            //{
            //    if (data.Length != 0)
            //        IA.Monitor.Log.Warning(PluginSettings.Name, "Файл данных плагина повреждён. Не удалось загрузить битовый массив.");
            //    return;
            //}

            //PluginSettings.IsStage1Completed = data[0] != 0;
            //PluginSettings.IsStage2Completed = data[1] != 0;
        }

        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new Settings(
                        storage.sensors.EnumerateSensors().Count() == 0
                        ? 1
                        : storage.sensors.EnumerateSensors().Max(s => s.ID) + 1)
                        );
            }
        }
#endregion

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        // <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description()),
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.SENSOR_PLACEMENT.Description())
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return null;
            }
        }

    } // END: class JavaScriptParser_NodeJS


    internal class RunConfiguration
    {
        //на основе main.js
        public string pathRunner;
        public string fileWithFilelist;
        public ulong sensorStartId;
        public string dirWithClearFiles;
        public string dirWithLabFiles;
        public string dirWithReports;
        public int level;

        public override string ToString()
        {
            return string.Join(" ",
                fileWithFilelist,
                sensorStartId.ToString(),
                level.ToString(),
                dirWithLabFiles,
                dirWithReports,
                dirWithClearFiles
                );
        }
    }
}
