﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using IA.Extensions;
using Store;

namespace IA.Plugins.Parsers.CommonUtils
{
    /// <summary>
    /// Класс формиравания отчетов операторов после обработки исходных текстов парсерами
    /// </summary>
    public class StatementPrinter
    {
        private XmlTextWriter xtw;

        private Storage storage;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="xmlFullFilePath">Путь к xml-файлу отчета.</param>
        /// <param name="storage">Хранилище</param>
        public StatementPrinter(String xmlFullFilePath, Storage storage)
        {
            this.storage = storage;

            xtw = new XmlTextWriter(xmlFullFilePath, Encoding.UTF8);
        }

        /// <summary>
        /// Выполнение генерации отчета
        /// </summary>
        public void Execute()
        {
            xtw.WriteStartDocument();
            xtw.WriteStartElement("Files");
            xtw.Formatting = Formatting.Indented;

            GenReport();

            xtw.WriteEndElement();
            xtw.WriteEndDocument();

            xtw.Flush();
            xtw.Close();
        }

        /// <summary>
        /// Генерация отчета.
        /// </summary>
        /// <param name="statement">Конструкция.</param>
        private void GenReport(IStatement statement = null)
        {
            // Если st = null, значит мы не в функции
            if (statement == null)
            {
                foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix)
                                            .Concat(storage.files.EnumerateFiles(enFileKind.externalFile)
                                                .Where(file => file.FileNameForReports.Equals("Глобальное окружение")))) // ходим по файлам  
                {
                    xtw.WriteStartElement("file");
                    xtw.WriteAttributeString("Name", file.FileNameForReports);

                    foreach (IFunction function in file.FunctionsDefinedInTheFile()) // ходим по функциям в файле
                    {
                        xtw.WriteStartElement("Function");
                        xtw.WriteAttributeString("Name", function.FullNameForReports);
                        xtw.WriteAttributeString("Id", function.Id.ToString());

                        //переменные объявленные в функции
                        PrintVariables(function);

                        if (function.EntryStatement != null)
                            GenReport(function.EntryStatement);

                        xtw.WriteEndElement();
                    }
                    xtw.WriteEndElement();
                }
            }
            else
            {
                //есть стейтмент - значит мы внутри функции
                //цикл
                IStatement iter = statement;
                do
                {
                    switch (iter.Kind)
                    {
                        #region STOperational
                        case ENStatementKind.STOperational:
                            {
                                PrintOperational(iter);

                            }
                            break;
                        #endregion
                        #region STDoAndWhile
                        case ENStatementKind.STDoAndWhile:
                            {
                                IStatementDoAndWhile sta = (IStatementDoAndWhile)iter;
                                if (sta.IsCheckConditionBeforeFirstRun) // это DO WHILE или WHILE DO
                                {//WHILE DO
                                    xtw.WriteStartElement("while_do");

                                    xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                    xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                                    xtw.WriteAttributeString("File", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");
                                    PrintOperation(sta.Condition);

                                    if (sta.Body != null)
                                    {
                                        GenReport(sta.Body);
                                    }

                                    xtw.WriteEndElement();
                                }
                                else
                                {//DO WHILE
                                    xtw.WriteStartElement("do_while");

                                    xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                    xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                                    xtw.WriteAttributeString("File", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location"); 
                                    if (sta.Body != null)
                                    {
                                        GenReport(sta.Body);
                                    }

                                    PrintOperation(sta.Condition);

                                    xtw.WriteEndElement();
                                }

                            }
                            break;
                        #endregion
                        #region STReturn
                        case ENStatementKind.STReturn:
                            {
                                IStatementReturn sta = (IStatementReturn)iter;

                                PrintOperation(sta.ReturnOperation);

                                xtw.WriteStartElement("return");

                                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                            xtw.WriteAttributeString("File", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");
                                xtw.WriteEndElement();

                            }
                            break;
                        #endregion
                        #region STBreak
                        case ENStatementKind.STBreak:
                            {
                                xtw.WriteStartElement("break");

                                xtw.WriteAttributeString("Position", iter.FirstSymbolLocation != null ? '(' + iter.FirstSymbolLocation.GetLine().ToString() + ',' + iter.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", iter.SensorBeforeTheStatement != null ? iter.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                                xtw.WriteAttributeString("File", iter.FirstSymbolLocation != null ? '(' + iter.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STGoto
                        case ENStatementKind.STGoto:
                            {
                                xtw.WriteStartElement("goto");

                                xtw.WriteAttributeString("Position", iter.FirstSymbolLocation != null ? '(' + iter.FirstSymbolLocation.GetLine().ToString() + ',' + iter.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", iter.SensorBeforeTheStatement != null ? iter.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                                xtw.WriteAttributeString("File", iter.FirstSymbolLocation != null ? '(' + iter.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");

                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STContinue
                        case ENStatementKind.STContinue:
                            {
                                IStatementContinue sta = (IStatementContinue)iter;

                                xtw.WriteStartElement("continue");

                                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                            xtw.WriteAttributeString("File", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");

                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STIf
                        case ENStatementKind.STIf:
                            {
                                IStatementIf sta = (IStatementIf)iter;

                                xtw.WriteStartElement("if");
                                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                            xtw.WriteAttributeString("EndPosition", sta.LastSymbolLocation != null ? '(' + sta.LastSymbolLocation.GetLine().ToString() + ',' + sta.LastSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                            xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                            xtw.WriteAttributeString("File", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");
                                if (sta.Condition != null)
                                {
                                    PrintOperation(sta.Condition);
                                }
                                if (sta.ThenStatement != null)
                                {
                                    xtw.WriteStartElement("then");

                                    GenReport(sta.ThenStatement);

                                    xtw.WriteEndElement();
                                }
                                if (sta.ElseStatement != null)
                                {
                                    xtw.WriteStartElement("else");

                                    GenReport(sta.ElseStatement);

                                    xtw.WriteEndElement();
                                }
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STFor
                        case ENStatementKind.STFor:
                            {
                                IStatementFor sta = (IStatementFor)iter;

                                xtw.WriteStartElement("for");
                                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                            xtw.WriteAttributeString("File", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");

                                PrintOperation(sta.Start);
                                PrintOperation(sta.Condition);
                                PrintOperation(sta.Iteration);
                                if (sta.Body != null)
                                {
                                    xtw.WriteStartElement("body");
                                    GenReport(sta.Body);
                                    xtw.WriteEndElement();
                                }
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STForEach
                        case ENStatementKind.STForEach:
                            {

                                IStatementForEach sta = (IStatementForEach)iter;
                                xtw.WriteStartElement("foreach");
                                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                            xtw.WriteAttributeString("File", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");

                                PrintOperation(sta.Head);
                                if (sta.Body != null)
                                {
                                    xtw.WriteStartElement("body");
                                    GenReport(sta.Body);
                                    xtw.WriteEndElement();
                                }
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STSwitch
                        case ENStatementKind.STSwitch:
                            {
                                IStatementSwitch sta = (IStatementSwitch)iter;

                                xtw.WriteStartElement("switch");
                                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                            xtw.WriteAttributeString("File", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");

                                PrintOperation(sta.Condition);

                                foreach (KeyValuePair<IOperation, IStatement> casies in sta.Cases())
                                {
                                    xtw.WriteStartElement("case");

                                    PrintOperation(casies.Key);

                                    if (casies.Value != null) GenReport(casies.Value);

                                    xtw.WriteEndElement();
                                }
                                if (sta.DefaultCase() != null)
                                {
                                    xtw.WriteStartElement("default");

                                    GenReport(sta.DefaultCase());

                                    xtw.WriteEndElement();
                                }
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STThrow
                        case ENStatementKind.STThrow:
                            {
                                IStatementThrow sta = (IStatementThrow)iter;

                                xtw.WriteStartElement("throw");
                                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                            xtw.WriteAttributeString("File", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetFile().FileNameForReports + ')' : "NUll_Location");

                                PrintOperation(sta.Exception);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STTryCatchFinally
                        case ENStatementKind.STTryCatchFinally:
                            {
                                IStatementTryCatchFinally sta = (IStatementTryCatchFinally)iter;

                                if (sta.TryBlock != null)
                                {
                                    xtw.WriteStartElement("try");
                                    xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");

                                    GenReport(sta.TryBlock);

                                    xtw.WriteEndElement();
                                }
                                else break;

                                foreach (IStatement catches in sta.Catches())
                                {
                                    xtw.WriteStartElement("catch");
                                    xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");

                                    GenReport(catches);

                                    xtw.WriteEndElement();
                                }

                                if (sta.FinallyBlock != null)
                                {
                                    xtw.WriteStartElement("finally");
                                    xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");

                                    GenReport(sta.FinallyBlock);

                                    xtw.WriteEndElement();
                                }
                            }
                            break;
                        #endregion
                        #region STUsing
                        case ENStatementKind.STUsing:
                            {
                                IStatementUsing sta = (IStatementUsing)iter;
                                xtw.WriteStartElement("using");
                                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");

                                PrintOperation(sta.Head);
                                if (sta.Body != null)
                                {
                                    xtw.WriteStartElement("body");
                                    GenReport(sta.Body);
                                    xtw.WriteEndElement();
                                }
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STYieldReturn
                        case ENStatementKind.STYieldReturn:
                            {
                                IStatementYieldReturn sta = (IStatementYieldReturn)iter;
                                
                                xtw.WriteStartElement("yield_return");

                                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                                xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");
                                
                                PrintOperation(sta.YieldReturnOperation);

                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region DEFAULT
                        default:
                            xtw.WriteStartElement(iter.Kind.ToString());
                            xtw.WriteEndElement();
                            break;
                        #endregion
                    }

                } while ((iter = iter.NextInLinearBlock) != null);
            }
            return;
        }

        private void PrintOperational(IStatement iter)
        {
            xtw.WriteStartElement("IOperational");
            if (iter != null)
            {
                IStatementOperational sta = (IStatementOperational)iter;
                IOperation op = sta.Operation; // получаем инкапсулированный Operation

                xtw.WriteAttributeString("Position", sta.FirstSymbolLocation != null ? '(' + sta.FirstSymbolLocation.GetLine().ToString() + ',' + sta.FirstSymbolLocation.GetColumn().ToString() + ')' : "NUll_Location");
                xtw.WriteAttributeString("SensorID", sta.SensorBeforeTheStatement != null ? sta.SensorBeforeTheStatement.ID.ToString() : "NULL_Sensor");

                PrintOperation(op);
            }
            xtw.WriteEndElement();
        }

        private void PrintOperation(IOperation op)
        {
            xtw.WriteStartElement("IOperation");
            if (op != null)
            {

                foreach (IFunction[] fun in op.SequentiallyCallsFunctions())// если несколько в одну линию в квадратных скобках и через запятые
                {
                    String tmps = "";
                    String ids = "";
                    if (fun.Length != 0)
                    {

                        if (fun.Length > 1) // если точно неизвестно какая функция вызывается вывести все варианты
                        {
                            foreach (IFunction fun1 in fun) // но сначала их надо собрать в кучу
                            {
                                tmps += "[" + fun1.FullNameForReports + "],";
                                ids += "[" + fun1.Id + "],";
                            }
                            tmps.TrimEnd(','); // и убрать лишнюю запятую
                            xtw.WriteStartElement("Function");
                            xtw.WriteAttributeString("Name", tmps);
                            xtw.WriteAttributeString("Id", ids);
                            xtw.WriteEndElement();
                        }
                        else
                        {//Если вариант один, выводим его 
                            xtw.WriteStartElement("Function");
                            xtw.WriteAttributeString("Name", fun[0].FullNameForReports);
                            xtw.WriteAttributeString("Id", fun[0].Id.ToString());
                            xtw.WriteEndElement();
                        }
                    }
                }
            }
            xtw.WriteEndElement();
        }

        private void PrintVariables(IFunction fun)
        {
            foreach (Variable var in storage.variables.EnumerateVariables())
            {
                if (var.Definitions().Length > 0 && var.Definitions()[0].GetFunctionId() == fun.Id)
                {
                    xtw.WriteStartElement("Variable");
                    xtw.WriteAttributeString("Name", var.FullName);
                    xtw.WriteAttributeString("Definition", '(' + var.Definitions()[0].GetLine().ToString() + ',' + var.Definitions()[0].GetColumn().ToString() + ')');

                    //геты
                    foreach (Location getLoc in var.ValueGetPosition())
                    {
                        xtw.WriteStartElement("Get");
                        xtw.WriteAttributeString("Position", '(' + getLoc.GetLine().ToString() + ',' + getLoc.GetColumn().ToString() + ')');
                        xtw.WriteEndElement();
                    }

                    //сеты
                    foreach (Location setLoc in var.ValueSetPosition())
                    {
                        xtw.WriteStartElement("Set");
                        xtw.WriteAttributeString("Position", '(' + setLoc.GetLine().ToString() + ',' + setLoc.GetColumn().ToString() + ')');
                        xtw.WriteEndElement();
                    }

                    //колы
                    foreach (Location callLoc in var.ValueCallPosition())
                    {
                        xtw.WriteStartElement("Call");
                        xtw.WriteAttributeString("Position", '(' + callLoc.GetLine().ToString() + ',' + callLoc.GetColumn().ToString() + ')');
                        xtw.WriteEndElement();
                    }

                    xtw.WriteEndElement();
                }
            }
        }


        /// <summary>
        /// Второй вриант функции генерации xml отчета
        /// Не требует файлов в отличие от Execute()
        /// </summary>
        public void Execute_NoFile()
        {
            xtw.WriteStartDocument();
            xtw.Formatting = Formatting.Indented;

            GenReport_2();

            xtw.WriteEndDocument();

            xtw.Flush();
            xtw.Close();
        }


        Dictionary<UInt64, UInt64> filesTable;
        Dictionary<UInt64, UInt64> namespaceTable;
        Dictionary<UInt64, UInt64> classesTable;
        Dictionary<UInt64, UInt64> operationsTable;
        Dictionary<UInt64, UInt64> statementsTable;
        Dictionary<UInt64, UInt64> functionsTable;
        Dictionary<UInt64, UInt64> variablesTable;
        /// <summary>
        /// Генерация отчета для Execute_NoFile()
        /// </summary>
        private void GenReport_2()
        {
            ulong uID = 1; // сквозной уникальный идентификатор для всего xml документа
            filesTable = new Dictionary<UInt64, UInt64>();
            namespaceTable = new Dictionary<UInt64, UInt64>();
            classesTable = new Dictionary<UInt64, UInt64>();
            operationsTable = new Dictionary<UInt64, UInt64>();
            statementsTable = new Dictionary<UInt64, UInt64>();
            functionsTable = new Dictionary<UInt64, UInt64>();
            variablesTable = new Dictionary<UInt64, UInt64>();
            //
            xtw.WriteStartElement("Database");
            // xtw.WriteAttributeString("xmlns", "", null, "http://www.w3.org/2001/XMLSchema");
            /* Files */
            var v_Files = storage.files.EnumerateFiles(enFileKind.anyFile).ToList();
            if (v_Files.Count > 0)
            {
                xtw.WriteStartElement("Files");
                foreach (var cFile in v_Files)
                {
                    xtw.WriteStartElement("File");
                    xtw.WriteAttributeString("fullname", cFile.RelativeFileName_Original);
                    xtw.WriteAttributeString("ID", filesUID(cFile.Id, ref uID).ToString());
                    xtw.WriteEndElement();
                }
                xtw.WriteEndElement();
            }
            /* Namespace */
            List<Namespace> v_Nmspcs = storage.namespaces.EnumerateNamespaces().ToList();
            if (v_Nmspcs.Count > 0)
            {
                xtw.WriteStartElement("Namespaces");
                foreach (Namespace c_Nmspcs in v_Nmspcs)
                {
                    xtw.WriteStartElement("Namespace");
                    xtw.WriteAttributeString("ID", namespacesUID(c_Nmspcs.Id, ref uID).ToString());
                    xtw.WriteAttributeString("Name", c_Nmspcs.FullName);
                    xtw.WriteEndElement();
                }
                xtw.WriteEndElement();
            }
            /* Class */
            List<Class> v_Class = storage.classes.EnumerateClasses().ToList();
            if (v_Class.Count > 0)
            {
                xtw.WriteStartElement("Classes");
                foreach (Class c_Class in v_Class)
                {
                    //< par:Class ID = "1000.00" Name = "string" Namespace = "string" >
                    xtw.WriteStartElement("Class");
                    xtw.WriteAttributeString("ID", classesUID(c_Class.Id, ref uID).ToString());
                    xtw.WriteAttributeString("Name", c_Class.Name);
                    xtw.WriteAttributeString("FullName", c_Class.FullName);
                    List<IFunction> v_methds = c_Class.EnumerateMethods() as List<IFunction>;
                    if (v_methds != null)
                    {
                        foreach (IFunction c_methd in v_methds)
                        {
                            xtw.WriteAttributeString("Method_ID", functionsUID(c_methd.Id, ref uID).ToString());
                            xtw.WriteAttributeString("Method_Name", c_methd.NameForReports);
                        }
                    }
                    xtw.WriteEndElement();
                }
                xtw.WriteEndElement();
            }
            v_Class.Clear();
            /* Variables */
            List<Variable> vVars = storage.variables.EnumerateVariables().ToList();
            if (vVars.Count > 0)
            {
                xtw.WriteStartElement("Variables");
                foreach (Variable cVar in vVars)
                {
                    xtw.WriteStartElement("Variable");
                    xtw.WriteAttributeString("ID", variablesUID(cVar.Id, ref uID).ToString());
                    xtw.WriteAttributeString("Name", cVar.FullName);
                    xtw.WriteAttributeString("Namespace", "");
                    xtw.WriteAttributeString("isGlobal", cVar.isGlobal.ToString().ToLower());

                    xtw.WriteStartElement("Definition");
                    foreach (Location location in cVar.Definitions())
                    {
                        printLocation(location, ref uID);
                    }
                    xtw.WriteEndElement();

                    //Место присвоения переменной значения другой переменной
                    xtw.WriteStartElement("AssignmentFrom");
                    foreach (IAssignment cAssgn in cVar.Enumerate_AssignmentsFrom())
                    {
                        Variable vrblFrom = cAssgn.From;
                        xtw.WriteStartElement("VariableReference");
                        xtw.WriteAttributeString("ID", variablesUID(vrblFrom.Id, ref uID).ToString());
                        xtw.WriteAttributeString("kind", "variable"); // здесь как-то нужно указать тип переменной
                        xtw.WriteEndElement();
                        printLocation(cAssgn.Position, ref uID);
                    }
                    xtw.WriteEndElement();

                    //Места присвоения указателя на функцию
                    xtw.WriteStartElement("PointsToFunctions");
                    foreach (PointsToFunction cAssgn in cVar.Enumerate_PointsToFunctions())
                    {
                        xtw.WriteStartElement("FunctionReference");
                        xtw.WriteAttributeString("ID", functionsUID(cAssgn.function, ref uID).ToString());
                        xtw.WriteAttributeString("kind", "variable"); // здесь как-то нужно указать тип переменной
                        xtw.WriteEndElement();
                        printLocation(cAssgn.position, ref uID);
                    }
                    xtw.WriteEndElement();

                    xtw.WriteStartElement("ValueCallPosition");
                    foreach (Location callLoc in cVar.ValueCallPosition())
                    {
                        xtw.WriteStartElement("FunctionReference"); // здесь берется явно что-то не то
                        xtw.WriteAttributeString("ID", functionsUID(callLoc.GetFunctionId(), ref uID).ToString());
                        // здесь получить бы тип функции, но реализация пока что отложено на будущее
                        xtw.WriteEndElement();
                        printLocation(callLoc, ref uID);
                    }
                    xtw.WriteEndElement();

                    xtw.WriteStartElement("ValueGetPosition");
                    foreach (Location getLoc in cVar.ValueGetPosition())
                    {
                        printLocation(getLoc, ref uID);
                    }
                    xtw.WriteEndElement();

                    xtw.WriteStartElement("ValueSetPosition");
                    foreach (Location setLoc in cVar.ValueSetPosition())
                    {
                        printLocation(setLoc, ref uID);
                    }
                    xtw.WriteEndElement();
                    xtw.WriteEndElement();
                }
                xtw.WriteEndElement();
            }
            vVars.Clear();
            /* Functions */
            List<IFunction> vFuncs = storage.functions.EnumerateFunctions(enFunctionKind.ALL).ToList();
            if (vFuncs.Count > 0)
            {
                xtw.WriteStartElement("Functions");
                foreach (IFunction cFunc in vFuncs)
                {
                    bool isMethod = cFunc is IMethod;
                    if (isMethod)
                        xtw.WriteStartElement("Method");
                    else
                        xtw.WriteStartElement("Function");
                    ulong local_uID = 0;
                    if (!functionsTable.ContainsKey(cFunc.Id))
                    {
                        local_uID = uID;
                        functionsTable.Add(cFunc.Id, uID++);
                    }
                    else
                        local_uID = functionsTable[cFunc.Id];
                    xtw.WriteAttributeString("ID", local_uID.ToString());
                    xtw.WriteAttributeString("Name", cFunc.FullName);
                    xtw.WriteAttributeString("Namespace", ""); // признак не заносится в Хранилище
                    if (isMethod)
                    {
                        if ( ((IMethod)cFunc).isOverrideBase != null )
                            xtw.WriteAttributeString("isOverrideBase", ((IMethod) cFunc).isOverrideBase.ToString());
                    }

                    xtw.WriteStartElement("Defenition");
                    Location location = cFunc.Definition();
                    xtw.WriteAttributeString("line", location.GetLine().ToString());
                    xtw.WriteAttributeString("column", location.GetColumn().ToString());
                    xtw.WriteAttributeString("file", filesTable[location.GetFile().Id].ToString());
                    xtw.WriteEndElement();
                        
                    foreach (Location loc in cFunc.Declarations())
                    {
                        xtw.WriteStartElement("Declaration");
                        xtw.WriteAttributeString("line", loc.GetLine().ToString());
                        xtw.WriteAttributeString("column", loc.GetColumn().ToString());
                        xtw.WriteAttributeString("file", filesTable[loc.GetFile().Id].ToString());
                        xtw.WriteEndElement();
                    }
                    // <EntryStatement ID="1" kind="STOperational" />
                    if (cFunc.EntryStatement != null)
                    {
                        xtw.WriteStartElement("EntryStatement");
                        xtw.WriteAttributeString("ID", statementsuID(cFunc.EntryStatement, ref uID).ToString());
                        xtw.WriteAttributeString("kind", cFunc.EntryStatement.Kind.ToString());
                        xtw.WriteEndElement(); // </EntryStatement>
                    }
                    xtw.WriteEndElement();// </Function> or </Method>
                }
                xtw.WriteEndElement();
            }
            vFuncs.Clear();
            /* Statements */
            List<IStatement> v_Stmnts = storage.statements.EnumerateStatements().ToList();
            if (v_Stmnts.Count > 0)
            {
                xtw.WriteStartElement("Statements");
                //xtw.WriteAttributeString("xmlns", "", null, "http://www.w3.org/2001/XMLSchema");
                foreach (IStatement iter in v_Stmnts)
                {
                    ulong local_uID = statementsuID(iter, ref uID);

                    switch (iter.Kind)
                    {
                        #region STOperational 
                        case ENStatementKind.STOperational:
                            {
                                var st = (IStatementOperational) iter;
                                xtw.WriteStartElement("STOperational");
                                if (st.Operation != null)
                                {
                                    xtw.WriteAttributeString("operation", local_uID.ToString());
                                }
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STDoAndWhile
                        case ENStatementKind.STDoAndWhile:
                            {
                                IStatementDoAndWhile sta = (IStatementDoAndWhile) iter;
                                xtw.WriteStartElement("STDoAndWhile");
                                xtw.WriteAttributeString("condition", local_uID.ToString());
                                if (sta.IsCheckConditionBeforeFirstRun)
                                    xtw.WriteAttributeString("isCheckConditionBeforeFirstRun", "1");
                                else
                                    xtw.WriteAttributeString("isCheckConditionBeforeFirstRun", "0");
                                //if (sta.SensorBeforeTheStatement != null)
                                //{
                                //    xtw.WriteStartElement("SensorBeforeTheStatement", sta.SensorBeforeTheStatement.ID.ToString());// еще можно печатать kind-позицию датчика
                                //    xtw.WriteEndElement();
                                //}
                                // откуда это вообще взять
                                // <!--0 to 3 repetitions: -->
                                // <else ID = "anySimpleType" kind = "STBreak" isOnBreak = "Everytime"/>
                                print_PSF(iter, ref uID);
                                xtw.WriteStartElement("body"); //<body ID = "anySimpleType" kind = "STBreak"/>
                                local_uID = 0;
                                if (!statementsTable.ContainsKey(sta.Body.Id))
                                {
                                    local_uID = uID;
                                    statementsTable.Add(sta.Body.Id, uID++);
                                }
                                else
                                    local_uID = statementsTable[sta.Body.Id];

                                xtw.WriteAttributeString("ID", statementsuID(sta.Body, ref uID).ToString());
                                xtw.WriteAttributeString("kind", sta.Body.Kind.ToString());
                                xtw.WriteEndElement(); // </body>
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STReturn
                        case ENStatementKind.STReturn:
                            {
                                IStatementReturn sta = (IStatementReturn) iter;
                                xtw.WriteStartElement("STReturn");
                                local_uID = 0;
                                if (sta.ReturnOperation != null)
                                {
                                    if (!statementsTable.ContainsKey(sta.ReturnOperation.Id))
                                    {
                                        local_uID = uID;
                                        statementsTable.Add(sta.ReturnOperation.Id, uID++);
                                    }
                                    else
                                        local_uID = statementsTable[sta.ReturnOperation.Id];
                                }
                                xtw.WriteAttributeString("ReturnOperation", operationuID(sta.ReturnOperation, ref uID).ToString());
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STBreak
                        case ENStatementKind.STBreak:
                            {
                                xtw.WriteStartElement("STBreak");
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STGoto
                        case ENStatementKind.STGoto:
                            {
                                xtw.WriteStartElement("STGoto");
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STContinue
                        case ENStatementKind.STContinue:
                            {
                                IStatementContinue sta = (IStatementContinue)iter;
                                xtw.WriteStartElement("STContinue");
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STIf
                        case ENStatementKind.STIf:
                            {
                                IStatementIf sta = (IStatementIf)iter;

                                xtw.WriteStartElement("STIf");
                                xtw.WriteAttributeString("condition", operationuID(sta.Condition, ref uID).ToString());
                                print_PSF(iter, ref uID);
                                    
                                if (sta.ThenStatement != null)
                                {
                                    xtw.WriteStartElement("then");
                                    print_IK((IStatement)sta.ThenStatement, ref uID);
                                    xtw.WriteEndElement();
                                }
                                if (sta.ElseStatement != null)
                                {
                                    xtw.WriteStartElement("else");
                                    print_IK((IStatement)sta.ElseStatement, ref uID);
                                    xtw.WriteEndElement();
                                }
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STFor
                        case ENStatementKind.STFor:
                            {
                                IStatementFor sta = (IStatementFor)iter;
                                xtw.WriteStartElement("STFor");
                                xtw.WriteAttributeString("start", operationuID(sta.Start, ref uID).ToString());
                                xtw.WriteAttributeString("condition", operationuID(sta.Condition, ref uID).ToString());
                                xtw.WriteAttributeString("iteration", operationuID(sta.Iteration, ref uID).ToString());
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STForEach
                        case ENStatementKind.STForEach:
                            {
                                IStatementForEach sta = (IStatementForEach)iter;
                                xtw.WriteStartElement("STForEach");
                                xtw.WriteAttributeString("head", operationuID(sta.Head, ref uID).ToString());
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STSwitch
                        case ENStatementKind.STSwitch:
                            {
                                IStatementSwitch sta = (IStatementSwitch)iter;
                                xtw.WriteStartElement("STSwitch");
                                print_PSF(iter, ref uID);
                                foreach (KeyValuePair<IOperation, IStatement> casies in sta.Cases())
                                {
                                    xtw.WriteStartElement("case");
                                    xtw.WriteAttributeString("condition", operationuID(sta.Condition, ref uID).ToString());
                                    xtw.WriteStartElement("block");
                                    xtw.WriteString("0");
                                    xtw.WriteEndElement();
                                    xtw.WriteEndElement();
                                }
                                if (sta.DefaultCase() != null)
                                {
                                    xtw.WriteStartElement("default");
                                    xtw.WriteStartElement("block");
                                    xtw.WriteString("0");
                                    xtw.WriteEndElement();
                                    xtw.WriteEndElement();
                                }
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STThrow
                        case ENStatementKind.STThrow:
                            {
                                IStatementThrow sta = (IStatementThrow)iter;
                                xtw.WriteStartElement("STThrow");
                                xtw.WriteAttributeString("exception", operationuID(sta.Exception, ref uID).ToString());
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STTryCatchFinally
                        case ENStatementKind.STTryCatchFinally:
                            {
                                IStatementTryCatchFinally sta = (IStatementTryCatchFinally)iter;
                                xtw.WriteStartElement("STTryCatchFinally");
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STUsing
                        case ENStatementKind.STUsing:
                            {
                                IStatementUsing sta = (IStatementUsing)iter;
                                xtw.WriteStartElement("STUsing");
                                xtw.WriteAttributeString("head", operationuID(sta.Head, ref uID).ToString());
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region STYieldReturn
                        case ENStatementKind.STYieldReturn:
                            {
                                IStatementYieldReturn sta = (IStatementYieldReturn)iter;
                                xtw.WriteStartElement("STYieldReturn");
                                xtw.WriteAttributeString("YieldReturnOperation", operationuID(sta.YieldReturnOperation, ref uID).ToString());
                                print_PSF(iter, ref uID);
                                xtw.WriteEndElement();
                            }
                            break;
                        #endregion
                        #region DEFAULT
                        default: // - код не проверен
                            xtw.WriteStartElement(iter.Kind.ToString());
                            print_PSF(iter, ref uID);
                            xtw.WriteEndElement();
                            break;
                            #endregion
                    }
                } /* END: foreach (IStatement p_stmnt in v_Stmnts) */
                xtw.WriteEndElement();// </Statements>
            } /* foreach ( Statements ) */
            v_Stmnts.Clear();
            /* Operations */
            List<IStatement> v_opers = storage.statements.EnumerateStatements().Where(
                g => g.Kind == ENStatementKind.STOperational && ((IStatementOperational)g).Operation != null).ToList();
            if (v_opers.Count > 0)
            {
                xtw.WriteStartElement("Operations");
                foreach (IStatement c_oper in v_opers)
                {
                    var st = (IStatementOperational) c_oper;
                    var op = st.Operation;
                    //note if variable did not point to function - corresponding function wouldn't be returned

                    xtw.WriteStartElement("Operation");
                    xtw.WriteStartElement("callNode");
                    foreach (var funcArray in op.SequentiallyCallsFunctions())
                    {
                        funcArray.ForEach(g =>
                        {
                            xtw.WriteStartElement("callFunction");
                            xtw.WriteAttributeString("ID", functionsUID(g, ref uID).ToString());
                            xtw.WriteAttributeString("kind", (g is IMethod)? "method" : "function");
                            xtw.WriteEndElement();
                        });
                    }
                    xtw.WriteEndElement();
                    xtw.WriteEndElement();
                }
                xtw.WriteEndElement();// </Operations>
            }
            v_opers.Clear();
            xtw.WriteEndElement(); /* /Database */
        } /* END: void GenReport_2(IStatement statement = null) */
        
        private void PrintOperation_2(IOperation op, ref ulong uID)
        {
            if (op != null)
            {
                xtw.WriteStartElement("Operation");
                foreach (IFunction[] fun in op.SequentiallyCallsFunctions())// если несколько в одну линию в квадратных скобках и через запятые
                {
                    String tmps = "";
                    String ids = "";
                    if (fun.Length != 0)
                    {

                        if (fun.Length > 1) // если точно неизвестно какая функция вызывается вывести все варианты
                        {
                            foreach (IFunction fun1 in fun) // но сначала их надо собрать в кучу
                            {
                                tmps += "[" + fun1.FullNameForReports + "],";
                                ids += "[" + functionsUID(fun1, ref uID) + "],";
                            }
                            tmps.TrimEnd(','); // и убрать лишнюю запятую
                            xtw.WriteStartElement("Function");
                            xtw.WriteAttributeString("Name", tmps);
                            xtw.WriteAttributeString("Id", ids);
                            xtw.WriteEndElement();
                        }
                        else
                        {//Если вариант один, выводим его 
                            xtw.WriteStartElement("Function");
                            xtw.WriteAttributeString("Name", fun[0].FullNameForReports);
                            xtw.WriteAttributeString("Id", functionsUID(fun[0].Id, ref uID).ToString());
                            xtw.WriteEndElement();
                        }
                    }
                }
                xtw.WriteEndElement();
            }
        } /* END: PrintOperation_2() */
        
        /// <summary>
        /// Печать общей части у Statement и общего уникального идентификатора
        /// Position, Sensor, File
        /// </summary>
        void print_PSF(IStatement p_stmnt, ref ulong uID)
        {
            xtw.WriteAttributeString("ID", statementsuID(p_stmnt, ref uID).ToString());

            if (p_stmnt.NextInLinearBlock != null)
            {
                xtw.WriteStartElement("NextInLinearBlock");
                print_IK(p_stmnt.NextInLinearBlock, ref uID);
                xtw.WriteEndElement();
            }

            if (p_stmnt.SensorBeforeTheStatement != null)
            {
                xtw.WriteStartElement("SensorBeforeTheStatement");
                xtw.WriteString(p_stmnt.SensorBeforeTheStatement.ID.ToString());
                //xtw.WriteAttributeString("kind", p_stmnt.SensorBeforeTheStatement.Kind.ToString()); // по идее вычисляется при занесении
                xtw.WriteEndElement();
            }

            xtw.WriteStartElement("FirstSymbolLocation");
            if (p_stmnt.FirstSymbolLocation != null) printLocation(p_stmnt.FirstSymbolLocation, ref uID);
            xtw.WriteEndElement();
            xtw.WriteStartElement("LastSymbolLocation");
            if (p_stmnt.LastSymbolLocation != null) printLocation(p_stmnt.LastSymbolLocation, ref uID);
            xtw.WriteEndElement();
        }

        /// <summary>
        /// Печать ID, kind у последующий частей операторов
        /// if - then - else (например)
        /// </summary>
        void print_IK(IStatement p_stmnt)
        {
            if (p_stmnt != null)
            {
                xtw.WriteAttributeString("ID", p_stmnt.Id.ToString());
                xtw.WriteAttributeString("kind", p_stmnt.Kind.ToString());
            }
            else
            {
                xtw.WriteAttributeString("ID", "0");
                xtw.WriteAttributeString("kind", "0");
            }
        }

        /// <summary>
        /// Печать ID, kind у последующий частей операторов
        /// if - then - else (например)
        /// </summary>
        void print_IK(IStatement p_stmnt, ref ulong uID)
        {
            if (p_stmnt != null)
            {
                xtw.WriteAttributeString("ID", statementsuID(p_stmnt, ref uID).ToString());
                xtw.WriteAttributeString("kind", p_stmnt.Kind.ToString());
            }
            else
            {
                xtw.WriteAttributeString("ID", "0");
                xtw.WriteAttributeString("kind", "0");
            }
        }

        void printLocation(Location loc, ref ulong uID)
        {
            xtw.WriteStartElement("LocationLine");
            xtw.WriteAttributeString("line", loc.GetLine().ToString());
            xtw.WriteAttributeString("column", loc.GetColumn().ToString());
            xtw.WriteAttributeString("file", filesUID(loc.GetFile().Id, ref uID).ToString());
            if (loc.GetFunctionId() != Store.Const.CONSTS.WrongID)
            {
                xtw.WriteAttributeString("function", functionsUID(loc.GetFunctionId(), ref uID).ToString());
            }
            xtw.WriteEndElement();
        }

        ulong filesUID(ulong fileID, ref ulong uID)
        {
            ulong local_uID = 0;
            if (!filesTable.ContainsKey(fileID))
            {
                local_uID = uID;
                filesTable.Add(fileID, uID++);
            }
            else
                local_uID = filesTable[fileID];
            return local_uID;
        }

        ulong namespacesUID(ulong nsID, ref ulong uID)
        {
            ulong local_uID = 0;
            if (!namespaceTable.ContainsKey(nsID))
            {
                local_uID = uID;
                namespaceTable.Add(nsID, uID++);
            }
            else
                local_uID = namespaceTable[nsID];
            return local_uID;
        }

        ulong classesUID(ulong classID, ref ulong uID)
        {
            ulong local_uID = 0;
            if (!classesTable.ContainsKey(classID))
            {
                local_uID = uID;
                classesTable.Add(classID, uID++);
            }
            else
                local_uID = classesTable[classID];
            return local_uID;
        }

        ulong variablesUID(ulong varID, ref ulong uID)
        {
            ulong local_uID = 0;
            if (!variablesTable.ContainsKey(varID))
            {
                local_uID = uID;
                variablesTable.Add(varID, uID++);
            }
            else
                local_uID = variablesTable[varID];
            return local_uID;
        }

        ulong functionsUID(IFunction func, ref ulong uID)
        {
            ulong local_uID = 0;
            if (func != null)
            {
                if (!functionsTable.ContainsKey(func.Id))
                {
                    local_uID = uID;
                    functionsTable.Add(func.Id, uID++);
                }
                else
                    local_uID = functionsTable[func.Id];
            }
            return local_uID;
        }

        ulong functionsUID(ulong funcID, ref ulong uID)
        {
            ulong local_uID = 0;
            if (!functionsTable.ContainsKey(funcID))
            {
                local_uID = uID;
                functionsTable.Add(funcID, uID++);
            }
            else
                local_uID = functionsTable[funcID];
            
            return local_uID;
        }

        ulong operationuID(IOperation opr, ref ulong uID)
        {
            ulong local_uID = 0;
            if (opr != null)
            {
                if (!operationsTable.ContainsKey(opr.Id))
                {
                    local_uID = uID;
                    operationsTable.Add(opr.Id, uID++);
                }
                else
                    local_uID = operationsTable[opr.Id];
            }
            return local_uID;
        }

        ulong statementsuID(IStatement stm, ref ulong uID)
        {
            ulong local_uID = 0;
            if (stm != null)
            {
                if (!statementsTable.ContainsKey(stm.Id))
                {
                    local_uID = uID;
                    statementsTable.Add(stm.Id, uID++);
                }
                else
                    local_uID = statementsTable[stm.Id];
            }
            return local_uID;
        }

    }/* END: public class StatementPrinter */
}
