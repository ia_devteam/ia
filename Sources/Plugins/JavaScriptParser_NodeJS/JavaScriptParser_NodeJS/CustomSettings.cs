﻿using System;
using System.Windows.Forms;
using IA.Plugins.Parsers.JavaScriptParser_NodeJS;

namespace IA.Plugins.Parsers.JavaScriptParser_NodeJS
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        internal Settings(ulong minimumStartSensor)
        {
            InitializeComponent();

            //Основные настройки для элементов форм
            //Выбор уровня НДВ
            isLevel2_radioButton.Checked = PluginSettings.IsLevel2;
            isLevel3_radioButton.Checked = PluginSettings.IsLevel3;

            //Номер первого датчика
            numericUpDown1.Minimum = minimumStartSensor;
            numericUpDown1.Maximum = ulong.MaxValue;
            numericUpDown1.Value = 
                    (PluginSettings.FirstSensorNumber > minimumStartSensor)
                    ? PluginSettings.FirstSensorNumber
                    : minimumStartSensor;
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.IsLevel2 = isLevel2_radioButton.Checked;
            PluginSettings.IsLevel3 = isLevel3_radioButton.Checked;
            PluginSettings.IsNoSensors = level0_radioButton.Checked;
            PluginSettings.FirstSensorNumber = (ulong)numericUpDown1.Value;

            if (isLevel2_radioButton.Checked)
            {
                //PluginSettings.SensorFunctionText_Level2 = sensorFunctionText_textBox.Text;
                //PluginSettings.SensorText_Level2 = sensorText_textBox.Text;
            }
            else if (isLevel3_radioButton.Checked)
            {
                //PluginSettings.SensorFunctionText_Level3 = sensorFunctionText_textBox.Text;
                //PluginSettings.SensorText_Level3 = sensorText_textBox.Text;
            }
        }
        // Задаем номер первого датчика
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            PluginSettings.FirstSensorNumber = (ulong)numericUpDown1.Value;
        }
        // Устанавливаем уровень 'НДВ-2'
        private void isLevel2_radioButton_MouseClick(object sender, MouseEventArgs e)
        {
            PluginSettings.IsLevel2 = isLevel2_radioButton.Checked;
            PluginSettings.IsLevel3 = isLevel3_radioButton.Checked = !isLevel2_radioButton.Checked;
        }
        // Устанавливаем уровень 'НДВ-3'
        private void isLevel3_radioButton_MouseClick(object sender, MouseEventArgs e)
        {
            PluginSettings.IsLevel3 = isLevel3_radioButton.Checked;
            PluginSettings.IsLevel2 = isLevel2_radioButton.Checked = !isLevel3_radioButton.Checked;
        }
    }
}
