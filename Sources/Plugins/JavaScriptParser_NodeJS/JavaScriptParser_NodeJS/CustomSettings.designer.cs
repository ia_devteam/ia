﻿namespace IA.Plugins.Parsers.JavaScriptParser_NodeJS
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            //base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.additionalSettings_groupBox = new System.Windows.Forms.GroupBox();
            this.additionalSettings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.firstSensorNumber_groupBox = new System.Windows.Forms.GroupBox();
            this.firstSensorNumber_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.level_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.level0_radioButton = new System.Windows.Forms.RadioButton();
            this.lblVersion = new System.Windows.Forms.Label();
            this.isLevel3_radioButton = new System.Windows.Forms.RadioButton();
            this.isLevel2_radioButton = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sensors_groupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.firstSensorNumber_label = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.additionalSettings_groupBox.SuspendLayout();
            this.firstSensorNumber_groupBox.SuspendLayout();
            this.level_tableLayoutPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.main_tableLayoutPanel.SuspendLayout();
            this.sensors_groupBox.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.additionalSettings_groupBox, 0, 4);
            this.settings_tableLayoutPanel.Controls.Add(this.firstSensorNumber_groupBox, 0, 1);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 5;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(200, 100);
            this.settings_tableLayoutPanel.TabIndex = 0;
            // 
            // additionalSettings_groupBox
            // 
            this.additionalSettings_groupBox.Controls.Add(this.additionalSettings_tableLayoutPanel);
            this.additionalSettings_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.additionalSettings_groupBox.Location = new System.Drawing.Point(3, 53);
            this.additionalSettings_groupBox.Name = "additionalSettings_groupBox";
            this.additionalSettings_groupBox.Size = new System.Drawing.Size(194, 44);
            this.additionalSettings_groupBox.TabIndex = 6;
            this.additionalSettings_groupBox.TabStop = false;
            this.additionalSettings_groupBox.Text = "Дополнительные параметры";
            // 
            // additionalSettings_tableLayoutPanel
            // 
            this.additionalSettings_tableLayoutPanel.ColumnCount = 1;
            this.additionalSettings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.additionalSettings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.additionalSettings_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.additionalSettings_tableLayoutPanel.Name = "additionalSettings_tableLayoutPanel";
            this.additionalSettings_tableLayoutPanel.RowCount = 1;
            this.additionalSettings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.additionalSettings_tableLayoutPanel.Size = new System.Drawing.Size(188, 25);
            this.additionalSettings_tableLayoutPanel.TabIndex = 1;
            // 
            // firstSensorNumber_groupBox
            // 
            this.firstSensorNumber_groupBox.Controls.Add(this.firstSensorNumber_tableLayoutPanel);
            this.firstSensorNumber_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstSensorNumber_groupBox.Location = new System.Drawing.Point(3, 53);
            this.firstSensorNumber_groupBox.Name = "firstSensorNumber_groupBox";
            this.firstSensorNumber_groupBox.Size = new System.Drawing.Size(194, 49);
            this.firstSensorNumber_groupBox.TabIndex = 7;
            this.firstSensorNumber_groupBox.TabStop = false;
            this.firstSensorNumber_groupBox.Text = "Номер первого датчика";
            // 
            // firstSensorNumber_tableLayoutPanel
            // 
            this.firstSensorNumber_tableLayoutPanel.ColumnCount = 1;
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.firstSensorNumber_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstSensorNumber_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.firstSensorNumber_tableLayoutPanel.Name = "firstSensorNumber_tableLayoutPanel";
            this.firstSensorNumber_tableLayoutPanel.RowCount = 1;
            this.firstSensorNumber_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.firstSensorNumber_tableLayoutPanel.Size = new System.Drawing.Size(188, 30);
            this.firstSensorNumber_tableLayoutPanel.TabIndex = 0;
            // 
            // level_tableLayoutPanel
            // 
            this.level_tableLayoutPanel.ColumnCount = 4;
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Controls.Add(this.level0_radioButton, 2, 0);
            this.level_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.level_tableLayoutPanel.Name = "level_tableLayoutPanel";
            this.level_tableLayoutPanel.RowCount = 1;
            this.level_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Size = new System.Drawing.Size(188, 25);
            this.level_tableLayoutPanel.TabIndex = 3;
            // 
            // level0_radioButton
            // 
            this.level0_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.level0_radioButton.AutoSize = true;
            this.level0_radioButton.Location = new System.Drawing.Point(291, 4);
            this.level0_radioButton.Name = "level0_radioButton";
            this.level0_radioButton.Size = new System.Drawing.Size(138, 17);
            this.level0_radioButton.TabIndex = 2;
            this.level0_radioButton.Text = "Не вставлять датчики";
            this.level0_radioButton.UseVisualStyleBackColor = true;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(3, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(64, 13);
            this.lblVersion.TabIndex = 11;
            this.lblVersion.Text = "10.10.17.45";
            // 
            // isLevel3_radioButton
            // 
            this.isLevel3_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isLevel3_radioButton.AutoSize = true;
            this.isLevel3_radioButton.Location = new System.Drawing.Point(103, 9);
            this.isLevel3_radioButton.Name = "isLevel3_radioButton";
            this.isLevel3_radioButton.Size = new System.Drawing.Size(94, 17);
            this.isLevel3_radioButton.TabIndex = 1;
            this.isLevel3_radioButton.Text = "3-й уровень";
            this.isLevel3_radioButton.UseVisualStyleBackColor = true;
            this.isLevel3_radioButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.isLevel3_radioButton_MouseClick);
            // 
            // isLevel2_radioButton
            // 
            this.isLevel2_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isLevel2_radioButton.AutoSize = true;
            this.isLevel2_radioButton.Location = new System.Drawing.Point(3, 9);
            this.isLevel2_radioButton.Name = "isLevel2_radioButton";
            this.isLevel2_radioButton.Size = new System.Drawing.Size(94, 17);
            this.isLevel2_radioButton.TabIndex = 0;
            this.isLevel2_radioButton.Text = "2-й уровень";
            this.isLevel2_radioButton.UseVisualStyleBackColor = true;
            this.isLevel2_radioButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.isLevel2_radioButton_MouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(634, 55);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Уровень вставки датчиков";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.isLevel3_radioButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.isLevel2_radioButton, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(628, 36);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.sensors_groupBox, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.groupBox1, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.lblVersion, 0, 0);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 9;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(640, 480);
            this.main_tableLayoutPanel.TabIndex = 9;
            // 
            // sensors_groupBox
            // 
            this.sensors_groupBox.Controls.Add(this.tableLayoutPanel4);
            this.sensors_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensors_groupBox.Location = new System.Drawing.Point(3, 113);
            this.sensors_groupBox.Name = "sensors_groupBox";
            this.sensors_groupBox.Size = new System.Drawing.Size(634, 49);
            this.sensors_groupBox.TabIndex = 6;
            this.sensors_groupBox.TabStop = false;
            this.sensors_groupBox.Text = "Расстановка датчиков";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.firstSensorNumber_label, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.numericUpDown1, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(628, 30);
            this.tableLayoutPanel4.TabIndex = 6;
            // 
            // firstSensorNumber_label
            // 
            this.firstSensorNumber_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.firstSensorNumber_label.AutoSize = true;
            this.firstSensorNumber_label.Location = new System.Drawing.Point(3, 8);
            this.firstSensorNumber_label.Name = "firstSensorNumber_label";
            this.firstSensorNumber_label.Size = new System.Drawing.Size(131, 13);
            this.firstSensorNumber_label.TabIndex = 1;
            this.firstSensorNumber_label.Text = "Номер первого датчика:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDown1.Location = new System.Drawing.Point(140, 5);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(485, 20);
            this.numericUpDown1.TabIndex = 0;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // Settings
            // 
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(640, 480);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.additionalSettings_groupBox.ResumeLayout(false);
            this.firstSensorNumber_groupBox.ResumeLayout(false);
            this.level_tableLayoutPanel.ResumeLayout(false);
            this.level_tableLayoutPanel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.main_tableLayoutPanel.PerformLayout();
            this.sensors_groupBox.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.RadioButton level0_radioButton;
        private System.Windows.Forms.GroupBox additionalSettings_groupBox;
        private System.Windows.Forms.TableLayoutPanel level_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel additionalSettings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox firstSensorNumber_groupBox;
        private System.Windows.Forms.TableLayoutPanel firstSensorNumber_tableLayoutPanel;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.RadioButton isLevel3_radioButton;
        private System.Windows.Forms.RadioButton isLevel2_radioButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.GroupBox sensors_groupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label firstSensorNumber_label;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
    }
}
