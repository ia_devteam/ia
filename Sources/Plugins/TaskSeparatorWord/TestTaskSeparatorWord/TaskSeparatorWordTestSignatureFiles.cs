﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestUtils;
using Word = Microsoft.Office.Interop.Word;
using System.Linq;

namespace TestTaskSeparatorWord
{
    [TestClass]
    public class TaskSeparatorWordTestSignatureFiles
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// проверка работы с файлом с сигнатурами без разделов
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWordTest_SignatureFiles()
        {
            object missing = System.Reflection.Missing.Value;
            Object readOnly1 = false;
            object fName;
            object NOSaveChanges = Word.WdSaveOptions.wdDoNotSaveChanges;

            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                // plugin
                new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

                // isUseCachedStorage
                false,

                // prepareStorage  
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Шаблон.dot"));
                    writer.Add(false);
                    writer.Add(1);
                    writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Signatures.txt"));
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Source\"));
                    TestUtilsClass.Run_IndexingFileContent(storage);
                },  
                
                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Res_Sign");
                    
                    foreach (string file in Directory.EnumerateFiles(path, "~*"))
                        File.Delete(file);

                    foreach (string file in Directory.EnumerateFiles(reportsPath, "~*"))
                        File.Delete(file);

                    if (Directory.EnumerateFiles(path).Count() != Directory.EnumerateFiles(reportsPath).Count())
                        return false;

                    foreach (string file in Directory.EnumerateFiles(path))
                        if (file.EndsWith(".txt"))
                        {
                            if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath), false, true))
                                return false;
                        }
                        else
                        {
                            if (!TestUtilsClass.Reports_DOC_Compare(file, file.Replace(path, reportsPath)))
                                return false;
                        }

                    return true;
                },        
   
                // isUpdateReport
                false,    
                    
                // changeSettingsBeforeRerun              
                (plugin, testMaterialsPath) => { },  
                    
                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },  
             
                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            
                );
        }
    }
}
