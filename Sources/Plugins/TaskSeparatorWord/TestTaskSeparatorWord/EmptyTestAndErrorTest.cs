﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestUtils;
using System.Collections.Generic;
using System.Linq;

namespace TestTaskSeparatorWord
{
    [TestClass]
    public class EmptyTestAndErrorTest
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// нет шаблона Word
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_NoTemplateTestTaskSeparatorWord()
        {
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>();
            messageTypes.Add(IA.Monitor.Log.Message.enType.ERROR);
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>();
            IA.Monitor.Log.Message mes = new IA.Monitor.Log.Message("Сигнатурный анализ",
                                 IA.Monitor.Log.Message.enType.ERROR,
                                 "Файл шаблона документа не найден.");
            messages.Add(mes);

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                string.Empty,

                // plugin
                new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

                // isUseCachedStorage
                false,

                // prepareStorage  
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(Path.Combine(testMaterialsPath, @"Шаблон.dot"));
                    writer.Add(false);
                    writer.Add(1);
                    writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Signatures.txt"));
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Source\"));
                    TestUtilsClass.Run_IndexingFileContent(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun              
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; },
                true);
            }
        }

        /// <summary>
        /// нет файла с сигнатурами
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_NoSignaturesTestTaskSeparatorWord()
        {
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>();
            messageTypes.Add(IA.Monitor.Log.Message.enType.ERROR);
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>();
            IA.Monitor.Log.Message mes = new IA.Monitor.Log.Message("Сигнатурный анализ",
                                 IA.Monitor.Log.Message.enType.ERROR,
                                 "Файлы с сигнатурами не указаны.");
            messages.Add(mes);

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                string.Empty,

                // plugin
                new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

                // isUseCachedStorage
                false,

                // prepareStorage  
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Шаблон.dot"));
                    writer.Add(false);
                    writer.Add(0);
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Source\"));
                    TestUtilsClass.Run_IndexingFileContent(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun              
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; },
                true);
            }
        }

        /// <summary>
        /// неверный путь к файлу с сигнатурами
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_IncorrectPathToSignaturesTestTaskSeparatorWord()
        {
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>();
            messageTypes.Add(IA.Monitor.Log.Message.enType.ERROR);
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>();
            IA.Monitor.Log.Message mes = new IA.Monitor.Log.Message("Сигнатурный анализ",
                                 IA.Monitor.Log.Message.enType.ERROR,
                                 "Файл с сигнатурами " + Path.Combine(testUtils.TestMatirialsDirectoryPath, @"TaskSeparatorWord\ignatures.txt") + " не найден.");
            messages.Add(mes);

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                string.Empty,

                // plugin
                new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

                // isUseCachedStorage
                false,

                // prepareStorage  
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(Path.Combine(testMaterialsPath, @"Шаблон.dot"));
                    writer.Add(false);
                    writer.Add(1);
                    writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\ignatures.txt"));
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Source\"));
                    TestUtilsClass.Run_IndexingFileContent(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun              
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; },
                true);
            }
        }

        /// <summary>
        /// В шаблоне отсутствует один из стилей. Файлы сгенерируются, но без этого стиля
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_IncorrectTemplate()
        {
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>();
            messageTypes.Add(IA.Monitor.Log.Message.enType.WARNING);
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>();
            IA.Monitor.Log.Message mes = new IA.Monitor.Log.Message("Генератор документов Word",
                                 IA.Monitor.Log.Message.enType.WARNING, "Не найден стиль \"Нашел 1\"");
            messages.Add(mes);
            mes = new IA.Monitor.Log.Message("Генератор документов Word",
                                 IA.Monitor.Log.Message.enType.WARNING, "Не найден стиль \"Путь\"");
            messages.Add(mes);
            mes = new IA.Monitor.Log.Message("Генератор документов Word",
                                 IA.Monitor.Log.Message.enType.WARNING, "Не найден стиль \"Нашел 1\"");
            messages.Add(mes);
            mes = new IA.Monitor.Log.Message("Генератор документов Word",
                                 IA.Monitor.Log.Message.enType.WARNING, "Не найден стиль \"Путь\"");
            messages.Add(mes);
            mes = new IA.Monitor.Log.Message("Генератор документов Word",
                                 IA.Monitor.Log.Message.enType.WARNING, "Не найден стиль \"Нашел 1\"");
            messages.Add(mes);
            mes = new IA.Monitor.Log.Message("Генератор документов Word",
                                 IA.Monitor.Log.Message.enType.WARNING, "Не найден стиль \"Путь\"");
            messages.Add(mes);
            mes = new IA.Monitor.Log.Message("Генератор документов Word",
                     IA.Monitor.Log.Message.enType.WARNING, "Не найден стиль \"Нашел 1\"");
            messages.Add(mes);
            mes = new IA.Monitor.Log.Message("Генератор документов Word",
                                 IA.Monitor.Log.Message.enType.WARNING, "Не найден стиль \"Путь\"");
            messages.Add(mes);

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                    string.Empty,

                    // plugin 
                    new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

                    // isUseCachedStorage
                    false,

                    // prepareStorage                                         
                    (storage, testMaterialsPath) => { },

                    // customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        //выбор настроек
                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                        writer.Add(false);
                        writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Шаблон Bad.dot"));
                        writer.Add(false);
                        writer.Add(2);
                        writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Signatures.txt"));
                        writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Сигнатуры для .NET.txt"));
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                        TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Source\"));
                        TestUtilsClass.Run_IndexingFileContent(storage);
                    },

                    // checkStorage
                    (storage, testMaterialsPath) => { return true; },

                    // checkReportBeforeRerun                       
                    (reportsPath, testMaterialsPath) =>
                    {
                        string path = Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Res");
                        
                        foreach (string file in Directory.EnumerateFiles(reportsPath, "~*"))
                            File.Delete(file);

                        foreach (string file in Directory.EnumerateFiles(path, "~*"))
                            File.Delete(file);

                        if (Directory.EnumerateFiles(path).Count() != Directory.EnumerateFiles(reportsPath).Count())
                            return false;

                        foreach (string file in Directory.EnumerateFiles(path))
                        {
                            if (file.EndsWith(".doc"))
                            {
                                if (!TestUtilsClass.Reports_DOC_Compare(file, file.Replace(path, reportsPath)))
                                    return false;
                            }
                            else
                            {
                                if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath), isIgnoreLinesOrder: true))
                                    return false;
                            }
                        }
                        return true;
                    },

                    // isUpdateReport
                    false,

                    // changeSettingsBeforeRerun                     
                    (plugin, testMaterialsPath) => { },

                    // checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                     // checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                    );
            }
        }

        /// <summary>
        /// Пустая строка в списке сигнатур. Игнорируются. 
        /// в строке нет ничего кроме пробела. Игнорируется 
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_EmptyLinesInSignatures()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                // plugin 
                new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

                // isUseCachedStorage
                false,

                // prepareStorage                                         
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //выбор настроек
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Шаблон.dot"));
                    writer.Add(false);
                    writer.Add(2);
                    writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Сигнатуры для .NET bad.txt"));
                    writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Signatures.txt"));
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Source\"));
                    TestUtilsClass.Run_IndexingFileContent(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun                       
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Res");

                    foreach (string file in Directory.EnumerateFiles(reportsPath, "~*"))
                        File.Delete(file);

                    foreach (string file in Directory.EnumerateFiles(path, "~*"))
                        File.Delete(file);

                    if (Directory.EnumerateFiles(path).Count() != Directory.EnumerateFiles(reportsPath).Count())
                        return false;


                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (file.EndsWith(".txt"))
                        {
                            if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath), false, true))
                                return false;
                        }
                        else
                        {
                            if (!TestUtilsClass.Reports_DOC_Compare(file, file.Replace(path, reportsPath)))
                                return false;
                        }
                    }

                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun                     
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                 // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// Одна из сигнатур содержит пробел. Информируем пользователя и игнорируем сигнатуру 
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_SpaceInSignature()
        {
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>();
            messageTypes.Add(IA.Monitor.Log.Message.enType.WARNING);
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>();
            IA.Monitor.Log.Message mes = new IA.Monitor.Log.Message("Генератор документов Word",
                                 IA.Monitor.Log.Message.enType.ERROR, "Сигнатура \"Load LibraryA\" содержит символ пробела и будет проигнорирована.");
            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                    string.Empty,

                    // plugin 
                    new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

                    // isUseCachedStorage
                    false,

                    // prepareStorage                                         
                    (storage, testMaterialsPath) => { },

                    // customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        //выбор настроек
                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                        writer.Add(false);
                        writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Шаблон.dot"));
                        writer.Add(false);
                        writer.Add(2);
                        writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Сигнатуры для .NET bad space.txt"));
                        writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Signatures.txt"));
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                        TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Source\"));
                        TestUtilsClass.Run_IndexingFileContent(storage);
                    },

                    // checkStorage
                    (storage, testMaterialsPath) => { return true; },

                    // checkReportBeforeRerun                       
                    (reportsPath, testMaterialsPath) =>
                    {
                        string path = Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Res_space");

                        foreach (string file in Directory.EnumerateFiles(reportsPath, "~*"))
                            File.Delete(file);

                        foreach (string file in Directory.EnumerateFiles(path, "~*"))
                            File.Delete(file);

                        if (Directory.EnumerateFiles(path).Count() != Directory.EnumerateFiles(reportsPath).Count())
                            return false;


                        foreach (string file in Directory.EnumerateFiles(path))
                        {
                            if (file.EndsWith(".txt"))
                            {
                                if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath), false, true))
                                    return false;
                            }
                            else
                            {
                                if (!TestUtilsClass.Reports_DOC_Compare(file, file.Replace(path, reportsPath)))
                                    return false;
                            }
                        }

                        return true;
                    },

                    // isUpdateReport
                    false,

                    // changeSettingsBeforeRerun                     
                    (plugin, testMaterialsPath) => { },

                    // checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                     // checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                    );
            }
        }

        /// <summary>
        /// Нестандартные кодировки в файлах с сигнатурами. Игнорируются. 
        /// Если кодировка будет неверной - файл не будет разделен на строки, как положено. 
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_BadEncodings()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                // plugin 
                new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

                // isUseCachedStorage
                false,

                // prepareStorage                                         
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //выбор настроек
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Шаблон.dot"));
                    writer.Add(false);
                    writer.Add(2);
                    writer.Add(Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Сигнатуры для .NET bad encoding UTF8.txt"));
                    writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Сигнатуры для .NET bad encoding Unix.txt"));
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Source\"));
                    TestUtilsClass.Run_IndexingFileContent(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun                       
                (reportsPath, testMaterialsPath) =>
                {
                    string path = System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Res_encoding");
                    
                    foreach (string file in Directory.EnumerateFiles(reportsPath, "~*"))
                        File.Delete(file);

                    foreach (string file in Directory.EnumerateFiles(path, "~*"))
                        File.Delete(file);

                    if (Directory.EnumerateFiles(path).Count() != Directory.EnumerateFiles(reportsPath).Count())
                        return false;
                                        
                    foreach (string file in Directory.EnumerateFiles(path))
                        if (!TestUtilsClass.Reports_DOC_Compare(file, file.Replace(path, reportsPath)))
                            return false;

                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun                     
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                 // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        ///  При сигнатурном анализе учитываются протоколы Understand. Если вхождение сигнатуры являеся вызовом или определением функции из исходных текстов,
        ///  то по умолчанию (без мождификации пользхователем) вхождение не попадает в результаты. 
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_WithUnderstand()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                // plugin 
                new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

                // isUseCachedStorage
                false,

                // prepareStorage                                         
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //выбор настроек
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false);
                    writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Шаблон.dot"));
                    writer.Add(true);
                    writer.Add(1);
                    writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Сигнатуры для .NET.txt"));
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Source\"));
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Understand\"), @"F:\TaskSeparatorWord");
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun                       
                (reportsPath, testMaterialsPath) =>
                {
                    string path = System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\resWithUnderstand");
                    
                    foreach (string file in Directory.EnumerateFiles(reportsPath, "~*"))
                        File.Delete(file);

                    foreach (string file in Directory.EnumerateFiles(path, "~*"))
                        File.Delete(file);

                    if (Directory.EnumerateFiles(path).Count() != Directory.EnumerateFiles(reportsPath).Count())
                        return false;

                    foreach (string file in Directory.EnumerateFiles(path))
                        if (!TestUtilsClass.Reports_DOC_Compare(file, file.Replace(path, reportsPath)))
                            return false;

                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun                     
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                 // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// нет вхождений сигнатур в файлы, полученные прогоном Fill File List. В результатах нет файлов Word. 
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_NoSignaturesFoundTestTaskSeparatorWord()
        {
            testUtils.RunTest(
                // sourcePrefixPart
            string.Empty,

            // plugin
            new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

            // isUseCachedStorage
            false,

            // prepareStorage  
            (storage, testMaterialsPath) => { },

            // customizeStorage
            (storage, testMaterialsPath) =>
            {
                Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                writer.Add(false);
                writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Шаблон.dot"));
                writer.Add(false);
                writer.Add(1);
                writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Signatures.txt"));
                storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"FillFileList\EmptyFolder\"));
                TestUtilsClass.Run_IndexingFileContent(storage);
            },

            // checkStorage
            (storage, testMaterialsPath) => { return true; },

            // checkReportBeforeRerun
            (reportsPath, testMaterialsPath) =>
            {
                string path = System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Res_NoSignaturesFound");

                if (Directory.EnumerateFiles(path).Count() != Directory.EnumerateFiles(reportsPath).Count())
                    return false;

                foreach (string file in Directory.EnumerateFiles(path))
                    if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        return false;

                return true;
            },

            // isUpdateReport
            false,

            // changeSettingsBeforeRerun              
            (plugin, testMaterialsPath) => { },

            // checkStorageAfterRerun
            (storage, testMaterialsPath) => { return true; },

            // checkReportAfterRerun
            (reportsPath, testMaterialsPath) => { return true; },
            true);
        }

        /// <summary>
        /// Тест сигнатур с пустой строкой в конце и без. Вхождений нет. 
        /// </summary>
        [TestMethod]
        public void TaskSeparatorWord_EmptyStringInTheEndOfSignaturesTestTaskSeparatorWord()
        {
            testUtils.RunTest(
                // sourcePrefixPart
            string.Empty,

            // plugin
            new IA.Plugins.Analyses.TaskSeparatorWord.TaskSeparatorWord(),

            // isUseCachedStorage
            false,

            // prepareStorage  
            (storage, testMaterialsPath) => { },

            // customizeStorage
            (storage, testMaterialsPath) =>
            {
                Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                writer.Add(false);
                writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Шаблон.dot"));
                writer.Add(false);
                writer.Add(2);
                writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Signatures.txt"));
                writer.Add(System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Signatures2.txt"));
                storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD, writer);

                if (!Directory.Exists(Path.Combine(testMaterialsPath, @"FillFileList\EmptyFolder\")))
                    Directory.CreateDirectory(Path.Combine(testMaterialsPath, @"FillFileList\EmptyFolder\"));

                TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"FillFileList\EmptyFolder\"));
                TestUtilsClass.Run_IndexingFileContent(storage);
            },

            // checkStorage
            (storage, testMaterialsPath) => { return true; },

            // checkReportBeforeRerun
            (reportsPath, testMaterialsPath) =>
            {
                string path = System.IO.Path.Combine(testMaterialsPath, @"TaskSeparatorWord\Res_EmptyStringInTheEndOfSignatures");

                if (Directory.EnumerateFiles(path).Count() != Directory.EnumerateFiles(reportsPath).Count())
                    return false;

                foreach (string file in Directory.EnumerateFiles(path))
                    if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        return false;

                return true;
            },

            // isUpdateReport
            false,

            // changeSettingsBeforeRerun              
            (plugin, testMaterialsPath) => { },

            // checkStorageAfterRerun
            (storage, testMaterialsPath) => { return true; },

            // checkReportAfterRerun
            (reportsPath, testMaterialsPath) => { return true; },
            true);
        }
    }
}
