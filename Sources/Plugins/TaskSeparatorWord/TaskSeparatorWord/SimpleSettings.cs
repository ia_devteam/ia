﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.TaskSeparatorWord
{
    /// <summary>
    /// Простые настройки пользователя
    /// </summary>
    public partial class SimpleSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Элемент управления выбора файлов с сигнатурами
        /// </summary>
        private SignatureFilesControl signatureFilesControl = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        public SimpleSettings()
        {
            InitializeComponent();

            // Инициализация элемента управления выбора файлов с сигнатурами
            signatureFilesControl = new SignatureFilesControl()
            {
                SignatureFiles = PluginSettings.InputFiles,
                Dock = DockStyle.Fill
            };

            settings_tableLayoutPanel.Controls.Add(signatureFilesControl, 0, 0);
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.InputFiles = signatureFilesControl.SignatureFiles;
        }
    }
}
