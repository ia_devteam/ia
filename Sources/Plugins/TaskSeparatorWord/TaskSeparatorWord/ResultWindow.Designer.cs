﻿namespace IA.Plugins.Analyses.TaskSeparatorWord
{
    partial class ResultWindow
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultWindow_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.selectAll_button = new System.Windows.Forms.Button();
            this.uncheckSelected_button = new System.Windows.Forms.Button();
            this.deselectAll_button = new System.Windows.Forms.Button();
            this.checkSelected_button = new System.Windows.Forms.Button();
            this.allEntries_listView = new System.Windows.Forms.ListView();
            this.Вхождение = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.selectedCount_label = new System.Windows.Forms.Label();
            this.passedCounter_label = new System.Windows.Forms.Label();
            this.fileText_richTextBox = new System.Windows.Forms.RichTextBox();
            this.resultWindow_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultWindow_tableLayoutPanel
            // 
            this.resultWindow_tableLayoutPanel.ColumnCount = 5;
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.resultWindow_tableLayoutPanel.Controls.Add(this.selectAll_button, 0, 3);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.uncheckSelected_button, 3, 3);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.deselectAll_button, 1, 3);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.checkSelected_button, 2, 3);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.allEntries_listView, 0, 0);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.selectedCount_label, 0, 2);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.passedCounter_label, 0, 1);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.fileText_richTextBox, 4, 0);
            this.resultWindow_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultWindow_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.resultWindow_tableLayoutPanel.Name = "resultWindow_tableLayoutPanel";
            this.resultWindow_tableLayoutPanel.RowCount = 4;
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.resultWindow_tableLayoutPanel.Size = new System.Drawing.Size(639, 336);
            this.resultWindow_tableLayoutPanel.TabIndex = 0;
            // 
            // selectAll_button
            // 
            this.selectAll_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectAll_button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.selectAll_button.Location = new System.Drawing.Point(3, 294);
            this.selectAll_button.Name = "selectAll_button";
            this.selectAll_button.Size = new System.Drawing.Size(73, 39);
            this.selectAll_button.TabIndex = 0;
            this.selectAll_button.Text = "Выбрать все";
            this.selectAll_button.UseVisualStyleBackColor = true;
            this.selectAll_button.Click += new System.EventHandler(this.selectAll_button_Click);
            // 
            // uncheckSelected_button
            // 
            this.uncheckSelected_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uncheckSelected_button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.uncheckSelected_button.Location = new System.Drawing.Point(240, 294);
            this.uncheckSelected_button.Name = "uncheckSelected_button";
            this.uncheckSelected_button.Size = new System.Drawing.Size(73, 39);
            this.uncheckSelected_button.TabIndex = 3;
            this.uncheckSelected_button.Text = "Снять выделенные";
            this.uncheckSelected_button.UseVisualStyleBackColor = true;
            this.uncheckSelected_button.Click += new System.EventHandler(this.uncheckSelected_button_Click);
            // 
            // deselectAll_button
            // 
            this.deselectAll_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deselectAll_button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.deselectAll_button.Location = new System.Drawing.Point(82, 294);
            this.deselectAll_button.Name = "deselectAll_button";
            this.deselectAll_button.Size = new System.Drawing.Size(73, 39);
            this.deselectAll_button.TabIndex = 1;
            this.deselectAll_button.Text = "Снять все";
            this.deselectAll_button.UseVisualStyleBackColor = true;
            this.deselectAll_button.Click += new System.EventHandler(this.deselectAll_button_Click);
            // 
            // checkSelected_button
            // 
            this.checkSelected_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkSelected_button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkSelected_button.Location = new System.Drawing.Point(161, 294);
            this.checkSelected_button.Name = "checkSelected_button";
            this.checkSelected_button.Size = new System.Drawing.Size(73, 39);
            this.checkSelected_button.TabIndex = 2;
            this.checkSelected_button.Text = "Отметить выделенные";
            this.checkSelected_button.UseVisualStyleBackColor = true;
            this.checkSelected_button.Click += new System.EventHandler(this.checkSelected_button_Click);
            // 
            // allEntries_listView
            // 
            this.allEntries_listView.CheckBoxes = true;
            this.allEntries_listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Вхождение});
            this.resultWindow_tableLayoutPanel.SetColumnSpan(this.allEntries_listView, 4);
            this.allEntries_listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allEntries_listView.Location = new System.Drawing.Point(3, 3);
            this.allEntries_listView.Name = "allEntries_listView";
            this.allEntries_listView.Size = new System.Drawing.Size(310, 245);
            this.allEntries_listView.TabIndex = 2;
            this.allEntries_listView.UseCompatibleStateImageBehavior = false;
            this.allEntries_listView.View = System.Windows.Forms.View.Details;
            this.allEntries_listView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.allEntries_listView_ItemChecked);
            this.allEntries_listView.SelectedIndexChanged += new System.EventHandler(this.allEntries_listView_SelectedIndexChanged);
            this.allEntries_listView.Resize += new System.EventHandler(this.allEntries_listView_Resize);
            // 
            // Вхождение
            // 
            this.Вхождение.Text = "Вхождение";
            this.Вхождение.Width = 397;
            // 
            // selectedCount_label
            // 
            this.selectedCount_label.AutoSize = true;
            this.resultWindow_tableLayoutPanel.SetColumnSpan(this.selectedCount_label, 4);
            this.selectedCount_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectedCount_label.Location = new System.Drawing.Point(3, 271);
            this.selectedCount_label.Name = "selectedCount_label";
            this.selectedCount_label.Size = new System.Drawing.Size(310, 20);
            this.selectedCount_label.TabIndex = 3;
            // 
            // passedCounter_label
            // 
            this.passedCounter_label.AutoSize = true;
            this.resultWindow_tableLayoutPanel.SetColumnSpan(this.passedCounter_label, 4);
            this.passedCounter_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.passedCounter_label.Location = new System.Drawing.Point(3, 251);
            this.passedCounter_label.Name = "passedCounter_label";
            this.passedCounter_label.Size = new System.Drawing.Size(310, 20);
            this.passedCounter_label.TabIndex = 4;
            // 
            // fileText_richTextBox
            // 
            this.fileText_richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileText_richTextBox.Location = new System.Drawing.Point(319, 3);
            this.fileText_richTextBox.Name = "fileText_richTextBox";
            this.resultWindow_tableLayoutPanel.SetRowSpan(this.fileText_richTextBox, 4);
            this.fileText_richTextBox.Size = new System.Drawing.Size(317, 330);
            this.fileText_richTextBox.TabIndex = 1;
            this.fileText_richTextBox.Text = "";
            // 
            // ResultWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resultWindow_tableLayoutPanel);
            this.Name = "ResultWindow";
            this.Size = new System.Drawing.Size(639, 336);
            this.resultWindow_tableLayoutPanel.ResumeLayout(false);
            this.resultWindow_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel resultWindow_tableLayoutPanel;
        private System.Windows.Forms.RichTextBox fileText_richTextBox;
        private System.Windows.Forms.Button selectAll_button;
        private System.Windows.Forms.Button deselectAll_button;
        private System.Windows.Forms.ListView allEntries_listView;
        private System.Windows.Forms.Button checkSelected_button;
        private System.Windows.Forms.Button uncheckSelected_button;
        private System.Windows.Forms.ColumnHeader Вхождение;
        private System.Windows.Forms.Label selectedCount_label;
        private System.Windows.Forms.Label passedCounter_label;
    }
}
