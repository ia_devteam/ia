﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FileOperations;
using DocExplorer;

using IA.Extensions;

namespace IA.Plugins.Analyses.TaskSeparatorWord
{
    /// <summary>
    /// Класс окна с результатами плагина
    /// </summary>
    public partial class ResultWindow : UserControl
    {
        /// <summary>
        /// Работаем с несколькими выделениями?
        /// </summary>
        bool ProcessingAPack;

        /// <summary>
        /// Отрисовка при загрузке? 
        /// </summary>
        bool starting;

        /// <summary>
        /// Конструктор
        /// </summary>
        public ResultWindow()
        {
            InitializeComponent();
            ProcessingAPack = true;
            starting = true;

            allEntries_listView.Columns[0].Width = allEntries_listView.Width;

            foreach (Docs docsInfo in TaskSeparatorWord.Documents)
                if (docsInfo.docs.Count > 0)
                    foreach (Doc doc in docsInfo.docs)
                    {
                        ListViewItem item = new ListViewItem(doc.ToString())
                        {
                            Checked = doc.Real,
                            Tag = doc
                        };
                        allEntries_listView.Items.Add(item);
                    }
        }

        /// <summary>
        /// Обработчик - выбрали элемент в списке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void allEntries_listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            starting = false;
            ProcessingAPack = false;
            if (allEntries_listView.SelectedItems != null && allEntries_listView.SelectedItems.Count == 1 && allEntries_listView.SelectedItems[0] != null)
            {
                AbstractReader sr = null;
                try
                {
                    sr = new AbstractReader(((Doc)(allEntries_listView.SelectedItems[0].Tag)).FullFileName);
                }
                catch (Exception ex)
                {
                    //Формируем сообщение
                    string message = new string[]
                    {
                        "Не удалось открыть файл " + ((Doc)(allEntries_listView.SelectedItems[0].Tag)).FullFileName + ".",
                        ex.ToFullMultiLineMessage()
                    }.ToMultiLineString();

                    IA.Monitor.Log.Error(PluginSettings.Name, message);
                }

                if ((int)((Doc)(allEntries_listView.SelectedItems[0].Tag)).Offset + 1000 < sr.getNormalizedText().Length)
                    fileText_richTextBox.Text = sr.getNormalizedText().Substring(0, (int)((Doc)(allEntries_listView.SelectedItems[0].Tag)).Offset + 1000);
                else
                    fileText_richTextBox.Text = sr.getNormalizedText();

                fileText_richTextBox.SelectAll();
                fileText_richTextBox.SelectionBackColor = Color.White;
                fileText_richTextBox.SelectionColor = Color.Black;

                fileText_richTextBox.Select((int)((Doc)(allEntries_listView.SelectedItems[0].Tag)).Offset, ((Doc)(allEntries_listView.SelectedItems[0].Tag)).Identifiers[((Doc)(allEntries_listView.SelectedItems[0].Tag)).Identifiers.Length-1].Length);
                fileText_richTextBox.ScrollToCaret();
                fileText_richTextBox.SelectionBackColor = Color.Black;
                fileText_richTextBox.SelectionColor = Color.White;
            }

            if (allEntries_listView.SelectedIndices.Count != 1)
                passedCounter_label.Text = "";
            else
                passedCounter_label.Text = "Просмотрено " + allEntries_listView.SelectedIndices[0].ToString() + " из " + allEntries_listView.Items.Count.ToString();
        }

        /// <summary>
        /// Кнопка - выбрать всё
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectAll_button_Click(object sender, EventArgs e)
        {
            starting = false;
            ProcessingAPack = true;

            for (int i = 0; i < allEntries_listView.Items.Count; ++i)
                allEntries_listView.Items[i].Checked = true;

            allEntries_listView.Focus();
            ProcessingAPack = false;
            selectedCount_label.Text = allEntries_listView.CheckedItems.Count + " элементов выбрано. ";
        }

        /// <summary>
        /// Кнопка - снять всё
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deselectAll_button_Click(object sender, EventArgs e)
        {
            starting = false;
            ProcessingAPack = true;
            for (int i = 0; i < allEntries_listView.Items.Count; ++i)
                allEntries_listView.Items[i].Checked = false;

            int j = 0;
            foreach (Docs docsInfo in TaskSeparatorWord.Documents)
                if (docsInfo.docs.Count > 0)
                    foreach (Doc doc in docsInfo.docs)
                        if (doc.Real)
                            ++j;

            allEntries_listView.Focus();
            ProcessingAPack = false;
            selectedCount_label.Text = allEntries_listView.CheckedItems.Count + " элементов выбрано. ";
        }

        /// <summary>
        /// Обработчик - отметили элемент в списке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void allEntries_listView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!starting)
            {
                ((Doc)(e.Item.Tag)).Real = e.Item.Checked;
                if (!ProcessingAPack)
                    selectedCount_label.Text = allEntries_listView.CheckedItems.Count + " элементов выбрано. ";
            }
        }

        /// <summary>
        /// Обработчик - отметить выделенные
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkSelected_button_Click(object sender, EventArgs e)
        {
            starting = false;
            ProcessingAPack = true;
            foreach (ListViewItem item in allEntries_listView.SelectedItems)
            {
                item.Checked = true;
                ((Doc)(item.Tag)).Real = true;
            }
            allEntries_listView.Focus();

            ProcessingAPack = false;
            selectedCount_label.Text = allEntries_listView.CheckedItems.Count + " элементов выбрано. ";
        }

        /// <summary>
        /// Обработчик - снять выделенные
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uncheckSelected_button_Click(object sender, EventArgs e)
        {
            starting = false;
            ProcessingAPack = true;
            foreach (ListViewItem item in allEntries_listView.SelectedItems)
            {
                item.Checked = false;
                ((Doc)(item.Tag)).Real = false;
            }
            allEntries_listView.Focus();
            ProcessingAPack = false;
            selectedCount_label.Text = allEntries_listView.CheckedItems.Count + " элементов выбрано. ";
        }

        /// <summary>
        /// Обработчик - изменили размер списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void allEntries_listView_Resize(object sender, EventArgs e)
        {
            if (!starting)
            {
                allEntries_listView.Columns[0].Width = allEntries_listView.Width;
            }
        }
    }
}
