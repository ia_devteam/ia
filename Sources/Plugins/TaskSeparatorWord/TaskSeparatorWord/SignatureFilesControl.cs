﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using IOController;

namespace IA.Plugins.Analyses.TaskSeparatorWord
{
    /// <summary>
    /// Контрол - работа со списком файлов с сигнатурами
    /// </summary>
    public partial class SignatureFilesControl : UserControl
    {
        /// <summary>
        /// Список файлов с сигнатурами
        /// </summary>
        internal List<string> SignatureFiles
        {
            get
            {
                return this.signatureFiles_listBox.Items.Cast<string>().ToList();
            }
            set
            {
                signatureFiles_listBox.Items.Clear();
                signatureFiles_listBox.Items.AddRange(value.ToArray());
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public SignatureFilesControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Кнопка - Добавить файлы с сигнатурами из папки в список
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addFolder_button_Click(object sender, EventArgs e)
        {
            string path = DirectoryController.SelectByUser();

            if (path == null)
                return;

            signatureFiles_listBox.Items.AddRange(System.IO.Directory.EnumerateFiles(path, "*", System.IO.SearchOption.AllDirectories).ToArray<string>());
        }

        /// <summary>
        /// Кнопка - Добавить файл с сигнатурами в список
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addFile_button_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;

                signatureFiles_listBox.Items.Add(ofd.FileName);
            }
        }

        /// <summary>
        /// Кнопка - Удалить файл с сигнатурами из списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void delete_button_Click(object sender, EventArgs e)
        {
            if (signatureFiles_listBox.SelectedItems == null || signatureFiles_listBox.SelectedItems.Count == 0)
                return;

            foreach (object item in new List<object>(signatureFiles_listBox.SelectedItems.Cast<object>()))
                signatureFiles_listBox.Items.Remove(item);
        }

        /// <summary>
        /// Обработчик - активность кнопки удаления файла из спсика при выборе элемента в списке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void signatureFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            delete_button.Enabled = signatureFiles_listBox.SelectedItems != null && signatureFiles_listBox.SelectedItems.Count > 0;
        }
    }
}
