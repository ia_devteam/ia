using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using DocExplorer;
using Store;
using Store.Table;

using IA.Extensions;
using System.Collections.Specialized;
using FileOperations;

namespace IA.Plugins.Analyses.TaskSeparatorWord
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.TASK_SEPARATOR_WORD;

        /// <summary>
        /// Имя плагина
        /// </summary>
        internal const string Name = "Сигнатурный анализ";

        #region Settings

        /// <summary>
        /// Путь к шаблону документа задаётся по умолчанию?
        /// </summary>
        internal static bool IsDefault_TemplateFile;

        /// <summary>
        /// Вставлять фрагменты кода в отчеты
        /// </summary>
        internal static bool IsInstertLines;

        /// <summary>
        /// Файл шаблона документа
        /// </summary>
        internal static string TemplateFile;

        /// <summary>
        /// файлы с сигнатурами
        /// </summary>
        internal static StringCollection InputFilesCollection;

        /// <summary>
        /// Файлы с сигнатурами
        /// </summary>
        internal static List<string> InputFiles
        {
            get
            {
                List<string> ret = new List<string>();

                if (PluginSettings.InputFilesCollection == null)
                    return ret;

                ret.AddRange(PluginSettings.InputFilesCollection.Cast<string>());

                return ret;
            }
            set
            {
                PluginSettings.InputFilesCollection = new StringCollection();

                if (value != null)
                    PluginSettings.InputFilesCollection.AddRange(value.ToArray());
            }
        }

        #endregion

        /// <summary>
        /// Задачи плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка файлов с сигнатурами")]
            CONFIG_FILES_PROCESSING
        }

        /// <summary>
        /// Задачи отчетов плагина
        /// </summary>
        internal enum TasksReports
        {
            [Description("Генерация файлов")]
            FILES_GENERATING
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public class TaskSeparatorWord : IA.Plugin.Interface
    {
        /// <summary>
        /// Постфикс пути до файла шаблона документа
        /// </summary>
        internal const string TemplateFilePostfix = @"Common\Normal1.dot";

        class Elem
        {
            public int part;
            public string line;
            public Elem(int Part, string Line)
            {
                part = Part;
                line = Line;
            }
        }
        List<Elem> dic;
        int m = 0;

        /// <summary>
        /// Документы для отчета
        /// </summary>
		public static List<Docs> Documents = new List<Docs>();
        
        /// <summary>
        /// Текущие настройки
        /// </summary>
        IA.Plugin.Settings settings;   
     
        /// <summary>
        /// Текущее хранилище
        /// </summary>
        Storage storage;


        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return  Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW |
                            Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | 
                                Plugin.Capabilities.RESULT_WINDOW | 
                                    Plugin.Capabilities.REPORTS | 
                                        Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        ulong docCount = 0;
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            // Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            //генерация текстовых файлов с описанием результатов
            GenerateProtocol(reportsDirectoryPath);
            
            // формируем содержимое тасков
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReports.FILES_GENERATING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)Documents.Count);


            ////обработка идентификаторов и формирование файлов с протоколами
            for (int i = 0; i < Documents.Count; ++i)
            {
                DocClass.GenerateDocs(Documents[i], i, reportsDirectoryPath, Documents, PluginSettings.TemplateFile, PluginSettings.IsInstertLines);
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReports.FILES_GENERATING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)i + 1);
            }

            foreach (string file in Directory.EnumerateFiles(reportsDirectoryPath, "~*"))
                File.Delete(file);
        }

        /// <summary>
        /// Запуск генерации протоколов
        /// </summary>
        /// <param name="reportsPath">Путь, по которому надо размещать файлы с протоколами</param>
        /// <returns></returns>
        private void GenerateProtocol(string reportsPath)
        {
            docCount = 0;        
            // короткий протокол
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportsPath, "protocol.txt")))
            {
                writer.WriteLine("Всего сигнатур: " + Documents.Count.ToString());
                int n = 0, m = 0;

                int ok;
                foreach (Docs docs in Documents)
                {
                    ok = 0;
                    foreach (Doc doc in docs.docs)
                        if (doc.Real)
                        {
                            ok = 1;
                            ++docCount;
                        }
                        else
                        {
                            if (ok != 1)
                                ok = 2;
                        }
                    if (ok == 1)
                        ++n;
                    if (ok == 2)
                        ++m;
                }
                writer.WriteLine("Из них были выбраны для дальнейшего анализа:" + n.ToString());
                writer.WriteLine("Из них не были выбраны для дальнейшего анализа:" + m.ToString());
                writer.WriteLine("Из них не найдены:" + (Documents.Count - m - n).ToString());
                writer.WriteLine("Всего сгенерировано файлов:" + docCount.ToString());
                writer.WriteLine("");
                writer.WriteLine("Файлы по части и идентификатору:");
                foreach (Docs docs in Documents)
                {
                    n = 0;
                    foreach (Doc doc in docs.docs)
                        if (doc.Real)
                            ++n;
                    writer.WriteLine("\t Часть " + docs.part + ", Идентификатор " + docs.ToString() + ". Раз встречается: " + n.ToString());
                }
            }
            //полный протокол
            using (StreamWriter sw = new StreamWriter(Path.Combine(reportsPath, "protocolFull.txt")))
            {
                sw.WriteLine("Обнаружены сигнатуры:");
                for (int i = 0; i < Documents.Count; ++i)
                {
                    if (Documents[i].docs.Count != 0 && Documents[i].isReal())
                    {
                        sw.WriteLine(Documents[i].ToString());
                    }
                }
                sw.WriteLine("");
                for (int i = 0; i < Documents.Count; ++i)
                {
                    if (Documents[i].docs.Count != 0)
                    {
                        bool ok = true;
                        for (int j = 0; j < Documents[i].docs.Count; ++j)
                        {
                            if (Documents[i].docs[j].Real)
                            {
                                if (ok)
                                {
                                    ok = false;
                                    sw.WriteLine(Documents[i].ToString());
                                }
                                sw.WriteLine("\t" + Documents[i].docs[j].FileName + ", " + Documents[i].docs[j].line.ToString());
                            }
                        }
                    }
                }
            }

            if (PluginSettings.IsInstertLines)
            {
                //полный протокол со строками
                using (StreamWriter sw = new StreamWriter(Path.Combine(reportsPath, "protocolFullWithLines.txt")))
                {
                    sw.WriteLine("Обнаружены сигнатуры:");
                    for (int i = 0; i < Documents.Count; ++i)
                    {
                        if (Documents[i].docs.Count != 0 && Documents[i].isReal())
                        {
                            sw.WriteLine(Documents[i].ToString());
                        }
                    }
                    sw.WriteLine("");
                    sw.WriteLine("Полный протокол");
                    for (int i = 0; i < Documents.Count; ++i)
                    {
                        if (Documents[i].docs.Count != 0)
                        {
                            bool ok = true;
                            for (int j = 0; j < Documents[i].docs.Count; ++j)
                            {
                                if (Documents[i].docs[j].Real)
                                {
                                    if (ok)
                                    {
                                        ok = false;
                                        sw.WriteLine("\t\tСигнатура " + Documents[i].ToString());
                                    }
                                    sw.WriteLine("\tФайл " + Documents[i].docs[j].FileName + ", " + Documents[i].docs[j].line.ToString());
                                    sw.WriteLine("Фрагмент файла:");
                                    foreach (string line in Documents[i].docs[j].Lines)
                                    {
                                        if (line != null)
                                            sw.WriteLine(line);
                                    }
                                    sw.WriteLine("");
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;
            PluginSettings.IsDefault_TemplateFile = Properties.Settings.Default.IsDefault_TemplateFile;
            PluginSettings.IsInstertLines = Properties.Settings.Default.IsInstertLines;
            PluginSettings.TemplateFile = Properties.Settings.Default.TemplateFile;
            PluginSettings.InputFilesCollection = Properties.Settings.Default.InputFilesCollection;
            #region Получаем результаты из хранилища
            try
            {
                Store.Table.IBufferReader ibr = storage.pluginResults.LoadResult(PluginSettings.ID);

                if (ibr != null)
                {
                    Documents = new List<Docs>();
                    int n = ibr.GetInt32();
                    for (int i = 0; i < n; ++i)
                    {
                        int part = ibr.GetInt32();
                        int q = ibr.GetInt32();
                        string[] signs = new string[q];
                        for(int k = 0; k < q; ++k)
                        {
                            signs[k] = ibr.GetString();
                        }
                        Docs newDocs = new Docs(part, signs);
                        newDocs.FileWithDescription = ibr.GetString();
                        int nDocs = ibr.GetInt32();
                        for (int j = 0; j < nDocs; ++j)
                        {
                            int ln = ibr.GetInt32();
                            ulong off = ibr.GetUInt64();
                            string fName = ibr.GetString();
                            string fullfName = ibr.GetString();
                            bool real = true; ibr.GetBool(ref real);
                            int nIdents = ibr.GetInt32();
                            string[] Idents = new string[nIdents];
                            for (int k = 0; k < nIdents; ++k)
                            {
                                Idents[k] = ibr.GetString();
                            }
                            List<string> Lines = new List<string>();
                            for (int k = 0; k < 3; ++k)
                            {
                                Lines.Add(ibr.GetString());
                            }

                            Doc newDoc = new Doc(real, fName, fullfName, off, ln, Idents, Lines);

                            newDocs.docs.Add(newDoc);
                        }
                        Documents.Add(newDocs);
                    }
                }
            }
            catch { }
            #endregion
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.IsDefault_TemplateFile);
            PluginSettings.TemplateFile = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.IsInstertLines);
            int n = settingsBuffer.GetInt32();
            PluginSettings.InputFilesCollection = new StringCollection();
            for (int i = 0; i < n; ++i)
            {
                PluginSettings.InputFilesCollection.Add(settingsBuffer.GetString());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.IsDefault_TemplateFile = true;
            PluginSettings.IsInstertLines = false;
            PluginSettings.TemplateFile = Path.Combine(Environment.CurrentDirectory, TemplateFilePostfix);

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            Documents.Clear();

            int counter = 0, counterDocs = 0;
            // по результатам чтения формируем таски
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CONFIG_FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)PluginSettings.InputFiles.Count);

            foreach (string fileName in PluginSettings.InputFiles)
            {
                // загрузка файла описания структуры идентификаторов
                StreamReader sr = new StreamReader(fileName);
                string line;
                int part = 0;

                dic = new List<Elem>();

                // вычитываем сигнатуры
                while ((line = sr.ReadLine()) != null)
                {
                    line = line.Replace("::", ".");
                    if (line.StartsWith("\t"))
                    {                        
                        line = line.Substring(1);
                        part = Convert.ToInt32(line);
                    }
                    else
                    {
                        line = line.Trim(); // удалим пробелы из конца
                        if (line.Contains(" "))
                        {
                            Monitor.Log.Error(Name, "Сигнатура \"" + line + "\" содержит символ пробела и будет проигнорирована.");
                            continue;
                        }
                        if (string.IsNullOrEmpty(line))
                            continue;
                        dic.Add(new Elem(part, line));
                    }
                }
                sr.Close();

                // используем результаты индекса
                Store.FilesIndex i = storage.filesIndex;
                char[] seps = { '.' };
                for (int j = 0; j < dic.Count; ++j)
                {
                    string[] signatures = dic[j].line.Split(seps);
                    Documents.Add(new Docs(dic[j].part, signatures));

                    // проверим, что все пространства имен и имена классов встречаются в исходниках
                    bool SignatureNotFound = false;
                    foreach (string sign in signatures)
                    {
                        Store.FilesIndex.Occurrence[] occs = i.Find(sign).ToArray();
                        if (occs.Length == 0)
                        {
                            SignatureNotFound = true;
                            break;
                        }
                    }

                    //берем результат индексатора
                    Store.FilesIndex.Occurrence[] rs = i.Find(signatures[signatures.Length - 1]).ToArray();

                    // формируем структуры для всех вхождений сигнатуры
                    for (int k = 0; k < rs.Count(); ++k)
                    {
                        List<string> lines = new List<string>();
                        if (PluginSettings.IsInstertLines)
                        {
                            // собираем строки в окрестностях                            
                            AbstractReader ar = new AbstractReader(rs[k].File.FullFileName_Original);
                            if (rs[k].Line > 1)
                            {
                                lines.Add(ar.getLineN((int)rs[k].Line - 1));
                            }
                            else
                            {
                                lines.Add(null);
                            }
                            lines.Add(ar.getLineN((int)rs[k].Line));
                            if (ar.getLinesCount() > (int)rs[k].Line)
                            {
                                lines.Add(ar.getLineN((int)rs[k].Line + 1));
                            }
                            else
                            {
                                lines.Add(null);
                            }
                        }
                        Documents[counter + j].docs.Add(new Doc(rs[k],!SignatureNotFound && Check(signatures[signatures.Length - 1], rs[k]), signatures, lines));
                    }
                }
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.CONFIG_FILES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)counterDocs + 1);
                ++counterDocs;
                counter += dic.Count;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.IsDefault_TemplateFile = PluginSettings.IsDefault_TemplateFile;
            Properties.Settings.Default.IsInstertLines = PluginSettings.IsInstertLines;
            Properties.Settings.Default.TemplateFile = PluginSettings.TemplateFile;
            Properties.Settings.Default.InputFilesCollection = PluginSettings.InputFilesCollection;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.IsDefault_TemplateFile);
            settingsBuffer.Add(PluginSettings.TemplateFile);
            settingsBuffer.Add(PluginSettings.IsInstertLines);
            settingsBuffer.Add(PluginSettings.InputFiles.Count);
            foreach (string file in PluginSettings.InputFiles)
                settingsBuffer.Add(file);
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
            Store.Table.IBufferWriter ibw = Store.Table.WriterPool.Get();
            ibw.Add(Documents.Count);
            foreach (Docs docs in Documents)
            {
                ibw.Add(docs.part);
                ibw.Add(docs.identifiers.Length);
                foreach(string sign in docs.identifiers)
                {
                    ibw.Add(sign);
                }
                ibw.Add(docs.FileWithDescription);
                ibw.Add(docs.docs.Count);
                foreach (Doc doc in docs.docs)
                {
                    ibw.Add(doc.line);
                    ibw.Add(doc.Offset);
                    ibw.Add(doc.FileName);
                    ibw.Add(doc.FullFileName);
                    ibw.Add(doc.Real); 
                    ibw.Add(doc.Identifiers.Length);
                    foreach (string sign in doc.Identifiers)
                    {
                        ibw.Add(sign);
                    }
                    foreach (string line in doc.Lines)
                    {
                        ibw.Add(line);
                    }
                }
            }
            storage.pluginResults.SaveResults(PluginSettings.ID, ibw);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.CONFIG_FILES_PROCESSING.Description(), (uint)Documents.Count)
                };
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

		{
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.TasksReports.FILES_GENERATING.Description(), 0)
                };
            }
		}		
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return new ResultWindow();
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {                
                return (UserControl)(settings = new CustomSettings());
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new SimpleSettings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (PluginSettings.InputFiles.Count == 0)
            {
                message = "Файлы с сигнатурами не указаны.";
                return false;
            }

            foreach (string file in PluginSettings.InputFiles)
                if (!File.Exists(file))
                {
                    message = "Файл с сигнатурами " + file + " не найден.";
                    return false;
                }

            if (!File.Exists(PluginSettings.TemplateFile))
            {
                message = "Файл шаблона документа не найден.";
                return false;
            }

            return true;
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        #endregion

        /// <summary>
        ///проверим, является ли функция нелокальной
        /// </summary>
        /// <param name="line">имя функции</param>
        /// <param name="occ">вхождение в исходники</param>
        /// <returns></returns>
        private bool Check(string line, Store.FilesIndex.Occurrence occ)
        {
            foreach (Store.IFunction fn in storage.functions.GetFunction(line))
            {
                Location fnDefinition = fn.Definition();
                if (fnDefinition != null)
                {
                    if (fnDefinition.GetOffset() == occ.Offset)
                        return false;
                    foreach (Store.IFunctionCall call in fn.CalledBy())
                    {
                        if (call.CallLocation.GetOffset() == occ.Offset)
                        {
                            return false;
                        }
                    }
                }
            }

            foreach (Store.Variable var in storage.variables.GetVariable(line))
            {
                if (var.Definitions() != null && var.Definitions().Length > 0)
                {
                    if (var.Definitions()[0].GetOffset() == occ.Offset)
                        return false;
                    foreach (Store.Location loc in var.ValueCallPosition())
                    {
                        if (loc.GetOffset() == occ.Offset)
                        {
                            return false;
                        }
                    }
                    foreach (Store.Location loc in var.ValueGetPosition())
                    {
                        if (loc.GetOffset() == occ.Offset)
                        {
                            return false;
                        }
                    }
                    foreach (Store.Location loc in var.ValueSetPosition())
                    {
                        if (loc.GetOffset() == occ.Offset)
                        {
                            return false;
                        }
                    }
                    foreach (Store.IAssignment ass in var.Enumerate_AssignmentsFrom())
                    {
                        if (ass.Position.GetOffset() == occ.Offset)
                        {
                            return false;
                        }
                    }
                    foreach (Store.IAssignment point in var.Enumerate_PointsToFunctions())
                    {
                        if (point.Position.GetOffset() == occ.Offset)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }
    }

}
