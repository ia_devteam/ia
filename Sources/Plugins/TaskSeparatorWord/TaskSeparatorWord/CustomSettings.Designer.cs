﻿namespace IA.Plugins.Analyses.TaskSeparatorWord
{
    partial class CustomSettings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.partsFile_groupBox = new System.Windows.Forms.GroupBox();
            this.partsFile_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.partsFile_textBox = new System.Windows.Forms.TextBox();
            this.partsFile_button = new System.Windows.Forms.Button();
            this.templateFile_groupBox = new System.Windows.Forms.GroupBox();
            this.templateFile_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.templateFile_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.isDefault_templateFile_checkBox = new System.Windows.Forms.CheckBox();
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.partsFile_groupBox.SuspendLayout();
            this.partsFile_tableLayoutPanel.SuspendLayout();
            this.templateFile_groupBox.SuspendLayout();
            this.templateFile_tableLayoutPanel.SuspendLayout();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // partsFile_groupBox
            // 
            this.partsFile_groupBox.Controls.Add(this.partsFile_tableLayoutPanel);
            this.partsFile_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.partsFile_groupBox.Location = new System.Drawing.Point(3, 16);
            this.partsFile_groupBox.Name = "partsFile_groupBox";
            this.partsFile_groupBox.Size = new System.Drawing.Size(494, 48);
            this.partsFile_groupBox.TabIndex = 4;
            this.partsFile_groupBox.TabStop = false;
            // 
            // partsFile_tableLayoutPanel
            // 
            this.partsFile_tableLayoutPanel.ColumnCount = 2;
            this.partsFile_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.partsFile_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.partsFile_tableLayoutPanel.Controls.Add(this.partsFile_textBox, 0, 0);
            this.partsFile_tableLayoutPanel.Controls.Add(this.partsFile_button, 1, 0);
            this.partsFile_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.partsFile_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.partsFile_tableLayoutPanel.Name = "partsFile_tableLayoutPanel";
            this.partsFile_tableLayoutPanel.RowCount = 1;
            this.partsFile_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.partsFile_tableLayoutPanel.Size = new System.Drawing.Size(488, 29);
            this.partsFile_tableLayoutPanel.TabIndex = 0;
            // 
            // partsFile_textBox
            // 
            this.partsFile_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.partsFile_textBox.Location = new System.Drawing.Point(3, 3);
            this.partsFile_textBox.Name = "partsFile_textBox";
            this.partsFile_textBox.Size = new System.Drawing.Size(382, 20);
            this.partsFile_textBox.TabIndex = 2;
            // 
            // partsFile_button
            // 
            this.partsFile_button.Location = new System.Drawing.Point(391, 3);
            this.partsFile_button.Name = "partsFile_button";
            this.partsFile_button.Size = new System.Drawing.Size(94, 22);
            this.partsFile_button.TabIndex = 0;
            this.partsFile_button.Text = "Обзор";
            this.partsFile_button.UseVisualStyleBackColor = true;
            this.partsFile_button.Click += new System.EventHandler(this.partsFile_button_Click);
            // 
            // templateFile_groupBox
            // 
            this.templateFile_groupBox.Controls.Add(this.templateFile_tableLayoutPanel);
            this.templateFile_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.templateFile_groupBox.Location = new System.Drawing.Point(3, 3);
            this.templateFile_groupBox.Name = "templateFile_groupBox";
            this.templateFile_groupBox.Size = new System.Drawing.Size(500, 79);
            this.templateFile_groupBox.TabIndex = 5;
            this.templateFile_groupBox.TabStop = false;
            this.templateFile_groupBox.Text = "Файл шаблона документа";
            // 
            // templateFile_tableLayoutPanel
            // 
            this.templateFile_tableLayoutPanel.ColumnCount = 1;
            this.templateFile_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.templateFile_tableLayoutPanel.Controls.Add(this.templateFile_textBox, 0, 1);
            this.templateFile_tableLayoutPanel.Controls.Add(this.isDefault_templateFile_checkBox, 0, 0);
            this.templateFile_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.templateFile_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.templateFile_tableLayoutPanel.Name = "templateFile_tableLayoutPanel";
            this.templateFile_tableLayoutPanel.RowCount = 2;
            this.templateFile_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.templateFile_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.templateFile_tableLayoutPanel.Size = new System.Drawing.Size(494, 60);
            this.templateFile_tableLayoutPanel.TabIndex = 0;
            // 
            // templateFile_textBox
            // 
            this.templateFile_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.templateFile_textBox.Location = new System.Drawing.Point(3, 33);
            this.templateFile_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.templateFile_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.templateFile_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.templateFile_textBox.Name = "templateFile_textBox";
            this.templateFile_textBox.Size = new System.Drawing.Size(488, 25);
            this.templateFile_textBox.TabIndex = 0;
            // 
            // isDefault_templateFile_checkBox
            // 
            this.isDefault_templateFile_checkBox.AutoSize = true;
            this.isDefault_templateFile_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isDefault_templateFile_checkBox.Location = new System.Drawing.Point(3, 3);
            this.isDefault_templateFile_checkBox.Name = "isDefault_templateFile_checkBox";
            this.isDefault_templateFile_checkBox.Size = new System.Drawing.Size(488, 24);
            this.isDefault_templateFile_checkBox.TabIndex = 1;
            this.isDefault_templateFile_checkBox.Text = "По умолчанию";
            this.isDefault_templateFile_checkBox.UseVisualStyleBackColor = true;
            this.isDefault_templateFile_checkBox.CheckedChanged += new System.EventHandler(this.isDefault_templateFile_checkBox_CheckedChanged);
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.templateFile_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 2;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(506, 235);
            this.settings_tableLayoutPanel.TabIndex = 0;
            // 
            // CustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(506, 235);
            this.partsFile_groupBox.ResumeLayout(false);
            this.partsFile_tableLayoutPanel.ResumeLayout(false);
            this.partsFile_tableLayoutPanel.PerformLayout();
            this.templateFile_groupBox.ResumeLayout(false);
            this.templateFile_tableLayoutPanel.ResumeLayout(false);
            this.templateFile_tableLayoutPanel.PerformLayout();
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel partsFile_tableLayoutPanel;
        private System.Windows.Forms.TextBox partsFile_textBox;
        private System.Windows.Forms.Button partsFile_button;
        private System.Windows.Forms.GroupBox templateFile_groupBox;
        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox partsFile_groupBox;
        private Controls.TextBoxWithDialogButton templateFile_textBox;
        private System.Windows.Forms.TableLayoutPanel templateFile_tableLayoutPanel;
        private System.Windows.Forms.CheckBox isDefault_templateFile_checkBox;


    }
}
