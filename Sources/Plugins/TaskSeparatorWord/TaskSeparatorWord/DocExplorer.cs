﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Word = Microsoft.Office.Interop.Word;
using System.Diagnostics;

namespace DocExplorer
{
    /// <summary>
    /// Класс, описывающий колонки таблицы. Список Title должен содержать 3 элемента
    /// </summary>
    public class Columns
    {
        public List<string> Title = new List<string>();
        public List<Description> Rows = new List<Description>();
    }

    /// <summary>
    /// Описание сущности
    /// </summary>
    public class Description
    {
        /// <summary>
        /// имя сущности
        /// </summary>
        public string Name = "";
        
        /// <summary>
        /// файл определения
        /// </summary>
        public string DefinitionPath = "";

        /// <summary>
        /// строка определения
        /// </summary>
        public int Line = 0;

        /// <summary>
        /// строка вызова данной функции или обращения к данной переменной
        /// </summary>
        public int Called = 0;

        public Description()
        {
        }
    }

    /// <summary>
    /// описание функции
    /// </summary>
    public class FuncDescription : Description
    {   
        /// <summary>
        /// функции, вызывающие данную
        /// </summary>
        public List<Description> Callers = new List<Description>();

        public FuncDescription(string name, string definitionPath, int line)
        {
            Name = name;
            DefinitionPath = definitionPath;
            Line = line;
        }

        public FuncDescription(string name, string definitionPath, int line, int called)
        {
            Name = name;
            DefinitionPath = definitionPath;
            Line = line;
            Called = called;
        }
    }

    /// <summary>
    /// описание переменной
    /// </summary>
    public class VariableDescription : Description
    {
        /// <summary>
        /// функции, осуществляющие присвоение данной переменной
        /// </summary>
        public List<Description> Setters = new List<Description>();

        /// <summary>
        /// функции, осуществляющие использование значение данной переменной
        /// </summary>
        public List<Description> Getters = new List<Description>();

        public VariableDescription(string name, string definitionPath, int line)
        {
            Name = name;
            DefinitionPath = definitionPath;
            Line = line;
        }

        public VariableDescription(string name, string definitionPath, int line, int called)
        {
            Name = name;
            DefinitionPath = definitionPath;
            Line = line;
            Called = called;
        }
    }

    /// <summary>
    /// класс для создания документов для невлияния или сигнатурного поиска
    /// </summary>
    public static class DocClass
    {
        /// <summary>
        /// создать документы для невлияния или сигнатурного поиска
        /// </summary>
        /// <param name="docs">описания вхождений сигнатуры</param>
        /// <param name="j">номер сигнатуры</param>
        /// <param name="path">путь вывода отчета</param>
        /// <param name="Docs">Полный список описаний вхождений сигнатур</param>
        /// <param name="templateFile">Файл шаблона</param>
        /// <param name="insertLines">Писать фрагменты кода в протокол?</param>
        public static void GenerateDocs(Docs docs, int j, string path, List<Docs> Docs, string templateFile, bool insertLines)
        {
            object missing = System.Reflection.Missing.Value;
            object SaveChanges = Word.WdSaveOptions.wdSaveChanges;              // сохраняем изменения в документе
            object NOSaveChanges = Word.WdSaveOptions.wdDoNotSaveChanges;       // не сохраняем изменения в документе

            if (docs.isReal())
            {                
                var lst1 = (from p in Process.GetProcessesByName("WINWORD") select p.Id).ToList();

                Word.Document worddocumentRez = null;
                object fileName = (Path.Combine(path, docs.part.ToString() + docs.identifiers[docs.identifiers.Length - 1] + ".doc"));
                object template1 = templateFile;
                Word.Document template;
                Word.Application wordapp1 = new Word.Application();

                var lst2 = from p in Process.GetProcessesByName("WINWORD") select p.Id;
                var pid = lst2.Where(i => !lst1.Contains(i)).ToList()[0];//(from p in lst2 where !lst1.Contains(p) select p).ToList()[0];

                template = wordapp1.Documents.Add(ref template1);
                object FileFormat = Word.WdSaveFormat.wdFormatDocument;
                template.SaveAs(ref fileName, ref FileFormat);
                worddocumentRez = template;
                try
                {
                    Word.Style StyleFind, StylePath, StyleCode, StyleUsual;
                    try
                    {
                        StyleFind = worddocumentRez.Styles["Нашел 1"];
                    }
                    catch
                    {
                        StyleFind = null;
                        IA.Monitor.Log.Warning("Генератор документов Word", "Не найден стиль \"Нашел 1\"");
                    }

                    try
                    {
                        StyleUsual = worddocumentRez.Styles["Обычный"];
                    }
                    catch
                    {
                        StyleUsual = null;
                        IA.Monitor.Log.Warning("Генератор документов Word", "Не найден стиль \"Обычный\"");
                    }

                    try
                    {
                        StyleCode = worddocumentRez.Styles["Код"];
                    }
                    catch
                    {
                        StyleCode = null;
                        IA.Monitor.Log.Warning("Генератор документов Word", "Не найден стиль \"Код\"");
                    }

                    try
                    {
                        StylePath = worddocumentRez.Styles["Путь"];
                    }
                    catch
                    {
                        StylePath = null;
                        IA.Monitor.Log.Warning("Генератор документов Word", "Не найден стиль \"Путь\"");
                    }

                    int beginCounter = 0;                    
                    foreach (Doc doc in docs.docs)
                    {
                        if (doc.Real)
                        {
                            // генерим заголовок
                            Object begin = beginCounter;
                            Word.Range wordrange = worddocumentRez.Range(ref begin, ref begin);
                            wordrange.Text += "В файле " + doc.FileName + " на строке " + doc.line + " найдено использование " + doc.Identifiers[doc.Identifiers.Length-1] + "\n";
                            int counter = wordrange.Text.Length;

                            // применяем стиль "Нашел 1" к заголовку
                            wordrange = worddocumentRez.Range(ref begin,(int)begin + wordrange.Text.Length);
                            wordrange.Select();
                            object oWordStyle = StyleFind;
                            if (StyleFind != null)
                            {
                                wordrange.set_Style(ref oWordStyle);
                            }

                            // применяем стиль "Путь" к пути до вхождения сигнатуры
                            wordrange = worddocumentRez.Range(beginCounter + 8, beginCounter + 8 + doc.FileName.Length);
                            wordrange.Select();
                            oWordStyle = StylePath;
                            if (StylePath != null)
                            {
                                wordrange.set_Style(ref oWordStyle);
                            }
                            beginCounter += counter;

                            if (insertLines)
                            {
                                // добавляем фрагмент кода
                                begin = beginCounter;
                                wordrange = worddocumentRez.Range(ref begin, ref begin);
                                foreach (string line in doc.Lines)
                                {
                                    if (line != null)
                                        wordrange.Text += line + "\n";
                                }
                                wordrange.Text += " \n";

                                beginCounter += wordrange.Text.Length;

                                // выделяем вхождение сигнатуры
                                int offset = (int)doc.Column + ((doc.Lines[0] != null)? doc.Lines[0].Length : 0) - 1;
                                wordrange = worddocumentRez.Range((int)begin + offset, (int)begin + offset + doc.Identifiers[doc.Identifiers.Count() - 1].Length);
                                wordrange.Select();
                                wordrange.Bold = 1;
                                
                                // применяем стиль "Код" к фрагменту кода
                                wordrange = worddocumentRez.Range(ref begin, (int)beginCounter);
                                wordrange.Select();
                                oWordStyle = StyleCode;
                                if (StyleCode != null)
                                {
                                    wordrange.set_Style(ref oWordStyle);
                                }

                                //сделаем следующий за кодом абзац абзацем обычного стиля
                                wordrange = worddocumentRez.Range((int)begin + wordrange.Text.Length-2, (int)begin + wordrange.Text.Length);
                                wordrange.Select();
                                oWordStyle = StyleUsual;
                                if (StyleUsual != null)
                                {
                                    wordrange.set_Style(ref oWordStyle);
                                }
                            }                            
                        }
                    }
                }

                finally
                {
                    worddocumentRez.Save();

                    //закрываем приложения. Если этого не делать то процессы WORD.exe остаются в памяти, что приводит к понятным последствиям при множественных запусках данной программки ;)
                    #region Сохранение документа и завершение работы Word
                    //закрываем приложения. Если этого не делать то процессы WORD.exe остаются в памяти, что приводит к понятным последствиям при множественных запусках данной программки ;)
                    ((Word._Application)wordapp1).Quit(ref SaveChanges);

                    //string str = @" taskkill /F /IM WINWORD.EXE /T"; //перед taskkill обязателен пробел. 
                    //ExecuteCommand(str);

                    //System.Runtime.InteropServices.Marshal.ReleaseComObject(wordapp1);
                    //System.Threading.Thread.Sleep(1000);

                    try
                    {
                        Process WordProcess = System.Diagnostics.Process.GetProcessById(pid);
                        WordProcess.Kill();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(wordapp1);
                        System.Threading.Thread.Sleep(1000);
                    }
                    catch { }
                    #endregion
                }
            }
        }

        /// <summary>
        /// Исполнение команды.
        /// </summary>
        /// <param name="str">Строка команды.</param>
        static private void ExecuteCommand(string str)
        {
            ProcessStartInfo startinfo;
            Process process = null;
            OperatingSystem os;
            string command;

            // Команда которую будет выполнять
            command = str;

            try
            {
                // Работаем только если это WinNT
                os = Environment.OSVersion;
                if (os.Platform != PlatformID.Win32NT)
                {
                    throw new PlatformNotSupportedException("Supported on Windows NT or later only");
                }
                os = null;

                // Проверка
                if (command == null || command.Trim().Length == 0)
                {
                    throw new ArgumentNullException("command");
                }

                startinfo = new ProcessStartInfo();

                // Запускаем через cmd
                startinfo.FileName = "cmd.exe";
                // Ключ /c - выполнение команды
                startinfo.Arguments = "/C " + command;

                // Не используем shellexecute
                startinfo.UseShellExecute = false;
                // Перенаправить вывод на обычную консоль
                startinfo.RedirectStandardOutput = true;
                // Не надо окон
                startinfo.CreateNoWindow = true;
                // Стартуем
                process = Process.Start(startinfo);
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка запуска программы " + str,ex);
            }
            finally
            {
                if (process != null)
                {
                    // Закрываем
                    process.Close();
                }
                // Освобождаем
                process = null;
                startinfo = null;
                GC.Collect();
            }

        }

    }



    /// <summary>
    /// описатель вхождений сигнатур
    /// </summary>
    public class Docs
    {
        /// <summary>
        /// описатели документов
        /// </summary>
        public List<Doc> docs;

        /// <summary>
        /// сигнатура
        /// </summary>
        public string[] identifiers;

        /// <summary>
        /// номер части отчета
        /// </summary>
        public int part;

        /// <summary>
        /// файл с сигнатурой
        /// </summary>
        public string FileWithDescription;

        public Docs(int Part, string[] Identifiers)
        {
            identifiers = Identifiers;
            docs = new List<Doc>();
            part = Part;
            FileWithDescription = "";
        }
        public Docs(int Part, string[] Identifiers, string fileWithDescription)
        {
            identifiers = Identifiers;
            docs = new List<Doc>();
            part = Part;
            FileWithDescription = fileWithDescription;
        }

        public bool isReal()
        {
            foreach(Doc doc in docs)
            {
                if (doc.Real)
                    return true;
            }
            return false;
        }

        override public string ToString()
        {
            string name = "";

            foreach (string ident in identifiers)
            {
                name += '.' + ident;
            }
            name = name.Substring(1);
            return name;
        }
    }

    /// <summary>
    /// описатель вхождения сигнатуры
    /// </summary>
    public class Doc
    {
        /// <summary>
        /// имя файла для отчета
        /// </summary>
        public string FileName;

        /// <summary>
        /// полное имя файла
        /// </summary>
        public string FullFileName; 

        /// <summary>
        /// строка вхождения
        /// </summary>
        public int line;

        /// <summary>
        /// подходит ли вхождение для отчета
        /// </summary>
        public bool Real;

        /// <summary>
        /// смещение вхождения относительно начала файла
        /// </summary>
        public ulong Offset;

        /// <summary>
        /// сигнатура
        /// </summary>
        public string[] Identifiers;

        /// <summary>
        /// строки до, с и после вхождения (если есть)
        /// </summary>
        public List<string> Lines;

        /// <summary>
        /// столбец, в котором нашли сигнатуру 
        /// </summary>
        public ulong Column;

        public Doc(Store.FilesIndex.Occurrence occ, string [] idetifiers, List<string> lines)
        {
            Real = true;
            FileName = occ.File.FileNameForReports;
            FullFileName = occ.File.FullFileName_Original;
            Offset = occ.Offset;
            line = (int)occ.Line;
            Identifiers = idetifiers;
            Lines = lines;
        }
        public Doc(bool real, string fileName, string fullFileName, ulong offset, int Line, string[] idetifiers, List<string> lines)
        {
            Real = real;
            FileName = fileName;
            FullFileName = fullFileName;
            Offset = offset;
            line = Line;
            Identifiers = idetifiers;
            Lines = lines;
        }

        public Doc(Store.FilesIndex.Occurrence occ, bool real, string[] idetifiers, List<string> lines)
        {
            Real = real;
            FileName = occ.File.FileNameForReports;
            FullFileName = occ.File.FullFileName_Original;
            Offset = occ.Offset;
            Column = occ.Column;
            
            line = (int)occ.Line;
            Identifiers = idetifiers;
            Lines = lines;
        }

        override public string ToString()
        {
            string name = "";

            foreach (string ident in Identifiers)
            {
                name += '.' + ident;
            }
            name = name.Substring(1);
            return "[" + name + "] " + FileName + ", " + line;
        }
    }
}
