﻿namespace IA.Plugins.Analyses.TaskSeparatorWord
{
    partial class SignatureFilesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.signatureFiles_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.addFolder_button = new System.Windows.Forms.Button();
            this.addFile_button = new System.Windows.Forms.Button();
            this.delete_button = new System.Windows.Forms.Button();
            this.signatureFiles_listBox = new System.Windows.Forms.ListBox();
            this.signatureFiles_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // signatureFiles_tableLayoutPanel
            // 
            this.signatureFiles_tableLayoutPanel.ColumnCount = 2;
            this.signatureFiles_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.signatureFiles_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103F));
            this.signatureFiles_tableLayoutPanel.Controls.Add(this.addFolder_button, 1, 0);
            this.signatureFiles_tableLayoutPanel.Controls.Add(this.addFile_button, 1, 1);
            this.signatureFiles_tableLayoutPanel.Controls.Add(this.delete_button, 1, 2);
            this.signatureFiles_tableLayoutPanel.Controls.Add(this.signatureFiles_listBox, 0, 0);
            this.signatureFiles_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.signatureFiles_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.signatureFiles_tableLayoutPanel.Name = "signatureFiles_tableLayoutPanel";
            this.signatureFiles_tableLayoutPanel.RowCount = 2;
            this.signatureFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.signatureFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.signatureFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.signatureFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.signatureFiles_tableLayoutPanel.Size = new System.Drawing.Size(592, 216);
            this.signatureFiles_tableLayoutPanel.TabIndex = 8;
            // 
            // addFolder_button
            // 
            this.addFolder_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addFolder_button.Location = new System.Drawing.Point(492, 3);
            this.addFolder_button.Name = "addFolder_button";
            this.addFolder_button.Size = new System.Drawing.Size(97, 39);
            this.addFolder_button.TabIndex = 8;
            this.addFolder_button.Text = "Добавить папку с сигнатурами";
            this.addFolder_button.UseVisualStyleBackColor = true;
            this.addFolder_button.Click += new System.EventHandler(this.addFolder_button_Click);
            // 
            // addFile_button
            // 
            this.addFile_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addFile_button.Location = new System.Drawing.Point(492, 48);
            this.addFile_button.Name = "addFile_button";
            this.addFile_button.Size = new System.Drawing.Size(97, 39);
            this.addFile_button.TabIndex = 9;
            this.addFile_button.Text = "Добавить файл с сигнатурами";
            this.addFile_button.UseVisualStyleBackColor = true;
            this.addFile_button.Click += new System.EventHandler(this.addFile_button_Click);
            // 
            // delete_button
            // 
            this.delete_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.delete_button.Location = new System.Drawing.Point(492, 93);
            this.delete_button.Name = "delete_button";
            this.delete_button.Size = new System.Drawing.Size(97, 39);
            this.delete_button.TabIndex = 10;
            this.delete_button.Text = "Удалить";
            this.delete_button.UseVisualStyleBackColor = true;
            this.delete_button.Click += new System.EventHandler(this.delete_button_Click);
            // 
            // signatureFiles_listBox
            // 
            this.signatureFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.signatureFiles_listBox.FormattingEnabled = true;
            this.signatureFiles_listBox.Location = new System.Drawing.Point(3, 3);
            this.signatureFiles_listBox.Name = "signatureFiles_listBox";
            this.signatureFiles_tableLayoutPanel.SetRowSpan(this.signatureFiles_listBox, 4);
            this.signatureFiles_listBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.signatureFiles_listBox.Size = new System.Drawing.Size(483, 210);
            this.signatureFiles_listBox.TabIndex = 11;
            // 
            // SignatureFilesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.signatureFiles_tableLayoutPanel);
            this.Name = "SignatureFilesControl";
            this.Size = new System.Drawing.Size(592, 216);
            this.signatureFiles_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel signatureFiles_tableLayoutPanel;
        private System.Windows.Forms.Button addFolder_button;
        private System.Windows.Forms.Button addFile_button;
        private System.Windows.Forms.Button delete_button;
        private System.Windows.Forms.ListBox signatureFiles_listBox;
    }
}
