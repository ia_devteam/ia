﻿using System;
using System.IO;
using System.Windows.Forms;

namespace IA.Plugins.Analyses.TaskSeparatorWord
{
    /// <summary>
    /// Класс настроек плагина
    /// </summary>
    internal partial class CustomSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Путь до файла шаблона документа
        /// </summary>
        private string templateFile = Path.Combine(Environment.CurrentDirectory, TaskSeparatorWord.TemplateFilePostfix);

        /// <summary>
        /// Элемент управления выбора файлов с сигнатурами
        /// </summary>
        private SignatureFilesControl signatureFilesControl = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        internal CustomSettings()
        {
            InitializeComponent();
            
            // Путь к файлу с шаблоном
            isDefault_templateFile_checkBox.Checked = PluginSettings.IsDefault_TemplateFile;
            templateFile_textBox.Enabled = !isDefault_templateFile_checkBox.Checked;
            templateFile_textBox.Text = PluginSettings.IsDefault_TemplateFile ? templateFile : PluginSettings.TemplateFile;

            // Инициализация элемента управления выбора файлов с сигнатурами
            signatureFilesControl = new SignatureFilesControl()
            {
                SignatureFiles = PluginSettings.InputFiles,
                Dock = DockStyle.Fill
            };

            settings_tableLayoutPanel.Controls.Add(signatureFilesControl, 1, 0);
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.IsDefault_TemplateFile = isDefault_templateFile_checkBox.Checked;
            PluginSettings.TemplateFile = templateFile_textBox.Text;
            PluginSettings.InputFiles = signatureFilesControl.SignatureFiles;
        }

        /// <summary>
        /// Кнопка выбора файла с разбиением на части
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void partsFile_button_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { FileName = partsFile_textBox.Text })
            {
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;

                partsFile_textBox.Text = ofd.FileName;
            }
        }

        /// <summary>
        /// Обработчик - Путь к шаблону документа задаётся по умолчанию?
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isDefault_templateFile_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            templateFile_textBox.Enabled = !isDefault_templateFile_checkBox.Checked;

            templateFile_textBox.Text = isDefault_templateFile_checkBox.Checked ? templateFile : String.Empty;
        }        
    }
}
