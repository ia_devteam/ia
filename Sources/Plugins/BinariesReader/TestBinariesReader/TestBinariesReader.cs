﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using TestUtils;

namespace TestBinariesReader
{
    [TestClass]
    public class TestBinariesReader
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// анализ бинарного файла .NET при помощи ilDasm
        /// </summary>
        [TestMethod]
        public void BinariesReader_DotNET()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Source\.NET\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\.NET"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// анализ нативного бинарного файла при помощи tdump 
        /// </summary>
        [TestMethod]
        public void BinariesReader_Native()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Source\Native"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\Native"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }
                
        /// <summary>
        /// анализ файла драйвера при помощи DumpBin 
        /// </summary>
        [TestMethod]
        public void BinariesReader_Dumpbin()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinariesReader\DumpBin\Source"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\DumpBin\Res"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        ///  тест обработки elf-файла с декорированием экспортируемых и иммпортируемых функций
        /// </summary>
        [TestMethod]
        public void BinariesReader_Elf2()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Source\NoBinaries"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\Elf2"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        ///  тест без бинарных файлов на входе
        /// </summary>
        [TestMethod]
        public void BinariesReader_NoBinaries()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinariesReader\NoBinaries\Source\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\NoBinaries\Res"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест на бинарном файле без сегментов кода
        /// </summary>
        [TestMethod]
        public void BinariesReader_NoCode()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Source\NoCode"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\NoCode"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        ///  тест обработки elf-файла без декорирования экспортируемых и иммпортируемых функций
        /// </summary>
        [TestMethod]
        public void BinariesReader_Elf()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Source\Elf"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\Elf"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест на файле с расширением exe, но без исполнимого кода
        /// </summary>
        [TestMethod]
        public void BinariesReader_ErrorExe()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Source\ErrorExe"));
                    Store.IFile file = storage.files.Find(Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Source\ErrorExe\1.exe"));
                    file.fileType = Store.SpecialFileTypes.EXE_FILE;
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\ErrorExe"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест на хранилище, в которое не загружены файлы
        /// </summary>
        [TestMethod]
        public void BinariesReader_NoFiles()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) => { },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\NoFiles"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест на полном наборе файлов для проверки работы на папке с вложенными подпапками
        /// </summary>
        [TestMethod]
        public void BinariesReader_Common()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.BinariesReader.BinariesReader(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"BinaryRightsChecker\Source"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },

                // checkStorage
                (storage, testMaterialsPath) => { return true; },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"BinariesReader\Common"), reportsPath);
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }
    }
}
