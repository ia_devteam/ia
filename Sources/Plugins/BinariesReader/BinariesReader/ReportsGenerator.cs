﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace IA.Plugins.Analyses.BinariesReader
{
    /// <summary>
    /// Класс, реализующий логику работы плагина при генерации отчётов
    /// </summary>
    internal class ReportsGenerator
    {
        /// <summary>
        /// Путь до директории для формирования отчётов
        /// </summary>
        string reportsDirectoryPath;

        /// <summary>
        /// Информация о всех бибилиотеках
        /// </summary>
        List<DllInfo> allDllInfo;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="reportsDirectoryPath">Директория для формирования отчётов</param>
        /// <param name="allDllInfo">Информация о всех библиотеках</param>
        internal ReportsGenerator(string reportsDirectoryPath, List<DllInfo> allDllInfo)
        {
            //Проверка на разумность
            if (reportsDirectoryPath == null)
                throw new Exception("Директория для формирования отчётов не определена.");

            //Проверка на разумность
            if (!Directory.Exists(reportsDirectoryPath))
                throw new Exception("Директория для формирования отчётов не существует.");

            //Проверка на разумность
            if (allDllInfo == null)
                throw new Exception("Отсутствует информация о бибилиотеках.");

            this.reportsDirectoryPath = reportsDirectoryPath;
            this.allDllInfo = allDllInfo;
        }

        /// <summary>
        /// Генерация отчетов импорта/экспорта функций.
        /// </summary>
        internal void GenerateSimple()
        {
            //список внешних бинарных файлов
            using (StreamWriter writer = new StreamWriter(Path.Combine(this.reportsDirectoryPath, "resultFunctions.il")))
            {
                //пишем результаты
                foreach (DllInfo dllInfo in allDllInfo)
                {
                    writer.WriteLine(dllInfo.FileName);

                    foreach (ImportedFunction Function in dllInfo.ImpFunctions.Where(f => f.Assembly != null))
                        writer.WriteLine("\t[" + Function.Assembly + "] " + Function.Name);
                }
            }

            using (StreamWriter writer = new StreamWriter(Path.Combine(this.reportsDirectoryPath, "exportFunctions.il")))
            {
                //пишем результаты
                foreach (DllInfo dllInfo in allDllInfo.Where(dll => !dll.IsExternal))
                {
                    writer.WriteLine(dllInfo.FileName);
                    writer.WriteLine("assembly: " + dllInfo.AssemblyName);

                    foreach (string Function in dllInfo.ExpFunctions)
                        writer.WriteLine("\t" + Function);

                    writer.WriteLine("\n");
                }
            }
        }

        /// <summary>
        /// Генерация отчетов о внешних сборках.
        /// </summary>
        internal void GenerateExternalAssemblies()
        {
            List<string> innerAssemblies = new List<string>();
            List<string> externalAssemblies = new List<string>();

            using (StreamWriter writer = new StreamWriter(Path.Combine(this.reportsDirectoryPath, "ExternalDlls1.il")))
            {
                foreach (DllInfo dllInfo in allDllInfo.Where(dll => !innerAssemblies.Contains(dll.AssemblyName) && !dll.IsExternal))
                    if (!innerAssemblies.Contains(dllInfo.AssemblyName) && !dllInfo.IsExternal)
                        innerAssemblies.Add(dllInfo.AssemblyName);

                foreach (DllInfo dllInfo in allDllInfo)
                    foreach (ImportedFunction function in dllInfo.ImpFunctions)
                        if (!innerAssemblies.Contains(function.Assembly) && !externalAssemblies.Contains(function.Assembly))
                            externalAssemblies.Add(function.Assembly);

                foreach (string dllName in externalAssemblies)
                    if (dllName != string.Empty)
                        writer.WriteLine(dllName);
            }
        }

        /// <summary>
        /// Генерация отчетов о внешних функциях.
        /// </summary>
        internal void GenerateExternalFunctions()
        {
            List<string> innerAssemblies = new List<string>();
            List<string> externalAssemblies = new List<string>();

            using (StreamWriter writer = new StreamWriter(Path.Combine(this.reportsDirectoryPath, "ExternalFunctions1.il")))
            {
                foreach (DllInfo dllInfo in allDllInfo)
                    if (!innerAssemblies.Contains(dllInfo.AssemblyName) && !dllInfo.IsExternal)
                        innerAssemblies.Add(dllInfo.AssemblyName);

                foreach (DllInfo dll in allDllInfo)
                    foreach (ImportedFunction function in dll.ImpFunctions)
                        if (function.Name != string.Empty && !innerAssemblies.Contains(function.Assembly) && !externalAssemblies.Contains(("[" + function.Assembly + "]\t" + function.Name)))
                            externalAssemblies.Add("[" + function.Assembly + "]\t" + function.Name);

                externalAssemblies.Sort();

                foreach (string dllName in externalAssemblies)
                    writer.WriteLine(dllName);
            }
        }

        /// <summary>
        /// замена  mscorelib на соответствующие пространства имен
        /// </summary>
        internal void ReviewReports()
        {
            string[] ExcludeLibs = { };

            StreamReader sr = new StreamReader(Path.Combine(this.reportsDirectoryPath, "ExternalFunctions1.il"));
            StreamReader sr2 = new StreamReader(Path.Combine(this.reportsDirectoryPath, "ExternalDlls1.il"));
            StreamWriter sw1 = new StreamWriter(Path.Combine(this.reportsDirectoryPath, "ExternalDlls.il"), false);
            StreamWriter sw2 = new StreamWriter(Path.Combine(this.reportsDirectoryPath, "ExternalFunctions.il"), false);

            string line, libName, subline;
            List<string> libs = new List<string>();
            while ((line = sr.ReadLine()) != null)
            {
                if (line.Contains(",class"))
                    continue;
                if (line.StartsWith("[mscorlib]	"))
                {
                    line = line.Substring(11);
                    if (line.Contains("<"))
                    {
                        subline = line.Substring(0, line.IndexOf("<"));
                    }
                    else
                    {
                        subline = line;
                    }
                    if (subline.Contains(".ctor"))
                    {
                        subline = subline.Substring(0, line.IndexOf(".ctor"));
                    }
                    libName = subline.Substring(0, subline.LastIndexOf("."));
                    line = line.Substring(libName.Length + 1);
                    sw2.WriteLine("[" + libName + "]\t" + libName + "." + line);
                    if (!libs.Contains(libName))
                    {
                        bool ok = true;
                        foreach (string lb in ExcludeLibs)
                        {
                            if (libName.Contains(lb))
                                ok = false;
                        }
                        if (ok)
                        {
                            libs.Add(libName);
                        }
                    }
                }
                else
                {
                    bool ok = true;
                    foreach(string lb in ExcludeLibs)
                    {
                        if (line.Contains(lb))
                            ok = false;
                    }
                    if (ok)
                    {
                        sw2.WriteLine(line);
                    }
                }
            }

            while ((line = sr2.ReadLine()) != null)
            {
                bool ok = true;
                if (line != "mscorlib" && !libs.Contains("line"))
                {
                    foreach (string lb in ExcludeLibs)
                    {
                        if (line.Contains(lb))
                            ok = false;
                    }
                    if (ok)
                    {
                        libs.Add(line);
                    }
                }
            }
            foreach (string lib in libs)
            {
                sw1.WriteLine(lib);
            }


            sw1.Close();
            sw2.Close();
            sr.Close();
            sr2.Close();

            File.Delete(Path.Combine(this.reportsDirectoryPath, "ExternalFunctions1.il"));
            File.Delete(Path.Combine(this.reportsDirectoryPath, "ExternalDlls1.il"));
        }
    }
}

