﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace IA.Plugins.Analyses.BinariesReader
{
    internal partial class ResultWindow : UserControl
    {
        [DllImport("ElfDemangle.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        extern static public int Demangle(string mangled, int options, StringBuilder sb, int len);

        List<string> reserve = new List<string>();

        /// <summary>
        /// Информация о библиотеках
        /// </summary>
        List<DllInfo> allDllInfo = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="allDllInfo">Информация о всех библиотеках</param>
        internal ResultWindow(List<DllInfo> allDllInfo)
        {
            this.allDllInfo = allDllInfo;

            InitializeComponent();

            foreach (DllInfo dllInfo in allDllInfo.Where(dll => dll.IsElf))
                foreach(ImportedFunction importedFunction in dllInfo.ImpFunctions)
                {
                    StringBuilder stringBuilder = new StringBuilder(8000);

                    if (Demangle(importedFunction.Name, SetOptions(), stringBuilder, 8000) == 0)
                        importedFunction.Name = stringBuilder.ToString();

                    functions_listBox.Items.Add(importedFunction);
                    reserve.Add(importedFunction.Name);
                }
            
            decorations_listBox.Items.AddRange(Enum.GetNames(typeof(BinariesReader.enOptions)));
        }

        /// <summary>
        /// Установка маски в зависимости от выделенных элементов списка
        /// </summary>
        /// <returns></returns>
        private int SetOptions()
        {
            int mask = 0;

            foreach (string item in decorations_listBox.SelectedItems)
                mask |= (int)(BinariesReader.enOptions)Enum.Parse(typeof(BinariesReader.enOptions), item);

            return mask;
        }

        /// <summary>
        /// Обработчик - Выбрали элемент списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void decorationsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            functions_listBox.Items.Clear();

            int i = 0;
            foreach (DllInfo dllInfo in this.allDllInfo.Where(dll => dll.IsElf))
                foreach (ImportedFunction importedFunction in dllInfo.ImpFunctions)
                {
                    StringBuilder stringBuilder = new StringBuilder(8000);
                    if (Demangle(reserve[i], SetOptions(), stringBuilder, 8000) == 0)
                        importedFunction.Name = stringBuilder.ToString();

                    functions_listBox.Items.Add(importedFunction);
                    ++i;
                }
        }
    }
}
