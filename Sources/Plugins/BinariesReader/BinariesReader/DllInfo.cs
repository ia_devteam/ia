﻿using System.Collections.Generic;

namespace IA.Plugins.Analyses.BinariesReader
{
    /// <summary>
    /// Класс информации о библиотеке.
    /// </summary>
    internal class DllInfo
    {
        /// <summary>
        /// Имя библиотеки.
        /// </summary>
        internal string FileName { get; set; }

        /// <summary>
        /// Полное имя библиотеки.
        /// </summary>
        internal string FullFileName { get; set; }

        /// <summary>
        /// Имя сборки.
        /// </summary>
        internal string AssemblyName { get; set; }

        /// <summary>
        /// Внешняя ли библиотека?
        /// </summary>
        internal bool IsExternal { get; set; }

        /// <summary>
        /// Elf-файл?
        /// </summary>
        internal bool IsElf { get; set; }
        
        #region Список внешних функций

        /// <summary>
        /// Список внешних функций.
        /// </summary>
        List<string> expFunctions;

        /// <summary>
        /// Список внешних функций.
        /// </summary>
        internal List<string> ExpFunctions
        {
            get
            {
                return expFunctions ?? (expFunctions = new List<string>());
            }
            set
            {
                expFunctions = value;
            }
        }

        #endregion

        #region Список импортируемых функций

        /// <summary>
        /// Список импортируемых функций.
        /// </summary>
        List<ImportedFunction> impFunctions;

        /// <summary>
        /// Список импортируемых функций.
        /// </summary>
        internal List<ImportedFunction> ImpFunctions
        {
            get
            {
                return impFunctions ?? (impFunctions = new List<ImportedFunction>());
            }
            set
            {
                impFunctions = value;
            }
        }

        #endregion
    }

    /// <summary>
    /// Класс экспортируемой функции.
    /// </summary>
    internal class ExportedFunction
    {
        /// <summary>
        /// Имя функции.
        /// </summary>
        internal string Name { get; private set; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="name">Имя функции.</param>
        internal ExportedFunction(string name)
        {
            this.Name = name;
        }


    }

    /// <summary>
    /// Класс импортируемой функции.
    /// </summary>
    internal class ImportedFunction
    {
        /// <summary>
        /// Имя сборки.
        /// </summary>
        internal string Assembly { get; private set; }

        /// <summary>
        /// Имя функции.
        /// </summary>
        internal string Name { get; set; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="assembly">Имя сборки.</param>
        /// <param name="name">Имя функции.</param>
        internal ImportedFunction(string assembly, string name)
        {
            this.Assembly = assembly;
            this.Name = name;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
