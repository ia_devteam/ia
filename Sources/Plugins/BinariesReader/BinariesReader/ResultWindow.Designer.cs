﻿namespace IA.Plugins.Analyses.BinariesReader
{
    partial class ResultWindow
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tabeLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.functions_listBox = new System.Windows.Forms.ListBox();
            this.decorations_listBox = new System.Windows.Forms.ListBox();
            this.main_tabeLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tabeLayoutPanel
            // 
            this.main_tabeLayoutPanel.ColumnCount = 2;
            this.main_tabeLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_tabeLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_tabeLayoutPanel.Controls.Add(this.functions_listBox, 0, 0);
            this.main_tabeLayoutPanel.Controls.Add(this.decorations_listBox, 1, 0);
            this.main_tabeLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tabeLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tabeLayoutPanel.Name = "main_tabeLayoutPanel";
            this.main_tabeLayoutPanel.RowCount = 1;
            this.main_tabeLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_tabeLayoutPanel.Size = new System.Drawing.Size(511, 329);
            this.main_tabeLayoutPanel.TabIndex = 0;
            // 
            // functions_listBox
            // 
            this.functions_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.functions_listBox.FormattingEnabled = true;
            this.functions_listBox.Location = new System.Drawing.Point(3, 3);
            this.functions_listBox.Name = "functions_listBox";
            this.functions_listBox.Size = new System.Drawing.Size(249, 323);
            this.functions_listBox.TabIndex = 0;
            // 
            // decorations_listBox
            // 
            this.decorations_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.decorations_listBox.FormattingEnabled = true;
            this.decorations_listBox.Location = new System.Drawing.Point(258, 3);
            this.decorations_listBox.Name = "decorations_listBox";
            this.decorations_listBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.decorations_listBox.Size = new System.Drawing.Size(250, 323);
            this.decorations_listBox.TabIndex = 1;
            this.decorations_listBox.SelectedIndexChanged += new System.EventHandler(this.decorationsListBox_SelectedIndexChanged);
            // 
            // ResultWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tabeLayoutPanel);
            this.Name = "ResultWindow";
            this.Size = new System.Drawing.Size(511, 329);
            this.main_tabeLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tabeLayoutPanel;
        public System.Windows.Forms.ListBox functions_listBox;
        private System.Windows.Forms.ListBox decorations_listBox;
    }
}
