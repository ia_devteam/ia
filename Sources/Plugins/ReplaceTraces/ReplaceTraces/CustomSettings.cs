﻿using System.Windows.Forms;

namespace IA.Plugins.Utils.ReplaceTraces
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        internal Settings()
        {
            InitializeComponent();

            oldReportsDirectoryPath_textBox.Text = PluginSettings.OldReportsDirectoryPath;
            newReportsDirectoryPath_textBox.Text = PluginSettings.NewReportsDirectoryPath;
            sourceTracesDirectoryPath_textBox.Text = PluginSettings.SourceTracesDirectoryPath;
            resultTracesDirectoryPath_textBox.Text = PluginSettings.ResultTracesDirectoryPath;
            checkBoxCorrPos.Checked = PluginSettings.CorrectPos;

        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.OldReportsDirectoryPath = oldReportsDirectoryPath_textBox.Text;
            PluginSettings.NewReportsDirectoryPath = newReportsDirectoryPath_textBox.Text;
            PluginSettings.SourceTracesDirectoryPath = sourceTracesDirectoryPath_textBox.Text;
            PluginSettings.ResultTracesDirectoryPath = resultTracesDirectoryPath_textBox.Text;
            PluginSettings.CorrectPos = checkBoxCorrPos.Checked;
        }

    }
}
