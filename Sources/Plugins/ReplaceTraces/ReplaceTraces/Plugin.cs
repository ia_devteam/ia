using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Utils.ReplaceTraces
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.REPLACE_TRACES;

        /// <summary>
        /// Имя плагина
        /// </summary>
        internal const string Name = "Замена трасс";

        #region Settings

        /// <summary>
        /// Папка со старыми отчетами
        /// </summary>
        internal static string OldReportsDirectoryPath;
        
        /// <summary>
        /// Папка с новыми отчетами
        /// </summary>
        internal static string NewReportsDirectoryPath;

        /// <summary>
        /// Папка с исходными трассами
        /// </summary>
        internal static string SourceTracesDirectoryPath;

        /// <summary>
        /// Папка с результирующими трассами
        /// </summary>
        internal static string ResultTracesDirectoryPath;

        internal static bool CorrectPos;

        #endregion

        /// <summary>
        /// Задачи плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Анализ отчетов")]
            REPORTS_ANALIZING,
            [Description("Замена трасс")]
            TRACES_SUBSTITUTION
        }
    }

    public class ReplaceTraces : IA.Plugin.Interface
    {
        /// <summary>
        /// Текущие настройки
        /// </summary>
        Settings settings;

        SortedList<int, int> dReplace = new SortedList<int, int>();
        List<string> dConflictsOld = new List<string>();
        List<string> dConflictsNew = new List<string>();        

        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW | Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS | Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
		}

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            File.WriteAllLines(Path.Combine(reportsDirectoryPath, "Замена датчиков.txt"), dReplace.Select(x => x.Key + "-" + x.Value).ToArray());
            File.WriteAllLines(Path.Combine(reportsDirectoryPath, "Повторы функций в старых отчетах.txt"), dConflictsOld);
            File.WriteAllLines(Path.Combine(reportsDirectoryPath, "Повторы функций в новых отчетах.txt"), dConflictsNew);
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            PluginSettings.OldReportsDirectoryPath = Properties.Settings.Default.OldReportsDirectoryPath;
            PluginSettings.NewReportsDirectoryPath = Properties.Settings.Default.NewReportsDirectoryPath;
            PluginSettings.SourceTracesDirectoryPath = Properties.Settings.Default.SourceTracesDirectoryPath;
            PluginSettings.ResultTracesDirectoryPath = Properties.Settings.Default.ResultTracesDirectoryPath;
            PluginSettings.CorrectPos = Properties.Settings.Default.CorrectPos;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.OldReportsDirectoryPath = settingsBuffer.GetString();
            PluginSettings.NewReportsDirectoryPath = settingsBuffer.GetString();
            PluginSettings.SourceTracesDirectoryPath = settingsBuffer.GetString();
            PluginSettings.ResultTracesDirectoryPath = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.CorrectPos);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            SortedList<string, int> dOld = new SortedList<string, int>();
            SortedList<string, int> dNew = new SortedList<string, int>();
            dReplace.Clear();
            dConflictsOld.Clear();
            dConflictsNew.Clear();

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.REPORTS_ANALIZING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)((new DirectoryInfo(PluginSettings.OldReportsDirectoryPath)).GetFiles("*.*", SearchOption.AllDirectories).Length + (new DirectoryInfo(PluginSettings.NewReportsDirectoryPath)).GetFiles("*.*", SearchOption.AllDirectories).Length));
            int CntReports = 0;
            int specCorrPos = 0;
            foreach (FileInfo file in (new DirectoryInfo(PluginSettings.OldReportsDirectoryPath)).GetFiles("*.*", SearchOption.AllDirectories))
            {
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(file.FullName);
                    while (true)
                    {
                        string s = sr.ReadLine();
                        if (s == null)
                            break;
                        string[] ssplit = s.ToLower().Split('\t');
                        if (ssplit.Length < 6)
                            continue;
                        if (ssplit[2].ToLower() == "unknownfunctionrnt")
                            continue;
                        string key = ssplit.Skip(2).Aggregate((a, b) => a + " " + b);
                        string val = ssplit[1];
                        int value;
                        int value1;
                        if(!int.TryParse(val,out value))
                            continue;
                        if(dOld.TryGetValue(key,out value1))
                        {
                            if (PluginSettings.CorrectPos) // у Аиста нет номеров позиций - только строки
                            {
                                key += " " + specCorrPos++;
                                if(!dOld.TryGetValue(key,out value1))
                                {
                                    dOld.Add(key,value);
                                }
                                else
                                {
                                    dConflictsOld.Add(key+" "+value);
                                }
                            }
                        }
                        else
                        {
                            specCorrPos = 0;
                            dOld.Add(key, value);
                        }
                    }
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
                CntReports++;
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.REPORTS_ANALIZING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)CntReports);

            }

            foreach (FileInfo file in (new DirectoryInfo(PluginSettings.NewReportsDirectoryPath)).GetFiles("*.*", SearchOption.AllDirectories))
            {
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(file.FullName);
                    while (true)
                    {
                        string s = sr.ReadLine();
                        if (s == null)
                            break;
                        string[] ssplit = s.ToLower().Split('\t');
                        if (ssplit.Length < 6)
                            continue;
                        if (ssplit[2].ToLower() == "unknownfunctionrnt")
                            continue;
                        string key = ssplit.Skip(2).Aggregate((a, b) => a + " " + b);
                        string val = ssplit[1];
                        int value;
                        int value1;
                        if(!int.TryParse(val,out value))
                            continue;
                        if(dNew.TryGetValue(key,out value1))
                        {
                            if (PluginSettings.CorrectPos) // у Аиста нет номеров позиций - только строки
                            {
                                key += " " + specCorrPos++;
                                if(!dNew.TryGetValue(key,out value1))
                                {
                                    dNew.Add(key,value);
                                }
                                else
                                {
                                    dConflictsNew.Add(key+" "+value);
                                }
                            }
                        }
                        else
                        {
                            specCorrPos = 0;
                            dNew.Add(key, value);
                        }
                    }
                }
                finally
                {
                    if (sr != null)
                        sr.Close();
                }
                CntReports++;
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.REPORTS_ANALIZING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)CntReports);

            }

            foreach(KeyValuePair<string,int> el in dOld)
            {
                int n1;
                if (dNew.TryGetValue(el.Key, out n1))
                    dReplace.Add(el.Value, n1);
                else
                    dReplace.Add(el.Value, 0);
            }

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.TRACES_SUBSTITUTION.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)(new DirectoryInfo(PluginSettings.SourceTracesDirectoryPath)).GetFiles("*.*", SearchOption.AllDirectories).Length);

            int CntSensors = 0;
            int nCountS = 0;
            int nCountD = 0;
            int iS;
            int iD;
            foreach (FileInfo file in (new DirectoryInfo(PluginSettings.SourceTracesDirectoryPath)).GetFiles("*.*", SearchOption.AllDirectories))
            {
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.TRACES_SUBSTITUTION.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++CntSensors);

                if (!file.FullName.Contains(".svn"))
                {
                    BinaryReader br = null;
                    BinaryWriter bw = null;
                    TextReader tr = null;
                    TextWriter tw = null;
                    try
                    {

                        br = new BinaryReader(new FileStream(file.FullName, FileMode.Open, FileAccess.Read));
                        if (Directory.Exists(System.IO.Path.GetDirectoryName(file.FullName.Replace(PluginSettings.SourceTracesDirectoryPath, PluginSettings.ResultTracesDirectoryPath))) == false)
                            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(file.FullName.Replace(PluginSettings.SourceTracesDirectoryPath, PluginSettings.ResultTracesDirectoryPath)));
                        bw = new BinaryWriter(new FileStream(file.FullName.Replace(PluginSettings.SourceTracesDirectoryPath, PluginSettings.ResultTracesDirectoryPath), FileMode.OpenOrCreate, FileAccess.Write));

                        byte[] arrA = new byte[sizeof(Int32)];
                        byte[] arrB = new byte[sizeof(Int32)];
                        Int32 lastValue = 0;
                        //Читаем первые 4 байта
                        int length = br.Read(arrA, 0, sizeof(Int32));
                        if (length != sizeof(Int32))
                            continue;
                        //Проверяем, не текст ли
                        string s = Encoding.Default.GetString(arrA, 0, 4);
                        int value;
                        if (int.TryParse(s, out value)) // Поправить
                        {
                            br.Close();
                            br = null;
                            bw.Close();
                            bw = null;
                            //Обработка текстовых трасс
                            string sI, sO;
                            tr = new StreamReader(file.FullName);
                            tw = new StreamWriter(file.FullName.Replace(PluginSettings.SourceTracesDirectoryPath, PluginSettings.ResultTracesDirectoryPath));
                            while (true)
                            {
                                sI = tr.ReadLine();
                                if (sI == null)
                                    break;
                                if (Int32.TryParse(sI, out iS))
                                {

                                    if (dReplace.TryGetValue(iS, out iD))
                                    {
                                        if (iD != 0)
                                        {
                                            sO = iD.ToString();
                                            tw.WriteLine(sO);
                                            nCountD++;
                                        }
                                    }

                                }
                                else
                                {
                                    //Error
                                    continue;
                                }
                                nCountS++;
                            }

                        }
                        else
                        {
                            while (true)
                            {
                                iS = BitConverter.ToInt32(arrA, 0);
                                iD = -2;
                                if (iS != -1)
                                {
                                    if (!dReplace.TryGetValue(iS, out iD))
                                        iD = -2;
                                }
                                else
                                {
                                    if (lastValue != -1)
                                        iD = iS;

                                }
                                lastValue = iS;
                                if (iD != -2 && ID!=0)
                                {
                                    byte[] data = BitConverter.GetBytes(iD);
                                    Array.Copy(data, 0, arrB, 0, sizeof(Int32));
                                    bw.Write(data);
                                    nCountD++;
                                }
                                nCountS++;

                                length = br.Read(arrA, 0, sizeof(Int32));
                                if (length != sizeof(Int32))
                                    break;
                            }
                        }
                    }
                    finally
                    {
                        if (br != null)
                            br.Close();
                        if (bw != null)
                            bw.Close();
                        if (tr != null)
                            tr.Close();
                        if (tw != null)
                            tw.Close();

                    }
                }
            }
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.TRACES_SUBSTITUTION.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)(new DirectoryInfo(PluginSettings.SourceTracesDirectoryPath)).GetFiles("*.*", SearchOption.AllDirectories).Length);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.OldReportsDirectoryPath = PluginSettings.OldReportsDirectoryPath;
            Properties.Settings.Default.NewReportsDirectoryPath = PluginSettings.NewReportsDirectoryPath;
            Properties.Settings.Default.SourceTracesDirectoryPath = PluginSettings.SourceTracesDirectoryPath;
            Properties.Settings.Default.ResultTracesDirectoryPath = PluginSettings.ResultTracesDirectoryPath;
            Properties.Settings.Default.CorrectPos = PluginSettings.CorrectPos;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.OldReportsDirectoryPath);
            settingsBuffer.Add(PluginSettings.NewReportsDirectoryPath);
            settingsBuffer.Add(PluginSettings.SourceTracesDirectoryPath);
            settingsBuffer.Add(PluginSettings.ResultTracesDirectoryPath);
            settingsBuffer.Add(PluginSettings.CorrectPos);
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.REPORTS_ANALIZING.Description(), 1),
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.TRACES_SUBSTITUTION.Description(), 1)
                };
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

		{
            get
            {
			    return null;
            }
		}

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                settings = new Settings();
                return settings;
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return this.CustomSettingsWindow;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (!Directory.Exists(PluginSettings.NewReportsDirectoryPath))
            {
                message = "Путь к директории новых отчетов не найден.";
                return false;
            }

            if (!Directory.Exists(PluginSettings.OldReportsDirectoryPath))
            {
                message = "Путь к директории старых отчетов не найден.";
                return false;
            }

            if (!Directory.Exists(PluginSettings.ResultTracesDirectoryPath))
            {
                message = "Путь к директории с результирующими трассами не найден.";
                return false;
            }

            if (!Directory.Exists(PluginSettings.SourceTracesDirectoryPath))
            {
                message = "Путь к директориис исходными трассами не найден.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        #endregion

        private void NormReport(List<string[]> oR, List<string[]> nR)
        {
            int i1 = 0;
            int i2 = 0;
            int delta = 0;
            bool finish = false;
            while (true)
            {
                while (true)
                {
                    if (oR[i1][1].ToLower() != nR[i2][1].ToLower() || oR[i1][2].ToLower() != nR[i2][2].ToLower())
                        break;
                    if (!IsEqual(oR[i1], nR[i2]))
                    {
                        string filename = oR[i1][2];
                        int i = 0;
                        while (true)
                        {
                            if (oR[i1 + i][2] != filename)
                                break;
                            oR[i1 + i][3] = (int.Parse(oR[i1 + i][3]) + delta).ToString();
                            i++;
                        }
                    }
                    for (; i1 < oR.Count && i2 < nR.Count; i1++, i2++)
                    {
                        if (IsEqual(oR[i1], nR[i2]) == false)
                        {
                            delta = int.Parse(nR[i2][3]) - int.Parse(oR[i1][3]);
                            break;
                        }
                    }
                    if (i1 >= oR.Count)
                    {
                        break;
                    }
                    if (i2 >= nR.Count)
                    {
                        break;
                    }
                }

                while (true)
                {
                    if (i2 >= nR.Count)
                    {
                        finish = true;
                        break;
                    }
                    if (nR.Skip(i2).Where(x => x[1].ToLower() == oR[i1][1].ToLower() && x[2].ToLower() == oR[i1][2].ToLower()).Count() == 0)
                    {
                        i1++;
                        if (i1 >= oR.Count)
                        {
                            finish = true;
                            break;
                        }
                    }
                    else
                        break;
                }
                if (!finish)
                {
                    if (oR[i1][1].ToLower() == nR[i2][1].ToLower() && oR[i1][2].ToLower() == nR[i2][2].ToLower())
                    {
                        finish = false;
                    }
                    else
                    {
                        while (true)
                        {
                            i2++;
                            if (i2 >= nR.Count)
                            {
                                i2 = nR.Count - 1;
                                finish = true;
                                break;
                            }
                            if (oR[i1][1].ToLower() == nR[i2][1].ToLower() && oR[i1][2].ToLower() == nR[i2][2].ToLower())
                            {

                                delta = int.Parse(nR[i2][3]) - int.Parse(oR[i1][3]);
                                break;
                            }
                        }
                    }
                    finish = false;
                }

                if (finish)
                    break;
            }

        }
        private bool IsEqual(string[] a, string[] b)
        {
            int l = a.Length;
            if (l != b.Length)
                return false;
            for (int i = 1; i < l; i++)
            {
                if (a[i].ToLower() != b[i].ToLower())
                    return false;
            }
            return true;
        }
    }
}
