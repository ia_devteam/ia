﻿namespace IA.Plugins.Utils.ReplaceTraces
{
    partial class Settings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.oldReportsDirectoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.oldReportsDirectoryPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.newReportsDirectoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.newReportsDirectoryPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.sourceTracesDirectoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.sourceTracesDirectoryPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.resultTracesDirectoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.resultTracesDirectoryPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.checkBoxCorrPos = new System.Windows.Forms.CheckBox();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.oldReportsDirectoryPath_groupBox.SuspendLayout();
            this.newReportsDirectoryPath_groupBox.SuspendLayout();
            this.sourceTracesDirectoryPath_groupBox.SuspendLayout();
            this.resultTracesDirectoryPath_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.oldReportsDirectoryPath_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Controls.Add(this.newReportsDirectoryPath_groupBox, 0, 1);
            this.settings_tableLayoutPanel.Controls.Add(this.sourceTracesDirectoryPath_groupBox, 0, 2);
            this.settings_tableLayoutPanel.Controls.Add(this.resultTracesDirectoryPath_groupBox, 0, 3);
            this.settings_tableLayoutPanel.Controls.Add(this.checkBoxCorrPos, 0, 4);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 6;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(513, 256);
            this.settings_tableLayoutPanel.TabIndex = 12;
            // 
            // oldReportsDirectoryPath_groupBox
            // 
            this.oldReportsDirectoryPath_groupBox.Controls.Add(this.oldReportsDirectoryPath_textBox);
            this.oldReportsDirectoryPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oldReportsDirectoryPath_groupBox.Location = new System.Drawing.Point(3, 3);
            this.oldReportsDirectoryPath_groupBox.Name = "oldReportsDirectoryPath_groupBox";
            this.oldReportsDirectoryPath_groupBox.Size = new System.Drawing.Size(507, 49);
            this.oldReportsDirectoryPath_groupBox.TabIndex = 0;
            this.oldReportsDirectoryPath_groupBox.TabStop = false;
            this.oldReportsDirectoryPath_groupBox.Text = "Путь к директории со старыми отчетами";
            // 
            // oldReportsDirectoryPath_textBox
            // 
            this.oldReportsDirectoryPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oldReportsDirectoryPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.oldReportsDirectoryPath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.oldReportsDirectoryPath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.oldReportsDirectoryPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.oldReportsDirectoryPath_textBox.Name = "oldReportsDirectoryPath_textBox";
            this.oldReportsDirectoryPath_textBox.Size = new System.Drawing.Size(501, 25);
            this.oldReportsDirectoryPath_textBox.TabIndex = 0;
            // 
            // newReportsDirectoryPath_groupBox
            // 
            this.newReportsDirectoryPath_groupBox.Controls.Add(this.newReportsDirectoryPath_textBox);
            this.newReportsDirectoryPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newReportsDirectoryPath_groupBox.Location = new System.Drawing.Point(3, 58);
            this.newReportsDirectoryPath_groupBox.Name = "newReportsDirectoryPath_groupBox";
            this.newReportsDirectoryPath_groupBox.Size = new System.Drawing.Size(507, 49);
            this.newReportsDirectoryPath_groupBox.TabIndex = 1;
            this.newReportsDirectoryPath_groupBox.TabStop = false;
            this.newReportsDirectoryPath_groupBox.Text = "Путь к директории с новыми отчётами";
            // 
            // newReportsDirectoryPath_textBox
            // 
            this.newReportsDirectoryPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newReportsDirectoryPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.newReportsDirectoryPath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.newReportsDirectoryPath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.newReportsDirectoryPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.newReportsDirectoryPath_textBox.Name = "newReportsDirectoryPath_textBox";
            this.newReportsDirectoryPath_textBox.Size = new System.Drawing.Size(501, 25);
            this.newReportsDirectoryPath_textBox.TabIndex = 0;
            // 
            // sourceTracesDirectoryPath_groupBox
            // 
            this.sourceTracesDirectoryPath_groupBox.Controls.Add(this.sourceTracesDirectoryPath_textBox);
            this.sourceTracesDirectoryPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceTracesDirectoryPath_groupBox.Location = new System.Drawing.Point(3, 113);
            this.sourceTracesDirectoryPath_groupBox.Name = "sourceTracesDirectoryPath_groupBox";
            this.sourceTracesDirectoryPath_groupBox.Size = new System.Drawing.Size(507, 49);
            this.sourceTracesDirectoryPath_groupBox.TabIndex = 2;
            this.sourceTracesDirectoryPath_groupBox.TabStop = false;
            this.sourceTracesDirectoryPath_groupBox.Text = "Путь к директории с исходными трассами";
            // 
            // sourceTracesDirectoryPath_textBox
            // 
            this.sourceTracesDirectoryPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceTracesDirectoryPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.sourceTracesDirectoryPath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.sourceTracesDirectoryPath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.sourceTracesDirectoryPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.sourceTracesDirectoryPath_textBox.Name = "sourceTracesDirectoryPath_textBox";
            this.sourceTracesDirectoryPath_textBox.Size = new System.Drawing.Size(501, 25);
            this.sourceTracesDirectoryPath_textBox.TabIndex = 0;
            // 
            // resultTracesDirectoryPath_groupBox
            // 
            this.resultTracesDirectoryPath_groupBox.Controls.Add(this.resultTracesDirectoryPath_textBox);
            this.resultTracesDirectoryPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultTracesDirectoryPath_groupBox.Location = new System.Drawing.Point(3, 168);
            this.resultTracesDirectoryPath_groupBox.Name = "resultTracesDirectoryPath_groupBox";
            this.resultTracesDirectoryPath_groupBox.Size = new System.Drawing.Size(507, 49);
            this.resultTracesDirectoryPath_groupBox.TabIndex = 3;
            this.resultTracesDirectoryPath_groupBox.TabStop = false;
            this.resultTracesDirectoryPath_groupBox.Text = "Путь к директории с конечными трассами";
            // 
            // resultTracesDirectoryPath_textBox
            // 
            this.resultTracesDirectoryPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultTracesDirectoryPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.resultTracesDirectoryPath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.resultTracesDirectoryPath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.resultTracesDirectoryPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.resultTracesDirectoryPath_textBox.Name = "resultTracesDirectoryPath_textBox";
            this.resultTracesDirectoryPath_textBox.Size = new System.Drawing.Size(501, 25);
            this.resultTracesDirectoryPath_textBox.TabIndex = 0;
            // 
            // checkBoxCorrPos
            // 
            this.checkBoxCorrPos.AutoSize = true;
            this.checkBoxCorrPos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxCorrPos.Location = new System.Drawing.Point(3, 223);
            this.checkBoxCorrPos.Name = "checkBoxCorrPos";
            this.checkBoxCorrPos.Size = new System.Drawing.Size(507, 24);
            this.checkBoxCorrPos.TabIndex = 4;
            this.checkBoxCorrPos.Text = "В файлах отчетов могут повторяться позиции датчиков";
            this.checkBoxCorrPos.UseVisualStyleBackColor = true;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(513, 256);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.settings_tableLayoutPanel.PerformLayout();
            this.oldReportsDirectoryPath_groupBox.ResumeLayout(false);
            this.newReportsDirectoryPath_groupBox.ResumeLayout(false);
            this.sourceTracesDirectoryPath_groupBox.ResumeLayout(false);
            this.resultTracesDirectoryPath_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox oldReportsDirectoryPath_groupBox;
        private System.Windows.Forms.GroupBox newReportsDirectoryPath_groupBox;
        private System.Windows.Forms.GroupBox sourceTracesDirectoryPath_groupBox;
        private System.Windows.Forms.GroupBox resultTracesDirectoryPath_groupBox;
        private Controls.TextBoxWithDialogButton oldReportsDirectoryPath_textBox;
        private Controls.TextBoxWithDialogButton newReportsDirectoryPath_textBox;
        private Controls.TextBoxWithDialogButton sourceTracesDirectoryPath_textBox;
        private Controls.TextBoxWithDialogButton resultTracesDirectoryPath_textBox;
        private System.Windows.Forms.CheckBox checkBoxCorrPos;
    }
}
