﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.BinPropertyReader
{
    /// <summary>
    /// Класс, реализующий элемент управления задания всех настроек плагина
    /// </summary>
    public partial class CustomSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public CustomSettings()
        {
            InitializeComponent();

            directoryPath_textBoxWithDialogButton.Text = PluginSettings.folderName;
        }

        /// <summary>
        /// Сохранение введенных настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.folderName = directoryPath_textBoxWithDialogButton.Text;
        }
    }
}
