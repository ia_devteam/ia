﻿namespace IA.Plugins.Analyses.BinPropertyReader
{
    partial class CustomSettings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.directoryPath_groupBox = new System.Windows.Forms.GroupBox();
            this.directoryPath_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.directoryPath_textBoxWithDialogButton = new IA.Controls.TextBoxWithDialogButton();
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.directoryPath_groupBox.SuspendLayout();
            this.directoryPath_tableLayoutPanel.SuspendLayout();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // directoryPath_groupBox
            // 
            this.directoryPath_groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.directoryPath_groupBox.Controls.Add(this.directoryPath_tableLayoutPanel);
            this.directoryPath_groupBox.Location = new System.Drawing.Point(3, 3);
            this.directoryPath_groupBox.Name = "directoryPath_groupBox";
            this.directoryPath_groupBox.Size = new System.Drawing.Size(833, 49);
            this.directoryPath_groupBox.TabIndex = 3;
            this.directoryPath_groupBox.TabStop = false;
            this.directoryPath_groupBox.Text = "Папка с приложениями и библиотеками";
            // 
            // directoryPath_tableLayoutPanel
            // 
            this.directoryPath_tableLayoutPanel.ColumnCount = 1;
            this.directoryPath_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.directoryPath_tableLayoutPanel.Controls.Add(this.directoryPath_textBoxWithDialogButton, 0, 0);
            this.directoryPath_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.directoryPath_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.directoryPath_tableLayoutPanel.Name = "directoryPath_tableLayoutPanel";
            this.directoryPath_tableLayoutPanel.RowCount = 1;
            this.directoryPath_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.directoryPath_tableLayoutPanel.Size = new System.Drawing.Size(827, 30);
            this.directoryPath_tableLayoutPanel.TabIndex = 2;
            // 
            // directoryPath_textBoxWithDialogButton
            // 
            this.directoryPath_textBoxWithDialogButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.directoryPath_textBoxWithDialogButton.Location = new System.Drawing.Point(3, 3);
            this.directoryPath_textBoxWithDialogButton.MaximumSize = new System.Drawing.Size(0, 24);
            this.directoryPath_textBoxWithDialogButton.MinimumSize = new System.Drawing.Size(150, 24);
            this.directoryPath_textBoxWithDialogButton.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.directoryPath_textBoxWithDialogButton.Name = "directoryPath_textBoxWithDialogButton";
            this.directoryPath_textBoxWithDialogButton.Size = new System.Drawing.Size(821, 24);
            this.directoryPath_textBoxWithDialogButton.TabIndex = 0;
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.directoryPath_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 2;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(839, 170);
            this.settings_tableLayoutPanel.TabIndex = 4;
            // 
            // CustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(839, 170);
            this.directoryPath_groupBox.ResumeLayout(false);
            this.directoryPath_tableLayoutPanel.ResumeLayout(false);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox directoryPath_groupBox;
        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel directoryPath_tableLayoutPanel;
        private Controls.TextBoxWithDialogButton directoryPath_textBoxWithDialogButton;
    }
}
