﻿using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Diagnostics;

using IA.Extensions;

namespace IA.Plugins.Analyses.BinPropertyReader
{
    /// <summary>
    /// Класс настроек плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Чтение свойств бинарных файлов";

        /// <summary>
        /// Иденитификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.BIN_PROPERTY_READER;

        #region Settings

        /// <summary>
        /// Переменная для хранения директории с приложениями
        /// </summary>
        internal static string folderName;

        #endregion

        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Чтение файлов")]
            BIN_FILES
        };
        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum TaskReports
        {
            [Description("Запись свойств файла")]
            WRITE_BIN_FILE
        };


    }


    /// <summary>
    /// Основной класс плагина
    /// </summary>
    public class BinPropertyReader : IA.Plugin.Interface
    {
        /// <summary>
        /// Текущие настройки
        /// </summary>

        private CustomSettings settings; 

        /// <summary>
        /// Прочитанные свойства файлов
        /// </summary>
        ReaderExeProperties exeProperies;

        #region plugin interface

        /// <summary>
        ///  <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get { return PluginSettings.ID; }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW |
                    //Plugin.Capabilities.RESULT_WINDOW |
                                    Plugin.Capabilities.REPORTS |
                                        Plugin.Capabilities.RERUN;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.UserControl CustomSettingsWindow
        {
            get
            {
                return settings = new CustomSettings();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.UserControl RunWindow
        {
            get { return null; }
        }
        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Forms.UserControl ResultWindow
        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(Name, PluginSettings.Tasks.BIN_FILES.Description())
                };
            }
        }
        /// <summary>
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                    {
                        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.TaskReports.WRITE_BIN_FILE.Description(),0)
                    };
            }
        }
        /// <summary>
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"></param>
        public void Initialize(Store.Storage storage)
        {
            PluginSettings.folderName = Properties.Settings.Default.FolderName;
        }

        /// <summary>
        ///  <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"></param>
        public void LoadSettings(Store.Table.IBufferReader settingsBuffer)
        {
            PluginSettings.folderName = settingsBuffer.GetString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="settingsString"></param>
        public void SetSimpleSettings(string settingsString)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="settingsBuffer"></param>
        public void SaveSettings(Store.Table.IBufferWriter settingsBuffer)
        {
            Properties.Settings.Default.FolderName = PluginSettings.folderName;
            Properties.Settings.Default.Save();

            settingsBuffer.Add(PluginSettings.folderName);
        }
        /// <summary>
        /// 
        /// </summary>
        public void SaveResults()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;
            if (settings != null && PluginSettings.folderName != string.Empty)
            {
                settings.Save();
                return true;
            }
            else
            {
                message = "Не задана директория с приложениями и библиотеками: ";
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportsDirectoryPath"></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            if (string.IsNullOrEmpty(reportsDirectoryPath))
            {
                Monitor.Log.Error(Name, "Папка с отчетами не определена.");
                return;
            }

            if (exeProperies == null)
            {                               
                    Monitor.Log.Warning(Name, "Плагин не был запущен.");
                    return;
            }
            exeProperies.WriteToCSV(reportsDirectoryPath);
        }

        /// <summary>
        /// 
        /// </summary>
        public void StopRunning()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        public void Run()
        {
            //Получаем свойства файлов
            exeProperies = new ReaderExeProperties(PluginSettings.folderName);
            return;
        }
        #endregion

        #region functions

        /// <summary>
        /// Класс, обеспечивающий функциональность плагина
        /// </summary>
        class ReaderExeProperties
        {
            /// <summary>
            /// Список свойств для каждого файла
            /// </summary>
            List<string> exeProperties;
            public ReaderExeProperties(string folderName)
            {
                this.exeProperties = GetExeProperties(GetExeFiles(folderName));
            }

            /// <summary>
            /// Получение списка свойств для каждого файла
            /// </summary>
            /// <param name="filePaths">список файлов</param>
            /// <returns>списко свойств файлов</returns>
            private List<string> GetExeProperties(List<string> filePaths)
            {
                List<string> exeProperties = new List<string>();
                ulong counter = 0;
                foreach (string path in filePaths)
                {
                    exeProperties.Add(ReadExeFile(path).GetCsVString());
                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.BIN_FILES.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                }
                return exeProperties;
            }

            /// <summary>
            /// Функция выдает список файлов с раширениями "*.exe", "*.dll" ///////, "*.lib"
            /// </summary>
            /// <param name="directory">директория с файлами</param>
            /// <returns>список полных путей к файлам </returns>
            private List<string> GetExeFiles(string directory)
            {
                string[] extentionPatterns = { "*.exe", "*.dll" };

                List<string> allFiles = new List<string>();
                if (!string.IsNullOrEmpty(directory))
                {
                    foreach (string ext in extentionPatterns)
                        allFiles.AddRange(Directory.EnumerateFiles(directory, ext, SearchOption.AllDirectories));
                }
                //Обновляем таск
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.BIN_FILES.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)allFiles.Count);
                return allFiles;
            }

            /// <summary>
            /// Запись в файл .csv c именем BinPropertyRead.csv свойств файлов в указанную директорию
            /// </summary>
            /// <param name="outputDir">директория, в которую сохраняется файл</param>
            public void WriteToCSV(string outputDir)
            {
               
                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TaskReports.WRITE_BIN_FILE.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)exeProperties.Count);
                    string outputPath = Path.Combine(outputDir, "BinPropertyRead.csv");
                    if (File.Exists(outputPath))
                    {
                        File.Delete(outputPath);
                    }
                    using (FileStream fs = new FileStream(outputPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                    {
                        if (exeProperties.Count > 0)
                        {
                            using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
                            {
                                ulong counter = 0;
                                foreach (string exeP in exeProperties)
                                {
                                    sw.WriteLine(exeP);
                                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TaskReports.WRITE_BIN_FILE.Description(), Monitor.Tasks.Task.UpdateType.STAGE, counter++);
                                }
                            }
                        }
                        else
                            Monitor.Log.Warning(PluginSettings.Name, "Файлы для обработки не найдены.");                       
                    }                
                }
                
            /// <summary>
            /// Пытаемся прочитать свойства версии
            /// </summary>
            /// <param name="exe">параметры файла</param>
            private void TryGetVersionProperty(ExeProperties exe)
            {

                FileVersionInfo vers;
                try
                {
                    if ((vers = FileVersionInfo.GetVersionInfo(exe.FilePath)) != null)
                    {

                        exe.Description = GetValue(vers.FileDescription.ToString());
                        exe.FileVersion = GetValue(vers.FileVersion);
                        exe.ProductName = GetValue(vers.ProductName);
                        exe.AutorRights = GetValue(vers.LegalCopyright);
                        exe.Language = GetValue(vers.Language);
                        exe.OriginalName = GetValue(vers.OriginalFilename);
                        exe.CompanyName = GetValue(vers.CompanyName);
                    }
                }
                catch { }
            }

            /// <summary>
            /// Пытаемся прочитать информацию о файле
            /// </summary>
            /// <param name="exe">параметры файла</param>
            private void TryGetInfFile(ExeProperties exe)
            {
                try
                {
                    FileInfo inffile = new FileInfo(exe.FilePath);
                    if ((inffile = new FileInfo(exe.FilePath)) != null)
                    {
                        exe.Size = GetValue(inffile.Length.ToString());
                        exe.DateofChange = GetValue(inffile.LastWriteTime.ToString());
                    }
                }
                catch { }
            }

            /// <summary>
            /// Определение назначения
            /// </summary>
            /// <param name="exe"></param>
            private void TryGetExtention(ExeProperties exe)
            {
                string ext;
                if ((ext = Path.GetExtension(exe.FilePath)) == ".dll")
                {
                    exe.Type = "Расширение приложения";
                }
                if (ext == ".exe")
                {
                    exe.Type = "Приложение";
                }
            }
            /// <summary>
            /// Чтение свойств файла
            /// </summary>
            /// <param name="filePath">путь к файлу</param>
            /// <returns>свойства файла</returns>
            public ExeProperties ReadExeFile(string filePath)
            {
                ExeProperties exe = new ExeProperties();
                if (!string.IsNullOrEmpty(filePath))
                {
                    exe.FilePath = filePath;
                    exe.Location = GetValue(filePath.Replace(PluginSettings.folderName, ""));
                    exe.DateofChange = GetValue(File.GetLastWriteTime(exe.FilePath).ToString());
                    TryGetVersionProperty(exe);
                    TryGetExtention(exe);
                    TryGetInfFile(exe);
                }
                return exe;
            }

            string GetValue(string value)
            {
                return (value == null) ? "" : value;
            }
        }
    }

    /// <summary>
    /// Класс, описывающий структуру свойств приложений и библиотек
    /// </summary>
    class ExeProperties
    {
        public string FilePath = string.Empty;     //Полный путь файла
        public string Location = string.Empty;     //Местоположение относительно директории (папки и имя файла) для инициализации
        public string Description = string.Empty;  //Описание файла
        public string Type = string.Empty;         //Тип файла
        public string FileVersion = string.Empty;  //Версия файла
        public string ProductName = string.Empty;  //Название продукта
        public string AutorRights = string.Empty;  //Авторские права
        public string Size = string.Empty;         //Размер
        public string DateofChange = string.Empty; //Дата изменения
        public string Language = string.Empty;     //Язык
        public string OriginalName = string.Empty; //Исходное имя файла
        public string CompanyName = string.Empty;  //Производитель

        /// <summary>
        /// Переводит свойства экземпляра в строковый вид
        /// </summary>
        /// <returns>строку свойств</returns>
        public string GetCsVString()
        {
            string sep = ";";
            string result = Location + sep;
            result += CompanyName + sep;
            result += ProductName + sep;
            result += FileVersion + sep;
            result += AutorRights + sep;
            result += OriginalName + sep;
            result += Description + sep;
            result += Type + sep;
            result += Size + sep;
            result += DateofChange + sep;
            result += Language;
            return result;
        }
    }

        #endregion

}













