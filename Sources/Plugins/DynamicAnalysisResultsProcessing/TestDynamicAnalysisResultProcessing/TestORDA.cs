﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Store.Table;
using TestUtils;
using Store;
using IOController;
using IA.Plugins.Analyses.DynamicAnalysisResultsProcessing;

namespace TestDynamicAnalysisResultProcessing
{
    [TestClass]
    public class TestDynamicAnalysisResultProcessing
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Список сообщений
            /// </summary>
            public List<string> Messages
            {
                get
                {
                    return messages;
                }
            }
        }

        /// <summary>
        /// Перехватчик сообщений монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        private Storage currentStorage;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();

            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// После каждого теста очищаем текущие поля.
        /// </summary>
        [TestCleanup]
        public void EachTest_Cleanup()
        {
            //Сбрасываем информацию о текущем Хранилище
            currentStorage = null;
        }

        /// <summary>
        /// Базовая проверка режима обработки трасс в соответствии с 3 уровнем контроля НДВ.
        /// </summary>
        [TestMethod]
        public void DARP_IsItWorksSomeHow_3lvl()
        {
            //Подготовка Хранилища
            TestUtils.TestUtilsClass.CustomizeStorage customizeStorageAction =
                (storage, testMaterialsPath) =>
                {
                    PrepareStorage(storage, testMaterialsPath, @"Sources\CSharp\FindStringsInFiles\");
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\FindStringsInFiles\"));
                    currentStorage = storage;
                    DARP_Prepare(testMaterialsPath);
                };

            //Запуск теста плагина
            DARP_RunTest(customizeStorageAction, this.DARP_CheckReports);
        }

        /// <summary>
        /// Базовая проверка режима обработки трасс в соответствии со 2 уровнем контроля НДВ.
        /// </summary>
        [TestMethod]
        public void DARP_IsItWorksSomeHow_2lvl()
        {
            //Подготовка Хранилища
            TestUtils.TestUtilsClass.CustomizeStorage customizeStorageAction =
                (storage, testMaterialsPath) =>
                {
                    PrepareStorage(storage, testMaterialsPath, @"Sources\CSharp\FindStringsInFiles\");
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\FindStringsInFiles\"), level2: true);
                    currentStorage = storage;
                    DARP_Prepare(testMaterialsPath, mode: DynamicAnalysisResultsProcessing.Modes.NDV2);
                };

            //Запуск теста плагина
            DARP_RunTest(customizeStorageAction, this.DARP_CheckReports);
        }

        /// <summary>
        /// Базовая проверка режима вычисления покрытия.
        /// </summary>
        [TestMethod]
        public void DARP_IsItWorksSomeHow_CoverOnly()
        {
            //Подготовка Хранилища
            TestUtils.TestUtilsClass.CustomizeStorage customizeStorageAction =
                (storage, testMaterialsPath) =>
                {
                    PrepareStorage(storage, testMaterialsPath, @"Sources\CSharp\FindStringsInFiles\");
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\FindStringsInFiles\"));
                    currentStorage = storage;
                    DARP_Prepare(testMaterialsPath, mode: DynamicAnalysisResultsProcessing.Modes.CoverOnly);
                };

            //Запуск теста плагина
            DARP_RunTest(customizeStorageAction, this.DARP_CheckReports);
        }

        /// <summary>
        /// Well Estimated Implementation Due Enought Report
        /// Проверка обрезания датчиков, их сдвига, обработки текстовых трасс по ключевому слову,
        /// вычитывания текстовых файлов в разных кодировках.
        /// Дополнительно проверяется функционал контроля вхождения функций.
        /// </summary>
        [TestMethod]
        public void DART_Weider()
        {
            //Подготовка Хранилища
            TestUtils.TestUtilsClass.CustomizeStorage customizeStorageAction =
                (storage, testMaterialsPath) =>
                {
                    PrepareStorage(storage, testMaterialsPath, @"Sources\CSharp\FindStringsInFiles\");
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\FindStringsInFiles\"));
                    currentStorage = storage;
                    DARP_Prepare(testMaterialsPath,
                        mode: DynamicAnalysisResultsProcessing.Modes.NDV3,
                        keyWord: "SENSOR",
                        rangeFrom: 1,
                        rangeTo: 3,
                        sensorOffset: 2,
                        controlFunctionsFile: Path.Combine(testMaterialsPath, String.Format(@"DynamicAnalysisResultProcessing\{0}\controlfuncs.txt", testUtils.TestName)));
                };

            //Запуск теста плагина
            DARP_RunTest(customizeStorageAction, this.DARP_CheckReports);
        }

        /// <summary>
        /// Проверка обработки текстовых и бинарных трасс в разных архивах и подкаталогах.
        /// Так же проверка функционала учёта списка значимых функций.
        /// </summary>
        [TestMethod]
        public void DARP_Cpp3lvl()
        {
            const string understandReportsPrefixPart = @"Understand\CPP\easing\";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\Sources\CPP\easing\";
            //Подготовка Хранилища
            TestUtils.TestUtilsClass.CustomizeStorage customizeStorageAction =
                (storage, testMaterialsPath) =>
                {
                    PrepareStorage(storage, testMaterialsPath, @"Sources\CPP\easing\");
                    TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                    TestUtilsClass.Run_CS_Sensors(storage);

                    currentStorage = storage;
                    DARP_Prepare(
                        testMaterialsPath,
                        necessaryFunctionsList: new List<string> { Path.Combine(testMaterialsPath, @"DynamicAnalysisResultProcessing\DARP_Cpp3lvl\necessaryFunctionsList.txt") }
                        );
                };

            //Запуск теста плагина
            DARP_RunTest(customizeStorageAction, this.DARP_CheckReports);
        }

        /// <summary>
        /// Проверка обработки бинарных трасс в формате JS.
        /// </summary>
        [TestMethod]
        public void DARP_JSFormat()
        {
            //Подготовка Хранилища
            TestUtils.TestUtilsClass.CustomizeStorage customizeStorageAction =
                (storage, testMaterialsPath) =>
                {
                    var file = storage.files.Add(enFileKindForAdd.externalFile);
                    file.FullFileName_Original = "testFile";
                    var func1 = storage.functions.AddFunction();
                    func1.Name = "func1";
                    func1.AddDefinition(file, 0);
                    func1.AddSensor(1, Kind.START);
                    func1.AddSensor(2, Kind.INTERNAL);
                    func1.UseKind = enFunctionUseKinds.PROGRAM_START;

                    var func2 = storage.functions.AddFunction();
                    func2.Name = "func2";
                    func2.AddDefinition(file, 100);
                    func2.AddSensor(3, Kind.START);
                    func2.AddSensor(4, Kind.FINAL);
                    func1.AddCall(func2);                    

                    currentStorage = storage;
                    DARP_Prepare(testMaterialsPath);
                };

            //Запуск теста плагина
            DARP_RunTest(customizeStorageAction, this.DARP_CheckReports);
        }

        /// <summary>
        /// Тест на поведение в ситуациях плохого ввода:
        /// пустой каталог с трассами, пустой файл трасс, много -1 подряд в трассе,
        /// трасса есть левый бинарник с произвольным содержимым.
        /// Проверка выдачи сообщений о соответствующий проблемах.
        /// </summary>
        [TestMethod]
        public void DARP_CheckBadSituations()
        {
            const string understandReportsPrefixPart = @"Understand\CPP\easing\";
            const string origSourcePath = @"d:\ia5\git\Tests\Materials\Sources\CPP\easing\";

            TestUtils.TestUtilsClass.LogSimpleListenerForTests listener = new TestUtilsClass.LogSimpleListenerForTests();

            TestUtils.TestUtilsClass.CustomizeStorage customizeStorageAction = (s, t) =>
            {
                PrepareStorage(s, t, @"Sources\CPP\easing\");
                TestUtilsClass.Run_UnderstandImporter(s, Path.Combine(t, understandReportsPrefixPart), origSourcePath);
                TestUtilsClass.Run_CS_Sensors(s);

                currentStorage = s;
                DARP_Prepare(t);
            };

            string traceDirectory = Path.Combine(testUtils.TestMatirialsDirectoryPath, @"DynamicAnalysisResultProcessing\DARP_CheckBadSituations", "trace");

            DirectoryInfo di = new DirectoryInfo(traceDirectory);

            Action<string> renewTraceDirectory = s =>
            {
                listener.Clear();
                if (di.Exists)
                    di.Delete(true);

                di.Create();
                foreach (var file in Directory.EnumerateFiles(traceDirectory + s))
                {
                    File.Copy(file, Path.Combine(traceDirectory, Path.GetFileName(file)));
                }
            };

            renewTraceDirectory("");
            DARP_RunTest(customizeStorageAction, (string reportsPath, string testMaterialsPath) => { return true; });

            Assert.IsTrue(listener.CheckMessage("Файлы для обработки не обнаружены.", IA.Monitor.Log.Message.enType.WARNING),
                            "Плагин не выдал предупреждение об отсутствующий файлах для обработки.");

            renewTraceDirectory("_empty_file");
            DARP_RunTest(customizeStorageAction, (string reportsPath, string testMaterialsPath) => { return true; });

            Assert.IsTrue(listener.CheckMessage("Файл <", IA.Monitor.Log.Message.enType.WARNING),
                            "Плагин не выдал предупреждение о пустом файле с трассой.");

            renewTraceDirectory("_bin");
            DARP_RunTest(customizeStorageAction, (string reportsPath, string testMaterialsPath) => { return true; });
            Assert.IsTrue(listener.CheckMessage("Встречена пустая трасса", IA.Monitor.Log.Message.enType.WARNING),
                "Плагин не выдал предупреждение о пустой трассе при обработке файла с несколькими -1 подряд.");
            Assert.IsTrue(listener.Count(m => m.Text.Contains("Встречена пустая трасса")) >= 3,
                "Плагин выдал меньше предупреждений о пустой трассе чем ожидалось.");
            Assert.IsTrue(listener.CheckMessage("Датчик не может иметь номер", IA.Monitor.Log.Message.enType.ERROR),
                "Плагин не выдал ошибки о наличии в трассе датчиков с неположительными номерами (за исключением -1).");
            Assert.IsTrue(listener.Count(m => m.Text.Contains("Датчик не может иметь номер")) >= 2,
                "Плагин выдал меньше предупреждений наличии в трассе датчиков с неположительными номерами чем ожидалось.");

            di.Delete(true);
        }

        /// <summary>
        /// Проверка выдачи сообщения о неверной настройке диапазона
        /// </summary>
        [TestMethod]
        public void DARP_WrongRangeSettings()
        {
            try
            {
                testUtils.RunTest(
                    string.Empty,                                                       //sourcePostfix
                    new DynamicAnalysisResultsProcessing(),                             //plugin
                    false,                                                              //isUseCacheStorage
                    (storage, testMaterialsPath) => { },                                //prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        IFunction func1 = storage.functions.AddFunction();
                        func1.AddSensor(1, Kind.START);
                        func1.Essential = enFunctionEssentials.REDUNDANT;

                        IFunction func2 = storage.functions.AddFunction();
                        func2.AddSensor(2, Kind.START);
                        func2.Essential = enFunctionEssentials.REDUNDANT;

                        currentStorage = storage;

                        //Создаем временную папку с временными трассами
                        string tempTracesDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Temp");
                        IOController.DirectoryController.Create(tempTracesDirectoryPath);

                        string tempTracesFilePath = Path.Combine(tempTracesDirectoryPath, "temp_traces.txt");
                        using (StreamWriter writer = new StreamWriter(tempTracesFilePath))
                        {
                            writer.WriteLine("1");
                        }

                        //Настраиваем плагин
                        DARP_Prepare(tempTracesDirectoryPath, rangeFrom: 3, rangeTo: 1);
                    },                                                                  //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        return true;
                    },                                                                  // checkStorage
                    (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                    false,                                                              // isUpdateReport
                    (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                    (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                    );
            }
            catch (Exception ex)
            {
                string message = "Нижняя граница диапазона должна быть меньше либо равна верхней границе.";

                Assert.IsTrue(ex.Message.Contains(message), "Сообщение не совпадает с ожидаемым <" + message + ">.");
            }
            finally
            {
                if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                    DirectoryController.Remove(testUtils.TestRunDirectoryPath);
            }
        }

        /// <summary>
        /// Проверка неизменности причины избыточности. 
        /// Настроек по проставлению меток нет.
        /// В трассе фигурирует датчик с функцией, у которой есть вызоваемые функции, но эти функции не меняют причину избыточности и не фигурируют в трассе.
        /// Хранилище настраивается вручную. Каждая функция избыточная.
        /// </summary>
        [TestMethod]
        public void DARP_MarkAs_NOTHING()
        {
            testUtils.RunTest(
                string.Empty,                                                       //sourcePostfix
                new DynamicAnalysisResultsProcessing(),                             //plugin
                false,                                                              //isUseCacheStorage
                (storage, testMaterialsPath) => { },                                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    func1.AddSensor(1, Kind.START);
                    func1.Essential = enFunctionEssentials.REDUNDANT;

                    IFunction func2 = storage.functions.AddFunction();
                    func2.AddSensor(2, Kind.START);
                    func2.Essential = enFunctionEssentials.REDUNDANT;
                    func1.AddCall(func2);

                    currentStorage = storage;

                    //Создаем временную папку с временными трассами
                    string tempTracesDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Temp");
                    IOController.DirectoryController.Create(tempTracesDirectoryPath);

                    string tempTracesFilePath = Path.Combine(tempTracesDirectoryPath, "temp_traces.txt");
                    using (StreamWriter writer = new StreamWriter(tempTracesFilePath))
                    {
                        writer.WriteLine("1");
                    }

                    //Настраиваем плагин
                    DARP_Prepare(tempTracesDirectoryPath);
                },                                                                  //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(storage.functions.GetFunction(2).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.NOTHING),
                        "Функциям проставлена причина существенности.");

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка установки причин существенности для всех вызывающих функции. 
        /// Вызывающие функции не были вызваны в трассах.
        /// Хранилище настраивается вручную. Каждая функция избыточная.
        /// </summary>
        [TestMethod]
        public void DARP_MarkAs_TRACE_CHECKER_CALLED()
        {
            testUtils.RunTest(
                string.Empty,                                                       //sourcePostfix
                new DynamicAnalysisResultsProcessing(),                             //plugin
                false,                                                              //isUseCacheStorage
                (storage, testMaterialsPath) => { },                                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    func1.AddSensor(1, Kind.START);
                    func1.Essential = enFunctionEssentials.REDUNDANT;

                    IFunction func2 = storage.functions.AddFunction();
                    func2.AddSensor(2, Kind.START);
                    func2.Essential = enFunctionEssentials.REDUNDANT;
                    func1.AddCall(func2);

                    currentStorage = storage;

                    //Создаем временную папку с временными трассами
                    string tempTracesDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Temp");
                    IOController.DirectoryController.Create(tempTracesDirectoryPath);

                    string tempTracesFilePath = Path.Combine(tempTracesDirectoryPath, "temp_traces.txt");
                    using (StreamWriter writer = new StreamWriter(tempTracesFilePath))
                    {
                        writer.WriteLine("1");
                    }

                    //Настраиваем плагин
                    DARP_Prepare(tempTracesDirectoryPath, isMarkCalledFunctions: true);
                },                                                                  //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(
                        storage.functions.EnumerateFunctions(enFunctionKind.ALL).All(func => func.EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED)),
                        "Функциям не верно проставлена причина существенности.");

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка установки причины существенности функции при непредвиденном переходе. 
        /// Такая функция нигде не вызывается.
        /// Хранилище настраивается вручную. Каждая функция избыточная.
        /// </summary>
        [TestMethod]
        public void DARP_MarkAs_TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES()
        {
            testUtils.RunTest(
                string.Empty,                                                       //sourcePostfix
                new DynamicAnalysisResultsProcessing(),                             //plugin
                false,                                                              //isUseCacheStorage
                (storage, testMaterialsPath) => { },                                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    func1.AddSensor(1, Kind.START);
                    func1.Essential = enFunctionEssentials.REDUNDANT;

                    IFunction func2 = storage.functions.AddFunction();
                    func2.AddSensor(2, Kind.START);
                    func2.Essential = enFunctionEssentials.REDUNDANT;
                    func1.AddCall(func2);

                    currentStorage = storage;

                    //Создаем временную папку с временными трассами
                    string tempTracesDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Temp");
                    IOController.DirectoryController.Create(tempTracesDirectoryPath);

                    string tempTracesFilePath = Path.Combine(tempTracesDirectoryPath, "temp_traces.txt");
                    using (StreamWriter writer = new StreamWriter(tempTracesFilePath))
                    {
                        writer.WriteLine("1");
                    }

                    //Настраиваем плагин
                    DARP_Prepare(tempTracesDirectoryPath, isMarkNotCallableEntriesAsProgramEntry: true);
                },                                                                  //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(
                        storage.functions.GetFunction(1).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES),
                        "Функции 1 не верно проставлена причина существенности.");
                    Assert.IsTrue(
                        storage.functions.GetFunction(2).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.NOTHING),
                        "Функции 2 проставлена причина существенности.");

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка установки причины существенности функции при непредвиденном переходе. 
        /// Такая функция вызывается в другой функции.
        /// Хранилище настраивается вручную. Каждая функция избыточная.
        /// </summary>
        [TestMethod]
        public void DARP_MarkAs_TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES()
        {
            testUtils.RunTest(
                string.Empty,                                                       //sourcePostfix
                new DynamicAnalysisResultsProcessing(),                             //plugin
                false,                                                              //isUseCacheStorage
                (storage, testMaterialsPath) => { },                                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    func1.AddSensor(1, Kind.START);
                    func1.Essential = enFunctionEssentials.REDUNDANT;

                    IFunction func2 = storage.functions.AddFunction();
                    func2.AddSensor(2, Kind.START);
                    func2.Essential = enFunctionEssentials.REDUNDANT;
                    func1.AddCall(func2);

                    currentStorage = storage;

                    //Создаем временную папку с временными трассами
                    string tempTracesDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Temp");
                    IOController.DirectoryController.Create(tempTracesDirectoryPath);

                    string tempTracesFilePath = Path.Combine(tempTracesDirectoryPath, "temp_traces.txt");
                    using (StreamWriter writer = new StreamWriter(tempTracesFilePath))
                    {
                        writer.WriteLine("2");
                    }

                    //Настраиваем плагин
                    DARP_Prepare(tempTracesDirectoryPath, isMarkAllEntriesAsProgramEntry: true);
                },                                                                  //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(
                        storage.functions.GetFunction(2).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES),
                        "Функции 2 не верно проставлена причина существенности.");
                    Assert.IsTrue(
                        storage.functions.GetFunction(1).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.NOTHING),
                        "Функции 1 проставлена причина существенности.");

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка установки причины существенности функции при непредвиденном переходе. 
        /// Одна функция вызывает другую. В трассе есть разрыв.
        /// Хранилище настраивается вручную. Каждая функция избыточная.
        /// </summary>
        [TestMethod]
        public void DARP_MarkAs_WITH_AND_NO_CALLES()
        {
            testUtils.RunTest(
                string.Empty,                                                       //sourcePostfix
                new DynamicAnalysisResultsProcessing(),                             //plugin
                false,                                                              //isUseCacheStorage
                (storage, testMaterialsPath) => { },                                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    func1.AddSensor(1, Kind.START);
                    func1.Essential = enFunctionEssentials.REDUNDANT;

                    IFunction func2 = storage.functions.AddFunction();
                    func2.AddSensor(2, Kind.START);
                    func2.Essential = enFunctionEssentials.REDUNDANT;
                    func1.AddCall(func2);

                    IFunction func3 = storage.functions.AddFunction();
                    func3.AddSensor(3, Kind.START);
                    func3.Essential = enFunctionEssentials.REDUNDANT;
                    func1.AddCall(func3);

                    IFunction func4 = storage.functions.AddFunction();
                    func4.AddSensor(4, Kind.START);
                    func4.Essential = enFunctionEssentials.REDUNDANT;
                    func2.AddCall(func4);

                    currentStorage = storage;

                    //Создаем временную папку с временными трассами
                    string tempTracesDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Temp");
                    IOController.DirectoryController.Create(tempTracesDirectoryPath);

                    string tempTracesFilePath = Path.Combine(tempTracesDirectoryPath, "temp_traces.txt");
                    using (StreamWriter writer = new StreamWriter(tempTracesFilePath))
                    {
                        writer.WriteLine("1\n-1\n2");
                    }

                    //Настраиваем плагин
                    DARP_Prepare(tempTracesDirectoryPath, isMarkNotCallableEntriesAsProgramEntry: true, isMarkAllEntriesAsProgramEntry: true);
                },                                                                  //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(
                        storage.functions.GetFunction(1).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES),
                        "Функции 1 не верно проставлена причина существенности.");
                    Assert.IsTrue(
                        storage.functions.GetFunction(2).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES),
                        "Функции 2 не верно проставлена причина существенности.");
                    Assert.IsTrue(
                        storage.functions.GetFunction(3).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.NOTHING) &&
                        storage.functions.GetFunction(4).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.NOTHING),
                        "Функциям проставлена причина существенности.");

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка установки причины существенности функции при непредвиденном переходе. 
        /// Такая функция нигде не вызывается.
        /// У функции есть вызываемые функции, которым также устанавливается причина существенности.
        /// Хранилище настраивается вручную. Каждая функция избыточная.
        /// </summary>
        [TestMethod]
        public void DARP_MarkAs_NO_CALLES_AND_HAS_CALLS()
        {
            testUtils.RunTest(
                string.Empty,                                                       //sourcePostfix
                new DynamicAnalysisResultsProcessing(),                             //plugin
                false,                                                              //isUseCacheStorage
                (storage, testMaterialsPath) => { },                                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    func1.AddSensor(1, Kind.START);
                    func1.Essential = enFunctionEssentials.REDUNDANT;

                    IFunction func2 = storage.functions.AddFunction();
                    func2.AddSensor(2, Kind.START);
                    func2.Essential = enFunctionEssentials.REDUNDANT;
                    func1.AddCall(func2);

                    currentStorage = storage;

                    //Создаем временную папку с временными трассами
                    string tempTracesDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Temp");
                    IOController.DirectoryController.Create(tempTracesDirectoryPath);

                    string tempTracesFilePath = Path.Combine(tempTracesDirectoryPath, "temp_traces.txt");
                    using (StreamWriter writer = new StreamWriter(tempTracesFilePath))
                    {
                        writer.WriteLine("1");
                    }

                    //Настраиваем плагин
                    DARP_Prepare(tempTracesDirectoryPath, isMarkNotCallableEntriesAsProgramEntry: true, isMarkCalledFunctions: true);
                },                                                                  //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(
                        storage.functions.GetFunction(1).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES),
                        "Функции 1 не верно проставлена причина существенности.");
                    Assert.IsTrue(
                        storage.functions.GetFunction(2).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED),
                        "Функции 2 не верно проставлена причина существенности.");

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка установки причины существенности функции при непредвиденном переходе. 
        /// Такая функция вызывается другой функции.
        /// У функции есть вызываемые функции, которым также устанавливается причина существенности.
        /// Хранилище настраивается вручную. Каждая функция избыточная.
        /// </summary>
        [TestMethod]
        public void DARP_MarkAs_WITH_CALLES_AND_HAS_CALLS()
        {
            testUtils.RunTest(
                string.Empty,                                                       //sourcePostfix
                new DynamicAnalysisResultsProcessing(),                             //plugin
                false,                                                              //isUseCacheStorage
                (storage, testMaterialsPath) => { },                                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    func1.AddSensor(1, Kind.START);
                    func1.Essential = enFunctionEssentials.REDUNDANT;

                    IFunction func2 = storage.functions.AddFunction();
                    func2.AddSensor(2, Kind.START);
                    func2.Essential = enFunctionEssentials.REDUNDANT;
                    func1.AddCall(func2);

                    IFunction func3 = storage.functions.AddFunction();
                    func3.AddSensor(3, Kind.START);
                    func3.Essential = enFunctionEssentials.REDUNDANT;
                    func2.AddCall(func3);

                    currentStorage = storage;

                    //Создаем временную папку с временными трассами
                    string tempTracesDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Temp");
                    IOController.DirectoryController.Create(tempTracesDirectoryPath);

                    string tempTracesFilePath = Path.Combine(tempTracesDirectoryPath, "temp_traces.txt");
                    using (StreamWriter writer = new StreamWriter(tempTracesFilePath))
                    {
                        writer.WriteLine("2");
                    }

                    //Настраиваем плагин
                    DARP_Prepare(tempTracesDirectoryPath, isMarkAllEntriesAsProgramEntry: true, isMarkCalledFunctions: true);
                },                                                                  //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(
                        storage.functions.GetFunction(1).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.NOTHING),
                        "Функции 1 проставлена причина существенности.");
                    Assert.IsTrue(
                        storage.functions.GetFunction(2).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES),
                        "Функции 2 не верно проставлена причина существенности.");
                    Assert.IsTrue(
                        storage.functions.GetFunction(3).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED),
                        "Функции 3 не верно проставлена причина существенности.");

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка установки причины существенности функции при непредвиденном переходе. 
        /// Одна функция вызывает другую. В трассе есть разрыв.
        /// У функции есть вызываемые функции, которым также устанавливается причина существенности.
        /// Хранилище настраивается вручную. Каждая функция избыточная.
        /// </summary>
        [TestMethod]
        public void DARP_MarkAs_NO_AND_WITH_CALLES_AND_HAS_CALLS()
        {
            testUtils.RunTest(
                string.Empty,                                                       //sourcePostfix
                new DynamicAnalysisResultsProcessing(),                             //plugin
                false,                                                              //isUseCacheStorage
                (storage, testMaterialsPath) => { },                                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    func1.AddSensor(1, Kind.START);
                    func1.Essential = enFunctionEssentials.REDUNDANT;

                    IFunction func2 = storage.functions.AddFunction();
                    func2.AddSensor(2, Kind.START);
                    func2.Essential = enFunctionEssentials.REDUNDANT;
                    func1.AddCall(func2);

                    IFunction func3 = storage.functions.AddFunction();
                    func3.AddSensor(3, Kind.START);
                    func3.Essential = enFunctionEssentials.REDUNDANT;
                    func2.AddCall(func3);

                    currentStorage = storage;

                    //Создаем временную папку с временными трассами
                    string tempTracesDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Temp");
                    IOController.DirectoryController.Create(tempTracesDirectoryPath);

                    string tempTracesFilePath = Path.Combine(tempTracesDirectoryPath, "temp_traces.txt");
                    using (StreamWriter writer = new StreamWriter(tempTracesFilePath))
                    {
                        writer.WriteLine("1\n2\n-1\n2\n3");
                    }

                    //Настраиваем плагин
                    DARP_Prepare(tempTracesDirectoryPath, isMarkNotCallableEntriesAsProgramEntry: true, isMarkAllEntriesAsProgramEntry: true, isMarkCalledFunctions: true);
                },                                                                  //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(
                        storage.functions.GetFunction(1).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES),
                        "Функции 1 не верно проставлена причина существенности.");
                    Assert.IsTrue(
                        storage.functions.GetFunction(2).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES),
                        "Функции 2 не верно проставлена причина существенности.");
                    Assert.IsTrue(
                        storage.functions.GetFunction(3).EssentialMarkedType.Equals(enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED),
                        "Функции 3 не верно проставлена причина существенности.");

                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },               // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Тест на проверку восстановлению пути по трассе по 3 уровню.
        /// Датчики стоят в начале и в конце функций.
        /// Проверяются построенные трассы и генерация пути алгортима по функциям.
        /// </summary>
        [TestMethod]
        public void DARP_FunctionsTest3lvl_StartEnd()
        {
            string sourcesDirectoryPostfix = @"Sources\CSharp\SimpleFuncCalls";

            DynamicAnalysisResultsProcessing testPlugin = new DynamicAnalysisResultsProcessing();

            testUtils.RunTest(
                string.Empty,                                       // sourcePrefixPart
                testPlugin,                                         // plugin
                false,                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    PrepareStorage(storage, testMaterialsPath, sourcesDirectoryPostfix);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\SimpleFuncCalls"));

                    currentStorage = storage;
                    DARP_Prepare(
                        testMaterialsPath,
                        mode: DynamicAnalysisResultsProcessing.Modes.NDV3
                        );
                },                                                      // customizeStorage
                (storage, testMaterialsPath) => { return true; },       // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    string etalonReportsDirectoryPath = Path.Combine(testMaterialsPath, String.Format(@"DynamicAnalysisResultProcessing\{0}\result\", testUtils.TestName));
                    string testCorrectnessDirectoryPath = Path.Combine(reportsPath, @"Проверка корректности трасс\");

                    foreach (string etalonReportFullPath in DirectoryController.GetFiles(etalonReportsDirectoryPath))
                    {
                        TestUtilsClass.Report_7z_Compare(etalonReportFullPath, Path.Combine(testCorrectnessDirectoryPath, Path.GetFileName(etalonReportFullPath)),
                            currentStorage, testPlugin.ID, (f1, f2) => TestUtilsClass.Reports_TXT_Compare(f1, f2));
                    }

                    return true;
                },                                                      // checkReportBeforeRerun
                false,                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }    // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку восстановлению пути по трассе по 2 уровню.
        /// Проверяются построенные трассы и генерация пути алгортима по функциям.
        /// </summary>
        [TestMethod, Ignore] //Ignore до тех пор, пока плагин не будет переработан для выдачи правильной корректности
        public void DARP_FunctionsTest2lvl()
        {
            string sourcesDirectoryPostfix = @"Sources\CSharp\SimpleFuncCalls";

            testUtils.RunTest(
                string.Empty,                                       // sourcePrefixPart
                new DynamicAnalysisResultsProcessing(),             // plugin
                false,                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    PrepareStorage(storage, testMaterialsPath, sourcesDirectoryPostfix);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, @"Sources\CSharpBuilt\SimpleFuncCalls"), level2: true);

                    currentStorage = storage;
                    DARP_Prepare(
                        testMaterialsPath,
                        mode: DynamicAnalysisResultsProcessing.Modes.NDV2
                        );
                },                                                      // customizeStorage
                (storage, testMaterialsPath) => { return true; },       // checkStorage
                (reportsPath, testMaterialsPath) => { return false; },  // checkReportBeforeRerun
                false,                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }    // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку восстановлению пути по трассе по 3 уровню
        /// Датчики стоят только в начале функций.
        /// Проверяются построенные трассы и генерация пути алгортима по функциям.
        /// </summary>
        [TestMethod]
        public void DARP_FunctionsTest3lvl_Start()
        {
            string sourcesDirectoryPostfix = @"CS_Sensors\SimpleFuncCalls";
            const string understandReportsPrefixPart = @"Understand\CS_Sensors\SimpleFuncCalls";
            const string origSourcePath = TestUtilsClass.PathConstant;
            DynamicAnalysisResultsProcessing testPlugin = new DynamicAnalysisResultsProcessing();

            testUtils.RunTest(
                string.Empty,                                       // sourcePrefixPart
                testPlugin,                                         // plugin
                false,                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    PrepareStorage(storage, testMaterialsPath, sourcesDirectoryPostfix);
                    TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath);
                    TestUtilsClass.Run_CS_Sensors(storage);

                    currentStorage = storage;
                    DARP_Prepare(
                        testMaterialsPath,
                        mode: DynamicAnalysisResultsProcessing.Modes.NDV3
                        );
                },                                                      // customizeStorage
                (storage, testMaterialsPath) => { return true; },       // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    string etalonReportsDirectoryPath = Path.Combine(testMaterialsPath, String.Format(@"DynamicAnalysisResultProcessing\{0}\result\", testUtils.TestName));
                    string testCorrectnessDirectoryPath = Path.Combine(reportsPath, @"Проверка корректности трасс\");

                    foreach (string etalonReportFullPath in DirectoryController.GetFiles(etalonReportsDirectoryPath))
                    {
                        TestUtilsClass.Report_7z_Compare(etalonReportFullPath, Path.Combine(testCorrectnessDirectoryPath, Path.GetFileName(etalonReportFullPath)),
                            currentStorage, testPlugin.ID, (f1, f2) => TestUtilsClass.Reports_TXT_Compare(f1, f2));
                    }

                    return true;
                },                                                      // checkReportBeforeRerun
                false,                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }    // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Тест на проверку работы плагина по 3-му уровню в рамках большого объёма входных данных (около 1Гб)
        /// Для прохождения теста достаточно корректного завершения работы плагина.
        /// </summary>
        [TestMethod, Ignore]
        public void DARP_HighPressure_Level3()
        {
            const string sourcesDirectoryPostfix = @"Sources\CSharp\DotNetZip\";
            const string sourcesBuiltDirectoryPostfix = @"Sources\CSharpBuilt\DotNetZip\";

            DynamicAnalysisResultsProcessing testPlugin = new DynamicAnalysisResultsProcessing();

            testUtils.RunTest(
                sourcesDirectoryPostfix,                            // sourcePostfixPart
                testPlugin,                                         // plugin
                false,                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    //Предварительно запускаем необходимые плагины
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(
                        storage,
                        Path.Combine(testUtils.TestMatirialsDirectoryPath, sourcesBuiltDirectoryPostfix),
                        1,
                        true
                    );
                    TestUtilsClass.Run_PointsToAnalysesSimple(storage);
                    TestUtilsClass.Run_WayGenerator(storage, false);

                    //Сохраняем текущее Хранилище
                    this.currentStorage = storage;

                    //Настраиваем плагин для запуска
                    DARP_Prepare(
                        testMaterialsPath,
                        mode: DynamicAnalysisResultsProcessing.Modes.NDV3
                    );
                },                                                      // customizeStorage
                (storage, testMaterialsPath) => { return true; },       // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                      // checkReportBeforeRerun
                false,                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }    // checkReportAfterRerun
            );
        }

        #region Общие методы
        /// <summary>
        /// Функция для подготовки Хранилища
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="testMaterialsPath">Путь до директории с материалами для тестирования</param>
        /// <param name="sourcesDirectoryPostfix">Постфикс пути до тестируемых данных в рамках материалов для тестирования</param>
        private void PrepareStorage(Storage storage, string testMaterialsPath, string sourcesDirectoryPostfix)
        {
            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcesDirectoryPostfix));
            TestUtilsClass.Run_IdentifyFileTypes(storage);
            TestUtilsClass.Run_IndexingFileContent(storage);
        }

        /// <summary>
        /// Конфигурирование плагина "Обработка результатов динамического анализа".
        /// </summary>
        /// <param name="testMaterialsPath">Путь до директории с материалами для тестирования.</param>
        /// <param name="mode">Режим работы плагина.</param>
        /// <param name="keyWord">Ключевое слово для обработки текстовых трасс.</param>
        /// <param name="rangeFrom">Нижняя граница номеров обрабатываемых датчиков.</param>
        /// <param name="rangeTo">Верхняя граница номеров обрабатываемых датчиков.</param>
        /// <param name="sensorOffset">Величина сдвига обрабатываемых номеров датчиков.</param>
        /// <param name="isMarkNotCallableEntriesAsProgramEntry">Помечать все входы в программу как существенные.</param>
        /// <param name="isMarkAllEntriesAsProgramEntry">Помечать все вызванные входы в программу как существенные.</param>
        /// <param name="isMarkCalledFunctions">Помечать вызванные функции как существенные.</param>
        /// <param name="necessaryFunctionsList">Список файлов с функциями, значимость которых обоснована разработчиком.</param>
        /// <param name="internalFunctionsFiles">Список файлов с функциями, являющимися встроенными в язык программирования.</param>
        /// <param name="controlFunctionsFile">Список функций, появление которых в трассах надо протоколировать.</param>
        private void DARP_Prepare(
            string testMaterialsPath,
            DynamicAnalysisResultsProcessing.Modes mode = DynamicAnalysisResultsProcessing.Modes.NDV3,
            string keyWord = "",
            long rangeFrom = -1,
            long rangeTo = -1,
            ulong sensorOffset = 0,
            bool isMarkNotCallableEntriesAsProgramEntry = false,
            bool isMarkAllEntriesAsProgramEntry = false,
            bool isMarkCalledFunctions = false,
            List<string> necessaryFunctionsList = null,
            List<string> internalFunctionsFiles = null,
            string controlFunctionsFile = "",
            bool isGenerateRestoreTrace = true
        )
        {
            if (testMaterialsPath.Equals(testUtils.TestMatirialsDirectoryPath))
                testMaterialsPath = Path.Combine(testMaterialsPath, String.Format(@"DynamicAnalysisResultProcessing\{0}\trace\", testUtils.TestName));

            IBufferWriter buf = WriterPool.Get();

            buf.Add(false); //режим работы всегда обычный
            buf.Add(testMaterialsPath);
            buf.Add(Convert.ToInt32(mode));
            buf.Add(!String.IsNullOrEmpty(keyWord));
            buf.Add(keyWord);
            buf.Add(!((rangeFrom == -1) || (rangeTo == -1)));
            buf.Add(rangeFrom);
            buf.Add(rangeTo);
            buf.Add(sensorOffset != 0);
            buf.Add(sensorOffset);
            buf.Add(isMarkNotCallableEntriesAsProgramEntry);
            buf.Add(isMarkAllEntriesAsProgramEntry);
            buf.Add(isMarkCalledFunctions);

            if (necessaryFunctionsList != null)
            {
                buf.Add(necessaryFunctionsList.Count);
                foreach (string s in necessaryFunctionsList)
                    buf.Add(s);
            }
            else
            {
                buf.Add(0);
            }

            buf.Add(controlFunctionsFile);

            if (internalFunctionsFiles != null)
            {
                buf.Add(internalFunctionsFiles.Count);
                foreach (string s in internalFunctionsFiles)
                    buf.Add(s);
            }
            else
            {
                buf.Add(0);
            }

            buf.Add(isGenerateRestoreTrace);

            buf.Add(0);

            currentStorage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.DYNAMIC_ANALYSIS_RESULTS_PROCESSING, buf);
        }

        /// <summary>
        /// Метод проверки отчётов по плагину "Обработка результатов динамического анализа"
        /// </summary>
        /// <param name="reportsPath">Путь до директории с отчётами</param>
        /// <param name="testMaterialsPath">Путь до директории с материалами для тестирования</param>
        /// <returns></returns>
        private bool DARP_CheckReports(string reportsPath, string testMaterialsPath)
        {
            const string coverSubPath = @"Проверка покрытия трасс\";
            string etalonReportsPath = Path.Combine(testMaterialsPath, String.Format(@"DynamicAnalysisResultProcessing\{0}\report\", testUtils.TestName));
            Assert.IsTrue(TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(reportsPath, coverSubPath), Path.Combine(etalonReportsPath, coverSubPath)),
                "Отчёт проверки покрытия после проведения теста не совпадает с эталоном.");

            if (testUtils.TestName.Contains("CoverOnly"))
                return true;

            string etalonCorrectnessPath = Path.Combine(etalonReportsPath, @"Проверка корректности трасс\");
            string testCorrectnessPath = Path.Combine(reportsPath, @"Проверка корректности трасс\");

            List<string> etalonFiles = DirectoryController.GetFiles(etalonCorrectnessPath);
            List<string> testFiles = DirectoryController.GetFiles(testCorrectnessPath);

            Assert.IsTrue(etalonFiles.Count == testFiles.Count,
                "Количество архивированных отчётов в эталоне {0} тестового.", etalonFiles.Count > testFiles.Count ? "больше" : "меньше");

            foreach (string etalonGzFile in etalonFiles)
            {
                string testGzFile = Path.Combine(testCorrectnessPath, Path.GetFileName(etalonGzFile ?? String.Empty));

                Assert.IsTrue(FileController.IsExists(testGzFile),
                    "Эталонный отчёт {0} не найден среди тестовых.", Path.GetFileName(etalonGzFile));

                TestUtilsClass.Report_7z_Compare(etalonGzFile, testGzFile,
                    currentStorage, Store.Const.PluginIdentifiers.DYNAMIC_ANALYSIS_RESULTS_PROCESSING,
                    (f1, f2) => TestUtilsClass.Reports_TXT_Compare(f1, f2));
            }

            return true;
        }

        /// <summary>
        /// Запуск тестирования плагина
        /// </summary>
        /// <param name="customizeStorageAction">Действие по подготовке Хранилища перед запуском теста</param>
        private void DARP_RunTest(
            TestUtilsClass.CustomizeStorage customizeStorageAction,
            TestUtilsClass.CheckReports checkReportsAction
        )
        {
            testUtils.RunTest(
                string.Empty,
                new DynamicAnalysisResultsProcessing(),
                false, //useCached
                null, //PrepareCached
                customizeStorageAction,
                (a, b) => true, //checkStorage
                checkReportsAction, //checkReports
                false, //rerun
                null,
                null,
                null
            );
        }
        #endregion
    }
}
