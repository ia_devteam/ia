// Этот файл используется анализом кода для поддержки атрибутов 
// SuppressMessage, примененных в этом проекте. 
// Подавления на уровне проекта либо не имеют целевого объекта, либо для них задан 
// конкретный объект и область пространства имен, тип, член и т. д.
//
// Чтобы добавить подавление к этому файлу, щелкните правой кнопкой 
// мыши сообщение в списке ошибок, укажите на команду "Подавить сообщения" и выберите вариант 
// "В файле проекта для блокируемых предупреждений".
// Нет необходимости вручную добавлять подавления к этому файлу.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable", Scope = "type", Target = "Analyse.TraceChecker")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Scope = "member", Target = "Analyse.TraceChecker.#pluginDependency()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Analyse")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Проверить аргументы или открытые методы", MessageId = "0", Scope = "member", Target = "Analyse.Settings.#.ctor(Analyse.TraceChecker)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Не передавать литералы в качестве локализованных параметров", MessageId = "Store.Log.Warning(System.String)", Scope = "member", Target = "Analyse.Logger.#EntryFunction(Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Не передавать литералы в качестве локализованных параметров", MessageId = "Store.Log.Warning(System.String)", Scope = "member", Target = "Analyse.Logger.#FunctionCall(Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Не передавать литералы в качестве локализованных параметров", MessageId = "Store.Log.Warning(System.String)", Scope = "member", Target = "Analyse.Logger.#NextSensor(Analyse.CallStack,Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Не передавать литералы в качестве локализованных параметров", MessageId = "System.Console.Write(System.String)", Scope = "member", Target = "Analyse.Logger.#NextSensor(Analyse.CallStack,Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Не передавать литералы в качестве локализованных параметров", MessageId = "Store.Log.Warning(System.String)", Scope = "member", Target = "Analyse.Logger.#PopStackFunction(Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Не передавать литералы в качестве локализованных параметров", MessageId = "Store.Log.Warning(System.String)", Scope = "member", Target = "Analyse.Logger.#SensorInSameFunction(Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Int32.ToString", Scope = "member", Target = "Analyse.Logger.#NextSensor(Analyse.CallStack,Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.UInt64.ToString", Scope = "member", Target = "Analyse.Logger.#EntryFunction(Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.UInt64.ToString", Scope = "member", Target = "Analyse.Logger.#NextSensor(Analyse.CallStack,Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.UInt64.ToString", Scope = "member", Target = "Analyse.Logger.#SensorInSameFunction(Store.Sensor)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "storage", Scope = "member", Target = "Analyse.TraceChecker.#Initialize(Store.Storage,Store.Table.IBufferReader)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1824:MarkAssembliesWithNeutralResourcesLanguage")]
