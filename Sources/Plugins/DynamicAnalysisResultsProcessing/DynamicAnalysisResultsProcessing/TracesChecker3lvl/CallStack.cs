using System;
using System.Collections.Generic;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing.NDV3
{
    /// <summary>
    /// ����� ������������ ��� �������� ����� �������
    /// </summary>
    internal sealed class CallStack
    {
        /// <summary>
        /// ����������� ������
        /// </summary>
        internal CallStack(Logger logger)
        {
            cursor = -1;
            peek = 0;
            this.logger = logger;
        }

        /// <summary>
        /// �������� ������� �� ������� �����. ���������� �������� ������, ���������� �������� �� PopNotRemove.
        /// </summary>
        /// <param name="sensorId">������ ��� ���������� � ����.</param>
        internal void Push(UInt64 sensorId)
        {
            //��������
            if (array.Count <= peek)
                array.Add(sensorId);
            else
                array[peek] = sensorId;

            peek++;

            //������ ����� �����, ���� �� ��������
            if (peek > 10 * 1024 * 1024 / 64)
            {
                array.RemoveRange(0, 5 * 1024 * 1024 / 64);
                peek -= 5 * 1024 * 1024 / 64;
            }

            cursor = peek - 1;
        }

        /// <summary>
        /// ��������� ��������� ����� �� �����, �� ������ �������� �� �����. ���������� �������, �� ������� ��������� ������ � ���������� �� ���� �����.
        /// </summary>
        /// <returns></returns>
        internal UInt64 PopNotRemove()
        {
            cursor--;
            return array[cursor + 1];
        }

        /// <summary>
        /// ������� ������� ������� ����� ��� ��������.
        /// </summary>
        /// <returns></returns>
        internal UInt64 Peek()
        {
            return array[cursor];
        }

        /// <summary>
        /// ������������ �� ��� ������ PopNotRemove ��� �������� �����.
        /// </summary>
        /// <returns></returns>
        internal bool IsBottom()
        {
            return cursor < 0;
        }

        /// <summary>
        /// ������� �����, ����������� ��� ������ ������� PopNotRemove. � ������� ����� ��������� ��� ��������, ������������ ������� � ��������
        /// �� ��� ���� �������. ��� ���������� ��-�� ����, ��� 1 �� �����, ������������� ����������� PopNotRemove, � 1 ��-�� ����, ��� ��������������� 
        /// ������� ������� �� ���������.
        /// </summary>
        internal void AcceptPop()
        {
            for (int i = peek - 1; i > cursor; i--)
                logger.PopStackFunction(array[i]);

            peek = cursor + 1;
        }

        /// <summary>
        /// ���������� �� ����� ������� �������
        /// </summary>
        /// <param name="curSensorId">������ ������ �� �������, ��������� ������������ �������� ������������ ������������</param>
        internal void PopTopFunction(UInt64 curSensorId)
        {
            logger.PopStackFunction(curSensorId);
            peek -= 1;
            cursor = peek - 1;
        }


        /// <summary>
        /// �������� �����, ����������� PopNotRemove. ������ ������������ �� ������� �����.
        /// </summary>
        internal void DeclinePop()
        {
            cursor = peek - 1;
        }

        /// <summary>
        /// ����
        /// </summary>
        internal List<UInt64> array = new List<UInt64>();

        /// <summary>
        /// ��������� �� �������� �����.
        /// </summary>
        int cursor;

        /// <summary>
        /// ���� ���������� ��������� �����.
        /// </summary>
        internal int peek;

        /// <summary>
        /// ������ ��� ������� �������
        /// </summary>
        Logger logger;
    }
}
