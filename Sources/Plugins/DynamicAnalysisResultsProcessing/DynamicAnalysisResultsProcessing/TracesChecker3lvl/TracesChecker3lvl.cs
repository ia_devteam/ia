﻿using System;
using System.Collections.Generic;

using Store;
using IA.Extensions;
using IA.Plugins.Analyses.DynamicAnalysisResultsProcessing.NDV3;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    /// <summary>
    /// Модуль проверки трасс в соответсвии с 3 уровнем контроля НДВ
    /// </summary>
    public sealed class TracesChecker3lvl
    {
        /// <summary>
        /// Текущее Хранилище для работы.
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Двумерная матрица вызовов функций
        /// </summary>
        private HashSet<UInt64> []calles;

        /// <summary>
        /// Для каждого датчика ставит в соответствие идентификатор функции, в которой он реализован
        /// </summary>
        private UInt64[] sensorFunctions;

        /// <summary>
        /// Грязный хак на ограничение количества обработанных трасс.
        /// </summary>
        int maxSensorToProceed = 1310720; //5 Mb
        int sensorsProceeded;

        /// <summary>
        /// Функция заполнения матрицы вызовов функций. Используется для оптимизации.
        /// </summary>
        internal void FillFunctionCallsTable()
        {
            //Заполняем calles
            List<HashSet<UInt64>> res = new List<HashSet<UInt64>>();
            ulong counter = 0;

            foreach (IFunction func in storage.functions.EnumerateFunctions(enFunctionKind.ALL))
            {
                if (res.Count <= (int)func.Id || res[(int)func.Id] != null)
                {
                    if ((int)func.Id > res.Count - 1)
                        for (int i = res.Count; i <= (int)func.Id; i++)
                            res.Add(null);

                    res[(int)func.Id] = new HashSet<ulong>();
                }

                foreach (IFunctionCall funcCalls in func.Calles())
                    res[(int)func.Id].Add(funcCalls.Calles.Id);

                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.NDV3_FUNCTIONS_CALLS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }

            calles = res.ToArray();

            //Заполняем sensorFunctions
            sensorFunctions = new UInt64[storage.sensors.GetMaxSensorNumber() + 1];

            foreach(Sensor sens in storage.sensors.EnumerateSensors())
            {
                sensorFunctions[sens.ID] = sens.GetFunctionID();
            }
        }

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        internal TracesChecker3lvl(Storage storage)
        {
            this.storage = storage;
            sensorsProceeded = 0;
        }

        /// <summary>
        /// Выполнить проверку для трассы.
        /// </summary>
        /// <param name="trace">Трасса для проверки.</param>
        internal void CheckTrace(ITrace trace)
        {
            //Если трасса не передана, то выходим
            if (trace == null)
                return;

            //Заводим новый чистый объект класса журналирования
            Logger logger = new Logger(storage, trace.Id);
            logger.Clear();

            //Запуск журналирования
            logger.AnalyseStarted();

            //Заводим чистый стек
            CallStack callStack = new CallStack(logger);
            Sensor sensor;

            //Основной цикл обработки трассы
            while ((sensor = trace.NextSensor()) != null)
            {
                if (++sensorsProceeded > maxSensorToProceed)
                    break;
                UInt64 sensorId = sensor.ID;
                var sensorFunctionID = sensorFunctions[sensorId];

                //если функции в Хранилище нет, то это не есть хорошо
                if (sensorFunctionID == Store.Const.CONSTS.WrongID)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось найти функцию, соответствующую датчику: " + sensor.ID);
                    continue;
                }

                //отмечаем вызов датчика для оценки покрытия
                if (DynamicAnalysisResultsProcessing.cover.Call(sensorId)) //если это первое найденное использование данного датчика, то логируем данный факт
                    logger.SensorIsCalled(sensor);


                if (callStack.IsBottom()) //Если это начало трассы. Либо если был выход из функции main (или её аналога)
                {
                    callStack.Push(sensorId);
                    logger.EntryFunction(sensor);
                    continue;
                }

                //Смотрим, является ли датчик началом функции. Если является, то приоритетно пытаемся найти вызов функции
                if(sensor.Kind == Kind.START)
                {
                    //Осуществляем поиск вызова данной функции откуда-то из глубин стека (сверху вниз)
                    if (FindCall(callStack, sensorFunctionID))
                        //В случае, если мы обнаружили вызов функции
                        logger.FunctionCall(sensor);
                    
                    else
                      //Непредвиденный переход
                        logger.EntryFunction(sensor);
                    

                    //Добавляем финальную точку в стек
                    callStack.Push(sensorId);

                    continue;
                }

                bool is_same_function; //Переход в пределах функции
                bool is_call; //Вызов функции
                FindTransition(callStack, sensorFunctionID, out is_same_function, out is_call);

                //Если переход существует, то считаем, что мы видим выход из функций.
                //Если переход не существует, то считаем, что мы видим новый вход в программу.
                if (is_same_function | is_call)
                {
                    if (is_same_function)
                    {
                        if (sensor.Kind == Kind.FINAL) //Если датчик является выходом из функции
                           //Снимаем его с вершины стека
                            //Снятие с вершины стека производится на всех уровнях контроля
                            callStack.PopTopFunction(sensorId);
                        else //Если это датчик в той же самой функции, но не является выходом из функции. Просто отмечаем этот факт
                            logger.SensorInSameFunction(sensor);

                        //Датчик, в который перешли, в стек не добавляем. Всё равно он в той же функции, ничего не изменится
                    }
                    else //is_call = true;
                    {
                        logger.FunctionCall(sensor);

                        if (sensor.Kind == Kind.FINAL) //Если датчик является выходом из функции.
                        
                            //В стек ничего не пишем, а просто логируем выход из функции
                            logger.PopStackFunction(sensorId);
                        
                        else //Добавляем финальную точку в стек
                            callStack.Push(sensorId);
                    }
                }
                else
                {
                    if (sensor.Kind == Kind.FINAL) //Если датчик является выходом из функции
                       //Снимаем его с вершины стека
                        //Снятие с вершины стека производится на всех уровнях контроля
                        logger.PopStackFunction(sensorId);
                    
                    else
                    {
                        logger.EntryFunction(sensor);

                        //Добавляем финальную точку в стек
                        callStack.Push(sensorId);
                    }
                }

            }

            //Остановка журналирования
            logger.AnalyseFinished();
            logger.Close();

            //закрываем трассу
            trace.Close();
        }

        /// <summary>
        /// Найти ближайший по стеку возможный вызов функции
        /// </summary>
        /// <param name="callStack"></param>
        /// <param name="sensorFunctionID"></param>
        /// <returns></returns>
        private bool FindCall(CallStack callStack, ulong sensorFunctionID)
        {
            bool isCall = false;

            while (!callStack.IsBottom())
            {
                if (calles[(int)sensorFunctions[callStack.Peek()]].Contains(sensorFunctionID))
                {
                    //Значит это правильный вызов
                    isCall = true;

                    //Убираем голову стека
                    callStack.AcceptPop();

                    break;
                }

                callStack.PopNotRemove();
            }

            if(!isCall) //Если переход не найден, то оставляем стек неизменным
                callStack.DeclinePop();

            return isCall;
        }

        /// <summary>
        /// Найти ближайший по стеку переход
        /// </summary>
        /// <param name="callStack"></param>
        /// <param name="sensorFunctionID"></param>
        /// <param name="is_same_function"></param>
        /// <param name="is_call"></param>
        private void FindTransition(CallStack callStack, ulong sensorFunctionID, out bool is_same_function, out bool is_call)
        {
            //Проверяем, существует ли переход в новый датчик из какого-либо датчика в стеке вызовов
            is_same_function = false;
            is_call = false;
            while (!callStack.IsBottom())
            {
                var stack_sensorFuncID = sensorFunctions[callStack.Peek()];
                if (stack_sensorFuncID == sensorFunctionID)
                //Если переход допустим
                {
                    is_same_function = true;
                    callStack.AcceptPop();
                    break;
                }
                else
                {
                    if (calles[(int)stack_sensorFuncID] != null && calles[(int)stack_sensorFuncID].Contains(sensorFunctionID))
                    {
                        is_call = true;
                        callStack.AcceptPop();
                        break;
                    }
                }
                
                //Идем к следующему элементу стека
                callStack.PopNotRemove();
            }

            if (!is_same_function && !is_call)
                //Если ничего не нашли, то и стек не меняем
                callStack.DeclinePop();

        }
    }
}
