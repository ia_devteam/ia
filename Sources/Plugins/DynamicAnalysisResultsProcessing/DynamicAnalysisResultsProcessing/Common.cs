﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading;

using IA.Extensions;
using IOController;


namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    using CustomExtensions;
    /// <summary>
    /// Класс со вспомогательным функционалом, который незачем держать в теле plugin.cs.
    /// </summary>
    class AuxiliaryJobs
    {
        /// <summary>
        /// Экземпляр класса прослушивания событий файловой системы. Используется для отслеживания состояния папки для импорта.
        /// </summary>
        FileSystemWatcher importFolderWatcher;

        /// <summary>
        /// Список обрабатываемых файлов.
        /// </summary>
        internal List<string> filesToProcess { get; private set; }

        /// <summary>
        /// Временный каталог для распаковки архивированных трасс.
        /// </summary>
        string tempDirectory;

        /// <summary>
        /// Количество обработанных файлов для отображения хода обработки.
        /// </summary>
        internal ulong importedFilesCount;

        /// <summary>
        /// Количество файлов для обработки с учётом уже обраотанных и удалённых (интерактивный режим).
        /// </summary>
        internal ulong totalFilesToProcess;

        /// <summary>
        /// Массив известных расширений архивов
        /// </summary>
        private string[] archiveExtensions = new string[] { ".7z", ".xz", ".bz2", ".gz", ".tar", ".tgz", ".zip", ".wim", ".arj", ".cab", ".chm", ".cpio", ".cramfs", ".deb", ".dmg", ".fat", ".hfs", ".iso", ".lzh", ".lzma", ".mbr", ".msi", ".nsis", ".ntfs", ".rar", ".rpm", ".squashfs", ".udf", ".vhd", ".wim", ".xar", ".z" };

        /// <summary>
        /// Конструктор для класса вспомогательных функций.
        /// </summary>
        /// <param name="tempDirectory">Временный каталог хранилища.</param>
        internal AuxiliaryJobs(string tempDirectory)
        {
            this.filesToProcess = new List<string>();
            this.tempDirectory = tempDirectory;
            importedFilesCount = 0;
        }

        /// <summary>
        /// Подготовка плагина к работе.
        /// </summary>
        /// <returns>True - если подготовка прошла успешна. False - в противном случае.</returns>
        internal bool PreparePlugin()
        {
            DynamicAnalysisResultsProcessing.STOP = false;

            //Добавляем в список для обработки то, что уже есть в папке
            totalFilesToProcess = filesToProcess.AddWCount(DirectoryController.GetFiles(PluginSettings.TracesDirectoryPath, true));
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.TRACES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, totalFilesToProcess);
            if (DynamicAnalysisResultsProcessing.progress.IsHandleCreated)
                DynamicAnalysisResultsProcessing.progress.BeginInvoke((Action<int>)DynamicAnalysisResultsProcessing.progress.UpdatePercent, 0);


            //Настройка основного режима
            if (!PluginSettings.IsInteractiveMode)
            {
                return true;
            }

            //Настройка интерактивного режима
            importFolderWatcher = new FileSystemWatcher
            {
                Path = PluginSettings.TracesDirectoryPath,
                NotifyFilter = NotifyFilters.DirectoryName
            };

            //Указываем, что мы хотим прослушивать именно эту папку

            importFolderWatcher.NotifyFilter = NotifyFilters.DirectoryName;

            importFolderWatcher.NotifyFilter |= NotifyFilters.FileName;
            importFolderWatcher.NotifyFilter |= NotifyFilters.Attributes;

            //Указываем, что нужно контролировать
            importFolderWatcher.Created += eventRaised;

            try
            {
                importFolderWatcher.EnableRaisingEvents = true;
            }
            catch (ArgumentException)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, "Запуск интерактивного режима невозможен. Доступ к папке закрыт.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Функция обработки файла с трассами.
        /// </summary>
        /// <param name="file">Путь к файлу для обработки.</param>
        internal void ProcessFile(string file)
        {
            if (String.IsNullOrWhiteSpace(file))
                return;

            FileStream fileStream;
            while (true)
            { //Проверка на доступность файла.
                int trycount = 0;
                try
                { //Если удачно открывается - идём на обработку.
                    fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
                    break;
                }
                catch (FileNotFoundException)
                { //Если файла не существует - уходим.
                    return;
                }
                catch (IOException)
                { //Если файл заблокирован - ждём-с.
                    trycount++;
                    if (trycount == 100)
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Не далось обработать файл <" + file + ">. Файл заблокирован.");
                        return;
                    }
                    Thread.Sleep(5000);
                    continue;
                }
            }

            if (archiveExtensions.Contains(Path.GetExtension(file).ToLower()))
            {
                foreach (FileOperations.ArchiveReader.ModifiedFileStream fs in FileOperations.ArchiveReader.Read(file, tempDirectory))
                {
                    if (File.Exists(fs.FilePath)) //иногда бывает и такое 0_о
                        DynamicAnalysisResultsProcessing.importer.ImportFile(fs);
                    if (DynamicAnalysisResultsProcessing.STOP)
                        break;
                }
            }
            else
            {
                //Запускаем импорт файла
                DynamicAnalysisResultsProcessing.importer.ImportFile(fileStream);
            }

            fileStream.Dispose();

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.TRACES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++importedFilesCount);
        }

        /// <summary>
        /// Функция обработчик событий в файловой системе.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void eventRaised(object sender, FileSystemEventArgs e)
        {
            int oldCount = filesToProcess.Count;
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Created:

                    string path = e.FullPath;

                    //захватываем разделяемый ресурс и добавляем элемент в список
                    lock (filesToProcess)
                    {
                        if (FileController.IsExists(path))
                        {
                            //Если был помещён файл
                            if (!filesToProcess.Contains(path))
                                totalFilesToProcess += filesToProcess.AddWCount(path);
                        }
                        else if (DirectoryController.IsExists(path))
                        {
                            //Если была помещена директория
                            foreach (string file in DirectoryController.GetFiles(path, true))
                                if (!filesToProcess.Contains(file))
                                    totalFilesToProcess += filesToProcess.AddWCount(file);
                        }
                        else
                            IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось обработать элемент: " + path);
                    }
                    break;
                default:
                    break;
            }
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.TRACES_PROCESSING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, totalFilesToProcess);
        }

        /// <summary>
        /// Функция реализущая работу потока обработчика папки с трассами.
        /// </summary>
        internal void ProcessThreadFunction()
        {
            //ОБЫЧНЫЙ РЕЖИМ
            if (!PluginSettings.IsInteractiveMode)
            {
                if (filesToProcess.Count == 0)
                {
                    Monitor.Log.Warning(PluginSettings.Name, "Файлы для обработки не обнаружены.");
                    return;
                }
                foreach (string file in filesToProcess)
                {
                    try
                    {
                        ProcessFile(file);
                    }
                    catch (PathTooLongException)
                    {
                        Monitor.Log.Error(PluginSettings.Name, "Ошибка чтения файла <" + file + ">. Слишком длинный путь.");
                    }
                    if (DynamicAnalysisResultsProcessing.STOP)
                        break;
                }

                return;
            }

            //ИНТЕРАКТИВНЫЙ РЕЖИМ
            IA.Monitor.Log.Information(PluginSettings.Name, "Интерактивный режим работы успешно запущен.");

            while (true)
            {
                if (DynamicAnalysisResultsProcessing.STOP)
                    break;

                //ждём пока в списке на обработку что-то появится
                if (filesToProcess.Count == 0)
                {
                    Thread.Sleep(1000);
                    continue;
                }

                //если что-то появилось, вынимаем
                string file = filesToProcess[0];

                try
                {
                    //обрабатываем
                    ProcessFile(file);
                }
                catch (PathTooLongException)
                {
                    Monitor.Log.Error(PluginSettings.Name, "Ошибка чтения файла <" + file + ">. Слишком длинный путь. Удалён.");
                }

                //удаляем обработанный файл из списка обработки
                lock (filesToProcess)
                {
                    if (filesToProcess.Contains(file))
                        filesToProcess.Remove(file);
                }

                //Удаляем файл после обработки
                if (FileController.IsExists(file))
                    FileController.Remove(file);

                //проверяем директории на пустоту (если какая-то директория стала пустой - удаляем)
                foreach (string directory in DirectoryController.GetSubDirectories(PluginSettings.TracesDirectoryPath, true))
                {
                    if (DirectoryController.IsExists(directory))
                    {
                        if (DirectoryController.GetFiles(directory, true).Count == 0)
                            DirectoryController.Remove(directory);
                    }
                }
            }

            IA.Monitor.Log.Information(PluginSettings.Name, "Интерактивный режим работы успешно остановлен.");
        }

    }

    namespace CustomExtensions
    {
        internal static class ListExtensions
        {
            internal static ulong AddWCount(this List<string> list, string item)
            {
                list.Add(item);
                return 1UL;
            }

            internal static ulong AddWCount(this List<string> list, IEnumerable<string> input)
            {
                int count = list.Count;
                list.AddRange(input);
                return (ulong)(list.Count - count);
            }

            internal static System.Collections.Specialized.StringCollection ToStringCollection(this List<string> list)
            {
                System.Collections.Specialized.StringCollection result = new System.Collections.Specialized.StringCollection();

                result.AddRange(list.ToArray());

                return result;
            }
        }

        internal static class EnumExtensions
        {
            internal static T ToEnum<T>(this int input, T defValue)
            {
                if (Enum.IsDefined(typeof(T), input))
                    return (T)Enum.ToObject(typeof(T), input);
                return defValue;
            }
        }

        internal static class StringCollectionExtensions
        {
            internal static List<string> ToList(this System.Collections.Specialized.StringCollection collection)
            {
                return collection.Cast<string>().ToList();
            }
        }
    }
}
