﻿namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    partial class CustomSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.keyWord_textBox = new System.Windows.Forms.TextBox();
            this.isRange_checkBox = new System.Windows.Forms.CheckBox();
            this.isKeyWord_checkBox = new System.Windows.Forms.CheckBox();
            this.isOffset_checkBox = new System.Windows.Forms.CheckBox();
            this.mode_groupBox = new System.Windows.Forms.GroupBox();
            this.mode_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.basicMode_radioButton = new System.Windows.Forms.RadioButton();
            this.interactiveMode_radioButton = new System.Windows.Forms.RadioButton();
            this.restrictions_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isGenerateRestoredTrace_checkBox = new System.Windows.Forms.CheckBox();
            this.rangeTo_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.rangeFrom_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.range_label = new System.Windows.Forms.Label();
            this.offset_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.checker_groupBox = new System.Windows.Forms.GroupBox();
            this.checker_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.isMarkCalledFunctions_checkBox = new System.Windows.Forms.CheckBox();
            this.isMarkNotCallableEntriesAsProgramEntry_checkBox = new System.Windows.Forms.CheckBox();
            this.isMarkAllEntriesAsProgramEntry_checkBox = new System.Windows.Forms.CheckBox();
            this.cover_groupBox = new System.Windows.Forms.GroupBox();
            this.cover_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.necessaryFunctionsFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.necessaryFunctionsFiles_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.necessoryFunctionsFiles_delete_button = new System.Windows.Forms.Button();
            this.necessoryFunctionsFiles_listBox = new System.Windows.Forms.ListBox();
            this.necessoryFunctionsFiles_add_button = new System.Windows.Forms.Button();
            this.controlFunctionsFilePath_groupBox = new System.Windows.Forms.GroupBox();
            this.controlFunctionsFilePath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.level_groupBox = new System.Windows.Forms.GroupBox();
            this.level_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.level3_radioButton = new System.Windows.Forms.RadioButton();
            this.level2_radioButton = new System.Windows.Forms.RadioButton();
            this.coverOnly_radioButton = new System.Windows.Forms.RadioButton();
            this.internalFunctionsFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.internalFunctionsFiles_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.internalFunctionsFiles_listBox = new System.Windows.Forms.ListBox();
            this.internalFunctionsFiles_add_button = new System.Windows.Forms.Button();
            this.internalFunctionsFiles_delete_button = new System.Windows.Forms.Button();
            this.restrictions_groupBox = new System.Windows.Forms.GroupBox();
            this.tracesPath_groupBox = new System.Windows.Forms.GroupBox();
            this.tracesPath_textbox = new IA.Controls.TextBoxWithDialogButton();
            this.foreignLibsPaths_groupbox = new System.Windows.Forms.GroupBox();
            this.foreignLibsPaths_textBox = new System.Windows.Forms.TextBox();
            this.mode_groupBox.SuspendLayout();
            this.mode_tableLayoutPanel.SuspendLayout();
            this.restrictions_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeTo_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeFrom_numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offset_numericUpDown)).BeginInit();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.checker_groupBox.SuspendLayout();
            this.checker_tableLayoutPanel.SuspendLayout();
            this.cover_groupBox.SuspendLayout();
            this.cover_tableLayoutPanel.SuspendLayout();
            this.necessaryFunctionsFiles_groupBox.SuspendLayout();
            this.necessaryFunctionsFiles_tableLayoutPanel.SuspendLayout();
            this.controlFunctionsFilePath_groupBox.SuspendLayout();
            this.level_groupBox.SuspendLayout();
            this.level_tableLayoutPanel.SuspendLayout();
            this.internalFunctionsFiles_groupBox.SuspendLayout();
            this.internalFunctionsFiles_tableLayoutPanel.SuspendLayout();
            this.restrictions_groupBox.SuspendLayout();
            this.tracesPath_groupBox.SuspendLayout();
            this.foreignLibsPaths_groupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // keyWord_textBox
            // 
            this.keyWord_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.restrictions_tableLayoutPanel.SetColumnSpan(this.keyWord_textBox, 3);
            this.keyWord_textBox.Location = new System.Drawing.Point(203, 3);
            this.keyWord_textBox.Name = "keyWord_textBox";
            this.keyWord_textBox.Size = new System.Drawing.Size(362, 20);
            this.keyWord_textBox.TabIndex = 15;
            // 
            // isRange_checkBox
            // 
            this.isRange_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isRange_checkBox.AutoSize = true;
            this.isRange_checkBox.Location = new System.Drawing.Point(3, 30);
            this.isRange_checkBox.Name = "isRange_checkBox";
            this.isRange_checkBox.Size = new System.Drawing.Size(194, 17);
            this.isRange_checkBox.TabIndex = 14;
            this.isRange_checkBox.Text = "Ограничить датчики диапазоном";
            this.isRange_checkBox.UseVisualStyleBackColor = true;
            this.isRange_checkBox.CheckedChanged += new System.EventHandler(this.isRange_checkBox_CheckedChanged);
            // 
            // isKeyWord_checkBox
            // 
            this.isKeyWord_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isKeyWord_checkBox.AutoSize = true;
            this.isKeyWord_checkBox.Location = new System.Drawing.Point(3, 4);
            this.isKeyWord_checkBox.Name = "isKeyWord_checkBox";
            this.isKeyWord_checkBox.Size = new System.Drawing.Size(194, 17);
            this.isKeyWord_checkBox.TabIndex = 13;
            this.isKeyWord_checkBox.Text = "Ключевое слово";
            this.isKeyWord_checkBox.UseVisualStyleBackColor = true;
            this.isKeyWord_checkBox.CheckedChanged += new System.EventHandler(this.isKeyWord_checkBox_CheckedChanged);
            // 
            // isOffset_checkBox
            // 
            this.isOffset_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isOffset_checkBox.AutoSize = true;
            this.isOffset_checkBox.Location = new System.Drawing.Point(3, 56);
            this.isOffset_checkBox.Name = "isOffset_checkBox";
            this.isOffset_checkBox.Size = new System.Drawing.Size(194, 17);
            this.isOffset_checkBox.TabIndex = 18;
            this.isOffset_checkBox.Text = "Сместить номера датчиков на";
            this.isOffset_checkBox.UseVisualStyleBackColor = true;
            this.isOffset_checkBox.CheckedChanged += new System.EventHandler(this.isOffset_checkBox_CheckedChanged);
            // 
            // mode_groupBox
            // 
            this.mode_groupBox.Controls.Add(this.mode_tableLayoutPanel);
            this.mode_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mode_groupBox.Location = new System.Drawing.Point(3, 3);
            this.mode_groupBox.Name = "mode_groupBox";
            this.mode_groupBox.Size = new System.Drawing.Size(574, 44);
            this.mode_groupBox.TabIndex = 22;
            this.mode_groupBox.TabStop = false;
            this.mode_groupBox.Text = "Режим обработки трасс";
            // 
            // mode_tableLayoutPanel
            // 
            this.mode_tableLayoutPanel.ColumnCount = 3;
            this.mode_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.mode_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.mode_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mode_tableLayoutPanel.Controls.Add(this.basicMode_radioButton, 0, 0);
            this.mode_tableLayoutPanel.Controls.Add(this.interactiveMode_radioButton, 1, 0);
            this.mode_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mode_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.mode_tableLayoutPanel.Name = "mode_tableLayoutPanel";
            this.mode_tableLayoutPanel.RowCount = 1;
            this.mode_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mode_tableLayoutPanel.Size = new System.Drawing.Size(568, 25);
            this.mode_tableLayoutPanel.TabIndex = 0;
            // 
            // basicMode_radioButton
            // 
            this.basicMode_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.basicMode_radioButton.AutoSize = true;
            this.basicMode_radioButton.Location = new System.Drawing.Point(3, 4);
            this.basicMode_radioButton.Name = "basicMode_radioButton";
            this.basicMode_radioButton.Size = new System.Drawing.Size(106, 17);
            this.basicMode_radioButton.TabIndex = 0;
            this.basicMode_radioButton.TabStop = true;
            this.basicMode_radioButton.Text = "Обычный";
            this.basicMode_radioButton.UseVisualStyleBackColor = true;
            // 
            // interactiveMode_radioButton
            // 
            this.interactiveMode_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.interactiveMode_radioButton.AutoSize = true;
            this.interactiveMode_radioButton.Location = new System.Drawing.Point(115, 4);
            this.interactiveMode_radioButton.Name = "interactiveMode_radioButton";
            this.interactiveMode_radioButton.Size = new System.Drawing.Size(106, 17);
            this.interactiveMode_radioButton.TabIndex = 1;
            this.interactiveMode_radioButton.TabStop = true;
            this.interactiveMode_radioButton.Text = "Интерактивный";
            this.interactiveMode_radioButton.UseVisualStyleBackColor = true;
            // 
            // restrictions_tableLayoutPanel
            // 
            this.restrictions_tableLayoutPanel.ColumnCount = 4;
            this.restrictions_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.restrictions_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.restrictions_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.restrictions_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.restrictions_tableLayoutPanel.Controls.Add(this.isGenerateRestoredTrace_checkBox, 0, 3);
            this.restrictions_tableLayoutPanel.Controls.Add(this.rangeTo_numericUpDown, 3, 1);
            this.restrictions_tableLayoutPanel.Controls.Add(this.keyWord_textBox, 1, 0);
            this.restrictions_tableLayoutPanel.Controls.Add(this.rangeFrom_numericUpDown, 1, 1);
            this.restrictions_tableLayoutPanel.Controls.Add(this.isRange_checkBox, 0, 1);
            this.restrictions_tableLayoutPanel.Controls.Add(this.isOffset_checkBox, 0, 2);
            this.restrictions_tableLayoutPanel.Controls.Add(this.isKeyWord_checkBox, 0, 0);
            this.restrictions_tableLayoutPanel.Controls.Add(this.range_label, 2, 1);
            this.restrictions_tableLayoutPanel.Controls.Add(this.offset_numericUpDown, 1, 2);
            this.restrictions_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.restrictions_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.restrictions_tableLayoutPanel.Name = "restrictions_tableLayoutPanel";
            this.restrictions_tableLayoutPanel.RowCount = 4;
            this.restrictions_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.restrictions_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.restrictions_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.restrictions_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.restrictions_tableLayoutPanel.Size = new System.Drawing.Size(568, 105);
            this.restrictions_tableLayoutPanel.TabIndex = 25;
            // 
            // isGenerateRestoredTrace_checkBox
            // 
            this.isGenerateRestoredTrace_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isGenerateRestoredTrace_checkBox.AutoSize = true;
            this.restrictions_tableLayoutPanel.SetColumnSpan(this.isGenerateRestoredTrace_checkBox, 4);
            this.isGenerateRestoredTrace_checkBox.Location = new System.Drawing.Point(3, 83);
            this.isGenerateRestoredTrace_checkBox.Name = "isGenerateRestoredTrace_checkBox";
            this.isGenerateRestoredTrace_checkBox.Size = new System.Drawing.Size(562, 17);
            this.isGenerateRestoredTrace_checkBox.TabIndex = 22;
            this.isGenerateRestoredTrace_checkBox.Text = "Генерировать восстановленные трассы";
            this.isGenerateRestoredTrace_checkBox.UseVisualStyleBackColor = true;
            // 
            // rangeTo_numericUpDown
            // 
            this.rangeTo_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rangeTo_numericUpDown.Location = new System.Drawing.Point(397, 29);
            this.rangeTo_numericUpDown.Name = "rangeTo_numericUpDown";
            this.rangeTo_numericUpDown.Size = new System.Drawing.Size(168, 20);
            this.rangeTo_numericUpDown.TabIndex = 1;
            this.rangeTo_numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // rangeFrom_numericUpDown
            // 
            this.rangeFrom_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rangeFrom_numericUpDown.Location = new System.Drawing.Point(203, 29);
            this.rangeFrom_numericUpDown.Name = "rangeFrom_numericUpDown";
            this.rangeFrom_numericUpDown.Size = new System.Drawing.Size(168, 20);
            this.rangeFrom_numericUpDown.TabIndex = 0;
            this.rangeFrom_numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // range_label
            // 
            this.range_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.range_label.AutoSize = true;
            this.range_label.Location = new System.Drawing.Point(377, 32);
            this.range_label.Name = "range_label";
            this.range_label.Size = new System.Drawing.Size(14, 13);
            this.range_label.TabIndex = 20;
            this.range_label.Text = "-";
            this.range_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // offset_numericUpDown
            // 
            this.offset_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.restrictions_tableLayoutPanel.SetColumnSpan(this.offset_numericUpDown, 3);
            this.offset_numericUpDown.Location = new System.Drawing.Point(203, 55);
            this.offset_numericUpDown.Name = "offset_numericUpDown";
            this.offset_numericUpDown.Size = new System.Drawing.Size(362, 20);
            this.offset_numericUpDown.TabIndex = 21;
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.mode_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Controls.Add(this.checker_groupBox, 0, 5);
            this.settings_tableLayoutPanel.Controls.Add(this.cover_groupBox, 0, 6);
            this.settings_tableLayoutPanel.Controls.Add(this.level_groupBox, 0, 2);
            this.settings_tableLayoutPanel.Controls.Add(this.internalFunctionsFiles_groupBox, 0, 7);
            this.settings_tableLayoutPanel.Controls.Add(this.restrictions_groupBox, 0, 4);
            this.settings_tableLayoutPanel.Controls.Add(this.tracesPath_groupBox, 0, 1);
            this.settings_tableLayoutPanel.Controls.Add(this.foreignLibsPaths_groupbox, 0, 3);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 8;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.65863F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.34137F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(580, 865);
            this.settings_tableLayoutPanel.TabIndex = 26;
            // 
            // checker_groupBox
            // 
            this.checker_groupBox.Controls.Add(this.checker_tableLayoutPanel);
            this.checker_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checker_groupBox.Location = new System.Drawing.Point(3, 388);
            this.checker_groupBox.Name = "checker_groupBox";
            this.checker_groupBox.Size = new System.Drawing.Size(574, 94);
            this.checker_groupBox.TabIndex = 26;
            this.checker_groupBox.TabStop = false;
            this.checker_groupBox.Text = "Установление существенности функций на основе анализа трасс";
            // 
            // checker_tableLayoutPanel
            // 
            this.checker_tableLayoutPanel.ColumnCount = 1;
            this.checker_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.checker_tableLayoutPanel.Controls.Add(this.isMarkCalledFunctions_checkBox, 0, 2);
            this.checker_tableLayoutPanel.Controls.Add(this.isMarkNotCallableEntriesAsProgramEntry_checkBox, 0, 0);
            this.checker_tableLayoutPanel.Controls.Add(this.isMarkAllEntriesAsProgramEntry_checkBox, 0, 1);
            this.checker_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checker_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.checker_tableLayoutPanel.Name = "checker_tableLayoutPanel";
            this.checker_tableLayoutPanel.RowCount = 3;
            this.checker_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.checker_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.checker_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.checker_tableLayoutPanel.Size = new System.Drawing.Size(568, 75);
            this.checker_tableLayoutPanel.TabIndex = 0;
            // 
            // isMarkCalledFunctions_checkBox
            // 
            this.isMarkCalledFunctions_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isMarkCalledFunctions_checkBox.AutoSize = true;
            this.isMarkCalledFunctions_checkBox.Checked = true;
            this.isMarkCalledFunctions_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.isMarkCalledFunctions_checkBox.Location = new System.Drawing.Point(3, 53);
            this.isMarkCalledFunctions_checkBox.Name = "isMarkCalledFunctions_checkBox";
            this.isMarkCalledFunctions_checkBox.Size = new System.Drawing.Size(562, 17);
            this.isMarkCalledFunctions_checkBox.TabIndex = 29;
            this.isMarkCalledFunctions_checkBox.Text = "Помечать все вызываемые функции как существенные";
            this.isMarkCalledFunctions_checkBox.UseVisualStyleBackColor = true;
            // 
            // isMarkNotCallableEntriesAsProgramEntry_checkBox
            // 
            this.isMarkNotCallableEntriesAsProgramEntry_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isMarkNotCallableEntriesAsProgramEntry_checkBox.AutoSize = true;
            this.isMarkNotCallableEntriesAsProgramEntry_checkBox.Location = new System.Drawing.Point(3, 3);
            this.isMarkNotCallableEntriesAsProgramEntry_checkBox.Name = "isMarkNotCallableEntriesAsProgramEntry_checkBox";
            this.isMarkNotCallableEntriesAsProgramEntry_checkBox.Size = new System.Drawing.Size(562, 17);
            this.isMarkNotCallableEntriesAsProgramEntry_checkBox.TabIndex = 27;
            this.isMarkNotCallableEntriesAsProgramEntry_checkBox.Text = "Помечать функции с непредвиденным переходом и не имеющие вызовов, как существенны" +
    "е";
            this.isMarkNotCallableEntriesAsProgramEntry_checkBox.UseVisualStyleBackColor = true;
            // 
            // isMarkAllEntriesAsProgramEntry_checkBox
            // 
            this.isMarkAllEntriesAsProgramEntry_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isMarkAllEntriesAsProgramEntry_checkBox.AutoSize = true;
            this.isMarkAllEntriesAsProgramEntry_checkBox.Location = new System.Drawing.Point(3, 28);
            this.isMarkAllEntriesAsProgramEntry_checkBox.Name = "isMarkAllEntriesAsProgramEntry_checkBox";
            this.isMarkAllEntriesAsProgramEntry_checkBox.Size = new System.Drawing.Size(562, 17);
            this.isMarkAllEntriesAsProgramEntry_checkBox.TabIndex = 28;
            this.isMarkAllEntriesAsProgramEntry_checkBox.Text = "Помечать функции с непредвиденным переходом как существенные";
            this.isMarkAllEntriesAsProgramEntry_checkBox.UseVisualStyleBackColor = true;
            // 
            // cover_groupBox
            // 
            this.cover_groupBox.Controls.Add(this.cover_tableLayoutPanel);
            this.cover_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cover_groupBox.Location = new System.Drawing.Point(3, 488);
            this.cover_groupBox.Name = "cover_groupBox";
            this.cover_groupBox.Size = new System.Drawing.Size(574, 239);
            this.cover_groupBox.TabIndex = 27;
            this.cover_groupBox.TabStop = false;
            this.cover_groupBox.Text = "Проверка покрытия трасс";
            // 
            // cover_tableLayoutPanel
            // 
            this.cover_tableLayoutPanel.ColumnCount = 1;
            this.cover_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cover_tableLayoutPanel.Controls.Add(this.necessaryFunctionsFiles_groupBox, 0, 0);
            this.cover_tableLayoutPanel.Controls.Add(this.controlFunctionsFilePath_groupBox, 0, 1);
            this.cover_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cover_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.cover_tableLayoutPanel.Name = "cover_tableLayoutPanel";
            this.cover_tableLayoutPanel.RowCount = 2;
            this.cover_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cover_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.cover_tableLayoutPanel.Size = new System.Drawing.Size(568, 220);
            this.cover_tableLayoutPanel.TabIndex = 0;
            // 
            // necessaryFunctionsFiles_groupBox
            // 
            this.necessaryFunctionsFiles_groupBox.Controls.Add(this.necessaryFunctionsFiles_tableLayoutPanel);
            this.necessaryFunctionsFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.necessaryFunctionsFiles_groupBox.Location = new System.Drawing.Point(3, 3);
            this.necessaryFunctionsFiles_groupBox.Name = "necessaryFunctionsFiles_groupBox";
            this.necessaryFunctionsFiles_groupBox.Size = new System.Drawing.Size(562, 164);
            this.necessaryFunctionsFiles_groupBox.TabIndex = 0;
            this.necessaryFunctionsFiles_groupBox.TabStop = false;
            this.necessaryFunctionsFiles_groupBox.Text = "Файлы с именами избыточных функций, для которых разработчик дал обоснование их не" +
    "обходимости";
            // 
            // necessaryFunctionsFiles_tableLayoutPanel
            // 
            this.necessaryFunctionsFiles_tableLayoutPanel.ColumnCount = 2;
            this.necessaryFunctionsFiles_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.necessaryFunctionsFiles_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.necessaryFunctionsFiles_tableLayoutPanel.Controls.Add(this.necessoryFunctionsFiles_delete_button, 1, 1);
            this.necessaryFunctionsFiles_tableLayoutPanel.Controls.Add(this.necessoryFunctionsFiles_listBox, 0, 0);
            this.necessaryFunctionsFiles_tableLayoutPanel.Controls.Add(this.necessoryFunctionsFiles_add_button, 1, 0);
            this.necessaryFunctionsFiles_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.necessaryFunctionsFiles_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.necessaryFunctionsFiles_tableLayoutPanel.Name = "necessaryFunctionsFiles_tableLayoutPanel";
            this.necessaryFunctionsFiles_tableLayoutPanel.RowCount = 3;
            this.necessaryFunctionsFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.necessaryFunctionsFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.necessaryFunctionsFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.necessaryFunctionsFiles_tableLayoutPanel.Size = new System.Drawing.Size(556, 145);
            this.necessaryFunctionsFiles_tableLayoutPanel.TabIndex = 3;
            // 
            // necessoryFunctionsFiles_delete_button
            // 
            this.necessoryFunctionsFiles_delete_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.necessoryFunctionsFiles_delete_button.Location = new System.Drawing.Point(459, 33);
            this.necessoryFunctionsFiles_delete_button.Name = "necessoryFunctionsFiles_delete_button";
            this.necessoryFunctionsFiles_delete_button.Size = new System.Drawing.Size(94, 24);
            this.necessoryFunctionsFiles_delete_button.TabIndex = 2;
            this.necessoryFunctionsFiles_delete_button.Text = "Удалить";
            this.necessoryFunctionsFiles_delete_button.UseVisualStyleBackColor = true;
            this.necessoryFunctionsFiles_delete_button.Click += new System.EventHandler(this.necessoryFunctionsFiles_delete_button_Click);
            // 
            // necessoryFunctionsFiles_listBox
            // 
            this.necessoryFunctionsFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.necessoryFunctionsFiles_listBox.FormattingEnabled = true;
            this.necessoryFunctionsFiles_listBox.Location = new System.Drawing.Point(3, 3);
            this.necessoryFunctionsFiles_listBox.Name = "necessoryFunctionsFiles_listBox";
            this.necessaryFunctionsFiles_tableLayoutPanel.SetRowSpan(this.necessoryFunctionsFiles_listBox, 3);
            this.necessoryFunctionsFiles_listBox.Size = new System.Drawing.Size(450, 139);
            this.necessoryFunctionsFiles_listBox.TabIndex = 0;
            this.necessoryFunctionsFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.necessoryFunctionsFiles_listBox_SelectedIndexChanged);
            // 
            // necessoryFunctionsFiles_add_button
            // 
            this.necessoryFunctionsFiles_add_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.necessoryFunctionsFiles_add_button.Location = new System.Drawing.Point(459, 3);
            this.necessoryFunctionsFiles_add_button.Name = "necessoryFunctionsFiles_add_button";
            this.necessoryFunctionsFiles_add_button.Size = new System.Drawing.Size(94, 24);
            this.necessoryFunctionsFiles_add_button.TabIndex = 1;
            this.necessoryFunctionsFiles_add_button.Text = "Добавить";
            this.necessoryFunctionsFiles_add_button.UseVisualStyleBackColor = true;
            this.necessoryFunctionsFiles_add_button.Click += new System.EventHandler(this.necessoryFunctionsFiles_add_button_Click);
            // 
            // controlFunctionsFilePath_groupBox
            // 
            this.controlFunctionsFilePath_groupBox.Controls.Add(this.controlFunctionsFilePath_textBox);
            this.controlFunctionsFilePath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlFunctionsFilePath_groupBox.Location = new System.Drawing.Point(3, 173);
            this.controlFunctionsFilePath_groupBox.Name = "controlFunctionsFilePath_groupBox";
            this.controlFunctionsFilePath_groupBox.Size = new System.Drawing.Size(562, 44);
            this.controlFunctionsFilePath_groupBox.TabIndex = 1;
            this.controlFunctionsFilePath_groupBox.TabStop = false;
            this.controlFunctionsFilePath_groupBox.Text = "Файл со списоком функций для проведения контроля вхождения";
            // 
            // controlFunctionsFilePath_textBox
            // 
            this.controlFunctionsFilePath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlFunctionsFilePath_textBox.Location = new System.Drawing.Point(3, 16);
            this.controlFunctionsFilePath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.controlFunctionsFilePath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.controlFunctionsFilePath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.controlFunctionsFilePath_textBox.Name = "controlFunctionsFilePath_textBox";
            this.controlFunctionsFilePath_textBox.Size = new System.Drawing.Size(556, 25);
            this.controlFunctionsFilePath_textBox.TabIndex = 0;
            // 
            // level_groupBox
            // 
            this.level_groupBox.Controls.Add(this.level_tableLayoutPanel);
            this.level_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_groupBox.Location = new System.Drawing.Point(3, 108);
            this.level_groupBox.Name = "level_groupBox";
            this.level_groupBox.Size = new System.Drawing.Size(574, 44);
            this.level_groupBox.TabIndex = 29;
            this.level_groupBox.TabStop = false;
            this.level_groupBox.Text = "Уровень НДВ";
            // 
            // level_tableLayoutPanel
            // 
            this.level_tableLayoutPanel.ColumnCount = 3;
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.level_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Controls.Add(this.level3_radioButton, 1, 0);
            this.level_tableLayoutPanel.Controls.Add(this.level2_radioButton, 0, 0);
            this.level_tableLayoutPanel.Controls.Add(this.coverOnly_radioButton, 2, 0);
            this.level_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.level_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.level_tableLayoutPanel.Name = "level_tableLayoutPanel";
            this.level_tableLayoutPanel.RowCount = 1;
            this.level_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.level_tableLayoutPanel.Size = new System.Drawing.Size(568, 25);
            this.level_tableLayoutPanel.TabIndex = 1;
            // 
            // level3_radioButton
            // 
            this.level3_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.level3_radioButton.AutoSize = true;
            this.level3_radioButton.Location = new System.Drawing.Point(93, 4);
            this.level3_radioButton.Name = "level3_radioButton";
            this.level3_radioButton.Size = new System.Drawing.Size(84, 17);
            this.level3_radioButton.TabIndex = 1;
            this.level3_radioButton.TabStop = true;
            this.level3_radioButton.Text = "3-й уровень";
            this.level3_radioButton.UseVisualStyleBackColor = true;
            // 
            // level2_radioButton
            // 
            this.level2_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.level2_radioButton.AutoSize = true;
            this.level2_radioButton.Checked = true;
            this.level2_radioButton.Location = new System.Drawing.Point(3, 4);
            this.level2_radioButton.Name = "level2_radioButton";
            this.level2_radioButton.Size = new System.Drawing.Size(84, 17);
            this.level2_radioButton.TabIndex = 0;
            this.level2_radioButton.TabStop = true;
            this.level2_radioButton.Text = "2-й уровень";
            this.level2_radioButton.UseVisualStyleBackColor = true;
            // 
            // coverOnly_radioButton
            // 
            this.coverOnly_radioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.coverOnly_radioButton.AutoSize = true;
            this.coverOnly_radioButton.Location = new System.Drawing.Point(183, 4);
            this.coverOnly_radioButton.Name = "coverOnly_radioButton";
            this.coverOnly_radioButton.Size = new System.Drawing.Size(382, 17);
            this.coverOnly_radioButton.TabIndex = 2;
            this.coverOnly_radioButton.TabStop = true;
            this.coverOnly_radioButton.Text = "Быстрое вычисление покрытия";
            this.coverOnly_radioButton.UseVisualStyleBackColor = true;
            // 
            // internalFunctionsFiles_groupBox
            // 
            this.internalFunctionsFiles_groupBox.Controls.Add(this.internalFunctionsFiles_tableLayoutPanel);
            this.internalFunctionsFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.internalFunctionsFiles_groupBox.Location = new System.Drawing.Point(3, 733);
            this.internalFunctionsFiles_groupBox.Name = "internalFunctionsFiles_groupBox";
            this.internalFunctionsFiles_groupBox.Size = new System.Drawing.Size(574, 129);
            this.internalFunctionsFiles_groupBox.TabIndex = 31;
            this.internalFunctionsFiles_groupBox.TabStop = false;
            this.internalFunctionsFiles_groupBox.Text = "Файлы со списками встроенных функций в языки программирования";
            // 
            // internalFunctionsFiles_tableLayoutPanel
            // 
            this.internalFunctionsFiles_tableLayoutPanel.ColumnCount = 2;
            this.internalFunctionsFiles_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.internalFunctionsFiles_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.internalFunctionsFiles_tableLayoutPanel.Controls.Add(this.internalFunctionsFiles_listBox, 0, 0);
            this.internalFunctionsFiles_tableLayoutPanel.Controls.Add(this.internalFunctionsFiles_add_button, 1, 0);
            this.internalFunctionsFiles_tableLayoutPanel.Controls.Add(this.internalFunctionsFiles_delete_button, 1, 1);
            this.internalFunctionsFiles_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.internalFunctionsFiles_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.internalFunctionsFiles_tableLayoutPanel.Name = "internalFunctionsFiles_tableLayoutPanel";
            this.internalFunctionsFiles_tableLayoutPanel.RowCount = 3;
            this.internalFunctionsFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.internalFunctionsFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.internalFunctionsFiles_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.internalFunctionsFiles_tableLayoutPanel.Size = new System.Drawing.Size(568, 110);
            this.internalFunctionsFiles_tableLayoutPanel.TabIndex = 30;
            // 
            // internalFunctionsFiles_listBox
            // 
            this.internalFunctionsFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.internalFunctionsFiles_listBox.FormattingEnabled = true;
            this.internalFunctionsFiles_listBox.Location = new System.Drawing.Point(3, 3);
            this.internalFunctionsFiles_listBox.Name = "internalFunctionsFiles_listBox";
            this.internalFunctionsFiles_tableLayoutPanel.SetRowSpan(this.internalFunctionsFiles_listBox, 3);
            this.internalFunctionsFiles_listBox.Size = new System.Drawing.Size(457, 104);
            this.internalFunctionsFiles_listBox.TabIndex = 0;
            this.internalFunctionsFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.internalFunctionsFiles_listBox_SelectedIndexChanged);
            // 
            // internalFunctionsFiles_add_button
            // 
            this.internalFunctionsFiles_add_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.internalFunctionsFiles_add_button.Location = new System.Drawing.Point(466, 3);
            this.internalFunctionsFiles_add_button.Name = "internalFunctionsFiles_add_button";
            this.internalFunctionsFiles_add_button.Size = new System.Drawing.Size(99, 23);
            this.internalFunctionsFiles_add_button.TabIndex = 1;
            this.internalFunctionsFiles_add_button.Text = "Добавить";
            this.internalFunctionsFiles_add_button.UseVisualStyleBackColor = true;
            this.internalFunctionsFiles_add_button.Click += new System.EventHandler(this.internalFunctionsFiles_add_button_Click);
            // 
            // internalFunctionsFiles_delete_button
            // 
            this.internalFunctionsFiles_delete_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.internalFunctionsFiles_delete_button.Location = new System.Drawing.Point(466, 33);
            this.internalFunctionsFiles_delete_button.Name = "internalFunctionsFiles_delete_button";
            this.internalFunctionsFiles_delete_button.Size = new System.Drawing.Size(99, 23);
            this.internalFunctionsFiles_delete_button.TabIndex = 2;
            this.internalFunctionsFiles_delete_button.Text = "Удалить";
            this.internalFunctionsFiles_delete_button.UseVisualStyleBackColor = true;
            this.internalFunctionsFiles_delete_button.Click += new System.EventHandler(this.internalFunctionsFiles_delete_button_Click);
            // 
            // restrictions_groupBox
            // 
            this.restrictions_groupBox.Controls.Add(this.restrictions_tableLayoutPanel);
            this.restrictions_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.restrictions_groupBox.Location = new System.Drawing.Point(3, 258);
            this.restrictions_groupBox.Name = "restrictions_groupBox";
            this.restrictions_groupBox.Size = new System.Drawing.Size(574, 124);
            this.restrictions_groupBox.TabIndex = 33;
            this.restrictions_groupBox.TabStop = false;
            this.restrictions_groupBox.Text = "Ограничения";
            // 
            // tracesPath_groupBox
            // 
            this.tracesPath_groupBox.Controls.Add(this.tracesPath_textbox);
            this.tracesPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tracesPath_groupBox.Location = new System.Drawing.Point(3, 53);
            this.tracesPath_groupBox.Name = "tracesPath_groupBox";
            this.tracesPath_groupBox.Size = new System.Drawing.Size(574, 49);
            this.tracesPath_groupBox.TabIndex = 34;
            this.tracesPath_groupBox.TabStop = false;
            this.tracesPath_groupBox.Text = "Каталог с трассами для обработки";
            // 
            // tracesPath_textbox
            // 
            this.tracesPath_textbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tracesPath_textbox.Location = new System.Drawing.Point(3, 16);
            this.tracesPath_textbox.MaximumSize = new System.Drawing.Size(0, 24);
            this.tracesPath_textbox.MinimumSize = new System.Drawing.Size(150, 24);
            this.tracesPath_textbox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.tracesPath_textbox.Name = "tracesPath_textbox";
            this.tracesPath_textbox.Size = new System.Drawing.Size(568, 24);
            this.tracesPath_textbox.TabIndex = 0;
            // 
            // foreignLibsPaths_groupbox
            // 
            this.foreignLibsPaths_groupbox.Controls.Add(this.foreignLibsPaths_textBox);
            this.foreignLibsPaths_groupbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.foreignLibsPaths_groupbox.Location = new System.Drawing.Point(3, 158);
            this.foreignLibsPaths_groupbox.Name = "foreignLibsPaths_groupbox";
            this.foreignLibsPaths_groupbox.Size = new System.Drawing.Size(574, 94);
            this.foreignLibsPaths_groupbox.TabIndex = 35;
            this.foreignLibsPaths_groupbox.TabStop = false;
            this.foreignLibsPaths_groupbox.Text = "Список путей до сторонних библиотек в исходных файлах";
            // 
            // foreignLibsPaths_textBox
            // 
            this.foreignLibsPaths_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.foreignLibsPaths_textBox.Location = new System.Drawing.Point(3, 16);
            this.foreignLibsPaths_textBox.Multiline = true;
            this.foreignLibsPaths_textBox.Name = "foreignLibsPaths_textBox";
            this.foreignLibsPaths_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.foreignLibsPaths_textBox.Size = new System.Drawing.Size(568, 75);
            this.foreignLibsPaths_textBox.TabIndex = 0;
            this.foreignLibsPaths_textBox.TextChanged += new System.EventHandler(this.foreignLibsPaths_textBox_TextChanged);

            // 
            // CustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.MinimumSize = new System.Drawing.Size(580, 600);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(580, 865);
            this.mode_groupBox.ResumeLayout(false);
            this.mode_tableLayoutPanel.ResumeLayout(false);
            this.mode_tableLayoutPanel.PerformLayout();
            this.restrictions_tableLayoutPanel.ResumeLayout(false);
            this.restrictions_tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeTo_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeFrom_numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offset_numericUpDown)).EndInit();
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.checker_groupBox.ResumeLayout(false);
            this.checker_tableLayoutPanel.ResumeLayout(false);
            this.checker_tableLayoutPanel.PerformLayout();
            this.cover_groupBox.ResumeLayout(false);
            this.cover_tableLayoutPanel.ResumeLayout(false);
            this.necessaryFunctionsFiles_groupBox.ResumeLayout(false);
            this.necessaryFunctionsFiles_tableLayoutPanel.ResumeLayout(false);
            this.controlFunctionsFilePath_groupBox.ResumeLayout(false);
            this.level_groupBox.ResumeLayout(false);
            this.level_tableLayoutPanel.ResumeLayout(false);
            this.level_tableLayoutPanel.PerformLayout();
            this.internalFunctionsFiles_groupBox.ResumeLayout(false);
            this.internalFunctionsFiles_tableLayoutPanel.ResumeLayout(false);
            this.restrictions_groupBox.ResumeLayout(false);
            this.tracesPath_groupBox.ResumeLayout(false);
            this.foreignLibsPaths_groupbox.ResumeLayout(false);
            this.foreignLibsPaths_groupbox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox keyWord_textBox;
        private System.Windows.Forms.CheckBox isRange_checkBox;
        private System.Windows.Forms.CheckBox isKeyWord_checkBox;
        private System.Windows.Forms.CheckBox isOffset_checkBox;
        private System.Windows.Forms.GroupBox mode_groupBox;
        private System.Windows.Forms.TableLayoutPanel mode_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel restrictions_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox checker_groupBox;
        private System.Windows.Forms.CheckBox isMarkAllEntriesAsProgramEntry_checkBox;
        private System.Windows.Forms.CheckBox isMarkNotCallableEntriesAsProgramEntry_checkBox;
        private System.Windows.Forms.GroupBox cover_groupBox;
        private System.Windows.Forms.TableLayoutPanel cover_tableLayoutPanel;
        private System.Windows.Forms.GroupBox necessaryFunctionsFiles_groupBox;
        private System.Windows.Forms.ListBox necessoryFunctionsFiles_listBox;
        private System.Windows.Forms.GroupBox controlFunctionsFilePath_groupBox;
        private System.Windows.Forms.CheckBox isMarkCalledFunctions_checkBox;
        private System.Windows.Forms.RadioButton basicMode_radioButton;
        private System.Windows.Forms.RadioButton interactiveMode_radioButton;
        private System.Windows.Forms.TableLayoutPanel necessaryFunctionsFiles_tableLayoutPanel;
        private System.Windows.Forms.Button necessoryFunctionsFiles_delete_button;
        private System.Windows.Forms.Button necessoryFunctionsFiles_add_button;
        private System.Windows.Forms.GroupBox level_groupBox;
        private System.Windows.Forms.TableLayoutPanel level_tableLayoutPanel;
        private System.Windows.Forms.GroupBox internalFunctionsFiles_groupBox;
        private System.Windows.Forms.TableLayoutPanel internalFunctionsFiles_tableLayoutPanel;
        private System.Windows.Forms.ListBox internalFunctionsFiles_listBox;
        private System.Windows.Forms.RadioButton level3_radioButton;
        private System.Windows.Forms.RadioButton level2_radioButton;
        private System.Windows.Forms.NumericUpDown rangeFrom_numericUpDown;
        private System.Windows.Forms.NumericUpDown rangeTo_numericUpDown;
        private System.Windows.Forms.Label range_label;
        private System.Windows.Forms.TableLayoutPanel checker_tableLayoutPanel;
        private System.Windows.Forms.Button internalFunctionsFiles_add_button;
        private System.Windows.Forms.Button internalFunctionsFiles_delete_button;
        private System.Windows.Forms.GroupBox restrictions_groupBox;
        private System.Windows.Forms.NumericUpDown offset_numericUpDown;
        private Controls.TextBoxWithDialogButton controlFunctionsFilePath_textBox;
        private System.Windows.Forms.GroupBox tracesPath_groupBox;
        private Controls.TextBoxWithDialogButton tracesPath_textbox;
        private System.Windows.Forms.RadioButton coverOnly_radioButton;
        private System.Windows.Forms.CheckBox isGenerateRestoredTrace_checkBox;
        private System.Windows.Forms.GroupBox foreignLibsPaths_groupbox;
        private System.Windows.Forms.TextBox foreignLibsPaths_textBox;

    }
}
