﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Linq.Expressions;
using System.IO;
using Store;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    /// <summary>
    /// Модуль импорта трасс
    /// </summary>
    public class TracesImporter
    {
        /// <summary>
        /// текущее Хранилище для работы.
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Делегат для обновления процента покрытия в окне прогресса.
        /// </summary>
        /// <param name="percent">Текущий процент покрытия.</param>
        delegate void UpdatePercent(int percent);

        /// <summary>
        /// Делегат для обновления процента покрытия в окне прогресса.
        /// </summary>
        UpdatePercent updateDelegate;

        /// <summary>
        /// Временная директория в Хранилище для хранения конвертированных файлов.
        /// </summary>
        string tempDirectory;

        /// <summary>
        /// Типы файлов для обработки.
        /// </summary>
        private enum TraceTypes
        {
            DIGIT = 1,
            TEXT = 2,
            BINARY = 3,
            JS = 4,
            EMPTY = 255
        }

        /// <summary>
        /// Действие по обработке трассы в зависимости от уровня НДВ
        /// </summary>
        private readonly Action<string> checkTrace;

        /// <summary>
        /// Текущая обрабатываемая трасса.
        /// </summary>
        string traceFile;

        FileStream _inputFilestream;
        /// <summary>
        /// Текущий обрабатываемый поток.
        /// </summary>
        FileStream InputFileStream
        {
            get { return _inputFilestream; }
            set
            {
                _inputFilestream = value;
                inputFileName = value.Name;
            }
        }

        /// <summary>
        /// Имя текущего файла из обрабатываемого потока.
        /// </summary>
        string inputFileName;

        /// <summary>
        /// Маска для определения текстовой трассы.
        /// </summary>
        /// <remarks>
        /// 32 байта по 8 бит = 256 значений. Т.е. маска отражает те символы, которые не могут встретится в трассе
        /// </remarks>
        readonly byte[] textMask = { 0xFF, 0x9B, 0xFF, 0xFF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        /// <summary>
        /// Маска для определения цифровой трассы.
        /// </summary>
        /// <remarks>
        /// 32 байта по 8 бит = 256 значений. Т.е. маска отражает те символы, которые не могут встретится в трассе //DONE Kokin Добавлен знак "-"
        /// </remarks>
        readonly byte[] digitMask = { 0xFF, 0xDB, 0xFF, 0xFF, 0xFF, 0xFB, 0, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

        /// <summary>
        /// Размер буфера при чтении бинарных файлов.
        /// </summary>
        const int DefFileSize = 4194304;

        const int BinarySensorSize = sizeof(Int32);

        /// <summary>
        /// Буффер для байтовых операций с файлами.
        /// </summary>
        readonly byte[] byteArray;

        /// <summary>
        /// Размер буфера для вычисления количества символов с кодами от 0 до 255.
        /// </summary>
        const int SignatureSIZE = 256;

        /// <summary>
        /// Буффер для количества встреч символов в файле при вычислении типа.
        /// </summary>
        readonly Int64[] signature;

        uint traceFileNameIndex = 0;
        /// <summary>
        /// Объект записи файла трассы. Создаётся для каждой трассы.
        /// </summary>
        BinaryWriter binaryTraceWriter;

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="storage">Хранилище, по которому работаем.</param>
        /// <param name="tempdir">Временный каталог для складывания врмененных трасс.</param>
        internal TracesImporter(Storage storage, string tempdir)
        {
            this.storage = storage;
            tempDirectory = tempdir;
            sensorsNotExisted = new HashSet<int>();
            badSensorsList = new HashSet<int>();
            //выбор метода обработки трасс            
            switch (PluginSettings.Mode)
            {
                case DynamicAnalysisResultsProcessing.Modes.NDV2:
                    checkTrace = input => DynamicAnalysisResultsProcessing.checker2.CheckTrace(storage.traces.AddTrace(input));
                    break;
                case DynamicAnalysisResultsProcessing.Modes.NDV3:
                    checkTrace = input => DynamicAnalysisResultsProcessing.checker.CheckTrace(storage.traces.AddTrace(input));
                    break;
                case DynamicAnalysisResultsProcessing.Modes.CoverOnly:
                    checkTrace = DynamicAnalysisResultsProcessing.cover.CheckTrace;
                    break;
            }

            updateDelegate = DynamicAnalysisResultsProcessing.progress.UpdatePercent;

            byteArray = new byte[DefFileSize];
            signature = new Int64[SignatureSIZE];
        }

        /// <summary>
        /// Магия определения типа файла по маске.
        /// </summary>
        /// <param name="sF">
        /// Массив из 256 значений, каждое из которых отображает количество символов 
        /// 8-битной таблицы кодирования с соответствующим кодом (от 0 до 255).
        /// </param>
        /// <param name="mask">Маска, с которой сверяться.</param>
        /// <returns></returns>
        private Int64 MaskSum(Int64[] sF, byte[] mask)
        {
            Int64 ret = 0;
            for (int i = 0; i < 256; i++)
            {
                //математическая магия, суть которой в том, что перебираются позиции в mask
                //и если в маске на текущей позиции не нуль, то в возврат добавляется количество символов
                //с номером, равным текущему (i).
                //фактически, mask хранит 32 байтовых значения, каждое из которых содержит 8 бит, обозначающих присутствие или 
                //отсутствие символа с данным номером в маске. 
                //Итого, в mask заложено 256 0/1 значений о присутствии в проверяемом массиве символов.
                if (((0x80 >> (i % 8)) & mask[i / 8]) != 0)
                {
                    ret += sF[i];
                }
            }
            return ret;
        }

        /// <summary>
        /// Функция определения типа трассы. Анализируются первые 4 Кб файла. 
        /// Если файл пустой - так и сообщается.
        /// Если встречаются печатные цифры - числовая. 
        /// Если кроме них ещё и печатные нецифровые символы - текстовая (требует определения ключевого слова для обработки).
        /// Иначе бинарная.
        /// </summary>
        /// <returns>Тип обрабатываемой трассы.</returns>
        private TraceTypes TraceType()
        {
            Array.Clear(signature, 0, SignatureSIZE);

            int bytesReaded = 0;
            using (BinaryReader br = new BinaryReader(new FileStream(inputFileName, FileMode.Open, FileAccess.Read, FileShare.Read)))
            {
                bytesReaded = br.Read(byteArray, 0, DefFileSize); //вычитать DefFileSIZE (4 Кб) из файла
            }

            if (bytesReaded == 0)
                return TraceTypes.EMPTY;

            if (bytesReaded > 2 * 4)
            {
                int firstInt32 = BitConverter.ToInt32(byteArray, 0);
                int secondInt32 = BitConverter.ToInt32(byteArray, 4);
                if (firstInt32 == secondInt32 && firstInt32 == -2)
                    return TraceTypes.JS;
            }

            for (int i = 0; i < bytesReaded; i++)
            {
                signature[byteArray[i]]++; //посчитать, сколько каких символов в этих первых 4 Кб набралось.
            }

            if (MaskSum(signature, digitMask) == 0)
                return TraceTypes.DIGIT;
            if (MaskSum(signature, textMask) == 0)
                return TraceTypes.TEXT;
            return TraceTypes.BINARY;

        }

        /// <summary>
        /// Функция импорта файла с трассами.
        /// </summary>
        /// <param name="fs">Поток для чтения файла.</param>
        internal void ImportFile(FileStream fs)
        {
            InputFileStream = fs;

            //Определяем тип трассы
            Array.Clear(byteArray, 0, DefFileSize);
            var traceType = TraceType();
            Array.Clear(byteArray, 0, DefFileSize);

            GenerateNextTraceFileName();

            switch (traceType)
            {
                case TraceTypes.TEXT:
                    ImportTextTrace();
                    break;
                case TraceTypes.DIGIT:
                    ImportDigitTrace();
                    break;
                case TraceTypes.BINARY:
                    ImportBinaryTrace();
                    break;
                case TraceTypes.JS:
                    ImportJSTrace();
                    break;
                case TraceTypes.EMPTY:
                    Monitor.Log.Warning(PluginSettings.Name, string.Format("Файл <{0}> пуст.", fs.Name));
                    break;
                default:
                    throw new Exception("Обнаружена трасса недопустимого формата.");
            }

            ProcessTrace();
        }
        #region TraceImporters implementations
        /// <summary>
        /// Функция импорта бинарного файла с трассами.
        /// </summary>
        private void ImportBinaryTrace()
        {
            int countSensors = 0;

            using (BinaryReader reader = new BinaryReader(InputFileStream))
            {
                int length = 0;
                while ((length = reader.Read(byteArray, 0, DefFileSize)) != 0)
                {
                    for (int j = 0; j < length - 3; j += BinarySensorSize)
                    {
                        countSensors++;
                        int sensorId = BitConverter.ToInt32(byteArray, j);

                        WriteSensor(sensorId, countSensors);
                    }

                    if (DynamicAnalysisResultsProcessing.STOP)
                    {
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Функция импорта текстового файла с трассами, где датчики расположены по одному в строке.
        /// </summary>
        private void ImportDigitTrace()
        {
            int countLines = 0;

            using (StreamReader srd = new StreamReader(InputFileStream))
            {
                string line = "";
                while ((line = srd.ReadLine()) != null)
                {
                    countLines++;

                    int sensorId;
                    if (!int.TryParse(line, out sensorId))
                    {
                        IA.Monitor.Log.Error(PluginSettings.Name, "Датчик \"" + line +
                            "\" не является числовым. Файл: " + inputFileName +
                            " . Позиция: " + countLines);
                        continue;
                    }

                    WriteSensor(sensorId, countLines);

                    if (DynamicAnalysisResultsProcessing.STOP)
                    {
                        break;
                    }
                }
            }

            return;
        }

        /// <summary>
        /// Функция импорта текстового файла с трассами, где датчики расположены сразу после ключевого слова.
        /// Без ключевого слова не работает.
        /// </summary>
        private void ImportTextTrace()
        {
            int countLines = 0;

            if (!PluginSettings.IsKeyWord || (String.IsNullOrEmpty(PluginSettings.KeyWord)))
            {
                IA.Monitor.Log.Error(PluginSettings.Name, "Для файла " + inputFileName + " не определено ключевое слово для поиска датчиков");
                return;
            }

            using (StreamReader srt = new StreamReader(InputFileStream))
            {
                string line = null;
                while ((line = srt.ReadLine()) != null)
                {
                    countLines++;

                    int index = -1;
                    while (true)
                    {
                        index = line.IndexOf(PluginSettings.KeyWord, index + 1, StringComparison.Ordinal);

                        if (index == -1)
                            break;

                        int nD = index + PluginSettings.KeyWord.Length;

                        if (nD < line.Length && line[nD] == '-')
                            nD++;

                        while (nD < line.Length && line[nD] >= '0' && line[nD] <= '9')
                            nD++;

                        int sensorId;
                        string sensorSubstring = line.Substring(index + PluginSettings.KeyWord.Length, nD - index - PluginSettings.KeyWord.Length);
                        if (!int.TryParse(sensorSubstring, out sensorId))
                        {
                            IA.Monitor.Log.Error(PluginSettings.Name, "Датчик " + sensorSubstring +
                                " не является числовым. Файл: " + inputFileName +
                                " . Позиция: " + countLines);
                            continue;
                        }

                        WriteSensor(sensorId, countLines);

                        if (DynamicAnalysisResultsProcessing.STOP)
                        {
                            break;
                        }
                    }

                }
            }

            return;
        }

        /// <summary>
        /// Функция импорта трассы, собранной сервером приёма сообщений от JavaScript-датчика. Формат одного датчика Int32Int32, где первое число - номер датчика, второе - порядковый номер отправки датчика.
        /// </summary>
        private void ImportJSTrace()
        {
            using (BinaryReader reader = new BinaryReader(InputFileStream))
            {
                int length = 0;
                Dictionary<int, int> sensorsReaded = new Dictionary<int, int>();
                reader.Read(byteArray, 0, BinarySensorSize * 2); //пропускаем первые 2 записи -2 -2

                // одинаковый алгоритм обработки датчиков нужен в двух разных местах. Чтобы не нарушать порядок кода в файле сброс реализован в виде внутреннего делегата.
                #region //--***--//
                Action write = () => {
                    int min = sensorsReaded.Keys.Min();
                    List<int> usedSerials = new List<int>();
                    for (int i = min, max = min + sensorsReaded.Count; i < max; i++)                    
                        if (sensorsReaded.ContainsKey(i))
                        {
                            WriteSensor(sensorsReaded[i]);
                            usedSerials.Add(i);
                        }
                        else
                        {
                            Monitor.Log.Warning(PluginSettings.Name, String.Format("В трассе JS в файле {0} пропущен датчик с порядковым номером {1}.", inputFileName, i));
                        }
                    
                    if (usedSerials.Count != sensorsReaded.Count)
                    {
                        foreach (var sId in sensorsReaded.Keys.Except(usedSerials).OrderBy(k => k).Select(k => sensorsReaded[k]))
                            WriteSensor(sId);
                    }
                    WriteSensor(-1);//текущая трасса закончена. Может породить пустую трассу.

                    sensorsReaded.Clear();
                };
                #endregion //--***--//

                while ((length = reader.Read(byteArray, 0, DefFileSize - (DefFileSize % (BinarySensorSize * 2)))) != 0) //DefFileSize - (DefFileSize % 8) гарантирует, что считается кратное двойному датчику количество байт
                {
                    for (int j = 0; j < length; j += BinarySensorSize * 2)
                    {
                        int sensorId = BitConverter.ToInt32(byteArray, j);
                        int sensorSerialNum = BitConverter.ToInt32(byteArray, j + BinarySensorSize);

                        if (sensorsReaded.ContainsKey(sensorSerialNum) || sensorsReaded.Count > int.MaxValue / 4) //ограничимся макмимумом в 2 гига на трассу. Если больше - нормальная обработка дико усложняется.
                        {
                            write();//--***--//
                        }

                        sensorsReaded[sensorSerialNum] = sensorId;                        
                    }

                    if (DynamicAnalysisResultsProcessing.STOP)
                    {
                        return;
                    }                    
                }
                write();//--***--//
            }
        }

        #endregion

        HashSet<int> sensorsNotExisted;
        HashSet<int> badSensorsList;
        /// <summary>
        /// Функция записи датчика в текущий поток записи <c>BinaryWriter</c>. 
        /// Если встречается -1, закрывает текущий файл, отправляет его на обработку и открывает новый файл на запись для следующей трассы.
        /// </summary>
        /// <param name="sensorId">Номер датчика для записи.</param>
        /// <param name="readOffset">Не используется. Номер позиции в читаемом файле, чтобы указать его в случае ошибки ненахождения датчика в хранилище.</param>
        private void WriteSensor(int sensorId, int readOffset = -1)
        {
            if (sensorId == -1)
            {
                ProcessTrace();
                GenerateNextTraceFileName();
                return;
            }

            if (sensorId <= 0)
            {
                badSensorsList.Add(sensorId);
                return;
            }

            //Режем по заданному диапазону
            if (PluginSettings.IsRange && ((uint)PluginSettings.RangeFrom > sensorId || sensorId > (uint)PluginSettings.RangeTo))
                return;

            //Прибавляем заданное смещение
            if (PluginSettings.IsOffset)
                sensorId += (int)PluginSettings.Offset;

            //Ищем считанный номер датчика в Хранилище
            if (DynamicAnalysisResultsProcessing.cover.SensorToArray.ContainsKey((ulong)sensorId))
                binaryTraceWriter.Write(sensorId);
            else
                sensorsNotExisted.Add(sensorId);
        }

        /// <summary>
        /// Создать файл во временном каталоге для трассы, которая уже будет отдана на анализ дальше. Открыть файл на запись <c>binaryTraceWriter</c>.
        /// </summary>
        private void GenerateNextTraceFileName()
        {
            traceFile = Path.GetFileNameWithoutExtension(inputFileName);
            traceFile = traceFile + "_converted" + unchecked(++traceFileNameIndex) + Path.GetExtension(inputFileName);
            traceFile = Path.Combine(tempDirectory, traceFile);

            binaryTraceWriter = new BinaryWriter(new FileStream(traceFile, FileMode.Create));
        }

        /// <summary>
        /// Закрыть запись и обработать трассу.
        /// </summary>
        private void ProcessTrace()
        {
            binaryTraceWriter.Close();

            foreach (var sId in sensorsNotExisted)
                IA.Monitor.Log.Error(PluginSettings.Name, "Датчик " + sId +
                    " отсутствует в Хранилище. Файл: " + inputFileName +
                    " . ");
            foreach (var sId in badSensorsList)
                IA.Monitor.Log.Error(PluginSettings.Name,
                    "Датчик не может иметь номер <" + sId + ">, проверьте трассу. Файл: " + inputFileName);

            sensorsNotExisted.Clear();
            badSensorsList.Clear();

            if (DynamicAnalysisResultsProcessing.STOP)
                return;

            if (new FileInfo(traceFile).Length != 0)
            {
                checkTrace(traceFile);
                //Обновляем процент для интерактивного режима
                if (PluginSettings.IsInteractiveMode)
                    DynamicAnalysisResultsProcessing.progress.Invoke(updateDelegate, DynamicAnalysisResultsProcessing.cover.GetPercent());
            }
            else
            {
                Monitor.Log.Warning(PluginSettings.Name, "Встречена пустая трасса в файле <" + inputFileName + ">");
            }
        }
    }
}
