﻿namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    partial class ResultWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.percent_groupBox = new System.Windows.Forms.GroupBox();
            this.percent_label = new System.Windows.Forms.Label();
            this.resultWindow_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.percent_groupBox.SuspendLayout();
            this.resultWindow_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // percent_groupBox
            // 
            this.percent_groupBox.Controls.Add(this.percent_label);
            this.percent_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.percent_groupBox.Location = new System.Drawing.Point(3, 3);
            this.percent_groupBox.Name = "percent_groupBox";
            this.percent_groupBox.Size = new System.Drawing.Size(201, 108);
            this.percent_groupBox.TabIndex = 0;
            this.percent_groupBox.TabStop = false;
            this.percent_groupBox.Text = "Финальный процент покрытия";
            // 
            // percent_label
            // 
            this.percent_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.percent_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.percent_label.Location = new System.Drawing.Point(3, 16);
            this.percent_label.Name = "percent_label";
            this.percent_label.Size = new System.Drawing.Size(195, 89);
            this.percent_label.TabIndex = 0;
            this.percent_label.Text = "0%";
            this.percent_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // resultWindow_tableLayoutPanel
            // 
            this.resultWindow_tableLayoutPanel.ColumnCount = 2;
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 207F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.resultWindow_tableLayoutPanel.Controls.Add(this.percent_groupBox, 0, 0);
            this.resultWindow_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultWindow_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.resultWindow_tableLayoutPanel.Name = "resultWindow_tableLayoutPanel";
            this.resultWindow_tableLayoutPanel.RowCount = 2;
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 114F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.resultWindow_tableLayoutPanel.Size = new System.Drawing.Size(392, 263);
            this.resultWindow_tableLayoutPanel.TabIndex = 1;
            // 
            // ResultWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resultWindow_tableLayoutPanel);
            this.Name = "ResultWindow";
            this.Size = new System.Drawing.Size(392, 263);
            this.percent_groupBox.ResumeLayout(false);
            this.resultWindow_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox percent_groupBox;
        private System.Windows.Forms.Label percent_label;
        private System.Windows.Forms.TableLayoutPanel resultWindow_tableLayoutPanel;
    }
}
