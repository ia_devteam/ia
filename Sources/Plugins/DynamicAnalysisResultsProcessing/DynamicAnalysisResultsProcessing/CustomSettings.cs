﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Linq;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class CustomSettings : UserControl, IA.Plugin.Settings
    {

        /// <summary>
        /// Конструктор
        /// </summary>
        internal CustomSettings()
        {
            InitializeComponent();

            basicMode_radioButton.Checked = !PluginSettings.IsInteractiveMode;
            interactiveMode_radioButton.Checked = PluginSettings.IsInteractiveMode;

            tracesPath_textbox.Text = PluginSettings.TracesDirectoryPath;

            level2_radioButton.CheckedChanged +=
                (o, e) => { internalFunctionsFiles_groupBox.Enabled = level2_radioButton.Checked; };
            level2_radioButton.Checked = PluginSettings.Mode == DynamicAnalysisResultsProcessing.Modes.NDV2;
            level3_radioButton.Checked = PluginSettings.Mode == DynamicAnalysisResultsProcessing.Modes.NDV3;
            coverOnly_radioButton.CheckedChanged +=
                (o, e) => { checker_groupBox.Enabled = !coverOnly_radioButton.Checked; };
            coverOnly_radioButton.Checked = PluginSettings.Mode == DynamicAnalysisResultsProcessing.Modes.CoverOnly;

            isKeyWord_checkBox.Checked = keyWord_textBox.Enabled = PluginSettings.IsKeyWord;
            keyWord_textBox.Text = PluginSettings.KeyWord;

            rangeTo_numericUpDown.Minimum = rangeFrom_numericUpDown.Minimum = 1;
            rangeTo_numericUpDown.Maximum = rangeFrom_numericUpDown.Maximum = ulong.MaxValue;
            isRange_checkBox.Checked = rangeFrom_numericUpDown.Enabled = rangeTo_numericUpDown.Enabled = PluginSettings.IsRange;
            rangeFrom_numericUpDown.Value = PluginSettings.RangeFrom;
            rangeTo_numericUpDown.Value = PluginSettings.RangeTo;

            offset_numericUpDown.Minimum = 0;
            offset_numericUpDown.Maximum = ulong.MaxValue;
            isOffset_checkBox.Checked = offset_numericUpDown.Enabled = PluginSettings.IsOffset;
            offset_numericUpDown.Value = PluginSettings.Offset;

            isGenerateRestoredTrace_checkBox.Checked = PluginSettings.IsGenerateRestoredTraces;

            //Проверка корректности трасс
            isMarkNotCallableEntriesAsProgramEntry_checkBox.Checked = PluginSettings.IsMarkNotCallableEntriesAsProgramEntry;
            isMarkAllEntriesAsProgramEntry_checkBox.Checked = PluginSettings.IsMarkAllEntriesAsProgramEntry;
            isMarkCalledFunctions_checkBox.Checked = PluginSettings.IsMarkCalledFunctions;

            //Проверка покрытия трасс
            necessoryFunctionsFiles_listBox.Items.AddRange(PluginSettings.NecessaryFunctionsFiles.ToArray());
            controlFunctionsFilePath_textBox.Text = PluginSettings.ControlFunctionsFilePath;
            internalFunctionsFiles_listBox.Items.AddRange(PluginSettings.InternalFunctionsFiles.ToArray());

            //Задаём изначальную доступность кнопок
            necessoryFunctionsFiles_delete_button.Enabled = internalFunctionsFiles_delete_button.Enabled = false;

            foreignLibsPaths_textBox.Text = String.Join(Environment.NewLine, PluginSettings.ForeignLibsPaths);//
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.IsInteractiveMode = !basicMode_radioButton.Checked;

            PluginSettings.TracesDirectoryPath = tracesPath_textbox.Text;

            PluginSettings.Mode =
                level2_radioButton.Checked ? DynamicAnalysisResultsProcessing.Modes.NDV2 :
                level3_radioButton.Checked ? DynamicAnalysisResultsProcessing.Modes.NDV3 :
                DynamicAnalysisResultsProcessing.Modes.CoverOnly;

            PluginSettings.IsKeyWord = isKeyWord_checkBox.Checked;
            PluginSettings.KeyWord = keyWord_textBox.Text;

            PluginSettings.IsRange = isRange_checkBox.Checked;
            PluginSettings.RangeFrom = (ulong)rangeFrom_numericUpDown.Value;
            PluginSettings.RangeTo = (ulong)rangeTo_numericUpDown.Value;

            PluginSettings.IsOffset = isOffset_checkBox.Checked;
            PluginSettings.Offset = (ulong)offset_numericUpDown.Value;

            PluginSettings.IsGenerateRestoredTraces = isGenerateRestoredTrace_checkBox.Checked;

            //Проверка корректности трасс
            PluginSettings.IsMarkNotCallableEntriesAsProgramEntry = isMarkNotCallableEntriesAsProgramEntry_checkBox.Checked;
            PluginSettings.IsMarkAllEntriesAsProgramEntry = isMarkAllEntriesAsProgramEntry_checkBox.Checked;
            PluginSettings.IsMarkCalledFunctions = isMarkCalledFunctions_checkBox.Checked;

            //Проверка покрытия трасс
            PluginSettings.NecessaryFunctionsFiles = necessoryFunctionsFiles_listBox.Items.Cast<string>().ToList();
            PluginSettings.ControlFunctionsFilePath = controlFunctionsFilePath_textBox.Text;
            PluginSettings.InternalFunctionsFiles = internalFunctionsFiles_listBox.Items.Cast<string>().ToList();

            PluginSettings.ForeignLibsPaths = foreignLibsPaths_textBox.Text.Split(new String[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        /// <summary>
        /// Обработчик - ввести ключевое слово
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isKeyWord_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            keyWord_textBox.Enabled = isKeyWord_checkBox.Checked;
            keyWord_textBox.Text = string.Empty;
        }

        /// <summary>
        /// Обработчик - использовать диапазон
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isRange_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            rangeFrom_numericUpDown.Enabled = rangeTo_numericUpDown.Enabled = isRange_checkBox.Checked;
        }

        /// <summary>
        /// Обработчик - использовать смещение датчиков
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isOffset_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            offset_numericUpDown.Enabled = isOffset_checkBox.Checked;
        }

        /// <summary>
        /// Кнопка добавления файла в список файлов с необходимыми функциями
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void necessoryFunctionsFiles_add_button_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;

                if (!necessoryFunctionsFiles_listBox.Items.Contains(ofd.FileName))
                    necessoryFunctionsFiles_listBox.Items.Add(ofd.FileName);
            }
        }

        /// <summary>
        /// Кнопка удаления файла из списка файлов с необходимыми функциями
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void necessoryFunctionsFiles_delete_button_Click(object sender, EventArgs e)
        {
            if (necessoryFunctionsFiles_listBox.SelectedItem != null)
                necessoryFunctionsFiles_listBox.Items.Remove(necessoryFunctionsFiles_listBox.SelectedItem);
        }

        /// <summary>
        /// Обработчик - выбрали файл в списке файлов с необходимыми функциями
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void necessoryFunctionsFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            necessoryFunctionsFiles_delete_button.Enabled = necessoryFunctionsFiles_listBox.SelectedItem != null;
        }

        /// <summary>
        /// Кнопка добавления файла в список файлов со списком встроенных функций
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void internalFunctionsFiles_add_button_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog of = new OpenFileDialog())
            {
                if (of.ShowDialog() != DialogResult.OK)
                    return;

                if (!internalFunctionsFiles_listBox.Items.Contains(of.FileName))
                    internalFunctionsFiles_listBox.Items.Add(of.FileName);
            }
        }

        /// <summary>
        /// Кнопка удаления файла из списка файлов со списком встроенных функций
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void internalFunctionsFiles_delete_button_Click(object sender, EventArgs e)
        {
            if (internalFunctionsFiles_listBox.SelectedItem != null)
                internalFunctionsFiles_listBox.Items.Remove(internalFunctionsFiles_listBox.SelectedItem);
        }

        /// <summary>
        /// Обработчик - выбрали файл в списке файлов со списком встроенных функций
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void internalFunctionsFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            internalFunctionsFiles_delete_button.Enabled = internalFunctionsFiles_listBox.SelectedItem != null;
        }

        private void foreignLibsPaths_textBox_TextChanged(object sender, EventArgs e)
        {
            //TODO: filter paths to existing in storage.
        }
    }
}
