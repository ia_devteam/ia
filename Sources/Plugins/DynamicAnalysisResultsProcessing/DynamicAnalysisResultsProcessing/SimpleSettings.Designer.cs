﻿namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    partial class SimpleSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tracesPath_groupBox = new System.Windows.Forms.GroupBox();
            this.tracesPath_textbox = new IA.Controls.TextBoxWithDialogButton();
            this.tracesPath_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 2;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(448, 55);
            this.settings_tableLayoutPanel.TabIndex = 0;
            // 
            // tracesPath_groupBox
            // 
            this.tracesPath_groupBox.Controls.Add(this.tracesPath_textbox);
            this.tracesPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tracesPath_groupBox.Location = new System.Drawing.Point(0, 0);
            this.tracesPath_groupBox.Name = "tracesPath_groupBox";
            this.tracesPath_groupBox.Size = new System.Drawing.Size(448, 55);
            this.tracesPath_groupBox.TabIndex = 35;
            this.tracesPath_groupBox.TabStop = false;
            this.tracesPath_groupBox.Text = "Каталог с трассами для обработки";
            // 
            // tracesPath_textbox
            // 
            this.tracesPath_textbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tracesPath_textbox.Location = new System.Drawing.Point(3, 16);
            this.tracesPath_textbox.MaximumSize = new System.Drawing.Size(0, 24);
            this.tracesPath_textbox.MinimumSize = new System.Drawing.Size(150, 24);
            this.tracesPath_textbox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.tracesPath_textbox.Name = "tracesPath_textbox";
            this.tracesPath_textbox.Size = new System.Drawing.Size(442, 24);
            this.tracesPath_textbox.TabIndex = 0;
            // 
            // SimpleSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tracesPath_groupBox);
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "SimpleSettings";
            this.Size = new System.Drawing.Size(448, 55);
            this.tracesPath_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox tracesPath_groupBox;
        private Controls.TextBoxWithDialogButton tracesPath_textbox;
    }
}
