﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Store;
using IA.Extensions;
using System.Linq;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    internal class TracesCover
    {
        /// <summary>
        /// Класс, отражающий отличительные особенности той или иной функции.
        /// </summary>
        private class func_is
        {
            /// <summary>
            /// Функция используется хотя бы в одной трассе.
            /// </summary>
            public bool worked;

            /// <summary>
            /// Идентификатор датчика, вставленного в эту функцию.
            /// Если идентификатор WrongId, то датчик в эту функцию не вставлен.
            /// </summary>
            //public UInt64 sensorId;
            public bool haveSensor;

            /// <summary>
            /// Находится или нет в списке функций, выделенных разработчиками как избыточные, но не удаляемые по каким-то соображениям.
            /// </summary>
            public int inList;

            /// <summary>
            /// Находится в списке функций, опознанных как избыточные.
            /// </summary>
            public bool isRedundant;

            /// <summary>
            /// Находится в списке функций для контроля.
            /// </summary>
            public bool isControl;

            public bool inForeignLibsList;
        }

        /// <summary>
        /// Класс, отражающий отличительные особенности того или иного датчика
        /// </summary>
        private class sensor_is
        {
            /// <summary>
            /// Номер функции (в которую вставлен датчик) в списке функций, выделенных разработчиками как избыточные, но не удаляемые по каким-то соображениям.
            /// </summary>
            public int inList;

            //True - если функция, в которую вставлен датчик является избыточной. False - иначе.
            public bool isRedundant;

            /// <summary>
            /// Номер функции из хранилища, в которую вставлен датчик (оптимизация: меняем количество обращений к хранилищу на память).
            /// </summary>
            public ulong funcId;

            /// <summary>
            /// Имя функции из хранилища (оптимизация: меняем количество обращений к хранилищу на память).
            /// </summary>
            public string functionNameForReport;

            public bool inForeignLibsList;
        }

        /// <summary>
        /// Битовый массив для хранения информации о покрытии.
        /// </summary>
        private BitArray bitArray;

        /// <summary>
        /// Таблица соответствия номеров датчиков и битового массива
        /// </summary>
        internal Dictionary<ulong, int> SensorToArray;

        HashSet<ulong> WorkedSensors;

        /// <summary>
        /// Функция для получения списка необходимых избыточных функций из строки настроек
        /// </summary>
        /// <returns>Возвращает список необходимых избыточных функций</returns>
        internal static List<string> GetNecessoryFunctionsList()
        {
            return PluginSettings.NecessaryFunctionsFiles;
        }

        /// <summary>
        /// Текущее Хранилище для работы
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Словарь, где каждому идентификатору функции сопоставляются её свойства.
        /// </summary>
        private Dictionary<UInt64, func_is> functions = new Dictionary<ulong, func_is>();

        /// <summary>
        /// Словарь, где каждому идентификатору датчика сопоставляются его свойства.
        /// </summary>
        private Dictionary<UInt64, sensor_is> sensors = new Dictionary<ulong, sensor_is>();


        private ulong sensorsCoveredByTraces = 0;
        private ulong sensorsCoveredByTracesWithoutForeignLibs = 0;

        private ulong sensorsTotalWithoutForeignLibs = 0;
        private ulong sensorsTotalFromLists = 0;
        private ulong sensorsTotalFromRedunant = 0;
        //private ulong sensorsCoveredByTracesAndLists = 0;
        //private ulong sensorsCoveredByTracesListsAndRedundant = 0;
        private ulong functionsCovered = 0;
        private ulong functionsCoveredWithoutForeignLibs = 0;

        private ulong functionsWithSensors = 0;
        private ulong functionsWithSensorsWithoutForeignLibs = 0;
        private ulong sensors_without_function = 0;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="storage">Текущее Хранилище для работы</param>
        internal TracesCover(Storage storage)
        {
            this.storage = storage;
        }

        /// <summary>
        /// Построение таблиц перекодировки номеров датчиков и бинарного массива
        /// </summary>
        internal void MakeSensorsToBitArrayTable()
        {
            SensorToArray = new Dictionary<ulong, int>();
            int index = 0;
            foreach (Sensor sns in storage.sensors.EnumerateSensors())
            {
                SensorToArray.Add(sns.ID, index++);
            }

        }

        /// <summary>
        /// Функция обработчик вызова датчика.
        /// </summary>
        /// <param name="sensorID">Идентификатор вызванного датчика.</param>
        /// <returns>true, если данный датчик ранее не вызывался</returns>
        internal bool Call(ulong sensorID)
        {
            if (!bitArray.Get(SensorToArray[sensorID]))
            {
                bitArray.Set(SensorToArray[sensorID], true);
                sensorsCoveredByTraces++;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Функция для получения текущего процента покрытия.
        /// </summary>
        /// <returns>Текущий процент покрытия.</returns>
        internal int GetPercent()
        {
            try
            {
                return Percent(sensorsCoveredByTraces, (ulong)SensorToArray.Count);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Функция сохранения информации о покрытии в файл.
        /// </summary>
        /// <param name="filePath">Путь к файлу, куда следует сохранить информацию о покрытии.</param>
        internal void SaveBitArray(string filePath)
        {
            if (bitArray != null)
            {
                byte[] byteArray = new byte[bitArray.Length / 8 + (bitArray.Length % 8 == 0 ? 0 : 1)];
                bitArray.CopyTo(byteArray, 0);
                File.WriteAllBytes(filePath, byteArray);
            }
        }

        /// <summary>
        /// Функция загрузки информации о покрытии из файла.
        /// </summary>
        /// <param name="filePath">Путь к файлу, откуда следует загрузить информацию о покрытии.</param>
        internal void LoadBitArray(string filePath)
        {
            //обнуляем счётчик вызванных датчиков
            sensorsCoveredByTraces = 0;

            //считываем массив байт из файла
            if (IOController.FileController.IsExists(filePath)) //Если файл уже есть
            {
                bitArray = new BitArray(File.ReadAllBytes(filePath));
                for (int i = 0; i < bitArray.Length; i++)
                    if (bitArray.Get(i))
                        sensorsCoveredByTraces++;
            }
            else //Создаем минимальный
                bitArray = new BitArray(1, false);

            //общее число датчиков в хранилище
            bitArray.Length = (int)storage.sensors.Count();
        }

        /// <summary>
        /// Функция генерации отчётов о покрытии.
        /// </summary>
        /// <param name="reportsPath">Путь к директории, куда следует сохранить отчёт о покрытии.</param>
        internal void GenerateReports(string reportsPath)
        {
            //Делаем предварительную очистку
            sensors.Clear();
            functions.Clear();

            //Анализируем все датчики и функции в Хранилище
            AnalizeSensorsAndFunctions();
            File.WriteAllLines(Path.Combine(reportsPath, "worked_sensors_list.txt"), WorkedSensors.Select(i => i.ToString()));
            //переменная для отображения прогресса
            ulong counter;

            List<string[]> CoverStatistics = new List<string[]>();
            List<string[]> UncoveredSensors = new List<string[]>();
            List<string[]> UncoveredSensorsWithoutFromLists = new List<string[]>();
            List<string[]> UncoveredSensorsWithoutFromListsAndRedundant = new List<string[]>();
            List<string[]> UncoveredFunctions = new List<string[]>();
            List<string[]> ControlledFunctions = new List<string[]>();
            List<string[]> ControlledFunctionsStats = new List<string[]>();

            //Генерация отчёта CoverStatistics (XML)
            CoverStatistics.Add(new string[] { "Результаты", "Количество/Процент" });
            CoverStatistics.Add(new string[] { "Всего опознано датчиков", 
                sensors.Count.ToString() });
            CoverStatistics.Add(new string[] { "Сработало датчиков", 
                sensorsCoveredByTraces.ToString() });
            CoverStatistics.Add(new string[] { "Процент сработавших датчиков (%)", 
                Percent(sensorsCoveredByTraces, (ulong)sensors.Count).ToString() });
            //CoverStatistics.Add(new string[] { "Число датчиков, либо сработавших, либо вставленных в функцию, входящую в списки", sensorsCoveredByTracesAndLists.ToString() });
            CoverStatistics.Add(new string[] { "Процент сработавших датчиков без учёта датчиков, вставленных в функции, входящие в списки (%)", 
                Percent(sensorsCoveredByTraces, (ulong)sensors.Count - sensorsTotalFromLists).ToString() });
            //CoverStatistics.Add(new string[] { "Число датчиков, либо сработавших, либо входящих в избыточную функцию", sensorsCoveredByTracesListsAndRedundant.ToString() });
            CoverStatistics.Add(new string[] { "Процент сработавших датчиков без учёта датчиков, вставленных в функции, входящие в списки либо входящих в избыточную функцию (%)",
                Percent(sensorsCoveredByTraces, (ulong)sensors.Count - sensorsTotalFromLists - sensorsTotalFromRedunant).ToString() });
            CoverStatistics.Add(new string[] { "Число функций, в которые вставлены датчики", 
                functionsWithSensors.ToString() });
            CoverStatistics.Add(new string[] { "Число датчиков, не ассоциированных ни с одной функцией", 
                sensors_without_function.ToString() });
            CoverStatistics.Add(new string[] { "Сработало функций", 
                functionsCovered.ToString() });
            CoverStatistics.Add(new string[] { "Процент сработавших функций относительно числа функций, в которые вставлены датчики (%)", 
                Percent(functionsCovered, functionsWithSensors).ToString() });
            XMLReport.XMLReport.Report(Path.Combine(reportsPath, "CoverStatistics.xml"), "Статистика", CoverStatistics);

            //Генерация отчёта CoverStatistics (TXT)
            StreamWriter writerCoverStatistics = new StreamWriter(Path.Combine(reportsPath, "CoverStatistics.txt"));
            writerCoverStatistics.WriteLine("Всего опознано датчиков "
                + sensors.Count.ToString());
            writerCoverStatistics.WriteLine("Всего опознано датчиков без учёта сторонних библиотек "
                + sensorsTotalWithoutForeignLibs.ToString());
            writerCoverStatistics.WriteLine("Сработало датчиков "
                + sensorsCoveredByTraces.ToString());
            writerCoverStatistics.WriteLine("Сработало датчиков без учёта сторонних библиотек "
                + sensorsCoveredByTracesWithoutForeignLibs.ToString());
            writerCoverStatistics.WriteLine("Процент сработавших датчиков "
                + Percent(sensorsCoveredByTraces, (ulong)sensors.Count).ToString() + "%");
            writerCoverStatistics.WriteLine("Процент сработавших датчиков без учёта сторонних библиотек "
                + Percent(sensorsCoveredByTracesWithoutForeignLibs, (ulong)sensorsTotalWithoutForeignLibs).ToString() + "%");
            //writerCoverStatistics.WriteLine("Число датчиков, либо сработавших, либо вставленных в функцию, входящую в списки " + sensorsCoveredByTracesAndLists.ToString());
            writerCoverStatistics.WriteLine("Процент сработавших датчиков без учёта датчиков, вставленных в функции, входящие в списки "
                + Percent(sensorsCoveredByTraces, (ulong)sensors.Count - sensorsTotalFromLists).ToString() + "%");
            //writerCoverStatistics.WriteLine("Число датчиков, либо сработавших, либо вставленных в функцию, входящую в списки, либо входящих в избыточную функцию " + sensorsCoveredByTracesListsAndRedundant.ToString());
            writerCoverStatistics.WriteLine("Процент сработавших датчиков без учёта датчиков, вставленных в функции, входящие в списки либо входящих в избыточную функцию "
                + Percent(sensorsCoveredByTraces, (ulong)sensors.Count - sensorsTotalFromLists - sensorsTotalFromRedunant).ToString() + "%");
            writerCoverStatistics.WriteLine("Число функций, в которые вставлены датчики "
                + functionsWithSensors.ToString());
            writerCoverStatistics.WriteLine("Число функций, в которые вставлены датчики без учёта сторонних библиотек "
                + functionsWithSensorsWithoutForeignLibs.ToString());
            writerCoverStatistics.WriteLine("Число датчиков, не ассоциированных ни с одной функцией "
                + sensors_without_function.ToString());
            writerCoverStatistics.WriteLine("Сработало функций "
                + functionsCovered.ToString());
            writerCoverStatistics.WriteLine("Сработало функций без учёта сторонних библиотек "
                + functionsCoveredWithoutForeignLibs.ToString());
            writerCoverStatistics.WriteLine("Процент сработавших функций относительно числа функций, в которые вставлены датчики "
                + Percent(functionsCovered, functionsWithSensors).ToString() + "%");
            writerCoverStatistics.WriteLine("Процент сработавших функций относительно числа функций, в которые вставлены датчики без учёта сторонних библиотек "
                + Percent(functionsCoveredWithoutForeignLibs, functionsWithSensorsWithoutForeignLibs).ToString() + "%");
            writerCoverStatistics.Close();

            //Генерация остальных отчётов по датчикам
            UncoveredSensors.Add(new string[] { "Номер датчика", "Функция" });
            StreamWriter writerUncoveredSensors = new StreamWriter(Path.Combine(reportsPath, "UncoveredSensors.txt"));
            StreamWriter writerUncoveredSensorsWithoutForeign = new StreamWriter(Path.Combine(reportsPath, "UncoveredSensorsPure.txt"));

            UncoveredSensorsWithoutFromLists.Add(new string[] { "Номер датчика", "Функция" });
            StreamWriter writerUncoveredSensorsWithoutFromLists = new StreamWriter(Path.Combine(reportsPath, "UncoveredSensorsWithoutFromLists.txt"));
            UncoveredSensorsWithoutFromListsAndRedundant.Add(new string[] { "Номер датчика", "Функция" });
            StreamWriter writerUncoveredSensorsWithoutFromListsAndRedundant = new StreamWriter(Path.Combine(reportsPath, "UncoveredSensorsWithoutFromListsAndRedundant.txt"));

            counter = 0;

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.SENSORS_REPORTS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)sensors.Count);

            HashSet<ulong> funcsWithSensors = new HashSet<ulong>();

            foreach (KeyValuePair<UInt64, sensor_is> pair in sensors)
            {
                funcsWithSensors.Add(pair.Value.funcId);
                if (!bitArray.Get(SensorToArray[pair.Key]))
                {
                    string sensorIdText = pair.Key.ToString();

                    //Если датчику не присвоена функция, значит ошибка в Хранилище
                    if (pair.Value.funcId == Store.Const.CONSTS.WrongID)
                    {
                        IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось найти функцию, соответствующую датчику: " + sensorIdText);
                        IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.SENSORS_REPORTS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                        continue;
                    }

                    string functionText = pair.Value.functionNameForReport;

                    //UncoveredSensors
                    writerUncoveredSensors.WriteLine(sensorIdText + " в функции " + functionText);

                    if (!pair.Value.inForeignLibsList)
                        writerUncoveredSensorsWithoutForeign.WriteLine(sensorIdText + " в функции " + functionText);

                    UncoveredSensors.Add(new string[] { sensorIdText, functionText });

                    if (pair.Value.inList == -1)
                    {
                        //UncoveredSensorsWithoutFromLists
                        writerUncoveredSensorsWithoutFromLists.WriteLine(sensorIdText + " в функции " + functionText);
                        UncoveredSensorsWithoutFromLists.Add(new string[] { sensorIdText, functionText });

                        if (!pair.Value.isRedundant)
                        {
                            //UncoveredSensorsWithoutFromListsAndRedundant
                            writerUncoveredSensorsWithoutFromListsAndRedundant.WriteLine(sensorIdText + " в функции " + functionText);
                            UncoveredSensorsWithoutFromListsAndRedundant.Add(new string[] { sensorIdText, functionText });
                        }
                    }
                }
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.SENSORS_REPORTS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }
            writerUncoveredSensors.Close();
            writerUncoveredSensorsWithoutForeign.Close();

            XMLReport.XMLReport.Report(Path.Combine(reportsPath, "UncoveredSensors.xml"), "UncoveredSensors", UncoveredSensors);
            writerUncoveredSensorsWithoutFromLists.Close();
            XMLReport.XMLReport.Report(Path.Combine(reportsPath, "UncoveredSensorsWithoutFromLists.xml"), "UncoveredSensorsWithoutFromLists", UncoveredSensorsWithoutFromLists);
            writerUncoveredSensorsWithoutFromListsAndRedundant.Close();
            XMLReport.XMLReport.Report(Path.Combine(reportsPath, "UncoveredSensorsWithoutFromListsAndRedundant.xml"), "UncoveredSensorsWithoutFromListsAndRedundant", UncoveredSensorsWithoutFromListsAndRedundant);

            //Генерация отчётов по функциям
            UncoveredFunctions.Add(new string[] { "Функция", "Файл", "Строка" });
            StreamWriter writerUncoveredFunctions = new StreamWriter(Path.Combine(reportsPath, "UncoveredFunctions.txt"));
            StreamWriter writerUncoveredFunctionsWithoutForeign = new StreamWriter(Path.Combine(reportsPath, "UncoveredFunctionsPure.txt"));

            ControlledFunctions.Add(new string[] { "Функция", "Была вызвана", "Вызвала" });
            StreamWriter writerControlledFunctions = new StreamWriter(Path.Combine(reportsPath, "ControlledFunctions.txt"));

            int controlledSum = 0, controlledCalled = 0;

            counter = 0;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.FUNCTIONS_REPORT.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)functions.Count);
            foreach (KeyValuePair<UInt64, func_is> pair in functions)
            {
                if (pair.Value.worked && !pair.Value.isControl)
                {
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.FUNCTIONS_REPORT.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                    continue;
                }

                IFunction function = storage.functions.GetFunction(pair.Key);
                Location definition = function.Definition();

                string name = function.FullNameForReports;

                if (!pair.Value.worked && definition != null && funcsWithSensors.Contains(function.Id) && !function.IsExternal)
                {

                    string file = definition.GetFile().FileNameForReports;
                    string line = definition.GetLine().ToString();

                    writerUncoveredFunctions.WriteLine("Функция: " + name + " Файл: " + file + " Строка: " + line);

                    if (!pair.Value.inForeignLibsList)
                        writerUncoveredFunctionsWithoutForeign.WriteLine("Функция: " + name + " Файл: " + file + " Строка: " + line);

                    if (!String.IsNullOrEmpty(name))
                        UncoveredFunctions.Add(new string[] { name, file, line });
                }

                if (pair.Value.isControl)
                {
                    controlledSum++;
                    if (pair.Value.worked)
                    {
                        controlledCalled++;
                        //функция отработала, пишем откуда она была вызвана и кто мог ее вызвать
                        writerControlledFunctions.WriteLine(name + " была вызвана");
                        ControlledFunctions.Add(new string[] { name, "", "" });
                        foreach (var callee in function.CalledBy())
                        {
                            IFunction callerFunc = callee.Caller;
                            if (functions[callerFunc.Id].worked)
                            {
                                writerControlledFunctions.WriteLine(" -> " + callerFunc.FullNameForReports);
                                ControlledFunctions.Add(new string[] { "->", callerFunc.FullNameForReports, "" });
                            }
                        }
                        writerControlledFunctions.WriteLine(name + " вызвала");
                        foreach (var callee in function.Calles())
                        {
                            IFunction callesFunc = callee.Calles;
                            if (functions[callesFunc.Id].worked)
                            {
                                writerControlledFunctions.WriteLine(" -< " + callesFunc.FullNameForReports);
                                ControlledFunctions.Add(new string[] { "<-", "", callesFunc.FullNameForReports });
                            }
                        }
                    }
                    else
                    {
                        writerControlledFunctions.WriteLine(name + " не вызывалась");
                        ControlledFunctions.Add(new string[] { name, "не вызывалась", "" });
                    }
                }
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.FUNCTIONS_REPORT.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }
            writerUncoveredFunctions.Close();
            writerUncoveredFunctionsWithoutForeign.Close();

            XMLReport.XMLReport.Report(Path.Combine(reportsPath, "UncoveredFunctions.xml"), "UncoveredFunctions", UncoveredFunctions);
            writerControlledFunctions.Close();
            XMLReport.XMLReport.Report(Path.Combine(reportsPath, "ControlledFunctions.xml"), "ControlledFunctions", ControlledFunctions);

            //Отчёт ControlledFunctionsStats
            StreamWriter writerControlledFunctionsStats = new StreamWriter(Path.Combine(reportsPath, "ControlledFunctionsStats.txt"));
            writerControlledFunctionsStats.WriteLine("Из " + controlledSum + " функций для контроля было вызвано " + controlledCalled + " функций");
            writerControlledFunctionsStats.Close();
        }

        /// <summary>
        /// Функция анализа всех функций и датчиков в Хранилище.
        /// </summary>
        private void AnalizeSensorsAndFunctions()
        {
            //переменная для отображения прогресса
            ulong counter;

            sensorsCoveredByTracesWithoutForeignLibs = 0;

            //sensorsCoveredByTracesAndLists = 0;
            //sensorsCoveredByTracesListsAndRedundant = 0;
            sensorsTotalWithoutForeignLibs = 0;
            sensorsTotalFromLists = 0;
            sensorsTotalFromRedunant = 0;

            functionsCovered = 0;
            functionsCoveredWithoutForeignLibs = 0;

            functionsWithSensors = 0;
            sensors_without_function = 0;
            WorkedSensors = new HashSet<ulong>();

            //Загружаем все функции
            counter = 0;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.FUNCTIONS_FROM_STORAGE.Description(), Monitor.Tasks.Task.UpdateType.STAGES, storage.functions.Count);
            foreach (IFunction function in storage.functions.EnumerateFunctions(enFunctionKind.ALL))
            {
                try
                {
                    functions.Add(function.Id, new func_is
                    {
                        haveSensor = false,
                        worked = false,
                        isRedundant = function.Essential == enFunctionEssentials.REDUNDANT,
                        inList = -1,
                        isControl = false
                    });
                    IA.Monitor.Tasks.Update(PluginSettings.Name,
                        PluginSettings.TasksReport.FUNCTIONS_FROM_STORAGE.Description(),
                        Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                }
                catch (Exception ex)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, "Не загружена из Хранилища функция " + function.FullName);
                }
            }
       

            //Загружаем перечень необходимых избыточных функций
            if (GetNecessoryFunctionsList().Count != 0)
                LoadNecessaryFunctions();

            //Загружаем перечень контролируемых функций
            if (PluginSettings.ControlFunctionsFilePath != "")
                LoadControlFunctions();

            //Загружаем все датчики
            counter = 0;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.SENSORS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, storage.sensors.Count());
            foreach (Sensor sensor in storage.sensors.EnumerateSensors())
            {
                try
                {
                    sensor_is currentSensor = new sensor_is
                    {
                        inList = -1,
                        isRedundant = false,
                        funcId = Store.Const.CONSTS.WrongID,
                        functionNameForReport = String.Empty,
                        inForeignLibsList = false
                    };

                    IFunction func = sensor.GetFunction();
                    if (func != null)
                    {
                        currentSensor.funcId = func.Id;
                        currentSensor.functionNameForReport = func.FullNameForReports;

                        func_is funcIs = functions[func.Id];
                        funcIs.haveSensor = true;
                        currentSensor.inList = funcIs.inList;

                        currentSensor.isRedundant = func.Essential == enFunctionEssentials.REDUNDANT;

                        //Думаем о том, вставлен ли датчик в сторонние библиотеки
                        Location loc = func.Definition();
                        if (loc != null) //Не каждая функция имеет место определения
                        {
                            var sensorFile = loc.GetFile().RelativeFileName_Original;
                            //sensor.GetBeforeStatement().FirstSymbolLocation.GetFile().RelativeFileName_Original;

                            if (
                                !PluginSettings.ForeignLibsPaths.Any(
                                    (l) => sensorFile.StartsWith(l, StringComparison.OrdinalIgnoreCase)))
                            {
                                sensorsTotalWithoutForeignLibs++;
                            }
                            else
                            {
                                currentSensor.inForeignLibsList = true;
                                funcIs.inForeignLibsList = true;
                            }
                        }
                    }
                    else
                    {
                        sensors_without_function++;
                    }

                    sensors.Add(sensor.ID, currentSensor);
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.SENSORS.Description(),
                        Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                }
                catch (Exception ex)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, "Не удалось обработать датчик " + sensor.ID);
                }
            }

            //анализируем датчики
            counter = 0;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.SENSORS_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)sensors.Count);
            foreach (KeyValuePair<UInt64, sensor_is> pair in sensors)
            {
                if (bitArray.Get(SensorToArray[pair.Key])) //проверка вызван ли датчик
                {
                    WorkedSensors.Add(pair.Key);
                    //sensorsCoveredByTracesAndLists++;
                    //sensorsCoveredByTracesListsAndRedundant++;

                    ulong ID = sensors[pair.Key].funcId;
                    if (ID != Store.Const.CONSTS.WrongID)
                        functions[ID].worked = true;

                    if (!pair.Value.inForeignLibsList)
                        sensorsCoveredByTracesWithoutForeignLibs++;
                }
                else if (pair.Value.inList != -1)
                {
                    sensorsTotalFromLists++;
                    //sensorsCoveredByTracesAndLists++;
                    //sensorsCoveredByTracesListsAndRedundant++;
                }
                else if (pair.Value.isRedundant)
                {
                    sensorsTotalFromRedunant++;
                    //sensorsCoveredByTracesListsAndRedundant++;
                }
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.SENSORS_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }

            //анализируем функции
            counter = 0;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.FUNCTIONS_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)functions.Count);
            foreach (KeyValuePair<UInt64, func_is> pair in functions)
            {
                if (pair.Value.worked)
                {
                    functionsCovered++;
                    if (!pair.Value.inForeignLibsList)
                        functionsCoveredWithoutForeignLibs++;
                }
                if (pair.Value.haveSensor)
                {
                    functionsWithSensors++;
                    if (!pair.Value.inForeignLibsList)
                        functionsWithSensorsWithoutForeignLibs++;
                }
                    
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.FUNCTIONS_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }
        }

        /// <summary>
        /// Загрузить список избыточных функций, для которых разработчик отметил их необходимость.
        /// </summary>
        private void LoadNecessaryFunctions()
        {
            //подготовка к выводу прогресса
            ulong counter = 0;
            foreach (string fileName in GetNecessoryFunctionsList())
                foreach (string line in System.IO.File.ReadLines(fileName))
                    counter++;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.ESSENTIAL_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, counter);

            //Импорт списков трасс
            counter = 0;
            int listNumber = 0;
            foreach (string fileName in GetNecessoryFunctionsList())
            {
                foreach (string line in System.IO.File.ReadLines(fileName))
                {
                    string[] param = line.Split('\t');

                    if (param.Length != 3)
                    {
                        IA.Monitor.Log.Error(PluginSettings.Name, "Формат строки " + line + " в файле " + fileName + "некорректен. Табуляций должно быть ровно 2. Строка пропущена.");
                        IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.ESSENTIAL_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                        continue;
                    }

                    UInt64 lineParam;
                    if (!UInt64.TryParse(param[2], out lineParam))
                    {
                        IA.Monitor.Log.Error(PluginSettings.Name, "Формат строки " + line + " в файле " + fileName + "некорректен. 3й параметр должен быть номером строки в файле. Строка пропущена.");
                        IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.ESSENTIAL_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                        continue;
                    }
                    bool isFound = false;
                    foreach (IFunction function in storage.functions.FindFunction(param[0].Replace("::", ".").Split('.')))
                    {
                        Location locat = function.Definition();
                        if (locat != null && locat.GetLine() == lineParam)
                            if (Files.CanonizeFileName(locat.GetFile().FileNameForReports) == Files.CanonizeFileName(param[1]))
                            {
                                func_is funcIs = functions[function.Id];
                                if (funcIs.inList != -1)
                                    IA.Monitor.Log.Warning(PluginSettings.Name, "Функция " + line + " определна в нескольких списках. Впервые встретилась в списке " + GetNecessoryFunctionsList()[funcIs.inList] + ". В списке " + fileName + " функция проигнорирована");
                                else
                                    functions[function.Id].inList = listNumber;

                                isFound = true;
                            }
                    }

                    if (!isFound)
                        IA.Monitor.Log.Error(PluginSettings.Name, "Строка " + line + " проигнорирована, так как указанная в ней функция не найдена");

                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.ESSENTIAL_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);

                }
                listNumber++;
            }
        }

        /// <summary>
        /// Загрузить список контролируемых функций (функции вызов, которых в трассах нужно проконтролировать).
        /// </summary>
        private void LoadControlFunctions()
        {
            //подготовка к выводу прогресса
            ulong counter = 0;
            foreach (string line in System.IO.File.ReadLines(PluginSettings.ControlFunctionsFilePath))
                counter++;
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.CONTROLLED_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, counter);

            //Импорт списка функций
            counter = 0;
            foreach (string line in System.IO.File.ReadLines(PluginSettings.ControlFunctionsFilePath))
            {
                string[] param = line.Split('\t');
                if (param.Length != 3)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, "Формат строки " + line + "некорректен. Табуляций должно быть ровно 2. Строка пропущена.");
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.ESSENTIAL_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                    continue;
                }

                UInt64 lineParam;
                if (!UInt64.TryParse(param[2], out lineParam))
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, "Формат строки " + line + "некорректен. 3й параметр должен быть номером строки в файле. Строка пропущена.");
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.ESSENTIAL_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                    continue;
                }
                bool isFound = false;
                foreach (IFunction function in storage.functions.FindFunction(param[0].Replace("::", ".").Split('.')))
                {
                    Location locat = function.Definition();
                    if (locat != null && locat.GetLine() == lineParam)
                        if (Files.CanonizeFileName(locat.GetFile().FileNameForReports) == Files.CanonizeFileName(param[1]))
                            isFound = functions[function.Id].isControl = true;
                }

                if (!isFound)
                    IA.Monitor.Log.Error(PluginSettings.Name, "Строка " + line + " проигнорирована, так как указанная в ней функция не найдена");

                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.CONTROLLED_FUNCTIONS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
            }
        }

        /// <summary>
        /// Функция подсчёта процента сработавших датчиков.
        /// </summary>
        /// <param name="count">Число сработавших датчков.</param>
        /// <param name="all">Общее число датчиков.</param>
        /// <returns>Процент сработавшищ датчиков.</returns>
        private int Percent(ulong count, ulong all)
        {
            return (all != 0) ? (int)(count * 100 / all) : 0;
        }

        /// <summary>
        /// Вычисление сработавших датчиков в трассе. Вычисляется _только_ покрытие.
        /// </summary>
        /// <param name="filename">Имя файла с одной бинарной трассой без -1.</param>
        internal void CheckTrace(string filename)
        {
            using (BinaryReader br = new BinaryReader(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read)))
            {
                try
                {
                    while (true)
                    {
                        int currentSensorID = br.ReadInt32();
                        if (currentSensorID == -1)
                            break;
                        Call((ulong)currentSensorID);
                    }
                }
                catch (EndOfStreamException)
                { }
            }

            IOController.FileController.Remove(filename); //эмуляция поведения при добавлении трассы в хранилище - файл с исходной трассой удаляется.
        }
    }
}
