﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    /// <summary>
    /// Реализация окна настройки в простом режиме
    /// </summary>
    internal partial class SimpleSettings : UserControl, IA.Plugin.Settings
    {
        
        /// <summary>
        /// Конструктор
        /// </summary>
        internal SimpleSettings()
        {
            InitializeComponent();
            tracesPath_textbox.Text = PluginSettings.TracesDirectoryPath;
        }

        public void Save()
        {
            PluginSettings.TracesDirectoryPath = tracesPath_textbox.Text;
        }
    }
}
