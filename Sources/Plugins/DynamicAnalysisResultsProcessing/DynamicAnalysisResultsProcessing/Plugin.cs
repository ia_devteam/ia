using IA.Extensions;
using IOController;
using Store;
using Store.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    using CustomExtensions;

    /// <summary>
    /// Класс для хранения настроек плагина. Настройки представляют собой статические поля, чтобы к ним можно было получить доступ из любой части приложения.
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Обработка результатов динамического анализа";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.DYNAMIC_ANALYSIS_RESULTS_PROCESSING;

        /// <summary>
        /// Наименования тасков плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка трасс")]
            TRACES_PROCESSING,

            [Description("Заполнение таблицы переходов НДВ2")]
            NDV2_TRANSITION_TABLES,

            [Description("Заполнение таблицы вызовов функций")]
            NDV3_FUNCTIONS_CALLS
        };

        /// <summary>
        /// Наименования задач отчетов
        /// </summary>
        internal enum TasksReport
        {
            [Description("Копирование протоколов проверки корректности трасс из Хранилища")]
            PROTOCOLS_CORRECTNESS_TRACES,

            [Description("Выгрузка функций из Хранилища")]
            FUNCTIONS_FROM_STORAGE,

            [Description("Загрузка списка принудительно существенных функций")]
            ESSENTIAL_FUNCTIONS,

            [Description("Загрузка списка контролируемых функций")]
            CONTROLLED_FUNCTIONS,

            [Description("Выгрузка датчиков из Хранилища")]
            SENSORS,

            [Description("Анализ датчиков")]
            SENSORS_ANALYSIS,

            [Description("Анализ функций")]
            FUNCTIONS_ANALYSIS,

            [Description("Генерация отчётов по датчикам")]
            SENSORS_REPORTS,

            [Description("Генерация отчётов по функциям")]
            FUNCTIONS_REPORT
        }

        #region Настройки

        /// <summary>
        /// Выбран интерактивный режим импорта трасс?
        /// </summary>
        internal static bool IsInteractiveMode;

        /// <summary>
        /// Путь к директории с трассами для импорта
        /// </summary>
        internal static string TracesDirectoryPath;

        /// <summary>
        /// Принимать во внимание датчики, стоящие только после ключевого слова?
        /// </summary>
        internal static bool IsKeyWord;

        /// <summary>
        /// Ключевое слово
        /// </summary>
        internal static string KeyWord;

        /// <summary>
        /// Принимать во внимание датчики только в заданном диапазоне?
        /// </summary>
        internal static bool IsRange;

        /// <summary>
        /// Нижняя граница диапазона
        /// </summary>
        internal static ulong RangeFrom;

        /// <summary>
        /// Верхняя граница диапазона
        /// </summary>
        internal static ulong RangeTo;

        /// <summary>
        /// Произвести смещение датчиков?
        /// </summary>
        internal static bool IsOffset;

        /// <summary>
        /// Величина смещения
        /// </summary>
        internal static ulong Offset;

        /// <summary>
        /// Создавать восстановленные трассы по датчикам?
        /// </summary>
        internal static bool IsGenerateRestoredTraces;

        /// <summary>
        /// Помечать функции, отмеченные как входы в программу и не имеющие вызовов, как существенные?
        /// </summary>
        internal static bool IsMarkNotCallableEntriesAsProgramEntry;

        /// <summary>
        /// Помечать функции, отмеченные как входы в программу, как существенные?
        /// </summary>
        internal static bool IsMarkAllEntriesAsProgramEntry;

        /// <summary>
        /// Помечать все вызываемые функции как существенные?
        /// </summary>
        internal static bool IsMarkCalledFunctions;

        /// <summary>
        /// Список путей до сторонних библиотек в исходных файлах.
        /// </summary>
        internal static List<string> ForeignLibsPaths;

        /// <summary>
        /// Список файлов с именами избыточных функций, для которых разработчик дал обоснование их необходимости
        /// </summary>
        internal static List<string> NecessaryFunctionsFiles;

        /// <summary>
        /// Путь к файлу со списоком функций для проведения контроля вхождения
        /// </summary>
        internal static string ControlFunctionsFilePath;

        /// <summary>
        /// Список файлов со списками встроенных функций в языки программирования
        /// </summary>
        internal static List<string> InternalFunctionsFiles;

        /// <summary>
        /// Режим обработки трасс
        /// </summary>
        internal static DynamicAnalysisResultsProcessing.Modes Mode;

        #endregion Настройки
    }

    /// <summary>
    /// Класс реализующий интерфейс плагина
    /// </summary>
    public sealed class DynamicAnalysisResultsProcessing : IA.Plugin.Interface
    {
        /// <summary>
        /// Варианты режимов обработки трасс
        /// </summary>
        public enum Modes
        {
            /// <summary>
            /// Обработка трасс в соответствии с 2 уровнем контроля НДВ
            /// </summary>
            [Description("Обработка трасс в соответствии с 2 уровнем контроля НДВ")]
            NDV2 = 2,

            /// <summary>
            /// Обработка трасс в соответствии с 3 уровнем контроля НДВ
            /// </summary>
            [Description("Обработка трасс в соответствии с 3 уровнем контроля НДВ")]
            NDV3 = 3,

            /// <summary>
            /// Обработка трасс только для вычисления покрытия
            /// </summary>
            [Description("Обработка трасс только для вычисления покрытия")]
            CoverOnly = 1
        }

        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Форма настроек плагина
        /// </summary>
        private IA.Plugin.Settings settings;

        /// <summary>
        /// Форма прогресса плагина
        /// </summary>
        internal static Progress progress
        {
            get { return _progress ?? (_progress = new Progress(0)); }
        }

        private static Progress _progress;

        /// <summary>
        /// Форма результатов плагина
        /// </summary>
        private ResultWindow results;

        /// <summary>
        /// Объект класса импортера. Импортер отвечает за импорт трасс и доступен из любой части плагина.
        /// </summary>
        internal static TracesImporter importer;

        /// <summary>
        /// Объект класса обработки трасс по 3 уровню контроля НДВ.
        /// </summary>
        internal static TracesChecker3lvl checker;

        /// <summary>
        /// Объект класса обработки трасс по 2 уровню контроля НДВ.
        /// </summary>
        internal static TraceChecker2lvl checker2;

        /// <summary>
        /// Объект класса, реализующего вычисление покрытия.
        /// </summary>
        internal static TracesCover cover;

        /// <summary>
        /// Объект вспомогательного класса с необходимым функционалом.
        /// </summary>
        internal AuxiliaryJobs jobMaster;
                
        /// <summary>
        /// Список непредвиденных функций
        /// </summary>
        internal static HashSet<UInt64> fullUnforeseenTransitionsList;

        /// <summary>
        /// Флаг плавной остановки плагина. TRUE - запущена плавная остановка. FALSE - плагин работает.
        /// </summary>
        internal static bool STOP { get; set; }

        #region PluginInterface

        /// <summary>
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            if (storage == null || storage.sensors.EnumerateSensors().FirstOrDefault() == null)
            {
                IA.Monitor.Log.Warning(Name, "В хранилище не задано ни одного датчика!");
                return;
            }

            if (cover == null)
            {
                Monitor.Log.Warning(PluginSettings.Name,
                    "Для формирования отчета необходимо перезапустить плагин.\nВ настройках в поле \"Каталог с трассами для обработки\" достаточно указать путь к пустой папке.", true);
                return;
            }

            if (PluginSettings.Mode != Modes.CoverOnly)
            {
                #region Отчёт по проверке корректности

                string checkerReportsPath = Path.Combine(reportsDirectoryPath, "Проверка корректности трасс");
                string checkerReportsStoragePath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "TraceChecker");

                //Создаём директорию для отчётов
                if (!DirectoryController.IsExists(checkerReportsPath))
                    DirectoryController.Create(checkerReportsPath);

                if (DirectoryController.IsExists(checkerReportsStoragePath))
                {
                    //Дополнительный протокол
                    //В протокол помещается перечень всех функций, признаваемых входами в программу, из всех трасс.       
                    Log logFullUnforeseenTransitions = storage.logs("TraceChecker\\00_TraceCheckerFullUnforeseenTransitionsList.log");
                    foreach (UInt64 functionID in fullUnforeseenTransitionsList)
                    {
                        IFunction function = storage.functions.GetFunction(functionID);
                        Location functionLocation = function.Definition();

                        //Функции без места определения не логируем
                        if (functionLocation == null)
                            continue;

                        logFullUnforeseenTransitions.Warning(
                            functionID.ToString() + "\t"
                            + function.FullNameForReports + "\t"
                            + functionLocation.GetFile().FileNameForReports + "\t"
                            + functionLocation.GetLine().ToString() + "\t"
                            + functionLocation.GetColumn().ToString());
                    }
                    logFullUnforeseenTransitions.StopWriting();

                    //Готовим информацию для отображения прогресса
                    ulong counter = 0;
                    var files = DirectoryController.GetFiles(checkerReportsStoragePath, false);
                    counter = (ulong)files.Count;

                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.PROTOCOLS_CORRECTNESS_TRACES.Description(), Monitor.Tasks.Task.UpdateType.STAGES, counter);

                    //Просто копируем логи по каждой трассе из Хранилища в указанную папку
                    counter = 0;

                    foreach (string file in files)
                    {
                        string fileName = Path.GetFileName(file);

                        if (fileName.Contains("_TraceChecker")
                            || fileName.Contains("_TraceStatStatements.log"))
                            FileController.Copy(file, Path.Combine(checkerReportsPath, fileName));

                        //отображаем прогресс
                        IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.TasksReport.PROTOCOLS_CORRECTNESS_TRACES.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                    }

                    //Удаляем дополнительный протокол
                    logFullUnforeseenTransitions.Clear();
                }

                #endregion Отчёт по проверке корректности
            }

            #region Отчёт по проверке покрытия
            
            string coverReportsPath = reportsDirectoryPath + "\\Проверка покрытия трасс";

            //Создаём директорию для отчётов
            if (!DirectoryController.IsExists(coverReportsPath))
                DirectoryController.Create(coverReportsPath);

            //Генерируем отчёты
            cover.GenerateReports(coverReportsPath);

            #endregion Отчёт по проверке покрытия
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;
            
            PluginSettings.IsInteractiveMode = Properties.Settings.Default.IsInteractiveMode;
            PluginSettings.TracesDirectoryPath = Properties.Settings.Default.TracesDirectoryPath;
            PluginSettings.Mode = Properties.Settings.Default.Mode.ToEnum<Modes>(Modes.CoverOnly);
            PluginSettings.IsKeyWord = Properties.Settings.Default.IsKeyWord;
            PluginSettings.KeyWord = Properties.Settings.Default.KeyWord;
            PluginSettings.IsRange = Properties.Settings.Default.IsRange;
            PluginSettings.RangeFrom = Properties.Settings.Default.RangeFrom;
            PluginSettings.RangeTo = Properties.Settings.Default.RangeTo;
            PluginSettings.IsGenerateRestoredTraces = Properties.Settings.Default.IsGenerateRestoredTraces;
            PluginSettings.IsOffset = Properties.Settings.Default.IsOffset;
            PluginSettings.Offset = Properties.Settings.Default.Offset;
            PluginSettings.IsMarkNotCallableEntriesAsProgramEntry = Properties.Settings.Default.IsMarkNotCallableEntriesAsProgramEntry;
            PluginSettings.IsMarkAllEntriesAsProgramEntry = Properties.Settings.Default.IsMarkAllEntriesAsProgramEntry;
            PluginSettings.IsMarkCalledFunctions = Properties.Settings.Default.IsMarkCalledFunctions;
            PluginSettings.NecessaryFunctionsFiles = Properties.Settings.Default.NecessaryFunctionsFilesCollection != null ?
                                                            Properties.Settings.Default.NecessaryFunctionsFilesCollection.ToList() :
                                                                new List<string>();
            PluginSettings.ControlFunctionsFilePath = Properties.Settings.Default.ControlFunctionsFilePath;
            PluginSettings.InternalFunctionsFiles = Properties.Settings.Default.InternalFunctionsFilesCollection != null ?
                                                            Properties.Settings.Default.InternalFunctionsFilesCollection.ToList() :
                                                                new List<string>();

            PluginSettings.ForeignLibsPaths = Properties.Settings.Default.ForeignLibsPaths != null ?
                                                            Properties.Settings.Default.ForeignLibsPaths.ToList() :
                                                                new List<string>();

            //Выгружаем информацию о непредвиденных переходах
            IBufferReader storageReader = storage.pluginResults.LoadResult(this.ID);
            if(storageReader != null)
            {
                int count = storageReader.GetInt32();
                for (int i = 0; i < count; i++)
                {
                    fullUnforeseenTransitionsList.Add(storageReader.GetUInt64());
                }
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            bool b = false;

            settingsBuffer.GetBool(ref b);
            PluginSettings.IsInteractiveMode = b;

            PluginSettings.TracesDirectoryPath = settingsBuffer.GetString();

            PluginSettings.Mode = settingsBuffer.GetInt32().ToEnum<Modes>(Modes.CoverOnly);

            settingsBuffer.GetBool(ref b);
            PluginSettings.IsKeyWord = b;
            PluginSettings.KeyWord = settingsBuffer.GetString();

            settingsBuffer.GetBool(ref b);
            PluginSettings.IsRange = b;
            PluginSettings.RangeFrom = settingsBuffer.GetUInt64();
            PluginSettings.RangeTo = settingsBuffer.GetUInt64();

            settingsBuffer.GetBool(ref b);
            PluginSettings.IsOffset = b;
            PluginSettings.Offset = settingsBuffer.GetUInt64();

            settingsBuffer.GetBool(ref b);
            PluginSettings.IsMarkNotCallableEntriesAsProgramEntry = b;
            settingsBuffer.GetBool(ref b);
            PluginSettings.IsMarkAllEntriesAsProgramEntry = b;
            settingsBuffer.GetBool(ref b);
            PluginSettings.IsMarkCalledFunctions = b;

            int necessaryFunctionsFilesCount = settingsBuffer.GetInt32();
            for (int i = 0; i < necessaryFunctionsFilesCount; i++)
                PluginSettings.NecessaryFunctionsFiles.Add(settingsBuffer.GetString());

            PluginSettings.ControlFunctionsFilePath = settingsBuffer.GetString();

            int internalFunctionsFilesCount = settingsBuffer.GetInt32();
            for (int i = 0; i < internalFunctionsFilesCount; i++)
                PluginSettings.InternalFunctionsFiles.Add(settingsBuffer.GetString());
            try
            {
                settingsBuffer.GetBool(ref b);
                PluginSettings.IsGenerateRestoredTraces = b;
            }
            catch
            {
                PluginSettings.IsGenerateRestoredTraces = true;
            }

            try
            {
                int foreignLibsPathsCount = settingsBuffer.GetInt32();
                for (int i = 0; i < foreignLibsPathsCount; i++)
                    PluginSettings.ForeignLibsPaths.Add(settingsBuffer.GetString());
            }
            catch
            {
                
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.IsInteractiveMode = false;
            PluginSettings.IsKeyWord = false;
            PluginSettings.KeyWord = String.Empty;
            PluginSettings.IsRange = false;
            PluginSettings.RangeFrom = 1;
            PluginSettings.RangeTo = 1;
            PluginSettings.IsOffset = false;
            PluginSettings.Offset = 0;
            PluginSettings.IsGenerateRestoredTraces = false;
            PluginSettings.IsMarkNotCallableEntriesAsProgramEntry = false;
            PluginSettings.IsMarkAllEntriesAsProgramEntry = false;
            PluginSettings.IsMarkCalledFunctions = true;
            PluginSettings.NecessaryFunctionsFiles = new List<string>();
            PluginSettings.ControlFunctionsFilePath = String.Empty;
            PluginSettings.InternalFunctionsFiles = new List<string>();
            PluginSettings.TracesDirectoryPath = Path.Combine(settingsString, @"DynamicAnalysis\Traces\");

            PluginSettings.Mode = Modes.NDV3;
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            if (storage.sensors.EnumerateSensors().FirstOrDefault() == null)
            {
                Monitor.Log.Warning(Name, "В хранилище не задано ни одного датчика. Работа плагина остановлена.");
                return;
            }

            //Создаём новые объекты рабочих модулей при каждом запуске, чтобы они правильно инициализировались при перезапусках с разными настройками
            cover = new TracesCover(storage);
            importer = new TracesImporter(storage, storage.GetTempDirectory(ID).Path);
            checker = new TracesChecker3lvl(storage);
            checker2 = new TraceChecker2lvl(storage);
            jobMaster = new AuxiliaryJobs(storage.GetTempDirectory(ID).Path);
            
            fullUnforeseenTransitionsList = new HashSet<UInt64>();

            // Построение таблиц перекодировки номеров датчиков и бинарного массива
            cover.MakeSensorsToBitArrayTable();

            //Загружаем битовый массив (при необходимости расширяется)
            cover.LoadBitArray(storage.pluginData.GetDataFileName(ID));

            switch (PluginSettings.Mode)
            {
                case Modes.NDV2:
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.NDV2_TRANSITION_TABLES.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)storage.sensors.Count());
                    checker2.InitDependencies();
                    break;

                case Modes.NDV3:
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.NDV3_FUNCTIONS_CALLS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)storage.functions.Count);
                    checker.FillFunctionCallsTable();
                    break;

                default:
                    break;
            }

            //Подгатавливаем всё необходимое для старта
            if (!jobMaster.PreparePlugin())
                return;

            //создаём поток для обработки
            var task = System.Threading.Tasks.Task.Factory.StartNew(() => jobMaster.ProcessThreadFunction());
            task.Wait();
            
            //Сохраняем битовый массив в файл с данными плагина
            cover.SaveBitArray(storage.pluginData.GetDataFileName(ID));
            
            return;
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
            //Сохраняем информацию о непредвиденных переходах
            IBufferWriter storageWriter = WriterPool.Get();
            storageWriter.Add(fullUnforeseenTransitionsList.Count);
            foreach(UInt64 functionID in fullUnforeseenTransitionsList)
            {
                storageWriter.Add(functionID);
            }
            storage.pluginResults.SaveResults(ID, storageWriter);
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.IsInteractiveMode = PluginSettings.IsInteractiveMode;
            Properties.Settings.Default.TracesDirectoryPath = PluginSettings.TracesDirectoryPath;
            int mode = Convert.ToInt32(PluginSettings.Mode);
            Properties.Settings.Default.Mode = mode;
            Properties.Settings.Default.IsKeyWord = PluginSettings.IsKeyWord;
            Properties.Settings.Default.KeyWord = PluginSettings.KeyWord;
            Properties.Settings.Default.IsRange = PluginSettings.IsRange;
            Properties.Settings.Default.RangeFrom = PluginSettings.RangeFrom;
            Properties.Settings.Default.RangeTo = PluginSettings.RangeTo;
            Properties.Settings.Default.IsOffset = PluginSettings.IsOffset;
            Properties.Settings.Default.Offset = PluginSettings.Offset;
            Properties.Settings.Default.IsGenerateRestoredTraces = PluginSettings.IsGenerateRestoredTraces;
            Properties.Settings.Default.IsMarkNotCallableEntriesAsProgramEntry = PluginSettings.IsMarkNotCallableEntriesAsProgramEntry;
            Properties.Settings.Default.IsMarkAllEntriesAsProgramEntry = PluginSettings.IsMarkAllEntriesAsProgramEntry;
            Properties.Settings.Default.IsMarkCalledFunctions = PluginSettings.IsMarkCalledFunctions;
            Properties.Settings.Default.NecessaryFunctionsFilesCollection = PluginSettings.NecessaryFunctionsFiles.ToStringCollection();
            Properties.Settings.Default.ControlFunctionsFilePath = PluginSettings.ControlFunctionsFilePath;
            Properties.Settings.Default.InternalFunctionsFilesCollection = PluginSettings.InternalFunctionsFiles.ToStringCollection();
            Properties.Settings.Default.ForeignLibsPaths = PluginSettings.ForeignLibsPaths.ToStringCollection();
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.IsInteractiveMode);
            settingsBuffer.Add(PluginSettings.TracesDirectoryPath);
            settingsBuffer.Add(mode);
            settingsBuffer.Add(PluginSettings.IsKeyWord);
            settingsBuffer.Add(PluginSettings.KeyWord);
            settingsBuffer.Add(PluginSettings.IsRange);
            settingsBuffer.Add(PluginSettings.RangeFrom);
            settingsBuffer.Add(PluginSettings.RangeTo);
            settingsBuffer.Add(PluginSettings.IsOffset);
            settingsBuffer.Add(PluginSettings.Offset);
            settingsBuffer.Add(PluginSettings.IsMarkNotCallableEntriesAsProgramEntry);
            settingsBuffer.Add(PluginSettings.IsMarkAllEntriesAsProgramEntry);
            settingsBuffer.Add(PluginSettings.IsMarkCalledFunctions);

            settingsBuffer.Add(PluginSettings.NecessaryFunctionsFiles.Count);
            foreach (string file in PluginSettings.NecessaryFunctionsFiles)
                settingsBuffer.Add(file);

            settingsBuffer.Add(PluginSettings.ControlFunctionsFilePath);

            settingsBuffer.Add(PluginSettings.InternalFunctionsFiles.Count);
            foreach (string file in PluginSettings.InternalFunctionsFiles)
                settingsBuffer.Add(file);

            settingsBuffer.Add(PluginSettings.IsGenerateRestoredTraces);

            settingsBuffer.Add(PluginSettings.ForeignLibsPaths.Count);
            foreach (string file in PluginSettings.ForeignLibsPaths)
                settingsBuffer.Add(file);
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new SimpleSettings());
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new CustomSettings());
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return _progress = new Progress(cover != null ? cover.GetPercent() : 0);
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return results = new ResultWindow((cover != null) ? cover.GetPercent() : 0);
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = String.Empty;

            if (settings != null)
                settings.Save();

            //Проверяем наличие директории для импорта
            if (!IOController.DirectoryController.IsExists(PluginSettings.TracesDirectoryPath))
            {
                message = "Путь к директории с трассами для импорта не найден.";
                return false;
            }

            if (PluginSettings.IsRange && PluginSettings.RangeFrom > PluginSettings.RangeTo)
            {
                message = "Нижняя граница диапазона должна быть меньше либо равна верхней границе.";
                return false;
            }

            return true;
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                List<Monitor.Tasks.Task> tasks = new List<Monitor.Tasks.Task>();
                try
                {
                    switch (PluginSettings.Mode)
                    {
                        case Modes.NDV2:
                            tasks.Add(PluginSettings.Tasks.NDV2_TRANSITION_TABLES.ToMonitorTask(Name));
                            break;

                        case Modes.NDV3:
                            tasks.Add(PluginSettings.Tasks.NDV3_FUNCTIONS_CALLS.ToMonitorTask(Name));
                            break;

                        default:
                            break;
                    }
                }
                catch
                { }

                tasks.Add(PluginSettings.Tasks.TRACES_PROCESSING.ToMonitorTask(Name));

                return tasks;
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                List<Monitor.Tasks.Task> reportTasks = new List<Monitor.Tasks.Task>();
                EnumLoop<PluginSettings.TasksReport>.ForEach(f => reportTasks.Add(f.ToMonitorTask(Name)));
                return reportTasks;
            }
        }

        /// <summary>
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
            DynamicAnalysisResultsProcessing.STOP = true;
        }
        
        /// <summary>
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW | Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.RESULT_WINDOW | Plugin.Capabilities.REPORTS | Plugin.Capabilities.RERUN | Plugin.Capabilities.RUN_WINDOW | Plugin.Capabilities.STOP_SMOOTHLY;
            }
        }
        #endregion PluginInterface
    }
}