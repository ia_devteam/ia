﻿using System.Collections.Generic;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing.NDV2
{

    /// <summary>
    /// Шаблон класса стека
    /// </summary>
    internal class CallStack
    {
        /// <summary>
        /// Стек реализован как список
        /// </summary>
        List<ulong> array = new List<ulong>();

        /// <summary>
        /// Поместить в стек
        /// </summary>
        /// <param name="sensorId">элемент стека (шаблон класса)</param>
        /// <returns>элемент стека</returns>
        public ulong Push(ulong sensorId)
        {
            array.Add(sensorId);
            return sensorId;
        }

        /// <summary>
        /// Взять из стека и уменьшить стек
        /// </summary>
        /// <returns>элемент стека</returns>
        public ulong Pop()
        {
            if (array.Count > 0)
            {
                ulong ret = array[array.Count - 1];
                array.RemoveAt(array.Count - 1);
                return ret;
            }
            else
            {
                return default(ulong);
            }
        }

        /// <summary>
        /// Взять из стека (n) элементов и уменьшить стек
        /// </summary>
        /// <param name="nsteps">количество (n)</param>
        /// <returns>последний взятый элемент стека</returns>
        public ulong Pop(int nsteps)
        {
            ulong ret = default(ulong);
            for (; array.Count > nsteps && nsteps > 0; nsteps--)
            {
                Pop();
            }
            ret = peek();
            return ret;
        }

        /// <summary>
        /// Взять элемент из стека, не уменьшая стек
        /// </summary>
        /// <returns>элемент стека</returns>
        public ulong peek()
        {
            if (array.Count > 0)
            {
                ulong ret = array[array.Count - 1];
                return ret;
            }
            else
            {
                return default(ulong);
            }
        }

        /// <summary>
        /// Взять определенный элемент из стека (n)
        /// </summary>
        /// <param name="index">номер элемента от начала стека (n)</param>
        /// <returns>элемент стека</returns>
        public ulong peek(int index)
        {
            if (array.Count > index && index >= 0)
            {
                ulong ret = array[index];
                return ret;
            }
            else
            {
                return default(ulong);
            }
        }

        /// <summary>
        /// Размер стека
        /// </summary>
        public int Count
        {
            get
            {
                return array.Count;
            }
        }

        /// <summary>
        /// Очистка стека
        /// </summary>
        public void Clear()
        {
            array.Clear();
        }

        /// <summary>
        /// Проверка, что стек пустой
        /// </summary>
        /// <returns>true - если пустой</returns>
        public bool Empty()
        {
            return array.Count == 0;
        }
    }


}
