﻿using System;
using System.Collections.Generic;

using Store;
using System.IO;
using IA.Extensions;
using IA.Plugins.Analyses.DynamicAnalysisResultsProcessing.NDV2;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    /// <summary>
    /// Модуль проверки трасс в соответствии с 2 уровнем контроля НДВ
    /// </summary>
    public sealed class TraceChecker2lvl
    {
        /// <summary>
        /// Текущее Хранилище для работы.
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Класс хранит характеристики и зависимости датчиков
        /// </summary>
        private class SensorDependencies
        {
            public static readonly string StatementDoesNotExist = "Отсутствует";
            public ulong sensor;
            public string StName;
            public List<ulong> Calls;
            public List<ulong> Nexts;
            public bool IsReturn;
            public bool IsBreak;
            public bool IsStartFunction;
            public SensorDependencies(ulong sns)
            {
                sensor = sns;
                StName = StatementDoesNotExist;
                Calls = new List<ulong>();
                Nexts = new List<ulong>();
                IsReturn = false;
                IsBreak = false;
                IsStartFunction = false;
            }
        }

        /// <summary>
        /// Словарь содержит соответствие между номером датчика и его зависимостями от других датчиков.
        /// </summary>
        Dictionary<ulong, SensorDependencies> sensorsDepenciesDic = new Dictionary<ulong, SensorDependencies>();

        /// <summary>
        /// Стек вызовов датчиков в трассе.
        /// </summary>
        CallStack sensorsCallStack = new CallStack();

        /// <summary>
        /// Список имён функций, встроенных в язык программирования. Если датчик вставлен в такую функцию, его вызов игнорируется.
        /// </summary>
        public static HashSet<string> FuncHash = new HashSet<string>();
        /// <summary>
        /// Словарь возможных выражений (ENStatementKind) с числом обработанных выражений каждого типа.
        /// </summary>
        public static Dictionary<string, int> statStmts = new Dictionary<string, int>();

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage"></param>
        public TraceChecker2lvl(Storage storage)
        {
            this.storage = storage;
        }

        /// <summary>
        /// Построение таблицы зависимостей датчиков
        /// </summary>
        public void InitDependencies()
        {
            FuncHash.Clear();
            statStmts.Clear();

            foreach (string s in Enum.GetNames(typeof(ENStatementKind)))
            {
                statStmts.Add(s, 0);
            }

            statStmts.Add(SensorDependencies.StatementDoesNotExist, 0);

            foreach (string filename in PluginSettings.InternalFunctionsFiles)
            {
                try
                {
                    string[] sa = File.ReadAllLines(filename);
                    foreach (string s in sa)
                        FuncHash.Add(s);
                }
                catch
                {
                }
            }

            ulong count = 0;
            foreach (Sensor sns in storage.sensors.EnumerateSensors())
            {
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.NDV2_TRANSITION_TABLES.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++count);

                sensorsDepenciesDic.Add(sns.ID, new SensorDependencies(sns.ID));
                IStatement stmt = sns.GetBeforeStatement();
                if (stmt != null)
                {
                    if (stmt.NextInLinearBlock != null && stmt.NextInLinearBlock.SensorBeforeTheStatement != null)
                        sensorsDepenciesDic[sns.ID].Nexts.Add(stmt.NextInLinearBlock.SensorBeforeTheStatement.ID);
                    else
                        sensorsDepenciesDic[sns.ID].IsBreak = true;

                    sensorsDepenciesDic[sns.ID].StName = stmt.Kind.ToString();



                    if (sns.GetFunction() != null && sns.GetFunction().EntryStatement == stmt)
                    {
                        //Начало функции (для возвратов)
                        sensorsDepenciesDic[sns.ID].IsStartFunction = true;
                    }
                    try
                    {
                        switch ((ENStatementKind)stmt.Kind)
                        {
                            case ENStatementKind.STDoAndWhile:
                                IStatementDoAndWhile isdw = stmt as IStatementDoAndWhile;
                                if (isdw.IsCheckConditionBeforeFirstRun) //while do
                                {
                                    if (isdw.Condition != null)
                                    {
                                        operationInnerFunctions(isdw.Condition, sns.ID);
                                    }
                                    if (isdw.Body != null && isdw.Body.SensorBeforeTheStatement != null)
                                    {
                                        sensorsDepenciesDic[sns.ID].Calls.Add(isdw.Body.SensorBeforeTheStatement.ID);
                                    }
                                }
                                else //do while
                                {
                                    if (isdw.Body != null && isdw.Body.SensorBeforeTheStatement != null)
                                    {
                                        sensorsDepenciesDic[sns.ID].Calls.Add(isdw.Body.SensorBeforeTheStatement.ID);
                                    }
                                    if (isdw.Condition != null)
                                    {
                                        operationInnerFunctions(isdw.Condition, sns.ID);
                                    }
                                }
                                break;

                            case ENStatementKind.STFor:
                                IStatementFor ifor = stmt as IStatementFor;
                                if (ifor.Start != null)
                                    operationInnerFunctions(ifor.Start, sns.ID);
                                if (ifor.Condition != null)
                                    operationInnerFunctions(ifor.Condition, sns.ID);

                                if (ifor.Body != null && ifor.Body.SensorBeforeTheStatement != null)
                                {
                                    sensorsDepenciesDic[sns.ID].Calls.Add(ifor.Body.SensorBeforeTheStatement.ID);
                                }
                                if (ifor.Iteration != null)
                                    operationInnerFunctions(ifor.Iteration, sns.ID);
                                break;

                            case ENStatementKind.STForEach:
                                IStatementForEach ifch = stmt as IStatementForEach;
                                if (ifch.Head != null)
                                {
                                    operationInnerFunctions(ifch.Head, sns.ID);
                                }
                                if (ifch.Body != null && ifch.Body.SensorBeforeTheStatement != null)
                                {
                                    sensorsDepenciesDic[sns.ID].Calls.Add(ifch.Body.SensorBeforeTheStatement.ID);
                                }
                                break;

                            case ENStatementKind.STGoto:
                                IStatementGoto igt = stmt as IStatementGoto;
                                if (igt.Destination != null && igt.Destination.SensorBeforeTheStatement != null)
                                    sensorsDepenciesDic[sns.ID].Nexts.Add(igt.Destination.SensorBeforeTheStatement.ID);
                                break;

                            case ENStatementKind.STIf:
                                IStatementIf iif = stmt as IStatementIf;

                                if (iif.Condition != null)
                                {
                                    operationInnerFunctions(iif.Condition, sns.ID);
                                }
                                if (iif.ThenStatement != null && iif.ThenStatement.SensorBeforeTheStatement != null)
                                {
                                    sensorsDepenciesDic[sns.ID].Calls.Add(iif.ThenStatement.SensorBeforeTheStatement.ID);
                                }
                                if (iif.ElseStatement != null)
                                {
                                    if (iif.ElseStatement.SensorBeforeTheStatement != null)
                                    {
                                        sensorsDepenciesDic[sns.ID].Calls.Add(iif.ElseStatement.SensorBeforeTheStatement.ID);
                                    }
                                    //Иначе собираем все elseif
                                    else
                                    {
                                        IStatementIf iifprev, iifnext;
                                        if (iif.ElseStatement.Kind == ENStatementKind.STIf)
                                        {
                                            iifprev = iif;
                                            while (true)
                                            {
                                                if (iifprev.ElseStatement.Kind == ENStatementKind.STIf)
                                                {
                                                    iifnext = iifprev.ElseStatement as IStatementIf;

                                                    if (iifnext.Condition != null)
                                                    {
                                                        operationInnerFunctions(iifnext.Condition, sns.ID);
                                                    }
                                                    if (iifnext.ThenStatement != null && iifnext.ThenStatement.SensorBeforeTheStatement != null)
                                                    {
                                                        sensorsDepenciesDic[sns.ID].Calls.Add(iifnext.ThenStatement.SensorBeforeTheStatement.ID);
                                                    }
                                                    if (iifnext.ElseStatement != null)
                                                    {
                                                        if (iifnext.ElseStatement.SensorBeforeTheStatement != null)
                                                        {
                                                            sensorsDepenciesDic[sns.ID].Calls.Add(iifnext.ElseStatement.SensorBeforeTheStatement.ID);
                                                            break;
                                                        }
                                                        else
                                                            iifprev = iifnext;
                                                    }
                                                    else
                                                        break;
                                                }
                                                else
                                                    break;
                                            }
                                        }
                                    }
                                }

                                break;

                            case ENStatementKind.STOperational:
                                IStatementOperational iso = stmt as IStatementOperational;
                                if (iso.Operation != null)
                                {
                                    operationInnerFunctions(iso.Operation, sns.ID);
                                }
                                break;

                            case ENStatementKind.STSwitch:
                                IStatementSwitch isw = stmt as IStatementSwitch;
                                if (isw.Condition != null)
                                {
                                    operationInnerFunctions(isw.Condition, sns.ID);
                                }
                                foreach (KeyValuePair<IOperation, IStatement> pair in isw.Cases())
                                {
                                    operationInnerFunctions(pair.Key, sns.ID);
                                    if (pair.Value != null && pair.Value.SensorBeforeTheStatement != null)
                                    {
                                        sensorsDepenciesDic[sns.ID].Calls.Add(pair.Value.SensorBeforeTheStatement.ID);
                                    }
                                }
                                if (isw.DefaultCase() != null && isw.DefaultCase().SensorBeforeTheStatement != null)
                                {
                                    sensorsDepenciesDic[sns.ID].Calls.Add(isw.DefaultCase().SensorBeforeTheStatement.ID);
                                }
                                break;

                            case ENStatementKind.STThrow:
                                IStatementThrow isth = stmt as IStatementThrow;
                                if (isth.Exception != null)
                                {
                                    operationInnerFunctions(isth.Exception, sns.ID);
                                }
                                break;

                            case ENStatementKind.STTryCatchFinally:
                                IStatementTryCatchFinally istcf = stmt as IStatementTryCatchFinally;
                                if (istcf.TryBlock != null && istcf.TryBlock.SensorBeforeTheStatement != null)
                                {
                                    sensorsDepenciesDic[sns.ID].Calls.Add(istcf.TryBlock.SensorBeforeTheStatement.ID);
                                }
                                if (istcf.ElseBlock != null && istcf.ElseBlock.SensorBeforeTheStatement != null)
                                {
                                    sensorsDepenciesDic[sns.ID].Calls.Add(istcf.ElseBlock.SensorBeforeTheStatement.ID);
                                }
                                if (istcf.FinallyBlock != null && istcf.FinallyBlock.SensorBeforeTheStatement != null)
                                {
                                    sensorsDepenciesDic[sns.ID].Calls.Add(istcf.FinallyBlock.SensorBeforeTheStatement.ID);
                                }
                                foreach (IStatement ist in istcf.Catches())
                                {
                                    if (ist.SensorBeforeTheStatement != null)
                                        sensorsDepenciesDic[sns.ID].Calls.Add(ist.SensorBeforeTheStatement.ID);
                                }
                                break;

                            case ENStatementKind.STUsing:
                                IStatementUsing isu = stmt as IStatementUsing;
                                if (isu.Head != null)
                                {
                                    operationInnerFunctions(isu.Head, sns.ID);
                                }
                                if (isu.Body != null && isu.Body.SensorBeforeTheStatement != null)
                                {
                                    sensorsDepenciesDic[sns.ID].Calls.Add(isu.Body.SensorBeforeTheStatement.ID);
                                }
                                break;

                            case ENStatementKind.STBreak:
                                sensorsDepenciesDic[sns.ID].IsBreak = true;
                                break;

                            case ENStatementKind.STContinue:
                                //переписываем все списки от начала цикла
                                IStatementContinue icnt = stmt as IStatementContinue;
                                if (icnt.ContinuingLoop != null && icnt.ContinuingLoop.SensorBeforeTheStatement != null)
                                {
                                    ulong id = icnt.ContinuingLoop.SensorBeforeTheStatement.ID;
                                    foreach (ulong ts in sensorsDepenciesDic[id].Calls)
                                        sensorsDepenciesDic[sns.ID].Calls.Add(ts);
                                    foreach (ulong ts in sensorsDepenciesDic[id].Nexts)
                                        sensorsDepenciesDic[sns.ID].Calls.Add(ts);
                                    sensorsDepenciesDic[sns.ID].IsReturn = sensorsDepenciesDic[id].IsReturn;
                                    sensorsDepenciesDic[sns.ID].IsBreak = sensorsDepenciesDic[id].IsBreak;
                                }
                                break;

                            case ENStatementKind.STReturn:
                                IStatementReturn iret = stmt as IStatementReturn;
                                sensorsDepenciesDic[sns.ID].IsReturn = true;
                                break;

                            default:
                                throw new Store.Const.StorageException("Неизвестный вид оператора");
                        }
                    }
                    catch (Exception ex)
                    {
                        string error = ex.Message;
                    }
                }
                else
                {
                    Monitor.Log.Error(PluginSettings.Name, "Отсутствует Statement для датчика " + sns.ID);
                }
            }

            Sensor prev = null;
            foreach (Sensor sns in storage.sensors.EnumerateSensors())
            {
                if (prev == null)
                {
                    prev = sns;
                    continue;
                }

                if (sns.GetFunctionID() == ulong.MaxValue)
                    continue;

                if (prev.GetFunction().FullName != sns.GetFunction().FullName)
                {
                    sensorsDepenciesDic[prev.ID].IsReturn = true;
                }
                prev = sns;
            }

            if (prev != null)
                sensorsDepenciesDic[prev.ID].IsReturn = true;
        }

        /// <summary>
        /// Обход вызываемых в рамках данной операции функций с занесением номеров датчиков в цепочки от данного. См. <c>SensorDependencies</c>.
        /// </summary>
        /// <param name="operation"><c>IOperation</c> с которого спрашивать исполняемые функции.</param>
        /// <param name="sensorID">Идентификатор сенсора в операции.</param>
        private void operationInnerFunctions(IOperation operation, ulong sensorID)
        {
            foreach (var ifunctions in operation.SequentiallyCallsFunctions())
                foreach (IFunction function in ifunctions)
                {
                    IStatement entryStatement = function.EntryStatement;
                    if (entryStatement != null)
                    {
                        Sensor sensorBeforeStatement = entryStatement.SensorBeforeTheStatement;

                        if (sensorBeforeStatement != null)
                            sensorsDepenciesDic[sensorID].Calls.Add(function.EntryStatement.SensorBeforeTheStatement.ID);
                    }
                    else
                    {
                        if (function.IsExternal && !FuncHash.Contains(function.Name))
                        {
                            sensorsDepenciesDic[sensorID].Calls.Add(0);
                        }
                    }
                }
        }

        /// <summary>
        /// Функция выполняет обход цепочки датчиков, находя неожиданные разрывы и протоколируя свои шаги в логи.
        /// </summary>
        /// <param name="prevSensor">предыдущий датчик</param>
        /// <param name="nextSensor">следующий датчик</param>
        /// <param name="newCall">вызов подпрограммы</param>
        /// <param name="logger">класс для формирования лога</param>
        /// <param name="sensorSerialNumberInTrace">Порядковый номер датчика в трассе.</param>
        /// <returns>элемент стека</returns>
        internal ulong CheckSensor(ulong prevSensor, ulong nextSensor, bool newCall, Logger logger, int sensorSerialNumberInTrace)
        {
            if (prevSensor == 0 && sensorsCallStack.Empty() && newCall)
            {
                return sensorsCallStack.Push(nextSensor); //Начало цепочки
            }

            if (prevSensor == 0)
            {
                // Разрыв цепочки - лог
                logger.ErrMsg(sensorSerialNumberInTrace, nextSensor, "Разрыв цепочки. Отсутствует prev");
                logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Разрыв цепочки. Отсутствует prev");
                return sensorsCallStack.Push(nextSensor); //Начало цепочки
            }

            if (sensorsCallStack.Empty())
            {
                // Разрыв цепочки - лог
                logger.ErrMsg(sensorSerialNumberInTrace, nextSensor, "Разрыв цепочки. Пустой стек");
                logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Разрыв цепочки. Пустой стек");
                return sensorsCallStack.Push(nextSensor); //Начало цепочки
            }

            try
            {
                if (prevSensor != sensorsCallStack.peek())
                    prevSensor = sensorsCallStack.peek();

                SensorDependencies prevSensorDepencies = sensorsDepenciesDic[prevSensor];

                if (prevSensorDepencies.Calls.Contains(nextSensor))
                {
                    //Вызов функции - лог
                    logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Вызов функции");
                    return sensorsCallStack.Push(nextSensor);
                }

                if (prevSensorDepencies.Nexts.Contains(nextSensor))
                {
                    ulong se = sensorsCallStack.Pop();
                    //Переход - лог
                    logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Переход");
                    return sensorsCallStack.Push(nextSensor);
                }

                if (prevSensorDepencies.Calls.Contains(0)) //этот код очень близок условию через одни вниз (помечено ***)
                {
                    //Вызов функции - лог
                    for (int i = 0; i < prevSensorDepencies.Calls.Count; i++)
                    {
                        if (prevSensorDepencies.Calls[i] == 0)
                        {
                            prevSensorDepencies.Calls[i] = nextSensor;
                            break;
                        }
                    }
                    logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Вызов функции");
                    return sensorsCallStack.Push(nextSensor);
                }

                if (prevSensorDepencies.IsReturn)
                {
                    int index;

                    index = LocateCall(nextSensor);
                    if (index >= 0)
                    {
                        ulong se = sensorsCallStack.Pop(index);
                        logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Возврат и вызов функции");
                        return CheckSensor(se, nextSensor, newCall, logger, sensorSerialNumberInTrace);

                    }
                    index = LocateNext(nextSensor);
                    if (index >= 0)
                    {
                        ulong se = sensorsCallStack.Pop(index);
                        logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Возврат и переход");
                        return CheckSensor(se, nextSensor, newCall, logger, sensorSerialNumberInTrace);

                    }
                    index = LocateEmpty();
                    if (index >= 0)
                    {
                        ulong se = sensorsCallStack.Pop(index);
                        for (int j = 0; j < sensorsDepenciesDic[se].Calls.Count; j++)
                        {
                            if (sensorsDepenciesDic[se].Calls[j] == 0)
                            {
                                sensorsDepenciesDic[se].Calls[j] = nextSensor;
                                break;
                            }
                        }
                        logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Несколько возвратов и вызов функции");
                        return sensorsCallStack.Push(nextSensor);
                    }

                    {
                        logger.ErrMsg(sensorSerialNumberInTrace, nextSensor, "Разрыв цепочки. Не найден уровень возврата из подпрограммы");
                        logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Разрыв цепочки. Не найден уровень возврата из подпрограммы");
                        sensorsCallStack.Clear();
                        return sensorsCallStack.Push(nextSensor); //Начало цепочки
                    }
                }

                if (prevSensorDepencies.Calls.Contains(0)) // ***
                {
                    if (!prevSensorDepencies.IsReturn)
                    {
                        //Вызов функции - лог
                        for (int i = 0; i < prevSensorDepencies.Calls.Count; i++)
                        {
                            if (prevSensorDepencies.Calls[i] == 0)
                            {
                                prevSensorDepencies.Calls[i] = nextSensor;
                                break;
                            }
                        }
                        logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Несколько возвратов и вызов функции");
                        return sensorsCallStack.Push(nextSensor);
                    }
                }

                if (prevSensorDepencies.IsBreak)
                {
                    if (sensorsCallStack.Count > 0)
                    {
                        sensorsCallStack.Pop();
                        prevSensor = sensorsCallStack.peek();
                        logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Возврат из функции");
                        return CheckSensor(prevSensor, nextSensor, newCall, logger, sensorSerialNumberInTrace);
                    }
                    else
                    {
                        // Разрыв цепочки - лог
                        logger.ErrMsg(sensorSerialNumberInTrace, nextSensor, "Разрыв цепочки. Не найден уровень Break");
                        logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Разрыв цепочки. Не найден уровень Break");
                        return sensorsCallStack.Push(nextSensor); //Начало цепочки
                    }
                }
            }
            catch
            {
                // Не удалось найти датчик (prev) - лог
                logger.ErrMsg(sensorSerialNumberInTrace, nextSensor, "Не удалось найти такой датчик");
                logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Не удалось найти такой датчик");
                return sensorsCallStack.Push(nextSensor); //Начало цепочки
            }
            // Разрыв цепочки - лог
            logger.ErrMsg(sensorSerialNumberInTrace, nextSensor, "Неопределенная ошибка/Непредвиденный переход");
            logger.AllMess(sensorSerialNumberInTrace, nextSensor, "Неопределенная ошибка/Непредвиденный переход");
            return sensorsCallStack.Push(nextSensor); //Начало цепочки
        }

        /// <summary>
        /// Поиск вызова функции в стеке датчиков
        /// </summary>
        /// <param name="toFind">номер искомого датчика</param>
        /// <returns>номер элемента стека</returns>
        internal int LocateCall(ulong toFind)
        {
            int i, k;
            bool bFound = false;
            for (i = sensorsCallStack.Count - 1, k = 0; i >= 0; i--, k++)
            {
                if (sensorsDepenciesDic[sensorsCallStack.peek(i)].Calls.Contains(toFind))
                {
                    bFound = true;
                    break;
                }
            }
            if (bFound)
                return k;
            else
                return -1;

        }

        /// <summary>
        /// Поиск следующих датчиков в стеке датчиков
        /// </summary>
        /// <param name="toFind">номер искомого датчика</param>
        /// <returns>номер элемента стека</returns>
        internal int LocateNext(ulong toFind)
        {
            int i, k;
            bool bFound = false;
            for (i = sensorsCallStack.Count - 1, k = 0; i >= 0; i--, k++)
            {
                if (sensorsDepenciesDic[sensorsCallStack.peek(i)].Nexts.Contains(toFind))
                {
                    bFound = true;
                    break;
                }
            }
            if (bFound)
                return k;
            else
                return -1;

        }

        /// <summary>
        /// Поиск свободной функции в списках вызовов в стеке
        /// </summary>
        /// <returns>номер элемента стека</returns>
        internal int LocateEmpty()
        {
            int i, k;
            bool bFound = false;
            for (i = sensorsCallStack.Count - 1, k = 0; i >= 0; i--, k++)
            {
                if (sensorsDepenciesDic[sensorsCallStack.peek(i)].Calls.Contains(0))
                {
                    bFound = true;
                    break;
                }
            }
            if (bFound)
                return k;
            else
                return -1;

        }

        /// <summary>
        /// Функция протоколирует встретившиеся в трассе переходы.
        /// </summary>
        /// <param name="trace">Трасса для проверки.</param>
        internal void CheckTrace(ITrace trace)
        {
            //Если трасса не передана, то выходим
            if (trace == null)
                return;

            //Нет датчиков
            if (sensorsDepenciesDic.Count == 0)
                return;

            //Заводим новый чистый объект класса журналирования
            Logger logger = new Logger(storage, trace.Id);
            logger.Clear();

            //Запуск журналирования
            logger.AnalyseStarted();

            Sensor sensor;
            ulong prev = 0;
            int currentSensorSerialNumber = 0;

            while ((sensor = trace.NextSensor()) != null)
            {
                IFunction currentFunction = sensor.GetFunction();
                //если функции в Хранилище нет, то это не есть хорошо
                if (currentFunction == null)
                {
                    Monitor.Log.Error(PluginSettings.Name, "Не удалось найти функцию, соответствующую датчику: " + sensor.ID);
                    continue;
                }
                logger.SensorIsCalled(sensor);
                //отмечаем вызов датчика
                DynamicAnalysisResultsProcessing.cover.Call(sensor.ID);

                if (sensorsCallStack.Count == 0)
                {
                    logger.EntryFunction(sensor);
                }

                statStmts[sensorsDepenciesDic[sensor.ID].StName]++;

                currentSensorSerialNumber++;

                prev = CheckSensor(prev, sensor.ID, (currentFunction.EntryStatement != null) ? currentFunction.EntryStatement.SensorBeforeTheStatement == sensor : true, logger, currentSensorSerialNumber);
            }

            foreach (KeyValuePair<string, int> pair in statStmts)
            {
                logger.logStatStatements.Warning(pair.Key + ": " + pair.Value.ToString("### ### ### ### ### ###"));
            }

            //Остановка журналирования
            logger.AnalyseFinished();
            logger.Close();

            //закрываем трассу
            trace.Close();
        }

    }
}
