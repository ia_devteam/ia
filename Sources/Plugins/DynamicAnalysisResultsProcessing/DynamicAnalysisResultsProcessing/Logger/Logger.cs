using System;
using System.Collections.Generic;
using Store;
using System.IO;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    /// <summary>
    /// Класс формирования отчётов о корректности трасс.
    /// </summary>
    internal sealed class Logger
    {
        Functions functions;
        Storage storage;

        /// <summary>
        /// Лог, в который записываются все обнаруженные несоответствия.
        /// </summary>
        internal Log logMistakes { get; private set; }

        /// <summary>
        /// Лог, в который записывается восстановленная трасса.
        /// </summary>
        internal Log logTraceRestored { get; private set; }

        /// <summary>
        /// В протокол помещается статистика по элементам языка.
        /// </summary>
        internal Log logStatStatements { get; private set; }

        /// <summary>
        /// В протокол помещается перечень функций, признаваемых входами в программу.
        /// </summary>
        internal Log logUnforeseenTransitions { get; private set; }
        
        /// <summary>
        /// Перечень функций, признанных входами в программу.
        /// </summary>
        HashSet<UInt64> EntryFunctions;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="storage">Текущее Хранилище для работы.</param>
        /// <param name="traceID">Идентификатор трассы.</param>
        internal Logger(Storage storage, ulong traceID)
        {
            this.functions = storage.functions;
            this.storage = storage;

            string traceCheckerDirectory = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "TraceChecker");

            if (!IOController.DirectoryController.IsExists(traceCheckerDirectory))
                IOController.DirectoryController.Create(traceCheckerDirectory);
                        
            logMistakes = storage.logs("TraceChecker\\" + traceID + "_TraceCheckerLog.log");
            logTraceRestored = storage.logs("TraceChecker\\" + traceID + "_TraceCheckerRestoredTrace.log");
            logUnforeseenTransitions = storage.logs("TraceChecker\\" + traceID + "_TraceCheckerUnforeseenTransitions.log");
            logStatStatements = storage.logs("TraceChecker\\" + traceID + "_TraceStatStatements.log");
        }

        /// <summary>
        /// Функция очистки журнала
        /// </summary>
        internal void Clear()
        {
            logMistakes.Clear();
            logTraceRestored.Clear();
            logUnforeseenTransitions.Clear();
            logStatStatements.Clear();
        }

        /// <summary>
        /// Функция закрытия журнала.
        /// </summary>
        internal void Close()
        {
            logMistakes.StopWriting();
            logTraceRestored.StopWriting();
            logUnforeseenTransitions.StopWriting();
            logStatStatements.StopWriting();
        }

        /// <summary>
        /// Функция начала журналирования.
        /// </summary>
        internal void AnalyseStarted()
        {
            EntryFunctions = new HashSet<ulong>();
        }

        /// <summary>
        /// Данная функция вызывается при первой встрече датчика при обработке трасс
        /// </summary>
        /// <param name="sensorId">Текущий датчик.</param>
        internal void SensorIsCalled(Sensor sensorId)
        {
            //Выполняем пометку избыточности
            MarkFunctionEssential(sensorId.GetFunction(), enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED);
        }

        /// <summary>
        /// Функция конца журналирования.
        /// </summary>
        internal void AnalyseFinished()
        {
            //пишем в лог все найденные входы в программу
            foreach (var funcID in EntryFunctions)
            {
                IFunction function = storage.functions.GetFunction(funcID);
                Location functionLocation = function.Definition();
                
                //Функции без места определения не логируем
                if (functionLocation == null)
                    continue;

                logUnforeseenTransitions.Warning(
                    funcID.ToString() + "\t"
                    + function.FullNameForReports + "\t"
                    + functionLocation.GetFile().FileNameForReports + "\t"
                    + functionLocation.GetLine().ToString() + "\t"
                    + functionLocation.GetColumn().ToString());
            }
            EntryFunctions = null;
        }

        /// <summary>
        /// Функция пишет в лог ошибок (например, вход в программу)  (Используется алгоритмом для 2 уровня).//TODO: WUT???
        /// </summary>
        /// <param name="serialNumber">Порядковый номер датчика в трассе.</param>
        /// <param name="sensorNumber">Номер датчика.</param>
        /// <param name="message">Сообщение для записи в лог.</param>
        internal void ErrMsg(int serialNumber, ulong sensorNumber, string message)
        {
            Sensor sensor = storage.sensors.GetSensor(sensorNumber);
            IFunction function = sensor.GetFunction();
            logMistakes.Warning("" + serialNumber + ". " + message + ". Датчик " + sensorNumber + " в функции " + function.Name);
        }

        /// <summary>
        /// Функция пишет в лог трасс с обработанными именами функций (Используется алгоритмом для 2 уровня).//TODO: WUT???
        /// </summary>
        /// <param name="serialNumber">Порядковый номер датчика в трассе.</param>
        /// <param name="sensorNumber">Номер датчика.</param>
        /// <param name="message">Сообщение для записи в лог.</param>
        internal void AllMess(int serialNumber, ulong sensorNumber, string message)
        {
            if (!PluginSettings.IsGenerateRestoredTraces)
                return;

            Sensor sensor = storage.sensors.GetSensor(sensorNumber);
            IFunction function = sensor.GetFunction();
            logTraceRestored.Warning("" + serialNumber + ". " + message + ". Датчик " + sensorNumber + " в функции " + function.Name);
        }


        /// <summary>
        /// Функция пишет в лог, что текущий датчик является входом в программу.
        /// </summary>
        /// <param name="sensorId">Текущий датчик.</param>
        internal void EntryFunction(Sensor sensorId)
        {
            IFunction function = sensorId.GetFunction();

            logMistakes.Warning("Обнаружен непредвиденный переход. Датчик " + sensorId.ID.ToString() + " в функции " + function.Name);

            if (PluginSettings.IsGenerateRestoredTraces)
                logTraceRestored.Warning("Непредвиденный переход: вошли в функцию. Датчик " + sensorId.ID.ToString() + " в функции " + function.Name);

            if (!EntryFunctions.Contains(function.Id))
            {
                EntryFunctions.Add(function.Id);

                if (!DynamicAnalysisResultsProcessing.fullUnforeseenTransitionsList.Contains(function.Id))
                    DynamicAnalysisResultsProcessing.fullUnforeseenTransitionsList.Add(function.Id);

                //Проверяем, надо ли помечать неизбыточностью
                if (PluginSettings.IsMarkNotCallableEntriesAsProgramEntry)
                {
                    IEnumerator<IFunctionCall> en = function.CalledBy().GetEnumerator();
                    if (!en.MoveNext()) //то есть существует хотя бы один вызов функции
                        MarkFunctionEssential(function, enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES);
                    en.Dispose();
                }

                if (PluginSettings.IsMarkAllEntriesAsProgramEntry)
                    MarkFunctionEssential(function, enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES);
            }
        }

        /// <summary>
        /// Функция пишет в лог, что текущий датчик находится в текущей функции.
        /// </summary>
        /// <param name="sensorId">Текущий датчик.</param>
        internal void SensorInSameFunction(Sensor sensorId)
        {
            if (!PluginSettings.IsGenerateRestoredTraces)
                return;

            logTraceRestored.Warning("Датчик в той же функции. Датчик " + sensorId.ID.ToString() + " в функции " + sensorId.GetFunction().Name);
        }

        /// <summary>
        /// Функция пишет в лог, что текущий датчик является выходом из функции.
        /// </summary>
        /// <param name="sensorId">Текущий датчик.</param>
        internal void PopStackFunction(UInt64 sensorId)
        {
            if (!PluginSettings.IsGenerateRestoredTraces)
                return;

            Sensor sensor = storage.sensors.GetSensor(sensorId);
            logTraceRestored.Warning("Вышли из функции " + sensor.GetFunction().Name + ". Датчик " + sensor.ID.ToString());
        }

        /// <summary>
        /// Функция пишет в лог, что была вызвана функция соотвтетсвующая текущему датчику.
        /// </summary>
        /// <param name="sensorId">Текущий датчик.</param>
        internal void FunctionCall(Sensor sensorId)
        {
            if (!PluginSettings.IsGenerateRestoredTraces)
                return;

            logTraceRestored.Warning("Вызвали функцию " + sensorId.GetFunction().Name + ". Датчик " + sensorId.ID.ToString());
        }
        
        /// <summary>
        /// Вспомогательный метод записи в лог сообщения. 
        /// </summary>
        /// <param name="log">Лог.</param>
        /// <param name="message">сообщение</param>
        internal void LogMessage(Log log, string message)
        {
            log.Warning(message);
        }

        /// <summary>
        /// Функция размечает функции в Хранилище в зависимости от встретившихся в трассах датчиков.
        /// </summary>
        /// <param name="function"></param>
        /// <param name="mark"></param>
        /// <param name="funcCallsAlreadyCounted">Список идентификаторов функций, уже пройденных в рамках выставления флагов значимости в рекурсии.</param>
        private void MarkFunctionEssential(IFunction function, enFunctionEssentialMarkedTypes mark, HashSet<ulong> funcCallsAlreadyCounted = null)
        {
            //Проверка на цикличность при выставлении значимости в рекурсии и флаге IsMarkCalledFunctions
            if (funcCallsAlreadyCounted != null &&
                funcCallsAlreadyCounted.Contains(function.Id))
                return;

            //Проверяем, что ранее эта функция не была помечена другим плагином.
            //Заодно пропускаем пометку enFunctionEssentialMarkedType.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES
            //Если была - пропускаем
            if (function.EssentialMarkedType != enFunctionEssentialMarkedTypes.NOTHING &&
                function.EssentialMarkedType != enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED &&
                function.EssentialMarkedType != enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES)
                return;

            //Помечаем функцию, как существенную
            if (function.Essential != enFunctionEssentials.ESSENTIAL)
                function.Essential = enFunctionEssentials.ESSENTIAL;

            //На больших проектах уходит в слишком глубокую рекурсию, пока закомментрую, надо переделывать.
            //Если задано в настройках, то помечаем все вызываемые функции как существенные
            if (PluginSettings.IsMarkCalledFunctions)
            {
                if (funcCallsAlreadyCounted == null)
                    funcCallsAlreadyCounted = new HashSet<ulong>();

                funcCallsAlreadyCounted.Add(function.Id);

                foreach (IFunctionCall funcCall in function.Calles())
                {
                    MarkFunctionEssential(funcCall.Calles, enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED, funcCallsAlreadyCounted);
                }
            }

            if (mark == enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES)
            {
                //Поднимаем пометку
                function.EssentialMarkedType = enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES;
                return;
            }

            if (function.EssentialMarkedType == enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES)
                //Выше поднять не сможем
                return;

            if (mark == enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES)
            {
                //Поднимаем
                function.EssentialMarkedType = enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES;
                return;
            }

            //Остался последний вариант поднятия. Проверку выполняем для оптимизации: меньше сохранений в базу
            if (function.EssentialMarkedType == enFunctionEssentialMarkedTypes.NOTHING)
                function.EssentialMarkedType = enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED;

        }
    }
}
