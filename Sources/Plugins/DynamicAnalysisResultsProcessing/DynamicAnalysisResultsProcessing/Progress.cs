﻿using System;
using System.Windows.Forms;

namespace IA.Plugins.Analyses.DynamicAnalysisResultsProcessing
{
    /// <summary>
    /// Класс формы текущего прогресса плагина.
    /// </summary>
    internal partial class Progress : UserControl
    {
        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="percent">Процент покрытия для отображения</param>
        internal Progress(int percent)
        {
            InitializeComponent();
            UpdatePercent(percent);
        }

        /// <summary>
        /// Функция обновления процента покрытия на форме.
        /// </summary>
        /// <param name="value">Процент покрытия для отображеня.</param>
        internal void UpdatePercent(int value)
        {
            if (value < 0 || value > 100)
                throw new Exception("Процент покрытия должен быть в диапазоне от 0 до 100.");
            percent_label.Text = value.ToString() + "%";
            percent_label.Update();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DynamicAnalysisResultsProcessing.STOP = true;
        }
    }
}
