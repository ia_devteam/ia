﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestUtils;
using System.IO;
using IA.Plugins.Analyses.SQLAnalyse;

namespace TestSqlAnalyse
{
    [TestClass]
    public class TestSqlAnalyse
    {
        #region Константы
        private string SqlServerName = "";
        private string SqlServerLogin = "";
        private string SqlServerPassword = "";
        #endregion
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            TestUtilsClass.SQL_SetupTestConnection();

            SqlServerName = IA.Sql.ServerConfiguration.Name;
            SqlServerLogin = IA.Sql.ServerConfiguration.Login;
            SqlServerPassword = IA.Sql.ServerConfiguration.Password;
        }

        /// <summary>
        /// База данных Microsoft SQL (официальный пример БД для SQL 2000), пример с тригеррами
        /// </summary>
        [TestMethod]
        public void SQLAnalyse_MSSQL_Pubs()
        {

            testUtils.RunTest(
                //sourcePrefixPart
                string.Empty,

                //plugin
                new SQLAnalyse(),

                //isUseCachedStorage
                false,

                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                },

                //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                    settingsBuffer.Add(true);
                    settingsBuffer.Add(SqlServerName);
                    settingsBuffer.Add(false);
                    settingsBuffer.Add(SqlServerLogin);
                    settingsBuffer.Add(SqlServerPassword);
                    settingsBuffer.Add("pubs");
                    settingsBuffer.Add("");
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SQL_ANALYSE, settingsBuffer);

                },

                //checkStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },

                //checkreports
                (reportsPath, testMaterialsPath) =>
                {
                    //Сравниваем директории с отчётами
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"SQLAnalyse\Reports\pubs\"), reportsPath);
                },

                //isUpdateReport
                false,

                //changeSettingsBeforRerun
                (plugin, testMaterialsPath) => { },

                //checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                //checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );

        }

        /// <summary>
        /// База данных Microsoft SQL (официальный пример БД для SQL 2000), пример без тригерров
        /// </summary>
        [TestMethod]
        public void SQLAnalyse_MSSQL_NorthWind()
        {

            testUtils.RunTest(
                //sourcePrefixPart
                string.Empty,

                //plugin
                new SQLAnalyse(),

                //isUseCachedStorage
                false,

                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                },

                //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                    settingsBuffer.Add(true);
                    settingsBuffer.Add(SqlServerName);
                    settingsBuffer.Add(false);
                    settingsBuffer.Add(SqlServerLogin);
                    settingsBuffer.Add(SqlServerPassword);
                    settingsBuffer.Add("Northwind");
                    settingsBuffer.Add("");
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SQL_ANALYSE, settingsBuffer);

                },

                //checkStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },

                //checkreports
                (reportsPath, testMaterialsPath) =>
                {
                    //Сравниваем директории с отчётами
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"SQLAnalyse\Reports\Northwind\"), reportsPath);
                },

                //isUpdateReport
                false,

                //changeSettingsBeforRerun
                (plugin, testMaterialsPath) => { },

                //checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                //checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );

        }

        /// <summary>
        /// База данных Microsoft Office Access (более старых версийб чем 2007), одна процедура и несколько таблиц без связей
        /// </summary>
        [TestMethod, Ignore]
        public void SQLAnalyse_MSAccess_SCHEMA()
        {
            string sourceDirectoryPostfix = @"Sources\SQLAccess";

            testUtils.RunTest(
                //sourcePrefixPart
                string.Empty,

                //plugin
                new SQLAnalyse(),

                //isUseCachedStorage
                false,

                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                },

                //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                    settingsBuffer.Add(false);
                    settingsBuffer.Add("");
                    settingsBuffer.Add(false);
                    settingsBuffer.Add("");
                    settingsBuffer.Add("");
                    settingsBuffer.Add("");
                    settingsBuffer.Add(Path.Combine(testMaterialsPath, sourceDirectoryPostfix, "SCHEMA.MDB"));
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SQL_ANALYSE, settingsBuffer);

                },

                //checkStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },

                //checkreports
                (reportsPath, testMaterialsPath) =>
                {
                    //Сравниваем директории с отчётами
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"SQLAnalyse\Reports\SCHEMA\"), reportsPath);
                },

                //isUpdateReport
                false,

                //changeSettingsBeforRerun
                (plugin, testMaterialsPath) => { },

                //checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                //checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );

        }

        /// <summary>
        /// База данных Microsoft Office Access 2007, одна таблица и несколько запросов без связей
        /// </summary>
        [TestMethod, Ignore]
        public void SQLAnalyse_MSAccess_SytemObjectQueries()
        {
            string sourceDirectoryPostfix = @"Sources\SQLAccess";

            testUtils.RunTest(
                //sourcePrefixPart
                string.Empty,

                //plugin
                new SQLAnalyse(),

                //isUseCachedStorage
                false,

                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                },

                //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                    settingsBuffer.Add(false);
                    settingsBuffer.Add("");
                    settingsBuffer.Add(false);
                    settingsBuffer.Add("");
                    settingsBuffer.Add("");
                    settingsBuffer.Add("");
                    settingsBuffer.Add(Path.Combine(testMaterialsPath, sourceDirectoryPostfix, "SystemObjectQueries.accdb"));
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SQL_ANALYSE, settingsBuffer);

                },

                //checkStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },

                //checkreports
                (reportsPath, testMaterialsPath) =>
                {
                    //Сравниваем директории с отчётами
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"SQLAnalyse\Reports\SytemObjectQueries\"), reportsPath);
                },

                //isUpdateReport
                false,

                //changeSettingsBeforRerun
                (plugin, testMaterialsPath) => { },

                //checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                //checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );

        }

        /// <summary>
        /// База данных Microsoft Office Access 2007, достаточно большая  - есть связи между объектами
        /// </summary>
        [TestMethod, Ignore]
        public void SQLAnalyse_MSAccess_Borei()
        {
            string sourceDirectoryPostfix = @"Sources\SQLAccess";

            testUtils.RunTest(
                //sourcePrefixPart
                string.Empty,

                //plugin
                new SQLAnalyse(),

                //isUseCachedStorage
                false,

                //prepareStorage
                (storage, testMaterialsPath) =>
                {
                },

                //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                    settingsBuffer.Add(false);
                    settingsBuffer.Add("");
                    settingsBuffer.Add(false);
                    settingsBuffer.Add("");
                    settingsBuffer.Add("");
                    settingsBuffer.Add("");
                    settingsBuffer.Add(Path.Combine(testMaterialsPath, sourceDirectoryPostfix, "Борей 2007.accdb"));
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.SQL_ANALYSE, settingsBuffer);

                },

                //checkStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },

                //checkreports
                (reportsPath, testMaterialsPath) =>
                {
                    //Сравниваем директории с отчётами
                    return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"SQLAnalyse\Reports\Borei\"), reportsPath);
                },

                //isUpdateReport
                false,

                //changeSettingsBeforRerun
                (plugin, testMaterialsPath) => { },

                //checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                //checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );

        }
    }
}
