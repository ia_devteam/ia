﻿namespace IA.Plugins.Analyses.SQLAnalyse
{
    partial class CustomSettings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.sql_groupBox = new System.Windows.Forms.GroupBox();
            this.dataBase_label = new System.Windows.Forms.Label();
            this.dataBase_comboBox = new System.Windows.Forms.ComboBox();
            this.main_tabControl = new System.Windows.Forms.TabControl();
            this.msSQL_tabPage = new System.Windows.Forms.TabPage();
            this.msSQL_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.testConnection_button = new System.Windows.Forms.Button();
            this.msAccess_tabPage = new System.Windows.Forms.TabPage();
            this.msAccess_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.access_groupBox = new System.Windows.Forms.GroupBox();
            this.access_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.access_textBoxWithDialogButton = new IA.Controls.TextBoxWithDialogButton();
            this.main_tabControl.SuspendLayout();
            this.msSQL_tabPage.SuspendLayout();
            this.msSQL_tableLayoutPanel.SuspendLayout();
            this.msAccess_tabPage.SuspendLayout();
            this.msAccess_tableLayoutPanel.SuspendLayout();
            this.access_groupBox.SuspendLayout();
            this.access_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sql_groupBox
            // 
            this.msSQL_tableLayoutPanel.SetColumnSpan(this.sql_groupBox, 5);
            this.sql_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sql_groupBox.Location = new System.Drawing.Point(3, 3);
            this.sql_groupBox.Name = "sql_groupBox";
            this.sql_groupBox.Size = new System.Drawing.Size(640, 144);
            this.sql_groupBox.TabIndex = 0;
            this.sql_groupBox.TabStop = false;
            this.sql_groupBox.Text = "Параметры базы данных Microsoft SQL";
            // 
            // dataBase_label
            // 
            this.dataBase_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dataBase_label.AutoSize = true;
            this.dataBase_label.Location = new System.Drawing.Point(3, 158);
            this.dataBase_label.Name = "dataBase_label";
            this.dataBase_label.Size = new System.Drawing.Size(94, 13);
            this.dataBase_label.TabIndex = 1;
            this.dataBase_label.Text = "База данных";
            this.dataBase_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataBase_comboBox
            // 
            this.dataBase_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.msSQL_tableLayoutPanel.SetColumnSpan(this.dataBase_comboBox, 4);
            this.dataBase_comboBox.FormattingEnabled = true;
            this.dataBase_comboBox.Location = new System.Drawing.Point(103, 154);
            this.dataBase_comboBox.Name = "dataBase_comboBox";
            this.dataBase_comboBox.Size = new System.Drawing.Size(540, 21);
            this.dataBase_comboBox.TabIndex = 6;
            this.dataBase_comboBox.DropDown += new System.EventHandler(this.dataBase_comboBox_DropDown);
            // 
            // main_tabControl
            // 
            this.main_tabControl.Controls.Add(this.msSQL_tabPage);
            this.main_tabControl.Controls.Add(this.msAccess_tabPage);
            this.main_tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tabControl.Location = new System.Drawing.Point(0, 0);
            this.main_tabControl.Name = "main_tabControl";
            this.main_tabControl.SelectedIndex = 0;
            this.main_tabControl.Size = new System.Drawing.Size(660, 379);
            this.main_tabControl.TabIndex = 2;
            // 
            // msSQL_tabPage
            // 
            this.msSQL_tabPage.Controls.Add(this.msSQL_tableLayoutPanel);
            this.msSQL_tabPage.Location = new System.Drawing.Point(4, 22);
            this.msSQL_tabPage.Name = "msSQL_tabPage";
            this.msSQL_tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.msSQL_tabPage.Size = new System.Drawing.Size(652, 353);
            this.msSQL_tabPage.TabIndex = 0;
            this.msSQL_tabPage.Text = "MSSQL";
            this.msSQL_tabPage.UseVisualStyleBackColor = true;
            // 
            // msSQL_tableLayoutPanel
            // 
            this.msSQL_tableLayoutPanel.ColumnCount = 5;
            this.msSQL_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.msSQL_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.msSQL_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.msSQL_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.msSQL_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.msSQL_tableLayoutPanel.Controls.Add(this.sql_groupBox, 0, 0);
            this.msSQL_tableLayoutPanel.Controls.Add(this.dataBase_label, 0, 1);
            this.msSQL_tableLayoutPanel.Controls.Add(this.dataBase_comboBox, 1, 1);
            this.msSQL_tableLayoutPanel.Controls.Add(this.testConnection_button, 2, 2);
            this.msSQL_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.msSQL_tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.msSQL_tableLayoutPanel.Name = "msSQL_tableLayoutPanel";
            this.msSQL_tableLayoutPanel.RowCount = 4;
            this.msSQL_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.msSQL_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.msSQL_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.msSQL_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.msSQL_tableLayoutPanel.Size = new System.Drawing.Size(646, 347);
            this.msSQL_tableLayoutPanel.TabIndex = 1;
            // 
            // testConnection_button
            // 
            this.testConnection_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.testConnection_button.Location = new System.Drawing.Point(276, 183);
            this.testConnection_button.Name = "testConnection_button";
            this.testConnection_button.Size = new System.Drawing.Size(94, 24);
            this.testConnection_button.TabIndex = 0;
            this.testConnection_button.Text = "Проверка";
            this.testConnection_button.UseVisualStyleBackColor = true;
            this.testConnection_button.Click += new System.EventHandler(this.testConnection_button_Click);
            // 
            // msAccess_tabPage
            // 
            this.msAccess_tabPage.Controls.Add(this.msAccess_tableLayoutPanel);
            this.msAccess_tabPage.Location = new System.Drawing.Point(4, 22);
            this.msAccess_tabPage.Name = "msAccess_tabPage";
            this.msAccess_tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.msAccess_tabPage.Size = new System.Drawing.Size(652, 353);
            this.msAccess_tabPage.TabIndex = 1;
            this.msAccess_tabPage.Text = "MS Access";
            this.msAccess_tabPage.UseVisualStyleBackColor = true;
            // 
            // msAccess_tableLayoutPanel
            // 
            this.msAccess_tableLayoutPanel.ColumnCount = 1;
            this.msAccess_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.msAccess_tableLayoutPanel.Controls.Add(this.access_groupBox, 0, 0);
            this.msAccess_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.msAccess_tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.msAccess_tableLayoutPanel.Name = "msAccess_tableLayoutPanel";
            this.msAccess_tableLayoutPanel.RowCount = 2;
            this.msAccess_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.msAccess_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.msAccess_tableLayoutPanel.Size = new System.Drawing.Size(646, 347);
            this.msAccess_tableLayoutPanel.TabIndex = 3;
            // 
            // access_groupBox
            // 
            this.access_groupBox.Controls.Add(this.access_tableLayoutPanel);
            this.access_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.access_groupBox.Location = new System.Drawing.Point(3, 3);
            this.access_groupBox.Name = "access_groupBox";
            this.access_groupBox.Size = new System.Drawing.Size(640, 49);
            this.access_groupBox.TabIndex = 2;
            this.access_groupBox.TabStop = false;
            this.access_groupBox.Text = "Файл базы данных Microsoft Access";
            // 
            // access_tableLayoutPanel
            // 
            this.access_tableLayoutPanel.ColumnCount = 1;
            this.access_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.access_tableLayoutPanel.Controls.Add(this.access_textBoxWithDialogButton, 0, 0);
            this.access_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.access_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.access_tableLayoutPanel.Name = "access_tableLayoutPanel";
            this.access_tableLayoutPanel.RowCount = 1;
            this.access_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.access_tableLayoutPanel.Size = new System.Drawing.Size(634, 30);
            this.access_tableLayoutPanel.TabIndex = 0;
            // 
            // access_textBoxWithDialogButton
            // 
            this.access_textBoxWithDialogButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.access_textBoxWithDialogButton.Location = new System.Drawing.Point(3, 3);
            this.access_textBoxWithDialogButton.MaximumSize = new System.Drawing.Size(0, 24);
            this.access_textBoxWithDialogButton.MinimumSize = new System.Drawing.Size(150, 24);
            this.access_textBoxWithDialogButton.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.access_textBoxWithDialogButton.Name = "access_textBoxWithDialogButton";
            this.access_textBoxWithDialogButton.Size = new System.Drawing.Size(628, 24);
            this.access_textBoxWithDialogButton.TabIndex = 0;
            // 
            // CustomSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tabControl);
            this.Name = "CustomSettings";
            this.Size = new System.Drawing.Size(660, 379);
            this.main_tabControl.ResumeLayout(false);
            this.msSQL_tabPage.ResumeLayout(false);
            this.msSQL_tableLayoutPanel.ResumeLayout(false);
            this.msSQL_tableLayoutPanel.PerformLayout();
            this.msAccess_tabPage.ResumeLayout(false);
            this.msAccess_tableLayoutPanel.ResumeLayout(false);
            this.access_groupBox.ResumeLayout(false);
            this.access_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox sql_groupBox;
        private System.Windows.Forms.Label dataBase_label;
        private System.Windows.Forms.ComboBox dataBase_comboBox;
        private System.Windows.Forms.GroupBox access_groupBox;
        private System.Windows.Forms.TableLayoutPanel access_tableLayoutPanel;
        private Controls.TextBoxWithDialogButton access_textBoxWithDialogButton;
        private System.Windows.Forms.TabControl main_tabControl;
        private System.Windows.Forms.TabPage msSQL_tabPage;
        private System.Windows.Forms.TableLayoutPanel msSQL_tableLayoutPanel;
        private System.Windows.Forms.TabPage msAccess_tabPage;
        private System.Windows.Forms.TableLayoutPanel msAccess_tableLayoutPanel;
        private System.Windows.Forms.Button testConnection_button;
    }
}
