﻿using Microsoft.SqlServer.Management.Common;

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using IA.Extensions;

using IOController;

namespace IA.Plugins.Analyses.SQLAnalyse
{
    using DBValues;
    using DBValues.Procedures;

    using MicrosoftSql = Microsoft.SqlServer.Management.Smo;

    /// <summary>
    /// Вспомогательный класс для разбора тестов процедур (парсинга), выделения параметров и переменных
    /// </summary>
    internal class MsDatabaseAnalyzer
    { 
        /// <summary>
        /// Список для хранения импортированных процедур
        /// </summary>
        private List<Procedure> procedures = null;

        /// <summary>
        /// Список для хранения импортированных таблиц
        /// </summary>
        private List<Table> tables = null;

        /// <summary>
        /// Путь до директории для выгрузки различной информации при анализе
        /// </summary>
        private string dataDirectoryPath = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="dataDirectoryPath">Путь до директории для выгрузки различной информации при анализе</param>
        internal MsDatabaseAnalyzer(string dataDirectoryPath)
        {
            //Проверка на разумность
            if (dataDirectoryPath == null)
                throw new ArgumentNullException("scriptsDirectoryPath", "Путь до директории для выгрузки скриптов найденных процедур не определён.");

            //Директория должна существовать и быть пустой
            if (DirectoryController.IsExists(dataDirectoryPath))
                DirectoryController.Clear(dataDirectoryPath);
            else
                DirectoryController.Create(dataDirectoryPath);

            //Инициализация полей
            this.procedures = new List<Procedure>();
            this.tables = new List<Table>();
            this.dataDirectoryPath = dataDirectoryPath;
        }

        #region Анализ
        /// <summary>
        /// Запустить анализ базы данный MS Sql
        /// </summary>
        /// <param name="serverName">Имя сервера</param>
        /// <param name="isWindowsAuthentification">Вид аутентификации</param>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        /// <param name="database">База данных</param>
        internal void MsSql_Analyse(string serverName, bool isWindowsAuthentification, string login, string password, string database)
        {
            //Проверка на разумность
            serverName.ThrowIfNull("Имя серверо не определено.");
            login.ThrowIfNull("Логин не определён.");
            password.ThrowIfNull("Пароль не определён.");
            database.ThrowIfNull("Имя базы данных не определено.");

            //Получаем строку соединенеия
            string connectionString = this.MsSql_GetConnectionString(serverName, isWindowsAuthentification, login, password, database);

            //Получение самих объектов
            var xxx = (new MicrosoftSql.Server(new ServerConnection(new SqlConnection(connectionString))));
            MicrosoftSql.Database db = (new MicrosoftSql.Server(new ServerConnection(new SqlConnection(connectionString)))).Databases[database];
            MicrosoftSql.TableCollection msTables = db.Tables;
            MicrosoftSql.StoredProcedureCollection msProcedures = db.StoredProcedures;
            MicrosoftSql.UserDefinedFunctionCollection msFunctions = db.UserDefinedFunctions;
            MicrosoftSql.DatabaseDdlTriggerCollection msDDLTriggers = db.Triggers;

            // Вычисление общего количества объектов для отображения в мониторе
            int total = msTables.Count;
            msTables.Cast<MicrosoftSql.Table>().ForEach(t => total += t.Triggers.Count);
            total += msProcedures.Count;
            total += msFunctions.Count;
            total += msDDLTriggers.Count;
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SQL_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)total);

            //Счётчик для подсчёта прогресса
            int counter = 0;

            // Таблицы и триггеры
            this.MsSql_Import_Tables(msTables, ref counter);

            // Хранимые процедуры
            this.MsSql_Import_Procedures(msProcedures, ref counter);

            // Функции
            this.MsSql_Import_Functions(msFunctions, ref counter);

            //DDL Триггеры
            this.MsSql_Import_DDLTriggers(msDDLTriggers, ref counter);

            //Запускаем анализ
            Analyse();
        }

        /// <summary>
        /// Запустить анализ базы данный MS Access
        /// </summary>
        /// <param name="accessDatabaseFilePath">Путь до файла базы данных MS Access</param>
        internal void MsAccess_Analyse(string accessDatabaseFilePath)
        {
            //Проверка на разумность
            if (String.IsNullOrWhiteSpace(accessDatabaseFilePath))
                throw new Exception("Путь до файла базы данных MS Access не задан.");

            //Проверка на существование файла
            if (!FileController.IsExists(accessDatabaseFilePath))
                throw new Exception("Путь до файла базы данных MS Access <" + accessDatabaseFilePath + "> не найден.");

            //Получаем строку соединенеия
            string connectionString = this.MsAccess_GetConnectionString(accessDatabaseFilePath);

            using (OleDbConnection databaseConnection = new OleDbConnection(connectionString))
            {
                //Открываем соединение с базой данных
                databaseConnection.Open();

                //Получение самих объектов
                DataTable accessTables = databaseConnection.GetSchema("Tables", new string[] { null, null, null, "TABLE" });
                DataTable accessProcedures = databaseConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Procedures, new object[] { null, null, null, null });

                // Вычисление общего количества объектов для отображения в мониторе
                int total = accessTables.Rows.Count + accessProcedures.Rows.Count;
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.ACCESS_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)total);

                //Счётчик для подсчёта прогресса
                int counter = 0;

                //Таблицы
                this.MsAccess_Import_Tables(accessTables, databaseConnection, ref counter);

                //Процедуры
                this.MsAccess_Import_Procedures(accessProcedures, databaseConnection, ref counter);
            }

            Analyse();
        }

        /// <summary>
        /// олучить строку соединения с базой данных MS Sql
        /// </summary>
        /// <param name="serverName">Имя сервера</param>
        /// <param name="isWindowsAuthentification">Метод аутентификации</param>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        /// <returns></returns>
        private string MsSql_GetConnectionString(string serverName, bool isWindowsAuthentification, string login, string password, string database)
        {
            SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder()
            {
                DataSource = serverName ?? String.Empty,
                InitialCatalog = database ?? String.Empty,
                IntegratedSecurity = isWindowsAuthentification,
                UserID = isWindowsAuthentification ? String.Empty : (login ?? String.Empty),
                Password = isWindowsAuthentification ? String.Empty : (password ?? String.Empty),
                AsynchronousProcessing = true
            };

            return connectionBuilder.ConnectionString;
        }

        /// <summary>
        /// Получить строку соединения с базой данных MS Access
        /// </summary>
        /// <param name="accessDatabaseFilePath">Путь до файла базы данных MS Access</param>
        /// <returns></returns>
        private string MsAccess_GetConnectionString(string accessDatabaseFilePath)
        {
            return @"Provider=Microsoft.ACE.OLEDB.12.0;Persist Security Info=True;Data Source=" + (accessDatabaseFilePath ?? String.Empty) + ";";
        }

        /// <summary>
        /// Запуск анализа накопленной информации
        /// </summary>
        private void Analyse()
        {
            this.procedures.ForEach(p =>
            {
                p.GenerateCallingList(this.procedures);
                p.GenerateGlobalVariables();
                p.GenerateLocalVariables();
                p.GenerateTables(this.tables);
            });
        }

        #region Sql
        /// <summary>
        /// Импорт информации о процедурах из базы данных MS Sql
        /// </summary>
        /// <param name="msProcedures">Список процедур из базы данных. Не может быть Null.</param>
        /// <param name="counter">Счётчик для подсчёта прогресса</param>
        private void MsSql_Import_Procedures(MicrosoftSql.StoredProcedureCollection msProcedures, ref int counter)
        {
            foreach (MicrosoftSql.StoredProcedure msProcedure in msProcedures)
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SQL_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);

                if (msProcedure.IsEncrypted || msProcedure.IsSystemObject)
                    continue;

                string msProcedureName = this.GetObjectName(msProcedure);
                string msProcedureSchema = this.GetObjectSchema(msProcedure);

                //Получаем текст скрипта в виде строки
                string procedure_scriptText = this.GetSqlObjectScript(msProcedure);

                //Если скрипта нет, то ничего не делаем
                if (procedure_scriptText == null)
                    continue;

                //Генерируем скприпт процедуры
                this.GenerateScriptFile(
                    Path.Combine(this.dataDirectoryPath, msProcedureSchema, "sp"),
                    msProcedureName,
                    procedure_scriptText
                );

                Procedure procedure = new Procedure(msProcedure.Name, AbstractProcedure.ProcedureType.PROCEDURE, procedure_scriptText, msProcedure.ToString());

                foreach (MicrosoftSql.StoredProcedureParameter parameter in msProcedure.Parameters)
                {
                    string sOut = "";

                    if (parameter.IsOutputParameter)
                        sOut = " out";

                    if (parameter.DataType.ToString().Contains("char"))
                    {
                        if (parameter.DataType.MaximumLength == -1)
                            procedure.InputParams.Add(new Variable(parameter.Name, parameter.DataType + "(max)" + sOut));
                        else
                            procedure.InputParams.Add(new Variable(parameter.Name, parameter.DataType + "(" + parameter.DataType.MaximumLength + ")" + sOut));
                    }
                    else
                        procedure.InputParams.Add(new Variable(parameter.Name, parameter.DataType + sOut));
                }

                this.procedures.Add(procedure);
            }
        }

        /// <summary>
        /// Импорт информации о функциях из базы данных MS Sql
        /// </summary>
        /// <param name="msFunctions">Список функций из базы данных. Не может быть Null.</param>
        /// <param name="counter">Счётчик для подсчёта прогресса</param>
        private void MsSql_Import_Functions(MicrosoftSql.UserDefinedFunctionCollection msFunctions, ref int counter)
        {
            foreach (MicrosoftSql.UserDefinedFunction msFunction in msFunctions)
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SQL_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);

                if (msFunction.IsEncrypted || msFunction.IsSystemObject)
                    continue;

                string msFunctionSchema = this.GetObjectSchema(msFunction);
                string msFunctionName = this.GetObjectName(msFunction);

                //Получаем текст скрипта в виде строки
                string function_scriptText = this.GetSqlObjectScript(msFunction);

                //Если скрипта нет, то ничего не делаем
                if (function_scriptText == null)
                    continue;

                //Генерируем скприпт функции
                this.GenerateScriptFile(
                    Path.Combine(this.dataDirectoryPath, msFunctionSchema, "fn"),
                    msFunctionName,
                    function_scriptText
                );

                Procedure function = new Procedure(msFunction.Name, AbstractProcedure.ProcedureType.FUNCTION, function_scriptText, msFunction.ToString());

                foreach (MicrosoftSql.UserDefinedFunctionParameter parameter in msFunction.Parameters)
                {
                    if (parameter.DataType.ToString().Contains("char"))
                    {
                        if (parameter.DataType.MaximumLength == -1)
                            function.InputParams.Add(new Variable(parameter.Name, parameter.DataType + "(max)"));
                        else
                            function.InputParams.Add(new Variable(parameter.Name, parameter.DataType + "(" + parameter.DataType.MaximumLength + ")"));
                    }
                    else
                        function.InputParams.Add(new Variable(parameter.Name, parameter.DataType.ToString()));
                }

                this.procedures.Add(function);
            }
        }

        /// <summary>
        /// Импорт информации о триггерах из базы данных MS Sql
        /// </summary>
        /// <param name="msTable">Таблица из базы данных. Не может быть Null.</param>
        /// <param name="counter">Счётчик для подсчёта прогресса</param>
        private void MsSql_Import_Triggers(MicrosoftSql.Table msTable, ref int counter)
        {
            string msTableName = this.GetObjectName(msTable);
            string msTableSchema = this.GetObjectSchema(msTable);

            foreach (MicrosoftSql.Trigger msTrigger in msTable.Triggers)
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SQL_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);

                if (msTrigger.IsEncrypted || msTrigger.IsSystemObject)
                    continue;

                //Получаем текст скрипта в виде строки
                string trigger_scriptText = this.GetSqlObjectScript(msTrigger);

                //Если скрипта нет, то ничего не делаем
                if (trigger_scriptText == null)
                    continue;

                //Генерируем скприпт триггера
                this.GenerateScriptFile(
                    Path.Combine(this.dataDirectoryPath, msTableSchema, "tr", msTableName),
                    msTrigger.Name,
                    trigger_scriptText
                );

                Trigger trigger = new Trigger(msTrigger.Name, AbstractProcedure.ProcedureType.TRIGGER, trigger_scriptText, msTable.ToString());

                if (msTrigger.Insert)
                    trigger.InputParams.Add(new Variable("INSERT", ""));
                if (msTrigger.Update)
                    trigger.InputParams.Add(new Variable("UPDATE", ""));
                if (msTrigger.Delete)
                    trigger.InputParams.Add(new Variable("DELETE", ""));

                this.procedures.Add(trigger);
            }
        }

        /// <summary>
        /// Импорт информации о DDL триггерах из базы данных MS Sql
        /// </summary>
        /// <param name="msDDLTriggers">Список DDL триггеров из базы данных. Не может быть Null.</param>
        /// <param name="counter">Счётчик для подсчёта прогресса</param>
        private void MsSql_Import_DDLTriggers(MicrosoftSql.DatabaseDdlTriggerCollection msDDLTriggers, ref int counter)
        {
            foreach (MicrosoftSql.DatabaseDdlTrigger msDDLTrigger in msDDLTriggers)
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SQL_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);

                if (msDDLTrigger.IsEncrypted && msDDLTrigger.IsSystemObject)
                    continue;

                string msDDLTriggerName = this.GetObjectName(msDDLTrigger);
                string msDDLTriggerSchema = this.GetObjectSchema(msDDLTrigger);
                
                //Получаем текст скрипта в виде строки
                string ddlTrigger_scriptText = this.GetSqlObjectScript(msDDLTrigger);

                //Если скрипта нет, то ничего не делаем
                if (ddlTrigger_scriptText == null)
                    continue;

                //Генерируем скприпт DDL триггера
                this.GenerateScriptFile(
                    Path.Combine(this.dataDirectoryPath, msDDLTriggerSchema, msDDLTrigger.ToString()),
                    msDDLTriggerName,
                    ddlTrigger_scriptText
                );

                Trigger trigger = new Trigger(msDDLTrigger.Name, AbstractProcedure.ProcedureType.DDL_TRIGGER, ddlTrigger_scriptText, String.Empty);

                MicrosoftSql.DatabaseDdlTriggerEventSet msDDlTriggerEventSet = msDDLTrigger.DdlTriggerEvents;

                if (msDDlTriggerEventSet.AddRoleMember)
                    trigger.InputParams.Add(new Variable("AddRoleMember", ""));
                if (msDDlTriggerEventSet.AddSignature)
                    trigger.InputParams.Add(new Variable("AddSignature", ""));
                if (msDDlTriggerEventSet.AddSignatureSchemaObject)
                    trigger.InputParams.Add(new Variable("AddSignatureSchemaObject", ""));
                if (msDDlTriggerEventSet.AlterApplicationRole)
                    trigger.InputParams.Add(new Variable("AlterApplicationRole", ""));
                if (msDDlTriggerEventSet.AlterAssembly)
                    trigger.InputParams.Add(new Variable("AlterAssembly", ""));
                if (msDDlTriggerEventSet.AlterAsymmetricKey)
                    trigger.InputParams.Add(new Variable("AlterAsymmetricKey", ""));
                if (msDDlTriggerEventSet.AlterAuthorizationDatabase)
                    trigger.InputParams.Add(new Variable("AlterAuthorizationDatabase", ""));
                if (msDDlTriggerEventSet.AlterBrokerPriority)
                    trigger.InputParams.Add(new Variable("AlterBrokerPriority", ""));
                if (msDDlTriggerEventSet.AlterCertificate)
                    trigger.InputParams.Add(new Variable("AlterCertificate", ""));
                if (msDDlTriggerEventSet.AlterDatabaseAuditSpecification)
                    trigger.InputParams.Add(new Variable("AlterDatabaseAuditSpecification", ""));
                if (msDDlTriggerEventSet.AlterDatabaseEncryptionKey)
                    trigger.InputParams.Add(new Variable("AlterDatabaseEncryptionKey", ""));
                if (msDDlTriggerEventSet.AlterExtendedProperty)
                    trigger.InputParams.Add(new Variable("AlterExtendedProperty", ""));
                if (msDDlTriggerEventSet.AlterFulltextCatalog)
                    trigger.InputParams.Add(new Variable("AlterFulltextCatalog", ""));
                if (msDDlTriggerEventSet.AlterFulltextIndex)
                    trigger.InputParams.Add(new Variable("AlterFulltextIndex", ""));
                if (msDDlTriggerEventSet.AlterFulltextStoplist)
                    trigger.InputParams.Add(new Variable("AlterFulltextStoplist", ""));
                if (msDDlTriggerEventSet.AlterFunction)
                    trigger.InputParams.Add(new Variable("AlterFunction", ""));
                if (msDDlTriggerEventSet.AlterIndex)
                    trigger.InputParams.Add(new Variable("AlterIndex", ""));
                if (msDDlTriggerEventSet.AlterMasterKey)
                    trigger.InputParams.Add(new Variable("AlterMasterKey", ""));
                if (msDDlTriggerEventSet.AlterMessageType)
                    trigger.InputParams.Add(new Variable("AlterMessageType", ""));
                if (msDDlTriggerEventSet.AlterPartitionFunction)
                    trigger.InputParams.Add(new Variable("AlterPartitionFunction", ""));
                if (msDDlTriggerEventSet.AlterPartitionScheme)
                    trigger.InputParams.Add(new Variable("AlterPartitionScheme", ""));
                if (msDDlTriggerEventSet.AlterPlanGuide)
                    trigger.InputParams.Add(new Variable("AlterPlanGuide", ""));
                if (msDDlTriggerEventSet.AlterProcedure)
                    trigger.InputParams.Add(new Variable("AlterProcedure", ""));
                if (msDDlTriggerEventSet.AlterQueue)
                    trigger.InputParams.Add(new Variable("AlterQueue", ""));
                if (msDDlTriggerEventSet.AlterRemoteServiceBinding)
                    trigger.InputParams.Add(new Variable("AlterRemoteServiceBinding", ""));
                if (msDDlTriggerEventSet.AlterRole)
                    trigger.InputParams.Add(new Variable("AlterRole", ""));
                if (msDDlTriggerEventSet.AlterRoute)
                    trigger.InputParams.Add(new Variable("AlterRoute", ""));
                if (msDDlTriggerEventSet.AlterSchema)
                    trigger.InputParams.Add(new Variable("AlterSchema", ""));
                if (msDDlTriggerEventSet.AlterService)
                    trigger.InputParams.Add(new Variable("AlterService", ""));
                if (msDDlTriggerEventSet.AlterSymmetricKey)
                    trigger.InputParams.Add(new Variable("AlterSymmetricKey", ""));
                if (msDDlTriggerEventSet.AlterTable)
                    trigger.InputParams.Add(new Variable("AlterTable", ""));
                if (msDDlTriggerEventSet.AlterTrigger)
                    trigger.InputParams.Add(new Variable("AlterTrigger", ""));
                if (msDDlTriggerEventSet.AlterUser)
                    trigger.InputParams.Add(new Variable("AlterUser", ""));
                if (msDDlTriggerEventSet.AlterView)
                    trigger.InputParams.Add(new Variable("AlterView", ""));
                if (msDDlTriggerEventSet.AlterXmlSchemaCollection)
                    trigger.InputParams.Add(new Variable("AlterXmlSchemaCollection", ""));
                if (msDDlTriggerEventSet.BindDefault)
                    trigger.InputParams.Add(new Variable("BindDefault", ""));
                if (msDDlTriggerEventSet.BindRule)
                    trigger.InputParams.Add(new Variable("BindRule", ""));
                if (msDDlTriggerEventSet.CreateApplicationRole)
                    trigger.InputParams.Add(new Variable("CreateApplicationRole", ""));
                if (msDDlTriggerEventSet.CreateAssembly)
                    trigger.InputParams.Add(new Variable("CreateAssembly", ""));
                if (msDDlTriggerEventSet.CreateAsymmetricKey)
                    trigger.InputParams.Add(new Variable("CreateAsymmetricKey", ""));
                if (msDDlTriggerEventSet.CreateBrokerPriority)
                    trigger.InputParams.Add(new Variable("CreateBrokerPriority", ""));
                if (msDDlTriggerEventSet.CreateCertificate)
                    trigger.InputParams.Add(new Variable("CreateCertificate", ""));
                if (msDDlTriggerEventSet.CreateContract)
                    trigger.InputParams.Add(new Variable("CreateContract", ""));
                if (msDDlTriggerEventSet.CreateDatabaseAuditSpecification)
                    trigger.InputParams.Add(new Variable("CreateDatabaseAuditSpecification", ""));
                if (msDDlTriggerEventSet.CreateDatabaseEncryptionKey)
                    trigger.InputParams.Add(new Variable("CreateDatabaseEncryptionKey", ""));
                if (msDDlTriggerEventSet.CreateDefault)
                    trigger.InputParams.Add(new Variable("CreateDefault", ""));
                if (msDDlTriggerEventSet.CreateEventNotification)
                    trigger.InputParams.Add(new Variable("CreateEventNotification", ""));
                if (msDDlTriggerEventSet.CreateExtendedProperty)
                    trigger.InputParams.Add(new Variable("CreateExtendedProperty", ""));
                if (msDDlTriggerEventSet.CreateFulltextCatalog)
                    trigger.InputParams.Add(new Variable("CreateFulltextCatalog", ""));
                if (msDDlTriggerEventSet.CreateFulltextIndex)
                    trigger.InputParams.Add(new Variable("CreateFulltextIndex", ""));
                if (msDDlTriggerEventSet.CreateFulltextStoplist)
                    trigger.InputParams.Add(new Variable("CreateFulltextStoplist", ""));
                if (msDDlTriggerEventSet.CreateFunction)
                    trigger.InputParams.Add(new Variable("CreateFunction", ""));
                if (msDDlTriggerEventSet.CreateIndex)
                    trigger.InputParams.Add(new Variable("CreateFunction", ""));
                if (msDDlTriggerEventSet.CreateMasterKey)
                    trigger.InputParams.Add(new Variable("CreateMasterKey", ""));
                if (msDDlTriggerEventSet.CreateMessageType)
                    trigger.InputParams.Add(new Variable("CreateMessageType", ""));
                if (msDDlTriggerEventSet.CreatePartitionFunction)
                    trigger.InputParams.Add(new Variable("CreatePartitionFunction", ""));
                if (msDDlTriggerEventSet.CreatePartitionScheme)
                    trigger.InputParams.Add(new Variable("CreatePartitionScheme", ""));
                if (msDDlTriggerEventSet.CreatePlanGuide)
                    trigger.InputParams.Add(new Variable("CreatePlanGuide", ""));
                if (msDDlTriggerEventSet.CreateProcedure)
                    trigger.InputParams.Add(new Variable("CreateProcedure", ""));
                if (msDDlTriggerEventSet.CreateQueue)
                    trigger.InputParams.Add(new Variable("CreateQueue", ""));
                if (msDDlTriggerEventSet.CreateRemoteServiceBinding)
                    trigger.InputParams.Add(new Variable("CreateRemoteServiceBinding", ""));
                if (msDDlTriggerEventSet.CreateRole)
                    trigger.InputParams.Add(new Variable("CreateRole", ""));
                if (msDDlTriggerEventSet.CreateRoute)
                    trigger.InputParams.Add(new Variable("CreateRoute", ""));
                if (msDDlTriggerEventSet.CreateRule)
                    trigger.InputParams.Add(new Variable("CreateRule", ""));
                if (msDDlTriggerEventSet.CreateSchema)
                    trigger.InputParams.Add(new Variable("CreateSchema", ""));
                if (msDDlTriggerEventSet.CreateService)
                    trigger.InputParams.Add(new Variable("CreateService", ""));
                if (msDDlTriggerEventSet.CreateSpatialIndex)
                    trigger.InputParams.Add(new Variable("CreateSpatialIndex", ""));
                if (msDDlTriggerEventSet.CreateStatistics)
                    trigger.InputParams.Add(new Variable("CreateStatistics", ""));
                if (msDDlTriggerEventSet.CreateSymmetricKey)
                    trigger.InputParams.Add(new Variable("CreateSymmetricKey", ""));
                if (msDDlTriggerEventSet.CreateSynonym)
                    trigger.InputParams.Add(new Variable("CreateSynonym", ""));
                if (msDDlTriggerEventSet.CreateTable)
                    trigger.InputParams.Add(new Variable("CreateTable", ""));
                if (msDDlTriggerEventSet.CreateTrigger)
                    trigger.InputParams.Add(new Variable("CreateTrigger", ""));
                if (msDDlTriggerEventSet.CreateType)
                    trigger.InputParams.Add(new Variable("CreateType", ""));
                if (msDDlTriggerEventSet.CreateUser)
                    trigger.InputParams.Add(new Variable("CreateUser", ""));
                if (msDDlTriggerEventSet.CreateView)
                    trigger.InputParams.Add(new Variable("CreateView", ""));
                if (msDDlTriggerEventSet.CreateXmlIndex)
                    trigger.InputParams.Add(new Variable("CreateXmlIndex", ""));
                if (msDDlTriggerEventSet.CreateXmlSchemaCollection)
                    trigger.InputParams.Add(new Variable("CreateXmlSchemaCollection", ""));
                if (msDDlTriggerEventSet.DdlApplicationRoleEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlApplicationRoleEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlAssemblyEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlAssemblyEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlAsymmetricKeyEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlAsymmetricKeyEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlAuthorizationDatabaseEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlAuthorizationDatabaseEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlBrokerPriorityEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlBrokerPriorityEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlCertificateEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlCertificateEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlContractEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlContractEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlCryptoSignatureEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlCryptoSignatureEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlDatabaseAuditSpecificationEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlDatabaseAuditSpecificationEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlDatabaseEncryptionKeyEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlDatabaseEncryptionKeyEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlDatabaseLevelEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlDatabaseLevelEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlDatabaseSecurityEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlDatabaseSecurityEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlDefaultEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlDefaultEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlEventNotificationEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlEventNotificationEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlExtendedPropertyEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlExtendedPropertyEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlFulltextCatalogEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlFulltextCatalogEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlFulltextStoplistEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlFulltextStoplistEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlFunctionEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlFunctionEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlGdrDatabaseEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlGdrDatabaseEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlIndexEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlIndexEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlMasterKeyEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlMasterKeyEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlMessageTypeEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlMessageTypeEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlPartitionEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlPartitionEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlPartitionFunctionEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlPartitionFunctionEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlPartitionSchemeEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlPartitionSchemeEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlPlanGuideEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlPlanGuideEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlProcedureEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlProcedureEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlQueueEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlQueueEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlRemoteServiceBindingEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlRemoteServiceBindingEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlRoleEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlRoleEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlRuleEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlRuleEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlSchemaEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlSchemaEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlServiceEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlServiceEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlSsbEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlSsbEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlStatisticsEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlStatisticsEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlSymmetricKeyEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlSymmetricKeyEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlSynonymEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlSynonymEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlTableEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlTableEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlTableViewEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlTableViewEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlTriggerEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlTriggerEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlTypeEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlTypeEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlUserEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlUserEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlViewEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlViewEventsEvents", ""));
                if (msDDlTriggerEventSet.DdlXmlSchemaCollectionEventsEvents)
                    trigger.InputParams.Add(new Variable("DdlXmlSchemaCollectionEventsEvents", ""));
                if (msDDlTriggerEventSet.DenyDatabase)
                    trigger.InputParams.Add(new Variable("DenyDatabase", ""));
                if (msDDlTriggerEventSet.Dirty)
                    trigger.InputParams.Add(new Variable("Dirty", ""));
                if (msDDlTriggerEventSet.DropApplicationRole)
                    trigger.InputParams.Add(new Variable("DropApplicationRole", ""));
                if (msDDlTriggerEventSet.DropAssembly)
                    trigger.InputParams.Add(new Variable("DropAssembly", ""));
                if (msDDlTriggerEventSet.DropAsymmetricKey)
                    trigger.InputParams.Add(new Variable("DropAsymmetricKey", ""));
                if (msDDlTriggerEventSet.DropBrokerPriority)
                    trigger.InputParams.Add(new Variable("DropBrokerPriority", ""));
                if (msDDlTriggerEventSet.DropCertificate)
                    trigger.InputParams.Add(new Variable("DropCertificate", ""));
                if (msDDlTriggerEventSet.DropContract)
                    trigger.InputParams.Add(new Variable("DropContract", ""));
                if (msDDlTriggerEventSet.DropDatabaseAuditSpecification)
                    trigger.InputParams.Add(new Variable("DropDatabaseAuditSpecification", ""));
                if (msDDlTriggerEventSet.DropDatabaseEncryptionKey)
                    trigger.InputParams.Add(new Variable("DropDatabaseEncryptionKey", ""));
                if (msDDlTriggerEventSet.DropDefault)
                    trigger.InputParams.Add(new Variable("DropDefault", ""));
                if (msDDlTriggerEventSet.DropEventNotification)
                    trigger.InputParams.Add(new Variable("DropEventNotification", ""));
                if (msDDlTriggerEventSet.DropExtendedProperty)
                    trigger.InputParams.Add(new Variable("DropExtendedProperty", ""));
                if (msDDlTriggerEventSet.DropFulltextCatalog)
                    trigger.InputParams.Add(new Variable("DropFulltextCatalog", ""));
                if (msDDlTriggerEventSet.DropFulltextIndex)
                    trigger.InputParams.Add(new Variable("DropFulltextIndex", ""));
                if (msDDlTriggerEventSet.DropFulltextStoplist)
                    trigger.InputParams.Add(new Variable("DropFulltextStoplist", ""));
                if (msDDlTriggerEventSet.DropFunction)
                    trigger.InputParams.Add(new Variable("DropFunction", ""));
                if (msDDlTriggerEventSet.DropIndex)
                    trigger.InputParams.Add(new Variable("DropIndex", ""));
                if (msDDlTriggerEventSet.DropMasterKey)
                    trigger.InputParams.Add(new Variable("DropMasterKey", ""));
                if (msDDlTriggerEventSet.DropMessageType)
                    trigger.InputParams.Add(new Variable("DropMessageType", ""));
                if (msDDlTriggerEventSet.DropPartitionFunction)
                    trigger.InputParams.Add(new Variable("DropPartitionFunction", ""));
                if (msDDlTriggerEventSet.DropPartitionScheme)
                    trigger.InputParams.Add(new Variable("DropPartitionScheme", ""));
                if (msDDlTriggerEventSet.DropPlanGuide)
                    trigger.InputParams.Add(new Variable("DropPlanGuide", ""));
                if (msDDlTriggerEventSet.DropProcedure)
                    trigger.InputParams.Add(new Variable("DropProcedure", ""));
                if (msDDlTriggerEventSet.DropQueue)
                    trigger.InputParams.Add(new Variable("DropQueue", ""));
                if (msDDlTriggerEventSet.DropRemoteServiceBinding)
                    trigger.InputParams.Add(new Variable("DropRemoteServiceBinding", ""));
                if (msDDlTriggerEventSet.DropRole)
                    trigger.InputParams.Add(new Variable("DropRole", ""));
                if (msDDlTriggerEventSet.DropRoleMember)
                    trigger.InputParams.Add(new Variable("DropRoleMember", ""));
                if (msDDlTriggerEventSet.DropRoute)
                    trigger.InputParams.Add(new Variable("DropRoute", ""));
                if (msDDlTriggerEventSet.DropRule)
                    trigger.InputParams.Add(new Variable("DropRule", ""));
                if (msDDlTriggerEventSet.DropSchema)
                    trigger.InputParams.Add(new Variable("DropSchema", ""));
                if (msDDlTriggerEventSet.DropService)
                    trigger.InputParams.Add(new Variable("DropService", ""));
                if (msDDlTriggerEventSet.DropSignature)
                    trigger.InputParams.Add(new Variable("DropSignature", ""));
                if (msDDlTriggerEventSet.DropSignatureSchemaObject)
                    trigger.InputParams.Add(new Variable("DropSignatureSchemaObject", ""));
                if (msDDlTriggerEventSet.DropStatistics)
                    trigger.InputParams.Add(new Variable("DropStatistics", ""));
                if (msDDlTriggerEventSet.DropSymmetricKey)
                    trigger.InputParams.Add(new Variable("DropSymmetricKey", ""));
                if (msDDlTriggerEventSet.DropSynonym)
                    trigger.InputParams.Add(new Variable("DropSynonym", ""));
                if (msDDlTriggerEventSet.DropTable)
                    trigger.InputParams.Add(new Variable("DropTable", ""));
                if (msDDlTriggerEventSet.DropTrigger)
                    trigger.InputParams.Add(new Variable("DropTrigger", ""));
                if (msDDlTriggerEventSet.DropType)
                    trigger.InputParams.Add(new Variable("DropType", ""));
                if (msDDlTriggerEventSet.DropUser)
                    trigger.InputParams.Add(new Variable("DropUser", ""));
                if (msDDlTriggerEventSet.DropView)
                    trigger.InputParams.Add(new Variable("DropView", ""));
                if (msDDlTriggerEventSet.DropXmlSchemaCollection)
                    trigger.InputParams.Add(new Variable("DropXmlSchemaCollection", ""));
                if (msDDlTriggerEventSet.GrantDatabase)
                    trigger.InputParams.Add(new Variable("GrantDatabase", ""));
                if (msDDlTriggerEventSet.Rename)
                    trigger.InputParams.Add(new Variable("Rename", ""));
                if (msDDlTriggerEventSet.RevokeDatabase)
                    trigger.InputParams.Add(new Variable("RevokeDatabase", ""));
                if (msDDlTriggerEventSet.UnbindDefault)
                    trigger.InputParams.Add(new Variable("UnbindDefault", ""));
                if (msDDlTriggerEventSet.UnbindRule)
                    trigger.InputParams.Add(new Variable("UnbindRule", ""));
                if (msDDlTriggerEventSet.UpdateStatistics)
                    trigger.InputParams.Add(new Variable("UpdateStatistics", ""));

                this.procedures.Add(trigger);
            }
        }

        /// <summary>
        /// Импорт информации о таблицах из базы данных MS Sql
        /// </summary>
        /// <param name="msTables">Список таблиц из базы данных. Не может быть Null.</param>
        /// <param name="counter">Счётчик для подсчёта прогресса</param>
        private void MsSql_Import_Tables(MicrosoftSql.TableCollection msTables, ref int counter)
        {
            const string reportName = "Список таблиц";

            StringBuilder tables_reportText = new StringBuilder();

            foreach (Microsoft.SqlServer.Management.Smo.Table msTable in msTables)
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.SQL_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);

                string msTableName = this.GetObjectName(msTable);

                Table table = new Table(msTable.ToString(), msTableName);

                tables.Add(table);

                tables_reportText.AppendLine();
                tables_reportText.AppendLine(msTable.ToString());

                string msColumnName = String.Empty;
                string msColumnType = String.Empty;
                foreach (MicrosoftSql.Column msColumn in msTable.Columns)
                {
                    msColumnName = msColumn.Name;

                    if (msColumn.DataType.ToString().Contains("char"))
                    {
                        if (msColumn.DataType.MaximumLength == -1)
                        {
                            msColumnType = msColumn.DataType + "(max)";
                            tables_reportText.Append("    " + msColumn.Name + " " + msColumn.DataType + "(max)" + Environment.NewLine);
                        }
                        else
                        {
                            msColumnType = msColumn.DataType + "(" + msColumn.DataType.MaximumLength + ")";
                            tables_reportText.Append("    " + msColumn.Name + " " + msColumn.DataType + "(" + msColumn.DataType.MaximumLength + ")" + Environment.NewLine);
                        }
                    }
                    else
                    {
                        msColumnType = msColumn.DataType.ToString();
                        tables_reportText.Append("    " + msColumn.Name + " " + msColumn.DataType + Environment.NewLine);
                    }

                    table.TableColumns.Add(new Variable(msColumnName, msColumnType));
                }

                this.MsSql_Import_Triggers(msTable, ref counter);
            }

            string reportDirectoryPath = Path.Combine(this.dataDirectoryPath, reportName + ".txt");
            File.WriteAllText(reportDirectoryPath, tables_reportText.ToString());
        }

        /// <summary>
        /// Получаем скприпт объекта базы данных MS SQL
        /// </summary>
        /// <param name="sqlObject">Объект базы данных MS SQL</param>
        /// <returns>Текст скрипта</returns>
        private string GetSqlObjectScript(MicrosoftSql.IScriptable sqlObject)
        {
            StringCollection script = null;

            try
            {
                script = sqlObject.Script();
            }
            catch (Exception)
            {
            }

            if (script == null)
                return null;

            StringBuilder ret = new StringBuilder();

            foreach (string line in script)
                ret.AppendLine(line);

            return ret.ToString();
        }
        #endregion

        #region Access
        /// <summary>
        /// Импорт информации о процедурах из базы данных MS Access
        /// </summary>
        /// <param name="accessProcedures">Список процедур из базы данных. Не может быть Null.</param>
        /// <param name="databaseConnection">Соединение с базой данных</param>
        /// <param name="counter">Счётчик для подсчёта прогресса</param>
        private void MsAccess_Import_Procedures(DataTable accessProcedures, OleDbConnection databaseConnection, ref int counter)
        {
            foreach (DataRow accessProcedure in accessProcedures.Rows)
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.ACCESS_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);

                if (accessProcedure.ItemArray.Count() < 5)
                    continue;

                string accessProcedureName = accessProcedure.ItemArray[2].ToString();
                string accessProcedureBody = accessProcedure.ItemArray[4].ToString();

                //Генерируем скприпт триггера
                this.GenerateScriptFile(
                    Path.Combine(this.dataDirectoryPath, "sp"),
                    accessProcedureName,
                    accessProcedureBody
                );

                Procedure procedure = new Procedure(accessProcedureName, AbstractProcedure.ProcedureType.PROCEDURE, accessProcedureBody);

                if (accessProcedureBody.ToLower().StartsWith("parameters") && accessProcedureBody.Contains(";"))
                {
                    int firstpos = "parameters ".Length;
                    int lastpos = accessProcedureBody.IndexOf(";");

                    foreach (string par in accessProcedureBody.Substring(firstpos, lastpos - firstpos).Split(',').Select(x => x.Trim()))
                    {
                        if (par.Contains("]"))
                        {
                            string[] nametype = par.Split(']').ToArray();
                            if (nametype.Length == 2)
                                procedure.InputParams.Add(new Variable(nametype[0] + "]", nametype[1].Trim()));
                        }
                        else
                        {
                            string[] nametype = par.Split(' ').Where(x => x != "").ToArray();
                            if (nametype.Length == 2)
                                procedure.InputParams.Add(new Variable("[" + nametype[0] + "]", nametype[1]));
                        }
                    }
                }

                this.procedures.Add(procedure);
            }
        }

        /// <summary>
        /// Импорт информации о таблицах из базы данных MS Access
        /// </summary>
        /// <param name="accessTables">Список таблиц из базы данных. Не может быть Null.</param>
        /// <param name="databaseConnection">Соединение с базой данных</param>
        /// <param name="counter">Счётчик для подсчёта прогресса</param>
        private void MsAccess_Import_Tables(DataTable accessTables, OleDbConnection databaseConnection, ref int counter)
        {
            const string reportName = "Список таблиц";

            StringBuilder tables_reportText = new StringBuilder();

            foreach (DataRow accessTable in accessTables.Rows)
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.ACCESS_ANALYSIS.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)++counter);

                string accessTableFullName = accessTable["TABLE_NAME"].ToString();
                string accessTableName = this.GetObjectName(accessTable["TABLE_NAME"]);

                tables_reportText.AppendLine();
                tables_reportText.AppendLine(accessTableFullName);

                Table table = new Table(accessTableFullName, accessTableName);

                OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter(@"SELECT * FROM [" + accessTableFullName + "]", databaseConnection);

                DataTable dataTable = new DataTable();

                oleDbDataAdapter.Fill(dataTable);

                foreach (DataColumn column in dataTable.Columns)
                {
                    string type = column.DataType.ToString();

                    if (type.Contains("System."))
                        type = type.Substring("System.".Length);

                    table.TableColumns.Add(new Variable(column.ToString(), type));

                    tables_reportText.AppendLine("    " + column.ToString() + " " + type);
                }

                tables.Add(table);
            }

            string reportDirectoryPath = Path.Combine(this.dataDirectoryPath, reportName + ".txt");
            File.WriteAllText(reportDirectoryPath, tables_reportText.ToString());
        }
        #endregion

        /// <summary>
        /// Получаем имя объекта
        /// </summary>
        /// <param name="sqlObject">Объект</param>
        /// <returns>Имя</returns>
        private string GetObjectName(Object sqlObject)
        {
            if (sqlObject == null)
                return null;

            string ret = sqlObject.ToString();

            if (ret.Contains("."))
                ret = ret.Split('.')[1];

            return ret.Trim(new char[] { '[', ']' });
        }

        /// <summary>
        /// Получаем наименование схемы объекта
        /// </summary>
        /// <param name="sqlObject">Объект</param>
        /// <returns>Схема</returns>
        private string GetObjectSchema(Object sqlObject)
        {
            if (sqlObject == null)
                return null;

            string ret = sqlObject.ToString();

            return ret.Contains(".") ? ret.Split('.')[0].Trim(new char[] { '[', ']' }) : "not_present";
        }

        /// <summary>
        /// Генерация файла скрипта
        /// </summary>
        /// <param name="scriptDirectoryPath">Путь до директории, куда следует поместить файл скрипта. Не может быть Null.</param>
        /// <param name="scriptFileName">Имя файла скрипта</param>
        /// <param name="scriptText">Текст скрипта</param>
        private void GenerateScriptFile(string scriptDirectoryPath, string scriptFileName, string scriptText)
        {
            if (!Directory.Exists(scriptDirectoryPath))
                Directory.CreateDirectory(scriptDirectoryPath);

            File.WriteAllText(Path.Combine(scriptDirectoryPath, (scriptFileName ?? "UnknownScript") + ".sql"), scriptText ?? String.Empty);
        }
        #endregion

        #region Генерация отчётов
        /// <summary>
        /// Запускаем генерацию отчтов по анализу базы данных MS SQL
        /// </summary>
        /// <param name="reportsDirectoryPath">Директория для формирования отчётов</param>
        internal void MsSql_GenerateReports(string reportsDirectoryPath)
        {
            //Отчет "Список процедур"
            this.MsSql_GenerateReport_Procedures(reportsDirectoryPath);

            //Формируем основные отчёты
            this.GenerateReports(reportsDirectoryPath);
        }

        /// <summary>
        /// Запускаем генерацию отчтов по анализу базы данных MS Access
        /// </summary>
        /// <param name="reportsDirectoryPath">Директория для формирования отчётов</param>
        internal void MsAccess_GenerateReports(string reportsDirectoryPath)
        {
            //Отчет "Список процедур"
            this.MsAccess_GenerateReport_Procedures(reportsDirectoryPath);

            //Формируем основные отчёты
            this.GenerateReports(reportsDirectoryPath);
        }

        /// <summary>
        /// Запуск генерации отчётов по накопленной информации
        /// </summary>
        /// <param name="reportsDirectoryPath">Директория для формирования отчётов</param>
        private void GenerateReports(string reportsDirectoryPath)
        {
            //Информационные объекты
            this.GenerateReport_ProceduresObjects(reportsDirectoryPath);

            //Связи по информации
            this.GenerateReport_ProceduresInfoDependencies(reportsDirectoryPath);

            //Связи по управлению
            this.GenerateReport_ProceduresControlDependencies(reportsDirectoryPath);
        }

        /// <summary>
        /// Генерация отчётов по списку процедур в рамках базы данных MS Sql
        /// </summary>
        /// <param name="reportsDirectoryPath"></param>
        private void MsSql_GenerateReport_Procedures(string reportsDirectoryPath)
        {
            const string reportName = "Список процедур";

            StringBuilder reportText = new StringBuilder();

            foreach (Procedure dbp in this.procedures)
            {
                if (dbp.Type == AbstractProcedure.ProcedureType.TRIGGER)
                {
                    Trigger dbt = dbp as Trigger;
                    reportText.AppendLine();
                    reportText.AppendLine(dbt.Type.Description() + "\t" + dbt.TableName + ".[" + dbt.Name + "]");
                }
                else
                {
                    reportText.AppendLine();
                    reportText.AppendLine(dbp.Type.Description() + "\t" + dbp.Name);
                }

                if (dbp.Type.Description().Contains("Триггер"))
                    reportText.AppendLine("\t" + "События: ");
                else
                    reportText.AppendLine("\t" + "Параметры: ");

                foreach (Variable v in dbp.InputParams)
                {
                    reportText.AppendLine("\t\t" + v.Name + "\t" + v.Type);
                }
            }

            string reportPath = Path.Combine(reportsDirectoryPath, reportName + ".txt");
            File.WriteAllText(reportPath, reportText.ToString());
        }
        
        /// <summary>
        /// Генерация отчётов по списку процедур в рамках базы данных MS Access
        /// </summary>
        /// <param name="reportsDirectoryPath"></param>
        private void MsAccess_GenerateReport_Procedures(string reportsDirectoryPath)
        {
            const string reportName = "Список процедур";

            StringBuilder reportText = new StringBuilder();

            foreach (Procedure procedure in this.procedures)
            {
                reportText.AppendLine();
                reportText.AppendLine(procedure.Type.Description() + "\t" + procedure.Name);
                reportText.AppendLine("\t" + "Параметры: ");

                foreach (Variable inputParam in procedure.InputParams)
                    reportText.AppendLine("\t\t" + inputParam.Name + "\t" + inputParam.Type);
            }

            string reportPath = Path.Combine(reportsDirectoryPath, reportName + ".txt");
            File.WriteAllText(reportPath, reportText.ToString());
        }

        /// <summary>
        /// Формирование отчёта по связям функциональных объектов по управлению
        /// </summary>
        /// <param name="reportsDirectoryPath">Директория для формирования отчётов</param>
        private void GenerateReport_ProceduresControlDependencies(string reportsDirectoryPath)
        {
            const string reportName = "Связи функциональных объектов по управлению";

            StringBuilder reportText = new StringBuilder();

            reportText.Append(reportName + Environment.NewLine + Environment.NewLine);

            //Проходим по всем вызовам процедур из других процедур
            foreach (var procedure in this.procedures.Where(p => p.CallingList.Count > 0))
            {
                switch (procedure.Type)
                {
                    case AbstractProcedure.ProcedureType.TRIGGER:
                        reportText.Append(Environment.NewLine + "Вызывающий триггер " + procedure.FullName + Environment.NewLine + Environment.NewLine);
                        break;
                    case AbstractProcedure.ProcedureType.FUNCTION:
                        reportText.Append(Environment.NewLine + "Вызывающая функция " + procedure.FullName + Environment.NewLine + Environment.NewLine);
                        break;
                    default:
                        reportText.Append(Environment.NewLine + "Вызывающая процедура " + procedure.FullName + Environment.NewLine + Environment.NewLine);
                        break;
                }

                foreach (var callingProcedure in procedure.CallingList)
                {
                    switch (callingProcedure.Type)
                    {
                        case AbstractProcedure.ProcedureType.TRIGGER:
                            reportText.Append("\t" + "Вызываемый триггер " + callingProcedure.FullName + Environment.NewLine);
                            break;
                        case AbstractProcedure.ProcedureType.FUNCTION:
                            reportText.Append("\t" + "Вызываемая функция " + callingProcedure.FullName + Environment.NewLine);
                            break;
                        default:
                            reportText.Append("\t" + "Вызываемая процедура " + callingProcedure.FullName + Environment.NewLine);
                            break;
                    }

                    reportText.Append("\t\t" + callingProcedure.LineOrPosition + "\t" + "Параметры вызова: " + callingProcedure.Params + Environment.NewLine);
                }
                reportText.Append(Environment.NewLine);
            }

            string reportPath = Path.Combine(reportsDirectoryPath, reportName + ".txt");
            File.WriteAllText(reportPath, reportText.ToString());
        }

        /// <summary>
        /// Формирование отчёта по информационным объектам процедур
        /// </summary>
        /// <param name="reportsDirectoryPath">Директория для формирования отчётов</param>
        private void GenerateReport_ProceduresObjects(string reportsDirectoryPath)
        {
            const string reportName = "Информационные объекты процедур";

            StringBuilder reportText = new StringBuilder();

            reportText.Append(reportName + Environment.NewLine + Environment.NewLine);

            foreach (var procedure in this.procedures)
            {
                switch (procedure.Type)
                {
                    case AbstractProcedure.ProcedureType.TRIGGER:
                        reportText.Append("Триггер " + procedure.FullName + Environment.NewLine);
                        break;
                    case AbstractProcedure.ProcedureType.FUNCTION:
                        reportText.Append("Функция " + procedure.FullName + Environment.NewLine);
                        break;
                    default:
                        reportText.Append("Процедура " + procedure.FullName + Environment.NewLine);
                        break;
                }

                reportText.Append("\t" + "Информационные объекты" + Environment.NewLine);


                if (procedure.Type == AbstractProcedure.ProcedureType.TRIGGER)
                    reportText.Append("\t\t" + "События:" + Environment.NewLine);
                else
                    reportText.Append("\t\t" + "Параметры:" + Environment.NewLine);

                foreach (var t in procedure.InputParams)
                    reportText.Append("\t\t\t" + t.Name + "\t" + t.Type + Environment.NewLine);

                reportText.Append("\t\t" + "Таблицы:" + Environment.NewLine);

                foreach (var t in procedure.Tables)
                    reportText.Append("\t\t\t" + t.Name + Environment.NewLine);

                reportText.Append("\t\t" + "Глобальные переменные:" + Environment.NewLine);

                foreach (var t in procedure.GlobalParams)
                    reportText.Append("\t\t\t" + t.Name + "\t" + t.Type + Environment.NewLine);

                reportText.Append("\t\t" + "Локальные переменные:" + Environment.NewLine);

                foreach (var t in procedure.LocalParams)
                    reportText.Append("\t\t\t" + t.Name + "\t" + t.Type + Environment.NewLine);

                reportText.Append(Environment.NewLine);
            }

            string reportPath = Path.Combine(reportsDirectoryPath, reportName + ".txt");
            File.WriteAllText(reportPath, reportText.ToString());
        }

        /// <summary>
        /// Формирование отчёта по связям функциональных объектов по информации
        /// </summary>
        /// <param name="reportsDirectoryPath">Директория для формирования отчётов</param>
        private void GenerateReport_ProceduresInfoDependencies(string reportsDirectoryPath)
        {
            const string reportName = "Связи функциональных объектов по информации";

            StringBuilder reportText = new StringBuilder();

            reportText.Append(reportName + Environment.NewLine + Environment.NewLine);

            foreach (var procedure in this.procedures)
            {
                switch (procedure.Type)
                {
                    case AbstractProcedure.ProcedureType.TRIGGER:
                        reportText.Append("Триггер " + procedure.FullName + Environment.NewLine);
                        break;
                    case AbstractProcedure.ProcedureType.FUNCTION:
                        reportText.Append("Функция " + procedure.FullName + Environment.NewLine);
                        break;
                    default:
                        reportText.Append("Процедура " + procedure.FullName + Environment.NewLine);
                        break;
                }

                Dictionary<string, List<string>> dv = new Dictionary<string, List<string>>();
                Dictionary<string, List<string>> dp = new Dictionary<string, List<string>>();
                Dictionary<string, List<string>> dt = new Dictionary<string, List<string>>();
                StringBuilder repName = new StringBuilder();

                foreach (var callingProcedure in procedure.CallingList)
                {
                    foreach (var localParam in procedure.LocalParams.Where(p => callingProcedure.Params.Contains(p.Name)))
                        this.AddProcedureParamToDictionary(dp, callingProcedure, localParam);

                    foreach (var inputParam in procedure.InputParams.Where(p => callingProcedure.Params.Contains(p.Name)))
                        this.AddProcedureParamToDictionary(dp, callingProcedure, inputParam);
                }

                foreach (var procedure2 in this.procedures.Where(p2 => p2 != procedure))
                    foreach (var procedure2_globalParam in procedure2.GlobalParams.Where(p2 => procedure.GlobalParams.Any(p1 => p1.Name == p2.Name)))
                        this.AddProcedureParamToDictionary(dp, procedure2, procedure2_globalParam);

                foreach (var procedure2 in this.procedures.Where(p2 => p2 != procedure))
                    foreach (var procedure2_table in procedure2.Tables.Where(t2 => procedure.Tables.Any(t1 => t1.Name == t2.Name)))
                        this.AddProcedureParamToDictionary(dt, procedure2, procedure2_table);

                if (dp.Count == 0)
                {
                    reportText.Append("\tСвязей по переменным нет." + Environment.NewLine);
                }
                else
                {
                    reportText.Append("\tСвязи по переменным и параметрам для:" + Environment.NewLine);
                    foreach (string s in dp.Keys)
                    {
                        reportText.Append("\t\t" + s + Environment.NewLine);

                        foreach (string p in dp[s])
                            reportText.Append("\t\t\t" + p + Environment.NewLine);
                    }
                }

                if (dt.Count == 0)
                {
                    reportText.Append("\tСвязей по таблицам нет." + Environment.NewLine);
                }
                else
                {
                    reportText.Append("\tСвязи по таблицам БД для:" + Environment.NewLine);
                    foreach (string s in dt.Keys)
                    {
                        reportText.Append("\t\t" + s + Environment.NewLine);

                        foreach (string p in dt[s])
                            reportText.Append("\t\t\t" + p + Environment.NewLine);
                    }
                }

                reportText.Append(Environment.NewLine);
            }

            string reportPath = Path.Combine(reportsDirectoryPath, reportName + ".txt");
            File.WriteAllText(reportPath, reportText.ToString());
        } 
        #endregion

        /// <summary>
        /// Полезный метод. Используется для формирования отчётов.
        /// </summary>
        /// <param name="dictionary">Словарь</param>
        /// <param name="procedure">Процедура</param>
        /// <param name="param">Параметр</param>
        private void AddProcedureParamToDictionary(Dictionary<string, List<string>> dictionary, AbstractProcedure procedure, Variable param)
        {
            StringBuilder ret = new StringBuilder();

            switch (procedure.Type)
            {
                case AbstractProcedure.ProcedureType.TRIGGER:
                    ret.Append("Триггера  " + procedure.FullName);
                    break;
                case AbstractProcedure.ProcedureType.FUNCTION:
                    ret.Append("Функции   " + procedure.FullName);
                    break;
                default:
                    ret.Append("Процедуры " + procedure.FullName);
                    break;
            }

            string key = ret.ToString();

            if (!dictionary.Keys.Contains(key))
                dictionary.Add(key, new List<string>());

            if (!dictionary[key].Contains(param.Name))
                dictionary[key].Add(param.Name);
        }
    }
}
