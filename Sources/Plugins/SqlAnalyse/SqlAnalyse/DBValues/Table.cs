﻿using System.Collections.Generic;

namespace IA.Plugins.Analyses.SQLAnalyse.DBValues
{
    /// <summary>
    /// Таблица
    /// </summary>
    internal class Table
    {
        /// <summary>
        /// Имя таблицы
        /// </summary>
        internal string Name { get; private set; }

        /// <summary>
        /// Наименование схемы
        /// </summary>
        internal string TableScheme { get; private set; }

        /// <summary>
        /// Список колонок таблицы
        /// </summary>
        internal List<Variable> TableColumns { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя таблицы</param>
        /// <param name="tableScheme">Наименование схемы</param>
        internal Table(string name, string tableScheme)
        {
            this.Name = name;
            this.TableScheme = tableScheme;
            this.TableColumns = new List<Variable>();
        }
    }
}
