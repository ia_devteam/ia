﻿namespace IA.Plugins.Analyses.SQLAnalyse.DBValues
{
    /// <summary>
    /// Переменная
    /// </summary>
    internal class Variable
    {
        /// <summary>
        /// Имя переменной
        /// </summary>
        internal string Name { get; private set; }

        /// <summary>
        /// Тип переменной
        /// </summary>
        internal string Type { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя переменной</param>
        /// <param name="type">Тип переменной</param>
        internal Variable(string name, string type)
        {
            this.Name = name;
            this.Type = type;
        }
    }
}
