﻿using System;
using System.Collections.Generic;

namespace IA.Plugins.Analyses.SQLAnalyse.DBValues.Procedures
{
    /// <summary>
    /// Триггер
    /// </summary>
    internal class Trigger : Procedure
    {
        /// <summary>
        /// Имя таблицы. Не может быть Null.
        /// </summary>
        internal string TableName { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя триггера. Не может быть Null.</param>
        /// <param name="type">Тип триггера</param>
        /// <param name="script">Исходный код триггера. Не может быть Null.</param>
        /// <param name="tableName">Имя таблицыю. Не может быть Null.</param>
        internal Trigger(string name, AbstractProcedure.ProcedureType type, string script, string tableName)
            : base(name, type, script, tableName + ".[" + name + "]")
        {
            if (tableName == null)
                throw new ArgumentNullException("tableName", "Имя таблицы триггера не определено.");

            this.TableName = tableName;
        }

        /// <summary>
        /// Формирование списка используемых таблиц
        /// </summary>
        /// <param name="tables">Список таблиц</param>
        internal override void GenerateTables(List<Table> tables)
        {
            ProcedureScript script = new ProcedureScript(this.Script.RemoveCommentsAndHeader());

            foreach (var table in tables)
            {
                if (this.TableName == table.Name)
                    continue;

                int startIndex = 0;
                int currentIndex = 0;
                string tableName = script.GetNextProcedureName(table.TableScheme, startIndex, out currentIndex);

                if (currentIndex >= 0)
                    this.Tables.Add(new Variable(table.Name, tableName));
            }
        } 
    }
}
