﻿
using System;

namespace IA.Plugins.Analyses.SQLAnalyse.DBValues.Procedures
{
    /// <summary>
    /// Вызываемая процедура
    /// </summary>
    internal class CallingProcedure : AbstractProcedure
    {
        /// <summary>
        /// Параметры вызываемой процедуры. Не может быть Null.
        /// </summary>
        internal string Params { get; private set; }

        /// <summary>
        /// Строка или позиция вызываемой процедуры в вызываемой программе
        /// </summary>
        internal int LineOrPosition { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="fullName">Полное имя вызываемой процедуры. Не может быть Null.</param>
        /// <param name="params">Параметры вызываемой процедуры. Не может быть Null.</param>
        /// <param name="type">Тип вызываемой процедуры</param>
        /// <param name="lineOrPosition">Полное имя процедуры. Если Null - используется значение переданное в первом параметре.</param>
        internal CallingProcedure(string fullName, string @params, AbstractProcedure.ProcedureType type, int lineOrPosition)
            : base(fullName, type, fullName)
        {
            if (fullName == null)
                throw new ArgumentNullException("fullName", "Полное имя вызываемой процедуры не определено.");

            if (@params == null)
                throw new ArgumentNullException("params", "Параметры вызываемой процедуры не определены.");

            this.Params = @params;
            this.LineOrPosition = lineOrPosition;
        }
    }
}
