﻿using System;
using System.Linq;

namespace IA.Plugins.Analyses.SQLAnalyse.DBValues.Procedures
{
    /// <summary>
    /// Абстрактная процедура с исходным кодом
    /// </summary>
    class AbstractProcedureWithScript : AbstractProcedure
    {
        /// <summary>
        /// Класс, обеспечивающий работу со скриптами процедур
        /// </summary>
        internal class ProcedureScript
        {
            /// <summary>
            /// Скрипт процедуры
            /// </summary>
            private readonly string script = null;

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="script">Скрипт процедуры</param>
            internal ProcedureScript(string script)
            {
                //Запоминмаем сам скрипт
                this.script = script;
            }

            /// <summary>
            /// Удаление кмментариев из t-sql скрипта
            /// </summary>
            private string RemoveComments()
            {
                string Ret = "";
                int start = 0;
                int end = -1;
                int tmp = 0;
                while (start != -1)
                {
                    end = this.script.IndexOf("/*", start);
                    if (end != -1)
                    {
                        Ret += this.script.Substring(start, end - start);
                        start = end + 2;
                        end = this.script.IndexOf("*/", start);
                        if (end == -1)
                        {
                            // Ошибка t-sql
                            break;
                        }
                        tmp = start - 2;
                        while (true)
                        {
                            int tmp0 = tmp + 2;
                            tmp = this.script.Substring(tmp0, end - tmp0).IndexOf("/*");
                            if (tmp != -1)
                            {
                                tmp += tmp0;
                                end = this.script.IndexOf("*/", end + 2);
                                if (end == -1)
                                {
                                    // Ошибка t-sql
                                    break;
                                }
                            }
                            else
                                break;
                        }
                        start = end + 2;
                    }
                    else
                    {
                        Ret += this.script.Substring(start);
                        break;
                    }
                }
                // --
                string nocomm = "";
                start = 0;
                end = -1;
                while (true)
                {
                    end = Ret.IndexOf("--", start);
                    if (end != -1)
                    {
                        nocomm += Ret.Substring(start, end - start);
                        start = end + 2;
                        end = Ret.IndexOf("\n", start);
                        if (end == -1)
                        {
                            // Ошибка t-sql
                            break;
                        }
                        start = end;
                    }
                    else
                    {
                        nocomm += Ret.Substring(start);
                        break;
                    }
                }

                return nocomm;
            }

            /// <summary>
            /// Удаление кмментариев и заголовка из t-sql скрипта
            /// </summary>
            /// <returns></returns>
            internal string RemoveCommentsAndHeader()
            {
                //Удаляем комментарий
                string ret = this.RemoveComments();

                int start = ret.IndexOf("create", 0, StringComparison.InvariantCultureIgnoreCase);

                if (start <= 0)
                    return ret;

                start = ret.IndexOf("as", start, StringComparison.InvariantCultureIgnoreCase);

                if (start <= 0)
                    return ret;

                start = ret.IndexOf("begin", start, StringComparison.InvariantCultureIgnoreCase);

                if (start <= 0)
                    return ret;

                return this.script.Substring(start);
            }

            /// <summary>
            /// Поиск имени процедуры (функции)
            /// </summary>
            /// <param name="name">Шаблон поиска</param>
            /// <param name="startIndex">Индекс начала поиска</param>
            /// <param name="resultIndex">Индекс конца поиска. -1, если ничего не нашли.</param>
            /// <returns>Искомое имя процедуры.</returns>
            internal string GetNextProcedureName(string name, int startIndex, out int resultIndex)
            {
                resultIndex = this.script.IndexOf(name, startIndex, StringComparison.OrdinalIgnoreCase);

                if (resultIndex < 0)
                    return String.Empty;

                while (resultIndex >= 0)
                {
                    //Проверяем что до слова идёт разделитель
                    if (resultIndex > 0)
                    {
                        char c = script[resultIndex - 1];

                        if (IsNotDelim(c))
                        {
                            //Если нет, ищем дальше
                            resultIndex = this.script.IndexOf(name, resultIndex + 1, StringComparison.OrdinalIgnoreCase);
                            continue;
                        }
                    }

                    //Проверяем, что после слова идёт разделитель
                    if (resultIndex + name.Length < this.script.Length - 1)
                    {
                        char c = this.script[resultIndex + name.Length];

                        if (IsNotDelim(c))
                        {
                            //Если нет, ищем дальше
                            resultIndex = this.script.IndexOf(name, resultIndex + 1, StringComparison.OrdinalIgnoreCase);
                            continue;
                        }
                    }

                    return name;
                }

                resultIndex = -1;

                return String.Empty;
            }

            /// <summary>
            /// Поиск параметров следующих за именем процедуры
            /// </summary>
            /// <param name="startIndex">Индекс начала поиска</param>
            /// <param name="resultIndex">Индекс конца поиска</param>
            /// <returns>Параметры процедуры</returns>
            internal string GetNextProcedureParams(int startIndex, out int resultIndex)
            {
                int level = 0;
                int retStartIndex = 0;
                int retEndIndex = 0;

                for (int currentIndex = startIndex; currentIndex < this.script.Length; currentIndex++)
                {
                    char c = this.script[currentIndex];

                    if (c == '(')
                    {
                        if (level == 0)
                            retStartIndex = currentIndex;

                        level++;
                    }

                    if (c == ')')
                    {
                        level--;

                        if (level == 0)
                        {
                            resultIndex = retEndIndex = currentIndex;

                            return this.script.Substring(retStartIndex + 1, retEndIndex - retStartIndex - 1);
                        }
                    }
                }

                resultIndex = -1;

                return String.Empty;
            }

            /// <summary>
            /// Поиск имени следующей переменной
            /// </summary>
            /// <param name="startIndex">Индекс начала поиска</param>
            /// <param name="resultIndex">Индекс конца поиска</param>
            /// <param name="declare">Cтрока "declare" или ","</param>
            /// <returns>Искомое имя переменной</returns>
            internal string GetNextVariableName(int startIndex, out int resultIndex, string declare)
            {
                resultIndex = this.script.IndexOf(declare, startIndex, StringComparison.OrdinalIgnoreCase);

                if (resultIndex < 0)
                    return String.Empty;

                if (declare == ",")
                {
                    string delem = this.script.Substring(startIndex, resultIndex - startIndex).Trim(new char[] { ' ', '\t', '\r', '\n' });

                    if (delem != String.Empty)
                    {
                        resultIndex = -1;
                        return String.Empty;
                    }
                    else
                    {
                        startIndex = resultIndex;
                        return GetNextLocalVariableName(startIndex, out resultIndex);
                    }

                }
                else
                {
                    while (resultIndex >= 0)
                    {
                        if (resultIndex > 0)
                        {
                            char c = this.script[resultIndex - 1];

                            if (IsNotDelim(c))
                            {
                                resultIndex = this.script.IndexOf(declare, resultIndex + 1, StringComparison.OrdinalIgnoreCase);
                                continue;
                            }
                        }

                        if (resultIndex + declare.Length < this.script.Length - 1)
                        {
                            int end = resultIndex + declare.Length;
                            char lc = this.script[end];
                            if (IsNotDelim(lc))
                            {
                                resultIndex = this.script.IndexOf(declare, resultIndex + 1, StringComparison.OrdinalIgnoreCase);
                                continue;
                            }
                        }

                        startIndex = resultIndex + declare.Length;

                        return GetNextLocalVariableName(startIndex, out resultIndex);
                    }

                    return String.Empty;
                }
            }

            /// <summary>
            /// Поиск следующей локальной переменной
            /// </summary>
            /// <param name="startIndex">Индекс начала поиска</param>
            /// <param name="resultIndex">Индекс конца поиска</param>
            /// <returns>Искомое имя локальной переменной</returns>
            private string GetNextLocalVariableName(int startIndex, out int resultIndex)
            {
                int ptr1 = startIndex;

                while (IsNotDelim(this.script[ptr1], new char[] { '@' }))
                    ptr1++;

                while (!IsNotDelim(this.script[ptr1], new char[] { '@' }))
                    ptr1++;

                if (this.script[ptr1] != '@')
                {
                    ptr1--;
                    return GetNextWord(ptr1, out resultIndex);
                }

                resultIndex = this.script.IndexOf("@", startIndex, StringComparison.OrdinalIgnoreCase);

                if (resultIndex < 0)
                    return String.Empty;

                startIndex = resultIndex;

                resultIndex++;

                while (resultIndex < this.script.Length)
                {
                    char c = this.script[resultIndex];

                    if (!IsNotDelim(c))
                        break;

                    resultIndex++;
                }

                return this.script.Substring(startIndex, resultIndex - startIndex);
            }

            /// <summary>
            /// Поиск типа переменной
            /// </summary>
            /// <param name="startIndex">Индекс начала поиска</param>
            /// <param name="resultIndex">Индекс конца поиска</param>
            /// <returns>Искомый тип переменной</returns>
            internal string GetNextVariableType(int startIndex, out int resultIndex)
            {
                resultIndex = startIndex;

                char c = this.script[resultIndex];

                while (resultIndex < this.script.Length)
                {
                    c = this.script[resultIndex];
                    if (IsNotDelim(c))
                        break;

                    resultIndex++;
                }

                startIndex = resultIndex;

                while (resultIndex < this.script.Length)
                {
                    c = this.script[resultIndex];

                    if (!IsNotDelim(c, new char[] { '(', ')' }))
                        break;

                    resultIndex++;
                }

                string ret = this.script.Substring(startIndex, resultIndex - startIndex);

                startIndex = resultIndex;

                if (ret.ToLower() == "as")
                    ret = GetNextVariableType(startIndex, out resultIndex);

                if (resultIndex == this.script.Length)
                    resultIndex = -1;

                return ret;
            }

            /// <summary>
            /// Поиск следующей глобальной переменной
            /// </summary>
            /// <param name="startIndex">Индекс начала поиска</param>
            /// <param name="resultIndex">Индекс конца поиска</param>
            /// <returns>Найденное имя глобальной переменной</returns>
            internal string GetNextGlobalVariableName(int startIndex, out int resultIndex)
            {
                int start = 0, end = 0;

                resultIndex = this.script.IndexOf("@@", startIndex, StringComparison.OrdinalIgnoreCase);

                if (resultIndex < 0)
                    return String.Empty;

                while (resultIndex >= 0)
                {
                    if (resultIndex > 0)
                    {
                        char c = this.script[resultIndex - 1];

                        if (IsNotDelim(c))
                        {
                            resultIndex = this.script.IndexOf("@@", resultIndex + 1);
                            continue;
                        }
                    }

                    if (resultIndex + "@@".Length < this.script.Length - 1)
                    {
                        start = resultIndex;
                        end = resultIndex + "@@".Length;
                        char c = this.script[end];

                        while (IsNotDelim(c))
                        {
                            end++;
                            c = this.script[end];
                        }

                        resultIndex = end;
                    }

                    return this.script.Substring(start, end - start);
                }

                return String.Empty;
            }

            /// <summary>
            /// Следующее слово
            /// </summary>
            /// <param name="startIndex">Индекс начала поиска</param>
            /// <param name="resultIndex">Индекс конца поиска</param>
            /// <returns>Найденное слово</returns>
            private string GetNextWord(int startIndex, out int resultIndex)
            {
                resultIndex = startIndex;

                char c = this.script[resultIndex];

                while (resultIndex < this.script.Length)
                {
                    c = this.script[resultIndex];

                    if (IsNotDelim(c))
                        break;

                    resultIndex++;
                }

                int wordStartIndex = resultIndex;

                while (resultIndex < this.script.Length)
                {
                    c = this.script[resultIndex];

                    if (!IsNotDelim(c))
                        break;

                    resultIndex++;
                }

                if (resultIndex == this.script.Length)
                    resultIndex = -1;

                return this.script.Substring(wordStartIndex, resultIndex - wordStartIndex);
            }

            /// <summary>
            /// Символ - не разделитель?
            /// </summary>
            /// <param name="lc">Символ</param>
            /// <returns></returns>
            private bool IsNotDelim(char lc)
            {
                return
                    lc >= 'A' && lc <= 'Z' ||
                        lc >= 'a' && lc <= 'z' ||
                            lc == '_' ||
                                lc >= '0' && lc <= '9' ||
                                    lc >= 'А' && lc <= 'Я' ||
                                        lc >= 'а' && lc <= 'я' ||
                                            lc == '$';
            }

            /// <summary>
            /// Символ - не разделитель?
            /// </summary>
            /// <param name="c">Cимвол</param>
            /// <param name="notDelimChars">Дополнительный массив символов - не разделителей</param>
            /// <returns></returns>
            private bool IsNotDelim(char c, char[] notDelimChars)
            {
                return IsNotDelim(c) || notDelimChars.Contains(c);
            }
        }

        /// <summary>
        /// Исходный код процедуры. Не может быть Null.
        /// </summary>
        internal ProcedureScript Script { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя процедуры. Не может быть Null.</param>
        /// <param name="type">Тип процедуры</param>
        /// <param name="script">Исходный код процедуры. Не может быть Null.</param>
        /// <param name="fullName">Полное имя процедуры. Если Null - используется значение переданное в первом параметре.</param>
        internal AbstractProcedureWithScript(string name, ProcedureType type, string script, string fullName = null)
            : base(name, type, fullName)
        {
            if (script == null)
                throw new ArgumentNullException("script", "Исходный код процедуры не определён.");

            this.Script = new ProcedureScript(script);
        }
    }
}
