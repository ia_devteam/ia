﻿using System;
using System.ComponentModel;

namespace IA.Plugins.Analyses.SQLAnalyse.DBValues.Procedures
{
    /// <summary>
    /// Абстрактная процедура
    /// </summary>
    internal abstract class AbstractProcedure
    {
        /// <summary>
        /// Тип процедуры
        /// </summary>
        internal enum ProcedureType
        {
            /// <summary>
            /// Хранимая процедура
            /// </summary>
            [Description("Хранимая процедура")]
            PROCEDURE,
            /// <summary>
            /// Функция
            /// </summary>
            [Description("Функция")]
            FUNCTION,
            /// <summary>
            /// Триггер
            /// </summary>
            [Description("Триггер")]
            TRIGGER,
            /// <summary>
            /// DDL Триггер
            /// </summary>
            [Description("DDL Триггер")]
            DDL_TRIGGER
        }

        /// <summary>
        /// Имя процедуры. Не может быть Null.
        /// </summary>
        internal string Name { get; private set; }

        /// <summary>
        /// Тип процедуры
        /// </summary>
        internal ProcedureType Type { get; private set; }

        /// <summary>
        /// Полное имя процедуры. Не может быть Null.
        /// </summary>
        internal string FullName { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя процедуры. Не может быть Null</param>
        /// <param name="type">Тип процедуры</param>
        /// <param name="fullName">Полное имя процедуры. Если Null - используется значение переданное в первом параметре.</param>
        internal AbstractProcedure(string name, ProcedureType type, string fullName = null)
        {
            if (name == null)
                throw new ArgumentNullException("name", "Имя процедуры не определено.");

            this.Name = name;
            this.Type = type;
            this.FullName = fullName ?? name;
        }
    }
}
