﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace IA.Plugins.Analyses.SQLAnalyse.DBValues.Procedures
{
    /// <summary>
    /// Процедура
    /// </summary>
    internal class Procedure : AbstractProcedureWithScript
    {
        /// <summary>
        /// Список входных параметров. Не может быть Null.
        /// </summary>
        internal List<Variable> InputParams { get; private set; }

        /// <summary>
        /// Список локальных параметров. Не может быть Null.
        /// </summary>
        internal List<Variable> LocalParams { get; private set; }

        /// <summary>
        /// Список глобальных параметров. Не может быть Null.
        /// </summary>
        internal List<Variable> GlobalParams { get; private set; }

        /// <summary>
        /// Список таблиц. Не может быть Null.
        /// </summary>
        internal List<Variable> Tables { get; private set; }

        /// <summary>
        /// Список вызываемых функций и процедур. Не может быть Null.
        /// </summary>
        internal List<CallingProcedure> CallingList { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя процедуры. Не может быть Null.</param>
        /// <param name="type">Тип процедуры</param>
        /// <param name="script">Исходный код процедуры. Не может быть Null.</param>
        /// <param name="fullName">Полное имя процедуры. Если Null - используется значение переданное в первом параметре.</param>
        internal Procedure(string name, ProcedureType type, string script, string fullName = null)
            : base(name, type, script, fullName)
        {
            this.InputParams = new List<Variable>();
            this.LocalParams = new List<Variable>();
            this.GlobalParams = new List<Variable>();
            this.Tables = new List<Variable>();
            this.CallingList = new List<CallingProcedure>();
        }

        /// <summary>
        /// Формирование списка локальных переменных
        /// </summary>
        internal void GenerateLocalVariables()
        {
            int startIndex = 0;
            int resultIndex = 0;
            string declare = "declare";

            ProcedureScript script = new ProcedureScript(this.Script.RemoveCommentsAndHeader());

            while (startIndex >= 0)
            {
                string variableName = script.GetNextVariableName(startIndex, out resultIndex, declare);

                declare = ",";

                if (resultIndex >= 0)
                {
                    startIndex = resultIndex;

                    string variableType = script.GetNextVariableType(startIndex, out resultIndex);

                    startIndex = resultIndex;

                    if (resultIndex >= 0)
                    {
                        startIndex = resultIndex;
                        this.LocalParams.Add(new Variable(variableName, variableType));
                    }
                }
                else
                {
                    declare = "declare";

                    variableName = script.GetNextVariableName(startIndex, out resultIndex, declare);

                    declare = ",";

                    startIndex = resultIndex;

                    if (resultIndex >= 0)
                    {
                        string variableType = script.GetNextVariableType(startIndex, out resultIndex);

                        startIndex = resultIndex;

                        if (resultIndex >= 0)
                        {
                            startIndex = resultIndex;
                            this.LocalParams.Add(new Variable(variableName, variableType));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Формирование списка глобальных переменных
        /// </summary>
        internal void GenerateGlobalVariables()
        {
            int startIndex = 0;
            int resultIndex = 0;

            ProcedureScript script = new ProcedureScript(this.Script.RemoveCommentsAndHeader());

            while (startIndex >= 0)
            {
                string variableName = script.GetNextGlobalVariableName(startIndex, out resultIndex);

                startIndex = resultIndex;

                if (resultIndex >= 0)
                {
                    startIndex = resultIndex;

                    if (resultIndex >= 0)
                        if (!this.GlobalParams.Select(a => a.Name).Contains(variableName))
                            this.GlobalParams.Add(new Variable(variableName, String.Empty));
                }
            }
        }

        /// <summary>
        /// Формирование списка используемых таблиц
        /// </summary>
        /// <param name="tables">Список таблиц</param>
        internal virtual void GenerateTables(List<Table> tables)
        {
            ProcedureScript script = new ProcedureScript(this.Script.RemoveCommentsAndHeader());

            foreach (var table in tables)
            {
                int startIndex = 0;
                int currentIndex = 0;
                string tableName = script.GetNextProcedureName(table.TableScheme, startIndex, out currentIndex);

                if (currentIndex >= 0)
                    this.Tables.Add(new Variable(table.Name, tableName));
            }
        }

        /// <summary>
        /// Формирование списка вызываемых процедур
        /// </summary>
        /// <param name="procedures">Список всевозможных процедур</param>
        internal void GenerateCallingList(List<Procedure> procedures)
        {
            ProcedureScript script = new ProcedureScript(this.Script.RemoveCommentsAndHeader());

            foreach (var procedure in procedures)
            {
                if (this == procedure)
                    continue;

                int startIndex = 0;
                int resultIndex = 0;

                while (startIndex >= 0)
                {
                    string procedure_name = script.GetNextProcedureName(procedure.Name, startIndex, out resultIndex);

                    startIndex = resultIndex;

                    if (resultIndex >= 0)
                    {
                        int start = resultIndex;

                        string @params = script.GetNextProcedureParams(startIndex, out resultIndex);

                        startIndex = resultIndex;

                        if (resultIndex >= 0)
                            this.CallingList.Add(new CallingProcedure(procedure.FullName, @params, procedure.Type, start));
                    }

                }

            }
        }
    }
}
