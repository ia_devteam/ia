﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;

using IA.Extensions;
using IA.Sql.Controls;

namespace IA.Plugins.Analyses.SQLAnalyse
{
    /// <summary>
    /// Класс, реализующий все настройки плагина
    /// </summary>
    internal partial class CustomSettings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Элемент управления - Настройки сервера проектов
        /// </summary>
        private SqlServerConnectionControl sqlServerConnectionControl = null;

        /// <summary>
        /// Конструктор формы
        /// </summary>
        internal CustomSettings()
        {
            InitializeComponent();

            //Инициализиреум элемент управления - Настроки сервера проектов
            this.sqlServerConnectionControl = new SqlServerConnectionControl()
            {
                Dock = DockStyle.Fill,
                ServerName = PluginSettings.Sql.ServerName,
                IsWindowsAuthentication = PluginSettings.Sql.IsWindowsAuthentification,
                ServerLogin = PluginSettings.Sql.Login,
                ServerPassword = PluginSettings.Sql.Password
            };
            sql_groupBox.Controls.Add(this.sqlServerConnectionControl);

            dataBase_comboBox.Text = PluginSettings.Sql.Database;
            access_textBoxWithDialogButton.Text = PluginSettings.Access.DatabaseFilePath;

            main_tabControl.SelectedTab = (PluginSettings.IsSQL) ? msSQL_tabPage : msAccess_tabPage;
        }

        /// <summary>
        /// Получение строки соединения для доступа к безе данных MS SQL 
        /// </summary>
        /// <returns></returns>
        private string GetSqlConnectionString()
        {
            SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder()
            {
                DataSource = sqlServerConnectionControl.ServerName,
                InitialCatalog = dataBase_comboBox.Text,
                IntegratedSecurity = sqlServerConnectionControl.IsWindowsAuthentication,
                UserID = sqlServerConnectionControl.IsWindowsAuthentication ? String.Empty : sqlServerConnectionControl.ServerLogin,
                Password = sqlServerConnectionControl.IsWindowsAuthentication ? String.Empty : sqlServerConnectionControl.ServerPassword,
                AsynchronousProcessing = true
            };

            return connectionBuilder.ConnectionString;
        }
                
        /// <summary>
        /// Проверка соединения с базой
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void testConnection_button_Click(object sender, EventArgs e)
        {
            try
            {
                //Проверка на то, что указано имя базы данных
                if (String.IsNullOrWhiteSpace(dataBase_comboBox.Text))
                    throw new Exception("Имя базы данных для соединения не указано.");

                //Пытаемся подключиться
                using (SqlConnection connection = new SqlConnection(GetSqlConnectionString()))
                    connection.Open();

                //Отображаем сообщение об успехе
                Monitor.Log.Information(PluginSettings.Name, "Проверка соединения с базой данных MS SQL завершилась успешно.", true);
            }
            catch (Exception ex)
            {
                //Формируем сообщение
                string message = new string[]
                    {
                        "Ошибка при попытке установить соединение с базой данных MS SQL.",
                        ex.ToFullMultiLineMessage()
                    }.ToMultiLineString();

                //Отображаем ошибку
                Monitor.Log.Error(PluginSettings.Name, message, true);
            }

            
        }

        /// <summary>
        /// Выбор базы данных из списка
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataBase_comboBox_DropDown(object sender, EventArgs e)
        {
            try
            {
                dataBase_comboBox.Items.Clear();

                using (SqlConnection connection = new SqlConnection(GetSqlConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("select name from sys.databases", connection))
                        using (SqlDataReader dataReader = command.ExecuteReader())
                            while (dataReader.Read())
                                dataBase_comboBox.Items.Add(dataReader["name"]);
                }

                if (dataBase_comboBox.Items.Count > 0)
                    dataBase_comboBox.SelectedIndex = 0;
            }
            catch (SqlException ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    "Ошибка при получении списка баз данных MS SQL сервера.",
                    ex.ToFullMultiLineMessage()
                }.ToMultiLineString();

                //Отображаем ошибку
                Monitor.Log.Error(PluginSettings.Name, message, true);
            }
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.IsSQL = main_tabControl.SelectedTab.Equals(msSQL_tabPage);

            if (PluginSettings.IsSQL)
            {
                PluginSettings.Sql.ServerName = sqlServerConnectionControl.ServerName;
                PluginSettings.Sql.Database = dataBase_comboBox.Text;
                PluginSettings.Sql.IsWindowsAuthentification = sqlServerConnectionControl.IsWindowsAuthentication;
                PluginSettings.Sql.Login = sqlServerConnectionControl.ServerLogin;
                PluginSettings.Sql.Password = sqlServerConnectionControl.ServerPassword;
            }
            else
            {
                PluginSettings.Access.DatabaseFilePath = access_textBoxWithDialogButton.Text;
            }
        }        
    }
}
