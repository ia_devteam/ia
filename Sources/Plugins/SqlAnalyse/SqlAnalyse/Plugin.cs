﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

using IA.Extensions;
using Store;
using Store.Table;

using IOController;

namespace IA.Plugins.Analyses.SQLAnalyse
{
    /// <summary>
    /// Стандартный класс констант и установок плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Анализ баз данных (SQL, Access)";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.SQL_ANALYSE;
        /// <summary>
        /// Наименования тасков плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Анализ базы данных Microsoft SQL")]
            SQL_ANALYSIS,
            [Description("Анализ базы данных Microsoft Access")]
            ACCESS_ANALYSIS
        };

        #region Settings

        /// <summary>
        /// Тип соединения
        /// </summary>
        internal static bool IsSQL;

        /// <summary>
        /// Настройки, отосящиеся к базе данных MS SQL
        /// </summary>
        internal static class Sql
        {
            /// <summary>
            /// Наименование сервера
            /// </summary>
            internal static string ServerName;

            /// <summary>
            /// Наименование базы данных
            /// </summary>
            internal static string Database;

            /// <summary>
            /// Тип соединения
            /// </summary>
            internal static bool IsWindowsAuthentification;

            /// <summary>
            /// Логин
            /// </summary>
            internal static string Login;

            /// <summary>
            /// Пароль
            /// </summary>
            internal static string Password;
        }

        /// <summary>
        /// Настройки, отосящиеся к базе данных MS Access
        /// </summary>
        internal static class Access
        {
            /// <summary>
            /// Путь до файла базы данных MS Access
            /// </summary>
            internal static string DatabaseFilePath;
        }
        #endregion
    }

    /// <summary>
    /// Класс плагина
    /// </summary>
    public sealed class SQLAnalyse : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage = null;

        /// <summary>
        /// Настройки плагина
        /// </summary>
        IA.Plugin.Settings settings = null;

        /// <summary>
        /// Объект класса-анализатора баз данных
        /// </summary>
        private MsDatabaseAnalyzer analyzer = null;

        /// <summary>
        /// Директория для формирования результатов анализа
        /// </summary>
        private string analyserDataDirectoryPath = null;

        #region Интерфейс плагина

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return
                    Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW |
                    Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW |
                    Plugin.Capabilities.RERUN |
                    Plugin.Capabilities.REPORTS;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.IsSQL = Properties.Settings.Default.IsSQL;
            PluginSettings.Sql.ServerName = Properties.Settings.Default.Sql_ServerName;
            PluginSettings.Sql.IsWindowsAuthentification = Properties.Settings.Default.Sql_IsWindowsAuthentification;
            PluginSettings.Sql.Login= Properties.Settings.Default.Sql_Login;
            PluginSettings.Sql.Password = Properties.Settings.Default.Sql_Password;
            PluginSettings.Sql.Database = Properties.Settings.Default.Sql_Database;
            PluginSettings.Access.DatabaseFilePath = Properties.Settings.Default.Access_DatabaseFilePath;

            //Задаём директорию для формирования результатов анализа
            this.analyserDataDirectoryPath = storage.pluginData.GetDataFolder(PluginSettings.ID);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.IsSQL);
            PluginSettings.Sql.ServerName = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.Sql.IsWindowsAuthentification);
            PluginSettings.Sql.Login = settingsBuffer.GetString();
            PluginSettings.Sql.Password = settingsBuffer.GetString();
            PluginSettings.Sql.Database = settingsBuffer.GetString();
            PluginSettings.Access.DatabaseFilePath = settingsBuffer.GetString();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new CustomSettings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new CustomSettings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            Properties.Settings.Default.IsSQL = PluginSettings.IsSQL;
            Properties.Settings.Default.Sql_ServerName = PluginSettings.Sql.ServerName;
            Properties.Settings.Default.Sql_IsWindowsAuthentification = PluginSettings.Sql.IsWindowsAuthentification;
            Properties.Settings.Default.Sql_Login = PluginSettings.Sql.Login;
            Properties.Settings.Default.Sql_Password = PluginSettings.Sql.Password;
            Properties.Settings.Default.Sql_Database = PluginSettings.Sql.Database;
            Properties.Settings.Default.Access_DatabaseFilePath = PluginSettings.Access.DatabaseFilePath;
            Properties.Settings.Default.Save();

            settingsBuffer.Add(PluginSettings.IsSQL);
            settingsBuffer.Add(PluginSettings.Sql.ServerName);
            settingsBuffer.Add(PluginSettings.Sql.IsWindowsAuthentification);
            settingsBuffer.Add(PluginSettings.Sql.Login);
            settingsBuffer.Add(PluginSettings.Sql.Password);
            settingsBuffer.Add(PluginSettings.Sql.Database);
            settingsBuffer.Add(PluginSettings.Access.DatabaseFilePath);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = String.Empty;

            if (settings != null)
                settings.Save();

            //Sql
            if (PluginSettings.IsSQL)
            {
                if (string.IsNullOrWhiteSpace(PluginSettings.Sql.ServerName))
                {
                    message = "Не указано имя MS SQL сервера.";
                    return false;
                }
                if (string.IsNullOrWhiteSpace(PluginSettings.Sql.Database))
                {
                    message = "Не указано имя базы данных MS SQL сервера.";
                    return false;
                }
                if (!PluginSettings.Sql.IsWindowsAuthentification && String.IsNullOrWhiteSpace(PluginSettings.Sql.Login))
                {
                    message = "Не указано имя пользователя MS SQL сервера.";
                    return false;
                }
            }
            //Access
            else
            {
                if (String.IsNullOrWhiteSpace(PluginSettings.Access.DatabaseFilePath))
                {
                    message = "Не указан путь до файла базы данных MS Access.";
                    return false;
                }

                if (!FileController.IsExists(PluginSettings.Access.DatabaseFilePath))
                {
                    message = "Путь до файла базы данных MS Access <" + PluginSettings.Access.DatabaseFilePath + "> не существует.";
                    return false;
                }

                string fileExtensionLower = new FileInfo(PluginSettings.Access.DatabaseFilePath).Extension.ToLower();
                if (fileExtensionLower != ".mdb" && fileExtensionLower != ".accdb")
                {
                    message = "Файл базы данных MS Access имеет недопустимое расширение <" + fileExtensionLower + ">.";
                    return false;
                }
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                if (PluginSettings.IsSQL)
                {
                    return new List<Monitor.Tasks.Task>()
                    {
                        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.SQL_ANALYSIS.Description(), 1)
                    };
                }
                else
                {
                    return new List<Monitor.Tasks.Task>()
                    {
                        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.ACCESS_ANALYSIS.Description(), 1)
                    };
                }
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            //Формируем новый объект анализатора
            this.analyzer = new MsDatabaseAnalyzer(this.analyserDataDirectoryPath);

            //Запускаем анализ
            if (PluginSettings.IsSQL)
                analyzer.MsSql_Analyse(
                    PluginSettings.Sql.ServerName,
                    PluginSettings.Sql.IsWindowsAuthentification,
                    PluginSettings.Sql.Login,
                    PluginSettings.Sql.Password,
                    PluginSettings.Sql.Database
                );
            else
                analyzer.MsAccess_Analyse(
                    PluginSettings.Access.DatabaseFilePath
                );
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            //Если анализ не проводился, более ничего не делаем
            if (analyzer == null)
                return;

            //Копируем имеющиеся результаты анализа
            IOController.DirectoryController.Copy(this.analyserDataDirectoryPath, reportsDirectoryPath, true);

            //Генерируем отчёты
            if (PluginSettings.IsSQL)
                analyzer.MsSql_GenerateReports(reportsDirectoryPath);
            else
                analyzer.MsAccess_GenerateReports(reportsDirectoryPath);
        }
        #endregion Интерфейс плагина
    }
}
