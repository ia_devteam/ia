﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using Store.Table;
using TestUtils;
using IA.Plugins.Parsers.PyParser;

namespace TestPyParser
{
    /// <summary>
    /// Класс для тестов парсера JavaScript (NodeJS)
    /// </summary>
    [TestClass]
    public class TestPyParser
    {

        #region ServiceRegion
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Список сообщений
            /// </summary>
            public List<string> Messages
            {
                get 
                {
                    return messages;
                }
            }
        }

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Путь до каталога с эталонными исходными текстами
        /// </summary>
        private string labSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонным дампами Хранилища
        /// </summary>
        private string dumpSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонными отчетами
        /// </summary>
        private string reportSampleDirectoryPath;

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными лабораторным текстам
        /// </summary>
        private const string LAB_SAMPLES_SUBDIRECTORY = @"PyParser\Labs";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными дампами Хранилища
        /// </summary>
        private const string DUMP_SAMPLES_SUBDIRECTORY = @"PyParser\Dump";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными отчетами
        /// </summary>
        private const string REPORT_SAMPLES_SUBDIRECTORY = @"PyParser\Reports";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        private const ulong pluginID = Store.Const.PluginIdentifiers.PYTHON_PARSER;

        /// <summary>
        /// Перехватчик сообщений монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName); 
            labSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, LAB_SAMPLES_SUBDIRECTORY);
            dumpSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, DUMP_SAMPLES_SUBDIRECTORY);
            reportSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, REPORT_SAMPLES_SUBDIRECTORY);
        }

        /// <summary>
        /// Подготовка Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="testMaterialsPath">Путь до каталога с материалами.</param>
        /// <param name="sourcePostfixPart">Подпуть до каталога с исходными файлами.</param>
        /// <param name="isLevel2">Уровень НДВ. true - 2-ой уровень</param>
        private void CustomizeStorage(Storage storage, string testMaterialsPath, string sourcePostfixPart, bool isLevel2, ulong firstSensorNumber)
        {
            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
            TestUtilsClass.Run_IdentifyFileTypes(storage);
            TestUtilsClass.Run_CheckSum(storage);

            SettingUpPlugin(storage, firstSensorNumber, isLevel2);
        }

        /// <summary>
        /// Настройка плагина
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="firstSensorNumber">Номер первого датчика. Не может быть отрицательным.</param>
        /// <param name="isLevel2">true - 2-ой уровень.</param>
        private void SettingUpPlugin(Storage storage, UInt64 firstSensorNumber, bool isLevel2)
        {
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(isLevel2, !isLevel2);
            buffer.Add(firstSensorNumber);
            buffer.Add("Sensor");
            buffer.Add("sensors");
            buffer.Add(0);

            storage.pluginSettings.SaveSettings(pluginID, buffer);
        }

        /// <summary>
        /// Дамп Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <returns>Путь до каталога, куда был произведен дамп Хранилища</returns>
        private string Dump(Storage storage)
        {
            string dumpDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.STORAGE), "Dump");

            if (!Directory.Exists(dumpDirectoryPath))
                Directory.CreateDirectory(dumpDirectoryPath);

            storage.ClassesDump(dumpDirectoryPath);
            storage.FilesDump(dumpDirectoryPath);
            storage.FunctionsDump(dumpDirectoryPath);
            storage.VariablesDump(dumpDirectoryPath);

            return dumpDirectoryPath;
        }

        /// <summary>
        /// Проверка дампа Хранилища
        /// </summary>
        /// <param name="dumpDirectoryPath">Путь до каталога дампа в Хранилище. Не может быть пустым.</param>
        /// <param name="dumpSamplesDirectoryPath">Путь до каталога с эталонным дампом. Не может быть пустым.</param>
        private void CheckDump(string dumpDirectoryPath, string dumpSamplesDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(dumpDirectoryPath, dumpSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Дамп Хранилища не совпадает с эталонными.");
        }

        /// <summary>
        /// Проверка лабораторных исходных файлов
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="labSamplesDirectoryPath">Путь до каталога с эталонными лабораторными исходными текстами. Не может быть пустым.</param>
        private void CheckLabs(Storage storage, string labSamplesDirectoryPath)
        {
            string labsDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "Python");

            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(labsDirectoryPath, labSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Исходные тексты со вставленными датчиками не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка отчетов
        /// </summary>
        /// <param name="xmlSampleDirectoryPath">Путь до каталога с эталонными отчетами. Не может быть пустым.</param>
        /// <param name="storageReportsDirectoryPath">Путь до каталога с отчетами в Хранилище. Не может быть пустым.</param>
        private void CheckXML(string xmlSampleDirectoryPath, string storageReportsDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_XML_Compare(storageReportsDirectoryPath, xmlSampleDirectoryPath);

            Assert.IsTrue(isEqual, "Отчеты не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка ожидаемого сообщения в логе монитора
        /// </summary>
        /// <param name="message">Текст сообщения. Не может быть пустым.</param>
        private void CheckMessage(string message)
        {
            bool isContainMessage = listener.ContainsPart(message);

            Assert.IsTrue(isContainMessage, "Сообщение не совпадает с ожидаемым.");
        }

        #endregion

        [TestMethod]
        public void PyParser_EmptyFolder()
        {
            string sourcePostfixPart = @"Sources\PyParser\Empty";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   CheckMessage("Нет файлов для обработки.");

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_BadFile()
        {
            string sourcePostfixPart = @"Sources\PyParser\BadFile";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   CheckMessage("Файл не распознан парсером: my.py");

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_DebugDummyTestLvl3()
        {
            string sourcePostfixPart = @"Sources\PyParser\Dummy";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\Dummy"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\Dummy"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\Dummy"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_DebugDummyTestLvl2()
        {
            string sourcePostfixPart = @"Sources\PyParser\Dummy";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\Dummy"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\Dummy"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\Dummy"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_BreakStatement2()
        {
            string sourcePostfixPart = @"Sources\PyParser\BreakStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\BreakStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\BreakStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\BreakStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_BreakStatement3()
        {
            string sourcePostfixPart = @"Sources\PyParser\BreakStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\BreakStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\BreakStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\BreakStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_ClassDef2()
        {
            string sourcePostfixPart = @"Sources\PyParser\ClassDef";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\ClassDef"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\ClassDef"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\ClassDef"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_ClassDef3()
        {
            string sourcePostfixPart = @"Sources\PyParser\ClassDef";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\ClassDef"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\ClassDef"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\ClassDef"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_ContinueStatement2()
        {
            string sourcePostfixPart = @"Sources\PyParser\ContinueStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\ContinueStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\ContinueStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\ContinueStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_ContinueStatement3()
        {
            string sourcePostfixPart = @"Sources\PyParser\ContinueStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\ContinueStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\ContinueStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\ContinueStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_ForStatement2()
        {
            string sourcePostfixPart = @"Sources\PyParser\ForStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\ForStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\ForStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\ForStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_ForStatement3()
        {
            string sourcePostfixPart = @"Sources\PyParser\ForStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\ForStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\ForStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\ForStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_FunctionCall2()
        {
            string sourcePostfixPart = @"Sources\PyParser\FunctionCall";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\FunctionCall"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\FunctionCall"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\FunctionCall"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_FunctionCall3()
        {
            string sourcePostfixPart = @"Sources\PyParser\FunctionCall";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\FunctionCall"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\FunctionCall"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\FunctionCall"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_FunctionDef2()
        {
            string sourcePostfixPart = @"Sources\PyParser\FunctionDef";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\FunctionDef"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\FunctionDef"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\FunctionDef"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_FunctionDef3()
        {
            string sourcePostfixPart = @"Sources\PyParser\FunctionDef";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\FunctionDef"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\FunctionDef"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\FunctionDef"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_IfStatement2()
        {
            string sourcePostfixPart = @"Sources\PyParser\IfStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\IfStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\IfStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\IfStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_IfStatement3()
        {
            string sourcePostfixPart = @"Sources\PyParser\IfStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\IfStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\IfStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\IfStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_PassStatement2()
        {
            string sourcePostfixPart = @"Sources\PyParser\PassStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\PassStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\PassStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\PassStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_PassStatement3()
        {
            string sourcePostfixPart = @"Sources\PyParser\PassStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\PassStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\PassStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\PassStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_RaiseStatement2()
        {
            string sourcePostfixPart = @"Sources\PyParser\RaiseStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\RaiseStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\RaiseStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\RaiseStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_RaiseStatement3()
        {
            string sourcePostfixPart = @"Sources\PyParser\RaiseStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\RaiseStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\RaiseStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\RaiseStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_ReturnStatement2()
        {
            string sourcePostfixPart = @"Sources\PyParser\ReturnStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\ReturnStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\ReturnStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\ReturnStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_ReturnStatement3()
        {
            string sourcePostfixPart = @"Sources\PyParser\ReturnStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\ReturnStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\ReturnStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\ReturnStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_TryStatement2()
        {
            string sourcePostfixPart = @"Sources\PyParser\TryStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\TryStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\TryStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\TryStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_TryStatement3()
        {
            string sourcePostfixPart = @"Sources\PyParser\TryStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\TryStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\TryStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\TryStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_WhileStatement2()
        {
            string sourcePostfixPart = @"Sources\PyParser\WhileStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"2\WhileStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"2\WhileStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"2\WhileStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }

        [TestMethod]
        public void PyParser_WhileStatement3()
        {
            string sourcePostfixPart = @"Sources\PyParser\WhileStatement";
            PyParser testPlugin = new PyParser();

            testUtils.RunTest(
               sourcePostfixPart,                                                               //sourcePostFixPart
               testPlugin,                                                                      //plugin
               false,                                                                           //isUseCachedStorage               
               (storage, testMaterialsPath) => { },                                             //prepareStorage               
               (storage, testMaterialsPath) =>
               {
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, false, 1);
               },                                                                               //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"3\WhileStatement"));
                   CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, @"3\WhileStatement"));

                   return true;
               },                                                                               //checkStorage
               (reportsFullPath, testMaterialsPath) =>
               {
                   testPlugin.GenerateStatements(reportsFullPath);

                   CheckXML(Path.Combine(reportSampleDirectoryPath, @"3\WhileStatement"), reportsFullPath);

                   return true;
               },                                                                               //checkreports
               false,                                                                           //isUpdateReport
               (plugin, testMaterialsPath) => { },                                              //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },                                //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }                             //checkReportAfterRerun
               );
        }
    }
}
