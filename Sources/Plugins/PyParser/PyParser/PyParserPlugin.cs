﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

using IA.Plugin;
using Store;
using Store.Table;

using IA.Extensions;
using IA.Plugins.Parsers.CommonUtils;
using FileOperations;

namespace IA.Plugins.Parsers.PyParser
{
    internal static class PluginSettings
    {
        public const uint ID = Store.Const.PluginIdentifiers.PYTHON_PARSER;
        public const string Name = "Парсер Python";
       
        /// <summary>
        /// Задачи плагинов
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка файлов")]
            FILES_PROCESSING,
            [Description("Расстановка датчиков")]
            SENSOR_PLACEMENT
        };

        #region Settings
        /// <summary>
        /// 2-й уровень вставки датчиков
        /// </summary>
        internal static bool IsLevel2;
        /// <summary>
        /// 3-й уровень вставки датчиков
        /// </summary>
        internal static bool IsLevel3;

        internal static string SensorFunctionName;

        internal static string SensorModuleName;

        internal static int UselessNumber;
        /// <summary>
        /// Номер первого датчика
        /// </summary>
        internal static ulong FirstSensorNumber;
        #endregion
        
    }

    public class PyParser : IA.Plugin.Interface
    {

        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Настройки
        /// </summary>
        IA.Plugin.Settings settings;
        
        /// <summary>
        /// Признак был создан парсером XML или же нет
        /// </summary>
        public bool IsXML = false;

        /// <summary>
        /// Число файлов *.js на обработку парсеру
        /// </summary>
        public long pyFileCount = 0;

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public IA.Plugin.Capabilities Capabilities
        {
            get
            {
#if DEBUG
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS;
#else
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
#endif
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
		}

        /// <summary>
        /// Метод генерации отчетов для тестов
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до каталога с отчетами плагина</param>
        public void GenerateStatements(string reportsDirectoryPath)
        {
            (new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage)).Execute();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            #if DEBUG
            // Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            if (string.IsNullOrEmpty(reportsDirectoryPath))
                throw new Exception("Путь к каталогу с отчетами не определен.");

            //StatementPrinter
            //StatementPrinter sprinter = new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage);
            //sprinter.Execute();
            #endif
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID {get { return PluginSettings.ID; } }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            PluginSettings.IsLevel2 = true;
            PluginSettings.IsLevel3 = false;
            PluginSettings.FirstSensorNumber = Properties.Settings.Default.FirstSensorNumber;
            PluginSettings.SensorFunctionName = Properties.Settings.Default.SensorFunctionName;
            PluginSettings.SensorModuleName = Properties.Settings.Default.SensorModuleName;
            PluginSettings.UselessNumber = Properties.Settings.Default.UselessNumber;
            this.storage = storage;

            //Выгружаем настройки из данных плагина
            LoadPluginData();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.IsLevel2, ref PluginSettings.IsLevel3);
            PluginSettings.FirstSensorNumber = settingsBuffer.GetUInt64();
            PluginSettings.SensorFunctionName = settingsBuffer.GetString();
            PluginSettings.SensorModuleName = settingsBuffer.GetString();
            PluginSettings.UselessNumber = settingsBuffer.GetInt32();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.FirstSensorNumber = (storage != null && storage.sensors.EnumerateSensors().Any())
                ? storage.sensors.EnumerateSensors().Max(s => s.ID) + 1
                : 1;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "NDV:3":
                    PluginSettings.IsLevel3 = true;
                    PluginSettings.IsLevel2 = false;
                    break;
                case "NDV:2":
                    PluginSettings.IsLevel2 = true;
                    PluginSettings.IsLevel3 = false;
                    break;
                default:
                    throw new NotImplementedException();
            }
            //everything else - by default
        }
        
        public UserControl SimpleSettingsWindow
        { get { return null; } }

        /// <summary> 
        /// 
        /// </summary>
        /// <param name="IsTestEnv">При тестовой среде удаляются номера дтчиков и # у ReplaceReturn</param>
        public void Run()
        {
            if (storage != null && storage.sensors.Count() > 0 && storage.sensors.EnumerateSensors().Max(s => s.ID) >=
                PluginSettings.FirstSensorNumber)
            {
                ulong tmp_FirstSensorNumber = PluginSettings.FirstSensorNumber;
                PluginSettings.FirstSensorNumber = storage.sensors.EnumerateSensors().Max(s => s.ID) + 1;
                Monitor.Log.Warning(PluginSettings.Name,
                    "Расстановка датчиков с номера <" + tmp_FirstSensorNumber +
                    "> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <" +
                    PluginSettings.FirstSensorNumber + ">.");
            }
            // все что возвращает 'IA'
            string[] filesWithJS = storage.files.EnumerateFiles(enFileKind.fileWithPrefix)
                //.Where(f => f.fileType.ContainsLanguages(enLanguage.Python))
                .Select(f => f.FullFileName_Original)
                .ToArray();

            // здесь выбираем только *.py файлы
            string[] onlyPyFiles = filesWithJS.Where(f => f.EndsWith(".py")).ToArray(); //we are not interested in pyc
            pyFileCount = onlyPyFiles.LongLength;

            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong)pyFileCount);
            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)1).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong)pyFileCount);
            if (onlyPyFiles.Length == 0)
                Monitor.Log.Error(PluginSettings.Name, "Нет файлов для обработки.");
            else
            {

                RunConfiguration config = new RunConfiguration();
                string currentExeDir = IA.RuntimeDirectories.IARuntimeDirs.Plugins.ToString();
                config.PythonPath = Path.Combine(Directory.GetCurrentDirectory(), currentExeDir);

                config.FileListPath = Path.Combine(storage.GetTempDirectory(ID).Path, "filelist.txt");
                File.WriteAllLines(config.FileListPath, onlyPyFiles); //filesWithJS);

                config.FirstSensorNumber = PluginSettings.FirstSensorNumber; //storage.sensors.GetMaxSensorNumber() + 1;
                config.CleanFilesDir = storage.appliedSettings.FileListPath;

                //config.LabFilesDir = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "JavaScript");
                config.LabFilesDir = storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB);
                if (Directory.Exists(config.LabFilesDir))
                    Directory.Delete(config.LabFilesDir, true);
                Directory.CreateDirectory(config.LabFilesDir);
                storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB);
                config.LabFilesDir = config.LabFilesDir = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.SOURCES_LAB), "Python") + "\\";
                config.XmlFilePath = storage.pluginData.GetDataFolder(ID) + "\\python.xml";
                if (PluginSettings.IsLevel2)
                    config.InsertionLevel = 2;
                else
                    config.InsertionLevel = 3;
                config.SensorFunctionName = PluginSettings.SensorFunctionName;
                config.SensorModuleName = PluginSettings.SensorModuleName;
                config.UselessNumber = PluginSettings.UselessNumber;

                Directory.CreateDirectory(config.LabFilesDir);
                var t = Task.Run(() => ThreadRunNinjaPY(config));
                t.Wait();

                if (IsXML)
                {   // а вдруг xml окажется невалидным
                    //проверка - все ли построилось
                    var labPyFiles = Directory.EnumerateFiles(config.LabFilesDir, "*.py", SearchOption.AllDirectories);
                    if (labPyFiles.Count() != onlyPyFiles.Length)
                    {
                        var stripLabPath = config.LabFilesDir.Trim();
                        if (!(stripLabPath.EndsWith("/") || stripLabPath.EndsWith("\\"))) stripLabPath += "/";
                        var stripCleanPath = config.CleanFilesDir.Trim();
                        if (!(stripCleanPath.EndsWith("/") || stripCleanPath.EndsWith("\\"))) stripCleanPath += "/";
                        var labFilesStrippedPath = labPyFiles.Select(x => x.ToLower().Substring(stripLabPath.Length));
                        var origFilesStrippedPath = onlyPyFiles.Select(x => x.ToLower().Substring(stripCleanPath.Length));
                        var missing = origFilesStrippedPath.Except(labFilesStrippedPath);
                        foreach (var missingFile in missing)
                        {
                            Monitor.Log.Error(PluginSettings.Name, "Файл не распознан парсером: " + missingFile);
                        }
                    }
                        
                    try
                    {
                        var startSensorCount = storage.sensors.Count();
                        StorageImporter.XMLStorageImporterForParser
                            importer = new StorageImporter.XMLStorageImporterForParser();
                        XmlDocument xdoc = new XmlDocument();
                        xdoc.Load(config.XmlFilePath);
                        Monitor.Log.Warning(PluginSettings.Name, "Запуск импортера xml файла от парсера.");
                        importer.ImportXML(xdoc, storage);
                        Monitor.Log.Warning(PluginSettings.Name, $"Вставлено датчиков: {(storage.sensors.Count() - startSensorCount)}");
                    }
                    catch (System.Exception ex)
                    {
                        Monitor.Log.Error(PluginSettings.Name,
                            "Критическая ошибка при выполнении импорта данных из xml: " + ex.Message);
                    }
                }
            }
        }

        // для запуска парсера в отдельном потоке
        private void ThreadRunNinjaPY(RunConfiguration config)
        {
            string currentExeDir = IA.RuntimeDirectories.IARuntimeDirs.Plugins.ToString();
            string currentDir = Path.Combine(Directory.GetCurrentDirectory(), currentExeDir);
            string pypyEngine = Path.Combine(currentDir, "pypy2-v5.10.0-win32\\pypy.exe");

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.FileName = pypyEngine;
            
            startInfo.Arguments = string.Format("-m ninjapy -f={0} -x={1} -s={2} -o={3} -n={4} -i={5} -l={6} -m={7}",
                config.FileListPath, config.XmlFilePath, config.CleanFilesDir, config.LabFilesDir, 
                config.SensorFunctionName, config.FirstSensorNumber, config.InsertionLevel, config.SensorModuleName);

            if (config.UselessNumber != 0)
            {
                startInfo.Arguments += " -d=" + config.UselessNumber;
            }

            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.WorkingDirectory = currentDir;
            var pr = Process.Start(startInfo);
            while (!pr.StandardOutput.EndOfStream)
            {
                string line = pr.StandardOutput.ReadLine();
                Monitor.Log.Information(PluginSettings.Name, line);
            }
            pr.WaitForExit();
            IsXML = File.Exists(config.XmlFilePath);
        }

#region Misc

        public string Name { get { return PluginSettings.Name; } }


        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = String.Empty;

            if (settings != null)
                settings.Save();

            if (String.IsNullOrWhiteSpace(PluginSettings.SensorFunctionName) || String.IsNullOrWhiteSpace(PluginSettings.SensorModuleName))
            {
                message = "Текст датчика не задан.";
                return false;
            }

            if (PluginSettings.UselessNumber < 0)
            {
                message = "Недопустимый номер бесполезного датчика";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.IsLevel2 = PluginSettings.IsLevel2;
            Properties.Settings.Default.IsLevel3 = PluginSettings.IsLevel3;
            Properties.Settings.Default.FirstSensorNumber = PluginSettings.FirstSensorNumber;
            Properties.Settings.Default.SensorFunctionName = PluginSettings.SensorFunctionName;
            Properties.Settings.Default.SensorModuleName = PluginSettings.SensorModuleName;
            Properties.Settings.Default.UselessNumber = PluginSettings.UselessNumber;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.IsLevel2, PluginSettings.IsLevel3);
            settingsBuffer.Add(PluginSettings.FirstSensorNumber);
            settingsBuffer.Add(PluginSettings.SensorFunctionName);
            settingsBuffer.Add(PluginSettings.SensorModuleName);
            settingsBuffer.Add(PluginSettings.UselessNumber);
        }

        private void LoadPluginData()
        {

        }

        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new Settings(
                        storage.sensors.EnumerateSensors().Count() == 0
                        ? 1
                        : storage.sensors.EnumerateSensors().Max(s => s.ID) + 1)
                        );
            }
        }
#endregion

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        // <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description()),
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.SENSOR_PLACEMENT.Description())
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return null;
            }
        }

    } // END: class JavaScriptParser_NodeJS


    internal class RunConfiguration
    {
        public string PythonPath;
        public string FileListPath;
        public ulong FirstSensorNumber;
        public string CleanFilesDir;
        public string LabFilesDir;
        public string XmlFilePath;
        public int InsertionLevel;
        public int UselessNumber;
        public string SensorFunctionName;
        public string SensorModuleName;
    }
}
