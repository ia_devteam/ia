﻿using System;
using System.Windows.Forms;
using IA.Plugins.Parsers.PyParser;

namespace IA.Plugins.Parsers.PyParser
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        internal Settings(ulong minimumStartSensor)
        {
            InitializeComponent();

            //Основные настройки для элементов форм
            //Выбор уровня НДВ
            isLevel2_radioButton.Checked = PluginSettings.IsLevel2;
            isLevel3_radioButton.Checked = PluginSettings.IsLevel3;

            //Номер первого датчика
            firstSensorNum_num.Minimum = minimumStartSensor;
            firstSensorNum_num.Maximum = ulong.MaxValue;
            firstSensorNum_num.Value = 
                    (PluginSettings.FirstSensorNumber > minimumStartSensor)
                    ? PluginSettings.FirstSensorNumber
                    : minimumStartSensor;
            sensorName_tbx.Text = PluginSettings.SensorFunctionName;
            moduleName_tb.Text = PluginSettings.SensorModuleName;
            uselessNumber_num.Value = PluginSettings.UselessNumber;
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.IsLevel2 = isLevel2_radioButton.Checked;
            PluginSettings.IsLevel3 = isLevel3_radioButton.Checked;
            PluginSettings.FirstSensorNumber = (ulong)firstSensorNum_num.Value;
            PluginSettings.SensorFunctionName = sensorName_tbx.Text;
            PluginSettings.SensorModuleName = moduleName_tb.Text;
            PluginSettings.UselessNumber = (int)uselessNumber_num.Value;
        }
        // Задаем номер первого датчика
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            PluginSettings.FirstSensorNumber = (ulong)firstSensorNum_num.Value;
        }
        // Устанавливаем уровень 'НДВ-2'
        private void isLevel2_radioButton_MouseClick(object sender, MouseEventArgs e)
        {
            PluginSettings.IsLevel2 = isLevel2_radioButton.Checked;
            PluginSettings.IsLevel3 = isLevel3_radioButton.Checked = !isLevel2_radioButton.Checked;
        }
        // Устанавливаем уровень 'НДВ-3'
        private void isLevel3_radioButton_MouseClick(object sender, MouseEventArgs e)
        {
            PluginSettings.IsLevel3 = isLevel3_radioButton.Checked;
            PluginSettings.IsLevel2 = isLevel2_radioButton.Checked = !isLevel3_radioButton.Checked;
        }
    }
}
