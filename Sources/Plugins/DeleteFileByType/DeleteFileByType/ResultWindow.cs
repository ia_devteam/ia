﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Store;

namespace IA.Plugins.Analyses.DeleteFileByType
{
    /// <summary>
    /// Класс окна с результатами
    /// </summary>
    internal partial class ResultWindow : UserControl
    {
        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Список избыточных типов файлов
        /// </summary>
        List<FileType> redundantFileTypes;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage">Текущее хранилище.</param>
        /// <param name="redundantTypes">Список избыточных типов файлов.</param>
        internal ResultWindow(Storage storage, List<FileType> redundantTypes)
        {
            InitializeComponent();

            //Запоминаем текущее Хранилище
            this.storage = storage;

            //Запоминаем текущий список избыточных типов файлов
            redundantFileTypes = redundantTypes;

            //Если Хранилище не задано, ничего не делаем
            if (storage == null)
                return;
            
            //Формируем список доступных типов файлов
            foreach (FileType fileType in storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Select(f => f.fileType).Distinct())
            {
                if (fileType.Contains().HasFlag(enTypeOfContents.REDUNDANT))
                    redundantTypes_listBox.Items.Add(fileType);
                else 
                    essentialTypes_listBox.Items.Add(fileType);
            }

            //Определяем изначальную доступность кнопок
            toEssential_button.Enabled = toRedundant_button.Enabled = false;
        }

        /// <summary>
        /// Переместить тип в список существенных типов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toEssential_button_Click(object sender, EventArgs e)
        {
            foreach (FileType fType in essentialTypes_listBox.SelectedItems)
                redundantFileTypes.Remove(fType);

            //Перемещаем все выбранные элементы из списка избыточных типов
            MoveItem(redundantTypes_listBox, essentialTypes_listBox);

            redundantTypes_listBox.ClearSelected();
        }

        /// <summary>
        /// Переместить тип в список избыточных типов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toRedundant_button_Click(object sender, EventArgs e)
        {
            foreach (FileType fType in essentialTypes_listBox.SelectedItems)
                redundantFileTypes.Add(fType);

            //Перемещаем все выбранные элементы из списка существенных типов
            MoveItem(essentialTypes_listBox, redundantTypes_listBox);

            essentialTypes_listBox.ClearSelected();
        }
        
        private void essentialTypes_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (essentialTypes_listBox.SelectedItems == null || essentialTypes_listBox.SelectedItems.Count == 0)
                return;

            redundantTypes_listBox.ClearSelected();

            //Задаём активность кнопок
            toRedundant_button.Enabled = true;
            toEssential_button.Enabled = false;
        }

        private void redundantTypes_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (redundantTypes_listBox.SelectedItems == null || redundantTypes_listBox.SelectedItems.Count == 0)
                return;

            essentialTypes_listBox.ClearSelected();

            //Задаём активность кнопок
            toRedundant_button.Enabled = false;
            toEssential_button.Enabled = true;
        }

        /// <summary>
        /// Переместить тип из одного листбокса в другой
        /// </summary>
        /// <param name="from_listBox">Листбокс откуда нужно произвести перемещение</param>
        /// <param name="to_listBox">Листбокс куда нужно произвести перемещение</param>
        private void MoveItem(ListBox from_listBox, ListBox to_listBox)
        {
            foreach (object item in from_listBox.SelectedItems.Cast<object>().ToList())
            {                
                from_listBox.Items.Remove(item);
                to_listBox.Items.Add(item);
            }
        }
    }
}
