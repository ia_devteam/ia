﻿namespace IA.Plugins.Analyses.DeleteFileByType
{
    partial class ResultWindow
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.toEssential_button = new System.Windows.Forms.Button();
            this.toRedundant_button = new System.Windows.Forms.Button();
            this.redundantTypes_groupBox = new System.Windows.Forms.GroupBox();
            this.redundantTypes_listBox = new System.Windows.Forms.ListBox();
            this.essentialTypes_groupBox = new System.Windows.Forms.GroupBox();
            this.essentialTypes_listBox = new System.Windows.Forms.ListBox();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.redundantTypes_groupBox.SuspendLayout();
            this.essentialTypes_groupBox.SuspendLayout();
            this.main_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toEssential_button
            // 
            this.toEssential_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.toEssential_button.Location = new System.Drawing.Point(221, 295);
            this.toEssential_button.Name = "toEssential_button";
            this.toEssential_button.Size = new System.Drawing.Size(212, 23);
            this.toEssential_button.TabIndex = 1;
            this.toEssential_button.Text = "Тип существенен";
            this.toEssential_button.UseVisualStyleBackColor = true;
            this.toEssential_button.Click += new System.EventHandler(this.toEssential_button_Click);
            // 
            // toRedundant_button
            // 
            this.toRedundant_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.toRedundant_button.Location = new System.Drawing.Point(3, 295);
            this.toRedundant_button.Name = "toRedundant_button";
            this.toRedundant_button.Size = new System.Drawing.Size(212, 23);
            this.toRedundant_button.TabIndex = 0;
            this.toRedundant_button.Text = "Тип избыточен";
            this.toRedundant_button.UseVisualStyleBackColor = true;
            this.toRedundant_button.Click += new System.EventHandler(this.toRedundant_button_Click);
            // 
            // redundantTypes_groupBox
            // 
            this.redundantTypes_groupBox.Controls.Add(this.redundantTypes_listBox);
            this.redundantTypes_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.redundantTypes_groupBox.Location = new System.Drawing.Point(221, 3);
            this.redundantTypes_groupBox.Name = "redundantTypes_groupBox";
            this.redundantTypes_groupBox.Size = new System.Drawing.Size(212, 286);
            this.redundantTypes_groupBox.TabIndex = 3;
            this.redundantTypes_groupBox.TabStop = false;
            this.redundantTypes_groupBox.Text = "Избыточные типы файлов";
            // 
            // redundantTypes_listBox
            // 
            this.redundantTypes_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.redundantTypes_listBox.FormattingEnabled = true;
            this.redundantTypes_listBox.Location = new System.Drawing.Point(3, 16);
            this.redundantTypes_listBox.Name = "redundantTypes_listBox";
            this.redundantTypes_listBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.redundantTypes_listBox.Size = new System.Drawing.Size(206, 267);
            this.redundantTypes_listBox.TabIndex = 0;
            this.redundantTypes_listBox.SelectedIndexChanged += new System.EventHandler(this.redundantTypes_listBox_SelectedIndexChanged);
            // 
            // essentialTypes_groupBox
            // 
            this.essentialTypes_groupBox.Controls.Add(this.essentialTypes_listBox);
            this.essentialTypes_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.essentialTypes_groupBox.Location = new System.Drawing.Point(3, 3);
            this.essentialTypes_groupBox.Name = "essentialTypes_groupBox";
            this.essentialTypes_groupBox.Size = new System.Drawing.Size(212, 286);
            this.essentialTypes_groupBox.TabIndex = 2;
            this.essentialTypes_groupBox.TabStop = false;
            this.essentialTypes_groupBox.Text = "Существенные типы файлов";
            // 
            // essentialTypes_listBox
            // 
            this.essentialTypes_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.essentialTypes_listBox.FormattingEnabled = true;
            this.essentialTypes_listBox.Location = new System.Drawing.Point(3, 16);
            this.essentialTypes_listBox.Name = "essentialTypes_listBox";
            this.essentialTypes_listBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.essentialTypes_listBox.Size = new System.Drawing.Size(206, 267);
            this.essentialTypes_listBox.TabIndex = 0;
            this.essentialTypes_listBox.SelectedIndexChanged += new System.EventHandler(this.essentialTypes_listBox_SelectedIndexChanged);
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 2;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_tableLayoutPanel.Controls.Add(this.toRedundant_button, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.redundantTypes_groupBox, 1, 0);
            this.main_tableLayoutPanel.Controls.Add(this.essentialTypes_groupBox, 0, 0);
            this.main_tableLayoutPanel.Controls.Add(this.toEssential_button, 1, 1);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 2;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(436, 322);
            this.main_tableLayoutPanel.TabIndex = 7;
            // 
            // ResultWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "ResultWindow";
            this.Size = new System.Drawing.Size(436, 322);
            this.redundantTypes_groupBox.ResumeLayout(false);
            this.essentialTypes_groupBox.ResumeLayout(false);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox redundantTypes_groupBox;
        private System.Windows.Forms.ListBox redundantTypes_listBox;
        private System.Windows.Forms.Button toRedundant_button;
        private System.Windows.Forms.Button toEssential_button;
        private System.Windows.Forms.GroupBox essentialTypes_groupBox;
        private System.Windows.Forms.ListBox essentialTypes_listBox;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;

    }
}
