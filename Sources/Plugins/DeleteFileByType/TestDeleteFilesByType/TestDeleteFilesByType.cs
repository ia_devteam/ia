﻿using System.Linq;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using TestUtils;
using IA.Extensions;

namespace TestDeleteFilesByType
{
    [TestClass]
    public class TestDeleteFilesByType
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }  

        /// <summary>
        /// Проверка корректности отработки плагина при стандартных настройках.
        /// Типы файлов в Хранилище определяются плагином Идентификация файлов
        /// </summary>
        [TestMethod]
        public void DeleteFileByType_TestIdentifyFileTypes()
        {
            const string sourcePostfixPart = @"DeleteFileByType\RandomFiles";
            testUtils.RunTest(
                sourcePostfixPart,                                                      // sourcePostfixPart
                new IA.Plugins.Analyses.DeleteFileByType.DeleteFileByType(),            // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    WriteToReport(reportNew, storage);

                    string reportOld = Path.Combine(testMaterialsPath, @"DeleteFileByType\ReportTestIdentifyFile.txt");

                    return TestUtilsClass.Reports_TXT_Compare(reportOld, reportNew, false, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }

        private void WriteToReport(string report, Storage storage)
        {
            using (StreamWriter writer = new StreamWriter(report))
            {
                foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
                {
                    writer.WriteLine("{0}\t{1}", file.FileNameForReports, file.FileEssential.ToString());
                }
            }
        }
               
        /// <summary>
        /// Проверка корректности отработки плагина, где типы файлов и избыточность файлов проставлены вручную.
        /// Все файлы папки "Sources\JavaScript" делятся на попалам. 
        /// Первая половина файлов делается избыточной, вторая - не избыточной. 
        /// После этого для каждого второго файла из папки назначается избыточный тип файлов, а остальным - не избыточный. 
        /// В результате получаем все комбинации избыточности файлов и типов. 
        /// </summary>
        [TestMethod]
        public void DeleteFileByType_RandomRedunduncy()
        {
            const string sourcePostfixPart = @"Sources\JavaScript";
            testUtils.RunTest(
                sourcePostfixPart,                                                           // sourcePostfixPart
                new IA.Plugins.Analyses.DeleteFileByType.DeleteFileByType(),            // Plugin
                false,                                                                  // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                    // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    var list = storage.files.EnumerateFiles(enFileKind.fileWithPrefix).ToList();

                    list.ForEach(f => f.FileEssential = enFileEssential.SUPERFLUOUS);
                    list.Take(list.Count / 2).ForEach(f => f.FileEssential = enFileEssential.ESSENTIAL);

                    foreach (IFile file in list)
                        file.fileType = ((list.IndexOf(file) % 2) == 0) ? new FileType(@"OBJ file") : new FileType("Исходные тексты на языке C"); //TODO: переписать на более логичные типы файлов (содержащий флаг "обычно избыточный" и не содержащий)
                },                                                                      // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string reportNew = Path.Combine(testUtils.TestRunDirectoryPath, @"ReportNew.txt");
                    WriteToReport(reportNew, storage);

                    string reportOld = Path.Combine(testMaterialsPath, @"DeleteFileByType\ReportRedunduncy.txt");

                    return TestUtilsClass.Reports_TXT_Compare(reportOld, reportNew, false, false, testMaterialsPath);
                },                                                                      // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                   // checkReportBeforeRerun
                false,                                                                  // isUpdateReport
                (plugin, testMaterialsPath) => { },                                     // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                       // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                    // checkReportAfterRerun
                );
        }
    }
}
