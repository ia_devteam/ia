﻿using Microsoft.TeamFoundation.VersionControl.Client;
using System.Collections.Generic;
using System.Text;

namespace TFSInterop
{
    partial class TFSserverInterop
    {
        Dictionary<int, List<changetInfo>> changesetsItems; //itemId, list of changesets where itemId was
        Dictionary<int, List<changetInfo>> ItemsInChangesets; //changesetId, list of items in changeset with changetypes

        public void FillChangesetsItems(int startChangeset, int endChangeset)
        {
            changesetsItems = new Dictionary<int,List<changetInfo>>();
            for (int i = startChangeset; i <= endChangeset; i++)
            {
                var changeset = m_vcs.GetChangeset(i, true, false);
                var changes = changeset.Changes;
                for (int j = 0; j < changes.Length; j++)
                {
                    var change = changes[j];
                    var item = change.Item;
                    
                    if (!item.ServerItem.StartsWith(this.TeamProjectPath))
                        break;

                    if (item.ItemType != ItemType.File)
                        continue;

                    if (!changesetsItems.ContainsKey(item.ItemId))
                        changesetsItems.Add(item.ItemId, new List<changetInfo>());
                    changesetsItems[item.ItemId].Add(new changetInfo() { changesetId = changeset.ChangesetId, typeOfChange = change.ChangeType });
                }
            }
        }

        public void FillChangesetsItems2(int startChangeset, int endChangeset)
        {
            ItemsInChangesets = new Dictionary<int, List<changetInfo>>();
            for (int i = startChangeset; i <= endChangeset; i++)
            {
                var changeset = m_vcs.GetChangeset(i, true, false);
                var changes = changeset.Changes;
                for (int j = 0; j < changes.Length; j++)
                {
                    var change = changes[j];
                    var item = change.Item;

                    if (!item.ServerItem.StartsWith(this.TeamProjectPath))
                        break;

                    if (item.ItemType != ItemType.File)
                        continue;

                    if (!ItemsInChangesets.ContainsKey(changeset.ChangesetId))
                        ItemsInChangesets.Add(changeset.ChangesetId, new List<changetInfo>());
                    ItemsInChangesets[changeset.ChangesetId].Add(new changetInfo() { itemId = item.ItemId, typeOfChange = change.ChangeType });
                }
            }
        }

        public string PrintChangesetsItems()
        {
            StringBuilder sb = new StringBuilder(changesetsItems.Count * 200);

            foreach (var kvp in changesetsItems)
            {
                sb.Append(kvp.Key.ToString());
                Item curItem = m_vcs.GetItem(kvp.Key, kvp.Value[0].changesetId, false);
                sb.Append('\t');
                sb.AppendLine(curItem.ServerItem);
                
                int count = kvp.Value.Count;
                for (int i = 0; i < count; i++)
                {
                    sb.Append('\t');
                    sb.Append(kvp.Value[i].changesetId);
                    sb.Append('\t');
                    sb.AppendLine(kvp.Value[i].typeOfChange.ToString());
                }
                
            }

            return sb.ToString();
        }

        public string PrintChangesetsItems2()
        {
            StringBuilder sb = new StringBuilder(ItemsInChangesets.Count * 200);

            foreach (var kvp in ItemsInChangesets)
            {
                sb.AppendLine(kvp.Key.ToString());
                int count = kvp.Value.Count;
                for (int i = 0; i < count; i++)
                {
                    Item curItem = m_vcs.GetItem(kvp.Key, kvp.Value[0].itemId, false);
                    sb.Append('\t');
                    sb.Append(curItem.ServerItem);
                    sb.Append('\t');
                    sb.Append(kvp.Value[i].itemId);
                    sb.Append('\t');
                    sb.AppendLine(kvp.Value[i].typeOfChange.ToString());
                }

            }

            return sb.ToString();
        }
    }

    struct changetInfo
    {
        public int changesetId;
        public int itemId;
        public ChangeType typeOfChange;
    }
}
