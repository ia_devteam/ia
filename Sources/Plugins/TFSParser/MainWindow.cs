﻿using System;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace TFSHistoryParser2
{
    public partial class MainWindow : Form
    {
        delegate void dummy(string s);
        delegate void dummy2();

        dummy setResultLabel;
        dummy2 changesetChangedDelegate;

        bool handlersSet;
        TFSInterop.TFSserverInterop TFSinterop;

        public MainWindow()
        {
            InitializeComponent();

            changesetChangedDelegate = progressChangesets.PerformStep;
            setResultLabel = (s) => this.Text = s;

            handlersSet = false;

            this.Text = "Выгрузка изменений в TFS. Ожидание...";
        }

        private void TfsSet()
        {
            var tfs = new TFSInterop.TFSserverInterop(txt_TFSCollectionURL.Text, txt_TeamProjectPath.Text);
            this.TFSinterop = tfs;

            tfs.ChangesetProcessedEvent += ChangesetChangedHandler;

            txt_TFSCollectionURL.ReadOnly = true;
            txt_TeamProjectPath.ReadOnly = true;

            handlersSet = true;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!handlersSet)
                TfsSet();

            this.Text = "Обработка запроса";

            int startChangeset = (int)numStartChangeset.Value;
            int endChangeset = (int)numEndChangeset.Value;

            progressChangesets.Maximum = int.MaxValue;
            progressChangesets.Minimum = 0;

            progressChangesets.Minimum = startChangeset;
            progressChangesets.Value = startChangeset;
            progressChangesets.Maximum = endChangeset;

            progressChangesets.Step = 1;

            var task = Task.Factory.StartNew(() => this.TFSinterop.DownloadChangesetItems(startChangeset, endChangeset, txtResultFolderPath.Text));

            task.ContinueWith((t) => SetCaption(t.IsFaulted));
        }

        private void ChangesetChangedHandler(Object o, EventArgs e)
        {
            if (this.InvokeRequired) this.Invoke(changesetChangedDelegate); else changesetChangedDelegate();
        }

        private void SetCaption(bool faulted)
        {
            string result = string.Empty;

            if (faulted)
                result = "Завершено с ошибками.";
            else
                result = "Успешно завершено.";

            if (this.InvokeRequired) this.Invoke(setResultLabel, result); else setResultLabel(result);
        }

        private void Error(string message)
        {
            MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

    }
}
