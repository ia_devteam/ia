﻿using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;

using System;
using System.Collections.Generic;
using System.IO;

namespace TFSInterop
{
    partial class TFSserverInterop
    {
        private TfsTeamProjectCollection m_tpc;
        private VersionControlServer m_vcs;

        string[] _teamProjectsPatchs;
        
        //string _teamProjectPath;
        /// <summary>
        /// This field is for identification of concrete branch on server
        /// </summary>
        public string TeamProjectPath
        {
            get
            { return String.Join(";", _teamProjectsPatchs); }
            set
            {
                _teamProjectsPatchs = value.Split(';');
                for (int i = 0; i < _teamProjectsPatchs.Length; i++)
                    if (!_teamProjectsPatchs[i].EndsWith("/"))
                        _teamProjectsPatchs[i] += "/";
            }
        }

        public event EventHandler ChangesetProcessedEvent;

        public class ChangesetAcquiredEventArgs : EventArgs { public int ChangesCount { get; private set; } public ChangesetAcquiredEventArgs(int i) { ChangesCount = i; } }


        private VersionSpec CurrectChangesetVersionSpec;
        private Dictionary<int, HashSet<int>> downloadedItemsHash; //itemid, changesets ids
        private Dictionary<int, List<int>> itemsToDownloadHash; // -//-

        public TFSserverInterop(string teamProjectCollectionURI, string teamProjectPath)
            : this(new Uri(teamProjectCollectionURI), teamProjectPath)
        { }

        public TFSserverInterop(Uri teamProjectCollectionURI, string teamProjectPath)
        {
            m_tpc = new TfsTeamProjectCollection(teamProjectCollectionURI);
            m_vcs = m_tpc.GetService<VersionControlServer>();
            this.TeamProjectPath = teamProjectPath;

            ChangesetProcessedEvent += (o, i) => { };
        }

        public void DownloadChangesetItems(int startChangesetId, int endChangesetId, string destinationPath)
        {
            downloadedItemsHash = new Dictionary<int, HashSet<int>>();
            itemsToDownloadHash = new Dictionary<int, List<int>>();
            StreamWriter sw = new StreamWriter(Path.Combine(destinationPath, "Protocol.txt"));
            
            for (int i = startChangesetId; i <= endChangesetId; i++)
            {
                DownloadChangesetItems(i, destinationPath, sw);
                ChangesetProcessedEvent(null, null);
            }

            sw.Close();
            return;



            foreach (var kvp in itemsToDownloadHash)
            {
                foreach (var changesetId in kvp.Value)
                {
                    if (downloadedItemsHash.ContainsKey(kvp.Key) && downloadedItemsHash[kvp.Key].Contains(changesetId))
                        continue;

                    var item = m_vcs.GetItem(kvp.Key, changesetId, true);

                    int currentTeamProjectForItem = GetNumberOfTeamProjectForServerItem(item.ServerItem);

                    item.DownloadFile(PrepareFullPathForFile(destinationPath, item.ServerItem, changesetId, "", currentTeamProjectForItem));
                }
            }
        }

        /// <summary>
        /// Download files from typed changeset to destination path with rename: added second-level extension in format .CHANGESETID
        /// </summary>
        /// <param name="changesetId"></param>
        /// <param name="destinationPath"></param>
        private bool DownloadChangesetItems(int changesetId, string destinationPath, StreamWriter sw)
        {
            try
            {
                var testChange = m_vcs.GetChangeset(changesetId, true, true).Changes;
                if (testChange.Length == 0)
                    return false;

                var itemTest = testChange[0].Item.ServerItem;

                int currentTeamProjectForItem = GetNumberOfTeamProjectForServerItem(itemTest);

                if (currentTeamProjectForItem == -1 && (testChange.Length == 1 || GetNumberOfTeamProjectForServerItem(testChange[1].Item.ServerItem) == -1))
                    return false;

                var changes = m_vcs.GetChangeset(changesetId, true, true).Changes;

                foreach (Change ch in changes)
                {
                    sw.WriteLine(ch.Item.ServerItem + "\t" + ch.ChangeType.ToString() + "\t" + changesetId.ToString() + "\t" + m_vcs.GetChangeset(changesetId, true, true).CreationDate.ToString() + "\t" + m_vcs.GetChangeset(changesetId, true, true).Comment);
                }

                CurrectChangesetVersionSpec = VersionSpec.ParseSingleSpec(changesetId.ToString(), Environment.UserName);

                for (int i = 0; i < changes.Length; i++)
                {
                    var change = changes[i];
                    var item = change.Item;

                    currentTeamProjectForItem = GetNumberOfTeamProjectForServerItem(item.ServerItem);

                    if (currentTeamProjectForItem == -1)
                        return false;

                    if (item.ItemType != ItemType.File || //if item is directory or
                        (downloadedItemsHash.ContainsKey(item.ItemId) && downloadedItemsHash[item.ItemId].Contains(changesetId))) //if item already downloaded within this changesetId
                    {
                        continue;
                    }

                    string secondExtension = String.Empty;

                    if ((change.ChangeType & (ChangeType.Add | ChangeType.Branch)) != 0) //if added or if merged from branch
                        secondExtension = ".!added!";
                    else if ((change.ChangeType & ChangeType.Delete) != 0)
                    {
                        File.Create(PrepareFullPathForFile(destinationPath, item.ServerItem, changesetId, ".!deleted!", currentTeamProjectForItem)).Dispose();
                        continue;
                    }

                    item.DownloadFile(PrepareFullPathForFile(destinationPath, item.ServerItem, changesetId, secondExtension, currentTeamProjectForItem));

                    if (!downloadedItemsHash.ContainsKey(item.ItemId))
                        downloadedItemsHash.Add(item.ItemId, new HashSet<int>());
                    downloadedItemsHash[item.ItemId].Add(item.ChangesetId);

                    if (secondExtension.Length == 0)
                    {
                        if (!itemsToDownloadHash.ContainsKey(item.ItemId))
                            itemsToDownloadHash.Add(item.ItemId, new List<int>());
                        itemsToDownloadHash[item.ItemId].Add(changesetId - 1);
                    }
                }
            }
            catch (Microsoft.TeamFoundation.VersionControl.Client.ResourceAccessException) { }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="serverPath"></param>
        /// <param name="changesetId"></param>
        /// <param name="secondExtension"></param>
        /// <returns>Fully qualified File Path</returns>
        private string PrepareFullPathForFile(string root, string serverPath, int changesetId, string secondExtension, int teamProjectIndex)
        {
            string relativeDirName = Path.GetDirectoryName(serverPath.Substring(this._teamProjectsPatchs[teamProjectIndex].Length).Replace('/', '\\').TrimStart('\\'));
            string filename = Path.GetFileNameWithoutExtension(serverPath) + secondExtension + "." + changesetId.ToString() + Path.GetExtension(serverPath);
            string localRelativeName = Path.Combine(relativeDirName, filename);

            string fullName = Path.Combine(root, localRelativeName);
            string dir = Path.GetDirectoryName(fullName);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return fullName;
        }

        private int GetNumberOfTeamProjectForServerItem(string serverItem)
        {
            for (int i =0; i < _teamProjectsPatchs.Length; i++)
                if (serverItem.StartsWith(_teamProjectsPatchs[i]))
                    return i;
            return -1;
        }

    }

}
