﻿namespace TFSHistoryParser2
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numStartChangeset = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numEndChangeset = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtResultFolderPath = new System.Windows.Forms.TextBox();
            this.progressChangesets = new System.Windows.Forms.ProgressBar();
            this.btnStart = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_TFSCollectionURL = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_TeamProjectPath = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numStartChangeset)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numEndChangeset)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // numStartChangeset
            // 
            this.numStartChangeset.Location = new System.Drawing.Point(20, 19);
            this.numStartChangeset.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.numStartChangeset.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStartChangeset.Name = "numStartChangeset";
            this.numStartChangeset.Size = new System.Drawing.Size(120, 20);
            this.numStartChangeset.TabIndex = 0;
            this.numStartChangeset.Value = new decimal(new int[] {
            17721,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.numEndChangeset);
            this.groupBox1.Controls.Add(this.numStartChangeset);
            this.groupBox1.Location = new System.Drawing.Point(14, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(299, 48);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Диапазон ревизий (changeset)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(146, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "по";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "с";
            // 
            // numEndChangeset
            // 
            this.numEndChangeset.Location = new System.Drawing.Point(168, 19);
            this.numEndChangeset.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.numEndChangeset.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numEndChangeset.Name = "numEndChangeset";
            this.numEndChangeset.Size = new System.Drawing.Size(120, 20);
            this.numEndChangeset.TabIndex = 1;
            this.numEndChangeset.Value = new decimal(new int[] {
            19045,
            0,
            0,
            0});
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtResultFolderPath);
            this.groupBox4.Location = new System.Drawing.Point(14, 66);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(299, 44);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Путь до каталога для результатов выгрузки";
            // 
            // txtResultFolderPath
            // 
            this.txtResultFolderPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResultFolderPath.Location = new System.Drawing.Point(3, 16);
            this.txtResultFolderPath.Name = "txtResultFolderPath";
            this.txtResultFolderPath.Size = new System.Drawing.Size(293, 20);
            this.txtResultFolderPath.TabIndex = 0;
            this.txtResultFolderPath.Text = "d:\\1\\";
            // 
            // progressChangesets
            // 
            this.progressChangesets.Location = new System.Drawing.Point(14, 264);
            this.progressChangesets.Name = "progressChangesets";
            this.progressChangesets.Size = new System.Drawing.Size(299, 23);
            this.progressChangesets.Step = 1;
            this.progressChangesets.TabIndex = 4;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(17, 222);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(299, 23);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "Выполнить выгрузку изменённых файлов";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(101, 248);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Прогресс по ревизиям";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_TFSCollectionURL);
            this.groupBox2.Location = new System.Drawing.Point(14, 116);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(299, 44);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "TFS Collection URL";
            // 
            // txt_TFSCollectionURL
            // 
            this.txt_TFSCollectionURL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_TFSCollectionURL.Location = new System.Drawing.Point(3, 16);
            this.txt_TFSCollectionURL.Name = "txt_TFSCollectionURL";
            this.txt_TFSCollectionURL.Size = new System.Drawing.Size(293, 20);
            this.txt_TFSCollectionURL.TabIndex = 0;
            this.txt_TFSCollectionURL.Text = "http://ztfs:8080/tfs";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txt_TeamProjectPath);
            this.groupBox3.Location = new System.Drawing.Point(14, 166);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(299, 44);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Team Project Path";
            // 
            // txt_TeamProjectPath
            // 
            this.txt_TeamProjectPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_TeamProjectPath.Location = new System.Drawing.Point(3, 16);
            this.txt_TeamProjectPath.Name = "txt_TeamProjectPath";
            this.txt_TeamProjectPath.Size = new System.Drawing.Size(293, 20);
            this.txt_TeamProjectPath.TabIndex = 0;
            this.txt_TeamProjectPath.Text = "$/FCOD/branches/F.4.3";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 297);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.progressChangesets);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainWindow";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numStartChangeset)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numEndChangeset)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numStartChangeset;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numEndChangeset;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtResultFolderPath;
        private System.Windows.Forms.ProgressBar progressChangesets;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt_TFSCollectionURL;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txt_TeamProjectPath;
    }
}

