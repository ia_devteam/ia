using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;
using TestUtils;
using System.Text;
using System.Collections.Generic;

using System;
using System.IO;
using IA.Plugins.Importers.XR_F_IfDef_Importer;
using IOController;


namespace Test_XR_F_IfDef_Importer
{
    /// <summary>
    /// ����� ������� "������ ���������� ����������� �������� ����-� (XR_F_IfDef)"
    /// </summary>
    [TestClass]
    public class XR_F_IfDef_ImporterTest
    {
        /// <summary>
        /// Summary description for MainTest
        /// </summary>
        [TestClass]
        public class Test_XR_F_IfDef_Importer
        {
            /// <summary>
            /// �������� ���� �� ���������� � ��������� ��������
            /// </summary>
            private string sourceDirectoryPathPostfix;

            /// <summary>
            /// ������� ���� ��� ����
            /// </summary>
            public string sourceDirectoryPathPrefixForAIST;

            /// <summary>
            /// ���� �� ������
            /// </summary>
            private string xrefReportPathPostfix;

            /// <summary>
            /// ���� �� ����������, ���������� ��������� ���� ���������
            /// </summary>
            private string dumpSamplesDirectoryPostfix;

            /// <summary>
            /// ���� �� ���������� � ��������� �������� (� ������ ������)
            /// </summary>
            private string sourceDirectoryPathInReport;

            /// <summary>
            /// ���� � ������ Understand
            /// </summary>
            private string understandReportsPath;

            /// <summary>
            /// ��������� ����������� ���������
            /// </summary>
            private TestUtilsClass.LogSimpleListenerForTests listener;

            /// <summary>
            /// ������� ������������� ���������
            /// </summary>
            Dictionary<string, IA.Monitor.Log.Message.enType> messagesToControl;

            /// <summary>
            /// ��������� �������������� ������������ ��� ������� �����
            /// </summary>
            private TestUtilsClass testUtils;

            private TestContext testContextInstance;
            /// <summary>
            /// ���������� � ������� �����
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            /// <summary>
            /// ������ ��, ��� ���������� ������� ����� �������� ������� �����
            /// </summary>
            [TestInitialize]
            public void EachTest_Initialize()
            {
                //������������� �������������� ������������ ��� ������� �����
                testUtils = new TestUtilsClass(testContextInstance.TestName);
                listener = null;
                messagesToControl = null;
            }

            /// <summary>
            /// ���������: ����������� OpenSource C ������ zlib, �������� ������������ ������� �������� � �������
            /// </summary>
            [TestMethod]
            public void XR_F_IfDef_Importer_001()
            {
                //����� ��������� �����
                this.sourceDirectoryPathPostfix = "Sources\\XR_F_IfDef_Importer\\001\\zlib";
                this.xrefReportPathPostfix = "XR_F_IfDef_Importer\\Reports\\001\\_1_Din_.Rep";
                this.dumpSamplesDirectoryPostfix = "XR_F_IfDef_Importer\\Dump\\001";
                this.sourceDirectoryPathInReport = "D:\\Akok\\App\\Projects\\Work\\TestsAist\\002Source\\zlib";
                this.understandReportsPath = "XR_F_IfDef_Importer\\Understand\\001";

                //��������� ������
                RunPlugin();
            }

            /// <summary>
            /// ���������: ����������� OpenSource C ���� ���� crc32.c, �������� ������������ ������� �������� � �������
            /// </summary>
            [TestMethod]
            public void XR_F_IfDef_Importer_002()
            {
                //����� ��������� �����
                this.sourceDirectoryPathPostfix = "Sources\\XR_F_IfDef_Importer\\002";
                this.xrefReportPathPostfix = "XR_F_IfDef_Importer\\Reports\\002\\_1_Din_.Rep";
                this.dumpSamplesDirectoryPostfix = "XR_F_IfDef_Importer\\Dump\\002";
                this.sourceDirectoryPathInReport = @"D:\Akok\App\TIA\IA\Tests\Materials\Sources\XR_F_IfDef_Importer\002";
                this.understandReportsPath = "XR_F_IfDef_Importer\\Understand\\002";
                //��������� ������
                RunPlugin();
            }

            /// <summary>
            /// ���������: ����������� OpenSource C++ ���� ���� easing.cpp, �������� ������������ ������� �������� � �������
            /// </summary>
            [TestMethod]
            public void XR_F_IfDef_Importer_003()
            {
                //����� ��������� �����
                this.sourceDirectoryPathPostfix = "Sources\\XR_F_IfDef_Importer\\003";
                this.xrefReportPathPostfix = "XR_F_IfDef_Importer\\Reports\\003\\_1_Din_.Rep";
                this.dumpSamplesDirectoryPostfix = "XR_F_IfDef_Importer\\Dump\\003";
                this.sourceDirectoryPathInReport = @"D:\Akok\App\TIA\IA\Tests\Materials\Sources\XR_F_IfDef_Importer\003";
                this.understandReportsPath = "XR_F_IfDef_Importer\\Understand\\003";

                //��������� ������
                RunPlugin();
            }

            /// <summary>
            /// ���������: ����������� OpenSource C++ ���� ���� easing.cpp, ����������� ���� ������ ������� �������� ���� => 
            /// � ��������� ��� ��������
            /// </summary>
            [TestMethod]
            public void XR_F_IfDef_Importer_004()
            {
                //����� ��������� �����
                this.sourceDirectoryPathPostfix = "Sources\\XR_F_IfDef_Importer\\003";
                this.xrefReportPathPostfix = "XR_F_IfDef_Importer\\Reports\\004\\_1_Din_.Rep";
                this.dumpSamplesDirectoryPostfix = "XR_F_IfDef_Importer\\Dump\\004";
                this.sourceDirectoryPathInReport = @"D:\Akok\App\TIA\IA\Tests\Materials\Sources\XR_F_IfDef_Importer\003";
                this.understandReportsPath = "XR_F_IfDef_Importer\\Understand\\003";
                //����� ��������� ��� ������������
                this.messagesToControl = new Dictionary<string, IA.Monitor.Log.Message.enType>()
                { 
                    {xrefReportPathPostfix, IA.Monitor.Log.Message.enType.WARNING}
                };

                //��������� ������
                RunPlugin();
            }

            /// <summary>
            /// ���������: ����������� OpenSource C++ ���� ���� easing.cpp, ���� ������ � ������� �������� ���� �� ������ ���������� � ����������� �������� => 
            /// � ��������� ��� ��������
            /// </summary>
            [TestMethod]
            public void XR_F_IfDef_Importer_005()
            {
                //����� ��������� �����
                this.sourceDirectoryPathPostfix = "Sources\\XR_F_IfDef_Importer\\002";
                this.xrefReportPathPostfix = "XR_F_IfDef_Importer\\Reports\\003\\_1_Din_.Rep";
                this.dumpSamplesDirectoryPostfix = "XR_F_IfDef_Importer\\Dump\\004";
                this.sourceDirectoryPathInReport = @"D:\Akok\App\TIA\IA\Tests\Materials\Sources\XR_F_IfDef_Importer\002";
                this.sourceDirectoryPathPrefixForAIST = @"D:\Akok\App\TIA\IA\Tests\Materials\Sources\XR_F_IfDef_Importer\002";
                this.understandReportsPath = "XR_F_IfDef_Importer\\Understand\\002";
                //����� ��������� ��� ������������
                this.messagesToControl = new Dictionary<string, IA.Monitor.Log.Message.enType>(1)
                { 
                    {@"D:\Akok\App\TIA\IA\Tests\Materials\Sources\XR_F_IfDef_Importer\003\easing.cpp", IA.Monitor.Log.Message.enType.ERROR}
                };

                //��������� ������
                RunPlugin();
            }

            /// <summary>
            /// ���������: ����������� OpenSource C++ ���� ���� easing.cpp, ���� ������ � ������� �������� ���� � ������ ������� => 
            /// � ��������� ��� ��������
            /// </summary>
            [TestMethod]
            public void XR_F_IfDef_Importer_006()
            {
                //����� ��������� �����
                this.sourceDirectoryPathPostfix = "Sources\\XR_F_IfDef_Importer\\002";
                this.xrefReportPathPostfix = "XR_F_IfDef_Importer\\Reports\\006\\_1_KartaDin.Map";
                this.dumpSamplesDirectoryPostfix = "XR_F_IfDef_Importer\\Dump\\004";
                this.sourceDirectoryPathInReport = @"D:\Akok\App\TIA\IA\Tests\Materials\Sources\XR_F_IfDef_Importer\002";
                this.understandReportsPath = "XR_F_IfDef_Importer\\Understand\\002";
                try
                {
                    //��������� ������
                    RunPlugin();
                }
                catch (Exception e)
                {
                    //����� ��������� ��� ������������
                    this.messagesToControl = new Dictionary<string, IA.Monitor.Log.Message.enType>(2)
                                        { 
                                            {"��������� ����������������� �������: ", IA.Monitor.Log.Message.enType.ERROR},
                                            {xrefReportPathPostfix, IA.Monitor.Log.Message.enType.ERROR}
                                        };

                    foreach (var message in messagesToControl)
                        Assert.IsTrue(e.Message.Contains(message.Key), "��� �������� �� �������� ��������� � ������ <" + message + ">");

                    //������� (�����) ����� �����, �.�. ���������� ��� �� ������!
                    DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
                }
            }

            /// <summary>
            /// ���������: ����������� OpenSource C++ ���� ���� easing.cpp, ���� ������ � ������� �������� ���� �� ������ ���������� � ��������� �������� => 
            /// � ��������� ��� ��������
            /// </summary>
            [TestMethod]
            public void XR_F_IfDef_Importer_007()
            {
                //����� ��������� �����
                this.sourceDirectoryPathPostfix = "Sources\\XR_F_IfDef_Importer\\002";
                this.xrefReportPathPostfix = "XR_F_IfDef_Importer\\Reports\\003\\_1_Din_.Rep";
                this.dumpSamplesDirectoryPostfix = "XR_F_IfDef_Importer\\Dump\\004";
                this.sourceDirectoryPathInReport = @"D:\Akok\App\TIA\IA\Tests\Materials\Sources\XR_F_IfDef_Importer\002";
                this.sourceDirectoryPathPrefixForAIST = @"D:\Akok\App\TIA\IA\Tests\Materials\Sources\XR_F_IfDef_Importer\003";
                this.understandReportsPath = "XR_F_IfDef_Importer\\Understand\\002";
                //����� ��������� ��� ������������
                this.messagesToControl = new Dictionary<string, IA.Monitor.Log.Message.enType>(1)
                { 
                    {@"���� <easing.cpp> �� ������ � ���������.", IA.Monitor.Log.Message.enType.ERROR}
                };

                //��������� ������
                RunPlugin();
            }

            /// <summary>
            /// ������� ����� ���� ������. ������ ������� � ��������� ��� ����������.
            /// </summary>
            private void RunPlugin()
            {
                //�������������� ��������� ���������
                this.listener = new TestUtils.TestUtilsClass.LogSimpleListenerForTests();

                testUtils.RunTest(
                    //sourcePrefixPart
                    string.Empty,

                    //plugin
                    new Importer_XR_F_IfDef(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    null,

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        //��������� ������ "������ ������ ������"
                        TestUtilsClass.Run_FillFileList(storage, System.IO.Path.Combine(testMaterialsPath, sourceDirectoryPathPostfix));
                        
                        //��������� ������ "����� ���������� Underdtand"
                        TestUtilsClass.Run_UnderstandImporter(storage, System.IO.Path.Combine(testMaterialsPath, understandReportsPath), sourceDirectoryPathInReport);

                        //����� ��������� �������
                        Store.Table.IBufferWriter settingsBuffer = Store.Table.WriterPool.Get();
                        string settings = System.IO.Path.Combine(testMaterialsPath, xrefReportPathPostfix) + "@" + sourceDirectoryPathPrefixForAIST;
                        settingsBuffer.Add(settings);

                        //�������� ��������� � ���������
                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.XR_F_IFDEF_IMPORTER, settingsBuffer);
                    },

                    //checkStorage
                    (storage, testMaterialsPath) =>
                    {
                        //������ ���� ��������� � �������� ���� �� ����������, ���� �� ��� ���������
                        string dumpDirectoryPath = SensorsDump(storage);

                        //�������� ���� �� ����������, ���������� ��������� ����
                        string dumpSamplesDirectoryPath = Path.Combine(testMaterialsPath, dumpSamplesDirectoryPostfix);

                        //���������� ������� ���� � ���������
                        CheckDump(dumpDirectoryPath, dumpSamplesDirectoryPath);

                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) => true
                    ,

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    null,

                    //checkStorageAfterRerun
                    null,

                    //checkReportAfterRerun
                    null
                   );

                //��������� �������������� ���������
                if (messagesToControl != null)
                    foreach (var message in messagesToControl)
                        Assert.IsTrue(listener.CheckMessage(message.Key, message.Value), "��� �������� �� �������� ��������� � ������ <" + message.Key + ">");
            }

            /// <summary>
            /// ���� �������� �� ���������
            /// </summary>
            /// <param name="storage">���������. �� ����� ���� null.</param>
            /// <returns>���� �� ��������, ���� ��� ���������� ���� ���������</returns>
            private string SensorsDump(Storage storage)
            {
                //��������� ���� �� ����������, ���� ����� ��������� ���� ���������
                string dumpDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Dump");

                //������ ����������, ���� � �� ����
                if (!Directory.Exists(dumpDirectoryPath))
                    Directory.CreateDirectory(dumpDirectoryPath);

                StringBuilder stringBuilder = new StringBuilder();
                foreach (Sensor sensor in storage.sensors.EnumerateSensors())
                    stringBuilder.AppendLine(string.Format("{0} - {1}", sensor.ID, sensor.GetFunction().FullNameForReports));

                File.WriteAllText(Path.Combine(dumpDirectoryPath, "Sensors.txt"), stringBuilder.ToString());

                return dumpDirectoryPath;
            }

            /// <summary>
            /// �������� ����� ���������
            /// </summary>
            /// <param name="dumpDirectoryPath">���� �� �������� ����� � ���������. �� ����� ���� ������.</param>
            /// <param name="dumpSamplesDirectoryPath">���� �� �������� � ��������� ������. �� ����� ���� ������.</param>
            private void CheckDump(string dumpDirectoryPath, string dumpSamplesDirectoryPath)
            {
                bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(dumpDirectoryPath, dumpSamplesDirectoryPath);

                Assert.IsTrue(isEqual, "���� �������� ��������� �� ��������� � ����������.");
            }
        }
    }
}
