﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Linq;

using Store;
using Store.Table;

namespace IA.Plugins.Importers.XR_F_IfDef_Importer
{
    using AistSubsystem.XR_F_IfDef;

    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.XR_F_IFDEF_IMPORTER;

        /// <summary>
        /// Имя плагина
        /// </summary>
        internal const string Name = "Импорт протоколов расстановки датчиков АИСТ-С (XR_F_IfDef)";

        #region Settings

        private static List<XR_F_IfDefProtocol> protocolListlocal;
        /// <summary>
        /// Путь к протоколу
        /// </summary>
        internal static List<XR_F_IfDefProtocol> protocolList
        {
            get { return protocolListlocal; }
            set
            {
                protocolListlocal = value;
                Properties.Settings.Default.ReportFiles = PackProjectsToString(protocolListlocal);
                Properties.Settings.Default.Save();
            }
        }

        static public List<XR_F_IfDefProtocol> ParseStringForProjects(string input)
        {
            var result = new List<XR_F_IfDefProtocol>();
            if (!string.IsNullOrWhiteSpace(input))
            {
                if (input.Contains("@"))
                    foreach (var line in input.Split(';'))
                    {
                        string[] project = line.Split('@');
                        if (File.Exists(project[0]))
                        {
                            var protocol = new XR_F_IfDefProtocol(project[0]);
                            if (project.Length > 1 && !String.IsNullOrWhiteSpace(project[1]))
                                protocol.CorrectedFilesInReportPrefix = project[1];
                            result.Add(protocol);
                        }
                        else
                            IA.Monitor.Log.Warning(Name, "Файл протокола <" + project[0] + "> не найден!");
                    }
                else
                    IA.Monitor.Log.Warning(Name, "Строка настроек содержит неверную информацию!");
            }            

            return result;
        }

        static public string PackProjectsToString(List<XR_F_IfDefProtocol> input)
        {
            string result = String.Empty;

            foreach (var project in input)
            {
                result += String.Format("{0}@{1}", project.ReportPath, project.CorrectedFilesInReportPrefix);
            }

            return result;
        }

        #endregion
    }

    /// <summary>
    /// Плагин импорта протоколов утилиты расстановки датчиков XR_F_IfDef из состава АИСТ-С
    /// </summary>
    public class Importer_XR_F_IfDef : IA.Plugin.Interface
    {
        /// <summary>
        /// Текущее хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Текущие настройки
        /// </summary>
        Settings settings;

        #region PluginInterface Members

        /// <summary>
        /// Возможности плагина.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.RERUN;
            }
        }

        /// <summary>
        /// Приказ остановить работу.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        /// <summary>
        /// Сотворить отчёты.
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
        }

        /// <summary>
        /// Идентификатор плагина.
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary>
        /// Инициализация плагина.
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.protocolList = PluginSettings.ParseStringForProjects(Properties.Settings.Default.ReportFiles);
        }

        /// <summary>
        /// Загрузка настроек из хранилища.
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.protocolList = PluginSettings.ParseStringForProjects(settingsBuffer.GetString());
        }

        /// <summary>
        /// Установка настроек для простого режима.
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //FIXME - Данный плагин пока не используется в простом режиме
            //PluginSettings.protocolList = PluginSettings.ParseStringForProjects(settingsString);

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Запуск плагина на выполнение своей задачи.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            foreach (var protocol in PluginSettings.protocolList)
            {
                Analyzer analyzer = new Analyzer();
                analyzer.Run(storage, protocol.ReportPath, protocol.CorrectedFilesInReportPrefix);
            }
        }

        /// <summary>
        /// Сохранение результатов плагина.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary>
        /// Сохранение настроек в хранилище.
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.ReportFiles = PluginSettings.PackProjectsToString(PluginSettings.protocolList);
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(Properties.Settings.Default.ReportFiles);
        }

        /// <summary>
        /// Окно результатов.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Окно настроек в обычном режиме.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return settings = new Settings();
            }
        }

        /// <summary>
        /// Окно настроек в простом режиме.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Проверка правильности настроек плагина.
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (PluginSettings.protocolList.Any(p => !p.IsProtocolFormatAccepted ||
                !IOController.FileController.IsExists(p.ReportPath)))
            {
                message = "Файлы протоколов не существуют: "
                    + String.Join(";", PluginSettings.protocolList.Where(p => !IOController.FileController.IsExists(p.ReportPath)))
                    + "."
                    + Environment.NewLine;
                message = "Протоколы неподдерживаемого формата: "
                    + String.Join(";", PluginSettings.protocolList.Where(p => !p.IsProtocolFormatAccepted))
                    + ".";
                return false;
            }

            return true;
        }

        /// <summary>
        /// Список задач для отображения пользователю в овремя выполнения.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Список задач для отображения пользователю во время генерации отчётов.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Самоназвание плагина.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get { return PluginSettings.Name; }
        }

        /// <summary>
        /// Окно выполнения работы плагина.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        #endregion

    }
}
