using System;
using System.Collections.Generic;
using System.Text;

using Store;

namespace IA.Plugins.Importers.XR_F_IfDef_Importer
{
    class Analyzer
    {
        Storage storage;
        Functions functions;
        Files files;

        static List<string> badFunctions = new List<string> { "+++", "&&", "||", ">=", "<=", "==", "!=", "+=", "|=", "&=", "=", "/=", "[]", "++", "--", "!", "+", "", "-=", "/", "\\" };
        static List<string> badPartsOfFunctionNames = new List<string> { "(", ")", "," };

        internal void Run(Storage storage, string Path, string pathToRemove)
        {
            this.storage = storage;
            this.functions = storage.functions;
            this.files = storage.files;

            using (System.IO.StreamReader reader = new System.IO.StreamReader(Path, Encoding.GetEncoding(1251)))
            {
                string currentLine;
                IFile file = null;
                IFunction function = null;

                bool errorFileState = false;

                while ((currentLine = reader.ReadLine()) != null)
                {
                    if (String.IsNullOrWhiteSpace(currentLine)) //���������� ������ ������
                        continue;

                    if (!(currentLine[0] == ' ' || currentLine[0] == '=') && currentLine.Contains(@":\")) //������ �������� ��� �����
                    {
                        if (!currentLine.StartsWith(pathToRemove, StringComparison.OrdinalIgnoreCase))
                        {
                            IA.Monitor.Log.Error(PluginSettings.Name, String.Format("������ <{0}> �� �������� ���������� ���� �� ����� �������� ������. ����������� � ���� ������� ����� ���������.", currentLine));
                            errorFileState = true;
                            continue;
                        }
                        string realFileRelativeName = currentLine.Remove(0, pathToRemove.Length).Trim('\\');  //DONE Kokin - ��� ����� ��� \
                        file = files.Find(realFileRelativeName);
                        if (file == null) //���� �� ������. ������, � ��� ������� ����������� � ������. ������.
                        {
                            IA.Monitor.Log.Error(PluginSettings.Name, String.Format("���� <{0}> �� ������ � ���������. ����������� � ���� ������� ����� ���������.", realFileRelativeName));
                            errorFileState = true;
                        }
                        else
                        {
                            errorFileState = false;
                            function = null;
                        }
                        continue;
                    }

                    if (errorFileState)
                        continue;

                    //��� �������� �������
                    uint senID;
                    Store.Kind senType;
                    string FuncName;
                    uint stringNum;

                    //��������� ������
                    if (!ParseSenLine(currentLine, out senType, out senID, out FuncName, out stringNum))
                        continue;

                    bool isGoodFuncName = true;
                    if (FuncName != null)
                        foreach (string part in badPartsOfFunctionNames)
                            if (FuncName.Contains(part))
                            {
                                isGoodFuncName = false;
                                break;
                            }

                    if (FuncName != null && !badFunctions.Contains(FuncName) && isGoodFuncName) //������ �������� ��� �������. ���� ��.
                    {
                        //���������� ��� �����
                        while (FuncName.StartsWith(":"))
                            FuncName = FuncName.Substring(1);

                        //������ �����
                        function = GetFunctionByFile(file, FuncName);

                        if (function == null)
                            function = GetFunctionDirectly(FuncName);

                        //��������� - �������� ��� �����
                        if (function == null)
                        {
                            uint minVal = uint.MaxValue;
                            IFunction minFunction = null;
                            foreach (IFunction f in file.FunctionsDefinedInTheFile())
                            {
                                Location loc = f.Definition();
                                if (loc.GetFile() == file && loc.GetLine() <= stringNum && stringNum - loc.GetLine() < minVal)
                                {
                                    minVal = (uint)stringNum - (uint)loc.GetLine();
                                    minFunction = f;
                                }
                            }
                            if (minFunction != null)
                                function = minFunction;
                        }

                        if (function == null) //������� �� ������ ������ �� �������
                        {
                            IA.Monitor.Log.Error(PluginSettings.Name, "������� " + FuncName + " �� ����� " + file.FullFileName_Original + " � ��������� �� ����������������. ��������� ������� ����� ���� ���������.");
                            IFunction[] search = functions.GetFunction("UnknownFunctionRNT");
                            if (search.Length == 0)
                            {
                                function = functions.AddFunction();
                                function.Name = "UnknownFunctionRNT";
                                function.AddDefinition(file, 1);
                            }
                            else
                            {
                                function = search[0];

                                Location loc = function.Definition();
                                if (!(loc != null && loc.GetFile() == file))
                                    function.AddDefinition(file, 1);
                            }
                        }
                    }

                    //��������� ������
                    if (function == null)
                    { //���� �� ��� ����� ������� ��� ���������� ������ ������: ���� ������� � �������� ����������� ������ ������� � �������� � ��������.
                        IA.Monitor.Log.Error(PluginSettings.Name, "������ " + senID + " ��������������, ��� ��� ���������� ��� ������� �� ���������� � ���������");
                    }
                    else
                        function.AddSensor(senID, senType);
                }
            }
        }

        private bool ParseSenLine(string str, out Store.Kind senType, out uint senID, out string FuncName, out uint stringNum)
        {
            //������ ���
            int idx = 1;
            switch (str[idx])
            {
                case '1':
                    senType = Store.Kind.FINAL;
                    break;
                case '2':
                    senType = Store.Kind.FINAL;
                    break;
                case '4':
                    senType = Store.Kind.FINAL;
                    break;
                case '5':
                    senType = Store.Kind.START;
                    break;
                case '6':
                    senType = Store.Kind.INTERNAL;
                    break;
                case '7':
                    senType = Store.Kind.INTERNAL;
                    break;
                default:
                    IA.Monitor.Log.Error(PluginSettings.Name, "������ " + str + " ����� ����������� ���. �� ��������������.");
                    senType = Store.Kind.INTERNAL;
                    senID = 0;
                    FuncName = null;
                    stringNum = 0;
                    return false;
            }
            idx++;

            //���������� ����� �����
            skip_blanks(str, ref idx);
            skip_text(str, ref idx);

            //������ ������������� �������
            skip_blanks(str, ref idx);
            string ID;
            read_text(str, ref idx, out ID);
            senID = Convert.ToUInt32(ID);

            //���������� ����� �������
            skip_blanks(str, ref idx);
            skip_text(str, ref idx);

            //���������� ��������� �������� ���� �������
            skip_blanks(str, ref idx);
            skip_text(str, ref idx);

            //���������� ����� ������
            skip_blanks(str, ref idx);
            string strNum;
            read_text(str, ref idx, out strNum);
            stringNum = Convert.ToUInt32(strNum);
            skip_text(str, ref idx);

            //���������� ����� �������
            skip_blanks(str, ref idx);
            skip_text(str, ref idx);

            //���������� ��� �������
            skip_blanks(str, ref idx);
            skip_text(str, ref idx);

            //���������� ��������� �������
            skip_blanks(str, ref idx);
            skip_text(str, ref idx);

            //������ ��� �������
            skip_blanks(str, ref idx);
            if (str.Length > idx)
            {
                read_func_name(str, ref idx, out FuncName);
                if (FuncName[0] == '*')
                    FuncName = FuncName.Substring(1);
            }
            else
                FuncName = null;

            return true;
        }

        private void read_func_name(string str, ref int idx, out string res)
        {
            int lastIdx = idx;

            res = str.Substring(lastIdx, str.Length - lastIdx);
        }

        private void read_text(string str, ref int idx, out string res)
        {
            int lastIdx = idx;
            while (str.Length > idx && str[idx] != ' ')
                idx++;

            res = str.Substring(lastIdx, idx - lastIdx);
        }

        private void skip_text(string str, ref int idx)
        {
            while (str.Length > idx && str[idx] != ' ')
                idx++;
        }

        private void skip_blanks(string str, ref int idx)
        {
            while (str.Length > idx && str[idx] == ' ')
                idx++;
        }

        //���� ������� � ��������� �� ��������� �����
        private IFunction GetFunctionByFile(IFile file, string FuncName)
        {
            if (file == null)
                return null;

            IFunction[] foundedFunctions;
            if (FuncName.Contains("::")) //���� �� ������� �����, ���� �� �����, �������� ����� ���� �� �� ��������� ����� �����
            {
                string[] funcFullName = FuncName.Replace("::", ".").Split('.');
                foundedFunctions = functions.FindFunction(funcFullName);

                foreach (IFunction func in foundedFunctions)
                {
                    Store.Location location = func.Definition();
                    if (location != null && location.GetFileID() == file.Id)
                        return func;
                }

                foundedFunctions = functions.GetFunction(funcFullName[funcFullName.Length - 1]);
            }
            else
                foundedFunctions = functions.GetFunction(FuncName);

            foreach (IFunction func in foundedFunctions)
            {
                Store.Location location = func.Definition();
                if (location != null && location.GetFileID() == file.Id)
                    return func;
            }


            return null;
        }

        private IFunction GetFunctionDirectly(string FuncName)
        {
            IFunction[] foundedFunctions;
            if (FuncName.Contains("::")) //���� �� ������� �����, ���� �� �����, �������� ����� ���� �� �� ��������� ����� �����
            {
                string[] funcFullName = FuncName.Replace("::", ".").Split('.');
                foundedFunctions = functions.FindFunction(funcFullName);

                //���� ����� ���-�� � ���� ������� ��� ����������� �� �������� � ����� ������ ������ ���������
                if (foundedFunctions.Length != 0)
                {
                    foreach (IFunction func in foundedFunctions)
                        if (func.Definition() != null)
                            return func;
                    return foundedFunctions[0];
                }

                foundedFunctions = functions.GetFunction(funcFullName[funcFullName.Length - 1]);
            }
            else
                foundedFunctions = functions.GetFunction(FuncName);

            if (foundedFunctions.Length != 0)
            {
                foreach (IFunction func in foundedFunctions)
                    if (func.Definition() != null)
                        return func;
                return foundedFunctions[0];
            }

            return null;
        }
    }
}
