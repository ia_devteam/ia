using System.Windows.Forms;
using System.Linq;

namespace IA.Plugins.Importers.XR_F_IfDef_Importer
{
    /// <summary>
    /// �������� ����� ��������
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// �����������
        /// </summary>
        internal Settings()
        {
            InitializeComponent();
            foreach (var protocol in PluginSettings.protocolList)
                report_select_with_path_correction1.protocolList.Add(protocol);
        }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        public void Save()
        {
            PluginSettings.protocolList = report_select_with_path_correction1.protocolList.ToList();
        }

    }
}
