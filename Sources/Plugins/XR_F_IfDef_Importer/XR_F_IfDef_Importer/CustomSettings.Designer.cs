namespace IA.Plugins.Importers.XR_F_IfDef_Importer
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.report_select_with_path_correction1 = new IA.AistSubsystem.XR_F_IfDef.Report_select_with_path_correction();
            this.SuspendLayout();
            // 
            // report_select_with_path_correction1
            // 
            this.report_select_with_path_correction1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.report_select_with_path_correction1.Location = new System.Drawing.Point(0, 0);
            this.report_select_with_path_correction1.MinimumSize = new System.Drawing.Size(405, 260);
            this.report_select_with_path_correction1.Name = "report_select_with_path_correction1";
            this.report_select_with_path_correction1.Size = new System.Drawing.Size(501, 360);
            this.report_select_with_path_correction1.TabIndex = 0;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.report_select_with_path_correction1);
            this.MinimumSize = new System.Drawing.Size(415, 360);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(501, 360);
            this.ResumeLayout(false);

        }

        #endregion

        private AistSubsystem.XR_F_IfDef.Report_select_with_path_correction report_select_with_path_correction1;

    }
}
