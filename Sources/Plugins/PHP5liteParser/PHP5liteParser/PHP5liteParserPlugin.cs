﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

using IA.Plugin;
using Store;
using Store.Table;

using IA.Extensions;
using IA.Plugins.Parsers.CommonUtils;
using FileOperations;

namespace IA.Plugins.Parsers.PHP5liteParser
{
    internal static class PluginSettings
    {
        public const uint ID = Store.Const.PluginIdentifiers.PHP5_LITE_PARSER;
        public const string Name = "Парсер PHP5-lite";
       
        /// <summary>
        /// Задачи плагинов
        /// </summary>
        internal enum Tasks
        {
            [Description("Создание AST")]
            FILES_PROCESSING,
            [Description("Парсинг AST файлов")]
            ASTFILES_PARSING,
            [Description("Расстановка датчиков")]
            SENSOR_PLACEMENT,
            [Description("Запись лабораторных файлов")]
            LABS_WRITER
        };

        #region Settings
        /// <summary>
        /// 2-й уровень вставки датчиков
        /// </summary>
        internal static bool IsLevel2;
        /// <summary>
        /// 3-й уровень вставки датчиков
        /// </summary>
        internal static bool IsLevel3;

        /// <summary>
        /// Не вставлять датчики
        /// </summary>
        internal static bool IsNoSensors;
        /// <summary>
        /// Текст функции датчика для 2-го уровня
        /// </summary>
        internal static string SensorFunctionText_Level2;
        /// <summary>
        /// Текст датчика для 2-го уровня
        /// </summary>
        internal static string SensorText_Level2;
        /// <summary>
        /// Текст функции датчика для 3-го уровня
        /// </summary>
        internal static string SensorFunctionText_Level3;
        /// <summary>
        /// Текст датчика для 3-го уровня
        /// </summary>
        internal static string SensorText_Level3;
        /// <summary>
        /// Номер первого датчика
        /// </summary>
		internal static ulong FirstSensorNumber;
		
		/// <summary>
        /// Пропуск ошибок
        /// </summary>
        internal static bool IsSkipErrors;

        /// <summary>
        /// Номер материала, чтобы формировать имя файла с датчиками
        /// </summary>
        internal static int MaterialNum;

        #endregion

    }

    public class PHP5liteParser : IA.Plugin.Interface
    {

        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        /// <summary>
        /// Настройки
        /// </summary>
        IA.Plugin.Settings settings;

        public runEnv crEnv = new runEnv();

        /// <summary>
        /// для хранение настроек времени выполнения, которые будут перады
        /// в ParserCore
        /// </summary>
        public class runEnv
        {
            /// <summary>
            /// каталог для ast файлов
            /// </summary>
            public string pathPHPast = string.Empty;
            
            /// <summary>
            /// каталог до очищенных исходников
            /// </summary>
            public string pathCleared = string.Empty;
            
            /// <summary>
            /// каталог куда формировать лабораторные исходники
            /// </summary>
            public string pathLAB = string.Empty;
  
            /// <summary>
            /// ПРЕФИКС до каталога с ast файлами
            /// </summary>
            public string prefAST = string.Empty;
  
            /// <summary>
            /// массив, содержащий аст файлы
            /// <summary>
            public string[] arrayAST;

            /// <summary>
            /// массив, содержащий очищенные файлы исходников
            /// <summary>
            public string[] arraySRC;

            /// <summary>
            /// ПРЕФИКС до каталога с очищенными файлами
            /// </summary>
            public string prefSRC = string.Empty;

            /// <summary>
            /// Число файлов *.php на обработку парсеру
            /// </summary>
            public long NumPHP = 0;
            /// <summary>
            /// При записи текстовых датчиков в каталог '/var/tmp/'
            /// </summary>
            public string SensPrint =
                " $hFNTX = fopen(\"/var/tmp/FNTXSens_&.txt\", \"a\"); fputs($hFNTX, \"#\\n\"); fclose($hFNTX);";
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public IA.Plugin.Capabilities Capabilities
        {
            get
            {
#if DEBUG
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | Plugin.Capabilities.REPORTS;
#else
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW;
#endif
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
		}

        /// <summary>
        /// Метод генерации отчетов для тестов
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до каталога с отчетами плагина</param>
        public void GenerateStatements(string reportsDirectoryPath)
        {
            (new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage)).Execute();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            #if DEBUG
            // Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            if (string.IsNullOrEmpty(reportsDirectoryPath))
                throw new Exception("Путь к каталогу с отчетами не определен.");

            //StatementPrinter
            //StatementPrinter sprinter = new StatementPrinter(Path.Combine(reportsDirectoryPath, "source.xml"), storage);
            //sprinter.Execute();
            #endif
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID {get { return PluginSettings.ID; } }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            PluginSettings.IsLevel2 = true;
            PluginSettings.IsLevel3 = false;
            PluginSettings.IsNoSensors = false;
            PluginSettings.SensorFunctionText_Level2 = "function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open(\"GET\", \"http://localhost:8080/\"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} ";
            PluginSettings.SensorText_Level2 = "Sensor(#)";
            PluginSettings.SensorFunctionText_Level3 = "function SensorRnt(a) { var RNTfileWITHsensors_FileSystemObject, RNTfileWITHsensors_File; RNTfileWITHsensors_FileSystemObject = new ActiveXObject(\"Scripting.FileSystemObject\"); RNTfileWITHsensors_File = RNTfileWITHsensors_FileSystemObject.OpenTextFile(\"c:\\\\RNTSensorlog_JavaScript.log\", 8, true); RNTfileWITHsensors_File.WriteLine(a);RNTfileWITHsensors_File.Close();}";
            PluginSettings.SensorText_Level3 = "Sensor(#)";
            PluginSettings.FirstSensorNumber = 1;
            PluginSettings.IsSkipErrors = true;
            PluginSettings.MaterialNum = 1024;
            this.storage = storage;

            //Выгружаем настройки из данных плагина
            LoadPluginData();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.IsLevel2, ref PluginSettings.IsLevel3, ref PluginSettings.IsNoSensors);
            PluginSettings.FirstSensorNumber = settingsBuffer.GetUInt64();
            PluginSettings.SensorFunctionText_Level2 = settingsBuffer.GetString();
            PluginSettings.SensorText_Level2 = settingsBuffer.GetString();
            PluginSettings.SensorFunctionText_Level3 = settingsBuffer.GetString();
            PluginSettings.SensorText_Level3 = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.IsSkipErrors);
            PluginSettings.MaterialNum = settingsBuffer.GetInt32();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.IsNoSensors = false;
            PluginSettings.SensorFunctionText_Level2 =
                "function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open(\"GET\", \"http://localhost:8080/\"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} ";
            PluginSettings.SensorText_Level2 = "SensorRnt(#);";
            PluginSettings.SensorFunctionText_Level3 = String.Empty;
            PluginSettings.SensorText_Level3 =
                "var RNTfileWITHsensors_FileSystemObject, RNTfileWITHsensors_File; RNTfileWITHsensors_FileSystemObject = new ActiveXObject(\"Scripting.FileSystemObject\"); RNTfileWITHsensors_File = RNTfileWITHsensors_FileSystemObject.OpenTextFile(\"c:\\\\RNTSensorlog_JavaScript.log\", 8, true); RNTfileWITHsensors_File.WriteLine(#);RNTfileWITHsensors_File.Close();";
            PluginSettings.FirstSensorNumber = (storage != null && storage.sensors.EnumerateSensors().Any())
                ? storage.sensors.EnumerateSensors().Max(s => s.ID) + 1
                : 1;
            PluginSettings.IsSkipErrors = false;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "NDV:3":
                    PluginSettings.IsLevel3 = true;
                    PluginSettings.IsLevel2 = false;
                    break;
                case "NDV:2":
                    PluginSettings.IsLevel2 = true;
                    PluginSettings.IsLevel3 = false;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
        
        public UserControl SimpleSettingsWindow
        { get { return null; } }

        /// <summary> 
        /// 
        /// </summary>
        /// <param name="IsTestEnv">При тестовой среде удаляются номера дтчиков и # у ReplaceReturn</param>
        public void Run()
        {
            if (storage != null && storage.sensors.Count() > 0 && storage.sensors.EnumerateSensors().Max(s => s.ID) >=
                PluginSettings.FirstSensorNumber)
            {
                ulong tmp_FirstSensorNumber = PluginSettings.FirstSensorNumber;
                PluginSettings.FirstSensorNumber = storage.sensors.EnumerateSensors().Max(s => s.ID) + 1;
                Monitor.Log.Warning(PluginSettings.Name,
                    "Расстановка датчиков с номера <" + tmp_FirstSensorNumber +
                    "> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <" +
                    PluginSettings.FirstSensorNumber + ">.");
            }
            // все что возвращает 'IA'
            string[] filesWithPHP = storage.files.EnumerateFiles(enFileKind.fileWithPrefix)
                .Where(f => f.fileType.ContainsLanguages(enLanguage.PHP))
                .Select(f => f.FullFileName_Original)
                .ToArray();
            crEnv.NumPHP = filesWithPHP.LongLength;
            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong)crEnv.NumPHP);
            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)1).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong)crEnv.NumPHP);
            if (filesWithPHP.Length == 0)
                Monitor.Log.Error(PluginSettings.Name, "Исходных файлов php не обнаружено.");
            else
            {
                crEnv.pathPHPast = Path.Combine(storage.WorkDirectory.Path, "php_ast\\");
                crEnv.pathCleared = storage.WorkDirectory.CurrentSourcesDirectory;
                // TODO: убрать в будущем эту заплатку т.е. нужно чтобы в тестах были очищенные
                var arrFiles = Directory.GetFileSystemEntries(crEnv.pathCleared, "*", SearchOption.AllDirectories);
                if (arrFiles.Length == 0)
                {
                    var allFiles = storage.files.EnumerateFiles(enFileKind.fileWithPrefix)
                        .Where(f => f.fileType.ContainsLanguages(enLanguage.PHP)).ToList();
                    string srcOriginal = allFiles[0].FullFileName_Original.Substring(0, allFiles[0].FullFileName_Original.Length - allFiles[0].RelativeFileName_Original.Length);
                    allFiles = null;
                    foreach (var f in filesWithPHP)
                    {
                        string fName = f.Substring(srcOriginal.Length);
                        File.Copy(f, Path.Combine(crEnv.pathCleared, fName), true);
                    }
                }

                crEnv.SensPrint = crEnv.SensPrint.Replace("&", PluginSettings.MaterialNum.ToString());
                // TODO: получать от пользователя номер материала
                crEnv.pathLAB = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "PHP5LITE");//crEnv.pathCleared.Replace("src_original", "src_lab");
                var directory = Directory.GetParent(crEnv.pathLAB);
                directory.EnumerateFiles().ForEach(f => f.Delete());
                directory.EnumerateDirectories().ForEach(d => d.Delete(true));
                Directory.CreateDirectory(crEnv.pathLAB);
                crEnv.prefAST = crEnv.pathPHPast;
                crEnv.prefSRC = crEnv.pathCleared;

                var t = Task.Run(() => ThreadRunPHPast());
                t.Wait();
                // запускаем сам парсер на выполнение
                PHP5liteParserCore cParseCore = new PHP5liteParserCore(storage, crEnv);
                cParseCore.Run();
            }
        }

        // для запуска парсера (который строит ast по php файлам) в отдельном потоке
        // чисто так на всякий случай, чтобы вдруг стек не испортило или еще что-нибудь
        private void ThreadRunPHPast()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;

            string currentExeDir = IA.RuntimeDirectories.IARuntimeDirs.Plugins.ToString();
            string currentDir = Path.Combine(Directory.GetCurrentDirectory(), currentExeDir);
            string phpEngine = Path.Combine(currentDir, "php\\php.exe");
            string phpASTscript = Path.Combine(currentDir, "php\\test\\php_ast_luncher.php");

            startInfo.FileName = phpEngine;
            startInfo.Arguments = phpASTscript + " " + crEnv.pathCleared + " " + crEnv.pathPHPast;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.WorkingDirectory = Path.Combine(currentDir, "php");
            var pr = Process.Start(startInfo);
            List<string> astList = new List<string>();
            List<string> srcList = new List<string>();
            int iT = 1;
            while (!pr.StandardOutput.EndOfStream)
            {
                string line = pr.StandardOutput.ReadLine();
                if (line.Contains("Parse Error: "))
                {
                    Monitor.Log.Warning(PluginSettings.Name, line);
                    continue;
                }
                line = line.ToLower();
                srcList.Add(line);
                line = line.Replace(crEnv.prefSRC, crEnv.prefAST);
                astList.Add(line);
                if (iT < crEnv.NumPHP)
                    Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)0).Description(),
                    Monitor.Tasks.Task.UpdateType.STAGE, (ulong)iT++);
            }
            pr.WaitForExit();
            crEnv.arraySRC = srcList.ToArray();
            crEnv.arrayAST = astList.ToArray();
        }
        
        private void WashLabSources(string dirWithLabFiles)
        {
            string[] aLabFiles = Directory.GetFiles(dirWithLabFiles, "*", SearchOption.AllDirectories);
            foreach (string cFile in aLabFiles)
            {
                var aReaderObject = new AbstractReader(cFile);
                string fileContents = aReaderObject.getText();
                
                string replace = "Sensor()";
                Regex rgx_I = new Regex("Sensor\\(\\d+\\)");
                fileContents = rgx_I.Replace(fileContents, replace);

                replace = "IARETREPLACE";
                Regex rgx_II = new Regex("IARETREPLACE\\d+");
                fileContents = rgx_II.Replace(fileContents, replace);

                aReaderObject.writeTextToFile(cFile, fileContents);
            }
        }

#region Misc

        public string Name { get { return PluginSettings.Name; } }


        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (!(PluginSettings.IsLevel2 ? PluginSettings.SensorText_Level2 : 
                PluginSettings.SensorText_Level3).Contains("#") && !PluginSettings.IsNoSensors)
            {
                message = "Текст датчика должен содержать символ #.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.IsLevel2 = PluginSettings.IsLevel2;
            Properties.Settings.Default.IsLevel3 = PluginSettings.IsLevel3;
            Properties.Settings.Default.IsNoSensors = PluginSettings.IsNoSensors;
            Properties.Settings.Default.FirstSensorNumber = PluginSettings.FirstSensorNumber;
            Properties.Settings.Default.SensorFunctionText_Level2 = PluginSettings.SensorFunctionText_Level2;
            Properties.Settings.Default.SensorText_Level2 = PluginSettings.SensorText_Level2;
            Properties.Settings.Default.SensorFunctionText_Level3 = PluginSettings.SensorFunctionText_Level3;
            Properties.Settings.Default.SensorText_Level3 = PluginSettings.SensorText_Level3;
            Properties.Settings.Default.IsSkipErrors = PluginSettings.IsSkipErrors;
            Properties.Settings.Default.MaterialNum = PluginSettings.MaterialNum;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.IsLevel2, PluginSettings.IsLevel3, PluginSettings.IsNoSensors);
            settingsBuffer.Add(PluginSettings.FirstSensorNumber);
            settingsBuffer.Add(PluginSettings.SensorFunctionText_Level2);
            settingsBuffer.Add(PluginSettings.SensorText_Level2);
            settingsBuffer.Add(PluginSettings.SensorFunctionText_Level3);
            settingsBuffer.Add(PluginSettings.SensorText_Level3);
            settingsBuffer.Add(PluginSettings.IsSkipErrors);
            settingsBuffer.Add(PluginSettings.MaterialNum);
        }

        private void LoadPluginData()
        {
            //////считываем массив байт из файла
            //byte[] data = File.ReadAllBytes(storage.pluginData.GetDataFileName(PluginSettings.ID));

            ////Если файл данных плагина пуст, или содрежит недостаточно информации
            //if (data.Length != 2)
            //{
            //    if (data.Length != 0)
            //        IA.Monitor.Log.Warning(PluginSettings.Name, "Файл данных плагина повреждён. Не удалось загрузить битовый массив.");
            //    return;
            //}

            //PluginSettings.IsStage1Completed = data[0] != 0;
            //PluginSettings.IsStage2Completed = data[1] != 0;
        }

        public UserControl CustomSettingsWindow
        {
            get
            {
                return (UserControl)(settings = new Settings(
                        storage.sensors.EnumerateSensors().Count() == 0
                        ? 1
                        : storage.sensors.EnumerateSensors().Max(s => s.ID) + 1)
                        );
            }
        }
#endregion

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return null;
            }
        }

        // <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_PROCESSING.Description()),
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.ASTFILES_PARSING.Description()),
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.SENSOR_PLACEMENT.Description()),
                    new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.LABS_WRITER.Description())
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return null;
            }
        }

    } // END: class PHP5liteParser
}
