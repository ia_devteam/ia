﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FileOperations;
using IA.Extensions;
using Store;

namespace IA.Plugins.Parsers.PHP5liteParser
{
    class PHP5liteParserCore
    {
        // IA-Interface variables
        private Storage storage;
        private PHP5liteParser.runEnv crEnv;
        Dictionary<IFile, AbstractReader> filesContent = new Dictionary<IFile, AbstractReader>();
        Dictionary<IFile, string> filesWrite = new Dictionary<IFile, string>();

        // local variables
        private List<Function> vFuncs = new List<Function>();

        private ulong SensNum = 1; // текущий номер вставляемого датчика

       public PHP5liteParserCore(Storage storage, PHP5liteParser.runEnv crEnv)
        {
            this.storage = storage;
            this.crEnv = crEnv;
        }

        public void Run()
        {
            SensNum = PluginSettings.FirstSensorNumber;
            // TODO: теоретически могут быть возвращены файлы по которым аст не строятся
            // как-то это бы учесть

            parsePHPast();
            sensorPlacement();

            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 3).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong) crEnv.arraySRC.Length);
            // Обходим все файлы и пишем их на диск
            ulong lT = 1;
            foreach (var cFile in filesWrite)
            {
                Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 3).Description(),
                    Monitor.Tasks.Task.UpdateType.STAGE, lT++);
                //string fileLab = cFile.Key.FullFileName_Original.Substring(crEnv.prefSRC.Length, cFile.Key.FullFileName_Original.Length - crEnv.prefSRC.Length);
                string fileLab = cFile.Key.RelativeFileName_Original;
                string labPath = Path.Combine(crEnv.pathLAB, fileLab);
                string dirPath = Path.GetDirectoryName(labPath);
                
                AbstractReader aReaderObject = filesContent[cFile.Key];
                Directory.CreateDirectory(dirPath);
                aReaderObject.writeTextToFile(labPath, filesWrite[cFile.Key]);
            }
        }

        private void parsePHPast()
        {
            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)1).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong)crEnv.NumPHP);

            ulong cFn = 0;
            foreach (string cAST in crEnv.arrayAST)
            {
                string cFileSRC = crEnv.arraySRC[cFn];
                if (cFileSRC.Substring(crEnv.prefSRC.Length) == cAST.Substring(crEnv.prefAST.Length))
                {
                    AbstractReader aReaderObject = new FileOperations.AbstractReader(cFileSRC);
                    cFileSRC = cFileSRC.Substring(crEnv.prefSRC.Length);
                    string fileContent = aReaderObject.getText();
                    IFile cIFile = storage.files.Find(cFileSRC);
                    filesWrite.Add(cIFile, aReaderObject.getText());
                    // TODO: ищем открывающий тэг
                    filesContent.Add(cIFile, aReaderObject);
                    // заносим сам файл как функцию
                    int phpOffset = fileContent.IndexOf("<?php");
                    if (phpOffset >= 0)
                    {
                        string pTrim = fileContent.Substring(0, phpOffset).Trim();
                        if (pTrim.Length == 0)
                        {
                            Location cStartLocFile = new Location(1, 1, phpOffset);
                            Location cEndLocFile = new Location(1, 1, phpOffset);
                            Function cFuncFile = new Function(cFileSRC, cFileSRC, cStartLocFile, cEndLocFile);
                            ImportFunction(cFuncFile);
                        }
                    }

                    Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 1).Description(),
                        Monitor.Tasks.Task.UpdateType.STAGE, cFn + 1);
                    cFn++;

                    string[] cCONTENT = File.ReadAllLines(cAST);
                    string c_file = cAST; // еще его как-то канонизировать
                    c_file = c_file.Substring(crEnv.prefAST.Length);
                    Stack BStack = new Stack(); // стек открывающих скобок
                    Stack CStack = new Stack(); // стек имен классов, к которым привязывают методы
                    for (int iT = 0; iT < cCONTENT.Length; iT++)
                    {
                        // берем на вход открывающую скобку '{' и ее позицию
                        string c_line = cCONTENT[iT];
                        if (c_line.Contains("{") && c_line.Trim().Length == 1)
                        {
                            int bPos = c_line.IndexOf("{");
                            BStack.Push(bPos);
                        }
                        if (c_line.Contains("Stmt_Class") && !c_line.Contains("Stmt_ClassMethod") &&
                            !c_line.Contains("Stmt_ClassConst"))
                        {
                            while (!c_line.Contains("name"))
                            {
                                iT++;
                                c_line = cCONTENT[iT];
                            }
                            iT = iT + 2;
                            c_line = cCONTENT[iT];

                            Regex r = new Regex("\".*?\""); //new Regex("\"[\\w ]*\"");
                            MatchCollection mc = r.Matches(c_line);
                            c_line = mc[1].Value;

                            string class_name = c_line; //"CController"
                            class_name = class_name.Replace("\"", "");
                            class_name = class_name.Trim();
                            BClass Nclass = new BClass((int) BStack.Peek(), class_name);
                            CStack.Push(Nclass);
                        }

                        // выделяем методы и функции
                        if (c_line.Contains("Stmt_Function"))
                        {
                            iT++; // 'byRef' - переходим на эту строчку
                            iT++; // "name":
                            iT++; // "nodeType": "Identifier",
                            iT++; // "name": "get_last_event_by_triggerid",
                            // выделить имя функции
                            string c_name = cCONTENT[iT];
                            Regex r = new Regex("\".*?\""); //new Regex("\"[\\w ]*\"");
                            MatchCollection mc = r.Matches(c_name);
                            c_name = mc[1].Value;
                            c_name = c_name.Replace("\"", "");
                            c_name = c_name.Trim();
                            iT++; // "attributes": {

                            iT++; // "startLine": 22,
                            c_line = cCONTENT[iT];
                            Regex re = new Regex(@"\d+");
                            Match m1 = re.Match(c_line);
                            int LocLine = int.Parse(m1.Value);
                            int LocOffset = aReaderObject.getLineOffset(LocLine);
                           Location cStartLoc = new Location(0, 0, LocOffset);

                            iT++; // "endLine": 22
                            c_line = cCONTENT[iT];
                            Match m2 = re.Match(c_line);
                            LocLine = int.Parse(m2.Value);
                            LocOffset = aReaderObject.getLineOffset(LocLine);
                            Location cEndLoc = new Location(0, 0, LocOffset);

                            Function cFunc = new Function(c_name, c_file, cStartLoc, cEndLoc);
                            vFuncs.Add(cFunc);
                            ImportFunction(cFunc);
                        }

                        if (c_line.Contains("Stmt_ClassMethod"))
                        {
                            iT++;
                            iT++; // "name":
                            iT++; // "byRef":
                            iT++; // "name": {
                            iT++; // "nodeType": "Identifier",
                            // выделить имя функции
                            string c_name = cCONTENT[iT];
                            Regex r = new Regex("\".*?\""); //new Regex("\"[\\w ]*\"");
                            MatchCollection mc = r.Matches(c_name);
                            c_name = mc[1].Value;
                            c_name = c_name.Replace("\"", "");
                            c_name = c_name.Trim();
                            string class_name = "";
                            if (CStack.Count > 0)
                            {
                                BClass bClass = (BClass) CStack.Peek();
                                c_name = bClass.class_name + "::" + c_name;
                            }

                            iT++; // "attributes": {

                            iT++; // "startLine": 22,
                            c_line = cCONTENT[iT];
                            Regex re = new Regex(@"\d+");
                            Match m1 = re.Match(c_line);
                            //split[0]
                            int LocLine = int.Parse(m1.Value);
                            int LocOffset = aReaderObject.getLineOffset(LocLine);
                            string f_name = c_name.Substring(c_name.IndexOf("::") + 2);
                            LocOffset = aReaderObject.getLineOffset(LocLine);
                            Location cStartLoc = new Location(0, 0, LocOffset);

                            iT++; // "endLine": 22
                            c_line = cCONTENT[iT];
                            Match m2 = re.Match(c_line);
                            LocLine = int.Parse(m2.Value);
                            LocOffset = aReaderObject.getLineOffset(LocLine);
                            Location cEndLoc = new Location(0, 0, LocOffset);
                            // TODO а вообще имя метода заносится с именем класса или отдельно ?
                            Function cFunc = new Function(c_name, c_file, cStartLoc, cEndLoc);
                            vFuncs.Add(cFunc);
                            ImportFunction(cFunc);
                        }
                        // ищем скобки закрытия и выкидываем верхушки из стека классов
                        if (c_line.Contains("}") && c_line.Trim().Length == 1)
                        {
                            int bPos = c_line.IndexOf("}");
                            if (bPos == (int) BStack.Peek())
                            {
                                if (CStack.Count > 0)
                                {
                                    if (((BClass) CStack.Peek()).Bpos == bPos)
                                    {
                                        CStack.Pop();
                                    }
                                }
                                BStack.Pop();
                            }
                        }
                    } // END for (int iT = 0; iT < cCONTENT.Length; iT++)
                } // END foreach (string cAST in listAST)
            }
        }

        private void sensorPlacement()
        {
            var enf = storage.functions.EnumerateFunctions(enFunctionKind.ALL).ToArray();

            Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks) 2).Description(),
                Monitor.Tasks.Task.UpdateType.STAGES, (ulong) enf.LongLength);

            var pasterList = storage.functions.EnumerateFunctionLocationsInSortOrder().ToList();
            Dictionary<IFile, List<Tuple<IFunction, Store.Location>>> funcsLocs =
                new Dictionary<IFile, List<Tuple<IFunction, Store.Location>>>();
            foreach (var cFunLoc in pasterList)
            {
                if (!funcsLocs.ContainsKey(cFunLoc.location.GetFile()))
                {
                    List<Tuple<IFunction, Store.Location>> cTuple = new List<Tuple<IFunction, Store.Location>>();
                    cTuple.Add(new Tuple<IFunction, Store.Location>(cFunLoc.function, cFunLoc.location));
                    funcsLocs.Add(cFunLoc.location.GetFile(), cTuple);
                }
                else
                    funcsLocs[cFunLoc.location.GetFile()].Add(new Tuple<IFunction, Store.Location>(cFunLoc.function, cFunLoc.location));
            }

            int cFn = 1;
            foreach (var cFuncLocat in funcsLocs.Keys)
            {
                var sortOffset = funcsLocs[cFuncLocat].OrderByDescending(x => x.Item2.GetOffset()).ToList();
                string cWrite = filesWrite[sortOffset[0].Item2.GetFile()];

                foreach (var csOff in sortOffset)
                {
                    Monitor.Tasks.Update(PluginSettings.Name, ((PluginSettings.Tasks)2).Description(),
                        Monitor.Tasks.Task.UpdateType.STAGE, (ulong)cFn++);
                    
                    int pOffset = (int)csOff.Item2.GetOffset();
                    string nSensPrint1 = crEnv.SensPrint.Replace("#", SensNum.ToString());
                    // вставляем в сам файл как функцию
                    if (csOff.Item2.GetFile().RelativeFileName_Canonized == csOff.Item1.FullName)
                    {
                        pOffset += "<?php".Length;
                    }
                    else
                    {
                        pOffset = cWrite.IndexOf("{", pOffset);
                    }
                    cWrite = cWrite.Insert(pOffset + 1, nSensPrint1);
                    IFunction pIFunc = csOff.Item1;
                    pIFunc.AddSensor(SensNum, Kind.START);
                    SensNum++;
                }
                filesWrite[sortOffset[0].Item2.GetFile()] = cWrite;
            }
        }

        private void ImportFunction(Function cFunc)
        {
            //Создаём новый класс в Хранилище, если он ещё не создан
            IFunction func;
            func = !cFunc.IsMethod ? storage.functions.AddFunction() : storage.functions.AddMethod();

            //Читаем имя функции
            func.Name = cFunc.name;

            var iFile = storage.files.Find(cFunc.file);
            // cutting off .ast suffix
            if (iFile != null)
            {
                //Место определения функции
                if (cFunc.StartLocation.Offset > 0)
                    func.AddDefinition(iFile, (ulong) cFunc.StartLocation.Offset);
                else
                    func.AddDefinition(iFile, (ulong)cFunc.StartLocation.Col, (ulong)cFunc.StartLocation.Line);
                //Места декларации функции - здесь случай когда верхнее и нижнее отличается
                if (cFunc.StartLocation.Offset > 0)
                    func.AddDeclaration(iFile, (ulong)cFunc.StartLocation.Offset);
                else
                    func.AddDeclaration(iFile, (ulong)cFunc.StartLocation.Col, (ulong)cFunc.StartLocation.Line);

                bool isMethod = cFunc is MethodType;
                IMethod method = func as IMethod;
                if (method != null) //Заполняем поля, специфичные для метода
                {
                    method.isOverrideBase = isMethod;
                }
            }
            else
            {
                // TODO: сделать сообщение на лог ошибок о том, что не найден файл
            }
        }

        struct Location
        {
            public int Line;
            public int Col;
            public int Offset;

            public Location(int tLine, int tCol, int tOffset)
            {
                Line = tLine;
                Col = tCol;
                Offset = tOffset;
            }
        }

        // 'Function'
        struct Function
        {
            public string name; // название функции
            public string shrn; // короткое название функии без имени класса
            public string clnm; // имя класса к которому относится метод
            public string file; // имя файла
            public long senN;    // номер датчика который был вставлен для функции
            public Location StartLocation; // Start позиция 
            public Location EndLocation; // End позиция
            public Location InsertLocation; // позиция куда вставлен датчик
            public bool WasInsert;          // признак того, что был вставлен датчик
            public bool IsMethod;           // признак того что это метод
            public bool IsSelfFile;         // признак того, что это сам PHP файл

            public Function(string tname, string tfile, Location tStartLoc, Location tEndLoc)
            {
                name = tname;
                if (tname.Contains("::"))
                {
                    shrn = tname.Substring(tname.IndexOf("::") + 2);
                    clnm = tname.Substring(0, tname.IndexOf("::"));
                    IsMethod = true;
                }
                else
                {
                    clnm = "";
                    IsMethod = false;
                    shrn = tname;
                }
                IsSelfFile = false;
                file = tfile;
                senN = 0;
                StartLocation = tStartLoc;
                EndLocation = tEndLoc;
                InsertLocation = new Location(0, 0, 0);
                WasInsert = false;
            }
        }

        // имя класса и смещение '{' для него
        struct BClass
        {
            public int Bpos;
            public string class_name;

            public BClass(int pos, string name)
            {
                Bpos = pos;
                class_name = name;
            }
        }
        
    } // END : PHP5liteParserCore() 
} // END : IA.Plugins.Parsers.PHP5liteParser
