﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using Store.Table;
using TestUtils;
using IA.Plugins.Parsers.PHP5liteParser;
using IA.Plugins.Parsers.CommonUtils;

namespace TestPHP5liteParser
{
    /// <summary>
    /// Класс для тестов парсера PHP5-lite
    /// </summary>
    [TestClass]
    public class TestPHP5liteParser
    {

        #region ServiceRegion
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Список сообщений
            /// </summary>
            public List<string> Messages
            {
                get 
                {
                    return messages;
                }
            }
        }

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Путь до каталога с эталонными исходными текстами
        /// </summary>
        private string labSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонным дампами Хранилища
        /// </summary>
        private string dumpSampleDirectoryPath;

        /// <summary>
        /// Путь до каталога с эталонными отчетами
        /// </summary>
        private string reportSampleDirectoryPath;

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными лабораторным текстам
        /// </summary>
        private const string LAB_SAMPLES_SUBDIRECTORY = @"PHP5lite\Labs";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными дампами Хранилища
        /// </summary>
        private const string DUMP_SAMPLES_SUBDIRECTORY = @"PHP5lite\Dump";

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными отчетами
        /// </summary>
        private const string REPORT_SAMPLES_SUBDIRECTORY = @"PHP5lite\Reports";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        private const ulong pluginID = Store.Const.PluginIdentifiers.PHP5_LITE_PARSER;

        /// <summary>
        /// Перехватчик сообщений монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName); 
            labSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, LAB_SAMPLES_SUBDIRECTORY);
            dumpSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, DUMP_SAMPLES_SUBDIRECTORY);
            reportSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, REPORT_SAMPLES_SUBDIRECTORY);
        }

        /// <summary>
        /// Подготовка Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="testMaterialsPath">Путь до каталога с материалами.</param>
        /// <param name="sourcePostfixPart">Подпуть до каталога с исходными файлами.</param>
        /// <param name="isLevel2">Уровень НДВ. true - 2-ой уровень</param>
        private void CustomizeStorage(Storage storage, string testMaterialsPath, string sourcePostfixPart, bool isLevel2, ulong firstSensorNumber)
        {
            TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPart));
            TestUtilsClass.Run_IdentifyFileTypes(storage);
            TestUtilsClass.Run_CheckSum(storage);

            SettingUpPlugin(storage, firstSensorNumber, isLevel2);
        }

        /// <summary>
        /// Настройка плагина
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="firstSensorNumber">Номер первого датчика. Не может быть отрицательным.</param>
        /// <param name="isLevel2">true - 2-ой уровень.</param>
        private void SettingUpPlugin(Storage storage, UInt64 firstSensorNumber, bool isLevel2)
        {
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(isLevel2, !isLevel2, false);
            buffer.Add(firstSensorNumber);
            buffer.Add("function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open(\"GET\", \"http://localhost:8080/\"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} ");
            buffer.Add("SensorRnt(#);");
            buffer.Add("function SensorRnt(a) { var RNTfileWITHsensors_FileSystemObject, RNTfileWITHsensors_File; RNTfileWITHsensors_FileSystemObject = new ActiveXObject(\"Scripting.FileSystemObject\"); RNTfileWITHsensors_File = RNTfileWITHsensors_FileSystemObject.OpenTextFile(\"c:\\\\RNTSensorlog_JavaScript.log\", 8, true); RNTfileWITHsensors_File.WriteLine(a);RNTfileWITHsensors_File.Close();}");
            buffer.Add("SensorRnt(#);");
            buffer.Add(true);
            buffer.Add((int)1024);

            storage.pluginSettings.SaveSettings(pluginID, buffer);
        }

        /// <summary>
        /// Дамп Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <returns>Путь до каталога, куда был произведен дамп Хранилища</returns>
        private string Dump(Storage storage)
        {
            string dumpDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(WorkDirectory.enSubDirectories.STORAGE), "Dump");

            if (!Directory.Exists(dumpDirectoryPath))
                Directory.CreateDirectory(dumpDirectoryPath);

            storage.ClassesDump(dumpDirectoryPath);
            storage.FilesDump(dumpDirectoryPath);
            storage.FunctionsDump(dumpDirectoryPath);
            storage.VariablesDump(dumpDirectoryPath);

            return dumpDirectoryPath;
        }

        /// <summary>
        /// Проверка дампа Хранилища
        /// </summary>
        /// <param name="dumpDirectoryPath">Путь до каталога дампа в Хранилище. Не может быть пустым.</param>
        /// <param name="dumpSamplesDirectoryPath">Путь до каталога с эталонным дампом. Не может быть пустым.</param>
        private void CheckDump(string dumpDirectoryPath, string dumpSamplesDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(dumpDirectoryPath, dumpSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Дамп Хранилища не совпадает с эталонными.");
        }

        /// <summary>
        /// Проверка лабораторных исходных файлов
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="labSamplesDirectoryPath">Путь до каталога с эталонными лабораторными исходными текстами. Не может быть пустым.</param>
        private void CheckLabs(Storage storage, string labSamplesDirectoryPath)
        {
            string labsDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_LAB), "PHP5LITE");

            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(labsDirectoryPath, labSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Исходные тексты со вставленными датчиками не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка отчетов
        /// </summary>
        /// <param name="xmlSampleDirectoryPath">Путь до каталога с эталонными отчетами. Не может быть пустым.</param>
        /// <param name="storageReportsDirectoryPath">Путь до каталога с отчетами в Хранилище. Не может быть пустым.</param>
        private void CheckXML(string xmlSampleDirectoryPath, string storageReportsDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_XML_Compare(storageReportsDirectoryPath, xmlSampleDirectoryPath);

            Assert.IsTrue(isEqual, "Отчеты не совпадают с эталонными.");
        }

        /// <summary>
        /// Проверка ожидаемого сообщения в логе монитора
        /// </summary>
        /// <param name="message">Текст сообщения. Не может быть пустым.</param>
        private void CheckMessage(string message)
        {
            bool isContainMessage = listener.ContainsPart(message);

            Assert.IsTrue(isContainMessage, "Сообщение не совпадает с ожидаемым.");
        }

        #endregion

        #region IACommon
        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже присутствует в лабораторных тексах.
        /// </summary>
        [TestMethod]
        public void PHP5liteParser_WrongFirstSensor_InLabsTest()
        {
            PHP5liteParser testPlugin = new PHP5liteParser();
            var sourcePostfixPart = @"Sources\PHP5lite\OnlyPHPtag";
            testUtils.RunTest(
                sourcePostfixPart,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                       //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);

                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <1> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <2>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика не фигурировал в исходных файлах, 
        /// но оказался меньше, чем максимальный идентификатор в Хранилище.
        /// </summary>
        [TestMethod]
        public void PHP5liteParser_WrongFirstSensor_NotInLabsTest()
        {
            PHP5liteParser testPlugin = new PHP5liteParser();
            var sourcePostfixPart = @"Sources\PHP5lite\OnlyPHPtag";
            testUtils.RunTest(
                sourcePostfixPart,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(3, func1.Id, Kind.START);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);

                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 2);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка получения сообщения об установке неправильного стартового датчика.
        /// Проверяется случай, когда идентификатор стартового датчика уже фигурировал в исходных файлах, но был удален.
        /// </summary>
        [TestMethod]
        public void PHP5liteParser_WrongFirstSensor_NotInLabsDeletedTest()
        {
            PHP5liteParser testPlugin = new PHP5liteParser();
            var sourcePostfixPart = @"Sources\PHP5lite\OnlyPHPtag";
            testUtils.RunTest(
                sourcePostfixPart,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IFunction func1 = storage.functions.AddFunction();
                    storage.sensors.AddSensor(1, func1.Id, Kind.START);
                    storage.sensors.AddSensor(2, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(3, func1.Id, Kind.INTERNAL);
                    storage.sensors.AddSensor(4, func1.Id, Kind.FINAL);

                    storage.sensors.RemoveSensor(2);

                    CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 2);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    CheckMessage("Расстановка датчиков с номера <2> недоступна для вставки в исходные тексты. Расстановка датчиков будет начата с датчика <5>.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки плагина на пустой папке.
        /// </summary>
        [TestMethod]
        public void PHP5liteParser_EmptyFolderTest()
        {
            string sourcePostfixPart = @"EmptyFolder";

            testUtils.RunTest(
               sourcePostfixPart,                                           //sourcePostFixPart
               new PHP5liteParser(),                                      //plugin
               false,                                                       //isUseCachedStorage
               (storage, testMaterialsPath) => { },                         //prepareStorage
               (storage, testMaterialsPath) =>
               {
                   if (!Directory.Exists(Path.Combine(testMaterialsPath, sourcePostfixPart)))
                       Directory.CreateDirectory(Path.Combine(testMaterialsPath, sourcePostfixPart));
                   CustomizeStorage(storage, testMaterialsPath, sourcePostfixPart, true, 1);
               },                                                           //customizeStorage               
               (storage, testMaterialsPath) =>
               {
                   string dumpDirectoryPath = Dump(storage);

                   CheckMessage("Исходных файлов php не обнаружено.");
                   //CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, "EmptyFolder"));

                   return true;
               },                                                           //checkStorage
               (reportsFullPath, testMaterialsPath) => { return true; },    //checkreports
               false,                                                       //isUpdateReport
               (plugin, testMaterialsPath) => { },                          //changeSettingsBeforRerun
               (storage, testMaterialsPath) => { return true; },            //checkStorageAfterRerun
               (reportsPath, testMaterialsPath) => { return true; }         //checkReportAfterRerun
               );
        }

        #endregion

        /// <summary>
        /// в тесте проверятся факт вставки датчика в '<?php >'
        /// </summary>
        [TestMethod]
        public void PHP5liteParser_OnlyPHPtagTest()
        {
            string short_name = "OnlyPHPtag";
            string sourcePostfixPart = @"Sources\PHP5lite\" + short_name;
            PHP5liteParser testPlugin = new PHP5liteParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1
                        );
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\PHP5lite\Reports\" + short_name + ".xml";
                    bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return isEqual;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// в тесте проверятся объявление прототипа функции, а потом вызов её
        /// </summary>
        [TestMethod]
        public void PHP5liteParser_ConditionalFunctionsTest()
        {
            string short_name = "ConditionalFunctions";
            string sourcePostfixPart = @"Sources\PHP5lite\" + short_name;
            PHP5liteParser testPlugin = new PHP5liteParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1
                        );
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\PHP5lite\Reports\" + short_name + ".xml";
                    bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return isEqual;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// в тесте проверятся объявление вложенных функций
        /// </summary>
        [TestMethod]
        public void PHP5liteParser_FunctionsWithinFunctionsTest()
        {
            string short_name = "FunctionsWithinFunctions";
            string sourcePostfixPart = @"Sources\PHP5lite\" + short_name;
            PHP5liteParser testPlugin = new PHP5liteParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1
                        );
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\PHP5lite\Reports\" + short_name + ".xml";
                    bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return isEqual;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Class - проверяется объявление и вызов методов
        /// </summary>
        [TestMethod]
        public void PHP5liteParser_ClassTest()
        {
            string short_name = "Class";
            string sourcePostfixPart = @"Sources\PHP5lite\" + short_name;
            PHP5liteParser testPlugin = new PHP5liteParser();
            string reportPath = "";
            testUtils.RunTest(
                sourcePostfixPart, // sourcePostfixPart
                testPlugin, // Plugin
                false, // isUseCachedStorage
                (storage, testMaterialsPath) => { }, // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    CustomizeStorage(storage,
                        testMaterialsPath,
                        sourcePostfixPart,
                        true,
                        1
                        );
                }, // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //TestUtilsClass.Run_SensorTypeFixer(storage);
                    string dumpDirectoryPath = Dump(storage);
                    CheckLabs(storage, Path.Combine(labSampleDirectoryPath, @"" + short_name));
                    string samplesPath = dumpSampleDirectoryPath + @"\" + short_name;
                    CheckDump(dumpDirectoryPath, samplesPath);
                    reportPath = dumpDirectoryPath + @"\" + short_name + ".xml";
                    StatementPrinter printerXML = new StatementPrinter(reportPath, storage);
                    printerXML.Execute_NoFile();
                    return true;
                }, //checkreports
                (reportsFullPath, testMaterialsPath) =>
                {
                    string etalonPath = testMaterialsPath + @"\PHP5lite\Reports\" + short_name + ".xml";
                    bool isEqual = TestUtilsClass.Reports_XML_Compare(reportPath, etalonPath);
                    return isEqual;
                }, // checkReportBeforeRerun
                false, // isUpdateReport
                (plugin, testMaterialsPath) => { }, // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; }, // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; } // checkReportAfterRerun
                );
        }
    }
}
