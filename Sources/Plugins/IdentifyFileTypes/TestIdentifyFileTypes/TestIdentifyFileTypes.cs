using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Store;
using Store.Table;
using TestUtils;

namespace TestIdentifyFileType
{
    /// <summary>
    /// Summary description for MainTest
    /// </summary>
    [TestClass]
    public class TestIdentifyFileTypes
    {
        /// <summary>
        /// ��������� �������������� ������������ ��� ������� �����
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;

        private const string etalonDumpsPath = @"IdentifyFileTypes\Dumps";
        private const string etalonReportsPath = @"IdentifyFileTypes\Reports";
        /// <summary>
        /// ���������� � ������� �����
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// ������ ��, ��� ���������� ������� ����� �������� ������� �����
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //�������������� �������������� ������������ ��� ������� �����
            testUtils = new TestUtilsClass(testContextInstance.TestName);
            TestUtils.TestUtilsClass.SQL_SetupTestConnection();
        }

        /// <summary>
        /// ���������� � ������ ������ ������ ��������� � ����������.
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="testMaterials"></param>
        /// <param name="testDirName"></param>
        /// <returns></returns>
        private bool CompareDumps(Storage storage, string testMaterials, string testDirName)
        {
            storage.FilesDump();
            string etalonDumpDir = Path.Combine(testMaterials, etalonDumpsPath, testDirName);
            return TestUtilsClass.Reports_Directory_TXT_Compare(storage.GetInternalDumpsDirectory(), etalonDumpDir);
        }

        private bool CompareReports(string reportsFromStorage, string testMaterials, string testMaterialsSubdir)
        {
            return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterials, etalonReportsPath, testMaterialsSubdir), reportsFromStorage, true, testMaterials);
        }

        /// <summary>
        /// ������ �������� �����. ��-��������� ����������� ������ ������ � ������� � ����������. ����������� ����� ��������� �������� ���������.
        /// </summary>
        /// <param name="fileContentsToCheck">��� �����������, ������� ����������� ������ ���� � ������� ����� � ���������. ���� 0 - �������� �� ����������.</param>
        /// <param name="workPlace">�������� �������� ����� ��� ����������������.</param>
        /// <param name="workDescription">�������� �������� ����� ��� ����������������.</param>
        /// <param name="customStorageCheck">�������������� �������� ���������, ����������� ����� ���� ��������� �������� �� ����� CheckStorage. � ������ �������� �������� ������ ������ ���������� (������ - Assert). ���� null - ������������.</param>
        private void RunTest(enTypeOfContents fileContentsToCheck = 0, string workPlace = null, string workDescription = null, Action<Storage> customStorageCheck = null)
        {
            string testName = testUtils.TestName.Substring("IdentifyFileTypes_".Length);
            string sourcePrefixPart = Path.Combine(@"Sources\IdentifyFileTypes", testName);

            testUtils.RunTest(
                sourcePrefixPart, //sourcePrefixPart
                new IA.Plugins.Analyses.IdentifyFileTypes.IdentifyFileTypes(), //plugin
                false, //isUseCachedStorage
                null, //prepareStorage
                (storage, testMaterialsPath) =>
                { //customizeStorage
                    if (!Directory.Exists(Path.Combine(testMaterialsPath, sourcePrefixPart)))
                        Directory.CreateDirectory(Path.Combine(testMaterialsPath, sourcePrefixPart));

                    TestUtilsClass.Run_FillFileList(storage, System.IO.Path.Combine(testMaterialsPath, sourcePrefixPart));

                    IBufferWriter buffer = WriterPool.Get();
                    buffer.Add(workPlace ?? "������� ������� �����");
                    buffer.Add(workDescription ?? "");
                    ulong pluginID = Store.Const.PluginIdentifiers.IDENTIFY_FILE_TYPES;
                    storage.pluginSettings.SaveSettings(pluginID, buffer);
                },
                (storage, testMaterialsPath) =>
                {
                    //�������� ����� ��������: �������� ��������� �� ��������
                    Assert.IsTrue(CompareDumps(storage, testMaterialsPath, testName), "���� ������ ��������� �� ��������� � ����������.");
                    //�������� ����� ��������: ����� ��������� ��������� ���� �����������.
                    if (fileContentsToCheck != 0)
                        foreach (var file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
                        {
                            Assert.IsTrue((file.fileType.Contains() & fileContentsToCheck) == fileContentsToCheck, 
                                String.Format("���������� ����� <{0}> ���������� ��� <{1}>, ��� �� �������� � ���� ���������: <{2}>.", file.FileNameForReports, file.fileType.Contains(), fileContentsToCheck)
                                );
                        }

                    if (customStorageCheck != null)
                        customStorageCheck(storage);
                    return true;
                },                                                          //checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    //�������� ����� ��������: � �� �� ���������� �������� �����, ���������� ���������� ��������� � ���� ����������.
                    Assert.IsTrue(CompareReports(reportsPath, testMaterialsPath, testName), "������ �� ��������� � ����������.");
                    return true;
                }, //checkReportBeforRerun
                false, //isUpdateReport
                null, //changeSettingsBeforRerun
                null, //checkStorageAfterRerun
                null //checkReportAfterRerun
               );
        }

        /// <summary>
        /// ����������� ����: ������� ����� �����������
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_Trivial()
        {
            RunTest();
        }

        /// <summary>
        /// �������� ������������ ������������� ������� ����. 
        /// ������� ������� �����, �������� �� Default.
        /// �������� ����� ����� ���������� ����������, �� ������ ���� � ������ ������� ������.
        /// �����������, ��� ��� ���� ��������� ����������
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_WorkPlaceTest()
        {
            RunTest(workPlace: "�������� ������� �����", workDescription: "dummytext");           
        }

        /// <summary>
        /// �������� ������������ ������������� ������� ����. 
        /// ������� ������� ����� ��-���������.
        /// �������� ����� ����� ���������� ����������, �� ������ ���� � ������ ������� ������.
        /// �����������, ��� �� ��� ���� ��������� ������������
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_WorkPlaceDefault()
        {
            RunTest();
        }

        /// <summary>
        /// ������������ �������� �����, ���������� ��� ��������� ���� �����������:
        /// DATA, EXECUTABLE, TEXTSOURCES, BINARYSOURCES, DONTCONSIDERREGISTER, REDUNDANT
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_ContentsType()
        {
            RunTest(customStorageCheck: (s) => {
                enTypeOfContents allTypes = 0;
                IA.Extensions.EnumLoop<enTypeOfContents>.ForEach((t) => allTypes = allTypes | t);

                Assert.IsTrue(allTypes != 0, "�� ���������� �� ������ ���� ����������� ������.");

                enTypeOfContents usedTypes = 0;
                foreach (var file in s.files.EnumerateFiles(enFileKind.fileWithPrefix))
                {
                    usedTypes |= file.fileType.Contains();
                }

                Assert.IsTrue(usedTypes == allTypes, "�� ��� ���� ����������� ���� ����������. ����������� ����: <" + usedTypes.ToString() + ">.");
            }); ;
        }
        /// <summary>
        /// ������������ �������� �����, ���������� ��� ����������� DATA
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_Data()
        {
            RunTest(enTypeOfContents.DATA);
        }

        /// <summary>
        /// ������������ �������� �����, ���������� ��� ����������� EXECUTABLE
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_Executable()
        {
            RunTest(enTypeOfContents.EXECUTABLE);
        }

        /// <summary>
        /// ������������ �������� �����, ���������� ��� ����������� TEXTSOURCE
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_TextSource()
        {
            RunTest(enTypeOfContents.TEXTSOURCES);
        }

        /// <summary>
        /// ������������ �������� �����, ���������� ��� ����������� BINARYSOURCE
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_BinarySources()
        {
            RunTest(enTypeOfContents.BINARYSOURCES);
        }

        /// <summary>
        /// ������������ �������� �����, ���������� ��� ����������� DONTCONSIDERREGISTER
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_DontConsiderRegister()
        {
            RunTest(enTypeOfContents.DONTCONSIDERREGISTER);
        }

        /// <summary>
        /// ������������ �������� �����, ���������� ��� ����������� REDUNDANT
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_Redundant()
        {
            RunTest(enTypeOfContents.REDUNDANT);
        }

        /// <summary>
        /// ������������ �������� �����, ���������� ������ �� ��������� ������ ����������������
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_Languages()
        {
            RunTest();
        }

        /// <summary>
        /// ������������ �������� ����� ��� ����������
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_ExtensionAbsent()
        {
            RunTest();
        }

        /// <summary>
        /// ������������ �������� �����, ������� ����������, ��������������� ������������� ���� ������
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_ExtensionTheOnly()
        {
            RunTest(customStorageCheck: (s) => 
            {
                int numOfTypes = 1;
                HashSet<FileType> types = new HashSet<FileType>();
                foreach (var f in s.files.EnumerateFiles(enFileKind.anyFile))
                    types.Add(f.fileType);
                Assert.IsTrue(types.Count == numOfTypes, "���������� ������������ ����� ��������� ���������� ���������� (<" 
                    + numOfTypes+">). ����������: <"+ types.Count + ">.");
            });
        }

        /// <summary>
        /// ������������ �������� �����, ������� ����������, ��������������� ���������� ����� ������
        /// </summary>
        [TestMethod]    
        public void IdentifyFileTypes_ExtensionsSeveral()
        {
            RunTest();
        }

        /// <summary>
        /// ������������ �������� �����, ���� ������� ����� ������� ��������� Binary
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_PatternBinary()
        {
            RunTest();
        }

        /// <summary>
        /// ������������ �������� �����, ���� ������� ����� ������� ��������� Text
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_PatternText()
        {
            RunTest();
        }

        /// <summary>
        /// ������������ �������� �����, ���� ������� ����� ������� ��������� XML
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_PatternXML()
        {
            RunTest();
        }
        /// <summary>
        /// ������������ �������� �����, ���� ������� ����� ������� ��������� Executable
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_PatternExecutable()
        {
            RunTest();
        }

        /// <summary>
        /// ������������ �������� �����, �� ������� ��������������� ����� (=> �� ������������)
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_SignaturesNone()
        {
            RunTest();
        }

        /// <summary>
        /// ������������ �������� �����, ������� ����������, ��������������� ���� ����� ������.
        /// �������� �����:
        /// 1 - ������������� ������ 1 ����
        /// 2 - ������������� ������ 2 ����
        /// 3 - ������������� � 1 � 2 �����
        /// </summary>
        [TestMethod]
        public void IdentifyFileTypes_SignaturesTwo()
        {
            RunTest();
        }


    }
}
