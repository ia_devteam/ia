﻿using System.Collections.Generic;
using System.Linq;
using Store;



namespace IA.Plugins.Analyses.IdentifyFileTypes
{

    static class Log
    {
        
        /// <summary>
        /// Функция генерирует отчет в файл типа XML Spreadsheet
        /// </summary>
        /// <param name="path">путь к файлу отчета</param>
        /// <param name="files">файлы хранилища</param>
        internal static void GenerateReports1(string path, Files files)
        {

            IA.Sql.DatabaseConnections.SignatureFileDatabaseConnection sigSQL = new IA.Sql.DatabaseConnections.SignatureFileDatabaseConnection();
            List<string[]> FileFormat = sigSQL.FileFormat;
            List<string> dKeys = FileFormat.Select(x => x[4]).Distinct().ToList();
            Dictionary<string, string[]> dFileFormat = new Dictionary<string, string[]>();
            foreach (string s in dKeys)
            {
                dFileFormat.Add(s, FileFormat.Where(x => x[4] == s).FirstOrDefault());
            }

            List<classTmpReport> listCTR = new List<classTmpReport>();

            List<string[]> reportDetailed = new List<string[]>();
            reportDetailed.Add(new string[] { "Имя файла", "Расширение", "Описание", "Полное описание", "Может содержать фрагменты исходного текста", "Может содержать фрагменты исполняемого кода" });
            List<string[]> reportIntegral = new List<string[]>();
            reportIntegral.Add(new string[] { "Количество файлов", "Расширение", "Описание", "Полное описание", "Может содержать фрагменты исходного текста", "Может содержать фрагменты исполняемого кода" });

            List<Pair> sortedFiles = new List<Pair>();
            Pair pr;

            foreach (IFile file in files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                pr = Pair.Contains(sortedFiles, file.Extension);
                if (pr != null)
                {
                    pr.files.Add(file.Id);
                }
                else
                {
                    pr = new Pair();
                    pr.type = file.Extension;
                    pr.files.Add(file.Id);
                    sortedFiles.Add(pr);
                }
            }



            foreach (Pair pair in sortedFiles)
            {
                foreach (ulong fileID in pair.files)
                {
                    IFile file = files.GetFile(fileID);
                    if (file.fileType != Store.SpecialFileTypes.UNKNOWN)
                    {
                        string ShortDescription = file.fileType.ShortTypeDescription();
                        string Extension = dFileFormat[ShortDescription][6];
                        if (Extension == "")
                        {
                            Extension = "<Пусто>";
                        }
                        string Description = dFileFormat[ShortDescription][5];
                        string CheckSource = "Нет";
                        string CheckBinary = "Нет";
                        if ((file.fileType.Contains() & enTypeOfContents.TEXTSOURCES) == enTypeOfContents.TEXTSOURCES || (file.fileType.Contains() & enTypeOfContents.BINARYSOURCES) == enTypeOfContents.BINARYSOURCES)
                            CheckSource = "Да";
                        if ((file.fileType.Contains() & enTypeOfContents.EXECUTABLE) == enTypeOfContents.EXECUTABLE)
                            CheckBinary = "Да";
                        if (!string.IsNullOrEmpty(file.FileNameForReports))
                        {
                            reportDetailed.Add(new string[] { file.FileNameForReports, Extension, ShortDescription, Description, CheckSource, CheckBinary });
                        }
                        listCTR.Add(new classTmpReport(Extension, ShortDescription, CheckSource, CheckBinary, Description));
                    }
                    else
                    {
                        if (file.Extension != "")
                        {
                            listCTR.Add(new classTmpReport(file.Extension, "-- Неизвестный формат --", "", "", ""));
                        }
                        else
                        {
                            listCTR.Add(new classTmpReport("<Пусто>", "-- Неизвестный формат --", "", "", ""));
                        }
                    }
                }
            }

            // Заполняем таблицу даными о нераспознанных типах файлах
            foreach (IFile file in files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                if (file.fileType == Store.SpecialFileTypes.UNKNOWN)
                {
                    if (!string.IsNullOrEmpty(file.FileNameForReports))
                    {
                        reportDetailed.Add(new string[] { file.FileNameForReports, file.Extension, "-- Неизвестный формат --", "", "", "" });
                    }
                }
            }
            var listObj = (from r in listCTR
                           group r by new { ext = r.Extension, descr = r.Description, fulldescr = r.FullDescription, m1 = r.maybe1, m2 = r.maybe2 } into g
                           orderby g.Key.ext ascending
                           select new { cnt = g.Count(), ext = g.Key.ext, descr = g.Key.descr, fulldescr = g.Key.fulldescr, m1 = g.Key.m1, m2 = g.Key.m2 });

            foreach (var ob in listObj)
            {
                reportIntegral.Add(new string[] { ob.cnt.ToString(), ob.ext, ob.descr, ob.fulldescr, ob.m1, ob.m2 });
            }

            XMLReport.XMLReport.Report(System.IO.Path.Combine(path, "Типы файлов.xml"), "Типы файлов", reportDetailed);
            XMLReport.XMLReport.Report(System.IO.Path.Combine(path, "Группы файлов.xml"), "Группы файлов", reportIntegral);

        }
        
        /// <summary>
        /// Временный класс для формирования отчета
        /// </summary>
        class Pair
        {
            public List<ulong> files = new List<ulong>();
            public string type;
            public static Pair Contains(List<Pair> pairs, string tp)
            {
                foreach (Pair pair in pairs)
                {
                    if (pair.type == tp)
                    {
                        return pair;
                    }
                }
                return null;
            }

        }

        /// <summary>
        /// Временный класс для формирования отчета
        /// </summary>
        public class classTmpReport
        {
            public int Count;
            public string Extension;
            public string Description;
            public string FullDescription;
            public string maybe1;
            public string maybe2;
            public classTmpReport(string ext, string descr, string m1, string m2, string fd)
            {
                Count = 0;
                Extension = ext;
                Description = descr;
                maybe1 = m1;
                maybe2 = m2;
                FullDescription = fd;
            }
        }
    }
}
