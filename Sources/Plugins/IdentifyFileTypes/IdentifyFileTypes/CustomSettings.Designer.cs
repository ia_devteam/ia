﻿namespace IA.Plugins.Analyses.IdentifyFileTypes
{
    partial class Settings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.workplace_comboBox = new System.Windows.Forms.ComboBox();
            this.workplaceDescription_textBox = new System.Windows.Forms.TextBox();
            this.workplace_label = new System.Windows.Forms.Label();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 2;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.workplace_comboBox, 1, 0);
            this.settings_tableLayoutPanel.Controls.Add(this.workplaceDescription_textBox, 0, 1);
            this.settings_tableLayoutPanel.Controls.Add(this.workplace_label, 0, 0);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 3;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(508, 136);
            this.settings_tableLayoutPanel.TabIndex = 13;
            // 
            // workplace_comboBox
            // 
            this.workplace_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.workplace_comboBox.FormattingEnabled = true;
            this.workplace_comboBox.Location = new System.Drawing.Point(97, 3);
            this.workplace_comboBox.Name = "workplace_comboBox";
            this.workplace_comboBox.Size = new System.Drawing.Size(408, 21);
            this.workplace_comboBox.TabIndex = 2;
            this.workplace_comboBox.SelectedIndexChanged += new System.EventHandler(this.workplace_comboBox_SelectedIndexChanged);
            // 
            // workplaceDescription_textBox
            // 
            this.settings_tableLayoutPanel.SetColumnSpan(this.workplaceDescription_textBox, 2);
            this.workplaceDescription_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workplaceDescription_textBox.Location = new System.Drawing.Point(3, 29);
            this.workplaceDescription_textBox.Multiline = true;
            this.workplaceDescription_textBox.Name = "workplaceDescription_textBox";
            this.workplaceDescription_textBox.Size = new System.Drawing.Size(502, 98);
            this.workplaceDescription_textBox.TabIndex = 1;
            // 
            // workplace_label
            // 
            this.workplace_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.workplace_label.AutoSize = true;
            this.workplace_label.Location = new System.Drawing.Point(3, 6);
            this.workplace_label.Name = "workplace_label";
            this.workplace_label.Size = new System.Drawing.Size(88, 13);
            this.workplace_label.TabIndex = 0;
            this.workplace_label.Text = "Рабочее место:";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(508, 136);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.settings_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.Label workplace_label;
        private System.Windows.Forms.TextBox workplaceDescription_textBox;
        private System.Windows.Forms.ComboBox workplace_comboBox;
    }
}
