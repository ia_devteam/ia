﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using IA.Sql.DatabaseConnections;
using IA.Sql;

namespace IA.Plugins.Analyses.IdentifyFileTypes
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    public partial class Settings : UserControl, IA.Plugin.Settings
    {
        private IdentifyFileTypes plugin;
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="plugin">родительское окно</param>
        public Settings(IdentifyFileTypes plugin)
        {
            InitializeComponent();

            this.plugin = plugin;

            if (SqlConnector.SignatureDB.TestConnection())
            {
                this.plugin.WorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces;

                if (plugin.WorkPlaces.Count > 0)
                {
                    foreach (string[] sa in plugin.WorkPlaces)
                    {
                        workplace_comboBox.Items.Add(sa[1]);
                    }
                    if (PluginSettings.WorkPlace != null)
                        workplace_comboBox.Text = PluginSettings.WorkPlace;
                    else
                        workplace_comboBox.Text = plugin.WorkPlaces[0][1];
                }

                workplaceDescription_textBox.Text = PluginSettings.WorkPlaceDescription;
            }
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            if (this.plugin.WorkPlaces.Count > 0)
            {                
                List<string[]> tmp = this.plugin.WorkPlaces.Where(x => x[1] == workplace_comboBox.Text && x[2] == workplaceDescription_textBox.Text).ToList();
                if (tmp.Count == 0)
                {
                    string sPlaceName = workplace_comboBox.Text;
                    tmp = this.plugin.WorkPlaces.Where(x => x[1] == workplace_comboBox.Text).ToList();
                    if (tmp.Count == 0)
                    {
                        int maxid = this.plugin.WorkPlaces.Select(x => int.Parse(x[0])).Max() + 1;
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlacesInsert(new string[] { maxid.ToString(), workplace_comboBox.Text.ToString(), workplaceDescription_textBox.Text.ToString() });
                    }
                    else
                    {
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlacesUpdate(new string[] { workplace_comboBox.Text.ToString(), workplaceDescription_textBox.Text.ToString() });
                    }
                    this.plugin.WorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces;
                    workplace_comboBox.Items.Clear();
                    foreach (string[] sa in this.plugin.WorkPlaces)
                    {
                        workplace_comboBox.Items.Add(sa[1]);
                    }
                    workplace_comboBox.Text = sPlaceName;
                }
            }

            PluginSettings.WorkPlace = workplace_comboBox.Text;
            PluginSettings.WorkPlaceDescription = workplaceDescription_textBox.Text;
        }

        /// <summary>
        /// Обработчки выпадающего списка - установка рабочего места
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void workplace_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (workplace_comboBox.SelectedIndex == -1)
                return;

            workplaceDescription_textBox.Text = this.plugin.WorkPlaces[workplace_comboBox.SelectedIndex][2];
            
            this.plugin.WorkID = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkID = workplace_comboBox.Text;
            this.plugin.FileFormat = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat;
            this.plugin.Pattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern;                
        }
    }
}
