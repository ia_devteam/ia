using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Store;
using System.Linq;
using IA.Sql.DatabaseConnections;
using IA.Sql;

namespace IA.Plugins.Analyses.IdentifyFileTypes
{
    /// <summary>
    /// ����� ����������� �����������
    /// </summary>
    public partial class ResultControl : UserControl
    {
        Store.Storage Storage;
        internal IdentifyFileTypes Plugin;
        /// <summary>
        /// 
        /// </summary>
        public string ExtensionOnTB = "";
        
        /// <summary>
        /// ����� ��� ����������� ����������� ������ �������
        /// </summary>
        /// <param name="Storage"></param>
        /// <param name="plugin"></param>
        public ResultControl(Store.Storage Storage, IdentifyFileTypes plugin)
        {
            Plugin = plugin;
            this.Storage = Storage;
            InitializeComponent();

            refreshSignature();

            FillCategoryTree();

        }

        internal static Dictionary<string, enTypeOfContents> _contains = new Dictionary<string, enTypeOfContents>();
        // ������������ ������ FileNameForReports � Extension ��� ����
        private Dictionary<string, string> fExtCache = new Dictionary<string, string>();

        /// <summary>
        /// ��������� ������ � ������
        /// </summary>
        public void FillCategoryTree()
        {
            treeView1.Nodes.Clear();

            TreeNode NodeSources = new TreeNode();
            TreeNode NodeSourcesBin = new TreeNode();
            TreeNode NodeExes = new TreeNode();
            TreeNode NodeData = new TreeNode();
            TreeNode NodeUnknown = new TreeNode();

            NodeSources.Text = "�����, ���������� �������� ������";
            NodeSources.Name = "�����, ���������� �������� ������";
            NodeSources.Tag = (enTypeOfContents.TEXTSOURCES);
            treeView1.Nodes.Add(NodeSources);

            NodeSourcesBin.Text = "�����, ���������� �������� ������ � �������� ����";
            NodeSourcesBin.Name = "�����, ���������� �������� ������ � �������� ����";
            NodeSourcesBin.Tag = (enTypeOfContents.BINARYSOURCES);
            treeView1.Nodes.Add(NodeSourcesBin);

            NodeExes.Text = "����������� �����";
            NodeExes.Name = "����������� �����";
            NodeExes.Tag = enTypeOfContents.EXECUTABLE;
            treeView1.Nodes.Add(NodeExes);
      
            NodeData.Text = "����� � �������";
            NodeData.Name = "����� � �������";
            NodeData.Tag = enTypeOfContents.DATA;
            treeView1.Nodes.Add(NodeData);

            NodeUnknown.Text = "�������������� �����";
            NodeUnknown.Name = "�������������� �����";
            NodeUnknown.Tag = 0;
            treeView1.Nodes.Add(NodeUnknown);

            Files files = (Storage != null) ? Storage.files : null;
            if(files!=null)
                foreach (IFile file in files.EnumerateFiles(enFileKind.fileWithPrefix))
                {
                fExtCache.Add(file.FileNameForReports, file.Extension);
                if (file.fileType == Store.SpecialFileTypes.UNKNOWN)
                {
                    AddNode(NodeUnknown, file.Extension, file);
                }
                else
                {
                    enTypeOfContents temp; // =  labels.fileType.Contains();

                    if (!_contains.TryGetValue(file.fileType.ShortTypeDescription(), out temp))
                    {
                        temp = file.fileType.Contains();
                        _contains.Add(file.fileType.ShortTypeDescription(), temp);
                    }
                    if ((temp & enTypeOfContents.BINARYSOURCES) == enTypeOfContents.BINARYSOURCES)
                            AddNode(NodeSourcesBin, file.fileType.ShortTypeDescription(), file);

                    if ((temp & enTypeOfContents.TEXTSOURCES) == enTypeOfContents.TEXTSOURCES)
                            AddNode(NodeSources, file.fileType.ShortTypeDescription(), file);
                    if ((temp & enTypeOfContents.EXECUTABLE) == enTypeOfContents.EXECUTABLE)
                            AddNode(NodeExes, file.fileType.ShortTypeDescription(), file);
                    if ((temp & enTypeOfContents.DATA) == enTypeOfContents.DATA)
                            AddNode(NodeData, file.fileType.ShortTypeDescription(), file);
                    if ((temp == 0) || (temp == enTypeOfContents.DONTCONSIDERREGISTER))
                    {
                         AddNode(NodeData, file.fileType.ShortTypeDescription(), file);
                         AddNode(NodeExes, file.fileType.ShortTypeDescription(), file);
                         AddNode(NodeSources, file.fileType.ShortTypeDescription(), file);
                    }
                }
            }
            listBox1.Items.Clear();
        }

        /// <summary>
        /// �������� ���� � ������
        /// </summary>
        /// <param name="Node"></param>
        /// <param name="type"></param>
        /// <param name="file"></param>
        private static void AddNode(TreeNode Node, string type, IFile file)
        {
            TreeNode[] nodes = Node.Nodes.Find(type, true);
            if (nodes.Length != 0)
                ((List<IFile>)nodes[0].Tag).Add(file);
            else
            {
                TreeNode NodeTemp = new TreeNode();
                NodeTemp.Text = type;
                NodeTemp.Name = type;
                NodeTemp.Tag = new List<IFile>();
                ((List<IFile>)NodeTemp.Tag).Add(file);
                Node.Nodes.Add(NodeTemp);
            }
        }

        /// <summary>
        /// ��������� ������ �������, ���������������� ���� ������
        /// </summary>
        /// <param name="node"></param>
        public void FillFileTree(TreeNode node)
        {
            if (node != null)
            {
                listBox1.Items.Clear();
                listBox1.Items.AddRange(((List<IFile>)node.Tag).ToArray());
            }
        }

        /// <summary>
        /// ������ ��� ���������� ������ ������, � ����������� �� ������ ����������� �������� ������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //listBox2.SelectedIndex = -1;
            textBox2.Text = "";
            label2.Text = "";
            label3.Text = "";
            richTextBox1.Text = "";
            //splitContainer5.Panel2Collapsed = true;
            if (treeView1.SelectedNode != null && treeView1.SelectedNode.Level == 1)
            {
                textBox1.Text = ":";
                textBox1.Text = ((List<IFile>)treeView1.SelectedNode.Tag).First().Extension;
                FillFileTree(treeView1.SelectedNode);
            }
            if (treeView1.SelectedNode.Level == 0)
            {
                listBox1.Items.Clear();
            }
            //splitContainer5.Panel2Collapsed = true;
        }

        /// <summary>
        /// �������� ��� ��������� ����� � ������ ������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                string f_name = listBox1.Items[listBox1.SelectedIndex].ToString();
                textBox1.Text = fExtCache.ContainsKey(f_name) ? fExtCache[f_name] : "";
                byte[] firstBytes = null;

                bool TextFile = false;
                if (listBox1.SelectedItem != null)
                    TextFile = CheckTextOrNot.TextOrNot(((IFile)listBox1.SelectedItem).FullFileName_Original, out firstBytes);

                richTextBox1.Text = CheckTextOrNot.DefaultEncoding(firstBytes);
                IA.Common.DynamicByteProvider dbp = new IA.Common.DynamicByteProvider(firstBytes);
                hexBoxView1.ByteProvider = dbp;

                if (TextFile)
                {
                    splitContainer4.Panel2Collapsed = false;
                    splitContainer4.Panel1Collapsed = true;

                    richTextBox1.Visible = true;
                    hexBoxView1.Visible = false;
                    button3.Text = "HEX MODE";

                }
                else
                {
                    splitContainer4.Panel1Collapsed = false;
                    splitContainer4.Panel2Collapsed = true;

                    richTextBox1.Visible = false;
                    hexBoxView1.Visible = true;
                    button3.Text = "TEXT MODE";
                }
                if (listBox1.SelectedItem != null)
                {
                    //��������� ���������� � ����
                    //IFile label = (IFile)listBox1.SelectedItem;

                    if (IdentifyFileTypes.results.Count > 0)
                    {
                        int ind = IdentifyFileTypes.results.BinarySearch(new FileIdentified(((IFile)listBox1.SelectedItem).Id, new List<FileType>()));
                        FileIdentified res = IdentifyFileTypes.results[ind];
                        //listBox2.SelectedIndex = -1;
                        if (res.Types != null)
                        {
                            if (res.Types.Count == 1 && res.Types[0] == SpecialFileTypes.UNKNOWN)
                                textBox2.Text = "�� ������� ��������������� �����. " + (TextFile ? "��������� ����" : "�������� ����");
                            else
                            {
                                textBox2.Text = "������������ �����: " + res.Types.Count + ". " + (TextFile ? "��������� ����" : "�������� ����");
                                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                                {
                                    dataGridView1.Rows[i].Cells[4].Style.BackColor = DataGridView.DefaultBackColor;
                                }
                                foreach (FileType tp in res.Types)
                                {
                                    string sid = tp.ToInt32().ToString();
                                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                                    {
                                        if (sid == dataGridView1.Rows[i].Cells[1].Value.ToString())
                                            dataGridView1.Rows[i].Cells[4].Style.BackColor = Color.Yellow;
                                    }

                                }
                            }
                        }
                        else
                            textBox2.Text = "������ ��������� �����. " + (TextFile ? "��������� ����" : "�������� ����");
                    }
                }
            }
        }
        /// <summary>
        /// ������ �������� ����� � ������������� ����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetButton_Click(object sender, EventArgs e)
        {
            
            List<object> toDelete = new List<object>();
            if (listBox1.SelectedIndices.Count != 0 && (dataGridView1.SelectedCells.Count>0))
            {
                foreach (IFile file in listBox1.SelectedItems)
                {
                    bool TextFile = true;
                    if (CheckText.Checked)
                    {
                        byte[] arrA;
                        TextFile = CheckTextOrNot.TextOrNot(file.FullFileName_Original,out arrA);
                    }
                    int ind = dataGridView1.SelectedCells[0].RowIndex;
                    string[] sa = new string[7];
                    sa[0]=dataGridView1.Rows[ind].Cells[0].Value.ToString();
                    sa[1]=dataGridView1.Rows[ind].Cells[1].Value.ToString();
                    sa[2]=dataGridView1.Rows[ind].Cells[2].Value.ToString();
                    sa[3]=dataGridView1.Rows[ind].Cells[3].Value.ToString();
                    sa[4]=dataGridView1.Rows[ind].Cells[4].Value.ToString();
                    sa[5]=dataGridView1.Rows[ind].Cells[5].Value.ToString();
                    sa[6]=dataGridView1.Rows[ind].Cells[6].Value.ToString();

                    FileType ft = new FileType(sa, TextFile);// (listBox3.SelectedIndex != -1) ? (FileType)listBox3.SelectedItem : ((fTypeInList)listBox2.SelectedItem).Type;
                    if (!CheckText.Checked  ||
                        ft.Contains() == enTypeOfContents.DATA ||
                        !TextFile && ((ft.Contains() & enTypeOfContents.EXECUTABLE) == enTypeOfContents.EXECUTABLE || 
                                      (ft.Contains() & enTypeOfContents.BINARYSOURCES) == enTypeOfContents.BINARYSOURCES || 
                                       MessageBox.Show("���� ���������������� �������� ����������� ����������. ��������� ������ ��������� ���?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)  ||
                        TextFile && ((ft.Contains() & enTypeOfContents.TEXTSOURCES) == enTypeOfContents.TEXTSOURCES || 
                                      MessageBox.Show("���� ���������������� �������� ��������� ����������. ��������� ������ ����������� ���?", "", MessageBoxButtons.YesNo) == DialogResult.Yes))
                    {
                        file.fileType = new Store.FileType(ft.ShortTypeDescription());
                        ((List<IFile>)treeView1.SelectedNode.Tag).Remove(file);

                        toDelete.Add(file);

                        if ((file.fileType.Contains() & enTypeOfContents.BINARYSOURCES) == enTypeOfContents.BINARYSOURCES)
                                AddNode(treeView1.Nodes[1], file.fileType.ShortTypeDescription(), file);
                        if ((file.fileType.Contains() & enTypeOfContents.TEXTSOURCES) ==  enTypeOfContents.TEXTSOURCES)
                                AddNode(treeView1.Nodes[0], file.fileType.ShortTypeDescription(), file);
                        if ((file.fileType.Contains() & enTypeOfContents.EXECUTABLE) == enTypeOfContents.EXECUTABLE)
                                AddNode(treeView1.Nodes[2], file.fileType.ShortTypeDescription(), file);
                        if ((file.fileType.Contains() & enTypeOfContents.DATA) == enTypeOfContents.DATA)
                                AddNode(treeView1.Nodes[3], file.fileType.ShortTypeDescription(), file);
                        if (file.fileType.Contains() == 0)
                        {
                            AddNode(treeView1.Nodes[0], file.fileType.ShortTypeDescription(), file);
                            AddNode(treeView1.Nodes[1], file.fileType.ShortTypeDescription(), file);
                            AddNode(treeView1.Nodes[2], file.fileType.ShortTypeDescription(), file);
                            AddNode(treeView1.Nodes[3], file.fileType.ShortTypeDescription(), file);
                        }
                    }
                }
                if (((List<IFile>)treeView1.SelectedNode.Tag).Count == 0)
                {
                    TreeNode node = treeView1.SelectedNode.Parent;
                    treeView1.Nodes.Remove(treeView1.SelectedNode);
                    treeView1.SelectedNode = node;
                    treeView1.Select();
                }

            }
            foreach (object obj in toDelete)
            {
                listBox1.Items.Remove(obj);
            }       
             
        }

        /// <summary>
        /// �������� ����� �������� � �����
        /// </summary>
        /// <param name="id"></param>
        /// <param name="wid"></param>
        /// <param name="act"></param>
        /// <param name="widcurrent"></param>
        /// <param name="tabname"></param>
        public void newTabPage(string id,string wid, string act, string widcurrent, string tabname)
        {
            System.Windows.Forms.UserControl NewRecordWindow = new NewSignatureRecord(this, id, wid, act, widcurrent); //�������� ����� � ����������������� �����������

            tabControl1.TabPages.Add(tabname);
            tabControl1.SelectTab(tabControl1.TabPages.Count - 1); // ������� �� ����� ��������

            NewRecordWindow.SetBounds(0, 0, tabControl1.Size.Width - 10, tabControl1.Size.Height - 20); //��������� ����� ��� ������� ����
            NewRecordWindow.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;

            tabControl1.TabPages[tabControl1.TabPages.Count - 1].Controls.Add(NewRecordWindow); // ��������� ����� � ����
            tabControl1.TabPages[0].Hide();
        }

        /// <summary>
        /// ������ "����� ������"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click_1(object sender, EventArgs e)
        {
            string id = "0";
            string wid = "1";
            ExtensionOnTB = textBox1.Text;
            if (dataGridView1.SelectedCells.Count > 0)
            {
                id = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[0].Value.ToString();
                wid = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[2].Value.ToString();
            }

            newTabPage(id, wid, "New", Plugin.WorkID, "����� ������");
            Plugin.FileFormat = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat;
            Plugin.Pattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern;
        }

        /// <summary>
        /// ������ "������������� ������"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count > 0)
            {
                string id = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[0].Value.ToString();
                string wid = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[2].Value.ToString();
                newTabPage(id, wid, "Update", Plugin.WorkID, "������������� ������");
                Plugin.FileFormat = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat;
                Plugin.Pattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern;
           }
        }

        /// <summary>
        /// ���������� �����
        /// </summary>
        public void refreshSignature()
        {
            if (SqlConnector.SignatureDB.TestConnection())
            {
                //string sourcePath = Properties.Settings.Default.sourcePath;
                string savedWorkPlace = Properties.Settings.Default.WorkPlace;
                Store.PluginSettings storageSettings = (Storage != null) ? Storage.pluginSettings : null;
                Store.Table.IBufferReader reader = (storageSettings != null) ? (Store.Table.IBufferReader)storageSettings.LoadSettings((ulong)(PluginSettings.ID)) : null;
                if (reader != null)
                {
                    //sourcePath = reader.GetString();
                    savedWorkPlace = reader.GetString();
                }

                Plugin.FileFormat = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat;
                Plugin.Pattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern;
                Plugin.WorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces;

                if (savedWorkPlace != null)
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkID = savedWorkPlace;
                else
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkID = IdentifyFileTypes.DefaultWorkPlaceName;
                Plugin.WorkID = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkID;
                Plugin.lsExFileFormat = new List<string[]>();
                List<string> lsExtension;
                List<string> lsFFID;
                Files files = (Storage != null) ? Storage.files : null;
                if (Plugin.lsAppExt == null)
                {
                    Plugin.lsAppExt = new List<string>();
                    if (files != null)
                        foreach (IFile file in files.EnumerateFiles(enFileKind.fileWithPrefix))
                        {
                            string ext = file.Extension.ToLower();
                            if (!Plugin.lsAppExt.Contains(ext))
                                Plugin.lsAppExt.Add(ext);
                        }
                }
                foreach (string[] sa in Plugin.FileFormat)
                {
                    if (sa[6] == "")
                    {
                        Plugin.lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], "" });
                    }
                    else
                    {
                        string ss6 = sa[6].ToLower();
                        if (ss6.IndexOf('.') >= 0)
                        {
                            // ��� �����, ������� "." � ������ ��� ����� ������ - ������� ��
                            if (ss6.StartsWith("."))
                                ss6 = ss6.Substring(1);
                            if (ss6.EndsWith("."))
                                ss6 = ss6.Substring(0, ss6.Length - 1);

                            string[] sb = ss6.Split('.');
                            foreach (string s in sb)
                            {
                                if (Plugin.lsAppExt.Contains(s))
                                    Plugin.lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], s });
                            }
                        }
                        else
                            //if (Plugin.lsAppExt.Contains(sa[6]))
                            Plugin.lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], ss6 });
                    }
                }
                lsExtension = Plugin.lsExFileFormat.Where(x => (Plugin.Pattern.Where(y => y[1] == x[1] && y[2] == x[2]).Count() > 0)).Where(x => (x[2] == "1" || x[2] == Plugin.WorkID)).OrderBy(x => x[6]).Select(x => x[6]).Distinct().ToList();

                lsFFID = Plugin.lsExFileFormat.Where(x => (x[2] == "1" || x[2] == Plugin.WorkID)).Select(x => x[1]).Distinct().ToList();
                Plugin.lsGoodExt = new List<string>();
                Plugin.dictFormats = new Dictionary<string, List<string[]>>();
                List<string> lsBadExt = new List<string>();
                foreach (string s in Plugin.lsAppExt)
                {
                    if (lsExtension.Contains(s))
                    {
                        Plugin.lsGoodExt.Add(s);
                    }
                    else
                    {
                        lsBadExt.Add(s);
                    }
                }
                Plugin.dictFormats = new Dictionary<string, List<string[]>>();
                Plugin.dictPatterns = new Dictionary<string, List<string[]>>();
                foreach (string s in Plugin.lsGoodExt)
                {
                    Plugin.dictFormats.Add(s, new List<string[]>());
                    Plugin.dictFormats[s] = Plugin.lsExFileFormat.Where(x => x[6] == s && (x[2] == "1" || x[2] == Plugin.WorkID)).Distinct().ToList();
                }
                foreach (string s in lsFFID)
                {
                    Plugin.dictPatterns.Add(s, new List<string[]>());
                    Plugin.dictPatterns[s] = Plugin.Pattern.Where(x => x[1] == s && (x[2] == "1" || x[2] == Plugin.WorkID)).Distinct().ToList();
                }
                string savetext = textBox1.Text;
                textBox1.Text = ":";
                textBox1.Text = savetext;
            }
        }

        /// <summary>
        /// ������ "�������� ���� ����"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                byte[] firstBytes=null;
                System.IO.BinaryReader br = null;
                System.IO.FileStream fs = null;
                long length = 0;
                try
                {
                    fs = new System.IO.FileStream(((IFile)listBox1.SelectedItem).FullFileName_Original, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    br = new System.IO.BinaryReader(fs);
                    length = fs.Length;
                    if (length > Int32.MaxValue)
                        length = Int32.MaxValue;
                    firstBytes = new byte[length];
                    length = br.Read(firstBytes, 0, (int)length);
                }
                catch (Exception ex)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, ex.Message, true);
                    return;
                }
                finally
                {
                    if (fs != null)
                        fs.Close();
                }
                richTextBox1.Text = CheckTextOrNot.DefaultEncoding(firstBytes);
                IA.Common.DynamicByteProvider dbp = new IA.Common.DynamicByteProvider(firstBytes);
                hexBoxView1.ByteProvider = dbp;

            }
        }

        /// <summary>
        /// �������� �� �������� ����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultControl_Leave(object sender, EventArgs e)
        {
            if (hexBoxView1.ByteProvider != null)
            {
                IDisposable byteProvider = hexBoxView1.ByteProvider as IDisposable;
                if (byteProvider != null)
                    byteProvider.Dispose();
                hexBoxView1.ByteProvider = null;
            }
        }

        /// <summary>
        /// ������ - ������������� "TEXT MODE"/"HEX MODE"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click_1(object sender, EventArgs e)
        {
            if (button3.Text == "HEX MODE")
            {
                splitContainer4.Panel1Collapsed = false;
                splitContainer4.Panel2Collapsed = true;
                richTextBox1.Visible = false;
                hexBoxView1.Visible = true;
                button3.Text = "TEXT MODE";
            }
            else
            {
                splitContainer4.Panel2Collapsed = false;
                splitContainer4.Panel1Collapsed = true;
                richTextBox1.Visible = true;
                hexBoxView1.Visible = false;
                button3.Text = "HEX MODE";
            }
            Application.DoEvents();
        }

        /// <summary>
        /// ������ "������� ���"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count > 0)
            {
                string id = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[0].Value.ToString();
                string wid = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex].Cells[2].Value.ToString();
                newTabPage(id, wid, "Delete", Plugin.WorkID, "������� ������");
                Plugin.FileFormat = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat;
                Plugin.Pattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern;

            }
        }

        /// <summary>
        /// �������� ��� ��������� ���������� �����: ������ ������ ����� � �������� ������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1304:SpecifyCultureInfo", MessageId = "System.String.ToLower")]
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ExtensionOnTB = textBox1.Text;
            dataGridView1.Rows.Clear();
            List<string> lst = Plugin.lsExFileFormat.Where(x => x[6].ToLower()==textBox1.Text.ToLower() && (x[2]=="1"||x[2]==Plugin.WorkID)).Select(x => x[0]).Distinct().ToList();
            List<string[]> lsCurrFF = Plugin.FileFormat.Where(x => lst.Contains(x[0])).ToList();
            foreach (string[] sa in lsCurrFF)
            {
                dataGridView1.Rows.Add(sa);
            }
            if (dataGridView1.Rows.Count > 0)
            {
                dataGridView1.Rows[0].Cells[4].Selected = false;
                dataGridView1.Rows[0].Cells[6].Selected = true;
            }

        }

        /// <summary>
        /// ��������� ������ � ������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A && e.Control)
            {
                for (int i = 0; i < listBox1.Items.Count; ++i)
                {
                    listBox1.SelectedIndices.Add(i);
                }
            }
        }

        /// <summary>
        /// �������� ��� ��������� ��������� � ������� ����� ������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView2.Rows.Clear();
            if (dataGridView1.SelectedCells.Count > 0)
            {
                int index = dataGridView1.SelectedCells[0].RowIndex;
                string FileFormatID = NewSignatureRecord.GetDGVValue(dataGridView1, index, 1);
                string SelectedWorkID = NewSignatureRecord.GetDGVValue(dataGridView1, index, 2);
                if (SelectedWorkID == "1")
                {
                    button4.Enabled = false;
                }
                else
                {
                    button4.Enabled = true;
                }
                List<string[]> lsSelectedP = Plugin.Pattern.Where(x => ((x[1] == FileFormatID) && (x[2] == SelectedWorkID))).ToList();
                foreach (string[] sa in lsSelectedP)
                    dataGridView2.Rows.Add(sa);
                int typeOF;
                string sTypeOF = "";
                if (!int.TryParse(NewSignatureRecord.GetDGVValue(dataGridView1, index, 3), out typeOF))
                {
                    typeOF = 0;
                }
                if ((typeOF & (int)enTypeOfContents.DATA) == (int)enTypeOfContents.DATA)
                {
                    sTypeOF += "���� � �������";
                }
                else
                {
                    if ((typeOF & (int)enTypeOfContents.EXECUTABLE) == (int)enTypeOfContents.EXECUTABLE)
                    {
                        sTypeOF += "����������� ����";
                    }
                    else
                    {
                        if ((typeOF & (int)enTypeOfContents.TEXTSOURCES) == (int)enTypeOfContents.TEXTSOURCES)
                        {
                            sTypeOF += "�������� ���� � ��������� �������";
                        }
                        else
                        {
                            if ((typeOF & (int)enTypeOfContents.BINARYSOURCES) == (int)enTypeOfContents.BINARYSOURCES)
                            {
                                sTypeOF += "�������� ���� � �������� �������";
                            }
                        }
                    }
                }
                if ((typeOF & (int)enTypeOfContents.DONTCONSIDERREGISTER) == (int)enTypeOfContents.DONTCONSIDERREGISTER)
                {
                    sTypeOF += ". ���������� ��� ����� ��������";
                }
                string wpl = Plugin.WorkPlaces.Where(x => x[0] == SelectedWorkID).Select(x => x[1]).First();
                label2.Text = wpl;
                label3.Text = sTypeOF;
            }
        }

    }
}
