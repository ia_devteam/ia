using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using Store;
using Store.Table;

using IA.Extensions;
using IA.Sql.DatabaseConnections;
using IA.Sql;

namespace IA.Plugins.Analyses.IdentifyFileTypes
{

    /// <summary>
    /// Класс настроек плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Идентификация файлов";
        
        /// <summary>
        /// Иденитификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.IDENTIFY_FILE_TYPES;

        /// <summary>
        /// Наименования тасков плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка файлов")]
            FILES_IDENTIFYING
        };

        //Настройки
        internal static string WorkPlace;
        internal static string WorkPlaceDescription;

    }

    /// <summary>
    /// плагин IdentifyFileTypes
    /// </summary>
    public sealed class IdentifyFileTypes : Plugin.Interface
    {
        /// <summary>
        /// название базового рабочего места
        /// </summary>
        public const string DefaultWorkPlaceName = "Базовое рабочее место";

        /// <summary>
        /// рабочие места
        /// </summary>
        public List<string[]> WorkPlaces = null;

        /// <summary>
        /// полные описания форматов
        /// </summary>
        public List<string[]> FileFormat = null;

        /// <summary>
        /// шаблон
        /// </summary>
        public List<string[]> Pattern = null;

        /// <summary>
        /// список расширений файлов
        /// </summary>
        public List<string> lsAppExt = null;

        /// <summary>
        /// 
        /// </summary>
        public string WorkID = "1";
        const string Unknown = "420";

        /// <summary>
        /// 
        /// </summary>
        public static string[] ArrUnkn;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<string[]>> dictPatterns;
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<string[]>> dictFormats;
        /// <summary>
        /// 
        /// </summary>
        public List<string> lsGoodExt;
        /// <summary>
        /// 
        /// </summary>
        public List<string[]> lsExFileFormat;

        /// <summary>
        /// Хранилище
        /// </summary>
        Store.Storage storage;

        /// <summary>
        /// Таблица файлов хранилища
        /// </summary>
        Files files;

        /// <summary>
        /// Настройки
        /// </summary>
        Settings settings;

        /// <summary>
        /// результаты работы
        /// </summary>
        static public List<FileIdentified> results = new List<FileIdentified>();

        /// <summary>
        /// Идентификация типа конкретного файла
        /// </summary>
        /// <param name="file">Исследуемый файл</param>
        /// <returns>Список типов, соответствующий файлу</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public List<FileType> Analyse(IFile file)
        {
            try
            {
                if (file != null)
                {
                    List<FileType> result = new List<FileType>();
                    List<Type> SuitableTypes = new List<Type>();
                    Type type;
                    int contents;

                    byte[] firstBytes;
                    List<string> lsIDs;
                    if(dictFormats.Keys.Contains(file.Extension.ToLower()))
                        lsIDs = dictFormats[file.Extension.ToLower()].Select(x => x[1]).Distinct().ToList();
                    else
                        lsIDs = new List<string>();

                    bool boolTextFile = CheckTextOrNot.TextOrNot(file.FullFileName_Original, out firstBytes);
                    string firstContext = BitConverter.ToString(firstBytes).Replace("-", "");
                    foreach (string typeID in lsIDs)
                    {
                        type = new Type(storage, dictFormats[file.Extension.ToLower()].Where(x=>x[1]==typeID).First(),boolTextFile);
                        contents = int.Parse(dictFormats[file.Extension.ToLower()].Where(x => x[1] == typeID).First()[3]);
                        if ((contents & (int)enTypeOfContents.TEXTSOURCES) == (int)enTypeOfContents.TEXTSOURCES && boolTextFile || (contents & (int)enTypeOfContents.BINARYSOURCES) == (int)enTypeOfContents.BINARYSOURCES && !boolTextFile ||
                            (contents & (int)enTypeOfContents.EXECUTABLE) == (int)enTypeOfContents.EXECUTABLE && !boolTextFile || (contents & (int)enTypeOfContents.DATA) == (int)enTypeOfContents.DATA || contents == 0)
                        {
                            // Разбор типов шаблонов
                            List<string[]> lsCurrPatt = dictPatterns[typeID];
                            bool bKnown = false;
                            foreach (string[] sa in lsCurrPatt)
                            {
                                switch (sa[3].ToLower())
                                {
                                    case "binary":
                                        if (sa[4].ToLower() == "begins" && sa[5].Length > 0)
                                        {
                                            if (firstContext.StartsWith(sa[5]))
                                            {
                                                bKnown = true;
                                            }
                                            if (sa[6].ToLower() == "ends" && sa[7].Length > 0)
                                            {
                                                bKnown = false;
                                                // Здесь нужно прочитать последние байты файла и сличить
                                                string lastContent = CheckTextOrNot.LastBytes(file.FullFileName_Original);
                                                if (lastContent.EndsWith(sa[7]))
                                                {
                                                    bKnown = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (sa[6].ToLower() == "ends" && sa[7].Length > 0)
                                            {
                                                // Здесь нужно прочитать последние байты файла и сличить
                                                string lastContent = CheckTextOrNot.LastBytes(file.FullFileName_Original);
                                                if (firstContext.EndsWith(sa[7]))
                                                {
                                                    bKnown = true;
                                                }
                                            }
                                        }
                                        break;
                                    case "text":
                                        if (sa[4].ToLower() == "contains" && sa[5].Length > 0)
                                        {
                                            double weight, sum = 0;
                                            if (sa[6].ToLower() == "weight" && double.TryParse(sa[7], out weight))
                                            {
                                                string textFile = CheckTextOrNot.DefaultEncoding(firstBytes).ToLower();
                                                string[] sb = sa[5].ToLower().Split('~').Where(x=>x!="").ToArray();
                                                foreach(string ss in sb)
                                                {
                                                    int index1 = textFile.IndexOf(ss); 
                                                    if (index1!=-1)
                                                        sum += weight;
                                                    if (sum >= 100)
                                                    {
                                                        bKnown = true;
                                                        break;
                                                    }
                                                }
                                                // Дочитываем файл ???
                                            }
                                        }
                                        break;
                                    case "xml":
                                        try
                                        {
                                            XmlDocument doc = new XmlDocument();
                                            doc.Load(file.FullFileName_Original);
                                            if (sa[4].ToLower() != "contains" || sa[5].Length == 0)
                                            {
                                                bKnown = true;
                                            }
                                        }
                                        catch (XmlException)
                                        {
                                            bKnown = false;
                                        }

                                        if (sa[4].ToLower() == "contains" && sa[5].Length > 0)
                                        {
                                            bKnown = false;
                                            double weight, sum = 0;
                                            if (sa[6].ToLower() == "weight" && double.TryParse(sa[7], out weight))
                                            {
                                                string textFile = CheckTextOrNot.DefaultEncoding(firstBytes).ToLower();
                                                string[] sb = sa[5].ToLower().Split('~').Where(x => x != "").ToArray();
                                                foreach (string ss in sb)
                                                {
                                                    if (textFile.Contains(ss))
                                                        sum += weight;
                                                    if (sum >= 100)
                                                    {
                                                        bKnown = true;
                                                        break;
                                                    }
                                                }
                                                // дочитываем файл ???
                                            }

                                        }
                                        break;
                                    case "executable":
                                        if (sa[4].ToLower() == "method" && sa[5].Length > 0 && sa[6].ToLower() == "checktype" && sa[7].Length > 0)
                                        {
                                            try
                                            {
                                                if (ExecuteExtProg(sa[5], file.FullFileName_Original + " " + sa[7]) == 0)
                                                    bKnown = true;
                                            }
                                            catch
                                            {
                                                bKnown = false;
                                            }
                                        }
                                        break;
                                }
                                if (bKnown)
                                {
                                    break;
                                }

                            }
                            if (bKnown)
                            {
                                SuitableTypes.Add(type);
                            }

                        }
                    }

                    if (SuitableTypes.Count == 0)
                    {
                        result.Add(SpecialFileTypes.UNKNOWN);
                        return result;
                    }

                    foreach (Type elem in SuitableTypes)
                    {
                        result.Add(elem.fType);
                    }


                    if (file == null || file.fileType == Store.SpecialFileTypes.UNKNOWN || file.fileType == null)
                    {
                        if (result.Count > 1 )
                        {
                            file.fileType = Store.SpecialFileTypes.UNKNOWN;
                        }
                        else
                        {
                            if (result.Count == 1)
                            {
                                file.fileType = new Store.FileType(result[0].ShortTypeDescription());
                            }
                            else
                            {
                                file.fileType = Store.SpecialFileTypes.UNKNOWN;
                            }
                        }
                    }
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                string error = ex.Message;
                return null;
            }
        }

        /// <summary>
        /// Исполнение внешней программы
        /// </summary>
        /// <param name="extprog"></param>
        /// <param name="progargs"></param>
        /// <returns></returns>
        public static int ExecuteExtProg(string extprog, string progargs)
        {
            if (!System.IO.File.Exists(extprog))
            {
                throw new NotImplementedException("Программа " + extprog + " не найдена!");
            }

            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = extprog;
            cmdStartInfo.Arguments = progargs;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = cmdStartInfo;

            p.Start();
            p.WaitForExit();
            return p.ExitCode;
        }

        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return  Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | 
                                Plugin.Capabilities.RESULT_WINDOW | 
                                    Plugin.Capabilities.REPORTS | 
                                        Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.WorkPlace = Properties.Settings.Default.WorkPlace;
            PluginSettings.WorkPlaceDescription = Properties.Settings.Default.WorkPlaceDescription;

            files = storage.files;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.WorkPlace = settingsBuffer.GetString();
            PluginSettings.WorkPlaceDescription = settingsBuffer.GetString();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.WorkPlace = DefaultWorkPlaceName;
            PluginSettings.WorkPlaceDescription = String.Empty;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_IDENTIFYING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, files.Count());
            
            FileFormat = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat;
            Pattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern;
            WorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces;
            if (!string.IsNullOrWhiteSpace(PluginSettings.WorkPlace))
                ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkID = PluginSettings.WorkPlace;
            else
                throw new Exception("Плагин запущен, но не инициализирован (WorkPlace пуст)");
            WorkID = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkID;

            ArrUnkn = FileFormat.Where(x => x[1] == Unknown).First();

            results.Clear();

            dictPatterns = new Dictionary<string, List<string[]>>();
            lsAppExt = new List<string>();

            //
            foreach (IFile file in files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                string ext = file.Extension.ToLower();
                if (!lsAppExt.Contains(ext))
                    lsAppExt.Add(ext);
            }
             
            lsExFileFormat = new List<string[]>();
            List<string> lsExtension;
            List<string> lsFFID;
            foreach (string[] sa in FileFormat)
            {
                if (sa[6] == "")
                {
                    lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], "" });
                }
                else
                {
                    string ss6 = sa[6].ToLower();
                    if (ss6.IndexOf('.') >= 0)
                    {
                        // Для строк, имеющих "." в начале или конце строки - убираем их
                        if (ss6.StartsWith("."))
                            ss6 = ss6.Substring(1);
                        if(ss6.EndsWith("."))
                            ss6 = ss6.Substring(0,ss6.Length-1);
                        string sext = ss6;
                        string[] sb = ss6.Split('.');
                        foreach (string s in sb)
                        {
                            if (s != "")
                            {
                                if (lsAppExt.Contains(s))
                                    lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], s });
                            }
                            else
                            {
                                lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], s });
                            }

                        }
                    }
                    else
                        if (lsAppExt.Contains(ss6))
                            lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], ss6 });
                }
            }
            lsExtension = lsExFileFormat.Where(x => (Pattern.Where(y => y[1] == x[1] && y[2] == x[2]).Count() > 0)).Where(x => (x[2] == "1" || x[2] == WorkID)).OrderBy(x => x[6]).Select(x => x[6]).Distinct().ToList();
            
            lsFFID = lsExFileFormat.Where(x => (x[2] == "1" || x[2] == WorkID)).Select(x => x[1]).Distinct().ToList();
            lsGoodExt = new List<string>();
            dictFormats = new Dictionary<string, List<string[]>>();
            List<string> lsBadExt = new List<string>();
            
            foreach (string s in lsAppExt)
            {
                if (lsExtension.Contains(s))
                {
                    lsGoodExt.Add(s);
                }
                else
                {
                    lsBadExt.Add(s);
                }
            }
              
            foreach (string s in lsAppExt)
            {
                dictFormats.Add(s,new List<string[]>());
                dictFormats[s] = lsExFileFormat.Where(x => x[6] == s && (x[2] == "1" || x[2] == WorkID)).Distinct().ToList();
            }
            foreach (string s in lsFFID)
            {
                dictPatterns.Add(s, new List<string[]>());
                dictPatterns[s] = Pattern.Where(x => x[1]==s && (x[2] == "1" || x[2] == WorkID)).Distinct().ToList();
            }

            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_IDENTIFYING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, files.Count());
            ulong MonStage = 0;
            foreach (IFile file in files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                results.Add(new FileIdentified(file.Id, Analyse(file)));
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_IDENTIFYING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++MonStage);
                IA.Monitor.Status.ShowMessage(Monitor.Status.MessageType.PERMANENT, "Обработка файла: " + file.FullFileName_Original);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.WorkPlace = PluginSettings.WorkPlace;
            Properties.Settings.Default.WorkPlaceDescription = PluginSettings.WorkPlaceDescription;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.WorkPlace);
            settingsBuffer.Add(PluginSettings.WorkPlaceDescription);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (string.IsNullOrEmpty(PluginSettings.WorkPlace))
            {
                message = "Не указано рабочее место.";
                return false;
            }
            if (string.IsNullOrEmpty(PluginSettings.WorkPlaceDescription) && PluginSettings.WorkPlace != DefaultWorkPlaceName)
            {
                message = "Не описано рабочее место.";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            // Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            Log.GenerateReports1(reportsDirectoryPath, files);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>() { new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.FILES_IDENTIFYING.Description()) };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                settings = new Settings(this);
                return settings;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                return new ResultControl(storage, this);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        #endregion
    }

    /// <summary>
    /// распознанный файл
    /// </summary>
    public class FileIdentified : IComparable
    {
        ulong ID;
        /// <summary>
        /// подходящие типы
        /// </summary>
        public List<FileType> Types;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="id"></param>
        /// <param name="types"></param>
        public FileIdentified(ulong id, List<FileType> types)
        {
            ID = id;
            Types = types;
        }

        #region Члены IComparable

        /// <summary>
        /// сравнение
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            FileIdentified Obj = (FileIdentified)obj;
            if (ID == (Obj).ID)
            {
                return 0;
            }
            else
            {
                if (ID > Obj.ID)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }

        #endregion
    }
}
