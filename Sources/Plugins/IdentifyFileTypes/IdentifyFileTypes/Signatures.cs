﻿using System;
using System.Collections.Generic;
using Store;
using System.Linq;


namespace IA.Plugins.Analyses.IdentifyFileTypes
{


    /// <summary>
    /// Класс специальных типов файлов
    /// </summary>
    public static class SpecialFileTypes
    {
        /// <summary>
        /// неизвестный тип
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "UNKNOWN")]
        public static FileType UNKNOWN
        {
            get
            {
                FileType type = new FileType(IdentifyFileTypes.ArrUnkn, false);
                return type;
            }
        }
    }
    
    /// <summary>
    /// Класс типов файлов
    /// </summary>
    public class FileType
    {
        /// <summary>
        /// Идентификатор типа
        /// </summary>
        UInt32 TypeID;
        
        /// <summary>
        /// Файл является текстовым
        /// </summary>
        public bool isText;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="sa">Массив строк из базы данных, характеризующий тип</param>
        /// <param name="istext">true, если файл текстовый</param>
        public FileType(string[] sa, bool istext)
        {
            if (!UInt32.TryParse(sa[1], out TypeID))
            {
                TypeID = 0;
            }
            if (sa[5] != null)
                typeDescription = sa[5];
            else
                typeDescription = "";
            int t;
            if (int.TryParse(sa[3], out t))
            {
                contains = (enTypeOfContents)t;
            }
            else
                contains = 0;
            if (sa[6] != null)
                extensions = sa[6].Split(' ').ToList();
            else
                extensions = new List<string>();
            //this.storage = storage;
            if (sa[4] != null)
                shortTypeDescription = sa[4];
            else
                shortTypeDescription = "";
            isText = istext;
        }
 
        /// <summary>
        /// Идентификатор типа
        /// </summary>
        /// <returns></returns>
        public UInt32 ToInt32()
        {
            return TypeID;
        }
        
        /// <summary>
        /// Перегруженная функция преобразования к строке
        /// </summary>
        /// <returns></returns>
        override public string ToString()
        {
            return (String.IsNullOrEmpty(ShortTypeDescription())) ? TypeDescription() : "(" + Extensions().Aggregate((a, b) => a + " " + b) + ") " + ShortTypeDescription();
        }

        /// <summary>
        /// Оператор сравнения типов ==
        /// </summary>
        /// <param name="type1">Тип 1</param>
        /// <param name="type2">Тип 2</param>
        /// <returns>true, если равны</returns>
        static public bool operator ==(FileType type1, FileType type2)
        {
            return (object.Equals(type1, null) || object.Equals(type2, null)) ? false : type1.TypeID == type2.TypeID;
        }

        /// <summary>
        /// Оператор сравнения типов !=
        /// </summary>
        /// <param name="type1">Тип 1</param>
        /// <param name="type2">Тип 2</param>
        /// <returns>true, если равны</returns>
        static public bool operator !=(FileType type1, FileType type2)
        {
            return !(type1 == type2);
        }

        /// <summary>
        /// Перегруженная функция Equals
        /// </summary>
        /// <param name="obj">любой объект</param>
        /// <returns>true, если равны</returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// Перегруженная функция GetHashCode
        /// </summary>
        /// <returns>HashCode</returns>
        public override int GetHashCode()
        {
            return TypeID.GetHashCode();
        }

        /// <summary>
        /// Короткое описание типа файла
        /// </summary>
        string shortTypeDescription;

        /// <summary>
        /// Получить короткое описание типа файла
        /// </summary>
        /// <returns></returns>
        public string ShortTypeDescription()
        {
            return shortTypeDescription;
        }

        /// <summary>
        /// Полное описание типа файла
        /// </summary>
        string typeDescription;

        /// <summary>
        /// Получить полное описание типа файла
        /// </summary>
        /// <returns></returns>
        public string TypeDescription()
        {
            return typeDescription;
        }

        /// <summary>
        /// Статический словарь типов содержимого файлов
        /// </summary>
        public static Dictionary<uint, enTypeOfContents> _contains = new Dictionary<uint, enTypeOfContents>();

        /// <summary>
        /// Тип содержимого файла
        /// </summary>
        enTypeOfContents contains;

        /// <summary>
        /// Получить тип содержимого файла
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.String)")]
        public enTypeOfContents Contains()
        {
            enTypeOfContents ttype;
            if (_contains.TryGetValue(TypeID, out ttype))
                return ttype;
            else
            {
                _contains.Add(TypeID, contains); 
                return contains;
            }
        }

        /// <summary>
        /// Список раширений у файлов данного типа
        /// </summary>
        List<string> extensions;
        
        /// <summary>
        /// Получить список раширений у файлов данного типа
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        public List<string> Extensions()
        {
            return extensions;
        }
    }

    /// <summary>
    /// Класс Type
    /// </summary>
    public class Type
    {
        /// <summary>
        /// Тип файла
        /// </summary>
        public FileType fType;

        /// <summary>
        /// Хранилище
        /// </summary>
        public Storage storage;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage">хранилище</param>
        /// <param name="sa">Массив строк из базы данных, характеризующий тип</param>
        /// <param name="istext">true, если файл текстовый</param>
        public Type(Storage storage, string[] sa, bool istext)
        {
            this.storage = storage;
            this.fType = new FileType(sa, istext);
        }
    }
}