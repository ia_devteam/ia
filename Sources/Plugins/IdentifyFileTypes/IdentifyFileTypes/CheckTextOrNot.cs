﻿using System;
using System.Linq;
using System.Text;
using System.IO;

namespace IA.Plugins.Analyses.IdentifyFileTypes
{
    /// <summary>
    /// Класс для определения характеристик файлов
    /// </summary>
    static class CheckTextOrNot
    {
        /// <summary>
        /// Статическая переменная о кодировке последнего открытого файла
        /// </summary>
        static Encoding encoding = Encoding.Default;
        
        /// <summary>
        /// Типы файлов - текстовые или бинарные
        /// </summary>
        enum FileTYPES
        {
            TEXT = 0,
            BINARY = 1
        }

        /// <summary>
        /// Подсчитывает сумму чисел по маске.
        /// </summary>
        /// <param name="sF">массив чисел</param>
        /// <param name="mask">битовая маска</param>
        /// <returns>сумма чисел</returns>
        static private Int64 MaskSum(Int64[] sF, byte[] mask)
        {
            Int64 ret = 0;
            for (int i = 0; i < 256; i++)
            {
                if (((0x80 >> (i % 8)) & mask[i / 8]) != 0)
                {
                    ret += sF[i];
                }
            }
            return ret;
        }

        /// <summary>
        /// Определяет тип массива по его содержимому.
        /// </summary>
        /// <param name="arr">массив данных</param>
        /// <returns>тип массива</returns>
        static private FileTYPES SignatureType(byte[] arr)
        {
            //Битовая маска для определения текстовых файлов
            byte[] TextMask = { 0xFF, 0x9B, 0xFF, 0xDF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            //Сигнатура файла
            Int64[] Signature = new Int64[256];
            for (int i = 0; i < arr.Length; i++)
                Signature[arr[i]]++;

            return MaskSum(Signature, TextMask) == 0 ? FileTYPES.TEXT : FileTYPES.BINARY;

        }

        /// <summary>
        /// Проверка, является ли файл текстовым
        /// </summary>
        /// <param name="filename">имя файла</param>
        /// <param name="arrA">массив первых байтов файла</param>
        /// <returns>true, если файл текстовый</returns>
        public static bool TextOrNot(string filename, out byte[] arrA)
        {
            const int DefFileSIZE = 16384;
            arrA = new byte[DefFileSIZE];
            BinaryReader br = null;
            FileStream fs = null;
            int length = 0;
            try
            {
                fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                br = new BinaryReader(fs);
                length = br.Read(arrA, 0, DefFileSIZE);
            }
            catch
            {
                IA.Monitor.Log.Error(PluginSettings.Name, "Файл не найден: " + filename, true);
                return false;
            }         
            finally
            {
                if (fs != null)
                    fs.Close();
            }

            if (length != 0)
            {
                if (length < DefFileSIZE)
                    arrA = arrA.Take(length).ToArray();
                return (SignatureType(FileEncoding(arrA)) == FileTYPES.TEXT);
            }
            else
            {
                arrA = new byte[0];
            }
            return false;
        }
        
        /// <summary>
        /// Возвращает шестнадцатеричное представление последних байт файла
        /// </summary>
        /// <param name="filename">имя файла</param>
        /// <returns>шестнадцатеричная строка</returns>
        public static string LastBytes(string filename)
        {
            string txt = "";
            const int DefFileSIZE = 500;
            BinaryReader br = null;
            FileStream fs = null;
            long length = 0;
            try
            {
                fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                br = new BinaryReader(fs);
                length = fs.Length;
                if (length > DefFileSIZE)
                {
                    fs.Seek(DefFileSIZE, SeekOrigin.End);
                    length = DefFileSIZE;
                }
                byte[] arrA = new byte[length];
                br.Read(arrA, 0, (int)length);
                txt = BitConverter.ToString(arrA).Replace("-", "");
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
            return txt;
        }
        /// <summary>
        /// Перекодировка в Default массив байт исходного массива байт
        /// </summary>
        /// <param name="arrayIn">массив байт, представляющий собой содержимое файла</param>
        /// <returns>массив байт в кодировке Default</returns>
        public static byte[] FileEncoding(byte[] arrayIn)
        {
            if (arrayIn == null || arrayIn.Length < 3)
                return arrayIn;
            string txt = Encoding.Default.GetString(arrayIn.Take(3).ToArray());
            if (txt.StartsWith("п»ї"))
                return Encoding.Default.GetBytes(Encoding.UTF8.GetString(arrayIn));
            else if (txt.StartsWith("яю"))
                return Encoding.Default.GetBytes(Encoding.Unicode.GetString(arrayIn));
            return arrayIn;
        }
        /// <summary>
        /// Перекодировка в строку массива байт
        /// </summary>
        /// <param name="arrayIn"></param>
        /// <returns></returns>
        public static string DefaultEncoding(byte[] arrayIn)
        {
            if (arrayIn == null)
                return null;
            string txt;
            txt = System.Text.Encoding.Default.GetString(arrayIn);
            encoding = Encoding.Default;
            if (txt.StartsWith("п»ї"))
            {
                txt = System.Text.Encoding.UTF8.GetString(arrayIn);
                encoding = Encoding.UTF8;
            }
            else if (txt.StartsWith("яю"))
            {
                txt = System.Text.Encoding.Unicode.GetString(arrayIn);
                encoding = Encoding.Unicode;
            }

            return txt;
        }
    }
}
