﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Store;
using System.Text.RegularExpressions;
using System.Linq;
using IA.Sql.DatabaseConnections;
using IA.Sql;

namespace IA.Plugins.Analyses.IdentifyFileTypes
{

    /// <summary>
    /// Форма для создания, изменения, удаления типа файлов.
    /// </summary>
    public partial class NewSignatureRecord : UserControl
    {
        ResultControl res;

        string ID;
        string FFID;
        string WIDselected;
        string WIDcurrent;
        string sAction;
        string ExtensionTB;
        List<string[]> lsFileFormat=new List<string[]>();
        List<string> lsTypeOfPatterns=new List<string>();
        Dictionary<string, string> TypeOfFilesToDescription = new Dictionary<string,string>();
        Dictionary<string, string> DescriptionToTypeOfFiles = new Dictionary<string,string>();

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="parent">Родительское окно</param>
        /// <param name="sid">Идентификатор типа файла</param>
        /// <param name="wid">Идентификатор выбранного рабочего места (может быть Default)</param>
        /// <param name="act">операция над типом файла</param>
        /// <param name="widcurrent">Идентификатор текущего рабочего места</param>
        public  NewSignatureRecord(ResultControl parent, string sid,string wid, string act, string widcurrent)
        {
            res = parent;
            sAction = act;
            ID = sid;
            WIDselected = wid;
            WIDcurrent = widcurrent;
            ExtensionTB = res.ExtensionOnTB;
            InitializeComponent();

            EnumAttributes();

            textBoxShortDescription.Focus();
            if (WIDselected == "1" && WIDcurrent == "1")
            {
                EnableFields(false);
                buttonSave.Enabled = false;
            }
            labelWorkPlace.Text = res.Plugin.WorkPlaces.Where(x => x[0] == WIDcurrent).Select(x => x[1]).FirstOrDefault();
            if (WIDselected != WIDcurrent && sAction == "Update")
                sAction = "NewUpdate"; // Для нас Update в этом случае = New

            foreach (string s in GetLanguages())
            {
                listBoxLanguadges.Items.Add(s);
            }

            foreach (string s in GetContentsType())
            {
                listBoxTypeOfFiles.Items.Add(TypeOfFilesToDescription[s]);
            }

            if (sAction != "New")
            {
                FFID = res.Plugin.FileFormat.Where(x => x[0] == ID).Select(x => x[1]).FirstOrDefault();
            }
            switch (sAction)
            {
                case "New":
                    break;
                case "Update":
                    break;
                case "NewUpdate":
                    break;
                case "Delete":
                    EnableFields(false);
                    if (WIDselected == "1")
                    {
                        sAction = "View";
                        buttonSave.Text = "Закрыть";
                    }
                    else
                        buttonSave.Text = "Удалить";
                    break;
                default:
                    EnableFields(false);
                    buttonSave.Hide();
                    buttonCancel.Text = "Закрыть";
                    break;
            }
            lsTypeOfPatterns = res.Plugin.Pattern.Select(x => x[3]).Distinct().ToList();
            DataGridViewComboBoxColumn cmb = dataGridViewPatterns.Columns[3] as DataGridViewComboBoxColumn;
            foreach (string s in lsTypeOfPatterns)
            {
                cmb.Items.Add(s);
            }
            FillForm();
        }

        /// <summary>
        /// Получение списка языков из enum
        /// </summary>
        /// <returns>массив строк</returns>
        private string[] GetLanguages()
        {
            return Enum.GetNames(typeof(enLanguage));
        }

        /// <summary>
        /// Установка маски, соответствующей списку выделенных языков
        /// </summary>
        /// <returns>маска</returns>
        private ulong SetLanguage()
        {
            ulong mask = 0;
            foreach(string s in listBoxLanguadges.SelectedItems)
                mask |= (ulong)(enLanguage)Enum.Parse(typeof(enLanguage), s);
            return mask;
        }


        /// <summary>
        /// Получение массива языков по маске 
        /// </summary>
        /// <param name="mask">маска</param>
        /// <returns>массив строк</returns>
        private string[] GetLanguages(ulong mask)
        {
            return ((enLanguage)mask).ToString().Split(',');
        }

        /// <summary>
        /// Включить/выключть некоторые поля
        /// </summary>
        /// <param name="e">true - вкючить</param>
        private void EnableFields(bool e)
        {
            textBoxShortDescription.Enabled = e;
            textBoxDescription.Enabled = e;
            textBoxExtensions.Enabled = e;
            listBoxTypeOfFiles.Enabled = e;
            listBoxLanguadges.Enabled = e;
            ButtonAddPattern.Enabled = e;
            ButtonDeletePattern.Enabled = e;
            dataGridViewPatterns.Enabled = e;
        }

        /// <summary>
        /// Функция заполнения формы начальными значениями
        /// </summary>
        private void FillForm()
        {
            string FFID = "";
            string FFWID = "";
            try
            {
                FFID = res.Plugin.FileFormat.Where(x => x[0] == ID).First()[1];
                FFWID = res.Plugin.FileFormat.Where(x => x[0] == ID).First()[2];
            }
            catch
            {
                FFID = "";
                FFWID = "";
            }

            if (ID != "0")
            {
                lsFileFormat = res.Plugin.FileFormat.Where(x => x[0] == ID).ToList();
                if (sAction == "New")
                {
                    ID = (res.Plugin.FileFormat.Select(x => int.Parse(x[1])).Max() + 1).ToString();
                    lsFileFormat[0][1] = ID;
                    lsFileFormat[0][2] = res.Plugin.WorkID;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ExtensionTB))
                    textBoxExtensions.Text = ExtensionTB;

                int maxFFID = res.Plugin.FileFormat.Select(x => int.Parse(x[1])).Max() + 1;
                lsFileFormat.Add(new string[] { "", maxFFID.ToString(), res.Plugin.WorkID, "63", textBoxShortDescription.Text, textBoxDescription.Text, textBoxExtensions.Text,"0" });
            }
            if (lsFileFormat.Count > 0)
            {
                textBoxShortDescription.Text = lsFileFormat[0][4];
                textBoxDescription.Text = lsFileFormat[0][5];
                textBoxExtensions.Text = lsFileFormat[0][6];
                ulong mask;
                if(ulong.TryParse(lsFileFormat[0][7],out mask))
                {
                    listBoxLanguadges.SelectedIndices.Clear();
                    string[] sa = GetLanguages(mask);
                    for(int i=0; i<listBoxLanguadges.Items.Count;i++)
                    {
                        if(sa.Contains(listBoxLanguadges.Items[i].ToString()))
                            listBoxLanguadges.SelectedIndices.Add(i);
                    }

                }


                ulong TypeOfFile;
                if (!ulong.TryParse(lsFileFormat[0][3], out TypeOfFile))
                {
                    TypeOfFile = 0;
                }
                else
                {
                    listBoxTypeOfFiles.SelectedIndices.Clear();
                    string[] sa = GetContentsType(TypeOfFile);
                    for (int i = 0; i < listBoxTypeOfFiles.Items.Count; i++)
                    {
                        if (sa.Contains(listBoxTypeOfFiles.Items[i].ToString()))
                            listBoxTypeOfFiles.SelectedIndices.Add(i);
                    }
                }
                //RegisterConsiderCheckBox.Checked = ((TypeOfFile & 16) == 16);
                List<string[]> lsPattern = res.Plugin.Pattern.Where(x => x[1] == lsFileFormat[0][1] && x[2] == lsFileFormat[0][2]).ToList();
                if (ID != "0" && sAction == "New")
                {
                    lsPattern = res.Plugin.Pattern.Where(x => x[1] == FFID && x[2] == FFWID).ToList();
                }
                dataGridViewPatterns.Rows.Clear();
                foreach (string[] sa in lsPattern)
                {
                    dataGridViewPatterns.Rows.Add(sa);
                }

            }
        }

        /// <summary>
        /// Проверка корректности данных формы
        /// </summary>
        /// <returns></returns>
        private bool CheckData()
        {
            if (string.IsNullOrEmpty(textBoxShortDescription.Text))
            {
                MessageBox.Show("Укажите значимые данные", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                textBoxShortDescription.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(textBoxDescription.Text))
            {
                MessageBox.Show("Укажите значимые данные", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                textBoxDescription.Focus();
                return false;
            }
            if (dataGridViewPatterns.Rows.Count == 0)
            {
                if (DialogResult.OK != MessageBox.Show("Укажите шаблоны.\nЕсли вы настаиваете на отсутствии шаблонов, нажмите \"OK\"", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop))
                {
                    textBoxExtensions.Focus();
                    return false;
                }
            }
            for (int i = 0; i < dataGridViewPatterns.Rows.Count; i++)
            {
                Regex isPer;

                string TypeOfPattern = GetDGVValue(dataGridViewPatterns, i, 3);
                string PatternF = GetDGVValue(dataGridViewPatterns, i, 4);
                string PatternFValue = GetDGVValue(dataGridViewPatterns, i, 5);
                string PatternS = GetDGVValue(dataGridViewPatterns, i, 6);
                string PatternSValue = GetDGVValue(dataGridViewPatterns, i, 7);
                switch (TypeOfPattern.ToLower())
                {
                    case "binary":
                        TypeOfPattern = "Binary";
                        if (PatternF.ToLower() != "begins")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Заголовок\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[4].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        PatternF = "Begins";
                        if (PatternFValue.Length == 0 || PatternFValue.Length % 2 != 0)
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Шаблон\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[5].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        Regex isNotHex = new Regex("[^a-fA-F0-9]");
                        if (isNotHex.IsMatch(PatternFValue))
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Шаблон\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[5].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        dataGridViewPatterns.Rows[i].Cells[5].Value = PatternFValue.ToUpper();
                        if (PatternS.Length!=0 && PatternS.ToLower() != "ends")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Заголовок2\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[6].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        if (PatternS.ToLower() == "ends")
                        {
                            PatternS = "Ends";
                            if (PatternS.Length != 0 && (PatternSValue.Length == 0 || PatternSValue.Length % 2 != 0))
                            {
                                MessageBox.Show("Проверьте параметры в поле \"Шаблон2\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                dataGridViewPatterns.Rows[i].Cells[7].Selected = true;
                                dataGridViewPatterns.Focus();
                                return false;
                            }
                            if (PatternS.Length != 0 && isNotHex.IsMatch(PatternSValue))
                            {
                                MessageBox.Show("Проверьте параметры в поле \"Шаблон2\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                dataGridViewPatterns.Rows[i].Cells[7].Selected = true;
                                dataGridViewPatterns.Focus();
                                return false;
                            }
                            dataGridViewPatterns.Rows[i].Cells[7].Value = PatternSValue.ToUpper();
                        }
                        break;
                    case "xml":
                        TypeOfPattern = "XML";
                        if (PatternF.ToLower() != "contains")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Заголовок\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[4].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        PatternF = "Contains";
                        if (PatternFValue.Length==0)
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Шаблон\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[5].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        if (PatternS.ToLower() != "weight")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Заголовок2\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[6].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        PatternS = "Weight";
                        isPer = new Regex(@"^\d{2,2}$");
                        if (!isPer.IsMatch(PatternSValue) && PatternSValue != "100")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Шаблон2\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[7].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        break;
                    case "text":
                        TypeOfPattern = "TEXT";
                        if (PatternF.ToLower() != "contains")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Заголовок\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[4].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        PatternF = "Contains";
                        if (PatternFValue.Length==0)
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Шаблон\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[5].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        if (PatternS.ToLower() != "weight")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Заголовок2\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[6].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        PatternS = "Weight";
                        isPer = new Regex(@"^\d{2,2}$");
                        if (!isPer.IsMatch(PatternSValue) && PatternSValue != "100")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Шаблон2\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[7].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        break;
                    case "executable":
                        TypeOfPattern = "Executable";
                        if (PatternF.ToLower() != "method")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Заголовок\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[4].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        PatternF = "Method";
                        if (PatternFValue.Length==0)
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Шаблон\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[5].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        if (PatternS.ToLower()!="checktype")
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Заголовок2\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[6].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        PatternS = "CheckType";
                        if (PatternSValue.Length==0)
                        {
                            MessageBox.Show("Проверьте параметры в поле \"Шаблон2\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            dataGridViewPatterns.Rows[i].Cells[7].Selected = true;
                            dataGridViewPatterns.Focus();
                            return false;
                        }
                        break;
                    default:
                        MessageBox.Show("Проверьте параметры в поле \"Тип\"", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        dataGridViewPatterns.Rows[i].Cells[3].Selected = true;
                        dataGridViewPatterns.Focus();
                        return false;
                }
                dataGridViewPatterns.Rows[i].Cells[3].Value = TypeOfPattern;
                dataGridViewPatterns.Rows[i].Cells[4].Value = PatternF;
                dataGridViewPatterns.Rows[i].Cells[6].Value = PatternS;
            }
            return true;
        }

        /// <summary>
        /// Форматирование фанных формы
        /// </summary>
        private void FormatData()
        {
            textBoxShortDescription.Text = textBoxShortDescription.Text.Trim();
            textBoxDescription.Text = textBoxDescription.Text.Trim();

            if (listBoxTypeOfFiles.SelectedIndex == -1)
                listBoxTypeOfFiles.SelectedIndex = 0;
            if (textBoxShortDescription.Text.Length > 1000)
            {
                textBoxShortDescription.Text = textBoxShortDescription.Text.Substring(0, 1000);
            }
            if (textBoxDescription.Text.Length > 1000)
            {
                textBoxDescription.Text = textBoxDescription.Text.Substring(0, 1000);
            }
            if (textBoxExtensions.Text.Length > 4000)
            {
                textBoxExtensions.Text = textBoxExtensions.Text.Substring(0, 4000);
            }

        }

        /// <summary>
        /// Прочитать значение ячейки из DataGridView
        /// </summary>
        /// <param name="dgv">DataGridView</param>
        /// <param name="index">строка</param>
        /// <param name="column">столбец</param>
        /// <returns>значение</returns>
        public static string GetDGVValue(DataGridView dgv, int index, int column)
        {
            return (dgv.Rows[index].Cells[column].Value == null) ? "" : dgv.Rows[index].Cells[column].Value.ToString();
        }

        private ulong ToTypeContents()
        {
            ulong mask = 0;
            foreach (string s in listBoxTypeOfFiles.SelectedItems)
            {
                mask |= (ulong)(enTypeOfContents)Enum.Parse(typeof(enTypeOfContents), DescriptionToTypeOfFiles[s]);
            }
            return mask;
        }

        /// <summary>
        /// Получение описания типа файла по маске 
        /// </summary>
        /// <param name="mask">маска</param>
        /// <returns>массив строк</returns>
        private string[] GetContentsType(ulong mask)
        {
            return ((enTypeOfContents)mask).ToString().Split(',').Select(x=>TypeOfFilesToDescription[x.Trim()]).ToArray();
        }
        
        /// <summary>
        /// Получение всех имен Енума enTypeOfContents
        /// </summary>
        /// <returns></returns>
        private string[] GetContentsType()
        {
            return Enum.GetNames(typeof(enTypeOfContents)).Select(x=>x.Trim()).ToArray();
        }

        /// <summary>
        /// Заполнение словарей Енум->Аттрибут и Аттрибут->Енум
        /// </summary>
        private void EnumAttributes()
        {
            foreach(enTypeOfContents en in Enum.GetValues(typeof(enTypeOfContents)))
            {
                TypeOfFilesToDescription.Add(en.ToString().Trim(), GetDescription(en).Trim());
                DescriptionToTypeOfFiles.Add(GetDescription(en).Trim(), en.ToString().Trim());
            }
        }

        /// <summary>
        /// Получение аттрибута у Енума
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(Enum value)
        {
            DescriptionAttribute[] da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false));
            return da.Length > 0 ? da[0].Description : value.ToString();
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1307:SpecifyStringComparison", MessageId = "System.String.EndsWith(System.String)")]
        private void button2_Click(object sender, EventArgs e)
        {
  
            switch (sAction)
            {
                case "New":
                    FormatData();
                    if (CheckData() == false)
                        return;
                    if (res.Plugin.FileFormat.Where(x => x[4] == textBoxShortDescription.Text).Count() > 0)
                    {
                        if(MessageBox.Show("Указанное краткое описание типов файлов уже существует.\n Нажмите OK, если уверенны.", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop)!=DialogResult.OK)
                            return;
                    }
                    FFID = (((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat.Select(x => int.Parse(x[1])).Max() + 1).ToString();
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatInsert(new string[] { FFID, WIDcurrent, ToTypeContents().ToString(), textBoxShortDescription.Text, textBoxDescription.Text, textBoxExtensions.Text, SetLanguage().ToString() });
                    string IDL1 = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatLastID(new string[] { FFID, WIDcurrent });
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistoryAdd((new string[] { IDL1, WIDcurrent }));
                    for (int i = 0; i < dataGridViewPatterns.Rows.Count; i++)
                    {
                        string TypeOfPattern = GetDGVValue(dataGridViewPatterns, i, 3);
                        string PatternF = GetDGVValue(dataGridViewPatterns, i, 4);
                        string PatternFValue = GetDGVValue(dataGridViewPatterns, i, 5);
                        string PatternS = GetDGVValue(dataGridViewPatterns, i, 6);
                        string PatternSValue = GetDGVValue(dataGridViewPatterns, i, 7);
                        if (TypeOfPattern.ToLower() != "binary" && TypeOfPattern.ToLower() != "text" && TypeOfPattern.ToLower() != "xml" && TypeOfPattern.ToLower() != "executable")
                        {
                            MessageBox.Show("Неверный тип шаблона", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        switch (TypeOfPattern.ToLower())
                        {
                            case "binary":
                                TypeOfPattern = "Binary";
                                PatternF = "Begins";
                                PatternFValue = PatternFValue.ToUpper();
                                if (!string.IsNullOrEmpty(PatternS) && !string.IsNullOrEmpty(PatternSValue))
                                {
                                    PatternS = "Ends";
                                    PatternSValue = PatternSValue.ToUpper();
                                }
                                else
                                {
                                    PatternS = "";
                                    PatternSValue = "";
                                }
                                break;
                            case "text":
                                TypeOfPattern = "TEXT";
                                PatternF = "Contains";
                                PatternS = "Weight";
                                break;
                            case "xml":
                                TypeOfPattern = "XML";
                                PatternF = "Contains";
                                PatternS = "Weight";
                                break;
                            case "executable":
                                TypeOfPattern = "Executable";
                                PatternF = "Method";
                                PatternS = "CheckType";
                                break;
                        }


                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternInsert(new string[] { FFID, WIDcurrent, TypeOfPattern, PatternF, PatternFValue, PatternS, PatternSValue });
                    }
                    break;
                case "Update":
                    FormatData();
                    if (CheckData() == false)
                        return;
                    if (res.Plugin.FileFormat.Where(x => x[4] == textBoxShortDescription.Text && x[0] != ID).Count() > 0)
                    {
                        if (MessageBox.Show("Указанное краткое описание типов файлов уже существует.\n Нажмите OK, если уверенны.", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
                            return;
                    }
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatUpdate(new string[] { ID, FFID, WIDcurrent, (ToTypeContents()).ToString(), textBoxShortDescription.Text, textBoxDescription.Text, textBoxExtensions.Text, SetLanguage().ToString() });
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistoryAdd((new string[] { ID, WIDcurrent }));

                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternDelete(new string[] { FFID, WIDcurrent });
                    for (int i = 0; i < dataGridViewPatterns.Rows.Count; i++)
                    {
                        string TypeOfPattern = GetDGVValue(dataGridViewPatterns, i, 3);
                        string PatternF = GetDGVValue(dataGridViewPatterns, i, 4);
                        string PatternFValue = GetDGVValue(dataGridViewPatterns, i, 5);
                        string PatternS = GetDGVValue(dataGridViewPatterns, i, 6);
                        string PatternSValue = GetDGVValue(dataGridViewPatterns, i, 7);
                        if (TypeOfPattern.ToLower() != "binary" && TypeOfPattern.ToLower() != "text" && TypeOfPattern.ToLower() != "xml" && TypeOfPattern.ToLower() != "executable")
                        {
                            MessageBox.Show("Неверный тип шаблона", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        switch (TypeOfPattern.ToLower())
                        {
                            case "binary":
                                TypeOfPattern = "Binary";
                                PatternF = "Begins";
                                PatternFValue = PatternFValue.ToUpper();
                                if (!string.IsNullOrEmpty(PatternS) && !string.IsNullOrEmpty(PatternSValue))
                                {
                                    PatternS = "Ends";
                                    PatternSValue = PatternSValue.ToUpper();
                                }
                                else
                                {
                                    PatternS = "";
                                    PatternSValue = "";
                                }
                                break;
                            case "text":
                                TypeOfPattern = "TEXT";
                                PatternF = "Contains";
                                PatternS = "Weight";
                                break;
                            case "xml":
                                TypeOfPattern = "XML";
                                PatternF = "Contains";
                                PatternS = "Weight";
                                break;
                            case "executable":
                                TypeOfPattern = "Executable";
                                PatternF = "Method";
                                PatternS = "CheckType";
                                break;
                        }
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternInsert(new string[] { FFID, WIDcurrent, TypeOfPattern, PatternF, PatternFValue, PatternS, PatternSValue });
                    }
                    break;
                case "NewUpdate":
                    FormatData();
                    if (CheckData() == false)
                        return;
                    FFID = (((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat.Select(x => int.Parse(x[1])).Max() + 1).ToString();
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatInsert(new string[] { FFID, WIDcurrent, ToTypeContents().ToString(), textBoxShortDescription.Text, textBoxDescription.Text, textBoxExtensions.Text, SetLanguage().ToString() });
                    string IDL = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatLastID(new string[] { FFID, WIDcurrent });
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistoryAdd((new string[] { IDL, WIDcurrent }));
                    for (int i = 0; i < dataGridViewPatterns.Rows.Count; i++)
                    {
                        string TypeOfPattern = GetDGVValue(dataGridViewPatterns, i, 3);
                        string PatternF = GetDGVValue(dataGridViewPatterns, i, 4);
                        string PatternFValue = GetDGVValue(dataGridViewPatterns, i, 5);
                        string PatternS = GetDGVValue(dataGridViewPatterns, i, 6);
                        string PatternSValue = GetDGVValue(dataGridViewPatterns, i, 7);
                        if (TypeOfPattern.ToLower() != "binary" && TypeOfPattern.ToLower() != "text" && TypeOfPattern.ToLower() != "xml" && TypeOfPattern.ToLower() != "executable")
                        {
                            MessageBox.Show("Неверный тип шаблона", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        switch (TypeOfPattern.ToLower())
                        {
                            case "binary":
                                TypeOfPattern = "Binary";
                                PatternF = "Begins";
                                PatternFValue = PatternFValue.ToUpper();
                                if (!string.IsNullOrEmpty(PatternS) && !string.IsNullOrEmpty(PatternSValue))
                                {
                                    PatternS = "Ends";
                                    PatternSValue = PatternSValue.ToUpper();
                                }
                                else
                                {
                                    PatternS = "";
                                    PatternSValue = "";
                                }
                                break;
                            case "text":
                                TypeOfPattern = "TEXT";
                                PatternF = "Contains";
                                PatternS = "Weight";
                                break;
                            case "xml":
                                TypeOfPattern = "XML";
                                PatternF = "Contains";
                                PatternS = "Weight";
                                break;
                            case "executable":
                                TypeOfPattern = "Executable";
                                PatternF = "Method";
                                PatternS = "CheckType";
                                break;
                        }


                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternInsert(new string[] { FFID, WIDcurrent, TypeOfPattern, PatternF, PatternFValue, PatternS, PatternSValue });
                    }
                    break;
                case "Delete":
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatDelete(new string[] {ID});
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternDelete(new string[] {FFID,WIDselected });
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistoryRemove(new string[] { ID });
                    //res.Plugin.SQLConnection.DeleteTreeByID(ID);
                    break;
            }

            res.refreshSignature();

            MessageBox.Show("Изменения будут доступны после перезапуска плагина", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Control[] rr = res.Controls.Find("tabControl1", false);
            ((TabControl)rr[0]).TabPages[((TabControl)rr[0]).SelectedIndex].Dispose();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Control[] rr = res.Controls.Find("tabControl1", false);
            ((TabControl)rr[0]).TabPages[((TabControl)rr[0]).SelectedIndex].Dispose();
        }


        private void AddSignatureButton_Click(object sender, EventArgs e)
        {
            string[] sa = new string[8];
            sa[0] = "";
            sa[1]=lsFileFormat[0][1];
            sa[2]=lsFileFormat[0][2];
            if (listBoxTypeOfFiles.Text != "Исходные тексты в текстовом формате")
            {
                sa[3] = "Binary";
                sa[4] = "Begins";
                sa[5] = "";
                sa[6] = "";
                sa[7] = "";
            }
            else
            {
                sa[3] = "TEXT";
                sa[4] = "Contains";
                sa[5] = "";
                sa[6] = "Weight";
                sa[7] = "100";
            }
            dataGridViewPatterns.Rows.Add(sa);
            dataGridViewPatterns.Rows[dataGridViewPatterns.Rows.Count - 1].Cells[5].Selected = true;
        }

        private void UpdateSignatureButton_Click(object sender, EventArgs e)
        {
            if (dataGridViewPatterns.SelectedCells.Count==1)
            {
                int index = dataGridViewPatterns.SelectedCells[0].RowIndex;
                string PID = dataGridViewPatterns.Rows[index].Cells[0].Value.ToString();
                if (int.TryParse(PID, out index))
                {

                }
            }
        }

        private void RemoveSignatureButton_Click(object sender, EventArgs e)
        {
            if (dataGridViewPatterns.SelectedCells.Count>0)
            {
                dataGridViewPatterns.Rows.RemoveAt(dataGridViewPatterns.SelectedCells[0].RowIndex);
            }
        }


        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                if (dataGridViewPatterns.SelectedCells.Count == 1)
                {
                    int index = e.RowIndex;
                    switch (dataGridViewPatterns.Rows[index].Cells[3].Value.ToString().ToLower())
                    {
                        case "binary":
                            dataGridViewPatterns.Rows[index].Cells[4].Value = "Begins";
                            dataGridViewPatterns.Rows[index].Cells[5].Value = "";
                            dataGridViewPatterns.Rows[index].Cells[6].Value = "";
                            dataGridViewPatterns.Rows[index].Cells[7].Value = "";
                            break;
                        case "executable":
                            dataGridViewPatterns.Rows[index].Cells[4].Value = "Method";
                            dataGridViewPatterns.Rows[index].Cells[5].Value = "";
                            dataGridViewPatterns.Rows[index].Cells[6].Value = "CheckType";
                            dataGridViewPatterns.Rows[index].Cells[7].Value = "";
                            break;
                        default:
                            dataGridViewPatterns.Rows[index].Cells[4].Value = "Contains";
                            dataGridViewPatterns.Rows[index].Cells[5].Value = "";
                            dataGridViewPatterns.Rows[index].Cells[6].Value = "Weight";
                            dataGridViewPatterns.Rows[index].Cells[7].Value = "100";
                            break;

                    }
                    dataGridViewPatterns.Rows[index].Cells[3].Selected = false;
                    dataGridViewPatterns.Rows[index].Cells[5].Selected = true;

                }
            }
        }

    }
}
