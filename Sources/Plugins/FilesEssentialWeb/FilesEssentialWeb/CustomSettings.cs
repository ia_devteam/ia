﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.FilesEssentialWeb
{
    /// <summary>
    /// Основной класс настроек
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        internal Settings()
        {
            InitializeComponent();
            
            handyFilePath_textBox.Text = PluginSettings.fileManuals;
            symbols_textBox.Text = PluginSettings.Symbols;
            isUseData_checkBox.Checked = PluginSettings.IsUseDATA;
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            PluginSettings.fileManuals = handyFilePath_textBox.Text;
            PluginSettings.Symbols = symbols_textBox.Text;
            PluginSettings.IsUseDATA = isUseData_checkBox.Checked;
        }
    }
}
