﻿/*
 * Плагин Избыточность WEB файлов
 * Файл ResultWindow.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Кокин
 * 
 * Плагин предназначен для выявления зависимостей файлов относительно друг друга и 
 * построения графа зависимостей при выделении некоторых файлов в качестве корневых узлов.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Store;

namespace IA.Plugins.Analyses.FilesEssentialWeb
{
    /// <summary>
    /// Класс окна результатов работы плагина
    /// </summary>
    internal partial class ResultWindow : UserControl
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        FilesEssentialWeb.ResultLists resultLists;
        
        WebFile FileToFind = null;
        string SearchName = null;
        

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="resLists"></param>
        internal ResultWindow(Storage storage, FilesEssentialWeb.ResultLists resLists)
        {
            this.storage = storage;
            resultLists = resLists;

            InitializeComponent();

            FilesEssentialWeb.CreateListChars();

            // сначала пишем корневые
            sourceFiles_listBox.Items.AddRange(resultLists.listRoots.ToArray());

            sourceFiles_listBox.Items.AddRange(resultLists.listSources.Except(resultLists.listRoots).ToArray());
            sourceFiles_groupBox.Text = "Исходные файлы (" + sourceFiles_listBox.Items.Count + ")";
            
            Recalculate();
        }

        void Recalculate()
        {
            FilesEssentialWeb.Essential(resultLists);
            Rearrange();
        }
        void Rearrange()
        {
            ManualFiles_listBox.Items.Clear();
            ManualFiles_listBox.Items.AddRange(resultLists.listManual.ToArray());
            essentialFiles_listBox.Items.Clear();
            essentialFiles_listBox.Items.AddRange(resultLists.listEssential.ToArray());
            essentialFiles_groupBox.Text = "Неизбыточные файлы " + essentialFiles_listBox.Items.Count;
            redunduntFiles_listBox.Items.Clear();
            redunduntFiles_listBox.Items.AddRange(resultLists.listRedundant.ToArray());
            redunduntFiles_groupBox.Text = "Избыточные файлы " + redunduntFiles_listBox.Items.Count;
        }

        /// <summary>
        /// Поиск всех родителей
        /// </summary>
        /// <param name="name"></param>
        private void FindAllParentsAndChilder(string name)
        {
            if (string.IsNullOrEmpty(name))
                return;

            name = name.ToLower();
            ClassNode.Node<WebFile> res = resultLists.listNodes.Find(x => x.Value.ReportName.ToLower() == name);

            FileToFind = res.Value;
            
            SearchName = FileToFind.ReportName;

            parents_listBox.Items.Clear();
            parents_listBox.Items.AddRange(res.Parent.Select(x => x.Value.ReportName).ToArray());
            parents_groupBox.Text = "Родители выбранного файла " + parents_listBox.Items.Count;

            children_listBox.Items.Clear();
            children_listBox.Items.AddRange(res.Children.Select(x => x.Value.ReportName).ToArray());
            Children_GroupBox.Text = "Дети выбранного файла " + children_listBox.Items.Count;
        }

        /// <summary>
        /// Обработчик - выбрали элемент в списке sourceFiles_listBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sourceFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sourceFiles_listBox.SelectedIndex != -1)
            {
                FindAllParentsAndChilder(sourceFiles_listBox.Text);

                ShowFileAndSelect(sourceFiles_listBox.Text, "");
            }
        }

        /// <summary>
        /// Обработчик - выбрали элемент в списке essentialFiles_listBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void essentialFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FindAllParentsAndChilder(essentialFiles_listBox.Text);


            ShowFileAndSelect(essentialFiles_listBox.Text, "");
        }

        /// <summary>
        /// Обработчик - выбрали элемент в списке redunduntFiles_listBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void redunduntFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FindAllParentsAndChilder(redunduntFiles_listBox.Text);


            ShowFileAndSelect(redunduntFiles_listBox.Text, "");
        }

        /// <summary>
        /// Кнопка - копирование в список вручную отобранных файлов из исходных файлов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fromSourcesToManualFiles_button_Click(object sender, EventArgs e)
        {
            if (sourceFiles_listBox.SelectedItems.Count > 0)
            {
                resultLists.listManual.AddRange(sourceFiles_listBox.SelectedItems.Cast<string>().ToArray());
                resultLists.listManual = resultLists.listManual.Distinct().ToList();
            }
            Recalculate();
        }

        private void ShowFileAndSelect(string FileName, string textToSelect = "")
        {
            sourceText_richTextBox.Text = null;
            string sFileBody = FilesEssentialWeb.DefaultEncoding(FilesEssentialWeb.ReadFile(storage.appliedSettings.FileListPath + FileName));
            sourceText_richTextBox.Text = sFileBody;

            if (!string.IsNullOrEmpty(textToSelect))
            {
                sFileBody = sourceText_richTextBox.Text.ToLower();
                sourceText_richTextBox.SelectionColor = Color.Black;
                int ptr = 0;
                int optr;
                string sres = FilesEssentialWeb.TryGetMaxPath(sFileBody, textToSelect, ptr, out optr);
                while (optr != -1)
                    if ((storage.appliedSettings.FileListPath + SearchName).ToLower().Replace(@"/", @"\").IndexOf(sres) != -1)
                    {
                        int index;
                        if (optr > 1)
                        {
                            string sres1 = FilesEssentialWeb.GetCurrentPath(sFileBody, optr - 1, out index);
                            if (index >= sres.Length)
                            {
                                sourceText_richTextBox.Select(index - sres1.Length, sres1.Length);
                                sourceText_richTextBox.SelectionColor = Color.Red;
                                sourceText_richTextBox.ScrollToCaret();
                            }
                            break;
                        }
                    }
                    else
                    {
                        ptr = optr;
                        sres = FilesEssentialWeb.TryGetMaxPath(sFileBody, textToSelect, ptr, out optr);
                    }
            }
            sourceText_groupBox.Text = "Исходный текст: " + parents_listBox.Text;
        }

        /// <summary>
        /// Обработчик - выбрали элемент в списке parents_listBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void parents_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (parents_listBox.SelectedIndex != -1)
            {
                ClassNode.Node<WebFile> res = resultLists.listNodes.Find(x => x.Value.ReportName.ToLower() == SearchName);                
                if (res != null)
                {
                    ShowFileAndSelect(parents_listBox.Text, res.Value.Name);
                }
            }
        }

        /// <summary>
        /// Кнопка - удаление из списка вручную отобранных файлов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteFromManualFiles_button_Click(object sender, EventArgs e)
        {
            resultLists.listManual = resultLists.listManual.Where(x => !ManualFiles_listBox.SelectedItems.Cast<string>().Contains(x)).ToList();
            Recalculate();
        }

        /// <summary>
        /// Сохранение и запуск геренации отчетов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveAndRun_button_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Кнопка - переместить в избыточные файлы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fromEssentialToRedundantFiles_button_Click(object sender, EventArgs e)
        {
            List<string> ls = essentialFiles_listBox.SelectedItems.Cast<string>().ToList();
            foreach (string item in ls)
            {
                resultLists.listRedundant.Add(item);
                resultLists.listEssential.Remove(item);
            }
            Rearrange();
        }

        /// <summary>
        /// Кнопка - переместить из избыточных файлов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fromRedundantToEssentialFiles_button_Click(object sender, EventArgs e)
        {
            List<string> ls = redunduntFiles_listBox.SelectedItems.Cast<string>().ToList();
            foreach (string item in ls)
            {
                resultLists.listEssential.Add(item);
                resultLists.listRedundant.Remove(item);
            }
            Rearrange();
        }

        /// <summary>
        /// Кнопка - повторный поиск
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void findAgain_button_Click(object sender, EventArgs e)
        {
            if (sourceText_richTextBox.Text != null && sourceText_richTextBox.Text != "" && sourceText_richTextBox.SelectionStart >= 0 && sourceText_richTextBox.SelectionLength > 0)
            {
                int index1 = sourceFiles_listBox.Items.IndexOf(essentialFiles_listBox.Text);
                int index = sourceText_richTextBox.SelectionStart + sourceText_richTextBox.SelectionLength;
                int ptr = index;
                int optr;
                string sres = FilesEssentialWeb.TryGetMaxPath(sourceText_richTextBox.Text, resultLists.listNodes[index1].Value.Name.ToLower(), ptr, out optr);
                while (optr != -1)
                    if ((storage.appliedSettings.FileListPath + essentialFiles_listBox.Text).ToLower().IndexOf(sres) != -1)
                    {
                        if (optr > 1)
                        {
                            string sres1 = FilesEssentialWeb.GetCurrentPath(sourceText_richTextBox.Text, optr - 1, out index);
                            if (index >= sres.Length)
                            {
                                sourceText_richTextBox.Select(index - sres1.Length, sres1.Length);
                                sourceText_richTextBox.SelectionColor = Color.Red;
                                sourceText_richTextBox.ScrollToCaret();
                            }
                            break;
                        }
                    }
                    else
                    {
                        ptr = optr;
                        sres = FilesEssentialWeb.TryGetMaxPath(sourceText_richTextBox.Text, resultLists.listNodes[index1].Value.Name.ToLower(), ptr, out optr);
                    }
            }
        }

        private void ManualFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FindAllParentsAndChilder(ManualFiles_listBox.Text);


            ShowFileAndSelect(ManualFiles_listBox.Text, "");

        }

        private void children_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (children_listBox.SelectedIndex != -1)
            {
                sourceText_richTextBox.Text = null;
                string sFileBody = FilesEssentialWeb.DefaultEncoding(FilesEssentialWeb.ReadFile(storage.appliedSettings.FileListPath + children_listBox.Text));
                sourceText_richTextBox.Text = sFileBody;
                sFileBody = sourceText_richTextBox.Text.ToLower();
                sourceText_richTextBox.SelectionColor = Color.Black;               
                sourceText_groupBox.Text = "Исходный текст: " + children_listBox.Text;
            }

        }
    }
}
