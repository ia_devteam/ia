﻿namespace IA.Plugins.Analyses.FilesEssentialWeb
{
    partial class Settings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.symbols_textBox = new System.Windows.Forms.TextBox();
            this.isUseData_checkBox = new System.Windows.Forms.CheckBox();
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.handyFilePath_groupBox = new System.Windows.Forms.GroupBox();
            this.handyFilePath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.symbols_groupBox = new System.Windows.Forms.GroupBox();
            this.symbols_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.handyFilePath_groupBox.SuspendLayout();
            this.symbols_groupBox.SuspendLayout();
            this.symbols_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // symbols_textBox
            // 
            this.symbols_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.symbols_textBox.Location = new System.Drawing.Point(3, 5);
            this.symbols_textBox.Name = "symbols_textBox";
            this.symbols_textBox.Size = new System.Drawing.Size(444, 20);
            this.symbols_textBox.TabIndex = 31;
            // 
            // isUseData_checkBox
            // 
            this.isUseData_checkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.isUseData_checkBox.AutoSize = true;
            this.isUseData_checkBox.Location = new System.Drawing.Point(3, 116);
            this.isUseData_checkBox.Name = "isUseData_checkBox";
            this.isUseData_checkBox.Size = new System.Drawing.Size(456, 17);
            this.isUseData_checkBox.TabIndex = 32;
            this.isUseData_checkBox.Text = "Использовать содержимое файлов данных (DATA) для поиска зависимостей";
            this.isUseData_checkBox.UseVisualStyleBackColor = true;
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.isUseData_checkBox, 0, 2);
            this.settings_tableLayoutPanel.Controls.Add(this.handyFilePath_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Controls.Add(this.symbols_groupBox, 0, 1);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 3;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(462, 137);
            this.settings_tableLayoutPanel.TabIndex = 33;
            // 
            // handyFilePath_groupBox
            // 
            this.handyFilePath_groupBox.Controls.Add(this.handyFilePath_textBox);
            this.handyFilePath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.handyFilePath_groupBox.Location = new System.Drawing.Point(3, 3);
            this.handyFilePath_groupBox.Name = "handyFilePath_groupBox";
            this.handyFilePath_groupBox.Size = new System.Drawing.Size(456, 49);
            this.handyFilePath_groupBox.TabIndex = 2;
            this.handyFilePath_groupBox.TabStop = false;
            this.handyFilePath_groupBox.Text = "Вручную отобранные файлы";
            // 
            // handyFilePath_textBox
            // 
            this.handyFilePath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.handyFilePath_textBox.Location = new System.Drawing.Point(3, 16);
            this.handyFilePath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.handyFilePath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.handyFilePath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.handyFilePath_textBox.Name = "handyFilePath_textBox";
            this.handyFilePath_textBox.Size = new System.Drawing.Size(450, 25);
            this.handyFilePath_textBox.TabIndex = 0;
            // 
            // symbols_groupBox
            // 
            this.symbols_groupBox.Controls.Add(this.symbols_tableLayoutPanel);
            this.symbols_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.symbols_groupBox.Location = new System.Drawing.Point(3, 58);
            this.symbols_groupBox.Name = "symbols_groupBox";
            this.symbols_groupBox.Size = new System.Drawing.Size(456, 49);
            this.symbols_groupBox.TabIndex = 3;
            this.symbols_groupBox.TabStop = false;
            this.symbols_groupBox.Text = "Допустимые символы";
            // 
            // symbols_tableLayoutPanel
            // 
            this.symbols_tableLayoutPanel.ColumnCount = 1;
            this.symbols_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.symbols_tableLayoutPanel.Controls.Add(this.symbols_textBox, 0, 0);
            this.symbols_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.symbols_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.symbols_tableLayoutPanel.Name = "symbols_tableLayoutPanel";
            this.symbols_tableLayoutPanel.RowCount = 1;
            this.symbols_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.symbols_tableLayoutPanel.Size = new System.Drawing.Size(450, 30);
            this.symbols_tableLayoutPanel.TabIndex = 1;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(462, 137);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.settings_tableLayoutPanel.PerformLayout();
            this.handyFilePath_groupBox.ResumeLayout(false);
            this.symbols_groupBox.ResumeLayout(false);
            this.symbols_tableLayoutPanel.ResumeLayout(false);
            this.symbols_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox symbols_textBox;
        private System.Windows.Forms.CheckBox isUseData_checkBox;
        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox handyFilePath_groupBox;
        private System.Windows.Forms.GroupBox symbols_groupBox;
        private System.Windows.Forms.TableLayoutPanel symbols_tableLayoutPanel;
        private Controls.TextBoxWithDialogButton handyFilePath_textBox;
    }
}
