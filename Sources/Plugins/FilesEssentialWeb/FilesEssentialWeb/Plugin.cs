/*
 * Плагин Избыточность WEB файлов
 * Файл FilesEssentialWeb.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчик: Кокин
 * 
 * Плагин предназначен для выявления зависимостей файлов относительно друг друга и 
 * построения графа зависимостей при выделении некоторых файлов в качестве корневых узлов.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;

namespace IA.Plugins.Analyses.FilesEssentialWeb
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Избыточность файлов WEB сайтов";

        /// <summary>
        /// Иденитификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.FILE_ESSENTIAL_WEB;

        #region Settings
        
        /// <summary>
        /// Вручную выбранные файлы
        /// </summary>
        internal static string fileManuals;

        /// <summary>
        /// Допустимые символы
        /// </summary>
        internal static string Symbols;

        /// <summary>
        /// Использование файлов данных
        /// </summary>
        internal static bool IsUseDATA;

        #endregion

        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Анализ файлов WEB сайтов")]
            WEB_FILES_ANALIZING
        };
    }

    /// <summary>
    /// плагин FilesEssentialWeb
    /// </summary>
    public class FilesEssentialWeb : IA.Plugin.Interface
    {
        #region FilesEssentialWeb fields

        /// <summary>
        /// Размер части файла, используемого для идентификации типа
        /// </summary>
        const int DefFileSIZE = 16384;
        
        Storage storage;

        Settings settings;

        /// <summary>
        /// true  --- если результаты работы плагина ещё не загружались (или генерировались)
        /// </summary>
        bool isNeedLoadResults = true;


        public ResultLists resultLists = new ResultLists();

        /// <summary>
        /// Список символов, 
        /// </summary>
        static List<char> listChar = new List<char>();

        /// <summary>
        /// Глобальная переменная для рекурсии "Путь найден!"
        /// </summary>
        static bool bPathFound = false;

        #endregion

        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return      Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW | 
                                Plugin.Capabilities.RESULT_WINDOW |
                                    Plugin.Capabilities.REPORTS |
                                        Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;
            
            PluginSettings.fileManuals = Properties.Settings.Default.fileManuals;
            PluginSettings.Symbols = Properties.Settings.Default.Symbols;
            PluginSettings.IsUseDATA = Properties.Settings.Default.IsUseDATA;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.fileManuals = settingsBuffer.GetString();
            PluginSettings.Symbols = settingsBuffer.GetString();
            settingsBuffer.GetBool(ref PluginSettings.IsUseDATA);
        }


        /// <summary>
        /// Прочитать результаты работы плагина, если это требуется
        /// </summary>
        private void LoadResults()
        {
            if (!isNeedLoadResults)
                return;

            IBufferReader reader = storage.pluginResults.LoadResult(PluginSettings.ID);
            if (reader != null)
            {
                resultLists.listNodes = (List<ClassNode.Node<WebFile>>)FilesEssentialWeb.BytesToObject(reader.GetByteArray());
                resultLists.listManual = (List<string>)FilesEssentialWeb.BytesToObject(reader.GetByteArray());

                RebuildIndexes(resultLists);
            }

            isNeedLoadResults = false;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.fileManuals = string.Empty;
            PluginSettings.IsUseDATA = false;
            PluginSettings.Symbols = @"A-Z|a-z|А-Я|а-я|0-9|\|/|.|~|@|#|%|^|-|_|(|)|;|,| |$|:|[|]";

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (settings = new Settings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                LoadResults();

                return new ResultWindow(storage, resultLists);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.fileManuals = PluginSettings.fileManuals;
            Properties.Settings.Default.Symbols = PluginSettings.Symbols;
            Properties.Settings.Default.IsUseDATA = PluginSettings.IsUseDATA;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.fileManuals);
            settingsBuffer.Add(PluginSettings.Symbols);
            settingsBuffer.Add(PluginSettings.IsUseDATA);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (!String.IsNullOrWhiteSpace(PluginSettings.fileManuals) && !File.Exists(PluginSettings.fileManuals))
                {
                    message = "Файл с вручную отобранными файлами не найден.";
                    return false;
                }
            if (!String.IsNullOrWhiteSpace(PluginSettings.fileManuals))
            {
                string[] listManuals = File.ReadAllLines(PluginSettings.fileManuals).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
                List<string> lsStorageFiles = storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Select(x => x.FileNameForReports).ToList();
                foreach (string filename in listManuals)
                {
                    if (!lsStorageFiles.Contains(filename))
                    {
                        message = "Отобранный файл отсутствует в Хранилище: " + filename;
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                    {
                        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.WEB_FILES_ANALIZING.Description(),0)
                    };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            //string sFileBody = null;
            //bool n_done = true;    
            isNeedLoadResults = false; //Если запустились повторно, то читать сохранённые результаты смысла не было

            // чистим списки
            resultLists.Clear();

            // Настройка списка символов, допустимых в качестве пути к файлам
            CreateListChars();

            // Формирование узлов графа зависимости файлов и выбор всех уникальных имен
            foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                ClassNode.Node<WebFile> node = new ClassNode.Node<WebFile>(new WebFile(file, file.NameWithoutExtension));
                resultLists.listNodes.Add(node);

                List<ClassNode.Node<WebFile>> list;
                if (!resultLists.ListNodesCategories.TryGetValue(node.Value.Name.ToLowerInvariant(), out list))
                {
                    list = new List<ClassNode.Node<WebFile>>();
                    resultLists.ListNodesCategories[node.Value.Name.ToLowerInvariant()] = list;
                }
                list.Add(node);
            }            

            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WEB_FILES_ANALIZING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)resultLists.listNodes.Count);

            // основной перебор
            int i = 0; //Счётчик для отображения прогресса
            foreach (List<ClassNode.Node<WebFile>> currListNodes in resultLists.ListNodesCategories.Values)
            {
                ///Для каждого включения имени файла без расширения в исходные тексты
                string searchString = IndexableName(currListNodes[0].Value.Name);
                foreach (FilesIndex.Occurrence occ in storage.filesIndex.FindWithoutRegister(searchString))
                {
                    string fn = storage.files.GetFile(occ.FileID).FullFileName_Canonized;  //Имя файла, в котором обнаружено включение

                    if (currListNodes.All(currNode => currNode.Value.StorageFileID == occ.FileID || LinkExisted(occ.FileID, currNode))) //Если для всех файлов связь уже построена, то и делать ничего не надо
                        continue;

                    //Читаем файл
                    FileOperations.AbstractReader buffer = null;
                    try
                    {
                        buffer = new FileOperations.AbstractReader(fn);//, AbstractReader.OpenMode.SLASH0IGNORE);
                    }
                    catch (Exception ex)
                    {
                        //Формируем сообщение
                        Monitor.Log.Error(PluginSettings.Name, new string[] { "Не удалось открыть файл " + fn + ".", ex.ToFullMultiLineMessage() }.ToMultiLineString());
                        continue;
                    }

                    string fileBody = buffer.getText(); //Не мудрствуя, считываем файл целиком

                    List<ClassNode.Node<WebFile>> nodes;
                    List<ClassNode.Node<WebFile>> candidates = currListNodes.FindAll(node => node.Value.StorageFileID != occ.FileID); //Из числа кандидатов исключаем сам файл с реализацией
                    if (IsOurAccurance(fileBody, occ, candidates, fn, out nodes)) //Проверяем, что это вхождение - действительно имя нашего файла
                    {
                        foreach(ClassNode.Node<WebFile> node in nodes)
                            FindNode(occ.FileID).AddChildren(node); //Добавляем связь
                        //n_done = false; //Раз есть хоть одно вхождение, то анализ по второму алгоритму делать не надо
                    }
                }

                //if (!n_done)
                //{
                i += currListNodes.Count;
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WEB_FILES_ANALIZING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)(i + 1));
                //}
            }

            #region Старый закомментированный алгоритм
            /*
            // Если не индексировали, делаем цикл в цикле: в каждом теле файла ищем вхождения имен остальных файлов
            if (n_done)
            {
                for (i = 0; i < resultLists.listNodes.Count; i++)
                {
                    ClassNode.Node<WebFile> currNode = resultLists.listNodes[i];

                    IFile file = storage.files.GetFile(currNode.Value.StorageFileID);
                    if ((file.fileType.Contains() == enTypeOfContents.DATA) && !PluginSettings.IsUseDATA)
                    {
                        IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WEB_FILES_ANALIZING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)(i + 1));
                        continue;
                    }

                    sFileBody = DefaultEncoding(ReadFile(currNode.Value.FullName)).ToLower();

                    for (int j = 0; j < resultLists.listNodes.Count; j++)
                    {
                        if (i == j)
                            continue;
                        string stop = resultLists.listNodes[j].Value.Name.ToLower();
                        int index = sFileBody.IndexOf(resultLists.listNodes[j].Value.Name.ToLower());

                        if (index != -1)
                        {
                            int ptr = index;
                            int optr;
                            string sres = TryGetMaxPath(sFileBody, resultLists.listNodes[j].Value.Name.ToLower(), ptr, out optr);
                            while (optr != -1)
                            {
                                if (resultLists.listNodes[j].Value.FullName.IndexOf(sres) != -1)
                                {
                                    currNode.AddChildren(resultLists.listNodes[j]);
                                    break;
                                }
                                else
                                {
                                    ptr = optr;
                                    sres = TryGetMaxPath(sFileBody, resultLists.listNodes[j].Value.Name.ToLower(), ptr, out optr);
                                }
                            }
                        }

                        if (resultLists.listDefines.Keys.Contains(currNode.Value.FullName) && resultLists.listDefines[currNode.Value.FullName].Contains(resultLists.listNodes[j].Value.FullName))
                            currNode.AddChildren(resultLists.listNodes[j]);
                    }
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WEB_FILES_ANALIZING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)(i + 1));
                }
            }
            */
            #endregion

            RebuildIndexes(resultLists);
        }

        /// <summary>
        /// При индексации мы отрезаем только идентификаторы. Однако имена файлов содержат более интересные символы. Сокращаем список до индексируемых символов
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private string IndexableName(string p)
        {
            List<char> listChar = storage.appliedSettings.IndexChars; //Символы, использованные при индексации

            int idx = 0;

            while (p.Length > idx && (((p[idx] >= 'A') && (p[idx] <= 'Z')) || (p[idx] == '_') || ((p[idx] >= 'a') && (p[idx] <= 'z')) || ((p[idx] >= 'А') && (p[idx] <= 'Я')) || ((p[idx] >= 'а') && (p[idx] <= 'я')) || ((p[idx] >= '0') && (p[idx] <= '9')) || (listChar.Count > 0) && (listChar.Contains(p[idx]))))
                idx++;

            if (idx == 0)
                Monitor.Log.Error(this.Name, "Выявлено имя файла, которое не могло использоваться при индексации: " + p);

            return p.Substring(0, idx);
        }

        /// <summary>
        /// Проверяет найденное вхождение на то, является ли оно ссылкой на файл
        /// </summary>
        /// <param name="fileBody"></param>
        /// <param name="currNode"></param>
        /// <param name="fn"></param>
        /// <param name="node">Выбранные файлы, на которые осуществляется ссылка. Если мы не можем определиться между какими-то файлами, то выбираем их все</param>
        /// <returns></returns>
        private bool IsOurAccurance(string fileBody, FilesIndex.Occurrence occurance,  List<ClassNode.Node<WebFile>> childListNodes, string parrentFileName, out List<ClassNode.Node<WebFile>> nodes)
        {
            //Извлекаем путь, записанный в найденном вхождении
            string path = ExtractPath(fileBody, occurance); 

            //Приводим путь к нормальному виду, удаляя все лишние символы
            
            if (path.Length !=0 && path[0] != '\\' && path[0] != '/') //Добавляем символ пути вначале, если надо
                path = '\\' + path.Substring(1);

            path = parrentFileName.Substring(0, parrentFileName.LastIndexOf('\\')) + path; //Добавляем к пути путь до файла, в котором мы нашли вхождение. Как правило, пути строятся относительно данной папки
                                                                                           //Это позволяет разробраться с различными конструкциями типа ../

            path = IOController.DirectoryController.CanonizePath(path);  //Теперь канонизируем путь

            return ComparePaths(childListNodes, path, out nodes);
        }

        /// <summary>
        /// Сравнивает пути. Если они совпадают больше, чем на имя файла, то пути считаются соответствующими друг другу
        /// </summary>
        /// <param name="p"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool ComparePaths(List<ClassNode.Node<WebFile>> candidates, string pathFromFile, out List<ClassNode.Node<WebFile>> res)
        {
            string[] p2 = pathFromFile.Split('\\');

            //Если имена файлов содержат расширение и совпадают с расширением, то порядок
            if (p2[p2.Length - 1].Contains('.'))
            {
                res = candidates.FindAll( node =>  node.Value.ExtName.ToLowerInvariant() == p2[p2.Length - 1]);

                if (res.Count == 1) //Вот оно - наше единственное
                    return true;

                if (res.Count > 1) //Найдено несколько. Надо смотреть пути
                    candidates = res;
            }

            //Имя файла из файла должно быть частью имени полного файла. Расширение может отсутствовать
            string[] names2 = p2[p2.Length - 1].Split('.');
            candidates = candidates.FindAll(node => 
                {
                    string[] names1 = node.Value.ExtName.ToLowerInvariant().Split('.');

                    if (names1.Length != names2.Length && names1.Length != names2.Length + 1)
                        return false;
                    for (int i = 0; i < names2.Length; i++)
                        if (names1[i] != names2[i])
                            return false;

                    return true;
                });

            //Если полные имена файлов не совпадают, то должны совпадать имена папок, непосредственно в которых лежат файлы
            candidates = candidates.FindAll(node =>
                {
                    string[] p1 = node.Value.FullName.Split('\\');
                    return p1[p1.Length - 2] == p2[p2.Length - 2];
                });

            //Если в результате анализа путей никаких вариантов не осталось, то останавливаемся
            if (candidates.Count == 0)
            {
                res = null;
                return false;
            }

            //Теперь ищем наиболее соответствующий вариант. То, что хотя бы один вариант подходит - уже точно.
            //Движемся по пути назад и сравниваем имена каталогов
            int j = 3;
            while(candidates.Count > 1 && j <= p2.Length)
            {
                List<ClassNode.Node<WebFile>> found = candidates.FindAll(node =>
                    {
                        string[] p1 = node.Value.FullName.Split('\\');
                        return p1.Length >= j && p1[p1.Length - j] == p2[p2.Length - j];
                    });

                //Проверяем, что движение по путям далее является позитивным
                if (found.Count > 0)
                    candidates = found;
                else
                { //Совпадение путей закончилось. Все предыдущие кандидаты являются равнозначно соответствующими
                    res = candidates;
                    return true;
                }

                j++;
            }

            res = candidates;
            //По умолчанию - вхождение не наше
            return true;
        }

        /// <summary>
        /// Извлекает имя файла из неполноценного имени файла (без пути)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string GetFileName(string path)
        {
            if(!path.Contains('.'))
                return path;

            return path.Substring(0, path.IndexOf('.'));
        }

        /// <summary>
        /// Имея место вхождения, вытаскиваем определяемый им путь
        /// </summary>
        /// <param name="fileBody"></param>
        /// <param name="occurance"></param>
        /// <returns></returns>
        private string ExtractPath(string fileBody, FilesIndex.Occurrence occurance)
        {
            int leftIdx = (int) occurance.Offset;
            int rightIdx = (int) occurance.Offset;

            while(leftIdx > 0 && listChar.Contains(fileBody[leftIdx]))
            {leftIdx --;}

            while (rightIdx < fileBody.Length -1 && listChar.Contains(fileBody[rightIdx]))
            { rightIdx++; }

            return fileBody.Substring(leftIdx, rightIdx - leftIdx);
        }

        /// <summary>
        /// Находит файл по его идентификатору
        /// 
        /// Реализация крайне неэффективная. Если будет тормозить, надо будет переписать на индексы
        /// </summary>
        /// <param name="fileID"></param>
        /// <returns></returns>
        private ClassNode.Node<WebFile> FindNode(UInt64 fileID)
        {
            return resultLists.listNodes.Find(node => node.Value.StorageFileID == fileID);
        }

        /// <summary>
        /// Проверяет, если ли родительская связь между parrentNode и childFileID.
        /// 
        /// Реализация крайне неэффективная. Если будет тормозить, надо будет сделать индексы и вообще переписать всё вокруг
        /// </summary>
        /// <param name="parrentNode"></param>
        /// <param name="childFileID"></param>
        /// <returns>true, если childFileID является ребенком parrentNode<returns>
        private bool LinkExisted(UInt64 ParrentFileID, ClassNode.Node<WebFile> ChildNode)
        {
            return resultLists.listNodes.Any(node => node.Value.StorageFileID == ParrentFileID && node.Children.Contains(ChildNode));
        }

        /// <summary>
        /// Формирование списков неизбыточных и избыточных файлов
        /// </summary>
        public static void Essential(ResultLists resultLists)
        {
            if (resultLists.listManual.Count != 0)
                resultLists.listEssential = resultLists.listManual.Select(x => AllChild(resultLists.listNodes[resultLists.listSources.IndexOf(x)], resultLists).ToList()).Aggregate((a, b) => new List<string>(a.Concat(b))).Distinct().ToList();
            else
                resultLists.listEssential = new List<string>();
            resultLists.listRedundant = resultLists.listSources.Where(x => !resultLists.listEssential.Contains(x)).ToList();
            resultLists.listExplanation.Clear();
            foreach (string ss in resultLists.listEssential)
                resultLists.listExplanation.Add(Explain(resultLists.listNodes[resultLists.listSources.IndexOf(ss)], resultLists));
        }

        private static void RebuildIndexes(ResultLists resultLists)
        {
            // Заполняем таблицы
            resultLists.listSources = resultLists.listNodes.Select(x => x.Value.ReportName).ToList();
            resultLists.listRoots = resultLists.listNodes.Where(x => x.Children.Count > 0 & x.Parent.Count == 0).Select(x => x.Value.ReportName).ToList();
            if (!string.IsNullOrWhiteSpace(PluginSettings.fileManuals))
                resultLists.listManual = File.ReadAllLines(PluginSettings.fileManuals).Where(x => resultLists.listSources.Contains(x)).Distinct().ToList();
            else
                resultLists.listManual = new List<string>();

            Essential(resultLists);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
            List<string> fullEssential = resultLists.listEssential.Select(x => Path.Combine(storage.appliedSettings.FileListPath, x)).ToList();
            foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
            {

                if (fullEssential.Contains(file.FullFileName_Original))
                    file.FileEssential = enFileEssential.ESSENTIAL;
                else
                    file.FileEssential = enFileEssential.SUPERFLUOUS;
            }
            FilesEssentialWeb.Essential(resultLists);


            // Сохранить в таблице Storage.PluginResults список узлов графа listNodes
            byte[] arr = ObjectToBytes(resultLists.listNodes);
            Store.Table.IBufferWriter ibw = Store.Table.WriterPool.Get();
            ibw.Add(arr);
            
            // Сохранить в таблице Storage.PluginResults список вручную отобранных файлов
            arr = ObjectToBytes(resultLists.listManual);
            ibw.Add(arr);
            storage.pluginResults.SaveResults(PluginSettings.ID, ibw);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            LoadResults();

            //Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");
            
            //FIXME - тут неплохо было бы добавить прогресс генерации отчётов (по количеству отчётов)
            List<string> report1 = new List<string>(resultLists.listEssential);
            report1.Insert(0, "Неизбыточные файлы");
            File.WriteAllLines(Path.Combine(reportsDirectoryPath, "Неизбыточные файлы.txt"), report1);

            resultLists.listRedundant.Sort();
            using (StreamWriter Writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Избыточные файлы.txt")))
            {
                Writer.WriteLine("Избыточные файлы");
                for (int i = 0; i < resultLists.listRedundant.Count; i++)
                    Writer.WriteLine(resultLists.listRedundant[i]);
            }
            resultLists.listExplanation.Sort();
            using (StreamWriter Writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Обоснование.txt")))
            {
                Writer.WriteLine("Цепочка зависимостй, приводящая к заведомо неизбыточным файлам");
                for (int i = 0; i < resultLists.listExplanation.Count; i++)
                    Writer.WriteLine(resultLists.listExplanation[i]);
            }

            resultLists.listManual.Sort();
            using (StreamWriter Writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Вручную выбранные файлы.txt")))
            {
                Writer.WriteLine("Неизбыточные файлы, указанные в установках, а также установленные вручную");
                for (int i = 0; i < resultLists.listManual.Count; i++)
                    Writer.WriteLine(resultLists.listManual[i]);
            }

            using (StreamWriter Writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Зависимости.txt")))
            {
                Writer.WriteLine("Зависимости файлов");
                foreach (ClassNode.Node<WebFile> node in resultLists.listNodes)
                    foreach (ClassNode.Node<WebFile> child in node.Children)
                        Writer.WriteLine(node.Value.ReportName + " -> " + child.Value.ReportName);
            }

            using (StreamWriter Writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Корневые файлы.txt")))
            {
                Writer.WriteLine("Корневые файлы. Таковыми являются файлы, которые не использует ни один другой файл, но которые, в свою очередь, сами используют файлы.");
                foreach (ClassNode.Node<WebFile> node in resultLists.listNodes)
                    if (node.Parent.Count == 0 && node.Children.Count > 0)
                        Writer.WriteLine(node.Value.ReportName);
            }

            using (StreamWriter Writer = new StreamWriter(Path.Combine(reportsDirectoryPath, "Файлы без зависимостей.txt")))
            {
                Writer.WriteLine("Файлы, не имеющие зависимостей. То есть ни один файл не использует их и они не используют ни один файл.");
                foreach (ClassNode.Node<WebFile> node in resultLists.listNodes)
                    if (node.Parent.Count == 0 && node.Children.Count == 0)
                        Writer.WriteLine(node.Value.ReportName);
            }

        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }

        #endregion

        #region Other Code

        /// <summary>
        /// Формирование списка зависимостей
        /// </summary>
        /// <param name="tmp"></param>
        /// <param name="ls"></param>
        internal static void CollectAllChildNames(ClassNode.Node<WebFile> tmp, ref List<string> ls)
        {
            if (tmp.Marker)
                return;

            ls.Add(tmp.Value.ReportName);
            tmp.Marker = true;

            foreach (ClassNode.Node<WebFile> child in tmp.Children)
                CollectAllChildNames(child, ref ls);

        }

        /// <summary>
        /// Формирование списка зависимостей от файла
        /// </summary>
        /// <param name="tmp">файл</param>
        /// <param name="resultLists"></param>
        /// <returns></returns>
        internal static List<string> AllChild(ClassNode.Node<WebFile> tmp, ResultLists resultLists)
        {
            List<string> ret = new List<string>();
            ClearAllMarks(resultLists.listNodes);
            CollectAllChildNames(tmp, ref ret);
            return ret;
        }

        /// <summary>
        /// Формирование списка обоснований для отчета
        /// </summary>
        /// <param name="nd">Текущий файл</param>
        /// <param name="resultLists">список вручную отобранных файлов</param>
        /// <returns></returns>
        internal static string Explain(ClassNode.Node<WebFile> nd, ResultLists resultLists)
        {
            List<string> ls1 = new List<string>();
            List<string> ls2 = new List<string>();
            bPathFound = false;

            ClearAllMarks(resultLists.listNodes);

            MinPath(nd, ls1, ref ls2, 0, resultLists.listEssential, resultLists);
            return ls2.Count > 1 ? ls2.Aggregate((a, b) => a + " -> " + b) : nd.Value.ReportName + " - установлено вручную";

            //List<string> NodesChain = MinPath(nd,ls2);
            //return NodesChain.Count > 1 ? NodesChain.Aggregate((a, b) => a + " -> " + b) : nd.Value.ReportName + " - установлено вручную";
        }

        /// <summary>
        /// Поиск минимального пути для отчета обоснования (рекурсивная)
        /// </summary>
        /// <param name="nd">текущий файл</param>
        /// <param name="ls1">временный список зависимостей</param>
        /// <param name="ls2">выходной список - минимальная зависимость</param>
        /// <param name="level">уровень рекурсии</param>
        /// <param name="lsUR">список неизбыточных файлов</param>
        /// <param name="resultLists">результат</param>
        private static void MinPath(ClassNode.Node<WebFile> nd, List<string> ls1, ref List<string> ls2, int level, List<string> lsUR, ResultLists resultLists)
        {
            if (bPathFound)
                return;
            if (nd.Marker)
                return;
            if (!lsUR.Contains(nd.Value.ReportName))
                return;
            nd.Marker = true;
            if (ls1.Count > level)
                ls1 = ls1.Take(level).ToList();
            ls1.Add(nd.Value.ReportName);
            level++;
            if (resultLists.listManual.Contains(nd.Value.ReportName))
            {
                bPathFound = true;
                ls2.AddRange(ls1);
                return;
            }
            else
            {
                foreach (ClassNode.Node<WebFile> nd1 in nd.Parent)
                    MinPath(nd1, ls1, ref ls2, level, lsUR, resultLists);
            }
        }

        /// <summary>
        /// Поиск минимального пути для отчета обоснования (рекурсивная)
        /// </summary>
        /// <param name="nd">Экземпляр класса ClassNode.Node<WebFile></param>
        /// <param name="nodesChain">Список вручную отобранных файлов</param>
        /// <returns>Список зависимостей</returns>
        private static List<string> MinPath(ClassNode.Node<WebFile> nd, List<string> nodesChain, ResultLists resultLists)
        {
            if (bPathFound)
                return nodesChain;
            if (nd.Marker)
                return nodesChain;
            nd.Marker = true;
            nodesChain.Add(nd.Value.ReportName);
            if (resultLists.listManual.Contains(nd.Value.ReportName))
            {
                bPathFound = true;
                return nodesChain;
            }
            else
            {
                //nd.Parent.Where(x => !PluginSettings.listEssential.Contains(x.Value.ReportName)).ForEach((x) => { MinPath(x, nodesChain); });

                foreach (ClassNode.Node<WebFile> nd1 in nd.Parent.Where(x => !resultLists.listEssential.Contains(x.Value.ReportName)))
                    return MinPath(nd1, nodesChain, resultLists);
            }
            return nodesChain;
        }
        
        /// <summary>
        /// Конвертация сериализуемого объекта в байтовый массив
        /// </summary>
        /// <param name="dataObj"></param>
        /// <returns></returns>
        internal static byte[] ObjectToBytes(object dataObj)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();

            formatter.Serialize(ms, dataObj);
            
            return ms.GetBuffer();
        }
        
        /// <summary>
        /// Конвертация байтового массива в сериализуемый объект 
        /// </summary>
        /// <param name="dataBytes"></param>
        /// <returns></returns>
        internal static object BytesToObject(byte[] dataBytes)
        {
            return new BinaryFormatter().Deserialize(new MemoryStream(dataBytes));
        }
        
        /// <summary>
        /// Получение максимальной строки по правилам пути ОС, содержащей шаблон в виде имени файла
        /// </summary>
        /// <param name="sFileBody"></param>
        /// <param name="shablon"></param>
        /// <param name="ptr"></param>
        /// <param name="optr"></param>
        /// <returns></returns>
        internal static string TryGetMaxPath(string sFileBody, string shablon, int ptr, out int optr)
        {
            if (shablon.Length == 0) //Проверка на пустые шаблоны.
            {
                optr = -1;
                return null;
            }

            optr = sFileBody.IndexOf(shablon, ptr);

            if (optr == -1)
                return String.Empty;

            ptr = optr;
            // Ищем в тексте влево и вправо от найденного слова все допустимые символы в наименовании пути к файлу
            while (ptr > 0)
            {
                if (IsChar(sFileBody[ptr-1], listChar))
                    ptr--;
                else
                    break;
            }

            optr += shablon.Length;

            while (optr < sFileBody.Length-1)
            {
                if (IsChar(sFileBody[optr], listChar))
                    optr++;
                else
                    break;
            }

            // Делаем знак разделения папок одинаковым
            string sret = sFileBody.Substring(ptr, optr - ptr).Replace("/", "\\");

            // Если путь включаает в себя элементы ..\, то все, что до них (включая) - нужно убрать, т.к. мы не знаем стартовой папки
            int ptr1 = sret.LastIndexOf("..\\");

            if (ptr1 >= 0)
            {
                //optr += ptr1 + "..\\".Length;
                sret = sret.Substring(ptr1 + "..\\".Length);

            }

            ptr1 = sret.LastIndexOf(".\\");

            if (ptr1 >= 0)
                sret = sret.Replace(".\\","");

            if (sret.StartsWith("\\"))
                sret.TrimStart('\\');

            return sret;
        }
        
        /// <summary>
        /// Получение максимальной строки по правилам пути ОС, начиная от текущей позиции в файле
        /// </summary>
        /// <param name="sFileBody"></param>
        /// <param name="ptr"></param>
        /// <param name="optr"></param>
        /// <returns></returns>
        internal static string GetCurrentPath(string sFileBody, int ptr, out int optr)
        {
            if (!IsChar(sFileBody[ptr], listChar))
            {
                optr = -1;
                return "";
            }

            // Ищем в тексте влево и вправо от найденного слова все допустимые символы в наименовании пути к файлу
            optr = ptr;
            while (ptr >= 0)
            {
                if (IsChar(sFileBody[ptr], listChar))
                    ptr--;
                else
                    break;
            }

            ptr++;
            while (optr < sFileBody.Length)
            {
                if (IsChar(sFileBody[optr], listChar))
                    optr++;
                else
                    break;
            }

            // Делаем знак разделения папок одинаковым
            return sFileBody.Substring(ptr, optr - ptr);
        }
        
        /// <summary>
        /// Проверка, является ли символ подходящим в качестве символа пути ОС
        /// </summary>
        /// <param name="x"></param>
        /// <param name="ls"></param>
        /// <returns></returns>
        static bool IsChar(char x, List<char> ls)
        {
            return ls.Contains(x);
        }

        /// <summary>
        /// Формирование списка символов из заготовки
        /// </summary>
        internal static void CreateListChars()
        {
            string listCharInit;

            if (string.IsNullOrWhiteSpace(PluginSettings.Symbols))
                listCharInit = @"A-Z|a-z|А-Я|а-я|0-9|\|/|.|~|@|#|%|^|-|_|(|)|;|,| |$|:|[|]|";
            else
                listCharInit = PluginSettings.Symbols;

            listChar.Clear();

            string[] sa = listCharInit.Split('|').Where(x => x != string.Empty).ToArray();

            foreach (string s in sa)
            {
                switch (s.Length)
                {
                    case 1:
                        listChar.Add(s[0]);
                        break;
                    case 3:
                        if (s[1] == '-')
                        {
                            string[] sb = s.Split('-').Where(x => x != "").ToArray();
                            if (sb.Length == 2 && (int)sb[1][0] >= (int)sb[0][0])
                            {
                                listChar.AddRange(Enumerable.Range((int)sb[0][0], (int)sb[1][0] - (int)sb[0][0] + 1).Select(x => Convert.ToChar(x)).ToList());
                            }
                        }
                        break;
                }
            }
            listChar = listChar.Distinct().ToList();
        }  
        
        /// <summary>
        /// Чтение файла в байтовый массив
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        internal static byte[] ReadFile(string filePath)
        {
            byte[] buffer = null;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            
            try
            {
                int length = (int)fileStream.Length;
                buffer = new byte[length];
                if (length != fileStream.Read(buffer, 0, length))
                    MessageBox.Show("Ошибка чтения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                fileStream.Close();
            }

            return buffer;
        }

        /// <summary>
        /// Очистка всех пометок в графе
        /// </summary>
        /// <param name="listNodes"></param>
        internal static void ClearAllMarks(List<ClassNode.Node<WebFile>> listNodes)
        {
            listNodes.ForEach(n => n.Marker = false);
        }

        /// <summary>
        /// Конвертация байтового массива в строку с учетом кодировки файла
        /// </summary>
        /// <param name="arrayIn"></param>
        /// <returns></returns>
        internal static string DefaultEncoding(byte[] arrayIn)
        {
            string txt = System.Text.Encoding.Default.GetString(arrayIn);

            Encoding encoding = Encoding.Default;

            if (txt.StartsWith("п»"))
            {
                txt = System.Text.Encoding.UTF8.GetString(arrayIn);
                encoding = Encoding.UTF8;
            }
            else if (txt.StartsWith("яю"))
            {
                txt = System.Text.Encoding.Unicode.GetString(arrayIn);
                encoding = Encoding.Unicode;
            }

            return txt;
        }

        static Encoding encoding = Encoding.Default;

        /// <summary>
        /// Перекодировка байтового массива в строку с учетом кодировки файла и далее - в байтовый массив с кодировкой Default
        /// </summary>
        /// <param name="arrayIn">массив байт, представляющий собой содержимое файла</param>
        /// <returns>массив байт в кодировке Default</returns>
        internal static byte[] FileEncoding(byte[] arrayIn)
        {
            if (arrayIn == null || arrayIn.Length < 3)
                return arrayIn;

            string txt = Encoding.Default.GetString(arrayIn.Take(3).ToArray());

            if (txt.StartsWith("п»ї"))
                return Encoding.Default.GetBytes(Encoding.UTF8.GetString(arrayIn));
            else if (txt.StartsWith("яю"))
                return Encoding.Default.GetBytes(Encoding.Unicode.GetString(arrayIn));
            
            return arrayIn;
        }


        internal bool isTask()
        {
            return true;
        }


        #region Списки выходной формы
        /// <summary>
        /// Элементы класса используются в выходной форме и в тестах
        /// </summary>
        public class ResultLists
        {
            /// <summary>
            /// Исходные файлы
            /// </summary>
            public List<string> listSources = new List<string>();
            /// <summary>
            /// Корневые файлы
            /// </summary>
            public List<string> listRoots = new List<string>();
            /// <summary>
            /// Родители выделенного файла в списке неизбыточных файлов
            /// </summary>
            public List<string> listManual = new List<string>();
            /// <summary>
            /// Неизбыточные файлы
            /// </summary>
            public List<string> listEssential = new List<string>();
            /// <summary>
            /// Избыточные файлы
            /// </summary>
            public List<string> listRedundant = new List<string>();
            /// <summary>
            /// Обоснование
            /// </summary>
            public List<string> listExplanation = new List<string>();

            /// <summary>
            /// cписок узлов (файлов) - каждый узел может иметь детей из этого же списка
            /// </summary>
            public List<ClassNode.Node<WebFile>> listNodes = new List<ClassNode.Node<WebFile>>();
            
            /// <summary>
            /// список имен файлов и для каждого имени список подходящих файлов
            /// </summary>
            public Dictionary<string, List<ClassNode.Node<WebFile>>> ListNodesCategories = new Dictionary<string, List<ClassNode.Node<WebFile>>>();
            
            public void Clear()
            {
                listRedundant.Clear();
                listEssential.Clear();
                listExplanation.Clear();
                listNodes.Clear();
                ListNodesCategories.Clear();
            }
        }
        #endregion


    }
    /// <summary>
    /// Класс узла графа
    /// </summary>
    [Serializable]
    public class ClassNode
    {
        [Serializable]
        public class Node<T>
        {
            private NodeList<T> _Parent;
            private NodeList<T> _Children;
            internal NodeList<T> Parent
            {
                get { return _Parent; }
                set { _Parent = value; }
            }
            internal Node<T> AddParent(Node<T> t)
            {
                if (!_Parent.Contains(t))
                    _Parent.Add(t);
                if (!t.Children.Contains(this))
                    t.Children.Add(this);
                return this;
            }
            internal Node<T> RemoveParent(Node<T> t)
            {
                if (_Parent.Contains(t))
                    _Parent.Remove(t);
                if (t.Children.Contains(this))
                    t.Children.Remove(this);
                return this;
            }
            internal Node<T> AddChildren(Node<T> t)
            {
                if (!_Children.Contains(t))
                    _Children.Add(t);
                if (!t.Parent.Contains(this))
                    t.AddParent(this);
                return this;
            }

            internal NodeList<T> Children
            {
                get { return _Children; }
                private set { _Children = value; }
            }
            private T _Value;
            internal bool Marker
            {
                get;
                set;
            }
            internal T Value
            {
                get { return _Value; }
                set
                {
                    _Value = value;
                }
            }
            internal Node()
            {
                this.Value = default(T);
                Parent = new NodeList<T>();
                Children = new NodeList<T>();
                Marker = false;
            }
            internal Node(T Value)
            {
                this.Value = Value;
                Parent = new NodeList<T>();
                Children = new NodeList<T>();
                Marker = false;
            }
            internal void ClearChildMarks(Node<T> nt)
            {
                if (nt.Marker == false)
                    return;
                nt.Marker = false;
                foreach (Node<T> nch in nt.Children)
                    ClearChildMarks(nch);
            }
            internal void ClearParentMarks(Node<T> nt)
            {
                if (nt.Marker == false)
                    return;
                nt.Marker = false;
                foreach (Node<T> np in nt.Parent)
                    ClearParentMarks(np);
            }

        }
        
        /// <summary>
        /// Класс графа
        /// </summary>
        /// <typeparam name="T"></typeparam>
        [Serializable]
        internal class NodeList<T> : List<Node<T>>
        {
        }
    }

    /// <summary>
    /// Описание Web-файла
    /// </summary>
    [Serializable]
    public class WebFile
    {
        // APK - удалил IFile, т.к. тянет за собой все хранилище, и дальше - XMLDocuments...

        /// <summary>
        /// Имя файла, используемое при анализе. Может быть без расширения, может быть с расширением. Но в любом случае без пути.
        /// </summary>
        string name;

        /// <summary>
        /// Полный путь до web-файла. Канонизированный
        /// </summary>
        internal string FullName
        {
            set;
            get;
        }

        /// <summary>
        /// Ссылка на файл из Хранилища
        /// </summary>
        internal ulong StorageFileID
        {
            set;
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        internal string ExtName
        {
            set;
            get;
        }

        /// <summary>
        /// Имя фалй, используемое в отчётах. Путь сокращенный
        /// </summary>
        internal string ReportName
        {
            set;
            get;
        }

        /// <summary>
        /// Имя файла, используемое при анализе. Может быть без расширения, может быть с расширением. Но в любом случае без пути.
        /// </summary>
        internal string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Конструктор описания файла
        /// </summary>
        /// <param name="file">файл Хранилища</param>
        /// <param name="name">имя файла, используемое для анализа</param>
        internal WebFile(IFile file, string name)
        {
            StorageFileID = file.Id;
            FullName =  file.FullFileName_Canonized;
            ReportName = file.FileNameForReports;
            ExtName = file.NameAndExtension;
            this.name = name;
        }

        #endregion

    }
}
