﻿namespace IA.Plugins.Analyses.FilesEssentialWeb
{
    internal partial class ResultWindow
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.sourceFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.sourceFiles_listBox = new System.Windows.Forms.ListBox();
            this.fromSourcesToManualFiles_button = new System.Windows.Forms.Button();
            this.parents_groupBox = new System.Windows.Forms.GroupBox();
            this.parents_listBox = new System.Windows.Forms.ListBox();
            this.sourceText_groupBox = new System.Windows.Forms.GroupBox();
            this.sourceText_richTextBox = new System.Windows.Forms.RichTextBox();
            this.manualFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.deleteFromManualFiles_button = new System.Windows.Forms.Button();
            this.ManualFiles_listBox = new System.Windows.Forms.ListBox();
            this.essentialFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.fromEssentialToRedundantFiles_button = new System.Windows.Forms.Button();
            this.essentialFiles_listBox = new System.Windows.Forms.ListBox();
            this.fromRedundantToEssentialFiles_button = new System.Windows.Forms.Button();
            this.redunduntFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.redunduntFiles_listBox = new System.Windows.Forms.ListBox();
            this.resultWindow_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.Children_GroupBox = new System.Windows.Forms.GroupBox();
            this.children_listBox = new System.Windows.Forms.ListBox();
            this.sourceFiles_groupBox.SuspendLayout();
            this.parents_groupBox.SuspendLayout();
            this.sourceText_groupBox.SuspendLayout();
            this.manualFiles_groupBox.SuspendLayout();
            this.essentialFiles_groupBox.SuspendLayout();
            this.redunduntFiles_groupBox.SuspendLayout();
            this.resultWindow_tableLayoutPanel.SuspendLayout();
            this.Children_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // sourceFiles_groupBox
            // 
            this.sourceFiles_groupBox.Controls.Add(this.sourceFiles_listBox);
            this.sourceFiles_groupBox.Controls.Add(this.fromSourcesToManualFiles_button);
            this.sourceFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceFiles_groupBox.Location = new System.Drawing.Point(0, 0);
            this.sourceFiles_groupBox.Margin = new System.Windows.Forms.Padding(0);
            this.sourceFiles_groupBox.Name = "sourceFiles_groupBox";
            this.sourceFiles_groupBox.Size = new System.Drawing.Size(354, 178);
            this.sourceFiles_groupBox.TabIndex = 0;
            this.sourceFiles_groupBox.TabStop = false;
            this.sourceFiles_groupBox.Text = "Исходные файлы";
            // 
            // sourceFiles_listBox
            // 
            this.sourceFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceFiles_listBox.FormattingEnabled = true;
            this.sourceFiles_listBox.Location = new System.Drawing.Point(3, 16);
            this.sourceFiles_listBox.Margin = new System.Windows.Forms.Padding(0);
            this.sourceFiles_listBox.Name = "sourceFiles_listBox";
            this.sourceFiles_listBox.Size = new System.Drawing.Size(348, 159);
            this.sourceFiles_listBox.TabIndex = 0;
            this.sourceFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.sourceFiles_listBox_SelectedIndexChanged);
            // 
            // fromSourcesToManualFiles_button
            // 
            this.fromSourcesToManualFiles_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fromSourcesToManualFiles_button.Location = new System.Drawing.Point(190, 0);
            this.fromSourcesToManualFiles_button.Margin = new System.Windows.Forms.Padding(0);
            this.fromSourcesToManualFiles_button.Name = "fromSourcesToManualFiles_button";
            this.fromSourcesToManualFiles_button.Size = new System.Drawing.Size(147, 16);
            this.fromSourcesToManualFiles_button.TabIndex = 0;
            this.fromSourcesToManualFiles_button.Text = "Перенести в неизбыточные";
            this.fromSourcesToManualFiles_button.UseVisualStyleBackColor = true;
            this.fromSourcesToManualFiles_button.Click += new System.EventHandler(this.fromSourcesToManualFiles_button_Click);
            // 
            // parents_groupBox
            // 
            this.parents_groupBox.Controls.Add(this.parents_listBox);
            this.parents_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parents_groupBox.Location = new System.Drawing.Point(0, 178);
            this.parents_groupBox.Margin = new System.Windows.Forms.Padding(0);
            this.parents_groupBox.Name = "parents_groupBox";
            this.parents_groupBox.Size = new System.Drawing.Size(354, 178);
            this.parents_groupBox.TabIndex = 0;
            this.parents_groupBox.TabStop = false;
            this.parents_groupBox.Text = "Родители выбранного файла";
            // 
            // parents_listBox
            // 
            this.parents_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parents_listBox.FormattingEnabled = true;
            this.parents_listBox.Location = new System.Drawing.Point(3, 16);
            this.parents_listBox.Margin = new System.Windows.Forms.Padding(0);
            this.parents_listBox.Name = "parents_listBox";
            this.parents_listBox.Size = new System.Drawing.Size(348, 159);
            this.parents_listBox.TabIndex = 0;
            this.parents_listBox.SelectedIndexChanged += new System.EventHandler(this.parents_listBox_SelectedIndexChanged);
            // 
            // sourceText_groupBox
            // 
            this.sourceText_groupBox.Controls.Add(this.sourceText_richTextBox);
            this.sourceText_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceText_groupBox.Location = new System.Drawing.Point(354, 0);
            this.sourceText_groupBox.Margin = new System.Windows.Forms.Padding(0);
            this.sourceText_groupBox.Name = "sourceText_groupBox";
            this.resultWindow_tableLayoutPanel.SetRowSpan(this.sourceText_groupBox, 3);
            this.sourceText_groupBox.Size = new System.Drawing.Size(362, 534);
            this.sourceText_groupBox.TabIndex = 0;
            this.sourceText_groupBox.TabStop = false;
            this.sourceText_groupBox.Text = "Исходный тект";
            // 
            // sourceText_richTextBox
            // 
            this.sourceText_richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceText_richTextBox.Location = new System.Drawing.Point(3, 16);
            this.sourceText_richTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.sourceText_richTextBox.Name = "sourceText_richTextBox";
            this.sourceText_richTextBox.ReadOnly = true;
            this.sourceText_richTextBox.Size = new System.Drawing.Size(356, 515);
            this.sourceText_richTextBox.TabIndex = 0;
            this.sourceText_richTextBox.Text = "";
            // 
            // manualFiles_groupBox
            // 
            this.manualFiles_groupBox.Controls.Add(this.deleteFromManualFiles_button);
            this.manualFiles_groupBox.Controls.Add(this.ManualFiles_listBox);
            this.manualFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manualFiles_groupBox.Location = new System.Drawing.Point(716, 0);
            this.manualFiles_groupBox.Margin = new System.Windows.Forms.Padding(0);
            this.manualFiles_groupBox.Name = "manualFiles_groupBox";
            this.manualFiles_groupBox.Size = new System.Drawing.Size(348, 178);
            this.manualFiles_groupBox.TabIndex = 0;
            this.manualFiles_groupBox.TabStop = false;
            this.manualFiles_groupBox.Text = "Вручную отобранные файлы";
            // 
            // deleteFromManualFiles_button
            // 
            this.deleteFromManualFiles_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.deleteFromManualFiles_button.Location = new System.Drawing.Point(193, 0);
            this.deleteFromManualFiles_button.Margin = new System.Windows.Forms.Padding(0);
            this.deleteFromManualFiles_button.Name = "deleteFromManualFiles_button";
            this.deleteFromManualFiles_button.Size = new System.Drawing.Size(141, 16);
            this.deleteFromManualFiles_button.TabIndex = 2;
            this.deleteFromManualFiles_button.Text = "Очистить список неизбыточных";
            this.deleteFromManualFiles_button.UseVisualStyleBackColor = true;
            this.deleteFromManualFiles_button.Click += new System.EventHandler(this.deleteFromManualFiles_button_Click);
            // 
            // ManualFiles_listBox
            // 
            this.ManualFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ManualFiles_listBox.FormattingEnabled = true;
            this.ManualFiles_listBox.Location = new System.Drawing.Point(3, 16);
            this.ManualFiles_listBox.Margin = new System.Windows.Forms.Padding(0);
            this.ManualFiles_listBox.Name = "ManualFiles_listBox";
            this.ManualFiles_listBox.Size = new System.Drawing.Size(342, 159);
            this.ManualFiles_listBox.TabIndex = 0;
            this.ManualFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.ManualFiles_listBox_SelectedIndexChanged);
            // 
            // essentialFiles_groupBox
            // 
            this.essentialFiles_groupBox.Controls.Add(this.fromEssentialToRedundantFiles_button);
            this.essentialFiles_groupBox.Controls.Add(this.essentialFiles_listBox);
            this.essentialFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.essentialFiles_groupBox.Location = new System.Drawing.Point(716, 178);
            this.essentialFiles_groupBox.Margin = new System.Windows.Forms.Padding(0);
            this.essentialFiles_groupBox.Name = "essentialFiles_groupBox";
            this.essentialFiles_groupBox.Size = new System.Drawing.Size(348, 178);
            this.essentialFiles_groupBox.TabIndex = 0;
            this.essentialFiles_groupBox.TabStop = false;
            this.essentialFiles_groupBox.Text = "Неизбыточные файлы";
            // 
            // fromEssentialToRedundantFiles_button
            // 
            this.fromEssentialToRedundantFiles_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fromEssentialToRedundantFiles_button.Location = new System.Drawing.Point(193, 0);
            this.fromEssentialToRedundantFiles_button.Margin = new System.Windows.Forms.Padding(0);
            this.fromEssentialToRedundantFiles_button.Name = "fromEssentialToRedundantFiles_button";
            this.fromEssentialToRedundantFiles_button.Size = new System.Drawing.Size(141, 16);
            this.fromEssentialToRedundantFiles_button.TabIndex = 3;
            this.fromEssentialToRedundantFiles_button.Text = "Сделать избыточным";
            this.fromEssentialToRedundantFiles_button.UseVisualStyleBackColor = true;
            this.fromEssentialToRedundantFiles_button.Click += new System.EventHandler(this.fromEssentialToRedundantFiles_button_Click);
            // 
            // essentialFiles_listBox
            // 
            this.essentialFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.essentialFiles_listBox.FormattingEnabled = true;
            this.essentialFiles_listBox.Location = new System.Drawing.Point(3, 16);
            this.essentialFiles_listBox.Margin = new System.Windows.Forms.Padding(0);
            this.essentialFiles_listBox.Name = "essentialFiles_listBox";
            this.essentialFiles_listBox.Size = new System.Drawing.Size(342, 159);
            this.essentialFiles_listBox.Sorted = true;
            this.essentialFiles_listBox.TabIndex = 0;
            this.essentialFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.essentialFiles_listBox_SelectedIndexChanged);
            // 
            // fromRedundantToEssentialFiles_button
            // 
            this.fromRedundantToEssentialFiles_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fromRedundantToEssentialFiles_button.Location = new System.Drawing.Point(193, 0);
            this.fromRedundantToEssentialFiles_button.Margin = new System.Windows.Forms.Padding(0);
            this.fromRedundantToEssentialFiles_button.Name = "fromRedundantToEssentialFiles_button";
            this.fromRedundantToEssentialFiles_button.Size = new System.Drawing.Size(141, 16);
            this.fromRedundantToEssentialFiles_button.TabIndex = 3;
            this.fromRedundantToEssentialFiles_button.Text = "Сделать неизбыточным";
            this.fromRedundantToEssentialFiles_button.UseVisualStyleBackColor = true;
            this.fromRedundantToEssentialFiles_button.Click += new System.EventHandler(this.fromRedundantToEssentialFiles_button_Click);
            // 
            // redunduntFiles_groupBox
            // 
            this.redunduntFiles_groupBox.Controls.Add(this.fromRedundantToEssentialFiles_button);
            this.redunduntFiles_groupBox.Controls.Add(this.redunduntFiles_listBox);
            this.redunduntFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.redunduntFiles_groupBox.Location = new System.Drawing.Point(716, 356);
            this.redunduntFiles_groupBox.Margin = new System.Windows.Forms.Padding(0);
            this.redunduntFiles_groupBox.Name = "redunduntFiles_groupBox";
            this.redunduntFiles_groupBox.Size = new System.Drawing.Size(348, 178);
            this.redunduntFiles_groupBox.TabIndex = 0;
            this.redunduntFiles_groupBox.TabStop = false;
            this.redunduntFiles_groupBox.Text = "Избыточные файлы";
            // 
            // redunduntFiles_listBox
            // 
            this.redunduntFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.redunduntFiles_listBox.FormattingEnabled = true;
            this.redunduntFiles_listBox.Location = new System.Drawing.Point(3, 16);
            this.redunduntFiles_listBox.Margin = new System.Windows.Forms.Padding(0);
            this.redunduntFiles_listBox.Name = "redunduntFiles_listBox";
            this.redunduntFiles_listBox.Size = new System.Drawing.Size(342, 159);
            this.redunduntFiles_listBox.Sorted = true;
            this.redunduntFiles_listBox.TabIndex = 0;
            this.redunduntFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.redunduntFiles_listBox_SelectedIndexChanged);
            // 
            // resultWindow_tableLayoutPanel
            // 
            this.resultWindow_tableLayoutPanel.ColumnCount = 3;
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.11654F));
            this.resultWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.61278F));
            this.resultWindow_tableLayoutPanel.Controls.Add(this.Children_GroupBox, 0, 2);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.sourceText_groupBox, 1, 0);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.sourceFiles_groupBox, 0, 0);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.parents_groupBox, 0, 1);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.manualFiles_groupBox, 2, 0);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.essentialFiles_groupBox, 2, 1);
            this.resultWindow_tableLayoutPanel.Controls.Add(this.redunduntFiles_groupBox, 2, 2);
            this.resultWindow_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultWindow_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.resultWindow_tableLayoutPanel.Name = "resultWindow_tableLayoutPanel";
            this.resultWindow_tableLayoutPanel.RowCount = 3;
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.resultWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.resultWindow_tableLayoutPanel.Size = new System.Drawing.Size(1064, 534);
            this.resultWindow_tableLayoutPanel.TabIndex = 1;
            // 
            // Children_GroupBox
            // 
            this.Children_GroupBox.Controls.Add(this.children_listBox);
            this.Children_GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Children_GroupBox.Location = new System.Drawing.Point(0, 356);
            this.Children_GroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.Children_GroupBox.Name = "Children_GroupBox";
            this.Children_GroupBox.Size = new System.Drawing.Size(354, 178);
            this.Children_GroupBox.TabIndex = 1;
            this.Children_GroupBox.TabStop = false;
            this.Children_GroupBox.Text = "Дети выбранного файла";
            // 
            // children_listBox
            // 
            this.children_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.children_listBox.FormattingEnabled = true;
            this.children_listBox.Location = new System.Drawing.Point(3, 16);
            this.children_listBox.Margin = new System.Windows.Forms.Padding(0);
            this.children_listBox.Name = "children_listBox";
            this.children_listBox.Size = new System.Drawing.Size(348, 159);
            this.children_listBox.TabIndex = 0;
            this.children_listBox.SelectedIndexChanged += new System.EventHandler(this.children_listBox_SelectedIndexChanged);
            // 
            // ResultWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resultWindow_tableLayoutPanel);
            this.Name = "ResultWindow";
            this.Size = new System.Drawing.Size(1064, 534);
            this.sourceFiles_groupBox.ResumeLayout(false);
            this.parents_groupBox.ResumeLayout(false);
            this.sourceText_groupBox.ResumeLayout(false);
            this.manualFiles_groupBox.ResumeLayout(false);
            this.essentialFiles_groupBox.ResumeLayout(false);
            this.redunduntFiles_groupBox.ResumeLayout(false);
            this.resultWindow_tableLayoutPanel.ResumeLayout(false);
            this.Children_GroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox sourceFiles_groupBox;
        private System.Windows.Forms.ListBox sourceFiles_listBox;
        private System.Windows.Forms.Button fromSourcesToManualFiles_button;
        //private System.Windows.Forms.ListBox orphanFiles_listBox;
        private System.Windows.Forms.GroupBox parents_groupBox;
        private System.Windows.Forms.ListBox parents_listBox;
        //private System.Windows.Forms.ListBox children_listBox;
        private System.Windows.Forms.GroupBox manualFiles_groupBox;
        private System.Windows.Forms.Button deleteFromManualFiles_button;
        private System.Windows.Forms.ListBox ManualFiles_listBox;
        private System.Windows.Forms.GroupBox sourceText_groupBox;
        private System.Windows.Forms.RichTextBox sourceText_richTextBox;
        private System.Windows.Forms.GroupBox essentialFiles_groupBox;
        private System.Windows.Forms.ListBox essentialFiles_listBox;
        private System.Windows.Forms.GroupBox redunduntFiles_groupBox;
        private System.Windows.Forms.ListBox redunduntFiles_listBox;
        private System.Windows.Forms.Button fromRedundantToEssentialFiles_button;
        private System.Windows.Forms.Button fromEssentialToRedundantFiles_button;
        private System.Windows.Forms.TableLayoutPanel resultWindow_tableLayoutPanel;
        private System.Windows.Forms.GroupBox Children_GroupBox;
        private System.Windows.Forms.ListBox children_listBox;
        //private System.Windows.Forms.Button allParents_button;
    }
}
