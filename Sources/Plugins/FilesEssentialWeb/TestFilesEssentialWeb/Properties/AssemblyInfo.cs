﻿using System.Reflection;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется посредством следующего 
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("TestFilesEssentialWeb")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("TestFilesEssentialWeb")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Задание значения false для атрибута ComVisible приведет к тому, что типы из этой сборки станут невидимыми 
// для COM-компонентов.  Если к одному из типов этой сборки необходимо обращаться из 
// модели COM, задайте для атрибута ComVisible этого типа значение true.
[assembly: ComVisible(false)]

// Если данный проект доступен для модели COM, следующий GUID используется в качестве идентификатора библиотеки типов
[assembly: Guid("c62b07c0-42f7-43de-904f-265f422d26a1")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номера сборки и редакции по умолчанию 
// используя "*", как показано ниже:
// [сборка: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.0.2.4")]
[assembly: AssemblyFileVersion("3.0.2.4")]
