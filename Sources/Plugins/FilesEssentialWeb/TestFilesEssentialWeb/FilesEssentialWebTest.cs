﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestUtils;
using IA.Plugins.Analyses.FilesEssentialWeb;
using System.Linq;
using System.Collections.Generic;

namespace TestFilesEssentialWeb
{
    [TestClass]
    public class FilesEssentialWebTest
    {
        /// <summary>
        /// Summary description for MainTest
        /// </summary>
        [TestClass]
        public class TestFilesEssentialWeb
        {
            /// <summary>
            /// Экземпляр инфраструктуры тестирования для каждого теста
            /// </summary>
            private TestUtilsClass testUtils;

            private TestContext testContextInstance;
            /// <summary>
            /// Информация о текущем тесте
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            /// <summary>
            /// Делаем то, что необходимо сделать перед запуском каждого теста
            /// </summary>
            [TestInitialize]
            public void EachTest_Initialize()
            {
                //Иницииализация инфраструктуры тестирования для каждого теста
                testUtils = new TestUtilsClass(testContextInstance.TestName);
            }

            /// <summary>
            /// Запуск плагина на пустом множестве файлов.
            /// Результат - пустые отчеты.
            /// </summary>
            [TestMethod]
            public void FilesEssentialWeb_NoSources()
            {

                const string sourcePrefixPart = @"Sources\WebNoSources\";

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    new FilesEssentialWeb(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {

                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        if (!Directory.Exists(Path.Combine(testMaterialsPath, sourcePrefixPart)))
                            Directory.CreateDirectory(Path.Combine(testMaterialsPath, sourcePrefixPart));
                        TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);

                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();

                        // Вручную отобранные файлы 
                        writer.Add("");
                        // Допустимые символы
                        writer.Add("A-Z|a-z|А-Я|а-я|0-9|\\|/|.|~|@|#|%|^|-|_|(|)|;|,| |$|:|[|]");
                        // Использовать содержимое файлов данных для поиска зависимостей
                        writer.Add(false);

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.FILE_ESSENTIAL_WEB, writer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) => { return true; },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"FilesEssentialWeb\ReportsNoSources\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }

                        /// <summary>
            /// Запуск плагина на реальном наборе файлов. В именах файлов не может быть букв
            /// Файл вручную отобранных файлов - непустой
            /// Результат - зависимости теряются
            /// </summary>
            [TestMethod]
            public void FilesEssentialWeb_NotAllSymbols()
            {
                const string sourcePrefixPart = @"Sources\FileEssentialWEB\";

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    new FilesEssentialWeb(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        
                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        //TestUtils.TestUtilsClass.Run_IndexingFileContent(storage, true, true, true, true, true);
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);
                        TestUtilsClass.Run_IndexingFileContent(storage);
                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();

                        // Вручную отобранные файлы 
                        writer.Add(System.IO.Path.Combine(testMaterialsPath, @"FilesEssentialWeb\Settings\", "Handy.txt"));
                        // Допустимые символы
                        writer.Add("0-9|\\|/|.|~|@|#|%|^|-|_|(|)|;|,| |$|:|[|]");
                        // Использовать содержимое файлов данных для поиска зависимостей
                        writer.Add(false);

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.FILE_ESSENTIAL_WEB, writer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) => { return true; },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"FilesEssentialWeb\NotAllSymbols\"), reportsPath, isIgnoreLinesOrder: true);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }

            /// <summary>
            /// Запуск плагина на реальном наборе файлов
            /// Файл вручную отобранных файлов - непустой
            /// Результат - отчет с реальными связями
            /// </summary>
            [TestMethod]
            public void FilesEssentialWeb_FromPreviousResults()
            {
                const string sourcePrefixPart = @"Sources\FileEssentialWEB\";

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    new FilesEssentialWeb(),

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        
                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        //TestUtils.TestUtilsClass.Run_IndexingFileContent(storage, true, true, true, true, true);
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);
                        TestUtilsClass.Run_IndexingFileContent(storage);
                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();

                        // Вручную отобранные файлы 
                        writer.Add(System.IO.Path.Combine(testMaterialsPath, @"FilesEssentialWeb\Settings\", "Handy.txt"));
                        // Допустимые символы
                        writer.Add("A-Z|a-z|А-Я|а-я|0-9|\\|/|.|~|@|#|%|^|-|_|(|)|;|,| |$|:|[|]");
                        // Использовать содержимое файлов данных для поиска зависимостей
                        writer.Add(false);

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.FILE_ESSENTIAL_WEB, writer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) => { return true; },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"FilesEssentialWeb\Reports\"), reportsPath, isIgnoreLinesOrder: true);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }

            /// <summary>
            /// Запуск плагина на реальном наборе файлов
            /// путь до файла с настройками некорректный
            /// Результат - отчет с реальными связями
            /// </summary>
            [TestMethod]
            public void FilesEssentialWeb_InvalidSettigs()
            {
                //Формируем список типов отлавливаемых сообщений
                HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>()
                {
                    IA.Monitor.Log.Message.enType.ERROR
                };
                //Формируем список ожидаемых сообщений
                List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>()
                {
                    new IA.Monitor.Log.Message(
                        "Избыточность файлов WEB сайтов",
                        IA.Monitor.Log.Message.enType.ERROR,
                        "Файл с вручную отобранными файлами не найден."
                        )
                };

                using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
                {
                    const string sourcePrefixPart = @"Sources\WebNoSources\";

                    testUtils.RunTest(
                        //sourcePrefixPart
                        sourcePrefixPart,

                        //plugin
                        new FilesEssentialWeb(),

                        //isUseCachedStorage
                        false,

                        //prepareStorage
                        (storage, testMaterialsPath) =>
                        {

                        },

                        //customizeStorage
                        (storage, testMaterialsPath) =>
                        {
                            TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        //TestUtils.TestUtilsClass.Run_IndexingFileContent(storage, true, true, true, true, true);
                        TestUtils.TestUtilsClass.Run_IdentifyFileTypes(storage);
                            TestUtilsClass.Run_IndexingFileContent(storage);
                            Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();

                        // Вручную отобранные файлы 
                        writer.Add(System.IO.Path.Combine(testMaterialsPath, @"FilesEssentialWeb\Settings\", "Hand.txt"));
                        // Допустимые символы
                        writer.Add("A-Z|a-z|А-Я|а-я|0-9|\\|/|.|~|@|#|%|^|-|_|(|)|;|,| |$|:|[|]");
                        // Использовать содержимое файлов данных для поиска зависимостей
                        writer.Add(false);

                            storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.FILE_ESSENTIAL_WEB, writer);

                        },

                        //checkStorage
                        (storage, testMaterialsPath) => { return true; },

                        //checkreports
                        (reportsPath, testMaterialsPath) =>
                        {
                        //Сравниваем директории с отчётами
                        return true;
                        },

                        //isUpdateReport
                        false,

                        //changeSettingsBeforRerun
                        (plugin, testMaterialsPath) => { },

                        //checkStorageAfterRerun
                        (storage, testMaterialsPath) => { return true; },

                        //checkReportAfterRerun
                        (reportsPath, testMaterialsPath) => { return true; }, 
                        // проверять настройки
                        true
                       );
                }
            }

            /// <summary>
            /// Исходные файлы - из реального PHP проекта
            /// Вручную подряд неизбыточными выбраны 50 файлов
            /// Результат - есть реальные зависимости, сравнение с эталонными отчетами
            /// </summary>
            [TestMethod]
            public void FilesEssentialWeb_Reports()
            {

                const string sourcePrefixPart = @"Sources\FileEssentialWEB\";

                FilesEssentialWeb plug = new FilesEssentialWeb();

                testUtils.RunTest(
                    //sourcePrefixPart
                    sourcePrefixPart,

                    //plugin
                    plug,

                    //isUseCachedStorage
                    false,

                    //prepareStorage
                    (storage, testMaterialsPath) =>
                    {
                        
                    },

                    //customizeStorage
                    (storage, testMaterialsPath) =>
                    {
                        TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                        TestUtilsClass.Run_IdentifyFileTypes(storage);
                        TestUtilsClass.Run_IndexingFileContent(storage);
                        Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();

                        // Вручную отобранные файлы 
                        writer.Add("");
                        // Допустимые символы
                        writer.Add("A-Z|a-z|А-Я|а-я|0-9|\\|/|.|~|@|#|%|^|-|_|(|)|;|,| |$|:|[|]");
                        // Использовать содержимое файлов данных для поиска зависимостей
                        writer.Add(false);

                        storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.FILE_ESSENTIAL_WEB, writer);

                    },

                    //checkStorage
                    (storage, testMaterialsPath) => {
                        for (int i = 0; i < 50; ++i)
                        {
                            plug.resultLists.listManual.Add(plug.resultLists.listSources[i]);
                        }
                        plug.resultLists.listManual = plug.resultLists.listManual.Distinct().ToList();
                        FilesEssentialWeb.Essential(plug.resultLists);
                        return true;
                    },

                    //checkreports
                    (reportsPath, testMaterialsPath) =>
                    {
                        //Сравниваем директории с отчётами
                        return TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(testMaterialsPath, @"FilesEssentialWeb\Reports\"), reportsPath);
                    },

                    //isUpdateReport
                    false,

                    //changeSettingsBeforRerun
                    (plugin, testMaterialsPath) => { },

                    //checkStorageAfterRerun
                    (storage, testMaterialsPath) => { return true; },

                    //checkReportAfterRerun
                    (reportsPath, testMaterialsPath) => { return true; }
                   );

            }
        }
    }
}