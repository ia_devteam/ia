﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IA.Plugins.Analyses.SensorsReEnumerator;

using IOController;
using Store;
using Store.Table;
using TestUtils;

namespace TestSensorReEnumerator
{
    [TestClass]
    public class TestSensorReEnumerator
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Возвращает количество перехваченных сообщений
            /// </summary>
            /// <returns>Количество сообщений</returns>
            public int Count()
            {
                return messages.Count;
            }
        }

        /// <summary>
        /// Перехватчик сообщений из монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Общий тест на отработку плагина. Заменяет датчики С и С++.
        /// </summary>
        [TestMethod]
        public void SensorReEnumerator_ReEnumerate()
        {
            SensorsReEnumerator testPlugin = new SensorsReEnumerator();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    //Копируем отчеты (чтобы их не портил плагин)
                    string reportC = GetSampleReportCopyInStorage(storage, Path.Combine(testMaterialsPath, @"SensorReEnumerator\prepareStorage\c\xr_f_ifdef_report\_1_Din_.Rep"), "C");
                    string reportCPP = GetSampleReportCopyInStorage(storage, Path.Combine(testMaterialsPath, @"SensorReEnumerator\prepareStorage\cpp\xr_f_ifdef_report\_1_Din_.Rep"), "CPP");

                    //Копируем исходники (чтобы их не протил плагин)
                    string labsC = GetSampleLabSourcesCopyInStorage(storage, Path.Combine(testMaterialsPath, @"SensorReEnumerator\prepareStorage\c\src_w_sensors"), "C");
                    string labsCPP = GetSampleLabSourcesCopyInStorage(storage, Path.Combine(testMaterialsPath, @"SensorReEnumerator\prepareStorage\cpp\src_w_sensors"), "CPP");

                    IBufferWriter buffer = WriterPool.Get();

                    buffer.Add(1);
                    buffer.Add(2);
                    buffer.Add(reportCPP);
                    buffer.Add(labsCPP);
                    buffer.Add(reportC);
                    buffer.Add(labsC);

                    storage.pluginSettings.SaveSettings(testPlugin.ID, buffer);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, @"str\oldReports"),
                        Path.Combine(testMaterialsPath, @"SensorReEnumerator\checkStorage\reports")), "Исправленные отчёты не совпадают с образцами.");

                    Assert.IsTrue(TestUtilsClass.Reports_Directory_TXT_Compare(Path.Combine(storage.WorkDirectory.Path, @"str\oldLabs"),
                        Path.Combine(testMaterialsPath, @"SensorReEnumerator\checkStorage\labs")), "Исправленные исходные тексты не совпадают с образцами.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) =>
                {
                    string tempDirectoryPath = Path.Combine(testUtils.TestRunDirectoryPath, "Temp");
                    
                    GetSpecifiedStandartReportsCopy(
                        tempDirectoryPath,
                        Path.Combine(testMaterialsPath, @"SensorReEnumerator\checkReport"),
                        @"C:\MG\work\tfs\ia\Tests\RunTemp\SensorReEnumerator_ReEnumerate"
                        );

                    return TestUtilsClass.Reports_Directory_TXT_Compare(reportsFullPath, tempDirectoryPath, true);
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка выдачи сообщения об отсутствии отчетов АИСТа.
        /// </summary>
        [TestMethod]
        public void SensorReEnumerator_NoReports()
        {
            SensorsReEnumerator testPlugin = new SensorsReEnumerator();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IBufferWriter buffer = WriterPool.Get();

                    buffer.Add(1);
                    buffer.Add(0);

                    storage.pluginSettings.SaveSettings(testPlugin.ID, buffer);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) => 
                {
                    Assert.IsTrue(listener.ContainsPart("Не указано ни одного отчёта для обработки."), "Сообщение не совпадает с ожидаемым.");

                    return true; 
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка выдачи сообщения об отсутствии исходных текстов, упомянутых в отчете АИСТа.
        /// </summary>
        [TestMethod]
        public void SensorReEnumerator_NoFiles()
        {
            SensorsReEnumerator testPlugin = new SensorsReEnumerator();

            testUtils.RunTest(
                string.Empty,                                               //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    //Копируем отчеты (чтобы их не портил плагин)
                    string reportC = GetSampleReportCopyInStorage(storage, Path.Combine(testMaterialsPath, @"SensorReEnumerator\NoFiles\report\_1_Din_.Rep"), "C");

                    IBufferWriter buffer = WriterPool.Get();
                    
                    buffer.Add(1);
                    buffer.Add(1);
                    buffer.Add(reportC);
                    buffer.Add(string.Empty);

                    storage.pluginSettings.SaveSettings(testPlugin.ID, buffer);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(listener.ContainsPart("Некоторые файлы исходных текстов для перенумерации не существуют. Укажите верные настройки коррекции путей."), 
                        "Сообщение не совпадает с ожидаемым.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; },       //checkReportAfterRerun
                true
                );
        }

        /// <summary>
        /// Создание копии эталонного отчета в Хранилище по пути "Директория Хранилища\oldReports\Имя директории для копирования\_1_Din_.Rep"
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="sampleReportFilePath">Путь до эталонного файла отчета. Не может быть null.</param>
        /// <param name="copyDirectoryName">Имя директории для копирования. Не может быть null.</param>
        /// <returns>Путь до копии эталонного файла отчета.</returns>
        private string GetSampleReportCopyInStorage(Storage storage, string sampleReportFilePath, string copyDirectoryName)
        {
            string reportsXRFDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "oldReports", copyDirectoryName);
            string sampleReportCopyFilePath = Path.Combine(reportsXRFDirectoryPath, @"_1_Din_.Rep");

            //Используем System.IO поскольку DirectoryController не способен создавать дерево каталогов
            Directory.CreateDirectory(reportsXRFDirectoryPath);

            FileController.Copy(sampleReportFilePath, sampleReportCopyFilePath);

            return sampleReportCopyFilePath;
        }

        /// <summary>
        /// Создание копии эталонного лабораторных исходных текстов в Хранилище по пути "Директория Хранилища\oldLabs\Имя директории для копирования\"
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <param name="sampleLabSourcesDirectoryPath">Путь до директории с эталонными лабораторными исходными текстами. Не может быть null.</param>
        /// <param name="directoryTo">Имя директории для копирования. Не может быть null.</param>
        /// <returns>Путь до директории с копией эталонных лабораторных исходных текстов.</returns>
        private string GetSampleLabSourcesCopyInStorage(Storage storage, string sampleLabSourcesDirectoryPath, string directoryTo)
        {
            string sampleLabSourcesCopyDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "oldLabs", directoryTo);

            //Используем System.IO поскольку DirectoryController не способен создавать дерево каталогов
            Directory.CreateDirectory(sampleLabSourcesCopyDirectoryPath);
            
            DirectoryController.Copy(sampleLabSourcesDirectoryPath, sampleLabSourcesCopyDirectoryPath);

            return sampleLabSourcesCopyDirectoryPath;
        }

        /// <summary>
        /// Получение копии эталонных отчётов с измененёнными путями для тестирования на конкретной машине.
        /// </summary>
        /// <param name="standartReportsDirectoryPath">Путь к директории с эталонными отчетами. Не может быть Null.</param>
        /// <param name="specifiedStandartReportsDirectoryPath">Путь к директории с копией эталонных отчётов с подменёнными путями. Не может быть Null.</param>
        /// <param name="replaceString">Путь в отчетах, который будет изменен. Не может быть Null.</param>
        private void GetSpecifiedStandartReportsCopy(string standartReportsDirectoryPath, string specifiedStandartReportsDirectoryPath, string replaceString)
        {
            foreach (string file in Directory.EnumerateFiles(specifiedStandartReportsDirectoryPath, "*.*", SearchOption.AllDirectories))
            {
                string contents = new StreamReader(file, Encoding.Default).ReadToEnd().Replace(replaceString, testUtils.TestRunDirectoryPath);

                string newFileDirectoryPath = Path.Combine(standartReportsDirectoryPath, Path.GetDirectoryName(file).Substring(specifiedStandartReportsDirectoryPath.Length));
                
                if (!DirectoryController.IsExists(newFileDirectoryPath))
                    DirectoryController.Create(newFileDirectoryPath);

                string newFileFullPath = Path.Combine(newFileDirectoryPath, Path.GetFileName(file));
                using (StreamWriter writer = new StreamWriter(newFileFullPath, false, Encoding.Default))
                    writer.Write(contents);
            }
        }
    }
}
