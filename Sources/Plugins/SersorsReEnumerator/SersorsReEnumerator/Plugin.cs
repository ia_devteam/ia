﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;

using IA.Extensions;
using Store;
using Store.Table;

namespace IA.Plugins.Analyses.SensorsReEnumerator
{
    using IA.AistSubsystem.XR_F_IfDef;
    /// <summary>
    /// Статические настройки.
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Иденитификатор плагина
        /// </summary>
        internal static uint Id = Store.Const.PluginIdentifiers.SENSORS_REENUMERATOR;

        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal static string Name = "Перенумерация расставленных АИСТ-С датчиков в исходных текстах";

        #region Settings
        /// <summary>
        /// Отчеты АИСТа
        /// </summary>
        internal static List<XR_F_IfDefProtocol> reports;

        /// <summary>
        /// Номер стартового датчика
        /// </summary>
        internal static int startNumber;
        #endregion

        /// <summary>
        /// Задачи при работе.
        /// </summary>
        internal enum Tasks
        {
            [Description("Обработка исходных файлов")]
            WORK_WITH_FILES,
            [Description("Обработка файлов протоколов")]
            WORK_WITH_PROTOCOLS
        }
    }

    /// <summary>
    /// Плагин перенумерации расставленных АИСТ-С датчиком. Создан в связи с ограничением в 4000 файлов на один проход расстановки датчиков.
    /// </summary>
    public class SensorsReEnumerator : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Store.Storage storage;

        /// <summary>
        /// Настройки плагина
        /// </summary>
        Settings settings;

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get { return PluginSettings.Id; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get { return PluginSettings.Name; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get 
            { 
                return 
                    Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW |
                    Plugin.Capabilities.RERUN | 
                    Plugin.Capabilities.REPORTS; 
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get { return null; }
        }

        /// <summary>
        /// Окно настроек.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (settings = new Settings());
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return new List<Monitor.Tasks.Task>()
                {
                    PluginSettings.Tasks.WORK_WITH_FILES.ToMonitorTask(Name),
                    PluginSettings.Tasks.WORK_WITH_PROTOCOLS.ToMonitorTask(Name)
                };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            if (PluginSettings.reports == null)
                PluginSettings.reports = new List<XR_F_IfDefProtocol>();
            this.storage = storage;

            PluginSettings.startNumber = Properties.Settings.Default.StartSensorNumber;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            if (PluginSettings.reports == null)
                PluginSettings.reports = new List<XR_F_IfDefProtocol>();
            else
                PluginSettings.reports.Clear();

            PluginSettings.startNumber = settingsBuffer.GetInt32();

            int filesPairCount = settingsBuffer.GetInt32();
            for (int i = 0; i < filesPairCount; i++)
            {
                var report = new XR_F_IfDefProtocol(settingsBuffer.GetString())
                {
                    CorrectedFilesInReportPrefix = settingsBuffer.GetString()
                };
                PluginSettings.reports.Add(report);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //простой режим не предусмотрен
            throw new NotImplementedException(PluginSettings.Name + " не проектировался для работы в режиме проектов.");
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            Properties.Settings.Default.StartSensorNumber = PluginSettings.startNumber;
            Properties.Settings.Default.Save();

            settingsBuffer.Add(PluginSettings.startNumber);
            if (PluginSettings.reports != null)
            {
                settingsBuffer.Add(PluginSettings.reports.Count);
                foreach (var report in PluginSettings.reports)
                {
                    settingsBuffer.Add(report.ReportPath);
                    settingsBuffer.Add(report.CorrectedFilesInReportPrefix);
                }
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = String.Empty;

            if (settings != null)
                settings.Save();

            if (PluginSettings.reports.Any(r => r.FilesInReportWithCorrectedPaths.Any(f => !IOController.FileController.IsExists(f))))
                message = "Некоторые файлы исходных текстов для перенумерации не существуют. Укажите верные настройки коррекции путей.";

            return message == String.Empty;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            if (PluginSettings.reports.Count == 0)
            {
                Monitor.Log.Warning(PluginSettings.Name, "Не указано ни одного отчёта для обработки.");
                return;
            }

            
            List<string> filesToProceed = new List<string>();
            foreach (var report in PluginSettings.reports)
                filesToProceed.AddRange(report.FilesInReportWithCorrectedPaths);

            //настройка внутреннего модуля, выполняющего основную работу
            //установки по-умолчанию. В интерфейсе не предусмотрено возможности менять режим нумерации и логирования, пока нет таких задач, но задел сделан.
            SensorShift.Shifter shifter = new SensorShift.Shifter();
            shifter.Config.Files = filesToProceed;
            shifter.Config.MakeNumerationContinuous = true;
            shifter.Config.Logging = true;
            shifter.Config.SensorStartNumber = PluginSettings.startNumber;

            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WORK_WITH_FILES.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)filesToProceed.Count);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WORK_WITH_PROTOCOLS.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)PluginSettings.reports.Count);
            shifter.DoShiftInFilesFromConfig();

            string reportFilename = storage.pluginData.GetDataFileName(ID);
            System.IO.File.WriteAllLines(reportFilename, shifter.GetReportInList());

            var dummy = new SensorShift.AistReportFixer(PluginSettings.reports, shifter.GetReportRaw());
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            IOController.FileController.Copy(storage.pluginData.GetDataFileName(ID), System.IO.Path.Combine(reportsDirectoryPath, "SensorReEnumeration.txt"));
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }
    }
}
