﻿using System.Collections.Generic;

namespace IA.Plugins.Analyses.SensorsReEnumerator.SensorShift
{
    /// <summary>
    /// Класс реализует хранение параметров, необходимых для выполнения работы по сдвигу нумерации датчиков.
    /// </summary>
    internal class Configuration
    {
        /// <summary>
        /// Регулярное выражение для выделения датчика в тексте программ. 
        /// Если пустое, то поиск датчиков не производится.
        /// По-умолчанию: "____Din_Go\\s\\(\\\"\\d*\\\",\\d+\\);".
        /// </summary>
        public string SensorRegExp { get; set; }
        
        /// <summary>
        /// Регулярное выражение для выделения тектовой части датчика. 
        /// Если пустое, то обработка тектовой части не выполняется.
        /// По-умолчанию:  "(?LT=\")\d*(?LT=\")";.
        /// </summary>
        public string StringPartReplaceRegExp { get; set; }
        
        /// <summary>
        /// Функция по преобразованию тектовой части датчика. 
        /// Если не задана, то значение внутри кавычек обрамляется RNTtexthereTNR.
        /// </summary>
        public System.Text.RegularExpressions.MatchEvaluator StringPartReplaceFunc;
        
        /// <summary>
        /// Регулярное выражение для выделения численной части датчика. 
        /// Если пустое, то обработка численной части датчика не производится.
        /// По-умолчанию: "(?LT=,)\\d+".
        /// </summary>
        public string NumberPartReplaceRegExp { get; set; }
        
        /// <summary>
        /// Функция по преобразованию численной части датчика.
        /// Если флаг <c>MakeNumerationContinuous</c> установлен, то функция игнорируется.
        /// Если не задана, то текущее знаение складывается с <c>NumberPartShift</c>.
        /// </summary>
        public System.Text.RegularExpressions.MatchEvaluator NumberPartReplaceFunc;
        
        /// <summary>
        /// Значение, на которое будут сдвигаться номера датчиков.
        /// Имеет смысл только при сброшеном флаге <c>MakeNumerationContinuous</c>.
        /// По-умолчанию: 0.
        /// </summary>
        public long NumberPartShift { get; set; }
        
        /// <summary>
        /// Список файлов для обработки.
        /// </summary>
        public IEnumerable<string> Files { get; set; }
        
        /// <summary>
        /// Флаг указывает, следует ли приводить нумерацию к единому порядку 
        /// или выполнять простой сдвиг на указанную в <c>NumberPartShift</c> величину.
        /// По-умолчанию: false.
        /// </summary>
        public bool MakeNumerationContinuous { get; set; }
        
        /// <summary>
        /// Номер, с которого следует начинать нумерацию при обработке.
        /// Имеет смысл только при установленом флаге <c>MakeNumerationContinuous</c>.
        /// По-умолчанию: 1.
        /// </summary>
        public long SensorStartNumber { get; set; }
        
        /// <summary>
        /// Флаг указывает, следует ли создавать резервную копию каждого входного файла.
        /// Резервная копия имеет расширение .shifter_backup.
        /// По-умолчанию: false.
        /// </summary>
        public bool BackupFileBeforeWrite { get; set; }

        /// <summary>
        /// Флаг ведения подробного отчёта о произведённых заменах.
        /// По-умолчанию: true.
        /// </summary>
        public bool Logging { get; set; }
        
        /// <summary>
        /// Конструктор по-умолчанию. Инициализирует все поля умолчательными значениями.
        /// </summary>
        public Configuration()
        {
            SensorRegExp = "____Din_Go\\s\\(\\\"\\d*\\\",\\d+\\);";
            StringPartReplaceRegExp = "(?<=\\\")\\d*(?=\\\")";
            NumberPartReplaceRegExp = "(?<=,)\\d+";
            NumberPartShift = 0;
            MakeNumerationContinuous = false;
            SensorStartNumber = 1;
            BackupFileBeforeWrite = false;
            Logging = true;
        }
    }
}
