﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IA.Plugins.Analyses.SensorsReEnumerator.SensorShift
{
    /// <summary>
    /// Класс реализует собирание и выдачу информации о произведённых заменах в разных файлах.
    /// Для экономии памяти собирает только числовые номера датчиков. 
    /// Можно ввести уровень записи, при котором будут запоминаться символьные значения замены, но набольших проектах это сожрёт много памяти.
    /// </summary>
    class Reporter
    {
        Dictionary<string, Dictionary<long, long>> fileWithSensors;
        Dictionary<string, List<int>> fileWithErrors;

        string recentFile;

        /// <summary>
        /// Единственный конструктор.
        /// </summary>
        public Reporter()
        {
            fileWithSensors = new Dictionary<string, Dictionary<long, long>>();
            fileWithErrors = new Dictionary<string, List<int>>();
        }

        /// <summary>
        /// Добавить файл к списку обработанных.
        /// </summary>
        /// <param name="file"></param>
        public void AddFile(string file)
        {
            if (!fileWithSensors.ContainsKey(file))
                fileWithSensors.Add(file, new Dictionary<long, long>());
            if (!fileWithErrors.ContainsKey(file))
                fileWithErrors.Add(file, new List<int>());
            recentFile = file;
        }

        /// <summary>
        /// Добавить пару "старый-новый" к последнему файлу. 
        /// Ничего не происходит если небыло задано ниодного файла или если значение старого датчика уже вставлено.
        /// </summary>
        /// <param name="sensorOld">Номер старого датчика.</param>
        /// <param name="sensorNew">Номер нового датчика.</param>
        public void AddSensorPairToRecentFile(long sensorOld, long sensorNew)
        {
            if (String.IsNullOrWhiteSpace(recentFile) || 
                !fileWithSensors.ContainsKey(recentFile) || 
                fileWithSensors[recentFile].ContainsKey(sensorOld) ||
                fileWithSensors[recentFile].ContainsValue(sensorNew)
                )
                return;
            fileWithSensors[recentFile].Add(sensorOld, sensorNew);
        }

        /// <summary>
        /// Добавить запись об ошибочном месте.
        /// </summary>
        /// <param name="errorNumber">Число, выражающее позицию произошедшей ошибки.</param>
        public void AddErrorToRecentFile(int errorNumber)
        {
            if (String.IsNullOrWhiteSpace(recentFile) || !fileWithSensors.ContainsKey(recentFile))
                return;
            fileWithErrors[recentFile].Add(errorNumber);
        }

        /// <summary>
        /// Получить отчёт в виде массива строк.
        /// </summary>
        /// <returns></returns>
        public List<string> CreateReport()
        {
            var result = new List<string>();
            foreach (var table in fileWithSensors)
            {
                result.Add(table.Key);
                foreach (var pair in table.Value)
                    result.Add(pair.Key + " => " + pair.Value);
            }

            if (fileWithErrors.Any(i => i.Value.Count > 0))
            {
                result.Add("Errors");
                foreach (var table in fileWithErrors)
                {
                    if (table.Value.Count > 0)
                    {
                        result.Add(table.Key);
                        foreach (var num in table.Value)
                            result.Add(num.ToString());
                    }
                }
            }
            else
                result.Add("Errors not happen.");

            return result;
        }

        /// <summary>
        /// Получить отчёт в виде вложенных словарей для ручной обработки.
        /// </summary>
        public Dictionary<string, Dictionary<long, long>> ReportRaw
        {
            get { return fileWithSensors; }
        }
        /// <summary>
        /// Получить отчёт об ошибочных строках для каждого файла для ручной обработки.
        /// </summary>
        public Dictionary<string, List<int>> ReportErrorRaw
        { get { return fileWithErrors; } }

    }
}
