﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using IA.Extensions;

namespace IA.Plugins.Analyses.SensorsReEnumerator.SensorShift
{
    /// <summary>
    /// Класс реализует сдвиг нумерации датчиков на заданное число.
    /// </summary>
    internal class Shifter
    {
        /// <summary>
        /// Конфигурация данного экземпляра.
        /// </summary>
        public Configuration Config { get; private set; }

        long lastSensorNumber = 0;

        /// <summary>
        /// Номер текущего обработываемого датчика.
        /// </summary>
        public long LastSensorNumber { get { return lastSensorNumber; } }

        Regex ReplaceStringPartRegex;
        Regex ReplaceNumberPartRegex;
        Regex ReplaceSensorRegex;

        Reporter reporter;

        /// <summary>
        /// Получить отчёт в виде вложенных словарей для ручной обработки.
        /// </summary>
        /// <returns>null если отчёт не был включён или ни одного выполнения <c>DoShift</c> небыло.</returns>
        public Dictionary<string, Dictionary<long, long>> GetReportRaw()
        {
                if (Config.Logging && reporter != null)
                    return reporter.ReportRaw;
                return null;
        }

        /// <summary>
        /// Получить отчёт об ошибках для ручной обработки.
        /// </summary>
        /// <returns>null если отчёт не был включён или ни одного выполнения <c>DoShift</c> небыло.</returns>
        public Dictionary<string, List<int>> GetReportErrorRaw()
        {
            if (Config.Logging && reporter != null)
                return reporter.ReportErrorRaw;
            return null;
        }

        /// <summary>
        /// Получить отчёт в виде массива строк.
        /// </summary>
        /// <returns>null если отчёт не был включён или ни одного выполнения <c>DoShift</c> небыло.</returns>
        public List<string> GetReportInList()
        {
            if (Config.Logging && reporter != null)
                return reporter.CreateReport();
            return null;
        }

        /// <summary>
        /// Единственный конструктор.
        /// </summary>
        /// <param name="config">Настройки, с которыми работает. Если не задано, берутся настройки по-умолчанию.</param>
        public Shifter(Configuration config = null)
        {
            this.Config = config ?? new Configuration();
        }

        /// <summary>
        /// Инициализация полей. В частности, регулярных выражений.
        /// </summary>
        void Init()
        {
            if (Config.MakeNumerationContinuous)
                lastSensorNumber = Config.SensorStartNumber;
            if (Config.Logging)
                reporter = new Reporter();

            //перекомпилируем регулярку только если есть что компилировать, она не была задана или не равна предыдущей скомпилированной.
            if (!String.IsNullOrWhiteSpace(Config.StringPartReplaceRegExp) &&
                (ReplaceStringPartRegex == null ||
                ReplaceStringPartRegex.ToString() != Config.StringPartReplaceRegExp))
                ReplaceStringPartRegex = new Regex(Config.StringPartReplaceRegExp, RegexOptions.Compiled);


            if (!String.IsNullOrWhiteSpace(Config.NumberPartReplaceRegExp) &&
                (ReplaceNumberPartRegex == null ||
                ReplaceNumberPartRegex.ToString() != Config.NumberPartReplaceRegExp))
                ReplaceNumberPartRegex = new Regex(Config.NumberPartReplaceRegExp, RegexOptions.Compiled);

            if (!String.IsNullOrWhiteSpace(Config.SensorRegExp) &&
                (ReplaceSensorRegex == null ||
                ReplaceSensorRegex.ToString() != Config.SensorRegExp))
                ReplaceSensorRegex = new Regex(Config.SensorRegExp, RegexOptions.Compiled);
        }

        /// <summary>
        /// Выполнить работу по сдвигу номеров. Параметры работы задаются в <c>Config</c>. Результаты перезаписывают оригинальные файлы. Сначала обрабатывается числовая часть, затем строковая.
        /// </summary>
        public void DoShiftInFilesFromConfig()
        {
            Init();

            ulong count = 0;
            if (ReplaceSensorRegex != null && Config.Files != null)
                foreach (var file in Config.Files)
                {
                    Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.WORK_WITH_FILES.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++count);
                    if (!IOController.FileController.IsExists(file))//ещё не хватало от всякой дури падать
                        continue;

                    string fileContains = System.IO.File.ReadAllText(file);

                    if (Config.BackupFileBeforeWrite)
                        IOController.FileController.Copy(file, file + ".shifter_backup");

                    if (Config.Logging)
                        reporter.AddFile(file);

                    fileContains = ReplaceSensorRegex.Replace(fileContains, ReplaceSensorFunc);

                    File.WriteAllText(file, fileContains);
                }
            else
            {
                var message = "Выполнение плагина." + Environment.NewLine;
                if (ReplaceSensorRegex == null)
                    message += "Не задано регулярное выражение поиска датчика." + Environment.NewLine;
                if (Config.Files == null)
                    message += "Не заданы исходные файлы для обработки.";
                Monitor.Log.Error(PluginSettings.Name, message);
            }
        }

        /// <summary>
        /// Выполнить работу по сдвигу номеров датчиков в отдельно взятой строке. Параметры задаются в <c>Config</c>. Сначала обрабатывается числовая часть, затем строковая.
        /// </summary>
        /// <param name="lineToProceed">Строка для обработки.</param>
        /// <returns>Изменённая строка.</returns>
        public string DoShiftInString(string lineToProceed)
        {
            Init();

            if (ReplaceSensorRegex != null)
                return ReplaceSensorRegex.Replace(lineToProceed, ReplaceSensorFunc);
            return String.Empty;
        }

        
         // (?<=,)\d+ выкусывает номер после запятой
         // (?<=\")\d+(?=\") выкусывает номер в кавычках
         
        /// <summary>
        /// Функция обработки одного найденного датчика. Сначала обрабатывается числовая часть, затем строковая.
        /// </summary>
        /// <param name="m">Найденный датчик в виде резульатта поиска регулярного выражения.</param>
        /// <returns></returns>
        string ReplaceSensorFunc(Match m)
        {
            var currentSensor = m.Value;

            if (ReplaceNumberPartRegex != null)
            {
                currentSensor = ReplaceNumberPartRegex.Replace(currentSensor, ReplaceNumberInSensorFunc);
            }

            if (ReplaceStringPartRegex != null)
            {
                currentSensor = ReplaceStringPartRegex.Replace(currentSensor, ReplaceStringInSensorFunc);
            }

            if (Config.MakeNumerationContinuous)
                lastSensorNumber++;

            return currentSensor;
        }

        /// <summary>
        /// Функция обработки текстовой части датчика.
        /// </summary>
        /// <param name="m">Найденная текстовая часть датчика.</param>
        /// <returns></returns>
        string ReplaceStringInSensorFunc(Match m)
        {
            if (Config.StringPartReplaceFunc != null)
                return Config.StringPartReplaceFunc(m);

            return "RNT" + lastSensorNumber.ToString() + "TNR";
        }

        /// <summary>
        /// Функция обработки численной части датчика.
        /// </summary>
        /// <param name="m">Найденная численная часть датчика.</param>
        /// <returns></returns>
        string ReplaceNumberInSensorFunc(Match m)
        {
            if (Config.Logging)
            #region Логгирование включено
            { //если нужно записать произведённые замены - жить становится сложнее
                string founded = m.ToString();
                long oldSensor;
                if (long.TryParse(founded, out oldSensor))
                { //если получилось прочитать в найденом число
                    if (Config.MakeNumerationContinuous)
                    { //если строим новую нумерацию
                        reporter.AddSensorPairToRecentFile(oldSensor, lastSensorNumber);
                        return lastSensorNumber.ToString();
                    }
                    else
                    { //если изменяем старую нумерацию
                        if (Config.NumberPartReplaceFunc != null)
                        { //если задана ручная обработка числовой части
                            string result = Config.NumberPartReplaceFunc(m);
                            if (long.TryParse(result, out lastSensorNumber))
                                reporter.AddSensorPairToRecentFile(oldSensor, lastSensorNumber);
                            else
                                reporter.AddSensorPairToRecentFile(oldSensor, -1);
                            return result;
                        }
                        else
                        { //если ручная обработка числовой части не задана изменяем старую нумерацию по-умолчанию
                            lastSensorNumber = oldSensor + Config.NumberPartShift;
                            reporter.AddSensorPairToRecentFile(oldSensor, lastSensorNumber);
                            return lastSensorNumber.ToString();
                        }
                    }
                }
                else
                { //если прочитать найденное как число не получилось
                    if (Config.MakeNumerationContinuous)
                    {
                        reporter.AddErrorToRecentFile(m.Index);
                        return lastSensorNumber.ToString();
                    }
                    else
                    {
                        reporter.AddSensorPairToRecentFile(-1, -1);
                        return founded;
                    }
                }
            } 
            #endregion
            else
            { //если подробный отчёт не нужен, то алгоритм существенно проще.
                if (!Config.MakeNumerationContinuous)
                {
                    if (Config.NumberPartReplaceFunc != null)
                        return Config.NumberPartReplaceFunc(m);

                    if (long.TryParse(m.ToString(), out lastSensorNumber))
                        return (lastSensorNumber + Config.NumberPartShift).ToString();

                    return m.ToString();
                }
                else
                {
                    return lastSensorNumber.ToString();
                }
            }
        }
    }
}
