﻿namespace IA.Plugins.Analyses.SensorsReEnumerator
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.initialNumber_label = new System.Windows.Forms.Label();
            this.startSensor_numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.report_select_with_path_correction = new IA.AistSubsystem.XR_F_IfDef.Report_select_with_path_correction();
            ((System.ComponentModel.ISupportInitialize)(this.startSensor_numericUpDown)).BeginInit();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // initialNumber_label
            // 
            this.initialNumber_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.initialNumber_label.AutoSize = true;
            this.initialNumber_label.Location = new System.Drawing.Point(3, 373);
            this.initialNumber_label.Name = "initialNumber_label";
            this.initialNumber_label.Size = new System.Drawing.Size(144, 13);
            this.initialNumber_label.TabIndex = 8;
            this.initialNumber_label.Text = "Начальный номер датчика";
            // 
            // startSensor_numericUpDown
            // 
            this.startSensor_numericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.startSensor_numericUpDown.Location = new System.Drawing.Point(153, 370);
            this.startSensor_numericUpDown.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.startSensor_numericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.startSensor_numericUpDown.Name = "startSensor_numericUpDown";
            this.startSensor_numericUpDown.Size = new System.Drawing.Size(264, 20);
            this.startSensor_numericUpDown.TabIndex = 7;
            this.startSensor_numericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 2;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.startSensor_numericUpDown, 1, 1);
            this.settings_tableLayoutPanel.Controls.Add(this.initialNumber_label, 0, 1);
            this.settings_tableLayoutPanel.Controls.Add(this.report_select_with_path_correction, 0, 0);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 2;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(420, 395);
            this.settings_tableLayoutPanel.TabIndex = 6;
            // 
            // report_select_with_path_correction
            // 
            this.settings_tableLayoutPanel.SetColumnSpan(this.report_select_with_path_correction, 2);
            this.report_select_with_path_correction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.report_select_with_path_correction.Location = new System.Drawing.Point(3, 3);
            this.report_select_with_path_correction.MinimumSize = new System.Drawing.Size(405, 210);
            this.report_select_with_path_correction.Name = "report_select_with_path_correction";
            this.report_select_with_path_correction.Size = new System.Drawing.Size(414, 359);
            this.report_select_with_path_correction.TabIndex = 9;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.MaximumSize = new System.Drawing.Size(0, 395);
            this.MinimumSize = new System.Drawing.Size(420, 395);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(420, 395);
            ((System.ComponentModel.ISupportInitialize)(this.startSensor_numericUpDown)).EndInit();
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.settings_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label initialNumber_label;
        private System.Windows.Forms.NumericUpDown startSensor_numericUpDown;
        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private AistSubsystem.XR_F_IfDef.Report_select_with_path_correction report_select_with_path_correction;


    }
}
