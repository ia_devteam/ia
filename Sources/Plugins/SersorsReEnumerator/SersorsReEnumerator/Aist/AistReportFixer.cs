﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace IA.Plugins.Analyses.SensorsReEnumerator.SensorShift
{
    using AistSubsystem.XR_F_IfDef;
    
    /// <summary>
    /// Класс обеспечивает исправление номеров вставленных датчиков в отчётах XR_F_IfDef
    /// </summary>
    internal class AistReportFixer
    {
        IEnumerable<XR_F_IfDefProtocol> reportsXR_F_IfDef;
        Dictionary<string, Dictionary<long, long>> sensorShiftReport;
        Shifter shifter;

        string currentFileInReport;

        /// <summary>
        /// Единственный конструктор.
        /// </summary>
        /// <param name="reports">Перечень отчётов, которые надо исправить.</param>
        /// <param name="sensorShiftReport">Словарь файлов исходных текстов содержащихся датчиков формата "старый-новый".</param>
        public AistReportFixer(IEnumerable<XR_F_IfDefProtocol> reports, Dictionary<string, Dictionary<long, long>> sensorShiftReport)
        {
            reportsXR_F_IfDef = reports;
            this.sensorShiftReport = sensorShiftReport;
            Init();
            DoWork();
        }

        void Init()
        {
            shifter = new Shifter();
            shifter.Config.Logging = false;
            shifter.Config.MakeNumerationContinuous = false;
            shifter.Config.StringPartReplaceFunc = FirstPartFunc;
            shifter.Config.NumberPartReplaceFunc = SecondPartFunc;
        }

        private void DoWork()
        {
            foreach (var reportFile in reportsXR_F_IfDef)
            {
                try
                {
                    var lines = File.ReadAllLines(reportFile.ReportPath, Encoding.Default);

                    currentFileInReport = String.Empty;
                    for (int i = 0; i < lines.Length; i++)
                    {
                        if (String.IsNullOrWhiteSpace(lines[i]) || lines[i][0] == '=') //мусор вида ============== Уточнить ветки 109019...109018 ================
                            continue;
                        if (lines[i][0] == ' ')
                        {//строка с описанием датчика
                            lines[i] = shifter.DoShiftInString(lines[i]);
                            //получение нового номера
                            int pos_end = lines[i].IndexOf(");/*");
                            int pos_start = lines[i].LastIndexOf(',', pos_end) + 1;
                            int num = int.Parse(lines[i].Substring(pos_start, pos_end - pos_start));
                            //замена номера датчика в 3м столбце
                            pos_start = 8;
                            pos_end = pos_start;
                            while (lines[i][pos_end] == ' ') pos_end++;
                            while (Char.IsDigit(lines[i][pos_end])) pos_end++;
                            string tmp = lines[i].Remove(pos_start, pos_end - pos_start);

                            lines[i] = tmp.Insert(pos_start, num.ToString());

                        }
                        else
                        {//строка с названием файла
                            currentFileInReport = lines[i].Replace(reportFile.OriginalFilesInReportPrefix, reportFile.CorrectedFilesInReportPrefix);
                        }
                    }


                    File.WriteAllLines(reportFile.ReportPath, lines, Encoding.Default);
                }
                catch (Exception e)
                {
                    Monitor.Log.Error(PluginSettings.Name, "Ошибка чтения файла отчёта о вставке датчиков XR_F_IfDef. Неверный формат файла. Ошибка: " + e.Message);
                }
            }
        }

        long currentNumber;
        /// <summary>
        /// Функция замены текстовой части датчика АИСТ-С на RNT_предопределённыйдатчик_TNR. 
        /// Предопределённый датчик берётся из отчёта расстановки, передаваемого в конструкторе.
        /// </summary>
        string FirstPartFunc(System.Text.RegularExpressions.Match m)
        {
            return "RNT" + sensorShiftReport[currentFileInReport][currentNumber] + "TNR";
        }

        /// <summary>
        /// Функция замены числовой части датчика АИСТ-С на предопределённый из отчёта расстановки, передаваемого в конструкторе.
        /// </summary>
        string SecondPartFunc(System.Text.RegularExpressions.Match m)
        {
            currentNumber = long.Parse(m.ToString());
            return sensorShiftReport[currentFileInReport][currentNumber].ToString();
        }
    }
}
