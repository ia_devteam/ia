﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace IA.Plugins.Analyses.SensorsReEnumerator
{
    using AistSubsystem.XR_F_IfDef;
    /// <summary>
    /// Реализация класса настроек плагина
    /// </summary>
    public partial class Settings : UserControl, IA.Plugin.Settings
    {
        List<XR_F_IfDefProtocol> reports;

        /// <summary>
        /// Констркутор
        /// </summary>
        public Settings()
        {
            InitializeComponent();

            reports = PluginSettings.reports ?? new List<XR_F_IfDefProtocol>();
            foreach (var report in reports)
                report_select_with_path_correction.protocolList.Add(report);

            startSensor_numericUpDown.Value = PluginSettings.startNumber;
        }

        /// <summary>
        /// Сохранение настроек.
        /// </summary>
        public void Save()
        {
            PluginSettings.reports = report_select_with_path_correction.protocolList.ToList();
            PluginSettings.startNumber = Convert.ToInt32(startSensor_numericUpDown.Value);
        }
    }
}
