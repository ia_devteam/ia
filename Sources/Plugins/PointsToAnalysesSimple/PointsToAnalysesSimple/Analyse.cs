﻿using System;
using System.Collections.Generic;
using Store;

namespace IA.Plugins.Analyses.PointsToAnalysesSimple
{
    internal class Analyse
    {
        private Storage storage;

        internal Analyse(Storage storage)
        {
            this.storage = storage;
        }

        internal void Run()
        {
            // Строим очередь из всех переменных
            Queue<Variable> vars = new Queue<Variable>();
            foreach (Variable var in storage.variables.EnumerateVariables())
                vars.Enqueue(var);

            // До достижения верхней грани протаскиваем возможность указывания на функции
            Variable variable;
            try{
                while(true)
                {
                    variable = vars.Dequeue();
                    
                    foreach(IAssignment ass in variable.Enumerate_AssignmentsFrom())
                    {
                        foreach(PointsToFunction ptf in variable.Enumerate_PointsToFunctions())
                        {
                            if (!isPointsToFunction(ass.To, ptf.function))
                            {
                                PointsToFunction new_ptf = ass.To.AddPointsToFunctions();
                                new_ptf.function = ptf.function;
                                new_ptf.AddPosition(ass.Position);
                                new_ptf.isGetAddress = false;
                                new_ptf.Save();

                                AddToQueue(ref vars, ass.To);
                            }
                        }
                    }
                }
            }
            catch(InvalidOperationException){}


            // Для каждого вызова по указателю добавляем прямой вызов
            foreach (Variable var in storage.variables.EnumerateVariables())
            {
                foreach (Location loc in var.ValueCallPosition())
                {
                    foreach(PointsToFunction ptf in var.Enumerate_PointsToFunctions())
                    {
                        if(storage.functions.GetFunction(loc.GetFunctionId()).EntryStatement == null)
                            storage.functions.GetFunction(loc.GetFunctionId()).AddCall(storage.functions.GetFunction(ptf.function));
                    }
                }
            }
        }

        private bool isPointsToFunction(Variable variable, ulong function)
        {
            foreach (PointsToFunction ptf in variable.Enumerate_PointsToFunctions())
                if (ptf.function == function)
                    return true;

            return false;
        }

        private void AddToQueue(ref Queue<Variable> vars, Variable variable)
        {
            if (!vars.Contains(variable))
                vars.Enqueue(variable);
        }

    }
}
