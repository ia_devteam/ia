﻿using System.IO;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;
using Store;

namespace TestPointsToAnalysesSimple
{
    [TestClass]
    public class TestPointsToAnalysesSimple
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }
        
        /// <summary>
        /// Проверка корректности отработки плагина после запуска парсера JavaScript.
        /// </summary>
        [TestMethod]
        public void PointsToAnalysesSimple_JavaScriptParser()
        {
            string sourcePrefixPart = @"Sources\JS\UnexpectedInsertion";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.PointsToAnalysesSimple.PointsToAnalysesSimple(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_JavaScriptParser(storage);
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string storageGraphPath = Path.Combine(storage.WorkDirectory.Path, "graph", "graph.xml");
                    string graphModelPath = Path.Combine(testMaterialsPath, "PointsToAnalysesSimple\\JavaScriptParser", "graph.xml");
                    WriteGraph(storageGraphPath, storage);
                    return TestUtilsClass.Reports_XML_Compare(storageGraphPath, graphModelPath);
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина после запуска парсера C#.
        /// </summary>
        [TestMethod]
        public void PointsToAnalysesSimple_CSharpParser()
        {
            string sourcePrefixPart = @"Sources\CSharp\FindStringsInFiles";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.PointsToAnalysesSimple.PointsToAnalysesSimple(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testMaterialsPath, sourcePrefixPart));
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string storageGraphPath = Path.Combine(storage.WorkDirectory.Path, "graph", "graph.xml");
                    string graphModelPath = Path.Combine(testMaterialsPath, "PointsToAnalysesSimple\\CSharpParser", "graph.xml");
                    WriteGraph(storageGraphPath, storage);
                    return TestUtilsClass.Reports_XML_Compare(storageGraphPath, graphModelPath);
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Проверка корректности отработки плагина при ручном заполнении Хранилища информации о присваиваниях.
        /// </summary>
        [TestMethod]
        public void PointsToAnalysesSimple_RandomPointers()
        {
            testUtils.RunTest(
                string.Empty,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.PointsToAnalysesSimple.PointsToAnalysesSimple(),        // Plugin
                false,                                                          // isUseCachedStorage
                (storage, testMaterialsPath) => { },                            // prepareStorage
                (storage, testMaterialsPath) =>
                {

                    string testFilePath = Path.Combine(storage.appliedSettings.FileListPath, "test.cs");

                    Variable variableA = storage.variables.AddVariable();
                    variableA.Name = "A";
                    Variable variableB = storage.variables.AddVariable();
                    variableB.Name = "B";
                    Variable variableC = storage.variables.AddVariable();
                    variableC.Name = "C";
                    Variable variableD = storage.variables.AddVariable();
                    variableD.Name = "D";
                    Variable variableE = storage.variables.AddVariable();
                    variableE.Name = "E";


                    IFunction function1 = storage.functions.AddFunction();
                    function1.Name = "F1";
                    IFunction function2 = storage.functions.AddFunction();
                    function2.Name = "F2";
                    IFunction function3 = storage.functions.AddFunction();
                    function3.Name = "F3";
                    IFunction function4 = storage.functions.AddFunction();
                    function4.Name = "F4";
                    IFunction function5 = storage.functions.AddFunction();
                    function5.Name = "F5";


                    PointsToFunction ptf1 = variableC.AddPointsToFunctions();
                    ptf1.function = function1.Id;
                    ptf1.Save();

                    PointsToFunction ptf2 = variableA.AddPointsToFunctions();
                    ptf2.function = function2.Id;
                    ptf2.Save();

                    PointsToFunction ptf3 = variableD.AddPointsToFunctions();
                    ptf3.function = function3.Id;
                    ptf3.Save();

                    PointsToFunction ptf4 = variableB.AddPointsToFunctions();
                    ptf4.function = function4.Id;
                    ptf4.Save();

                    PointsToFunction ptf5 = variableE.AddPointsToFunctions();
                    ptf5.function = function5.Id;
                    ptf5.Save();


                    IAssignment assC = variableC.AddAssignmentsFrom();
                    assC.To = variableA;
                    assC.AddPosition(testFilePath, 1, 1, function1);
                    assC.Save();

                    IAssignment assA = variableA.AddAssignmentsFrom();
                    assA.To = variableB;
                    assA.AddPosition(testFilePath, 11, 1, function2);
                    assA.Save();

                    IAssignment assD = variableD.AddAssignmentsFrom();
                    assD.To = variableC;
                    assD.AddPosition(testFilePath, 21, 1, function3);
                    assD.Save();

                    IAssignment assE = variableE.AddAssignmentsFrom();
                    assE.To = variableD;
                    assE.AddPosition(testFilePath, 31, 1, function4);
                    assE.Save();                                        
                },                                                              // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string storageGraphPath = Path.Combine(storage.WorkDirectory.Path, "graph", "graph.xml");
                    string graphModelPath = Path.Combine(testMaterialsPath, "PointsToAnalysesSimple\\RandomPointers", "graph.xml");
                    WriteGraph(storageGraphPath, storage);
                    return TestUtilsClass.Reports_XML_Compare(storageGraphPath, graphModelPath);
                },                                                              // checkStorage
                (reportsPath, testMaterialsPath) => { return true; },                                                              // checkReportBeforeRerun
                false,                                                          // isUpdateReport
                (plugin, testMaterialsPath) => { },                             // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },               // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }            // checkReportAfterRerun
            );
        }

        /// <summary>
        /// Запись графа в xml-файл
        /// </summary>
        /// <param name="storageGraphPath">Путь до xml-файла</param>
        /// <param name="storage">Хранилище</param>
        private void WriteGraph(string storageGraphPath, Storage storage)
        {
            if (string.IsNullOrWhiteSpace(storageGraphPath) || storage == null)
                return;

            if (!Directory.Exists(Path.GetDirectoryName(storageGraphPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(storageGraphPath));

            using (XmlWriter xmlWriter = XmlWriter.Create(storageGraphPath))
            {
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("Graph");

                foreach (Variable var in storage.variables.EnumerateVariables())
                {
                    xmlWriter.WriteStartElement("Variable");
                    xmlWriter.WriteAttributeString("name", var.FullName);

                    foreach (PointsToFunction ptf in var.Enumerate_PointsToFunctions())
                    {
                        xmlWriter.WriteStartElement("Function");
                        xmlWriter.WriteAttributeString("name", storage.functions.GetFunction(ptf.function).FullNameForReports);
                        xmlWriter.WriteEndElement();
                    }

                    xmlWriter.WriteEndElement();
                }

                xmlWriter.WriteEndElement();
            }
        }
    }
}
