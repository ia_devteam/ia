﻿using System;
using System.Collections.Generic;

namespace IA.Plugins.Importers.AistImporter
{
    using AistSubsystem;
    /// <summary>
    /// Вспомогательный класс для инкапсуляции импорта данных в Хранилище.
    /// </summary>
    internal class Importer
    {
        internal Store.Storage storage;
        /// <summary>
        /// Конструктор с заданием Храниилща, над которым работать.
        /// </summary>
        /// <param name="storage"></param>
        public Importer(Store.Storage storage)
        {
            this.storage = storage;
        }

        Dictionary<int, Store.IFunction> storFuncs;

        /// <summary>
        /// Выполнить импорт функций из данной ветви отчёта АИСТ-С.
        /// </summary>
        /// <param name="bunch">Ветвь отчёта АИСТ-С.</param>
        public void ImportFunctions(AistBunch bunch)
        {
            if (bunch.Functions == null)
                return;

            storFuncs = new Dictionary<int, Store.IFunction>();
            foreach (var aistFunc in bunch.Functions)
            {
                if (!storFuncs.ContainsKey(aistFunc.NFun))
                {
                    var tmp = ImportFunction(aistFunc);
                    if (tmp != null)
                        storFuncs.Add(aistFunc.NFun, tmp);
                    else
                        Monitor.Log.Warning(PluginSettings.Name, String.Format("Функция не добавлена. Имя: {0}, Строка: {1}.", aistFunc.Name, aistFunc.AistLineNumber));
                }
                else
                    Monitor.Log.Warning(PluginSettings.Name, String.Format("Функция с номером <{3}> уже добавлена. Имя: {0}, Файл: {1}, Строка: {2}.", aistFunc.Name, aistFunc.SourceFile, aistFunc.AistLineNumber, aistFunc.NFun));
            }

            foreach (var pair in storFuncs)
            {
                foreach (var aistFuncNum in bunch.Functions.Find(f => f.NFun == pair.Key).AistCalls)
                {
                    if (storFuncs.ContainsKey(aistFuncNum))
                        pair.Value.AddCall(storFuncs[aistFuncNum]);
                }
            }

            foreach (var aistFunc in bunch.Functions)
            {
                if (!storFuncs.ContainsKey(aistFunc.NFun))
                    continue;

                var currentStorFunc = storFuncs[aistFunc.NFun];
                foreach (var calledByNum in aistFunc.AistCalledBy)
                    if (storFuncs.ContainsKey(calledByNum))
                        storFuncs[calledByNum].AddCall(currentStorFunc); //надо проверить, что значит: "Если у функции есть тело - будет сгенерирована ошибка"
            }
        }

        /// <summary>
        /// Преобразовать функцию формата АИСТ-С в формат Хранилища.
        /// </summary>
        /// <param name="function">Функция из отчёта АИСТ-С.</param>
        /// <returns></returns>
        private Store.IFunction ImportFunction(AistReportEntity function)
        {
            var file = storage.files.Find(function.SourceFile);
            if (file == null)
            {
                Monitor.Log.Warning(PluginSettings.Name, String.Format("Файл не найден: <{0}>", function.SourceFile));
                return null;
            }
            var storfunc = storage.functions.AddFunction();
            storfunc.AddDefinition(file, Convert.ToUInt64(function.AistLineNumber < 1 ? 1 : function.AistLineNumber), 1);//столбец в отчётах АИСТ-С не присутствует
            storfunc.Name = function.Name;
            storfunc.EssentialMarkedType = Store.enFunctionEssentialMarkedTypes.AIST_IMPORTED;
            switch (function.AistEssentialFlag)
            {
                case AistReportEntity.Essential.ESSENTIAL:
                    storfunc.Essential = Store.enFunctionEssentials.ESSENTIAL;
                    break;
                case AistReportEntity.Essential.REDUNANT:
                    storfunc.Essential = Store.enFunctionEssentials.REDUNDANT;
                    break;
                default:
                    storfunc.Essential = Store.enFunctionEssentials.UNKNOWN;
                    break;
            }
            return storfunc;
        }

        /// <summary>
        /// Выполнить импорт переменной формата АИСТ-С в Хранилище.
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        public bool ImportVariable(AistReportEntity variable)
        {
            var file = storage.files.Find(variable.SourceFile);
            if (file == null)
            {
                Monitor.Log.Warning(PluginSettings.Name, String.Format("Файл не найден: <{0}>", variable.SourceFile));
                return false;
            }

            var storvar = storage.variables.AddVariable();
            storvar.AddDefinition(file, Convert.ToUInt64(variable.AistLineNumber < 1 ? 1 : variable.AistLineNumber), 1, null);

            storvar.Name = variable.Name;
            return true;
        }
    }
}
