﻿using System.IO;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;
using IA.Plugins.Importers.AistImporter;

using Store;
using Store.Table;
using IOController;

namespace TestAistImporter
{
    [TestClass]
    public class TestAistImporter
    {
        /// <summary>
        /// Класс перехватчика сообщений из монитора
        /// </summary>
        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            /// <summary>
            /// Список перехваченных сообщений
            /// </summary>
            List<string> messages;

            /// <summary>
            /// Конструктор
            /// </summary>
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }

            /// <summary>
            /// Добавить сообщение в список
            /// </summary>
            /// <param name="message"></param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            /// <summary>
            /// Очистить список
            /// </summary>
            public void Clear()
            {
                messages.Clear();
            }

            /// <summary>
            /// Содержит ли список перехваченных сообщений ожидаемое сообщение
            /// </summary>
            /// <param name="partialString">Ожидаемое сообщение</param>
            /// <returns>true - сообщение было перехвачено</returns>
            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;

                return messages.Any(s => s.Contains(partialString));
            }

            /// <summary>
            /// Список сообщений
            /// </summary>
            public List<string> Messages
            {
                get
                {
                    return messages;
                }
            }
        }

        /// <summary>
        /// Перехватчик сообщений монитора
        /// </summary>
        private static BasicMonitorListener listener;

        /// <summary>
        /// Путь до каталога с эталонным дампами Хранилища
        /// </summary>
        private string dumpSampleDirectoryPath;

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        ulong pluginID = Store.Const.PluginIdentifiers.AIST_IMPORTER;

        /// <summary>
        /// Константа подпути в материалах до каталога с эталонными дампами Хранилища
        /// </summary>
        private const string DUMP_SAMPLES_SUBDIRECTORY = @"AistImporter\Dump";

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Информация о текущем тесте
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }
        
        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();

            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
            dumpSampleDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, DUMP_SAMPLES_SUBDIRECTORY);
        }

        /// <summary>
        /// Проверка корректности отработки импортера на простом примере C++.
        /// Сравнивается получишийся дамп с Хранилища с эталонным.
        /// </summary>
        [TestMethod]
        public void AistImporter_FullWork()
        {
            string sourcePostfixPart = @"CS_Sensors\SimpleFuncCalls";

            AistImporter testPlugin = new AistImporter();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) =>  { },                       //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);

                    IBufferWriter writer = WriterPool.Get();

                    writer.Add(Path.Combine(testMaterialsPath, @"AistImporter\AistReports", testUtils.TestName));
                    writer.Add(Path.Combine(@"d:\IATFS\Tests\Materials", sourcePostfixPart));

                    storage.pluginSettings.SaveSettings(pluginID, writer);

                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    string dumpDirectoryPath = Dump(storage);

                    Assert.IsTrue(listener.Messages.Count == 0, listener.Messages.FirstOrDefault());

                    CheckDump(dumpDirectoryPath, Path.Combine(dumpSampleDirectoryPath, testUtils.TestName));
                    
                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );        
        }

        /// <summary>
        /// Проверка выдачи сообщений об ошибках.
        /// </summary>
        [TestMethod]
        public void AistImporter_Errors()
        {
            string sourcePostfixPart = string.Empty;

            AistImporter testPlugin = new AistImporter();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                        //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    IBufferWriter writer = WriterPool.Get();

                    writer.Add(Path.Combine(testMaterialsPath, @"AistImporter\AistReports\AistImporter_FullWork"));
                    writer.Add(Path.Combine(@"d:\IATFS\Tests\Materials", sourcePostfixPart));

                    storage.pluginSettings.SaveSettings(pluginID, writer);
                },                                                          //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    Assert.IsTrue(listener.Messages.Count == 18, "Количество сообщений об ошибках не совпадает с ожидаемым.");

                    return true;
                },                                                          //checkStorage
                (reportsFullPath, testMaterialsPath) => { return true; },   //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );
        }

        /// <summary>
        /// Дамп Хранилища
        /// </summary>
        /// <param name="storage">Хранилище. Не может быть null.</param>
        /// <returns>Путь до каталога, куда был произведен дамп Хранилища</returns>
        private string Dump(Storage storage)
        {
            string dumpDirectoryPath = Path.Combine(storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.STORAGE), "Dump");

            if (!DirectoryController.IsExists(dumpDirectoryPath))
                DirectoryController.Create(dumpDirectoryPath);

            storage.ClassesDump(dumpDirectoryPath);
            storage.FilesDump(dumpDirectoryPath);
            storage.FunctionsDump(dumpDirectoryPath);
            storage.VariablesDump(dumpDirectoryPath);

            return dumpDirectoryPath;
        }

        /// <summary>
        /// Проверка дампа Хранилища
        /// </summary>
        /// <param name="dumpDirectoryPath">Путь до каталога дампа в Хранилище. Не может быть пустым.</param>
        /// <param name="dumpSamplesDirectoryPath">Путь до каталога с эталонным дампом. Не может быть пустым.</param>
        private void CheckDump(string dumpDirectoryPath, string dumpSamplesDirectoryPath)
        {
            bool isEqual = TestUtilsClass.Reports_Directory_TXT_Compare(dumpDirectoryPath, dumpSamplesDirectoryPath);

            Assert.IsTrue(isEqual, "Дамп Хранилища не совпадает с эталонными.");
        }

    }
}
