﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using TestUtils;
using System.Collections.Generic;

namespace TestManualFunctionRedundancy
{
    /// <summary>
    /// Summary description for TestManualFunctionRedundancy
    /// </summary>
    [TestClass]
    public class TestManualFunctionRedundancy
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Есть какие-то функции в хранилище
        /// пользователь нажал на кнопку "Неизбыточна"
        /// Плагин работает как обычно. 
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_ManualAccept()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\"),
                                                           "@#$%^&*");
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    bool res;
                    IA.Plugins.Analyses.ManualFunctionRedundancy.IADatabase.Read(false);
                    IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.Accept(IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.GetBestFunction(out res), Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);

                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\ManualAccept");
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        {
                            return false;
                        }
                    }
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }


        /// <summary>
        /// Есть какие-то функции в хранилище
        /// пользователь нажал на кнопку "Избыточна"
        /// Плагин работает как обычно. 
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_ManualReject()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\"),
                                                           "@#$%^&*");
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    bool res;
                    IA.Plugins.Analyses.ManualFunctionRedundancy.IADatabase.Read(false);
                    IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.Reject(IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.GetBestFunction(out res), Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT);

                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\ManualReject");
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        {
                            return false;
                        }
                    }
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// Есть какие-то функции в хранилище - больше двух
        /// пользователь нажал на кнопку "Пропустить", потом "Неизбыточна"
        /// Плагин работает как обычно. 
        /// После нажатия на кнопку переход к следующей функции
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_ManualPostponeAccept()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\"),
                                                           "@#$%^&*");
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    bool res;
                    IA.Plugins.Analyses.ManualFunctionRedundancy.IADatabase.Read(false);
                    IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.Postpone(IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.GetBestFunction(out res));
                    IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.Accept(IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.GetBestFunction(out res), Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);

                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\ManualPostponeAccept");
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        {
                            return false;
                        }
                    }
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// Есть какие-то функции в хранилище - больше двух
        /// пользователь нажал на кнопку "Пропустить", потом "Избыточна"
        /// Плагин работает как обычно. 
        /// После нажатия на кнопку переход к следующей функции
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_ManualPostponeReject()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\"),
                                                           "@#$%^&*");
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    bool res;
                    IA.Plugins.Analyses.ManualFunctionRedundancy.IADatabase.Read(false);
                    IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.Postpone(IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.GetBestFunction(out res));
                    IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.Reject(IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.GetBestFunction(out res), Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT);

                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\ManualPostponeReject");
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        {
                            return false;
                        }
                    }
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест GetBestFunction
        /// Проверяется, что GetBestFunction вернула функцию с наибольшим деревом вызовов 
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_GetBestFunction()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\"),
                                                           "@#$%^&*");
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    bool res;
                    IA.Plugins.Analyses.ManualFunctionRedundancy.IADatabase.Read(false);
                    Assert.IsTrue(IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.GetBestFunction(out res).Name == "Store.File_Impl.FileExistence.set");
                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест, использующий результаты пользователя от прошлого запуска. 
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_UserResults()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //выбор настроек
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false); // анализировать ли результаты трасс
                    writer.Add(Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\UserResults\UserResults")); //загрузить отчет по переменным
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.MANUAL_FUNCTION_REDUNDANCY, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\"),
                                                           "@#$%^&*");
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\UserResults\Res");
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        {
                            return false;
                        }
                    }
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест, использующий результаты анализа трасс. 
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_WithTraces()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //выбор настроек
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(true); // анализировать ли результаты трасс
                    writer.Add(""); //загрузить отчет по переменным
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.MANUAL_FUNCTION_REDUNDANCY, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\With traces\file\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, System.IO.Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\With traces\Understand\"),
                                                           @"f:\IATFS\Tests\Materials\ManualFunctionsRedundancy\With traces\file\");
                    TestUtilsClass.Run_HandMadeSensors(storage, Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\With traces\sens\sens.log"));
                    TestUtilsClass.Run_DynamicAnalysisResultsProcessing(storage, Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\With traces\trace\"));
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\With traces\Res");
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        {
                            return false;
                        }
                    }
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест, использующий результаты пользователя от прошлого запуска. В протоколе пользователя левые функции 
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_WrongUserResults()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //выбор настроек
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(false); // анализировать ли результаты трасс
                    writer.Add(Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\UserResults\BadUserResults")); //загрузить отчет по переменным
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.MANUAL_FUNCTION_REDUNDANCY, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, System.IO.Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\"),
                                                           "@#$%^&*");
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\UserResults\BadRes");
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        {
                            return false;
                        }
                    }
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест c настройкой для DFM-файлов, но DFM-файлыотсутствуют
        /// отличается от ManualAccept только включенной настройкой DFM
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_DFM_but_DFM_files_missing()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //выбор настроек
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(true); // анализировать ли DFM
                    writer.Add(false); // анализировать ли результаты трасс
                    writer.Add(""); //загрузить отчет по переменным
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.MANUAL_FUNCTION_REDUNDANCY, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Understand\"),
                                                          @"@#$%^&*");
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    bool res;
                    IA.Plugins.Analyses.ManualFunctionRedundancy.IADatabase.Read(false);
                    IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.Accept(IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.GetBestFunction(out res), Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);

                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\ManualAccept");
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        {
                            return false;
                        }
                    }
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }


        /// <summary>
        /// тест анализа DFM-файлов 
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_DFM()
        {
            testUtils.RunTest(
                // sourcePrefixPart
                string.Empty,

                 // plugin
                new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                // isUseCachedStorage
                false,

                // prepareStorage
                (storage, testMaterialsPath) => { },

                // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    //выбор настроек
                    Store.Table.IBufferWriter writer = Store.Table.WriterPool.Get();
                    writer.Add(true); // анализировать ли DFM
                    writer.Add(false); // анализировать ли результаты трасс
                    writer.Add(""); //загрузить отчет по переменным
                    storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.MANUAL_FUNCTION_REDUNDANCY, writer);

                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\DFM\Source"));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                    TestUtilsClass.Run_IndexingFileContent(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\DFM\Understand"),
                                                          @"f:\IATFS\Tests\Materials\ManualFunctionsRedundancy\DFM\Source\");
                },

                // checkStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },

                // checkReportBeforeRerun
                (reportsPath, testMaterialsPath) =>
                {
                    string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\DFM\Res");
                    foreach (string file in Directory.EnumerateFiles(path))
                    {
                        if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                        {
                            return false;
                        }
                    }
                    return true;
                },

                // isUpdateReport
                false,

                // changeSettingsBeforeRerun
                (plugin, testMaterialsPath) => { },

                // checkStorageAfterRerun
                (storage, testMaterialsPath) => { return true; },

                // checkReportAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }
                );
        }

        /// <summary>
        /// тест в отсутствие функций
        /// Пользователь нажал "Неизбыточно"
        /// </summary>
        [TestMethod]
        public void ManualFunctionRedundancy_NoFunctionsStorage()
        {
            HashSet<IA.Monitor.Log.Message.enType> messageTypes = new HashSet<IA.Monitor.Log.Message.enType>();
            messageTypes.Add(IA.Monitor.Log.Message.enType.WARNING);
            List<IA.Monitor.Log.Message> messages = new List<IA.Monitor.Log.Message>();
            IA.Monitor.Log.Message mes = new IA.Monitor.Log.Message("Избыточность по функциям", IA.Monitor.Log.Message.enType.WARNING, "Анализ завершен");
            messages.Add(mes);

            using (TestUtilsClass.LogListener listener = new TestUtilsClass.LogListener(messageTypes, messages))
            {
                testUtils.RunTest(
                    // sourcePrefixPart
                            string.Empty,

                             // plugin
                            new IA.Plugins.Analyses.ManualFunctionRedundancy.ManualFunctionRedundancy(),

                            // isUseCachedStorage
                            false,

                            // prepareStorage
                            (storage, testMaterialsPath) => { },

                            // customizeStorage
                            (storage, testMaterialsPath) =>
                            {
                                TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, @"UnderstandImporter\FunctionReferences\Source\"));
                                TestUtilsClass.Run_IdentifyFileTypes(storage);
                                TestUtilsClass.Run_IndexingFileContent(storage);
                            },

                            // checkStorage
                            (storage, testMaterialsPath) =>
                            {
                                bool res;
                                IA.Plugins.Analyses.ManualFunctionRedundancy.IADatabase.Read(false);
                                IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.Accept(IA.Plugins.Analyses.ManualFunctionRedundancy.Analyser.GetBestFunction(out res), Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);

                                return true;
                            },

                            // checkReportBeforeRerun
                            (reportsPath, testMaterialsPath) =>
                            {
                                string path = Path.Combine(testMaterialsPath, @"ManualFunctionsRedundancy\NoFunctions");
                                foreach (string file in Directory.EnumerateFiles(path))
                                {
                                    if (!TestUtilsClass.Reports_TXT_Compare(file, file.Replace(path, reportsPath)))
                                    {
                                        return false;
                                    }
                                }
                                return true;
                            },

                            // isUpdateReport
                            false,

                            // changeSettingsBeforeRerun
                            (plugin, testMaterialsPath) => { },

                            // checkStorageAfterRerun
                            (storage, testMaterialsPath) => { return true; },

                            // checkReportAfterRerun
                            (reportsPath, testMaterialsPath) => { return true; }
                            );
            }
        }
    }
}
