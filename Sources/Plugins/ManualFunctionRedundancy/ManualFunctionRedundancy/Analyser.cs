﻿using System.Collections.Generic;

namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    /// <summary>
    /// Основной класс, выполняющий действия по анализу.
    /// </summary>
    public static class Analyser
    {
        static public List<WorkSequenceElement> WorkSequence = new List<WorkSequenceElement>();
        
        /// <summary>
        /// Перечень загруженных функций.
        /// </summary>
        static public List<Function> Functions = new List<Function>();

        /// <summary>
        /// Перечень функций, ниотукуда не вызываемых
        /// </summary>
        static public List<Function> neverCalledFunctions = new List<Function>();

        /// <summary>
        /// Перечень загруженных файлов.
        /// </summary>
        static public List<File> Files = new List<File>();

        /// <summary>
        /// Перечень пропущенных пользователем функций
        /// </summary>
        static public List<Function> postponeFunctions = new List<Function>();

        /// <summary>
        /// тип анализа: по функциям или по файлам
        /// </summary>
        public enum enMode
        {
            FUNCTIONS,
            FILES
        }

        /// <summary>
        /// Поиск лучшей функции по количеству зависящих от нее функций
        /// </summary>
        /// <returns></returns>
        public static Function GetBestFunction(out bool isPostponeFlushed)
        {
            Function function;
            List<Function> toDelete = new List<Function>();

            isPostponeFlushed = false;

            //проверка наличия непросмотренных функций, ниоткуда не вызываемых
            if (neverCalledFunctions.Count == 0)
            {
                //есть ли пропущенные функции?
                if (postponeFunctions.Count != 0)
                {
                    //если да, то добавить пропущенные в непросмотренные...
                    neverCalledFunctions.AddRange(postponeFunctions);
                    //... и очистить список пропущенных функций
                    postponeFunctions.Clear();
                    isPostponeFlushed = true;
                }
                else
                {
                    //если в обоих списках пусто - надо посмотреть, не появились ли ниоткуда не вызываемые функции в основном списке
                    foreach (Function func in Functions)
                    {
                        if (func.CalledByDirectly.Count == 0)
                        {
                            neverCalledFunctions.Add(func);
                            toDelete.Add(func);
                        }
                    }
                    foreach (Function func in toDelete)
                    {
                        Functions.Remove(func);
                    }
                    toDelete.Clear();
                    if (neverCalledFunctions.Count == 0)
                    {
                        //теперь осталось проверить наличие "циклов" в списке вызываемых функций
                        if (Functions.Count != 0)
                        {
                            return Functions[0];
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }

            //найти функцию с наибольшим количеством зависимых функций
            function = neverCalledFunctions[0];
            int count = function.CallsDirectly.Count;
            for (int i = 1; i < neverCalledFunctions.Count; ++i)
            {
                if (count < neverCalledFunctions[i].CallsDirectly.Count)
                {
                    function = neverCalledFunctions[i];
                    count = neverCalledFunctions[i].CallsDirectly.Count;
                }
            }

            return function;
        }


        /// <summary>
        /// Заполняет перечень функций, прямые вызовы которых отсутствуют.
        /// </summary>
        internal static void FillNoCallsFunctions()
        {
            //отделение тех, которые ниоткуда вызваны, и добавление их в отдельный список
            foreach (Function func in Functions)
                if (func.CalledByDirectly.Count == 0 && func.EssentialMarkedType != Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT && 
                    func.EssentialMarkedType != Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT && !neverCalledFunctions.Contains(func))
                    neverCalledFunctions.Add(func);

            //удаление невызываемых функций из списка
            foreach (Function func in neverCalledFunctions)
            {
                Functions.Remove(func);
            }
        }


        /// <summary>
        /// отметить данную функцию как точку входа
        /// </summary>
        /// <param name="function"></param>
        public static void Accept(Function function, Store.enFunctionEssentialMarkedTypes reportSource)
        {
            if (function == null)
            {
                Monitor.Log.Warning("Избыточность по функциям", "Анализ завершен");
                return;
            }
            bool found = false;
            foreach (WorkSequenceElement elem in WorkSequence)
            {
                if (elem.function == function)
                {
                    found = true;
                }
            }
            if (!found)
            {
                WorkSequence.Add(new WorkSequenceElement(function));
            }
            //помечаем функцию как точку входа
            function.ProgramEntry();

            //помечаем как неизбыточные все функции, вызываемые из данной
            acceptCalled(function, reportSource);
        }

        /// <summary>
        /// пометить все вызываемые из переданной в качестве параметра функции как неизбыточные
        /// </summary>
        /// возвращает "избыточность функции" - false - если неизбыточна
        /// <param name="function"></param>
        internal static void acceptCalled(Function function, Store.enFunctionEssentialMarkedTypes reportSource)
        {
            //удалим данную функцию из списка непонятных
            neverCalledFunctions.Remove(function);
            Functions.Remove(function);
            postponeFunctions.Remove(function);

            if (function.Essentiality != Store.enFunctionEssentials.ESSENTIAL && reportSource == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER && WorkSequence != null && WorkSequence.Count != 0)
            {
                WorkSequence[WorkSequence.Count - 1].called.Add(function);
            }

            //помечаем данную функцию как неизбыточную
            function.Essential(reportSource);

            if (reportSource == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT)
                reportSource = Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER;
            if (reportSource == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_DFM)
                reportSource = Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_AFTER_DFM;
            if (reportSource == Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED)
                reportSource = Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_AUTO;
            if (reportSource == Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES)
                reportSource = Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_AUTO;
            if (reportSource == Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES)
                reportSource = Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_AUTO;

            //убираем из остальных функций ссылки на данную - чтоб каждый раз не вызывать обработчик
            foreach (Function func in function.CalledByDirectly)
            {
                if (function.EssentialMarkedType != Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT)
                {
                    func.CallsDirectly.Remove(function);
                }
            }
            
            //помечаем как неизбыточные все функции, вызываемые из данной
            while (function.CallsDirectly.Count != 0)
            {
                if (function.CallsDirectly[0].EssentialMarkedType != Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT && function.CallsDirectly[0].EssentialMarkedType != Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER)
                    acceptCalled(function.CallsDirectly[0], reportSource);
                else
                {
                    function.CallsDirectly.RemoveAt(0);
                }
            }
        }

        /// <summary>
        /// Обработка нажатия кнопки избыточности функции
        /// </summary>
        /// <param name="function"></param>
        public static void Reject(Function function, Store.enFunctionEssentialMarkedTypes reportSource)
        {
            if (function == null)
            {
                Monitor.Log.Warning("Избыточность по функциям", "Анализ завершен");
                return;
            } 
            bool found = false;
            foreach (WorkSequenceElement elem in WorkSequence)
            {
                if (elem.function == function)
                {
                    found = true;
                }
            }
            if (!found)
            {
                WorkSequence.Add(new WorkSequenceElement(function));
            }
            
            //Протоколируем действие пользователя
            function.Superfluous(reportSource);

            //Удаляем все упоминания о функции из наших структур
            foreach (Function func in function.CallsDirectly)
            {
                func.CalledByDirectly.Remove(function);
            }
            neverCalledFunctions.Remove(function);
            Functions.Remove(function);
        }

        /// <summary>
        /// отложить решение по данной функции
        /// </summary>
        /// <param name="function"></param>
        public static void Postpone(Function function)
        {
            if (function == null)
            {
                Monitor.Log.Warning("Избыточность по функциям", "Анализ завершен");
                return;
            } 
            neverCalledFunctions.Remove(function);
            postponeFunctions.Add(function);
        }

        static bool isAnalyseFinishedRunned = false;
        /// <summary>
        /// грубо завершить анализ (?)
        /// </summary>
        internal static void AnalyseFinished()
        {
            if (isAnalyseFinishedRunned)
                return;

            //Все оставшиеся нераспознанными файлы избыточны
            foreach (File file in Files)
                if (!(file.getKind()== Store.enFileEssential.ESSENTIAL))
                    file.Redundant();

            isAnalyseFinishedRunned = true;
        }

        /// <summary>
        /// снять все пометки с файлов
        /// </summary>
        private static void ReleaseFileMarks()
        {
            foreach (File file in Files)
                file.ReleaseMark();
        }

        /// <summary>
        /// начать работу заново
        /// </summary>
        internal static void Clear()
        {
            Functions = new List<Function>();
            Files = new List<File>();
            postponeFunctions = new List<Function>();
            neverCalledFunctions = new List<Function>();
        }
    }
}
