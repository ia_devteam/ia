﻿using System;
using System.Collections.Generic;
using Store;

namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    /// <summary>
    /// Обнаруживает в каждом DFM-файле указания на обработчики событий. Затем помечает неизбыточными все функции,
    /// у которых имя класса и имя функции совпадают с выделенными.
    /// </summary>
    static class AnalyseDFM
    {
        static Stack<string> Classes;
        static List<Function> Functions;

        static public void Run(Files files, List<Function> Functions)
        {
            foreach (IFile file in files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                if (file.fileType == SpecialFileTypes.BORLAND_DFM)
                    ParseFile(file, Functions);
            }
        }

        static private void ParseFile(IFile file, List<Function> Functions)
        {
            //if (!file.FullFileName_Original.EndsWith(@"info.dfm"))
            //    return;

            Classes = new Stack<string>();

            //Делаем выборку из функций, определенных в том же каталоге, в котором находится данный файл
            AnalyseDFM.Functions = new List<Function>();
            string folder = System.IO.Path.GetDirectoryName(file.FullFileName_Canonized);
            folder = Files.CanonizeFileName(folder);
            foreach (Function function in Functions)
            {
                if (function.DefFile != null)
                {
                    string path = function.DefFile.FullFileName;
                    path = System.IO.Path.GetDirectoryName(path);
                    path = Files.CanonizeFileName(path);
                    if (path == folder)
                        AnalyseDFM.Functions.Add(function);
                }
            }
            
            //Разбираем сам файл
            Antlr.Runtime.ANTLRFileStream stream = new Antlr.Runtime.ANTLRFileStream(file.FullFileName_Original);
            AnalyseDFMLexer lexer = new AnalyseDFMLexer(stream);
            AnalyseDFMParser parser = new AnalyseDFMParser(new Antlr.Runtime.CommonTokenStream(lexer));

            workFile = file.FullFileName_Original;
            AnalyseDFMParser.file_return ret;
            try
            {
                ret = parser.file();

                if(parser.NumberOfSyntaxErrors > 0)
                    throw new Exception("Не удалось разобрать файл " + file.FullFileName_Original);
            }
            catch(Exception ex)
            {
                throw new Exception("Не удалось разобрать файл " + file.FullFileName_Original + ".", ex);
            }
            workFile = null;

            if (Classes.Count != 0)
                IA.Monitor.Log.Error(PluginSettings.Name, "Структура файла " + file.FullFileName_Original + " не корректна");
        }


        static string workFile;
        
        internal static void PushID(Antlr.Runtime.IToken id2)
        {
            if (id2 == null)
                Classes.Push(null);
            else
            {
                Classes.Push(id2.Text);
            }
        }

        internal static void Pop()
        {
            if (Classes.Count == 0)
                throw new Exception("Встретилась неизвестная конструкция в файле" + workFile);
            
            Classes.Pop();
        }

        internal static void Reference(string id2)
        {
            foreach (Function function in Functions)
                foreach (string id1 in Classes)
                {
                    if (id1 == null)
                        continue;

                    if (id2.Contains("."))
                        IA.Monitor.Log.Warning(PluginSettings.Name, "Идентификатор с точкой пропущено: " + id2);

                    if (function.Name == id1 + "::" + id2 || function.Name.EndsWith(id1 + "." + id2))
                        Analyser.Accept(function, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_DFM);
                }
        }
    }
}
