grammar AnalyseDFM;

options 
{language = 'CSharp2'; 
output=AST; 
} 

@parser::namespace { IA.Plugins.Analyses.ManualFunctionRedundancy } // Or just @namespace { ... }

@lexer::namespace { IA.Plugins.Analyses.ManualFunctionRedundancy }

@parser::members
{
override public void EmitErrorMessage(string err)
{ throw new Exception(err);}
}

file 	:	(line NEWLINE {if(this.NumberOfSyntaxErrors > 0) throw new Exception(NEWLINE2.Line.ToString());})* EOF {if(this.NumberOfSyntaxErrors > 0) throw new Exception("file finished");}
	;
	
line	:	WS* ('object' | 'inherited' | 'inline') WS* ID WS* class_ref	
		|	WS* big_id WS* '=' WS* reference WS*						
		|	WS* 'end' '>'? WS*										{AnalyseDFM.Pop();}
		|	WS* 'item' WS*										{AnalyseDFM.PushID(null);}
		|	WS* big_id WS* '=' WS* sequence
		;

class_ref	:	':' 
				WS* 
				id2=ID 
				WS* 
				('[' (WS | ID | ',')* ']' WS*)?	
									{AnalyseDFM.PushID(id2);}
			|						{AnalyseDFM.PushID(null);}
			;	
		
reference
		: big_id	{AnalyseDFM.Reference($big_id.res);}
		| boool
		;		
sequence 
	:	'-'? DIGIT (DIGIT | '.')* WS*
	|	'[' (WS | ID | ',')* ']' WS*
	|	(NEWLINE WS*)? 
		( seq_sharp | seq_string) 
		(	seq_sharp 
		| 	seq_string 
		| 	WS '+' NEWLINE WS* (seq_sharp | seq_string)
		)*
	|	'{' (DIGIT | '#' | seq_string | ID | WS | NEWLINE)* '}' WS*
	|	'(' 
			(ID | DIGIT | '#' | '+' | seq_string | boool | WS | NEWLINE)* 
		')' WS*
	|	'<' '>'?
	;	

boool
	:	'True'
	|	'False'
	;


seq_sharp
	:	'#' (DIGIT)*
	;

seq_string
	:	'\'' (~'\'')* '\''
	;	
big_id returns [string res]	

	:	{$res = "";}
		id1 = ID		{$res += id1.Text;}
		(
			'.'			{$res += '.';}
		|	id2 = ID	{$res += id2.Text;}
		)*
	;	

ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;


WS  :   ( ' '
        | '\t'
        | '\r'
        ) 
    ;


DIGIT : ('0'..'9') 'd'?;

OTHERCHAR : ~('a'..'z'|'A'..'Z'|'0'..'9'|'_'|' '|'\t'|'\r'|'=');


NEWLINE
    : '\r'? '\n'
    ;
