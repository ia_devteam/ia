using System.Windows.Forms;

namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    /// <summary>
    /// �������� ����� �������� �������
    /// </summary>
    internal partial class Settings : UserControl, IA.Plugin.Settings
    {
        /// <summary>
        /// �����������
        /// </summary>
        internal Settings()
        {            
            InitializeComponent();

            traceCallesEssential_checkBox.Checked = PluginSettings.TCE;
            userProtocolPath_textBox.Text = PluginSettings.UserProtocolPath;
            (userProtocolPath_textBox.Dialog as FileDialog).Title = "�������� ���� � ���������� ������ ������������";
        }

        /// <summary>
        /// ���������� ��������
        /// </summary>
        public void Save()
        {
            PluginSettings.TCE = traceCallesEssential_checkBox.Checked;
            PluginSettings.UserProtocolPath = userProtocolPath_textBox.Text;
        }
    }
}
