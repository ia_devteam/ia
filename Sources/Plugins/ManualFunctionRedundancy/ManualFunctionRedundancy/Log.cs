using System;
using System.Collections.Generic;
using Store;
using System.IO;
using HTMLWriter;

namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    class Log
    {
        const string FullProtocolFunctionsName = "FullProtocolFunctions";
        const string FunctionResultsName = "FunctionResults.txt";
        const string UserResultsName = "UserResults.txt";

        /// <summary>
        /// ��������� �������
        /// </summary>
        /// <param name="ProtocolsPath"></param>
        /// <param name="storage"></param>
        internal static void GenerateReports(string ProtocolsPath, Storage storage)
        {
            GenerateWayData();

            ReportFunctionsFull(Path.Combine(ProtocolsPath, FullProtocolFunctionsName), storage);
            ReportFunctionsSimple(Path.Combine(ProtocolsPath, FunctionResultsName), storage);
            ReportUser(Path.Combine(ProtocolsPath, UserResultsName), storage);

            FreeWayData();
        }


        class Comparer : IComparer<Function>
        {
            #region IComparer<Function> Members

            public int Compare(Function x, Function y)
            {
                return x.Name.CompareTo(y.Name);
            }

            #endregion
        }


        /// <summary>
        /// ������� �������� �������� ��������
        /// </summary>
        /// <param name="FunctionResults"></param>
        private static void ReportUser(string FunctionResults, Storage storage)
        {
            System.IO.StreamWriter wr;

            wr = new System.IO.StreamWriter(FunctionResults);
            wr.WriteLine("����� � ���������: ");
            foreach (Function function in ExtractFunctions(() => IADatabase.EnumerateAcceptedByUser()))
            {
                wr.Write(" ");
                wr.Write("������� " + function.Name);
                File file = function.DefFile;
                if (file != null)
                    wr.Write(" ���������� � ����� " + file.FileNameForReports + " ������ " + function.defPosition.line.ToString());
                wr.WriteLine();
            }

            wr.WriteLine("���������� �������: ");
            foreach (Function function in ExtractFunctions(() => IADatabase.EnumerateRejectedByUser()))
            {
                wr.Write(" ");
                wr.Write("������� " + function.Name);
                File file = function.DefFile;
                if (file != null)
                    wr.Write(" ���������� � ����� " + file.FileNameForReports + " ������ " + function.defPosition.line.ToString());
                wr.WriteLine();
            }
            
            wr.Close();
        }

        delegate IEnumerable<Function> ListGenerator();

        private static List<Function> ExtractFunctions(ListGenerator generator)
        {
            List<Function> list = new List<Function>();
            foreach (Function function in generator())
                list.Add(function);

            list.Sort(new Comparer());
            return list;
        }

        /// <summary>
        /// ���������� ����������� �������� �� ��������
        /// </summary>
        /// <param name="FunctionResults"></param>
        private static void ReportFunctionsSimple(string FunctionResults, Storage storage)
        {
            System.IO.StreamWriter wr;

            wr = new System.IO.StreamWriter(FunctionResults);

            wr.WriteLine("������������ �������: ");
            foreach (Function function in ExtractFunctions(() => IADatabase.EnumerateEssentialFunctions()))
            {
                wr.Write(" ");
                wr.Write("������� " + function.Name);
                File file = function.DefFile;
                if(file != null)
                    wr.Write(" ���������� � ����� " + file.FileNameForReports + ", " + function.defPosition.line.ToString());
                wr.WriteLine();
            }

            wr.WriteLine();
            wr.WriteLine();
            wr.WriteLine();

            wr.WriteLine("���������� �������: ");
            foreach (Function function in ExtractFunctions(() => IADatabase.EnumerateRedundantFunctions()))
            {
                wr.Write(" ");
                wr.Write("������� " + function.Name);
                File file = function.DefFile;
                if(file != null)
                    wr.Write(" ���������� � ����� " + file.FileNameForReports + ", " + function.defPosition.line.ToString());
                wr.WriteLine();
            }

            wr.WriteLine();
            wr.WriteLine();
            wr.WriteLine();

            wr.WriteLine("����������� �������: ");
            foreach (Function function in ExtractFunctions(() => IADatabase.EnumerateUnknownFunctions()))
            {
                wr.Write(" ");
                wr.Write("������� " + function.Name);
                File file = function.DefFile;
                if (file != null)
                    wr.Write(" ���������� � ����� " + file.FileNameForReports + ", " + function.defPosition.line.ToString());
                wr.WriteLine();
            }

            wr.Close();
        }

        /// <summary>
        /// ������� ������ �������� �� ������
        /// </summary>
        /// <param name="FullProtocolFile"></param>
        private static void ReportFilesFull(string FullProtocolFile)
        {
            System.IO.StreamWriter wr;

            //������� �������� �� ������
            wr = new System.IO.StreamWriter(FullProtocolFile);
            foreach (File file in Analyser.Files)
            {
                switch (file.getKind())
                {
                    case enFileEssential.ESSENTIAL:
                        wr.Write("������������ ");
                        break;
                    case enFileEssential.SUPERFLUOUS:
                        wr.Write("���������� ");
                        break;
                    case enFileEssential.UNKNOWN:
                        wr.Write("�������������� ");
                        break;
                    default:
                        wr.Write("������. ��� ����� �� ��������");
                        break;
                }
                wr.WriteLine("���� " + file.FileNameForReports);

                if (file.getKind() == enFileEssential.ESSENTIAL)
                {
                    wr.Write("�������� ������������ �������: ");
                    foreach (Function func in file.containsFunctions)
                        if (func.Essentiality == Store.enFunctionEssentials.ESSENTIAL)
                        {
                            wr.Write(func.Name);
                            wr.Write(", ");
                        }

                    wr.WriteLine();
                }

                wr.WriteLine();
            }
            wr.Close();
        }

        private static void ReportFunctionsFull(string FullProtocolFunctions, Storage storage)
        {
            HTMLDoc doc = new HTMLDoc(FullProtocolFunctions, "�������� ������������ ������������ �� ��������");

            doc.font.size = 7;
            doc.font.style.Bold = true;
            doc.AddText("�������, �������� � �������� ������");

            doc.ResetFont();
            doc.font.size = 5;
            doc.AddText("���� ��������� ������ �������, ���������� ���������� �� ���� ��������, �������� � �������� ������. � ������ ����������� ��� ������������ ������� - ������ ������� �������� ����� �������, ������ - ��� ������� ����� �������� ����� �������� ������� � ����������� �� ��������������. ������ ������� �������� ������ ���������� �������, ������ - ������ ��������������� �������, ��������� - ������ ����������� ��������� �������. ������ ������, ������� � ��������� ������ ��������� � �������� ������.");

            doc.font.size = 6;
            doc.font.style.Bold = true;
            doc.AddText("������������ �������, �������� � �������� ������");
            doc.ResetFont();

            doc.font.size = 5;
            List<string> Caps = new List<string>();
            Caps.Add("��� �������");
            Caps.Add("�������� � �������");
            doc.AddTable(Caps);
            foreach (Function function in Analyser.Functions)
            {
                if (function.Essentiality == Store.enFunctionEssentials.ESSENTIAL)
                {
                    doc.AddRow();
                    doc.AddCell(function.Name.Replace(".", ". ").Replace(":", ": "), 1, 2);

                    if (function.DefFile != null && function.DefFile.FullFileName != "")
                        doc.AddCell(function.DefFile.FileNameForReports + ", " + function.defPosition.line.ToString(), 1, 1);
                    else
                        doc.AddCell("����� �������� ������� ����������", 1, 1);

                    doc.AddRow();

                    switch (function.EssentialMarkedType)
                    {
                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER:

                            List<Function> way = GetWayDataFunction(function, wayData => { return wayData.user; });

                            doc.AddCell("����� ������ � ����� � ���, ��� ������ ������� ���������� �� ������������ ������� (�������������� ������� ����������� ��������� �������), ��� �������������� ��������� �����: " + PrintWay(way), 1, 1);
                            //������� ���� ������ ���� �������
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_AFTER_DFM:

                            way = GetWayDataFunction(function, wayData => { return wayData.dfm; });
                            doc.AddCell("����� ������ � ����� � ���, ��� ������ ������� ���������� �� ������������ ������� (�������������� ������� ����������� �� ������ ������� dfm-������), ��� �������������� ��������� �����: " + PrintWay(way), 1, 1);
                            //������� ���� ������ ���� �������
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_DFM:

                            doc.AddCell("����� ������ � ����� � ���, ��� ������ ������� �������� ������������ ������� �������, ������������������ � dfm-�����", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_ONE_ENTRY_SEARCH:

                            doc.AddCell("����� ������ � ����� � ���, ��� ������������� ������ ������� ����������� � �������� ������� ������ ���� ���", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT:

                            doc.AddCell("����� ������ ��������� �������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT:

                            doc.AddCell("����� ������ ��������� �������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_AUTO:

                            doc.AddCell("����� ������ �� ������ ������. ������� ���������� (��������, �������������) �� �������, ����� ������� ��������� � ������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED:

                            doc.AddCell("����� ������ �� ������ ������� �����. ����� ������� ��������������� ����������� � ������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES:

                            doc.AddCell("����� ������ �� ������ ������. ����� ������� ��������������� ����������� � ������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES:

                            doc.AddCell("����� ������ �� ������ ������. ����� ������� ��������������� ����������� � ������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.NOTHING:

                            break;

                        default:
                            throw new Exception("����������� ������ Store.Function.enFunctionEssentialMarkedType ��� ���������� ������ ReportFunctionsFull");
                    }
                }
            }
            doc.FinishTable();

            doc.font.size = 6;
            doc.font.style.Bold = true;
            doc.AddText("���������� �������, �������� � �������� ������");
            doc.ResetFont();

            doc.font.size = 5;
            doc.AddTable(Caps);
            foreach (Function function in Analyser.Functions)
            {
                    
                if (function.Essentiality == Store.enFunctionEssentials.REDUNDANT)
                {
                    doc.AddRow();
                    doc.AddCell(function.Name.Replace(".", ". ").Replace(":", ": "), 1, 2);


                    if (function.DefFile != null && function.DefFile.FullFileName != "")
                        doc.AddCell(function.DefFile.FileNameForReports + ", " + function.defPosition.line.ToString(), 1, 1);
                    else
                        doc.AddCell("����� �������� ������� ����������", 1, 1);

                    doc.AddRow();

                    switch (function.EssentialMarkedType)
                    {
                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER:

                            List<Function> way = GetWayDataFunction(function, wayData => { return wayData.user; });

                            doc.AddCell("����� ������ � ����� � ���, ��� ������ ������� ���������� �� ������������ ������� (�������������� ������� ����������� ��������� �������), ��� �������������� ��������� �����: " + PrintWay(way), 1, 1);
                            //������� ���� ������ ���� �������
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_AFTER_DFM:

                            way = GetWayDataFunction(function, wayData => { return wayData.dfm; });
                            doc.AddCell("����� ������ � ����� � ���, ��� ������ ������� ���������� �� ������������ ������� (�������������� ������� ����������� �� ������ ������� dfm-������), ��� �������������� ��������� �����: " + PrintWay(way), 1, 1);
                            //������� ���� ������ ���� �������
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_DFM:

                            doc.AddCell("����� ������ � ����� � ���, ��� ������ ������� �������� ������������ ������� �������, ������������������ � dfm-�����", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_ONE_ENTRY_SEARCH:

                            doc.AddCell("����� ������ � ����� � ���, ��� ������������� ������ ������� ����������� � �������� ������� ������ ���� ���", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT:

                            doc.AddCell("����� ������ ��������� �������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT:

                            doc.AddCell("����� ������ ��������� �������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_AUTO:

                            doc.AddCell("����� ������ �� ������ ������. ������� ���������� (��������, �������������) �� �������, ����� ������� ��������� � ������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED:

                            doc.AddCell("����� ������ �� ������ ������� �����. ����� ������� ��������������� ����������� � ������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES:

                            doc.AddCell("����� ������ �� ������ ������. ����� ������� ��������������� ����������� � ������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES:

                            doc.AddCell("����� ������ �� ������ ������. ����� ������� ��������������� ����������� � ������", 1, 1);
                            break;

                        case Store.enFunctionEssentialMarkedTypes.NOTHING:

                            break;

                        default:
                            throw new Exception("����������� ������ Store.Function.enFunctionEssentialMarkedType ��� ���������� ������ ReportFunctionsFull");
                    }
                }
            }

            doc.FinishTable();
            doc.font.size = 6;
            doc.font.style.Bold = true;
            doc.AddText("�������������� �������, �������� � �������� ������");
            doc.ResetFont();

            doc.font.size = 5;
            doc.AddTable(Caps);
            foreach (Function function in Analyser.Functions)
            {
                if (function.DefFile != null && function.DefFile.FullFileName != "")
                {
                    if (function.Essentiality == Store.enFunctionEssentials.UNKNOWN)
                    {
                        doc.AddRow();
                        doc.AddCell(function.Name.Replace(".", ". ").Replace(":", ": "), 1, 1);

                        doc.AddCell(function.DefFile.FileNameForReports + ", " + function.defPosition.line.ToString(), 1, 1);
                    }
                }
            }


            doc.FinishTable();
            doc.font.size = 6;
            doc.font.style.Bold = true;
            doc.AddText("������� � ������������ ���������, �������� � �������� ������");
            doc.ResetFont();

            doc.font.size = 5;
            doc.AddTable(Caps);
            foreach (Function function in Analyser.Functions)
            {
                if (function.DefFile != null && function.DefFile.FullFileName != "")
                {
                    if (function.Essentiality != Store.enFunctionEssentials.REDUNDANT && function.Essentiality != Store.enFunctionEssentials.UNKNOWN && function.Essentiality != Store.enFunctionEssentials.ESSENTIAL)
                    {
                        doc.AddRow();
                        doc.AddCell(function.Name.Replace(".",". ").Replace(":",": "), 1, 1);

                        if (function.DefFile != null && function.DefFile.FullFileName != "")
                            doc.AddCell(function.DefFile.FileNameForReports, 1, 1);
                        else
                            doc.AddCell("�������� ������� ����������", 1, 1);
                    }
                }
            }

            doc.Close();
        }
        

        private static string PrintWay(List<Function> way)
        {
            string res = "";
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            for (int i = way.Count - 1; i >= 0; i--)
            {
                res += way[i].Name;
                if (i > 0)
                    res += " -> ";
            }
            return res.Replace(".",". ");           
        }


        //private static void GenerateWay(System.IO.StreamWriter wr, Function function, Store.Function.enFunctionEssentialMarkedType mark)
        //{

        //    List<Function> stack = new List<Function>();
        //    stack.Add(function);
        //    if (!GenerateWays(stack, wr, mark))
        //        Monitor.Monitor.Error("�� ������� ����������� ����� ��� ������� " + function.Name);
        //}

        //static string lastString = "";
        ///// <summary>
        ///// ������� ����, ������������� �������������� ������ �������
        ///// </summary>
        ///// <param name="stack"></param>
        ///// <param name="wr"></param>
        //private static bool GenerateWays(List<Function> stack, System.IO.StreamWriter wr, Store.Function.enFunctionEssentialMarkedType mark)
        //{
        //    Function func = stack[stack.Count-1];
                
        //    if (func.EssentialMarkedType == mark) //����� �� �����. ��������� ������
        //    {
        //        System.Text.StringBuilder str = new System.Text.StringBuilder();
        //        for (int i = stack.Count - 1; i >= 0; i--)
        //        {
        //            func = stack[i];
        //            str.Append(func.Name);
        //            if (i > 0)
        //                str.Append(" -> ");
        //        }
        //        if (lastString != str.ToString())
        //        {
        //            lastString = str.ToString();
        //            wr.WriteLine(str);
        //        }

        //        return true;
        //    }

        //    foreach(Function f in func.CalledByDirectly)
        //        if(! stack.Contains(f))
        //        {
        //            stack.Add(f);
        //            if (GenerateWays(stack, wr, mark))
        //                return true;
        //            stack.RemoveAt(stack.Count - 1);
        //        }

        //    return false;
        //}


        /// <summary>
        /// ���������� �������� �������� �� ������
        /// </summary>
        /// <param name="FileResults"></param>
        private static void ReportFilesSymple(string FileResults)
        {
            System.IO.StreamWriter wr;

            //������� �������� �� ������ ����������
            wr = new System.IO.StreamWriter(FileResults);
            wr.WriteLine("������������ �����: ");
            foreach (File file in Analyser.Files)
                if (file.getKind() == enFileEssential.ESSENTIAL)
                {
                    wr.Write(" ");
                    wr.WriteLine(file.FileNameForReports);
                }

            wr.WriteLine();
            wr.WriteLine();
            wr.WriteLine();

            wr.WriteLine("���������� �����: ");
            foreach (File file in Analyser.Files)
                if (file.getKind() == enFileEssential.SUPERFLUOUS)
                {
                    wr.Write(" ");
                    wr.WriteLine(file.FileNameForReports);
                }

            wr.WriteLine();
            wr.WriteLine();
            wr.WriteLine();

            wr.WriteLine("�������������� �����: ");
            foreach (File file in Analyser.Files)
                if (file.getKind() == enFileEssential.UNKNOWN)
                {
                    wr.Write(" ");
                    wr.WriteLine(file.FileNameForReports);
                }

            wr.WriteLine();
            wr.WriteLine();
            wr.WriteLine();

            wr.WriteLine("���������� �����: ");
            foreach (File file in Analyser.Files)
                if (file.getKind() != enFileEssential.ESSENTIAL && file.getKind() != enFileEssential.SUPERFLUOUS && file.getKind() != enFileEssential.UNKNOWN)
                {
                    wr.Write(" ");
                    wr.WriteLine(file.FileNameForReports);
                }
            

            wr.Close();
        }

         #region Way Data

        static Dictionary<Function, WayDataFunction> WayData;

        class WayDataFunction
        {
            public Function function;
            public WayDataFunction user = null;
            public WayDataFunction dfm = null;
        }

        private static void GenerateWayData()
        {
            WayData = new Dictionary<Function, WayDataFunction>();
            foreach (Function function in Analyser.Functions)
                switch (function.EssentialMarkedType)
                {
                    case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT:
                        WayDataFunction rootData = GetWayDataFunction(function);
                        GenerateWayData_rec(rootData,
                            data =>
                            {
                                return data.function.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT
                                    || data.user != null;
                            },
                            (current_data, prev_data) => { current_data.user = prev_data; });
                        break;
                    case Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_DFM:
                        rootData = GetWayDataFunction(function);
                        GenerateWayData_rec(rootData,
                            data =>
                            {
                                return data.function.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_DFM
                                    || data.dfm != null;
                            },
                            (current_data, prev_data) => { current_data.dfm = prev_data; });
                        break;
                }
        }

        delegate bool Creteria(WayDataFunction type);
        delegate void Setter(WayDataFunction current, WayDataFunction prev);

        private static void GenerateWayData_rec(WayDataFunction data, Creteria creteria, Setter set)
        {
            foreach (Function func in data.function.CallsDirectly)
            {
                WayDataFunction func_data = GetWayDataFunction(func);
                
                if (!creteria(func_data))
                {
                    set(func_data, data);
                    GenerateWayData_rec(func_data, creteria, set);
                }
            }
        }

        private static WayDataFunction GetWayDataFunction(Function function)
        {
            WayDataFunction res;
            if (!WayData.TryGetValue(function, out res))
            {
                res = new WayDataFunction();
                res.function = function;
                WayData.Add(function, res);
            }
            return res;
        }

        delegate WayDataFunction Next(WayDataFunction data);
        static List<Function> GetWayDataFunction(Function function, Next next)
        {
            List<Function> res = new List<Function>();
            res.Add(function);
            WayDataFunction current = GetWayDataFunction(function);

            while ((current = next(current)) != null)
                res.Add(current.function);

            return res;
        }


        private static void FreeWayData()
        {
            WayData = null;
        }
        #endregion
    }
}
