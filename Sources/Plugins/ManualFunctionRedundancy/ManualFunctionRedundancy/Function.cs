﻿using System;
using System.Collections.Generic;

namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    /// <summary>
    /// Класс, опеределяющий ссылку на вхождение функции
    /// </summary>
    public class FilePositionaRef
    {
        public File file;
        public UInt64 line;
        public UInt64 pos;


        public override bool Equals(object obj)
        {
            return obj != System.DBNull.Value && ((FilePositionaRef)obj).file.FullFileName == file.FullFileName && ((FilePositionaRef)obj).pos == pos;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public FilePositionaRef(File file, UInt64 Line, UInt64 Pos)
        {
            this.file = file;
            this.line = Line;
            this.pos = Pos;
        }
        public override string ToString()
        {
            return file.FileNameForReports + " (" + pos + ")";
        }
    }

    /// <summary>
    /// Описатель функции
    /// </summary>
    public class Function : IComparable<Function>
    {
        /// <summary>
        /// Может использоваться в Database для обратного отображения в БД. обязательно должно содержать уникальный идентификатор функции.
        /// </summary>
        internal Store.IFunction function;
 
        //место определения функции
        public FilePositionaRef defPosition;

        static List<Function> MarkedFuncs = new List<Function>();
        /// <summary>
        /// Перечень функций, которые вызываются из данной функции непосредственно.
        /// </summary>
        public List<Function> CallsDirectly = new List<Function>();
        /// <summary>
        /// Перечень функций, вызывающие данную функцию непосредственно.
        /// </summary>
        public List<Function> CalledByDirectly = new List<Function>();
        /// <summary>
        /// Список вхождений функции. На первом месте в списке - ссылка на определение функции
        /// </summary>
        public List<FilePositionaRef> References = new List<FilePositionaRef>();

        /// <summary>
        /// Список вхождений функции. На первом месте в списке - ссылка на определение функции
        /// </summary>
        public List<Store.IFunction> FunctionReferences = new List<Store.IFunction>();
        
        /// <summary>
        /// добавить функцию в список вызываемых ИЗ данной функцией
        /// </summary>
        /// <param name="func"></param>
        public void Add_CallsDirectly(Function func)
        {
            if (!CallsDirectly.Contains(func))
                CallsDirectly.Add(func);
        }

        public int CompareTo(Function other)
        {
            if ((defPosition == null || defPosition.file == null || defPosition.file.FullFileName == "") && (other.defPosition == null || other.defPosition.file == null || other.defPosition.file.FullFileName == ""))
            {
                return 0;
            }
            if (defPosition == null || defPosition.file == null || defPosition.file.FullFileName == "")
            {
                return -1;
            }
            if (other.defPosition == null || other.defPosition.file == null || other.defPosition.file.FullFileName == "")
            {
                return 1;
            }
            return string.Compare(defPosition.file.FullFileName, other.defPosition.file.FullFileName, true);
        }

        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// добавить функцию в список, вызывающих данную функцию
        /// </summary>
        /// <param name="func"></param>
        public void Add_CalledByDirectly(Function func)
        {
            if (!CalledByDirectly.Contains(func))
                CalledByDirectly.Add(func);
        }

        internal Function(File file, Store.IFunction function)
        {
            this.function = function;

            if(file != null)
                this.defPosition = new FilePositionaRef(file, GetLineInFile(file), GetPositionInFile(file));
            
            if (function.FullName.LastIndexOf(".") >= 0)
            {
                ShortName = function.FullName.Substring(function.FullName.LastIndexOf(".") + 1);
            }
            else
            {
                if (function.FullName.LastIndexOf(":") >= 0)
                {
                    ShortName = function.FullName.Substring(function.FullName.LastIndexOf(":") + 1);
                }
                else
                {
                    ShortName = function.FullName;
                }
            }

        }

        /// <summary>
        /// заполнить список ссылок на вхождения имени функции
        /// </summary>
        internal bool FillFileReferences()
        {
            FilePositionaRef tmp;
            int n = 0;
            bool overfulled = false, noSourceFilesFound = false;
            References.Clear();

            Store.Files fls = IADatabase.storage.files;
            File file;
            Store.FilesIndex i = IADatabase.storage.filesIndex;
            Store.enTypeOfContents type = Store.enTypeOfContents.DATA;
            if (ShortName != "")
            {
                foreach (Store.FilesIndex.Occurrence occ in i.Find(ShortName))
                {
                    file = new File(fls.GetFile(occ.FileID));
                    try
                    {
                        type = Store.enTypeOfContents.DATA;
                        type = fls.Find(file.FullFileName).fileType.Contains();
                    }
                    catch { }
                    if (type == Store.enTypeOfContents.TEXTSOURCES || type == Store.enTypeOfContents.BINARYSOURCES)
                    {
                        tmp = new FilePositionaRef(file, 0, occ.Offset);
                        if (!(References.Contains(tmp)) && System.IO.File.Exists(tmp.file.FullFileName))
                        {
                            References.Add(tmp);
                            ++n;
                        }

                        if (n >= 50)
                        {
                            overfulled = true;
                            break;
                        }
                    }
                    else
                    {
                        noSourceFilesFound = true;
                    }
                }

                if (noSourceFilesFound && !overfulled)
                {
                    References.Add(new FilePositionaRef(new File(fls.GetFile(1)), 100, 0));
                    foreach (Store.FilesIndex.Occurrence occ in i.Find(ShortName))
                    {
                        tmp = new FilePositionaRef(new File(fls.GetFile(occ.FileID)), 0, occ.Offset);
                        if (!(References.Contains(tmp)) && System.IO.File.Exists(tmp.file.FullFileName))
                        {
                            References.Add(tmp);
                            ++n;
                        }

                        if (n >= 50)
                        {
                            overfulled = true;
                            break;
                        }
                    }

                }
            }
            return overfulled;
        }

        /// <summary>
        /// заполнить список ссылок на вхождения имени функции
        /// </summary>
        internal bool FillFunctionReferences()
        {
            int n = 0;
            bool overfulled = false;
            FunctionReferences.Clear();

            Store.Files fls = IADatabase.storage.files;
            Store.Functions fns = IADatabase.storage.functions;
            Store.FilesIndex i = IADatabase.storage.filesIndex;
            Store.IFunction caller;
            if (ShortName != "")
            {
                foreach (Store.FilesIndex.Occurrence occ in i.Find(ShortName))
                {
                    caller = null;
                    int offset = -1;
                    foreach (Store.IFunction func in occ.File.FunctionsDefinedInTheFile())
                    {
                        Store.Location funcDefinition = func.Definition();
                        if (funcDefinition != null && (int)funcDefinition.GetOffset() > offset && funcDefinition.GetOffset() <= occ.Offset)
                        {
                            offset = (int)funcDefinition.GetLine();
                            caller = func;
                        }
                    }
                    if (caller != null && !(FunctionReferences.Contains(caller)))
                    {
                        FunctionReferences.Add(caller);
                        ++n;
                    }

                    if (n >= 50)
                    {
                        overfulled = true;
                        break;
                    }
                }
            }
            return overfulled;
        }

        /// <summary>
        /// заполнить полностью список ссылок на вхождения имени функции
        /// </summary>
        internal void FillFileReferencesFull()
        {
            FilePositionaRef tmp;
            bool noSourceFilesFound = false;
            References.Clear();
            File file;

            Store.Files fls = IADatabase.storage.files;
            Store.FilesIndex i = IADatabase.storage.filesIndex;
            if (ShortName != "")
            {
                foreach (Store.FilesIndex.Occurrence occ in i.Find(ShortName))
                {
                    file = new File(occ.File);
                    if (fls.Find(file.FullFileName).fileType.Contains() == Store.enTypeOfContents.TEXTSOURCES || fls.Find(file.FullFileName).fileType.Contains() == Store.enTypeOfContents.BINARYSOURCES)
                    {
                        tmp = new FilePositionaRef(new File(occ.File), 0, occ.Offset);
                        if (!(References.Contains(tmp)) && System.IO.File.Exists(tmp.file.FullFileName))
                        {
                            References.Add(tmp);
                        }
                    }
                    else
                    {
                        noSourceFilesFound = true;
                    }
                }

                if (noSourceFilesFound)
                {
                    References.Add(new FilePositionaRef(new File(fls.GetFile(1)), 100, 0));
                    foreach (Store.FilesIndex.Occurrence occ in i.Find(ShortName))
                    {
                        file = new File(occ.File);
                        tmp = new FilePositionaRef(new File(occ.File), 0, occ.Offset);
                        if (!(References.Contains(tmp)) && System.IO.File.Exists(tmp.file.FullFileName))
                        {
                            References.Add(tmp);
                        }

                    }

                }
            }
        }

        /// <summary>
        /// заполнить список ссылок на вхождения имени функции
        /// </summary>
        internal void FillFunctionReferencesFull()
        {
            FunctionReferences.Clear();

            Store.Files fls = IADatabase.storage.files;
            Store.Functions fns = IADatabase.storage.functions;
            File file;
            Store.FilesIndex i = IADatabase.storage.filesIndex;
            Store.IFunction caller;
            if (ShortName != "")
            {
                foreach (Store.FilesIndex.Occurrence occ in i.Find(ShortName))
                {
                    file = new File(occ.File);
                    caller = null;
                    int line = -1;
                    foreach (Store.IFunction func in file.containsFunctions)
                    {
                        Store.Location funcDefinitions = func.Definition();
                        if (funcDefinitions != null && (int)funcDefinitions.GetLine() > line && funcDefinitions.GetLine() <= occ.Line)
                        {
                            line = (int)funcDefinitions.GetLine();
                            caller = func;
                        }
                    }
                    if (!(FunctionReferences.Contains(caller)))
                    {
                        FunctionReferences.Add(caller);
                    }

                }
            }
        }

        /// <summary>
        /// Метка, используемая при обходах.
        /// </summary>
        private bool Mark = false;
        internal void ReleaseMark()
        {
            Mark = false;
        }
        internal void MarkMe()
        {
            Mark = true;
            MarkedFuncs.Add(this);
        }
        internal bool IsMarked()
        {
            return Mark;
        }
        internal static void ReleaseAllMarks()
        {
            foreach (Function func in MarkedFuncs)
                func.ReleaseMark();

            MarkedFuncs.Clear();
        }

        /// <summary>
        /// пометить данную функцию как неизбыточную
        /// </summary>
        internal void Essential(Store.enFunctionEssentialMarkedTypes reportSource)
        {
            function.Essential = Store.enFunctionEssentials.ESSENTIAL;
            function.EssentialMarkedType = reportSource;
        }

        /// <summary>
        /// пометить данную функцию как стартовую
        /// </summary>
        internal void ProgramEntry()
        {
            function.UseKind = Store.enFunctionUseKinds.PROGRAM_START;
        }

        /// <summary>
        /// пометить данную функцию как избыточную
        /// </summary>
        internal void Superfluous(Store.enFunctionEssentialMarkedTypes reportSource)
        {
            function.Essential = Store.enFunctionEssentials.REDUNDANT;
            function.EssentialMarkedType = reportSource;
        }

        /// <summary>
        /// файл, в котором данная функция описана
        /// </summary>
        public File DefFile
        {
            get
            {
                if (defPosition != null)
                    return defPosition.file;
                else
                    return null;
            }
        }

        /// <summary>
        /// имя функции
        /// </summary>
        public string Name
        { get { return function.FullName; } }

        /// <summary>
        /// сокращенное имя функции
        /// </summary>
        public string ShortName;
        
        

        /// <summary>
        /// венуть смещение описания функции относительно начала файла
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public UInt64 GetPositionInFile(File file)
        {
            Store.Location location = function.Definition();

            if (location != null && location.GetFileID() == file.getID())
                return location.GetOffset();
            
            return 0;
        }

        /// <summary>
        /// венуть строку описания функции
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public UInt64 GetLineInFile(File file)
        {
            Store.Location location = function.Definition();

            if (location != null && location.GetFileID() == file.getID())
                return location.GetLine();
            
            return 0;
        }

        /// <summary>
        /// вернуть текущий статус функции
        /// </summary>
        public Store.enFunctionEssentials Essentiality
        {
            get 
            {
                return function.Essential;
            }
            set
            {
                function.Essential = value;
            }
        }

        public Store.enFunctionEssentialMarkedTypes EssentialMarkedType
        {
            get
            {
                return function.EssentialMarkedType;
            }
            set
            {
                function.EssentialMarkedType = value;
            }
        }
        /// <summary>
        /// вернуть идентификатор функции
        /// </summary>
        /// <returns></returns>
        internal UInt64 getID()
        {
            return function.Id;
        }

    }
}
