﻿namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    partial class MainControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.ClickedList = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.AcceptChangesButton = new System.Windows.Forms.Button();
            this.CancelChangesButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.OptimumCheckBox = new System.Windows.Forms.CheckBox();
            this.ShowClickedList = new System.Windows.Forms.Button();
            this.UndoButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbFunction = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbShortName = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tbRedunFunctions = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btAccept = new System.Windows.Forms.Button();
            this.btReject = new System.Windows.Forms.Button();
            this.btPostpone = new System.Windows.Forms.Button();
            this.SortFunctionsButton = new System.Windows.Forms.Button();
            this.UnderstandRedundantCheckBox = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.counterFunctionsLeft = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.FunctionEntriesRadioButton = new System.Windows.Forms.RadioButton();
            this.SearchEntriesRadioButton = new System.Windows.Forms.RadioButton();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.StattrSearchButtonbutton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.ShowAllReferencesButton = new System.Windows.Forms.Button();
            this.entryList = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tbDefPlace = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.ViewFile = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 29;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.ClickedList, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(96, 100);
            this.tableLayoutPanel8.TabIndex = 33;
            // 
            // ClickedList
            // 
            this.ClickedList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClickedList.FormattingEnabled = true;
            this.ClickedList.HorizontalScrollbar = true;
            this.ClickedList.Location = new System.Drawing.Point(3, 3);
            this.ClickedList.Name = "ClickedList";
            this.ClickedList.Size = new System.Drawing.Size(90, 54);
            this.ClickedList.TabIndex = 36;
            this.ClickedList.SelectedIndexChanged += new System.EventHandler(this.ClickedList_SelectedIndexChanged);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.AcceptChangesButton, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.CancelChangesButton, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 63);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(90, 34);
            this.tableLayoutPanel9.TabIndex = 37;
            // 
            // AcceptChangesButton
            // 
            this.AcceptChangesButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AcceptChangesButton.Location = new System.Drawing.Point(3, 3);
            this.AcceptChangesButton.Name = "AcceptChangesButton";
            this.AcceptChangesButton.Size = new System.Drawing.Size(39, 28);
            this.AcceptChangesButton.TabIndex = 0;
            this.AcceptChangesButton.Text = "Применить";
            this.AcceptChangesButton.UseVisualStyleBackColor = true;
            this.AcceptChangesButton.Click += new System.EventHandler(this.AcceptChangesButton_Click);
            // 
            // CancelChangesButton
            // 
            this.CancelChangesButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CancelChangesButton.Location = new System.Drawing.Point(48, 3);
            this.CancelChangesButton.Name = "CancelChangesButton";
            this.CancelChangesButton.Size = new System.Drawing.Size(39, 28);
            this.CancelChangesButton.TabIndex = 1;
            this.CancelChangesButton.Text = "Отмена";
            this.CancelChangesButton.UseVisualStyleBackColor = true;
            this.CancelChangesButton.Click += new System.EventHandler(this.CancelChangesButton_Click);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel10.Controls.Add(this.label1, 1, 11);
            this.tableLayoutPanel10.Controls.Add(this.OptimumCheckBox, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.ShowClickedList, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.UndoButton, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.groupBox1, 0, 8);
            this.tableLayoutPanel10.Controls.Add(this.groupBox2, 0, 9);
            this.tableLayoutPanel10.Controls.Add(this.groupBox4, 0, 10);
            this.tableLayoutPanel10.Controls.Add(this.btAccept, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.btReject, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.btPostpone, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.SortFunctionsButton, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.UnderstandRedundantCheckBox, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.button1, 0, 13);
            this.tableLayoutPanel10.Controls.Add(this.counterFunctionsLeft, 0, 11);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(861, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 14;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(205, 599);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(138, 448);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "осталось";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OptimumCheckBox
            // 
            this.OptimumCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.OptimumCheckBox.Checked = true;
            this.OptimumCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tableLayoutPanel10.SetColumnSpan(this.OptimumCheckBox, 2);
            this.OptimumCheckBox.Location = new System.Drawing.Point(3, 3);
            this.OptimumCheckBox.Name = "OptimumCheckBox";
            this.OptimumCheckBox.Size = new System.Drawing.Size(199, 19);
            this.OptimumCheckBox.TabIndex = 30;
            this.OptimumCheckBox.Text = "Выдавать оптимальную функцию";
            this.OptimumCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.OptimumCheckBox.UseVisualStyleBackColor = true;
            this.OptimumCheckBox.CheckStateChanged += new System.EventHandler(this.OptimumCheckBox_CheckStateChanged);
            // 
            // ShowClickedList
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.ShowClickedList, 2);
            this.ShowClickedList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShowClickedList.Location = new System.Drawing.Point(3, 28);
            this.ShowClickedList.Name = "ShowClickedList";
            this.ShowClickedList.Size = new System.Drawing.Size(199, 24);
            this.ShowClickedList.TabIndex = 32;
            this.ShowClickedList.Text = "Показать список нажатий";
            this.ShowClickedList.UseVisualStyleBackColor = true;
            this.ShowClickedList.Click += new System.EventHandler(this.ShowClickedList_Clicked);
            // 
            // UndoButton
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.UndoButton, 2);
            this.UndoButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UndoButton.Location = new System.Drawing.Point(3, 58);
            this.UndoButton.Name = "UndoButton";
            this.UndoButton.Size = new System.Drawing.Size(199, 24);
            this.UndoButton.TabIndex = 31;
            this.UndoButton.Text = "Отменить последнее действие";
            this.UndoButton.UseVisualStyleBackColor = true;
            this.UndoButton.Click += new System.EventHandler(this.UndoButton_Click);
            // 
            // groupBox1
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.groupBox1, 2);
            this.groupBox1.Controls.Add(this.tbFunction);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 233);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(199, 94);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Полное имя";
            // 
            // tbFunction
            // 
            this.tbFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbFunction.Location = new System.Drawing.Point(3, 16);
            this.tbFunction.Multiline = true;
            this.tbFunction.Name = "tbFunction";
            this.tbFunction.ReadOnly = true;
            this.tbFunction.Size = new System.Drawing.Size(193, 75);
            this.tbFunction.TabIndex = 20;
            // 
            // groupBox2
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.groupBox2, 2);
            this.groupBox2.Controls.Add(this.tbShortName);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 333);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(199, 49);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Краткое имя";
            // 
            // tbShortName
            // 
            this.tbShortName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbShortName.Location = new System.Drawing.Point(3, 16);
            this.tbShortName.Multiline = true;
            this.tbShortName.Name = "tbShortName";
            this.tbShortName.ReadOnly = true;
            this.tbShortName.Size = new System.Drawing.Size(193, 30);
            this.tbShortName.TabIndex = 37;
            // 
            // groupBox4
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.groupBox4, 2);
            this.groupBox4.Controls.Add(this.tableLayoutPanel11);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 388);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(199, 49);
            this.groupBox4.TabIndex = 36;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Влияет на избыточность";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.83938F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.16062F));
            this.tableLayoutPanel11.Controls.Add(this.tbRedunFunctions, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.label5, 1, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(193, 30);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // tbRedunFunctions
            // 
            this.tbRedunFunctions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRedunFunctions.Location = new System.Drawing.Point(3, 5);
            this.tbRedunFunctions.Name = "tbRedunFunctions";
            this.tbRedunFunctions.ReadOnly = true;
            this.tbRedunFunctions.Size = new System.Drawing.Size(123, 20);
            this.tbRedunFunctions.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(132, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "функций.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btAccept
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.btAccept, 2);
            this.btAccept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btAccept.Location = new System.Drawing.Point(3, 173);
            this.btAccept.Name = "btAccept";
            this.btAccept.Size = new System.Drawing.Size(199, 24);
            this.btAccept.TabIndex = 15;
            this.btAccept.Text = "Функция неизбыточна";
            this.btAccept.UseVisualStyleBackColor = true;
            this.btAccept.Click += new System.EventHandler(this.btAccept_Click_1);
            // 
            // btReject
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.btReject, 2);
            this.btReject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btReject.Location = new System.Drawing.Point(3, 143);
            this.btReject.Name = "btReject";
            this.btReject.Size = new System.Drawing.Size(199, 24);
            this.btReject.TabIndex = 16;
            this.btReject.Text = "Функция избыточна";
            this.btReject.UseVisualStyleBackColor = true;
            this.btReject.Click += new System.EventHandler(this.btReject_Click_1);
            // 
            // btPostpone
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.btPostpone, 2);
            this.btPostpone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btPostpone.Location = new System.Drawing.Point(3, 113);
            this.btPostpone.Name = "btPostpone";
            this.btPostpone.Size = new System.Drawing.Size(199, 24);
            this.btPostpone.TabIndex = 17;
            this.btPostpone.Text = "Пропустить";
            this.btPostpone.UseVisualStyleBackColor = true;
            this.btPostpone.Click += new System.EventHandler(this.btPostpone_Click_1);
            // 
            // SortFunctionsButton
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.SortFunctionsButton, 2);
            this.SortFunctionsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SortFunctionsButton.Location = new System.Drawing.Point(3, 203);
            this.SortFunctionsButton.Name = "SortFunctionsButton";
            this.SortFunctionsButton.Size = new System.Drawing.Size(199, 24);
            this.SortFunctionsButton.TabIndex = 37;
            this.SortFunctionsButton.Text = "Упорядочить функции по пути";
            this.SortFunctionsButton.UseVisualStyleBackColor = true;
            this.SortFunctionsButton.Click += new System.EventHandler(this.SortFunctionsButton_Click);
            // 
            // UnderstandRedundantCheckBox
            // 
            this.UnderstandRedundantCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.UnderstandRedundantCheckBox.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.UnderstandRedundantCheckBox, 2);
            this.UnderstandRedundantCheckBox.Enabled = false;
            this.UnderstandRedundantCheckBox.Location = new System.Drawing.Point(3, 89);
            this.UnderstandRedundantCheckBox.Name = "UnderstandRedundantCheckBox";
            this.UnderstandRedundantCheckBox.Size = new System.Drawing.Size(199, 17);
            this.UnderstandRedundantCheckBox.TabIndex = 40;
            this.UnderstandRedundantCheckBox.Text = "Избыточна (Understand)";
            this.UnderstandRedundantCheckBox.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.button1, 2);
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 570);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(199, 26);
            this.button1.TabIndex = 41;
            this.button1.Text = "Автомат";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // counterFunctionsLeft
            // 
            this.counterFunctionsLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.counterFunctionsLeft.Location = new System.Drawing.Point(3, 445);
            this.counterFunctionsLeft.Name = "counterFunctionsLeft";
            this.counterFunctionsLeft.ReadOnly = true;
            this.counterFunctionsLeft.Size = new System.Drawing.Size(129, 20);
            this.counterFunctionsLeft.TabIndex = 23;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.groupBox3, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(466, 599);
            this.tableLayoutPanel3.TabIndex = 28;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel2.Controls.Add(this.FunctionEntriesRadioButton, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.SearchEntriesRadioButton, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.SearchTextBox, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.StattrSearchButtonbutton, 3, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 569);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(460, 27);
            this.tableLayoutPanel2.TabIndex = 23;
            // 
            // FunctionEntriesRadioButton
            // 
            this.FunctionEntriesRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.FunctionEntriesRadioButton.AutoSize = true;
            this.FunctionEntriesRadioButton.Checked = true;
            this.FunctionEntriesRadioButton.Location = new System.Drawing.Point(3, 5);
            this.FunctionEntriesRadioButton.Name = "FunctionEntriesRadioButton";
            this.FunctionEntriesRadioButton.Size = new System.Drawing.Size(127, 17);
            this.FunctionEntriesRadioButton.TabIndex = 0;
            this.FunctionEntriesRadioButton.TabStop = true;
            this.FunctionEntriesRadioButton.Text = "Вхождения функции";
            this.FunctionEntriesRadioButton.UseVisualStyleBackColor = true;
            this.FunctionEntriesRadioButton.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // SearchEntriesRadioButton
            // 
            this.SearchEntriesRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchEntriesRadioButton.AutoSize = true;
            this.SearchEntriesRadioButton.Location = new System.Drawing.Point(136, 5);
            this.SearchEntriesRadioButton.Name = "SearchEntriesRadioButton";
            this.SearchEntriesRadioButton.Size = new System.Drawing.Size(169, 17);
            this.SearchEntriesRadioButton.TabIndex = 1;
            this.SearchEntriesRadioButton.Text = "Вхождения идентификатора";
            this.SearchEntriesRadioButton.UseVisualStyleBackColor = true;
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchTextBox.Location = new System.Drawing.Point(311, 3);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(92, 20);
            this.SearchTextBox.TabIndex = 2;
            this.SearchTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // StattrSearchButtonbutton
            // 
            this.StattrSearchButtonbutton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StattrSearchButtonbutton.Location = new System.Drawing.Point(409, 3);
            this.StattrSearchButtonbutton.Name = "StattrSearchButtonbutton";
            this.StattrSearchButtonbutton.Size = new System.Drawing.Size(48, 21);
            this.StattrSearchButtonbutton.TabIndex = 3;
            this.StattrSearchButtonbutton.Text = "Поиск";
            this.StattrSearchButtonbutton.UseVisualStyleBackColor = true;
            this.StattrSearchButtonbutton.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel4);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 58);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(460, 505);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Список вхождений имен";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.ShowAllReferencesButton, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.entryList, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(454, 486);
            this.tableLayoutPanel4.TabIndex = 23;
            // 
            // ShowAllReferencesButton
            // 
            this.ShowAllReferencesButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShowAllReferencesButton.Enabled = false;
            this.ShowAllReferencesButton.Location = new System.Drawing.Point(3, 3);
            this.ShowAllReferencesButton.Name = "ShowAllReferencesButton";
            this.ShowAllReferencesButton.Size = new System.Drawing.Size(448, 24);
            this.ShowAllReferencesButton.TabIndex = 22;
            this.ShowAllReferencesButton.Text = "Показать полный список вхождений";
            this.ShowAllReferencesButton.UseVisualStyleBackColor = true;
            this.ShowAllReferencesButton.Click += new System.EventHandler(this.ShowAllReferencesButton_Click);
            // 
            // entryList
            // 
            this.entryList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.entryList.FormattingEnabled = true;
            this.entryList.HorizontalScrollbar = true;
            this.entryList.Location = new System.Drawing.Point(3, 33);
            this.entryList.Name = "entryList";
            this.entryList.Size = new System.Drawing.Size(448, 450);
            this.entryList.TabIndex = 0;
            this.entryList.SelectedIndexChanged += new System.EventHandler(this.entryList_SelectedIndexChanged);
            this.entryList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.entryList_KeyPress);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tableLayoutPanel6);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(460, 49);
            this.groupBox5.TabIndex = 25;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Место описания функции";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.tbDefPlace, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(454, 30);
            this.tableLayoutPanel6.TabIndex = 22;
            // 
            // tbDefPlace
            // 
            this.tbDefPlace.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDefPlace.Location = new System.Drawing.Point(3, 5);
            this.tbDefPlace.Name = "tbDefPlace";
            this.tbDefPlace.ReadOnly = true;
            this.tbDefPlace.Size = new System.Drawing.Size(448, 20);
            this.tbDefPlace.TabIndex = 21;
            this.tbDefPlace.Click += new System.EventHandler(this.tbDefPlace_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 210F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel10, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.splitContainer2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.ViewFile, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1069, 605);
            this.tableLayoutPanel1.TabIndex = 35;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(389, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tableLayoutPanel3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tableLayoutPanel8);
            this.splitContainer2.Panel2Collapsed = true;
            this.splitContainer2.Size = new System.Drawing.Size(466, 599);
            this.splitContainer2.SplitterDistance = 253;
            this.splitContainer2.TabIndex = 32;
            // 
            // ViewFile
            // 
            this.ViewFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ViewFile.Location = new System.Drawing.Point(3, 3);
            this.ViewFile.Name = "ViewFile";
            this.ViewFile.Size = new System.Drawing.Size(380, 599);
            this.ViewFile.TabIndex = 33;
            this.ViewFile.Text = "";
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label6);
            this.MinimumSize = new System.Drawing.Size(200, 200);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(1069, 605);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.CheckedListBox ClickedList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button AcceptChangesButton;
        private System.Windows.Forms.Button CancelChangesButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.CheckBox OptimumCheckBox;
        private System.Windows.Forms.Button ShowClickedList;
        private System.Windows.Forms.Button UndoButton;
        private System.Windows.Forms.Button btAccept;
        private System.Windows.Forms.Button btPostpone;
        private System.Windows.Forms.Button btReject;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbFunction;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbShortName;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TextBox counterFunctionsLeft;
        private System.Windows.Forms.TextBox tbRedunFunctions;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SortFunctionsButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RadioButton FunctionEntriesRadioButton;
        private System.Windows.Forms.RadioButton SearchEntriesRadioButton;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.Button StattrSearchButtonbutton;
        private System.Windows.Forms.CheckBox UnderstandRedundantCheckBox;
        private System.Windows.Forms.RichTextBox ViewFile;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button ShowAllReferencesButton;
        private System.Windows.Forms.ListBox entryList;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox tbDefPlace;


    }
}

