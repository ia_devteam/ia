﻿//#define cheat

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Store;
using FileOperations;

using IA.Extensions;

namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    internal partial class MainControl : UserControl
    {
        /// <summary>
        /// Функция, в настоящий момент предоставленная для обзора эксперту.
        /// </summary>
        Files files;

        Function function;

        File oldFile = null;

        bool auto = false;
 
        ManualFunctionRedundancy plugin;

        public MainControl(ManualFunctionRedundancy plugin)
        {
            InitializeComponent();

            this.files = IADatabase.storage.files;
            Next();
            this.plugin = plugin;
        }

        /// <summary>
        /// Вызывается из рабочего потока, когда надо спросить эксперта
        /// </summary>
        public void Next()
        {
            //осуществлена ли переброска отложенных функций в основной список
            bool isPosponedFlushed;
            Function fnOld;

            File file = null;
            do
            {
                fnOld = function;
                if (OptimumCheckBox.Checked)
                {
                    function = Analyser.GetBestFunction(out isPosponedFlushed);
                    if (isPosponedFlushed)
                        MessageBox.Show("Пропущенные функции были возвращены в рассмотрение");
                    if (function != null)
                    {
                        tbRedunFunctions.Text = function.CallsDirectly.Count.ToString();
                    }
                }
                else
                {
                    if (Analyser.neverCalledFunctions.Count != 0)
                    {
                        function = Analyser.neverCalledFunctions[0];
                    }
                    else
                    {
                        if (Analyser.Functions.Count != 0)
                        {
                            function = Analyser.Functions[0];
                        }
                        else
                        {
                            function = null;
                        }
                    }
                }
                if (function == null)
                {
                    oldFile = null;
                    ViewFile.Text = "Проверка избыточности завершена.";
                    counterFunctionsLeft.Text = "0";
                    tbRedunFunctions.Text = "";
                    return;
                }

                //выведение полного пути к функции
                file = function.DefFile;
                if (file == null)
                {
                    file = function.DefFile;
                    Analyser.neverCalledFunctions.Remove(function);
                    Analyser.Functions.Remove(function);
                }
#if cheat 
                else
                {
                    if (function != null && savedFunction == null && (function.function.EssentialUnderstand == enFunctionEssentialsUnderstand.ESSENTIAL || function.Name.EndsWith("Click") || function.Name.EndsWith("KeyPress") || function.Name.EndsWith("Down")
                        || function.Name.EndsWith("Check") || function.Name.EndsWith("Enter") || function.Name.EndsWith("Exit") || function.Name.EndsWith("Change") || function.Name.EndsWith("KeyUp")
                        || function.Name.EndsWith("MouseUp") || function.Name.EndsWith("Changing") || function.ShortName.StartsWith("On") || function.ShortName.StartsWith("DragOver")
                        || function.ShortName.StartsWith("SelectCell") || function.ShortName.StartsWith("Clicked") || function.ShortName.EndsWith("Clicked")))
                    {
                        Analyser.Accept(function, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                        continue;
                    }
                    function.FillFileReferences();
                    if (function != null && savedFunction == null && (function.function.EssentialUnderstand == enFunctionEssentialsUnderstand.ESSENTIAL || function.References.Count > 5 || function.Name.EndsWith("Click") || function.Name.EndsWith("KeyPress") || function.Name.EndsWith("Down")
                        || function.Name.EndsWith("Check") || function.Name.EndsWith("Enter") || function.Name.EndsWith("Exit") || function.Name.EndsWith("Change") || function.Name.EndsWith("KeyUp")
                        || function.Name.EndsWith("MouseUp") || function.Name.EndsWith("Changing") || function.ShortName.StartsWith("On") || function.ShortName.StartsWith("DragOver")
                        || function.ShortName.StartsWith("SelectCell") || function.ShortName.StartsWith("Clicked") || function.ShortName.EndsWith("Clicked") || function.References.Count == 0))
                    {
                        Analyser.Accept(function, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                        continue;
                    }
                    if (function != null && savedFunction == null && (function.References.Count <= 1 || function.function.EssentialUnderstand == enFunctionEssentialsUnderstand.REDUNDANT && function.References.Count <= 2))
                    {
                        Analyser.Reject(function, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT);
                        continue;
                    }
                }
#endif
            }
#if cheat
            while (file == null || function.function.EssentialUnderstand == enFunctionEssentialsUnderstand.ESSENTIAL || function.function.EssentialUnderstand == enFunctionEssentialsUnderstand.REDUNDANT && function.References.Count <= 2 || function.References.Count > 5  || function.Name.EndsWith("Click") || function.Name.EndsWith("KeyPress") || function.Name.EndsWith("Down")
                || function.Name.EndsWith("Check") || function.Name.EndsWith("Enter") || function.Name.EndsWith("Exit") || function.Name.EndsWith("Change") || function.Name.EndsWith("KeyUp")
                || function.Name.EndsWith("MouseUp") || function.Name.EndsWith("Changing") || function.ShortName.StartsWith("On") || function.ShortName.StartsWith("DragOver")
                || function.ShortName.StartsWith("SelectCell") || function.ShortName.StartsWith("Clicked") || function.ShortName.EndsWith("Clicked") || function.References.Count <= 1);
#else 
            while (file == null);
#endif
            loadFunctionToForm();
        }

        private void btPostpone_Click_1(object sender, EventArgs e)
        {
            if (function != null && savedFunction == null)
            {
                Analyser.Postpone(function);
                Next();
            }
        }

        private void btReject_Click_1(object sender, EventArgs e)
        {
            if (function != null && savedFunction == null)
            {
                Analyser.Reject(function, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT);
                if (tableLayoutPanel8.Visible)
                {
                    ClickedList.Items.Add(function, false);
                }
                Next();
            }
            if (savedFunction != null)
            {
                try
                {
                    int i;
                    if (function.Essentiality != Store.enFunctionEssentials.REDUNDANT)
                    {

                        for (i = 0; i < Analyser.WorkSequence.Count; ++i)
                        {
                            if (Analyser.WorkSequence[i].function == function)
                            {
                                break;
                            }
                        }
                        Analyser.WorkSequence[i].function.Essentiality = Store.enFunctionEssentials.REDUNDANT;
                        Analyser.WorkSequence[i].function.EssentialMarkedType = Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT;
                        foreach (Function fn in Analyser.WorkSequence[i].called)
                        {
                            fn.Essentiality = Store.enFunctionEssentials.UNKNOWN;
                            fn.EssentialMarkedType = Store.enFunctionEssentialMarkedTypes.NOTHING;
                            Analyser.Functions.Add(fn);
                        }
                        for (i = 0; i < ClickedList.Items.Count; ++i)
                        {
                            ClickedList.SetItemChecked(i, false);
                        }

                        //заполняем список невызываемых функций
                        Analyser.FillNoCallsFunctions();
                        
                        ClickedList.SelectedIndex = -1;

                        Next();
                    }
                    else
                    {
                        ClickedList.SelectedIndex = -1;
                        function = savedFunction;
                        savedFunction = null;
                        Next();
                    }
                }
                catch (Exception ex)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, ex.Message);
                }
            }
        }

        private void btAccept_Click_1(object sender, EventArgs e)
        {
            if (function != null && savedFunction == null)
            {
                Analyser.Accept(function, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                if (tableLayoutPanel8.Visible)
                {
                    ClickedList.Items.Add(function, true);
                }
                Next();
            }
            if (savedFunction != null)
            {
                try
                {
                    if (function.Essentiality != Store.enFunctionEssentials.ESSENTIAL)
                    {
                        int i;
                        for(i = 0; i < Analyser.WorkSequence.Count; ++i)
                        {
                            if (Analyser.WorkSequence[i].function == function)
                            {
                                break;
                            }
                        }
                        function.CallsDirectly.AddRange(Analyser.WorkSequence[i].called);
                        Analyser.Functions.Add(function);
                        Analyser.Accept(function, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                    }
                    for (int i = 0; i < ClickedList.Items.Count; ++i)
                    {
                        ClickedList.SetItemChecked(i, true);
                    }
                    ClickedList.SelectedIndex = -1;
                    function = savedFunction;
                    savedFunction = null;
                    Next();
                }
                catch (Exception ex)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, ex.Message);
                }


            }
        }

        int savedIndex = -1;

        private void entryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entryList.SelectedIndex != -1 && !(entryList.SelectedItem is string))
            {
                if (SearchEntriesRadioButton.Checked)
                {
                    if (((FilePositionaRef)entryList.SelectedItem).file != null && System.IO.File.Exists(((FilePositionaRef)entryList.SelectedItem).file.FullFileName))
                    {
                        if (!auto)
                            markSelection(ViewFile, ((FilePositionaRef)entryList.SelectedItem).file, (int)((FilePositionaRef)entryList.SelectedItem).pos, (int)SearchTextBox.Text.Length);
                    }
                }
                else if (function != null)
                {
                    if (function.References[entryList.SelectedIndex].file != null && System.IO.File.Exists(function.References[entryList.SelectedIndex].file.FullFileName))
                    {
                        if (!auto)
                            markSelection(ViewFile, function.References[entryList.SelectedIndex].file, (int)((FilePositionaRef)entryList.SelectedItem).pos, (int)function.ShortName.Length);
                    }
                    else
                    {
                        ViewFile.Text = "Файл с определением функции не известен";
                    }
                }
                savedIndex = entryList.SelectedIndex;
            }
            if (entryList.SelectedItem is string)
            {
                if (savedIndex > entryList.SelectedIndex)
                {
                    if (entryList.SelectedIndex > 0)
                    {
                        entryList.SelectedIndex = entryList.SelectedIndex - 1;
                    }
                    else
                    {
                        if (entryList.SelectedIndex < entryList.Items.Count - 1)
                        {
                            entryList.SelectedIndex = entryList.SelectedIndex + 1;
                        }
                    }
                }
                else
                {
                    if (entryList.SelectedIndex < entryList.Items.Count - 1)
                    {
                        entryList.SelectedIndex = entryList.SelectedIndex + 1;
                    }
                    else
                    {
                        if (entryList.SelectedIndex > 0)
                        {
                            entryList.SelectedIndex = entryList.SelectedIndex - 1;
                        }
                    }
                }
            }
        }

        private int CalcPos(RichTextBox ViewFile, ulong line, ulong col)
        {
            int n = 0;

            for (ulong i = 0; i < line; ++i)
            {
                n += ViewFile.Lines[i].Length;
            }

            return n + (int)(col + line);
        }

        private void entryList_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case 'y':
                case 'Y':
                    btAccept_Click_1(sender, null);
                    break;
                case 'n':
                case 'N':
                    btReject_Click_1(sender, null);
                    break;
                case 'P':
                case 'p':
                    btPostpone_Click_1(sender, null);
                    break;
                case 'c':
                    for (int i = 0; i < 100; ++i)
                        btAccept_Click_1(sender, null);
                    break;

            }
        }

        private void ViewFile_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case 'y':
                case 'Y':
                    btAccept_Click_1(sender, null);
                    break;
                case 'n':
                case 'N':
                    btReject_Click_1(sender, null);
                    break;
            }
        }

        private void UndoButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Analyser.WorkSequence.Count == 0)
                {
                    MessageBox.Show("Все действия отменены");
                    return;
                }
                Analyser.WorkSequence[Analyser.WorkSequence.Count - 1].function.Essentiality = Store.enFunctionEssentials.UNKNOWN;
                Analyser.WorkSequence[Analyser.WorkSequence.Count - 1].function.EssentialMarkedType = Store.enFunctionEssentialMarkedTypes.NOTHING;
                foreach (Function fn in Analyser.WorkSequence[Analyser.WorkSequence.Count - 1].called)
                {
                    fn.Essentiality = Store.enFunctionEssentials.UNKNOWN;
                    fn.EssentialMarkedType = Store.enFunctionEssentialMarkedTypes.NOTHING;
                }
                Analyser.Functions.AddRange(Analyser.WorkSequence[Analyser.WorkSequence.Count - 1].called);
                foreach (Function fn in Analyser.WorkSequence[Analyser.WorkSequence.Count - 1].called)
                {
                    foreach (Function func in fn.CalledByDirectly)
                    {
                        if (func.function.Essential == enFunctionEssentials.ESSENTIAL)
                        {
                            Analyser.Accept(fn, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER);
                            break;
                        }
                    }
                }
                Analyser.Functions.Add(Analyser.WorkSequence[Analyser.WorkSequence.Count - 1].function);

                Analyser.WorkSequence.RemoveAt(Analyser.WorkSequence.Count - 1);
                Analyser.FillNoCallsFunctions();
                    
                ClickedList.Items.Clear();
                foreach (WorkSequenceElement elem in Analyser.WorkSequence)
                {
                    ClickedList.Items.Add(elem.function, elem.function.Essentiality == Store.enFunctionEssentials.ESSENTIAL);
                }

                Next();
            }
            catch (Exception ex)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, ex.Message);
            }
        }

        private void AcceptChangesButton_Click(object sender, EventArgs e)
        {
            int i;
            try
            {
                foreach (Function function in ClickedList.Items)
                {
                    if (ClickedList.CheckedItems.Contains(function) && function.Essentiality == Store.enFunctionEssentials.REDUNDANT ||
                        !ClickedList.CheckedItems.Contains(function) && function.Essentiality == Store.enFunctionEssentials.ESSENTIAL)
                    {
 
                        for (i = 0; i < Analyser.WorkSequence.Count; ++i)
                        {
                            if (Analyser.WorkSequence[i].function == function)
                            {
                                function.CallsDirectly.AddRange(Analyser.WorkSequence[i].called);
                                Analyser.Functions.Add(function);
                                break;
                            }
                        }


                        if (ClickedList.CheckedItems.Contains(function))
                        {
                            Analyser.Accept(function, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                        }
                        else
                        {
                            Analyser.Reject(function, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT);
                            foreach (Function fn in Analyser.WorkSequence[i].called)
                            {
                                if (fn.EssentialMarkedType == enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER)
                                {
                                    fn.Essentiality = Store.enFunctionEssentials.UNKNOWN;
                                    fn.EssentialMarkedType = Store.enFunctionEssentialMarkedTypes.NOTHING;
                                    
                                    bool found = false;
                                    foreach (Function func in fn.CalledByDirectly)
                                    {
                                        if (func.function.Essential == enFunctionEssentials.ESSENTIAL)
                                        {
                                            Analyser.Accept(fn, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER);
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                    {
                                        foreach (Function func in Analyser.Functions)
                                        {
                                            if (fn.CalledByDirectly.Contains(func))
                                            {
                                                func.CallsDirectly.Add(fn);
                                            }
                                        }
                                        foreach (Function func in Analyser.neverCalledFunctions)
                                        {
                                            if (fn.CalledByDirectly.Contains(func))
                                            {
                                                func.CallsDirectly.Add(fn);
                                            }
                                        }
                                        foreach (Function func in Analyser.postponeFunctions)
                                        {
                                            if (fn.CalledByDirectly.Contains(func))
                                            {
                                                func.CallsDirectly.Add(fn);
                                            }
                                        }
                                    }
                                }
                            }
                            Analyser.WorkSequence[i].called.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, ex.Message);
            }
            if (savedFunction != null)
            {
                this.function = savedFunction;
                savedFunction = null;
                Next();
            }
        }

        public void loadFunctionToForm()
        {
            //выведение полного имени функции
            if (function.Name == "")
            {
                tbFunction.Text = "Анонимная функция";
            }        
            else
            {
                tbFunction.Text = function.Name;
            }


            //выведение сокращенного имени функции
            tbShortName.Text = function.ShortName;

            //заполнение списка вхождений функций
            ShowAllReferencesButton.Enabled = function.FillFileReferences();
            //ShowFunctionReferencesButton.Enabled = function.FillFunctionReferences();

            FunctionEntriesRadioButton.Checked = true;

            //выведение содержимого файла с описнием функции и подсветка места вхождения функции
            tbDefPlace.Text = function.defPosition.ToString().Insert(function.defPosition.ToString().LastIndexOf("(") + 1, "Смещение ");
            if (System.IO.File.Exists(function.DefFile.FullFileName))
            {
                if (function.References.Count == 0)
                {
                    function.References.Add(new FilePositionaRef(function.DefFile, function.defPosition.line, function.defPosition.pos));
                }                
            }

            entryList.Items.Clear();
            for (int i = 0; i < function.References.Count; ++i)
            {
                if (function.References[i].line != 0)
                {
                    entryList.Items.Add("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                }
                else
                {
                    entryList.Items.Add(new FilePositionaRef(function.References[i].file, function.References[i].line, function.References[i].pos));
                }
            }
            if (entryList.Items.Count > 0)
            {
                entryList.SelectedIndex = -1;
            }

            tbDefPlace_Click(null, null);

            counterFunctionsLeft.Text = (Analyser.Functions.Count + Analyser.neverCalledFunctions.Count + Analyser.postponeFunctions.Count).ToString();

#if cheat 
            string ln = ViewFile.Text.Substring(ViewFile.SelectionStart, ViewFile.Text.IndexOf('\n', ViewFile.SelectionStart) - ViewFile.SelectionStart);
            if (ln.Contains("public"))
            {
                Analyser.acceptCalled(function, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                Next();
            }
#endif
            UnderstandRedundantCheckBox.Checked = (function.function.EssentialUnderstand == enFunctionEssentialsUnderstand.REDUNDANT);

        }

        private void ShowClickedList_Clicked(object sender, EventArgs e)
        {
            splitContainer2.Panel2Collapsed = !splitContainer2.Panel2Collapsed;
            if (!splitContainer2.Panel2Collapsed)
            {
                ShowClickedList.Text = "Скрыть список нажатий";
                ClickedList.Items.Clear();
                foreach (WorkSequenceElement elem in Analyser.WorkSequence)
                {
                    ClickedList.Items.Add(elem.function, elem.function.Essentiality == Store.enFunctionEssentials.ESSENTIAL);
                }
                btPostpone.Enabled = false;
            }
            else
            {
                ShowClickedList.Text = "Показать список нажатий";
                if (savedFunction != null)
                {
                    function = savedFunction;
                    savedFunction = null;

                    loadFunctionToForm();
                }
                else
                {
                    Next();
                }
                btPostpone.Enabled = true;
            }

        }

        private void CancelChangesButton_Click(object sender, EventArgs e)
        {
            if (savedFunction != null)
            {
                function = savedFunction;
                savedFunction = null;
                Next();
            }
        }

        private Function savedFunction;

        private void ClickedList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ClickedList.SelectedIndex == -1)
            {
                return;
            }
            if (savedFunction == null)
            {
                savedFunction = function;
            }

            if (function != (Function)ClickedList.SelectedItem)
            {
                function = (Function)ClickedList.SelectedItem;
                loadFunctionToForm();
            }
        }

        private void ShowAllReferencesButton_Click(object sender, EventArgs e)
        {
            List<object> collector = new List<object>();

            function.FillFileReferencesFull();
            ShowAllReferencesButton.Enabled = false;
            //выведение содержимого файла с описнием функции и подсветка места вхождения функции
            collector.Clear();
            for (int i = 0; i < function.References.Count; ++i)
            {
                collector.Add(new FilePositionaRef(function.References[i].file, function.References[i].line,function.References[i].pos));
            }
            entryList.Items.Clear();
            for (int i = 0; i < function.References.Count; ++i)
            {
                if (function.References[i].line != 0)
                {
                    entryList.Items.Add("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                }
                else
                {
                    entryList.Items.Add(new FilePositionaRef(function.References[i].file, function.References[i].line, function.References[i].pos));
                }
            }
            if (entryList.Items.Count > 0)
            {
                entryList.SelectedIndex = -1;
            }
        }

        private void SortFunctionsButton_Click(object sender, EventArgs e)
        {
            Analyser.neverCalledFunctions.Sort();
        }

        private void OptimumCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            SortFunctionsButton.Enabled = OptimumCheckBox.Checked;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            SearchEntriesRadioButton.Checked = true;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (FunctionEntriesRadioButton.Checked)
            {
                ShowAllReferencesButton.Enabled = function.FillFileReferences();
                entryList.Items.Clear();
                for (int i = 0; i < function.References.Count; ++i)
                {
                    entryList.Items.Add(new FilePositionaRef(function.References[i].file, function.References[i].line, function.References[i].pos));
                }
            }
            else
            {
                ShowAllReferencesButton.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FilePositionaRef tmp;
            entryList.Items.Clear();

            Store.Files fls = IADatabase.storage.files;
            Store.FilesIndex i = IADatabase.storage.filesIndex;
            if (SearchTextBox.Text != "")
            {
                foreach (Store.FilesIndex.Occurrence occ in i.Find(SearchTextBox.Text))
                {
                    tmp = new FilePositionaRef(new File(occ.File), 0, occ.Offset);
                    if (!(entryList.Items.Contains(tmp)))
                    {
                        entryList.Items.Add(tmp);
                    }
                }
            }
        }

        void markSelection(RichTextBox viewFile, File File, int startPos, int length)
        {
            if (startPos < 0)
                return;
            string contents = "";
            try
            {
                if (oldFile == null || oldFile.FullFileName != File.FullFileName)
                {
                    ViewFile.Text = "Загрузка текста...";
                    contents = FileCashe.Find(File.FullFileName);
                    oldFile = File;
                    for (int i = contents.Substring(startPos).Split('\n').Length; i < 100; ++i)
                    {
                        contents += '\n';
                    }
                    if (contents.Length < 2000000 || MessageBox.Show("Большой файл. Продолжить открытие?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        viewFile.Text = contents.Replace("\n", Environment.NewLine);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    contents = viewFile.Text;
                    if (viewFile.Text.Length > startPos)
                    {
                        for (int i = viewFile.Text.Substring(startPos).Split('\n').Length; i < 100; ++i)
                        {
                            viewFile.Text += '\n';
                        }
                    }
                }
            }
            catch (System.IO.IOException)
            {

            }

            viewFile.SelectAll();
            viewFile.SelectionBackColor = Color.White;
            viewFile.SelectionColor = Color.Black;

            viewFile.Select(startPos, length);
            viewFile.SelectionBackColor = Color.Black;
            viewFile.SelectionColor = Color.White;
            int q = 7, stPos = startPos;
            while (q > 0 && startPos > 0)
            {
                if (contents[startPos] == '\n')
                {
                    --q;
                    if (q == 6)
                    {
                        stPos = startPos + 1;
                    }
                }
                --startPos;
            }
            if (q == 0)
            {
                viewFile.SelectionStart = startPos;
            }
            viewFile.ScrollToCaret();

            viewFile.SelectionStart = stPos;
        }

        private void tbDefPlace_Click(object sender, EventArgs e)
        {
            if (function.DefFile != null && System.IO.File.Exists(function.DefFile.FullFileName))
            {
                if (!auto)
                {
                    if (tbShortName.Text.Length != 0)
                        markSelection(ViewFile, function.DefFile, (int)function.defPosition.pos, tbShortName.Text.Length);
                    else
                        markSelection(ViewFile, function.DefFile, (int)function.defPosition.pos, 1);
                }
                entryList.SelectedIndex = -1;
            }
            else
            {
                ViewFile.Text = "Файл с описанием функции неизвестен";
            }
        }

        private void ShowFunctionReferencesButton_Click(object sender, EventArgs e)
        {
            List<object> collector = new List<object>();

            function.FillFunctionReferencesFull();
            ShowAllReferencesButton.Enabled = false;
            //выведение содержимого файла с описнием функции и подсветка места вхождения функции
            collector.Clear();
            for (int i = 0; i < function.FunctionReferences.Count; ++i)
            {
                collector.Add(function.FunctionReferences[i]);
            }
            entryList.Items.Clear();
            entryList.Items.AddRange(collector.ToArray());
            if (entryList.Items.Count > 0)
            {
                entryList.SelectedIndex = 0;
            }
        }


        public void button1_Click(object sender, EventArgs e)
        {
            this.auto = true;
            while (true)
            {
                Application.DoEvents();
                int cnt;
                if (!int.TryParse(counterFunctionsLeft.Text, out cnt) || (cnt <= 0))
                {
                    this.auto = false;
                    return;
                }
                if (UnderstandRedundantCheckBox.Checked)
                {
                    if (function != null && savedFunction == null)
                    {
                        Analyser.Reject(function, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT);
                        if (tableLayoutPanel8.Visible)
                        {
                            ClickedList.Items.Add(function, false);
                        }
                        Next();
                    }
                    if (savedFunction != null)
                    {
                        try
                        {
                            int i;
                            if (function.Essentiality != Store.enFunctionEssentials.REDUNDANT)
                            {

                                for (i = 0; i < Analyser.WorkSequence.Count; ++i)
                                {
                                    if (Analyser.WorkSequence[i].function == function)
                                    {
                                        break;
                                    }
                                }
                                Analyser.WorkSequence[i].function.Essentiality = Store.enFunctionEssentials.REDUNDANT;
                                Analyser.WorkSequence[i].function.EssentialMarkedType = Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT;
                                foreach (Function fn in Analyser.WorkSequence[i].called)
                                {
                                    fn.Essentiality = Store.enFunctionEssentials.UNKNOWN;
                                    fn.EssentialMarkedType = Store.enFunctionEssentialMarkedTypes.NOTHING;
                                    Analyser.Functions.Add(fn);
                                }
                                for (i = 0; i < ClickedList.Items.Count; ++i)
                                {
                                    ClickedList.SetItemChecked(i, false);
                                }

                                //заполняем список невызываемых функций
                                Analyser.FillNoCallsFunctions();
                            }
                            else
                            {
                                function = savedFunction;
                                savedFunction = null;
                            }

                            ClickedList.SelectedIndex = -1;
                            Next();
                        }
                        catch (Exception ex)
                        {
                            IA.Monitor.Log.Error(PluginSettings.Name, ex.Message);
                        }
                    }
                }
                else
                {
                    if (function != null && savedFunction == null)
                    {
                        Analyser.Accept(function, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                        if (tableLayoutPanel8.Visible)
                        {
                            ClickedList.Items.Add(function, true);
                        }
                        Next();
                    }
                    if (savedFunction != null)
                    {
                        try
                        {
                            if (function.Essentiality != Store.enFunctionEssentials.ESSENTIAL)
                            {
                                int i;
                                for (i = 0; i < Analyser.WorkSequence.Count; ++i)
                                {
                                    if (Analyser.WorkSequence[i].function == function)
                                    {
                                        break;
                                    }
                                }
                                function.CallsDirectly.AddRange(Analyser.WorkSequence[i].called);
                                Analyser.Functions.Add(function);
                                Analyser.Accept(function, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                            }
                            for (int i = 0; i < ClickedList.Items.Count; ++i)
                            {
                                ClickedList.SetItemChecked(i, true);
                            }
                            ClickedList.SelectedIndex = -1;
                            function = savedFunction;
                            savedFunction = null;
                            Next();
                        }
                        catch (Exception ex)
                        {
                            IA.Monitor.Log.Error(PluginSettings.Name, ex.Message);
                        }
                    }
                }
            }
        }
    }

    public class WorkSequenceElement
    {
        public Function function;
        public List<Function> called;
        public WorkSequenceElement(Function function)
        {
            this.function = function;
            called = new List<Function>();
        }
    }

    public class FileContents
    {
        public string Filename;
        public string Contents;

        public FileContents(string Fn, string C)
        {
            Filename = Fn;
            Contents = C;
        }
    }

    public static class FileCashe
    {
        static public List<FileContents> Filecashe = new List<FileContents>();
        static public int length = 0;

        static public string Add(string FileName)
        {
            AbstractReader reader = null;
            try
            {
                reader = new AbstractReader(FileName);
            }
            catch (Exception ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    "Не удалось открыть файл " + FileName + "."/*,
                    ex.ToFullMultiLineMessage()*/
                }.ToMultiLineString();

                IA.Monitor.Log.Error(PluginSettings.Name, message);
            }


            string Contents = reader.getNormalizedText();
            length += Contents.Length;
            while (length> 100000000 && Filecashe.Count > 1)
            {
                length -= Filecashe[0].Contents.Length;
                Filecashe.RemoveAt(0);
            }

            Filecashe.Add(new FileContents(FileName, Contents));
            return Contents;
        }

        static public string Find(string Filename)
        {
            foreach (FileContents FC in Filecashe)
            {
                if (FC.Filename == Filename)
                {
                    return FC.Contents;
                }
            }
            return Add(Filename);
        }

    }
}
