﻿using System;
using System.Collections.Generic;
using Store;

namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    /// <summary>
    /// Описатель файла
    /// </summary>
    public class File
    {
        //полное имя, расширение и ИД файла
        IFile file;

        //метки файла - для анализа избыточности по файлам
        public bool unrecognized = true;

        //список описанных функций
        internal List<Function> containsFunctions = new List<Function>();


        //полное имя файла
        public string FullFileName
        {
            get { return file.FullFileName_Original; }
        }

        public string FileNameForReports
        {
            get { return file.FileNameForReports; }
        }

        internal File(IFile file)
        {
            this.file = file;
        }

        /// <summary>
        /// Метка, используемая при обходах.
        /// </summary>
        private bool Mark = false;
        internal void ReleaseMark()
        {
            Mark = false;
        }
        internal void MarkMe()
        {
            Mark = true;
        }
        internal bool IsMarked()
        {
            return Mark;
        }

        //пометить файл как неизбыточный
        internal void Essential()
        {
            unrecognized = false;
            file.FileEssential = enFileEssential.ESSENTIAL;
        }


        //пометить файл как избыточный
        internal void Redundant()
        {
            unrecognized = false;
            file.FileEssential = enFileEssential.SUPERFLUOUS;
        }

        //вернуть текущий статус файла
        internal enFileEssential getKind()
        {
            return file.FileEssential;
        }

        //вернуть ИД файла
        internal UInt64 getID()
        {
            return file.Id;
        }
     }
}
