﻿using System;
using System.Collections.Generic;
using Store;

namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    public class IADatabase 
    {
        static public Storage storage;

        static public void Initialize(Storage storage)
        {
            IADatabase.storage = storage;
        }

        /// <summary>
        /// Прочитать данные из Хранилища
        /// </summary>
        /// <param name="skip">=true, если уже распознанные функции и файлы надо пропустить
        ///                    =false, если читать все оптом</param>
        static public void Read(bool skip)
        {
            Analyser.Clear();

            //Читаем перечень файлов
            Dictionary<UInt64, File> table = new Dictionary<ulong,File>();
            foreach (Store.IFile file in storage.files.EnumerateFiles(enFileKind.anyFile))
            {
                File f = new File(file);
                table.Add(file.Id, f);
                if (f.getKind() == Store.enFileEssential.UNKNOWN || !skip) //Если файл еще не распознан
                    Analyser.Files.Add(f);
                else
                    f.unrecognized = false;
            }

            //Читаем функции
            Store.Functions functions = storage.functions;            
            Dictionary<UInt64, Function> tabFunc = new Dictionary<ulong, Function>();
            foreach (Store.IFunction function in functions.EnumerateFunctions(enFunctionKind.REAL))
            {
                if (function.IsExternal)
                    continue;
                Store.IFile file = null;
                Store.Location def = function.Definition();
                if (def != null)
                    file = def.GetFile();

                Function func;
                if (file != null)
                {
                    // избыточность смотрим только для функций, имеющих определение
                    func = new Function(table[file.Id], function);

                    tabFunc.Add(function.Id, func);

                    if (func.Essentiality == Store.enFunctionEssentials.UNKNOWN || !skip) //Если функция уже распознана
                        Analyser.Functions.Add(func);
                }
            }

            //Создаем массив вызовов
            foreach (Function function in Analyser.Functions)
            {
                foreach (IFunctionCall func_c in function.function.Calles())
                {
                    if (func_c.Calles.IsExternal)
                        continue;
                    if (tabFunc.ContainsKey(func_c.Calles.Id))
                    {
                        // избыточность смотрим только для функций, имеющих определение
                        Function called = tabFunc[func_c.Calles.Id];

                        if (called.Essentiality == enFunctionEssentials.UNKNOWN || !skip)
                        {
                            function.Add_CallsDirectly(called);
                            called.Add_CalledByDirectly(function);
                        }
                    }
                }
                foreach (PointsToFunction ptf in function.function.OperatorsGetPointerToFunction())
                {
                    if (tabFunc.ContainsKey(ptf.function))
                    {
                        // избыточность смотрим только для функций, имеющих определение
                        Function called = tabFunc[ptf.function];

                        if (called.Essentiality == enFunctionEssentials.UNKNOWN || !skip)
                        {
                            function.Add_CallsDirectly(called);
                            called.Add_CalledByDirectly(function);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Используется в Enumerate для отсеивания функций, не попадающих под критерий.
        /// </summary>
        /// <param name="lables"></param>
        /// <returns></returns>
        delegate bool Comparer(Function lables);

        /// <summary>
        /// Перечисляет функции, удоблетворяющие функциональному критерию.
        /// </summary>
        /// <param name="comparer"></param>
        /// <returns></returns>
        private static IEnumerable<Function> Enumerate(Comparer comparer)
        {
            foreach (Function function in Analyser.Functions)
                if (comparer(function))
                    yield return function;
        }

        /// <summary>
        /// Перечислить все существенные функции.
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<Function> EnumerateEssentialFunctions()
        {
            return Enumerate(a => a.function.Essential == Store.enFunctionEssentials.ESSENTIAL);
        }

        /// <summary>
        /// Перечислить все избыточные функции
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<Function> EnumerateRedundantFunctions()
        {
            return Enumerate(a => a.function.Essential == Store.enFunctionEssentials.REDUNDANT);
        }

        /// <summary>
        /// Перечислить все функции, для которых избыточность не определялась
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<Function> EnumerateUnknownFunctions()
        {
            return Enumerate(a => (a.function.Essential == Store.enFunctionEssentials.UNKNOWN && a.DefFile != null && a.DefFile.FullFileName != ""));
        }

        /// <summary>
        /// Перечислить все функции, названные неизбыточными пользовалелем.
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<Function> EnumerateAcceptedByUser()
        {
            return Enumerate(a => a.function.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
        }

        /// <summary>
        /// Перечислить все функции, названные избыточными пользовалелем.
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<Function> EnumerateRejectedByUser()
        {
            return Enumerate(a => a.function.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT);
        }

        /// <summary>
        /// Перечислить все функции, названные неизбыточными при анализе трасс.
        /// </summary>
        /// <returns></returns>
        internal static IEnumerable<Function> EnumerateAcceptedByTraceChecker()
        {
            return Enumerate(a =>
                a.function.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_CALLED ||
                a.function.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_NO_CALLES ||
                a.function.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_PROGRAM_ENTRY_WITH_CALLES
                );
        }


        /// <summary>
        /// Стирает из базы все результаты предыдущих автоматических анализов.
        /// Все наработки пользователя сохраняются.
        /// </summary>
        internal static void ClearResultsExeptUser()
        {
            Store.Functions functions = storage.functions;

            foreach (Store.IFunction func in functions.EnumerateFunctions(enFunctionKind.REAL))
                if (func.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER ||
                    func.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.TRACE_CHECKER_AUTO ||
                    func.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_AFTER_DFM ||
                    func.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_DFM ||
                    func.EssentialMarkedType == Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_FULL_AUTO_ONE_ENTRY_SEARCH
                    )
                {
                    func.Essential = Store.enFunctionEssentials.UNKNOWN;
                    func.EssentialMarkedType = Store.enFunctionEssentialMarkedTypes.NOTHING;
                }
        }
    }




}
