using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using Store;
using Store.Table;

namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Избыточность по функциям";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.MANUAL_FUNCTION_REDUNDANCY;

        #region Settings

        /// <summary>
        /// Сделать неизбыточными все функции, которые могут быть вызваны из функций, встретившихся в трассах?
        /// </summary>
        internal static bool TCE;

        /// <summary>
        /// Протокол нажатий пользователя
        /// </summary>
        internal static string UserProtocolPath;

        #endregion
    }
    
    public class ManualFunctionRedundancy : IA.Plugin.Interface
    {
        #region ManualFunctionRedundancy fields

        Storage storage;
        MainControl mainControl;
        Settings settings;

        #endregion

        #region PluginInterface Members

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        private bool Auto = false;

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW |
                                Plugin.Capabilities.RESULT_WINDOW |
                                    Plugin.Capabilities.REPORTS |
                                        Plugin.Capabilities.RERUN;
            }
        }
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.TCE = Properties.Settings.Default.TCE;
            PluginSettings.UserProtocolPath = Properties.Settings.Default.UserProtocolPath;

            IADatabase.Initialize(storage);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.TCE);
            PluginSettings.UserProtocolPath = settingsBuffer.GetString();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.TCE = true;
            PluginSettings.UserProtocolPath = string.Empty;

            Auto = true;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return null;
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (settings = new Settings());
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                Analyser.WorkSequence.Clear();
                try
                {
                    //чтение из хранилища нераспознанных функций
                    IADatabase.Read(true);

                    //Заполняем перечень ниоткуда не вызываемых функций
                    Analyser.neverCalledFunctions.Clear();
                    Analyser.FillNoCallsFunctions();
                }
                catch (Exception ex)
                {
                    IA.Monitor.Log.Error(PluginSettings.Name, ex.Message);
                }

                return (mainControl = new MainControl(this));
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.TCE = PluginSettings.TCE;
            Properties.Settings.Default.UserProtocolPath = PluginSettings.UserProtocolPath;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.TCE);
            settingsBuffer.Add(PluginSettings.UserProtocolPath);
        }
        
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            if (!String.IsNullOrWhiteSpace(PluginSettings.UserProtocolPath) && !System.IO.File.Exists(PluginSettings.UserProtocolPath))
            {
                message = "Протокол нажатий пользователя не найден";
                return false;
            }

            return true;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            try
            {
                //Удаляем из базы все результаты, за исключением протокола нажатий эксперта.
                IADatabase.ClearResultsExeptUser();

                //чтение из хранилища ВСЕХ функций, включая распознанные!
                IADatabase.Read(false);

                if (PluginSettings.UserProtocolPath != String.Empty)
                {
                    LoadAndExecuteUserProtocol(PluginSettings.UserProtocolPath); // загружаем по возможности старый протокол
                }
                
                // выполняем анализ DFM 
                AnalyseDFM.Run(IADatabase.storage.files, Analyser.Functions);

                if (PluginSettings.TCE)
                    AcceptChildrenAfterTraceCheck();

                // очищаем последовательность нажатий клавиш пользователем
                Analyser.WorkSequence.Clear();

                //выполнение с функциями помеченными неизбыточными при анализе трасс
                List<Function> list = new List<Function>();
                foreach (Function func in IADatabase.EnumerateAcceptedByTraceChecker())
                    list.Add(func);
                foreach (Function func in list)
                {
                    if (func.CalledByDirectly.Count == 0 && func.EssentialMarkedType != enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT && func.EssentialMarkedType != enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER)
                    {
                        func.EssentialMarkedType = enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT;
                        Analyser.Accept(func, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT/*TRACE_CHECKER_AUTO*/);
                    }
                }
                foreach (Function func in list)
                {
                    if (func.EssentialMarkedType != enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT && func.EssentialMarkedType != enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_AUTO_AFTER_USER)
                    {
                        func.EssentialMarkedType = enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT;
                        Analyser.Accept(func, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT/*TRACE_CHECKER_AUTO*/);
                    }
                }

                //выполнение с функциями из хранилища всех дейстий из прошлого запуска
                //Сначала существенные функции
                list = new List<Function>();
                foreach (Function func in IADatabase.EnumerateAcceptedByUser())
                    list.Add(func);
                foreach (Function func in list)
                    Analyser.Accept(func, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);

                //Теперь несущественные
                list = new List<Function>();
                foreach (Function func in IADatabase.EnumerateRejectedByUser())
                    list.Add(func);
                foreach (Function func in list)
                    Analyser.Reject(func, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT);

                // отдельно обработаем Глобальное окружение
                foreach (Function func in Analyser.Functions)
                {
                    if (func.Name == "Глобальное окружение")
                    {
                        Analyser.Accept(func, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                IA.Monitor.Log.Error(PluginSettings.Name, ex.Message);
            }

            Analyser.Clear();

            if (Auto)
            {
                UserControl frm = ResultWindow;
                ((MainControl)frm).button1_Click(null, null);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            // Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");

            IADatabase.Read(false);
            Log.GenerateReports(reportsDirectoryPath, storage);
            Analyser.Clear();
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }
        
        #endregion

        #region Other Code

        private void LoadAndExecuteUserProtocol(string Path)
        {
            if (System.IO.File.Exists(Path))
            {
                IADatabase.Read(false);

                StreamReader reader = new StreamReader(Path);
                reader.ReadLine(); // пропустить фразу "Входы в программу: "

                string buffer;
                string fileName;
                string functionName;
                int pathStart;
                List<string> Funcs = new List<string>();
                while ((buffer = reader.ReadLine()) != "Избыточные функции: ")
                {
                    if ((pathStart = buffer.IndexOf(" определена в файле ")) == -1)
                    {
                        IA.Monitor.Log.Error(PluginSettings.Name, "Файл с протоколом работы пользователя некорректен");
                        return;
                    }
                    functionName = buffer.Substring(9, pathStart - 9);
                    fileName = buffer.Substring(pathStart + 20);

                    if (!fileName.StartsWith("\\"))
                    {
                        fileName = "\\" + fileName;
                    }
                    Funcs.Add(functionName + " " + fileName);
                }

                List<Function> toAccept = new List<Function>();
                foreach (Function func in Analyser.Functions)
                {
                    if (func.DefFile != null && (!func.DefFile.FileNameForReports.StartsWith("\\") && Funcs.Contains(func.Name + " \\" + func.DefFile.FileNameForReports + " строка " + func.defPosition.line.ToString()) ||
                         func.DefFile.FileNameForReports.StartsWith("\\") && Funcs.Contains(func.Name + " " + func.DefFile.FileNameForReports + " строка " + func.defPosition.line.ToString())))
                    {
                        toAccept.Add(func);
                    }
                }
                foreach (Function func in toAccept)
                {
                    Analyser.Accept(func, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                }
                toAccept.Clear();
                Funcs.Clear();

                while ((buffer = reader.ReadLine()) != null)
                {
                    if(buffer == "")
                    {
                        break;
                    }
                    if ((pathStart = buffer.IndexOf(" определена в файле ")) == -1)
                    {
                        IA.Monitor.Log.Error(PluginSettings.Name, "Файл с протоколом работы пользователя некорректен");
                        return;
                    }
                    functionName = buffer.Substring(9, pathStart - 9);
                    fileName = buffer.Substring(pathStart + 20);


                    if (!fileName.StartsWith("\\"))
                    {
                        fileName = "\\" + fileName;
                    } 
                    Funcs.Add(functionName + " " + fileName);
                }


                List<Function> toReject = new List<Function>();
                foreach (Function func in Analyser.Functions)
                {
                    if (func.DefFile != null && (!func.DefFile.FileNameForReports.StartsWith("\\") && Funcs.Contains(func.Name + " \\" + func.DefFile.FileNameForReports + " строка " + func.defPosition.line.ToString()) ||
                         func.DefFile.FileNameForReports.StartsWith("\\") && Funcs.Contains(func.Name + " " + func.DefFile.FileNameForReports + " строка " + func.defPosition.line.ToString())))
                    {
                        toReject.Add(func);
                    }
                }
                foreach (Function func in toReject)
                {
                    Analyser.Reject(func, enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_REJECT);
                }
                toReject.Clear();
                Funcs.Clear();
                reader.Close();
            }
        }

        private void AcceptChildrenAfterTraceCheck()
        {
            List<Function> list = new List<Function>();
            foreach (Function func in IADatabase.EnumerateEssentialFunctions())
                list.Add(func);

            List<Function> newList = new List<Function>();
            newList.AddRange(list);
            do
            {
                list = new List<Function>();
                list.AddRange(newList);
                foreach (Function fun in list)
                    foreach (Function fcl in fun.CallsDirectly)
                        if (!newList.Contains(fcl))
                            newList.Add(fcl);
            } while (newList.Count != list.Count);
            
            foreach (Function func in list)
            {
                func.EssentialMarkedType = enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT;
                Analyser.Accept(func, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT/*TRACE_CHECKER_AUTO*/);
            }
        }               

        public void Test_AcceptFunction(string name)
        {
            //чтение из хранилища ВСЕХ функций
            IADatabase.Read(false);

            foreach(Function func in IADatabase.EnumerateUnknownFunctions())
                if (func.function.FullName == name)
                {
                    Analyser.Accept(func, Store.enFunctionEssentialMarkedTypes.MANUAL_REDUNDANCY_USER_ACCEPT);
                    break;
                }

            Analyser.Clear();
        }

        #endregion
    }
}
