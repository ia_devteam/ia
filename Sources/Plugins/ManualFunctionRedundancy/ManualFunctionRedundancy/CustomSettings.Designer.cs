namespace IA.Plugins.Analyses.ManualFunctionRedundancy
{
    partial class Settings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.settings_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.userProtocolPath_groupBox = new System.Windows.Forms.GroupBox();
            this.traceCallesEssential_groupBox = new System.Windows.Forms.GroupBox();
            this.traceCallesEssential_checkBox = new System.Windows.Forms.CheckBox();
            this.userProtocolPath_textBox = new IA.Controls.TextBoxWithDialogButton();
            this.settings_tableLayoutPanel.SuspendLayout();
            this.userProtocolPath_groupBox.SuspendLayout();
            this.traceCallesEssential_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // settings_tableLayoutPanel
            // 
            this.settings_tableLayoutPanel.ColumnCount = 1;
            this.settings_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.Controls.Add(this.userProtocolPath_groupBox, 0, 0);
            this.settings_tableLayoutPanel.Controls.Add(this.traceCallesEssential_groupBox, 0, 1);
            this.settings_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.settings_tableLayoutPanel.Name = "settings_tableLayoutPanel";
            this.settings_tableLayoutPanel.RowCount = 3;
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settings_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settings_tableLayoutPanel.Size = new System.Drawing.Size(589, 104);
            this.settings_tableLayoutPanel.TabIndex = 3;
            // 
            // userProtocolPath_groupBox
            // 
            this.userProtocolPath_groupBox.Controls.Add(this.userProtocolPath_textBox);
            this.userProtocolPath_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userProtocolPath_groupBox.Location = new System.Drawing.Point(3, 3);
            this.userProtocolPath_groupBox.Name = "userProtocolPath_groupBox";
            this.userProtocolPath_groupBox.Size = new System.Drawing.Size(583, 49);
            this.userProtocolPath_groupBox.TabIndex = 3;
            this.userProtocolPath_groupBox.TabStop = false;
            this.userProtocolPath_groupBox.Tag = "";
            this.userProtocolPath_groupBox.Text = "��������� �������� ������� ������������";
            // 
            // traceCallesEssential_groupBox
            // 
            this.traceCallesEssential_groupBox.Controls.Add(this.traceCallesEssential_checkBox);
            this.traceCallesEssential_groupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.traceCallesEssential_groupBox.Location = new System.Drawing.Point(3, 58);
            this.traceCallesEssential_groupBox.Name = "traceCallesEssential_groupBox";
            this.traceCallesEssential_groupBox.Size = new System.Drawing.Size(583, 39);
            this.traceCallesEssential_groupBox.TabIndex = 6;
            this.traceCallesEssential_groupBox.TabStop = false;
            this.traceCallesEssential_groupBox.Text = "�������������� �����:";
            // 
            // traceCallesEssential_checkBox
            // 
            this.traceCallesEssential_checkBox.AutoSize = true;
            this.traceCallesEssential_checkBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.traceCallesEssential_checkBox.Location = new System.Drawing.Point(3, 16);
            this.traceCallesEssential_checkBox.Name = "traceCallesEssential_checkBox";
            this.traceCallesEssential_checkBox.Size = new System.Drawing.Size(577, 20);
            this.traceCallesEssential_checkBox.TabIndex = 0;
            this.traceCallesEssential_checkBox.Text = "������� ������������� ��� �������, ������� ����� ���� ������� �� �������, �������" +
    "������ � �������";
            this.traceCallesEssential_checkBox.UseVisualStyleBackColor = true;
            // 
            // userProtocolPath_textBox
            // 
            this.userProtocolPath_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userProtocolPath_textBox.Location = new System.Drawing.Point(3, 16);
            this.userProtocolPath_textBox.MaximumSize = new System.Drawing.Size(0, 25);
            this.userProtocolPath_textBox.MinimumSize = new System.Drawing.Size(400, 25);
            this.userProtocolPath_textBox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.userProtocolPath_textBox.Name = "userProtocolPath_textBox";
            this.userProtocolPath_textBox.Size = new System.Drawing.Size(577, 25);
            this.userProtocolPath_textBox.TabIndex = 0;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.settings_tableLayoutPanel);
            this.Name = "Settings";
            this.Size = new System.Drawing.Size(589, 104);
            this.settings_tableLayoutPanel.ResumeLayout(false);
            this.userProtocolPath_groupBox.ResumeLayout(false);
            this.traceCallesEssential_groupBox.ResumeLayout(false);
            this.traceCallesEssential_groupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel settings_tableLayoutPanel;
        private System.Windows.Forms.GroupBox userProtocolPath_groupBox;
        private System.Windows.Forms.GroupBox traceCallesEssential_groupBox;
        private System.Windows.Forms.CheckBox traceCallesEssential_checkBox;
        private Controls.TextBoxWithDialogButton userProtocolPath_textBox;

    }
}
