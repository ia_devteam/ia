﻿using IA.Plugins.Analyses.FilesEssential_FileMonitors;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TestUtils;

namespace TestFilesEssential_FileMonitors
{
    [TestClass]
    public class TestFilesEssential_FileMonitors
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        //private const string sourcePostfixPart = @"Sources\JavaScript\";
        private const string TemplatedProtocolPath = @"FileEssential_FileMonitors\protocol_template.csv";

        private const UInt64 pluginID = Store.Const.PluginIdentifiers.FILES_ESSENTIAL_FILE_MONITORS;

        private static BasicMonitorListener listener;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Инициализация общетестового окружения
        /// </summary>
        [ClassInitialize()]
        public static void OverallTest_Initialize(TestContext dummy)
        {
            listener = new BasicMonitorListener();
            IA.Monitor.Log.Register(listener);
        }

        /// <summary>
        /// Обнуление того, что надо обнулить
        /// </summary>
        [ClassCleanup()]
        public static void OverallTest_Cleanup()
        {
            IA.Monitor.Log.Unregister(listener);
            listener = null;
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            listener.Clear();
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }



        sealed class RunTest_FilesEssential_FileMonitors_GoodSituation_ProcessMonitor : TestUtilsClass.RunTestConfig
        {

            public override IA.Plugin.Interface Stage02Plugin
            {
                get { return new FilesEssential_FileMonitors(); }
            }

            public override string Stage01SourcesPostfix
            {
                get
                {
                    return @"FileEssential_FileMonitors\Sources\JavaScript";
                }
            }

            public override void Stage11PrepareStorageDelegate(Store.Storage storage, string testMaterialsDirectoryPath)
            {
                TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsDirectoryPath, Stage01SourcesPostfix));

                string localProtocolPath = Path.Combine(testMaterialsDirectoryPath, TemplatedProtocolPath);
                if (!System.IO.File.Exists(localProtocolPath))
                    throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Файл с тестовым протоколом не найден. Убедитесь в наличии файла " + localProtocolPath);

                string protocolWithReplace = System.IO.File.ReadAllText(localProtocolPath).Replace(TestUtils.TestUtilsClass.PathConstant_old, testMaterialsDirectoryPath + @"\FileEssential_FileMonitors");
                string localProtocolWithTruePaths = Path.Combine(storage.GetTempDirectory(pluginID).Path, "protocol.csv");
                try
                {
                    System.IO.File.WriteAllText(localProtocolWithTruePaths, protocolWithReplace);
                }
                catch (IOException ex)
                {
                    throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Не удалось записать файл " + localProtocolWithTruePaths + ".", ex);
                }

                Protocol testProtocol = new Protocol()
                {
                    Path = localProtocolWithTruePaths,
                    Type = Protocol.enType.PROCESS_MONITOR
                };

                Protocol.Pair prefixPair = new Protocol.Pair()
                {
                    Prefix = Path.Combine(testMaterialsDirectoryPath, Stage01SourcesPostfix),
                    Subway = String.Empty
                };

                testProtocol.Prefixes.Add(prefixPair);
                Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();
                buffer.Add((int)1);
                buffer.Add(testProtocol.ToConfigureString);
                storage.pluginSettings.SaveSettings(pluginID, buffer);
            }

            public override bool Stage12CheckStorageDelegate(Store.Storage storage, string testMaterialsDirectoryPath)
            {
                string[] goodFiles = { "activex.js", "dinocubj.js", "gerdigj.js" };
                string[] badFiles = { "jquery-1.11.0.js", "pyraminj.js", "roundyj.js" };

                CheckForRightMarkup(storage, goodFiles, badFiles);

                return true;
            }

            public override bool Stage13CheckReportsAfterRunDelegate(string reportsDirectoryPath, string testMaterialsDirectoryPath)
            {
                return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsDirectoryPath, @"FileEssential_FileMonitors\Reports\GoodSituations"), reportsDirectoryPath);
            }
        }

        /// <summary>
        /// Тест проверяет работу плагина при подаче ему одного лога Process Monitor с настроенной 1 парой префикс-постфикс. Базовый функционал.
        /// </summary>
        [TestMethod]
        public void FilesEssential_FileMonitors_GoodSituation_ProcessMonitor()
        {
            testUtils.RunTest<RunTest_FilesEssential_FileMonitors_GoodSituation_ProcessMonitor>();
        }
        /// <summary>
        /// Тест проверяет работу плагина при подаче ему одного лога Unix с настроенным префиксом.
        /// </summary>
        [TestMethod]
        public void FilesEssential_FileMonitors_GoodSituation_Unix()
        {
            string sourcePrefixPath = @"FileEssential_FileMonitors\Sources";
            /********************************************************/
            //сложная ситуация
            //тест хорошего протокола Unix с несколькими префиксами и постфиксами.
            testUtils.RunTest(
                //sourcesPostfix
                sourcePrefixPath,

                //plugin
                new FilesEssential_FileMonitors(),

                //isUseCachedStorage
                false,

                //prepareStorage
                null,

                //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePrefixPath));
                    //unix good with 3 prefixes and 2 postfixes
                    string localProtocolPath = Path.Combine(testMaterialsPath, TemplatedProtocolPath.Replace("protocol_template.csv", "unix_template.log"));
                    if (!System.IO.File.Exists(localProtocolPath))
                        throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Файл с тестовым протоколом не найден. Убедитесь в наличии файла " + localProtocolPath);

                    string tempDir = storage.GetTempDirectory(pluginID).Path;

                    string protocolWithReplace = System.IO.File.ReadAllText(localProtocolPath).Replace(TestUtils.TestUtilsClass.PathConstant_old, testMaterialsPath + @"\FileEssential_FileMonitors");
                    string processGood = Path.Combine(tempDir, "unix.log");// localProtocolPath.Replace("unix_template.log", "unix.log");
                    try
                    {
                        System.IO.File.WriteAllText(processGood, protocolWithReplace);
                    }
                    catch (IOException ex)
                    {
                        throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Не удалось записать файл " + processGood + ".", ex);
                    }

                    Protocol testProtocol = new Protocol()
                    {
                        Path = processGood,
                        Type = Protocol.enType.UNIX
                    };
                    {
                        Protocol.Pair prefixPair = new Protocol.Pair()
                        {
                            Prefix = Path.Combine(testMaterialsPath, sourcePrefixPath),
                            Subway = String.Empty
                        };

                        testProtocol.Prefixes.Add(prefixPair);
                    }

                    Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();
                    buffer.Add((int)1);
                    buffer.Add(testProtocol.ToConfigureString);
                    storage.pluginSettings.SaveSettings(pluginID, buffer);
                },

                //checkStorage
                (storage, testMaterialsPath) =>
                {
                    string[] goodFiles = 
                    { 
                        "dummy1readed.cpp", 
                        "Binaries/libxml2.dll",
                        "CSharp/FindStringsInFiles/FindStringsInFiles.sln",
                        "JavaScript/activex.js",
                        "CSharpBuilt/FindStringsInFiles/FindStringsInFiles.sln",
                        "CSharpBuilt/FindStringsInFiles/FindStringsInFiles/FindStringsInFiles.csproj"
                    };
                    string[] badFiles = 
                    { 
                        "dummy2created.cpp", 
                        "dummy2deleted.cpp", 
                        "CSharp/FindStringsInFiles/FindStringsInFiles/App.config",
                        "JavaScript/dinocubj.js",
                        "JavaScript/gerdigj.js",
                        "JavaScript/jquery-1.11.0.js",
                        "CSharpBuilt/FindStringsInFiles/FindStringsInFiles/App.config",
                        "CSharpBuilt/FindStringsInFiles/FindStringsInFiles/Program.cs"
                    };

                    CheckForRightMarkup(storage, goodFiles, badFiles);

                    var unknownFiles = storage.files
                        .EnumerateFiles(Store.enFileKind.anyFile)
                        .Where(f1 => !goodFiles.Any(f2 => f1.RelativeFileName_Original != f2)
                                    && badFiles.Any(f2 => f1.RelativeFileName_Original != f2)
                        ).ToList();

                    Assert.IsTrue(unknownFiles.All(f => f.FileEssential == Store.enFileEssential.UNKNOWN),
                        "Файл не упоминаемый в отчётах не помечен как неизвестный по избыточности.");

                    return true;
                },

                //checkreports
                (reportsPath, testMaterialsPath) =>
                {
                    //Сравниваем директории с отчётами
                    return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"FileEssential_FileMonitors\Reports\GoodSituationsUnix"), reportsPath);
                },

                //runTwice
                false, null, null, null
                );
        }

        private sealed class RunTest_FilesEssential_FileMonitors_NoChanges : TestUtilsClass.RunTestConfig
        {
            string sourcePrefixPart = @"FileEssential_FileMonitors\Sources\Binaries";
            public override string Stage01SourcesPostfix
            {
                get
                {
                    return sourcePrefixPart;
                }
            }

            public override void Stage11PrepareStorageDelegate(Store.Storage storage, string testMaterialsDirectoryPath)
            {
                TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsDirectoryPath, sourcePrefixPart));

                string localProtocolPath = Path.Combine(testMaterialsDirectoryPath, TemplatedProtocolPath);
                if (!System.IO.File.Exists(localProtocolPath))
                    throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Файл с тестовым протоколом не найден. Убедитесь в наличии файла " + localProtocolPath);

                string protocolWithReplace = System.IO.File.ReadAllText(localProtocolPath).Replace(TestUtils.TestUtilsClass.PathConstant_old, Path.Combine(testMaterialsDirectoryPath, sourcePrefixPart));
                string localProtocolWithTruePaths = Path.Combine(storage.GetTempDirectory(pluginID).Path, "protocol.csv");
                try
                {
                    System.IO.File.WriteAllText(localProtocolWithTruePaths, protocolWithReplace);
                }
                catch (IOException ex)
                {
                    throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Не удалось записать файл " + localProtocolWithTruePaths + ".", ex);
                }

                Protocol testProtocol = new Protocol()
                {
                    Path = localProtocolWithTruePaths,
                    Type = Protocol.enType.PROCESS_MONITOR
                };

                Protocol.Pair prefixPair = new Protocol.Pair()
                {
                    Prefix = Path.Combine(testMaterialsDirectoryPath, sourcePrefixPart),
                    Subway = String.Empty
                };

                testProtocol.Prefixes.Add(prefixPair);
                Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();
                buffer.Add((int)1);
                buffer.Add(testProtocol.ToConfigureString);
                storage.pluginSettings.SaveSettings(pluginID, buffer);
            }

            public override bool Stage12CheckStorageDelegate(Store.Storage storage, string testMaterialsDirectoryPath)
            {
                Assert.IsTrue(storage.files.EnumerateFiles(Store.enFileKind.anyFile).All(f => f.FileEssential == Store.enFileEssential.UNKNOWN),
                        "Файлы получили статус, отличный от <Неизвестный тип значимости>!");
                return true;
            }

            public override bool Stage13CheckReportsAfterRunDelegate(string reportsDirectoryPath, string testMaterialsDirectoryPath)
            {
                //Сравниваем директории с отчётами
                Assert.IsTrue(TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsDirectoryPath, @"FileEssential_FileMonitors\Reports\NoChanges"), reportsDirectoryPath), "Отчёты не совпадают с образцами!");
                Assert.IsTrue(listener.ContainsPart("По результатам анализа всех протоколов небыло найдено ни одного соответствия между файлами протоколов и файлами в Хранилище. Возможно плагин настроен неверно."), "В лог не было выдано сообщение \"Возможно плагин настроен неверно\"");
                return true;
            }
        }
        /// <summary>
        /// Тест проверяет выдачу сообщения в случае, когда после отработки никаких изменений в статусы файлов не внесено.
        /// </summary>
        [TestMethod]
        public void FilesEssential_FileMonitors_NoChanges()
        {
            testUtils.RunTest<FilesEssential_FileMonitors, RunTest_FilesEssential_FileMonitors_NoChanges>();
        }

        /// <summary>
        /// Тест проверяет работу плагина при подаче ему на вход двух логов разного происхождения.
        /// </summary>
        [TestMethod]
        public void FilesEssential_FileMonitors_TwoProtocols()
        {
            string sourcePostfixPath = @"FileEssential_FileMonitors\Sources";

            testUtils.RunTest(
                //sourcesPostfix
            sourcePostfixPath,

            //plugin
            new FilesEssential_FileMonitors(),

            //isUseCachedStorage
            false,

            //prepareStorage
            null,

            //customizeStorage
            (storage, testMaterialsPath) =>
            {
                TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPath));

                #region procmon
                string localProtocolPath = Path.Combine(testMaterialsPath, TemplatedProtocolPath);
                if (!System.IO.File.Exists(localProtocolPath))
                    throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Файл с тестовым протоколом не найден. Убедитесь в наличии файла " + localProtocolPath);

                string protocolWithReplace = System.IO.File.ReadAllText(localProtocolPath).Replace(TestUtils.TestUtilsClass.PathConstant_old, testMaterialsPath + @"\FileEssential_FileMonitors");
                string localProtocolWithTruePaths = Path.Combine(storage.GetTempDirectory(pluginID).Path, "protocol.csv");
                try
                {
                    System.IO.File.WriteAllText(localProtocolWithTruePaths, protocolWithReplace);
                }
                catch (IOException ex)
                {
                    throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Не удалось записать файл " + localProtocolWithTruePaths + ".", ex);
                }

                Protocol ProcMonProtocol = new Protocol()
                {
                    Path = localProtocolWithTruePaths,
                    Type = Protocol.enType.PROCESS_MONITOR
                };

                Protocol.Pair prefixPair = new Protocol.Pair()
                {
                    Prefix = Path.Combine(testMaterialsPath, sourcePostfixPath),
                    Subway = String.Empty
                };

                ProcMonProtocol.Prefixes.Add(prefixPair);
                #endregion

                #region unixmon
                //unix good with 3 prefixes and 2 postfixes
                localProtocolPath = Path.Combine(testMaterialsPath, TemplatedProtocolPath.Replace("protocol_template.csv", "unix_template.log"));
                if (!System.IO.File.Exists(localProtocolPath))
                    throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Файл с тестовым протоколом не найден. Убедитесь в наличии файла " + localProtocolPath);

                protocolWithReplace = System.IO.File.ReadAllText(localProtocolPath).Replace(TestUtils.TestUtilsClass.PathConstant_old, testMaterialsPath + @"\FileEssential_FileMonitors");
                string processGood = Path.Combine(storage.GetTempDirectory(pluginID).Path, "unix.log");
                try
                {
                    System.IO.File.WriteAllText(processGood, protocolWithReplace);
                }
                catch (IOException ex)
                {
                    throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Не удалось записать файл " + processGood + ".", ex);
                }

                Protocol UnixMonProtocol = new Protocol()
                {
                    Path = processGood,
                    Type = Protocol.enType.UNIX
                };
                {
                    prefixPair = new Protocol.Pair()
                    {
                        Prefix = Path.Combine(testMaterialsPath, sourcePostfixPath),
                        Subway = String.Empty
                    };

                    UnixMonProtocol.Prefixes.Add(prefixPair);

                    prefixPair = new Protocol.Pair()
                    {
                        Prefix = Path.Combine(testMaterialsPath, sourcePostfixPath + @"\JavaScript"),
                        Subway = String.Empty
                    };

                    UnixMonProtocol.Prefixes.Add(prefixPair);

                    prefixPair = new Protocol.Pair()
                    {
                        Prefix = Path.Combine(testMaterialsPath, sourcePostfixPath + @"\CSharpBuilt"),
                        Subway = String.Empty
                    };

                    UnixMonProtocol.Prefixes.Add(prefixPair);
                }
                #endregion

                Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();
                buffer.Add((int)2);
                buffer.Add(ProcMonProtocol.ToConfigureString);
                buffer.Add(UnixMonProtocol.ToConfigureString);
                storage.pluginSettings.SaveSettings(pluginID, buffer);
            },

            //checkStorage
            (storage, testMaterialsPath) =>
            {
                string[] goodFiles = 
                    {
                        "dummy1readed.cpp", 
                        "Binaries/libxml2.dll",
                        "CSharp/FindStringsInFiles/FindStringsInFiles.sln",
                        "JavaScript/activex.js",
                        "CSharpBuilt/FindStringsInFiles/FindStringsInFiles.sln",
                        "CSharpBuilt/FindStringsInFiles/FindStringsInFiles/FindStringsInFiles.csproj"
                    };
                string[] badFiles = 
                    {   
                        "dummy2created.cpp", 
                        "dummy2deleted.cpp", 
                        "CSharp/FindStringsInFiles/FindStringsInFiles/App.config",
                        "JavaScript/pyraminj.js", 
                        "JavaScript/roundyj.js",
                        "JavaScript/dinocubj.js",                        
                        "JavaScript/jquery-1.11.0.js",
                        "JavaScript/gerdigj.js",
                        "CSharpBuilt/FindStringsInFiles/FindStringsInFiles/App.config",
                        "CSharpBuilt/FindStringsInFiles/FindStringsInFiles/Program.cs"
                    };

                CheckForRightMarkup(storage, goodFiles, badFiles);

                return true;
            },

            //checkreports
            (reportsPath, testMaterialsPath) =>
            {
                return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"FileEssential_FileMonitors\Reports\TwoProtocols"), reportsPath);
            },

            //runTwice
            false, null, null, null
            );
        }

        /// <summary>
        /// Тест проверяет выдачу предупреждения плагином при подаче ему битого лога Process Monitor.
        /// </summary>
        [TestMethod]
        public void FilesEssential_FileMonitors_ProcessMonitorLogTruncated()
        {
            string sourcePostfixPath = @"FileEssential_FileMonitors\Sources\JavaScript";
            /******************************************/
            //простая ситуация
            //тест хорошего протокола ProcessMonitor с одним префиксом.
            testUtils.RunTest(
                //sourcesPostfix
                sourcePostfixPath,

                //plugin
                new FilesEssential_FileMonitors(),

                //isUseCachedStorage
                false,

                //prepareStorage
                null,

                //customizeStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtils.TestUtilsClass.Run_FillFileList(storage, Path.Combine(testMaterialsPath, sourcePostfixPath));

                    string localProtocolPath = Path.Combine(testMaterialsPath, TemplatedProtocolPath);
                    if (!System.IO.File.Exists(localProtocolPath))
                        throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Файл с тестовым протоколом не найден. Убедитесь в наличии файла " + localProtocolPath);

                    string protocolWithReplace = System.IO.File.ReadAllText(localProtocolPath).Substring(0, 130);
                    string localProtocolWithTruePaths = Path.Combine(storage.GetTempDirectory(pluginID).Path, "protocol.csv");
                    try
                    {
                        System.IO.File.WriteAllText(localProtocolWithTruePaths, protocolWithReplace);
                    }
                    catch (IOException ex)
                    {
                        throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Не удалось записать файл " + localProtocolWithTruePaths + ".", ex);
                    }

                    Protocol testProtocol = new Protocol()
                    {
                        Path = localProtocolWithTruePaths,
                        Type = Protocol.enType.PROCESS_MONITOR

                    };

                    Protocol.Pair prefixPair = new Protocol.Pair()
                    {
                        Prefix = Path.Combine(testMaterialsPath, sourcePostfixPath),
                        Subway = String.Empty
                    };

                    testProtocol.Prefixes.Add(prefixPair);

                    Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();
                    buffer.Add((int)1);
                    buffer.Add(testProtocol.ToConfigureString);
                    storage.pluginSettings.SaveSettings(pluginID, buffer);
                },

                //checkStorage
                (storage, testMaterialsPath) => { return true; },

                //checkreports
                (reportsPath, testMaterialsPath) =>
                {
                    Assert.IsTrue(TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"FileEssential_FileMonitors\Reports\LogTruncated"), reportsPath));
                    Assert.IsTrue(listener.ContainsPart("Строка имеет неверный формат (менее 7 полей разделённых запятыми)"));
                    return true;
                },

                //runTwice
                false, null, null, null
                );
        }
        /// <summary>
        /// Тест вывода ошибок в различных "плохих" ситуациях. Внутри последовательно несколько запусков.
        /// </summary>
        [TestMethod]
        public void FilesEssential_FileMonitors_BadSituations()
        {
            string sourcePostfixPath = @"FileEssential_FileMonitors\Sources\JavaScript";
            //Проверяется поведение в ситуации инициализация + старт, без настройки. 
            //Такая проверка показала себя бессмысленной, так как винда кеширует настройки и результат такого выполнения будет зависеть от наличия и содержания предыдущего теста плагина...
            //            
            //            testUtils.RunTest(sourcePostfixPart, new FilesEssential_FileMonitors(), false, null,
            //                //customize
            //    (storage, testMaterialsPath) =>
            //    {
            //        Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();
            //        buffer.Add((int)0);
            //        storage.pluginSettings.SaveSettings(pluginID, buffer);
            //    },
            //                //checkstorage
            //    (storage, testMaterialsPath) => { return true; },
            //                //checkreports
            //    (reportsFullPath, testMaterialsPath) => { return true; },
            //                //runTwice
            //    false, null, null, null);
            //            Assert.IsTrue(listener.ContainsPart("Не указан ни один протокол файловых мониторов") /*||
            //                          listener.ContainsPart("Настройки протокола некорректны (Протокол . Настройки в строку: \"\"). Требуется перенастройка.")*/, "Тест ИнициализацияИСтарт. Не обнаружена требуемая строка.");

            /****************************************/
            //пустой протокол
            testUtils.RunTest(sourcePostfixPath, new FilesEssential_FileMonitors(), false, null,
                //customize
                (storage, testMaterialsPath) =>
                {
                    string localProtocolPath = Path.Combine(testMaterialsPath, @"FileEssential_FileMonitors\protocol_empty.csv");
                    if (!System.IO.File.Exists(localProtocolPath))
                        throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Файл с тестовым протоколом не найден. Убедитесь в наличии файла " + localProtocolPath);

                    string localProtocolWithTruePaths = localProtocolPath;

                    Protocol testProtocol = new Protocol()
                    {
                        Path = localProtocolWithTruePaths,
                        Type = Protocol.enType.PROCESS_MONITOR
                    };

                    Protocol.Pair prefixPair = new Protocol.Pair()
                    {
                        Prefix = Path.Combine(testMaterialsPath, sourcePostfixPath),
                        Subway = String.Empty
                    };
                    testProtocol.Prefixes.Add(prefixPair);

                    Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();
                    buffer.Add((int)1);
                    buffer.Add(testProtocol.ToConfigureString);
                    storage.pluginSettings.SaveSettings(pluginID, buffer);
                },
                //checkstorage
                (storage, testMaterialsPath) => { return true; },
                //checkreports
                (reportsPath, testMaterialsPath) =>
                {
                    //Сравниваем директории с отчётами
                    return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"FileEssential_FileMonitors\Reports\EmptyProtocol"), reportsPath);
                },
                //runTwice
                false, null, null, null);
            Assert.IsTrue(listener.ContainsPart("Файл протокола Process Monitor пуст"), "Тест Пустой Протокол. Не обнаружено искомой строки \"Файл протокола Process Monitor пуст\"");

            /****************************************/
            //Проверяется правильный формат первой строки отчёта.
            testUtils.RunTest(sourcePostfixPath, new FilesEssential_FileMonitors(), false, null,
                //customize
            (storage, testMaterialsPath) =>
            {
                string localProtocolPath = Path.Combine(testMaterialsPath, @"FileEssential_FileMonitors\protocol_bad.csv");
                if (!System.IO.File.Exists(localProtocolPath))
                    throw new Microsoft.VisualStudio.TestTools.UnitTesting.InternalTestFailureException("Файл с тестовым протоколом не найден. Убедитесь в наличии файла " + localProtocolPath);

                Protocol testProtocol = new Protocol()
                {
                    Path = localProtocolPath,
                    Type = Protocol.enType.PROCESS_MONITOR
                };

                Protocol.Pair prefixPair = new Protocol.Pair()
                {
                    Prefix = Path.Combine(testMaterialsPath, sourcePostfixPath),
                    Subway = String.Empty
                };
                testProtocol.Prefixes.Add(prefixPair);

                Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();
                buffer.Add((int)1);
                buffer.Add(testProtocol.ToConfigureString);
                storage.pluginSettings.SaveSettings(pluginID, buffer);
            },
                //checkstorage
            (storage, testMaterialsPath) => { return true; },
                //checkreports
                (reportsPath, testMaterialsPath) =>
                {
                    //Сравниваем директории с отчётами
                    return TestUtilsClass.Reports_Directory_XML_Compare(Path.Combine(testMaterialsPath, @"FileEssential_FileMonitors\Reports\CheckFirstLine"), reportsPath);
                },
                //runTwice
            false, null, null, null);
            Assert.IsTrue(listener.ContainsPart("Первая строка протокола не соответствует поддерживаемому формату"), "Тест Неправильный Протокол. Не обнаружена требуемая строка \"Первая строка протокола не соответствует поддерживаемому формату\".");

            /****************************************/
            //Проверяется реакция на "плохие" настройки: задан протокол, но ни одно поле у протокола не заполнено, нет ни пути, ни префиксов, ничего.
            try
            {
                testUtils.RunTest(sourcePostfixPath, new FilesEssential_FileMonitors(), false, null,
                    //customize
                (storage, testMaterialsPath) =>
                {
                    Protocol testProtocol = new Protocol();

                    Store.Table.IBufferWriter buf = Store.Table.WriterPool.Get();
                    buf.Add((int)1);
                    buf.Add(testProtocol.ToConfigureString);
                    storage.pluginSettings.SaveSettings(pluginID, buf);
                },
                    //checkstorage
                (storage, testMaterialsPath) => true,
                    //checkreports
                    (reportsPath, testMaterialsPath) => true,
                    //runTwice
                false, null, null, null);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message.Contains("Настройки протоколов некорректны"), "Тест Неправильные настройки. Не обнаружена требуемая строка \"Настройки протоколов некорректны\".");
            }
        }

        static void CheckForRightMarkup(Store.Storage storage, IEnumerable<string> essentialFiles, IEnumerable<string> otherFiles)
        {
            foreach (var essFile in essentialFiles)
            {
                var file = storage.files.Find(essFile);
                Assert.IsFalse(file == null, String.Format("Файл <{0}> не обранужен в хранилище!", essFile));

                Assert.IsTrue(file.FileEssential == Store.enFileEssential.ESSENTIAL,
                    String.Format("Файл <{0}> не отмечен как значимый!", essFile));
            }

            foreach (var essFile in otherFiles)
            {
                var file = storage.files.Find(essFile);
                Assert.IsFalse(file == null, String.Format("Файл <{0}> не обранужен в хранилище!", essFile));

                Assert.IsTrue(file.FileEssential != Store.enFileEssential.ESSENTIAL ||
                    (
                    file.FileExistence != Store.enFileExistence.EXISTS_AND_UNCHANGED &&
                    file.FileExistence != Store.enFileExistence.EXISTS_AND_UNKNOWN
                    ),
                    String.Format("Файл <{0}> не отмечен как избыточный!", essFile));
            }
        }

        class BasicMonitorListener : IA.Monitor.Log.Interface
        {
            List<string> messages;
            public BasicMonitorListener()
            {
                messages = new List<string>();
            }
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                messages.Add(message.Text);
            }

            public void Clear()
            {
                messages.Clear();
            }

            public bool ContainsPart(string partialString)
            {
                if (messages.Count == 0)
                    return false;
                return messages.Any(s => s.Contains(partialString));
            }
        }
    }
}
