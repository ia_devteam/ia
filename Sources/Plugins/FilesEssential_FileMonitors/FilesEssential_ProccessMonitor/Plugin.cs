using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using Store;
using Store.Table;

using IA.Extensions;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("TestFilesEssential_FileMonitors")]

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    internal static class PluginSettings
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.FILES_ESSENTIAL_FILE_MONITORS;

        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Определение избыточных файлов по протоколам файловых мониторов";

        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Разбор протоколов")]
            EXECUTING,

            [Description("Разбор текущего протокола")]
            PROTOCOLREADING
        }
        /// <summary>
        /// Набор протоколов в строковом представлении
        /// </summary>
        internal static StringCollection ProtocolStringCollection;

        /// <summary>
        /// Набор протоколов
        /// </summary>
        internal static List<Protocol> ProtocolCollection
        {
            get
            {
                List<Protocol> ret = new List<Protocol>();

                if (PluginSettings.ProtocolStringCollection != null)
                    foreach (string protocolString in PluginSettings.ProtocolStringCollection)
                        ret.Add(new Protocol(protocolString));

                return ret;
            }
            set
            {
                PluginSettings.ProtocolStringCollection = new StringCollection();

                if (value != null)
                    foreach (Protocol protocol in value)
                        PluginSettings.ProtocolStringCollection.Add(protocol.ToConfigureString);
            }
        }
    }

    /// <summary>
    /// На основе лога, выдаваемого ProcessMonitor (или аналогичной утилиты), размечает избыточность и используемость файлов.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    public class FilesEssential_FileMonitors : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        private Storage storage;

        /// <summary>
        /// Настройки
        /// </summary>
        private ProtocolListCustomSettings_CompactControl settings;

        /// <summary>
        /// Файлы для обработки
        /// </summary>
        private Files files;

        internal Dictionary<string, ReportRecord> reportEss = new Dictionary<string, ReportRecord>();

        #region PluginInterface Members
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW |
                       Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW |
                       Plugin.Capabilities.RESULT_WINDOW |
                       Plugin.Capabilities.REPORTS |
                       Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.ProtocolStringCollection = Properties.Settings.Default.ProtocolStringCollection;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            //Получаем количество протоколов из буфера
            int protocolsCount = settingsBuffer.GetInt32();

            //Выгружаем протоколы из буфера
            List<Protocol> protocols = new List<Protocol>();
            for (int i = 0; i < protocolsCount; i++)
            {
                protocols.Add(new Protocol(settingsBuffer.GetString()));
            }

            //Задаём настройку плагина
            PluginSettings.ProtocolCollection = protocols;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                return this.CustomSettingsWindow;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return settings = new ProtocolListCustomSettings_CompactControl() { Protocols = PluginSettings.ProtocolCollection };
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                if (this.storage == null)
                    throw new Exception("Хранилище не определено.");

                return new ResultWindow(storage);
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.ProtocolStringCollection = PluginSettings.ProtocolStringCollection;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.ProtocolCollection.Count);
            foreach (Protocol protocol in PluginSettings.ProtocolCollection)
                settingsBuffer.Add(protocol.ToConfigureString);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.IsSettingsCorrect"/>
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {

            if (settings != null)
                ((IA.Plugin.Settings)settings).Save();
            bool result = true;

            message = PluginSettings.ProtocolCollection.Aggregate(
                            String.Empty,
                            (protocols, currentProtocol) =>
                                System.IO.File.Exists(currentProtocol.Path) ? protocols : protocols + currentProtocol.Path + " ",
                            protocols => String.IsNullOrWhiteSpace(protocols) ? String.Empty : "Файл(ы) протоколов не существуют: " + protocols
                        );

            message += PluginSettings.ProtocolCollection.Aggregate(
                            String.Empty,
                            (protocols, currentProtocol) => currentProtocol.Type == Protocol.enType.LS ||
                                (currentProtocol.Prefixes != null && currentProtocol.Prefixes.Count != 0)
                                ? protocols : protocols + currentProtocol.Path + " ",
                            protocols => String.IsNullOrWhiteSpace(protocols) ? String.Empty : " Не заданы префиксы для протоколов: " + protocols
                        );

            result = message == string.Empty;

            if (!result)
                message = "Настройки протоколов некорректны: " + message;

            return result;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                List<Monitor.Tasks.Task> tasks = new List<Monitor.Tasks.Task>();
                tasks.Add(new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.EXECUTING.Description(), 0));
                tasks.Add(new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.PROTOCOLREADING.Description(), 0));
                return tasks;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return null;
            }
        }




        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            this.files = new Files(storage, this);
            reportEss.Clear();

            // Сначала запишем все файлы в словарь
            foreach (IFile file in storage.files.EnumerateFiles(enFileKind.anyFile))
            {
                reportEss.Add(file.FileNameForReports, new ReportRecord(enFileEssential.UNKNOWN, "Отсутствуют (несущественны) записи в протоколах", 0));
            }

            //Установить количество стадий исполнения равным количеству протоколов
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.EXECUTING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)PluginSettings.ProtocolCollection.Count);

            if (PluginSettings.ProtocolCollection.Count == 0)
            {
                Monitor.Log.Error(PluginSettings.Name, "Не указан ни один протокол файловых мониторов.");
                return;
            }

            //Разбираем протоколы
            for (int i = 0; i < PluginSettings.ProtocolCollection.Count; i++)
            {
                IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.EXECUTING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, (ulong)(i + 1));

                try
                {
                    Protocol protocol = PluginSettings.ProtocolCollection[i];
                    //Установить количество стадий разбора у текущего протокола равным размеру файла в байтах.
                    IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.PROTOCOLREADING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)(new System.IO.FileInfo(protocol.Path)).Length);

                    switch (protocol.Type)
                    {
                        case Protocol.enType.PROCESS_MONITOR:
                            (new AnalyseProcessMonitor()).AnalyseFile(protocol, files, UpdateStatus);
                            break;

                        case Protocol.enType.UNIX:
                            (new AnalyseUnix()).AnalyseFile(protocol, files, UpdateStatus);
                            break;

                        case Protocol.enType.LS:
                            (new AnalyseLS()).AnalyseFile(protocol, files, UpdateStatus);
                            break;


                        default:
                            Monitor.Log.Error(PluginSettings.Name, "Неизвестный тип файла протокола: " + protocol.Path);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    if (!ex.Message.Contains("Путь имеет недопустимую форму") && !ex.Message.Contains("The path is not of a legal form.")) throw ex; //ожидаемая ошибка в случае отсутствия настроек
                }
            }

            this.files.FinalizeAlg();

            GenerateTempReports(storage.pluginData.GetDataFolder(ID));
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        private void UpdateStatus(ulong current)
        {
            IA.Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.PROTOCOLREADING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, current);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            IOController.DirectoryController.Copy(storage.pluginData.GetDataFolder(ID), reportsDirectoryPath, isRecursive: false);
        }

        /// <summary>
        /// получить промежуточные протоколы
        /// </summary>
        /// <param name="reportsDirectoryPath">путь к папке с протоколами</param>
        public void GenerateTempReports(string reportsDirectoryPath)
        {
            List<string[]> reportEssential = new List<string[]>();
            List<string[]> reportSuperFluous = new List<string[]>();
            reportEssential.Add(new string[] { "Наименование файла", "Наименование протокола", "Строка в протоколе" });
            reportSuperFluous.Add(new string[] { "Наименование файла", "Наименование протокола", "Строка в протоколе" });

            // Отчет в XML о избыточных и неизбыточных файлах:
            // Имя файла; Имя протокола; строка в протоколе;
            foreach (KeyValuePair<string, ReportRecord> kv in reportEss)
            {
                if (kv.Value.rtype == enFileEssential.ESSENTIAL)
                    reportEssential.Add(new string[] { kv.Key, kv.Value.protocolname, kv.Value.linenumber.ToString() });
                else
                    reportSuperFluous.Add(new string[] { kv.Key, kv.Value.protocolname, kv.Value.linenumber.ToString() });
            }
            XMLReport.XMLReport.Report(System.IO.Path.Combine(reportsDirectoryPath, "Существенные файлы.xml"), "Essential", reportEssential);
            XMLReport.XMLReport.Report(System.IO.Path.Combine(reportsDirectoryPath, "Избыточные файлы.xml"), "Superfluous", reportSuperFluous);
        }


        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }
        #endregion
    }


}
