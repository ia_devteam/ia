using System.Collections.Generic;

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{

    internal static class CommonUtils
    {
        private static int Comapare(Protocol.Pair x, Protocol.Pair y)
        {
            //Длинные префиксы должны анализироваться первыми, то есть стоять в списке спереди.
            if(x.Prefix.Length < y.Prefix.Length)
                return 1;
            if(x.Prefix.Length > y.Prefix.Length)
                return -1;
            
            int i = x.Prefix.CompareTo(y.Prefix);
            if(i != 0)
                return i;
            else
                return x.Subway.CompareTo(y.Subway);
        }

        internal static void SortPrefixes(List<Protocol.Pair> prefixes)
        {
            //Канонизируем префиксы путей
            foreach (Protocol.Pair pair in prefixes)
            {
                pair.Prefix = Store.Files.CanonizeFileName(pair.Prefix);
                pair.Subway = Store.Files.CanonizeFileName(pair.Subway);
            }

            prefixes.Sort(Comapare);
        }
    }

    /// <summary>
    /// Класс для чтения протокола
    /// </summary>
    internal class ProtocolReader
    {
        System.IO.StreamReader reader;

        public delegate void UpdateReaderStatus(ulong current);

        UpdateReaderStatus updateStatus;
        /// <summary>
        /// Конструктор
        /// </summary>
        internal ProtocolReader(string fileName, UpdateReaderStatus updateStatus = null)
        {
            reader = new System.IO.StreamReader(fileName);
            this.updateStatus = updateStatus;
        }

        internal bool EndOfStream
        {
            get { return reader.EndOfStream; }
        }

        internal string ReadLine()
        {
            string line = reader.ReadLine();
            if (updateStatus != null)
                updateStatus((ulong)reader.BaseStream.Position);
            return line;
        }

        internal IEnumerable<string> ReadLines()
        {
            string line = null;
            while ((line = reader.ReadLine()) != null)
            {
                if (updateStatus != null)
                    updateStatus((ulong)reader.BaseStream.Position);
                yield return line;                
            }
        }

        internal void Close()
        {
            reader.Close();
        }
    }
}
