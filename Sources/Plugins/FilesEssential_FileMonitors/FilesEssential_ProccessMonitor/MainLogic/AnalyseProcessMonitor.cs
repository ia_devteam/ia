﻿using System.Collections.Generic;

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    /// <summary>
    /// Выполняет разбор протоколов ProcessMonitor.
    /// </summary>
    internal class AnalyseProcessMonitor
    {
        /// <summary>
        /// Разбирает один файл протокола ProcessMonitor
        /// </summary>
        /// <param name="protocol">Разбираемый файл</param>
        /// <param name="files">Ссылка на раздел Хранилища</param>
        /// <param name="updateStatus"></param>
        internal void AnalyseFile(Protocol protocol, Files files, ProtocolReader.UpdateReaderStatus updateStatus = null)
        {
            var prefixes = protocol.Prefixes;
            //Если префиксы заданы пользователем, то сразу же выполняем их сортировку
            CommonUtils.SortPrefixes(prefixes);

            ProtocolReader reader = new ProtocolReader(protocol.Path, updateStatus);
            int lineNumber = 0;

            if (reader.EndOfStream)
            {
                Monitor.Log.Warning(PluginSettings.Name,
                    "Файл протокола Process Monitor пуст.");
                return;
            }

            //Проверки, является ли данный файл протоколом ProcessMonitor.
            //Заодно пропускаем первую строку с заголовками.

            string line;

            line = reader.ReadLine();
            lineNumber++;

            if (line != "\"Time of Day\",\"Process Name\",\"PID\",\"Operation\",\"Path\",\"Result\",\"Detail\"")
            {
                Monitor.Log.Error(PluginSettings.Name,
                    "Первая строка протокола не соответствует поддерживаемому формату Process Monitor.");
                return;
            }

            LineErrors errorsList = new LineErrors();
            //Поочередно разбираем каждую строку.
            while ((line = reader.ReadLine()) != null)
            {
                lineNumber++;

                var result = AcceptLine(line, prefixes, files, lineNumber, System.IO.Path.GetFileName(protocol.Path));

                if (result != LineErrorsType.None)
                {
                    errorsList.Add(result, lineNumber);
                }
            }

            reader.Close();

            foreach (var s in errorsList.GetErrors())
            {
                Monitor.Log.Warning(PluginSettings.Name, s);
            }
        }

        /// <summary>
        /// Разбирает одну строку протокола ProcessMonitor.
        /// </summary>
        /// <param name="line">Обрабатываемая строка.</param>
        /// <param name="prefixes">Заменяемые префиксы.</param>
        /// <param name="files">Ссылка на раздел Хранилища.</param>
        /// <param name="currentLine">текущая строка</param>
        /// <param name="protocolPath">протокол</param>
        /// <returns>Строка с текстом ошибки в случае наличия таковой либо пустая строка/null.</returns>
        private LineErrorsType AcceptLine(string line, List<Protocol.Pair> prefixes,  Files files, int currentLine, string protocolPath)
        {
            //Разбираем входную строку на кусочки
            //в некоторых версиях оказалось, что дата пишется не "8:39:37,4624926", а "8:39:37.4624926" - поэтому делаем проверку и замену
            //string[] tmp;
            //if ((tmp = line.Split(',')) != null && tmp[0].Contains("."))
            //{
            //    line = line.Replace(tmp[0], tmp[0].Replace(".", ","));
            //}

            int tmp = line.IndexOf('"'); //первая кавычка
            tmp = line.IndexOf('"', tmp + 1); //вторая кавычка

            int startIndex = line.IndexOf(',') < tmp ? 4 : 3; //если внутрях числа есть запятая - отсчёт по-старому, от 4 (0-based). Если внутрях точка - то отсчёт на 1 меньше - split по-короче выходит.

            string[] items = line.Split(',');

            if (items.Length < startIndex + 3) //7) //В каком-то протоколе обнаружилось имя файла, в котором присутствует переход на новую строку. 
                //Считаю, что такое имя не может существовать в ОС. Файл пропускаем.
                return LineErrorsType.WrongFormat;

            string Operation = items[startIndex];//items[4];
            string Path = items[startIndex + 1];//items[5];
            //Обрезаем кавычки
            Path = Path.Substring(1, Path.Length - 2);
            string currentFile = Path;
            //Если в имени пути есть двоеточие, то обрезаем его и все, что после него.
            //Добавлено после того, как в логе нашли путь "C:\__DiskT\1997\ScanifyAPI\Dataflow\Thumbs.db:encryptable"
            int idx = Path.LastIndexOf(':');
            if (idx > 1)
                Path = Path.Substring(0, idx);
            //Канонизируем путь
            Path = Store.Files.CanonizeFileName(Path);


            if (string.IsNullOrWhiteSpace(Path) || Path.Contains("*")) //В каком-то протоколе обнаружилось пустое имя файла. Такой случай не интересен, пропускаем.
                return LineErrorsType.BadFileName;

            bool Success = items[startIndex + 2] == "\"SUCCESS\"";//items[6] == "\"SUCCESS\"";
            string Options = items[startIndex + 3];//items[7];


            //В зависимости от команды помечаем файл
            if (Success)
            {
                switch (Operation)
                {
                    case "\"CreateFile\"":
                        if (!CheckContains(items, "Options: Directory")) //Это не папка
                            if (CheckContains(items, "Disposition: Open")) //Файл открывается без уничтожения модержимого
                                files.Referenced(Path, prefixes, protocolPath, currentLine);
                            else //Файл создается или перезаписывается
                                files.Reset(Path, prefixes, protocolPath, currentLine);
                        break;
                    case "\"WriteFile\"":
                    case "\"IRP_MJ_SET_INFORMATION\"":
                    case "\"IRP_MJ_WRITE\"":
                    case "\"FASTIO_ACQUIRE_FOR_MOD_WRITE\"":
                        files.Modified(Path, prefixes, protocolPath, currentLine);
                        break;
                    case "\"IRP_MJ_CLEANUP\"":
                    case "\"IRP_MJ_CLOSE\"":
                    case "\"IRP_MJ_LOCK_CONTROL\"":
                    case "\"IRP_MJ_QUERY_INFORMATION\"":
                    case "\"IRP_MJ_READ\"":
                    case "\"ReadFile\"":
                    case "\"QueryFileInternalInformationFile\"":
                    case "\"QueryEaInformationFile\"":
                    case "\"QueryStreamInformationFile\"":
                    case "\"QueryAttributeInformationVolume\"":
                    case "\"LockFile\"":
                    case "\"UnlockFileSingle\"":
                    case "\"FASTIO_RELEASE_FOR_MOD_WRITE\"":
                    case "\"FASTIO_ACQUIRE_FOR_CC_FLUSH\"":
                    case "\"FASTIO_RELEASE_FOR_CC_FLUSH\"":
                        files.Referenced(Path, prefixes, protocolPath, currentLine);
                        break;
                    case "\"IRP_MJ_CREATE\"":
                        if (Options.Contains("Read") || Options.Contains("Execute") || Options.Contains("Synchronize"))
                            files.Referenced(Path, prefixes, protocolPath, currentLine);
                        else
                            files.Reset(Path, prefixes, protocolPath, currentLine); //скорее всего тут резет, так как события креат у нас нет, необходимо подебажить опции на ворклоаде
                        break;
                    case "\"IRP_MJ_DIRECTORY_CONTROL\"":
                        if (Options == "\"Type: QueryDirectory")
                            if (files.IsFile(Path, prefixes))
                                files.Referenced(Path, prefixes, protocolPath, currentLine);
                        break;
                    case "\"QueryStandardInformationFile\"":
                        if (!CheckContains(items, "Directory: True"))
                            files.Referenced(Path, prefixes, protocolPath, currentLine);
                        break;
                    case "\"QueryBasicInformationFile\"":
                    case "\"QueryAllInformationFile\"":
                    case "\"QueryOpen\"":
                        if (!CheckContains(items, "FileAttributes: D"))
                            files.Referenced(Path, prefixes, protocolPath, currentLine);
                        break;
                    case "\"QueryAttributeTagFile\"":
                        if (!CheckContains(items, "Attributes: D"))
                            files.Referenced(Path, prefixes, protocolPath, currentLine);
                        break;
                    case "\"SetRenameInformationFile\"":
                        string DistinationFile = items[startIndex + 4].Substring(" FileName: ".Length - 1, items[startIndex + 4].Length - " FileName: ".Length);//items[8].Substring(" FileName: ".Length - 1, items[8].Length - " FileName: ".Length);
                        files.Referenced(Path, prefixes, protocolPath, currentLine);
                        files.Reset(DistinationFile, prefixes, protocolPath, currentLine);
                        break;
                    // FIXME Вызвыает большой лог в мониторе - нужно обрабатывать!
                    case "\"CloseFile\"":
                    case "\"QueryDirectory\"":
                    case "\"QueryNetworkOpenInformationFile\"":
                    case "\"SetBasicInformationFile\"":
                    case "\"SetDispositionInformationFile\"":
                    case "\"SetEndOfFileInformationFile\"":
                    case "\"SetAllocationInformationFile\"":
                        break;
                    default:
                        return LineErrorsType.UnknownOperation;
                }
            }
            return LineErrorsType.None;
        }

        private bool CheckContains(string[] items, string str)
        {
            foreach (string item in items)
                if (item.IndexOf(str) != -1)
                    return true;

            return false;
        }
    }
}
