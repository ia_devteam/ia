﻿using System;
using System.Collections.Generic;

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    /// <summary>
    /// Анлиз логов, сгенерированных при помощи ls
    /// Требуется два лога, сгенерированных следующими командами:
    /// 
    /// ls -R -lu --full-time
    /// ls -R -l --full-time
    /// </summary>
    class AnalyseLS
    {
        /// <summary>
        /// Разбирает один файл протокола Unix
        /// </summary>
        /// <param name="protocol">Разбираемый файл</param>
        /// <param name="sourcesDirectoryPath">папка с исходниками</param>
        /// <param name="files">список файлов</param>
        /// <param name="updateStatus"></param>
        /// <param name="logStartDateTime"> Время, с которого началась сборка. </param>
        internal void AnalyseFile(Protocol protocol, Files files, ProtocolReader.UpdateReaderStatus updateStatus)
        {
            //Делаем префиксы
            var prefixes = protocol.Prefixes;
            CommonUtils.SortPrefixes(prefixes);

            LineErrors errorList = ReadFile(protocol, files, updateStatus, prefixes, true);
            errorList.Add(ReadFile(protocol, files, updateStatus, prefixes, false));

            foreach (var errorMessage in errorList.GetErrors())
                Monitor.Log.Warning(PluginSettings.Name, errorMessage);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="protocol"></param>
        /// <param name="files"></param>
        /// <param name="updateStatus"></param>
        /// <param name="prefixes"></param>
        /// <param name="isModification"></param>
        /// <returns></returns>
        private static LineErrors ReadFile(Protocol protocol, Files files, ProtocolReader.UpdateReaderStatus updateStatus, List<Protocol.Pair> prefixes, bool isModification)
        {
            //Открываем чтение протокола
            ProtocolReader reader;
            if (isModification)
                reader = new ProtocolReader(protocol.Path, updateStatus);
            else
                reader = new ProtocolReader(protocol.PathAccess, updateStatus);

            string line = reader.ReadLine().Replace('/', '\\');
            Int64 lineNumber = 1;

            var errorList = new LineErrors();

            //Проверки, является ли данный файл протоколом.
            if (reader.EndOfStream)
            {
                errorList.Add(LineErrorsType.WrongFormat, (int)lineNumber);
                return errorList;
            }

            string filePath = null;

            //Поочередно разбираем каждую строку.
            while (line != null)
            {
                //Разбираем строку
                if (line.Length > 0)
                {
                    line = line.Replace('/', '\\');
                    switch (line[0])
                    {
                        case '.': //Строка с именем папки. Также отрезаем точку
                            filePath = line.Substring(1, line.IndexOf(':') - 1).TrimStart('\\');
                            break;
                        case '-': //строка с именем файла и временем доступа к нему
                            string[] split = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            string[] date = split[5].Split('-');
                            string[] time = split[6].Split(':', '.');

                            if (split.Length != 9)
                                errorList.Add(LineErrorsType.WrongFormat, (int)lineNumber);

                            DateTime d = new DateTime(Convert.ToInt32(date[0]), Convert.ToInt32(date[1]), Convert.ToInt32(date[2]),
                                                        Convert.ToInt32(time[0]), Convert.ToInt32(time[1]), Convert.ToInt32(time[2]));

                            if (d > protocol.logStartDateTime)
                            {
                                string fileName = line.Substring(line.IndexOf(split[8]));

                                //Если дата модификации позже начала компиляции, то что-то с файлом происходило. Запоминаем
                                if (isModification)
                                    files.Modified(System.IO.Path.Combine(filePath, fileName), prefixes, System.IO.Path.GetFileName(protocol.Path), lineNumber);
                                else
                                    files.Referenced(System.IO.Path.Combine(filePath, fileName), prefixes, System.IO.Path.GetFileName(protocol.Path), lineNumber);
                            }

                            break;
                    }
                }
                //Дочитываем протокол
                line = reader.ReadLine();
                lineNumber++;
            }

            reader.Close();

            return errorList;
        }
    }
}
