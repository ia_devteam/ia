﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Threading.Tasks;



namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    /// <summary>
    /// Выполняет обработку протоколов для Unix.
    /// 
    /// inotifywait -mr —format %w%f--%:e /usr/src/mono/
    /// </summary>
    internal class AnalyseUnix
    {
        /// <summary>
        /// Разбирает один файл протокола Unix
        /// </summary>
        /// <param name="protocol">Разбираемый файл</param>
        /// <param name="files">список файлов</param>
        /// <param name="updateStatus"></param>
        internal void AnalyseFile(Protocol protocol, Files files, ProtocolReader.UpdateReaderStatus updateStatus)
        {
            var prefixes = protocol.Prefixes;

            CommonUtils.SortPrefixes(prefixes);
            
            ProtocolReader reader = new ProtocolReader(protocol.Path, updateStatus);
            string protocolPath = System.IO.Path.GetFileName(protocol.Path);

            //Проверки, является ли данный файл протоколом ProcessMonitor.
            //Заодно пропускаем первую строку с заголовками.
            if (reader.EndOfStream)
            {
                Monitor.Log.Warning(PluginSettings.Name,
                    "Файл протокола пуст.");
                return;
            }

            string line;
            Int64 lineNumber = 0;
            Func<string, AcceptLineParams> parseLine = null;
            line = reader.ReadLine();
            lineNumber++;
            if (line == "Setting up watches.  Beware: since -r was given, this may take a while!")
            {
                line = reader.ReadLine();
                lineNumber++;
                if (line != "Watches established.")
                {
                    Monitor.Log.Error(PluginSettings.Name,
                        "Вторая строка протокола не соответствует поддерживаемому формату");
                    return;
                }
                line = reader.ReadLine();
                lineNumber++;

                parseLine = (l) =>
                {
                    int idx = line.LastIndexOf("--");
                    string optionsLine = line.Substring(idx + 2, line.Length - (idx + 2));
                    return new AcceptLineParams(
                        Store.Files.CanonizeFileName(line.Substring(0, idx)),
                        optionsLine,
                        optionsLine.Split(':')
                        );
                };
            }
            else
            {
                parseLine = (l) =>
                {   
                    int idx = line.LastIndexOf(" ");
                    var MonitorPath = line.Substring(0, idx);
                    int idx1 = MonitorPath.LastIndexOf(" ");
                    MonitorPath = Store.Files.CanonizeFileName(MonitorPath.Substring(0, idx1));
                    var optionsLine = line.Substring(idx1 + 1, idx - idx1 - 1);
                    var options = optionsLine.Split(',');
                    return new AcceptLineParams(
                        MonitorPath,
                        optionsLine,
                        options
                        );
                };
            }

            //List<string> errorList = new List<string>();
            var errorList = new LineErrors();
            //Поочередно разбираем каждую строку.
            while (line != null)
            {
                //string message = AcceptLine(line, prefixes, sourcesDirectoryPath, files, protocolPath, lineNumber++, logFormat);
                var result = AcceptLine(parseLine(line), prefixes, files, protocolPath, lineNumber);
                if (result != LineErrorsType.None)
                    errorList.Add(result, (int)lineNumber);

                line = reader.ReadLine();

                lineNumber++;
            }

            //foreach (var parsed in reader.ReadLines().AsParallel().AsOrdered().Select(parseLine))
            //{
            //    var result = AcceptLine(parsed, prefixes, files, protocolPath, lineNumber);
            //    if (result != LineErrorsType.None)
            //        errorList.Add(result, (int)lineNumber);
            //    lineNumber++;
            //}



            reader.Close();

            foreach (var errorMessage in errorList.GetErrors())
                Monitor.Log.Warning(PluginSettings.Name, errorMessage);
        }

        /// <summary>
        /// Анализ одной строки протокола Unix
        /// </summary>
        /// <param name="parsedLine">Обработанный текст строки</param>
        /// <param name="prefixes">Удаляемые префиксы</param>
        /// <param name="files"></param>
        /// <param name="protocolPath"></param>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        private LineErrorsType AcceptLine(AcceptLineParams parsedLine, List<Protocol.Pair> prefixes, Files files, string protocolPath, Int64 lineNumber)
        {
            string MonitorPath = parsedLine.MonitorPath;
            string optionsLine = parsedLine.OptionsLine;
            string[] options = parsedLine.Options;

            if (string.IsNullOrEmpty(MonitorPath)) //В каком-то протоколе обнаружилось пустое имя файла. Такой случай не инетерсен, пропускаем.
                return LineErrorsType.BadFileName;

            if (options[options.Length - 1] == "ISDIR")
                return LineErrorsType.None; //ежли строка молвит о каталоге - пропускаем. Интересны только файлы.


            //Перебираем все модификаторы, с которыми выполнена операция
            foreach (string option in options)
            {
                if (option == "ACCESS") //A watched file or a file within a watched directory was read from.
                    files.Referenced(MonitorPath, prefixes, protocolPath, lineNumber);

                else if (option == "MODIFY") //A watched file or a file within a watched directory was written to.
                    files.Modified(MonitorPath, prefixes, protocolPath, lineNumber);

                else if (option == "ATTRIB") //The metadata of a watched file or a file within a watched directory was modified. This includes timestamps, file permissions, extended attributes etc.
                    files.Referenced(MonitorPath, prefixes, protocolPath, lineNumber);

                else if (option == "CLOSE") //A watched file or a file within a watched directory was closed, after being opened in writeable mode. This does not necessarily imply the file was written to.
                { } //Игнорируем

                else if (option == "CLOSE_WRITE") //A watched file or a file within a watched directory was closed, after being opened in writeable mode. This does not necessarily imply the file was written to.
                { } //Игнорируем

                else if (option == "CLOSE_NOWRITE") //A watched file or a file within a watched directory was closed, after being opened in read-only mode.
                { } //Игнорируем

                else if (option == "OPEN") //A watched file or a file within a watched directory was opened.
                    files.Referenced(MonitorPath, prefixes, protocolPath, lineNumber);

                else if (option == "MOVED_TO") //A file or directory was moved into a watched directory. This event occurs even if the file is simply moved from and to the same directory.
                    files.Modified(MonitorPath, prefixes, protocolPath, lineNumber);

                else if (option == "MOVED_FROM") //A file or directory was moved from a watched directory. This event occurs even if the file is simply moved from and to the same directory.
                    files.Modified(MonitorPath, prefixes, protocolPath, lineNumber);

                else if (option == "CREATE") //A file or directory was created within a watched directory.
                    files.Reset(MonitorPath, prefixes, protocolPath, lineNumber);

                else if (option == "DELETE") //A file or directory within a watched directory was deleted.
                    files.Deleted(MonitorPath, prefixes, protocolPath, lineNumber);

                else if (option == "DELETE_SELF") //A watched file or directory was deleted. After this event the file or directory is no longer being watched. Note that this event can occur even if it is not explicitly being listened for.
                    files.Deleted(MonitorPath, prefixes, protocolPath, lineNumber);

                else if (option == "UNMOUNT") //The filesystem on which a watched file or directory resides was unmounted. After this event the file or directory is no longer being watched. Note that this event can occur even if it is not explicitly being listened to.
                { } //Игнорируем
                else
                {
                    return LineErrorsType.UnknownOperation;
                }
            }

            return LineErrorsType.None;
        }

        struct AcceptLineParams
        {
            public AcceptLineParams(string monitorPath, string optionsLine, string[] options)
            {
                MonitorPath = monitorPath;
                OptionsLine = optionsLine;
                Options = options;
            }

            public string MonitorPath;
            public string OptionsLine;
            public string[] Options;
        }
    }
}
