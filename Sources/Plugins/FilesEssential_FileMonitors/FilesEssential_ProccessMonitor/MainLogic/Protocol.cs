﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using IA.Extensions;

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    /// <summary>
    /// Описание настроек для одного файла с протоколом ProcessMonitor.
    /// </summary>
    internal class Protocol
    {
        /// <summary>
        /// Тип анализируемого протокола
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue")]
        internal enum enType
        {
            /// <summary>
            /// Протокол получен при помощи ProcessMonitor
            /// </summary>
            [Description("Process Monitor")]
            PROCESS_MONITOR = 1 << 0,

            /// <summary>
            /// Протокол получен для Unix
            /// </summary>
            [Description("Unix (inotifywait)")]
            UNIX = 1 << 1,

            /// <summary>
            /// Протокол получен для Unix
            /// </summary>
            [Description("Unix (ls)")]
            LS = 1 << 2
        }

        internal class Pair
        {
            internal string Prefix { get; set; }
            internal string Subway { get; set; }

            public Pair()
            {
                this.Prefix = null;
                this.Subway = null;
            }

            internal void Copy(Pair pair)
            {
                this.Prefix = pair.Prefix;
                this.Subway = pair.Subway;
            }

            internal bool IsValid(out string message)
            {
                message = String.Empty;

                if (String.IsNullOrWhiteSpace(this.Prefix) && String.IsNullOrWhiteSpace(this.Subway))
                {
                    message = "Набор префиксов должен содержать хотя бы одно значение.";
                    return false;
                }

                return true;
            }

            public override string ToString()
            {
                return "<" + this.Prefix + ", " + this.Subway + ">";
            }
        }

        /// <summary>
        /// Имя файла, содержащего протокол.
        /// В случае использования режима Unix (ls): хранит протокол, содержащий время последнего изменения файла.  
        /// </summary>
        internal string Path { get; set; }

        /// <summary>
        /// Имя файла, заполняемое только для режима Unix (ls). Хранит протокол, содержащий время последнего доступа к файлу. 
        /// </summary>
        internal string PathAccess { get; set; }

        /// <summary>
        /// Дата-время, требуемые для режима Unix (ls).
        /// </summary>
        internal DateTime logStartDateTime { get; set; }

        /// <summary>
        /// Тип протокола.
        /// </summary>
        internal enType Type { get; set; }

        /// <summary>
        /// Перечень отрезаемых префиксов
        /// </summary>
        internal List<Pair> Prefixes { get; set; }

        /// <summary>
        /// Информация о протоколе в строковом виде
        /// </summary>
        internal string ToConfigureString
        {
            get
            {
                string message;

                if (!this.IsValid(out message))
                {
                    Monitor.Log.Error(PluginSettings.Name, message);
                }

                List<string> ret = new List<string>();

                ret.Add(this.Path);
                ret.Add(this.PathAccess);
                ret.Add(this.logStartDateTime.ToBinary().ToString());

                ret.Add(((int)(this.Type == 0 ? enType.PROCESS_MONITOR : this.Type)).ToString());

                foreach (Pair pair in this.Prefixes)
                {
                    ret.Add(pair.Prefix ?? String.Empty);
                    ret.Add(pair.Subway ?? String.Empty);
                }

                return string.Join("$||$", ret.ToArray());
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        internal Protocol()
        {
            this.Path = String.Empty;
            this.PathAccess = String.Empty;
            this.logStartDateTime = DateTime.Now;
            this.Type = 0;
            this.Prefixes = new List<Pair>();
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="protocolString">Информация о протоколе в строковом виде</param>
        internal Protocol(string protocolString) : this()
        {
            string[] split = protocolString.Split(new string[] { "$||$" }, StringSplitOptions.None);

            try
            {
                this.Path = split[0];
                this.PathAccess = split[1];
                this.logStartDateTime = DateTime.FromBinary(Convert.ToInt64(split[2]));
                this.Type = (enType)Convert.ToInt32(split[3]);

                this.Prefixes = new List<Pair>();
                for (int i = 4; i < split.Length; i += 2)
                    this.Prefixes.Add(new Pair() { Prefix = split[i], Subway = split[i + 1] });
            }
            catch
            {
                Monitor.Log.Error(PluginSettings.Name,
                    "Настройки протокола некорректны (Протокол " + split[0] + ". Настройки в виде строки: \""+ protocolString +"\"). Требуется перенастройка.");
            }
        }

        /// <summary>
        /// Является ли протокол корректным
        /// </summary>
        internal bool IsValid(out string message)
        {
            message = String.Empty;

            if (String.IsNullOrWhiteSpace(this.Path))
            {
                message = "Путь к протоколу не задан.";
                return false;
            }

            if (!System.IO.File.Exists(this.Path))
            {
                message = "Путь к протоколу <" + this.Path + "> не существует.";
                return false;
            }

            if (!String.IsNullOrWhiteSpace(this.PathAccess) && !System.IO.File.Exists(this.PathAccess))
            {
                message = "Путь к протоколу последнего доступа <" + this.PathAccess + "> не существует.";
                return false;
            }

            if (this.Type == 0)
            {
                message = "Тип протокола не задан.";
                return false;
            }
            
            return true;
        }

        public override string ToString()
        {
            //return this.Path;
            return String.Format("{0}:{1}:{2}:{3}",
                this.Path,
                this.Type == enType.LS ? this.PathAccess + " " + this.logStartDateTime.ToString(): String.Empty, 
                this.Type == 0 ? "NONE" : this.Type.Description(), 
                this.Prefixes.Select(x => x.Prefix).Aggregate("",(total, prefix) => total +"," + prefix, (t) => t.Length > 0 ? t.Substring(1) : t));
        }
    }
}
