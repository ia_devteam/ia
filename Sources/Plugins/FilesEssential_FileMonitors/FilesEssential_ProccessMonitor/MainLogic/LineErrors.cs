﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

using IA.Extensions;

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    /// <summary>
    /// Типы возможных ошибок при обработке строк отчётов файловых мониторов.
    /// </summary>
    enum LineErrorsType
    {
        [Description("Строка имеет неверный формат (менее 7 полей разделённых запятыми)")]
        WrongFormat,
        [Description("В строке встретилось плохое имя файла")]
        BadFileName,
        [Description("В строке встретилась неизвестная операция")]
        UnknownOperation,
        [Description("Ошибки нет")]
        None
    }

    /// <summary>
    /// Класс-аггрегатор происходящих ошибок.
    /// </summary>
    internal class LineErrors
    {
        private readonly int LIMIT_ERROR_LINE_COUNT;

        private System.Collections.Concurrent.ConcurrentDictionary<LineErrorsType, List<int>> errors;

        /// <summary>
        /// Конструктор по-умолчанию.
        /// </summary>
        public LineErrors(int lineLimit = 512  /*It's a kind of magic*/)
        {
            
            errors = new System.Collections.Concurrent.ConcurrentDictionary<LineErrorsType, List<int>>();
            EnumLoop<LineErrorsType>.ForEach((t) => errors.AddOrUpdate(t, new List<int>(LIMIT_ERROR_LINE_COUNT), (a,b) => null));
            LIMIT_ERROR_LINE_COUNT = lineLimit;
        }
        
        /// <summary>
        /// Добавить в список ошибок обработок строку с ошибкой данного типа. При достижении порога новые записи не добавляются за неинформативностью этого занятия.
        /// </summary>
        /// <param name="type">Тип ошибки обработки строки.</param>
        /// <param name="lineNum">Порядковый номер строки.</param>
        public void Add(LineErrorsType type, int lineNum)
        {
            if (errors[type].Count < LIMIT_ERROR_LINE_COUNT)
                errors[type].Add(lineNum);
        }

        /// <summary>
        /// Добавляет другой список к этому
        /// </summary>
        /// <param name="list"></param>
        public void Add(LineErrors list)
        {
            foreach(var pair in list.errors)
                foreach(int i in pair.Value)
                    this.Add(pair.Key, i);
        }

        /// <summary>
        /// Признак наличия хотя бы одной зарегистрированной ошибки.
        /// </summary>
        public bool HasRecords { get { return errors.Any(kvp => kvp.Value.Count > 0); } }

        /// <summary>
        /// Получить сообщения о типах произошедших ошибках и номерах строк, вызвавших данные ошибки.
        /// 
        /// Функция позволяет не выкидывать пользователю 100500 однотипных сообщений. Такое выкидывание сильно тормозит GUI.
        /// Если сообщений одного типа мало - выкидывают они все. Если много - выкидывается унифицированное сообщение.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetErrors()
        {
            if (!HasRecords)
                yield break;

            //внутрях лямбды низя yield, поэтому такой костыль
            List<string> result = new List<string>();

            EnumLoop<LineErrorsType>.ForEach((t) =>
                {
                    if (errors[t].Count > 0)
                    {
                        var curList = errors[t];
                        string localResult = t.Description() + ": ";

                        int loopFrontier = Math.Min(curList.Count, LIMIT_ERROR_LINE_COUNT);

                        if (loopFrontier < 20 /*It's a kind of magic*/)
                        {
                            for (int i = 0; i < loopFrontier - 1; i++)
                                localResult += curList[i].ToString() + ",";
                            localResult += curList[loopFrontier - 1] + ".";
                        }
                        else
                        {
                            System.Text.StringBuilder sb = new System.Text.StringBuilder(loopFrontier * 5 /*It's a kind of magic*/);

                            for (int i = 0; i < loopFrontier - 1; i++)
                            {
                                sb.Append(curList[i]);
                                sb.Append(',');
                            }

                            sb.Append(curList[loopFrontier - 1]);
                            sb.Append('.');

                            if (loopFrontier != curList.Count)
                                sb.Append(" Дальнейшие ошибки не приведены ввиду их большого числа.");

                            localResult += sb.ToString();

                        }

                        result.Add(localResult);
                    }
                });

            foreach (var s in result)
                yield return s;
        }
    }
}
