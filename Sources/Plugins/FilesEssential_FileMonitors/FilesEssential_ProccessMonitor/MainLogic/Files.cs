using System;
using System.Collections.Generic;
using Store;

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{

    /// <summary>
    /// Представлние файла, упоминаемого в протоколе.
    /// </summary>
    internal abstract class File
    {
        /// <summary>
        /// Участвует ли файл в компиляции?
        /// </summary>
        internal abstract enFileEssential FileEssential { get; set; }

        /// <summary>
        /// Существует ли файл?
        /// </summary>
        internal abstract enFileExistence FileExistence { get; set; }
    }

    internal class ReportRecord
    {
        public enFileEssential rtype;
        public string protocolname;
        public Int64 linenumber;
        public ReportRecord(enFileEssential tp, string pn, Int64 ln)
        {
            rtype = tp;
            protocolname = pn;
            linenumber = ln;
        }

        public override string ToString()
        {
            return rtype.ToString();
        }
    }



    /// <summary>
    /// Файл, для которого удалось выполнить привязку к Хранилищу.
    /// </summary>
    internal class StorageFile : File
    {
        /// <summary>
        /// Объект файла в Хранилище
        /// </summary>
        internal IFile file;

        /// <summary>
        /// Участвует ли файл в компиляции?
        /// </summary>
        internal override enFileEssential FileEssential
        {
            get
            {
                return file.FileEssential;
            }
            set
            {
                Files.isAnyFileStatusChanged = true;
                file.FileEssential = value;
            }
        }

        /// <summary>
        /// Существует ли файл?
        /// </summary>
        internal override enFileExistence FileExistence
        {
            get
            {
                return file.FileExistence;
            }
            set
            {
                Files.isAnyFileStatusChanged = true;
                file.FileExistence = value;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="file">Объект файла в Хранилище</param>
        internal StorageFile(IFile file)
        {
            this.file = file;
        }
    }

    /// <summary>
    /// Файл, который не существует в составе исходных текстов
    /// </summary>
    internal class EmptyFile : File
    {
        internal override enFileEssential FileEssential
        {
            get
            {
                return enFileEssential.UNKNOWN;
            }
            set
            {
            }
        }
        internal override enFileExistence FileExistence
        {
            get
            {
                return enFileExistence.EXISTS_AND_UNKNOWN;
            }
            set
            {
            }
        }
    }

    /// <summary>
    /// Выполняет всю работу с Хранилищем в этом анализе
    /// </summary>
    internal class Files
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        private Storage storage = null;
        private string StoragePath_in = null;

        /// <summary>
        /// Флаг того, что хотя бы у одного файла была запись в поле FileEssential или FileExistence.
        /// </summary>
        public static bool isAnyFileStatusChanged = false;

        EmptyFile emptyFile = new EmptyFile();
        FilesEssential_FileMonitors plugin;
        internal Files(Storage storage, FilesEssential_FileMonitors plug)
        {
            this.storage = storage;
            StoragePath_in = storage.appliedSettings.FileListPath;
            isAnyFileStatusChanged = false;
            plugin = plug;
        }

        internal bool IsFile(string file_name, List<Protocol.Pair> prefixes)
        {
            try
            {
                return Find(file_name, prefixes, StoragePath_in) != null;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Этот метод вызывается для всех файлов, которые использовались при сборке.
        /// </summary>
        internal void Referenced(string file_name, List<Protocol.Pair> prefixes, string protocolPath, Int64 lineNumber)
        {
            File file = Find(file_name, prefixes, StoragePath_in);

            if (file.FileEssential == enFileEssential.UNKNOWN)
            {
                file.FileEssential = enFileEssential.ESSENTIAL;
                modEssentialsDictionary(file, protocolPath, lineNumber);
            }
        }

        /// <summary>
        /// Этот метод вызывается для всех файлов, которые были изменены при сборке.
        /// </summary>
        internal void Modified(string file_name, List<Protocol.Pair> prefixes, string protocolPath, Int64 lineNumber)
        {
            File file = Find(file_name, prefixes, StoragePath_in);

            if (file.FileEssential == enFileEssential.UNKNOWN || file.FileEssential == enFileEssential.ESSENTIAL)
            {
                file.FileEssential = enFileEssential.SUGGEST;
                modEssentialsDictionary(file, protocolPath, lineNumber);
            }

            if (file.FileExistence == enFileExistence.EXISTS_AND_UNKNOWN)
            {
                file.FileExistence = enFileExistence.EXISTS_AND_CHANGED;
            }
        }

        /// <summary>
        /// Этот метод вызывается для всех файлов, которые были уладены при сборке.
        /// </summary>
        internal void Deleted(string file_name, List<Protocol.Pair> prefixes, string protocolPath, Int64 lineNumber)
        {
            File file = Find(file_name, prefixes, StoragePath_in);

            // файл был удален при сборке - безусловно меняем на 'SUPERFLUOUS'
            file.FileEssential = enFileEssential.SUPERFLUOUS;
            modEssentialsDictionary(file, protocolPath, lineNumber);

            if (file.FileExistence == enFileExistence.EXISTS_AND_UNKNOWN)
            {
                file.FileExistence = enFileExistence.EXISTS_AND_CHANGED;
            }
            else if (file.FileExistence == enFileExistence.CREATED_AND_UNKNOWN)
            {
                file.FileExistence = enFileExistence.CREATED_AND_DELETED;
            }
        }

        internal void modEssentialsDictionary(File file, string protocolName, Int64 lineNumber)
        {
            StorageFile sfile = file as StorageFile;
            //У протокола берется только имя - приходит сверху от вызывающего метода 
            if (sfile != null)
            {
                string rname = sfile.file.FileNameForReports;
                ReportRecord tmp;
                if (plugin.reportEss.TryGetValue(rname, out tmp))
                {
                    tmp.rtype = file.FileEssential;
                    tmp.protocolname = protocolName;
                    tmp.linenumber = lineNumber;
                }
                else
                    plugin.reportEss.Add(rname, new ReportRecord(file.FileEssential, protocolName, lineNumber));
            }
        }

        /// <summary>
        /// Этот метод вызывается для всех файлов, которые были перезаписаны.
        /// </summary>
        internal void Reset(string file_name, List<Protocol.Pair> prefixes, string protocolPath, Int64 lineNumber)
        {
            File file = Find(file_name, prefixes, StoragePath_in);

            if (file.FileEssential == enFileEssential.UNKNOWN || file.FileEssential == enFileEssential.SUGGEST)
            {
                file.FileEssential = enFileEssential.RESET;
                modEssentialsDictionary(file, protocolPath, lineNumber);
            }

            if (file.FileExistence == enFileExistence.EXISTS_AND_UNKNOWN)
            {
                file.FileExistence = enFileExistence.EXISTS_AND_CHANGED;
            }
        }

        /// <summary>
        /// Этот метод необходимо вызвать, когда все возможные пометки уже сделаны.
        /// </summary>
        internal void FinalizeAlg()
        {
            //если ни у одного файла небыл изменён статус FileEssential или FileExistence кидаем предупреждение о том. Возможно это и нормальная ситуация, но, как правило, это означает ошибку в настройках.
            if (!isAnyFileStatusChanged)
                IA.Monitor.Log.Warning(PluginSettings.Name, "По результатам анализа всех протоколов небыло найдено ни одного соответствия между файлами протоколов и файлами в Хранилище. Возможно плагин настроен неверно.");
            //Теперь финализируем все файлы. Так как StorageFile пишет непосредственно в Хранилище, 
            //обрабатываем файлы из Хранилища.
            //Спорные файлы считаем избыточными!
            var existingMask = enFileExistence.EXISTS_AND_UNKNOWN | enFileExistence.EXISTS_AND_UNCHANGED | enFileExistence.EXISTS_AND_UNKNOWN;
            foreach (IFile file in storage.files.EnumerateFiles(Store.enFileKind.fileWithPrefix))
            {
                if ((file.FileExistence & existingMask) != 0
                    && new System.IO.FileInfo(file.FullFileName_Original).Length == 0) //TODO: реализовать тест
                {
                    file.FileExistence = enFileExistence.EXISTS_AND_UNCHANGED;
                    file.FileEssential = enFileEssential.ESSENTIAL;
                }
                else if (file.FileExistence == enFileExistence.CREATED_AND_UNKNOWN)
                    file.FileExistence = enFileExistence.CREATED_AND_UNDELETED;
                else if (file.FileExistence == enFileExistence.EXISTS_AND_UNKNOWN)
                    file.FileExistence = enFileExistence.EXISTS_AND_UNCHANGED;
            }
        }

        #region Реализация Find
        /// <summary>
        /// Возвращает структуру работы с файлом. Вне зависимости, удалось ли установить конкретный файл в Хранилище.
        /// </summary>
        private File Find(string file_name, List<Protocol.Pair> prefixes, string dest_prefix)
        {
            foreach (Protocol.Pair pair in prefixes)
            {
                IFile file = TryPrefix(file_name, pair, dest_prefix);

                if (file != null)
                    return new StorageFile(file);
            }

            if (prefixes.Count == 0)
            {
                IFile file = TryPrefix(file_name, new Protocol.Pair() { Prefix = "", Subway = "" }, dest_prefix);

                if (file != null)
                    return new StorageFile(file);
            }

            return emptyFile;
        }

        /// <summary>
        /// Проверяет, если применить данный префикс, будет ли обнаружен по пути файл?
        /// </summary>
        private IFile TryPrefix(string filePath, Protocol.Pair pair, string dest_prefix)
        {
            if (filePath.StartsWith(pair.Prefix))
                return storage.files.Find(RefactorPath(filePath, dest_prefix, pair));

            return null;
        }

        /// <summary>
        /// Перестройка пути в соответствии с префиксом.
        /// </summary>
        private string RefactorPath(string filePath, string dest_prefix, Protocol.Pair pair)
        {
            //StringBuilder builder = new StringBuilder(filePath);
            //builder.Remove(0, pair.Prefix.Length);
            //builder.Insert(0, pair.Subway);
            //builder.Insert(0, dest_prefix);
            //return builder.ToString();
            //создать две строки на основе трёх это быстрее, чем создать целый билдер, а потом внутри него сделать то же самое.
            return System.IO.Path.Combine(dest_prefix, pair.Subway.TrimStart('\\'), filePath.Substring(pair.Prefix.Length).TrimStart('\\'));
        }

        #endregion
    }
}
