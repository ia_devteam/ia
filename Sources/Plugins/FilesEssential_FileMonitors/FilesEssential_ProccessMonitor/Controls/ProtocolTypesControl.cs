﻿using System;
using System.Windows.Forms;

using IA.Extensions;

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    internal partial class ProtocolTypesControl : UserControl
    {
        /// <summary>
        /// Элемент управления - Таблица типов протокола
        /// </summary>
        private TableLayoutPanel main_tableLayoutPanel = null;

        public delegate void ClickHandler(object sender, EventArgs e);
        public event ClickHandler RadioButtonClick;

        /// <summary>
        /// Выбранный тип протокола
        /// </summary>
        internal Protocol.enType Type
        {
            get
            {
                foreach (Control control in main_tableLayoutPanel.Controls)
                    if (control is RadioButton && (control as RadioButton).Checked)
                        return (Protocol.enType)(control as RadioButton).Tag;

                return 0;
            }
            set
            {
                if (value == 0)
                {
                    foreach (var ctrl in main_tableLayoutPanel.Controls)
                    {
                        var rbtn = ctrl as RadioButton;
                        if (rbtn == null)
                            continue;
                        rbtn.Checked = false;
                    }
                    return;
                }

                (main_tableLayoutPanel.Controls[value.FullName()] as RadioButton).Checked = true;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        internal ProtocolTypesControl()
        {
            InitializeComponent();

            //Получаем общее число типов протокола
            int protocolTypesCount = Enum.GetNames(typeof(Protocol.enType)).Length;

            //Инициализация элемента управления - Таблица типов протокола
            main_tableLayoutPanel = new TableLayoutPanel()
            {
                Dock = DockStyle.Fill,
                RowCount = 1,
                ColumnCount = protocolTypesCount
            };

            //Проходим по всем типам протокола
            EnumLoop<Protocol.enType>.ForEach(type =>
                {
                    //Определяем ширину столбца
                    main_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100 / protocolTypesCount));

                    RadioButton radioButton = new RadioButton()
                    {
                        Dock = DockStyle.Fill,
                        Name = type.FullName(),
                        Text = type.Description(),
                        Tag = type
                    };
                    radioButton.Click += radioButton_Click;

                    main_tableLayoutPanel.Controls.Add(radioButton);
                });

            this.Controls.Add(main_tableLayoutPanel);
        }

        void radioButton_Click(object sender, EventArgs e)
        {
            if (RadioButtonClick != null)
                RadioButtonClick(sender, e);
        }

        /// <summary>
        /// Очистить
        /// </summary>
        internal void Clear()
        {
            //Снимаем отметки со всех типов
            foreach (Control control in main_tableLayoutPanel.Controls)
                if (control is RadioButton)
                    (control as RadioButton).Checked = false;
        }
    }
}
