﻿namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    partial class ResultWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileTypes_treeView = new System.Windows.Forms.TreeView();
            this.essentialFiles_listBox = new System.Windows.Forms.ListBox();
            this.redundantFiles_listBox = new System.Windows.Forms.ListBox();
            this.fileTypes_groupBox = new System.Windows.Forms.GroupBox();
            this.essentialFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.redundantFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.redunduncy_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.toRedundant_button = new System.Windows.Forms.Button();
            this.toEssential_button = new System.Windows.Forms.Button();
            this.resultsWindow_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.fileTypes_groupBox.SuspendLayout();
            this.essentialFiles_groupBox.SuspendLayout();
            this.redundantFiles_groupBox.SuspendLayout();
            this.redunduncy_tableLayoutPanel.SuspendLayout();
            this.resultsWindow_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileTypes_treeView
            // 
            this.fileTypes_treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileTypes_treeView.Location = new System.Drawing.Point(3, 16);
            this.fileTypes_treeView.Name = "fileTypes_treeView";
            this.fileTypes_treeView.Size = new System.Drawing.Size(332, 567);
            this.fileTypes_treeView.TabIndex = 0;
            this.fileTypes_treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.fileTypes_treeView_AfterSelect);
            // 
            // essentialFiles_listBox
            // 
            this.essentialFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.essentialFiles_listBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.essentialFiles_listBox.FormattingEnabled = true;
            this.essentialFiles_listBox.Location = new System.Drawing.Point(3, 16);
            this.essentialFiles_listBox.Name = "essentialFiles_listBox";
            this.essentialFiles_listBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.essentialFiles_listBox.Size = new System.Drawing.Size(332, 567);
            this.essentialFiles_listBox.TabIndex = 0;
            this.essentialFiles_listBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.redundantFiles_listBox_DrawItem);
            this.essentialFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.essentialFiles_listBox_SelectedIndexChanged);
            // 
            // redundantFiles_listBox
            // 
            this.redundantFiles_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.redundantFiles_listBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.redundantFiles_listBox.FormattingEnabled = true;
            this.redundantFiles_listBox.Location = new System.Drawing.Point(3, 16);
            this.redundantFiles_listBox.Name = "redundantFiles_listBox";
            this.redundantFiles_listBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.redundantFiles_listBox.Size = new System.Drawing.Size(332, 567);
            this.redundantFiles_listBox.TabIndex = 0;
            this.redundantFiles_listBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.redundantFiles_listBox_DrawItem);
            this.redundantFiles_listBox.SelectedIndexChanged += new System.EventHandler(this.redundantFiles_listBox_SelectedIndexChanged);
            // 
            // fileTypes_groupBox
            // 
            this.fileTypes_groupBox.Controls.Add(this.fileTypes_treeView);
            this.fileTypes_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileTypes_groupBox.Location = new System.Drawing.Point(3, 3);
            this.fileTypes_groupBox.Name = "fileTypes_groupBox";
            this.fileTypes_groupBox.Size = new System.Drawing.Size(338, 586);
            this.fileTypes_groupBox.TabIndex = 1;
            this.fileTypes_groupBox.TabStop = false;
            this.fileTypes_groupBox.Text = "Типы файлов";
            // 
            // essentialFiles_groupBox
            // 
            this.essentialFiles_groupBox.Controls.Add(this.essentialFiles_listBox);
            this.essentialFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.essentialFiles_groupBox.Location = new System.Drawing.Point(347, 3);
            this.essentialFiles_groupBox.Name = "essentialFiles_groupBox";
            this.essentialFiles_groupBox.Size = new System.Drawing.Size(338, 586);
            this.essentialFiles_groupBox.TabIndex = 2;
            this.essentialFiles_groupBox.TabStop = false;
            this.essentialFiles_groupBox.Text = "Существенные файлы";
            // 
            // redundantFiles_groupBox
            // 
            this.redundantFiles_groupBox.Controls.Add(this.redundantFiles_listBox);
            this.redundantFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.redundantFiles_groupBox.Location = new System.Drawing.Point(739, 3);
            this.redundantFiles_groupBox.Name = "redundantFiles_groupBox";
            this.redundantFiles_groupBox.Size = new System.Drawing.Size(338, 586);
            this.redundantFiles_groupBox.TabIndex = 3;
            this.redundantFiles_groupBox.TabStop = false;
            this.redundantFiles_groupBox.Text = "Избыточные файлы";
            // 
            // redunduncy_tableLayoutPanel
            // 
            this.redunduncy_tableLayoutPanel.ColumnCount = 1;
            this.redunduncy_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.redunduncy_tableLayoutPanel.Controls.Add(this.toRedundant_button, 0, 1);
            this.redunduncy_tableLayoutPanel.Controls.Add(this.toEssential_button, 0, 3);
            this.redunduncy_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.redunduncy_tableLayoutPanel.Location = new System.Drawing.Point(691, 3);
            this.redunduncy_tableLayoutPanel.Name = "redunduncy_tableLayoutPanel";
            this.redunduncy_tableLayoutPanel.RowCount = 5;
            this.redunduncy_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.28132F));
            this.redunduncy_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.redunduncy_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.437352F));
            this.redunduncy_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.redunduncy_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.28132F));
            this.redunduncy_tableLayoutPanel.Size = new System.Drawing.Size(42, 586);
            this.redunduncy_tableLayoutPanel.TabIndex = 4;
            // 
            // toRedundant_button
            // 
            this.toRedundant_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.toRedundant_button.Location = new System.Drawing.Point(3, 252);
            this.toRedundant_button.Name = "toRedundant_button";
            this.toRedundant_button.Size = new System.Drawing.Size(36, 23);
            this.toRedundant_button.TabIndex = 0;
            this.toRedundant_button.Text = ">>";
            this.toRedundant_button.UseVisualStyleBackColor = true;
            this.toRedundant_button.Click += new System.EventHandler(this.toRedundant_button_Click);
            // 
            // toEssential_button
            // 
            this.toEssential_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.toEssential_button.Location = new System.Drawing.Point(3, 311);
            this.toEssential_button.Name = "toEssential_button";
            this.toEssential_button.Size = new System.Drawing.Size(36, 23);
            this.toEssential_button.TabIndex = 1;
            this.toEssential_button.Text = "<<";
            this.toEssential_button.UseVisualStyleBackColor = true;
            this.toEssential_button.Click += new System.EventHandler(this.toEssential_button_Click);
            // 
            // resultsWindow_tableLayoutPanel
            // 
            this.resultsWindow_tableLayoutPanel.ColumnCount = 4;
            this.resultsWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.resultsWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.resultsWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.resultsWindow_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.resultsWindow_tableLayoutPanel.Controls.Add(this.fileTypes_groupBox, 0, 0);
            this.resultsWindow_tableLayoutPanel.Controls.Add(this.redundantFiles_groupBox, 3, 0);
            this.resultsWindow_tableLayoutPanel.Controls.Add(this.redunduncy_tableLayoutPanel, 2, 0);
            this.resultsWindow_tableLayoutPanel.Controls.Add(this.essentialFiles_groupBox, 1, 0);
            this.resultsWindow_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultsWindow_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.resultsWindow_tableLayoutPanel.Name = "resultsWindow_tableLayoutPanel";
            this.resultsWindow_tableLayoutPanel.RowCount = 1;
            this.resultsWindow_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.resultsWindow_tableLayoutPanel.Size = new System.Drawing.Size(1080, 592);
            this.resultsWindow_tableLayoutPanel.TabIndex = 5;
            // 
            // ResultWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.resultsWindow_tableLayoutPanel);
            this.Name = "ResultWindow";
            this.Size = new System.Drawing.Size(1080, 592);
            this.fileTypes_groupBox.ResumeLayout(false);
            this.essentialFiles_groupBox.ResumeLayout(false);
            this.redundantFiles_groupBox.ResumeLayout(false);
            this.redunduncy_tableLayoutPanel.ResumeLayout(false);
            this.resultsWindow_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox essentialFiles_listBox;
        private System.Windows.Forms.ListBox redundantFiles_listBox;
        private System.Windows.Forms.TreeView fileTypes_treeView;
        private System.Windows.Forms.GroupBox fileTypes_groupBox;
        private System.Windows.Forms.GroupBox essentialFiles_groupBox;
        private System.Windows.Forms.GroupBox redundantFiles_groupBox;
        private System.Windows.Forms.TableLayoutPanel redunduncy_tableLayoutPanel;
        private System.Windows.Forms.Button toRedundant_button;
        private System.Windows.Forms.Button toEssential_button;
        private System.Windows.Forms.TableLayoutPanel resultsWindow_tableLayoutPanel;
    }
}
