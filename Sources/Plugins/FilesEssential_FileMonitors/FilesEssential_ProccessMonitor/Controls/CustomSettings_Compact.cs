﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    /// <summary>
    /// форма упрощенных настроек
    /// </summary>
    public partial class ProtocolListCustomSettings_CompactControl : UserControl, IA.Plugin.Settings
    {
        private Protocol selectedProtocolLocal;
        private Protocol selectedProtocol
        {
            get { return selectedProtocolLocal; }
            set
            {
                if (value == null)
                {
                    protocolFilePath_textBoxWithButton.Text = String.Empty;
                    protocolFileAccessPath_textBoxWithButton.Text = String.Empty;
                    protocolTypesControl1.Type = 0;
                    prefixList_textBox.Text = String.Empty;

                    protocolFilePath_textBoxWithButton.Enabled = protocolFileAccessPath_textBoxWithButton.Enabled = dateTimePicker1.Enabled = protocolTypesControl1.Enabled = prefixList_textBox.Enabled = false;
                }
                else
                {
                    protocolFilePath_textBoxWithButton.Enabled = protocolFileAccessPath_textBoxWithButton.Enabled = dateTimePicker1.Enabled = protocolTypesControl1.Enabled = prefixList_textBox.Enabled = true;

                    protocolFilePath_textBoxWithButton.Text = value.Path;
                    protocolFileAccessPath_textBoxWithButton.Text = value.PathAccess;
                    dateTimePicker1.Value = value.logStartDateTime;
                    protocolTypesControl1.Type = value.Type;
                    split_ProtocolFilePaths.Panel2Collapsed = value.Type != Protocol.enType.LS;
                    prefixList_textBox.Text = value.Prefixes.Select(p => p.Prefix).Aggregate(String.Empty, (s, c) => s += c + Environment.NewLine);
                }
                selectedProtocolLocal = value;
            }
        }

        private string[] newLineSeparator;
        private IEnumerable<char> badPathChars;
        private char badCharReplace;
        private ProtocolTypesControl protocolTypesControl1;
        private bool suspendEvents;

        /// <summary>
        /// конструктор
        /// </summary>
        public ProtocolListCustomSettings_CompactControl()
        {
            InitializeComponent();

            Init();
        }

        private void Init()
        {
            protocolTypesControl1 = new ProtocolTypesControl();
            protocolType_groupBox.Controls.Add(protocolTypesControl1);
            protocolTypesControl1.Dock = DockStyle.Fill;
            protocolTypesControl1.RadioButtonClick += protocolTypesControl1_Click;
            protocolTypesControl1.RadioButtonClick += protocol_textBox_Leave;

            split_ProtocolFilePaths.Panel2Collapsed = true;

            protocolFilePath_textBoxWithButton.TextBox.TextChanged += textBox_TextChanged;
            protocolFileAccessPath_textBoxWithButton.TextBox.TextChanged += textBox_TextChanged;

            protocolFilePath_textBoxWithButton.Leave += protocol_textBox_Leave;
            protocolFileAccessPath_textBoxWithButton.Leave += protocol_textBox_Leave;
            prefixList_textBox.Leave += protocol_textBox_Leave;
            protocolTypesControl1.Leave += protocol_textBox_Leave;

            selectedProtocol = null;
            newLineSeparator = new string[] { Environment.NewLine };
            badPathChars = System.IO.Path.GetInvalidPathChars().Where(c => !Environment.NewLine.Contains(c));
            badCharReplace = '$';
            suspendEvents = false;
        }

        //хак для оперативного отображения внесённых изменений
        void protocol_textBox_Leave(object sender, EventArgs e)
        {
            protocolList_listBox.DisplayMember = "1";
            protocolList_listBox.DisplayMember = "";
        }

        /// <summary>
        /// Набор протоколов
        /// </summary>
        internal List<Protocol> Protocols
        {
            get
            {
                //Возвращаем все протоколы из списка
                return protocolList_listBox.Items.Cast<Protocol>().ToList();
            }
            set
            {
                //Очищаем список протоколов
                protocolList_listBox.Items.Clear();

                //Заполняем список протоколов
                if (value != null)
                    protocolList_listBox.Items.AddRange(value.Cast<object>().ToArray());
            }
        }

        private void protocolList_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (protocolList_listBox.SelectedIndex == -1)
            {
                selectedProtocol = null;
                return;
            }

            suspendEvents = true;
            selectedProtocol = protocolList_listBox.SelectedItem as Protocol;
            suspendEvents = false;
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            if (selectedProtocol == null || suspendEvents)
                return;

            TextBox txtbx = sender as TextBox;
            if (txtbx == null)
                return;

            foreach (var c in badPathChars)
                if (txtbx.Text.Contains(c))
                {
                    txtbx.Text = txtbx.Text.Replace(c, badCharReplace);
                    Monitor.Log.Warning(PluginSettings.Name, String.Format("Во введённой строке символ {0} был заменён на {1}", c, badCharReplace));
                }

            if (txtbx == prefixList_textBox)
            {
                var prefixes = new List<Protocol.Pair>();
                prefixes.AddRange(prefixList_textBox.Text.Split(newLineSeparator, StringSplitOptions.RemoveEmptyEntries).Select(s => new Protocol.Pair() { Prefix = s, Subway = String.Empty }));
                selectedProtocol.Prefixes = prefixes;
                return;
            }

            if (txtbx == protocolFilePath_textBoxWithButton.TextBox)
            {
                selectedProtocol.Path = txtbx.Text;
                return;
            }

            if (txtbx == protocolFileAccessPath_textBoxWithButton.TextBox)
            {
                selectedProtocol.PathAccess = txtbx.Text;
                return;
            }
        }

        private void protocolTypesControl1_Click(object sender, EventArgs e)
        {
            if (selectedProtocol == null || suspendEvents)
                return;

            if (protocolTypesControl1.Type != selectedProtocol.Type)
            {
                selectedProtocol.Type = protocolTypesControl1.Type;

                split_ProtocolFilePaths.Panel2Collapsed = selectedProtocol.Type != Protocol.enType.LS;                
            }
        }

        private void protocolAdd_button_Click(object sender, EventArgs e)
        {
            protocolList_listBox.Items.Add((object)(new Protocol()));
            protocolList_listBox.SelectedIndex = protocolList_listBox.Items.Count - 1;
        }

        private void protocolRemove_button_Click(object sender, EventArgs e)
        {
            if (protocolList_listBox.SelectedIndex == -1)
                return;
            protocolList_listBox.Items.Remove(protocolList_listBox.SelectedItem);
        }


        void Plugin.Settings.Save()
        {
            PluginSettings.ProtocolCollection = this.Protocols;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (selectedProtocol == null || suspendEvents)
                return;
            selectedProtocol.logStartDateTime = dateTimePicker1.Value;
        }
    }
}
