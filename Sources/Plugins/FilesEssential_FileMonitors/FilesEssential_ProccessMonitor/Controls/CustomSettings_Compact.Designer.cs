﻿namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    partial class ProtocolListCustomSettings_CompactControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.protocolList_groupBox = new System.Windows.Forms.GroupBox();
            this.protocolList_listBox = new System.Windows.Forms.ListBox();
            this.addRemoveButtons_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.protocolAdd_button = new System.Windows.Forms.Button();
            this.protocolRemove_button = new System.Windows.Forms.Button();
            this.protocolType_groupBox = new System.Windows.Forms.GroupBox();
            this.prefixList_groupBox = new System.Windows.Forms.GroupBox();
            this.prefixList_textBox = new System.Windows.Forms.TextBox();
            this.split_ProtocolFilePaths = new System.Windows.Forms.SplitContainer();
            this.pathToProtocolFile_groupBox = new System.Windows.Forms.GroupBox();
            this.protocolFilePath_textBoxWithButton = new IA.Controls.TextBoxWithDialogButton();
            this.tbllayout = new System.Windows.Forms.TableLayoutPanel();
            this.pathToProtocolFileAccess_groupBox = new System.Windows.Forms.GroupBox();
            this.protocolFileAccessPath_textBoxWithButton = new IA.Controls.TextBoxWithDialogButton();
            this.grpDateTime = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.main_tableLayoutPanel.SuspendLayout();
            this.protocolList_groupBox.SuspendLayout();
            this.addRemoveButtons_tableLayoutPanel.SuspendLayout();
            this.prefixList_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.split_ProtocolFilePaths)).BeginInit();
            this.split_ProtocolFilePaths.Panel1.SuspendLayout();
            this.split_ProtocolFilePaths.Panel2.SuspendLayout();
            this.split_ProtocolFilePaths.SuspendLayout();
            this.pathToProtocolFile_groupBox.SuspendLayout();
            this.tbllayout.SuspendLayout();
            this.pathToProtocolFileAccess_groupBox.SuspendLayout();
            this.grpDateTime.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.protocolList_groupBox, 0, 0);
            this.main_tableLayoutPanel.Controls.Add(this.addRemoveButtons_tableLayoutPanel, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.protocolType_groupBox, 0, 3);
            this.main_tableLayoutPanel.Controls.Add(this.prefixList_groupBox, 0, 4);
            this.main_tableLayoutPanel.Controls.Add(this.split_ProtocolFilePaths, 0, 2);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 6;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(1058, 348);
            this.main_tableLayoutPanel.TabIndex = 0;
            // 
            // protocolList_groupBox
            // 
            this.protocolList_groupBox.Controls.Add(this.protocolList_listBox);
            this.protocolList_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.protocolList_groupBox.Location = new System.Drawing.Point(3, 3);
            this.protocolList_groupBox.Name = "protocolList_groupBox";
            this.protocolList_groupBox.Size = new System.Drawing.Size(1052, 56);
            this.protocolList_groupBox.TabIndex = 0;
            this.protocolList_groupBox.TabStop = false;
            this.protocolList_groupBox.Text = "Список протоколов на обработку";
            // 
            // protocolList_listBox
            // 
            this.protocolList_listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.protocolList_listBox.FormattingEnabled = true;
            this.protocolList_listBox.Location = new System.Drawing.Point(3, 16);
            this.protocolList_listBox.Name = "protocolList_listBox";
            this.protocolList_listBox.Size = new System.Drawing.Size(1046, 37);
            this.protocolList_listBox.TabIndex = 0;
            this.protocolList_listBox.SelectedIndexChanged += new System.EventHandler(this.protocolList_listBox_SelectedIndexChanged);
            // 
            // addRemoveButtons_tableLayoutPanel
            // 
            this.addRemoveButtons_tableLayoutPanel.ColumnCount = 2;
            this.addRemoveButtons_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.addRemoveButtons_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.addRemoveButtons_tableLayoutPanel.Controls.Add(this.protocolAdd_button, 0, 0);
            this.addRemoveButtons_tableLayoutPanel.Controls.Add(this.protocolRemove_button, 1, 0);
            this.addRemoveButtons_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addRemoveButtons_tableLayoutPanel.Location = new System.Drawing.Point(3, 65);
            this.addRemoveButtons_tableLayoutPanel.Name = "addRemoveButtons_tableLayoutPanel";
            this.addRemoveButtons_tableLayoutPanel.RowCount = 1;
            this.addRemoveButtons_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.addRemoveButtons_tableLayoutPanel.Size = new System.Drawing.Size(1052, 36);
            this.addRemoveButtons_tableLayoutPanel.TabIndex = 1;
            // 
            // protocolAdd_button
            // 
            this.protocolAdd_button.Dock = System.Windows.Forms.DockStyle.Left;
            this.protocolAdd_button.Location = new System.Drawing.Point(3, 3);
            this.protocolAdd_button.Name = "protocolAdd_button";
            this.protocolAdd_button.Size = new System.Drawing.Size(150, 30);
            this.protocolAdd_button.TabIndex = 0;
            this.protocolAdd_button.Text = "Добавить протокол";
            this.protocolAdd_button.UseVisualStyleBackColor = true;
            this.protocolAdd_button.Click += new System.EventHandler(this.protocolAdd_button_Click);
            // 
            // protocolRemove_button
            // 
            this.protocolRemove_button.Dock = System.Windows.Forms.DockStyle.Right;
            this.protocolRemove_button.Location = new System.Drawing.Point(899, 3);
            this.protocolRemove_button.Name = "protocolRemove_button";
            this.protocolRemove_button.Size = new System.Drawing.Size(150, 30);
            this.protocolRemove_button.TabIndex = 1;
            this.protocolRemove_button.Text = "Удалить протокол";
            this.protocolRemove_button.UseVisualStyleBackColor = true;
            this.protocolRemove_button.Click += new System.EventHandler(this.protocolRemove_button_Click);
            // 
            // protocolType_groupBox
            // 
            this.protocolType_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.protocolType_groupBox.Location = new System.Drawing.Point(3, 160);
            this.protocolType_groupBox.Name = "protocolType_groupBox";
            this.protocolType_groupBox.Size = new System.Drawing.Size(1052, 50);
            this.protocolType_groupBox.TabIndex = 3;
            this.protocolType_groupBox.TabStop = false;
            this.protocolType_groupBox.Tag = "disableable";
            this.protocolType_groupBox.Text = "Тип протокола";
            // 
            // prefixList_groupBox
            // 
            this.prefixList_groupBox.Controls.Add(this.prefixList_textBox);
            this.prefixList_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prefixList_groupBox.Location = new System.Drawing.Point(3, 216);
            this.prefixList_groupBox.Name = "prefixList_groupBox";
            this.prefixList_groupBox.Size = new System.Drawing.Size(1052, 66);
            this.prefixList_groupBox.TabIndex = 4;
            this.prefixList_groupBox.TabStop = false;
            this.prefixList_groupBox.Tag = "disableable";
            this.prefixList_groupBox.Text = "Префиксы";
            // 
            // prefixList_textBox
            // 
            this.prefixList_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prefixList_textBox.Location = new System.Drawing.Point(3, 16);
            this.prefixList_textBox.Multiline = true;
            this.prefixList_textBox.Name = "prefixList_textBox";
            this.prefixList_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.prefixList_textBox.Size = new System.Drawing.Size(1046, 47);
            this.prefixList_textBox.TabIndex = 0;
            this.prefixList_textBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // split_ProtocolFilePaths
            // 
            this.split_ProtocolFilePaths.Dock = System.Windows.Forms.DockStyle.Fill;
            this.split_ProtocolFilePaths.Location = new System.Drawing.Point(3, 107);
            this.split_ProtocolFilePaths.Name = "split_ProtocolFilePaths";
            // 
            // split_ProtocolFilePaths.Panel1
            // 
            this.split_ProtocolFilePaths.Panel1.Controls.Add(this.pathToProtocolFile_groupBox);
            // 
            // split_ProtocolFilePaths.Panel2
            // 
            this.split_ProtocolFilePaths.Panel2.Controls.Add(this.tbllayout);
            this.split_ProtocolFilePaths.Panel2MinSize = 470;
            this.split_ProtocolFilePaths.Size = new System.Drawing.Size(1052, 47);
            this.split_ProtocolFilePaths.SplitterDistance = 578;
            this.split_ProtocolFilePaths.TabIndex = 5;
            // 
            // pathToProtocolFile_groupBox
            // 
            this.pathToProtocolFile_groupBox.Controls.Add(this.protocolFilePath_textBoxWithButton);
            this.pathToProtocolFile_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pathToProtocolFile_groupBox.Location = new System.Drawing.Point(0, 0);
            this.pathToProtocolFile_groupBox.Name = "pathToProtocolFile_groupBox";
            this.pathToProtocolFile_groupBox.Size = new System.Drawing.Size(578, 47);
            this.pathToProtocolFile_groupBox.TabIndex = 2;
            this.pathToProtocolFile_groupBox.TabStop = false;
            this.pathToProtocolFile_groupBox.Tag = "disableable";
            this.pathToProtocolFile_groupBox.Text = "Путь до файла протокола";
            // 
            // protocolFilePath_textBoxWithButton
            // 
            this.protocolFilePath_textBoxWithButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.protocolFilePath_textBoxWithButton.Location = new System.Drawing.Point(3, 16);
            this.protocolFilePath_textBoxWithButton.MaximumSize = new System.Drawing.Size(0, 24);
            this.protocolFilePath_textBoxWithButton.MinimumSize = new System.Drawing.Size(150, 24);
            this.protocolFilePath_textBoxWithButton.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.protocolFilePath_textBoxWithButton.Name = "protocolFilePath_textBoxWithButton";
            this.protocolFilePath_textBoxWithButton.Size = new System.Drawing.Size(572, 24);
            this.protocolFilePath_textBoxWithButton.TabIndex = 0;
            // 
            // tbllayout
            // 
            this.tbllayout.ColumnCount = 2;
            this.tbllayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbllayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tbllayout.Controls.Add(this.pathToProtocolFileAccess_groupBox, 0, 0);
            this.tbllayout.Controls.Add(this.grpDateTime, 1, 0);
            this.tbllayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbllayout.Location = new System.Drawing.Point(0, 0);
            this.tbllayout.Name = "tbllayout";
            this.tbllayout.RowCount = 1;
            this.tbllayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tbllayout.Size = new System.Drawing.Size(470, 47);
            this.tbllayout.TabIndex = 0;
            // 
            // pathToProtocolFileAccess_groupBox
            // 
            this.pathToProtocolFileAccess_groupBox.Controls.Add(this.protocolFileAccessPath_textBoxWithButton);
            this.pathToProtocolFileAccess_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pathToProtocolFileAccess_groupBox.Location = new System.Drawing.Point(3, 3);
            this.pathToProtocolFileAccess_groupBox.Name = "pathToProtocolFileAccess_groupBox";
            this.pathToProtocolFileAccess_groupBox.Size = new System.Drawing.Size(314, 41);
            this.pathToProtocolFileAccess_groupBox.TabIndex = 3;
            this.pathToProtocolFileAccess_groupBox.TabStop = false;
            this.pathToProtocolFileAccess_groupBox.Tag = "disableable";
            this.pathToProtocolFileAccess_groupBox.Text = "Путь до файла протокола последнего времени доступа";
            // 
            // protocolFileAccessPath_textBoxWithButton
            // 
            this.protocolFileAccessPath_textBoxWithButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.protocolFileAccessPath_textBoxWithButton.Location = new System.Drawing.Point(3, 16);
            this.protocolFileAccessPath_textBoxWithButton.MaximumSize = new System.Drawing.Size(0, 24);
            this.protocolFileAccessPath_textBoxWithButton.MinimumSize = new System.Drawing.Size(150, 24);
            this.protocolFileAccessPath_textBoxWithButton.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.protocolFileAccessPath_textBoxWithButton.Name = "protocolFileAccessPath_textBoxWithButton";
            this.protocolFileAccessPath_textBoxWithButton.Size = new System.Drawing.Size(308, 24);
            this.protocolFileAccessPath_textBoxWithButton.TabIndex = 0;
            // 
            // grpDateTime
            // 
            this.grpDateTime.Controls.Add(this.dateTimePicker1);
            this.grpDateTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDateTime.Location = new System.Drawing.Point(323, 3);
            this.grpDateTime.Name = "grpDateTime";
            this.grpDateTime.Size = new System.Drawing.Size(144, 41);
            this.grpDateTime.TabIndex = 4;
            this.grpDateTime.TabStop = false;
            this.grpDateTime.Tag = "disableable";
            this.grpDateTime.Text = "Дата начала";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dateTimePicker1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(3, 16);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(138, 20);
            this.dateTimePicker1.TabIndex = 0;
            this.dateTimePicker1.Tag = "";
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // ProtocolListCustomSettings_CompactControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.MinimumSize = new System.Drawing.Size(320, 275);
            this.Name = "ProtocolListCustomSettings_CompactControl";
            this.Size = new System.Drawing.Size(1058, 348);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.protocolList_groupBox.ResumeLayout(false);
            this.addRemoveButtons_tableLayoutPanel.ResumeLayout(false);
            this.prefixList_groupBox.ResumeLayout(false);
            this.prefixList_groupBox.PerformLayout();
            this.split_ProtocolFilePaths.Panel1.ResumeLayout(false);
            this.split_ProtocolFilePaths.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.split_ProtocolFilePaths)).EndInit();
            this.split_ProtocolFilePaths.ResumeLayout(false);
            this.pathToProtocolFile_groupBox.ResumeLayout(false);
            this.tbllayout.ResumeLayout(false);
            this.pathToProtocolFileAccess_groupBox.ResumeLayout(false);
            this.grpDateTime.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.GroupBox protocolList_groupBox;
        private System.Windows.Forms.ListBox protocolList_listBox;
        private System.Windows.Forms.TableLayoutPanel addRemoveButtons_tableLayoutPanel;
        private System.Windows.Forms.Button protocolAdd_button;
        private System.Windows.Forms.Button protocolRemove_button;
        private System.Windows.Forms.GroupBox pathToProtocolFile_groupBox;
        private IA.Controls.TextBoxWithDialogButton protocolFilePath_textBoxWithButton;
        private System.Windows.Forms.GroupBox protocolType_groupBox;
        private System.Windows.Forms.GroupBox prefixList_groupBox;
        private System.Windows.Forms.TextBox prefixList_textBox;
        private System.Windows.Forms.SplitContainer split_ProtocolFilePaths;
        private System.Windows.Forms.GroupBox pathToProtocolFileAccess_groupBox;
        private Controls.TextBoxWithDialogButton protocolFileAccessPath_textBoxWithButton;
        private System.Windows.Forms.TableLayoutPanel tbllayout;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox grpDateTime;
    }
}
