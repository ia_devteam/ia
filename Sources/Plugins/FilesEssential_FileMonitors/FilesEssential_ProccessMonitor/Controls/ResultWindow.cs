﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using Store;

namespace IA.Plugins.Analyses.FilesEssential_FileMonitors
{
    internal partial class ResultWindow : UserControl
    {
        //Хранилище, откуда мы будем брать информацию
        Storage storage;

        HashSet<IFile> essenFilesFromProjects;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="storage">Хранилище</param>
        internal ResultWindow(Storage storage)
        {
            this.storage = storage;

            //essenFilesFromProjects = GetCompileVStudFilesFromSuperluos();

            //foreach (IFile f in essenFilesFromProjects)
            //{
            //    f.FileEssential = enFileEssential.ESSENTIAL;
            //}

            InitializeComponent();

            //Очищаем все списки
            fileTypes_treeView.Nodes.Clear();
            essentialFiles_listBox.Items.Clear();
            redundantFiles_listBox.Items.Clear();

            //Делаем клавиши редактирования неактивными
            toEssential_button.Enabled = false;
            toRedundant_button.Enabled = false;

            //Заполняем списки резаультатами
            FillPluginResultsTree();
        }

        /// <summary>
        /// Проверка на то, является ли файл избыточным или нет
        /// </summary>
        /// <param name="file">Файл</param>
        /// <returns>True - избыточен; false - иначе.</returns>
        private bool IsEssential(IFile file)
        {
            return file.FileEssential == enFileEssential.ESSENTIAL;
            //||
            //          (file.FileEssential == enFileEssential.SUGGEST &&
            //            (
            //              file.FileExistence == enFileExistence.EXISTS_AND_CHANGED ||
            //            file.FileExistence == enFileExistence.EXISTS_AND_UNCHANGED ||
            //          file.FileExistence == enFileExistence.EXISTS_AND_UNKNOWN)
            //    );
        }

        /// <summary>
        /// Заполнить дерево с результатами работы плагина
        /// </summary>
        private void FillPluginResultsTree()
        {
            TreeNode nodeSources = new TreeNode();
            nodeSources.Text = "Файлы, содержащие исходные тексты";
            nodeSources.Name = "Файлы, содержащие исходные тексты";
            nodeSources.Tag = (enTypeOfContents.TEXTSOURCES | enTypeOfContents.BINARYSOURCES);
            fileTypes_treeView.Nodes.Add(nodeSources);

            TreeNode nodeExes = new TreeNode();
            nodeExes.Text = "Исполняемые файлы";
            nodeExes.Name = "Исполняемые файлы";
            nodeExes.Tag = enTypeOfContents.EXECUTABLE;
            fileTypes_treeView.Nodes.Add(nodeExes);

            TreeNode nodeData = new TreeNode();
            nodeData.Text = "Файлы с данными";
            nodeData.Name = "Файлы с данными";
            nodeData.Tag = enTypeOfContents.DATA;
            fileTypes_treeView.Nodes.Add(nodeData);

            TreeNode nodeUnknown = new TreeNode();
            nodeUnknown.Text = "Нераспознанные файлы";
            nodeUnknown.Name = "Нераспознанные файлы";
            nodeUnknown.Tag = 0;
            fileTypes_treeView.Nodes.Add(nodeUnknown);

            foreach (IFile file in storage.files.EnumerateFiles(enFileKind.fileWithPrefix))
            {
                //Если файл не распознан, то добавляем его к нераспознанным
                if (file.fileType == SpecialFileTypes.UNKNOWN)
                {
                    AddNode(nodeUnknown, file, file.Extension);
                    continue;
                }

                enTypeOfContents type = file.fileType.Contains();
                string typeDescription = file.fileType.ShortTypeDescription();

                if ((type & enTypeOfContents.BINARYSOURCES) == enTypeOfContents.BINARYSOURCES)
                    AddNode(nodeSources, file, typeDescription);
                if ((type & enTypeOfContents.TEXTSOURCES) == enTypeOfContents.TEXTSOURCES)
                    AddNode(nodeSources, file, typeDescription);
                if ((type & enTypeOfContents.EXECUTABLE) == enTypeOfContents.EXECUTABLE)
                    AddNode(nodeExes, file, typeDescription);
                if ((type & enTypeOfContents.DATA) == enTypeOfContents.DATA)
                    AddNode(nodeData, file, typeDescription);
                if ((type == 0) || (type == enTypeOfContents.DONTCONSIDERREGISTER))
                {
                    AddNode(nodeData, file, typeDescription);
                    AddNode(nodeExes, file, typeDescription);
                    AddNode(nodeSources, file, typeDescription);
                }
            }

            fileTypes_treeView.Sort();
        }

        //2016.11.10 Гарифулин М. Код работает криво, нужен конкретный анализ и фикс. Отключено.
        ///// <summary>
        ///// Нахождение в списке избыточных файлов, файлов необходимых при компиляции
        ///// </summary>
        ///// <returns></returns>
        //  HashSet<IFile> GetCompileVStudFilesFromSuperluos()
        //{
        //    List<IFile> vcproj= storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Where(f=>f.Extension=="vcproj").ToList();
        //    List<IFile> vcxproj= storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Where(f=>f.Extension=="vcxproj").ToList();
        //    List<IFile> csproj= storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Where(f=>f.Extension=="csproj").ToList();
            
        //    List<IFile> superluosFiles = storage.files.EnumerateFiles(enFileKind.anyFile)
        //        .Where(x => x.FileEssential != enFileEssential.ESSENTIAL).ToList();

        //    string dir = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_ORIGINAL);

        //    List<IFile> CompileFiles = new List<IFile>();
        //    foreach (IFile file in vcxproj)
        //    {
        //        IA.SlnResolver.VCX.VCXProjectReader reader = new IA.SlnResolver.VCX.VCXProjectReader();
        //        IA.SlnResolver.VCX.ProjectBase proj;
        //        if ((proj = reader.ReadProject(Path.Combine(dir,file.RelativeFileName_Canonized)))!=null)
        //            CompileFiles.AddRange(GetVCXFilesPaths(proj));
        //    }
        //    foreach (IFile file in vcproj)
        //    {
        //        IA.SlnResolver.VC.ProjectBase proj;
        //        IA.SlnResolver.VC.VCProjectReader reader = new IA.SlnResolver.VC.VCProjectReader();
        //        if ((proj = reader.ReadProject(Path.Combine(dir,file.RelativeFileName_Canonized)))!=null)
        //            CompileFiles.AddRange(GetVCFilesPaths(proj));
        //    }
        //    foreach (IFile file in csproj)
        //    {
        //        IA.SlnResolver.CS.CSProjectReader reader = new IA.SlnResolver.CS.CSProjectReader();
        //        IA.SlnResolver.CS.ProjectBase proj;
        //        if ((proj = reader.ReadProject(Path.Combine(dir,file.FullFileName_Original)))!=null)
        //            CompileFiles.AddRange(GetCSFilesPaths(proj));
        //    }

        //    List<IFile> files = CompileFiles.Intersect(superluosFiles).ToList();
                        
        //    HashSet<IFile> restoringFiles = new HashSet<IFile>(files);
        //    return restoringFiles;
        //}

        ///// <summary>
        ///// Получение списка компилируемых файлов VCX-проекта в хранилище
        ///// </summary>
        ///// <param name="project">структура проекта</param>
        ///// <returns>список компилируемых файлов</returns>
        //List<IFile> GetVCXFilesPaths(IA.SlnResolver.VCX.ProjectBase project)
        //{
        //    List<IFile> Ifiles = new List<IFile>();
        //    Ifiles.AddRange(FindFiles(project.SourceFiles, project.Path));
        //    Ifiles.AddRange(FindFiles(project.ResourceFiles, project.Path));
        //    Ifiles.AddRange(FindFiles(project.HeaderFiles, project.Path));
        //    return Ifiles;
        //}
        ///// <summary>
        ///// Получение списка компилируемых файлов VC-проекта в хранилище
        ///// </summary>
        ///// <param name="project">структура проекта</param>
        ///// <returns>список компилируемых файлов</returns>
        //List<IFile> GetVCFilesPaths(IA.SlnResolver.VC.ProjectBase project)
        //{
        //    List<IFile> Ifiles = new List<IFile>();

        //    Ifiles.AddRange(FindFiles(project.SourceFiles, project.Path));
        //    Ifiles.AddRange(FindFiles(project.ResourceFiles, project.Path));
        //    Ifiles.AddRange(FindFiles(project.HeaderFiles, project.Path));
        //    return Ifiles;
        //}
        ///// <summary>
        ///// Получение списка компилируемых файлов CS-проекта в хранилище
        ///// </summary>
        ///// <param name="project">структура проекта</param>
        ///// <returns>список компилируемых файлов</returns>
        //List<IFile> GetCSFilesPaths(IA.SlnResolver.CS.ProjectBase project)
        //{
        //    List<IFile> Ifiles = new List<IFile>();
        //    foreach (var file in project.SourceFiles)
        //    {
        //        string r = (Path.GetFullPath((project.Path.Replace(Path.GetFileName(project.Path), Path.Combine(file.Location, file.Name)))));
        //        IFile p = storage.files.Find(r);

        //        if (p != null)
        //        {
        //            Ifiles.Add(p);
        //        }
        //    }

        //    foreach (var file in project.ResourceFiles)
        //    {
        //        string r = (Path.GetFullPath((project.Path.Replace(Path.GetFileName(project.Path), Path.Combine(file.Location, file.Name)))));
        //        IFile p = storage.files.Find(r);
        //    }
        //    return Ifiles;
        //}
        
        /////Поиск файлов в хранилище по пути
        //private List<IFile> FindFiles (List<IA.SlnResolver.VC.ProjectSourceFile> files , string project)
        //{
        //    List<IFile> Ifiles = new List<IFile>();
        //    foreach (var file in files)
        //    {
        //        string fullPath = (Path.GetFullPath((project.Replace(Path.GetFileName(project), Path.Combine(file.Location, file.Name)))));
        //        IFile Ifile;
        //        if((Ifile=storage.files.Find(fullPath))!=null)              
        //            Ifiles.Add(Ifile);
        //    }
        //    return Ifiles;
        //}
        /////Поиск файлов в хранилище по пути
        //private List<IFile> FindFiles (List<IA.SlnResolver.VCX.ProjectSourceFile> files, string project)
        //{
        //    List<IFile> Ifiles = new List<IFile>();
        //    foreach (var file in files)
        //    {
        //        string fullPath = (Path.GetFullPath((project.Replace(Path.GetFileName(project), Path.Combine(file.Location,file.Name)))));
        //        IFile Ifile;
        //        if ((Ifile = storage.files.Find(fullPath)) != null)
        //            Ifiles.Add(Ifile);
        //    }
        //    return Ifiles;
        //}
      
    

         

        /// <summary>
        /// Добавляем элемент дерева типов файлов
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="file"></param>
        /// <param name="nodeName"></param>
        private static void AddNode(TreeNode parent, IFile file, string nodeName)
        {
            TreeNode[] nodes = parent.Nodes.Find(nodeName, true);

            if (nodes.Length > 1)
                throw new Exception();

            if (nodes.Length == 1)
                ((List<IFile>)nodes[0].Tag).Add(file);
            else
                parent.Nodes.Add(new TreeNode() { Name = nodeName, Text = nodeName, Tag = new List<IFile>() { file } });
        }

        /// <summary>
        /// Обновить содержимое списков файлов
        /// </summary>
        /// <param name="node"></param>
        private void RefreshFileLists(TreeNode node)
        {
            if (node == null)
                throw new Exception();

            //Очищаем
            essentialFiles_listBox.Items.Clear();
            redundantFiles_listBox.Items.Clear();

            //Заполняем
            List<IFile> files = (List<IFile>)node.Tag;
            foreach (IFile file in files)
            {
                if (IsEssential(file))
                    essentialFiles_listBox.Items.Add(file);
                else
                    redundantFiles_listBox.Items.Add(file);
            }
        }

        /// <summary>
        /// Что делать, когда выбран тип файлов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileTypes_treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            essentialFiles_listBox.SelectedIndex = -1;
            redundantFiles_listBox.SelectedIndex = -1;

            //Если мы выбрали то, что надо
            if (fileTypes_treeView.SelectedNode != null && fileTypes_treeView.SelectedNode.Level == 1)
            {
                RefreshFileLists(fileTypes_treeView.SelectedNode);
                //Если мы выбрали корневой элемент, то ничего не отображать
                if (fileTypes_treeView.SelectedNode.Level == 0)
                {
                    essentialFiles_listBox.Items.Clear();
                    redundantFiles_listBox.Items.Clear();
                }
            }
        }

        /// <summary>
        /// Перемещение файла в избыточные
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toRedundant_button_Click(object sender, EventArgs e)
        {
            if (essentialFiles_listBox.SelectedItems == null || essentialFiles_listBox.SelectedItems.Count == 0)
                return;

            foreach (object item in essentialFiles_listBox.SelectedItems)
            {
                IFile file = item as IFile;

                if (file == null)
                    throw new Exception();

                file.FileEssential = enFileEssential.SUPERFLUOUS;
            }

            essentialFiles_listBox.SelectedIndex = -1;
            redundantFiles_listBox.SelectedIndex = -1;

            RefreshFileLists(fileTypes_treeView.SelectedNode);
        }

        /// <summary>
        /// Перемещение файла в существенные
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toEssential_button_Click(object sender, EventArgs e)
        {
            if (redundantFiles_listBox.SelectedItems == null || redundantFiles_listBox.SelectedItems.Count == 0)
                return;

            foreach (object item in redundantFiles_listBox.SelectedItems)
            {
                IFile file = item as IFile;

                if (file == null)
                    throw new Exception();

                file.FileEssential = enFileEssential.ESSENTIAL;
            }

            essentialFiles_listBox.SelectedIndex = -1;
            redundantFiles_listBox.SelectedIndex = -1;

            RefreshFileLists(fileTypes_treeView.SelectedNode);
        }

        /// <summary>
        /// Обработчик - выбрале элемент в списке избыточных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void redundantFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            toEssential_button.Enabled = redundantFiles_listBox.SelectedIndices.Count != 0;
        }

        /// <summary>
        /// Обработчик - выбрали элемент в списке существенных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void essentialFiles_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            toRedundant_button.Enabled = essentialFiles_listBox.SelectedIndices.Count != 0;
        }

        /// <summary>
        /// Обработчик - отрисовка элемента
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void redundantFiles_listBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1)
                return;

            e.DrawBackground();

            IFile file = (IFile)((ListBox)sender).Items[e.Index];

            if (file == null)
                return; //some rare boggus occassions

            Color color;
            switch (file.FileEssential)
            {
                case enFileEssential.SUPERFLUOUS:
                    color = essenFilesFromProjects.Contains(file) ? Color.Violet : Color.White; 
                    break;
                case enFileEssential.UNKNOWN:
                    color = Color.Red;
                    break;
                case enFileEssential.SUGGEST:
                    color = Color.Yellow;
                    break;
                default: 
                    color = Color.White;
                    break;
            }

            //Brush currentBrush = Brushes.Black;
            if ((e.State & DrawItemState.Selected) != 0)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.FromKnownColor(KnownColor.Highlight)), e.Bounds);
                ControlPaint.DrawFocusRectangle(e.Graphics, e.Bounds);
            }
            else
                e.Graphics.FillRectangle(new SolidBrush(color), e.Bounds);

            e.Graphics.DrawString(file.RelativeFileName_Original, e.Font, Brushes.Black, e.Bounds, StringFormat.GenericDefault);



        }
    }
}
