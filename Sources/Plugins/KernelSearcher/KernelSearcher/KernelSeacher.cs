﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Management;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;

using Store;
using IA.Extensions;
using IOController;

namespace IA.Plugins.Analyses.KernelSearcher
{
    /// <summary>
    /// Основной класс контроля отсутствия перезаписи BIOS
    /// </summary>
    internal class KernelSeacher_logic
    {
        /// <summary>
        /// Список "плохих" файлов
        /// </summary>
        List<string> badFiles;

        /// <summary>
        /// Список бинарных файлов
        /// </summary>
        List<IFile> files;

        /// <summary>
        /// Директория с файлами, нужна для создания временных папок и файлов
        /// </summary>
        string initialDirectory;

        /// <summary>
        /// Каталог с отчетами
        /// </summary>
        string reportDirectory;

        /// <summary>
        /// Словарь сопоставлений "файл-дамп файла"
        /// </summary>
        Dictionary<string, string> filesRelations;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="storage">Текущее хранилище.</param>
        internal KernelSeacher_logic(Store.Storage storage)
        {            
            this.files = storage.files.EnumerateFiles(enFileKind.fileWithPrefix).Where(file => file.fileType.Contains().HasFlag(enTypeOfContents.EXECUTABLE)).ToList();
            this.initialDirectory = storage.appliedSettings.FileListPath;
            this.badFiles = new List<string>();
            filesRelations = new Dictionary<string, string>();
            this.reportDirectory = Path.Combine(storage.pluginData.GetDataFolder(PluginSettings.ID), "DumpedFiles");
            Directory.CreateDirectory(this.reportDirectory);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_DUMPING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)this.files.Count);
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.DUMPED_FILES_CHECKING.Description(), Monitor.Tasks.Task.UpdateType.STAGES, (ulong)this.files.Count);
        }
        
        /// <summary>
        /// Удаление дочернего процесса из системы
        /// </summary>
        /// <param name="childProcessId">ИД дочернего процесса.</param>
        /// <param name="parentProcessId">ИД родительского процесса.</param>
        private void KillChildProcess(int childProcessId, int parentProcessId)
        {
            int parentID = 0;
            using (ManagementObject mo = new ManagementObject("win32_process.handle='" + childProcessId.ToString() + "'"))
            {
                mo.Get();
                parentID = Convert.ToInt32(mo["ParentProcessId"]);
            }
            if (parentID == parentProcessId)
            {
                Process.GetProcessById(childProcessId).Kill();
            }
        }

        /// <summary>
        /// Дамп бинарных файлов
        /// </summary>
        internal void StartDump()
        {
            ulong counter = 0;
            // запускаем дамп для каждого файла
            foreach (IFile fileName in this.files)
            {

                //if (fileName.FileNameForReports == "")
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.FILES_DUMPING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);

                // Формируем структуру папок
                string inlineOutput = Path.Combine(this.reportDirectory, Path.GetDirectoryName(fileName.FullFileName_Original).Substring(this.initialDirectory.Length - 1));
                if (!Directory.Exists(inlineOutput))
                    Directory.CreateDirectory(inlineOutput);

                string temp = Path.Combine(this.reportDirectory, counter.ToString() + ".txt");
                File.Create(temp).Close();

                // информация о процессе
                Process p = new Process()
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = "cmd",
                        Arguments = string.Format("/C Common\\dumpbin.exe /all \"" + fileName.FullFileName_Original + "\" >> \"" + temp + "\""),
                        UseShellExecute = false,
                        CreateNoWindow = true
                    }
                };
                p.Start();

                //Переделал чисто из-за того, что существуют бинарники, на которых dumpbin ступорится...
                //Из-за этого - жестко убиваем dumpbin и link, которые вызвал текущий cmd, из системы
                if (!p.WaitForExit(3000))
                {
                    foreach (Process childDumpbin in Process.GetProcessesByName("dumpbin"))
                    {
                        int childDumpbinId = childDumpbin.Id;
                        KillChildProcess(childDumpbinId, p.Id);
                        foreach (Process childLink in Process.GetProcessesByName("link"))
                        {
                            int childLinkId = childLink.Id;
                            KillChildProcess(childLinkId, childDumpbinId);
                        }
                    }
                    Thread.Sleep(1000);
                }

                p.Close();

                using (StreamReader reader = new StreamReader(temp))
                {
                    string path = Path.Combine(this.reportDirectory, fileName.FullFileName_Original.Substring(this.initialDirectory.Length));
                    if (!System.IO.Directory.Exists(path.Substring(0, path.LastIndexOf("\\"))))
                    {
                        System.IO.Directory.CreateDirectory(path.Substring(0, path.LastIndexOf("\\")));
                    }

                    string fullDumpFilePath = path + ".dump";
                    using (StreamWriter writer = new StreamWriter(fullDumpFilePath, false, Encoding.Default))
                    {
                        writer.Write(reader.ReadToEnd());
                    }

                    filesRelations.Add(fullDumpFilePath, fileName.FullFileName_Original);
                }

                File.Delete(temp);
            }

            counter = 0;
            // пробегаемся по отчетам
            foreach (string file in Directory.EnumerateFiles(this.reportDirectory, "*.dump", SearchOption.AllDirectories))
            {
                Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.Tasks.DUMPED_FILES_CHECKING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, ++counter);
                if (System.IO.File.Exists(file))
                {
                    CheckDump(file);
                }

            }
        }
        
        /// <summary>
        /// Проверяем файлы дампа
        /// </summary>
        /// <param name="tempFile">Путь до созданного файла дампа</param>
        private void CheckDump(string tempFile)
        {
            bool needAdd_sub = false, needAdd_dll = false, needAdd_file = false;
            using (StreamReader sr = new StreamReader(tempFile, Encoding.Default))
            {
                while (!sr.EndOfStream)
                {
                    string st = sr.ReadLine();
                    if (st.IndexOf("subsystem (Native)") != -1)
                        if (Int32.Parse(Regex.Match(st, "\\d").Value) == 1)
                            needAdd_sub = true;
                    if (st.IndexOf("DLL characteristics") != -1)
                        if (Int32.Parse(Regex.Match(st, "\\d").Value) == 0)
                            needAdd_dll = true;
                    if (st.IndexOf("ntoskrnl.exe") != -1 || st.IndexOf("hal.dll") != -1 || st.IndexOf("ntkrnlmp.exe") != -1 ||
                        st.IndexOf("ntkrnlpa.exe") != -1 || st.IndexOf("ntkrpamp.exe") != -1)
                        needAdd_file = true;
                }
            }
            if (needAdd_sub && needAdd_dll && needAdd_file)
                this.badFiles.Add(filesRelations[tempFile]);            
        }
        
        /// <summary>
        /// Генерация отчетов
        /// </summary>
        internal void GenerateReports(string reportPath)
        {
            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.ReportTasks.REPORTS_SAVING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 1);
            
            // Копируем из папки отчетов плагина в папку отчетов, которую выбрал пользователь
            DirectoryController.Copy(this.reportDirectory, Path.Combine(reportPath, "DumpedFiles"), true);

            // Создаем протокол Report.txt
            using (StreamWriter writer = new StreamWriter(Path.Combine(reportPath, "DumpedFiles", "Report.txt"), false, Encoding.Default))
            {
                if (this.badFiles.Count > 0)
                {
                    writer.WriteLine("Найдено " + this.badFiles.Count + " файл(-а,-ов), функционирующих в режиме ядра:\n");
                    foreach (string file in this.badFiles)
                        writer.WriteLine(file.ToLower());
                }
                else
                    writer.WriteLine("Файлы, функционирующие в режиме ядра, не найдены.");
            }

            Monitor.Tasks.Update(PluginSettings.Name, PluginSettings.ReportTasks.REPORTS_SAVING.Description(), Monitor.Tasks.Task.UpdateType.STAGE, 2);
        }
    }
}
