﻿using System.IO;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using IA.Extensions;
using TestUtils;

namespace TestKernelSearcher
{
    [TestClass]
    public class TestKernelSearcher
    {
        private const string SAMPLES_SUB_DIRECTORY_PATH = "KernelSearcher";
        private const string SOURCES_SUB_DIRECTORY_PATH = "Sources";

        private string samplesDirectoryPath;
        private string sourcesDirectoryPath;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
            samplesDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, SAMPLES_SUB_DIRECTORY_PATH);
            sourcesDirectoryPath = Path.Combine(testUtils.TestMatirialsDirectoryPath, SOURCES_SUB_DIRECTORY_PATH);
        }
                
        /// <summary>
        /// Проверка корректности отработки плагина на пустой папке.
        /// </summary>
        [TestMethod]
        public void KernelSearcher_EmptyFolder()
        {
            const string sourcePrefixPart = @"EmptyFolder";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.KernelSearcher.KernelSearcher(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    if (!Directory.Exists(Path.Combine(samplesDirectoryPath, sourcePrefixPart)))
                        Directory.CreateDirectory(Path.Combine(samplesDirectoryPath, sourcePrefixPart));
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(samplesDirectoryPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);

                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    CheckFilesCount(reportsPath, Path.Combine(samplesDirectoryPath, sourcePrefixPart));

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки плагина на файлах, имеющих тип, который не содержит исполняемый код.
        /// Типы файлов в Хранилище задаются вручную.
        /// </summary>
        [TestMethod]
        public void KernelSearcher_AnotherType()
        {
            const string sourcePrefixPart = @"Javascript";

            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.KernelSearcher.KernelSearcher(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(sourcesDirectoryPath, sourcePrefixPart));
                    TestUtilsClass.Storage_Files_SetType(storage, new FileType("Текстовый файл"));
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return Directory.EnumerateFiles(reportsPath, "*.dump", SearchOption.AllDirectories).Count() == 0;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }
        
        /// <summary>
        /// Проверка корректности отработки плагина на файлах, не содержащих исполняемый код, но отмеченные в Хранилище как исполняемые
        /// Тип файлов в Хранилище задаются вручную
        /// </summary>
        [TestMethod]
        public void KernelSearcher_NotExecutable()
        {
            const string sourcePrefixPart = @"Javascript";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.KernelSearcher.KernelSearcher(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(sourcesDirectoryPath, sourcePrefixPart));
                    storage.files.EnumerateFiles(enFileKind.fileWithPrefix).ForEach(f => f.fileType = new FileType("Executable File"));
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    string tempDirectoryPath = Path.Combine(reportsPath, "Temp");
                    string reportsDirectoryPath = Path.Combine(samplesDirectoryPath, "NotExecutable");
                    string dumpedFilesDirectoryPath = Path.Combine(reportsPath, "DumpedFiles");
                    PrepareReports(tempDirectoryPath, reportsDirectoryPath);

                    CheckReports(tempDirectoryPath, dumpedFilesDirectoryPath);
                    CheckFilesCount(dumpedFilesDirectoryPath, Path.Combine(sourcesDirectoryPath, sourcePrefixPart));

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка корректности отработки плагина на файлах с исполняемым кодом.
        /// Сравнение эталонных отчетов с новыми.
        /// Также проверяется, что количество отчетов совпадает с количеством файлов с исполняемым кодом.
        /// </summary>
        [TestMethod]
        public void KernelSearcher_Reports()
        {
            const string sourcePrefixPart = @"Binaries";

            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.KernelSearcher.KernelSearcher(),            // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, Path.Combine(sourcesDirectoryPath, sourcePrefixPart));
                    TestUtilsClass.Run_IdentifyFileTypes(storage);
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    string tempDirectoryPath = Path.Combine(reportsPath, "Temp");
                    string reportsDirectoryPath = Path.Combine(samplesDirectoryPath, "Reports");
                    string dumpedFilesDirectoryPath = Path.Combine(reportsPath, "DumpedFiles");
                    PrepareReports(tempDirectoryPath, reportsDirectoryPath);

                    CheckReports(tempDirectoryPath, dumpedFilesDirectoryPath);
                    CheckFilesCount(dumpedFilesDirectoryPath, Path.Combine(sourcesDirectoryPath, sourcePrefixPart));

                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка количества сгенерированных отчетов и количество проверяемых файлов.
        /// </summary>
        /// <param name="reportsPath">Путь к отчетам. Не может быть Null.</param>
        /// <param name="testMaterialsPath">Путь к материалам. Не может быть Null.</param>
        private void CheckFilesCount(string reportsPath, string testMaterialsPath)
        {
            ulong reportFilesCount = (ulong)Directory.EnumerateFiles(reportsPath, "*.dump", SearchOption.AllDirectories).Count();
            ulong directoryFilesCount = (ulong)Directory.EnumerateFiles(testMaterialsPath, "*.*", SearchOption.AllDirectories).Count();

            Assert.IsTrue(reportFilesCount == directoryFilesCount, "Количество отчетов не совпадает с количеством файлов.");
        }

        private void PrepareReports(string tempDirectoryPath, string reportsDirectoryPath)
        {
            foreach (string file in Directory.EnumerateFiles(reportsDirectoryPath, "*.*", SearchOption.AllDirectories))
            {
                string contents = new StreamReader(file, Encoding.Default).ReadToEnd().Replace(@"d:\iatfs\tests\materials", testUtils.TestMatirialsDirectoryPath.ToLower());

                string newFileDirectoryPath = Path.Combine(tempDirectoryPath, Path.GetDirectoryName(file).Substring(reportsDirectoryPath.Length));
                if (!IOController.DirectoryController.IsExists(newFileDirectoryPath))
                    IOController.DirectoryController.Create(newFileDirectoryPath);

                string newFileFullPath = Path.Combine(newFileDirectoryPath, Path.GetFileName(file));
                using (StreamWriter writer = new StreamWriter(newFileFullPath, false, Encoding.Default))
                {
                    writer.Write(contents);
                }
            }
        }

        private void CheckReports(string tempDirectoryPath, string dumpedFilesDirectoryPath)
        {
            Assert.IsTrue(TestUtilsClass.Reports_Directory_TXT_Compare(tempDirectoryPath, dumpedFilesDirectoryPath), "Отчеты не совпадают с эталонными.");
        }
    }
}
