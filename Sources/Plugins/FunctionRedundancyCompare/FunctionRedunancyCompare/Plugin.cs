﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using Store;
using Store.Table;

namespace IA.Plugins.Analyses.FunctionRedunancyCompare
{
    using IA.AistSubsystem;

    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Сравнение протоколов избыточных функций АИСТ-С c Хранилищем";

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.FUNCTION_REDUNDANCY_COMPARE;

        internal static string AistReportDir = Properties.Settings.Default.AistReportDir;
        internal static string OldSourcesDir = Properties.Settings.Default.OldSourcesDir;
    }

    /// <summary>
    /// Плагин сравнивает результаты работы анализатора АИСТ-С и содержимого хранилища в части избыточности по функциям.
    /// </summary>
    public class FunctionRedunancyCompare : IA.Plugin.Interface
    {
        Storage storage;
        AistReportReaderSettingsControl settings;

        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get { return PluginSettings.ID; }
        }

        /// <summary>
        /// Имя плагина
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get { return PluginSettings.Name; }
        }

        /// <summary>
        /// Возможности плагина
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return
                    Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW |
                    Plugin.Capabilities.REPORTS |
                    Plugin.Capabilities.RERUN;
            }
        }

        /// <summary>
        /// Контрол настроек базового режима
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get { return null; }
        }

        /// <summary>
        /// Контрол настроек экспертного режима
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                return (settings = new AistReportReaderSettingsControl()
                    {
                        PathToAistReportDirectory = PluginSettings.AistReportDir,
                        PathToOldSourcesDirectory = PluginSettings.OldSourcesDir
                    });
            }
        }

        /// <summary>
        /// Контрол выполнения
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public UserControl RunWindow
        {
            get { return null; }
        }

        /// <summary>
        /// Контрол результата
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public UserControl ResultWindow
        {
            get { return null; }
        }

        /// <summary>
        /// Список задач при работе
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Tasks"/>
        /// </summary>
        public List<Monitor.Tasks.Task> Tasks
        {
            get { return null; }
        }

        /// <summary>
        /// Список задач при построении отчёта
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.TasksReport"/>
        /// </summary>
        public List<Monitor.Tasks.Task> TasksReport

        {
            get { return null; }
        }

        /// <summary>
        /// Процедура инициализации плагина
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.AistReportDir = Properties.Settings.Default.AistReportDir;
            PluginSettings.OldSourcesDir = Properties.Settings.Default.OldSourcesDir;
        }

        /// <summary>
        /// Загрузка настроект через Хранилище.
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            PluginSettings.AistReportDir = settingsBuffer.GetString();
            PluginSettings.OldSourcesDir = settingsBuffer.GetString();

            if (settings != null)
            {
                settings.PathToAistReportDirectory = PluginSettings.AistReportDir;
                settings.PathToOldSourcesDirectory = PluginSettings.OldSourcesDir;
            }
        }

        /// <summary>
        /// Загрузка настроек через переданную строку.
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //FIXME - плагин пока не используется в простом режиме
            //if (String.IsNullOrEmpty(settingsString) || !settingsString.Contains(';'))
            //    return;
            //string[] splitted = settingsString.Split(';');
            //if (splitted.Length < 2)
            //    return;
            //PluginSettings.AistReportDir = splitted[0];
            //PluginSettings.OldSourcesDir = splitted[1];
            //if (settings != null)
            //{
            //    settings.PathToAistReportDirectory = splitted[0];
            //    settings.PathToOldSourcesDirectory = splitted[1];
            //}

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Сохранить настройки в хранилище.
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.SaveSettings"/></param>
        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            Properties.Settings.Default.AistReportDir = PluginSettings.AistReportDir;
            Properties.Settings.Default.OldSourcesDir = PluginSettings.OldSourcesDir;
            Properties.Settings.Default.Save();

            settingsBuffer.Add(PluginSettings.AistReportDir);
            settingsBuffer.Add(PluginSettings.OldSourcesDir);
        }

        /// <summary>
        /// Сохранение результатов.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SaveResults"/>
        /// </summary>
        public void SaveResults()
        {
        }

        /// <summary>
        /// Проверка правильности настроек.
        /// </summary>
        /// <param name="message"><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></param>
        /// <returns><see cref="IA.Plugin.Interface.IsSettingsCorrect"/></returns>
        public bool IsSettingsCorrect(out string message)
        {
            message = String.Empty;

            if (settings != null)
            {
                PluginSettings.AistReportDir = settings.PathToAistReportDirectory;
                PluginSettings.OldSourcesDir = settings.PathToOldSourcesDirectory;

                if (String.IsNullOrWhiteSpace(PluginSettings.AistReportDir) ||
                    !IOController.DirectoryController.IsExists(PluginSettings.AistReportDir))
                {
                    message = "Каталог с отчётами АИСТ-С не существует.";
                    return false;
                }

                return true;
            }
            return true;
        }

        AistReportsReader report;
        /// <summary>
        /// Выполнение требуемой задачи
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Run"/>
        /// </summary>
        public void Run()
        {
            report = new AistReportsReader(PluginSettings.AistReportDir, oldFilePrefix: PluginSettings.OldSourcesDir, newFilePrefix: storage.appliedSettings.FileListPath);
            foreach (var bunch in report.EnumerateBunches())
            {
                CompareWithStorageEntities.EnrichEntitiesWithStorageInfo(storage, bunch.Functions);
                CompareWithStorageEntities.EnrichEntitiesWithStorageInfo(storage, bunch.Variables, true);
            }
            report.ToFile(storage.pluginData.GetDataFileName(ID));
        }

        /// <summary>
        /// Сформировать отчёты по работе
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.GenerateReports"/>
        /// </summary>
        /// <param name="reportsDirectoryPath"><see cref="IA.Plugin.Interface.GenerateReports"/></param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            if (report == null &&
                (report = AistReportsReader.FromFile(storage.pluginData.GetDataFileName(ID))) == null)
            {
                Monitor.Log.Error(this.Name, "Отчёт не может быть создан: отсутствуют данные для анализа.");
                return;
            }

            foreach (var bunch in report.EnumerateBunches())
            {
                if (bunch.Functions != null)
                    using (StreamWriter sw = new StreamWriter(Path.Combine(reportsDirectoryPath, bunch.Name + "_functions.txt")))
                    {
                        foreach (var entity in bunch.Functions)
                        {
                            sw.WriteLine(entity.Name);
                            sw.WriteLine("\t" + entity.EssentialReport);
                        }
                    }

                if (bunch.Variables != null)
                    using (StreamWriter sw = new StreamWriter(Path.Combine(reportsDirectoryPath, bunch.Name + "_variables.txt")))
                    {
                        foreach (var entity in bunch.Variables)
                        {
                            sw.WriteLine(entity.Name);
                            sw.WriteLine("\t" + entity.EssentialReport);
                        }
                    }
            }

            //statistics

            uint funcsTotalByAist = 0;
            uint funcsThatRedunantByAist = 0;
            uint funcsThatEssentialByAist = 0;
            uint funcsThatRedunantByAistButEssentialByStorage = 0;
            uint funcsThatEssentialByAistButRedunantByStorage = 0;
            uint funcsThatRedunantByAistAndRedunantByStorage = 0;
            uint funcsThatEssentialByAistAndEssentialByStorage = 0;
            uint funcsThatAreNotFuncsByStorage = 0;
            uint funcsThatAreUnknownByStorage = 0;
            uint varsTotalByAist = 0;
            uint varsThatAreUnknownByStorage = 0;

            foreach (var bunch in report.EnumerateBunches())
            {
                if (bunch.Functions != null)
                {
                    var funcs = bunch.Functions;

                    funcsTotalByAist += funcs.CountU();
                    funcsThatRedunantByAist += funcs.CountU(f => f.AistEssentialFlag == AistReportEntity.Essential.REDUNANT);
                    funcsThatEssentialByAist += funcs.CountU(f => f.AistEssentialFlag == AistReportEntity.Essential.ESSENTIAL);
                    funcsThatRedunantByAistButEssentialByStorage += funcs.CountU(f => f.AistEssentialFlag == AistReportEntity.Essential.REDUNANT &&
                        f.StorageEssentialFlag == AistReportEntity.Essential.ESSENTIAL);
                    funcsThatEssentialByAistButRedunantByStorage += funcs.CountU(f => f.AistEssentialFlag == AistReportEntity.Essential.ESSENTIAL &&
                        f.StorageEssentialFlag == AistReportEntity.Essential.REDUNANT);
                    funcsThatRedunantByAistAndRedunantByStorage += funcs.CountU(f => f.AistEssentialFlag == AistReportEntity.Essential.REDUNANT &&
                        f.StorageEssentialFlag == AistReportEntity.Essential.REDUNANT);
                    funcsThatEssentialByAistAndEssentialByStorage += funcs.CountU(f => f.AistEssentialFlag == AistReportEntity.Essential.ESSENTIAL &&
                        f.StorageEssentialFlag == AistReportEntity.Essential.ESSENTIAL);
                    funcsThatAreNotFuncsByStorage += funcs.CountU(f => f.StorageEssentialFlag == AistReportEntity.Essential.INFORMOBJECT);
                    funcsThatAreUnknownByStorage += funcs.CountU(f => f.StorageEssentialFlag == AistReportEntity.Essential.UNKNOWN);
                }

                if (bunch.Variables != null)
                {
                    varsTotalByAist += bunch.Variables.CountU();
                    varsThatAreUnknownByStorage += bunch.Variables.CountU(f => f.StorageEssentialFlag == AistReportEntity.Essential.UNKNOWN);
                }
            }

            using (StreamWriter sw = new StreamWriter(Path.Combine(reportsDirectoryPath, "statistics.txt")))
            {
                string toWrite;
                sw.WriteLine("Статистика сравнения:");
                sw.WriteLine("АИСТ-С обнаружил " + funcsTotalByAist.ToString() + " функциональных объектов.");
                sw.WriteLine("Из них: ");
                uint funcsThatFuncsByAustAndStorage = funcsTotalByAist - funcsThatAreNotFuncsByStorage - funcsThatAreUnknownByStorage;
                toWrite = String.Format("\tЯвляются функциональными объектами по Хранилищу: {0} ({1})", 
                    funcsThatFuncsByAustAndStorage, 
                    CompareWithStorageEntities.StringPercent(funcsThatFuncsByAustAndStorage, funcsTotalByAist));
                sw.WriteLine(toWrite);
                toWrite = String.Format("\tЯвляются информационными объектами по Хранилищу: {0} ({1})", 
                    funcsThatAreNotFuncsByStorage, 
                    CompareWithStorageEntities.StringPercent(funcsThatAreNotFuncsByStorage, funcsTotalByAist));
                sw.WriteLine(toWrite);
                toWrite = String.Format("\tНе распознаны по Хранилищу: {0} ({1})", 
                    funcsThatAreUnknownByStorage, 
                    CompareWithStorageEntities.StringPercent(funcsThatAreUnknownByStorage, funcsTotalByAist));
                sw.WriteLine(toWrite);
                toWrite = String.Format("\tПризнаны избыточными по АИСТ-С: {0} ({1})", 
                    funcsThatRedunantByAist, 
                    CompareWithStorageEntities.StringPercent(funcsThatRedunantByAist, funcsTotalByAist));
                sw.WriteLine(toWrite);
                toWrite = String.Format("\t\tИзбыточность подтверждена по Хранилищу: {0} ({1})", 
                    funcsThatRedunantByAistAndRedunantByStorage, 
                    CompareWithStorageEntities.StringPercent(funcsThatRedunantByAist, funcsThatRedunantByAistAndRedunantByStorage));
                sw.WriteLine(toWrite);
                toWrite = String.Format("\t\tИзбыточность опровергнута по Хранилищу: {0} ({1})", 
                    funcsThatRedunantByAistButEssentialByStorage, 
                    CompareWithStorageEntities.StringPercent(funcsThatRedunantByAist, funcsThatRedunantByAistButEssentialByStorage));
                sw.WriteLine(toWrite);
                toWrite = String.Format("\tПризнаны значимыми по АИСТ-С: {0} ({1})", 
                    funcsThatEssentialByAist, 
                    CompareWithStorageEntities.StringPercent(funcsThatEssentialByAist, funcsTotalByAist));
                sw.WriteLine(toWrite);
                toWrite = String.Format("\t\tЗначимость подтверждена по Хранилищу: {0} ({1})", 
                    funcsThatEssentialByAistAndEssentialByStorage, 
                    CompareWithStorageEntities.StringPercent(funcsThatEssentialByAistAndEssentialByStorage, funcsThatEssentialByAist));
                sw.WriteLine(toWrite);
                toWrite = String.Format("\t\tЗначимость опровергнута по Хранилищу: {0} ({1})", 
                    funcsThatEssentialByAistButRedunantByStorage, 
                    CompareWithStorageEntities.StringPercent(funcsThatEssentialByAistButRedunantByStorage, funcsThatEssentialByAist));
                sw.WriteLine(toWrite);
                toWrite = String.Format("АИСТ-С обнаружил {0} информационных объектов.", 
                    varsTotalByAist);
                sw.WriteLine(toWrite);
                sw.WriteLine("Из них: ");
                uint varsThatKnownbyStorage = varsTotalByAist - varsThatAreUnknownByStorage;
                toWrite = String.Format("\tЯвляются информационными объектами по Хранилищу: {0} ({1})", 
                    varsThatKnownbyStorage, 
                    CompareWithStorageEntities.StringPercent(varsThatKnownbyStorage, varsTotalByAist));
                sw.WriteLine(toWrite);
                toWrite = String.Format("\tНе распознаны по Хранилищу: {0} ({1})", 
                    varsThatAreUnknownByStorage, 
                    CompareWithStorageEntities.StringPercent(varsThatAreUnknownByStorage, varsTotalByAist));
                sw.WriteLine(toWrite);
            }
        }

        /// <summary>
        /// Остановить выполнение.
        /// </summary>
        /// <summary> 
        /// <see cref="IA.Plugin.Interface.StopRunning"/>
        /// </summary>
        public void StopRunning()
        {
        }
    }

    static class ListExtension
    {
        public static uint CountU(this System.Collections.IList list)
        {
            return (uint)list.Count;
        }

        public static uint CountU<T>(this IList<T> list, Func<T, bool> predicate)
        {
            return (uint)list.Count(predicate);
        }
    }

}

