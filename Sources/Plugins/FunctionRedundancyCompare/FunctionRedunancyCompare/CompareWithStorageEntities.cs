﻿using System;
using System.Collections.Generic;
using System.Linq;

using Store;

namespace IA.Plugins.Analyses.FunctionRedunancyCompare
{
    using IA.AistSubsystem;

    internal static class CompareWithStorageEntities
    {
        /// <summary>
        /// Дополнить информацию АИСТ-С информацией из Хранилища.
        /// </summary>
        /// <param name="storage">Хранилище, откуда доставать информацию.</param>
        /// <param name="listOfAistEntities">Список сущностей отчёта АИСТ-С.</param>
        /// <param name="onlyCheckVars">Флаг сверки только со списком переменных в Хранилище.</param>
        public static void EnrichEntitiesWithStorageInfo(Storage storage, List<AistReportEntity> listOfAistEntities, bool onlyCheckVars = false)
        {
            if (listOfAistEntities == null)
                return;
            foreach (var entity in listOfAistEntities)
            {
                string[] shortAistName = new string[] { entity.Name };
                string funcSourceFile = Files.CanonizeFileName(entity.SourceFile);
                Store.IFunction storFunc = null;
                if (!onlyCheckVars)
                    storFunc = storage.functions.FindFunction(shortAistName) //отобрать все функции хранилища, которые
                        .Where(f => (f.Definition() != null) && //имеют определение
                            //и это определение
                            String.Equals(f.Definition().GetFile().FullFileName_Canonized, funcSourceFile, StringComparison.OrdinalIgnoreCase) && //содержится в искомом файле
                            IsInEpsilonRange((int)f.Definition().GetLine(), entity.AistLineNumber, 2) //и примерно на той строчке
                          ).FirstOrDefault();
                if (storFunc != null)
                {
                    entity.StorageEntityName = storFunc.FullNameForReports;
                    entity.StorageLineNumber = (int)storFunc.Definition().GetLine();
                    if (storFunc.Essential == enFunctionEssentials.ESSENTIAL ||
                        storFunc.EssentialUnderstand == enFunctionEssentialsUnderstand.ESSENTIAL)
                    {
                        entity.StorageEssentialFlag = AistReportEntity.Essential.ESSENTIAL;
                        IFunctionCall calledByLocation = storFunc.CalledBy().FirstOrDefault();
                        if (calledByLocation != null)
                        {
                            entity.CallPlaceFileName = calledByLocation.CallLocation.GetFile().FullFileName_Original;
                            entity.CallPlaceLineNumber = (int)calledByLocation.CallLocation.GetLine();
                            entity.CallPlaceFunctionName = calledByLocation.Caller.FullNameForReports;
                        }
                        else
                        {
                            entity.CallPlaceFileName = "Не определено.";
                            entity.CallPlaceLineNumber = -1;
                            entity.CallPlaceFunctionName = "Не определено.";
                        }
                    }
                    else
                        entity.StorageEssentialFlag = AistReportEntity.Essential.REDUNANT;
                }
                else
                {
                    entity.StorageEssentialFlag = AistReportEntity.Essential.UNKNOWN;
                    //попробуем найти переменную с таким именем
                    var storVariablesWithSameName = storage.variables.FindVariable(shortAistName).Where(v => v.Definitions().Length != 0);
                    bool stopIterations = false;
                    foreach (var v in storVariablesWithSameName)
                    {
                        //мегамассив со всеми вхождениями даннйо переменной - АИСТ может за функцию посчитать любой вызов...
                        List<Store.Location> vardirs = new List<Store.Location>();
                        vardirs.AddRange(v.Definitions());
                        vardirs.AddRange(v.ValueCallPosition());
                        vardirs.AddRange(v.ValueGetPosition());
                        vardirs.AddRange(v.ValueSetPosition());
                        foreach (var location in vardirs)
                        {
                            if (funcSourceFile == location.GetFile().FullFileName_Canonized &&
                                IsInEpsilonRange((int)location.GetLine(), entity.AistLineNumber, 2))
                            {
                                entity.StorageEssentialFlag = AistReportEntity.Essential.INFORMOBJECT;
                                entity.CallPlaceFileName = location.GetFile().FullFileName_Original;
                                entity.CallPlaceLineNumber = (int)location.GetLine();
                                entity.CallPlaceFunctionName = location.GetFunctionId() == Store.Const.CONSTS.WrongID ?
                                                         @"не определено" :
                                                        storage.functions.GetFunction(location.GetFunctionId()).FullNameForReports;
                                stopIterations = true;
                                break;
                            }
                        }
                        if (stopIterations)
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Считать числа равными, если они находятся в эпсилон-окрестности друг-друга (нестрого).
        /// </summary>
        /// <param name="first">Первое число.</param>
        /// <param name="second">Второе число.</param>
        /// <param name="epsilon">Эпсилон-окрестность.</param>
        /// <returns>true - если находятся достаточно близко; false - если находятся достаточно далеко.</returns>
        static internal bool IsInEpsilonRange(int first, int second, int epsilon)
        {
            return Math.Abs(first - second) <= epsilon;
        }

        /// <summary>
        /// Получить строку, представляющую проценты как результат деления первого на второе с заданной точностью.
        /// </summary>
        /// <param name="divizer">Что делим.</param>
        /// <param name="divider">На что делим.</param>
        /// <param name="precision">Точность представления результата.</param>
        /// <returns>Строка вида num.precision%</returns>
        static internal string StringPercent(uint divizer, uint divider, uint precision = 1)
        {
            double result = 0;
            if (divider != 0)
            {
                result = (divizer * 1d) / divider;
            }
            return result.ToString("P" + precision);
        }

        static internal string StringPercent(ulong divizer, int divider, uint precision = 1)
        {
            return StringPercent((uint)divizer, divider, precision);
        }
    }



}
