﻿using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using IA.Plugins.Analyses.FunctionRedunancyCompare;
using TestUtils;

using IOController;
using Store.Table;

namespace TestFunctionRedunancyCompare
{
    [TestClass]
    public class TestFunctionRedunancyCompare
    {
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        ulong pluginID = Store.Const.PluginIdentifiers.FUNCTION_REDUNDANCY_COMPARE;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Сравнение с результатами импорта плагина UnderstandImpoter
        /// </summary>
        [TestMethod]
        public void FunctionRedunancyCompare_UnderstandImporter()
        {
            const string understandReportsPrefixPart = @"Understand\CS_Sensors\SimpleFuncCalls";
            const string origSourcePath = @"@#$%^&*"; 
            string sourcePostfixPart = @"CS_Sensors\SimpleFuncCalls";

            FunctionRedunancyCompare testPlugin = new FunctionRedunancyCompare();

            testUtils.RunTest(
                sourcePostfixPart,                                          //sourcePostfixPart
                testPlugin,                                                 //plugin
                false,                                                      //isUseCachedStorage
                (storage, testMaterialsPath) => { },                       //prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage);
                    TestUtilsClass.Run_UnderstandImporter(storage, Path.Combine(testMaterialsPath, understandReportsPrefixPart), origSourcePath, IsLoadVariables: true);

                    IBufferWriter writer = WriterPool.Get();

                    writer.Add(Path.Combine(testMaterialsPath, @"FunctionRedunancyCompare\AistReports", testUtils.TestName));
                    writer.Add(Path.Combine(@"d:\IATFS\Tests\Materials", sourcePostfixPart));

                    storage.pluginSettings.SaveSettings(pluginID, writer);

                },                                                          //customizeStorage
                (storage, testMaterialsPath) => { return true; },           //checkStorage
                (reportsFullPath, testMaterialsPath) => 
                {
                    string standartReportsDirectoryPath = Path.Combine(testMaterialsPath, @"FunctionRedunancyCompare\Reports", testUtils.TestName);
                    string specifiedStandartReportsDirectoryPath = Path.GetFullPath(Path.Combine(reportsFullPath, @"..\Temp"));
                    GetSpecifiedStandartReportsCopy(standartReportsDirectoryPath, specifiedStandartReportsDirectoryPath);

                    Assert.IsTrue(TestUtilsClass.Reports_Directory_TXT_Compare(reportsFullPath, specifiedStandartReportsDirectoryPath), "Отчеты не совпадают с эталонными.");

                    return true; 
                },                                                          //checkreports
                false,                                                      //isUpdateReport
                (plugin, testMaterialsPath) => { },                         //changeSettingsBeforRerun
                (storage, testMaterialsPath) => { return true; },           //checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }        //checkReportAfterRerun
                );    
        }

        /// <summary>
        /// Получение копии эталонных отчётов с измененёнными путями для тестирования на конкретной машине.
        /// </summary>
        /// <param name="standartReportsDirectoryPath">Путь к директории с эталонными отчетами. Не может быть Null.</param>
        /// <param name="specifiedStandartReportsDirectoryPath">Путь к директории с копией эталонных отчётов с подменёнными путями. Не может быть Null.</param>
        private void GetSpecifiedStandartReportsCopy(string standartReportsDirectoryPath, string specifiedStandartReportsDirectoryPath)
        {
            string testMatirialsDirectoryLowerCasePath = testUtils.TestMatirialsDirectoryPath.ToLower();

            foreach (string file in Directory.EnumerateFiles(standartReportsDirectoryPath, "*.*", SearchOption.AllDirectories))
            {
                string contents = new StreamReader(file, Encoding.Default).ReadToEnd().Replace("d:\\ia5\\git\\tests\\materials", testMatirialsDirectoryLowerCasePath);

                string newFileDirectoryPath = Path.Combine(specifiedStandartReportsDirectoryPath, Path.GetDirectoryName(file).Substring(standartReportsDirectoryPath.Length));
                if (!DirectoryController.IsExists(newFileDirectoryPath))
                    DirectoryController.Create(newFileDirectoryPath);

                string newFilePath = Path.Combine(newFileDirectoryPath, Path.GetFileName(file));
                using (StreamWriter writer = new StreamWriter(newFilePath, false, Encoding.Default))
                    writer.Write(contents);
            }
        }
    }
}
