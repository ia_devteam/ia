﻿using System.Collections.Generic;
using System.Linq;

namespace IA.Sql.DatabaseConnections
{
    /// <summary>
    /// Класс-обёртка для работы с базой данных DllKnowledgeBase
    /// </summary>
    public class DllKnowledgeBaseDatabaseConnection : IA.Sql.DatabaseConnection
    {
        /// <summary>
        /// Имя БД с которой ведётся работа
        /// </summary>
        public new const string DatabaseName = "DllKnowledgeBase";

        private List<byte[]> images = null;

        #region Свойства
        /// <summary>
        /// Выгрузка из базы таблицы DllMD5
        /// </summary>
        public List<string[]> DllBase
        {
            get
            {
                return this.ExecuteReader("select * from DllMD5");
            }
        }

        /// <summary>
        /// Выгрузка из базы таблицы Functions
        /// </summary>
        public List<string[]> Functions
        {
            get
            {
                return this.ExecuteReader("select * from Functions");
            }
        }

        /// <summary>
        /// Выгрузка из базы таблицы Imports
        /// </summary>
        public List<string[]> Imports
        {
            get
            {
                return this.ExecuteReader("select * from Imports");
            }
        }
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        public DllKnowledgeBaseDatabaseConnection() : base(DatabaseName)
        {
        }

        /// <summary>
        /// Выгрузка из базы таблицы записи для определенной библиотеки
        /// </summary>
        /// <param name="DllID">идентификатор библиотеки</param>
        /// <returns>результирующая таблица</returns>
        public List<string[]> DllBaseData(int DllID)
        {
            return this.ExecuteReader("select * from DllMD5 where DllID=@Param1", new string[] { DllID.ToString() });
        }

        /// <summary>
        /// Выгрузка из базы таблицы Functions для определенной библиотеки
        /// </summary>
        /// <param name="DllID">идентификатор библиотеки</param>
        /// <returns>результирующая таблица</returns>
        public List<string[]> DllFunctions(int DllID)
        {
            return this.ExecuteReader("select * from Functions where DllMd5ID=@Param1", new string[] { DllID.ToString() });
        }

        /// <summary>
        /// Выгрузка из базы таблицы Imports для определенной библиотеки
        /// </summary>
        /// <param name="DllID">идентификатор библиотеки</param>
        /// <returns>результирующая таблица</returns>
        public List<string[]> DllImports(int DllID)
        {
            return this.ExecuteReader("select * from Imports where DllMd5ID=@Param1", new string[] { DllID.ToString() });
        }

        /// <summary>
        /// Выгрузка из базы бинарного файла библиотеки
        /// </summary>
        /// <param name="DllID">идентификатор библиотеки</param>
        /// <returns>байтовый массив</returns>
        public byte[] DllImage(int DllID)
        {
            images = this.SelectBlob("select Dll_Image from DllImage where ID=@Param1", new string[] { DllID.ToString() });
            return images.Count > 0 ? images[0] : new byte[0];
        }

        /// <summary>
        /// Каскадное удаление из базы по идентификатору библиотеки
        /// </summary>
        /// <param name="DllID"></param>
        public void DeleteDllFromBase(int DllID)
        {
            string[] sParams = new string[] { DllID.ToString() };
            this.ExecuteNonQuery("delete from DllImage where ID=@Param1", sParams);
            this.ExecuteNonQuery("delete from Functions where DllMd5ID=@Param1", sParams);
            this.ExecuteNonQuery("delete from Imports where DllMd5ID=@Param1", sParams);
            this.ExecuteNonQuery("delete from DllMD5 where DllID=@Param1", sParams);
        }

        /// <summary>
        /// Удаление функций по идентификатору библиотеки
        /// </summary>
        /// <param name="DllID">идентификатор</param>
        public void DeleteDllFunctions(int DllID)
        {
            this.ExecuteNonQuery("delete from Functions where DllMd5ID=@Param1", new string[] { DllID.ToString() });
        }

        /// <summary>
        /// Удаление одной функции по ее идентификатору
        /// </summary>
        /// <param name="ID">идентификатор</param>
        public void DeleteOneFunction(int ID)
        {
            this.ExecuteNonQuery("delete from Functions where ID=@Param1", new string[] { ID.ToString() });
        }

        /// <summary>
        /// Удаление образа библиотеки по идентификатору библиотеки
        /// </summary>
        /// <param name="DllID">идентификатор</param>
        public void DeleteDllImage(int DllID)
        {
            this.ExecuteNonQuery("delete from DllImage where ID=@Param1", new string[] { DllID.ToString() });
        }

        /// <summary>
        /// Удаление из таблицы импортируемых функций по идентификатору библиотеки
        /// </summary>
        /// <param name="DllID">идентификатор</param>
        public void DeleteDllImports(int DllID)
        {
            this.ExecuteNonQuery("delete from Imports where DllMd5ID=@Param1", new string[] { DllID.ToString() });
        }

        /// <summary>
        /// Удаление одной импортируемой функции по ее идентификатору
        /// </summary>
        /// <param name="ID">идентификатор</param>
        public void DeleteOneImport(int ID)
        {
            this.ExecuteNonQuery("delete from Imports where ID=@Param1", new string[] { ID.ToString() });
        }

        /// <summary>
        /// Занесение в базу характеристик библиотеки
        /// </summary>
        /// <param name="dlldata">параметры библиотеки</param>
        /// <param name="image">образ библиотеки</param>
        /// <param name="functions">список экспортируемых функций</param>
        /// <param name="imports">список импортируемых функций</param>
        public void InsertDllToBase(string[] dlldata, byte[] image, List<string[]> functions, List<string[]> imports)
        {

            //Занесение характеристик DLL в базу и получение последнего идентификатора
            string sID = this.ExecuteScalar("insert into DllMD5 (DllName,DllAuthor,DllDescription,DllCheckSum,DllVersion,TypeOfDll) values(@Param1,@Param2,@Param3,@Param4,@Param5,@Param6); select IDENT_CURRENT('DllMD5');", dlldata);
            if(image!=null)
                this.InsertBlob("insert into DllImage (ID,Dll_Image) values(@Param1,@Image)", image, new string[] { sID });

            //Занесение в базу списка экспортируемых функций
            if(functions!=null)
                foreach (string[] sa in functions)
                    this.ExecuteNonQuery("insert into Functions (DllMd5ID,Name,Descr,OrdNum,TypeOfFunc) values(@Param1,@Param2,@Param3,@Param4,@Param5)", new string[] { sID, sa[0], sa[1], sa[2], sa[3] });

            //Занесение в базу списка импортируемых функций
            if (imports != null)
                foreach (string[] sa in imports)
                    this.ExecuteNonQuery("insert into Imports (DllMd5ID,Dll_Name,Func_Name,Descr,OrdNum,TypeOfFunc) values(@Param1,@Param2,@Param3,@Param4,@Param5,@Param6)", new string[] { sID, sa[0], sa[1], sa[2], sa[3], sa[4]});

        }

        /// <summary>
        /// Изменение типа библиотеки
        /// </summary>
        /// <param name="ID">идентификатор</param>
        /// <param name="TypeOf">тип</param>
        public void UpdateTypeOfDll(string ID, string TypeOf)
        {
            this.ExecuteNonQuery("update DllMD5 set TypeOfDll=@Param2 where DllID=@Param1", new string[] { ID, TypeOf });
        }

        /// <summary>
        /// Изменение типа экспортируемой функции
        /// </summary>
        /// <param name="ID">идентификатор</param>
        /// <param name="TypeOf">тип</param>
        public void UpdateTypeOfFunction(string ID, string TypeOf)
        {
            this.ExecuteNonQuery("update Functions set TypeOfFunc=@Param2 where ID=@Param1", new string[] { ID, TypeOf });
        }

        /// <summary>
        /// Изменение типа импортируемой функции
        /// </summary>
        /// <param name="ID">идентификатор</param>
        /// <param name="TypeOf">тип</param>
        public void UpdateTypeOfImport(string ID, string TypeOf)
        {
            this.ExecuteNonQuery("update Imports set TypeOfFunc=@Param2 where ID=@Param1", new string[] { ID, TypeOf });
        }

        /// <summary>
        /// Изменение параметров библиотеки
        /// </summary>
        /// <param name="sParams">список параметров</param>
        public void UpdateDll(string[] sParams)
        {
            this.ExecuteNonQuery("update DllMD5 set DllName=@Param2,DllAuthor=@Param3,DllDescription=@Param4,DllCheckSum=@Param5,DllVersion=@Param6,TypeOfDll=@Param7 where DllID=@Param1", sParams);
        }

        /// <summary>
        /// Изменение параметров экспортируемой функции
        /// </summary>
        /// <param name="sParams">список значений</param>
        public void UpdateFunction(string[] sParams)
        {
            this.ExecuteNonQuery("update Functions set Name=@Param2,Descr=@Param3,OrdNum=@Param4,TypeOfFunc=@Param5 where ID=@Param1", sParams);
        }

        /// <summary>
        /// Добавление функции в таблицу экспортируемых функций
        /// </summary>
        /// <param name="sParams">список значений</param>
        public void InsertFunction(string[] sParams)
        {
            this.ExecuteNonQuery("insert into Functions (DllMD5ID,Name,Descr,OrdNum,TypeOfFunc) values(@Param1,@Param2,@Param3,@Param4,@Param5)", sParams);
        }

        /// <summary>
        /// Добавление массива функции в таблицу экспортируемых функций
        /// </summary>
        /// <param name="DllID">Идентификатор библиотеки</param>
        /// <param name="sFuncs">список функций</param>
        public void InsertDllFunctions(int DllID, List<string[]> sFuncs)
        {
            foreach (string[] sa in sFuncs)
                this.ExecuteNonQuery("insert into Functions (DllMD5ID,Name,Descr,OrdNum,TypeOfFunc) values(@Param1,@Param2,@Param3,@Param4,@Param5)", new string[] { DllID.ToString() }.Concat(sa).ToArray());
        }

        /// <summary>
        /// Изменение параметров импортируемой функции
        /// </summary>
        /// <param name="sParams">список значений</param>
        public void UpdateImport(string[] sParams)
        {
            this.ExecuteNonQuery("update Imports set Dll_Name=@Param2,Func_Name=@Param3,Descr=@Param4,OrdNum=@Param5,TypeOfFunc=@Param6,Func_ID=@Param7 where DllID=@Param1", sParams);
        }

        /// <summary>
        /// Добавление функции в таблицу импортируемых функций
        /// </summary>
        /// <param name="sParams">список значений</param>
        public void InsertImport(string[] sParams)
        {
            this.ExecuteNonQuery("insert into Imports (DllMD5ID,Dll_Name,Func_Name,Descr,OrdNum,TypeOfFunc) values(@Param1,@Param2,@Param3,@Param4,@Param5,@Param6)", sParams);
        }
        /// <summary>
        /// Добавление массива импортируемых функций в таблицу импортируемых функций
        /// </summary>
        /// <param name="DllID">Идентификатор библиотеки</param>
        /// <param name="sImports">список функций</param>
        public void InsertDllImports(int DllID, List<string[]> sImports)
        {
            foreach(string[] sa in sImports)
                this.ExecuteNonQuery("insert into Imports (DllMD5ID, Dll_Name,Func_Name,Descr,OrdNum,TypeOfFunc) values(@Param1,@Param2,@Param3,@Param4,@Param5,@Param6)", new string[]{DllID.ToString()}.Concat(sa).ToArray());
        }
    }
}