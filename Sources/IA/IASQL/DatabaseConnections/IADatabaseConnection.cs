﻿using System;

namespace IA.Sql.DatabaseConnections
{
    /// <summary>
    /// Класс-обёртка для работы с базой данных ia.
    /// </summary>
    public class IADatabaseConnection : IA.Sql.DatabaseConnection
    {
        /// <summary>
        /// Имя базы данных с которой ведётся работа
        /// </summary>
        public new const string DatabaseName = "ia";

        /// <summary>
        /// Конструктор
        /// </summary>
        public IADatabaseConnection()
            : base(DatabaseName)
        {
        }

        /// <summary>
        /// Проверка версии базы данных
        /// </summary>
        /// <param name="applicationDatabaseVersion">Версия базы данных для корректной работы приложения</param>
        /// <param name="currentDatabaseVersion">Текущая версия базы данных</param>
        /// <returns></returns>
        public bool CheckVersion(uint applicationDatabaseVersion, out uint currentDatabaseVersion)
        {
            try
            {
                //Изначально версия БД не определена
                currentDatabaseVersion = 0;

                //Получаем последнюю версию БД
                string versionString = this.ExecuteScalar("SELECT MAX(version) FROM versions;");

                //Проверка на разумность
                if (String.IsNullOrWhiteSpace(versionString) || !UInt32.TryParse(versionString, out currentDatabaseVersion))
                    throw new Exception("Не удалось получить текущую версию базы данных.");

                //Проверка на разумность
                if (currentDatabaseVersion == 0)
                    throw new Exception("Неверный номер версии базы данных.");

                //Сопоставляем версии
                return currentDatabaseVersion == applicationDatabaseVersion;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при проверке версии базы данных.", ex);
            }
        }
    }
}
