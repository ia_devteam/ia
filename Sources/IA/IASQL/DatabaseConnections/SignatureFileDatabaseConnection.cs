﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IA.Sql.DatabaseConnections
{
    /// <summary>
    /// Класс-обёртка для работы с базой данных SignatureFile
    /// </summary>
    public class SignatureFileDatabaseConnection : IA.Sql.DatabaseConnection
    {
        /// <summary>
        /// Имя БД с которой ведётся работа
        /// </summary>
        public new const string DatabaseName = "SignatureFile";
            
        #region Свойства
        private static List<String[]> fileFormatCache = null; //костыль для хоть какого-то быстродействия
        /// <summary>
        /// Выгрузка из базы таблицы FileFormat
        /// </summary>
        public List<string[]> FileFormat
        {
            get
            {
                return fileFormatCache ?? (fileFormatCache = this.ExecuteReader("select * from FileFormat"));
            }
        }

        /// <summary>
        /// Выгрузка из базы таблицы Pattern
        /// </summary>
        public List<string[]> Pattern
        {
            get
            {
                return this.ExecuteReader("select * from Pattern");
            }
        }

        private List<string[]> workPlaces = null;
        /// <summary>
        /// Выгрузка из базы таблицы WorkPlaces
        /// </summary>
        public List<string[]> WorkPlaces
        {
            get
            {
                return workPlaces = this.ExecuteReader("select * from WorkPlaces");
            }
        }

        /// <summary>
        /// Выгрузка из базы таблицы WorkHistory
        /// </summary>
        public List<string[]> WorkHistory
        {
            get
            {
                return this.ExecuteReader("select * from WorkHistory");
            }
        }

        /// <summary>
        /// Выборка из базы идентификаторов таблицы FileFormat, попавших в историю
        /// </summary>
        public List<string> IDHistory
        {
            get
            {
                return this.ExecuteReader("select ID from WorkHistory where WID<>1").Select(x => x[0]).ToList();
            }
        }

        /// <summary>
        /// Выборка из базы идентификаторов таблицы WorkPlaces, попавших в историю
        /// </summary>
        public List<string> WIDHistory
        {
            get
            {
                return this.ExecuteReader("select WID from WorkHistory where WID<>1").Select(x => x[0]).ToList();
            }
        }

        private string workID = "1";
        private string workPL = "Базовое рабочее место";
        /// <summary>
        /// Установка нового рабочего места
        /// Чтение рабочего места
        /// </summary>
        public string WorkID
        {
            get
            {
                return workID;
            }
            set
            {
                workPL = value;
                workID = workPlaces.Where(x => x[1] == value).Select(x => x[0]).FirstOrDefault();
            }
        }
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        public SignatureFileDatabaseConnection() : base(DatabaseName)
        {
        }

        /// <summary>
        /// Добавление в историю пары (Идентификатор таблицы FileFormat, Идентификатор таблицы WorkPlaces)
        /// </summary>
        /// <param name="sParams">список идентификаторов</param>
        public void WorkHistoryAdd(string[] sParams)
        {
            WorkHistoryRemove(sParams);
            this.ExecuteNonQuery("insert into WorkHistory values(@Param1,@Param2)", sParams);
        }

        /// <summary>
        /// Удаление из истории идентификатора таблицы FileFormat
        /// </summary>
        /// <param name="sParams">идентификатор</param>
        public void WorkHistoryRemove(string[] sParams)
        {
            this.ExecuteNonQuery("delete from WorkHistory where ID=@Param1", sParams);
        }

        /// <summary>
        /// Вставка в таблицу FileFormat новых данных
        /// </summary>
        /// <param name="sParams">список значений</param>
        public void FileFormatInsert(string[] sParams)
        {
            this.ExecuteNonQuery("insert into FileFormat (FileFormatID,WorkID,TypeOfFile,DescriptionShort,DescriptionLarge,Extension,Language) values (@Param1,@Param2,@Param3,@Param4,@Param5,@Param6,@Param7)", sParams);
        }

        /// <summary>
        /// Максимальный (он же последний) идентификатор таблицы FileFormat
        /// </summary>
        /// <param name="sParams">код формата и идентификатор рабочего места</param>
        /// <returns>идентификатор</returns>
        public string FileFormatLastID(string[] sParams)
        {
            return this.ExecuteReader("select max(ID) from FileFormat where FileFormatID=@Param1 and WorkID=@Param2", sParams).Select(x => x[0]).FirstOrDefault();
        }

        /// <summary>
        /// Изменение записи таблицы FileFormat
        /// </summary>
        /// <param name="sParams">идентификатор и список новых значений</param>
        public void FileFormatUpdate(string[] sParams)
        {
            this.ExecuteNonQuery("update FileFormat set FileFormatID=@Param2,WorkID=@Param3,TypeOfFile=@Param4, DescriptionShort=@Param5, DescriptionLarge=@Param6, Extension=@Param7, Language=@Param8 where ID=@Param1", sParams);
        }

        /// <summary>
        /// Удаление записи в таблице FileFormat
        /// </summary>
        /// <param name="sParams">идентификатор</param>
        public void FileFormatDelete(string[] sParams)
        {
            this.ExecuteNonQuery("delete from FileFormat where ID=@Param1", sParams);
        }

        /// <summary>
        /// Вставка в таблицу Pattern новых данных
        /// </summary>
        /// <param name="sParams">список значений</param>
        public void PatternInsert(string[] sParams)
        {
            this.ExecuteNonQuery("insert into Pattern (FileFormatID,WorkID,TypeOfPattern,PatternF,PatternFValue,PatternS,PatternSValue) values (@Param1,@Param2,@Param3,@Param4,@Param5,@Param6,@Param7)", sParams);
        }

        /// <summary>
        /// Изменение записи таблицы Pattern
        /// </summary>
        /// <param name="sParams">идентификатор и список новых значений</param>
        public void PatternUpdate(string[] sParams)
        {
            this.ExecuteNonQuery("update Pattern set FileFormatID=@Param2,WorkID=@Param3,TypeOfPattern=@Param4, PatternF=@Param5, PatternFValue=@Param6, PatternS=@Param7, PatternSValue=@Param8 where PatternID=@Param1", sParams);
        }

        /// <summary>
        /// Удаление записи в таблице Pattern
        /// </summary>
        /// <param name="sParams">код FileFormat и рабочее место</param>
        public void PatternDelete(string[] sParams)
        {
            this.ExecuteNonQuery("delete from Pattern where FileFormatID=@Param1 and WorkID=@Param2", sParams);
        }

        /// <summary>
        /// Удаление записи в таблице Pattern
        /// </summary>
        /// <param name="sParams">идентификатор</param>
        public void PatternDeleteID(string[] sParams)
        {
            this.ExecuteNonQuery("delete from Pattern where PatternID=@Param1", sParams);
        }

        /// <summary>
        /// Вставка в таблицу WorkPlaces новых данных
        /// </summary>
        /// <param name="sParams">список значений</param>
        public void WorkPlacesInsert(string[] sParams)
        {
            this.ExecuteNonQuery("insert into WorkPlaces values (@Param1,@Param2,@Param3)", sParams);
        }

        /// <summary>
        /// Изменение записи таблицы WorkPlaces
        /// </summary>
        /// <param name="sParams">наименование и описание</param>
        public void WorkPlacesUpdate(string[] sParams)
        {
            this.ExecuteNonQuery("update WorkPlaces set Description=@Param2 where Name=@Param1", sParams);
        }

        /// <summary>
        /// Удаление записи в таблице WorkPlaces
        /// </summary>
        /// <param name="sParams">идентификатор</param>
        public void WorkPlacesDelete(string[] sParams)
        {
            this.ExecuteNonQuery("delete from WorkPlaces where ID=@Param1", sParams);
        }

        /// <summary>
        /// Установка рабочего места Default для всех записей Pattern, имеющих соответствующий код FileFormat и рабочее место
        /// </summary>
        /// <param name="sParams">код FileFormat и рабочее место</param>
        public void PatternToDefault(string[] sParams)
        {
            this.ExecuteNonQuery("update Pattern set WorkID=1 where FileFormatID=@Param1 and WorkID=@Param2", sParams);
        }

        /// <summary>
        /// Копирование в Default всех записей Pattern, имеющих соответствующий код FileFormat и рабочее место
        /// </summary>
        /// <param name="sParams">код FileFormat и рабочее место</param>
        public void PatternCopyToDefault(string[] sParams)
        {
            List<string[]> lr = this.ExecuteReader("select * from Pattern where FileFormatID=@Param1 and WorkID=@Param2", sParams);
            foreach (string[] sa in lr)
            {
                string[] sPar = sa.Skip(1).ToArray();
                sPar[0] = sParams[2];
                sPar[1] = "1";
                List<string[]> lt = this.ExecuteReader("select * from Pattern where FileFormatID=@Param1 and WorkID=@Param2 and TypeOfPattern=@Param3 and PatternF=@Param4 and PatternFValue=@Param5 and PatternS=@Param6 and PatternSValue=@Param7", sPar);
                if (lt.Count == 0)
                    this.ExecuteReader("insert into Pattern (FileFormatID,WorkID,TypeOfPattern,PatternF,PatternFValue,PatternS,PatternSValue) values (@Param1,@Param2,@Param3,@Param4,@Param5,@Param6,@Param7)", sPar);
            }
        }

        /// <summary>
        /// Установка рабочего места Default для записи FileFormat
        /// </summary>
        /// <param name="sParams">идентификатор</param>
        public void FileFormatToDefault(string[] sParams)
        {
            this.ExecuteNonQuery("update FileFormat set WorkID=1 where ID=@Param1", sParams);
        }
    }
}
