﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Data.Common;

using IA.Extensions;
using System.Data;

namespace IA.Sql
{

    /// <summary>
    /// Класс, реализующий соединение с произвольной базой данных SQL
    /// </summary>
    public abstract class DatabaseConnection
    {


        /// <summary>
        /// Объект соединения с базой данных
        /// </summary>
        internal DbConnection connection = null;

        /// <summary>
        /// Рузультат последней попытки соединения с базой данных
        /// </summary>
        public bool IsConnected { get; internal set; }

        /// <summary>
        /// База данных для соединения
        /// </summary>
        public string DatabaseName { get; private set; }

        /// <summary>
        /// Список ошибок, полученных в процессе выполнения запросов
        /// </summary>
        public string Error { get; private set; }

        /// <summary>
        /// Не выбрасывать исключения в случае возникновения ошибок
        /// </summary>
        public bool IsSkipExceptions { set; get; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="databaseName">Имя базы данных для соединения</param>
        public DatabaseConnection(string databaseName)
        {
            //По умполчанию исключения отключены
            this.IsSkipExceptions = false;
            this.Error = string.Empty;
            this.DatabaseName = databaseName;
            this.IsConnected = false;

            //Инициализируем соединение с базой данных
            this.InitializeConnection(
                ServerConfiguration.Name,
                ServerConfiguration.Login,
                ServerConfiguration.Password,
                ServerConfiguration.IsWindowsAuthentication
                );

            //Если настройки сервера изменяться, то переподключиться автоматически
            ServerConfiguration.ServerSettingsChangedEvent += this.InitializeConnection;
        }

        /// <summary>
        /// Инициализация соединения с БД
        /// </summary>
        /// <param name="serverName"></param>
        /// <param name="serverLogin"></param>
        /// <param name="serverPassword"></param>
        /// <param name="isWindowsAuthentication"></param>
        /// <param name="autonomy"></param>
        private void InitializeConnection(string serverName, string serverLogin, string serverPassword, bool isWindowsAuthentication)
        {
            if (System.IO.File.Exists(serverName))
            {
                this.connection = new SqlCeConnection($"Data Source={GetSqlCompactDBName(serverName, DatabaseName)};Persist Security Info=False;");
            }
            else
            {
                this.connection = new SqlConnection("Data Source=" + (serverName) + ";" +
                                                "Initial Catalog=" + this.DatabaseName + ";" +
                                                    (isWindowsAuthentication ? "Integrated Security=True; " : "Integrated Security=False;" + "User ID=" + (serverLogin) + ";" + "Password=" + (serverPassword) + ";") +
                                                        "Asynchronous Processing=True;");
            }
        }

        /// <summary>
        /// Сформировать полное имя файла с БД SqlCompact на основе имени базового файла путём замены расширения на .sdf и добавления суффикса с именем БД.
        /// </summary>
        /// <param name="basefilename">Полный путь до базового файла, рядом с которым будет сформированное полное имя файла БД.</param>
        /// <param name="dbname">Имя БД. Добавляется в суффикс имени файла.</param>
        /// <returns></returns>
        public static string GetSqlCompactDBName(string basefilename, string dbname)
        {
            return System.IO.Path.Combine(System.IO.Path.GetDirectoryName(basefilename),
                    System.IO.Path.GetFileNameWithoutExtension(basefilename) + dbname + ".sdf");
        }


        private dynamic GetCommandObject(string sql)
        {
            var result = this.connection.CreateCommand();
            result.CommandText = sql;
            return result;
        }

        /// <summary>
        /// Исполнение запроса к базе, без получения ответа
        /// </summary>
        /// <param name="sql">строка SQL</param>
        /// <param name="sParams">массив параметров для строки SQL</param>
        /// <returns>false при ошибке</returns>
        public bool ExecuteNonQuery(string sql, string[] sParams = null)
        {
            try
            {
                //Устанавка соединеня с БД
                this.connection.Open();

                //Инициализация команды запросом и параметрами соединения
                using (var command = GetCommandObject(sql))
                {
                    if (sParams != null)
                        for (int i = 0; i < sParams.Length; i++)
                            command.Parameters.AddWithValue("@Param" + (i + 1).ToString(), sParams[i]);

                    //Запуск асинхронного исполнения команды SQL
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    ex.Message,
                    "Команда SQL: " + sql
                }.ToMultiLineString();

                GenerateError(message);
                return false;
            }
            finally
            {
                //Закрываем соединение
                if (this.connection != null)
                    this.connection.Close();
            }

            return true;
        }

        /// <summary>
        /// Исполнение запроса к базе, ожидание одиночного ответа
        /// </summary>
        /// <param name="sql">строка SQL</param>
        /// <param name="sParams">массив параметров для строки SQL</param>
        /// <returns>результирующее значение</returns>
        public string ExecuteScalar(string sql, string[] sParams = null)
        {
            List<string[]> ls = ExecuteReader(sql, sParams);

            //Если запрос не был обработан, возвращаем ошибку
            if (ls == null)
                return null;

            //Если результат на запрос не удовлетворяет требуемым условиям, возвращаем ошибку
            if (ls.Count != 1 || ls[0].Length != 1)
            {
                GenerateError("Результат запроса на является скаляром" + "\n" + "Команда SQL: " + sql + "\n");
                return null;
            }

            return ls[0][0];
        }

        /// <summary>
        /// Исполнение запроса к базе, получение в ответ таблицы
        /// </summary>
        /// <param name="sql">строка SQL</param>
        /// <param name="sParams">массив параметров для строки SQL</param>
        /// <param name="replaceNullsString">Строка, которую возвращать в случае, если из БД получен NULL.</param>
        /// <returns>результирующая таблица</returns>
        public List<string[]> ExecuteReader(string sql, string[] sParams = null, string replaceNullsString = "")
        {
            //Создание результирующей таблицы
            List<string[]> ret = new List<string[]>();

            try
            {
                //Устанавка соединеня с БД
                this.connection.Open();

                //Инициализация команды запросом и параметрами соединения
                using (var command = GetCommandObject(sql))
                {
                    //Устанавка значения всех передаваемых параметров
                    if (sParams != null)
                        for (int i = 0; i < sParams.Length; i++)
                            command.Parameters.AddWithValue("@Param" + (i + 1).ToString(), sParams[i]);

                    //Чтение данных
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string[] st = new string[reader.FieldCount];
                            for (int i = 0; i < reader.FieldCount; i++)
                                st[i] = reader.IsDBNull(i) ? replaceNullsString : reader.GetValue(i).ToString();
                            ret.Add(st);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    ex.Message,
                    "Команда SQL: " + sql
                }.ToMultiLineString();

                GenerateError(message);
                return null;
            }
            finally
            {
                //Закрываем соединение
                if (this.connection != null)
                    this.connection.Close();
            }

            return ret;
        }

        /// <summary>
        /// Исполнение запроса к базе данных на чтение бинарных данных
        /// </summary>
        /// <param name="sql">строка SQL</param>
        /// <param name="sParams">массив параметров для строки SQL</param>
        /// <param name="pos">номер поля с бинарными данными</param>
        /// <returns>список массивов байтов</returns>
        public List<byte[]> SelectBlob(string sql, string[] sParams = null, int pos = 0)
        {
            //Создание результирующего массива байтов
            List<byte[]> ret = new List<byte[]>();

            try
            {
                this.connection.Open();

                using (var command = GetCommandObject(sql))
                {
                    //Устанавка значения всех передаваемых параметров
                    if (sParams != null)
                        for (int i = 0; i < sParams.Length; i++)
                            command.Parameters.AddWithValue("@Param" + (i + 1).ToString(), sParams[i]);

                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            if (!reader.IsDBNull(pos))
                                ret.Add((byte[])reader.GetValue(pos));
                    }
                }
            }
            catch (SqlException ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    ex.Message,
                    "Команда SQL: " + sql
                }.ToMultiLineString();

                GenerateError(message);
                return null;
            }
            finally
            {
                //Закрываем соединение
                if (this.connection != null)
                    this.connection.Close();
            }

            return ret;
        }

        /// <summary>
        /// Запись в базу бинарных данных
        /// </summary>
        /// <param name="sql">строка SQL</param>
        /// <param name="array">массив байтов</param>
        /// <param name="sParams">массив параметров для строки SQL</param>
        public bool InsertBlob(string sql, byte[] array, string[] sParams = null)
        {
            //Проверка на разумность
            if (array == null || array.Length == 0)
            {
                GenerateError("Не определен массив для записи в BLOB SQL");
                return false;
            }

            try
            {
                this.connection.Open();

                using (var command = GetCommandObject(sql))
                {
                    if (sParams != null)
                        for (int i = 0; i < sParams.Length; i++)
                            command.Parameters.AddWithValue("@Param" + (i + 1).ToString(), sParams[i]);

                    command.Parameters.Add("@Image", System.Data.SqlDbType.VarBinary).Value = array;

                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    ex.Message,
                    "Команда SQL: " + sql
                }.ToMultiLineString();

                GenerateError(message);
                return false;
            }
            finally
            {
                //Закрываем соединение
                if (this.connection != null)
                    this.connection.Close();
            }

            return true;
        }

        /// <summary>
        /// Сформировать ошибку\исключение
        /// </summary>
        /// <param name="message">Текст ошибки\исклюения</param>
        private void GenerateError(string message)
        {
            //Сохраняем ошибку
            this.Error = message;

            //Выдаём исключение, если это не запрещено
            if (!IsSkipExceptions)
                throw new Exception(message);
        }
    }

    /// <summary>
    /// Класс, реализующий настройки SQL сервера
    /// </summary>
    public static class ServerConfiguration
    {
        #region Делегаты и события
        /// <summary>
        /// Делегат для события - Настройки сервера были изменены
        /// </summary>
        /// <param name="name">Имя сервера</param>
        /// <param name="login">Имя пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        /// <param name="isWindowsAuthentication">Метод аутентификации</param>
        internal delegate void ServerSettingsChangedEventHandler(string name, string login, string password, bool isWindowsAuthentication);

        /// <summary>
        /// Событие - Настройки сервера были изменены
        /// </summary>
        internal static event ServerSettingsChangedEventHandler ServerSettingsChangedEvent;
        #endregion

        /// <summary>
        /// Имя Windows сервера\Имя сервера SQL
        /// </summary>
        public static string Name { get; private set; }

        /// <summary>
        /// Аутентификация Windows?
        /// </summary>
        public static bool IsWindowsAuthentication { get; private set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public static string Login { get; private set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public static string Password { get; private set; }

        /// <summary>
        /// Установить настройки сервера
        /// </summary>
        /// <param name="name">Имя сервера</param>
        /// <param name="login">Имя пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        /// <param name="isWindowsAuthentication">Аутентификация Windows?</param>
        public static void Set(string name, string login, string password, bool isWindowsAuthentication)
        {
            ServerConfiguration.Name = name;
            ServerConfiguration.Login = login;
            ServerConfiguration.Password = password;
            ServerConfiguration.IsWindowsAuthentication = isWindowsAuthentication;

            //Формируем событие
            if (ServerSettingsChangedEvent != null)
                ServerSettingsChangedEvent(name, login, password, isWindowsAuthentication);
        }

    }

}
