﻿namespace IA.Sql.Controls
{
    partial class SqlServerConnectionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.authentication_password_textBox = new System.Windows.Forms.TextBox();
            this.authentication_user_label = new System.Windows.Forms.Label();
            this.authentication_password_label = new System.Windows.Forms.Label();
            this.authentication_login_textBox = new System.Windows.Forms.TextBox();
            this.serverName_label = new System.Windows.Forms.Label();
            this.serverName_comboBox = new System.Windows.Forms.ComboBox();
            this.authentication_label = new System.Windows.Forms.Label();
            this.authentication_comboBox = new System.Windows.Forms.ComboBox();
            this.main_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 2;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.authentication_password_textBox, 1, 3);
            this.main_tableLayoutPanel.Controls.Add(this.authentication_user_label, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.authentication_password_label, 0, 3);
            this.main_tableLayoutPanel.Controls.Add(this.authentication_login_textBox, 1, 2);
            this.main_tableLayoutPanel.Controls.Add(this.serverName_label, 0, 0);
            this.main_tableLayoutPanel.Controls.Add(this.serverName_comboBox, 1, 0);
            this.main_tableLayoutPanel.Controls.Add(this.authentication_label, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.authentication_comboBox, 1, 1);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 5;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(666, 126);
            this.main_tableLayoutPanel.TabIndex = 1;
            // 
            // authentication_password_textBox
            // 
            this.authentication_password_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.authentication_password_textBox.Location = new System.Drawing.Point(153, 95);
            this.authentication_password_textBox.Name = "authentication_password_textBox";
            this.authentication_password_textBox.ReadOnly = true;
            this.authentication_password_textBox.Size = new System.Drawing.Size(510, 20);
            this.authentication_password_textBox.TabIndex = 35;
            this.authentication_password_textBox.UseSystemPasswordChar = true;
            // 
            // authentication_user_label
            // 
            this.authentication_user_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.authentication_user_label.AutoSize = true;
            this.authentication_user_label.Location = new System.Drawing.Point(3, 68);
            this.authentication_user_label.Name = "authentication_user_label";
            this.authentication_user_label.Size = new System.Drawing.Size(144, 13);
            this.authentication_user_label.TabIndex = 32;
            this.authentication_user_label.Text = "Имя пользователя:";
            this.authentication_user_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // authentication_password_label
            // 
            this.authentication_password_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.authentication_password_label.AutoSize = true;
            this.authentication_password_label.Location = new System.Drawing.Point(3, 98);
            this.authentication_password_label.Name = "authentication_password_label";
            this.authentication_password_label.Size = new System.Drawing.Size(144, 13);
            this.authentication_password_label.TabIndex = 34;
            this.authentication_password_label.Text = "Пароль:";
            this.authentication_password_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // authentication_login_textBox
            // 
            this.authentication_login_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.authentication_login_textBox.Location = new System.Drawing.Point(153, 65);
            this.authentication_login_textBox.Name = "authentication_login_textBox";
            this.authentication_login_textBox.ReadOnly = true;
            this.authentication_login_textBox.Size = new System.Drawing.Size(510, 20);
            this.authentication_login_textBox.TabIndex = 33;
            // 
            // serverName_label
            // 
            this.serverName_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.serverName_label.AutoSize = true;
            this.serverName_label.Location = new System.Drawing.Point(3, 8);
            this.serverName_label.Name = "serverName_label";
            this.serverName_label.Size = new System.Drawing.Size(144, 13);
            this.serverName_label.TabIndex = 0;
            this.serverName_label.Text = "Имя сервера:";
            this.serverName_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // serverName_comboBox
            // 
            this.serverName_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.serverName_comboBox.FormattingEnabled = true;
            this.serverName_comboBox.Location = new System.Drawing.Point(153, 4);
            this.serverName_comboBox.Name = "serverName_comboBox";
            this.serverName_comboBox.Size = new System.Drawing.Size(510, 21);
            this.serverName_comboBox.TabIndex = 39;
            // 
            // authentication_label
            // 
            this.authentication_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.authentication_label.AutoSize = true;
            this.authentication_label.Location = new System.Drawing.Point(3, 38);
            this.authentication_label.Name = "authentication_label";
            this.authentication_label.Size = new System.Drawing.Size(144, 13);
            this.authentication_label.TabIndex = 37;
            this.authentication_label.Text = "Аутентификация:";
            this.authentication_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // authentication_comboBox
            // 
            this.authentication_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.authentication_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.authentication_comboBox.FormattingEnabled = true;
            this.authentication_comboBox.Location = new System.Drawing.Point(153, 34);
            this.authentication_comboBox.Name = "authentication_comboBox";
            this.authentication_comboBox.Size = new System.Drawing.Size(510, 21);
            this.authentication_comboBox.TabIndex = 36;
            this.authentication_comboBox.SelectedValueChanged += new System.EventHandler(this.authentication_comboBox_SelectedValueChanged);
            // 
            // SqlServerConnectionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            //this.ServerName = "SqlServerConnectionControl";
            this.Size = new System.Drawing.Size(666, 126);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.main_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.TextBox authentication_password_textBox;
        private System.Windows.Forms.Label authentication_user_label;
        private System.Windows.Forms.Label authentication_password_label;
        private System.Windows.Forms.TextBox authentication_login_textBox;
        private System.Windows.Forms.Label serverName_label;
        private System.Windows.Forms.ComboBox serverName_comboBox;
        private System.Windows.Forms.Label authentication_label;
        private System.Windows.Forms.ComboBox authentication_comboBox;

    }
}