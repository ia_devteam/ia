﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;

namespace IA.Sql.Controls
{
    /// <summary>
    /// Пользовательская форма для проверки соединения с SQL
    /// </summary>
    public partial class SqlServerConnectionControl : UserControl
    {
        //Имя локального сервера
        private const string LocalHostName = "localhost";

        /// <summary>
        /// Выды аутентификации
        /// </summary>
        private enum Authentication
        {
            [Description("Windows")]
            WINDOWS,
            [Description("SQL Server")]
            SQLSERVER
        }

        /// <summary>
        /// Имя сервера
        /// </summary>
        public string ServerName
        {
            get
            {
                return serverName_comboBox.Text.Trim();
            }
            set
            {
                serverName_comboBox.Text = String.IsNullOrWhiteSpace(value) ? LocalHostName : value;
            }
        }

        private string serverLogin = String.Empty;
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string ServerLogin
        {
            get
            {
                return this.IsWindowsAuthentication ? String.Empty : authentication_login_textBox.Text.Trim();
            }
            set
            {
                //Запоминаем значение
                serverLogin = value ?? String.Empty;

                //Отображаем только в случае аутентификации SQL Server
                authentication_login_textBox.Text = this.IsWindowsAuthentication ? String.Empty : serverLogin;
                authentication_login_textBox.ReadOnly = this.IsWindowsAuthentication;
            }


        }

        private string serverPassword = String.Empty;
        /// <summary>
        /// Пароль
        /// </summary>
        public string ServerPassword
        {
            get
            {
                return this.IsWindowsAuthentication ? String.Empty : authentication_password_textBox.Text.Trim();
            }
            set
            {
                //Запоминаем значение
                serverPassword = value ?? String.Empty;

                //Отображаем только в случае аутентификации SQL Server
                authentication_password_textBox.Text = this.IsWindowsAuthentication ? String.Empty : serverPassword;
                authentication_password_textBox.ReadOnly = this.IsWindowsAuthentication;
            }
        }

        /// <summary>
        /// Аутентификация Windows?
        /// </summary>
        public bool IsWindowsAuthentication
        {
            get
            {
                return authentication_comboBox.Text == Authentication.WINDOWS.Description();
            }
            set
            {
                authentication_comboBox.Text = value ? Authentication.WINDOWS.Description() : Authentication.SQLSERVER.Description();
            }
        }
        
        /// <summary>
        /// Конструктор
        /// </summary>
        public SqlServerConnectionControl()
        {
            InitializeComponent();

            //Список доступных серверов
            serverName_comboBox.Items.Add(LocalHostName);
//            foreach (string server in SqlExtensions.GetAllSqlServers().Select(server => server.ToString()))
  //              serverName_comboBox.Items.Add(server);

            //Аутентификация
            EnumLoop<Authentication>.ForEach(authentication => authentication_comboBox.Items.Add(authentication.Description()));
            authentication_comboBox.Text = Authentication.WINDOWS.Description();
        }

        /// <summary>
        /// Обработчик - Выбрали метод аутентификации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authentication_comboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            //Аутентификация Windows
            if ((string)authentication_comboBox.Text == Authentication.WINDOWS.Description())
            {
                authentication_login_textBox.Text = authentication_password_textBox.Text = string.Empty;
                authentication_login_textBox.ReadOnly = authentication_password_textBox.ReadOnly = true;
                return;
            }

            //Аутентификация SQL Server
            if ((string)authentication_comboBox.Text == Authentication.SQLSERVER.Description())
            {
                authentication_login_textBox.ReadOnly = authentication_password_textBox.ReadOnly = false;
                authentication_login_textBox.Text = serverLogin;
                authentication_password_textBox.Text = serverPassword;
                return;
            }

            throw new Exception("Неизвесный тип аутентификации.");
        }


    }
}
