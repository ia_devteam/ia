﻿using IA.Sql.DatabaseConnections;
using System;
using System.Windows.Forms;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Microsoft.SqlServer.Management.Smo;

namespace IA.Sql
{
    /// <summary>
    /// класс для работы с SQL-соединением
    /// </summary>
    public class SQLConnection
    {
        #region Делегаты
        /// <summary>
        /// Делегат для изменения состояния элемента управления из параллельного потока
        /// </summary>
        private delegate void ChangeControlConnectionStateDelegate();

        /// <summary>
        /// делегат для событий соединения и разрыва соединения с БД
        /// </summary>
        public delegate void OnConnectHandler();
        #endregion

        #region события
        /// <summary>
        /// событие установления соединения с БД
        /// </summary>
        public event OnConnectHandler OnDatabaseConnect = null;
        /// <summary>
        /// событие разрыва соединения с БД
        /// </summary>
        public event OnConnectHandler OnDatabaseDisconnect = null;
        #endregion

        /// <summary>
        /// Текущее состояние соединения
        /// </summary>
        public bool? isConnected = null;

        /// <summary>
        /// Соединение с базой данных
        /// </summary>
        public Sql.DatabaseConnection DatabaseConnection { get; internal set; }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="databaseConnection">дескриптор соединения с БД</param>
        public SQLConnection(Sql.DatabaseConnection databaseConnection)
        {
            //Проверка на разумность
            if (databaseConnection == null)
                throw new Exception("Соединение с базой данных, от которого зависит элемент управления не определено.");

            //Запоминаем соединение
            this.DatabaseConnection = databaseConnection;
        }

        /// <summary>
        /// Задать состояние формы (в рамках соединенеия с базой данных)
        /// </summary>
        /// <param name="isDatabaseConnected">Установлено ли соединение с базой данных?</param>
        /// <param name="onConnect">функция-обработчик установления соединения с БД</param>
        /// <param name="onDisconnect">функция-обработчик разрыва соединения с БД</param>
        private void SetConnectionState(bool isDatabaseConnected, OnConnectHandler onConnect, OnConnectHandler onDisconnect)
        {
            //Переходим в новое состояние
            if (SqlConnector.MainForm != null && SqlConnector.MainForm.InvokeRequired)
            {
                if (isDatabaseConnected)
                    if (onConnect != null || this.OnDatabaseConnect != null)
                        SqlConnector.MainForm.Invoke(new ChangeControlConnectionStateDelegate(onConnect == null ? this.OnDatabaseConnect : onConnect));
                    else
                    if (onDisconnect != null || this.OnDatabaseDisconnect != null)
                        SqlConnector.MainForm.Invoke(new ChangeControlConnectionStateDelegate(onDisconnect == null ? this.OnDatabaseDisconnect : onDisconnect));
            }
            else
            {
                if (isDatabaseConnected)
                    (onConnect == null ? this.OnDatabaseConnect : onConnect)?.Invoke();
                else
                    (onDisconnect == null ? this.OnDatabaseDisconnect : onDisconnect)?.Invoke();
            }

            //Запоминаем новое состояние
            this.isConnected = isDatabaseConnected;
        }

        /// <summary>
        /// Попытка установить соединение с базой данных
        /// </summary>
        /// <returns>Результат попытки установить соединение с базой данных</returns>
        public bool TestConnection()
        {
            //Изначально результат попытки установить соединение положительный
            bool result = true;

            try
            {
                this.DatabaseConnection.connection.Open();
                this.DatabaseConnection.connection.Close();
            }
            catch
            {
                //Результат попытки установить соединенеие отрицательный
                result = false;
            }

            //Если результат попытки установить соединение изменился
            if (this.DatabaseConnection.IsConnected != result)
            {
                //Запоминаем результат
                this.DatabaseConnection.IsConnected = result;

                SynchronizeWithDatabaseConnection(null, null);
            }

            return result;
        }

        /// <summary>
        /// Синхронизация состояния формы с соединенем с базой данных, от которой она зависит
        /// </summary>
        /// <param name="onConnect">функция-обработчик установления соединения с БД</param>
        /// <param name="onDisconnect">функция-обработчик разрыва соединения с БД</param>
        /// <param name="isNeedNewConnectionTest">Требуется ли заново проверить наличие соединения?</param>
        public void SynchronizeWithDatabaseConnection(OnConnectHandler onConnect, OnConnectHandler onDisconnect, bool isNeedNewConnectionTest = false)
        {
            if (onConnect != null)
                Sql.SqlConnector.IADB.OnDatabaseConnect += onConnect; //DO SOMETHING WITH THIS!
            if (onDisconnect != null)
                Sql.SqlConnector.IADB.OnDatabaseDisconnect += onDisconnect;
            //Устанавливаем состояние формы (в рамках соединенеия с базой данных), если требуеся, пробуем заново подключиться к базе данных
            this.SetConnectionState(isNeedNewConnectionTest ? TestConnection() : this.DatabaseConnection.IsConnected, onConnect, onDisconnect);
        }
    }


    /// <summary>
    /// Класс реализующий форму, данные для которой получаются из базы данных SQL
    /// </summary>
    public static class SqlConnector
    {
        /// <summary>
        /// Соединение с базой данных проектов
        /// </summary>
        public static SQLConnection IADB = new SQLConnection(new IADatabaseConnection());

        /// <summary>
        /// Соединение с базой данных сигнатур типов файлов
        /// </summary>
        public static SQLConnection SignatureDB = new SQLConnection(new SignatureFileDatabaseConnection());

        /// <summary>
        /// Соединение с базой данных dll-библиотек
        /// </summary>
        public static SQLConnection DllKnowledgeDB = new SQLConnection(new DllKnowledgeBaseDatabaseConnection());

        private static SQLConnection[] save = new SQLConnection[3];// { null, null, null } ;

        private static string ServerNameSaved = null;

        /// <summary>
        /// Выгрузить копию БД в SqlCompact.
        /// </summary>
        /// <param name="filename">Имя файла, на основе которого будут генерироваться файлы локальной БД путём замены расширения на .sdf и добавления постфикса в виде имени БД.</param>
        /// <param name="signatureFileProjectId">Идентификатор проекта в БД типов файлов, который необходимо выгрузить в дополнение к стандартному.</param>
        public static void DumpToLocalDB(string filename, uint signatureFileProjectId = 0)
        {
            List<string> iadbInsertCommands = new List<string>();
            iadbInsertCommands.Add(@"INSERT [users] ([surname], [name], [patronymic], [deleted]) VALUES (N'OfflineUserSurname', N'OfflineUserName', N'OfflineUserPatronymic', N'false')");
            iadbInsertCommands.Add(@"SET IDENTITY_INSERT [versions] ON");
            iadbInsertCommands.Add("INSERT [versions] ([version]) VALUES (" + IADB.DatabaseConnection.ExecuteScalar("SELECT MAX(version) FROM versions;") + ")");
            iadbInsertCommands.Add(@"SET IDENTITY_INSERT [versions] OFF");

            string baseWorkId = SignatureDB.DatabaseConnection.ExecuteScalar("select ID from WorkPlaces where Name=N'Базовое рабочее место'");
            var SignatureFileInsertCommands = SqlCompactDumpScripts.PrepareSignatureFilesInsertCommands(baseWorkId);
            if (signatureFileProjectId != 0)
            {
                SignatureFileInsertCommands.Concat(SqlCompactDumpScripts.PrepareSignatureFilesInsertCommands(signatureFileProjectId.ToString()));
            }

            var iadbCommands = SqlCompactDumpScripts.GetCreateTablesScriptsForDB(SqlCompactDumpScripts.SupportedDb.IA)
                .Concat(iadbInsertCommands);
            var signatureFileCommands = SqlCompactDumpScripts.GetCreateTablesScriptsForDB(SqlCompactDumpScripts.SupportedDb.SignatureFile)
                .Concat(SignatureFileInsertCommands);

            SqlCompactDumpScripts.ExecuteSqlCompactCommands(SqlCompactDumpScripts.CreateSqlCompactDB(filename, IADB.DatabaseConnection.DatabaseName), iadbCommands);
            SqlCompactDumpScripts.ExecuteSqlCompactCommands(SqlCompactDumpScripts.CreateSqlCompactDB(filename, SignatureDB.DatabaseConnection.DatabaseName), signatureFileCommands);
        }

        /// <summary>
        /// Переключить IA на использование БД SqlCompact, приложенной к указанному файлу выгрузки хранилища.
        /// </summary>
        /// <param name="filename">Файл выгрузки хранилища, рядом с которым лежат требуемые для работы БД.</param>
        public static void UseSqlCompactDB(string filename)
        {
            ServerConfiguration.Set(filename, ServerConfiguration.Login, ServerConfiguration.Password, ServerConfiguration.IsWindowsAuthentication);
            IADB = new SQLConnection(new IADatabaseConnection());
            SignatureDB = new SQLConnection(new SignatureFileDatabaseConnection());
        }

        /// <summary>
        /// Подготовить соединения с БД к переходу в автономный режим.
        /// </summary>
        public static void PrepareOffline()
        {
            ServerNameSaved = ServerConfiguration.Name;
            save[0] = IADB; IADB = null;
            save[1] = SignatureDB; SignatureDB = null;
            save[2] = DllKnowledgeDB; DllKnowledgeDB = null;
        }

        /// <summary>
        /// Восстановить онлайн-режим работы с БД.
        /// </summary>
        public static void GoOnline()
        {
            if (save[0] != null)
            {
                IADB = save[0];
                SignatureDB = save[1];
                DllKnowledgeDB = save[2];
            }
            if (ServerNameSaved != null)
                ServerConfiguration.Set(ServerNameSaved, ServerConfiguration.Login, ServerConfiguration.Password, ServerConfiguration.IsWindowsAuthentication);
        }

        /// <summary>
        /// главная форма приложения. Используется для обращения к событиям соединения и разрыва соединения с БД (проверяется, надо ли обращаться к другому потоку)
        /// </summary>
        public static Form MainForm = null;
    }
}
