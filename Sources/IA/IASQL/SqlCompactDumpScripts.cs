﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IA.Sql
{
    /// <summary>
    ///Вспомогательный класс для инкапсуляции создания и наполнения автономной  SqlCompact.
    /// </summary>
    internal static class SqlCompactDumpScripts
    {
        internal enum SupportedDb
        {
            IA,
            SignatureFile
        }

        internal static IEnumerable<string> GetCreateTablesScriptsForDB(SupportedDb db)
        {
            switch (db)
            {
                case SupportedDb.IA:
                    return IAdbTables;
                case SupportedDb.SignatureFile:
                    return SignatureFileTables;
            }
            throw new NotSupportedException();
        }

        static readonly string[] IAdbTables = new string[]
        {
            @"CREATE TABLE [materials]( [id] [bigint] IDENTITY NOT NULL, [project] [bigint] NOT NULL, [version] [int] NOT NULL, [datetime] [datetime] NOT NULL DEFAULT (getdate()), [comment] [nvarchar](1000), CONSTRAINT [PK_materials] PRIMARY KEY ( [id]) )",
            @"CREATE TABLE [projects]( [id] [bigint] IDENTITY NOT NULL, [name] [nvarchar](255) , [path] [nvarchar](255) , [settings] [nvarchar](255) , CONSTRAINT [PK_projects_id2] PRIMARY KEY ( [id]) )",
            @"CREATE TABLE [storages]( [id] [bigint] IDENTITY NOT NULL, [materials] [bigint] NOT NULL, [parent] [bigint] NOT NULL CONSTRAINT [DF__storages__parent__03317E3D] DEFAULT (NULL), [datetime] [datetime] NOT NULL CONSTRAINT [DF__storages__dateti__0425A276] DEFAULT (getdate()), [creator] [bigint] NOT NULL, [empty] [nvarchar](5) CONSTRAINT [DF__storages__empty__0519C6AF] DEFAULT (N'true'), [comment] [nvarchar](1000) , [runned_plugins] [nvarchar](1000) CONSTRAINT [DF_storages_completed_plugins] DEFAULT (''), [completed_plugins] [nvarchar](1000) CONSTRAINT [DF_storages_completed_plugins_1] DEFAULT (''), [mode] [nvarchar](10) , CONSTRAINT [PK_storages_id] PRIMARY KEY ( [id]) )",
            @"CREATE TABLE [users]( [id] [bigint] IDENTITY NOT NULL, [surname] [nvarchar](50) , [name] [nvarchar](50) , [patronymic] [nvarchar](50) , [deleted] [nvarchar](5) CONSTRAINT [DF_users_deleted] DEFAULT (N'false'), CONSTRAINT [PK_users_id] PRIMARY KEY ( [id]) )",
            @"CREATE TABLE [versions]( [version] [int] IDENTITY NOT NULL,PRIMARY KEY ( [version]) )"
        };

        static readonly string[] SignatureFileTables = new string[]
        {
            @"CREATE TABLE [FileFormat]( [ID] [int] IDENTITY NOT NULL, [FileFormatID] [int] NOT NULL, [WorkID] [int] NOT NULL, [TypeOfFile] [int] NOT NULL, [DescriptionShort] [nvarchar](1000) , [DescriptionLarge] [nvarchar](1000) , [Extension] [nvarchar](4000) , [Language] [bigint] NULL, CONSTRAINT [PK_fileFormatId] PRIMARY KEY ([ID]) )",
            @"CREATE TABLE [Pattern]( [PatternID] [int] IDENTITY NOT NULL, [FileFormatID] [int] NOT NULL, [WorkID] [int] NOT NULL, [TypeOfPattern] [nvarchar](50) , [PatternF] [nvarchar](50) , [PatternFValue] [nvarchar](1000) , [PatternS] [nvarchar](50) , [PatternSValue] [nvarchar](1000))",
            @"CREATE TABLE [WorkHistory]( [ID] [int] NULL, [WID] [int] NULL)",
            @"CREATE TABLE [WorkPlaces]( [ID] [int] IDENTITY NOT NULL, [Name] [nvarchar](250) , [Description] [nvarchar](1000) )"
        };

        /// <summary>
        /// Создать (заменить если существует) файл БД SqlCompact. Имя файла формируется на основе <paramref name="basefilename"/> по правилам функции <see cref="DatabaseConnection.GetSqlCompactDBName"/>.
        /// <para>Возвращает строку подключения к созданной БД.</para>
        /// </summary>
        /// <param name="basefilename"></param>
        /// <param name="dbname"></param>
        /// <returns>Строка подключения к созданной БД SqlCompact.</returns>
        internal static string CreateSqlCompactDB(string basefilename, string dbname)
        {
            var resultfile = DatabaseConnection.GetSqlCompactDBName(basefilename, dbname);

            var constringbldr = new System.Data.SqlServerCe.SqlCeConnectionStringBuilder();
            constringbldr.DataSource = resultfile;
            using (System.Data.SqlServerCe.SqlCeEngine eng = new System.Data.SqlServerCe.SqlCeEngine(constringbldr.ToString()))
            {
                if (File.Exists(constringbldr.DataSource))
                    File.Delete(constringbldr.DataSource);
                eng.CreateDatabase();
            }
            return constringbldr.ToString();
        }

        const string getFileFormatEntries = @"select ID, FileFormatID, WorkID, TypeOfFile, DescriptionShort, DescriptionLarge, Extension, Language from FileFormat where WorkID = @Param1";
        const string getPatternsEntries = @"SELECT [PatternID], [FileFormatID], [WorkID], [TypeOfPattern], [PatternF], [PatternFValue], [PatternS], [PatternSValue] from Pattern where FileFormatID in (REPLACEWITHIDS) and WorkID = @Param1";
        internal static IEnumerable<string> PrepareSignatureFilesInsertCommands(string workid)
        {
            var result = new List<string>();

            List<string> UsedFileFormatIds = new List<string>();
            result.Add("SET IDENTITY_INSERT [FileFormat] ON");
            foreach (var line in SqlConnector.SignatureDB.DatabaseConnection
            .ExecuteReader(getFileFormatEntries, new string[] { workid }, "NULL")
            .Select(x => x.Select(y => y.Replace("'", "''")).ToArray()))
            {
                UsedFileFormatIds.Add(line[1]);
                result.Add("INSERT FileFormat (ID, [FileFormatID], [WorkID], [TypeOfFile], [DescriptionShort], [DescriptionLarge], [Extension], [Language])" +
                                           $"VALUES ({String.Join(",", line.Take(4))}, N'{String.Join("', N'", line.Skip(4).Take(3))}', {line[7]} )");
            }
            result.Add("SET IDENTITY_INSERT [FileFormat] OFF");
            result.Add("SET IDENTITY_INSERT [Pattern] ON");
            if (UsedFileFormatIds.Count > 0)
                foreach (var line in SqlConnector.SignatureDB.DatabaseConnection
                .ExecuteReader(getPatternsEntries.Replace("REPLACEWITHIDS", String.Join(",", UsedFileFormatIds)), new string[] { workid }, "NULL")
                .Select(x => x.Select(y => y.Replace("'", "''")).ToArray()))
                {
                    result.Add("INSERT [Pattern] ([PatternID], [FileFormatID], [WorkID], [TypeOfPattern], [PatternF], [PatternFValue], [PatternS], [PatternSValue])" +
                        $"VALUES ( {String.Join(",", line.Take(3))}, N'{String.Join("',N'", line.Skip(3))}' )");
                }
            result.Add("SET IDENTITY_INSERT [Pattern] OFF");

            return result;
        }

        internal static void ExecuteSqlCompactCommands(string connectionString, IEnumerable<string> commands)
        {
            using (var dbce = new System.Data.SqlServerCe.SqlCeConnection(connectionString))
            {
                dbce.Open();
                try
                {
                    foreach (var cmd in commands)
                        using (var cce = new System.Data.SqlServerCe.SqlCeCommand(cmd, dbce))
                            cce.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Monitor.Log.Error("Создание БД SqlCompact", ex.Message);
                }
            }
        }
    }
}
