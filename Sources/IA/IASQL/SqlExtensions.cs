﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace IA.Sql
{
    /// <summary>
    /// Класс, предоставляющий различные дополнительные операции с базами данных Sql
    /// </summary>
    public static class SqlExtensions
    {
        #region Делегаты
        /// <summary>
        /// Делегат для определения обработчика ошибок при попытке устаносить соединение со списком баз данных
        /// </summary>
        /// <param name="databaseConnections"></param>
        public delegate void TestDatabaseConnectionsErrorMethod(Sql.DatabaseConnection[] databaseConnections);
        #endregion

        /// <summary>
        /// Класс для получения списка SQL серверов
        /// </summary>
        public class SqlServer
        {
            /// <summary>
            /// Имя Sql сервера
            /// </summary>
            private string SqlServerName;
            
            /// <summary>
            /// Имя экземпляра базы данных Sql
            /// </summary>
            private string SqlObjectName;

            /// <summary>
            /// Перегруженная функция преобразования к строке 
            /// </summary>
            public override string ToString()
            {
                if (this.SqlObjectName == null || this.SqlObjectName == "MSSQLSERVER" || this.SqlObjectName == "SQLEXPRESS")
                    return this.SqlServerName;
                else
                    return this.SqlServerName + "\\" + this.SqlObjectName;
            }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="info"></param>
            internal SqlServer(string info)
            {
                string[] nvs = info.Split(';');
                for (int i = 0; i < nvs.Length; i++)
                {
                    switch (nvs[i].ToLower())
                    {
                        case "servername":
                            this.SqlServerName = nvs[i + 1];
                            break;

                        case "instancename":
                            this.SqlObjectName = nvs[i + 1];
                            break;

                    }
                }
            }
        }

        /// <summary>
        /// Функция для поиска доступных SQL серверов
        /// </summary>
        /// <returns></returns>
        public static SqlServer[] GetAllSqlServers()
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 3000);

            //Список найденных серверов
            List<SqlServer> servers = new List<SqlServer>();
            try
            {
                byte[] msg = new byte[] { 0x02 };
                IPEndPoint ep = new IPEndPoint(IPAddress.Broadcast, 1434);
                socket.SendTo(msg, ep);

                int cnt = 0;
                byte[] buf = new byte[1024];
                do
                {
                    cnt = socket.Receive(buf);
                    servers.Add(new SqlServer(System.Text.ASCIIEncoding.ASCII.GetString(buf, 3, BitConverter.ToInt16(buf, 1))));
                    socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 300);
                } while (cnt != 0);
            }
            catch (SocketException socex)
            {
                const int WSAETIMEDOUT = 10060;		//  Timeout соединения
                const int WSAEHOSTUNREACH = 10065;	// Нет пути к хосту 

                // если это не таймаут, то - исключение
                if (socex.ErrorCode == WSAETIMEDOUT || socex.ErrorCode == WSAEHOSTUNREACH)
                {
                }
                else
                {
                    throw;
                }
            }
            finally
            {
                socket.Close();
            }

            return servers.ToArray();
        }

        /// <summary>
        /// Проверка соединения со списком баз данных c отображением окна ожидания
        /// </summary>
        /// <param name="databaseConnections">Список соединенеий с базами данных для проверки</param>
        /// <param name="testDatabaseConnectionsErrorMethod">Обработчик неудачной проверки соединенеия со списком баз данных</param>
        public static void TestDatabaseConnectionsWithWaitWindow(Sql.SQLConnection[] databaseConnections, TestDatabaseConnectionsErrorMethod testDatabaseConnectionsErrorMethod = null)
        {
            //Отображаем окно ожидания и параллельно проверяем соединения
            List<Sql.DatabaseConnection> result = WaitWindow.Show(
                delegate(WaitWindow waitWindow)
                {
                    //В качестве результата будут передваться неустановленные соединенеия
                    List<Sql.DatabaseConnection> disconnectedDatabases = new List<Sql.DatabaseConnection>();

                    //Проверяем наличие соединения со всеми базами данных
                    foreach (Sql.SQLConnection databaseConnection in waitWindow.WorkerMethodArguments.Cast<Sql.SQLConnection>())
                    {
                        //Устанавливаем информационное сообщение
                        waitWindow.Message = "Проверка соединения с базой данных <" + databaseConnection.DatabaseConnection.DatabaseName + ">";

                        //Проверяем наличие соединения
                        if (databaseConnection.TestConnection())
                            continue;

                        //Добавляем базу данных в список отключённых
                        disconnectedDatabases.Add(databaseConnection.DatabaseConnection);
                    }

                    //Передаём результат
                    waitWindow.WorkerMethodResult = disconnectedDatabases;
                },
                null,
                databaseConnections
            ) as List<Sql.DatabaseConnection>;

            //Проверка на разумность
            if (result == null)
                throw new Exception("Результат выполнения рабочего метода не соответствует ожидаемому.");

            //Запускаем обработчик ошибок
            if (result.Count > 0 && testDatabaseConnectionsErrorMethod != null)
                testDatabaseConnectionsErrorMethod(result.ToArray());
        }
    }
}

