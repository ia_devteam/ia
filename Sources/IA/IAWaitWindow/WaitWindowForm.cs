﻿using System;
using System.Windows.Forms;

namespace IA
{
    /// <summary>
    /// Класс, реализующий форму окна ожидания
    /// </summary>
    internal partial class WaitWindowForm : Form
    {
        #region Делегаты
        /// <summary>
        /// Делегат для отображения сообщения
        /// </summary>
        /// <param name="message">Сообщение</param>
        private delegate void SetMessageDelegate(string message);
        #endregion

        /// <summary>
        /// Констркутор
        /// </summary>
		public WaitWindowForm()
        {
            //Инициализация основных компонентов
			InitializeComponent();
		}

        /// <summary>
        /// Обработчик - Что необходимо сделать при отрисовке формы
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);

            //Отрисовывем границу формы вручную
            ControlPaint.DrawBorder3D(e.Graphics, this.ClientRectangle, Border3DStyle.Raised);
        }

        /// <summary>
        /// Отобразить сообщение
        /// </summary>
        /// <param name="message">Сообщение</param>
        internal void SetMessage(string message)
        {
            if (label.InvokeRequired)
                label.Invoke(new SetMessageDelegate(this.SetMessage), message);
            else
            {

                label.Text = (message ?? String.Empty) + "...";
                label.Refresh();
            }
        }
	}
}
