﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace IA
{
    /// <summary>
    /// Класс, реализующий окно ожидания
    /// </summary>
	public sealed class WaitWindow
    {
        public static Form parent;
        #region Константы
        /// <summary>
        /// Задержка при отображении окна ожидания в миллисекундах
        /// </summary>
        private const int Delay = 1000;
        #endregion

        #region Делегаты
        /// <summary>
        /// Делегат для описания рабочего метода
        /// </summary>
        /// <param name="waitWindow"></param>
        public delegate void WorkMethodDelegate(WaitWindow waitWindow);
        #endregion

        #region Статические методы
        /// <summary>
        /// Отобразить окно ожидания со указанным сообщением на время выполнения указанного рабочего метода с указанными аргументами.
        /// Метод выполняется в параллельном потоке.
        /// </summary>
        /// <param name="workerMethod">Рабочий метод</param>
        /// <param name="message">Сообщение</param>
        /// <param name="workerMethodArguments">Аргументы рабочего метода</param>
        /// <returns></returns>
		public static object Show(WorkMethodDelegate workerMethod, string message = null, params object[] workerMethodArguments)
        {
            //Формируем окно ожидания
            WaitWindow window = new WaitWindow(workerMethod, message, new List<object>(workerMethodArguments));

            //Отображаем его и возвращаем результат
			return window.Show();
		} 
	    #endregion

        /// <summary>
        /// Форма окна ожидания
        /// </summary>
        private WaitWindowForm form = null;

        #region Поля
        /// <summary>
        /// Завершился ли рабочий метод?
        /// </summary>
        private bool isWorkerMethodCompleted = false;
        

        /// <summary>
        /// Исключение, возникшее в ходе выполнения рабочего метода
        /// </summary>
        private Exception workerMethodException = null;

        /// <summary>
        /// Семафор для межпоточного взаимодействия. Необходим для реализации секундной задержки отображения окна ожидания.
        /// </summary>
        private ManualResetEvent OneSecondDelaySemaphore = new ManualResetEvent(false);

        /// <summary>
        /// Объект для запуска выполнения рабочего метода в отдельном потоке
        /// </summary>
        private BackgroundWorker worker = null;
        #endregion

        #region Свойства
        /// <summary>
        /// Рабочий метод
        /// </summary>
		internal WorkMethodDelegate WorkerMethod { get; private set; }

        /// <summary>
        /// Аргументы рабочего метода
        /// </summary>
		public List<object> WorkerMethodArguments { get; private set; }

        /// <summary>
        /// Результат рабочего метода
        /// </summary>
        public object WorkerMethodResult { private get; set; }

        private string message = null;
        /// <summary>
    	/// Обновить отображаемое сообщение
    	/// </summary>
		public string Message
        {
            get
            {
                return this.message;
            }
			set
            {
                //Запоминаем актуальное сообщение
                this.message = value ?? "Пожалуйста, подождите";

                //Если форма существует, отображаем на ней сообщение
                if (this.form != null)
                    this.form.SetMessage(this.message);
			}
		}
	    #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="workerMethod">Рабочий метод</param>
        /// <param name="message">Сообщение</param>
        /// <param name="workerMethodArguments">Аргументы рабочего метода. Не может быть Null.</param>
        private WaitWindow(WorkMethodDelegate workerMethod, string message, List<object> workerMethodArguments)
        {
            //Инициализация полей
            this.worker = new BackgroundWorker();
            this.worker.DoWork += this.DoWork;
            this.worker.RunWorkerCompleted += this.WorkComplete;

            //Инициализация свойств
            this.WorkerMethod = workerMethod;
            this.Message = message;
            this.WorkerMethodArguments = workerMethodArguments;
        }

    	/// <summary>
    	/// Отобразить окно ожидания с указанным сообщением во время выполнения указанного рабочего метода с указанными аргументами
    	/// </summary>
    	/// <returns>Результат выполнения рабочего метода</returns>
		private object Show()
        {
            //Запускаем выполнение рабочего метода в отдельном потоке
            // TODO: вернуть окошко загрузки, победив race condition
            //worker.RunWorkerAsync();

            DoWork(null, null);

            //Ожидаем секунду перед отображением формы
            OneSecondDelaySemaphore.WaitOne(1000);

            //Если рабочий метод ещё не завершился
            if (!isWorkerMethodCompleted)
            {
                //Инициализируем и отображаем форму
                using (this.form = new WaitWindowForm() { StartPosition = System.Windows.Forms.FormStartPosition.CenterParent })
                {
                    //Задаём сообщение
                    this.form.SetMessage(this.Message);

                    //Отображаем форму

                    if (parent != null)
                        parent.Invoke((MethodInvoker)delegate
                        {
                            this.form.ShowDialog(parent);
                        });
                    else
                        this.form.ShowDialog();

                }
            }
            //Если возникло исключение, кидаем его
            if (this.workerMethodException != null)
                throw workerMethodException;

            //Получаем результат
            return this.WorkerMethodResult;
		}

        /// <summary>
        /// Обработчик - Действие для выполнения в асинхронном потоке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //Выполняем рабочий метод в отдельном потоке
                if ((this.WorkerMethod != null))
                    this.WorkerMethod(this);
            }
            catch (Exception ex)
            {
                //Если возникло исключение, запоминаем его
                this.workerMethodException = ex;
            }
            finally
            {
                //Отмечаем, что рабочий метод завершён
                this.isWorkerMethodCompleted = true;

                //Прерываем задержку отображения окна ожидания
                this.OneSecondDelaySemaphore.Set();
            }
        }

        /// <summary>
        /// Обработчик - Выполнение действия в асинхронном потоке завершено
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            //Если форма существует закрываем её
            if (this.form != null && this.form.IsHandleCreated)
            {
                this.form.Invoke((MethodInvoker)delegate
                {
                    this.form.Close();
                });
            }
        }
	}
}
