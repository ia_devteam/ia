﻿namespace IA.Network
{
    /// <summary>
    /// Класс, предоставляющий различные дополнительные операции с сетью
    /// </summary>
    public static class NetworkExtensions
    {
        /// <summary>
        /// Попытка установить соединение с сетевым ресурсом
        /// </summary>
        /// <param name="resource">Сетевой ресурс</param>
        /// <param name="isNeedWaitWindow">Отображать ли окно ожидания</param>
        /// <param name="waitWindowMessage">Сообщение для отображения в окне ожидания</param>
        /// <returns></returns>
        public static bool TestNetworkConnection(NetworkResource resource, bool isNeedWaitWindow = false, string waitWindowMessage = null)
        {
            NetworkConnection connection = null;
            if (Settings.AutonomyMode)
                return true;

            try
            {
                //Устанавливаем соединение
                connection = new NetworkConnection(resource, isNeedWaitWindow, waitWindowMessage);
            }
            catch
            {
                return false;
            }
            finally
            {
                //Если соединение ещё установлено
                if(connection != null)
                    //Разрываем соединение
                    connection.Dispose();
            }

            return true;
        }
    }
}
