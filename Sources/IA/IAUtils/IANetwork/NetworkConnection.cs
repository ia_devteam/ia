﻿using System;
using System.Runtime.InteropServices;

using IA.Extensions;

namespace IA.Network
{
    /// <summary>
    /// Класс, реализующий сетевой ресурс для подключения
    /// </summary>
    public class NetworkResource
    {   /// <summary>
        /// Имя сетевого ресурса
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// UNC имя сетевого ресурса
        /// </summary>
        public string UNCName { get { return System.IO.Path.Combine(@"\\", this.Name); } }

        /// <summary>
        /// Имя пользователя для подключения к сетевому ресурсу
        /// </summary>
        public string Login { get; private set; }

        /// <summary>
        /// Пароль пользователя для подключения к сетевому ресурсу
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя сетевого ресурса</param>
        /// <param name="login">Имя пользователя для подключения к сетевому ресурсу</param>
        /// <param name="password">Пароль пользователя для подключения к сетевому ресурсу</param>
        public NetworkResource(string name, string login, string password)
        {
            this.Name = name ?? string.Empty;
            this.Login = login ?? string.Empty;
            this.Password = password ?? string.Empty;
        }
    }

    /// <summary>
    /// Класс, реализующий сетевое соединение с сетевым ресурсом
    /// </summary>
    /// https://msdn.microsoft.com/en-us/library/windows/desktop/aa385353%28v=vs.85%29.aspx
    public class NetworkConnection : IDisposable
    {
        /// <summary>
        /// Класс, определяющий сетевой ресурс, к которому производится подключение
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        private class WinApiNetworkResource
        {
            /// <summary>
            /// Масштабы сетевого ресурса
            /// </summary>
            internal enum enScope : int
            {
                RESOURCE_CONNECTED = 1,
                RESOURCE_GLOBALNET = 2,
                RESOURCE_REMEMBERED = 3,
                RESOURCE_RECENT = 4,
                RESOURCE_CONTEXT = 5
            };

            /// <summary>
            /// Типы сетевого ресурса
            /// </summary>
            internal enum enType : int
            {
                RESOURCETYPE_ANY = 0,
                RESOURCETYPE_DISK = 1,
                RESOURCETYPE_PRINT = 2,
                RESOURCETYPE_RESERVED = 8,
            }

            /// <summary>
            /// Типы представления сетевого ресурса
            /// </summary>
            internal enum enDisplayType : int
            {
                Generic = 0x0,
                Domain = 0x01,
                Server = 0x02,
                Share = 0x03,
                File = 0x04,
                Group = 0x05,
                Network = 0x06,
                Root = 0x07,
                Shareadmin = 0x08,
                Directory = 0x09,
                Tree = 0x0a,
                Ndscontainer = 0x0b
            }

            internal enum enUsage : int
            {
                RESOURCEUSAGE_CONNECTABLE = 0x00000001,
                RESOURCEUSAGE_CONTAINER = 0x00000002,
                RESOURCEUSAGE_NOLOCALDEVICE = 0x00000004,
                RESOURCEUSAGE_SIBLING = 0x00000008,
                RESOURCEUSAGE_ATTACHED = 0x00000010
            }

            /// <summary>
            /// Масштаб сетевого ресурса
            /// </summary>
            internal enScope Scope { get; set; }

            /// <summary>
            /// Тип сетевого ресурса
            /// </summary>
            internal enType ResourceType { get; set; }

            /// <summary>
            /// Тип представления сетевого ресурса
            /// </summary>
            internal enDisplayType DisplayType { get; set; }

            /// <summary>
            /// Использование сетевого ресурса
            /// </summary>
            internal enUsage Usage { get; set; }

            /// <summary>
            /// Локальное имя сетевого ресурса
            /// </summary>
            internal string LocalName { get; set; }

            /// <summary>
            /// Удалённое имя сетевого ресурса
            /// </summary>
            internal string RemoteName { get; set; }

            /// <summary>
            /// Комментарий к сетевому ресурсу
            /// </summary>
            internal string Comment { get; set; }

            /// <summary>
            /// Провайдер
            /// </summary>
            internal string Provider { get; set; }

            /// <summary>
            /// Конструктор по-умолчанию. Инициализирует поля в значения, используемые всегда в рамках ИА.
            /// </summary>
            internal WinApiNetworkResource()
            {
                Scope = enScope.RESOURCE_GLOBALNET;
                ResourceType = enType.RESOURCETYPE_DISK;
                DisplayType = enDisplayType.Share;
                Usage = enUsage.RESOURCEUSAGE_CONNECTABLE;
            }
        }

        /// <summary>
        /// Сетевой ресурс, с которым производится соединение. Не может быть Null.
        /// </summary>
        public NetworkResource Resource { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="resource">Сетевой ресурс</param>
        /// <param name="isNeedWaitWindow">Требуется ли отображать окно ожидания?</param>
        /// <param name="waitWindowMessage">Сообщения для отображения в окне ожидания</param>
        public NetworkConnection(NetworkResource resource,  bool isNeedWaitWindow = false, string waitWindowMessage = null)
        {
            try
            {
                //Проверка на разумность
                if(resource == null)
                    throw new Exception("Сетевой ресурс для соединения не определён.");

                //Запонминаем информацию о соединении
                this.Resource = resource;

                //Если требуется окно ожидания
                if (isNeedWaitWindow)
                {
                    //Устанавливаем соединение, отображая окно ожидания
                    WaitWindow.Show(
                        (window) => this.Connect(),
                        waitWindowMessage ?? "Проверка соединения с сетевым ресурсом <" + resource.Name + ">"
                    );
                }
                //В противном случае
                else
                {
                    //Устанавливаем соединение
                    this.Connect();
                }
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при установке соединения с сетевым ресурсом <" + (resource.Name ?? "неизвестно") + ">.");
            }
        }

        /// <summary>
        /// Устанавливаем соединение
        /// </summary>
        private void Connect()
        {
            //Проверка на разумность
            if (String.IsNullOrWhiteSpace(this.Resource.Name))
                throw new Exception("Имя сетевого ресурса не задано.");

            WinApiNetworkResource winApiNetworkResource = new WinApiNetworkResource()
            {
                RemoteName = this.Resource.UNCName
            };

            //Устанавливаем соединение
            int result = WNetAddConnection2(winApiNetworkResource, this.Resource.Password, this.Resource.Login, 0);

            switch (result)
            {
                case 0:
                    break;
                case 67:
                    throw new Exception("Неверно задано имя сетевого ресурса.");
                case 1219:
                {
                    //Формируем сообщение
                    string message = new string[]
                    {
                        "При попытке установить соединение получен ответ от ОС Windows о том, что соединение занято.",
                        "Код ошибки: " + result,
                        "Подробности: Multiple connections to a server or shared resource by the same user, using more than one user name, are not allowed. Disconnect all previous connections to the server or shared resource and try again.",
                        "Возможное решение: использовать IPv4-адрес вместо сетевого имени либо наоборот."

                    }.ToMultiLineString();

                    throw new Exception(message);
                }
                case 1326:
                    throw new Exception("Неверное имя пользователя или пароль.");
                default:
                    throw new Exception("Номер ошибки: " + result + ".");
            }
        }

        /// <summary>
        /// Финализатор
        /// </summary>
        ~NetworkConnection()
        {
            Dispose(false);
        }

        /// <summary>
        /// Метод, отвечающий за разрыв установленного соединения в случае освобождения ресурсов
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Реализация виртуального метода интерфейса IDisposable. Разрыв установленного ранее соединения.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            WNetCancelConnection2(this.Resource.UNCName, 0, true);
        }

        #region Импортированные методы
        [DllImport("mpr.dll")]
        private static extern int WNetAddConnection2(WinApiNetworkResource netResource, string password, string username, int flags);

        [DllImport("mpr.dll")]
        private static extern int WNetCancelConnection2(string name, int flags, bool force); 
        #endregion
    }
}
