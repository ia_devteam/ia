﻿
using IA.Network;

namespace IA.Objects
{
    /// <summary>
    /// Класс, для взаимодействие с сетью
    /// </summary>
    internal static class NetworkExtensions
    {
        /// <summary>
        /// Сетевой ресурс - Сервер проектов
        /// </summary>
        internal static NetworkResource ProjectsServer =
            new NetworkResource( IA.Settings.ProjectsServer.Name,
                                                IA.Settings.ProjectsServer.Login,
                                                    IA.Settings.ProjectsServer.Password );
    }
}
