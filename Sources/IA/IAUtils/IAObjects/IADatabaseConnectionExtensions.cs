﻿
using IA.Extensions;
using IA.Sql.DatabaseConnections;

namespace IA.Objects
{
    /// <summary>
    /// Класс, реализующий расширения для объектов соединений с базой данных "ia"
    /// </summary>
    internal static class IADatabaseConnectionExtensions
    {
        /// <summary>
        /// Сформировать исключение, если соединение не определено
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <returns>Соединение с базой данных "ia"</returns>
        public static IADatabaseConnection ThrowIfNull(this IADatabaseConnection iaDatabaseConnection)
        {
            return iaDatabaseConnection.ThrowIfNull("Соединение с базой данных <ia> не определено.");
        }
    }
}
