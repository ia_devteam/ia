﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using IA.Extensions;
using IA.Network;
using IA.Sql.DatabaseConnections;

using IOController;

namespace IA.Objects
{
    using Sql;
    using Table = List<string[]>;

    /// <summary>
    /// Класс, реализующий материалы проекта
    /// </summary>
    public class Materials
    {
        /// <summary>
        /// Список операций над набором материалов, связанных с редактированием
        /// </summary>
        public enum EditOperation
        {
            /// <summary>
            /// Выбрать набор материалов
            /// </summary>
            [Description("Выбрать")]
            CHOOSE,
            /// <summary>
            /// Добавить набор материалов
            /// </summary>
            [Description("Добавить")]
            ADD,
            /// <summary>
            /// Удалить набор материалов
            /// </summary>
            [Description("Удалить")]
            REMOVE
        }

        /// <summary>
        /// Список операций над набором материалов, связанных с сервером
        /// </summary>
        public enum ServerOperation
        {
            /// <summary>
            /// Выгрузить очищенные исходные тексты
            /// </summary>
            [Description("Выгрузить очищенные исходные тексты")]
            DOWNLOAD_CLEAR_SRC,
            /// <summary>
            /// Выгрузить лабораторные исходные тексты
            /// </summary>
            [Description("Выгрузить лабораторные исходные тексты")]
            DOWNLOAD_LAB_SRC
        }

        /// <summary>
        /// Поддиректории набора материалов
        /// </summary>
        public enum enSubDirectories
        {
            /// <summary>
            /// Поддиректория для хранения Хранилищ
            /// </summary>
            [Description("Хранилища")]
            STORAGES,
            /// <summary>
            /// Поддиректория для храниния оригинальных исходных текстов
            /// </summary>
            [Description("Исходные тексты оригинальные")]
            SOURCES_ORIGINAL,
            /// <summary>
            /// Поддиректория для храниния очищенных исходных текстов
            /// </summary>
            [Description("Исходные тексты очищенные")]
            SOURCES_CLEAR,
            /// <summary>
            /// Поддиректория для храниния лабораторных исходных текстов
            /// </summary>
            [Description("Исходные тексты лабораторные")]
            SOURCES_LAB,
        }

        /// <summary>
        /// Список самых распространённых исключений при работе с набором материалов
        /// </summary>
        public enum Exceptions
        {
            /// <summary>
            /// Обнаружена неизвестная операция над набором материалов, связанная с редактированием
            /// </summary>
            [Description("Обнаружена неизвестная операция над набором материалов, связанная с редактированием.")]
            UNKNOWN_EDIT_OPERATION,
            /// <summary>
            /// Обнаружена неизвестная операция над набором материалов, связанная с сервером
            /// </summary>
            [Description("Обнаружена неизвестная операция над набором материалов, связанная с сервером.")]
            UNKNOWN_SERVER_OPERATION,
            /// <summary>
            /// Обнаружена неизвестная поддиректория набора материалов.
            /// </summary>
            [Description("Обнаружена неизвестная поддиректория набора материалов.")]
            UNKNOWN_SUBDIRECTORY
        }

        #region Константы
        /// <summary>
        /// 
        /// </summary>
        public const string DefaultComment = "<Комментарий отсутствует>";
        #endregion

        #region Свойства, получаемые из БД
        private uint id = 0;
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [Category("Основное")]
        [DisplayName("Идентификатор")]
        public uint ID
        {
            get
            {
                return id;
            }
        }

        private uint projectID = 0;
        /// <summary>
        /// Идентификатор родительского проекта
        /// </summary>
        [Browsable(false)]
        public uint ProjectID
        {
            get
            {
                return projectID;
            }
            private set
            {
                try
                {
                    //Проверка на разумность
                    if (value == 0)
                        throw new Exception("Неверный идентификатор родительского проекта: " + value + ".");

                    //Вносим изменения в БД
                    this.IADatabaseConnection.ExecuteNonQuery("UPDATE materials SET project = '" + (this.projectID = value) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить родительский проект для набора материалов.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private uint version = 0;
        /// <summary>
        /// Версия
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [Category("Основное")]
        [DisplayName("Версия")]
        public uint Version
        {
            get
            {
                return version;
            }
        }

        private DateTime datetime = DateTime.Now;
        /// <summary>
        /// Дата создания
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [Category("Основное")]
        [DisplayName("Дата создания")]
        public DateTime DateTime
        {
            get
            {
                return datetime;
            }
        }

        private string comment = String.Empty;
        /// <summary>
        /// Комментарий
        /// </summary>
        [Browsable(true)]
        [ReadOnly(false)]
        [DisplayName("Комментарий")]
        public string Comment
        {
            get
            {
                return comment;
            }
            set
            {
                try
                {
                    //Если комментарий не задан
                    if (String.IsNullOrWhiteSpace(value))
                        value = DefaultComment;

                    //Проверка на длину комментария
                    if (value.Length > 1000)
                        throw new Exception("Комментарий слишком длинный. Допустимая длина - 1000 символов.");

                    //Вносим изменения в БД
                    this.IADatabaseConnection.ExecuteNonQuery("UPDATE materials SET comment = @Param1 WHERE id = @Param2;", new string[] { this.comment = value, this.id.ToString() });
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить комментарий к набору материалов.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }
        #endregion

        #region Свойства
        /// <summary>
        /// Соединение с базой данных "ia"
        /// </summary>
        private IADatabaseConnection IADatabaseConnection { get; set; }

        private string path = null;
        /// <summary>
        /// Путь
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [Category("Расположения")]
        [DisplayName("Путь на сервере")]
        public string Path
        {
            get
            {
                return path ?? (path = System.IO.Path.Combine(IA.Settings.ProjectsServer.ProjectsDirectoryPath, Project.GetByID(this.ProjectID).Name, "Материалы ver. " + this.Version));
            }
        }
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="cortege">Запись в базе данных</param>
        private Materials(string[] cortege)
        {
            try
            {
                //Запоминаем соединение с базой данных
                this.IADatabaseConnection = ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (cortege.Length != 5)
                    throw new Exception("Неверный размер кортежа.");

                //Получаем идентификатор набора материалов
                this.id = Convert.ToUInt32(cortege[0]);

                //Проверяем разумность идентификатора на стадии формировании объекта, дальнейшие проверки внутри класса не требуются
                if (this.id == 0)
                    throw new Exception("Неверный идентификтор набора материалов: " + this.id + ".");

                //Получаем родительский проект набора материалов
                this.projectID = String.IsNullOrEmpty(cortege[1]) ? 0 : Convert.ToUInt32(cortege[1]);

                //Получаем версию набора материалов
                this.version = Convert.ToUInt32(cortege[2]);
                
                //Получаем время создания набора материалов
                this.datetime = DateTime.Parse(cortege[3]);

                //Получаем комментарий для набора материалов
                this.comment = cortege[4];
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении информации о наборе материалов из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Получить путь к поддиректории набора материалов
        /// </summary>
        /// <param name="subDirectory">Поддиректория набора материалов</param>
        /// <returns></returns>
        public string GetSubDirectoryPath(enSubDirectories subDirectory)
        {
            return System.IO.Path.Combine(this.Path, subDirectory.Description());
        }

        /// <summary>
        /// Выгрузить содержимое поддиректории набора материалов
        /// </summary>
        /// <param name="subDirectory">Поддиректория набора материалов</param>
        /// <param name="destinationDirectoryPath">Путь до директории, куда следует выгрузить содержимое поддиректории набора материалов</param>
        public void DownloadSubDirectoryContents(enSubDirectories subDirectory, string destinationDirectoryPath)
        {
            try
            {
                //Получаем путь до подиректории набора материалов
                string subDirectoryPath = this.GetSubDirectoryPath(subDirectory);

                //Выгружаем информацию с сервера с разархивацией
                Archiving.Extract.FromProjectsServer(subDirectoryPath, destinationDirectoryPath);
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем исключение
                throw new Exception("Ошибка при копировании содержимого поддиректории набора материалов с сервера проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        #region Статические методы
        /// <summary>
        /// Получить набор материалов по идентификтору
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="ID">Идентификатор набора материалов</param>
        /// <returns>Объект набора материалов</returns>
        public static Materials GetByID(uint ID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (ID == 0)
                    throw new Exception("Неверный идентификатор набора материалов: " + ID + ".");

                Table table = SqlConnector.IADB.DatabaseConnection.ExecuteReader("SELECT * FROM materials WHERE id = '" + ID + "';");

                //Проверка результата
                if (table.Count != 1)
                    throw new Exception((table.Count > 1 ? "Найдено несколько наборов материалов" : "Не найдено ни одного набора материалов") + " с заданным идентификатором: " + ID + ".");

                return new Materials(table[0]);
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении набора материалов по идентификатору из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Получить наборы материалов по заданному идентификатору родительского проекта
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="projectID">Идентификатор родительского проекта</param>
        /// <returns>Список объектов наборов материалов</returns>
        public static List<Materials> GetByProject(uint projectID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (projectID == 0)
                    throw new Exception("Неверный идентификатор родительского проекта: " + projectID + ".");

                List<Materials> ret = new List<Materials>();

                foreach (string[] cortege in ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteReader("SELECT * FROM materials WHERE project = '" + projectID + "' ORDER BY ID;"))
                    ret.Add(new Materials(cortege));

                return ret;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении наборов материалов по идентификатору родительского проекта из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Добавить набор материалов
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="originalSourcesDirectoryPath">Путь до директории с исходными текстами для анализа</param>
        /// <param name="projectID">Идентификатор родительского проекта</param>
        /// <param name="comment">Комментарий</param>
        /// <returns>Объект добавленного набора материалов</returns>
        public static Materials Add(string originalSourcesDirectoryPath, uint projectID, string comment = DefaultComment)
        {
            //Идентификатор нового набора материалов
            uint id = 0;

            try
            {
                //Устанавливаем соединение с сервером проектов
                using (new NetworkConnection(NetworkExtensions.ProjectsServer, true))
                {
                    //Проверка на разумность
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                    //Проверка на разумность
                    originalSourcesDirectoryPath.ThrowIfNull("Путь до директории с исходными текстами для анализа не определён.");

                    //Проверка на существование директории с исходными текстами для анализа
                    if(!DirectoryController.IsExists(originalSourcesDirectoryPath))
                        throw new Exception("Путь до директории с исходными текстами для анализа не существует.");

                    //Выясняем максимальный номер текущих материалов
                    string versionString = ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteScalar("SELECT MAX(version) AS version FROM materials WHERE project = '" + projectID + "';");

                    //Получаем номер новой версии
                    var version = 1 + (String.IsNullOrEmpty(versionString) ? 0 : Convert.ToInt32(versionString));

                    //Добавляем новыую запись о материалах
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteNonQuery("INSERT INTO materials (project, version, comment) VALUES(0,'" + version + "', '');");

                    //Получаем идентификатор вновь добавленного набора материалов
                    id = Convert.ToUInt32(((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteScalar("SELECT MAX(id) FROM materials;"));

                    //Получаем вновь созданный набор материалов
                    Materials materials = Materials.GetByID(id);

                    //Задаём родительский проект
                    materials.ProjectID = projectID;

                    //Задаём комментарий
                    materials.Comment = comment;

                    //Создаём директорию для вновь созданного набота материалов
                    if (DirectoryController.IsExists(materials.Path))
                        throw new Exception("Директория для вновь созданного набора материалов изначально не должна существовать.");
                    else
                        DirectoryController.Create(materials.Path);

                    //Создаём все поддиректории набора материалов
                    EnumLoop<enSubDirectories>.ForEach(directory => DirectoryController.Create(materials.GetSubDirectoryPath(directory)));

                    //Формируем путь до временной директории для создания архива
                    string tempDirectoryPath = System.IO.Path.Combine(IA.Settings.CommonWorkDirectoryPath, "_tmp");

                    //Загружаем исходные тексты для анализа с архивацией на сервер
                    Archiving.Archive.ToProjectsServer(originalSourcesDirectoryPath, tempDirectoryPath, materials.GetSubDirectoryPath(enSubDirectories.SOURCES_ORIGINAL));
                    
                    return materials;
                }
            }
            catch (Exception ex)
            {
                //Если успели создать запись в БД, удаляем её
                if (id != 0)
                    Materials.RemoveByID(id);
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при добавлении набора материалов в базу данных или на сервер проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Удалить набор материалов с заданным идентификатором 
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="ID">Идентификатор набора материалов</param>
        public static void RemoveByID(uint ID)
        {
            try
            {
                //Устанавливаем соединение с сервером проектов
                using (new NetworkConnection(NetworkExtensions.ProjectsServer, true))
                {
                    //Проверка на разумность
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                    //Проверка на разумность
                    if (ID == 0)
                        throw new Exception("Неверный идентификатор набора материалов: " + ID + ".");

                    Materials deletedMaterials = Materials.GetByID(ID);

                    //Удаляем все Хранилища связынные с данными материалами
                    Storage.RemoveByMaterials(ID);

                    //Удаляем директорию
                    if (DirectoryController.IsExists(deletedMaterials.Path))
                        DirectoryController.Remove(deletedMaterials.Path, true);

                    //Удаляем материалы из БД
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteNonQuery("DELETE FROM materials WHERE id = '" + ID + "';");
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при удалении набора материалов по идентификатору из базы данных или с сервера проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Удалить наборы материалов с заданным идентификатором родительского проекта
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="projectID">Идентификатор родительского проекта</param>
        public static void RemoveByProject(uint projectID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (projectID == 0)
                    throw new Exception("Неверный идентификатор родительского проекта: " + projectID + ".");

                foreach (string[] cortege in ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteReader("SELECT id FROM materials WHERE project = '" + projectID + "';"))
                    Materials.RemoveByID(Convert.ToUInt32(cortege[0]));
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при удалении набора материалов по идентификатору родительского проекта из базы данных или с сервера проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }
        #endregion
    }
}
