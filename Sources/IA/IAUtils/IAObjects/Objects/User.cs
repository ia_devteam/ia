﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using IA.Sql.DatabaseConnections;

namespace IA.Objects
{
    using Sql;
    using Table = List<string[]>;

    /// <summary>
    /// Класс, описывающий исключение, связанное с ненайденным пользователем
    /// </summary>
    public class UserNotFoundException : Exception
    {
        /// <summary>
        /// стандартный конструктор
        /// </summary>
        /// <param name="message"></param>
        public UserNotFoundException(string message):base(message)
        { }
    }

    /// <summary>
    /// Класс, описывающий пользователя
    /// </summary>
    public class User
    {
        /// <summary>
        /// Список операций над пользователями, связанных с редактированием
        /// </summary>
        public enum EditOperation
        {
            /// <summary>
            /// Добавить пользователя
            /// </summary>
            [Description("Добавить")]
            ADD,
            /// <summary>
            /// Удалить пользователся
            /// </summary>
            [Description("Удалить")]
            REMOVE,
            /// <summary>
            /// Редактировать пользователя
            /// </summary>
            [Description("Изменить")]
            EDIT
        }

        /// <summary>
        /// Список самых распространённых исключений при работе с пользователями
        /// </summary>
        public enum Exceptions
        {
            /// <summary>
            /// Обнаружена неизвестная операция с пользователем, связанная с редактированием
            /// </summary>
            [Description("Обнаружена неизвестная операция с пользователем, связанная с редактированием.")]
            UNKNOWN_EDIT_OPERATION
        }

        #region Свойства, получаемые из БД
        private uint id = 0;
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public uint ID
        {
            get
            {
                return id;
            }
        }

        private string surname = null;
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname
        {
            get
            {
                return this.surname;
            }
            set
            {
                try
                {
                    //Проверка на разумность
                    if (String.IsNullOrWhiteSpace(value))
                        throw new Exception("Фамилия пользователя не определено или пусто.");

                    //Проверяем что пользователя с указанным ФИО нет в БД
                    if (this.IADatabaseConnection.ExecuteReader("SELECT * FROM users WHERE surname = '" + value + "' AND name = '" + this.name + "' AND patronymic = '" + this.patronymic + "';").Count != 0)
                        throw new Exception("Такой пользователь уже существует.");

                    //Проверка на длину фамилии
                    if (value.Length > 50)
                        throw new Exception("Фамилия слишком длинная. Допустимая длина - 50 символов.");

                    //Вносим изменения в БД
                    this.IADatabaseConnection.ExecuteNonQuery("UPDATE users SET surname = '" + (this.surname = value) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    //Формируем исключение
                    throw new Exception("Ошибка при попытке изменить фамилию пользователя.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private string name = null;
        /// <summary>
        /// Имя
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                try
                {
                    //Проверка на разумность
                    if (String.IsNullOrWhiteSpace(value))
                        throw new Exception("Имя пользователя не определено или пусто.");

                    //Проверяем что пользователя с указанным ФИО нет в БД
                    if (this.IADatabaseConnection.ExecuteReader("SELECT * FROM users WHERE surname = '" + this.surname + "' AND name = '" + value + "' AND patronymic = '" + this.patronymic + "';").Count != 0)
                        throw new Exception("Такой пользователь уже существует.");

                    //Проверка на длину имени
                    if (value.Length > 50)
                        throw new Exception("Имя слишком длинное. Допустимая длина - 50 символов.");

                    //Вносим изменения в БД
                    this.IADatabaseConnection.ExecuteNonQuery("UPDATE users SET name = '" + (this.name = value) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    //Формируем исключение
                    throw new Exception("Ошибка при попытке изменить имя пользователя.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private string patronymic = null;
        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic
        {
            get
            {
                return this.patronymic;
            }
            set
            {
                try
                {
                    //Проверка на разумность
                    if (String.IsNullOrWhiteSpace(value))
                        throw new Exception("Отчество пользователя не определено или пусто.");

                    //Проверяем что пользователя с указанным ФИО нет в БД
                    if (this.IADatabaseConnection.ExecuteReader("SELECT * FROM users WHERE surname = '" + this.surname + "' AND name = '" + this.name + "' AND patronymic = '" + value + "';").Count != 0)
                        throw new Exception("Такой пользователь уже существует.");

                    //Проверка на длину отчества
                    if (value.Length > 50)
                        throw new Exception("Отчество слишком длинное. Допустимая длина - 50 символов.");

                    //Вносим изменения в БД
                    this.IADatabaseConnection.ExecuteNonQuery("UPDATE users SET patronymic = '" + (this.patronymic = value) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    //Формируем исключение
                    throw new Exception("Ошибка при попытке изменить отчество пользователя.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private bool isDeleted = false;
        /// <summary>
        /// Считается ли пользователь удалённым?
        /// </summary>
        public bool IsDeleted
        {
            get
            {
                return this.isDeleted;
            }
            set
            {
                try
                {
                    //Вносим изменения в БД
                    this.IADatabaseConnection.ExecuteNonQuery("UPDATE users SET deleted = '" + (this.isDeleted = value).ToString() + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке пометить пользователя, как удалённый/восстановленный.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }
        #endregion

        #region Свойства
        /// <summary>
        /// Соединение с базой данных "ia"
        /// </summary>
        private IADatabaseConnection IADatabaseConnection { get; set; }
        /// <summary>
        /// Полное имя
        /// </summary>
        public string Fullname
        {
            get
            {
                return this.surname + " " + this.name + " " + this.patronymic;
            }
        }
        #endregion

        static User offlineuser;
        public static User GetOfflineUser()
        {
            return offlineuser = new User("OfflineUser");
        }

        private User(string name)
        {
            this.id = 1;
            this.surname = this.name  = this.patronymic = name;
            this.isDeleted = false;            
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="cortege">Запись в базе данных</param>
        private User(string[] cortege)
        {
            try
            {
                //Запоминаем соединение с базой данных
                this.IADatabaseConnection = ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                if (cortege.Length != 5)
                    throw new Exception("Неверный размер кортежа.");

                //Получаем идентификатор пользователя
                this.id = Convert.ToUInt32(cortege[0]);

                //Проверяем разумность идентификатора на стадии формировании объекта, дальнейшие проверки внутри класса не требуются
                if (this.id == 0)
                    throw new Exception("Неверный идентификтор пользователя: " + this.id + ".");

                //Получаем фамилию пользователя
                this.surname = cortege[1];

                //Получаем имя пользователя
                this.name = cortege[2];

                //Получаем отчество пользователя
                this.patronymic = cortege[3];

                //Получаем информацию о том, считается ли пользователь удалённым или нет
                this.isDeleted = Convert.ToBoolean(cortege[4]);
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении информации о пользователе из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Перегруженный метод
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Fullname;
        }

        #region Сатические методы

        /// <summary>
        /// Получить список всех пользователей
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <returns>Список объектов пользователей</returns>
        public static List<User> GetAll()
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                List<User> ret = new List<User>();

                foreach (string[] cortege in ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteReader("SELECT * FROM users;"))
                    ret.Add(new User(cortege));

                return ret;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении списка всех пользователей из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Получить пользователя по идентификтору
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="ID">Идентификатор пользователя</param>
        /// <returns>Объект пользователя</returns>
        public static User GetByID(uint ID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (ID == 0)
                    throw new Exception("Неверный идентификатор пользователя: " + ID + ".");

                Table table = SqlConnector.IADB.DatabaseConnection.ExecuteReader("SELECT * FROM users WHERE id = '" + ID + "';");

                //Проверка результата
                if (table.Count != 1)
                {
                    if (table.Count > 1)
                        throw new Exception("Найдено несколько пользователей с заданным идентификатором: " + ID + ".");
                    else
                        throw new UserNotFoundException("Не найдено ни одного пользователя с заданным идентификатором: " + ID + ".");
                }

                return new User(table[0]);
            }
            catch(UserNotFoundException)
            {
                throw;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении пользователя по идентификатору из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Получить пользователя по полному имени
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="surname">Фамилия</param>
        /// <param name="name">Имя</param>
        /// <param name="patronymic">Отчество</param>
        /// <returns>Объект пользователя</returns>
        public static User GetByFullName(string surname, string name, string patronymic)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (String.IsNullOrWhiteSpace(surname) || String.IsNullOrWhiteSpace(name) || String.IsNullOrWhiteSpace(patronymic))
                    throw new Exception("Полное имя пользователя указано неверно.");

                Table table = SqlConnector.IADB.DatabaseConnection.ExecuteReader("SELECT * FROM users WHERE surname = '" + surname + "' AND name = '" + name + "' AND patronymic = '" + patronymic + "';");

                //Проверка результата
                if (table.Count > 1)
                    throw new Exception("Найдено несколько пользователей с заданным полным именем: " + surname + " " + name + " " + patronymic + ".");

                return (table.Count == 0) ? null : new User(table[0]);
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении пользователя по полному имени из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Добавить пользователя
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="surname">Фамилия пользователя</param>
        /// <param name="name">Имя пользователя</param>
        /// <param name="patronymic">Отчество пользователя</param>
        /// <returns>Объект добавленного пользователя</returns>
        public static User Add(string surname, string name, string patronymic)
        {
            //Идентификатор нового пользователя
            uint id = 0;

            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Добавляем нового пользователя в БД
                SqlConnector.IADB.DatabaseConnection.ExecuteNonQuery("INSERT INTO users (surname, name, patronymic) VALUES('', '', '');");

                //Получаем идентификатор вновь добавленного пользователя
                id = Convert.ToUInt32(SqlConnector.IADB.DatabaseConnection.ExecuteScalar("SELECT MAX(id) FROM users;"));

                //Получаем вновь созданного пользователя
                User user = User.GetByID(id);

                //Задаём фамилию пользователя
                user.Surname = surname;

                //Задаём имя пользователя
                user.Name = name;

                //Задаём отчество пользователя
                user.Patronymic = patronymic;

                return user;
            }
            catch (Exception ex)
            {
                //Если успели создать запись в БД, удаляем её
                if (id != 0)
                    User.RemoveByID(id);

#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при добавлении пользователя в базу данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Удалить пользователя по идентификатору
        /// </summary>
        /// <param name="iaDatabaseConnection">соединение с БД</param>
        /// <param name="ID">Идентификатор пользователя</param>
        public static void RemoveByID(uint ID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (ID == 0)
                    throw new Exception("Неверный идентификатор проекта: " + ID + ".");

                //Удаляем пользователя из базы данных
                SqlConnector.IADB.DatabaseConnection.ExecuteNonQuery("DELETE FROM users WHERE id = '" + ID + "';");
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при удалении пользователя по идентификатору из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }
        #endregion
    }
}
