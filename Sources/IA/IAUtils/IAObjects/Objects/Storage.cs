﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.ComponentModel;
using IA.Network;
using IA.Plugin;
using IA.Sql.DatabaseConnections;

using IOController;

namespace IA.Objects
{
    using Table = List<string[]>;
    using Store;
    using Sql;
    /// <summary>
    /// Класс, описывающий Хранилище
    /// </summary>
    public class Storage
    {
        /// <summary>
        /// Режим работы с Хранилищем
        /// </summary>
        public enum enMode
        {
            /// <summary>
            /// Основной режим
            /// </summary>
            [Description("Основной режим")]
            BASIC,
            /// <summary>
            /// Экспертный режим
            /// </summary>
            [Description("Экспертный режим")]
            EXPERT,
        }

        /// <summary>
        /// Класс, отвечающий за взаимодействие с реальным Хранилищем
        /// </summary>
        public class StorageController
        {
            #region Константы
            /// <summary>
            /// Имя экземпляра класса
            /// </summary>
            public const string ObjectName = "Контроллер Хранилища";

            /// <summary>
            /// Имя файла архива с Хранилищем
            /// </summary>
            private const string StorageArchiveName = "storage";
            #endregion

            private WorkDirectory workDirectory;

            /// <summary>
            /// Хранилище (Не может быть NULL)
            /// </summary>
            public Store.Storage Storage { get; private set; }

            /// <summary> 
            /// Родитель (Объект Хранилища из БД) (Не может быть NULL)
            /// </summary>
            private IA.Objects.Storage parent;

            /// <summary>
            /// Конструктор
            /// </summary>
            public StorageController(Storage parent)
            {
                //Проверка на разумность
                if (parent == null)
                    throw new Exception("Ошибка создания контроллера Хранилища. Родитель (Объект Хранилища в базе данных) не может быть неопределён.");

                //Хранилище изначально не создано
                this.Storage = new Store.Storage();

                //Запоминаем родителя
                this.parent = parent;
            }

            /// <summary>
            /// Открыть хранилище
            /// </summary>
            /// <param name="mode">Режим открытия Хранилища</param>
            public void Open(Store.Storage.OpenMode mode)
            {
                try
                {
                    //Проверка на разумность
                    if (parent.WorkDirectory == null)
                        throw new Exception("Путь к рабочей директории не определён.");

                    //Проверка на разумность
                    if (!DirectoryController.IsExists(parent.WorkDirectory.Path))
                        throw new Exception("Путь к рабочей директории <" + parent.WorkDirectory.Path + "> не существует.");

                    //Проверка на то, что Хранилище было закрыто
                    if (!this.Storage.isClosed)
                        throw new Exception("Хранилище не было закрыто.");

                    //Получаем путь до поддиректории для Хранилища
                    string storageDirectoryPath = parent.WorkDirectory.GetSubDirectoryPathWithUnpack(Store.WorkDirectory.enSubDirectories.STORAGE);

                    switch (mode)
                    {
                        case Store.Storage.OpenMode.CREATE:
                            {
                                //Проверка на то что поддиректория для Хранилища пуста
                                if (!DirectoryController.IsEmpty(storageDirectoryPath))
                                    throw new Exception("Поддиректория для Хранилища <" + storageDirectoryPath + "> не пуста.");

                                break;
                            }
                        case Store.Storage.OpenMode.OPEN:
                            {
                                //Проверка на то что в поддиректория для Хранилища содержит Хранилище
                                if (!Store.Storage.IsStorageExists(parent.WorkDirectory))
                                    throw new Exception("Поддиректория для Хранилища <" + storageDirectoryPath + "> не содержит Хранилище.");

                                break;
                            }
                        default:
                            throw new Exception("Неизвестный режим открытия Хранилища.");
                    }

                    //Создаём\Открываем Хранилище
                    this.Storage.Open(parent.WorkDirectory, mode);

                    //Проходим по всем плагинам
                    HandlersInitial.GetAll().ForEach(plugin =>
                    {
                        //Инициализируем плагин
                        plugin.Initialize(this.Storage);

                        //Получаем настройки плагина из Хранилища
                        plugin.LoadSettings(this.Storage);
                    });

                    //Если открыли Хранилище
                    if (mode == Store.Storage.OpenMode.OPEN)
                    {
                        //Отсутствующие плагины можно смело пропускать
                        Func<uint, bool> missAllowedCondition = (pluginID) => { return true; };

                        //Выгружаем информацию о выполненных плагинах
                        HandlersInitial.GetSomeByIDs(
                            parent.Runned_Plugins,
                            missAllowedCondition
                        ).ForEach(p => p.RunResult = Handler.ExecutionResult.SUCCESS);

                        //Выгружаем информацию о завершённых плагинах
                        HandlersInitial.GetSomeByIDs(
                            parent.Completed_Plugins,
                            missAllowedCondition
                        ).ForEach(p => p.IsCompleted = true);
                    }
                }
                catch (Exception ex)
                {
                    //Если Хранилище удалось открыть на более низком уровне, закрываем его
                    if (!this.Storage.isClosed)
                        this.Storage.Close();

                    //Формируем сообщение
                    string message =
#if DEBUG
                        "Ошибка при " + (mode == Store.Storage.OpenMode.CREATE ? "создании" : "открытии") + " Хранилища (" + ObjectName + ").";
#else
                        "Ошибка при открытии Хранилища.";
#endif

                    //Формируем исключение
                    throw new Exception(message, ex);
                }
            }

            /// <summary>
            /// Закрыть хранилище
            /// </summary>
            public void Close()
            {
                try
                {
                    //Проверка на разумность
                    if (this.Storage.isClosed)
                        throw new Exception("Хранилище либо не было открыто, либо было закрыто ранее.");

                    //Закрываем Хранилище
                    workDirectory = this.Storage.WorkDirectory;
                    this.Storage.Close();
                }
                catch (Exception ex)
                {
                    //Формируем сообщение
                    string message =
#if DEBUG
                        "Ошибка при закрытии Хранилища (" + ObjectName + ").";
#else
                        "Ошибка при закрытии Хранилища.";
#endif

                    //Формируем исключение
                    throw new Exception(message, ex);
                }
            }

            /// <summary>
            /// Выгрузить Хранилище с сервера
            /// </summary>
            public void Download()
            {
                try
                {
                    //Проверка на разумность
                    if (parent.WorkDirectory == null)
                        throw new Exception("Рабочая директория не определена.");

                    //Если Хранилище на сервере не пусто
                    if (!parent.IsEmpty)
                        //Выгружаем (с разархивацией) Хранилище с сервера
                        this.Download(
                            parent.Path,
                            Store.WorkDirectory.enSubDirectories.STORAGE
                        );

                    //Получаем набор материалов
                    Materials materials = Materials.GetByID(parent.MaterialsID);

                    //Выгружаем (с разархивацией) оригинальные исходные тексты с сервера
                    this.Download(
                        materials.GetSubDirectoryPath(Materials.enSubDirectories.SOURCES_ORIGINAL),
                        Store.WorkDirectory.enSubDirectories.SOURCES_ORIGINAL
                    );

                    //Выгружаем (с разархивацией) очищенные исходные тексты с сервера
                    this.Download(
                        materials.GetSubDirectoryPath(Materials.enSubDirectories.SOURCES_CLEAR),
                        Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR
                    );

                    //Выгружаем (с разархивацией) лабораторные исходные тексты с сервера
                    this.Download(
                        materials.GetSubDirectoryPath(Materials.enSubDirectories.SOURCES_LAB),
                        Store.WorkDirectory.enSubDirectories.SOURCES_LAB
                    );
                }
                catch (Exception ex)
                {
                    //Формируем сообщение
                    string message =
#if DEBUG
                        "Ошибка при выгрузке Хранилища с сервера проектов (" + ObjectName + ").";
#else
                        "Ошибка при выгрузке Хранилища с сервера проектов.";
#endif

                    //Формируем исключение
                    throw new Exception(message, ex);
                }
            }

            /// <summary>
            /// Загрузить Хранилище на сервер
            /// </summary>
            /// <param name="userID">Идентификатор пользователя</param>
            /// <param name="mode">Режим работы с Хранилищем</param>
            /// <param name="comment">Комментарий</param>
            public void Upload(uint userID, Storage.enMode mode, string comment = IA.Objects.Storage.StorageDefaultComment)
            {
                try
                {
                    //Проверка на разумность
                    if (parent.WorkDirectory == null)
                        throw new Exception("Рабочая директория не определена.");

                    //Получаем список идентификаторов выполненных плигинов
                    uint[] runned_plugins = HandlersInitial.GetSomeByCondition(p => p.RunResult == Handler.ExecutionResult.SUCCESS).Select(p => p.ID).ToArray();

                    //Получаем список идентификаторов завершённых
                    uint[] completed_plugins = HandlersInitial.GetSomeByCondition(p => p.IsCompleted).Select(p => p.ID).ToArray();

                    //Создаём новое Хранилище в БД
                    Objects.Storage storage = Objects.Storage.Add(parent.materialsID, parent.ID, userID, mode, false, comment, runned_plugins, completed_plugins);

                    //Загружаем (с архивацией) Хранилище на сервер
                    this.ArchiveAndUpload(
                        Store.WorkDirectory.enSubDirectories.STORAGE,
                        storage.Path
                    );

                    //Устанавливаем соединение с сервером проектов
                    using (new NetworkConnection(NetworkExtensions.ProjectsServer, true))
                    {
                        //Получаем набор материалов
                        Materials materials = Materials.GetByID(parent.MaterialsID);

                        //Если очищенных исходных текстов не было и они появились
                        this.UploadIfNo(
                            Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR,
                            materials,
                            Materials.enSubDirectories.SOURCES_CLEAR
                        );

                        //Если лабораторных исходных текстов не было или их набор изменился
                        this.UploadWithReplace(
                            Store.WorkDirectory.enSubDirectories.SOURCES_LAB,
                            materials,
                            Materials.enSubDirectories.SOURCES_LAB
                        );
                    }
                }
                catch (Exception ex)
                {
                    //Формируем сообщение
                    string message =
#if DEBUG
                        "Ошибка при загрузке Хранилища на сервер проектов (" + ObjectName + ").";
#else
                        "Ошибка при загрузке Хранилища на сервер проектов.";
#endif

                    //Формируем исключение
                    throw new Exception(message, ex);
                }
            }

            /// <summary>
            /// Скопировать содержимое поддиректории рабочей директории Хранилища
            /// </summary>
            /// <param name="workSubDirectory">Поддиректория рабочей директории Хранилища</param>
            /// <param name="destinationDirectoryPath">Путь до директории, куда следует скопировать содержимое поддиректории рабочей директории Хранилища</param>
            public void CopyWorkSubDirectoryContents(Store.WorkDirectory.enSubDirectories workSubDirectory, string destinationDirectoryPath)
            {
                try
                {
                    //Копируем содержимое поддиректории рабочей директории Хранилища
                    DirectoryController.Copy(this.Storage.WorkDirectory.GetSubDirectoryPath(workSubDirectory), destinationDirectoryPath);
                }
                catch (Exception ex)
                {
                    //Формируем сообщение
                    string message =
#if DEBUG
                        "Ошибка при копировании содержимого поддиректории рабочей директории Хранилища (" + ObjectName + ").";
#else
                        "Ошибка при копировании данных из Хранилища.";
#endif

                    //Формируем исключение
                    throw new Exception(message, ex);
                }
            }

            /// <summary>
            /// Выгрузить информацию с серерва её в поддиректорию рабочей директории Хранилища
            /// </summary>
            /// <param name="serverDirectoryPath">Путь до директории на сервере</param>
            /// <param name="workSubDirectory">Поддиректория рабочей директории Хранилища</param>
            private void Download(string serverDirectoryPath, Store.WorkDirectory.enSubDirectories workSubDirectory)
            {
                //Получаем путь до поддиректории рабочей директории Хранилища
                string workSubDirectoryPath = parent.WorkDirectory.GetSubDirectoryPath(workSubDirectory);

                //Выгружаем информацию с сервера с разархивацией
                Archiving.Extract.FromProjectsServer(serverDirectoryPath, workSubDirectoryPath);
            }

            /// <summary>
            /// Заархивировать указанную поддиректорию рабочей директории Хранилища и загрузить её на сервер
            /// </summary>
            /// <param name="workSubDirectory">Поддиректория рабочей директории Хранилища</param>
            /// <param name="serverDirectoryPath">Путь до директории на сервере</param>
            private void ArchiveAndUpload(Store.WorkDirectory.enSubDirectories workSubDirectory, string serverDirectoryPath)
            {
                //Получаем путь до поддиректории рабочей директории Хранилища
                if (workDirectory != null && workDirectory.IsFolderUnpacked(workSubDirectory) &&
                    Directory.EnumerateFileSystemEntries(parent.WorkDirectory.GetSubDirectoryPath(workSubDirectory)).Count() != 0)
                {
                    string workSubDirectoryPath = parent.WorkDirectory.GetSubDirectoryPath(workSubDirectory);

                    //Формируем путь до временной директории
                    string tempDirectoryPath = System.IO.Path.Combine(parent.workDirectory.Path, "_tmp_archive");

                    //Архивируем информацию с загрузкой на сервер
                    Archiving.Archive.ToProjectsServer(workSubDirectoryPath, tempDirectoryPath, serverDirectoryPath);
                }
                else
                {
                    DirectoryController.Copy(parent.WorkDirectory.GetSubDirectoryPath(workSubDirectory), serverDirectoryPath);
                }
            }

            /// <summary>
            /// Загрузить информацию из поддиректории рабочей директории Хранилища в поддиректорию набора материалов на сервере при условии,
            /// что на сервере такой информации нет.
            /// </summary>
            /// <param name="workSubDirectory">Поддиректория рабочей директории Хранилища</param>
            /// <param name="materials">Набор материалов</param>
            /// <param name="materialsSubDirectory">Поддиректория набора материалов</param>
            private void UploadIfNo(Store.WorkDirectory.enSubDirectories workSubDirectory, Materials materials, Materials.enSubDirectories materialsSubDirectory)
            {
                //Получаем путь до поддиректории рабочей директории Хранилища
                string workSubDirectoryPath = parent.WorkDirectory.GetSubDirectoryPathWithUnpack(workSubDirectory);

                //Получаем путь до поддиректории набора материалов
                string materialsSubDirectoryPath = materials.GetSubDirectoryPath(materialsSubDirectory);

                //Если нечего загружать, ничего не делаем
                if (DirectoryController.IsEmpty(workSubDirectoryPath))
                    return;

                //Если на сервере уже есть какая-то информация, ничего не делаем
                if (!DirectoryController.IsEmpty(materialsSubDirectoryPath))
                    return;

                //Загружаем (с архивацией) содержимое поддиректории рабочей директории Хранилища на сервер
                this.ArchiveAndUpload(
                    workSubDirectory,
                    materialsSubDirectoryPath
                );
            }

            /// <summary>
            /// Загрузить информацию из поддиректории рабочей директории Хранилища в поддиректорию набора материалов на сервере при условии,
            /// что информация на свереве не совпадает с информацией в поддиректории рабочей директории Хранилища.
            /// </summary>
            /// <param name="workSubDirectory">Поддиректория рабочей директории Хранилища</param>
            /// <param name="materials">Набор материалов</param>
            /// <param name="materialsSubDirectory">Поддиректория набора материалов</param>
            private void UploadWithReplace(Store.WorkDirectory.enSubDirectories workSubDirectory, Materials materials, Materials.enSubDirectories materialsSubDirectory)
            {
                //Получаем путь до поддиректории рабочей директории Хранилища
                string workSubDirectoryPath = parent.WorkDirectory.GetSubDirectoryPath(workSubDirectory);

                //Получаем путь до поддиректории набора материалов
                string materialsSubDirectoryPath = materials.GetSubDirectoryPath(materialsSubDirectory);

                //Если нечего загружать, ничего не делаем
                if (DirectoryController.IsEmpty(workSubDirectoryPath))
                    return;

                //FIXME - тут нужно реализовать сверку содержимого, а не просто каждый раз подменять

                //Очищаем директорию на сервере
                DirectoryController.Clear(materialsSubDirectoryPath);

                //Загружаем (с архивацией) содержимое поддиректории рабочей директории Хранилища на сервер
                this.ArchiveAndUpload(
                    workSubDirectory,
                    materialsSubDirectoryPath
                );
            }
        }

        /// <summary>
        /// Список операций над Хранилищем, связанных с редактированием
        /// </summary>
        public enum EditOperation
        {
            /// <summary>
            /// Выбрать Хранилище
            /// </summary>
            [Description("Выбрать")]
            CHOOSE,
            /// <summary>
            /// Добавить Хранилище
            /// </summary>
            [Description("Добавить")]
            ADD,
            /// <summary>
            /// Удалить Хранилище
            /// </summary>
            [Description("Удалить")]
            REMOVE,
            /// <summary>
            /// Выгрузить Хранилище для автономной работы
            /// </summary>
            [Description("Выгрузить")]
            UNLOAD
        }

        /// <summary>
        /// Список операций над Хранилищем, связанных с сервером
        /// </summary>
        public enum ServerOperation
        {
            ///// <summary>
            ///// Загрузить Хранилище с сервера
            ///// </summary>
            //[Description("Выгрузить")]
            //DOWNLOAD,
            /// <summary>
            /// Сохранить Хранилище на сервере
            /// </summary>
            [Description("Загрузить")]
            UPLOAD,
            /// <summary>
            /// Сохранить Хранилище на сервере
            /// </summary>
            [Description("Обработка пакета хранилищ")]
            PACKPROCESS
        }

        /// <summary>
        /// Список операций над Хранилищем, связанных с просмотром его содержимого
        /// </summary>
        public enum BrowseOperation
        {
            /// <summary>
            /// Отобразить файлы, содержащиеся в Хранилище
            /// </summary>
            [Description("Файлы")]
            FILES,
            /// <summary>
            /// Отобразить функции, содержащиеся в Хранилище
            /// </summary>
            [Description("Функции")]
            FUNCTIONS,
            /// <summary>
            /// Отобразить переменные, содержащиеся в Хранилище
            /// </summary>
            [Description("Переменные")]
            VARIABLES
        }

        /// <summary>
        /// Список самых распространённых исключений при работе с Хранилищем
        /// </summary>
        public enum Exceptions
        {
            /// <summary>
            /// Обнаружена неизвестная операция с Хранилищем, связанная с редактированием
            /// </summary>
            [Description("Обнаружена неизвестная операция с Хранилищем, связанная с редактированием.")]
            UNKNOWN_EDIT_OPERATION,
            /// <summary>
            /// Обнаружена неизвестная операция с Хранилищем, связанная с сервером
            /// </summary>
            [Description("Обнаружена неизвестная операция с Хранилищем, связанная с сервером.")]
            UNKNOWN_SERVER_OPERATION,
            /// <summary>
            /// Обнаружена неизвестная операция с Хранилищем, связанная с просмотром его содержимого
            /// </summary>
            [Description("Обнаружена неизвестная операция с Хранилищем, связанная с просмотром его содержимого.")]
            UNKNOWN_BROWSE_OPERATION
        }

        #region Константы
        /// <summary>
        /// Комментарий для Хранилищ по умолчанию
        /// </summary>
        public const string StorageDefaultComment = "<Комментарий отсутствует>";

        /// <summary>
        /// Комментарий для автоматически сгенерированных Хранилищ
        /// </summary>
        public const string StorageAutoGeneratedComment = "<Автоматически сгенерированное Хранилище>";
        #endregion

        #region Свойства, получаемые из БД
        private uint id = 0;
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [Category("Основное")]
        [DisplayName("Идентификатор")]
        public uint ID
        {
            get
            {
                return id;
            }
        }

        private uint materialsID = 0;
        /// <summary>
        /// Идентификатор материалов
        /// </summary>
        [Browsable(false)]
        public uint MaterialsID
        {
            get
            {
                return materialsID;
            }
            private set
            {
                try
                {

                    if (value == 0)
                        throw new Exception("Неверный идентифиатор набора материалов: " + value + ".");

                    this.materialsID = value;
                    if (!Settings.AutonomyMode)
                        this.IADatabaseConnection.ExecuteNonQuery("UPDATE storages SET materials = '" + (this.materialsID) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить набор материалов, к которому принадлежит Хранилище.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private uint parentID = 0;
        /// <summary>
        /// Идентификатор родителя (Хранилища)
        /// </summary>
        [Browsable(false)]
        public uint ParentID
        {
            get
            {
                return parentID;
            }
            private set
            {
                try
                {
                    this.parentID = value;
                    if (!Settings.AutonomyMode)
                        this.IADatabaseConnection.ExecuteNonQuery("UPDATE storages SET parent = '" + (this.parentID) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить родительское Хранилище для заданного Хранилища.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private DateTime datetime = DateTime.Now;
        /// <summary>
        /// Дата создания
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [Category("Основное")]
        [DisplayName("Дата создания")]
        public DateTime DateTime
        {
            get
            {
                return datetime;
            }
        }

        private uint creatorID = 0;
        /// <summary>
        /// Идентификатор пользователя, который создал Хранилище
        /// </summary>
        [Browsable(false)]
        public uint CreatorID
        {
            get
            {
                return creatorID;
            }
            private set
            {
                try
                {
                    //Проверка на разумность
                    if (value == 0)
                        throw new Exception("Неверный идентификатор создателя Хранилища: " + value + ".");
                    this.creatorID = value;
                    if (!Settings.AutonomyMode)
                        this.IADatabaseConnection.ExecuteNonQuery("UPDATE storages SET creator = '" + (this.creatorID) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить создателя Хранилища.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private bool isEmpty = true;
        /// <summary>
        /// Является ли данное Хранилище пустым (в нём нет ни одного файла)
        /// </summary>
        [Browsable(true)]
        [DisplayName("Пусто")]
        public bool IsEmpty
        {
            get
            {
                return isEmpty;
            }
            set
            {
                try
                {
                    this.isEmpty = value;
                    if (!Settings.AutonomyMode)
                        this.IADatabaseConnection.ExecuteNonQuery("UPDATE storages SET empty = '" + (this.isEmpty).ToString() + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить свойство Хранилища.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private string comment = null;
        /// <summary>
        /// Комментарий
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [DisplayName("Комментарий")]
        public string Comment
        {
            get
            {
                return comment;
            }
            set
            {
                try
                {
                    //Если комментарий не задан
                    if (String.IsNullOrWhiteSpace(value))
                        value = Storage.StorageDefaultComment;

                    //Проверка на длину комментария
                    if (value.Length > 1000)
                        throw new Exception("Комментарий слишком длинный. Допустимая длина - 1000 символов.");

                    this.comment = value;

                    if (!Settings.AutonomyMode)
                        this.IADatabaseConnection.ExecuteNonQuery("UPDATE storages SET comment = '" + (this.comment) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить комментарий к Хранилищу в базе данных.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private uint[] runned_plugins = null;
        /// <summary>
        /// Выполненные плагины. Работа с окнами результатов данных плагинов не проводилась или не была завершена полностью. (Не может быть NULL)
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [DisplayName("Выполненные плагины")]
        public uint[] Runned_Plugins
        {
            get
            {
                return runned_plugins;
            }
            set
            {
                try
                {

                    //Проверка на разумность
                    if (value == null)
                        throw new Exception("Список плагинов не определён.");

                    //Список выполненных плагинов может только расширяться
                    if (value.Length < this.runned_plugins.Length)
                        throw new Exception("Попытка сократить список плагинов. Такая операция некорректна.");

                    this.runned_plugins = value;

                    if (!Settings.AutonomyMode)
                        this.IADatabaseConnection.ExecuteNonQuery("UPDATE storages SET runned_plugins = '" + String.Join(";", (this.runned_plugins)) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить список выполненных в рамках Хранилища плагинов.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private uint[] completed_plugins = null;
        /// <summary>
        /// Завершённые плагины. Работа с окнами результатов данных плагинов была завершена полностью. (Не может быть NULL)
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [DisplayName("Завершённые плагины")]
        public uint[] Completed_Plugins
        {
            get
            {
                return completed_plugins;
            }
            set
            {
                try
                {

                    //Проверка на разумность
                    if (value == null)
                        throw new Exception("Список плагинов не определён.");

                    //Список завершённых плагинов может только расширяться
                    if (value.Length < this.completed_plugins.Length)
                        throw new Exception("Попытка сократить список плагинов. Такая операция некорректна.");

                    //Завершённый плагин не может быть невыполненным
                    if (!value.All(id => this.runned_plugins.Contains(id)))
                        throw new Exception("Попытка пометить плагин как завершённый, в то время как он не был выполнен.");
                    this.completed_plugins = value;

                    if (!Settings.AutonomyMode)
                        this.IADatabaseConnection.ExecuteNonQuery("UPDATE storages SET completed_plugins = '" + String.Join(";", (this.completed_plugins)) + "' WHERE id = '" + this.id + "';");

                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить список завершённых в рамках Хранилища плагинов.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private enMode mode = enMode.EXPERT;
        /// <summary>
        /// Режим работы
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [Category("Основное")]
        [DisplayName("Режим работы")]
        public enMode Mode
        {
            get
            {
                return this.mode;
            }
            set
            {
                this.mode = value;
                if (!Settings.AutonomyMode)
                    try
                    {
                        //Вносим изменения в БД                    
                        switch (this.mode)
                        {
                            case enMode.BASIC:
                                this.IADatabaseConnection.ExecuteNonQuery("UPDATE storages SET mode = 'user' WHERE id = '" + this.id + "';");
                                break;
                            case enMode.EXPERT:
                                this.IADatabaseConnection.ExecuteNonQuery("UPDATE storages SET mode = 'expert' WHERE id = '" + this.id + "';");
                                break;
                            default:
                                throw new Exception("Обнаружен неизвестный режим работы с Хранилищем.");
                        }

                    }
                    catch (Exception ex)
                    {
#if DEBUG
                        throw new Exception("Ошибка при попытке изменить режим работы с Хранилищем.", ex);
#else
                        //Пробрасываем исключение
                        throw;
#endif
                    }
            }
        }
        #endregion

        #region Свойства
        /// <summary>
        /// Соединение с базой данных "ia"
        /// </summary>
        private IADatabaseConnection IADatabaseConnection { get; set; }

        /// <summary>
        /// Полное имя создателя Хранилища
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [Category("Основное")]
        [DisplayName("Создатель")]
        public string CreatorFullName
        {
            get
            {
                return this.CreatorID != 0 ? User.GetByID(this.CreatorID).Fullname : "Пользователь не определён";
            }
        }

        private string path = null;
        /// <summary>
        /// Путь
        /// </summary>
        [Browsable(true)]
        [ReadOnly(true)]
        [Category("Расположения")]
        [DisplayName("Путь на сервере")]
        public string Path
        {
            get
            {
                return path ?? (path = System.IO.Path.Combine(Materials.GetByID(this.MaterialsID).GetSubDirectoryPath(Materials.enSubDirectories.STORAGES), this.ID.ToString()));
            }
        }

        private Store.WorkDirectory workDirectory = null;
        /// <summary>
        /// Рабочая директория
        /// </summary>
        [Browsable(false)]
        public Store.WorkDirectory WorkDirectory
        {
            get
            {
                return this.workDirectory;
            }
            set
            {
                //Сохраняем переданное значение
                this.workDirectory = value;
            }
        }

        /// <summary>
        /// Контроллер (Не может быть NULL)
        /// </summary>
        [Browsable(false)]
        public StorageController Controller { get; private set; }
        #endregion

        private Storage()
        {
            Controller = new StorageController(this);
            IADatabaseConnection = ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="cortege">Запись в базе данных</param>
        private Storage(string[] cortege) : this()
        {
            try
            {
                //Проверка на разумность
                if (cortege.Length != 10)
                    throw new Exception("Неверный размер кортежа.");

                //Получаем идентификатор проекта
                this.id = Convert.ToUInt32(cortege[0]);

                //Проверяем разумность идентификатора на стадии формировании объекта, дальнейшие проверки внутри класса не требуются
                if (this.id == 0)
                    throw new Exception("Неверный идентификтор Хранилища: " + this.ID + ".");

                //Получаем идентификатор набора материалов, к которыму принадлежит Хранилище
                this.materialsID = Convert.ToUInt32(cortege[1]);

                //Получаем идентификатор родительского Хранилища
                this.parentID = String.IsNullOrEmpty(cortege[2]) ? 0 : Convert.ToUInt32(cortege[2]);

                //Получаем дату и время создания Хранилища
                this.datetime = DateTime.Parse(cortege[3]);

                //Получаем идентификатор текущего пользователя Хранилища
                this.creatorID = Convert.ToUInt32(cortege[4]);

                //Получаем информацию о том пусто ли Хранилище на сервере или нет
                this.isEmpty = Convert.ToBoolean(cortege[5]);

                //Получаем комментарий к Хранилищу
                this.comment = cortege[6];

                //Получаем список выполненных плагинов в рамках Хранилища
                this.runned_plugins = String.IsNullOrEmpty(cortege[7]) ? new uint[0] : cortege[7].Split(';').Select(i => uint.Parse(i)).ToArray();

                //Получаем список завершённых плагинов в рамках Хранилища
                this.completed_plugins = String.IsNullOrEmpty(cortege[8]) ? new uint[0] : cortege[8].Split(';').Select(i => uint.Parse(i)).ToArray();

                switch (cortege[9])
                {
                    case "user": this.mode = enMode.BASIC; break;
                    case "expert": this.mode = enMode.EXPERT; break;
                    default:
                        throw new Exception("Обнаружен неизвестный режим работы с Хранилищем");
                }

                //Задаём рабочую директорию
                this.WorkDirectory = null;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении информации о Хранилище из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Получить Хранилище по идентификтору
        /// </summary>
        /// <param name="ID">Идентификатор Хранилища</param>
        /// <returns>Объект Хранилища</returns>
        public static Storage GetByID(uint ID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (ID == 0)
                    throw new Exception("Неверный идентификатор Хранилища: " + ID + ".");

                Table table = ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteReader("SELECT * FROM storages WHERE id = '" + ID + "';");

                //Проверка результата
                if (table.Count != 1)
                    throw new Exception((table.Count > 1 ? "Найдено несколько Хранилищ" : "Не найдено ни одного Хранилища") + " с заданным идентификатором: " + ID + ".");

                return new Storage(table[0]);
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении Хранилища по идентификатору из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Получить Хранилища по заданному идентификатору родительского Хранилища
        /// </summary>
        /// <param name="parentID">Идентификатор родительского Хранилища</param>
        /// <returns>Список объектов Хранилищ</returns>
        public static List<Storage> GetByParent(uint parentID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                List<Storage> ret = new List<Storage>();

                foreach (string[] cortege in ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteReader("SELECT * FROM storages WHERE parent = '" + parentID + "';"))
                    ret.Add(new Storage(cortege));

                return ret;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении Хранилищ по идентификатору родительского Хранилища из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        public static Storage GetDummy(uint id = 1, uint creatorid = 1, uint materialsid = 1, uint parentid = 1)
        {
            var result = new Storage()
            {
                id = id,
                comment = "",
                completed_plugins = new uint[0],
                creatorID = creatorid,
                isEmpty = false,
                materialsID = materialsid,
                mode = enMode.EXPERT,
                runned_plugins = new uint[0],
                parentID = parentid,
                WorkDirectory = null
            };
            return result;
        }

        /// <summary>
        /// Получить Хранилища по заданному набору материалов
        /// </summary>
        /// <param name="materialsID">Идентификатор набора материалов</param>
        /// <returns>Список объектов Хранилищ</returns>
        public static List<Storage> GetByMaterials(uint materialsID)
        {
            try
            {
                //Проверка на разумность
                if (materialsID == 0)
                    throw new Exception("Неверный идентификатор набора материалов: " + materialsID + ".");

                List<Storage> ret = new List<Storage>();

                foreach (string[] cortege in ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteReader("SELECT * FROM storages WHERE materials = '" + materialsID + "';"))
                    ret.Add(new Storage(cortege));

                return ret;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении Хранилищ по идентификатору набора материалов из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Добавить Хранилище
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="materialsID">Идентификатор набора материалов</param>
        /// <param name="parentID">Идентификатор родителя (Хранилища)</param>
        /// <param name="creatorID">Идентификатор создателя</param>
        /// <param name="mode">Режим открытия хранилища</param>
        /// <param name="empty">Пусто ли Хранилище?</param>
        /// <param name="comment">Комментарий</param>
        /// <param name="runned_plugins">Список идентификаторов выполненных плагинов</param>
        /// <param name="completed_plugins">Список идентификаторов завершенных плагинов</param>
        /// <returns>Объект добавленнного Хранилища</returns>
        public static Storage Add(uint materialsID, uint parentID, uint creatorID, enMode mode, bool empty = true, string comment = Storage.StorageDefaultComment, uint[] runned_plugins = null, uint[] completed_plugins = null)
        {
            //Идентификатор нового Хранилища
            uint id = 0;

            try
            {
                //Устанавливаем соединение с сервером проектов
                using (new NetworkConnection(NetworkExtensions.ProjectsServer, true))
                {
                    //Проверка на разумность
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                    //Добавляем новое Хранилище в БД
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteNonQuery("INSERT INTO storages (materials, parent, creator, mode, empty, comment) VALUES(0, 0, 0, 'expert', 'TRUE', '');");

                    //Получаем идентификатор вновь добавленного Хранилища
                    id = Convert.ToUInt32(((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteScalar("SELECT MAX(id) FROM storages;"));

                    //Получаем вновь созданное Хранилище
                    Storage storage = Storage.GetByID(id);

                    //Задаём идентификатор набора материалов
                    storage.MaterialsID = materialsID;

                    //Задаём идентификатор родительского Хранилища
                    storage.ParentID = parentID;

                    //Задаём идентификатор создателя Хранилища
                    storage.CreatorID = creatorID;

                    //Задаём режим работы с Хранилищем
                    storage.Mode = mode;

                    //Задаём пусто ли Хранилище на сервере
                    storage.IsEmpty = empty;

                    //Задаём комментарий к Хранилищу
                    storage.Comment = comment;

                    //Задаём выполненные плагины в рамках Хранилища
                    storage.Runned_Plugins = runned_plugins ?? new uint[0];

                    //Задаём завершённые плагины в рамках Хранилища
                    storage.Completed_Plugins = completed_plugins ?? new uint[0];

                    //Создаём директорию для материалов вновь созданного Хранилища
                    if (DirectoryController.IsExists(storage.Path))
                        throw new Exception("Директория для материалов нового Хранилища изначально не должна существовать.");
                    else
                        DirectoryController.Create(storage.Path);

                    return storage;
                }
            }
            catch (Exception ex)
            {
                //Если успели создать запись в БД, удаляем её
                if (id != 0)
                    Storage.RemoveByID(id);

#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при добавлении Хранилища в базу данных или на сервер проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Удалить Хранилище с заданным идентификатором
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="ID">Идентификатор Хранилища</param>
        public static void RemoveByID(uint ID)
        {
            try
            {
                //Устанавливаем соединение с сервером проектов
                using (new NetworkConnection(NetworkExtensions.ProjectsServer, true))
                {
                    //Проверка на разумность
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                    //Проверка на разумность
                    if (ID == 0)
                        throw new Exception("Неверный идентификатор Хранилища: " + ID + ".");

                    //Получаем Хранилище, которое следует удалить
                    Storage storage = Storage.GetByID(ID);

                    //Удаляем директорию
                    if (DirectoryController.IsExists(storage.Path))
                        DirectoryController.Remove(storage.Path, true);

                    //Удаляем Хранилище
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteNonQuery("DELETE FROM storages WHERE id = '" + ID + "';");

                    //Проставляем дочерним Хранилищам неопределённого отца
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteNonQuery("UPDATE storages SET parent = 0 WHERE parent = '" + ID + "';");
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при удалении Хранилища по идентификатору из базы данных или с сервера проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Удалить все Хранилища с заданным родителем (Хранилищем)
        /// </summary>
        /// <param name="parentID">Идентификатор родителя (Хранилища)</param>
        public static void RemoveByParent(uint parentID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                foreach (string[] cortege in ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteReader("SELECT id FROM storages WHERE parent = '" + parentID + "';"))
                    Storage.RemoveByID(Convert.ToUInt32(cortege[0]));
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при удалении Хранилища по идентификатору родительского Хранилища из базы данных или с сервера проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Удалить все Хранилища по заданным материалам
        /// </summary>
        /// <param name="materialsID">Идентификатор материалов</param>
        public static void RemoveByMaterials(uint materialsID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (materialsID == 0)
                    throw new Exception("Неверный идентификатор набора материалов: " + materialsID + ".");

                foreach (string[] cortege in ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteReader("SELECT id FROM storages WHERE materials = '" + materialsID + "';"))
                    Storage.RemoveByID(Convert.ToUInt32(cortege[0]));
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при удалении Хранилища по идентификатору набора материалов из базы данных или с сервера проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }
    }

    /// <summary>
    /// Класс для работы с метаинформацией хранилища, выгружаемого для автономной работы
    /// </summary>
    public class StorageUnloadInfo
    {
        [System.Xml.Serialization.XmlAttribute]
        public uint Id { get; set; }
        [System.Xml.Serialization.XmlAttribute]
        public uint ParentId { get; set; }
        [System.Xml.Serialization.XmlAttribute]
        public uint MaterialsId { get; set; }
        [System.Xml.Serialization.XmlAttribute]
        public string Comment { get; set; }
        [System.Xml.Serialization.XmlAttribute]
        public uint CreatorId { get; set; }

        public uint[] RunnedPlugins { get; set; }

        public uint[] CompletedPlugins { get; set; }
        /// <summary>
        /// Информация о статусе выполнения плагинов
        /// </summary>
        public HandlersInitial.PluginRunInfo[] PluginRunInfos { get; set; }

        /// <summary>
        /// Собрать метаинформацию о хранилище и выполненных плагинах для последующего восстановления.
        /// </summary>
        /// <param name="storage"></param>
        /// <returns></returns>
        public static StorageUnloadInfo Create(Storage storage)
        {
            return new StorageUnloadInfo()
            {
                Id = storage.ID,
                ParentId = storage.ParentID,
                MaterialsId = storage.MaterialsID,
                CreatorId = storage.CreatorID,
                Comment = storage.Comment,
                RunnedPlugins = storage.Runned_Plugins,
                CompletedPlugins = storage.Completed_Plugins,
                PluginRunInfos = HandlersInitial.PluginRunInfo.DumpPluginRunInfo().ToArray()
            };

        }

        /// <summary>
        /// Восстановить метаинформацию хранилища.
        /// </summary>
        /// <returns></returns>
        public Storage RestoreStorage()
        {
            var result = Storage.GetDummy(Id, CreatorId, MaterialsId, ParentId);
            result.Comment = Comment;
            result.Runned_Plugins = RunnedPlugins;
            result.Completed_Plugins = CompletedPlugins;
            result.IsEmpty = false;
            HandlersInitial.GetAll().ForEach((h) =>
                {
                    var pri = PluginRunInfos.FirstOrDefault(x => x.ID == h.ID);
                    h.RunResult = pri?.RunResult ?? Handler.ExecutionResult.NO;
                    h.IsCompleted = pri?.IsCompleted ?? false;
                }
            );

            return result;
        }
    }
}
