﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using IA.Network;
using IA.Sql.DatabaseConnections;

using IOController;

namespace IA.Objects
{
    using Sql;
    using Table = List<string[]>;

    /// <summary>
    /// Класс реализующий отдельный проект
    /// </summary>
    public class Project
    {
        /// <summary>
        /// Список операций над проектом, связанных с редактированием
        /// </summary>
        public enum EditOperation
        {
            ///// <summary>
            ///// Выбрать проект
            ///// </summary>
            //[Description("Выбрать")]
            //CHOOSE,
            /// <summary>
            /// Добавить проект
            /// </summary>
            [Description("Добавить")]
            ADD,
            /// <summary>
            /// Удалить проект
            /// </summary>
            [Description("Удалить")]
            REMOVE,
        }

        /// <summary>
        /// Список операций над проектом, связанных с сервером
        /// </summary>
        public enum ServerOperation
        {
            /// <summary>
            /// Начать работу над проектом
            /// </summary>
            [Description("Открыть")]
            OPEN,
            /// <summary>
            /// Закончить работы с проектом
            /// </summary>
            [Description("Закрыть")]
            CLOSE
        }

        /// <summary>
        /// Список операций над проектом, связанных с пользователем
        /// </summary>
        public enum UserOperation
        {
            /// <summary>
            /// Получить отчёты по проекту
            /// </summary>
            [Description("Получить отчёты")]
            GET_REPORTS
        }

        /// <summary>
        /// Список самых распространённых исключений при работе с проектами
        /// </summary>
        public enum Exceptions
        {
            /// <summary>
            /// Обнаружена неизвестная операция над проектом, связанная с редактированием
            /// </summary>
            [Description("Обнаружена неизвестная операция над проектом, связанная с редактированием.")]
            UNKNOWN_EDIT_OPERATION,
            /// <summary>
            /// Обнаружена неизвестная операция над проектом, связанная с сервером
            /// </summary>
            [Description("Обнаружена неизвестная операция над проектом, связанная с сервером.")]
            UNKNOWN_SERVER_OPERATION,
            /// <summary>
            /// Обнаружена неизвестная операция над проектом, связанная с пользователем
            /// </summary>
            [Description("Обнаружена неизвестная операция над проектом, связанная с пользователем.")]
            UNKNOWN_USER_OPERATION
        }

        #region Свойства, получаемые из БД
        private uint id = 0;
        /// <summary>
        /// Идентификатор
        /// </summary>
        public uint ID
        {
            get
            {
                return id;
            }
        }

        private string name = null;
        /// <summary>
        /// Наименование проекта
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
            private set
            {
                try
                {
                    //Проверка на разумность
                    if (String.IsNullOrWhiteSpace(value))
                        throw new Exception("Имя проекта не определено или пусто.");

                    //Проверяем что проекта с таким именем нет в БД
                    if (this.IADatabaseConnection.ExecuteReader("SELECT * FROM projects WHERE name = '" + value + "';").Count != 0)
                        throw new Exception("Проект с таким именем уже существует.");

                    //Проверка на то, что имя не содержит недопустимых символов
                    if (value.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) != -1)
                        throw new Exception("Имя содержит недопустимые символы.");

                    //Проверка на длину имени
                    if (value.Length > 255)
                        throw new Exception("Имя слишком длинное. Допустимя длина - 255 символов.");

                    //Вносим изменения в БД
                    this.IADatabaseConnection.ExecuteNonQuery("UPDATE projects SET name = '" + (this.name = value) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить имя проекта.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private string settings = null;
        /// <summary>
        /// Строка настроек проекта
        /// </summary>
        public string Settings
        {
            get
            {
                return settings;
            }
            set
            {
                try
                {
                    //Проверка на разумность
                    if (value == null)
                        throw new Exception("Строка настроек не определена.");

                    //Вносим изменения в БД
                    this.IADatabaseConnection.ExecuteNonQuery("UPDATE projects SET settings = '" + (this.settings = value) + "' WHERE id = '" + this.id + "';");
                }
                catch (Exception ex)
                {
#if DEBUG
                    throw new Exception("Ошибка при попытке изменить строку настроек проекта в базе данных.", ex);
#else
                    //Пробрасываем исключение
                    throw;
#endif
                }
            }
        }

        private string uncPath = null;
        /// <summary>
        /// Путь к проекту на сервере проектов.
        /// </summary>
        public string UNCPath
        {
            get
            {
                //Если ещё не задавался либо если настройки с момента последнего обращения поменялись - меняем на актуальный
                if (uncPath == null || !uncPath.StartsWith(IA.Settings.ProjectsServer.ProjectsDirectoryPath))
                {
                    uncPath = System.IO.Path.Combine(IA.Settings.ProjectsServer.ProjectsDirectoryPath, this.Name);
                }

                return uncPath;
            }
        }
        #endregion

        #region Свойства
        /// <summary>
        /// Соединение с базой данных "ia"
        /// </summary>
        private IADatabaseConnection IADatabaseConnection { get; set; }
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="cortege">Запись в базе данных</param>
        private Project(string[] cortege)
        {
            try
            {
                //Запоминаем соединение с базой данных
                this.IADatabaseConnection = ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Провера на разумность
                if (cortege.Length != 4)
                    throw new Exception("Неверный размер кортежа.");

                //Получаем идентификатор проекта
                this.id = Convert.ToUInt32(cortege[0]);

                //Проверяем разумность идентификатора на стадии формировании объекта, дальнейшие проверки внутри класса не требуются
                if (this.id == 0)
                    throw new Exception("Неверный идентификтор проекта: " + this.id + ".");

                //Получаем имя проекта
                this.name = cortege[1];
                
                //Получаем строку настроек проекта
                this.settings = cortege[3];
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении информации о проекте из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        #region Статические методы
        /// <summary>
        /// Получить список всех проектов
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <returns>Список объектов проектов</returns>
        public static List<Project> GetAll()
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                Table table = ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteReader("SELECT * FROM projects ORDER BY NAME;");

                List<Project> ret = new List<Project>();

                foreach (string[] cortege in table)
                    ret.Add(new Project(cortege));

                return ret;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении списка всех проектов из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Получить проект по идентификтору
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="ID">Идентификатор проекта</param>
        /// <returns>Объект проекта</returns>
        public static Project GetByID(uint ID)
        {
            try
            {
                //Проверка на разумность
                ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                //Проверка на разумность
                if (ID == 0)
                    throw new Exception("Неверный идентификатор проекта: " + ID + ".");

                Table table = SqlConnector.IADB.DatabaseConnection.ExecuteReader("SELECT * FROM projects WHERE id = '" + ID + "';");
                //Проверка результата
                if (table.Count != 1)
                    throw new Exception((table.Count > 1 ? "Найдено несколько проектов" : "Не найдено ни одного проекта") + " с заданным идентификатором: " + ID + ".");

                return new Project(table[0]);
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при получении проекта по идентификатору из базы данных.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Добавить проект
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="name">Имя проекта</param>
        /// <param name="settings">Настройки проекта</param>
        /// <param name="serverProjectsPath">Путь до директории с проектами на сервере проектов</param>
        /// <returns>Объект добавленного проекта</returns>
        public static Project Add(string name, string settings, string serverProjectsPath)
        {
            //Идентификатор нового проекта
            uint id = 0;

            try
            {
                using (new NetworkConnection(NetworkExtensions.ProjectsServer, true))
                {
                    //Проверка на разумность
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                    //Проверка на то, что директори проектов на сервере существует
                    if (!DirectoryController.IsExists(serverProjectsPath))
                        throw new Exception("Указанная директория " + serverProjectsPath + " для хранения проектов на сервере не существует.");

                    //Добавляем новый проект в БД
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteNonQuery("INSERT INTO projects (name, settings, path) VALUES('', '', '');");

                    //Получаем идентификатор вновь добавленного проекта
                    id = Convert.ToUInt32(((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteScalar("SELECT MAX(id) FROM projects;"));

                    //Получаем вновь созданный проект
                    Project project = Project.GetByID(id);

                    //Задаём имя проекта
                    project.Name = name = String.IsNullOrWhiteSpace(name) ? "Проект №" + id : name;

                    //Задаём строку настроек
                    project.Settings = settings;

                    //Получаем путь до директории материалов проекта
                    string path = System.IO.Path.Combine(serverProjectsPath, name);

                    //Создаём директорию для материалов вновь созданного проекта на сервере
                    if (DirectoryController.IsExists(path))
                        throw new Exception("Директория для материалов нового проекта изначально не должна существовать.");
                    else
                        DirectoryController.Create(path);

                    return project;
                }
            }
            catch (Exception ex)
            {
                //Если успели создать запись в БД, удаляем её
                if (id != 0)
                    Project.RemoveByID(id);

#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при добавлении проекта в базу данных или на сервер проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Удалить проект по идентификатору
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="ID">Идентификатор проекта</param>
        public static void RemoveByID(uint ID)
        {
            try
            {
                //Устанавливаем соединение с сервером проектов
                using (new NetworkConnection(NetworkExtensions.ProjectsServer, true))
                {
                    //Проверка на разумность
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ThrowIfNull();

                    //Проверка на разумность
                    if (ID == 0)
                        throw new Exception("Неверный идентификатор проекта: " + ID + ".");

                    //Получаем проект по индентификатору
                    Project project = Project.GetByID(ID);

                    //Удаляем все материалы
                    Materials.RemoveByProject(ID);

                    //Удаляем директорию проекта с сервера проектов
                    if (DirectoryController.IsExists(project.UNCPath) 
                        && !project.UNCPath.Equals(IA.Settings.ProjectsServer.ProjectsDirectoryPath)) //fix bug #1
                        DirectoryController.Remove(project.UNCPath, true);

                    //Удаляем проект из базы данных
                    ((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).ExecuteNonQuery("DELETE FROM projects WHERE id = '" + ID + "';");
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем ошибку
                throw new Exception("Ошибка при удалении проекта по идентификатору из базы данных или с сервера проектов.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }
        #endregion
    }
}
