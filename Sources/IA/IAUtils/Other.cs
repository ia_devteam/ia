﻿using System.ComponentModel;

namespace IA
{
    namespace Actions
    {
        //internal enum View
        //{
        //    [Description("Список плагинов")]
        //    PLUGINS,
        //    [Description("Панель инструментов")]
        //    TOOLS,
        //    [Description("Лог")]
        //    LOG,
        //    [Description("Прогресс")]
        //    PROGRESS,
        //}

        public enum Global
        {
            [Description("Пользователь")]
            USER,
            [Description("Настройки приложения")]
            SETTINGS,
            [Description("Экспертный режим")]
            EXPERT_MODE,
            [Description("О программе")]
            ABOUT
        }

        public enum Exceptions
        {
            //[Description("Обнаружена неизвестная операция отображения в главной форме.")]
            //UNKNOWN_VIEW_ACTION,
            [Description("Обнаружена неизвестная глобальная операция в приложении.")]
            UNKNOWN_GLOBAL_ACTION
        }
    }

    /// <summary>
    /// Тип отображаемой информации на информационной панели
    /// </summary>
    public enum InfoType
    {
        /// <summary>
        /// Текущий пользователь
        /// </summary>
        [Description("Пользователь")]
        USER,
        /// <summary>
        /// Текущий проект
        /// </summary>
        [Description("Проект")]
        PROJECT,
        /// <summary>
        /// Текущие материалы
        /// </summary>
        [Description("Материалы")]
        MATERIALS,
        /// <summary>
        /// Текущее Хранилище
        /// </summary>
        [Description("Хранилище")]
        STORAGE,
        /// <summary>
        /// Текущая рабочая директория
        /// </summary>
        [Description("Рабочая директория")]
        WORKDIRECTORY
    }

}


