﻿namespace IA.Constants
{
    /// <summary>
    /// Класс, содержащий константные значения связанные с приложением в целом
    /// </summary>
    public static class Files
    {
        /// <summary>
        /// Имя файла, содержащего критические ошибки приложения
        /// </summary>
        public const string ErrorLogFileName = "error.log";
    }
}
