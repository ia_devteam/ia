﻿namespace IA.Constants
{
    /// <summary>
    /// Класс, содержащий константные значения связанные сообщениями
    /// </summary>
    public static class Message
    {
        /// <summary>
        /// Обратитесь к разработчику.
        /// </summary>
        public static string СontactTheManufacturer = "Обратитесь к разработчику.";
        /// <summary>
        /// Приложение будет закрыто.
        /// </summary>
        public static string ApplicationWillBeClosed = "Приложение будет закрыто.";
        /// <summary>
        /// Для возобновления нормальной работы требуется переустановка приложения.
        /// </summary>
        public static string ReinstallIsNeeded = "Для возобновления нормальной работы требуется переустановка приложения.";
    }
}
