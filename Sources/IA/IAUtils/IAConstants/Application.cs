﻿namespace IA.Constants
{
    /// <summary>
    /// Класс, содержащий константные значения связанные с приложением в целом
    /// </summary>
    public static class Application
    {
        /// <summary>
        /// Полное наименование приложения
        /// </summary>
        public const string FullName = "Interlanguage Analyzer";

        /// <summary>
        /// Краткое наименование приложения
        /// </summary>
        public const string ShortName = "IA";
    }
}
