﻿namespace IA.Constants
{
    /// <summary>
    /// Класс, содержащий константные значения связанные с компанией-разработчиком приложения
    /// </summary>
    public static class Developer
    {
        /// <summary>
        /// Наименование компании-разработчика приложения
        /// </summary>
        public const string Name = "ЗАО \"РНТ\"";

        /// <summary>
        /// Почтовый адрес компании-разработчика приложения
        /// </summary>
        public const string PostAddress = "129515, Москва, ул. 2-я Останкинская, д. 6";

        /// <summary>
        /// Адрес электронной почты компании-разработчика приложения
        /// </summary>
        public const string Email = "sales@IA.ru";

        /// <summary>
        /// Адрес сайта компании-разработчика приложения
        /// </summary>
        public const string WebSite = "www.IA.ru";
    }
}
