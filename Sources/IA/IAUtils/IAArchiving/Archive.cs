﻿using System;
using System.IO;
using IA.Network;

using FileOperations;

using IOController;

namespace IA.Archiving
{
    /// <summary>
    /// Класс содержит в себе полезные методы для архивации в рамках приложения IA
    /// </summary>
    public static class Archive
    {
        /// <summary>
        /// Архивация данных из указанной директории в указанную директорию на сервере проектов
        /// </summary>
        /// <param name="sourceDirectoryPath">Путь до директории для архивации. Директория должна существовать.</param>
        /// <param name="tempDirectoryPath">Путь до временной директории. Не обязана существовать. Удаляется после использования.</param>
        /// <param name="projectsServerDirectoryPath">Путь до директории на сервере проектов. Должна существовать.</param>
        public static void ToProjectsServer(string sourceDirectoryPath, string tempDirectoryPath, string projectsServerDirectoryPath)
        {
            try
            {
                //Устанавливаем соединение с сервером проектов
                using (
                    new NetworkConnection(
                        new NetworkResource(
                            IA.Settings.ProjectsServer.Name,
                            IA.Settings.ProjectsServer.Login,
                            IA.Settings.ProjectsServer.Password
                        ),
                        true
                    )
                )
                {
                    //Проверка на разумность
                    if (sourceDirectoryPath == null)
                        throw new Exception("Путь до директории для архивирования не определён.");
                    if (tempDirectoryPath == null)
                        throw new Exception("Путь до временной директории не определён.");
                    if (projectsServerDirectoryPath == null)
                        throw new Exception("Путь до директории на сервере проектов не определён.");

                    //Проверка на существование директорий
                    if (!DirectoryController.IsExists(sourceDirectoryPath))
                        throw new Exception("Путь до диреткории для архивирования не существует.");
                    if (!DirectoryController.IsExists(projectsServerDirectoryPath))
                        throw new Exception("Путь до директории на сервере проектов не существует.");

                    //Если временная директория не сущевствует, создаём её
                    if (!DirectoryController.IsExists(tempDirectoryPath))
                        DirectoryController.Create(tempDirectoryPath);

                    //Формируем имя архивируемой директории

                    string sourceDirectoryName = Path.GetFileName(sourceDirectoryPath.Trim(new char[]{'\\'}));

                    //Формируем путь до будущего файла с архивом
                    string archiveFilePath = Path.Combine(tempDirectoryPath, sourceDirectoryName + ArchiveWriter.ArchiveDefaultExtension);

                    //Архивируем
                    ArchiveWriter.ArchiveDirectoryContents(sourceDirectoryPath, archiveFilePath);

                    //Копируем временную директорию с архивом на сервер 
                   DirectoryController.Copy(tempDirectoryPath, projectsServerDirectoryPath);
                }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при архивации данных c помещением на сервер проектов.", ex);
            }
            finally
            {
                //Если временная директория была создана
                if(DirectoryController.IsExists(tempDirectoryPath))
                    //Удаляем временную директорию
                    DirectoryController.Remove(tempDirectoryPath, true);
            }
        }
    }
}
