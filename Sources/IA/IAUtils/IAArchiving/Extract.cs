﻿using System;
using System.Collections.Generic;
using System.Linq;

using IA.Network;

using FileOperations;

using IOController;

namespace IA.Archiving
{
    /// <summary>
    /// Класс содержит в себе полезные методы для разархивации в рамках приложения IA
    /// </summary>
    public static class Extract
    {
        /// <summary>
        /// Получение данных из указанной директории на сервере проектов в указанную директорию. 
        /// 
        /// Без разархивирования! 
        /// </summary>
        /// <param name="projectsServerDirectoryPath">Путь до директории на сервере проектов. Должна существовать.</param>
        /// <param name="destinationDirectoryPath">Путь до директории для разархивации. Директория должна существовать.</param>
        public static void FromProjectsServer(string projectsServerDirectoryPath, string destinationDirectoryPath)
        {
            try
            {
                //Устанавливаем соединение с сервером проектов
                using (
                    new NetworkConnection(
                        new NetworkResource(
                            IA.Settings.ProjectsServer.Name,
                            IA.Settings.ProjectsServer.Login,
                            IA.Settings.ProjectsServer.Password
                        ),
                        true
                    )
                )
                {
                    //Проверка на разумность
                    if(projectsServerDirectoryPath == null)
                        throw new Exception("Путь до директории на сервере проектов не определён.");
                    if(destinationDirectoryPath == null)
                        throw new Exception("Путь до директории для разархивирования не определён.");

                    //Проверка на существование директорий
                    if (!DirectoryController.IsExists(projectsServerDirectoryPath))
                        throw new Exception("Путь до директории на сервере проектов не существует.");
                    if (!DirectoryController.IsExists(destinationDirectoryPath))
                        throw new Exception("Путь до диреткории для разархивирования не существует.");

                    //Получаем все файлы, содержащиеся в директории на сервере проектов
                    List<string> projectsServerDirectoryFiles = DirectoryController.GetFiles(projectsServerDirectoryPath);

                    //Получаем путь до архива в директории на сервере проектов
                    string archiveFilePath = projectsServerDirectoryFiles.FirstOrDefault(filePath => filePath.EndsWith(ArchiveWriter.ArchiveDefaultExtension));

                    //Если на сервере Храниться незаархивированная информация
                    if (archiveFilePath == null)
                    {
                        ////Формируем исключение
                        //throw new Exception("Обнаружена информация на сервере, хранимая в незаархивированном виде.");

                        #region FIXME - убрать, когда вся информация на сервер будет храниться в сжатом виде
                        //Копируем содержимое директории на сервере в поддиректорию рабочей директории Хранилища
                        DirectoryController.Copy(projectsServerDirectoryPath, destinationDirectoryPath);
                        return; 
                        #endregion
                    }

                    //Получаем путь до локальной копии архива
                    string archiveCopyFilePath = PathController.ChangePrefix(archiveFilePath, projectsServerDirectoryPath, destinationDirectoryPath);

                    //Создаём локальную копию архива
                    FileController.Copy(archiveFilePath, archiveCopyFilePath);

                }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при выгрузке данных с сервера проектов с последующей разархивацией.", ex);
            }
        }
    }
}
