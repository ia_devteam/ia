﻿using System.ComponentModel;

namespace IA
{
    /// <summary>
    /// Всевозможные исклюения, которые могут быть брошены в рамках работы основной формы приложения
    /// </summary>
    public enum Exceptions
    {
        [Description("Обнаружен неизвестный режим работы основной формы приложения.")]
        UNKNOWN_MODE,
        [Description("Обнаружено неизвестное состояние основной формы приложения.")]
        UNKNOWN_STATE,
        [Description("Обнаружен неизвестный тип отображаемой информации на информационной панели.")]
        UNKNOWN_INFO_TYPE,

        [Description("Данная операция недоступна для анонимного пользователя.")]
        NOT_ALLOWED_FOR_ANONYMOUS_USER,

        /*Проект*/
        [Description("Невозможно открыть проект. Работа над предыдущим проектом была завершена некорректно.")]
        ILLEGAL_PROJECT_OPEN,
        [Description("Невозможно закрыть проект. Проект ещё не был открыт или был закрыт ранее.")]
        ILLEGAL_PROJECT_CLOSE,

        /*Хранилище*/
        [Description("Невозможно выгрузить Хранилище. Работа над предыдущим Хранилищем была завершена некорректно.")]
        ILLEGAL_STORAGE_DOWNLOAD,
        [Description("Невозможно загрузить Хранилище. Хранилище ещё не было открыто или было закрыто ранее.")]
        ILLEGAL_STORAGE_UPLOAD,

        /*Рабочий процесс*/
        [Description("Невозможно запустить рабочий процесс. Проект не был открыт.")]
        ILLEGAL_WORKFLOW_RUN,
    }
}
