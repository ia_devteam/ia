﻿using System;
using System.Windows.Forms;

namespace IA
{
    using Semaphore = System.Threading.ManualResetEvent;

    /// <summary>
    /// Класс, реализующий обновление данных по таймеру. Ожидается использование буфера для накапливания информации.
    /// </summary>
    public class UpdateByTimer
    {
        /// <summary>
        /// Хранится ли в буфере информация, требующая обновления
        /// </summary>
        public enum HasSomethingToUpdate
        {
            /// <summary>
            /// Не хранится
            /// </summary>
            NO,
            /// <summary>
            /// Хранится
            /// </summary>
            YES
        }

        /// <summary>
        /// Таймер для обновления.
        /// </summary>
        private Timer timer = null;

        /// <summary>
        /// Семафор для синхронизации основного потока и потока обновления
        /// </summary>
        private Semaphore semaphore = new Semaphore(true);
            
        /// <summary>
        /// Реализация обновления данных из буфера
        /// </summary>
        private Func<HasSomethingToUpdate> implementation_UpdateFromBuffer;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public UpdateByTimer(Func<HasSomethingToUpdate> implementation_UpdateFromBuffer, int interval = 1000)
        {
            //Запоминаем реализацию обновления данных из буфера
            this.implementation_UpdateFromBuffer = implementation_UpdateFromBuffer;

            //Инициализируем таймер
            timer = new Timer() { Interval = interval };
            timer.Tick += (sender, e) => this.GetFromBuffer();
        }

        /// <summary>
        /// Помещение данных в буфер для последующего обновления.
        /// </summary>
        /// <param name="implementation_AddToBuffer">Реализация помещения данных в буфер.</param>
        public void AddToBuffer(System.Action implementation_AddToBuffer)
        {
            //Ожидаем завершения обновления
            semaphore.WaitOne();

            //Блокируем поток обновления
            semaphore.Reset();

            //Помещаем данные в буфер
            if(implementation_AddToBuffer != null)
                implementation_AddToBuffer();

            //Если таймер не активен, активируем его
            if (!timer.Enabled)
                timer.Start();

            //Разблокируем поток обновления
            semaphore.Set();
        }

        /// <summary>
        /// Обновление данных из буфера. Вызов данного метода происходит каждый раз при срабатывании таймера.
        /// </summary>
        public void GetFromBuffer()
        {
            //Ожидаем завершения заполнения буфера
            semaphore.WaitOne();

            //Блокируем основной поток
            semaphore.Reset();

            //Обновляем данные из буфера (если метод вернул false - останавливаем таймер, иначе таймер продолжает рабтать)
            if (implementation_UpdateFromBuffer != null)
                switch(implementation_UpdateFromBuffer())
                {
                    case HasSomethingToUpdate.NO:
                        {
                            //Если данных для обновления нет - останавливаем таймер
                            if (timer.Enabled)
                                timer.Stop();

                            break;
                        }
                    case HasSomethingToUpdate.YES:
                        {
                            //Если данные для обновления есть - возобновляем работу таймера
                            if (!timer.Enabled)
                                timer.Start();

                            break;
                        }
                    default:
                        throw new Exception("Неизвестный показатель наличия данных для обновления в буфере");
                }

            //Разблокируем основной поток
            semaphore.Set();
        }
    }
}
