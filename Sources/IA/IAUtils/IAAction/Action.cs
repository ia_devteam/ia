﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace IA
{
    /// <summary>
    /// Действие
    /// </summary>
    public class Action
    {
        /// <summary>
        /// Ключ
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Текст
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Изображение 
        /// </summary>
        public Image Image { get; set; }

        /// <summary>
        /// Сочетание клавиш
        /// </summary>
        public Keys ShortcutKeys { get; set; }

        /// <summary>
        /// Обработчик действия
        /// </summary>
        public EventHandler<Action> ActionHandler { get; set; }

        private bool enabled;
        /// <summary>
        /// Доступность действия
        /// </summary>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;

                if (EnabledChangedEvent != null)
                    EnabledChangedEvent(value);
            }
        }

        private bool visible;
        /// <summary>
        /// Видимость действия
        /// </summary>
        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;

                if (VisibleChangedEvent != null)
                    VisibleChangedEvent(value);
            }
        }

        /// <summary>
        /// Возможно ли отметить действие
        /// </summary>
        public bool Checkable { get; set; }

        private bool @checked;
        /// <summary>
        /// Отмечено ли действие
        /// </summary>
        public bool Checked
        {
            get
            {
                return @checked;
            }
            set
            {
                @checked = value;

                if (CheckedChangedEvent != null)
                    CheckedChangedEvent(value);
            }
        }

        #region Делегаты и события

        /// <summary>
        /// Делегат для события - Доступность действия изменена
        /// </summary>
        /// <param name="value"></param>
        public delegate void EnabledChangedEventHandler(bool value);

        /// <summary>
        /// Событие - Доступность действия изменена
        /// </summary>
        public event EnabledChangedEventHandler EnabledChangedEvent;

        /// <summary>
        /// Делегат для события - Видимость действия изменена
        /// </summary>
        /// <param name="value"></param>
        public delegate void VisibleChangedEventHandler(bool value);

        /// <summary>
        /// Событие - Видимость действия изменена
        /// </summary>
        public event VisibleChangedEventHandler VisibleChangedEvent;

        /// <summary>
        /// Делегат для события - Действие отмечено
        /// </summary>
        /// <param name="value"></param>
        public delegate void CheckedChangedEventHandler(bool value);

        /// <summary>
        /// Событие - Действие отмечено
        /// </summary>
        public event CheckedChangedEventHandler CheckedChangedEvent; 

        #endregion
     
        /// <summary>
        /// Конструктор
        /// </summary>
        public Action()
        {
            this.Key = String.Empty;
            this.Text = String.Empty;
            this.Image = null;
            this.ShortcutKeys = Keys.None;
            this.Enabled = true;
            this.Checkable = false;
            this.Checked = false;
            this.Visible = true;

            this.ActionHandler = null;
        }

        /// <summary>
        /// Получить кнопку соответствующую действию
        /// </summary>
        /// <returns>Кнопка</returns>
        public Button ToButton()
        {
            //Формируем результат
            Button ret = new Button()
            {
                Enabled = this.Enabled,
                Image = this.Image,
                Name = this.Key,
                Text = this.Text,
                Visible = this.Visible
            };

            //Связываем обработчики
            ret.Click += (sender, e) => this.ActionHandler(sender, this);

            //Связываем доступность действия с доступностью кнопки
            this.EnabledChangedEvent += delegate(bool value) { ret.Enabled = value; };

            //Связываем видимость действия с видимостью кнопки
            this.VisibleChangedEvent += delegate(bool value) { ret.Visible = value; };

            return ret;
        }

        /// <summary>
        /// Получить кнопку панели инструментов соответствующую действию
        /// </summary>
        /// <returns>Кнопка панели инструметов</returns>
        public ToolStripButton ToToolStripButton()
        {
            //Формируем результат
            ToolStripButton ret = new ToolStripButton()
            {
                Checked = this.Checked,
                CheckOnClick = this.Checkable,
                Enabled = this.Enabled,
                Image = this.Image,
                Name = this.Key,
                Text = this.Text,
                Visible = this.Visible
            };

            //Связываем обработчики
            ret.Click += (sender, e) => this.ActionHandler(sender, this);

            //Связываем доступность действия с доступностью кнопки панели инструментов
            this.EnabledChangedEvent += delegate(bool value) { ret.Enabled = value; };

            //Связываем видимость действия с видимостью кнопки панели инструментов
            this.VisibleChangedEvent += delegate(bool value) { ret.Visible = value; };

            //Связываем отмеченность действия с отмеченностью кнопки панели инструметов
            this.CheckedChangedEvent += delegate(bool value) { ret.Checked = value; };

            return ret;
        }

        /// <summary>
        /// Получить кнопку панели инструментов с выпадающий списком соответствующую действию
        /// </summary>
        /// <returns>Кнопка панели инструметов</returns>
        public ToolStripDropDownButton ToToolStripDropDownButton()
        {
            //Формируем результат
            ToolStripDropDownButton ret = new ToolStripDropDownButton()
            {
                Enabled = this.Enabled,
                Image = this.Image,
                Name = this.Key,
                Text = this.Text,
                Visible = this.Visible
            };

            //Связываем обработчики
            ret.Click += (sender, e) => this.ActionHandler(sender, this);

            //Связываем доступность действия с доступностью кнопки панели инструментов с выпадающий списком 
            this.EnabledChangedEvent += delegate(bool value) { ret.Enabled = value; };

            //Связываем видимость действия с видимостью кнопки панели инструментов с выпадающий списком 
            this.VisibleChangedEvent += delegate(bool value) { ret.Visible = value; };

            return ret;
        }

        /// <summary>
        /// Получить элемент меню соответствующий действию
        /// </summary>
        /// <returns>Элемент меню</returns>
        public ToolStripMenuItem ToToolStripMenuItem()
        {
            //Формируем результат
            ToolStripMenuItem ret = new ToolStripMenuItem()
            {
                Checked = this.Checked,
                CheckOnClick = this.Checkable,
                Enabled = this.Enabled,
                Image = this.Image,
                Name = this.Key,
                ShortcutKeys = this.ShortcutKeys,
                Text = this.Text,
                Visible = this.Visible
            };

            //Связываем обработчики
            ret.Click += (sender, e) => this.ActionHandler(sender, this);

            //Связываем доступность действия с доступностью элемента меню
            this.EnabledChangedEvent += delegate(bool value) { ret.Enabled = value; };

            //Связываем видимость действия с видимостью элемента меню
            this.VisibleChangedEvent += delegate(bool value) { ret.Visible = value; };

            //Связываем отмеченность действия с отмеченностью элемента меню
            this.CheckedChangedEvent += delegate(bool value) { ret.Checked = value; };

            return ret;
        }
    }
}
