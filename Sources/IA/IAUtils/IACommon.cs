﻿using IA.Objects;
using IA.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAUtils
{
    /// <summary>
    /// хранилище для пакетного запуска
    /// </summary>
    public class StorageForPack
    {
        /// <summary>
        /// хранилище
        /// </summary>
        public Storage storage;

        /// <summary>
        /// путь к трассам
        /// </summary>
        public string TracesPath;
    }

    public static class IACommon
    {
        /// <summary>
        /// Делегат для события - Дважды кликнули по Хранилищу
        /// </summary>
        /// <param name="storage"></param>
        public delegate bool StorageDoubleClickEventHandler(Storage storage, bool isInBackground = false);

        /// <summary>
        /// Событие - Дважды кликнули по Хранилищу
        /// </summary>
        public static StorageDoubleClickEventHandler StorageDoubleClick;

        public delegate bool StorageUnloadClickEventHandler(Storage storage, string destination);

        public static StorageUnloadClickEventHandler StorageUnloadClick;

        /// <summary>
        /// делегат для события - запустили пакетную обработку хранилищ
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="isInBackground"></param>
        /// <returns></returns>
        public delegate bool StoragePackStartEventHandler(Queue<StorageForPack> storage, Handlers plugins, string ReportsPath);

        /// <summary>
        /// Событие - запустили пакетную обработку хранилищ
        /// </summary>
        public static StoragePackStartEventHandler StoragePackStart;

        public static User CurrentUser;
    }
}
