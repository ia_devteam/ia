﻿namespace IA.Controls
{
    partial class ProjectSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_groupBox = new System.Windows.Forms.GroupBox();
            this.main_splitContainer = new System.Windows.Forms.SplitContainer();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.presets_comboBox = new System.Windows.Forms.ComboBox();
            this.disable_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.disable_image_label = new System.Windows.Forms.Label();
            this.disable_text_label = new System.Windows.Forms.Label();
            this.main_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).BeginInit();
            this.main_splitContainer.Panel1.SuspendLayout();
            this.main_splitContainer.Panel2.SuspendLayout();
            this.main_splitContainer.SuspendLayout();
            this.main_tableLayoutPanel.SuspendLayout();
            this.disable_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_groupBox
            // 
            this.main_groupBox.Controls.Add(this.main_splitContainer);
            this.main_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_groupBox.Location = new System.Drawing.Point(0, 0);
            this.main_groupBox.Name = "main_groupBox";
            this.main_groupBox.Size = new System.Drawing.Size(405, 525);
            this.main_groupBox.TabIndex = 0;
            this.main_groupBox.TabStop = false;
            // 
            // main_splitContainer
            // 
            this.main_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_splitContainer.Location = new System.Drawing.Point(3, 16);
            this.main_splitContainer.Name = "main_splitContainer";
            this.main_splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // main_splitContainer.Panel1
            // 
            this.main_splitContainer.Panel1.Controls.Add(this.main_tableLayoutPanel);
            // 
            // main_splitContainer.Panel2
            // 
            this.main_splitContainer.Panel2.Controls.Add(this.disable_tableLayoutPanel);
            this.main_splitContainer.Size = new System.Drawing.Size(399, 506);
            this.main_splitContainer.SplitterDistance = 253;
            this.main_splitContainer.TabIndex = 2;
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.presets_comboBox, 0, 0);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 2;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(399, 253);
            this.main_tableLayoutPanel.TabIndex = 1;
            // 
            // presets_comboBox
            // 
            this.presets_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.presets_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.presets_comboBox.FormattingEnabled = true;
            this.presets_comboBox.Location = new System.Drawing.Point(3, 4);
            this.presets_comboBox.Name = "presets_comboBox";
            this.presets_comboBox.Size = new System.Drawing.Size(393, 21);
            this.presets_comboBox.Sorted = true;
            this.presets_comboBox.TabIndex = 1;
            // 
            // disable_tableLayoutPanel
            // 
            this.disable_tableLayoutPanel.ColumnCount = 3;
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.Controls.Add(this.disable_image_label, 1, 1);
            this.disable_tableLayoutPanel.Controls.Add(this.disable_text_label, 1, 2);
            this.disable_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.disable_tableLayoutPanel.Name = "disable_tableLayoutPanel";
            this.disable_tableLayoutPanel.RowCount = 4;
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.Size = new System.Drawing.Size(399, 249);
            this.disable_tableLayoutPanel.TabIndex = 2;
            // 
            // disable_image_label
            // 
            this.disable_image_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_image_label.Image = global::IA.Controls.Properties.Resources.Disable_Icon_Settings;
            this.disable_image_label.Location = new System.Drawing.Point(102, 45);
            this.disable_image_label.Name = "disable_image_label";
            this.disable_image_label.Size = new System.Drawing.Size(194, 128);
            this.disable_image_label.TabIndex = 0;
            this.disable_image_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // disable_text_label
            // 
            this.disable_text_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_text_label.Location = new System.Drawing.Point(102, 173);
            this.disable_text_label.Name = "disable_text_label";
            this.disable_text_label.Size = new System.Drawing.Size(194, 30);
            this.disable_text_label.TabIndex = 1;
            this.disable_text_label.Text = "Список настроек недоступен";
            this.disable_text_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ProjectSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_groupBox);
            this.Name = "ProjectSettingsControl";
            this.Size = new System.Drawing.Size(405, 525);
            this.main_groupBox.ResumeLayout(false);
            this.main_splitContainer.Panel1.ResumeLayout(false);
            this.main_splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).EndInit();
            this.main_splitContainer.ResumeLayout(false);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.disable_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox main_groupBox;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.ComboBox presets_comboBox;
        private System.Windows.Forms.SplitContainer main_splitContainer;
        private System.Windows.Forms.TableLayoutPanel disable_tableLayoutPanel;
        private System.Windows.Forms.Label disable_image_label;
        private System.Windows.Forms.Label disable_text_label;
    }
}
