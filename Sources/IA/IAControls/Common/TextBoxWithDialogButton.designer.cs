﻿namespace IA.Controls
{
    partial class TextBoxWithDialogButton
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt = new System.Windows.Forms.TextBox();
            this.btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt
            // 
            this.txt.Location = new System.Drawing.Point(0, 2);
            this.txt.Name = "txt";
            this.txt.Size = new System.Drawing.Size(69, 20);
            this.txt.TabIndex = 0;
            this.txt.WordWrap = false;
            // 
            // btn
            // 
            this.btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn.Location = new System.Drawing.Point(75, 0);
            this.btn.MaximumSize = new System.Drawing.Size(75, 23);
            this.btn.MinimumSize = new System.Drawing.Size(75, 23);
            this.btn.Name = "btn";
            this.btn.Size = new System.Drawing.Size(75, 23);
            this.btn.TabIndex = 1;
            this.btn.Text = "Обзор";
            this.btn.UseVisualStyleBackColor = true;
            this.btn.Click += new System.EventHandler(this.ButtonClick);
            // 
            // TextBoxWithDialogButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn);
            this.Controls.Add(this.txt);
            this.MaximumSize = new System.Drawing.Size(0, 24);
            this.MinimumSize = new System.Drawing.Size(150, 24);
            this.Name = "TextBoxWithDialogButton";
            this.Size = new System.Drawing.Size(150, 24);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt;
        private System.Windows.Forms.Button btn;
    }
}
