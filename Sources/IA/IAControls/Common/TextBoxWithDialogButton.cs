﻿using System;
using System.Windows.Forms;

using IOController;

namespace IA.Controls
{
    /// <summary>
    /// Класс элемента пользовательского интерфейса, реализующий текстовую строку с приклеенной кнопкой. Основное использивание - выбор файла или каталога.
    /// </summary>
    public partial class TextBoxWithDialogButton : UserControl
    {
        /// <summary>
        /// Конструктор по-умолчанию.
        /// </summary>
        public TextBoxWithDialogButton()
        {
            InitializeComponent();

            Mode = DialogTypes.OpenFileDialog;

            txt.AutoCompleteMode = AutoCompleteMode.Suggest;
        } 

        /// <summary>
        /// Обработчик изменения размеров.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            txt.Width = this.Width - btn.Width - 5;
        }

        /// <summary>
        /// Объект кнопки для тонкой настройки.
        /// Обработчик нажатия уже установлен, менять не рекомендуется.
        /// </summary>
        public Button Button
        {
            get
            {
                return btn;
            }
        }

        /// <summary>
        /// Объект текстового поля для тонкой настройки.
        /// </summary>
        public TextBox TextBox
        {
            get { return txt; }
        }

        /// <summary>
        /// Содержание текстовой строки.
        /// </summary>
        public new string Text
        {
            get { return txt.Text; }
            set { txt.Text = value ?? String.Empty; }
        }

        /// <summary>
        /// Очистить текстовую строку.
        /// </summary>
        public void Clear()
        {
            txt.Clear();
        }

        /// <summary>
        /// Перечень видов диалогов выбора, поддерживаемых элементом.
        /// </summary>
        public enum DialogTypes
        {
            /// <summary>
            /// Диалог открытия файла.
            /// </summary>
            OpenFileDialog,
            /// <summary>
            /// Диалог сохранения файла.
            /// </summary>
            SaveFileFialog,
            /// <summary>
            /// Диалог выбора директории.
            /// </summary>
            FolderBrowserDialog
        }

        /// <summary>
        /// Объект диалога выбора для тонкой настройки.
        /// </summary>
        public CommonDialog Dialog { get; private set; }

        DialogTypes mode;

        /// <summary>
        /// Режим работы элемента (какой диалог отображается при нажатии на кнопку).
        /// </summary>
        public DialogTypes Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
                switch (mode)
                {
                    case DialogTypes.FolderBrowserDialog:
                        Dialog = new FolderBrowserDialog();
                        txt.AutoCompleteSource = AutoCompleteSource.FileSystemDirectories;
                        break;
                    case DialogTypes.OpenFileDialog:
                        Dialog = new OpenFileDialog();
                        txt.AutoCompleteSource = AutoCompleteSource.FileSystem;
                        break;
                    case DialogTypes.SaveFileFialog:
                        Dialog = new SaveFileDialog();
                        txt.AutoCompleteSource = AutoCompleteSource.FileSystem;
                        break;
                }

            }
        }

        void ButtonClick(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(Text))
                if (mode == DialogTypes.FolderBrowserDialog)
                {
                    if (DirectoryController.IsExists(Text))
                        (Dialog as FolderBrowserDialog).SelectedPath = Text;
                }
                else
                    if (FileController.IsExists(Text))
                        (Dialog as FileDialog).FileName = Text;
                    else
                        if (DirectoryController.IsExists(System.IO.Path.GetDirectoryName(Text)))
                            (Dialog as FileDialog).InitialDirectory = Text;


            if (Dialog.ShowDialog() != DialogResult.OK)
                return;

            if (mode == DialogTypes.FolderBrowserDialog)
                this.Text = (Dialog as FolderBrowserDialog).SelectedPath;
            else
                this.Text = (Dialog as FileDialog).FileName;
        }
    }
}
