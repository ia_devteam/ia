﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace IA.Controls.Common
{
    /// <summary>
    /// Класс, реализующий элемент управления - Панель с кнопками
    /// </summary>
    public class ButtonsPanel : Control
    {
        #region Константы
        /// <summary>
        /// Ширина кнопки
        /// </summary>
        public const int ButtonWidth = 200;

        /// <summary>
        /// Высота кнопки
        /// </summary>
        public const int ButtonHeight = 30; 
        #endregion

        /// <summary>
        /// Задать набор кнопок для отображения на панели
        /// </summary>
        /// <param name="buttons">Набор кнопок</param>
        public void SetButtons(Dictionary<string, EventHandler> buttons)
        {
            //Проверка на разумность
            if (buttons == null || buttons.Count == 0)
                throw new Exception("Ошибка при задании набора кнопок для элемента управления \"Панель с кнопками\". Набор кнопок пуст или не определён.");

            //Очищаем список дочерних элементов управления
            this.Controls.Clear();

            //Создаём таблицу с кнопками
            TableLayoutPanel buttons_tableLayoutPanel = new TableLayoutPanel()
            {
                ColumnCount = buttons.Count + 2,
                Dock = DockStyle.Fill,
                RowCount = 1,
            };

            buttons_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            buttons_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

            int column = 1;
            foreach (string key in buttons.Keys)
            {
                Button button = new Button()
                {
                    Text = key,
                    Height = ButtonsPanel.ButtonHeight,
                    Width = ButtonsPanel.ButtonWidth,
                    Anchor = AnchorStyles.Left | AnchorStyles.Right,
                };

                button.Click += buttons[key];
                buttons_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                buttons_tableLayoutPanel.Controls.Add(button, column++, 0);
            }

            buttons_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

            //Добавляем таблицу с кнопками на корневой элемент управления
            this.Controls.Add(buttons_tableLayoutPanel);
        }
    }
}
