﻿using System.Windows.Forms;

namespace IA.Controls
{
    partial class LogControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_dataGridView = new System.Windows.Forms.DataGridView();
            this.main_groupBox = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.main_dataGridView)).BeginInit();
            this.main_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_dataGridView
            // 
            this.main_dataGridView.AllowUserToAddRows = false;
            this.main_dataGridView.AllowUserToDeleteRows = false;
            this.main_dataGridView.AllowUserToResizeColumns = false;
            this.main_dataGridView.AllowUserToResizeRows = false;
            this.main_dataGridView.BackgroundColor = System.Drawing.Color.White;
            this.main_dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.main_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.main_dataGridView.ColumnHeadersVisible = false;
            this.main_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_dataGridView.Location = new System.Drawing.Point(3, 16);
            this.main_dataGridView.MultiSelect = false;
            this.main_dataGridView.Name = "main_dataGridView";
            this.main_dataGridView.RowHeadersVisible = false;
            this.main_dataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.main_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.main_dataGridView.Size = new System.Drawing.Size(361, 156);
            this.main_dataGridView.TabIndex = 0;
            // 
            // main_groupBox
            // 
            this.main_groupBox.Controls.Add(this.main_dataGridView);
            this.main_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_groupBox.Location = new System.Drawing.Point(0, 0);
            this.main_groupBox.Name = "main_groupBox";
            this.main_groupBox.Size = new System.Drawing.Size(367, 175);
            this.main_groupBox.TabIndex = 0;
            this.main_groupBox.TabStop = false;
            this.main_groupBox.Text = "Лог";
            // 
            // LogControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_groupBox);
            this.Name = "LogControl";
            this.Size = new System.Drawing.Size(367, 175);
            ((System.ComponentModel.ISupportInitialize)(this.main_dataGridView)).EndInit();
            this.main_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView main_dataGridView;
        private GroupBox main_groupBox;
    }
}
