﻿using System.Windows.Forms;

namespace IA.Controls
{
    partial class WorkspaceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_splitContainer = new System.Windows.Forms.SplitContainer();
            this.main_groupBox = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).BeginInit();
            this.main_splitContainer.SuspendLayout();
            this.main_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_splitContainer
            // 
            this.main_splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.main_splitContainer.Location = new System.Drawing.Point(0, 0);
            this.main_splitContainer.Name = "main_splitContainer";
            this.main_splitContainer.Dock = DockStyle.Fill;
            this.main_splitContainer.IsSplitterFixed = true;
            this.main_splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.main_splitContainer.Panel2Collapsed = true;
            this.main_splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            // 
            // main_splitContainer.Panel1
            // 
            this.main_splitContainer.Panel1.BackgroundImage = Properties.Resources.Wallpaper;
            this.main_splitContainer.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.main_splitContainer.Size = new System.Drawing.Size(150, 100);
            this.main_splitContainer.TabIndex = 0;
            // 
            // WorkspaceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_splitContainer);
            this.Name = "WorkspaceControl";
            this.Size = new System.Drawing.Size(409, 201);
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).EndInit();
            this.main_splitContainer.ResumeLayout(false);
            this.main_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox main_groupBox;
    }
}
