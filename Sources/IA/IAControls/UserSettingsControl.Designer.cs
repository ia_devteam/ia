﻿namespace IA.Controls
{
    partial class UserSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_groupBox = new System.Windows.Forms.GroupBox();
            this.main_splitContainer = new System.Windows.Forms.SplitContainer();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.patronymic_groupBox = new System.Windows.Forms.GroupBox();
            this.patronymic_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.patronymic_textBox = new System.Windows.Forms.TextBox();
            this.name_groupBox = new System.Windows.Forms.GroupBox();
            this.name_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.name_textBox = new System.Windows.Forms.TextBox();
            this.surname_groupBox = new System.Windows.Forms.GroupBox();
            this.surname_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.surname_textBox = new System.Windows.Forms.TextBox();
            this.disable_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.disable_image_label = new System.Windows.Forms.Label();
            this.disable_text_label = new System.Windows.Forms.Label();
            this.main_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).BeginInit();
            this.main_splitContainer.Panel1.SuspendLayout();
            this.main_splitContainer.Panel2.SuspendLayout();
            this.main_splitContainer.SuspendLayout();
            this.main_tableLayoutPanel.SuspendLayout();
            this.patronymic_groupBox.SuspendLayout();
            this.patronymic_tableLayoutPanel.SuspendLayout();
            this.name_groupBox.SuspendLayout();
            this.name_tableLayoutPanel.SuspendLayout();
            this.surname_groupBox.SuspendLayout();
            this.surname_tableLayoutPanel.SuspendLayout();
            this.disable_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_groupBox
            // 
            this.main_groupBox.Controls.Add(this.main_splitContainer);
            this.main_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_groupBox.Location = new System.Drawing.Point(0, 0);
            this.main_groupBox.Name = "main_groupBox";
            this.main_groupBox.Size = new System.Drawing.Size(504, 544);
            this.main_groupBox.TabIndex = 1;
            this.main_groupBox.TabStop = false;
            this.main_groupBox.Text = "Настройки пользователя";
            // 
            // main_splitContainer
            // 
            this.main_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_splitContainer.Location = new System.Drawing.Point(3, 16);
            this.main_splitContainer.Name = "main_splitContainer";
            this.main_splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // main_splitContainer.Panel1
            // 
            this.main_splitContainer.Panel1.Controls.Add(this.main_tableLayoutPanel);
            // 
            // main_splitContainer.Panel2
            // 
            this.main_splitContainer.Panel2.Controls.Add(this.disable_tableLayoutPanel);
            this.main_splitContainer.Size = new System.Drawing.Size(498, 525);
            this.main_splitContainer.SplitterDistance = 262;
            this.main_splitContainer.TabIndex = 1;
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.patronymic_groupBox, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.name_groupBox, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.surname_groupBox, 0, 0);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 4;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(498, 262);
            this.main_tableLayoutPanel.TabIndex = 0;
            // 
            // patronymic_groupBox
            // 
            this.patronymic_groupBox.Controls.Add(this.patronymic_tableLayoutPanel);
            this.patronymic_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patronymic_groupBox.Location = new System.Drawing.Point(3, 113);
            this.patronymic_groupBox.Name = "patronymic_groupBox";
            this.patronymic_groupBox.Size = new System.Drawing.Size(492, 49);
            this.patronymic_groupBox.TabIndex = 2;
            this.patronymic_groupBox.TabStop = false;
            this.patronymic_groupBox.Text = "Отчество";
            // 
            // patronymic_tableLayoutPanel
            // 
            this.patronymic_tableLayoutPanel.ColumnCount = 1;
            this.patronymic_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.patronymic_tableLayoutPanel.Controls.Add(this.patronymic_textBox, 0, 0);
            this.patronymic_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patronymic_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.patronymic_tableLayoutPanel.Name = "patronymic_tableLayoutPanel";
            this.patronymic_tableLayoutPanel.RowCount = 1;
            this.patronymic_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.patronymic_tableLayoutPanel.Size = new System.Drawing.Size(486, 30);
            this.patronymic_tableLayoutPanel.TabIndex = 0;
            // 
            // patronymic_textBox
            // 
            this.patronymic_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.patronymic_textBox.Location = new System.Drawing.Point(3, 5);
            this.patronymic_textBox.Name = "patronymic_textBox";
            this.patronymic_textBox.Size = new System.Drawing.Size(480, 20);
            this.patronymic_textBox.TabIndex = 3;
            // 
            // name_groupBox
            // 
            this.name_groupBox.Controls.Add(this.name_tableLayoutPanel);
            this.name_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_groupBox.Location = new System.Drawing.Point(3, 58);
            this.name_groupBox.Name = "name_groupBox";
            this.name_groupBox.Size = new System.Drawing.Size(492, 49);
            this.name_groupBox.TabIndex = 1;
            this.name_groupBox.TabStop = false;
            this.name_groupBox.Text = "Имя";
            // 
            // name_tableLayoutPanel
            // 
            this.name_tableLayoutPanel.ColumnCount = 1;
            this.name_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.name_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.name_tableLayoutPanel.Controls.Add(this.name_textBox, 0, 0);
            this.name_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.name_tableLayoutPanel.Name = "name_tableLayoutPanel";
            this.name_tableLayoutPanel.RowCount = 1;
            this.name_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.name_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.name_tableLayoutPanel.Size = new System.Drawing.Size(486, 30);
            this.name_tableLayoutPanel.TabIndex = 0;
            // 
            // name_textBox
            // 
            this.name_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.name_textBox.Location = new System.Drawing.Point(3, 5);
            this.name_textBox.Name = "name_textBox";
            this.name_textBox.Size = new System.Drawing.Size(480, 20);
            this.name_textBox.TabIndex = 2;
            // 
            // surname_groupBox
            // 
            this.surname_groupBox.Controls.Add(this.surname_tableLayoutPanel);
            this.surname_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.surname_groupBox.Location = new System.Drawing.Point(3, 3);
            this.surname_groupBox.Name = "surname_groupBox";
            this.surname_groupBox.Size = new System.Drawing.Size(492, 49);
            this.surname_groupBox.TabIndex = 0;
            this.surname_groupBox.TabStop = false;
            this.surname_groupBox.Text = "Фамилия";
            // 
            // surname_tableLayoutPanel
            // 
            this.surname_tableLayoutPanel.ColumnCount = 1;
            this.surname_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.surname_tableLayoutPanel.Controls.Add(this.surname_textBox, 0, 0);
            this.surname_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.surname_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.surname_tableLayoutPanel.Name = "surname_tableLayoutPanel";
            this.surname_tableLayoutPanel.RowCount = 1;
            this.surname_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.surname_tableLayoutPanel.Size = new System.Drawing.Size(486, 30);
            this.surname_tableLayoutPanel.TabIndex = 0;
            // 
            // surname_textBox
            // 
            this.surname_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.surname_textBox.Location = new System.Drawing.Point(3, 5);
            this.surname_textBox.Name = "surname_textBox";
            this.surname_textBox.Size = new System.Drawing.Size(480, 20);
            this.surname_textBox.TabIndex = 1;
            // 
            // disable_tableLayoutPanel
            // 
            this.disable_tableLayoutPanel.ColumnCount = 3;
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.Controls.Add(this.disable_image_label, 1, 1);
            this.disable_tableLayoutPanel.Controls.Add(this.disable_text_label, 1, 2);
            this.disable_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.disable_tableLayoutPanel.Name = "disable_tableLayoutPanel";
            this.disable_tableLayoutPanel.RowCount = 4;
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.Size = new System.Drawing.Size(498, 259);
            this.disable_tableLayoutPanel.TabIndex = 3;
            // 
            // disable_image_label
            // 
            this.disable_image_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_image_label.Image = global::IA.Controls.Properties.Resources.Disable_Icon_Settings;
            this.disable_image_label.Location = new System.Drawing.Point(152, 50);
            this.disable_image_label.Name = "disable_image_label";
            this.disable_image_label.Size = new System.Drawing.Size(194, 128);
            this.disable_image_label.TabIndex = 0;
            this.disable_image_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // disable_text_label
            // 
            this.disable_text_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_text_label.Location = new System.Drawing.Point(152, 178);
            this.disable_text_label.Name = "disable_text_label";
            this.disable_text_label.Size = new System.Drawing.Size(194, 30);
            this.disable_text_label.TabIndex = 1;
            this.disable_text_label.Text = "Список настроек недоступен";
            this.disable_text_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UserSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_groupBox);
            this.Name = "UserSettingsControl";
            this.Size = new System.Drawing.Size(504, 544);
            this.main_groupBox.ResumeLayout(false);
            this.main_splitContainer.Panel1.ResumeLayout(false);
            this.main_splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).EndInit();
            this.main_splitContainer.ResumeLayout(false);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.patronymic_groupBox.ResumeLayout(false);
            this.patronymic_tableLayoutPanel.ResumeLayout(false);
            this.patronymic_tableLayoutPanel.PerformLayout();
            this.name_groupBox.ResumeLayout(false);
            this.name_tableLayoutPanel.ResumeLayout(false);
            this.name_tableLayoutPanel.PerformLayout();
            this.surname_groupBox.ResumeLayout(false);
            this.surname_tableLayoutPanel.ResumeLayout(false);
            this.surname_tableLayoutPanel.PerformLayout();
            this.disable_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox main_groupBox;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.GroupBox name_groupBox;
        private System.Windows.Forms.TableLayoutPanel name_tableLayoutPanel;
        private System.Windows.Forms.TextBox name_textBox;
        private System.Windows.Forms.GroupBox surname_groupBox;
        private System.Windows.Forms.TableLayoutPanel surname_tableLayoutPanel;
        private System.Windows.Forms.TextBox surname_textBox;
        private System.Windows.Forms.GroupBox patronymic_groupBox;
        private System.Windows.Forms.TableLayoutPanel patronymic_tableLayoutPanel;
        private System.Windows.Forms.TextBox patronymic_textBox;
        private System.Windows.Forms.SplitContainer main_splitContainer;
        private System.Windows.Forms.TableLayoutPanel disable_tableLayoutPanel;
        private System.Windows.Forms.Label disable_image_label;
        private System.Windows.Forms.Label disable_text_label;
    }
}
