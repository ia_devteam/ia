﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace IA.Controls
{
    /// <summary>
    /// Класс, описывающий элемент управления - Окно "О программе"
    /// </summary>
    public partial class AboutControl : UserControl
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public AboutControl()
        {
            InitializeComponent();

            //Задаём настройки панели для отображения иконки приложения
            this.main_pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            this.main_pictureBox.Image = Properties.Resources.Application_Icon;

            //Наименование
            this.fullName_text_label.Text = IA.Constants.Application.FullName;

            //Версия
            this.version_text_label.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            //Разработчик
            this.developer_text_label.Text = IA.Constants.Developer.Name;

            //Почтовый адрес
            this.postAddress_text_label.Text = IA.Constants.Developer.PostAddress;
            
            //E-mail
            LinkLabel.Link developerEmailLink = new LinkLabel.Link()
            {
                LinkData = "mailto:" + IA.Constants.Developer.Email
            };
            this.email_linkLabel.Text = IA.Constants.Developer.Email;
            this.email_linkLabel.Links.Add(developerEmailLink);
            this.email_linkLabel.LinkClicked += delegate(object sender, LinkLabelLinkClickedEventArgs e)
            {
                Process.Start(e.Link.LinkData as String);
            };

            //Веб сайт
            LinkLabel.Link developerWebSiteLink = new LinkLabel.Link()
            {
                LinkData = IA.Constants.Developer.WebSite
            };
            this.webSite_linkLabel.Text = IA.Constants.Developer.WebSite;
            this.webSite_linkLabel.Links.Add(developerWebSiteLink);
            this.webSite_linkLabel.LinkClicked += delegate(object sender, LinkLabelLinkClickedEventArgs e)
            {
                Process.Start(e.Link.LinkData as String);
            };

            //Все права защищены
            this.allRightsReserved_label.Text = "©" + IA.Constants.Developer.Name + ", " + this.GetExecutingAssemblyDateTime().Year + ". Все права защищены.";
        }

        /// <summary>
        /// Получить дату создания текущей сборки
        /// </summary>
        /// <returns></returns>
        private DateTime GetExecutingAssemblyDateTime()
        {
            string assemblyPath = Assembly.GetCallingAssembly().Location;
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            byte[] b = new byte[2048];

            using (FileStream reader = new FileStream(assemblyPath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                reader.Read(b, 0, 2048);

            int i = BitConverter.ToInt32(b, c_PeHeaderOffset);
            int secondsSince1970 = BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
            
            DateTime ret = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            ret = ret.AddSeconds(secondsSince1970);
            ret = ret.ToLocalTime();
            
            return ret;
        }
    }
}
