﻿using System;
using System.Drawing;
using System.Windows.Forms;
using IA.Extensions;
using IA.Objects;

namespace IA.Controls
{
    /// <summary>
    /// Класс, реализующий контрол выбора\настройки проекта
    /// </summary>
    public partial class ProjectSelectControl : Form
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Контрол выбора проекта";

        /// <summary>
        /// перерисовать форму, если есть изменения
        /// </summary>
        public void ShakeForm()
        {
            if (materialsSelectControl != null)
                materialsSelectControl.ShakeForm(Result);
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="parent">родительская форма</param>
        public ProjectSelectControl()
        {
            InitializeComponent();
            
            pBox.Image.RotateFlip(RotateFlipType.Rotate180FlipNone);
        }

        /// <summary>
        /// Обработчик - Нажали на проект
        /// </summary>
        /// <param name="project"></param>
        private void projectListControl_Click(Project project)
        {
            try
            {
                //Определяем доступен ли список настроек
                //materialsSelectControl.IsEnabled = project != null;
                
                //main_splitContainer.Panel2.Controls.Remove(materialsSelectControl);
                
                //Если проект не выбран, ничего не делаем
                if (project == null)
                    return;

                //Отображаем материалы
                materialsSelectControl.ShakeForm(project);

                //main_splitContainer.Panel2.Controls.Add(materialsSelectControl);

                Properties.Settings.Default.LastProject = project.ID;
                Properties.Settings.Default.Save();

                Result = project;
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Однократное нажатие на проект в списке", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// родительская форма
        /// </summary>
        Control _parent = null;

        Control parent
        {
            get
            {
                if (_parent == null)
                {
                    this._parent = Parent;
                    while (this.parent.Parent != null)
                        this._parent = this.parent.Parent;
                }
                return _parent;
            }
        }

        public Project Result { get; set; }
    }
}
