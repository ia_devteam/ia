﻿namespace IA.Controls
{
    partial class UserAddOrEditControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ready_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ready_button = new System.Windows.Forms.Button();
            this.main_tableLayoutPanel.SuspendLayout();
            this.ready_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.ready_tableLayoutPanel, 0, 1);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 2;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(396, 362);
            this.main_tableLayoutPanel.TabIndex = 0;
            // 
            // ready_tableLayoutPanel
            // 
            this.ready_tableLayoutPanel.ColumnCount = 3;
            this.ready_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ready_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.ready_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ready_tableLayoutPanel.Controls.Add(this.ready_button, 1, 0);
            this.ready_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ready_tableLayoutPanel.Location = new System.Drawing.Point(3, 315);
            this.ready_tableLayoutPanel.Name = "ready_tableLayoutPanel";
            this.ready_tableLayoutPanel.RowCount = 1;
            this.ready_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ready_tableLayoutPanel.Size = new System.Drawing.Size(390, 44);
            this.ready_tableLayoutPanel.TabIndex = 0;
            // 
            // ready_button
            // 
            this.ready_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ready_button.Location = new System.Drawing.Point(98, 3);
            this.ready_button.Name = "ready_button";
            this.ready_button.Size = new System.Drawing.Size(194, 38);
            this.ready_button.TabIndex = 1;
            this.ready_button.Text = "Готово";
            this.ready_button.UseVisualStyleBackColor = true;
            this.ready_button.Click += new System.EventHandler(this.ready_button_Click);
            // 
            // UserAddOrEditControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "UserAddOrEditControl";
            this.Size = new System.Drawing.Size(396, 362);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.ready_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel ready_tableLayoutPanel;
        private System.Windows.Forms.Button ready_button;
    }
}
