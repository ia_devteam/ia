﻿using System;
using System.Windows.Forms;

using IA.Extensions;
using IA.Objects;

namespace IA.Controls
{
    /// <summary>
    /// Класс, реализующий элемент управления для добавления нового проекта
    /// </summary>
    public partial class ProjectAddControl : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public const string ObjectName = "Окно добавления проекта";

        /// <summary>
        /// Элемент управления - Окно настроек проекта
        /// </summary>
        private ProjectSettingsControl projectSettingsControl = null;

        /// <summary>
        /// Делегат для события - Добавление нового проекта завершено
        /// </summary>
        /// <param name="project">Добавленный проект. Не может быть - Null.</param>
        internal delegate void ReadyEventHandler(Project project);

        /// <summary>
        /// Событие - Добавление нового проекта завершено
        /// </summary>
        internal event ReadyEventHandler ReadyEvent;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        public ProjectAddControl()
        {
            InitializeComponent();

            //Инициализация элемента управления - окно настроек проекта
            projectSettingsControl = new ProjectSettingsControl(IA.Config.Controller.Conditions, IA.Config.Controller.Settings)
            {
                Dock = DockStyle.Fill,
                IsBlocked = false,
                IsEnabled = true,
            };

            //Добавляем окно настроек проекта в основную таблицу
            main_tableLayoutPanel.Controls.Add(projectSettingsControl, 0, 1);
            
            Sql.SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку "Готово"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ready_button_Click(object sender, EventArgs e)
        {
            //Проект, который предположительно будет создан
            Project project = null;

            try
            {
                //Проверяем настройки проекта на адекватность
                try
                {
                    //Пытаемся построить рабочий процесс и убеждаемся, что он не содержит ошибок в логике
                    IA.Config.Controller.GetSpecificGlobalWorkflow(projectSettingsControl.UserSettingsSet);
                }
                catch (IA.Config.Objects.Workflows.Workflow.ContentErrorException ex)
                {
                    //Формируем исключение
                    throw new Exception("Ошибка в настройках проекта.", ex);
                }

                //Если имя проекта не было задано
                if (String.IsNullOrWhiteSpace(name_textBox.Text))
                {
                    //Уточняем у пользователя намеренно ли он не задал имя?
                    switch (MessageBox.Show("Имя проекта не задано. Задать имя проекта автоматически?", "Внимание", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                    {
                        case DialogResult.Yes:
                            break;
                        case DialogResult.No:
                        case DialogResult.Cancel:
                            return;
                    }
                }

                //Отображаем окно ожидания
                project = WaitWindow.Show(
                    delegate(WaitWindow window)
                    {
                        //Добавляем новый проект в БД
                        window.WorkerMethodResult = Project.Add(name_textBox.Text, projectSettingsControl.UserSettingsSet.ToString(), IA.Settings.ProjectsServer.ProjectsDirectoryPath);
                    },
                    "Добавление проекта" + (String.IsNullOrWhiteSpace(name_textBox.Text) ? String.Empty : " <" + name_textBox.Text + ">")
                ) as Project;

                //Проверка результата
                project.ThrowIfNull("Результат добавления нового проекта в базу данных не соответствует ожидаемому.");
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при добавлении проекта.", (message) => Monitor.Log.Error(ObjectName, message, true));

                //Если проект по какой-то причине уже был добавлен в базу данных
                if (project != null)  
                    //Удаляем проект из базы данных
                    Project.RemoveByID(project.ID);

                return;
            }

            //Формируем событие
            if (ReadyEvent != null)
                ReadyEvent(project);
        }
        
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Делаем его недоступным
            this.Enabled = false;
        }
    }
}
