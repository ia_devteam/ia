﻿using System;
using System.Windows.Forms;

using IA.Objects;
using IA.Extensions;

namespace IA.Controls
{
    /// <summary>
    /// Реализация элемента управления создания и редактирования пользователя
    /// </summary>
    public partial class UserAddOrEditControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        public const string ObjectName = "Окно добавления/редактирования информации о пользователе";

        /// <summary>
        /// Элемент управления - Окно настроек пользователя
        /// </summary>
        private UserSettingsControl userSettingsControl = null;

        /// <summary>
        /// Делегат для события - Создание/редактирование пользователя.
        /// </summary>
        /// <param name="user">Пользователь. Не может быть - Null.</param>
        internal delegate void ReadyEventHandler(User user);

        /// <summary>
        /// Событие - Создание/редактирование пользователя завершено
        /// </summary>
        internal event ReadyEventHandler ReadyEvent;

        /// <summary>
        /// Объект пользователя
        /// </summary>
        private User user = null;



        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="user">Пользователь. Если Null - добавление пользователя, иначе - редактирование пользователя.</param>
        public UserAddOrEditControl(User user = null)
        {
            InitializeComponent();

            //Запоминаем объект пользователя
            this.user = user;

            //Инициализация элемента управления - окно настроек проекта
            userSettingsControl = new UserSettingsControl()
            {
                Dock = DockStyle.Fill,
                IsBlocked = false,
                IsEnabled = true,
                User = user
            };

            //Добавляем окно настроек проекта в основную таблицу
            main_tableLayoutPanel.Controls.Add(userSettingsControl, 0, 0);
            
            Sql.SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку "Готово"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ready_button_Click(object sender, EventArgs e)
        {
            //Находимся ли мы в режиме добавления пользователя
            bool isAdd = this.user == null;

            try
            {
                    //Ищем пользователя с заданным полным именем в БД
                    User userToRestore = User.GetByFullName(userSettingsControl.UserSurname, userSettingsControl.UserName, userSettingsControl.UserPatronymic);

                    //Если пользователя с заданным полным именем нет в БД
                    if (userToRestore == null)
                    {
                        //Если мы находимся в режиме добавления пользователя
                        if(isAdd)
                        {
                            //Добавляем пользователя в БД
                            this.user = User.Add(userSettingsControl.UserSurname, userSettingsControl.UserName, userSettingsControl.UserPatronymic);
                        }
                        //Если мы находимся в режиме редактирования пользователя
                        else
                        {
                            //Если изменилась фамилия, меняем её в БД
                            if (this.user.Surname != userSettingsControl.UserSurname)
                                this.user.Surname = userSettingsControl.UserSurname;

                            //Если изменилось имя, меняем его в БД
                            if (this.user.Name != userSettingsControl.UserName)
                                this.user.Name = userSettingsControl.UserName;

                            //Если изменилось отчество, меняем его в БД
                            if (this.user.Patronymic != userSettingsControl.UserPatronymic)
                                this.user.Patronymic = userSettingsControl.UserPatronymic;
                        }
                    }
                    //Если пользователя с заданным полным именем уже есть в БД
                    else
                    {
                        //Если пользователь помечен как удалённый
                        if (userToRestore.IsDeleted)
                        {
                            //Если мы находимся в режиме добавления пользователя
                            if (isAdd)
                            {
                                //Задаём вопрос
                                switch (MessageBox.Show("Такой пользователь был удалён ранее. Восстановить его?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                                {
                                    case DialogResult.Yes:
                                        {
                                            //Восстанавливаем пользователя
                                            userToRestore.IsDeleted = false;

                                            break;
                                        }
                                    case DialogResult.No:
                                        return;
                                }
                            }
                            //Если мы находимся в режиме редактирования пользователя
                            else
                            { 
                                //Формируем исключение
                                throw new Exception(
                                    "Пользователь с именем <" + userToRestore.Fullname + "> уже есть в базе данных, но помечен, как удалённый. " +
                                        "Для восстановления пользователя воспользуйтесь формой добавления."
                                );
                            }
                        }
                        else
                        {
                            //Формируем исключение
                            throw new Exception("Пользователь с именем <" + userToRestore.Fullname + "> уже есть в базе данных.");
                        }

                        //Получаем ссылку на восстановленного пользователя в качестве результата
                        this.user = userToRestore;
                    }

                    //Проверка на разумность
                    if (this.user == null)
                        throw new Exception("Неизвестная ошибка.");
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при " + (isAdd ? "добавлении" : "редактировании") + " информации о пользователе.", (message) => Monitor.Log.Error(ObjectName, message, true));

                //Если мы находимся в режиме добавления пользователя и пользователь по какой-то причине уже был добавлен в базу данных, удаляем его
                if (isAdd && user != null)
                    User.RemoveByID(user.ID);

                return;
            }

            //Формируем событие
            if (ReadyEvent != null)
                ReadyEvent(this.user);
        }
        
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Делаем его недоступным
            this.Enabled = false;
        }
    }
}
