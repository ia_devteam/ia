﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

using IA.Objects;
using IA.Extensions;

namespace IA.Controls
{
    /// <summary>
    /// Реализация элемента управления - Свойства Хранилища
    /// </summary>
    public partial class StorageSettingsControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Свойства Хранилища";

        /// <summary>
        /// Режимы кнопки редактирования
        /// </summary>
        private enum EditButtonMode
        {
            /// <summary>
            /// Режим редактирования
            /// </summary>
            [Description("Изменить")]
            EDIT,
            /// <summary>
            /// Режим сохранения изменений
            /// </summary>
            [Description("Готово")]
            SAVE
        }

        private Storage storage = null;
        /// <summary>
        /// Текущее Хранилище, чьи свойства необходимо отобразить
        /// </summary>
        public Storage Storage
        {
            set
            {
                //Если ничего не поменялось, то ничего не делаем
                if (storage != null && value != null && storage.ID == value.ID)
                    return;

                //Сохраняем переданное значение
                this.storage = value;

                //Отображаем свойства текущего Хранилища
                this.Generate();
            }
        }

        /// <summary>
        /// Включить\отключить доступность списка настроек. В режиме недоступности список свойств не отображается.
        /// </summary>
        public bool IsEnabled
        {
            set
            {
                //Скрываем\Показываем сообщение о недоступности настроек
                main_splitContainer.Panel1Collapsed = !value;
                main_splitContainer.Panel2Collapsed = value;
            }
        }

        /// <summary>
        /// Включить\отключить режим блокирование списка настроек. В режиме блокирования список свойств отображается, но никакие действия не доступны.
        /// </summary>
        public bool IsBlocked
        {
            set
            {
                this.main_tableLayoutPanel.Enabled = !value;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public StorageSettingsControl()
        {
            InitializeComponent();

            //Отображаем свойства текущего Хранилища
            this.Generate();
        }

        /// <summary>
        /// Отображение свойсвт текущего Хранилища
        /// </summary>
        private void Generate()
        {
            //Если Хранилище не задано
            if (this.storage == null)
                return;

            //Отображаем создатебя Хранилища
            creator_label.Text = "Создатель: " + this.storage.CreatorFullName;
            creator_label.Refresh();

            //Отображаем дату и время создания Хранилища
            datetime_label.Text = "Дата и время создания: " + storage.DateTime.ToShortDateString() + " " + storage.DateTime.ToShortTimeString();
            datetime_label.Refresh();

            //Отображаем информацию о том, содержит ли Хранилище данные
            isEmpty_label.Text = "Данные: " + (storage.IsEmpty ? "Нет" : "Да");
            isEmpty_label.Refresh();

            //Отображаем режим работы Хранилища
            mode_label.Text = "Режим создания: " + storage.Mode.Description();
            mode_label.Refresh();

            //Отображаем комментарий к Хранилищу
            comment_textBox.Enabled = false;
            comment_textBox.Text = storage.Comment;

            //Переводим кнопку комментария в режим редактирования
            comment_button.Text = EditButtonMode.EDIT.Description();
            comment_button.Tag = EditButtonMode.EDIT;
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку изменения\сохранения комментария
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comment_button_Click(object sender, EventArgs e)
        {
            try
            {
                //Действуем в зависимости от режима кнопки
                switch ((EditButtonMode)comment_button.Tag)
                {
                    case EditButtonMode.EDIT:
                        {
                            //Меняем доступность комментария
                            comment_textBox.Enabled = true;

                            //Меняем режим кнопки
                            comment_button.Text = EditButtonMode.SAVE.Description();
                            comment_button.Tag = EditButtonMode.SAVE;

                            break;
                        }
                    case EditButtonMode.SAVE:
                        {
                            //Изменяем комментарий в БД
                            this.storage.Comment = comment_textBox.Text;

                            //Меняем доступность комментария
                            comment_textBox.Enabled = false;

                            //Меняем режим кнопки
                            comment_button.Text = EditButtonMode.EDIT.Description();
                            comment_button.Tag = EditButtonMode.EDIT;

                            //Обновляем отображаемый комментарий
                            comment_textBox.Text = storage.Comment;
                            
                            break;
                        }
                    default:
                        throw new Exception("Обнаружен неизвестный режим работы кнопки редактирования комментария.");
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при изменении комментария к Хранилищу.", (message) => Monitor.Log.Error(StorageSettingsControl.ObjectName, message, true));

                return;
            }
        }
    }
}
