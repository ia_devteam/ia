﻿namespace IA.Controls
{
    partial class OtherSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.workDirectoryPath_button = new System.Windows.Forms.Button();
            this.workDirectoryPath_textBox = new System.Windows.Forms.TextBox();
            this.workDirectoryPath_label = new System.Windows.Forms.Label();
            this.main_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 3;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.workDirectoryPath_button, 2, 0);
            this.main_tableLayoutPanel.Controls.Add(this.workDirectoryPath_textBox, 1, 0);
            this.main_tableLayoutPanel.Controls.Add(this.workDirectoryPath_label, 0, 0);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 2;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(568, 40);
            this.main_tableLayoutPanel.TabIndex = 1;
            // 
            // workDirectoryPath_button
            // 
            this.workDirectoryPath_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.workDirectoryPath_button.Location = new System.Drawing.Point(471, 6);
            this.workDirectoryPath_button.Name = "workDirectoryPath_button";
            this.workDirectoryPath_button.Size = new System.Drawing.Size(94, 23);
            this.workDirectoryPath_button.TabIndex = 0;
            this.workDirectoryPath_button.Text = "Обзор...";
            this.workDirectoryPath_button.UseVisualStyleBackColor = true;
            this.workDirectoryPath_button.Click += new System.EventHandler(this.workDirectoryPath_button_Click);
            // 
            // workDirectoryPath_textBox
            // 
            this.workDirectoryPath_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.workDirectoryPath_textBox.Location = new System.Drawing.Point(153, 7);
            this.workDirectoryPath_textBox.Name = "workDirectoryPath_textBox";
            this.workDirectoryPath_textBox.Size = new System.Drawing.Size(312, 20);
            this.workDirectoryPath_textBox.TabIndex = 1;
            // 
            // workDirectoryPath_label
            // 
            this.workDirectoryPath_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.workDirectoryPath_label.AutoSize = true;
            this.workDirectoryPath_label.Location = new System.Drawing.Point(3, 11);
            this.workDirectoryPath_label.Name = "workDirectoryPath_label";
            this.workDirectoryPath_label.Size = new System.Drawing.Size(144, 13);
            this.workDirectoryPath_label.TabIndex = 5;
            this.workDirectoryPath_label.Text = "Рабочая директория:";
            this.workDirectoryPath_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OtherSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "OtherSettingsControl";
            this.Size = new System.Drawing.Size(568, 40);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.main_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.Button workDirectoryPath_button;
        private System.Windows.Forms.TextBox workDirectoryPath_textBox;
        private System.Windows.Forms.Label workDirectoryPath_label;

    }
}
