﻿namespace IA.Controls
{
    partial class SelectSingleStorage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxWithDialogButton1 = new IA.Controls.TextBoxWithDialogButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxWithDialogButton1);
            this.groupBox1.Controls.Add(this.btnOk);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 70);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выбор хранилища для автономной работы";
            // 
            // textBoxWithDialogButton1
            // 
            this.textBoxWithDialogButton1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBoxWithDialogButton1.Location = new System.Drawing.Point(3, 16);
            this.textBoxWithDialogButton1.MaximumSize = new System.Drawing.Size(0, 24);
            this.textBoxWithDialogButton1.MinimumSize = new System.Drawing.Size(150, 24);
            this.textBoxWithDialogButton1.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.OpenFileDialog;
            this.textBoxWithDialogButton1.Name = "textBoxWithDialogButton1";
            this.textBoxWithDialogButton1.Size = new System.Drawing.Size(254, 24);
            this.textBoxWithDialogButton1.TabIndex = 1;
            // 
            // btnOk
            // 
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnOk.Location = new System.Drawing.Point(3, 44);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(254, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Открыть";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // SelectSingleStorage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(0, 70);
            this.MinimumSize = new System.Drawing.Size(260, 70);
            this.Name = "SelectSingleStorage";
            this.Size = new System.Drawing.Size(260, 70);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private TextBoxWithDialogButton textBoxWithDialogButton1;
        private System.Windows.Forms.Button btnOk;
    }
}
