﻿using IA.Controls;
using System.Drawing;
using System.Windows.Forms;
namespace IA.Controls
{
    partial class ProjectSelectControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pBox = new System.Windows.Forms.PictureBox();
            this.main_splitContainer = new System.Windows.Forms.SplitContainer();
            this.tabProjectsAndArrowLeft = new System.Windows.Forms.TableLayoutPanel();
            this.materialsSelectControl = new IA.Controls.MaterialsSelectControl();
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).BeginInit();
            this.projectListControl = new IA.Controls.ProjectListControl(Properties.Settings.Default.LastProject);
            this.main_splitContainer.Panel1.SuspendLayout();
            this.main_splitContainer.Panel2.SuspendLayout();
            this.main_splitContainer.SuspendLayout();
            this.tabProjectsAndArrowLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // pBox
            // 
            this.pBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.pBox.Image = Properties.Resources.Common_Button_Back;
            this.pBox.Location = new System.Drawing.Point(379, 3);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(44, 50);
            this.pBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBox.TabIndex = 1;
            this.pBox.TabStop = false;
            // 
            // main_splitContainer
            // 
            this.main_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_splitContainer.Location = new System.Drawing.Point(0, 0);
            this.main_splitContainer.Name = "main_splitContainer";
            // 
            // main_splitContainer.Panel1
            // 
            this.main_splitContainer.Panel1.Controls.Add(this.tabProjectsAndArrowLeft);
            // 
            // main_splitContainer.Panel2
            // 
            this.main_splitContainer.Panel2.Controls.Add(this.materialsSelectControl);
            this.main_splitContainer.Size = new System.Drawing.Size(1285, 580);
            this.main_splitContainer.SplitterDistance = 426;
            this.main_splitContainer.TabIndex = 0;
            // 
            // tabProjectsAndArrowLeft
            // 
            this.tabProjectsAndArrowLeft.ColumnCount = 2;
            this.tabProjectsAndArrowLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tabProjectsAndArrowLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tabProjectsAndArrowLeft.Controls.Add(this.projectListControl, 0, 0);
            this.tabProjectsAndArrowLeft.Controls.Add(this.pBox, 1, 0);
            this.tabProjectsAndArrowLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabProjectsAndArrowLeft.Location = new System.Drawing.Point(0, 0);
            this.tabProjectsAndArrowLeft.Name = "tabProjectsAndArrowLeft";
            this.tabProjectsAndArrowLeft.RowCount = 1;
            this.tabProjectsAndArrowLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tabProjectsAndArrowLeft.Size = new System.Drawing.Size(426, 580);
            this.tabProjectsAndArrowLeft.TabIndex = 0;
            // 
            // projectListControl
            // 
            this.projectListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectListControl.Location = new System.Drawing.Point(3, 3);
            this.projectListControl.Name = "projectListControl";
            this.projectListControl.Size = new System.Drawing.Size(370, 574);
            this.projectListControl.TabIndex = 0;
            this.projectListControl.ProjectClickEvent += this.projectListControl_Click;
            // 
            // materialsSelectControl1
            // 
            this.materialsSelectControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialsSelectControl.Location = new System.Drawing.Point(0, 0);
            this.materialsSelectControl.Name = "materialsSelectControl1";
            this.materialsSelectControl.Result = null;
            this.materialsSelectControl.Size = new System.Drawing.Size(855, 580);
            this.materialsSelectControl.TabIndex = 0;
            // 
            // ProjectSelectControl
            // 
            this.ClientSize = new System.Drawing.Size(1285, 580);
            this.Controls.Add(this.main_splitContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProjectSelectControl";
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).EndInit();
            this.main_splitContainer.Panel1.ResumeLayout(false);
            this.main_splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).EndInit();
            this.main_splitContainer.ResumeLayout(false);
            this.tabProjectsAndArrowLeft.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer main_splitContainer;
        private Controls.ProjectListControl projectListControl;
        private TableLayoutPanel tabProjectsAndArrowLeft;
        private PictureBox pBox;
        private MaterialsSelectControl materialsSelectControl;
    }
}