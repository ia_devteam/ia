﻿namespace IA.Controls
{
    partial class StorageSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.creator_label = new System.Windows.Forms.Label();
            this.datetime_label = new System.Windows.Forms.Label();
            this.isEmpty_label = new System.Windows.Forms.Label();
            this.mode_label = new System.Windows.Forms.Label();
            this.comment_label = new System.Windows.Forms.Label();
            this.comment_textBox = new System.Windows.Forms.TextBox();
            this.comment_button = new System.Windows.Forms.Button();
            this.main_splitContainer = new System.Windows.Forms.SplitContainer();
            this.disable_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.disable_image_label = new System.Windows.Forms.Label();
            this.disable_text_label = new System.Windows.Forms.Label();
            this.main_groupBox = new System.Windows.Forms.GroupBox();
            this.main_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).BeginInit();
            this.main_splitContainer.Panel1.SuspendLayout();
            this.main_splitContainer.Panel2.SuspendLayout();
            this.main_splitContainer.SuspendLayout();
            this.disable_tableLayoutPanel.SuspendLayout();
            this.main_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 3;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.creator_label, 0, 0);
            this.main_tableLayoutPanel.Controls.Add(this.datetime_label, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.isEmpty_label, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.mode_label, 0, 3);
            this.main_tableLayoutPanel.Controls.Add(this.comment_label, 0, 4);
            this.main_tableLayoutPanel.Controls.Add(this.comment_textBox, 1, 4);
            this.main_tableLayoutPanel.Controls.Add(this.comment_button, 2, 4);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 6;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(513, 163);
            this.main_tableLayoutPanel.TabIndex = 0;
            // 
            // creator_label
            // 
            this.creator_label.AutoSize = true;
            this.main_tableLayoutPanel.SetColumnSpan(this.creator_label, 3);
            this.creator_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.creator_label.Location = new System.Drawing.Point(3, 0);
            this.creator_label.Name = "creator_label";
            this.creator_label.Size = new System.Drawing.Size(507, 30);
            this.creator_label.TabIndex = 5;
            this.creator_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // datetime_label
            // 
            this.datetime_label.AutoSize = true;
            this.main_tableLayoutPanel.SetColumnSpan(this.datetime_label, 3);
            this.datetime_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datetime_label.Location = new System.Drawing.Point(3, 30);
            this.datetime_label.Name = "datetime_label";
            this.datetime_label.Size = new System.Drawing.Size(507, 30);
            this.datetime_label.TabIndex = 6;
            this.datetime_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // isEmpty_label
            // 
            this.isEmpty_label.AutoSize = true;
            this.main_tableLayoutPanel.SetColumnSpan(this.isEmpty_label, 3);
            this.isEmpty_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.isEmpty_label.Location = new System.Drawing.Point(3, 60);
            this.isEmpty_label.Name = "isEmpty_label";
            this.isEmpty_label.Size = new System.Drawing.Size(507, 30);
            this.isEmpty_label.TabIndex = 7;
            this.isEmpty_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mode_label
            // 
            this.mode_label.AutoSize = true;
            this.main_tableLayoutPanel.SetColumnSpan(this.mode_label, 3);
            this.mode_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mode_label.Location = new System.Drawing.Point(3, 90);
            this.mode_label.Name = "mode_label";
            this.mode_label.Size = new System.Drawing.Size(507, 30);
            this.mode_label.TabIndex = 8;
            this.mode_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comment_label
            // 
            this.comment_label.AutoSize = true;
            this.comment_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comment_label.Location = new System.Drawing.Point(3, 120);
            this.comment_label.Name = "comment_label";
            this.comment_label.Size = new System.Drawing.Size(81, 30);
            this.comment_label.TabIndex = 9;
            this.comment_label.Text = "Комментарий:";
            this.comment_label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comment_textBox
            // 
            this.comment_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comment_textBox.Location = new System.Drawing.Point(90, 125);
            this.comment_textBox.Name = "comment_textBox";
            this.comment_textBox.Size = new System.Drawing.Size(320, 20);
            this.comment_textBox.TabIndex = 1;
            // 
            // comment_button
            // 
            this.comment_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comment_button.Location = new System.Drawing.Point(416, 123);
            this.comment_button.Name = "comment_button";
            this.comment_button.Size = new System.Drawing.Size(94, 24);
            this.comment_button.TabIndex = 10;
            this.comment_button.Text = "Изменить";
            this.comment_button.UseVisualStyleBackColor = true;
            this.comment_button.Click += new System.EventHandler(this.comment_button_Click);
            // 
            // main_splitContainer
            // 
            this.main_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_splitContainer.Location = new System.Drawing.Point(3, 16);
            this.main_splitContainer.Name = "main_splitContainer";
            this.main_splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // main_splitContainer.Panel1
            // 
            this.main_splitContainer.Panel1.Controls.Add(this.main_tableLayoutPanel);
            // 
            // main_splitContainer.Panel2
            // 
            this.main_splitContainer.Panel2.Controls.Add(this.disable_tableLayoutPanel);
            this.main_splitContainer.Size = new System.Drawing.Size(513, 330);
            this.main_splitContainer.SplitterDistance = 163;
            this.main_splitContainer.TabIndex = 1;
            // 
            // disable_tableLayoutPanel
            // 
            this.disable_tableLayoutPanel.ColumnCount = 3;
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.Controls.Add(this.disable_image_label, 1, 1);
            this.disable_tableLayoutPanel.Controls.Add(this.disable_text_label, 1, 2);
            this.disable_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.disable_tableLayoutPanel.Name = "disable_tableLayoutPanel";
            this.disable_tableLayoutPanel.RowCount = 4;
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.Size = new System.Drawing.Size(513, 163);
            this.disable_tableLayoutPanel.TabIndex = 4;
            // 
            // disable_image_label
            // 
            this.disable_image_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_image_label.Image = global::IA.Controls.Properties.Resources.Disable_Icon_Settings;
            this.disable_image_label.Location = new System.Drawing.Point(159, 2);
            this.disable_image_label.Name = "disable_image_label";
            this.disable_image_label.Size = new System.Drawing.Size(194, 128);
            this.disable_image_label.TabIndex = 0;
            this.disable_image_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // disable_text_label
            // 
            this.disable_text_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_text_label.Location = new System.Drawing.Point(159, 130);
            this.disable_text_label.Name = "disable_text_label";
            this.disable_text_label.Size = new System.Drawing.Size(194, 30);
            this.disable_text_label.TabIndex = 1;
            this.disable_text_label.Text = "Список свойств недоступен";
            this.disable_text_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // main_groupBox
            // 
            this.main_groupBox.Controls.Add(this.main_splitContainer);
            this.main_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_groupBox.Location = new System.Drawing.Point(0, 0);
            this.main_groupBox.Name = "main_groupBox";
            this.main_groupBox.Size = new System.Drawing.Size(519, 349);
            this.main_groupBox.TabIndex = 2;
            this.main_groupBox.TabStop = false;
            this.main_groupBox.Text = "Свойства Хранилища";
            // 
            // StorageSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_groupBox);
            this.Name = "StorageSettingsControl";
            this.Size = new System.Drawing.Size(519, 349);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.main_tableLayoutPanel.PerformLayout();
            this.main_splitContainer.Panel1.ResumeLayout(false);
            this.main_splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).EndInit();
            this.main_splitContainer.ResumeLayout(false);
            this.disable_tableLayoutPanel.ResumeLayout(false);
            this.main_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.SplitContainer main_splitContainer;
        private System.Windows.Forms.TableLayoutPanel disable_tableLayoutPanel;
        private System.Windows.Forms.Label disable_image_label;
        private System.Windows.Forms.Label disable_text_label;
        private System.Windows.Forms.GroupBox main_groupBox;
        private System.Windows.Forms.TextBox comment_textBox;
        private System.Windows.Forms.Label creator_label;
        private System.Windows.Forms.Label datetime_label;
        private System.Windows.Forms.Label isEmpty_label;
        private System.Windows.Forms.Label mode_label;
        private System.Windows.Forms.Label comment_label;
        private System.Windows.Forms.Button comment_button;
    }
}
