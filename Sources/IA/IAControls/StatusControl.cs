﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using IA.Extensions;

namespace IA.MainForm.Controls
{
    /// <summary>
    /// Класс, реализующий элемент управления (Строка статуса)
    /// </summary>
    public partial class StatusControl : UserControl, Monitor.Status.Interface
    {
        /// <summary>
        /// Буфер обновления сообщений.
        /// </summary>
        private Dictionary<Monitor.Status.MessageType, string> buffer = new Dictionary<Monitor.Status.MessageType, string>();

        /// <summary>
        /// Объект класса, реализующий обновление информации по таймеру.
        /// </summary>
        private UpdateByTimer updateByTimer = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        public StatusControl()
        {
            InitializeComponent();
            //Инициализируем объект класса, реализующий обновление информации по таймеру
            updateByTimer = new UpdateByTimer(this.UpdateFromBuffer, 100);

            this.timer.Tick += delegate (object sender, EventArgs e)
            {
                //Отображаем последнее постоянное сообщение
                main_textBox.Text = buffer[Monitor.Status.MessageType.PERMANENT];
                main_textBox.Refresh();

                //Останавливаем таймер
                timer.Stop();
            };

            //Заполнение буфера обновления сообщений
            EnumLoop<Monitor.Status.MessageType>.ForEach(type => { buffer.Add(type, String.Empty); });
        }

        /// <summary>
        /// Отобразить сообщение в строке статуса.
        /// </summary>
        /// <param name="type">Тип сообщения.</param>
        /// <param name="text">Текст сообщения.</param>
        public void ShowMessage(Monitor.Status.MessageType type, string text)
        {
            updateByTimer.AddToBuffer(() => AddToBuffer(type, String.IsNullOrWhiteSpace(text) ? String.Empty : text));
        }

        /// <summary>
        /// Очистить строку статуса
        /// </summary>
        public void Clear()
        {
            //Отображаем пустое сообщение
            this.ShowMessage(Monitor.Status.MessageType.PERMANENT, String.Empty);
        }

        #region Работа с буфером обновления
        /// <summary>
        /// Добавление сообщения в буфер для последующего обновления.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="text"></param>
        private void AddToBuffer(Monitor.Status.MessageType type, string text)
        {
            //Задаём актуальное сообщение заданного типа
            buffer[type] = text;
        }

        /// <summary>
        /// Обновление сообщения из буфера.
        /// </summary>
        private UpdateByTimer.HasSomethingToUpdate UpdateFromBuffer()
        {
            //Если есть временное сообщение для отображения
            if (!String.IsNullOrWhiteSpace(buffer[Monitor.Status.MessageType.TEMPORARY]))
            {
                //Если таймер активен, останавливаем его
                if (timer.Enabled)
                    timer.Stop();

                //Отображаем временное сообщение
                main_textBox.Text = buffer[Monitor.Status.MessageType.TEMPORARY];
                main_textBox.Refresh();

                //Сбрасываем текст временного сообщения в буфере
                buffer[Monitor.Status.MessageType.TEMPORARY] = String.Empty;

                //Запускаем таймер
                timer.Start();

                return UpdateByTimer.HasSomethingToUpdate.NO;
            }

            //Если время отображения временного сообщения ещё не истекло, ничего не делаем
            if (timer.Enabled)
                return UpdateByTimer.HasSomethingToUpdate.NO;

            //Отображаем постоянное сообщение
            main_textBox.Text = buffer[Monitor.Status.MessageType.PERMANENT];
            main_textBox.Refresh();

            return UpdateByTimer.HasSomethingToUpdate.NO;
        }
        #endregion

    }
}
