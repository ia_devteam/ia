﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;

using IOController;

namespace IA.Controls
{
    /// <summary>
    /// Класс, описывающий элемент управления (Лог).
    /// </summary>
    public partial class LogControl : UserControl, Monitor.Log.Interface
    {
        #region Константы
        /// <summary>
        /// Имя экземпляра класса
        /// </summary>
        public const string ObjectName = "Лог";
        /// <summary>
        /// Максимальное число сообщений в логе
        /// </summary>
        private int MessagesCountMax = 500;

        /// <summary>
        /// Число сообщений сбрасываемых в дамп файл при переполнении лога
        /// </summary>
        private int MessagesCountToDump = 400;

        /// <summary>
        /// Имя файла, куда сбрасывается информация при переполнении лога
        /// </summary>
        private const string DumpFileName = "log.dump";

        /// <summary>
        /// Имя файла, куда сбрасывается информация по требования пользователя
        /// </summary>
        private const string LogFileName = "log.log";
        #endregion
        
        /// <summary>
        /// Буфер обновления лога.
        /// </summary>
        private List<Monitor.Log.Message> buffer = new List<Monitor.Log.Message>();

        /// <summary>
        /// Объект класса, реализующий обновление информации по таймеру.
        /// </summary>
        private UpdateByTimer updateByTimer = null;

        /// <summary>
        /// Список иконок для элемента управления
        /// </summary>
        ImageList icons = null;

        /// <summary>
        /// Путь до дамп файла, куда сбрасывается информация при переполнении Лога
        /// </summary>
        private string dumpFilePath = null;

        /// <summary>
        /// Количество сообщений от одного источника, после которого все новые будут тихо сбрасываться с файл без вывода на экран.
        /// </summary>
        private int MESSAGES_OVERFLOW_DUMP_THRESHOLD = 256;

        /// <summary>
        /// Словарь подсчёта сообщений от различных источников.
        /// </summary>
        Dictionary<Object, int> overflowMessagesThresholdDict = new Dictionary<object, int>();

        /// <summary>
        /// Словарь кеширования сообщений от различных источников.
        /// </summary>
        Dictionary<Object, List<Monitor.Log.Message>> overflowMessagesToDumpDict = new Dictionary<object, List<Monitor.Log.Message>>();

        /// <summary>
        /// Список источников, для которых уже было показано оповещение о сбрасывании избыточных сообщений в дамп.
        /// </summary>
        HashSet<object> overflowMessagesShowedList = new HashSet<object>();

        #region События и делегаты
        public delegate void LineClickedEventHandler(string line);

        /// <summary>
        /// Событие - Нажали на строчку лога
        /// </summary>
        public event LineClickedEventHandler LineClickedEvent;

        /// <summary>
        /// Делагат для события - Дважды нажали на строчку лога
        /// </summary>
        /// <param name="line"></param>
        public delegate void LineDoubleClickedEventHandler(string line);

        /// <summary>
        /// Событие - дважды нажали на строчку лога
        /// </summary>
        public event LineDoubleClickedEventHandler LineDoubleClickedEvent;
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        public LogControl()
        {
            InitializeComponent();
            //Инициализация объекта класса, реализующего обновление информации по таймеру
            updateByTimer = new UpdateByTimer(this.UpdateFromBuffer, 100);

            this.main_dataGridView.ContextMenuStrip = Menu_Initialization();

            //Формируем список иконок для сообщений Лога
            this.icons = new ImageList() { ColorDepth = ColorDepth.Depth32Bit, ImageSize = new System.Drawing.Size(16, 16) };

            //Присваиваем каждому типу сообщения Лога отдельную иконку
            EnumLoop<Monitor.Log.Message.enType>.ForEach(type =>
            {
                switch (type)
                {
                    case Monitor.Log.Message.enType.INFORMATION:
                        this.icons.Images.Add(type.FullName(), Properties.Resources.Log_Icon_Info);
                        break;
                    case Monitor.Log.Message.enType.WARNING:
                        this.icons.Images.Add(type.FullName(), Properties.Resources.Log_Icon_Warning);
                        break;
                    case Monitor.Log.Message.enType.ERROR:
                        this.icons.Images.Add(type.FullName(), Properties.Resources.Log_Icon_Error);
                        break;
                    default:
                        throw new Exception("Обнаружен неизвеснтый тип сообщения в Логе.");
                }
            });

            //Добавляем иконку для столбца "Отобразить сообщение полностью"
            this.icons.Images.Add("MakeOut", Properties.Resources.Log_Icon_Examine);

            //Что необходимо сделать после инициализации элемента управления
            this.Load += delegate (object sender, EventArgs e)
            {
                //Включаем двойную буферизацию для отключения мерцания при обновлении
                ControlExtentions.EnableDoubleBuffer(main_dataGridView);
            };

            //Создаём колонки
            DataGridViewImageColumn columnIcon = new DataGridViewImageColumn()
            {
                Name = "Icon",
                HeaderText = "Icon",
                Width = 25,
            };
            columnIcon.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewTextBoxColumn columnTime = new DataGridViewTextBoxColumn()
            {
                Name = "Time",
                HeaderText = "Time",
                ReadOnly = true,
                Width = 55,
            };
            columnTime.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewTextBoxColumn columnText = new DataGridViewTextBoxColumn()
            {
                Name = "Text",
                HeaderText = "Text",
                ReadOnly = true,
            };
            columnText.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            DataGridViewTextBoxColumn columnSender = new DataGridViewTextBoxColumn()
            {
                Name = "Sender",
                HeaderText = "Sender",
                ReadOnly = true,
            };
            columnSender.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridViewImageColumn columnMakeOut = new DataGridViewImageColumn()
            {
                Name = "MakeOut",
                HeaderText = "MakeOut",
                Width = 25,
            };
            columnMakeOut.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            //Добавляем созданные колонки
            main_dataGridView.Columns.AddRange(new DataGridViewColumn[]
                {
                    columnIcon,
                    columnTime,
                    columnText,
                    columnSender,
                    columnMakeOut
                });

            //Добавляем обработчики
            main_dataGridView.CellClick += delegate (object sender, DataGridViewCellEventArgs e)
            {
                if (e.ColumnIndex == 4)
                    this.ShowMessageByMessageBox(main_dataGridView.Rows[e.RowIndex].Tag as Monitor.Log.Message);
            };

            int offset = SystemInformation.VerticalScrollBarWidth + columnIcon.Width + columnTime.Width + columnMakeOut.Width;
            main_dataGridView.SizeChanged += delegate (object o, EventArgs e)
            {
                main_dataGridView.Columns["Text"].Width = (int)((main_dataGridView.Width - offset) * 0.7);
                main_dataGridView.Columns["Sender"].Width = (int)((main_dataGridView.Width - offset) * 0.3);
            };

            main_dataGridView.Click += (sender, e) => this.Log_Item_Click();
            main_dataGridView.DoubleClick += (sender, e) => this.Log_Item_DoubleClick();

            //Формируем основной групбокс для Лога
            

            #region Работа с дамп файлом
            //Формируем путь до дамп файла
            this.dumpFilePath = Path.Combine(Application.UserAppDataPath, DumpFileName);

            //Удаляем дамп файл, если существовал ранее
            if (FileController.IsExists(this.dumpFilePath))
                FileController.Remove(this.dumpFilePath);
            #endregion
        }

        /// <summary>
        /// Добавить сообщение в Лог
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void AddMessage(Monitor.Log.Message message)
        {
            var key = message.Sender;
            // логика отлова вала сообщений от одного источника. При превышении порога - дамп в файл.
            if (!overflowMessagesThresholdDict.ContainsKey(key))
                overflowMessagesThresholdDict.Add(key, 0);
            overflowMessagesThresholdDict[key] += 1;

            // при превышении количества сообщений от одного источника (всех типов)
            if (overflowMessagesThresholdDict[key] > MESSAGES_OVERFLOW_DUMP_THRESHOLD)
            {
                if (!overflowMessagesToDumpDict.ContainsKey(key))
                    overflowMessagesToDumpDict.Add(key, new List<Monitor.Log.Message>());
                overflowMessagesToDumpDict[key].Add(message);

                // если достигнут предел кеширования - сбрасываем в лог кеш
                if (overflowMessagesToDumpDict[key].Count > MessagesCountMax)
                    DumpOverflow(key);

                return;
            }

            //Если требуется дополнительное оповещение пользователя
            if (message.IsNotificationNeeded)
                this.ShowMessageByMessageBox(message);

            updateByTimer.AddToBuffer(() => this.AddToBuffer(message));
        }

        /// <summary>
        /// Сброс указанного количества первых сообщений лога в указанный файл
        /// </summary>
        /// <param name="count">Количество первых сообщений лога</param>
        private void Dump(int count)
        {
            //Если количество сообщений лога равно нулю, ничего не делаем
            if (count == 0)
                return;

            //Проверка на разумность
            if (count > main_dataGridView.RowCount)
                throw new Exception("Количество сообщений лога для сброса в дамп файл не может превышать количество сообщений в логе.");

            //Создаём временный массив для хранения сообщений лога
            string[] contents = new string[count];

            for (int i = 0; i < count; i++)
            {
                //Запоминаем сообщение лога
                contents[i] = ((Monitor.Log.Message)(main_dataGridView.Rows[0].Tag)).ToString();

                //Удаляем сообщение лога
                main_dataGridView.Rows.RemoveAt(0);
            }

            //Записываем все сообщения в дамп файл
            File.AppendAllLines(this.dumpFilePath, contents);
        }

        /// <summary>
        /// Сдампить в файл все сообщения от данного источника из словаря "перелива" сообщений. Кеш для данного ключа очищается.
        /// </summary>
        /// <param name="key">Объект - источник.</param>
        private void DumpOverflow(object key)
        {
            if (!overflowMessagesToDumpDict.ContainsKey(key))
                return;

            string dumpFullFilePath = DumpOverflowFileName(key);

            File.AppendAllLines(dumpFullFilePath, overflowMessagesToDumpDict[key].Select(m => m.ToString()).ToArray());

            overflowMessagesToDumpDict[key].Clear();

            if (!overflowMessagesShowedList.Contains(key))
            {
                var message = new Monitor.Log.Message(key.ToString(), Monitor.Log.Message.enType.WARNING, "Сообщения от <" + key.ToString() + "> из-за большого количества сдамплены в файл <" + dumpFullFilePath + ">.");

                updateByTimer.AddToBuffer(() => this.AddToBuffer(message));

                overflowMessagesShowedList.Add(key);
            }
        }

        /// <summary>
        /// Сброс в файлы всего содержимого кешей переполнения и их очистка.
        /// </summary>
        private void DumpOverflowWithCleaning(string dumpFileDirectory = null)
        {
            //если есть чего сбрасывать из кеша переполнения - сбрасываем
            foreach (var key in overflowMessagesToDumpDict.Keys)
            {
                DumpOverflow(key);
            }

            if (!String.IsNullOrWhiteSpace(dumpFileDirectory) && DirectoryController.IsExists(dumpFileDirectory))
            {
                foreach (var key in overflowMessagesToDumpDict.Keys)
                {
                    string currentDumpFile = DumpOverflowFileName(key);
                    if (FileController.IsExists(currentDumpFile))
                    {
                        WaitWindow.Show(
                            delegate (WaitWindow window)
                            {
                                //Копируем дамп файл
                                FileController.Copy(currentDumpFile, Path.Combine(dumpFileDirectory, Path.GetFileName(currentDumpFile)));
                            },
                            "Копирование ранее сохранённых сообщений лога"
                        );
                    }
                }
            }

            overflowMessagesThresholdDict.Clear();
            overflowMessagesToDumpDict.Clear();
        }

        private string DumpOverflowFileName(object key)
        {
            return this.dumpFilePath + "._" + key.ToString() + "_.log";
        }

        /// <summary>
        /// Обработчик - Элемент Лога - Нажатие
        /// </summary>
        private void Log_Item_Click()
        {
            try
            {
                //Проверка на разумность
                if (main_dataGridView.SelectedRows == null || main_dataGridView.SelectedRows.Count != 1)
                    return;

                //Получаем сообщение лога
                Monitor.Log.Message message = main_dataGridView.SelectedRows[0].Tag as Monitor.Log.Message;

                //Формируем событие
                if (LineClickedEvent != null)
                    LineClickedEvent(message.Text);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Однократное нажатие на строку лога", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Элемент Лога - Двойное нажатие
        /// </summary>
        private void Log_Item_DoubleClick()
        {
            try
            {
                //Проверка на разумность
                if (main_dataGridView.SelectedRows == null || main_dataGridView.SelectedRows.Count != 1)
                    return;

                //Получаем сообщение лога
                Monitor.Log.Message message = main_dataGridView.SelectedRows[0].Tag as Monitor.Log.Message;

                //Формируем событие
                if (LineDoubleClickedEvent != null)
                    LineDoubleClickedEvent(message.Text);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Двукратное нажатие на строку лога", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Отобразить сообщение пользователю в виде MessageBox'a
        /// </summary>
        /// <param name="message">Сообщение</param>
        private void ShowMessageByMessageBox(Monitor.Log.Message message)
        {
            switch (message.Type)
            {
                case Monitor.Log.Message.enType.INFORMATION:
                    MessageBox.Show(message.Text, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Monitor.Log.Message.enType.WARNING:
                    MessageBox.Show(message.Text, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                case Monitor.Log.Message.enType.ERROR:
                    MessageBox.Show(message.Text, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        #region Контекстное меню
        /// <summary>
        /// Сформировать контекстное меню
        /// </summary>
        /// <returns></returns>
        private ContextMenuStrip Menu_Initialization()
        {
            ContextMenuStrip ret = new ContextMenuStrip();

            ToolStripMenuItem item = null;

            item = new ToolStripMenuItem()
            {
                Name = "Log.Clear",
                Text = "Очистить"
            };
            item.Click += (sender, e) => this.Menu_Log_Clear_EventHandler();
            ret.Items.Add(item);

            item = new ToolStripMenuItem()
            {
                Name = "Log.Save",
                Text = "Сохранить в файл"
            };
            item.Click += (sender, e) => this.Menu_Log_Save_EventHandler();
            ret.Items.Add(item);

            return ret;
        }

        /// <summary>
        /// Обработчик - Меню - Очистить лог
        /// </summary>
        private void Menu_Log_Clear_EventHandler()
        {
            //Если лог пуст, ничего не делаем
            if (main_dataGridView.RowCount == 0)
                return;

            //Сбрасываем все сообщения в логе в дамп файл
            this.Dump(main_dataGridView.RowCount);

            this.DumpOverflowWithCleaning();
        }

        /// <summary>
        /// Обработчик - Меню - Сохранить лог в файл
        /// </summary>
        private void Menu_Log_Save_EventHandler()
        {
            //Получаем директорию файла с содержимым лога
            string logDirectoryPath = DirectoryController.SelectByUser(enableCreateFolder: true);

            //Если директория не задана, ничего не делаем
            if (logDirectoryPath == null)
                return;

            //Формируем имя файла с содержимым лога
            string logFilePath = Path.Combine(logDirectoryPath, LogFileName);

            //если есть чего сбрасывать из кеша переполнения - сбрасываем
            this.DumpOverflowWithCleaning(logFilePath);

            //Если есть дамп файл
            if (FileController.IsExists(this.dumpFilePath))
                //Отображаем окно ожидания
                WaitWindow.Show(
                    delegate (WaitWindow window)
                    {
                        //Копируем дамп файл
                        FileController.Copy(this.dumpFilePath, logFilePath);
                    },
                    "Копирование ранее сохранённых сообщений лога"
                );

            //Преобразуем сообщения лога в массив строк
            string[] contents = new string[main_dataGridView.RowCount];

            for (int i = 0; i < main_dataGridView.RowCount; i++)
                contents[i] = ((Monitor.Log.Message)(main_dataGridView.Rows[i].Tag)).ToString();

            //Записываем массив строк в файл лога
            File.AppendAllLines(logFilePath, contents.ToArray());

            //Отображаем сообщение об успехе
            Monitor.Log.Information(ObjectName, "Лог успешно сохранён в директорию <" + logDirectoryPath + ">.", true);
        }
        #endregion

        #region Работа с буфером обновления

        /// <summary>
        /// Добавление сообщения в буфер обновления.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        private void AddToBuffer(Monitor.Log.Message message)
        {
            //Добавляем сообщение в буфер обновления
            buffer.Add(message);
        }

        /// <summary>
        /// Обновление лога из буфера обновления
        /// </summary>
        private UpdateByTimer.HasSomethingToUpdate UpdateFromBuffer()
        {
            //Проходимся по всем сообщениям в буфере
            foreach (Monitor.Log.Message message in buffer)
            {
                DataGridViewRow row = new DataGridViewRow()
                {
                    Tag = message
                };

                row.CreateCells(main_dataGridView);

                row.Cells[0].Value = this.icons.Images[message.Type.FullName()];
                row.Cells[1].Value = message.DateTime.TimeOfDay.ToString(@"hh\:mm\:ss");
                row.Cells[2].Value = message.Text.Replace("\n", " ");
                row.Cells[3].Value = message.Sender;
                row.Cells[4].Value = this.icons.Images["MakeOut"];

                //Добавляем новый элемент лога
                main_dataGridView.Rows.Add(row);

                //Если лог переполнился
                if (main_dataGridView.Rows.Count > MessagesCountMax)
                    //Сбрасываем часть лога в дамп файл
                    this.Dump(MessagesCountToDump);
            }

            //Автоматически перемещаемся в конец лога
            if (main_dataGridView.RowCount != 0)
                main_dataGridView.FirstDisplayedScrollingRowIndex = main_dataGridView.Rows.Count - 1;

            //Если буфер не пуст, очищаем его
            if (buffer.Count != 0)
                buffer.Clear();

            //После каждого обновления буфер очищается, дополнительного обновления не требуется
            return UpdateByTimer.HasSomethingToUpdate.NO;
        }

        #endregion
    }
}
