﻿using System.Collections.Generic;

namespace IA.Controls.MainSettings
{
    partial class MainSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.projectsServerSettingsSubControl = new IA.Controls.MainSettings.SubControls.ProjectsServerSettingsSubControl();
            this.otherSettingsControl = new IA.Controls.MainSettings.SubControls.OtherSettingsSubControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.sqlServerSettingsSubControl = new IA.Controls.MainSettings.SubControls.SqlServerSettingsSubControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.LoadFromFileButton = new System.Windows.Forms.Button();
            this.SaveToFileButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.LoadFromFileButton, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.SaveToFileButton, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.MaximumSize = new System.Drawing.Size(700, 420);
            this.tableLayoutPanel1.MinimumSize = new System.Drawing.Size(700, 420);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(700, 420);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBox1, 3);
            this.groupBox1.Controls.Add(this.projectsServerSettingsSubControl);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(694, 134);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = this.otherSettingsControl.Text;
            // 
            // projectsServerSettingsSubControl
            // 
            this.projectsServerSettingsSubControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectsServerSettingsSubControl.Location = new System.Drawing.Point(3, 16);
            this.projectsServerSettingsSubControl.Name = "projectsServerSettingsSubControl";
            this.projectsServerSettingsSubControl.Size = new System.Drawing.Size(688, 115);
            this.projectsServerSettingsSubControl.TabIndex = 0;
            // 
            // otherSettingsControl
            // 
            this.otherSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.otherSettingsControl.Location = new System.Drawing.Point(3, 16);
            this.otherSettingsControl.Name = "otherSettingsControl";
            this.otherSettingsControl.Size = new System.Drawing.Size(688, 31);
            this.otherSettingsControl.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBox2, 3);
            this.groupBox2.Controls.Add(this.sqlServerSettingsSubControl);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 143);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(694, 154);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // sqlServerSettingsSubControl
            // 
            this.sqlServerSettingsSubControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sqlServerSettingsSubControl.Location = new System.Drawing.Point(3, 16);
            this.sqlServerSettingsSubControl.Name = "sqlServerSettingsSubControl";
            this.sqlServerSettingsSubControl.Size = new System.Drawing.Size(688, 135);
            this.sqlServerSettingsSubControl.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBox3, 3);
            this.groupBox3.Controls.Add(this.otherSettingsControl);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 303);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(694, 50);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            // 
            // LoadFromFileButton
            // 
            this.LoadFromFileButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LoadFromFileButton.Location = new System.Drawing.Point(3, 359);
            this.LoadFromFileButton.Name = "LoadFromFileButton";
            this.LoadFromFileButton.Size = new System.Drawing.Size(144, 25);
            this.LoadFromFileButton.TabIndex = 6;
            this.LoadFromFileButton.Text = "Загрузить из файла";
            this.LoadFromFileButton.UseVisualStyleBackColor = true;
            this.LoadFromFileButton.Click += new System.EventHandler(this.LoadFromFileButton_Click);
            // 
            // SaveToFileButton
            // 
            this.SaveToFileButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SaveToFileButton.Location = new System.Drawing.Point(153, 359);
            this.SaveToFileButton.Name = "SaveToFileButton";
            this.SaveToFileButton.Size = new System.Drawing.Size(144, 25);
            this.SaveToFileButton.TabIndex = 7;
            this.SaveToFileButton.Text = "Сохранить в файл";
            this.SaveToFileButton.UseVisualStyleBackColor = true;
            this.SaveToFileButton.Click += new System.EventHandler(this.SaveToFileButton_Click);
            // 
            // MainSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainSettingsControl";
            this.Size = new System.Drawing.Size(693, 405);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private SubControls.ProjectsServerSettingsSubControl projectsServerSettingsSubControl;
        private SubControls.SqlServerSettingsSubControl sqlServerSettingsSubControl;
        private SubControls.OtherSettingsSubControl otherSettingsControl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button LoadFromFileButton;
        private System.Windows.Forms.Button SaveToFileButton;
    }
}
