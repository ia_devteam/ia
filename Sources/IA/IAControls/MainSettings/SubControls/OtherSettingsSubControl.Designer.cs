﻿namespace IA.Controls.MainSettings.SubControls
{
    partial class OtherSettingsSubControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.otherSettingsControl = new IA.Controls.OtherSettingsControl();
            this.SuspendLayout();
            // 
            // otherSettingsControl1
            // 
            this.otherSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.otherSettingsControl.Location = new System.Drawing.Point(0, 0);
            this.otherSettingsControl.Name = "otherSettingsControl1";
            this.otherSettingsControl.Size = new System.Drawing.Size(595, 36);
            this.otherSettingsControl.TabIndex = 0;
            // 
            // OtherSettingsSubControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.otherSettingsControl);
            this.Name = "OtherSettingsSubControl";
            this.Size = new System.Drawing.Size(595, 36);
            this.ResumeLayout(false);
            this.Text = "Другие";
        }

        #endregion

        public OtherSettingsControl otherSettingsControl;
    }
}
