﻿using System.Data;
using System.Linq;
using System.Windows.Forms;
using IA.Sql;
using System.Collections.Generic;

namespace IA.Controls.MainSettings.SubControls
{
    /// <summary>
    /// Класс реализующий дочерний элемент основных настроек приложения - Настройки SQL сервера
    /// </summary>
    public partial class SqlServerSettingsSubControl : UserControl, IMainSettingsSubControl
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public SqlServerSettingsSubControl()
        {
            InitializeComponent();

            ((Control)this).Text = "SQL Сервер";

            sqlServerConnectionControl.IsWindowsAuthentication = Settings.SQLServer.IsWindowsAuthentication;
            sqlServerConnectionControl.ServerName = Settings.SQLServer.Name;
            sqlServerConnectionControl.ServerLogin = Settings.SQLServer.Login;
            sqlServerConnectionControl.ServerPassword = Settings.SQLServer.Password;

            chkOfflineMode.Checked = false;
            chkOfflineMode.CheckedChanged += ChkOfflineMode_CheckedChanged;
            chkOfflineMode.Checked = Settings.AutonomyMode;            
        }

        /// <summary>
        /// Проверка настроек
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        public bool Check(out string message)
        {
            //Изначально результат проверки положительный
            bool ret = true;
            string ret_message = "Соединение с SQL сервером прошло успешно.";

            if (Settings.AutonomyMode)
            {
                message = ret_message;
                return ret;
            }

            //Сохраняем существующие настройки 
            string savedSqlServerConnectionName = IA.Sql.ServerConfiguration.Name;
            string savedSqlServerConnectionLogin = IA.Sql.ServerConfiguration.Login;
            string savedSqlServerConnectionPassword = IA.Sql.ServerConfiguration.Password;
            bool savedSqlServerConnectionIsWindowsAuthentication = IA.Sql.ServerConfiguration.IsWindowsAuthentication;

            //Задаём настройки для проверки подключения
            Sql.ServerConfiguration.Set(sqlServerConnectionControl.ServerName,
                                            sqlServerConnectionControl.ServerLogin,
                                                sqlServerConnectionControl.ServerPassword,
                                                    sqlServerConnectionControl.IsWindowsAuthentication);

            List<SQLConnection> dbases = new List<SQLConnection>
            {
                SqlConnector.SignatureDB,
                SqlConnector.IADB,
#if !CERTIFIED            
                SqlConnector.DllKnowledgeDB
#endif
            };

            Sql.SqlExtensions.TestDatabaseConnectionsWithWaitWindow(
                dbases.ToArray(),
                delegate (Sql.DatabaseConnection[] databaseConnections)
                {
                    //Формируем сообщение об ошибке   
                    ret_message = "Отсутствует соединение со следующими базами данных:";

                    foreach (string databaseName in databaseConnections.Select(db => db.DatabaseName))
                        ret_message += "\n- " + databaseName;

                    ret = false;
                }
            );
            
            Sql.ServerConfiguration.Set(savedSqlServerConnectionName,
                                            savedSqlServerConnectionLogin,
                                                savedSqlServerConnectionPassword,
                                                    savedSqlServerConnectionIsWindowsAuthentication);

            //Задаём полученное сообщение
            message = ret_message;

            return ret;
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            if (Settings.AutonomyMode)
            {
                SqlConnector.PrepareOffline();                
                return;
            }
            SqlConnector.GoOnline();
            //Настройки настройки сервера MS SQL
            Settings.SQLServer.Set(sqlServerConnectionControl.ServerName,
                                        sqlServerConnectionControl.ServerLogin,
                                            sqlServerConnectionControl.ServerPassword,
                                                sqlServerConnectionControl.IsWindowsAuthentication);            
        }

        private void ChkOfflineMode_CheckedChanged(object sender, System.EventArgs e)
        {
            Settings.AutonomyMode = chkOfflineMode.Checked;
            sqlServerConnectionControl.Enabled = !chkOfflineMode.Checked;
        }
    }
}
