﻿using System;
using System.Windows.Forms;

using Store;
using System.IO;

namespace IA.Controls.MainSettings.SubControls
{
    /// <summary>
    /// Класс реализующий дочерний элемент основных настроек приложения - Другие настройки приложения
    /// </summary>
    public partial class OtherSettingsSubControl : UserControl, IMainSettingsSubControl
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public OtherSettingsSubControl()
        {
            InitializeComponent();
            otherSettingsControl.WorkDirectoryPath = IA.Settings.CommonWorkDirectoryPath;
        }

        /// <summary>
        /// Проверка настроек
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        public bool Check(out string message)
        {
            message = String.Empty;

            if (!Directory.Exists(otherSettingsControl.WorkDirectoryPath))
                Directory.CreateDirectory(otherSettingsControl.WorkDirectoryPath);
            bool ret = WorkDirectory.IsValidPath(otherSettingsControl.WorkDirectoryPath, out message);

            if (!ret)
                message = "Ошибка в пути до рабочей директории приложения. " + message;

            return ret;
        }

        /// <summary>
        /// Сохраненеие настроек
        /// </summary>
        public void Save()
        {
            IA.Settings.CommonWorkDirectoryPath = otherSettingsControl.WorkDirectoryPath;
        }
    }
}
