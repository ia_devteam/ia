﻿namespace IA.Controls.MainSettings.SubControls
{
    partial class SqlServerSettingsSubControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sqlServerConnectionControl = new IA.Sql.Controls.SqlServerConnectionControl();
            this.chkOfflineMode = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // sqlServerConnectionControl
            // 
            this.sqlServerConnectionControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.sqlServerConnectionControl.IsWindowsAuthentication = true;
            this.sqlServerConnectionControl.Location = new System.Drawing.Point(0, 0);
            this.sqlServerConnectionControl.Name = "sqlServerConnectionControl";
            this.sqlServerConnectionControl.ServerLogin = "";
            this.sqlServerConnectionControl.ServerName = "SqlServerConnectionControl";
            this.sqlServerConnectionControl.ServerPassword = "";
            this.sqlServerConnectionControl.Size = new System.Drawing.Size(555, 124);
            this.sqlServerConnectionControl.TabIndex = 0;
            // 
            // chkOfflineMode
            // 
            this.chkOfflineMode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkOfflineMode.AutoSize = true;
            this.chkOfflineMode.Location = new System.Drawing.Point(151, 130);
            this.chkOfflineMode.Name = "chkOfflineMode";
            this.chkOfflineMode.Size = new System.Drawing.Size(180, 17);
            this.chkOfflineMode.TabIndex = 1;
            this.chkOfflineMode.Text = "Работа в автономном режиме";
            this.chkOfflineMode.UseVisualStyleBackColor = true;
            // 
            // SqlServerSettingsSubControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkOfflineMode);
            this.Controls.Add(this.sqlServerConnectionControl);
            this.Name = "SqlServerSettingsSubControl";
            this.Size = new System.Drawing.Size(555, 160);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Sql.Controls.SqlServerConnectionControl sqlServerConnectionControl;
        private System.Windows.Forms.CheckBox chkOfflineMode;
    }
}
