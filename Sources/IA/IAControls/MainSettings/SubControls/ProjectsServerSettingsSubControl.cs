﻿using System;
using System.Windows.Forms;

using IA.Extensions;

using IOController;

namespace IA.Controls.MainSettings.SubControls
{
    /// <summary>
    /// Класс реализующий дочерний элемент основных настроек приложения - Настройки сервера проектов
    /// </summary>
    public partial class ProjectsServerSettingsSubControl : UserControl, IMainSettingsSubControl
    {

        /// <summary>
        /// Конструктор
        /// </summary>
        public ProjectsServerSettingsSubControl()
        {
            InitializeComponent();

            ((Control)this).Text = "Сервер проектов";

            projectsServerSettingsControl.Name = Settings.ProjectsServer.Name;
            projectsServerSettingsControl.Login = Settings.ProjectsServer.Login;
            projectsServerSettingsControl.Password = Settings.ProjectsServer.Password;
            projectsServerSettingsControl.ProjectsDirectoryPath = Settings.ProjectsServer.ProjectsDirectoryPath;
        }

        /// <summary>
        /// Проверка настроек
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        public bool Check(out string message)
        {
            message = "Проверка соединения с сервером проектов прошла успешно.";
            if (Settings.AutonomyMode)
                return true;

            //Формируем ресурс для подключения
            Network.NetworkResource projectsServerNetworkResource = new Network.NetworkResource(projectsServerSettingsControl.Name,
                                                                                projectsServerSettingsControl.Login,
                                                                                    projectsServerSettingsControl.Password);

            //Проверяем соединение с сервером проектов и существование директории проектов
            try
            {
                using (Network.NetworkConnection connection = 
                    new Network.NetworkConnection(
                        projectsServerNetworkResource,
                        true,
                        "Проверка соединения с сервером проектов <" + projectsServerNetworkResource.Name + ">"
                    )
                )
                {
                    //Проверяем существование директории проектов
                    if (!DirectoryController.IsExists(projectsServerSettingsControl.ProjectsDirectoryPath))
                    {
                        message = "Директория проектов <" + projectsServerSettingsControl.ProjectsDirectoryPath + "> не существует.";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                message = (new string[]
                {
                    "Отсутствует соединение с сервером проектов <" + projectsServerNetworkResource.Name + ">.",
                    ex.ToFullMultiLineMessage()
                }).ToMultiLineString();
                
                return false;
            }

            return true;
        }

        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {
            //Сохраняем настройки сервера
            Settings.ProjectsServer.Set(projectsServerSettingsControl.Name,
                                                projectsServerSettingsControl.Login,
                                                    projectsServerSettingsControl.Password,
                                                        projectsServerSettingsControl.ProjectsDirectoryPath);
        }
    }
}
