﻿/*
 * Главный интерфейс
 * Файл IASettings.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Юхтин
 * 
 * Форма общих настроек IA
 */

using IA.Sql.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace IA.Controls.MainSettings
{
    /// <summary>
    /// Форма общих настроек IA
    /// </summary>
    public partial class MainSettingsControl : UserControl
    {
        /// <summary>
        /// Всевозможные режимы отображения настроек приложения
        /// </summary>
        public enum enMode
        {
            /// <summary>
            /// Режим неизвестен
            /// </summary>
            UNKNOWN, //WHAT?!?!?!?!?!
            /// <summary>
            /// В виде таблицы
            /// </summary>
            TABLE,
            /// <summary>
            /// В виде закладок
            /// </summary>
            TABS
        }

        /// <summary>
        /// Список дочерних элементов управления с настройками. Не может быть Null.
        /// </summary>
        private List<IMainSettingsSubControl> subControls = null;

        [Flags]
        public enum SettingsElements
        {
            projectsServerSettingsSubControl = 1 << 0,
            sqlServerSettingsSubControl = 1 << 1,
            otherSettingsControl = 1 << 2
        }
        
        /// <summary>
        /// Конструктор класса
        /// </summary>
        public MainSettingsControl(SettingsElements SubControls)
        {
            InitializeComponent();

            //Задаём режим отображения настроек по умолчанию
            //this.Mode = enMode.TABLE;
            this.subControls = new List<IMainSettingsSubControl>();
            if ((SubControls & SettingsElements.projectsServerSettingsSubControl) == SettingsElements.projectsServerSettingsSubControl)
            {
                subControls.Add(projectsServerSettingsSubControl);
            }
            else
            {
                tableLayoutPanel1.RowStyles[0].Height = 0;
            }
            if ((SubControls & SettingsElements.sqlServerSettingsSubControl) == SettingsElements.sqlServerSettingsSubControl)
            {
                subControls.Add(sqlServerSettingsSubControl);
            }
            else
            { 
                tableLayoutPanel1.RowStyles[1].Height = 0;
            }
                if ((SubControls & SettingsElements.otherSettingsControl) == SettingsElements.otherSettingsControl)
            {
                subControls.Add(otherSettingsControl);
            }
            else
            {
                tableLayoutPanel1.RowStyles[2].Height = 0;
            }
        }

        /// <summary>
        /// Сохранить указанные настройки
        /// </summary>
        public void Save()
        {
            //Сохраняем настройки во всех дочерних элементах управления
            this.subControls.ForEach(sc => sc.Save());

            //Сохраняем настройки по умолчанию
            Settings.SaveAll();
        }


        /// <summary>
        /// Проверка указанных настроек
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns>Корректно ли введены настройки?</returns>
        public bool Check(out string message)
        {
            //Проверяем настройки во всех дочерних элементах управления
            foreach(IMainSettingsSubControl subControl in this.subControls)
                if (!subControl.Check(out message))
                    return false;

            message = "Проверка введённых настроек выполнена успешно!";

            return true;
        }

        private void LoadFromFileButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.Filter = "XML-документ | *.xml";
                if (fd.ShowDialog() == DialogResult.OK && File.Exists(fd.FileName))
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(fd.FileName);
                    projectsServerSettingsSubControl.projectsServerSettingsControl.Name = xdoc.GetElementsByTagName("projectsServerSettingsControl.Name")[0].InnerText;
                    projectsServerSettingsSubControl.projectsServerSettingsControl.Login = xdoc.GetElementsByTagName("projectsServerSettingsControl.Login")[0].InnerText;
                    projectsServerSettingsSubControl.projectsServerSettingsControl.Password = xdoc.GetElementsByTagName("projectsServerSettingsControl.Password")[0].InnerText;
                    projectsServerSettingsSubControl.projectsServerSettingsControl.ProjectsDirectoryPath = xdoc.GetElementsByTagName("projectsServerSettingsControl.ProjectsDirectoryPath")[0].InnerText;

                    bool res;
                    bool.TryParse(xdoc.GetElementsByTagName("SqlServerConnectionControl.IsWindowsAuthentication")[0].InnerText, out res);
                    sqlServerSettingsSubControl.sqlServerConnectionControl.IsWindowsAuthentication = res;
                    sqlServerSettingsSubControl.sqlServerConnectionControl.ServerName = xdoc.GetElementsByTagName("SqlServerConnectionControl.ServerName")[0].InnerText;
                    sqlServerSettingsSubControl.sqlServerConnectionControl.ServerLogin = xdoc.GetElementsByTagName("SqlServerConnectionControl.ServerLogin")[0].InnerText;
                    sqlServerSettingsSubControl.sqlServerConnectionControl.ServerPassword = xdoc.GetElementsByTagName("SqlServerConnectionControl.ServerPassword")[0].InnerText;
                    bool.TryParse(xdoc.GetElementsByTagName("SqlServerConnectionControl.Autonomy")[0].InnerText, out res);
                    Settings.AutonomyMode = res;

                    otherSettingsControl.otherSettingsControl.WorkDirectoryPath = xdoc.GetElementsByTagName("otherSettingsControl.WorkDirectoryPath")[0].InnerText;
                }
            }
            catch(Exception)
            {
                Monitor.Log.Error("Загрузка настроек", "Не удалось загрузить настройки", true);
            }
        }

        private void SaveToFileButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.Filter = "XML-документ | *.xml";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                string pathToXml = fd.FileName;
                XmlTextWriter textWritter = new XmlTextWriter(pathToXml, Encoding.UTF8);
                textWritter.WriteStartDocument();
                textWritter.WriteStartElement("settings");
                textWritter.WriteEndElement();
                textWritter.Close();


                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(pathToXml);

                XmlNode elem = xdoc.CreateElement("projectsServerSettingsControl");
                xdoc.DocumentElement.AppendChild(elem); 

                XmlElement subelem = xdoc.CreateElement("projectsServerSettingsControl.Name");
                subelem.InnerText = projectsServerSettingsSubControl.projectsServerSettingsControl.Name;
                elem.AppendChild(subelem);
                subelem = xdoc.CreateElement("projectsServerSettingsControl.Login");
                subelem.InnerText = projectsServerSettingsSubControl.projectsServerSettingsControl.Login;
                elem.AppendChild(subelem);
                subelem = xdoc.CreateElement("projectsServerSettingsControl.Password");
                subelem.InnerText = projectsServerSettingsSubControl.projectsServerSettingsControl.Password;
                elem.AppendChild(subelem);
                subelem = xdoc.CreateElement("projectsServerSettingsControl.ProjectsDirectoryPath");
                subelem.InnerText = projectsServerSettingsSubControl.projectsServerSettingsControl.ProjectsDirectoryPath;
                elem.AppendChild(subelem);


                elem = xdoc.CreateElement("SqlServerConnectionControl");
                xdoc.DocumentElement.AppendChild(elem);

                subelem = xdoc.CreateElement("SqlServerConnectionControl.IsWindowsAuthentication");
                subelem.InnerText = sqlServerSettingsSubControl.sqlServerConnectionControl.IsWindowsAuthentication.ToString();
                elem.AppendChild(subelem);
                subelem = xdoc.CreateElement("SqlServerConnectionControl.ServerName");
                subelem.InnerText = sqlServerSettingsSubControl.sqlServerConnectionControl.ServerName;
                elem.AppendChild(subelem);
                subelem = xdoc.CreateElement("SqlServerConnectionControl.ServerLogin");
                subelem.InnerText = sqlServerSettingsSubControl.sqlServerConnectionControl.ServerLogin;
                elem.AppendChild(subelem);
                subelem = xdoc.CreateElement("SqlServerConnectionControl.ServerPassword");
                subelem.InnerText = sqlServerSettingsSubControl.sqlServerConnectionControl.ServerPassword;
                elem.AppendChild(subelem);
                subelem = xdoc.CreateElement("SqlServerConnectionControl.Autonomy");
                subelem.InnerText = Settings.AutonomyMode.ToString();
                elem.AppendChild(subelem);

                elem = xdoc.CreateElement("otherSettingsControl");
                xdoc.DocumentElement.AppendChild(elem);

                subelem = xdoc.CreateElement("otherSettingsControl.WorkDirectoryPath");
                subelem.InnerText = otherSettingsControl.otherSettingsControl.WorkDirectoryPath;
                elem.AppendChild(subelem);

                xdoc.Save(pathToXml);
            }
        }
    }
}
