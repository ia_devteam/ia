﻿namespace IA.Controls.MainSettings
{
    /// <summary>
    /// Интерфейс вспомогательного элемента управления в рамках основных настроек приложения
    /// </summary>
    public interface IMainSettingsSubControl
    {
        /// <summary>
        /// Проверка корректности введённых настроек
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        bool Check(out string message);

        /// <summary>
        /// Сохранение введённых настроек
        /// </summary>
        void Save();
    }
}
