﻿namespace IA.Controls
{
    partial class PluginListControl
    {
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_groupBox = new System.Windows.Forms.GroupBox();
            this.main_splitContainer = new System.Windows.Forms.SplitContainer();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.disable_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.disable_image_label = new System.Windows.Forms.Label();
            this.disable_text_label = new System.Windows.Forms.Label();
            this.main_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).BeginInit();
            this.main_splitContainer.Panel1.SuspendLayout();
            this.main_splitContainer.Panel2.SuspendLayout();
            this.main_splitContainer.SuspendLayout();
            this.disable_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_groupBox
            // 
            this.main_groupBox.Controls.Add(this.main_splitContainer);
            this.main_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_groupBox.Location = new System.Drawing.Point(0, 0);
            this.main_groupBox.Name = "main_groupBox";
            this.main_groupBox.Size = new System.Drawing.Size(323, 438);
            this.main_groupBox.TabIndex = 0;
            this.main_groupBox.TabStop = false;
            this.main_groupBox.Text = "Плагины";
            // 
            // main_splitContainer
            // 
            this.main_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_splitContainer.Location = new System.Drawing.Point(3, 16);
            this.main_splitContainer.Name = "main_splitContainer";
            this.main_splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // main_splitContainer.Panel1
            // 
            this.main_splitContainer.Panel1.Controls.Add(this.main_tableLayoutPanel);
            // 
            // main_splitContainer.Panel2
            // 
            this.main_splitContainer.Panel2.Controls.Add(this.disable_tableLayoutPanel);
            this.main_splitContainer.Size = new System.Drawing.Size(317, 419);
            this.main_splitContainer.SplitterDistance = 207;
            this.main_splitContainer.TabIndex = 1;
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 2;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(317, 207);
            this.main_tableLayoutPanel.TabIndex = 1;
            // 
            // disable_tableLayoutPanel
            // 
            this.disable_tableLayoutPanel.ColumnCount = 3;
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.disable_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.Controls.Add(this.disable_image_label, 1, 1);
            this.disable_tableLayoutPanel.Controls.Add(this.disable_text_label, 1, 2);
            this.disable_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.disable_tableLayoutPanel.Name = "disable_tableLayoutPanel";
            this.disable_tableLayoutPanel.RowCount = 4;
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.disable_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.disable_tableLayoutPanel.Size = new System.Drawing.Size(317, 208);
            this.disable_tableLayoutPanel.TabIndex = 1;
            // 
            // disable_image_label
            // 
            this.disable_image_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_image_label.Image = IA.Controls.Properties.Resources.Plugin_Icon_Simple_Gray;
            this.disable_image_label.Location = new System.Drawing.Point(61, 24);
            this.disable_image_label.Name = "disable_image_label";
            this.disable_image_label.Size = new System.Drawing.Size(194, 130);
            this.disable_image_label.TabIndex = 0;
            this.disable_image_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // disable_text_label
            // 
            this.disable_text_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.disable_text_label.Location = new System.Drawing.Point(61, 154);
            this.disable_text_label.Name = "disable_text_label";
            this.disable_text_label.Size = new System.Drawing.Size(194, 30);
            this.disable_text_label.TabIndex = 1;
            this.disable_text_label.Text = "Список плагинов недоступен";
            this.disable_text_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PluginListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_groupBox);
            this.Name = "PluginListControl";
            this.Size = new System.Drawing.Size(323, 438);
            this.main_groupBox.ResumeLayout(false);
            this.main_splitContainer.Panel1.ResumeLayout(false);
            this.main_splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).EndInit();
            this.main_splitContainer.ResumeLayout(false);
            this.disable_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.GroupBox main_groupBox;
        private System.Windows.Forms.SplitContainer main_splitContainer;
        private System.Windows.Forms.Label disable_image_label;
        private System.Windows.Forms.TableLayoutPanel disable_tableLayoutPanel;
        private System.Windows.Forms.Label disable_text_label;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
    }
}
