﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using IA.Objects;
using IA.Extensions;
using IOController;
using System.IO;

namespace IA.Controls
{
    /// <summary>
    /// Класс, описывающий элемент управления - Список материалов
    /// </summary>
    public partial class MaterialsListControl : UserControl
    {
        /// <summary>
        /// Русскоязычное имя класса
        /// </summary>
        public const string ObjectName = "Список наборов материалов";

        /// <summary>
        /// Всевозможные действия для кнопок и пунктов меню
        /// </summary>
        private Dictionary<string, Action> actions = null;

        /// <summary>
        /// Список иконок для элемента управления
        /// </summary>
        private ImageList icons = null;

        /// <summary>
        /// Элемент управления - Список материалов
        /// </summary>
        private DataGridView main_dataGridView = null;

        /// <summary>
        /// последние выбранные ползователем материалы
        /// </summary>
        private uint lastSelectedMaterials = 0;

        /// <summary>
        /// Элемент управления - Таблица с кнопками
        /// </summary>
        private TableLayoutPanel buttons_tableLayoutPanel = null;

        private Project project = null;
        /// <summary>
        /// Проект для которого показываются материалы
        /// </summary>
        public Project Project
        {
            get
            {
                return project;
            }
        }

        #region Делегаты и события
        /// <summary>
        /// Делегат для события - Набор материалов выбран
        /// </summary>
        /// <param name="materials"></param>
        public delegate void MaterialsClickEventHandler(Materials materials);

        /// <summary>
        /// Событие - Набор материало выбран
        /// </summary>
        public event MaterialsClickEventHandler MaterialsClickEvent;
        #endregion

        /// <summary>
        /// перерисовать форму
        /// </summary>
        /// <param name="project"></param>
        /// <param name="lastSelectedMaterials"></param>
        public void ShakeForm(Project project = null, uint lastSelectedMaterials = 0)
        {
            this.lastSelectedMaterials = lastSelectedMaterials;

            //Сохраняем переданный проект
            this.project = project;

            //Генерируем список материалов
            this.Generate();
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public MaterialsListControl()
        {
            InitializeComponent();

            //Сохраняем переданный проект

            //Инициализируем все возможные действия
            this.actions = this.Actions_Initialization();

            //Формируем список иконок
            this.icons = new ImageList() { ColorDepth = ColorDepth.Depth32Bit, ImageSize = new System.Drawing.Size(32, 32) };
            this.icons.Images.Add("Materials", Properties.Resources.Materials_Icon);
            

            //Инициализация элемента управления - Список материалов
            main_dataGridView = new DataGridView()
            {
                AllowUserToAddRows = false,
                AllowUserToDeleteRows = false,
                AllowUserToOrderColumns = false,
                AllowUserToResizeColumns = false,
                AllowUserToResizeRows = false,
                BackgroundColor = System.Drawing.Color.White,
                CellBorderStyle = DataGridViewCellBorderStyle.None,
                ColumnHeadersVisible = false,
                ContextMenuStrip = this.Menu_Initialization(),
                Dock = DockStyle.Fill,
                MultiSelect = false,
                RowHeadersVisible = false,
                SelectionMode = DataGridViewSelectionMode.FullRowSelect
            };
            main_dataGridView.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            //Создаём колонки
            DataGridViewImageColumn columnIcon = new DataGridViewImageColumn()
            {
                Name = "Icon",
                HeaderText = String.Empty,
                Width = this.icons.ImageSize.Width,
            };
            columnIcon.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            columnIcon.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewTextBoxColumn columnName = new DataGridViewTextBoxColumn()
            {
                Name = "Name",
                HeaderText = "Наименование",
                ReadOnly = true,
                Width = 150,
            };
            columnName.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            columnName.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewTextBoxColumn columnDateTime = new DataGridViewTextBoxColumn()
            {
                Name = "DateTime",
                HeaderText = "Дата/Время",
                ReadOnly = true,
                Width = 90,
            };
            columnDateTime.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            columnDateTime.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewTextBoxColumn columnComment = new DataGridViewTextBoxColumn()
            {
                Name = "Comment",
                HeaderText = "Комментарий"
            };
            columnComment.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            columnComment.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            //Добавляем столбцы
            this.main_dataGridView.Columns.AddRange(new DataGridViewColumn[]
                {
                    columnIcon,
                    columnName,
                    columnDateTime,
                    columnComment
                });

            //Автоматически подгоняем ширину последнего стобца
            int offset = SystemInformation.VerticalScrollBarWidth + columnIcon.Width + columnName.Width + columnDateTime.Width;
            main_dataGridView.SizeChanged += delegate(object o, EventArgs e)
            {
                main_dataGridView.Columns["Comment"].Width = main_dataGridView.Width - offset;
            };

            //Обработчики событий
            main_dataGridView.SelectionChanged += (sender, e) => this.Materials_Click_EventHandler();
            main_dataGridView.SelectionChanged += (sender, e) => this.Materials_Choose_ActionHandler(sender, this.actions[Materials.EditOperation.CHOOSE.FullName()]);
            main_dataGridView.MouseDown += delegate(object sender, MouseEventArgs e)
            {
                //Одиночное нажатие любой кнопкой мыши
                if (e.Clicks == 1)
                {
                    //Получаем информацию о том, в какой зоне проведено нажатие
                    DataGridView.HitTestInfo hitTestInfo = main_dataGridView.HitTest(e.X, e.Y);

                    //Выбираем соответствующий элемент
                    main_dataGridView.CurrentCell = hitTestInfo.RowIndex == -1 ? null : main_dataGridView.Rows[hitTestInfo.RowIndex].Cells[hitTestInfo.ColumnIndex];
                }
            };
            main_dataGridView.CellEndEdit += delegate(object sender, DataGridViewCellEventArgs e)
            {
                try
                {
                    //Получаем набор материалов для обновления
                    Materials materialsToUpdate = this.main_dataGridView.Rows[e.RowIndex].Tag as Materials;

                    //Получаем редакатируемую ячейку
                    DataGridViewCell updatedCell = this.main_dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];

                    //Изменение комментария к набору материалов
                    if (e.ColumnIndex == 3)
                    {
                        if (updatedCell.Value == null)
                            updatedCell.Value = materialsToUpdate.Comment = Materials.DefaultComment;
                        else
                            materialsToUpdate.Comment = updatedCell.Value.ToString();
                    }
                }
                catch (Exception ex)
                {
                    //Обрабатываем исключение
                    ex.HandleMessage("Ошибка при попытке изменить набор материалов.", (message) => Monitor.Log.Error(ObjectName, message, true));
                }
            };


            this.main_tableLayoutPanel.Controls.Add(main_dataGridView, 0, 0);

            //Инициализация элемента управления - Таблица кнопок
            int buttonsCount = Enum.GetValues(typeof(Materials.EditOperation)).Length + Enum.GetValues(typeof(Materials.ServerOperation)).Length;

            this.buttons_tableLayoutPanel = new TableLayoutPanel()
            {
                Dock = DockStyle.Fill,
                RowCount = 1,
                ColumnCount = buttonsCount
            };

            this.buttons_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 30));

            //Добавляем кнопку для каждой операции редактирования над проектом
            int columnSizePercentValue = 100 / buttonsCount;
            EnumLoop<Materials.EditOperation>.ForEach(operation =>
            {
                if (operation != Materials.EditOperation.CHOOSE)
                {
                    this.buttons_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, columnSizePercentValue));

                    //Получаем кнопку
                    Button button = this.actions[operation.FullName()].ToButton();

                    //Модернизируем её
                    button.Dock = DockStyle.Fill;

                    //Добавляем кнопку в таблицу кнопок
                    this.buttons_tableLayoutPanel.Controls.Add(button);
                }
            });
            EnumLoop<Materials.ServerOperation>.ForEach(operation =>
            {
                this.buttons_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, columnSizePercentValue));

                //Получаем кнопку
                Button button = this.actions[operation.FullName()].ToButton();

                //Модернизируем её
                button.Dock = DockStyle.Fill;

                //Добавляем кнопку в таблицу кнопок
                this.buttons_tableLayoutPanel.Controls.Add(button);
            });

            this.main_tableLayoutPanel.Controls.Add(this.buttons_tableLayoutPanel, 0, 1);
            
            Sql.SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);

            //Генерируем список материалов
            this.Generate();
        }

        /// <summary>
        /// Инициализация всеовзможных действий
        /// </summary>
        private Dictionary<string, Action> Actions_Initialization()
        {
            //Формируем результат
            Dictionary<string, Action> ret = new Dictionary<string, Action>();

            //Операции редактирования над набором материалов
            EnumLoop<Materials.EditOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Materials.EditOperation.CHOOSE:
                        ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Text = operation.Description(), ActionHandler = this.Materials_Choose_ActionHandler });
                        break;
                    case Materials.EditOperation.ADD:
                        ret.Add(operation.FullName(), new Action() { Enabled = true, Key = operation.FullName(), Text = operation.Description(), ActionHandler = this.Materials_Add_ActionHandler });
                        break;
                    case Materials.EditOperation.REMOVE:
                        ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Text = operation.Description(), ActionHandler = this.Materials_Remove_ActionHandler });
                        break;
                    default:
                        throw new Exception(Materials.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                }
            });

            //Серверные операции над набором материалов
            EnumLoop<Materials.ServerOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Materials.ServerOperation.DOWNLOAD_CLEAR_SRC:
                        ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Text = operation.Description(), ActionHandler = (sender, action) => this.Materials_GetSubDirectoryContents_ActionHandler(sender, action, Materials.enSubDirectories.SOURCES_CLEAR) });
                        break;
                    case Materials.ServerOperation.DOWNLOAD_LAB_SRC:
                        ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Text = operation.Description(), ActionHandler = (sender, action) => this.Materials_GetSubDirectoryContents_ActionHandler(sender, action, Materials.enSubDirectories.SOURCES_LAB) });
                        break;
                    default:
                        throw new Exception(Materials.Exceptions.UNKNOWN_SERVER_OPERATION.Description());
                }
            });

            return ret;
        }

        /// <summary>
        /// Инициализация контекстного меню
        /// </summary>
        /// <returns></returns>
        private ContextMenuStrip Menu_Initialization()
        {
            ContextMenuStrip ret = new ContextMenuStrip();

            //Добавляем разделитель
            ret.Items.Add(new ToolStripSeparator() { Name = "Separator_Materials_Edit_Begin" });

            //Добавляем операции, связанные с редактированием
            EnumLoop<Materials.EditOperation>.ForEach(operation => ret.Items.Add(this.actions[operation.FullName()].ToToolStripMenuItem()));

            //Добавляем разделитель
            ret.Items.Add(new ToolStripSeparator() { Name = "Separator_Materials_Edit_End" });

            //Добавляем разделитель
            ret.Items.Add(new ToolStripSeparator() { Name = "Separator_Materials_Server_Begin" });

            //Добавляем операции, связанные с сервером
            EnumLoop<Materials.ServerOperation>.ForEach(operation => ret.Items.Add(this.actions[operation.FullName()].ToToolStripMenuItem()));

            //Добавляем разделитель
            ret.Items.Add(new ToolStripSeparator() { Name = "Separator_Materials_Server_End" });

            return ret;
        }

        /// <summary>
        /// Формирование списка материалов
        /// </summary>
        private void Generate()
        {
            //Очищаем список материалов
            this.Clear();

            //Если проект не указан, показываем пустой список
            if (project == null)
                return;

            //Формируем список материалов
            foreach (Materials materials in Materials.GetByProject(project.ID))
                AddItem(materials);
        }

        /// <summary>
        /// Очистка списка материалов
        /// </summary>
        private void Clear()
        {
            //Очищаем список материалов
            main_dataGridView.Rows.Clear();
        }

        /// <summary>
        /// Добавление набора материалов в список на отображение
        /// </summary>
        /// <param name="materials"></param>
        private void AddItem(Materials materials)
        {
            DataGridViewRow row = new DataGridViewRow()
            {
                Height = this.icons.ImageSize.Height,
                Tag = materials
            };

            row.CreateCells(this.main_dataGridView);

            row.Cells[0].Value = this.icons.Images["Materials"];
            row.Cells[1].Value = "Материалы ver. " + materials.Version.ToString();
            row.Cells[2].Value = materials.DateTime.ToShortDateString() + "\n" + materials.DateTime.ToShortTimeString();
            row.Cells[3].Value = materials.Comment;

            //Добавляем новый набор материалов и делаем его активным
            main_dataGridView.Rows.Add(row);

            if (lastSelectedMaterials == materials.ID)
                main_dataGridView.Rows[main_dataGridView.Rows.Count - 1].Selected = true;
        }

        /// <summary>
        /// Получить строку в списке по идентификтору набора материалов
        /// </summary>
        /// <param name="materialsID">Идентификатор набора материалов</param>
        /// <returns></returns>
        private DataGridViewRow GetRowByMaterialsID(uint materialsID)
        {
            return this.main_dataGridView.Rows.Cast<DataGridViewRow>().FirstOrDefault(r => (r.Tag as Materials).ID == materialsID); 
        }

        #region Обработчики событий
        /// <summary>
        /// Обработчик - Нажали на набор материалов в списке
        /// </summary>
        private void Materials_Click_EventHandler()
        {
            try
            {
                //Задаём активность действий в зависимости от выбранного набора материалов
                EnumLoop<Materials.EditOperation>.ForEach(operation =>
                {
                    switch (operation)
                    {
                        case Materials.EditOperation.ADD:
                            // Делаем кнопку Добавить в окне материалов доступной всегда
                            //this.actions[operation.FullName()].Enabled = this.main_dataGridView.SelectedRows == null || this.main_dataGridView.SelectedRows.Count == 0;
                            break;
                        case Materials.EditOperation.CHOOSE:
                            break;
                        case Materials.EditOperation.REMOVE:
                            this.actions[operation.FullName()].Enabled = this.main_dataGridView.SelectedRows != null && this.main_dataGridView.SelectedRows.Count == 1;
                            break;
                        default:
                            throw new Exception(Materials.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                    }
                });

                EnumLoop<Materials.ServerOperation>.ForEach(operation =>
                {
                    switch (operation)
                    {
                        case Materials.ServerOperation.DOWNLOAD_CLEAR_SRC:
                        case Materials.ServerOperation.DOWNLOAD_LAB_SRC:
                            this.actions[operation.FullName()].Enabled = this.main_dataGridView.SelectedRows != null && this.main_dataGridView.SelectedRows.Count == 1;
                            break;
                        default:
                            throw new Exception(Materials.Exceptions.UNKNOWN_SERVER_OPERATION.Description());
                    }
                });
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Однократное нажатие на набор материалов в списке", (message) => Monitor.Log.Error(ObjectName, message, true));
            }

        }
        #endregion

        #region Обработчики действий
        /// <summary>
        /// Обработчик - Выбрали набор материалов в списке
        /// </summary>
        private void Materials_Choose_ActionHandler(object sender, Action action)
        {
            try
            {
                //Если ничего не выбрано или выбрано слишком много - ничего не делаем
                if (main_dataGridView.SelectedRows == null || main_dataGridView.SelectedRows.Count != 1)
                    return;

                //Вызываем событие выбора материалов
                MaterialsClickEvent?.Invoke(main_dataGridView.SelectedRows[0].Tag as Materials);
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Добавить набор материалов
        /// </summary>
        private void Materials_Add_ActionHandler(object sender, Action action)
        {
            try
            {
                //Получаем директорию с исходными текстами для анализа от пользователя
                string originalSourcesDirectoryPath = DirectoryController.SelectByUser(null, "Укажите директорию с исходными текстами для анализа.", 
                                                                                       emptyPathPossible:true, enableComment:true);
                if (originalSourcesDirectoryPath == null)
                    return;

                string[] split = originalSourcesDirectoryPath.Split(new char[] { '?' });
                originalSourcesDirectoryPath = split[0];
                string comment = split[1];

                //Если директория с исходными текстами для анализа не выбрана, ничего не делаем
                if (originalSourcesDirectoryPath == null)
                    return;

                if (originalSourcesDirectoryPath == String.Empty)
                {
                    // установили галочку, что файлов не будет
                    Directory.CreateDirectory("EmptyFolder");
                    originalSourcesDirectoryPath = Path.GetFullPath("EmptyFolder");
                }
                //Отображаем окно ожидания
                Materials materials = WaitWindow.Show(
                    delegate(WaitWindow window)
                    {
                        //Создаём новый набор материалов в базе данных
                        window.WorkerMethodResult = Materials.Add(originalSourcesDirectoryPath, project.ID,
                          comment == "" ? (originalSourcesDirectoryPath == Path.GetFullPath("EmptyFolder")? "Нет файлов" : Materials.DefaultComment) : comment);
                    },
                    "Добавление набора материалов"
                ) as Materials;

                //Проверка результата
                materials.ThrowIfNull("Результат добавления нового набора материалов в базу данных не соответствует ожидаемому.");

                //Добавляем созданный набор материалов в список
                this.AddItem(materials);
                main_dataGridView.Rows[main_dataGridView.Rows.Count - 1].Selected = true;

                if (originalSourcesDirectoryPath == Path.GetFullPath("EmptyFolder"))
                {
                    // если генерили материалы на пустом наборе файлов
                    Directory.Delete("EmptyFolder");
                }

            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Удалить набор материалов
        /// </summary>
        private void Materials_Remove_ActionHandler(object sender, Action action)
        {
            //Объект набора материалов, который требуется удалить
            Materials materials = null;

            try
            {
                //Проверка на разумность
                if (main_dataGridView.SelectedRows == null || main_dataGridView.SelectedRows.Count != 1)
                    throw new Exception("Ничего не вывбрано или выбрано более одного .");

                //Задаём вопрос пользователю
                switch (MessageBox.Show("Набор материалов будет безвозвратно удалён. Вы уверены?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        break;
                    case DialogResult.No:
                        return;
                }

                //Получаем объект набора материалов, который требуется удалить
                materials = main_dataGridView.SelectedRows[0].Tag as Materials;

                //Отображаем окно ожидания
                WaitWindow.Show(
                    delegate(WaitWindow window)
                    {
                        //Удаляем набор материалов из базы данных
                        Materials.RemoveByID(materials.ID);
                    },
                    "Удаление набора материалов"
                );
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));

                return;
            }

            //Удаляем набор материалов из списка
            if (materials != null)
            {
                main_dataGridView.Rows.Remove(this.GetRowByMaterialsID(materials.ID));
                main_dataGridView.CurrentCell = null;
            }
        }

        /// <summary>
        /// Обработчик - Выгрузить содержимое поддиректории набора материалов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="subDirectory">Поддиректория набора материалов</param>
        private void Materials_GetSubDirectoryContents_ActionHandler(object sender, Action action, Materials.enSubDirectories subDirectory)
        {
            try
            {
                //Проверка на разумность
                if (main_dataGridView.SelectedRows == null || main_dataGridView.SelectedRows.Count != 1)
                    throw new Exception("Ничего не выбрано или выбрано более одного элемента списка.");

                //Формируем всевозможные сообщения
                string questionMessage = "Укажите директорию, куда следует поместить ";
                string waitWindowMessage = "Выгрузка ";
                string resultMessage = String.Empty;

                switch (subDirectory)
                {
                    case Materials.enSubDirectories.STORAGES:
                        questionMessage += "все доступные в рамках набора материалов Хранилища.";
                        waitWindowMessage += "всех доступных в рамках набора материалов Хранилищ";
                        resultMessage += "Все доступные в рамках набора материалов Хранилища";
                        break;
                    case Materials.enSubDirectories.SOURCES_ORIGINAL:
                        questionMessage += "оригинальные исходные тексты.";
                        waitWindowMessage += "оригинальных исходных текстов";
                        resultMessage += "Оригинальные исходные тексты";
                        break;
                    case Materials.enSubDirectories.SOURCES_CLEAR:
                        questionMessage += "очищенные исходные тексты.";
                        waitWindowMessage += "очищенных исходных текстов";
                        resultMessage += "Очищенные исходные тексты";
                        break;
                    case Materials.enSubDirectories.SOURCES_LAB:
                        questionMessage += "лабораторные исходные тексты.";
                        waitWindowMessage += "лабораторных исходных текстов";
                        resultMessage += "Лабораторные исходные тексты";
                        break;
                    default:
                        throw new Exception(Materials.Exceptions.UNKNOWN_SUBDIRECTORY.Description());
                }

                waitWindowMessage += " с сервера проектов";

                //Получаем путь до директории, куда следует скопировать содержимое, от пользователя
                string destinationDirectoryPath = DirectoryController.SelectByUser(null, questionMessage, true, enableCreateFolder: true);

                //Если путь до директории не указан, ничего не делаем
                if (destinationDirectoryPath == null)
                    return;

                resultMessage += " успешно помещены в директорию <" + destinationDirectoryPath + ">.";

                //Получаем объект набора материалов, содержимое поддиреткории которого, требуется получить
                Materials materials = main_dataGridView.SelectedRows[0].Tag as Materials;

                //Отобржаем окно ожидания
                WaitWindow.Show(
                    delegate(WaitWindow window)
                    {
                        //Выгружаем содержимое указанной поддиректории
                        materials.DownloadSubDirectoryContents(subDirectory, destinationDirectoryPath);
                    },
                    waitWindowMessage
                );

                //Отображаем сообщение о завершении
                MessageBox.Show(resultMessage, "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        } 
        #endregion
        
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления в соответствии с содержимым базы данных
            //дубляж. Вызов есть в конструкторе элемента формы
            //this.Generate();

            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Делаем его недоступным
            this.Enabled = false;
        }
    }
}
