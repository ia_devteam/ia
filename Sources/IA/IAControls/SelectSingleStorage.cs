﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IA.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SelectSingleStorage : UserControl
    {
        /// <summary>
        /// Событие срабатывает при нажати кнопки Ок
        /// </summary>
        public event EventHandler<string> StorageReadyToOpenEvent;
        /// <summary>
        /// 
        /// </summary>
        public SelectSingleStorage()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(textBoxWithDialogButton1.Text))
                StorageReadyToOpenEvent?.Invoke(this, textBoxWithDialogButton1.Text);
            else
                Monitor.Log.Warning("Открытие автономного хранилища", $"Указанный файл не существует (<{textBoxWithDialogButton1.Text}>)");
        }
    }
}
