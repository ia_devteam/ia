﻿using System.Windows.Forms;

namespace IA.Dialogs
{
    partial class StorageSelectControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_splitContainer = new System.Windows.Forms.SplitContainer();
            this.storageTreeControl = new IA.Controls.StorageTreeControl();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.pluginListControl = new IA.Controls.PluginListControl();
            this.storageSettingsControl = new IA.Controls.StorageSettingsControl();
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).BeginInit();
            this.main_splitContainer.Panel1.SuspendLayout();
            this.main_splitContainer.Panel2.SuspendLayout();
            this.main_splitContainer.SuspendLayout();
            this.main_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_splitContainer
            // 
            this.main_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_splitContainer.Location = new System.Drawing.Point(0, 0);
            this.main_splitContainer.Name = "main_splitContainer";
            // 
            // main_splitContainer.Panel1
            // 
            this.main_splitContainer.Panel1.Controls.Add(this.storageTreeControl);
            // 
            // main_splitContainer.Panel2
            // 
            this.main_splitContainer.Panel2.Controls.Add(this.main_tableLayoutPanel);
            this.main_splitContainer.Size = new System.Drawing.Size(984, 761);
            this.main_splitContainer.SplitterDistance = 652;
            this.main_splitContainer.TabIndex = 0;
            // 
            // storageTreeControl
            // 
            this.storageTreeControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.storageTreeControl.Enabled = true;
            this.storageTreeControl.Location = new System.Drawing.Point(0, 0);
            this.storageTreeControl.Name = "storageTreeControl";
            this.storageTreeControl.Size = new System.Drawing.Size(652, 761);
            this.storageTreeControl.TabIndex = 0;
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.pluginListControl, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.storageSettingsControl, 0, 0);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 2;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(328, 761);
            this.main_tableLayoutPanel.TabIndex = 0;
            // 
            // pluginListControl
            // 
            this.pluginListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pluginListControl.Location = new System.Drawing.Point(3, 203);
            this.pluginListControl.Name = "pluginListControl";
            this.pluginListControl.Size = new System.Drawing.Size(322, 555);
            this.pluginListControl.TabIndex = 0;
            // 
            // storageSettingsControl
            // 
            this.storageSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.storageSettingsControl.Location = new System.Drawing.Point(3, 3);
            this.storageSettingsControl.Name = "storageSettingsControl";
            this.storageSettingsControl.Size = new System.Drawing.Size(322, 194);
            this.storageSettingsControl.TabIndex = 1;
            // 
            // StorageSelectControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_splitContainer);
            this.Name = "StorageSelectControl";
            this.Size = new System.Drawing.Size(984, 761);
            this.main_splitContainer.Panel1.ResumeLayout(false);
            this.main_splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.main_splitContainer)).EndInit();
            this.main_splitContainer.ResumeLayout(false);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer main_splitContainer;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private Controls.PluginListControl pluginListControl;
        private Controls.StorageSettingsControl storageSettingsControl;
        private Controls.StorageTreeControl storageTreeControl;
    }
}