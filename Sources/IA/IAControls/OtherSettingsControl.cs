﻿using System;
using System.Windows.Forms;

namespace IA.Controls
{
    /// <summary>
    /// Класс, реализующий элемент управления - Другие настройки приложения
    /// </summary>
    public partial class OtherSettingsControl : UserControl
    {
        /// <summary>
        /// Рабочая директория приложения
        /// </summary>
        public string WorkDirectoryPath
        {
            get
            {
                return workDirectoryPath_textBox.Text;
            }
            set
            {
                workDirectoryPath_textBox.Text = value ?? string.Empty;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public OtherSettingsControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обработчик - нажали на клавишу "Обзор..."
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void workDirectoryPath_button_Click(object sender, EventArgs e)
        {
            string path = Store.WorkDirectory.SelectWorkDirectoryPathFromUser(workDirectoryPath_textBox.Text);

            if (path == null)
                return;

            workDirectoryPath_textBox.Text = path;
        }
    }
}
