﻿using System;
using System.Windows.Forms;

using IA.Objects;

namespace IA.Controls
{
    /// <summary>
    /// Реализация элемента управления настроек пользователя
    /// </summary>
    public partial class UserSettingsControl : UserControl
    {
        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string UserSurname
        {
            get
            {
                return surname_textBox.Text.Trim();
            }
        }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName
        {
            get 
            {
                return name_textBox.Text.Trim();
            }
        }

        /// <summary>
        /// Отчество пользователя
        /// </summary>
        public string UserPatronymic
        {
            get
            {
                return patronymic_textBox.Text.Trim();
            }
        }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User
        {            
            set
            {
                surname_textBox.Text    = value == null ? String.Empty : value.Surname;
                name_textBox.Text       = value == null ? String.Empty : value.Name;
                patronymic_textBox.Text = value == null ? String.Empty : value.Patronymic;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public UserSettingsControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Включить\отключить доступность списка настроек. В режиме недоступности список настроек не отображается.
        /// </summary>
        public bool IsEnabled
        {
            set
            {
                //Скрываем\Показываем сообщение о недоступности настроек
                main_splitContainer.Panel1Collapsed = !value;
                main_splitContainer.Panel2Collapsed = value;
            }
        }

        /// <summary>
        /// Включить\отключить режим блокирование списка настроек. В режиме блокирования список настроек отображается, но никакие действия не доступны.
        /// </summary>
        public bool IsBlocked
        {
            set
            {
                this.main_tableLayoutPanel.Enabled = !value;
            }
        }
    }
}
