﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Objects;

namespace IA.Controls
{
    using TreeViewer = Microsoft.Glee.GraphViewerGdi.GViewer;
    using TreeGraph = Microsoft.Glee.Drawing.Graph;
    using TreeNode = Microsoft.Glee.Drawing.Node;
    using TreeNodeColor = Microsoft.Glee.Drawing.Color;
    using TreeNodeShape = Microsoft.Glee.Drawing.Shape;
    using TreeEdge = Microsoft.Glee.Drawing.Edge;
    using static IAUtils.IACommon;
    /// <summary>
    /// Класс, описывающий элемент управления - Дерево Хранилищ
    /// </summary>
    public partial class StorageTreeControl : UserControl
    {
        #region Константы
        /// <summary>
        /// Русскоязычное имя класса
        /// </summary>
        public const string ObjectName = "Дерево Хранилищ";

        /// <summary>
        /// Стандартный цвет окантовки узла дерева Хранилищ
        /// </summary>
        private TreeNodeColor Node_Color_Default = TreeNodeColor.Black;

        /// <summary>
        /// Стандартный цвет заливки узла дерева Хранилищ
        /// </summary>
        private TreeNodeColor Node_FillColor_Default = TreeNodeColor.White;

        /// <summary>
        /// Цвет заливки активного узла дерева Хранилищ
        /// </summary>
        private TreeNodeColor Node_FillColor_Active = TreeNodeColor.Red;

        /// <summary>
        /// Цвет заливки узла дерева Хранилищ, соответствующего пустому Хранилищу
        /// </summary>
        private TreeNodeColor Node_FillColor_Empty = TreeNodeColor.Gray;

        /// <summary>
        /// Стандартная форма узла дерева Хранилищ
        /// </summary>
        private TreeNodeShape Node_Shape_Default = TreeNodeShape.Box;
        #endregion

        #region Делегаты и события
        /// <summary>
        /// Делегат для события - Кликнули по Хранилищу
        /// </summary>
        /// <param name="storage"></param>
        public delegate void StorageClickEventHandler(Storage storage);

        /// <summary>
        /// Событие - Кликнули по Хранилищу
        /// </summary>
        public event StorageClickEventHandler StorageClickEvent;
        
        /// <summary>
        /// Событие - Дважды кликнули по Хранилищу
        /// </summary>
        public event StorageDoubleClickEventHandler StorageDoubleClickEvent;
        public event StorageUnloadClickEventHandler StorageUnloadClickEvent;
        #endregion

        /// <summary>
        /// Текущий узел
        /// </summary>
        private TreeNode selectedNode = null;

        /// <summary>
        /// Всевозможные действия для кнопок и пунктов меню
        /// </summary>
        public Dictionary<Storage.EditOperation, Action> actions = null;

        /// <summary>
        /// Элемент управления - Таблица с кнопками
        /// </summary>
        private TableLayoutPanel buttons_tableLayoutPanel = null;

        /// <summary>
        /// Элемент управления - Вьювер Хранилищ
        /// </summary>
        private TreeViewer viewer = null;

        private Materials materials = null;
        /// <summary>
        /// Набор материалов, для которых необходио отобразить дерево Хранилищ
        /// </summary>
        public Materials Materials
        {
            set
            {
                //Запоминаем указанное значение
                materials = value;

                //Формируем дерево Хранилищ
                Generate();
            }
        }

        /// <summary>
        /// Пользователь в данный момент работающий с системой
        /// </summary>
        public User User { private get; set; }

        public void Rebuild(Materials materials = null, User user = null)
        {
            //Устанавливаем текущие материалы
            this.Materials = materials;

            //Устанавливаем текущего пользователя
            this.User = user;
        }

        public StorageTreeControl() : this(null, null)
        {
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="materials">Набор материалов</param>
        /// <param name="user">Пользователь</param>
        public StorageTreeControl( Materials materials, User user)
        {
            InitializeComponent();

            //Инициализируем все возможные действия
            this.actions = this.Actions_Initialization();

            //Инициализация элемента управления - Таблица кнопок
            int buttonsCount = Enum.GetValues(typeof(Materials.EditOperation)).Length + Enum.GetValues(typeof(Materials.ServerOperation)).Length;

            this.buttons_tableLayoutPanel = new TableLayoutPanel()
            {
                Dock = DockStyle.Fill,
                RowCount = 1,
                ColumnCount = buttonsCount
            };

            this.buttons_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100));

            //Добавляем кнопку для каждой операции редактирования над проектом
            int columnSizePercentValue = 100 / buttonsCount;
            EnumLoop<Storage.EditOperation>.ForEach(operation =>
            {
                this.buttons_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, columnSizePercentValue));

                //Получаем кнопку
                Button button = this.actions[operation].ToButton();

                //Модернизируем её
                button.Dock = DockStyle.Fill;

                //Добавляем кнопку в таблицу кнопок
                this.buttons_tableLayoutPanel.Controls.Add(button);
                
            });
            
            Sql.SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);

            this.main_tableLayoutPanel.Controls.Add(this.buttons_tableLayoutPanel, 0, 1);
            
            //Устанавливаем текущие материалы
            this.Materials = materials;

            //Устанавливаем текущего пользователя
            this.User = user;

        }

        /// <summary>
        /// Инициализация всеовзможных действий
        /// </summary>
        private Dictionary<Storage.EditOperation, Action> Actions_Initialization()
        {
            //Формируем результат
            Dictionary<Storage.EditOperation, Action> ret = new Dictionary<Storage.EditOperation, Action>();

            //Операции редактирования Хранилища
            EnumLoop<Storage.EditOperation>.ForEach(operation =>
            {
                var action = new Action() { Enabled = false, Key = operation.FullName(), Text = operation.Description()};
                switch (operation)
                    {
                        case Storage.EditOperation.CHOOSE:
                            action.ActionHandler = this.Storage_Choose_ActionHandler;                        
                            break;
                        case Storage.EditOperation.ADD:
                            action.Enabled = true;
                            action.ActionHandler = this.Storage_Add_ActionHandler;
                            break;
                        case Storage.EditOperation.REMOVE:
                            action.ActionHandler = this.Storage_Remove_ActionHandler;
                            break;
                        case Storage.EditOperation.UNLOAD:
                            action.ActionHandler = this.Storage_Unload_ActionHandler; 
                            break;
                        default:
                            throw new Exception(Storage.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                    }
                ret.Add(operation, action);
            });

            return ret;
        }

        /// <summary>
        /// Инициализация контекстного меню
        /// </summary>
        /// <returns></returns>
        private ContextMenuStrip Menu_Initialization()
        {
            ContextMenuStrip ret = new ContextMenuStrip();

            //Добавляем операции, связанные с редактированием
            EnumLoop<Storage.EditOperation>.ForEach(operation => ret.Items.Add(this.actions[operation].ToToolStripMenuItem()));

            //Обработчики событий
            ret.Opening += (sender, e) => this.Storage_Click_EventHandler();

            return ret;
        }

        /// <summary>
        /// Инициализация элемента управления - Вьювер Хранилищ
        /// </summary>
        /// <param name="graph">Граф для отображения</param>
        /// <returns></returns>
        private TreeViewer Viewer_Initialization(TreeGraph graph)
        {
            //Инициализация элемента управления - Вьювер Хранилищ
            TreeViewer ret = new TreeViewer()
            {
                Dock = DockStyle.Fill,
                Graph = graph,
                SaveButtonVisible = false,
                NavigationVisible = false
                
            };

            //Обработчики событий
            ret.MouseClick += (sender, e) => this.Storage_Click_EventHandler();
            ret.MouseDoubleClick += delegate(object sender, MouseEventArgs e)
            {
                //Если нажали правой клавишей, ничего не делаем
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                    return;

                //Запускаем обработчик двойного нажатия на Хранилище
                this.Storage_Choose_ActionHandler(sender, this.actions[Storage.EditOperation.CHOOSE]);
            };
            ret.KeyDown += delegate(object sender, KeyEventArgs e)
            {
                switch (e.KeyCode)
                {
                    case Keys.Enter:
                        {
                            //Выбираем Хранилище
                            this.Storage_Choose_ActionHandler(sender, this.actions[Storage.EditOperation.CHOOSE]);

                            return;
                        }
                    default:
                        return;
                }
            };

            //Контекстное меню            
            ret.ContextMenuStrip = this.Menu_Initialization();

            return ret;
        }

        /// <summary>
        /// Формирование дерева Хранилищ
        /// </summary>
        /// <returns></returns>
        private void Generate()
        {
            //Задаём активность действий по умолчанию
            EnumLoop<Storage.EditOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Storage.EditOperation.ADD:
                        this.actions[operation].Enabled = true;
                        break;
                    case Storage.EditOperation.CHOOSE:
                    case Storage.EditOperation.REMOVE:
                    case Storage.EditOperation.UNLOAD:
                        this.actions[operation].Enabled = false;
                        break;
                    default:
                        throw new Exception(Storage.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                }
            });

            //Создаём граф
            TreeGraph graph = new TreeGraph("graph");

            //Для каждого Хранилища создаём узел
            if (materials != null)
                foreach (Storage storage in Storage.GetByMaterials(materials.ID))
                    this.GenerateNode(graph, storage);

            //Инициализация элемента управления - Вьювер Хранилищ
            viewer = Viewer_Initialization(graph);

            //Очищаем дерево Хранилищ
            this.Clear();

            //Добавление элемента управления
            this.main_groupBox.Controls.Add(viewer);
        }

        /// <summary>
        /// Очистить дерево Хранилищ
        /// </summary>
        public void Clear()
        {
            //Скрываем дерево Хранилищ
            this.main_groupBox.Controls.Clear();

            //Выбранный узел неизвестен
            this.selectedNode = null;

            //Формируем событие (Хранилище не выбрано)
            StorageClickEvent?.Invoke(null);
        }

        /// <summary>
        /// Формирование стандартного узла дерева Хранилищ
        /// </summary>
        /// <param name="graph">Граф дерева Хранилищ</param>
        /// <param name="storage">Объект Хранилища</param>
        /// <returns></returns>
        private TreeNode GenerateNode(TreeGraph graph, Storage storage)
        {
            //Добавляем узел
            TreeNode ret = graph.AddNode(storage.ID.ToString());

            //Предаём ему свойства по умолчанию
            ret.UserData = storage;
            ret.Attr.Color = this.Node_Color_Default;
            ret.Attr.Fillcolor = this.Node_FillColor_Default;
            ret.Attr.Shape = this.Node_Shape_Default;

            //Если Хранилище пусто, то подсвечиваем
            if (storage.IsEmpty)
                ret.Attr.Fillcolor = this.Node_FillColor_Empty;

            //Добавляем связь, если она есть
            if (storage.ParentID != 0)
                this.GenerateEdge(graph, storage);
            
            return ret;
        }

        /// <summary>
        /// Формирование стандартной связи между узлами дерева Хранилищ
        /// </summary>
        /// <param name="graph">Граф дерева Хранилищ</param>
        /// <param name="storage">Объект Хранилища</param>
        /// <returns></returns>
        private TreeEdge GenerateEdge(TreeGraph graph, Storage storage)
        {
            return graph.AddEdge(storage.ParentID.ToString(), storage.ID.ToString());
        }

        /// <summary>
        /// Сброс дерева Хранилищ. Подсветка вершин устанавливается в первоначальное состояние.
        /// </summary>
        private void Reset()
        {
            foreach (TreeNode node in viewer.Graph.NodeMap.Values)
            {
                //Получаем Хранилище, соответствующее узлу
                Storage storage = node.UserData as Storage;

                //Если узлу не соответствует Хранилище
                if (storage == null)
                    throw new Exception("Найден узел в дереве Хранилищ, которому не было сопоставлено Хранилище.");

                //Если Хранилище пусто
                if (storage.IsEmpty)
                    //Подсвечиваем Хранилище, как пустое
                    node.Attr.Fillcolor = this.Node_FillColor_Empty;
                //В противном случае
                else
                    //Задаём стандарнтую подсветку для узла
                    node.Attr.Fillcolor = this.Node_FillColor_Default;
            }

            //Формируем событие (Хранилище не выбрано)
            StorageClickEvent?.Invoke(null);
        }

        /// <summary>
        /// Добавление узла в дерево
        /// </summary>
        /// <param name="storage"></param>
        /// <returns></returns>
        private TreeNode AddNode(Storage storage)
        {
            //Сбрасываем выделение активных узлов
            Reset();

            //Сохраняем граф который уже есть
            TreeGraph graph = viewer.Graph;    

            //Добавляем узел
            TreeNode node = this.GenerateNode(graph, storage);

            //Обновляем граф для отображения
            viewer.Graph = graph;

            return node;
        }

        /// <summary>
        /// Активыруем узел
        /// </summary>
        /// <param name="node"></param>
        private void ActivateNode(TreeNode node)
        {
            //Делаем узел красным
            node.Attr.Fillcolor = this.Node_FillColor_Active;

            //Формируем событие (Хранилище выбрано)
            StorageClickEvent?.Invoke((Storage)node.UserData);
        }

        #region Обработчики событий
        /// <summary>
        /// Обработчик - Нажали на Хранилище в дереве
        /// </summary>
        private void Storage_Click_EventHandler()
        {
            try
            {
                //Сбрасываем подсветку графа
                Reset();

                //Получаем выбранный узел
                this.selectedNode = viewer.SelectedObject as TreeNode;

                //Задаём активность действий в зависимости от выбранного Хранилища
                EnumLoop<Storage.EditOperation>.ForEach(operation =>
                {
                    switch (operation)
                    {
                        case Storage.EditOperation.ADD:
                            this.actions[operation].Enabled = true;
                            break;
                        case Storage.EditOperation.CHOOSE:
                        case Storage.EditOperation.UNLOAD:
                            this.actions[operation].Enabled = (this.selectedNode != null);
                            break;
                        case Storage.EditOperation.REMOVE:
                            this.actions[operation].Enabled = (this.selectedNode != null && ((Storage)this.selectedNode.UserData).ParentID != 0);
                            break;
                        default:
                            throw new Exception(Storage.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                    }
                });

                //Если выбран какой-либо узел
                if (this.selectedNode != null)
                    //Делаем его активным
                    ActivateNode(this.selectedNode);

                //Обновляем дерево
                viewer.Invalidate();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Однократное нажатие на Хранилище в дереве", (message) => Monitor.Log.Error(ObjectName, message, true));
            }

        }
        #endregion

        #region Обработчики действий
        /// <summary>
        /// Обработчик - Выбрали Хранилище в дереве
        /// </summary>
        private void Storage_Choose_ActionHandler(object sender, Action action)
        {
            try
            {
                //Если мы кликнули не по узлу, то ничего не делаем
                if (this.selectedNode == null)
                    return;

                //Формируем событие
                StorageDoubleClickEvent?.Invoke(this.selectedNode.UserData as Storage);
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
               ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Создать Хранилище
        /// </summary>
        public  void Storage_Add_ActionHandler()
        {
            try
            {
                //Проверка на разумность
                if (this.User == null)
                    throw new Exception("Текущий пользователь не указан.");

                //Отображаем окно ожидания
                Storage storage = WaitWindow.Show(
                    delegate(WaitWindow window)
                    {
                        //Добавляем Хранилище в базу данных
                        window.WorkerMethodResult = Storage.Add(materials.ID, 0, this.User.ID, Storage.enMode.EXPERT);
                    },
                    "Добавление Хранилища"
                ) as Storage;

                //Проверка результата
                storage.ThrowIfNull("Результат добавления нового Хранилища в базу данных не соответствует ожидаемому.");

                //Добавляем узел в дерево и делаем его активным
                TreeNode node = AddNode(storage);
                ActivateNode(node);
                selectedNode = node;
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
               ex.HandleAsActionHandlerException("Добавление хранилища", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        private void Storage_Add_ActionHandler(object sender, Action action)
        {
            try
            {
                //Проверка на разумность
                if (this.User == null)
                    throw new Exception("Текущий пользователь не указан.");

                //Отображаем окно ожидания
                Storage storage = WaitWindow.Show(
                    delegate(WaitWindow window)
                    {

                        //Добавляем Хранилище в базу данных
                        Storage rootStorage = Storage.GetByMaterials( materials.ID).FirstOrDefault(s => s.ParentID == 0);
                        if (rootStorage == null)
                            Monitor.Log.Error("StorageTreeControl", "В материалах отсутствует корневое хранилище! Дальнейшая работа невозможна. ");
                        window.WorkerMethodResult = Storage.Add(materials.ID, rootStorage.ID, this.User.ID, Storage.enMode.EXPERT);
                    },
                    "Добавление Хранилища"
                ) as Storage;

                //Проверка результата
                storage.ThrowIfNull("Результат добавления нового Хранилища в базу данных не соответствует ожидаемому.");

                //Добавляем узел в дерево и делаем его активным
                TreeNode node = AddNode(storage);
                ActivateNode(node);
                selectedNode = node;
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException("Добавление хранилища", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
                    
        private void RemoveChildren(uint ID)
        {
            foreach (Storage storage in Storage.GetByMaterials(materials.ID))
            {
                if (storage.ParentID == ID)
                {
                    RemoveChildren(storage.ID);
                    Storage.RemoveByID(storage.ID);
                }
            }
        }

        /// <summary>
        /// Обработчик - Удалить Хранилище
        /// </summary>
        private void Storage_Remove_ActionHandler(object sender, Action action)
        {
            try
            {
                //Проверка на разумность
                if (this.selectedNode == null)
                    throw new Exception("Ничего не выбрано.");

                if (MessageBox.Show("Удалить выбранное хранилище и всех его наследников?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //Отображаем окно ожидания
                    WaitWindow.Show(
                        delegate(WaitWindow window)
                        {
                            //Удаляем Хранилище и его детей из БД
                            RemoveChildren(((Storage)this.selectedNode.UserData).ID);
                            Storage.RemoveByID(((Storage)this.selectedNode.UserData).ID);
                        },
                        "Удаление Хранилища"
                    );
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
               ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }

            //Перестраиваем дерево с уже удалённым узлом
            Generate();
        }

        private void Storage_Unload_ActionHandler(object sender, Action action)
        {            
            try
            {
                if (this.selectedNode == null)
                    return;

                var storage = this.selectedNode.UserData as Storage;                
                storage.ThrowIfNull("Ошибка в определении текущего выбранного хранилища");
                if (storage.ParentID == 0)
                {
                    Monitor.Log.Warning(action.Text, "Выгрузка пустого хранилища не поддерживается.");
                    return;
                }

                string unloadDir;
                using (var dialog = new FolderBrowserDialog() { Description = "Выберите каталог для выгрузки", ShowNewFolderButton = true })
                {
                    if (dialog.ShowDialog() != DialogResult.OK)
                        return;

                    if (!System.IO.Directory.Exists(dialog.SelectedPath))
                    {
                        Monitor.Log.Error(action.Text,"Каталог для выгрузки материалов должен существовать");
                        return;
                    }
                    unloadDir = dialog.SelectedPath;
                }
                var result = StorageUnloadClickEvent?.Invoke(storage, unloadDir);
                if (result == null || result.Value == false)
                {
                    Monitor.Log.Error(action.Text, "Выгрузка завершилась с ошибкой.");
                    return;
                }
                Monitor.Log.Information(action.Text, $"Выгрузка в каталог {unloadDir} успешна.");
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        #endregion
        
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления в соответствии с содержимым базы данных
            this.Generate();

            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Делаем его недоступным
            this.Enabled = false;
        }

    }
}
