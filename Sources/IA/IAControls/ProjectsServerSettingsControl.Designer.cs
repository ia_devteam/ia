﻿namespace IA.Controls
{
    partial class ProjectsServerSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.password_label = new System.Windows.Forms.Label();
            this.login_label = new System.Windows.Forms.Label();
            this.name_label = new System.Windows.Forms.Label();
            this.projectsDirectoryPath_label = new System.Windows.Forms.Label();
            this.projectsDirectoryPath_postfix_textBox = new System.Windows.Forms.TextBox();
            this.projectsDirectoryPath_button = new System.Windows.Forms.Button();
            this.projectsDirectoryPath_prefix_textBox = new System.Windows.Forms.TextBox();
            this.name_textBox = new System.Windows.Forms.TextBox();
            this.login_textBox = new System.Windows.Forms.TextBox();
            this.password_textBox = new System.Windows.Forms.TextBox();
            this.main_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 4;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.password_label, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.login_label, 0, 1);
            this.main_tableLayoutPanel.Controls.Add(this.name_label, 0, 0);
            this.main_tableLayoutPanel.Controls.Add(this.projectsDirectoryPath_label, 0, 3);
            this.main_tableLayoutPanel.Controls.Add(this.projectsDirectoryPath_postfix_textBox, 2, 3);
            this.main_tableLayoutPanel.Controls.Add(this.projectsDirectoryPath_button, 3, 3);
            this.main_tableLayoutPanel.Controls.Add(this.projectsDirectoryPath_prefix_textBox, 1, 3);
            this.main_tableLayoutPanel.Controls.Add(this.name_textBox, 1, 0);
            this.main_tableLayoutPanel.Controls.Add(this.login_textBox, 1, 1);
            this.main_tableLayoutPanel.Controls.Add(this.password_textBox, 1, 2);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 5;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(669, 123);
            this.main_tableLayoutPanel.TabIndex = 1;
            // 
            // password_label
            // 
            this.password_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.password_label.AutoSize = true;
            this.password_label.Location = new System.Drawing.Point(3, 68);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(144, 13);
            this.password_label.TabIndex = 5;
            this.password_label.Text = "Пароль:";
            this.password_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // login_label
            // 
            this.login_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.login_label.AutoSize = true;
            this.login_label.Location = new System.Drawing.Point(3, 38);
            this.login_label.Name = "login_label";
            this.login_label.Size = new System.Drawing.Size(144, 13);
            this.login_label.TabIndex = 4;
            this.login_label.Text = "Имя пользователя:";
            this.login_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // name_label
            // 
            this.name_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.name_label.AutoSize = true;
            this.name_label.Location = new System.Drawing.Point(3, 8);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(144, 13);
            this.name_label.TabIndex = 1;
            this.name_label.Text = "Имя/IP-адрес сервера:";
            this.name_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // projectsDirectoryPath_label
            // 
            this.projectsDirectoryPath_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.projectsDirectoryPath_label.AutoSize = true;
            this.projectsDirectoryPath_label.Location = new System.Drawing.Point(3, 98);
            this.projectsDirectoryPath_label.Name = "projectsDirectoryPath_label";
            this.projectsDirectoryPath_label.Size = new System.Drawing.Size(144, 13);
            this.projectsDirectoryPath_label.TabIndex = 9;
            this.projectsDirectoryPath_label.Text = "Директория проектов:";
            this.projectsDirectoryPath_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // projectsDirectoryPath_postfix_textBox
            // 
            this.projectsDirectoryPath_postfix_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.projectsDirectoryPath_postfix_textBox.Location = new System.Drawing.Point(263, 95);
            this.projectsDirectoryPath_postfix_textBox.Name = "projectsDirectoryPath_postfix_textBox";
            this.projectsDirectoryPath_postfix_textBox.Size = new System.Drawing.Size(303, 20);
            this.projectsDirectoryPath_postfix_textBox.TabIndex = 10;
            // 
            // projectsDirectoryPath_button
            // 
            this.projectsDirectoryPath_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.projectsDirectoryPath_button.Location = new System.Drawing.Point(572, 93);
            this.projectsDirectoryPath_button.Name = "projectsDirectoryPath_button";
            this.projectsDirectoryPath_button.Size = new System.Drawing.Size(94, 23);
            this.projectsDirectoryPath_button.TabIndex = 11;
            this.projectsDirectoryPath_button.Text = "Обзор...";
            this.projectsDirectoryPath_button.UseVisualStyleBackColor = true;
            this.projectsDirectoryPath_button.Click += new System.EventHandler(this.serverDirectory_button_Click);
            // 
            // projectsDirectoryPath_prefix_textBox
            // 
            this.projectsDirectoryPath_prefix_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.projectsDirectoryPath_prefix_textBox.Enabled = false;
            this.projectsDirectoryPath_prefix_textBox.Location = new System.Drawing.Point(153, 95);
            this.projectsDirectoryPath_prefix_textBox.Name = "projectsDirectoryPath_prefix_textBox";
            this.projectsDirectoryPath_prefix_textBox.Size = new System.Drawing.Size(104, 20);
            this.projectsDirectoryPath_prefix_textBox.TabIndex = 12;
            // 
            // name_textBox
            // 
            this.name_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.main_tableLayoutPanel.SetColumnSpan(this.name_textBox, 3);
            this.name_textBox.Location = new System.Drawing.Point(153, 5);
            this.name_textBox.Name = "name_textBox";
            this.name_textBox.Size = new System.Drawing.Size(513, 20);
            this.name_textBox.TabIndex = 2;
            // 
            // login_textBox
            // 
            this.login_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.main_tableLayoutPanel.SetColumnSpan(this.login_textBox, 3);
            this.login_textBox.Location = new System.Drawing.Point(153, 35);
            this.login_textBox.Name = "login_textBox";
            this.login_textBox.Size = new System.Drawing.Size(513, 20);
            this.login_textBox.TabIndex = 3;
            // 
            // password_textBox
            // 
            this.password_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.main_tableLayoutPanel.SetColumnSpan(this.password_textBox, 3);
            this.password_textBox.Location = new System.Drawing.Point(153, 65);
            this.password_textBox.Name = "password_textBox";
            this.password_textBox.PasswordChar = '*';
            this.password_textBox.Size = new System.Drawing.Size(513, 20);
            this.password_textBox.TabIndex = 8;
            this.password_textBox.UseSystemPasswordChar = true;
            // 
            // ProjectsServerSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "ProjectsServerSettingsControl";
            this.Size = new System.Drawing.Size(669, 123);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.main_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.Label login_label;
        private System.Windows.Forms.Label name_label;
        private System.Windows.Forms.Label projectsDirectoryPath_label;
        private System.Windows.Forms.TextBox projectsDirectoryPath_postfix_textBox;
        private System.Windows.Forms.Button projectsDirectoryPath_button;
        private System.Windows.Forms.TextBox projectsDirectoryPath_prefix_textBox;
        private System.Windows.Forms.TextBox name_textBox;
        private System.Windows.Forms.TextBox login_textBox;
        private System.Windows.Forms.TextBox password_textBox;

    }
}
