﻿namespace IA.Controls
{
    partial class ProjectListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_groupBox = new System.Windows.Forms.GroupBox();
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.main_listView = new System.Windows.Forms.ListView();
            this.main_groupBox.SuspendLayout();
            this.main_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_groupBox
            // 
            this.main_groupBox.Controls.Add(this.main_tableLayoutPanel);
            this.main_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_groupBox.Location = new System.Drawing.Point(0, 0);
            this.main_groupBox.Name = "main_groupBox";
            this.main_groupBox.Size = new System.Drawing.Size(268, 464);
            this.main_groupBox.TabIndex = 8;
            this.main_groupBox.TabStop = false;
            this.main_groupBox.Text = "Список проектов";
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.main_listView, 0, 0);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 2;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(262, 445);
            this.main_tableLayoutPanel.TabIndex = 2;
            // 
            // main_listView
            // 
            this.main_listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_listView.HideSelection = false;
            this.main_listView.Location = new System.Drawing.Point(3, 3);
            this.main_listView.MultiSelect = false;
            this.main_listView.Name = "main_listView";
            this.main_listView.Size = new System.Drawing.Size(256, 404);
            this.main_listView.TabIndex = 0;
            this.main_listView.UseCompatibleStateImageBehavior = false;
            this.main_listView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.Project_Click_EventHandler);
            // 
            // ProjectListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_groupBox);
            this.Name = "ProjectListControl";
            this.Size = new System.Drawing.Size(268, 464);
            this.main_groupBox.ResumeLayout(false);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox main_groupBox;
        private System.Windows.Forms.ListView main_listView;
        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
    }
}
