﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Plugin;

namespace IA.Controls
{
    using Plugin = IA.Plugin.Handler;
    using Plugins = IA.Plugin.Handlers;
    using AllPlugins = IA.Plugin.HandlersInitial;

    /// <summary>
    /// Класс, описывающий список плагинов
    /// </summary>
    public partial class PluginListControl : UserControl
    {
        /// <summary>
        /// Класс, описывающий ListView без автоматического проставления "галочки" возле элемента при двойном щелчке по нему.
        /// </summary>
        private class NoDoubleClickAutoCheckListview : ListView
        {
            /// <summary>
            /// Была ли попытка проставить "галочку" по двойному клику
            /// </summary>
            private bool isCheckFromDoubleClick = false;

            /// <summary>
            /// Перегруженный обработчик базового класса
            /// </summary>
            /// <param name="e"></param>
            protected override void OnItemCheck(ItemCheckEventArgs e)
            {
                if (this.isCheckFromDoubleClick)
                {
                    e.NewValue = e.CurrentValue;
                    this.isCheckFromDoubleClick = false;
                }
                else
                    base.OnItemCheck(e);
            }

            /// <summary>
            /// Перегруженный обработчик базового класса
            /// </summary>
            /// <param name="e"></param>
            protected override void OnMouseDown(MouseEventArgs e)
            {
                //Определяем имел ли место двойной щелчок
                if ((e.Button == MouseButtons.Left) && (e.Clicks > 1))
                    this.isCheckFromDoubleClick = true;

                base.OnMouseDown(e);
            }

            /// <summary>
            /// Перегруженный обработчик базового класса
            /// </summary>
            /// <param name="e"></param>
            protected override void OnKeyDown(KeyEventArgs e)
            {
                this.isCheckFromDoubleClick = false;
                base.OnKeyDown(e);
            }
        }

        /// <summary>
        /// Класс, реализующий сопоставление плигинов и иконок
        /// </summary>
        private class PluginIconCollection
        {
            /// <summary>
            /// Всевозможные ключи иконок плагина
            /// </summary>
            internal enum PluginIconKey
            {
                SIMPLE_GREY,
                SIMPLE_GREEN,
                SIMPLE_YELLOW,
                SIMPLE_RED,
                COMPLEX_GREY,
                COMPLEX_GREEN,
                COMPLEX_YELLOW,
                COMPLEX_RED
            }

            /// <summary>
            /// Набор изображений иконок
            /// </summary>
            internal ImageList ImageList { get; private set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            internal PluginIconCollection()
            {
                //Создаём список иконок для плагинов
                this.ImageList = new ImageList()
                {
                    ColorDepth = ColorDepth.Depth32Bit,
                    ImageSize = new Size(32, 32)
                };

                this.ImageList.Images.Add(PluginIconKey.SIMPLE_GREY.ToString(), IA.Controls.Properties.Resources.Plugin_Icon_Simple_Gray);
                this.ImageList.Images.Add(PluginIconKey.SIMPLE_GREEN.ToString(), IA.Controls.Properties.Resources.Plugin_Icon_Simple_Green);
                this.ImageList.Images.Add(PluginIconKey.SIMPLE_YELLOW.ToString(), IA.Controls.Properties.Resources.Plugin_Icon_Simple_Yellow);
                this.ImageList.Images.Add(PluginIconKey.SIMPLE_RED.ToString(), IA.Controls.Properties.Resources.Plugin_Icon_Simple_Red);
                this.ImageList.Images.Add(PluginIconKey.COMPLEX_GREY.ToString(), IA.Controls.Properties.Resources.Plugin_Icon_Complex_Gray);
                this.ImageList.Images.Add(PluginIconKey.COMPLEX_GREEN.ToString(), IA.Controls.Properties.Resources.Plugin_Icon_Complex_Green);
                this.ImageList.Images.Add(PluginIconKey.COMPLEX_YELLOW.ToString(), IA.Controls.Properties.Resources.Plugin_Icon_Complex_Yellow);
                this.ImageList.Images.Add(PluginIconKey.COMPLEX_RED.ToString(), IA.Controls.Properties.Resources.Plugin_Icon_Complex_Red);
            }

            /// <summary>
            /// Получить ключ иконки указанного плагина
            /// </summary>
            /// <param name="plugin">Плагин</param>
            /// <returns></returns>
            internal static PluginIconKey GetPluginIconKey(Plugin plugin)
            {
                switch (plugin.RunResult)
                {
                    case Plugin.ExecutionResult.NO:
                        return plugin.IsAllCompulsoryAndBeforeCompleted ?
                               (plugin.IsHasDependencies ? PluginIconKey.COMPLEX_YELLOW : PluginIconKey.SIMPLE_YELLOW) :
                               (plugin.IsHasDependencies ? PluginIconKey.COMPLEX_GREY : PluginIconKey.SIMPLE_GREY);
                    case Plugin.ExecutionResult.SUCCESS:
                        return plugin.IsHasDependencies ? PluginIconKey.COMPLEX_GREEN : PluginIconKey.SIMPLE_GREEN;
                    case Plugin.ExecutionResult.ERROR:
                        return plugin.IsHasDependencies ? PluginIconKey.COMPLEX_RED : PluginIconKey.SIMPLE_RED;
                    default:
                        throw new Exception(Plugin.Exceptions.UNKNOWN_EXECUTION_RESULT.Description());
                }
            }
        }

        #region Константы
        /// <summary>
        /// Наименование объекта класса
        /// </summary>
        public const string ObjectName = "Список плагинов";

        /// <summary>
        /// Ключ для операции "Назад"
        /// </summary>
        private const string BackOperationKey = "Back";

        /// <summary>
        /// Текст для операции "Назад"
        /// </summary>
        private const string BackOperationText = "Назад";

        /// <summary>
        /// Ключ начального разделителя набора операций с отдельным плагином
        /// </summary>
        private const string menuSeparatorPluginOperationsBeginKey = "Separator_Plugin_Operations_Begin";

        /// <summary>
        /// Ключ конечного разделителя набора операций с отдельным плагином
        /// </summary>
        private const string menuSeparatorPluginOperationsEndKey = "Separator_Plugin_Operations_End";

        /// <summary>
        /// Имя категории для выбранного плагина
        /// </summary>
        private const string ChoosedPluginCategoryName = "Выбранный плагин";

        #region Цвета
        /// <summary>
        /// Цвет элемента списка, возле которого невозможно снять отметку
        /// </summary>
        private Color CheckedOnlyItemColor = Color.Black;

        /// <summary>
        /// Цвет элемента списка, возле которого можно как ставить, так и снимать отметку
        /// </summary>
        private Color CheckedOrUncheckedItemColor = Color.DimGray;

        /// <summary>
        /// Цвет элемента списка, возле которого невозможно поставить отметку
        /// </summary>
        private Color UncheckedOnlyItemColor = Color.Silver;
        #endregion

        #endregion

        #region События и делегаты
        /// <summary>
        /// Делагат для обновления текущего списка плагинов
        /// </summary>
        public delegate void Delegate_ResfreshPlugin(Plugin plugin);

        /// <summary>
        /// Делегат для события - Одиночный щелчок по плагину в списке
        /// </summary>
        public delegate void PluginClickEventHandler(Plugin plugin);

        /// <summary>
        /// Событие - Одиночный щелчок по плагину в списке
        /// </summary>
        public event PluginClickEventHandler PluginClickEvent;

        /// <summary>
        /// Делегат для события - Нажали на клавишу\меню "Настроить плагин"
        /// </summary>
        public delegate void PluginSettingsEventHandler(object sender, Action action, Plugins plugins);

        /// <summary>
        /// Событие - Нажали на клавишу\меню "Настроить плагин"
        /// </summary>
        public event PluginSettingsEventHandler PluginSettingsEvent;

        /// <summary>
        /// Делегат для события - Нажали на клавишу\меню "Запустить плагин"
        /// </summary>
        public delegate void PluginRunEventHandler(object sender, Action action, Plugins plugins);

        /// <summary>
        /// Событие - Нажали на клавишу\меню "Запустить плагин"
        /// </summary>
        public event PluginRunEventHandler PluginRunEvent;

        /// <summary>
        /// Делегат для события - Нажали на клавишу\меню "Отчёт по плагину"
        /// </summary>
        public delegate void PluginReportEventHandler(object sender, Action action, Plugins plugins);

        /// <summary>
        /// Событие - Нажали на клавишу\меню "Отчёт по плагину"
        /// </summary>
        public event PluginReportEventHandler PluginReportEvent;

        /// <summary>
        /// Делегат для события - Нажали на клавишу\меню "Результат плагина"
        /// </summary>
        public delegate void PluginResultEventHandler(object sender, Action action, Plugin plugin);

        /// <summary>
        /// Событие - Нажали на клавишу\меню "Результат плагина"
        /// </summary>
        public event PluginResultEventHandler PluginResultEvent;

        /// <summary>
        /// Делегат для события - Уровень отображения списка плагинов изменился
        /// </summary>
        /// <param name="rootPlugin">Корневой плагин уровня</param>
        public delegate void LevelChangedEventHandler(Plugin rootPlugin);

        /// <summary>
        /// Событие - Уровень отображения списка плагинов изменился
        /// </summary>
        public event LevelChangedEventHandler LevelChangedEvent;

        /// <summary>
        /// Делегат для события - Снята\установлена отметка возле плагина
        /// </summary>
        /// <param name="plugin">Плагин</param>
        public delegate void PluginCheckedChangedEventHandler(Plugin plugin);

        /// <summary>
        /// Событие - Снята\установлена отметка возле плагина
        /// </summary>
        public event PluginCheckedChangedEventHandler PluginCheckedChangedEvent;
        #endregion

        #region Поля
        /// <summary>
        /// Всевозможные действия для кнопок и пунктов меню
        /// </summary>
        private Dictionary<string, Action> actions = null;

        /// <summary>
        /// История путешествия по плагинам. Не может быть Null.
        /// </summary>
        private Stack<Plugin> history = new Stack<Plugin>();

        /// <summary>
        /// Внутренний элемент управления - "Список плагинов"
        /// </summary>
        private ListView main_listView = null;

        /// <summary>
        /// Кнопка "Назад"
        /// </summary>
        private Button back_button = null;
        #endregion

        #region Свойства
        private bool isReadOnly = false;
        /// <summary>
        /// Включить\отключить режим просмотра списка плагинов.
        /// В данном режиме никакие операции над плагинами невозможны.
        /// </summary>
        public bool IsReadOnly
        {
            set
            {
                //Сохраняем переданное значение
                this.isReadOnly = value;

                //Прячем\отображаем чекбоксы
                if (value)
                    this.main_listView.CheckBoxes = false;

                //Прячем\отображаем разделители
                this.main_listView.ContextMenuStrip.Items[menuSeparatorPluginOperationsBeginKey].Visible = !value;
                this.main_listView.ContextMenuStrip.Items[menuSeparatorPluginOperationsEndKey].Visible = !value;

                //Прячем отображаем операции
                EnumLoop<IA.Plugin.Operation>.ForEach(operation => this.actions[operation.FullName()].Visible = !value);
            }
        }

        /// <summary>
        /// Включить\отключить режим блокирование списка плагинов. В режиме блокирования список плагинов отображается, но никакие действия не доступны.
        /// </summary>
        public bool IsBlocked
        {
            set
            {
                //Определяем доступность списка плагинов
                this.main_listView.Enabled = !value;

                // Если элемент управления разблокируется, переводим на него фокус
                if (!value)
                    this.main_listView.Focus();

                //Определяем доступность кнопки "Назад"
                this.actions[BackOperationKey].Enabled = !value && this.history.Count != 0;
            }
        }

        /// <summary>
        /// Включить\отключить доступность списка плагинов. В режиме недоступности список плагинов не отображается.
        /// </summary>
        public bool IsEnabled
        {
            set
            {
                main_splitContainer.Panel1Collapsed = !value;
                main_splitContainer.Panel2Collapsed = value;
            }
        }

        /// <summary>
        /// Текущая определённая последовательность для мультизапуска плагина
        /// </summary>
        public IA.Plugin.Sequences.SpecificMultiRunSequence SpecificMultiRunSequence
        {
            get
            {
                //В основном списке ни о какой такой последовательности речь не идёт
                if (history.Count == 0)
                    return null;

                //Получаем определённую последовательность для мультизапуска корневого плагина из абстрактной последовательности и отмеченных плагинов
                return history.Peek().AbstractMultiRunSequence.ToSpecificMultiRunSequence(
                    new HashSet<uint>(
                        this.main_listView.Items.Cast<ListViewItem>().Where(i => i.Checked).Select(i => this.PluginFromItem(i).ID)
                    )
                );
            }
        }
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        public PluginListControl()
        {
            InitializeComponent();

            //Инициализируем все возможные действия
            this.actions = this.Actions_Initialization();

            #region Инициализируем внутренний список плагинов
            this.main_listView = new NoDoubleClickAutoCheckListview()
            {
                CheckBoxes = false,
                ContextMenuStrip = this.Menu_Initialization(),
                Dock = DockStyle.Fill,
                HeaderStyle = ColumnHeaderStyle.None,
                HideSelection = false,
                ListViewItemSorter = new IA.Plugin.Comparers.ComparerByName() as System.Collections.IComparer,
                MultiSelect = false,
                ShowGroups = true,
                SmallImageList = new PluginIconCollection().ImageList,
                View = View.Details
            };

            //Создаём единственную колонку с плигинами
            const string nameColumnKey = "Name";
            this.main_listView.Columns.Add(nameColumnKey, nameColumnKey, this.main_listView.Width - 40);
            this.main_listView.SizeChanged += delegate (object o, EventArgs e)
            {
                this.main_listView.Columns[nameColumnKey].Width = this.main_listView.Width - SystemInformation.VerticalScrollBarWidth;
            };

            //Обработчики событий
            this.main_listView.ItemCheck += this.Plugin_Check_EventHandler;
            this.main_listView.ItemChecked += this.Plugin_Checked_EventHandler;
            this.main_listView.SelectedIndexChanged += this.Plugin_Click_EventHandler;
            this.main_listView.DoubleClick += this.Plugin_DoubleClick_EventHandler;

            //Добавляем элемент управления в таблицу
            this.main_tableLayoutPanel.Controls.Add(this.main_listView, 0, 0);
            #endregion

            #region Инициализируем кнопку "Назад"
            //Получаем кнопку из действия
            this.back_button = this.actions[BackOperationKey].ToButton();

            //Модернизируем её
            this.back_button.Dock = DockStyle.Fill;
            this.back_button.ImageAlign = ContentAlignment.MiddleCenter;
            this.back_button.TextAlign = ContentAlignment.MiddleRight;
            this.back_button.TextImageRelation = TextImageRelation.ImageBeforeText;

            //Добавляем на элемент управления
            main_tableLayoutPanel.Controls.Add(this.back_button, 0, 1);
            #endregion

            //По умолчанию список плагинов недоступен
            this.IsEnabled = false;
        }

        /// <summary>
        /// Загружаем полный список плагинов.
        /// Метод вызывается один раз после того как в приложениие были загружены все найденные плагины.
        /// </summary>
        public void LoadPlugins()
        {
            //Узнаём находились ли мы в корне
            bool isLevelChanged = history.Count != 0;

            //Генерируем список всех плагинов
            Generate();

            //Формируем событие о том, что уровень отображения изменился
            if (isLevelChanged && this.LevelChangedEvent != null)
                this.LevelChangedEvent(null);

            //Реагируем на изменение состояния каждого плагина
            AllPlugins.GetAll().ForEach(p => p.PluginRunResultChangedEvent += (state) => this.PluginChanged_ThreadSafe(p));
        }

        /// <summary>
        /// Инициализация всеовзможных действий
        /// </summary>
        private Dictionary<string, Action> Actions_Initialization()
        {
            //Формируем результат
            Dictionary<string, Action> ret = new Dictionary<string, Action>();

            //Операция "Назад"
            Image back_image = ImageExtensions.GetResizedCopy(IA.Controls.Properties.Resources.Common_Button_Back, new Size(16, 16));
            ret.Add(BackOperationKey, new Action() { Enabled = false, Key = BackOperationKey, Image = back_image, Text = BackOperationText, ShortcutKeys = Keys.Control | Keys.Z, ActionHandler = this.Common_Back_ActionHandler });

            //Операции над плагином
            EnumLoop<IA.Plugin.Operation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case IA.Plugin.Operation.SETTINGS:
                        ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Image = IA.Controls.Properties.Resources.Plugin_Button_Settings, Text = operation.Description(), ActionHandler = (sender, action) => this.Plugin_Operation_Click_ActionHandler(sender, action, operation) });
                        break;
                    case IA.Plugin.Operation.RUN:
                        ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Image = IA.Controls.Properties.Resources.Plugin_Button_Run, Text = operation.Description(), ActionHandler = (sender, action) => this.Plugin_Operation_Click_ActionHandler(sender, action, operation) });
                        break;
                    case IA.Plugin.Operation.RESULT:
                        ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Image = IA.Controls.Properties.Resources.Plugin_Button_Result, Text = operation.Description(), ActionHandler = (sender, action) => this.Plugin_Operation_Click_ActionHandler(sender, action, operation) });
                        break;
                    case IA.Plugin.Operation.REPORT:
                        ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Image = IA.Controls.Properties.Resources.Plugin_Button_Report, Text = operation.Description(), ActionHandler = (sender, action) => this.Plugin_Operation_Click_ActionHandler(sender, action, operation) });
                        break;
                    default:
                        throw new Exception(IA.Plugin.Exceptions.UNKNOWN_OPERATION.Description());
                }
            });

            return ret;
        }

        /// <summary>
        /// Сформировать контекстное меню
        /// </summary>
        /// <returns></returns>
        private ContextMenuStrip Menu_Initialization()
        {
            //Инициализируем результат
            ContextMenuStrip ret = new ContextMenuStrip();

            //Добавляем разделитель
            ret.Items.Add(new ToolStripSeparator() { Name = menuSeparatorPluginOperationsBeginKey });

            //Добавляем операции над плагином
            EnumLoop<IA.Plugin.Operation>.ForEach(operation => ret.Items.Add(this.actions[operation.FullName()].ToToolStripMenuItem()));

            //Добавляем разделитель
            ret.Items.Add(new ToolStripSeparator() { Name = menuSeparatorPluginOperationsEndKey });

            return ret;
        }

        /// <summary>
        /// Обновить плагин в списке плагинов
        /// </summary>
        private void RefreshPlugin(Plugin plugin)
        {
            //Получаем элемент списка, соответствующий плагину
            ListViewItem item = this.ItemFromPlugin(plugin);

            //Если такого плагина нет в списке, ничего не делаем
            if (item == null)
                return;

            //Обновляем элемент списка
            item.Text = plugin.Name;
            item.ImageKey = PluginIconCollection.GetPluginIconKey(plugin).ToString();
        }

        /// <summary>
        /// Обновить плагин и все зависящие от него плагины в списке плагинов
        /// </summary>
        /// <param name="plugin"></param>
        private void RefreshPluginAndAllCompulsoryDependencies(Plugin plugin)
        {
            //Обновляем сам плагин
            this.RefreshPlugin(plugin);

            //Обновляем все плагины зависящие от заданного
            AllPlugins.GetSomeByCondition(p => p.IsDirectlyAndCompulsoryDepends(plugin.ID)).ForEach(p => this.RefreshPlugin(p));
        }

        /// <summary>
        /// Генерируем список всех плагинов
        /// </summary>
        private void Generate()
        {
            //Проверка на разумность
            if (history.Count != 0)
                throw new Exception("Недопустимая ситуация. История переходов между плагинами не пуста.");

            //Отключаем отметки
            if (this.main_listView.CheckBoxes)
                this.main_listView.CheckBoxes = false;

            //Очищаем список плагинов
            Clear();

            //Получаем все плагины
            Plugins plugins = AllPlugins.GetAll();

            //Сортируем плагины
            plugins.Sort(new IA.Plugin.Comparers.ComparerByName());

            Handlers.settingsChecker.Reset();
            //Добавляем все плагины в список
            foreach (Plugin plugin in plugins)
            {
                AddPlugin(plugin, plugin.Category);
                Handlers.settingsChecker.Add(plugin.ID, (plugin.Capabilities & Capabilities.CUSTOM_SETTINGS_WINDOW) != Capabilities.CUSTOM_SETTINGS_WINDOW);
            }

            //Формируем событие о том, что ничего не выбрано
            if (this.PluginClickEvent != null)
                this.PluginClickEvent(null);
        }

        /// <summary>
        /// Генерируем список плагинов для запуска в мультирежиме
        /// </summary>
        /// <param name="rootPlugin"></param>
        private void Generate(Plugin rootPlugin)
        {
            //Включаем отметки
            if (!this.isReadOnly && !this.main_listView.CheckBoxes)
                this.main_listView.CheckBoxes = true;

            //Очищаем список плагинов
            Clear();

            //Корневой элемент
            ListViewItem rootItem = null;

            //Добавляем плагин и все его зависимости в последовательности запуска
            foreach (Plugin plugin in rootPlugin.AbstractMultiRunSequence)
            {
                //Добавляем плагинв в список и получаем соответствующий ему элемент
                ListViewItem item = AddPlugin(plugin, "Мультизапуск плагина <" + rootPlugin.Name + ">");

                //Расрашиваем элемент в соответствии с его типом
                if (plugin.Equals(rootPlugin))
                {
                    //Раскрашиваем
                    item.ForeColor = CheckedOnlyItemColor;

                    //Запоминаем
                    rootItem = item;
                }
                //Если это прямая зависимость
                else if (rootPlugin.IsDirectlyDepends(plugin.ID))
                {
                    //Если это обязтельная зависимость
                    if (rootPlugin.IsDirectlyAndCompulsoryDepends(plugin.ID))
                    {
                        //Галочка стоит. Галочку снимать нельзя.
                        item.ForeColor = CheckedOnlyItemColor;
                    }
                    //Если это опциональная зависимость
                    else
                    {
                        //Галочка не стоит. Но поставить галочку можно.
                        item.ForeColor = CheckedOrUncheckedItemColor;
                    }
                }
                //Если это непрямая зависимость
                else if (rootPlugin.IsDepends(plugin.ID))
                {
                    //Если это обязтельная зависимость
                    if (rootPlugin.IsCompulsoryDepends(plugin.ID))
                    {
                        //Галочка стоит. Галочку снимать нельзя.
                        item.ForeColor = CheckedOnlyItemColor;
                    }
                    //Если это опциональная зависимость
                    else
                    {
                        //Галочка не стоит. Поставить галочку нельзя.
                        item.ForeColor = UncheckedOnlyItemColor;
                    }
                }
                //Если это вовсе не зависимость
                else
                    //Формируем исключение
                    throw new Exception("Недопустимая ситуация. Корневой плагин никак не зависит от плагина в списке для запуска.");
            }

            //Отмечаем корневой элемент (остальные зависимости отмечаются рекурсивно)
            rootItem.Checked = true;

            //Формируем событие о том, что ничего не выбрано
            if (this.PluginClickEvent != null)
                this.PluginClickEvent(null);
        }
        
        /// <summary>
        /// Очистка списка плагинов
        /// </summary>
        private void Clear()
        {
            this.main_listView.Items.Clear();
            this.main_listView.Groups.Clear();
        }

        /// <summary>
        /// Сбросить список плагинов
        /// </summary>
        public void Reset()
        {
            //Очищаем историю переходов
            history.Clear();

            //Узнаём находились ли мы в корне
            bool isLevelChanged = history.Count != 0;

            //Генерируем список плагинов заново
            Generate();

            //Формируем событие о том, что уровень отображения изменился
            if (isLevelChanged && this.LevelChangedEvent != null)
                this.LevelChangedEvent(null);
        }

        /// <summary>
        /// Добавляем категорию в список плагинов
        /// </summary>
        /// <param name="name"></param>
        private ListViewGroup AddCategory(string name)
        {
            if (this.main_listView.Groups[name] != null)
                return this.main_listView.Groups[name];

            ListViewGroup group = new ListViewGroup()
            {
                Name = name,
                Header = name,
                HeaderAlignment = HorizontalAlignment.Center
            };

            this.main_listView.Groups.Add(group);

            return group;
        }

        /// <summary>
        /// Добавляем плагин в список плагинов
        /// </summary>
        /// <param name="plugin">Плагин. Не может быть Null.</param>
        /// <param name="categoryName">Имя категории плагина. Не может быть Null.</param>
        /// <returns></returns>
        private ListViewItem AddPlugin(Plugin plugin, string categoryName)
        {
            //Создаём элемент плагина
            ListViewItem item = new ListViewItem()
            {
                Name = plugin.ID.ToString(),
                Text = plugin.Name,
                ImageKey = PluginIconCollection.GetPluginIconKey(plugin).ToString(),
                Group = this.main_listView.Groups[categoryName] == null ? AddCategory(categoryName) : this.main_listView.Groups[categoryName],
            };

            //Добавляем элемент в список
            this.main_listView.Items.Add(item);

            return item;
        }

        /// <summary>
        /// Обработчик - изменилось состояние плагина(в плане выполнения)
        /// </summary>
        public void PluginChanged_ThreadSafe(Plugin plugin)
        {
            //Обновляем список плагинов
            if (this.InvokeRequired)
                this.Invoke(new Delegate_ResfreshPlugin(this.RefreshPluginAndAllCompulsoryDependencies), new object[1] { plugin });
            else
                this.RefreshPluginAndAllCompulsoryDependencies(plugin);
        }

        /// <summary>
        /// Получить плагин по элементу списка.
        /// </summary>
        /// <param name="item">Элемент списка</param>
        /// <returns>Плагин. Null, если что-то пошло не так.</returns>
        private Plugin PluginFromItem(ListViewItem item)
        {
            return item == null ? null : AllPlugins.GetOneByID(UInt32.Parse(item.Name));
        }

        /// <summary>
        /// Получить элемент списка по указанному плагину.
        /// </summary>
        /// <param name="plugin">Плагин.</param>
        /// <returns>Элемент списка. Null, если в списке нет элемента, соответствующего указнному плагину.</returns>
        private ListViewItem ItemFromPlugin(Plugin plugin)
        {
            //Если плагин не определён
            if (plugin == null)
                return null;

            //Ищем элемент в списке
            return this.main_listView.Items[plugin.ID.ToString()];
        }

        #region Обработчики событий
        /// <summary>
        /// Обработчик - Одиночный щелчок по плагину
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Plugin_Click_EventHandler(object sender, EventArgs e)
        {
            try
            {
                //Получаем держатель плагина из тэга
                Plugin plugin = this.main_listView.SelectedItems.Count == 1 ? this.PluginFromItem(this.main_listView.SelectedItems[0]) : null;

                if (!this.isReadOnly)
                {
                    bool isReadyToRun = plugin != null ? plugin.IsReadyToRun : false;
                    //Определяем доступность операций над плагином
                    EnumLoop<IA.Plugin.Operation>.ForEach(operation =>
                    {
                        switch (operation)
                        {
                            case IA.Plugin.Operation.SETTINGS:
                                this.main_listView.ContextMenuStrip.Items[operation.FullName()].Enabled = plugin != null && plugin.IsReadyToShowCustomSettingsWindow;
                                break;
                            case IA.Plugin.Operation.RUN:
                                this.main_listView.ContextMenuStrip.Items[operation.FullName()].Enabled = isReadyToRun;
                                break;
                            case IA.Plugin.Operation.REPORT:
                                this.main_listView.ContextMenuStrip.Items[operation.FullName()].Enabled = plugin != null && plugin.IsReadyToGenerateReport;
                                break;
                            case IA.Plugin.Operation.RESULT:
                                this.main_listView.ContextMenuStrip.Items[operation.FullName()].Enabled = plugin != null && plugin.IsReadyToShowResultWindow;
                                break;
                            default:
                                throw new Exception(IA.Plugin.Exceptions.UNKNOWN_OPERATION.Description());
                        }
                    });
                }
                //Формируем событие
                if (PluginClickEvent != null)
                    PluginClickEvent(plugin);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Однократное нажатие на плагин в списке.", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Двойной щелчок по плагину
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plugin_DoubleClick_EventHandler(object sender, EventArgs e)
        {
            try
            {
                //Если выбрано более одного плагина, ничего не делаем
                if (this.main_listView.SelectedItems.Count != 1)
                    return;

                //Получаем держатель плагина из тэга
                Plugin plugin = this.PluginFromItem(this.main_listView.SelectedItems[0]);

                //Если плагин является корневым, то ничего не делаем
                if (history.Count > 0 && plugin.Equals(history.Peek()))
                    return;

                //Храним историю переходов
                history.Push(plugin);

                //Генерируем список зависимых плагинов
                Generate(plugin);

                //Делаем доступной кнопку "Назад"
                this.actions[BackOperationKey].Enabled = true;

                //Формируем событие о том, что уровень отображения изменился
                if (this.LevelChangedEvent != null)
                    this.LevelChangedEvent(plugin);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Двукратное нажатие на плагин в списке.", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Попытка поставить\снять галочку напротив плагина в списке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plugin_Check_EventHandler(object sender, ItemCheckEventArgs e)
        {
            try
            {
                //Если мы находимся в корне, формируем исключение
                if (history.Count == 0)
                    throw new Exception("Недопустимая ситуация. В общем списке плагинов отмечать элементы нельзя.");

                //Получаем текущий элемент
                ListViewItem currentItem = this.main_listView.Items[e.Index];

                //Рассматриваем все возможные случаи
                switch (e.NewValue)
                {
                    case CheckState.Checked:
                        {
                            //Если нельзя устанавливать отметку возле данного элемента
                            if (currentItem.ForeColor == UncheckedOnlyItemColor)
                                //Принудительно снимаем отметку
                                e.NewValue = CheckState.Unchecked;

                            break;
                        }
                    case CheckState.Unchecked:
                        {
                            //Если нельзя снимать отметку с данного элемента
                            if (currentItem.ForeColor == CheckedOnlyItemColor)
                                //Принудительно устанавливаем отметку
                                e.NewValue = CheckState.Checked;

                            break;
                        }
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Попытка посатвить\\снять галочку напротив плагина в списке.", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Поставили\сняли галочку напротив плагина в списке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plugin_Checked_EventHandler(object sender, ItemCheckedEventArgs e)
        {
            try
            {
                //Если мы находимся в корне, формируем исключение
                if (history.Count == 0)
                    throw new Exception("Недопустимая ситуация. В общем списке плагинов отмечать элементы нельзя.");

                //Получаем корневой плагин в наборе для запуска
                Plugin rootPlugin = history.Peek();

                //Получаем текущий плагин
                Plugin currentPlugin = this.PluginFromItem(e.Item);

                //Формируем событие
                if (this.PluginCheckedChangedEvent != null)
                    this.PluginCheckedChangedEvent(currentPlugin);

                //Если галочку установили
                if (e.Item.Checked)
                {
                    //Проходим по всем плагинам, от которых зависит текущий
                    foreach (Plugin plugin in currentPlugin.AbstractMultiRunSequence)
                    {
                        //Получаем элемент по ключу
                        ListViewItem item = this.ItemFromPlugin(plugin);

                        //Если такого элемента нет, ничего не делаем
                        if (item == null)
                            continue;

                        //Для тех, от кого напрямую зависим
                        if (currentPlugin.IsDirectlyDepends(plugin.ID))
                        {
                            //Если зависимость обязательная
                            if (currentPlugin.IsDirectlyAndCompulsoryDepends(plugin.ID))
                            {
                                //Если элемент допускает возможность отсутствия отметки
                                if (item.ForeColor != CheckedOnlyItemColor)
                                    //Расскрашиваем элемент: элемент помечен, снять отметку нельзя.
                                    item.ForeColor = CheckedOnlyItemColor;

                                //Устанавливает отметку
                                item.Checked = true;
                            }
                            //Если зависимость опциональная
                            else
                            {
                                //Если элемент не был отмечен без возможности установить отметку
                                if (item.ForeColor == UncheckedOnlyItemColor)
                                    //Расскрашиваем элемент: отметку можно как снять так и поставить.
                                    item.ForeColor = CheckedOrUncheckedItemColor;
                            }
                        }
                    }
                }
                //Если галочку сняли
                else
                {
                    //Проходим по всем плагинам в списке
                    foreach (Plugin plugin in rootPlugin.AbstractMultiRunSequence)
                    {
                        //Получаем элемент по ключу
                        ListViewItem item = this.ItemFromPlugin(plugin);

                        //Если такого элемента нет, ничего не делаем
                        if (item == null)
                            continue;

                        //Если галочка и так снята, ничего не делаем
                        if (item.ForeColor == UncheckedOnlyItemColor)
                            continue;

                        //Если рассматриваемый плагин является прямой зависимостью текущего плагина
                        if (currentPlugin.IsDirectlyDepends(plugin.ID))
                        {
                            //Получаем отмеченные плагины, напрямую зависящие от рассматриваемого плагина
                            Plugins checkedAndDirectlyDependsPlugins = new Plugins(
                                rootPlugin.AbstractMultiRunSequence.Where(p =>
                                {
                                    //Получаем элемент по идентификатору
                                    ListViewItem i = this.ItemFromPlugin(p);

                                    return i != null && i.Checked && p.IsDirectlyDepends(plugin.ID);
                                })
                            );

                            //Если таких плагинов нет, снимаем отметку
                            if (checkedAndDirectlyDependsPlugins.Count == 0)
                            {
                                //Расскрашиваем элемент: элемент не помечен, установить отметку нельзя.
                                item.ForeColor = UncheckedOnlyItemColor;

                                //Снимаем отметку с элемента
                                item.Checked = false;
                            }
                            //В противном случае
                            else
                            {
                                //Если для всех таких плагинов рассматриваемый плагин является опциональной зависимостью
                                if (checkedAndDirectlyDependsPlugins.All(p => !p.IsDirectlyAndCompulsoryDepends(plugin.ID)))
                                    //Если элемент не допускает снятие или установку отметки
                                    if (item.ForeColor != CheckedOrUncheckedItemColor)
                                        //Расскрашиваем элемент: отметку можно как снять так и поставить.
                                        item.ForeColor = CheckedOrUncheckedItemColor;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Установка\\снятие галочки напротив плагина в списке.", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        #endregion

        #region Обработчики действий
        /// <summary>
        /// Обрабочик - Назад
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        private void Common_Back_ActionHandler(object sender, Action action)
        {
            try
            {
                //Проверка на разумность
                if (history.Count == 0)
                    throw new Exception("Недопустимая ситуация. История переходов между плагинами не пуста.");

                //Получаем предыдущий корневой элемент
                Plugin previousRootPlugin = history.Pop();

                //Возвращаемся на шаг назад
                if (history.Count == 0)
                {
                    //Если мы в основном списке, делаем кнопку "Назад" недоступной
                    this.actions[BackOperationKey].Enabled = false;

                    //Генерируем полный список плагинов
                    Generate();

                    //Формируем событие о том, что уровень отображения изменился
                    if (this.LevelChangedEvent != null)
                        this.LevelChangedEvent(null);
                }
                else
                {
                    //Получаем текущий корневой элемент
                    Plugin currentRootPlugin = history.Peek();

                    //Генерируем список зависимых плагинов от текущего
                    Generate(currentRootPlugin);

                    //Формируем событие о том, что уровень отображения изменился
                    if (this.LevelChangedEvent != null)
                        this.LevelChangedEvent(currentRootPlugin);
                }

                //Получаем элемент, соответствующий плагину, от которого мы вернулись
                ListViewItem previousRootPluginItem = this.ItemFromPlugin(previousRootPlugin);

                //Проверка на разумность
                if (previousRootPluginItem == null)
                    throw new Exception("Недопустимая ситуация. Предыдущего по истории корневого плагина нет в списке.");

                //Убаждаемся, что элемент, соответствующий плагину, от которого мы вернулись, виден
                this.main_listView.EnsureVisible(previousRootPluginItem.Index);

                //Выбираем элемент, соответствующий плагину, от которого мы вернулись
                previousRootPluginItem.Selected = true;

                //Принудительно переводим фокус на список
                this.main_listView.Focus();
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Щелчок по операции над плагином
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="operation">Операция</param>
        private void Plugin_Operation_Click_ActionHandler(object sender, Action action, IA.Plugin.Operation operation)
        {
            try
            {
                //Если ничего не выбрано, ругаемся
                if (this.main_listView.SelectedIndices.Count != 1)
                    throw new Exception("Недопустимая ситуация. Ничего не выбрано или выбрано более одного плагина в списке.");

                //Получаем выбранный плагин
                Plugin pluginToProcess = this.PluginFromItem(this.main_listView.SelectedItems[0]);

                //Проверка на то, что элемент соответствует какому-либо плагину
                if (pluginToProcess == null)
                    throw new Exception("Недопустимая ситуация. Элемент в списке не соответствует ни одному из плагинов.");

                //Формируем список плагинов для обработки
                Handlers pluginsToProcess = new Handlers() { pluginToProcess };

                switch (operation)
                {
                    case IA.Plugin.Operation.SETTINGS:
                        {
                            if (PluginSettingsEvent != null)
                                PluginSettingsEvent(sender, action, pluginsToProcess);
                            break;
                        }
                    case IA.Plugin.Operation.RUN:
                        {
                            if (PluginRunEvent != null)
                                PluginRunEvent(sender, action, pluginsToProcess);
                            break;
                        }
                    case IA.Plugin.Operation.REPORT:
                        {
                            if (PluginReportEvent != null)
                                PluginReportEvent(sender, action, pluginsToProcess);
                            break;
                        }
                    case IA.Plugin.Operation.RESULT:
                        {
                            if (PluginResultEvent != null)
                                PluginResultEvent(sender, action, pluginToProcess);
                            break;
                        }
                    default:
                        throw new Exception(IA.Plugin.Exceptions.UNKNOWN_OPERATION.Description());
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        #endregion
    }
}
