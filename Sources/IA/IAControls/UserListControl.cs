﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Objects;

namespace IA.Controls
{
    /// <summary>
    /// Реализация элемента управления списка пользователей
    /// </summary>
    public partial class UserListControl : UserControl
    {
        /// <summary>
        /// Идентификатор текущего пользователя
        /// </summary>
        private uint currentUserID = 0;

        /// <summary>
        /// Всевозможные действия для кнопок и пунктов меню
        /// </summary>
        private Dictionary<string, Action> actions = null;

        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Список пользователей";

        /// <summary>
        /// Элемент управления - Таблица с кнопками
        /// </summary>
        private TableLayoutPanel buttons_tableLayoutPanel = null;

        #region Делегаты и события
        /// <summary>
        /// Делегат для события - Нажали по пользователю
        /// </summary>
        /// <param name="user"></param>
        public delegate void UserClickEventHandler(User user);

        /// <summary>
        /// Событие - Нажали по пользователю
        /// </summary>
        public event UserClickEventHandler UserClickEvent;
        #endregion

        /// <summary>
        /// Констркутор
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        /// <param name="currentUserID">Идетификатор текущего пользователя</param>
        public UserListControl(uint currentUserID)
        {
            //Запоминаем идентификатор текущего пользователя
            this.currentUserID = currentUserID;

            InitializeComponent();

            //Инициализируем все возможные действия
            this.actions = this.Actions_Initialization();

            //Иконки пользователей
            ImageList imageList16 = new ImageList() { ColorDepth = ColorDepth.Depth32Bit, ImageSize = new Size(16, 16) };
            //FIXME - поменять рисунок иконки пользователя
            imageList16.Images.Add("Default", Properties.Resources.User_Icon);

            //Инициализация списка пользователей
            this.main_listView.View = View.Details;
            this.main_listView.HeaderStyle = ColumnHeaderStyle.None;
            this.main_listView.MultiSelect = false;
            this.main_listView.SmallImageList = imageList16;
            this.main_listView.Sorting = SortOrder.Ascending;

            //Создаём единственную колонку с ФИО пользователя
            this.main_listView.Columns.Add("Fullname", "ФИО", main_listView.Width - 40);
            this.main_listView.SizeChanged += delegate(object o, EventArgs e) { main_listView.Columns["Fullname"].Width = main_listView.Width - 40; };

            //Добавляем необходимые обработчики
            this.main_listView.ItemSelectionChanged += (sender, e) => this.User_Click_EventHandler(sender, e);

            //Контекстное меню
            this.main_listView.ContextMenuStrip = this.Menu_Initialization();

            //Инициализация элемента управления - таблица кнопок
            int buttonsCount = Enum.GetValues(typeof(Project.EditOperation)).Length;

            this.buttons_tableLayoutPanel = new TableLayoutPanel()
            {
                Dock = DockStyle.Fill,
                RowCount = 1,
                ColumnCount = buttonsCount
            };

            this.buttons_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100));

            //Добавляем кнопку для каждой операции редактирования пользователя
            int columnSizePercentValue = 100 / buttonsCount;
            EnumLoop<User.EditOperation>.ForEach(operation =>
            {
                this.buttons_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, columnSizePercentValue));

                //Получаем кнопку
                Button button = this.actions[operation.FullName()].ToButton();

                //Модернизируем её
                button.Dock = DockStyle.Fill;

                //Добавляем кнопку в таблицу кнопок
                this.buttons_tableLayoutPanel.Controls.Add(button);
            });

            this.main_tableLayoutPanel.Controls.Add(this.buttons_tableLayoutPanel, 0, 1);
            
            Sql.SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);
        }

        /// <summary>
        /// Инициализация контекстного меню
        /// </summary>
        /// <returns></returns>
        private ContextMenuStrip Menu_Initialization()
        {
            ContextMenuStrip ret = new ContextMenuStrip();

            EnumLoop<User.EditOperation>.ForEach(operation => ret.Items.Add(this.actions[operation.FullName()].ToToolStripMenuItem()));

            return ret;
        }              

        /// <summary>
        /// Инициализация всеовзможных действий
        /// </summary>
        private Dictionary<string, Action> Actions_Initialization()
        {
            //Формируем результат
            Dictionary<string, Action> ret = new Dictionary<string, Action>();

            //Все операции редактирования пользователя
            EnumLoop<User.EditOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case User.EditOperation.ADD:
                        ret.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), ActionHandler = this.User_Add_ActionHandler });
                        break;
                    case User.EditOperation.REMOVE:
                        ret.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), ActionHandler = this.User_Remove_ActionHandler });
                        break;
                    case User.EditOperation.EDIT:
                        ret.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), ActionHandler = this.User_Edit_ActionHandler });
                        break;
                    default:
                        throw new Exception(User.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                }
            });

            return ret;
        }

        /// <summary>
        /// Форомирование списка пользователей
        /// Считается, что соединение с базой данных уже установленно
        /// </summary>
        private void Generate()
        {
            //Объявляем изначальную доступность операций с пользователем
            EnumLoop<User.EditOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case User.EditOperation.ADD:
                        this.actions[operation.FullName()].Enabled = true;
                        break;
                    case User.EditOperation.REMOVE:
                    case User.EditOperation.EDIT:
                        this.actions[operation.FullName()].Enabled = false;
                        break;
                    default:
                        throw new Exception(User.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                }
            });

            //Очищаем список пользователей
            this.Clear();

            //Считываем информацию из БД
            foreach (User user in User.GetAll().Where(u => !u.IsDeleted))
                //Добавляем всех пользователей (не помеченных, как удалённые) из БД в список
                AddItem(user);
        }

        /// <summary>
        /// Очистить список пользователей
        /// </summary>
        private void Clear()
        {
            main_listView.Items.Clear();
        }

        /// <summary>
        /// Добавить пользователя в список
        /// </summary>
        /// <param name="user"></param>
        private ListViewItem AddItem(User user)
        {
            //Проверка на разумность
            if (user == null)
                throw new Exception("Невозможно добавить пользователя. Пользователь не определён.");

            //Создаём элемент
            ListViewItem ret = new ListViewItem()
            {
                ImageKey = "Default",
                Name = user.ID.ToString(),
                Text = user.Fullname,
                Tag = user
            };

            //Добавляем элемент в список
            main_listView.Items.Add(ret);

            return ret;
        }

        /// <summary>
        /// Обновить информацию о пользователе в списке
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private ListViewItem UpdateItem(User user)
        {
            //Проверка на разумность
            if (user == null)
                throw new Exception("Невозможно обновить информацию о пользователе. Пользователь не определён.");

            //Получаем элемент списка для обновления
            ListViewItem ret = main_listView.Items[user.ID.ToString()];

            //Проверка на разумность
            if (ret == null)
                throw new Exception("Невозможно обновить информацию о пользователе. Пользователь отсутствует в списке.");

            //Обновляем элемент списка
            ret.Text = user.Fullname;
            ret.Tag = user;

            return ret;
        }

        #region Обработчики событий
        /// <summary>
        /// Обработчик - На пользователя нажали один раз
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void User_Click_EventHandler(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            try
            {
                //Задаём активность действий в зависимости от выбранного пользователя
                EnumLoop<User.EditOperation>.ForEach(operation =>
                {
                    switch (operation)
                    {
                        case User.EditOperation.ADD:
                            this.actions[operation.FullName()].Enabled = !e.IsSelected;
                            break;
                        case User.EditOperation.EDIT:
                        case User.EditOperation.REMOVE:
                            this.actions[operation.FullName()].Enabled = e.IsSelected;
                            break;
                        default:
                            throw new Exception(Project.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                    }
                });

                //Формируем событие
                if (UserClickEvent != null)
                    UserClickEvent(e.IsSelected ? e.Item.Tag as User : null);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Однократное нажатие на пользователя в списке", (message) => Monitor.Log.Error(UserListControl.ObjectName, message, true));

                return;
            }
        } 
        #endregion

        #region Обработчики действий
        /// <summary>
        /// Обработчик - Добавить пользователя
        /// </summary>
        private void User_Add_ActionHandler(object sender, Action action)
        {
            try
            {
                //Создаём форму добавления информации о пользователе
                Form userAddOrEditDialog = new Form()
                {
                    Icon = this.ParentForm.Icon,
                    StartPosition = FormStartPosition.CenterParent,
                    Size = new System.Drawing.Size(450, 280),
                    Text = "Добавление информации о пользователе",
                };

                //Создаём элемент управления добавления информации о пользователе
                UserAddOrEditControl userAddControl = new UserAddOrEditControl()
                {
                    Dock = DockStyle.Fill
                };

                //Добавляем обработчик на кнопку "Готово"
                userAddControl.ReadyEvent += delegate(User user)
                {
                    //Добавляем вновь созданного пользователя в список
                    this.AddItem(user).Selected = true;

                    //Говорим о положительном результате
                    userAddOrEditDialog.DialogResult = DialogResult.OK;

                    //Закрываем форму создания пользователя
                    userAddOrEditDialog.Close();
                };

                //Добавляем элемент управления добавления информации о пользователе на форму добавления информации о пользователе
                userAddOrEditDialog.Controls.Add(userAddControl);

                //Отображаем форму добавления информации о пользователе
                if (userAddOrEditDialog.ShowDialog() != DialogResult.OK)
                    return;

                //Принудительно переводим фокус на список
                this.main_listView.Focus();
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
               ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Отредактировать информацию о пользователе
        /// </summary>
        private void User_Edit_ActionHandler(object sender, Action action)
        {
            try
            {
                //Проверка на разумность
                if (main_listView.SelectedItems == null || main_listView.SelectedItems.Count != 1)
                    throw new Exception("Ничего не выбрано или выбрано более одного элемента списка.");

                //Получаем объект пользователя для редактирования
                User userToEdit = main_listView.SelectedItems[0].Tag as User;

                //Если мы пытаемся редактировать информацию о текущем пользователе
                if (userToEdit.ID == this.currentUserID)
                {
                    Monitor.Log.Error(UserListControl.ObjectName, "В данный момент ведётся работа под этим пользователем. Редактирование невозможно.", true);
                    return;
                }

                //Создаём форму редактирования информации о пользователе
                Form userAddOrEditDialog = new Form()
                {
                    Icon = this.ParentForm.Icon,
                    StartPosition = FormStartPosition.CenterParent,
                    Size = new System.Drawing.Size(450, 280),
                    Text = "Редактирование информации о пользователе",
                };

                //Создаём элемент управления редактирования информации о пользователе
                UserAddOrEditControl userEditControl = new UserAddOrEditControl(userToEdit)
                {
                    Dock = DockStyle.Fill
                };

                //Добавляем обработчик на кнопку "Готово"
                userEditControl.ReadyEvent += delegate(User user)
                {
                    //Делаем отредактированного пользователя активным
                    this.UpdateItem(user).Selected = true;

                    //Говорим о положительном результате
                    userAddOrEditDialog.DialogResult = DialogResult.OK;

                    //Закрываем форму редактирования пользователя
                    userAddOrEditDialog.Close();
                };

                //Добавляем элемент управления редактирования информации о пользователе на форму редактирования информации о пользователе
                userAddOrEditDialog.Controls.Add(userEditControl);

                //Снимаем выделение
                foreach (ListViewItem item in this.main_listView.SelectedItems)
                    item.Selected = false;

                //Отображаем форму редактирования информации о пользователе
                if (userAddOrEditDialog.ShowDialog() != DialogResult.OK)
                    return;
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
               ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Удалить пользователя
        /// </summary>
        private void User_Remove_ActionHandler(object sender, Action action)
        {
            //Объект пользователя, который требуется удалить
            User userToRemove = null;

            try
            {
                //Проверка на разумность
                if (main_listView.SelectedItems == null || main_listView.SelectedItems.Count != 1)
                    throw new Exception("Пользователь не выбран или выбраны несколько пользователей для удаления.");

                //Получаем объект пользователя, который требуется удалить
                userToRemove = main_listView.SelectedItems[0].Tag as User;

                //Если мы пытаемся удалить информацию о текущем пользователе
                if (userToRemove.ID == this.currentUserID)
                {
                    Monitor.Log.Error(UserListControl.ObjectName, "В данный момент ведётся работа под этим пользователем. Удаление невозможно.", true);
                    return;
                }

                //Помечаем проект в Д, как удалённый
                userToRemove.IsDeleted = true;
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));

                return;
            }

            //Удаляем пользователя из списка пользователей
            if (userToRemove != null)
                main_listView.Items.RemoveByKey(userToRemove.ID.ToString());
        } 
        #endregion
        
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления в соответствии с содержимым базы данных
            this.Generate();

            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Делаем его недоступным
            this.Enabled = false;
        }
    }
}
