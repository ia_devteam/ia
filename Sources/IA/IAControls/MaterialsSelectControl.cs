﻿using System.Windows.Forms;
using IA.Objects;
using System.Drawing;

namespace IA.Controls
{
    /// <summary>
    /// Класс, реализующий контрол выбора набора материалов
    /// </summary>
    public partial class MaterialsSelectControl : UserControl
    {        
        /// <summary>
        /// Обновиить контрол
        /// </summary>
        /// <param name="project"></param>
        public void ShakeForm(Project project)
        {
            materialsListControl.ShakeForm(project, Properties.Settings.Default.LastMaterials);
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public MaterialsSelectControl()
        {
            InitializeComponent();
            //this.parent = parent;
            //while (this.parent.Parent != null)
            //    this.parent = this.parent.Parent;

            pBox.Image.RotateFlip(RotateFlipType.Rotate270FlipNone);
        }

        /// <summary>
        /// Обработчик - Набор материалов выбран
        /// </summary>
        /// <param name="materials"></param>
        private void materialsListControl_MaterialsChosen(Materials materials)
        {
            Properties.Settings.Default.LastMaterials = materials.ID;
            Properties.Settings.Default.Save();

            storageSelectControl.Rebuild(materials, IAUtils.IACommon.CurrentUser);            

            // если материалы открыты первый раз, то диалоговое окно выбора хранилища ингорируется. 
            if (!storageSelectControl.ContainsStorages)
            {
                // сообщим пользователю об открытии хранилища
                Monitor.Log.Information("Открытие хранилища", "Открытие хранилища осуществлено успешно");
            }
            Result = materials;
        }

        /// <summary>
        /// родительская форма
        /// </summary>
        Control _parent = null;

        Control parent
        {
            get
            {
                if (_parent == null)
                {
                    this._parent = Parent;
                    while (this.parent.Parent != null)
                        this._parent = this.parent.Parent;
                }
                return _parent;
            }
        }

        /// <summary>
        /// результат выбора
        /// </summary>
        public Materials Result { get; set; }
    }
}
