﻿namespace IA.Controls
{
    partial class AboutControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.main_pictureBox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.fullName_header_label = new System.Windows.Forms.Label();
            this.version_header_label = new System.Windows.Forms.Label();
            this.developer_header_label = new System.Windows.Forms.Label();
            this.postAddress_header_label = new System.Windows.Forms.Label();
            this.email_header_label = new System.Windows.Forms.Label();
            this.webSite_header_label = new System.Windows.Forms.Label();
            this.fullName_text_label = new System.Windows.Forms.Label();
            this.version_text_label = new System.Windows.Forms.Label();
            this.developer_text_label = new System.Windows.Forms.Label();
            this.postAddress_text_label = new System.Windows.Forms.Label();
            this.allRightsReserved_label = new System.Windows.Forms.Label();
            this.webSite_linkLabel = new System.Windows.Forms.LinkLabel();
            this.email_linkLabel = new System.Windows.Forms.LinkLabel();
            this.main_tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_pictureBox)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 4;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 134F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.main_tableLayoutPanel.Controls.Add(this.main_pictureBox, 1, 1);
            this.main_tableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 2, 1);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 3;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(600, 250);
            this.main_tableLayoutPanel.TabIndex = 0;
            // 
            // main_pictureBox
            // 
            this.main_pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.main_pictureBox.Location = new System.Drawing.Point(28, 61);
            this.main_pictureBox.Name = "main_pictureBox";
            this.main_pictureBox.Size = new System.Drawing.Size(128, 128);
            this.main_pictureBox.TabIndex = 0;
            this.main_pictureBox.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.email_linkLabel, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.fullName_header_label, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.version_header_label, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.developer_header_label, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.postAddress_header_label, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.email_header_label, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.webSite_header_label, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.fullName_text_label, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.version_text_label, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.developer_text_label, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.postAddress_text_label, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.allRightsReserved_label, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.webSite_linkLabel, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(162, 28);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(410, 194);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // fullName_header_label
            // 
            this.fullName_header_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.fullName_header_label.AutoSize = true;
            this.fullName_header_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fullName_header_label.Location = new System.Drawing.Point(3, 3);
            this.fullName_header_label.Name = "fullName_header_label";
            this.fullName_header_label.Size = new System.Drawing.Size(116, 13);
            this.fullName_header_label.TabIndex = 0;
            this.fullName_header_label.Text = "Наименование:";
            this.fullName_header_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // version_header_label
            // 
            this.version_header_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.version_header_label.AutoSize = true;
            this.version_header_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.version_header_label.Location = new System.Drawing.Point(3, 23);
            this.version_header_label.Name = "version_header_label";
            this.version_header_label.Size = new System.Drawing.Size(116, 13);
            this.version_header_label.TabIndex = 1;
            this.version_header_label.Text = "Версия:";
            this.version_header_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // developer_header_label
            // 
            this.developer_header_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.developer_header_label.AutoSize = true;
            this.developer_header_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.developer_header_label.Location = new System.Drawing.Point(3, 43);
            this.developer_header_label.Name = "developer_header_label";
            this.developer_header_label.Size = new System.Drawing.Size(116, 13);
            this.developer_header_label.TabIndex = 2;
            this.developer_header_label.Text = "Разработчик:";
            this.developer_header_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // postAddress_header_label
            // 
            this.postAddress_header_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.postAddress_header_label.AutoSize = true;
            this.postAddress_header_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.postAddress_header_label.Location = new System.Drawing.Point(3, 63);
            this.postAddress_header_label.Name = "postAddress_header_label";
            this.postAddress_header_label.Size = new System.Drawing.Size(116, 13);
            this.postAddress_header_label.TabIndex = 3;
            this.postAddress_header_label.Text = "Почтовый адрес:";
            this.postAddress_header_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // email_header_label
            // 
            this.email_header_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.email_header_label.AutoSize = true;
            this.email_header_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.email_header_label.Location = new System.Drawing.Point(3, 83);
            this.email_header_label.Name = "email_header_label";
            this.email_header_label.Size = new System.Drawing.Size(116, 13);
            this.email_header_label.TabIndex = 4;
            this.email_header_label.Text = "E-mail:";
            this.email_header_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // webSite_header_label
            // 
            this.webSite_header_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.webSite_header_label.AutoSize = true;
            this.webSite_header_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.webSite_header_label.Location = new System.Drawing.Point(3, 103);
            this.webSite_header_label.Name = "webSite_header_label";
            this.webSite_header_label.Size = new System.Drawing.Size(116, 13);
            this.webSite_header_label.TabIndex = 5;
            this.webSite_header_label.Text = "Сайт:";
            this.webSite_header_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // fullName_text_label
            // 
            this.fullName_text_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.fullName_text_label.AutoSize = true;
            this.fullName_text_label.Location = new System.Drawing.Point(125, 3);
            this.fullName_text_label.Name = "fullName_text_label";
            this.fullName_text_label.Size = new System.Drawing.Size(282, 13);
            this.fullName_text_label.TabIndex = 6;
            this.fullName_text_label.Text = "Заполняется автоматически";
            // 
            // version_text_label
            // 
            this.version_text_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.version_text_label.AutoSize = true;
            this.version_text_label.Location = new System.Drawing.Point(125, 23);
            this.version_text_label.Name = "version_text_label";
            this.version_text_label.Size = new System.Drawing.Size(282, 13);
            this.version_text_label.TabIndex = 7;
            this.version_text_label.Text = "Заполняется автоматически";
            // 
            // developer_text_label
            // 
            this.developer_text_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.developer_text_label.AutoSize = true;
            this.developer_text_label.Location = new System.Drawing.Point(125, 43);
            this.developer_text_label.Name = "developer_text_label";
            this.developer_text_label.Size = new System.Drawing.Size(282, 13);
            this.developer_text_label.TabIndex = 8;
            this.developer_text_label.Text = "Заполняется автоматически";
            // 
            // postAddress_text_label
            // 
            this.postAddress_text_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.postAddress_text_label.AutoSize = true;
            this.postAddress_text_label.Location = new System.Drawing.Point(125, 63);
            this.postAddress_text_label.Name = "postAddress_text_label";
            this.postAddress_text_label.Size = new System.Drawing.Size(282, 13);
            this.postAddress_text_label.TabIndex = 9;
            this.postAddress_text_label.Text = "Заполняется автоматически";
            // 
            // allRightsReserved_label
            // 
            this.allRightsReserved_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.allRightsReserved_label.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.allRightsReserved_label, 2);
            this.allRightsReserved_label.Location = new System.Drawing.Point(3, 177);
            this.allRightsReserved_label.Name = "allRightsReserved_label";
            this.allRightsReserved_label.Size = new System.Drawing.Size(404, 13);
            this.allRightsReserved_label.TabIndex = 12;
            this.allRightsReserved_label.Text = "Заполняется автоматически";
            this.allRightsReserved_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // webSite_linkLabel
            // 
            this.webSite_linkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.webSite_linkLabel.AutoSize = true;
            this.webSite_linkLabel.Location = new System.Drawing.Point(125, 103);
            this.webSite_linkLabel.Name = "webSite_linkLabel";
            this.webSite_linkLabel.Size = new System.Drawing.Size(282, 13);
            this.webSite_linkLabel.TabIndex = 13;
            this.webSite_linkLabel.TabStop = true;
            this.webSite_linkLabel.Text = "Заполняется автоматически";
            // 
            // email_linkLabel
            // 
            this.email_linkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.email_linkLabel.AutoSize = true;
            this.email_linkLabel.Location = new System.Drawing.Point(125, 83);
            this.email_linkLabel.Name = "email_linkLabel";
            this.email_linkLabel.Size = new System.Drawing.Size(282, 13);
            this.email_linkLabel.TabIndex = 14;
            this.email_linkLabel.TabStop = true;
            this.email_linkLabel.Text = "Заполняется автоматически";
            // 
            // AboutControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "AboutControl";
            this.Size = new System.Drawing.Size(600, 250);
            this.main_tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.main_pictureBox)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.PictureBox main_pictureBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label fullName_header_label;
        private System.Windows.Forms.Label version_header_label;
        private System.Windows.Forms.Label developer_header_label;
        private System.Windows.Forms.Label postAddress_header_label;
        private System.Windows.Forms.Label email_header_label;
        private System.Windows.Forms.Label webSite_header_label;
        private System.Windows.Forms.Label fullName_text_label;
        private System.Windows.Forms.Label version_text_label;
        private System.Windows.Forms.Label developer_text_label;
        private System.Windows.Forms.Label postAddress_text_label;
        private System.Windows.Forms.Label allRightsReserved_label;
        private System.Windows.Forms.LinkLabel webSite_linkLabel;
        private System.Windows.Forms.LinkLabel email_linkLabel;
    }
}
