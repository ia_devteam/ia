﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Reflection;

namespace IA.Controls
{
    /// <summary>
    /// Элемент управления, используемый всеми расстановщиками датчиков: уровень расстановки, номер первого датчика и идентификатор модуля.
    /// Все существенные второстепенные элементы объявлены публичынми.
    /// </summary>
    public partial class ParsersLevelDATSID : UserControl
    {
        /// <summary>
        /// Единственный конструктор.
        /// </summary>
        public ParsersLevelDATSID()
        {
            InitializeComponent();

            numLevel.Value = 3;
            numLevel.Minimum = 2;
            numLevel.Maximum = 3;

            numDAT.Value = 1;
            numDAT.Minimum = 1;
            numDAT.Maximum = int.MaxValue;

            numSID.Value = 0;
            numSID.Minimum = 0;
            numSID.Maximum = int.MaxValue;

            txtSensorText.Text = "%DAT%,%SID%";
        }

        public enum Ctrls
        {
            Level, DAT, SID, Text
        }
        /// <summary>
        /// Инициализировать обработчики. Порядок передаваемых имён жёсткий.
        /// </summary>
        /// <param name="classname">Тип класса с параметрами для привязки.</param>
        /// <param name="names">null, если не нужно, имя параметра, если нужно.</param>
        /// /// <param name="obj">Нужен, если classname - не статический класс.</param>
        public ParsersLevelDATSID(Type classname, Dictionary<Ctrls, string> names, object obj = null) : this()
        {
            foreach (var kvp in names)
            {
                if (String.IsNullOrWhiteSpace(kvp.Value))
                    continue;

                var fi = classname.GetField(kvp.Value, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.FlattenHierarchy);
                if (fi == null)
                {
#if DEBUG
                    IA.Monitor.Log.Error(this.GetType().ToString(), String.Format("Класс <{0}> не содержит поле <{1}>.", classname.ToString(), kvp.Value));
#endif
                    continue;
                }

                if ((kvp.Key == Ctrls.Text && fi.FieldType != typeof(string))
                    || (kvp.Key != Ctrls.Text && fi.FieldType != typeof(int)))
                {
#if DEBUG
                    IA.Monitor.Log.Error(this.GetType().ToString(), String.Format("Поле <{0}> не соответствует ожидаемому типу (int или string).", kvp.Value));
#endif
                    continue;
                }

                Action<NumericUpDown> setNumHandler = (n) =>
                {
                    n.Value = (int)fi.GetValue(obj);
                    n.ValueChanged += (o, e) => fi.SetValue(obj, (int)n.Value);
                };

                switch (kvp.Key)
                {
                    case Ctrls.Level:
                        setNumHandler(numLevel);
                        break;
                    case Ctrls.DAT:
                        setNumHandler(numDAT);
                        break;
                    case Ctrls.SID:
                        setNumHandler(numSID);
                        break;
                    default:
                        txtSensorText.Text = (string)fi.GetValue(obj);
                        txtSensorText.TextChanged += (o, e) => fi.SetValue(obj, txtSensorText.Text);
                        break;
                }
            }
        }
    }
}
