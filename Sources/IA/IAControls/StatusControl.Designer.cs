﻿using System;
using System.Windows.Forms;

namespace IA.MainForm.Controls
{
    partial class StatusControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.main_textBox = new System.Windows.Forms.TextBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // main_textBox
            // 
            this.main_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.main_textBox.Enabled = false;
            this.main_textBox.Location = new System.Drawing.Point(3, 3);
            this.main_textBox.Name = "main_textBox";
            this.main_textBox.Size = new System.Drawing.Size(267, 20);
            this.main_textBox.TabIndex = 0;
            // 
            // timer
            // 
            this.timer.Interval = 3000;
            // 
            // StatusControl
            // 
            this.Controls.Add(this.main_textBox);
            this.Name = "StatusControl";
            this.Size = new System.Drawing.Size(273, 31);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private TextBox main_textBox;
        private Timer timer;
        #endregion
    }
}
