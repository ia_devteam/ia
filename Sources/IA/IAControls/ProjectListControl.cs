﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Objects;
using IOController;

namespace IA.Controls
{
    /// <summary>
    /// Класс, описывающий элемент управления - Дерево проектов
    /// </summary>
    public partial class ProjectListControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Список проектов";

        /// <summary>
        /// Элемент управления - Таблица с кнопками
        /// </summary>
        private TableLayoutPanel buttons_tableLayoutPanel = null;

        /// <summary>
        /// Всевозможные действия для кнопок и пунктов меню
        /// </summary>
        private Dictionary<string, Action> actions = null;

        private uint lastSelectedProject = 0;

        #region Делегаты и события
        /// <summary>
        /// Делегат для события - На проект нажали один раз
        /// </summary>
        /// <param name="project"></param>
        public delegate void ProjectClickEventHandler(Project project);

        /// <summary>
        /// Делегат для события - На проект нажали дважды
        /// </summary>
        /// <param name="project"></param>
        public delegate void ProjectDoubleClickEventHandler(Project project);

        /// <summary>
        /// Событие - На проект нажали один раз или проект был выбран автомтически
        /// </summary>
        public event ProjectClickEventHandler ProjectClickEvent;
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="lastSelectedProject">ID последнего открытого проекта</param>
        public ProjectListControl(uint lastSelectedProject = 0)
        {
            InitializeComponent();

            this.lastSelectedProject = lastSelectedProject;

            //Инициализируем все возможные действия
            this.actions = this.Actions_Initialization();
            
            //Иконки проектов
            ImageList imageList16 = null;
            imageList16 = new ImageList() { ColorDepth = ColorDepth.Depth32Bit, ImageSize = new Size(32, 32) };
            imageList16.Images.Add("Default", Properties.Resources.Project_Icon);

            //Инициализация списка проектов
            this.main_listView.View = View.Details;
            this.main_listView.HeaderStyle = ColumnHeaderStyle.None;
            this.main_listView.MultiSelect = false;
            this.main_listView.SmallImageList = imageList16;
            this.main_listView.Sorting = SortOrder.Ascending;

            //Создаём единственную колонку с именами проектов
            this.main_listView.Columns.Add("Name", "Name", main_listView.Width - 40);
            this.main_listView.SizeChanged += delegate(object o, EventArgs e) { main_listView.Columns["Name"].Width = main_listView.Width - 40; };

            //Контекстное меню
            this.main_listView.ContextMenuStrip = this.Menu_Initialization();

            //Инициализация элемента управления - таблица кнопок
            int buttonsCount = Enum.GetValues(typeof(Project.EditOperation)).Length;

            this.buttons_tableLayoutPanel = new TableLayoutPanel()
            {
                Dock = DockStyle.Fill,
                RowCount = 1,
                ColumnCount = buttonsCount
            };

            this.buttons_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100));

            //Добавляем кнопку для каждой операции редактирования над проектом
            int columnSizePercentValue = 100/buttonsCount;
            EnumLoop<Project.EditOperation>.ForEach(operation =>
            {
                this.buttons_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, columnSizePercentValue));

                //Получаем кнопку
                Button button = this.actions[operation.FullName()].ToButton();

                //Модернизируем её
                button.Dock = DockStyle.Fill;

                //Добавляем кнопку в таблицу кнопок
                this.buttons_tableLayoutPanel.Controls.Add(button);
            });
 
            this.main_tableLayoutPanel.Controls.Add(this.buttons_tableLayoutPanel, 0, 1);
            
            Sql.SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);
        }

        /// <summary>
        /// Инициализация всеовзможных действий
        /// </summary>
        private Dictionary<string, Action> Actions_Initialization()
        {
            //Формируем результат
            Dictionary<string, Action> ret = new Dictionary<string, Action>();

            //Все операции редактирования над проектом
            EnumLoop<Project.EditOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    //case Project.EditOperation.CHOOSE:
                    //    ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Text = operation.Description(), ActionHandler = this.Project_Choose_ActionHandler });
                    //    break;
                    case Project.EditOperation.ADD:
                        ret.Add(operation.FullName(), new Action() { Enabled = true, Key = operation.FullName(), Text = operation.Description(), ActionHandler = this.Project_Add_ActionHandler });
                        break;
                    case Project.EditOperation.REMOVE:
                        ret.Add(operation.FullName(), new Action() { Enabled = false, Key = operation.FullName(), Text = operation.Description(), ActionHandler = this.Project_Remove_ActionHandler });
                        break;
                    default:
                        throw new Exception(Project.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                }
            });

            return ret;
        }

        /// <summary>
        /// Инициализация контекстного меню
        /// </summary>
        /// <returns></returns>
        private ContextMenuStrip Menu_Initialization()
        {
            ContextMenuStrip ret = new ContextMenuStrip();

            EnumLoop<Project.EditOperation>.ForEach( operation => ret.Items.Add(this.actions[operation.FullName()].ToToolStripMenuItem()) );

            return ret;
        }

        /// <summary>
        /// Форомирование списка проектов
        /// </summary>
        private void Generate()
        {
            //Очищаем список проектов
            this.Clear();

            HashSet<string> projectDirNames = new HashSet<string>(
                DirectoryController.GetSubDirectories(IA.Settings.ProjectsServer.ProjectsDirectoryPath)
                .Select(PathController.TrimEndSeparator)
                .Select(System.IO.Path.GetFileName)
                );
            //Считываем информацию из БД
            foreach (Project project in Project.GetAll())
            {
                if (!projectDirNames.Contains(project.Name))
                {
                    Monitor.Log.Warning(ObjectName, "Ошибка при добавлении проекта <" + project.Name + "> в список проектов." + "\n" +
                            "Директория проекта не существует по заданному в настройках приложения пути: " + IA.Settings.ProjectsServer.ProjectsDirectoryPath + ".");
                }

                //Добавляем все проекты из БД в список
                AddItem(project);
            }
        }

        /// <summary>
        /// Очистить список проектов
        /// </summary>
        private void Clear()
        {
            //Очищаем список проектов
            main_listView.Items.Clear();
        }

        /// <summary>
        /// Добавить проект в список
        /// </summary>
        /// <param name="project"></param>
        private ListViewItem AddItem(Project project)
        {
            //Проверка на разумность
            if (project == null)
                throw new Exception("Невозможно добавить проект. Проект не определён.");

            //Создаём элемент
            ListViewItem ret = new ListViewItem()
            {
                ImageKey = "Default",
                Name = project.ID.ToString(),
                Text = project.Name,
                Tag = project
            };

            //Добавляем элемент в список
            main_listView.Items.Add(ret);

            if (lastSelectedProject == project.ID)
                main_listView.Items[main_listView.Items.IndexOfKey(project.ID.ToString())].Selected = true;

            return ret;
        }

        #region Обработчики событий
        /// <summary>
        /// Обработчик - На проект нажали один раз
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Project_Click_EventHandler(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            try
            {
                //Задаём активность действий в зависимости от выбранного проекта
                EnumLoop<Project.EditOperation>.ForEach(operation =>
                {
                    switch (operation)
                    {
                        case Project.EditOperation.ADD:
                            //this.actions[operation.FullName()].Enabled = !e.IsSelected;
                            break;
//                        case Project.EditOperation.CHOOSE:
                        case Project.EditOperation.REMOVE:
                            this.actions[operation.FullName()].Enabled = e.IsSelected;
                            break;
                        default:
                            throw new Exception(Project.Exceptions.UNKNOWN_EDIT_OPERATION.Description());
                    }
                });

                //Формируем событие
                ProjectClickEvent?.Invoke(e.IsSelected ? e.Item.Tag as Project : null);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Однократное нажатие на проект в списке", (message) => Monitor.Log.Error(ObjectName, message, true));

                return;
            }
        }
        #endregion

        #region Обработчики действий
        
        private void Project_Add_ActionHandler(object sender, Action action)
        {
            try
            {
                //Создаём форму создания нового проекта
                Form projectAddDialog = new Form()
                {
                    Icon = this.ParentForm.Icon,
                    KeyPreview = true,
                    StartPosition = FormStartPosition.CenterParent,
                    Size = new System.Drawing.Size(450, 630),
                    Text = "Создание проекта",
                };
                projectAddDialog.KeyDown += delegate(object s, KeyEventArgs e)
                {
                    switch (e.KeyCode)
                    {
                        case Keys.Escape:
                            {
                                //Закрываем диалог
                                projectAddDialog.DialogResult = DialogResult.Cancel;
                                projectAddDialog.Close();

                                return;
                            }
                        default:
                            return;
                    }
                };

                //Создаём элемент управления создания нового проекта
                ProjectAddControl projectAddControl = new ProjectAddControl()
                {
                    Dock = DockStyle.Fill
                };

                //Добавляем обработчик на кнопку "Готово"
                projectAddControl.ReadyEvent += delegate(Project project)
                {
                    //Добавляем вновь созданный проект в список
                    this.AddItem(project).Selected = true;

                    //Говорим о положительном результате
                    projectAddDialog.DialogResult = DialogResult.OK;

                    //Закрываем форму создания проекта
                    projectAddDialog.Close();
                };

                //Добавляем элемент управления создания нового проекта на форму создания нового проекта
                projectAddDialog.Controls.Add(projectAddControl);

                //Отображаем форму создания нового проекта
                if (projectAddDialog.ShowDialog() != DialogResult.OK)
                    return;

                //Принудительно переводим фокус на список
                this.main_listView.Focus();
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
               ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Удалить проект
        /// </summary>
        private void Project_Remove_ActionHandler(object sender, Action action)
        {
            //Объект проекта, который требуется удалить
            Project project = null;

            try
            {
                //Проверка на разумность
                if (main_listView.SelectedItems == null || main_listView.SelectedItems.Count != 1)
                    throw new Exception("Проект не выбран или выбрано более одного проекта для удаления.");

                //Получаем объект проекта, который требуется удалить
                project = main_listView.SelectedItems[0].Tag as Project;

                //Задаём вопрос пользователю
                switch (MessageBox.Show("Проект <" + project.Name + "> будет безвозвратно удалён. Вы уверены?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        break;
                    case DialogResult.No:
                        return;
                }

                //Отображаем окно ожидания
                WaitWindow.Show(
                    delegate(WaitWindow window)
                    {
                        //Удаляем проект из БД
                        Project.RemoveByID(project.ID);
                    },
                    "Удаление проекта" + (String.IsNullOrWhiteSpace(project.Name) ? String.Empty : " <" + project.Name + ">")
                );
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
               ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));

                return;
            }

            //Удаляем проект из списка проектов
            if (project != null)
                main_listView.Items.RemoveByKey(project.ID.ToString());
        }
        #endregion
        
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления в соответствии с содержимым базы данных
            this.Generate();

            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Делаем его недоступным
            this.Enabled = false;
        }
    }

}
