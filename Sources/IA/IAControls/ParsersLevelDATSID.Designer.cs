﻿namespace IA.Controls
{
    partial class ParsersLevelDATSID
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tblMain = new System.Windows.Forms.TableLayoutPanel();
            this.numDAT = new System.Windows.Forms.NumericUpDown();
            this.numSID = new System.Windows.Forms.NumericUpDown();
            this.lblLevel = new System.Windows.Forms.Label();
            this.lblDAT = new System.Windows.Forms.Label();
            this.lblSID = new System.Windows.Forms.Label();
            this.lblSensorText = new System.Windows.Forms.Label();
            this.txtSensorText = new System.Windows.Forms.TextBox();
            this.txtSensorTextHint = new System.Windows.Forms.TextBox();
            this.numLevel = new System.Windows.Forms.NumericUpDown();
            this.tblMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // tblMain
            // 
            this.tblMain.ColumnCount = 4;
            this.tblMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tblMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tblMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tblMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblMain.Controls.Add(this.numDAT, 1, 1);
            this.tblMain.Controls.Add(this.numSID, 2, 1);
            this.tblMain.Controls.Add(this.lblLevel, 0, 0);
            this.tblMain.Controls.Add(this.lblDAT, 1, 0);
            this.tblMain.Controls.Add(this.lblSID, 2, 0);
            this.tblMain.Controls.Add(this.lblSensorText, 0, 2);
            this.tblMain.Controls.Add(this.txtSensorText, 1, 2);
            this.tblMain.Controls.Add(this.txtSensorTextHint, 2, 2);
            this.tblMain.Controls.Add(this.numLevel, 0, 1);
            this.tblMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblMain.Location = new System.Drawing.Point(0, 0);
            this.tblMain.Name = "tblMain";
            this.tblMain.RowCount = 4;
            this.tblMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tblMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblMain.Size = new System.Drawing.Size(410, 90);
            this.tblMain.TabIndex = 0;
            // 
            // numDAT
            // 
            this.numDAT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numDAT.Location = new System.Drawing.Point(83, 23);
            this.numDAT.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numDAT.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDAT.Name = "numDAT";
            this.numDAT.Size = new System.Drawing.Size(144, 20);
            this.numDAT.TabIndex = 1;
            this.numDAT.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numSID
            // 
            this.numSID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numSID.Location = new System.Drawing.Point(233, 23);
            this.numSID.Name = "numSID";
            this.numSID.Size = new System.Drawing.Size(174, 20);
            this.numSID.TabIndex = 2;
            // 
            // lblLevel
            // 
            this.lblLevel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblLevel.AutoSize = true;
            this.lblLevel.Location = new System.Drawing.Point(3, 3);
            this.lblLevel.Name = "lblLevel";
            this.lblLevel.Size = new System.Drawing.Size(51, 13);
            this.lblLevel.TabIndex = 3;
            this.lblLevel.Text = "Уровень";
            // 
            // lblDAT
            // 
            this.lblDAT.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDAT.AutoSize = true;
            this.lblDAT.Location = new System.Drawing.Point(83, 3);
            this.lblDAT.Name = "lblDAT";
            this.lblDAT.Size = new System.Drawing.Size(144, 13);
            this.lblDAT.TabIndex = 4;
            this.lblDAT.Text = "Номер стартового датчика";
            // 
            // lblSID
            // 
            this.lblSID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSID.AutoSize = true;
            this.lblSID.Location = new System.Drawing.Point(233, 3);
            this.lblSID.Name = "lblSID";
            this.lblSID.Size = new System.Drawing.Size(126, 13);
            this.lblSID.TabIndex = 5;
            this.lblSID.Text = "Идентификатор версии";
            // 
            // lblSensorText
            // 
            this.lblSensorText.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSensorText.AutoSize = true;
            this.lblSensorText.Location = new System.Drawing.Point(3, 52);
            this.lblSensorText.Name = "lblSensorText";
            this.lblSensorText.Size = new System.Drawing.Size(47, 26);
            this.lblSensorText.TabIndex = 6;
            this.lblSensorText.Text = "Текст датчика";
            // 
            // txtSensorText
            // 
            this.txtSensorText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSensorText.Location = new System.Drawing.Point(83, 55);
            this.txtSensorText.Name = "txtSensorText";
            this.txtSensorText.Size = new System.Drawing.Size(144, 20);
            this.txtSensorText.TabIndex = 7;
            this.txtSensorText.Text = "%DAT%,%SID%";
            // 
            // txtSensorTextHint
            // 
            this.txtSensorTextHint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSensorTextHint.Location = new System.Drawing.Point(233, 48);
            this.txtSensorTextHint.Multiline = true;
            this.txtSensorTextHint.Name = "txtSensorTextHint";
            this.txtSensorTextHint.ReadOnly = true;
            this.txtSensorTextHint.Size = new System.Drawing.Size(174, 34);
            this.txtSensorTextHint.TabIndex = 8;
            this.txtSensorTextHint.Text = "%DAT% - номер датчика\r\n%SID% - идентификатор версии";
            // 
            // numLevel
            // 
            this.numLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numLevel.Location = new System.Drawing.Point(3, 23);
            this.numLevel.Name = "numLevel";
            this.numLevel.Size = new System.Drawing.Size(74, 20);
            this.numLevel.TabIndex = 9;
            // 
            // ParsersLevelDATSID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tblMain);
            this.MinimumSize = new System.Drawing.Size(410, 90);
            this.Name = "ParsersLevelDATSID";
            this.Size = new System.Drawing.Size(410, 90);
            this.tblMain.ResumeLayout(false);
            this.tblMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLevel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tblMain;
        public System.Windows.Forms.NumericUpDown numDAT;
        public System.Windows.Forms.NumericUpDown numSID;
        private System.Windows.Forms.Label lblLevel;
        private System.Windows.Forms.Label lblDAT;
        private System.Windows.Forms.Label lblSID;
        private System.Windows.Forms.Label lblSensorText;
        public System.Windows.Forms.TextBox txtSensorText;
        private System.Windows.Forms.TextBox txtSensorTextHint;
        public System.Windows.Forms.NumericUpDown numLevel;
    }
}
