﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace IA.Controls
{
    /// <summary>
    /// Класс, реализующий элемент управления (Рабочее пространство)
    /// </summary>
    public partial class WorkspaceControl : UserControl
    {
        /// <summary>
        /// Элемент управления - Основной сплит контейнер
        /// </summary>
        private SplitContainer main_splitContainer = null;

        /// <summary>
        /// Фоновое изображение
        /// </summary>
        private Image backgroundImage = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        public WorkspaceControl()
        {
            InitializeComponent();
            this.main_splitContainer.SplitterDistance = main_splitContainer.Height - IA.Controls.Common.ButtonsPanel.ButtonHeight - 10;
        }

        /// <summary>
        /// Задать элемент управления для отображения на рабочем пространстве
        /// </summary>
        /// <param name="control">Элемент управления</param>
        public void SetControl(Control control)
        {
            //Проверка на разумность
            if (control == null)
                throw new Exception("Ошибка при задании набора элементов управления для рабочего пространства. Элемент управления не определён.");

            //Задаём элементу управления необходимые свойства
            control.Dock = DockStyle.Fill;

            //Инициализация элемента управления - Панель
            Panel control_panel = new Panel()
            {
                AutoScroll = true,
                AutoScrollMinSize = control.Size,
                Dock = DockStyle.Fill,
            };
            control_panel.Controls.Add(control);

            //Удаляем фоновое изображение
            this.main_splitContainer.Panel1.BackgroundImage = null;

            //Добавляем панель элемента управления на рабочее пространство
            this.main_splitContainer.Panel1.Controls.Add(control_panel);
        }

        /// <summary>
        /// Задать набор элементов управления для отображения на рабочем пространстве
        /// </summary>
        /// <param name="controls">Набор элементов управления</param>
        public void SetControls(List<Control> controls)
        {
            //Проверка на разумность
            if (controls == null)
                throw new Exception("Ошибка при задании набора элементов управления для рабочего пространства. Набор элементов управления не определён.");

            //Инициализация элемента управления - Таблица
            TableLayoutPanel tableLayoutPanel = new TableLayoutPanel()
            {
                AutoScroll = true,
                Dock = DockStyle.Fill,
                RowCount = controls.Count * 2 + 1,
                ColumnCount = 1,
            };

            int autoScrollMinWidth = 0;
            int autoScrollMinHeight = 0;

            //Проходим по всем элементам управления
            foreach (Control control in controls)
            {
                //Проверка на разумность
                if (control == null)
                    throw new Exception("Ошибка при задании набора элементов управления для рабочего пространства. Элемент управления не определён.");

                //Задаём элементу управления необходимые свойства
                control.Dock = DockStyle.Fill;

                //Инициализация элемента управления - Метка
                Label control_label = this.GetControlLabel(control.Text);

                //Добавляем элемент управления в таблицу элементов управления
                tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                tableLayoutPanel.Controls.Add(control_label);
                tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, control.Height + 10));
                tableLayoutPanel.Controls.Add(control);

                //Получаем минимальную ширину таблицы при которой не нужно отображать скроллбар
                if (control.Width > autoScrollMinWidth)
                    autoScrollMinWidth = control.Width;

                //Обновляем манимальную высотутаблицы при которой не нужно отображать скроллбар
                autoScrollMinHeight += control_label.Height + control.Height + 10;
            }

            //Задаём минимальные размеры таблицы, при которых не надо отображать скроллбары
            tableLayoutPanel.AutoScrollMinSize = new Size(autoScrollMinWidth, autoScrollMinHeight);

            //Добвляем последнюю строку для заполнения свободного места на рабочем пространстве
            tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100));

            //Добавляем единственный столбец
            tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

            //Удаляем фоновое изображение
            this.main_splitContainer.Panel1.BackgroundImage = null;

            //Добавляем таблицу элементов управления на рабочее пространство
            this.main_splitContainer.Panel1.Controls.Add(tableLayoutPanel);
        }

        /// <summary>
        /// Задать набор кнопок для рабочего пространства
        /// </summary>
        /// <param name="buttons">Набор кнопок</param>
        public void SetButtons(Dictionary<string, EventHandler> buttons)
        {
            //Проверка на разумность
            if (buttons == null || buttons.Count == 0)
                throw new Exception("Ошибка при задании набора кнопок для рабочего пространства. Набор кнопок пуст или не определён.");

            //Инициализация элемента управления - Панель с кнопками
            IA.Controls.Common.ButtonsPanel buttonsPanel = new IA.Controls.Common.ButtonsPanel()
            {
                Dock = DockStyle.Fill
            };
            buttonsPanel.SetButtons(buttons);

            //Отображаем панель с кнопками
            this.main_splitContainer.Panel2.Controls.Clear();
            this.main_splitContainer.Panel2.Controls.Add(buttonsPanel);
            this.main_splitContainer.Panel2Collapsed = false;
        }

        private delegate void ClearAll();

        /// <summary>
        /// Очистить рабочее пространство
        /// </summary>
        public void Clear()
        {
            Control control = main_splitContainer as Control;

            if (control != null && control.InvokeRequired)
                control.Invoke(new ClearAll(ClearInternal));
            else
                ClearInternal();
        }

        private void ClearInternal()
        {
            //Очищаем панель с элементами управления
            this.main_splitContainer.Panel1.Controls.Clear();
            this.main_splitContainer.Panel1.BackgroundImage = this.backgroundImage;

            //Очищаем и скрываем панель с кнопками
            this.main_splitContainer.Panel2.Controls.Clear();
            this.main_splitContainer.Panel2Collapsed = true;
        }

        /// <summary>
        /// Получить метку для элемента управления
        /// </summary>
        /// <param name="text">Текст метки</param>
        /// <returns></returns>
        private Label GetControlLabel(string text)
        {
            //Формируем результат
            Label ret = new Label()
            {
                AutoSize = true,
                Anchor = AnchorStyles.Left | AnchorStyles.Right,
                Text = text,
                TextAlign = ContentAlignment.MiddleCenter,
            };
            ret.Font = new Font(ret.Font.FontFamily, 14);

            return ret;
        }
    }
}