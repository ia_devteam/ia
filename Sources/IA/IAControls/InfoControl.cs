﻿using System;
using System.Windows.Forms;
using IA.Extensions;

namespace IA.Controls
{
    public class InfoControl : StatusStrip
    {
        public InfoControl()
        {
            EnumLoop<IA.InfoType>.ForEach(info =>
            {
                ToolStripStatusLabel infoLabel = new ToolStripStatusLabel()
                {
                    Name = info.FullName(),
                    BorderSides = ToolStripStatusLabelBorderSides.Left | ToolStripStatusLabelBorderSides.Right,
                    Text = info.Description()
                };

                switch (info)
                {
                    case InfoType.USER: infoLabel.Text += ": Аноним"; break;
                    case InfoType.PROJECT: infoLabel.Text += ": не выбран"; break;
                    case InfoType.MATERIALS: infoLabel.Text += ": не выбраны"; break;
                    case InfoType.STORAGE: infoLabel.Text += ": не выбрано"; break;
                    case InfoType.WORKDIRECTORY: infoLabel.Text += ": не выбрана"; break;
                    default:
                        throw new Exception(Exceptions.UNKNOWN_INFO_TYPE.Description());
                }

                this.Items.Add(infoLabel);
            });
        }
    }
}
