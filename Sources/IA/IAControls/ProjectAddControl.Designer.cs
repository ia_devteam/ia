﻿namespace IA.Controls
{
    partial class ProjectAddControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ready_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.ready_button = new System.Windows.Forms.Button();
            this.name_groupBox = new System.Windows.Forms.GroupBox();
            this.name_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.name_textBox = new System.Windows.Forms.TextBox();
            this.main_tableLayoutPanel.SuspendLayout();
            this.ready_tableLayoutPanel.SuspendLayout();
            this.name_groupBox.SuspendLayout();
            this.name_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.ready_tableLayoutPanel, 0, 2);
            this.main_tableLayoutPanel.Controls.Add(this.name_groupBox, 0, 0);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 3;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(363, 135);
            this.main_tableLayoutPanel.TabIndex = 1;
            // 
            // ready_tableLayoutPanel
            // 
            this.ready_tableLayoutPanel.ColumnCount = 3;
            this.ready_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ready_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.ready_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.ready_tableLayoutPanel.Controls.Add(this.ready_button, 1, 0);
            this.ready_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ready_tableLayoutPanel.Location = new System.Drawing.Point(3, 88);
            this.ready_tableLayoutPanel.Name = "ready_tableLayoutPanel";
            this.ready_tableLayoutPanel.RowCount = 1;
            this.ready_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ready_tableLayoutPanel.Size = new System.Drawing.Size(357, 44);
            this.ready_tableLayoutPanel.TabIndex = 0;
            // 
            // ready_button
            // 
            this.ready_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ready_button.Location = new System.Drawing.Point(81, 7);
            this.ready_button.Name = "ready_button";
            this.ready_button.Size = new System.Drawing.Size(194, 30);
            this.ready_button.TabIndex = 0;
            this.ready_button.Text = "Готово";
            this.ready_button.UseVisualStyleBackColor = true;
            this.ready_button.Click += new System.EventHandler(this.ready_button_Click);
            // 
            // name_groupBox
            // 
            this.name_groupBox.Controls.Add(this.name_tableLayoutPanel);
            this.name_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_groupBox.Location = new System.Drawing.Point(3, 3);
            this.name_groupBox.Name = "name_groupBox";
            this.name_groupBox.Size = new System.Drawing.Size(357, 49);
            this.name_groupBox.TabIndex = 1;
            this.name_groupBox.TabStop = false;
            this.name_groupBox.Text = "Имя проекта";
            // 
            // name_tableLayoutPanel
            // 
            this.name_tableLayoutPanel.ColumnCount = 1;
            this.name_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.name_tableLayoutPanel.Controls.Add(this.name_textBox, 0, 0);
            this.name_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_tableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.name_tableLayoutPanel.Name = "name_tableLayoutPanel";
            this.name_tableLayoutPanel.RowCount = 1;
            this.name_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.name_tableLayoutPanel.Size = new System.Drawing.Size(351, 30);
            this.name_tableLayoutPanel.TabIndex = 1;
            // 
            // name_textBox
            // 
            this.name_textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.name_textBox.Location = new System.Drawing.Point(3, 5);
            this.name_textBox.Name = "name_textBox";
            this.name_textBox.Size = new System.Drawing.Size(345, 20);
            this.name_textBox.TabIndex = 0;
            // 
            // ProjectAddControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_tableLayoutPanel);
            this.Name = "ProjectAddControl";
            this.Size = new System.Drawing.Size(363, 135);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.ready_tableLayoutPanel.ResumeLayout(false);
            this.name_groupBox.ResumeLayout(false);
            this.name_tableLayoutPanel.ResumeLayout(false);
            this.name_tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel ready_tableLayoutPanel;
        private System.Windows.Forms.Button ready_button;
        private System.Windows.Forms.GroupBox name_groupBox;
        private System.Windows.Forms.TableLayoutPanel name_tableLayoutPanel;
        private System.Windows.Forms.TextBox name_textBox;
    }
}
