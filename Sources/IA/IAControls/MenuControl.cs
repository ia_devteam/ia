﻿using System.Windows.Forms;

namespace IA.Controls
{
    /// <summary>
    /// меню
    /// </summary>
    public class MenuControl : MenuStrip
    {
        /// <summary>
        /// Добавить корневой элемент меню
        /// </summary>
        /// <param name="root"></param>
        /// <param name="key"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public ToolStripMenuItem Add_MenuItem(ToolStripMenuItem root, string key, string text)
        {
            ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem()
            {
                Name = key,
                Text = text
            };

            if (root != null)
                root.DropDownItems.Add(toolStripMenuItem);
            else
                this.Items.Add(toolStripMenuItem);

            return toolStripMenuItem;
        }

        /// <summary>
        /// Добавить новый элемент меню
        /// </summary>
        /// <param name="root"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public ToolStripMenuItem Add_MenuItem(ToolStripMenuItem root, Action action)
        {
            //Получаем элемент меню по действию
            ToolStripMenuItem toolStripMenuItem = action.ToToolStripMenuItem();

            //Модернизируем его
            toolStripMenuItem.ImageScaling = ToolStripItemImageScaling.None;
            toolStripMenuItem.ShowShortcutKeys = action.ShortcutKeys != Keys.None;

            //Добавляем вновь созданный эелемент к корневому меню
            if (root != null)
                root.DropDownItems.Add(toolStripMenuItem);
            else
                this.Items.Add(toolStripMenuItem);

            return toolStripMenuItem;
        }

        /// <summary>
        /// Добавить разделитель
        /// </summary>
        /// <param name="root"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public ToolStripSeparator AddSeparator(ToolStripMenuItem root, string key)
        {
            //Формируем разделитель
            ToolStripSeparator separator = new ToolStripSeparator() { Name = key };

            //Добавляем вновь созданный разделитель к корневому меню
            root.DropDownItems.Add(separator);

            return separator;
        }
    }
}
