﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace IA.Controls
{
    /// <summary>
    /// Класс, реализующий панель инструментов, состоящую из набора отдельных кнопок и разделителей
    /// </summary>
    public class ToolsControl : ToolStrip
    {
        /// <summary>
        /// Добавить кнопку на панель инструментов
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public ToolStripButton AddButton(Action action)
        {
            //Получаем кнопку панели инструменов из действия
            ToolStripButton toolStripButton = action.ToToolStripButton();

            //Модернизируем её
            toolStripButton.Text = String.Empty;
            toolStripButton.ImageScaling = ToolStripItemImageScaling.None;
            toolStripButton.Size = new Size(50, 50);
            toolStripButton.ToolTipText = action.Text;

            //Добавляем кнопку на панель инструментов
            this.Items.Add(toolStripButton);

            return toolStripButton;
        }

        public ToolStripButton GetToolsControlButtonByAction(Action action)
        {
            return (ToolStripButton)(this.Items.Find(action.Key, true)[0]);
        }

        /// <summary>
        /// Добавить кнопку c выпадающим списком на панель инструментов
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public ToolStripDropDownButton AddDropDownButton(Action action)
        {
            //Получаем кнопку панели инструменов c выпадающим списком из действия
            ToolStripDropDownButton toolStripDropDownButton = action.ToToolStripDropDownButton();

            //Модернизируем её
            toolStripDropDownButton.Text = String.Empty;
            toolStripDropDownButton.ImageScaling = ToolStripItemImageScaling.None;
            toolStripDropDownButton.Size = new Size(50, 50);
            toolStripDropDownButton.ToolTipText = action.Text;

            //Добавляем кнопку на панель инструментов
            this.Items.Add(toolStripDropDownButton);

            return toolStripDropDownButton;
        }


        /// <summary>
        /// Добавить разделитель на панель инструментов
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ToolStripSeparator AddSeparator(string key)
        {
            ToolStripSeparator separator = new ToolStripSeparator() { Name = key };

            this.Items.Add(separator);

            return separator;
        }
    }
}
