﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using IA.Extensions;
using IA.Objects;

namespace IA.Controls
{
    /// <summary>
    /// Класс, описывающий элемент управления - Окно свойств проекта
    /// </summary>
    public partial class ProjectSettingsControl : UserControl
    {
        /// <summary>
        /// Класс, релаизующий отдельную настройку проекта
        /// </summary>
        private class Setting
        {
            /// <summary>
            /// Делегат для события - Изменилось состояние настройки (Выбрана\Не выбрана)
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="value"></param>
            internal delegate void CheckedChangedEventHandler(object sender, bool value);

            /// <summary>
            /// Событие - Изменилось состояние настройки (Выбрана\Не выбрана)
            /// </summary>
            internal event CheckedChangedEventHandler CheckedChangedEvent;

            /// <summary>
            /// Идентификатор настройки
            /// </summary>
            internal uint ID { get; private set; }

            /// <summary>
            /// Элемент управления связанный с настрокой
            /// </summary>
            internal Control Control { get; private set; }

            /// <summary>
            /// Выбрана ли дання настройка?
            /// </summary>
            internal bool Checked
            {
                get
                {
                    CheckBox checkbox;
                    if ((checkbox = this.Control as CheckBox) != null)
                        return checkbox.Checked;

                    CheckableGroupBox checkableGroupBox;
                    if ((checkableGroupBox = this.Control as CheckableGroupBox) != null)
                        return checkableGroupBox.Checked;

                    RadioButton radioButton;
                    if ((radioButton = this.Control as RadioButton) != null)
                        return radioButton.Checked;

                    throw new Exception("Неизвестный тип элемента управления для условия.");
                }
                set
                {
                    CheckBox checkbox;
                    if ((checkbox = this.Control as CheckBox) != null)
                    {
                        if (checkbox.Checked == value)
                            return;

                        checkbox.Checked = value;

                        return;
                    }

                    CheckableGroupBox сheckableGroupBox;
                    if ((сheckableGroupBox = this.Control as CheckableGroupBox) != null)
                    {
                        if (сheckableGroupBox.Checked == value)
                            return;

                        сheckableGroupBox.Checked = value;

                        return;
                    }

                    RadioButton radioButton;
                    if ((radioButton = this.Control as RadioButton) != null)
                    {
                        if (radioButton.Checked == value)
                            return;

                        radioButton.Checked = value;

                        return;
                    }

                    throw new Exception("Неизвестный тип элемента управления для условия.");
                }
            }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="id">Идентификатор</param>
            /// <param name="control">Элемент управления</param>
            internal Setting(uint id, Control control)
            {
                this.ID = id;
                this.Control = control;

                CheckBox checkBox;
                if ((checkBox = control as CheckBox) != null)
                {
                    checkBox.CheckedChanged += delegate(object sender, EventArgs e)
                    {
                        if (CheckedChangedEvent != null)
                            CheckedChangedEvent(this, checkBox.Checked);
                    };
                    return;
                }

                CheckableGroupBox checkableGroupBox;
                if ((checkableGroupBox = control as CheckableGroupBox) != null)
                {
                    checkableGroupBox.CheckedChanged += delegate(object sender, EventArgs e)
                    {
                        if (CheckedChangedEvent != null)
                            CheckedChangedEvent(this, checkableGroupBox.Checked);
                    };
                    return;
                }

                RadioButton radioButton;
                if ((radioButton = control as RadioButton) != null)
                {
                    radioButton.CheckedChanged += delegate(object sender, EventArgs e)
                    {
                        if (CheckedChangedEvent != null)
                            CheckedChangedEvent(this, radioButton.Checked);
                    };
                    return;
                }

                throw new Exception("Неизвестный тип элемента управления для настройки.");
            }
        }

        #region Константы

        /// <summary>
        /// Наименование эелемента управления по умолчанию
        /// </summary>
        private const string ThisDefaultName = "Настройки проекта";

        /// <summary>
        /// Наименование пресета для ручного выбора стадий работы с проектом
        /// </summary>
        private const string CustomPresetName = "<Вручную>";

        #endregion

        /// <summary>
        /// Элемент управления - Таблица настроек проекта
        /// </summary>
        private TableLayoutPanel settings_tableLayoutPanel = null;

        /// <summary>
        /// Конфигурация настроек
        /// </summary>
        private IA.Config.Objects.Settings configuration = null;

        /// <summary>
        /// Список настроек
        /// </summary>
        private List<Setting> settings = null;

        private Project project = null;
        /// <summary>
        /// Проект, чьи свойства необходимо отобразить
        /// </summary>
        public Project Project
        {
            set
            {
                //Если ничего не поменялось, то ничего не делаем
                if (project != null && value != null && project.ID == value.ID)
                    return;

                //Сохраняем переданное значение
                project = value;

                //Заполняем окно с настройками
                Generate();
            }
        }

        /// <summary>
        /// Настройки, выбранные пользователем
        /// </summary>
        public IA.Config.Objects.Settings.Set UserSettingsSet
        {
            get
            {
                return this.Settings_ToSet();
            }
        }

        /// <summary>
        /// Включить\отключить доступность списка настроек. В режиме недоступности список настроек не отображается.
        /// </summary>
        public bool IsEnabled
        {
            set
            {
                //Скрываем\Показываем сообщение о недоступности настроек
                main_splitContainer.Panel1Collapsed = !value;
                main_splitContainer.Panel2Collapsed = value;

                //Меняем заголовок элемента управления
                main_groupBox.Text = ThisDefaultName + (this.project == null || !value ? String.Empty : " <" + this.project.Name + ">");
            }
        }

        /// <summary>
        /// Включить\отключить режим блокирование списка настроек проекта. В режиме блокирования список настроек проекта отображается, но никакие действия не доступны.
        /// </summary>
        public bool IsBlocked
        {
            set
            {
                //Список пресетов недоступен
                this.presets_comboBox.Enabled = !value;

                //Настройки-стадии недоступны
                this.settings.ForEach(s => s.Control.Enabled = !value);
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public ProjectSettingsControl(IA.Config.EntityCollection<IA.Config.Objects.Condition> conditions, IA.Config.Objects.Settings configuration)
        {
            InitializeComponent();

            //Запоминаем конфигурацию настроек
            this.configuration = configuration;

            //Инициализируем списк настроек
            this.settings = new List<Setting>();

            // -- Отображаем пресеты --
            //Добавляем пресеты описанные в конфигурационном файле
            presets_comboBox.Items.AddRange(this.configuration.Presets.ToArray());
            //presets_comboBox.Text = CustomPresetName;
            presets_comboBox.SelectionChangeCommitted += (sender, e) => this.Preset_SetManually(presets_comboBox.SelectedItem as IA.Config.Objects.Settings.Preset);

            // -- Отображаем настройки --
            //Инициализация элемента управления - Таблица настроек проекта
            settings_tableLayoutPanel = new TableLayoutPanel()
            {
                AutoScroll = true,
                Dock = DockStyle.Fill,
                RowCount = 0,
                ColumnCount = 1
            };

            //В случае появления вертикального скролл бара, подгоняем таблицу под новый размер
            //settings_tableLayoutPanel.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);

            int conditionsControlsCounter = 0;
            foreach (uint conditionID in this.configuration.Structure.Elements.Keys)
            {
                //Получаем условие
                IA.Config.Objects.Condition condition = conditions[conditionID];

                //Получаем все дочерние условия для данного условия
                HashSet<uint> subConditionsIDs = this.configuration.Structure.Elements[conditionID];

                //Формируем элемент управления для условия
                CheckableGroupBox conditionControl = new CheckableGroupBox()
                {
                    AutoCheck = true,
                    Dock = DockStyle.Fill,
                    Height = (subConditionsIDs.Count + 1) * 30,
                    Text = condition.Name,
                };

                //При смене отметки - автоматически проставляем пресет
                conditionControl.Click += (sender, e) => this.Preset_SetAutomatically();

                //Создаём настройку
                Setting setting = new Setting(conditionID, conditionControl);
                setting.CheckedChangedEvent += this.Setting_ControlChecked_Changed;

                //Добавляем новую настройку в список настроек
                this.settings.Add(setting);

                //Задаём тип "отметки" для созданного групбокса
                switch (condition.Control)
                {
                    case IA.Config.Objects.Settings.Control.CHECKBOX: conditionControl.CheckableType = CheckableGroupBox.enCheckableType.CheckBox; break;
                    case IA.Config.Objects.Settings.Control.RADIOBUTTON: conditionControl.CheckableType = CheckableGroupBox.enCheckableType.RadioButton; break;
                    case Config.Objects.Settings.Control.NOTHING:
                    default:
                        throw new Exception("Неизвестный тип элемента управления для стадии.");
                }

                //Создаём таблицу для хранения поднастроек
                TableLayoutPanel conditions_tableLayoutControl = new TableLayoutPanel()
                {
                    Dock = DockStyle.Fill,
                    RowCount = subConditionsIDs.Count,
                    ColumnCount = 2
                };
                conditions_tableLayoutControl.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 30));
                conditions_tableLayoutControl.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

                int subConditionsControlsCounter = 0;
                foreach(uint subConditionID in subConditionsIDs)
                {
                    //Получаем дочерние условие
                    IA.Config.Objects.Condition subCondition = conditions[subConditionID];

                    Control subConditionControl = null;

                    switch (subCondition.Control)
                    {
                        case IA.Config.Objects.Settings.Control.CHECKBOX: subConditionControl = new CheckBox(); break;
                        case IA.Config.Objects.Settings.Control.RADIOBUTTON: subConditionControl = new RadioButton(); break;
                        case Config.Objects.Settings.Control.NOTHING:
                        default:
                            throw new Exception("Неизвестный тип элемента управления для стадии.");
                    }

                    subConditionControl.Text = subCondition.Name;
                    subConditionControl.Anchor = AnchorStyles.Left | AnchorStyles.Right;

                    //При смене отметки - автоматически проставляем пресет
                    subConditionControl.MouseClick += (sender, e) => this.Preset_SetAutomatically();

                    //Создаём поднастройку
                    Setting subSetting = new Setting(subConditionID, subConditionControl);
                    subSetting.CheckedChangedEvent += this.Setting_ControlChecked_Changed;

                    //Добавляем поднастройку в список настроек
                    this.settings.Add(subSetting);

                    conditions_tableLayoutControl.RowStyles.Add(new RowStyle(SizeType.Percent, 100 / subConditionsIDs.Count));
                    conditions_tableLayoutControl.Controls.Add(subConditionControl, 1, subConditionsControlsCounter++);
                }

                //Добавляем таблицу условий в групбокс стадии
                conditionControl.Controls.Add(conditions_tableLayoutControl);

                //Добавляем групбокс стадии в общую таблицу настроек
                settings_tableLayoutPanel.RowCount++;
                settings_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, conditionControl.Height));
                settings_tableLayoutPanel.Controls.Add(conditionControl, 0, conditionsControlsCounter++);
            }

            //Добавляем автоматически подстраиваемую под размер элемента управления строку
            settings_tableLayoutPanel.RowCount++;
            settings_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));

            //Добавляем таблицу настроек проекта в главную таблицу эелемента управления
            main_tableLayoutPanel.Controls.Add(settings_tableLayoutPanel, 0, 1);

            //Генерируем настройки
            Generate();
        }

        /// <summary>
        /// Сформировать список настроек для выбранного проекта
        /// </summary>
        private void Generate()
        {
            //Меняем заголовок элемента управления
            main_groupBox.Text = ThisDefaultName + (this.project == null ? String.Empty : " <" + this.project.Name + ">");

            //Если текущий проект не выбран, ничего более не делаем
            if (project == null)
            {
                //Ни одна настройка не выбрана
                this.settings.ForEach(s => s.Checked = false);

                return;
            }

            //Отмечаем выбранные стадии и условия
            Settings_FromSet(new Config.Objects.Settings.Set(this.project.Settings));

            //Автоматически проставляем значение пресета по выбранным настройкам
            Preset_SetAutomatically();
        }

        #region Пресеты
        /// <summary>
        /// Установить пресет "вручную".
        /// </summary>
        /// <param name="preset">Пресет</param>
        private void Preset_SetManually(IA.Config.Objects.Settings.Preset preset)
        {
            //Проверка на разумность
            if (preset == null)
                throw new Exception("Ошибка при отображении пресета. Пресет не определён.");

            Settings_FromSet(preset.DefaultSettingsSet);
        }

        /// <summary>
        /// Установить пресет "автоматически". Пресет автоматически определяется по выбранным настройкам.
        /// </summary>
        private void Preset_SetAutomatically()
        {
            //Получаем пресет по выбранным стадиям
            IA.Config.Objects.Settings.Preset preset = Preset_GetAutomatically();

            //Отображаем выбранный пресет
            //presets_comboBox.Text = preset == null ? CustomPresetName : preset.Name;
            presets_comboBox.SelectedItem = preset;
        }

        /// <summary>
        /// Получить пресет по выбранным настройкам.
        /// </summary>
        /// <returns></returns>
        private IA.Config.Objects.Settings.Preset Preset_GetAutomatically()
        {
            //Получаем набор настроек по выбранным настройкам
            IA.Config.Objects.Settings.Set currentSettingsSet = Settings_ToSet();

            //Инициализируем список отобранных пресетов при поиске
            List<IA.Config.Objects.Settings.Preset> pickedPresets = new List<IA.Config.Objects.Settings.Preset>();

            //Проходим по всем пресетам
            foreach (IA.Config.Objects.Settings.Preset preset in this.configuration.Presets)
                //Если множество обязательных настроек пресета является подмножеством множества настроек, отмеченных пользователем
                if (preset.CompulsorySettingsSet.IsSubsetOf(currentSettingsSet))
                    //Если множество обязательных настроек пресета совпадает с множеством настроек, отмеченных пользователем
                    if (preset.CompulsorySettingsSet.IsEqualsTo(currentSettingsSet))
                        return preset;
                    //Добавляем в список отобранных пресетов
                    else
                        pickedPresets.Add(preset);

            //Возвращаем пресет из списка отобранных с максимальным колчеством настроек. Если список пуст, то возвращаем - null, что означает обычный ручной режим выбора стадий.
            return pickedPresets.FirstOrDefault(preset => preset.CompulsorySettingsSet.ElementsCount() == pickedPresets.Max(p => p.CompulsorySettingsSet.ElementsCount()));
        }

        #endregion

        #region Настройки

        /// <summary>
        /// Обработчик - Изменилось состояние настройки (Выбрана\Не выбрана)
        /// </summary>
        /// <param name="sender">Настройка</param>
        /// <param name="value">Выбрана\Не выбрана</param>
        private void Setting_ControlChecked_Changed(object sender, bool value)
        {
            Setting setting = sender as Setting;

            if (setting == null)
                return;

            //Автомтически снимаем отметку со всех поднастроек
            if(!value && this.configuration.Structure.Elements.ContainsKey(setting.ID))
                this.settings.Where(s => this.configuration.Structure.Elements[setting.ID].Contains(s.ID)).ForEach(s => s.Checked = false);

            //Автоматически отмечаем поднастройки по умолчанию
            if(value && this.configuration.Defaults.Elements.ContainsKey(setting.ID))
                this.settings.Where(s => this.configuration.Defaults.Elements[setting.ID].Contains(s.ID)).ForEach(s => s.Checked = true);
        }

        /// <summary>
        /// Выбрать настройки в соответствии с набором настроек.
        /// </summary>
        /// <param name="set">Набор настроек.</param>
        private void Settings_FromSet(IA.Config.Objects.Settings.Set set)
        {
            //Размечаем настройки
            this.settings.ForEach(s => s.Checked = set.Elements.Keys.Contains(s.ID) || set.Elements.Values.Any(v => v.Contains(s.ID)));
        }

        /// <summary>
        /// Сформировать набор настроек в соответствии с выбранными настройками.
        /// </summary>
        /// <returns></returns>
        private IA.Config.Objects.Settings.Set Settings_ToSet()
        {
            IA.Config.Objects.Settings.Set set = new Config.Objects.Settings.Set(null);

            foreach(uint conditionID in this.configuration.Structure.Elements.Keys)
            {
                if(!this.settings.First(s => s.ID == conditionID).Checked)
                    continue;
                
                set.Elements.Add(conditionID, new HashSet<uint>());

                foreach (uint subConditionID in this.configuration.Structure.Elements[conditionID])
                {
                    if (!this.settings.First(s => s.ID == subConditionID).Checked)
                        continue;

                    set.Elements[conditionID].Add(subConditionID);
                }
            }

            return set;
        }
        #endregion
    }
}
