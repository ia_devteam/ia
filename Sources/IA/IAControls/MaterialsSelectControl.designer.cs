﻿using IA.Dialogs;
using System.Drawing;
using System.Windows.Forms;

namespace IA.Controls
{
    partial class MaterialsSelectControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialsGroupBox = new System.Windows.Forms.GroupBox();
            this.materialsListControl = new IA.Controls.MaterialsListControl();
            this.materialsAndStorages = new System.Windows.Forms.SplitContainer();
            this.tabProjectsAndArrowLeft = new System.Windows.Forms.TableLayoutPanel();
            this.pBox = new System.Windows.Forms.PictureBox();
            this.storageSelectControl = new IA.Dialogs.StorageSelectControl();
            this.materialsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.materialsAndStorages)).BeginInit();
            this.materialsAndStorages.Panel1.SuspendLayout();
            this.materialsAndStorages.Panel2.SuspendLayout();
            this.materialsAndStorages.SuspendLayout();
            this.tabProjectsAndArrowLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).BeginInit();
            this.SuspendLayout();
            // 
            // materialsGroupBox
            // 
            this.materialsGroupBox.Controls.Add(this.materialsListControl);
            this.materialsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialsGroupBox.Location = new System.Drawing.Point(3, 3);
            this.materialsGroupBox.Name = "materialsGroupBox";
            this.materialsGroupBox.Size = new System.Drawing.Size(578, 148);
            this.materialsGroupBox.TabIndex = 0;
            this.materialsGroupBox.TabStop = false;
            this.materialsGroupBox.Text = "Выбор материалов в проекте";
            // 
            // materialsListControl
            // 
            this.materialsListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialsListControl.Location = new System.Drawing.Point(3, 16);
            this.materialsListControl.Name = "materialsListControl";
            this.materialsListControl.Size = new System.Drawing.Size(572, 129);
            this.materialsListControl.TabIndex = 0;
            this.materialsListControl.MaterialsClickEvent += this.materialsListControl_MaterialsChosen;
            // 
            // materialsAndStorages
            // 
            this.materialsAndStorages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialsAndStorages.Location = new System.Drawing.Point(0, 0);
            this.materialsAndStorages.Name = "materialsAndStorages";
            this.materialsAndStorages.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // materialsAndStorages.Panel1
            // 
            this.materialsAndStorages.Panel1.Controls.Add(this.tabProjectsAndArrowLeft);
            // 
            // materialsAndStorages.Panel2
            // 
            this.materialsAndStorages.Panel2.Controls.Add(this.storageSelectControl);
            this.materialsAndStorages.Size = new System.Drawing.Size(584, 511);
            this.materialsAndStorages.SplitterDistance = 204;
            this.materialsAndStorages.TabIndex = 2;
            // 
            // tabProjectsAndArrowLeft
            // 
            this.tabProjectsAndArrowLeft.ColumnCount = 1;
            this.tabProjectsAndArrowLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 584F));
            this.tabProjectsAndArrowLeft.Controls.Add(this.materialsGroupBox, 0, 0);
            this.tabProjectsAndArrowLeft.Controls.Add(this.pBox, 1, 0);
            this.tabProjectsAndArrowLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabProjectsAndArrowLeft.Location = new System.Drawing.Point(0, 0);
            this.tabProjectsAndArrowLeft.Name = "tabProjectsAndArrowLeft";
            this.tabProjectsAndArrowLeft.RowCount = 2;
            this.tabProjectsAndArrowLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tabProjectsAndArrowLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tabProjectsAndArrowLeft.Size = new System.Drawing.Size(584, 204);
            this.tabProjectsAndArrowLeft.TabIndex = 1;
            // 
            // pBox
            // 
            this.pBox.Image = Properties.Resources.Common_Button_Back;
            this.pBox.Location = new System.Drawing.Point(3, 157);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(50, 44);
            this.pBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pBox.TabIndex = 1;
            this.pBox.TabStop = false;
            // 
            // storageSelectControl
            // 
            this.storageSelectControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.storageSelectControl.Location = new System.Drawing.Point(0, 0);
            this.storageSelectControl.Name = "storageSelectControl";
            this.storageSelectControl.Size = new System.Drawing.Size(584, 303);
            this.storageSelectControl.TabIndex = 0;
            // 
            // MaterialsSelectControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.materialsAndStorages);
            this.Name = "MaterialsSelectControl";
            this.Size = new System.Drawing.Size(584, 511);
            this.materialsGroupBox.ResumeLayout(false);
            this.materialsAndStorages.Panel1.ResumeLayout(false);
            this.materialsAndStorages.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.materialsAndStorages)).EndInit();
            this.materialsAndStorages.ResumeLayout(false);
            this.tabProjectsAndArrowLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox materialsGroupBox;
        private System.Windows.Forms.TableLayoutPanel tabProjectsAndArrowLeft;
        private System.Windows.Forms.SplitContainer materialsAndStorages;
        private PictureBox pBox;
        private Controls.MaterialsListControl materialsListControl;
        private StorageSelectControl storageSelectControl;
    }
}