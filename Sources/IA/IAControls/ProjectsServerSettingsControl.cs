﻿using System;
using System.Windows.Forms;

using IOController;

namespace IA.Controls
{
    /// <summary>
    /// Класс, реализующий элемент управления - Настройки сервера
    /// </summary>
    public partial class ProjectsServerSettingsControl : UserControl
    {
        /// <summary>
        /// Имя сервера проектов
        /// </summary>
        public new string Name
        {
            get
            {
                return name_textBox.Text.Trim();
            }
            set
            {
                name_textBox.Text = value ?? string.Empty;
            }
        }

        /// <summary>
        /// Имя пользователя для подключения к серверу проектов
        /// </summary>
        public string Login
        {
            get
            {
                return login_textBox.Text.Trim();
            }
            set
            {
                login_textBox.Text = value ?? string.Empty;
            }
        }

        /// <summary>
        /// Пароль для подключения к серверу проектов
        /// </summary>
        public string Password
        {
            get
            {
                return password_textBox.Text.Trim();
            }
            set
            {
                password_textBox.Text = value ?? string.Empty;
            }
        }

        /// <summary>
        /// Свойство - Директория на сервере для хранинения проектов
        /// </summary>
        public string ProjectsDirectoryPath
        {
            get
            {
                return System.IO.Path.Combine(projectsDirectoryPath_prefix_textBox.Text.Trim(), projectsDirectoryPath_postfix_textBox.Text.Trim());
            }
            set
            {
                //Получаем текущее имя сервера
                string name = name_textBox.Text;

                //Отображаем префикс и постфикс пути
                projectsDirectoryPath_prefix_textBox.Text = String.IsNullOrWhiteSpace(name) ? String.Empty : "\\\\" + name + "\\";
                if (value.IndexOf(name) == -1)
                {
                    Monitor.Log.Error("Выбор директории проектов", "Выбранная директория должна размещаться на сервере проектов!", true);
                }
                else
                {
                    projectsDirectoryPath_postfix_textBox.Text = (String.IsNullOrWhiteSpace(name) || value == null || value.IndexOf(name) == -1) ? String.Empty : value.Substring(value.IndexOf(name) + name.Length).Trim('\\');
                }
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public ProjectsServerSettingsControl()
        {
            InitializeComponent();

            //Добавляем автоматическое формирование префикса директории проектов на сервере
            name_textBox.TextChanged += delegate(object sender, EventArgs e)
            {
                projectsDirectoryPath_prefix_textBox.Text = String.IsNullOrWhiteSpace(name_textBox.Text) ? String.Empty : "\\\\" + name_textBox.Text + "\\";
            };
        }

        /// <summary>
        /// Обработчик - Нажали на клавишу выбора директории для хранения проектов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void serverDirectory_button_Click(object sender, EventArgs e)
        {
            //Показываем диалог выбора директории
            string path = DirectoryController.SelectByUser(this.ProjectsDirectoryPath, enableCreateFolder: true);

            //Если ничего не выбрали, то ничего не делаем
            if (path == null)
                return;

            //Отображаем выбранный путь
            this.ProjectsDirectoryPath = path;
        }
    }
}
