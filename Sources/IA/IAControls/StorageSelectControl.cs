﻿using System;
using System.Windows.Forms;

using IA.Controls;
using IA.Extensions;
using IA.Objects;

namespace IA.Dialogs
{
    /// <summary>
    /// Контрол для выбора хранилища
    /// </summary>
    public partial class StorageSelectControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Контрол выбора Хранилища";
                
        /// <summary>
        /// Текущие материалы
        /// </summary>
        private Materials materials = null;

        /// <summary>
        /// Текущий пользователь
        /// </summary>
        private User user = null;

        /// <summary>
        /// родительская форма
        /// </summary>
        Control _parent = null;

        /// <summary>
        /// Содержит созданные ранее хранилища? 
        /// </summary>
        public bool ContainsStorages = true;

        Control parent
        {
            get
            {
                if (_parent == null)
                {
                    this._parent = Parent;
                    while (this.parent.Parent != null)
                        this._parent = this.parent.Parent;
                }
                return _parent;
            }
        }

        public void Rebuild(Materials materials, User user)
        {
            this.materials = materials;

            //Текущий пользователь
            this.user = user;

            storageTreeControl.Rebuild(materials, user);

            if (Storage.GetByMaterials(materials.ID).Count == 0)
            {
                // еще не создано ни одного хранилища. Надо создать хранилище автоматически и выбрать его. 
                storageTreeControl.Storage_Add_ActionHandler();
                this.storageTreeControl.actions[Storage.EditOperation.CHOOSE].ActionHandler(null, this.storageTreeControl.actions[Storage.EditOperation.CHOOSE]);
                ContainsStorages = false;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="materials">Набор материалов</param>
        /// <param name="user">Пользователь</param>
        /// <param name="parent">Родительская форма</param>        
        public StorageSelectControl()
        {
            InitializeComponent();

            this.storageSettingsControl.IsBlocked = false;
            this.storageSettingsControl.IsEnabled = false;
            this.pluginListControl.IsEnabled = false;
            this.pluginListControl.IsReadOnly = true;

            this.pluginListControl.LoadPlugins();
            
            this.storageTreeControl.StorageClickEvent += this.storageTreeControl_Click;
            this.storageTreeControl.StorageDoubleClickEvent += IAUtils.IACommon.StorageDoubleClick;
            this.storageTreeControl.StorageUnloadClickEvent += IAUtils.IACommon.StorageUnloadClick;

        }

        /// <summary>
        /// Обработчик - Один раз кликнули по Хранилищу
        /// </summary>
        /// <param name="storage"></param>
        private void storageTreeControl_Click(Storage storage)
        {
            try
            {
                //Определяем доступен ли список свойств
                storageSettingsControl.IsEnabled = storage != null;

                //Определем доступность списка плагинов
                pluginListControl.IsEnabled = storage != null && !storage.IsEmpty;

                //Если Хранилище выбрано, отображаем его настройки
                if (storage != null)
                    //Отображаем настройки Хранилища
                    storageSettingsControl.Storage = storage;

                //Если Хранилище не выбрано, ничего не делаем
                if (storage == null || storage.IsEmpty)
                    return;

                //Помечаем все плагины как невыполненные
                Plugin.HandlersInitial.GetAll().ForEach(p => p.RunResult = Plugin.Handler.ExecutionResult.NO);

                //Отсутствующие плагины можно смело пропускать
                Func<uint, bool> isMissAllowedCondition = (pluginID) => true;

                //Помечаем выполненные в рамках Хранилища плагины
                Plugin.HandlersInitial.GetSomeByIDs(
                    storage.Runned_Plugins, 
                    isMissAllowedCondition
                ).ForEach(p => p.RunResult = Plugin.Handler.ExecutionResult.SUCCESS);

                //Помечаем завершённые в рамках Хранилища плагины
                Plugin.HandlersInitial.GetSomeByIDs(
                    storage.Completed_Plugins,
                    isMissAllowedCondition
                ).ForEach(p => p.IsCompleted = true);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Однократное нажатие на Хранилище в дереве", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
    }
}
