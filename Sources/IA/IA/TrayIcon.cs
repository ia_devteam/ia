﻿using System.Drawing;
using System.Windows.Forms;

namespace IA.MainForm
{
    /// <summary>
    /// Класс, описывающий иконку в трее
    /// </summary>
    internal class TrayIcon
    {
        /// <summary>
        /// Иконка в трее
        /// </summary>
        private NotifyIcon icon = null;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        internal TrayIcon()
        {
            //Инициализация иконки в трее
            icon = new NotifyIcon()
            {
                BalloonTipIcon = ToolTipIcon.Info,
                Icon = Icon.FromHandle(Properties.Resources.Application_Icon.GetHicon()),
                Visible = true
            };
        }

        /// <summary>
        /// Задать текст, отображаемый при наведении на иконку
        /// </summary>
        /// <param name="text">Текст</param>
        internal void SetText(string text)
        {
            icon.Text = text;
        }

        /// <summary>
        /// Задать обрабочик нажатия по иконке кнопкой мыши
        /// </summary>
        /// <param name="mouseEventHandler">Обработчик</param>
        internal void SetMouseClickEventHandler(MouseEventHandler mouseEventHandler)
        {
            icon.MouseClick += mouseEventHandler;
        }

        /// <summary>
        /// Отобразить всплывающее сообщение на заданное количество секунд
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        /// <param name="seconds">Количество секунд</param>
        internal void ShowMessage(string message, int seconds = 10)
        {
            icon.BalloonTipText = message;
            icon.ShowBalloonTip(seconds * 1000);
        }

        /// <summary>
        /// Спрятать иконку
        /// </summary>
        internal void Hide()
        {
            icon.Visible = false;
        }
    }
}
