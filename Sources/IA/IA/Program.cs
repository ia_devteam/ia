﻿/*
 * Главный интерфейс
 * Файл Program.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Лошкарёв, Юхтин
 * 
 * точка входа
 */

using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

using IA.Extensions;

namespace IA
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Проверка на отстутствие дубликатов приложения
            bool isNoDuplicates;
            Mutex mutex = new Mutex(true, "IAApplication", out isNoDuplicates);

            if (!isNoDuplicates)
            {
                string message = "Запущен еще один экземпляр приложения " + Constants.Application.ShortName + ". Не допускаете одновременного образения на запись к базе данных.";

                //Отображаем ошибку
                MessageBox.Show(message, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                // ни один экземпляр IA не запущен. Можно смело чистить рабочуюю папку (если не в автономном режиме)
                if (!String.IsNullOrWhiteSpace(IA.Settings.CommonWorkDirectoryPath))
                    try
                    {
                        IOController.DirectoryController.Clear(IA.Settings.CommonWorkDirectoryPath, true);
                    }
                    catch (System.IO.IOException)
                    {
                        MessageBox.Show("Не удается очистить рабочую директорию. Один или несколько файлов открыты в другой программе. Очистка не завершена. ");
                        Monitor.Log.Error("Очистка рабочей папки", "Не удается очистить рабочую директорию. Один или несколько файлов открыты в другой программе. Очистка не завершена. ");
                    }
            }

            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm.MainForm());
            }
            catch (Exception ex)
            {
                string message = new string[]
                {
                    "Произошла критическая ошибка в ходе работы приложения.",
#if DEBUG
                    ex.ToFullMultiLineMessage(),
#endif
                    Constants.Message.ApplicationWillBeClosed,
                    Constants.Message.СontactTheManufacturer
                }.ToMultiLineString();

                //Отображаем ошибку
                MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

#if !DEBUG
                //Записываем ошибку в файл
                string errorLogFilePath = Path.Combine(Application.UserAppDataPath, Constants.Files.ErrorLogFileName);
                System.IO.File.WriteAllText(errorLogFilePath, ex.ToFullMultiLineMessage());
#endif

                //Принудительно закрываем приложение
                Environment.Exit(1);

            }
            finally
            {
                //Теперь этот объект больше не нужен, его можно уничтожить
                GC.KeepAlive(mutex);
            }
        }
    }
}
