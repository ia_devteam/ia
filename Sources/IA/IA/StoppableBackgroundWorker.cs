﻿using System.ComponentModel;
using System.Threading;

namespace StoppableBackgroundWorker
{
    public class StoppableBackgroundWorker : BackgroundWorker
    {
        private Thread workerThread;

        public bool Suspended
        {
            get
            { 
                if(workerThread == null)
                    return true;
                return (workerThread.ThreadState & ThreadState.Suspended) == ThreadState.Suspended;
            }
        }

        protected override void OnDoWork(DoWorkEventArgs e)
        {
            workerThread = Thread.CurrentThread;
            try
            {
                base.OnDoWork(e);
            }
            catch (ThreadAbortException)
            {
                e.Cancel = true; //We must set Cancel property to true!
                Thread.ResetAbort(); //Prevents ThreadAbortException propagation
            }
        }

        public void Abort()
        {
            if (workerThread != null && workerThread.ThreadState != System.Threading.ThreadState.AbortRequested)
            {
                workerThread.Abort();
                workerThread = null;
            }
        }

        public void Suspend()
        {
            if (workerThread != null)
            {
                workerThread.Suspend();
            }
        }

        public void Resume()
        {
            if (workerThread != null)
            {
                workerThread.Resume();
            }
        }
    }
}
