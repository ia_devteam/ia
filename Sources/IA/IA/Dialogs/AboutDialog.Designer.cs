﻿namespace IA.Dialogs
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.aboutControl1 = new IA.Controls.AboutControl();
            this.SuspendLayout();
            // 
            // aboutControl1
            // 
            this.aboutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aboutControl1.Location = new System.Drawing.Point(0, 0);
            this.aboutControl1.Name = "aboutControl1";
            this.aboutControl1.Size = new System.Drawing.Size(484, 211);
            this.aboutControl1.TabIndex = 0;
            // 
            // AboutDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 211);
            this.Controls.Add(this.aboutControl1);
            this.Name = "AboutDialog";
            this.Text = "О программе";
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.AboutControl aboutControl1;
    }
}