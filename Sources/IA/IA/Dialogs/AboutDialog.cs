﻿using System.Windows.Forms;

using IA.Controls;
using IA.Extensions;

namespace IA.Dialogs
{
    /// <summary>
    /// Класс, описывающий диалог "О программе"
    /// </summary>
    public partial class AboutDialog : Form
    {
        //Элемент управления - "О программе"
        private AboutControl aboutControl = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        public AboutDialog()
        {
            InitializeComponent();

            //Задаём название формы
            this.Text = IA.Actions.Global.ABOUT.Description();
        }
    }
}
