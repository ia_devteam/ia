﻿using System.Data;
using System.Linq;
using System.Windows.Forms;

using IA.Objects;

namespace IA.Dialogs
{
    internal partial class AuthorizationDialog : Form
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Диалог авторизации";

        /// <summary>
        /// Выбранный пользователь
        /// </summary>
        internal User User
        {
            get
            {
                return user_comboBox.SelectedItem as User;
            }
            set
            {
                user_comboBox.SelectedItem = value == null ? null : user_comboBox.Items.Cast<User>().FirstOrDefault(u => u.ID == value.ID);
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        internal AuthorizationDialog()
        {
            InitializeComponent();

            //Задаём изображение кнопки входа
            enter_button.Image = Properties.Resources.Login_Button;

            //Задаём обработчики
            this.enter_button.Click += (sender, e) => this.ChooseSelectedUser();
                       
            Sql.SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);
        }

        /// <summary>
        /// Заполнить список пользователей для авторизации
        /// </summary>
        private void Generate()
        {
            //Очищаем список пользователей для авторизации
            this.Clear();

            //Заполняем список зарегистрированных пользователей в системе (кроме помеченных, как удёленные)
            user_comboBox.Items.AddRange(User.GetAll().Where(u => !u.IsDeleted).ToArray());
        }

        /// <summary>
        /// Очистить список пользователей для авторизации
        /// </summary>
        private void Clear()
        {
            user_comboBox.Items.Clear();
        }

        /// <summary>
        /// Выбор указанного в данный момент пользователя
        /// </summary>
        private void ChooseSelectedUser()
        {
            //Проверка на то, что пользователь выбран
            if (user_comboBox.SelectedItem == null)
            {
                Monitor.Log.Error(ObjectName, "Пользователь не выбран.", true);
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Обработчик - Нажали на клавишу на клавиатуре
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AuthorizationDialog_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        //Выбираем указанного в данный момент пользователя
                        this.ChooseSelectedUser();

                        return;
                    }
                case Keys.Escape:
                    {
                        //Закрываем диалог
                        this.DialogResult = DialogResult.Cancel;
                        this.Close();

                        return;
                    }
                default:
                    return;
            }
        }
        
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления в соответствии с содержимым базы данных
            this.Generate();

            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Делаем его недоступным
            this.Enabled = false;
        }
    }
}
