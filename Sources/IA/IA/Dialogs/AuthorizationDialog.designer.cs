﻿namespace IA.Dialogs
{
    partial class AuthorizationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.enter_button = new System.Windows.Forms.Button();
            this.user_comboBox = new System.Windows.Forms.ComboBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.user_label = new System.Windows.Forms.Label();
            this.user_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.mainLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.user_tableLayoutPanel.SuspendLayout();
            this.mainLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // enter_button
            // 
            this.enter_button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.enter_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.enter_button.Location = new System.Drawing.Point(3, 47);
            this.enter_button.Name = "enter_button";
            this.enter_button.Size = new System.Drawing.Size(328, 211);
            this.enter_button.TabIndex = 0;
            this.enter_button.Text = "Вход";
            this.enter_button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.enter_button.UseVisualStyleBackColor = true;
            // 
            // user_comboBox
            // 
            this.user_comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.user_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.user_comboBox.Location = new System.Drawing.Point(92, 8);
            this.user_comboBox.Name = "user_comboBox";
            this.user_comboBox.Size = new System.Drawing.Size(233, 21);
            this.user_comboBox.Sorted = true;
            this.user_comboBox.TabIndex = 2;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // user_label
            // 
            this.user_label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.user_label.Location = new System.Drawing.Point(3, 7);
            this.user_label.Name = "user_label";
            this.user_label.Size = new System.Drawing.Size(83, 23);
            this.user_label.TabIndex = 3;
            this.user_label.Text = "Пользователь:";
            this.user_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // user_tableLayoutPanel
            // 
            this.user_tableLayoutPanel.ColumnCount = 2;
            this.user_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.user_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.user_tableLayoutPanel.Controls.Add(this.user_label, 0, 0);
            this.user_tableLayoutPanel.Controls.Add(this.user_comboBox, 1, 0);
            this.user_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.user_tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.user_tableLayoutPanel.Name = "user_tableLayoutPanel";
            this.user_tableLayoutPanel.RowCount = 1;
            this.user_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.user_tableLayoutPanel.Size = new System.Drawing.Size(328, 38);
            this.user_tableLayoutPanel.TabIndex = 4;
            // 
            // mainLayoutPanel
            // 
            this.mainLayoutPanel.ColumnCount = 1;
            this.mainLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayoutPanel.Controls.Add(this.enter_button, 0, 1);
            this.mainLayoutPanel.Controls.Add(this.user_tableLayoutPanel, 0, 0);
            this.mainLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainLayoutPanel.Name = "mainLayoutPanel";
            this.mainLayoutPanel.RowCount = 2;
            this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayoutPanel.Size = new System.Drawing.Size(334, 261);
            this.mainLayoutPanel.TabIndex = 5;
            // 
            // AuthorizationDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 261);
            this.Controls.Add(this.mainLayoutPanel);
            this.KeyPreview = true;
            this.Name = "AuthorizationDialog";
            this.Text = "Окно авторизации";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AuthorizationDialog_KeyDown);
            this.user_tableLayoutPanel.ResumeLayout(false);
            this.mainLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button enter_button;
        private System.Windows.Forms.ComboBox user_comboBox;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Label user_label;
        private System.Windows.Forms.TableLayoutPanel user_tableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel mainLayoutPanel;
    }
}