﻿namespace IA.MainForm
{
    partial class StoragePackProcessing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TracesPaths = new System.Windows.Forms.DataGridView();
            this.Проект_Материалы_Комментарий = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Path2Traces = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialsList = new System.Windows.Forms.DataGridView();
            this.PluginsListBox = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ReportsPathTextBox = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.CowabungaButton = new System.Windows.Forms.Button();
            this.CheckTracesPathsButton = new System.Windows.Forms.Button();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.BrowseStoragesListButton = new System.Windows.Forms.Button();
            this.Checked = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ProjectColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WasBeforeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.MaterialsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TracesPaths)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialsList)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel1.Controls.Add(this.TracesPaths, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.MaterialsList, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.PluginsListBox, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.ReportsPathTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox2, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.button2, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.CowabungaButton, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.CheckTracesPathsButton, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.SearchTextBox, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.SearchButton, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.BrowseStoragesListButton, 3, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1403, 608);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // TracesPaths
            // 
            this.TracesPaths.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TracesPaths.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Проект_Материалы_Комментарий,
            this.Path2Traces});
            this.tableLayoutPanel1.SetColumnSpan(this.TracesPaths, 3);
            this.TracesPaths.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TracesPaths.Location = new System.Drawing.Point(810, 30);
            this.TracesPaths.Name = "TracesPaths";
            this.tableLayoutPanel1.SetRowSpan(this.TracesPaths, 4);
            this.TracesPaths.Size = new System.Drawing.Size(590, 575);
            this.TracesPaths.TabIndex = 0;
            this.TracesPaths.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.TracesPaths_CellValueChanged);
            // 
            // Проект_Материалы_Комментарий
            // 
            this.Проект_Материалы_Комментарий.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Проект_Материалы_Комментарий.FillWeight = 30F;
            this.Проект_Материалы_Комментарий.HeaderText = "Проект-Материалы-Комментарий";
            this.Проект_Материалы_Комментарий.Name = "Проект_Материалы_Комментарий";
            this.Проект_Материалы_Комментарий.ReadOnly = true;
            // 
            // Path2Traces
            // 
            this.Path2Traces.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Path2Traces.FillWeight = 70F;
            this.Path2Traces.HeaderText = "Путь";
            this.Path2Traces.Name = "Path2Traces";
            // 
            // MaterialsList
            // 
            this.MaterialsList.AllowUserToAddRows = false;
            this.MaterialsList.AllowUserToDeleteRows = false;
            this.MaterialsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MaterialsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Checked,
            this.ProjectColumn,
            this.WasBeforeColumn,
            this.MaterialsColumn,
            this.CommentColumn});
            this.tableLayoutPanel1.SetColumnSpan(this.MaterialsList, 3);
            this.MaterialsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MaterialsList.Location = new System.Drawing.Point(3, 30);
            this.MaterialsList.Name = "MaterialsList";
            this.tableLayoutPanel1.SetRowSpan(this.MaterialsList, 3);
            this.MaterialsList.Size = new System.Drawing.Size(521, 545);
            this.MaterialsList.TabIndex = 1;
            // 
            // PluginsListBox
            // 
            this.PluginsListBox.CheckOnClick = true;
            this.PluginsListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PluginsListBox.FormattingEnabled = true;
            this.PluginsListBox.Location = new System.Drawing.Point(530, 3);
            this.PluginsListBox.Name = "PluginsListBox";
            this.tableLayoutPanel1.SetRowSpan(this.PluginsListBox, 2);
            this.PluginsListBox.Size = new System.Drawing.Size(274, 516);
            this.PluginsListBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 27);
            this.label1.TabIndex = 3;
            this.label1.Text = "Путь к отчетам";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(810, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 27);
            this.label2.TabIndex = 4;
            this.label2.Text = "Путь к трассам";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ReportsPathTextBox
            // 
            this.ReportsPathTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReportsPathTextBox.Location = new System.Drawing.Point(103, 3);
            this.ReportsPathTextBox.Name = "ReportsPathTextBox";
            this.ReportsPathTextBox.Size = new System.Drawing.Size(329, 20);
            this.ReportsPathTextBox.TabIndex = 5;
            // 
            // textBox2
            // 
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.Location = new System.Drawing.Point(910, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(404, 20);
            this.textBox2.TabIndex = 6;
            this.textBox2.Text = "\\\\tishare\\Ti\\Синтез_ОС_7\\Packages_20161205\\Выгрузка результатов\\";
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(438, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 21);
            this.button1.TabIndex = 7;
            this.button1.Text = "Обзор";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(1320, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 21);
            this.button2.TabIndex = 8;
            this.button2.Text = "Обзор";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // CowabungaButton
            // 
            this.CowabungaButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CowabungaButton.Location = new System.Drawing.Point(530, 581);
            this.CowabungaButton.Name = "CowabungaButton";
            this.CowabungaButton.Size = new System.Drawing.Size(274, 24);
            this.CowabungaButton.TabIndex = 0;
            this.CowabungaButton.Text = "Cowabunga";
            this.CowabungaButton.UseVisualStyleBackColor = true;
            this.CowabungaButton.Click += new System.EventHandler(this.CowabungaButton_Click);
            // 
            // CheckTracesPathsButton
            // 
            this.CheckTracesPathsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CheckTracesPathsButton.Location = new System.Drawing.Point(530, 553);
            this.CheckTracesPathsButton.Name = "CheckTracesPathsButton";
            this.CheckTracesPathsButton.Size = new System.Drawing.Size(274, 22);
            this.CheckTracesPathsButton.TabIndex = 9;
            this.CheckTracesPathsButton.Text = "Проверить пути к трассам";
            this.CheckTracesPathsButton.UseVisualStyleBackColor = true;
            this.CheckTracesPathsButton.Click += new System.EventHandler(this.CheckTracesPathsButton_Click);
            // 
            // SearchTextBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.SearchTextBox, 2);
            this.SearchTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchTextBox.Location = new System.Drawing.Point(3, 581);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(429, 20);
            this.SearchTextBox.TabIndex = 10;
            // 
            // SearchButton
            // 
            this.SearchButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchButton.Location = new System.Drawing.Point(438, 581);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(86, 24);
            this.SearchButton.TabIndex = 11;
            this.SearchButton.Text = "Поиск";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // BrowseStoragesListButton
            // 
            this.BrowseStoragesListButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BrowseStoragesListButton.Location = new System.Drawing.Point(530, 525);
            this.BrowseStoragesListButton.Name = "BrowseStoragesListButton";
            this.BrowseStoragesListButton.Size = new System.Drawing.Size(274, 22);
            this.BrowseStoragesListButton.TabIndex = 12;
            this.BrowseStoragesListButton.Text = "Задать список пакетов";
            this.BrowseStoragesListButton.UseVisualStyleBackColor = true;
            this.BrowseStoragesListButton.Click += new System.EventHandler(this.BrowseStoragesListButton_Click);
            // 
            // Checked
            // 
            this.Checked.HeaderText = "";
            this.Checked.Name = "Checked";
            this.Checked.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Checked.Width = 30;
            // 
            // ProjectColumn
            // 
            this.ProjectColumn.HeaderText = "Проект";
            this.ProjectColumn.Name = "ProjectColumn";
            this.ProjectColumn.ReadOnly = true;
            this.ProjectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ProjectColumn.Width = 200;
            // 
            // WasBeforeColumn
            // 
            this.WasBeforeColumn.HeaderText = "Был";
            this.WasBeforeColumn.Name = "WasBeforeColumn";
            this.WasBeforeColumn.ReadOnly = true;
            // 
            // MaterialsColumn
            // 
            this.MaterialsColumn.HeaderText = "Материалы";
            this.MaterialsColumn.Name = "MaterialsColumn";
            this.MaterialsColumn.ReadOnly = true;
            this.MaterialsColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.MaterialsColumn.Width = 150;
            // 
            // CommentColumn
            // 
            this.CommentColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CommentColumn.HeaderText = "Комментарий";
            this.CommentColumn.Name = "CommentColumn";
            this.CommentColumn.ReadOnly = true;
            this.CommentColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // StoragePackProcessing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1403, 608);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "StoragePackProcessing";
            this.Text = "StoragePackProcessing";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TracesPaths)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaterialsList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button CowabungaButton;
        private System.Windows.Forms.DataGridView MaterialsList;
        private System.Windows.Forms.CheckedListBox PluginsListBox;
        private System.Windows.Forms.DataGridView TracesPaths;
        private System.Windows.Forms.DataGridViewTextBoxColumn PathTraces;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ReportsPathTextBox;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Проект_Материалы_Комментарий;
        private System.Windows.Forms.DataGridViewTextBoxColumn Path2Traces;
        private System.Windows.Forms.Button CheckTracesPathsButton;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Button BrowseStoragesListButton;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Checked;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn WasBeforeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialsColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommentColumn;
    }
}