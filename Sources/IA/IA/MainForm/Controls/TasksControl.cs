﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using IA.Extensions;

namespace IA.MainForm.Controls
{
    using TaskUpdateHistory = Dictionary<Monitor.Tasks.Task.UpdateType, ulong>;
    using PluginUpdateHistory = Dictionary<string, Dictionary<Monitor.Tasks.Task.UpdateType, ulong>>;
    using UpdateBuffer = Dictionary<string, Dictionary<string, Dictionary<Monitor.Tasks.Task.UpdateType, ulong>>>;
    using static ProgressControl;
    /// <summary>
    /// Класс, реализующий элемень упарвления (Окно тасков).
    /// </summary>
    internal partial class TasksControl : UserControl, Monitor.Tasks.Interface
    {
        /// <summary>
        /// Буфер обновления ткущей информации о тасках.
        /// </summary>
        private UpdateBuffer buffer = new UpdateBuffer();

        /// <summary>
        /// Объект класса, реализующий обновление информации по таймеру
        /// </summary>
        private UpdateByTimer updateByTimer = null;

        #region Делегаты и события
        /// <summary>
        /// Делегат для собятия - Значение прогресса плагина было изменено
        /// </summary>
        /// <param name="info"></param>
        public delegate void PluginProgressChangedEventHandler(Information info);

        /// <summary>
        /// Событие - Значение прогресса плагина было изменено
        /// </summary>
        public event PluginProgressChangedEventHandler PluginProgressChangedEvent;

        /// <summary>
        /// Делегат для события - Значение общего прогресса было изменено
        /// </summary>
        /// <param name="info"></param>
        public delegate void CommonProgressChangedEventHandler(Information info);

        /// <summary>
        /// Событие - Значение общего прогресса было изменено
        /// </summary>
        public event CommonProgressChangedEventHandler CommonProgressChangedEvent;
        #endregion

        /// <summary>
        /// Информация о прогрессе плагина.
        /// </summary>
        private Information PluginProgressInformation
        {
            set
            {
                //Генерируем событие
                if (PluginProgressChangedEvent != null)
                    PluginProgressChangedEvent(value);
            }
        }

        /// <summary>
        /// Информация об общем прогрессе.
        /// </summary>
        private Information CommonProgressInformation
        {
            set
            {
                //Генерируем событие
                if (CommonProgressChangedEvent != null)
                    CommonProgressChangedEvent(value);
            }
        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        internal TasksControl()
        {
            InitializeComponent();
            //Инициализируем объект класса, реализующий обновление информации по таймеру
            updateByTimer = new UpdateByTimer(this.UpdateFromBuffer, 100);

            //Что необходимо сделать после инициализации элемента управления
            this.Load += delegate (object sender, EventArgs e)
            {
                //Включаем двойную буферизацию для отключения мерцания при обновлении
                ControlExtentions.EnableDoubleBuffer(main_listView);
            };

            this.main_listView.Tag = new Information(0, 0);
            
            main_listView.SizeChanged += delegate (object o, EventArgs e)
            {
                main_listView.Columns["Name"].Width = (int)((main_listView.Width - 10) * 0.6) - 4 - SystemInformation.VerticalScrollBarWidth;
                main_listView.Columns["Tasks"].Width = (int)((main_listView.Width - 10) * 0.4);
            };
        }

        /// <summary>
        /// Очистить список тасков
        /// </summary>
        public void Clear()
        {
            //Очищаем буфер обновления
            buffer.Clear();

            //Не показываем никаких тасков
            main_listView.Groups.Clear();
            main_listView.Items.Clear();

            //Прогресс нулевой
            main_listView.Tag = new Information(0, 0);
            MainForm.numbersOfPluginsToRun = 0;

            //Отображаем нулевой прогресс
            this.PluginProgressInformation = new Information();
            this.CommonProgressInformation = new Information();
        }

        /// <summary>
        /// Дабавить таски для отображения
        /// </summary>
        /// <param name="tasks">Список тасков для добавления</param>
        public void AddRange(List<Monitor.Tasks.Task> tasks)
        {
            //Если список тасков не опрделён, ничего не делаем
            if (tasks == null)
                return;

            //Проходим по всем таскам
            foreach (Monitor.Tasks.Task task in tasks)
            {
                //Проверка на разумность
                if (task.Stage != 0)
                    throw new Exception("Попытка добавить таск с ненулевым начальным значнием.");

                //Сохраняем информацию о таске
                if (main_listView.Groups[task.PluginName] != null)
                {
                    //Проверка на разумность
                    if (main_listView.Groups[task.PluginName].Items.ContainsKey(task.Name))
                        throw new Exception("Попытка дважды добавить одини и тот же таск в список на отображение.");

                }
                else
                {
                    //Создаём группу
                    ListViewGroup group = new ListViewGroup()
                    {
                        Name = task.PluginName,
                        Header = task.PluginName,
                        HeaderAlignment = HorizontalAlignment.Center,
                        Tag = new Information(0, 0)
                    };

                    main_listView.Groups.Add(group);
                }

                //Обновляем информацию о прогрессе для группы и всего листвью
                (main_listView.Tag as Information).Stages += task.Stages;
                (main_listView.Groups[task.PluginName].Tag as Information).Stages += task.Stages;

                //Создаём элемент (1-й столбец)
                ListViewItem item = new ListViewItem()
                {
                    Group = main_listView.Groups[task.PluginName],
                    Name = task.Name,
                    Text = task.Name,
                    Tag = new Information(0, 0) { Stage = task.Stage, Stages = task.Stages }
                };

                //Создаём элемент (2-й столбец)
                ListViewItem.ListViewSubItem subItem = new ListViewItem.ListViewSubItem() { Name = "Tasks", Text = task.Stage + "/" + task.Stages };
                item.SubItems.Add(subItem);

                main_listView.Items.Add(item);
            }
        }

        /// <summary>
        /// Обновить текущую информацию о заданном таске
        /// </summary>
        /// <param name="pluginName">Имя плагина</param>
        /// <param name="taskName">Имя таска</param>
        /// <param name="type">Тип обновления</param>
        /// <param name="value">Значение</param>
        public void Update(string pluginName, string taskName, Monitor.Tasks.Task.UpdateType type, ulong value)
        {
            updateByTimer.AddToBuffer(() => AddToBuffer(pluginName, taskName, type, value));
        }

        /// <summary>
        /// Подсчёт процента по объекту информации о прогрессе
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private int Percent(Information info)
        {
            // 0 из 0 - это 100%
            if (info.Stage == 0 && info.Stages == 0)
                return 100;

            //Проверка на разумность
            if (info.Stage > info.Stages || info.Stages == 0)
                throw new ArgumentOutOfRangeException();

            return (int)((info.Stage * 100) / info.Stages);
        }

        #region Работа с буфером обновления
        /// <summary>
        /// Добавление информации об указанном таске в буфер для последующего обновления.
        /// </summary>
        /// <param name="pluginName">Имя плагина</param>
        /// <param name="taskName">Имя таска</param>
        /// <param name="type">Тип обновления</param>
        /// <param name="value">Значение</param>
        private void AddToBuffer(string pluginName, string taskName, Monitor.Tasks.Task.UpdateType type, ulong value)
        {
            //Проверка на разумность
            if (main_listView.Groups[pluginName] == null)
                throw new Exception("Для плагина <" + pluginName + "> не задано ни одного таска");

            //Проверка на разумность
            if (!main_listView.Groups[pluginName].Items.ContainsKey(taskName))
                throw new Exception("Для плагина <" + pluginName + "> не задана задача с именем " + taskName);

            //Добавляем новое значение в буфер обновления
            if (buffer.ContainsKey(pluginName))
            {
                if (buffer[pluginName].ContainsKey(taskName))
                {
                    if (buffer[pluginName][taskName].ContainsKey(type))
                        buffer[pluginName][taskName][type] = value;
                    else
                        buffer[pluginName][taskName].Add(type, value);
                }
                else
                    buffer[pluginName].Add(taskName, new TaskUpdateHistory() { { type, value } });
            }
            else
                buffer.Add(pluginName, new PluginUpdateHistory() { { taskName, new TaskUpdateHistory() { { type, value } } } });
        }

        /// <summary>
        /// Обновление информации о всех тасках из буфера.
        /// </summary>
        private UpdateByTimer.HasSomethingToUpdate UpdateFromBuffer()
        {
            //Для каждого плагина из буфера обновления
            foreach (string pluginName in buffer.Keys)
                //Для каждого таска из буфера обновления
                foreach (string taskName in buffer[pluginName].Keys)
                {
                    //Получаем текущую информацию о прогрессе (таск)
                    Information taskProgressInfo = main_listView.Groups[pluginName].Items[taskName].Tag as Information;

                    //Получаем текущую информацию о прогрессе (плагин)
                    Information pluginProgressInfo = main_listView.Groups[pluginName].Tag as Information;

                    //Получаем текущую информацию о прогрессе (все плагины)
                    Information commonProgressInfo = main_listView.Tag as Information;

                    //Проходим по всем обновлениям
                    foreach (KeyValuePair<Monitor.Tasks.Task.UpdateType, ulong> update in buffer[pluginName][taskName])
                    {
                        //Вносим необходимые изменнения
                        switch (update.Key)
                        {
                            case Monitor.Tasks.Task.UpdateType.STAGE:
                                {
                                    //Обновляем информацию о прогрессе для группы и всего листвью
                                    pluginProgressInfo.Stage += update.Value - taskProgressInfo.Stage;
                                    commonProgressInfo.Stage += update.Value - taskProgressInfo.Stage;

                                    //Обновляем информацию о прогрессе для таска
                                    taskProgressInfo.Stage = update.Value;

                                    break;
                                }
                            case Monitor.Tasks.Task.UpdateType.STAGES:
                                {
                                    //Обновляем информацию о прогрессе для группы и всего листвью
                                    pluginProgressInfo.Stages += update.Value - taskProgressInfo.Stages;
                                    commonProgressInfo.Stages += update.Value - taskProgressInfo.Stages;

                                    //Обновляем информацию о прогрессе для таска
                                    taskProgressInfo.Stages = update.Value;

                                    break;
                                }
                            default:
                                throw new Exception("Обнаружен неизвестный тип обновления тасков.");
                        }
                    }

                    //Обновляем отображаемое значение
                    main_listView.Groups[pluginName].Items[taskName].SubItems["Tasks"].Text = taskProgressInfo.Stage + "/" + taskProgressInfo.Stages;

                    //Обновляем значение прогресса плагина и общего прогресса
                    PluginProgressInformation = pluginProgressInfo;
                    CommonProgressInformation = commonProgressInfo;
                }

            //Очищаем буфер после обновления
            if (buffer.Count != 0)
                buffer.Clear();

            //После каждого обновления буфер очищается, а значит дополнительного обновления не требуется
            return UpdateByTimer.HasSomethingToUpdate.NO;
        }
        #endregion
    }
}
