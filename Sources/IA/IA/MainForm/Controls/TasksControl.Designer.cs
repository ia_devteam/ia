﻿using System.Windows.Forms;

namespace IA.MainForm.Controls
{
    partial class TasksControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_listView = new System.Windows.Forms.ListView();
            this.main_groupBox = new System.Windows.Forms.GroupBox();
            this.main_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.main_listView.Location = new System.Drawing.Point(40, 19);
            this.main_listView.Name = "listView1";
            this.main_listView.Size = new System.Drawing.Size(121, 97);
            this.main_listView.TabIndex = 0;
            this.main_listView.UseCompatibleStateImageBehavior = false;
            this.main_listView.Dock = DockStyle.Fill;
            this.main_listView.View = View.Details;
            this.main_listView.HeaderStyle = ColumnHeaderStyle.None;
            this.main_listView.MultiSelect = false;
            this.main_listView.ShowGroups = true;
            this.main_listView.FullRowSelect = true;
            this.main_listView.Columns.Add(new ColumnHeader() { Name = "Name" });
            this.main_listView.Columns.Add(new ColumnHeader() { Name = "Tasks", TextAlign = HorizontalAlignment.Right });

            // 
            // main_groupBox
            // 
            this.main_groupBox.Controls.Add(this.main_listView);
            this.main_groupBox.Location = new System.Drawing.Point(41, 26);
            this.main_groupBox.Name = "main_groupBox";
            this.main_groupBox.Size = new System.Drawing.Size(200, 100);
            this.main_groupBox.TabIndex = 1;
            this.main_groupBox.TabStop = false;
            this.main_groupBox.Dock = DockStyle.Fill;
            this.main_groupBox.Text = "Задачи";
            // 
            // TasksControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.main_groupBox);
            this.Name = "TasksControl";
            this.Size = new System.Drawing.Size(486, 181);
            this.main_groupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView main_listView;
        private System.Windows.Forms.GroupBox main_groupBox;
    }
}
