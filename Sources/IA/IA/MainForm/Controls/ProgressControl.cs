﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using Windows7.DesktopIntegration.WindowsForms;
using Windows7.DesktopIntegration;

namespace IA.MainForm.Controls
{
    /// <summary>
    /// Класс, реализующий окно прогресса
    /// </summary>
    internal partial class ProgressControl : UserControl
    {
        public class Information
        {
            /// <summary>
            /// Значение текущей стадии по умолчанию
            /// </summary>
            public const ulong StageDefaultValue = 0;

            /// <summary>
            /// Значение общего числа стадий по умолчанию
            /// </summary>
            public const ulong StagesDefaultValue = 100;

            /// <summary>
            /// Текущая стадия
            /// </summary>
            public ulong Stage { get; set; }

            /// <summary>
            /// Общее число стадий
            /// </summary>
            public ulong Stages { get; set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="stage">Текущая стадия</param>
            /// <param name="stages">Общее число стадий</param>
            public Information(ulong stage = StageDefaultValue, ulong stages = StagesDefaultValue)
            {
                this.Stage = stage;
                this.Stages = stages;
            }
        }
        /// <summary>
        /// Всевозможные типы прогресса
        /// </summary>
        internal enum ProgressType
        {
            /// <summary>
            /// Прогресс в рамках стадии\\плагина
            /// </summary>
            [Description("Cтадия\\Плагин")]
            PLUGIN,
            /// <summary>
            /// Прогресс в рамках списка плагинов
            /// </summary>
            [Description("Общий прогресс")]
            PLUGINLIST,
            /// <summary>
            /// Прогресс в рамках рабочего процесса
            /// </summary>
            [Description("Общий прогресс")]
            WORKFLOW
        }

        /// <summary>
        /// Класс, описывающий отдельный элемент прогресса
        /// </summary>
        internal class ProgressElement
        {
            /// <summary>
            /// Элемент управления - Наименование прогресса
            /// </summary>
            private Label progressName_Label = null;

            /// <summary>
            /// Элемент управления - Прогрессбар для отображения прогресса
            /// </summary>
            private ProgressBar progressBar_progressBar = null;

            /// <summary>
            /// Элемент управления - Процентаж прогресса
            /// </summary>
            private Label progressValue_Label = null;

            /// <summary>
            /// Информация о прогрессе
            /// </summary>
            private Information progressInfo = new Information();

            /// <summary>
            /// Элемент управления прогресса
            /// </summary>
            internal TableLayoutPanel Control { get; private set; }

            /// <summary>
            /// Тип элемента прогресса
            /// </summary>
            internal ProgressType Type { get; private set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            internal ProgressElement(ProgressType type)
            {
                this.Type = type;

                //Инициализация элемента управления - Наименование прогресса
                progressName_Label = new Label()
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Right,
                    TextAlign = System.Drawing.ContentAlignment.MiddleRight,
                    Text = type.Description() + ":"
                };

                //Инициализация элемента управления - Прогрессбар для отображения прогресса
                progressBar_progressBar = new ProgressBar()
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Right,
                    Minimum = 0,
                    Maximum = 100,
                    Value = 0
                };

                //Инициализация элемента управления - Процентаж прогресса 
                progressValue_Label = new Label()
                {
                    Anchor = AnchorStyles.Left | AnchorStyles.Right,
                    TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                    Text = "0%"
                };

                //Инициализация таблицы элементов управления
                TableLayoutPanel tableLayoutPanel = new TableLayoutPanel()
                {
                    Dock = DockStyle.Fill,
                    RowCount = 2,
                    ColumnCount = 3
                };

                //Инициализация элемента управления прогресса
                this.Control = new TableLayoutPanel()
                {
                    Dock = DockStyle.Fill,
                    RowCount = 1,
                    ColumnCount = 3
                };

                this.Control.RowStyles.Add(new ColumnStyle(SizeType.Percent, 100));

                this.Control.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 110));
                this.Control.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
                this.Control.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50));

                this.Control.Controls.Add(progressName_Label, 0, 0);
                this.Control.Controls.Add(progressBar_progressBar, 1, 0);
                this.Control.Controls.Add(progressValue_Label, 2, 0);
            }

            /// <summary>
            /// Метод для увеличения числа выполненных стадий на заданное число
            /// </summary>
            /// <param name="incValue">Число на, которое требутся увеличить количество выполненных стадий</param>
            internal void IncProgress(ulong incValue = 1)
            {
                this.progressInfo.Stage += incValue;

                this.UpdateControl();
            }

            /// <summary>
            /// Метод для уменьшения числа выполненных стадий на заданное число
            /// </summary>
            /// <param name="incValue">Число на, которое требутся уменьшить количество выполненных стадий</param>
            internal void DecProgress(ulong incValue = 1)
            {
                this.progressInfo.Stage -= incValue;

                this.UpdateControl();
            }

            /// <summary>
            /// Метод для задания числа выполненных стадий
            /// </summary>
            /// <param name="stage">Число выполненных стадий</param>
            internal void SetProgressStage(ulong stage)
            {
                this.progressInfo.Stage = stage;

                this.UpdateControl();
            }

            /// <summary>
            /// Метод для задания общего числа стадий
            /// </summary>
            /// <param name="stages">Общее число стадий</param>
            internal void SetProgressStages(ulong stages)
            {
                this.progressInfo.Stages = stages;

                this.UpdateControl();
            }

            /// <summary>
            /// Метод для задания информиции о прогрессе
            /// </summary>
            /// <param name="stage">Выполненное число стадий</param>
            /// <param name="stages">Общее число стадий</param>
            internal void SetProgress(ulong stage, ulong stages)
            {
                this.progressInfo.Stage = stage;
                this.progressInfo.Stages = stages;

                this.UpdateControl();
            }

            /// <summary>
            /// Метод для задания информиции о прогрессе
            /// </summary>
            /// <param name="information">Информация о прогрессе</param>
            internal void SetProgress(Information information)
            {
                this.progressInfo.Stage = information.Stage;
                this.progressInfo.Stages = information.Stages;

                this.UpdateControl();
            }

            /// <summary>
            /// Метод для задания максимально возможного прогресса
            /// </summary>
            internal void SetMaximumProgress()
            {
                this.progressInfo.Stage = this.progressInfo.Stages;

                this.UpdateControl();
            }

            /// <summary>
            /// Метод для сброса прогресса
            /// </summary>
            internal void Reset()
            {
                this.progressInfo.Stage = Information.StageDefaultValue;
                this.progressInfo.Stages = Information.StagesDefaultValue;
                MainForm.numbersOfPluginsToRun = 0;

                this.UpdateControl();
            }

            static int i;
            /// <summary>
            /// Метод для обновления информации о прогрессе в рамках элемента управления
            /// </summary>
            private void UpdateControl()
            {
                //2016.02.05 добавлено после неоднократных случайных вылетов из разных плагинов при неправильном задании значений
                var value = Percent(this.progressInfo);
                if (value < this.progressBar_progressBar.Minimum ||
                    value > this.progressBar_progressBar.Maximum)
                {
                    Monitor.Log.Error("Элемент прогресса", String.Format("Попытка установить значение прогресса равным <{0}> вне заданных пределов ({1},{2})",
                        value,
                        this.progressBar_progressBar.Minimum,
                        this.progressBar_progressBar.Maximum));
                    return;
                }

                this.progressValue_Label.Text = (this.progressBar_progressBar.Value = Percent(this.progressInfo)) + "%";

                // обновляем таскбар
                if (MainForm.numbersOfPluginsToRun != 0)
                {
                    if (MainForm.numbersOfPluginsToRun == 0)
                    {
                        WindowsFormsExtensions.SetTaskbarProgressState(MainForm.thisForm, Windows7Taskbar.ThumbnailProgressState.NoProgress);
                        WindowsFormsExtensions.SetTaskbarProgress(MainForm.thisForm, 0);
                    }
                    else
                    {
                        if (Type == ProgressType.PLUGIN && Percent(this.progressInfo) != 100)
                        {
                            WindowsFormsExtensions.SetTaskbarProgressState(MainForm.thisForm, Windows7Taskbar.ThumbnailProgressState.Normal);
                            WindowsFormsExtensions.SetTaskbarProgress(MainForm.thisForm, MainForm.currentProgress + Percent(this.progressInfo) / MainForm.numbersOfPluginsToRun);
                        }
                        else if (Type == ProgressType.PLUGINLIST)
                        {
                            WindowsFormsExtensions.SetTaskbarProgressState(MainForm.thisForm, Windows7Taskbar.ThumbnailProgressState.Normal);
                            MainForm.currentProgress = (MainForm.numbersOfPluginsToRun - ((MainForm)(MainForm.thisForm)).pluginsQueue_Count) * 100 / MainForm.numbersOfPluginsToRun;
                            WindowsFormsExtensions.SetTaskbarProgress(MainForm.thisForm, MainForm.currentProgress);
                        }
                    }
                }
                else
                {
                    WindowsFormsExtensions.SetTaskbarProgressState(MainForm.thisForm, Windows7Taskbar.ThumbnailProgressState.NoProgress);
                }
            }

            /// <summary>
            /// Подсчёт процента по информации о прогрессе
            /// </summary>
            /// <param name="information">Информация о прогрессе</param>
            /// <returns></returns>
            private int Percent(Information information)
            {
                // 0 из 0 - это 100%
                if (information.Stage == 0 && information.Stages == 0)
                    return 100;

                //Проверка на разумность
                if (information.Stages == 0)
                    throw new ArgumentOutOfRangeException();

                return (int)((information.Stage * 100) / information.Stages);
            }
        }

        /// <summary>
        /// Список элементов прогресса
        /// </summary>
        internal Dictionary<ProgressType, ProgressElement> Elements = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        internal ProgressControl()
        {
            InitializeComponent();

            //Инициализация списка составных элементов управления окна прогресса
            this.Elements = new Dictionary<ProgressType, ProgressElement>();

            //Добавляем все возможные типы элементов прогресса в список
            EnumLoop<ProgressType>.ForEach(type => Elements.Add(type, new ProgressElement(type)));

            //Создаём таблицу элементов прогресса и добавляем её на основной элемент управления
            this.Controls.Add(GenerateElementsTable(Enum.GetValues(typeof(ProgressType)).Cast<ProgressType>().ToArray()));
        }

        /// <summary>
        /// Сформировать элемент управления - таблицу элементов прогресса
        /// </summary>
        /// <param name="types">Типы элементов прогресса</param>
        /// <returns></returns>
        private TableLayoutPanel GenerateElementsTable(ProgressType[] types)
        {
            //Инициализация таблицы элементов управления
            TableLayoutPanel ret = new TableLayoutPanel()
            {
                Dock = DockStyle.Fill,
                RowCount = types.Length,
                ColumnCount = 1
            };

            int counter = 0;
            foreach (ProgressType type in types)
            {
                ret.RowStyles.Add(new RowStyle(SizeType.Percent, 100 / types.Length));
                ret.Controls.Add(this.Elements[type].Control, counter++, 0);
            }

            return ret;
        }

        /// <summary>
        /// Отобразить только выбранные элементы прогресса
        /// </summary>
        /// <param name="types">Типы элементов прогресса</param>
        internal void ShowSpecificElementsOnly(ProgressType[] types)
        {
            //Создаём таблицу выбранных элементов прогресса и добавляем её на основной элемент управления
            this.Controls.Clear();
            this.Controls.Add(this.GenerateElementsTable(types));
        }

        /// <summary>
        /// Сбросить окно прогресса
        /// </summary>
        public void Reset()
        {
            this.Elements.Values.ForEach(e => e.Reset());
        }
    }
}
