﻿using IA.Objects;
using IA.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IAUtils;
using System.IO;

namespace IA.MainForm
{
    public partial class StoragePackProcessing : Form
    {
        public StoragePackProcessing()
        {
            InitializeComponent();

            foreach (Project proj in Project.GetAll())
            {
                foreach (Materials materials in Materials.GetByProject(proj.ID))
                {
                    List<Storage> storages = Storage.GetByMaterials(materials.ID);
                    DataGridViewRow row = new DataGridViewRow();
                    row.Tag = new StorageForPack
                    {
                        storage = storages[storages.Count - 1],
                        TracesPath = ""
                    };

                    row.CreateCells(MaterialsList);

                    string projName = (proj.Name == "Синтез_ОС_7" || proj.Name == "Синтез_ОС_7_Part_2") ? materials.Comment : proj.Name;
                    projName = projName.Replace("<", "").Replace(">", "").Replace("?", "").Replace("*", "");
                    projName = Path.Combine(Properties.Settings.Default.ReportsPath, projName);

                    row.Cells[0].Value = false;  //(proj.Name == "Синтез_ОС_7" || proj.Name == "Синтез_ОС_7_Part_2") && materials.Comment != "<Комментарий отсутствует>";
                    row.Cells[2].Value = Directory.Exists(projName);
                    row.Cells[1].Value = proj.Name;
                    row.Cells[3].Value = "Материалы ver. " + materials.Version.ToString();
                    row.Cells[4].Value = materials.Comment;

                    MaterialsList.Rows.Add(row);
                }
            }            

            foreach (Plugin.Handler plugin in Plugin.HandlersInitial.GetAll())
            {
                PluginsListBox.Items.Add(plugin, /*plugin.Name == "Обработка результатов динамического анализа" || */plugin.Name == "Избыточность по функциям" || plugin.Name == "Генератор статических маршрутов");
            }

            ReportsPathTextBox.Text = Properties.Settings.Default.ReportsPath;
        }

        private void CowabungaButton_Click(object sender, EventArgs e)
        {
            Queue<StorageForPack> storages = new Queue<StorageForPack>();
            foreach(DataGridViewRow row in MaterialsList.Rows)
            {
                if ((bool)((DataGridViewCheckBoxCell)row.Cells[0]).Value == true)
                {
                    storages.Enqueue((StorageForPack)row.Tag);
                }
            }
            
            Handlers plugins = new Handlers(PluginsListBox.CheckedItems.Cast<Handler>());

            plugins.Sort();

            IAUtils.IACommon.StoragePackStart(storages, plugins, ReportsPathTextBox.Text);

            Properties.Settings.Default.ReportsPath = ReportsPathTextBox.Text;

            this.Close();
        }

        string findFolder(string subFolder)
        {
            IEnumerable<string> oldParts = Directory.EnumerateDirectories(Path.Combine(textBox2.Text, "_old-parts"));
            string dir = "";
            foreach (string oldPart in oldParts)
            {
                IEnumerable<string> old = Directory.EnumerateDirectories(oldPart, "*" + subFolder + "*");
                if (old.Count() != 0)
                    dir = old.First();
            }
            if (dir == "")
            {
                IEnumerable<string> Parts = Directory.EnumerateDirectories(textBox2.Text, "*_Part_*");
                foreach (string Part in Parts)
                {
                    IEnumerable<string> found = Directory.EnumerateDirectories(Part, "*" + subFolder + "*");
                    if (found.Count() != 0)
                    {
                        dir = found.First();
                        break;
                    }
                }
            }
            return dir;
        }

        private void CheckTracesPathsButton_Click(object sender, EventArgs e)
        {
            TracesPaths.Rows.Clear();
            Handlers plugins = new Handlers(PluginsListBox.CheckedItems.Cast<Handler>());

            Queue<StorageForPack> storages = new Queue<StorageForPack>();
            foreach (DataGridViewRow row in MaterialsList.Rows)
            {
                if ((bool)((DataGridViewCheckBoxCell)row.Cells[0]).Value == true)
                {
                    storages.Enqueue((StorageForPack)row.Tag);
                }
            }

            if (plugins.Contains(Plugin.HandlersInitial.GetOneByID(Store.Const.PluginIdentifiers.DYNAMIC_ANALYSIS_RESULTS_PROCESSING)))
            {
                foreach (StorageForPack storage in storages)
                {
                    Materials materials = Materials.GetByID(storage.storage.MaterialsID);
                    Project proj = Project.GetByID(materials.ProjectID);
                    string toSearch = (proj.Name == "Синтез_ОС_7" || proj.Name == "Синтез_ОС_7_Part_2") ? materials.Comment : proj.Name;

                    DataGridViewRow row = new DataGridViewRow()
                    {
                        Tag = storage
                    };

                    row.CreateCells(TracesPaths);
                    row.Cells[0].Value = proj.Name + ": " + materials.Comment;
                    if (storage.TracesPath == "")
                    {
                        row.Cells[1].Value = findFolder(toSearch);
                        storage.TracesPath = (string)row.Cells[1].Value;
                    }
                    else
                    {
                        row.Cells[1].Value = storage.TracesPath;
                    }
                    TracesPaths.Rows.Add(row);
                }
            }
        }

        private void TracesPaths_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && e.ColumnIndex != -1)
                ((StorageForPack)TracesPaths.Rows[e.RowIndex].Tag).TracesPath = (string)TracesPaths.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            int start = 0;
            if (MaterialsList.SelectedCells.Count != 0)
                start = MaterialsList.SelectedCells[0].RowIndex;
            for (int i = start + 1; i < MaterialsList.Rows.Count; ++i)
            {
                if (((string)MaterialsList[1, i].Value).Contains(SearchTextBox.Text) || ((string)MaterialsList[3, i].Value).Contains(SearchTextBox.Text))
                {
                    MaterialsList.CurrentCell = MaterialsList[1, i];
                    break;
                }
                if (i == MaterialsList.Rows.Count - 1 && start != 0)
                {
                    i = 0;
                    start = 0;
                }
            }
        }

        private void BrowseStoragesListButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader sr = new StreamReader(ofd.FileName))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        foreach (DataGridViewRow row in MaterialsList.Rows)
                        {
                            if (((string)row.Cells[1].Value).Contains(line) || ((string)row.Cells[3].Value).Contains(line))
                            {
                                ((DataGridViewCheckBoxCell)row.Cells[0]).Value = true;                                
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
