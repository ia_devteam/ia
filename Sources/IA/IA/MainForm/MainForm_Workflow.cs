﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using IA.Objects;
using IA.Extensions;

namespace IA.MainForm
{
    using Buttons = Dictionary<string, EventHandler>;

    using Plugin = IA.Plugin.Handler;
    using Plugins = IA.Plugin.Handlers;
    using WorkflowElement = IA.Config.Objects.Workflows.ISpecificElement;
    using WorkflowRunner = IA.Config.Objects.Workflows.Run;
    using WorkflowStopper = IA.Config.Objects.Workflows.Stop;

    /// <summary>
    /// Часть класса основной формы, отвечающая за работу с рабочим процессом
    /// </summary>
    public partial class MainForm
    {
        #region Поля
        /// <summary>
        /// Флаг. Требуется ли сгенерировать отчёты перед обработкой очередного элемента рабочего процесса.
        /// </summary>
        private bool isNeedToGenerateReportBefore = true;
        #endregion

        #region Буфер плагинов
        /// <summary>
        /// Буфер плагинов в рамках рабочего процесса. Не может быть Null.
        /// </summary>
        private Plugins workflowPluginsBuffer = new Plugins();
        
        /// <summary>
        /// Обработка буфера
        /// </summary>
        private void Workflow_Buffer_Process()
        {
            try
            {
                //Игнорируем все одновременно завершённые (а значит выполненные) плагины в буфере
                new Plugins(this.workflowPluginsBuffer).Where(p => p.IsCompleted).ForEach(p => this.workflowPluginsBuffer.Remove(p));

                //Если буфер пуст
                if (this.workflowPluginsBuffer.Count == 0)
                    throw new Exception("Недопустимая ситуация. Невозможно обработать буфер плагинов. Буфер пуст.");

                #region Если в буфере только один выполненный но незавершённый плагин

                //Если в очереди только один незавершённый плагин, то нужно начинать сразу с окна результатов
                if (this.workflowPluginsBuffer.Count == 1)
                {
                    //Получаем первый и единственный плогин в очереди
                    Plugin firstPlugin = this.workflowPluginsBuffer.First();

                    //Если плагин незавершён
                    if(firstPlugin.RunResult == Plugin.ExecutionResult.SUCCESS && !firstPlugin.IsCompleted)
                    {
                        //Отображаем окно результатов
                        bool isResultWindowExists;

                        //Отображаем окно с результатами единственнного плагина
                        if (!this.Plugin_ResultWindow_Show(firstPlugin, out isResultWindowExists, this.Workflow_Buffer_Handler_Plugins_Run_Completed, true))
                            throw new Exception("Отображение окна результатов плагина <" + firstPlugin.Name + ">  завершилось с ошибкой.");

                        //Окно с результатами обязано быть
                        if (!isResultWindowExists)
                            throw new Exception("Недопустимая ситуация. Окно с результатами для плагина <" + firstPlugin.Name + "> должно существовать.");

                        return;
                    }
                } 
                #endregion

                //Отображаем настройки всех плагинов в рамках буфера
                bool isSettingsWindowExists = false;

                Buttons settingsWindowButtons = new Buttons()
                {
                    {"Далее", (sender, e) => this.Workflow_Buffer_Handler_SettingsWindow_Closed() }
                };

                //Отображаем настройки всех плагинов в рамках буфера
                if (!this.Plugin_SettingsWindow_Show(this.workflowPluginsBuffer, out isSettingsWindowExists, settingsWindowButtons))
                    throw new Exception("Отображение окна настроек плагинов завершилось с ошибкой.");

                //Если ни у одного плагина в буфере нет окна настроек, запускаем их выполнение
                if (!isSettingsWindowExists)
                {
                    //Запускаем выполнение плагинов в рамках буфера
                    this.Plugin_Run_Launch(this.workflowPluginsBuffer, this.Workflow_Buffer_Handler_Plugins_Run_Completed);
                }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка в ходе обработки буфера плагинов в рамках рабочего процесса.", ex);
            }
        }

        /// <summary>
        /// Обработчик - Окно настроек плагинов в рамках буфера закрылось
        /// </summary>
        private void Workflow_Buffer_Handler_SettingsWindow_Closed()
        {
            try
            {
                //Проверяем настройки плагинов в рамках буфера
                if (!this.Plugin_Settings_Check(this.workflowPluginsBuffer))
                    return;

                //Сохраняем настройки плагинов в рамках буфера
                this.Plugin_Settings_Save(this.workflowPluginsBuffer);

                //Запускаем выполнение плагинов в рамках буфера
                this.Plugin_Run_Launch(this.workflowPluginsBuffer, this.Workflow_Buffer_Handler_Plugins_Run_Completed);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе выполнения рабочего процесса.",
                    (message) => this.Workflow_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message))
                );
            }
        }

        /// <summary>
        /// Обработчик - Выполнение плагинов в рамках буфера завершилось
        /// </summary>
        /// 
        /// <param name="result">Результат выполнения плагинов в рамках буфера</param>
        private void Workflow_Buffer_Handler_Plugins_Run_Completed(ExecutionResultInfo result)
        {
            try
            {
                //Проверка на разумность
                if (result == null)
                    throw new Exception("Результат выполнения плагинов в рамках буфера не определён.");

                //Если выполнение плагинов в рамках буфера завершилось некорректно
                if (result.Type == ExecutionResultInfo.ResultType.ERROR)
                {
                    //Завершаем выполнение рабочего процесса
                    this.Workflow_Run_Finish(result);
                    return;
                }

                

                //Если буфер пуст
                if (this.workflowPluginsBuffer.Count == 0)
                    throw new Exception("Недопустимая ситуация. Попытка обработки пустого буфера плагинов в рамках рабочего процесса.");

                //Сохраняем последний плагин в буфере
                Plugin last_plugin = this.workflowPluginsBuffer.Last();

                //Увеличиваем прогресс
                this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.WORKFLOW].IncProgress((ulong)workflowPluginsBuffer.Count);

                //Очещаем буфер плагинов
                workflowPluginsBuffer.Clear();

                 #region Если последний плагин в буфере многоразовый
                //Если последний плагин в буфера многоразовый
                if (last_plugin.IsReusable)
                {
                    //Отображаем сообщение о том, что нужно указать директорию для формирования отчётов по многоразовому плагину
                    Monitor.Log.Information(MainForm.ObjectName, "Укажите директорию для формирования отчётов по стадии <" + last_plugin.Name + ">.", true);

                    //Помещаем многоразовый плагин в буфер
                    this.workflowPluginsBuffer.Add(last_plugin);

                    //Генерируем отчёты по многоразовому плагину
                    this.Plugin_Report_Generate_Launch(this.workflowPluginsBuffer, this.Workflow_Buffer_Handler_Plugins_Report_Generate_Completed, true);

                    return;
                }
                #endregion

                //Переходем к выполнению следующего элемента рабочего процесса
                Workflow_Run_Next();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе выполнения рабочего процесса.",
                    (message) => this.Workflow_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message))
                );
            }
        }

        /// <summary>
        /// Обработчик - Генерация отчётов по плагинам в рамках буфера завершилась
        /// </summary>
        /// <param name="result">Результат генерации отчётов по плагином в рамках буфера</param>
        private void Workflow_Buffer_Handler_Plugins_Report_Generate_Completed(ExecutionResultInfo result)
        {
            try
            {
                //Проверка на разумность
                if (result == null)
                    throw new Exception("Результат генерации отчётов по плагинам в рамках буфера не определён.");

                //Если генерация отчётов по плагинам в рамках буфера завершилась некорректно
                if (result.Type == ExecutionResultInfo.ResultType.ERROR)
                {
                    //Завершаем выполнение рабочего процесса
                    this.Workflow_Run_Finish(result);
                    return;
                }

                #region Если единственный плагин в буфере многоразовый
                //Если в буфере всего один плагин
                if (this.workflowPluginsBuffer.Count == 1)
                {
                    Plugin firstPlugin = this.workflowPluginsBuffer.First();

                    //Если плагин многоразовый
                    if(firstPlugin.IsReusable)
                    {
                        //Задаём вопрос пользователю, завершена ли работа над плагином?
                        switch (MessageBox.Show("Работа на данной стадии завершена?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            case DialogResult.Yes:
                                {
                                    break;
                                }
                            case DialogResult.No:
                                {
                                    //Откатываем общий прогресс (основной режим)
                                    this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.WORKFLOW].DecProgress();

                                    //Помечаем многоразовый плагин, как незавершённый и невыполненный
                                    firstPlugin.RunResult = Plugin.ExecutionResult.NO;
                                    firstPlugin.IsCompleted = false;

                                    //Запускаем обработку плагина заново
                                    this.Workflow_Buffer_Process();

                                    return;
                                }
                        }
                    }
                }
                #endregion

                //Очищаем буфер плагинов
                this.workflowPluginsBuffer.Clear();

                //Переходим к обработке следующего элемента рабочего процесса
                this.Workflow_Run_Next();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе выполнения рабочего процесса.",
                    (message) => this.Workflow_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message))
                );
            }
        }
        #endregion

        /// <summary>
        /// Метод для получения плагина из элемента (раннера) рабочего процесса
        /// </summary>
        /// <param name="workflowRunner">Раннер</param>
        /// <returns>Плагин. Не может быть Null.</returns>
        private Plugin Workflow_Run_Element_GetPlugin(WorkflowRunner workflowRunner)
        {
            try
            {
                //Проверка на разумность
                if(workflowRunner == null)
                    throw new Exception("Элемент рабочего процесса не определён.");

                //Проверка на то, что запускается именно плагин, ничего другого запускать мы не умеем
                if (workflowRunner.EntityType != WorkflowRunner.enEntityType.PLUGIN)
                    throw new Exception("Недопустимый элемент рабочего процесса. Требуется запуск сущности отличной от плагина.");

                //Получаем плагин по идентификатору
                Plugin plugin = IA.Plugin.HandlersInitial.GetOneByID(workflowRunner.EntityID);

                return plugin;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при получении плагина из элемента (раннера) рабочего процесса.", ex);
            }
        }

        /// <summary>
        /// Метод для подготовки рабочего процесса перез запуском
        /// </summary>
        /// <param name="materials">Набор материалов</param>
        private void Workflow_Run_Prepare(Materials materials)
        {
            try
            {
                //Проверка на разумность
                if(this.CurrentWorkflow == null)
                    throw new Exception("Рабочий процесс не определён.");

                //Получаем список Хранилищ, созданных в основном режиме работы
                List<Storage> storages = Storage.GetByMaterials(materials.ID).Where(s => s.Mode == Storage.enMode.BASIC).ToList();

                //Если нет ни одного Хранилища, созданного в основном режиме работы
                if (storages.Count == 0)
                    throw new Exception("Недопустимая ситуация. Нет ни одного Хранилища, созданного в режиме работы <" + Mode.BASIC.Description() + ">, в рамках набора материалов.");

                //Проходим во всем Хранилищам, созданным в основном режиме работы, и оставлям только листовые вершины в дереве Хранилищ
                //Листовые вершины содержат максимальное число выполненных плагинов
                storages = storages.Where(parent => storages.All(s => s.ParentID != parent.ID)).ToList();

                //Если нет ни одного листового Хранилища, созданного в основном режиме работы
                if (storages.Count == 0)
                    throw new Exception("Недопустимая ситуация. Среди Хранилищ, созданных в режиме работы <" + Mode.BASIC.Description() + ">, нет ни одного листового.");

                //Индекс рассматриваемого Хранилища
                int storageIndex = 0;

                //Cписок неучтённых выполненных плагинов в рассматриваемом Хранилище
                List<uint> unconsideredRunnedPlugins = new List<uint>(storages[storageIndex].Runned_Plugins);

                //Cписок неучтённых завершённых плагинов в рассматриваемом Хранилище
                List<uint> unconsideredCompletedPlugins = new List<uint>(storages[storageIndex].Completed_Plugins);

                //Проходим последовательно по элементам рабочего процесса
                foreach (WorkflowElement workflowElement in new List<WorkflowElement>(this.CurrentWorkflow.Elements))
                {
                    //Если элемент является раннером
                    WorkflowRunner workflowRunner;
                    if ((workflowRunner = workflowElement as WorkflowRunner) != null)
                    {
                        //Получаем плагин из раннера
                        Plugin plugin = this.Workflow_Run_Element_GetPlugin(workflowRunner);

                        //Получаем информацию о том, выполнен ли плагин?
                        bool isRunned = unconsideredRunnedPlugins.Contains(plugin.ID);

                        //Получаем информацию о том, завершён ли плагин?
                        bool isCompleted = unconsideredCompletedPlugins.Contains(plugin.ID);

                        //Если плагин выполнен
                        if (isRunned)
                            //Удаляем выполненный плагин из списка нерассмотренных
                            unconsideredRunnedPlugins.Remove(plugin.ID);

                        //Если плагин завершён
                        if (isCompleted)
                            //Удаляем завершённый плагин из списка нерассмотренных
                            unconsideredCompletedPlugins.Remove(plugin.ID);

                        //Если плагин уже выполнен и завершён
                        if (isRunned && isCompleted)
                        {
                            //Удаляем выполненный раннер из рабочего процесса
                            this.CurrentWorkflow.Elements.Dequeue();

                            //Увеличиваем прогресс
                            this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.WORKFLOW].IncProgress();
                        }
                        //В противном случае
                        else
                        {
                            //Если есть ещё нерассмотренные выполненные плагины
                            if (unconsideredRunnedPlugins.Count != 0 || unconsideredCompletedPlugins.Count != 0)
                                throw new Exception("Недопустимая ситуация. Обнаружены плагины выполненные и завершённые в последовательности не соответствующей рабочему процессу.");

                            //Это не последнее листовое Хранилище
                            if (storageIndex != storages.Count - 1)
                                throw new Exception("Недопустимая ситуация. Обнаружены Хранилища сгенерированные в последовательности не соответствующей рабочему процессу.");

                            //Завершаем подготовку
                            break;
                        }

                        continue;
                    }

                    //Если элемент является стоппером
                    WorkflowStopper workflowStopper;
                    if ((workflowStopper = workflowElement as WorkflowStopper) != null)
                    {
                        //Если дошли до формирования нового Хранилища
                        if (workflowStopper.Need == WorkflowStopper.enNeed.STORAGE_NEW_SRC_CLEAR)
                        {
                            //Если есть ещё нерассмотренные выполненные или завершённые плагины
                            if (unconsideredRunnedPlugins.Count != 0 || unconsideredCompletedPlugins.Count != 0)
                                throw new Exception("Недопустимая ситуация. Обнаружены плагины выполненные в последовательности не соответствующей рабочему процессу.");

                            //Если ранее рабочий процесс дошёл до формирования нового Хранилища, то оно обязательно должно быть сформировано.
                            if (storageIndex == storages.Count - 1)
                                throw new Exception("Недопустимая ситуация. Обнаружены Хранилища сгенерированные в последовательности не соответствующей рабочему процессу.");

                            //Переходим к рассмотрению следующего Хранилища
                            storageIndex++;

                            //Формируем список неучтённых выполненных плагинов для следующего Хранилища
                            unconsideredRunnedPlugins = new List<uint>(storages[storageIndex].Runned_Plugins);

                            //Формируем список неучтённых завершённых плагинов для следующего Хранилища
                            unconsideredCompletedPlugins = new List<uint>(storages[storageIndex].Completed_Plugins);

                        }
                        //Если треубется проанализировать Хранилище на предмет избыточных функций
                        else if (workflowStopper.Need == WorkflowStopper.enNeed.STORAGE_FUNCTION_REDUNDANCY_ANALIZE)
                        {
                            //Если ранее рабочий процесс дошёл до проверки Хранилища, то проверка обязательно прошла
                        }
                        else if (workflowStopper.Need == WorkflowStopper.enNeed.GET_SRC_LAB)
                        {
                            //Если ранее рабочий процесс дошёл до предоставления лабораторных исходных текстов пользователю, то тексты были обязательно предоставлены
                        }
                        //Если дошли до конца рабочего процесса
                        else if (workflowStopper.Need == WorkflowStopper.enNeed.FINISH)
                        {
                            //Завершаем подготовку
                            break;
                        }
                        else
                            throw new Exception("Обнаружено необрабатываемое требование в рамках остановки рабочего процесса.");

                        //Удаляем выполненный стоппер из рабочего процесса
                        this.CurrentWorkflow.Elements.Dequeue();

                        continue;
                    }

                    //Формируем исключение
                    throw new Exception("Обнаружен необрабатываемый элемент рабочего процесса.");
                }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка в ходе подготовки рабочего процесса.", ex);
            }
        }

        /// <summary>
        /// Метод для обработки элемента рабочего процесса (раннера)
        /// </summary>
        /// <param name="workflowRunner">Раннер</param>
        /// <param name="isNeedUserIntervention">Требуется ли вмешательство пользователя на данном этапе?</param>
        private void Workflow_Run_Element_Processing(WorkflowRunner workflowRunner, out bool isNeedUserIntervention)
        {
            //Изначельно вмешательство пользователя не требуется
            isNeedUserIntervention = false;

            try
            {
                //Проверка на разумность
                if(workflowRunner == null)
                    throw new Exception("Элемент рабочего процесса не определён.");

                //Проверка на разумность
                if(this.workflowPluginsBuffer == null)
                    throw new Exception("Буфер плагинов в рамках рабочего процесса не определён.");

                //Получаем плагин из раннера
                Plugin plugin = this.Workflow_Run_Element_GetPlugin(workflowRunner);

                //Задаём настройки плагина в соотвествии со строкой настройки
                plugin.SetSimpleSettings(String.IsNullOrWhiteSpace(workflowRunner.EntitySettings) ? String.Empty : workflowRunner.EntitySettings);

                //Добавялем плагин в буфер плагинов
                this.workflowPluginsBuffer.Add(plugin);

                //Узнаём требуется ли вмешательство пользователя для выполнения плагина
                isNeedUserIntervention = plugin.IsCapableTo(IA.Plugin.Capabilities.RESULT_WINDOW) || plugin.IsReusable;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем исключение
                throw new Exception("Ошибка в ходе обработки элемента (раннера) рабочего процесса.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Метод для обработки элемента рабочего процесса (стоппера)
        /// </summary>
        /// <param name="workflowStopper">Стоппер</param>
        /// <param name="isNeedReports">Требуется ли генерация отчётов?</param>
        /// <param name="isFinish">Рабочий процесс завершён?</param>
        private void Workflow_Run_Element_Processing(WorkflowStopper workflowStopper, out bool isNeedReports, out bool isFinish)
        {
            //Изначально генерация отчётов не требуется
            isNeedReports = false;

            //Изначально рабочий процесс не считается завершённым
            isFinish = false;

            try
            {
                //Проверка на разумность
                if (workflowStopper == null)
                    throw new Exception("Элемент рабочего процесса не определён.");

                //Делаем то, что требуется
                switch (workflowStopper.Need)
                {
                    case WorkflowStopper.enNeed.STORAGE_NEW_SRC_CLEAR:
                        {
                            //Проверка на то, что в рамкаха текущего набора материалов есть хоть один неизбыточный файл
                            bool isSourcesClearExist = this.CurrentStorage.Controller.Storage.files.EnumerateFiles(Store.enFileKind.fileWithPrefix).Any(f => f.FileEssential == Store.enFileEssential.ESSENTIAL);

                            //Если все файлы в рамках текущего набора материалов избыточны
                            if (!isSourcesClearExist)
                                throw new Exception("В рамках текущего набора материалов все файлы избыточны. Дальнейший анализ невозможен.");

                            //На данном этапе необходимо принудительно сформировать отчёты
                            //Если мы пришли сюда первый раз - генерируем отчёты, второй раз - обрабатываем стоппер
                            if (this.isNeedToGenerateReportBefore)
                            {
                                //Опускаем флаг
                                this.isNeedToGenerateReportBefore = false;

                                //Отображаем сообщение о том, что нужно указать директорию для формирования отчётов по очистке
                                Monitor.Log.Information(MainForm.ObjectName, "Укажите директорию для генерации отчётов по очистке исходных текстов.", true);

                                //Генерируем отчёты по очистке
                                this.Plugin_Report_Generate_Launch(IA.Plugin.HandlersInitial.GetSomeByCondition(p => p.IsCompleted), this.Workflow_Buffer_Handler_Plugins_Report_Generate_Completed, true);

                                //Сообщаем о том, что генерация отчётов требуется
                                isNeedReports = true;

                                return;
                            }
                            //В противном случае
                            else
                                //Поднимаем флаг
                                this.isNeedToGenerateReportBefore = true;

                            //Принудительно получаем содержимое рабочей директории Хранилища (очищенные исходные тексты)
                            this.Storage_GetWorkSubDirectoryContents(this.CurrentStorage, Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR);

                            //Закрываем и загружаем на сервер текущее Хранилище в фоновом режиме
                            if (!this.Storage_Close(this.CurrentStorage, true, true))
                                throw new Exception("Не удалось закрыть и загрузить текущее Хранилище не сервер.");

                            //Создаём новое Хранилище
                            Storage storage = Storage.Add(this.CurrentMaterials.ID, 0, this.CurrentUser.ID, Storage.enMode.BASIC, true, Storage.StorageAutoGeneratedComment);

                            //Открываем новое Хранилище в фоновом режиме
                            if (!this.Storage_Open(storage, true))
                                throw new Exception("Не удалось открыть Хранилище или выгрузить его с сервера.");

                            break;
                        }
                    case WorkflowStopper.enNeed.STORAGE_FUNCTION_REDUNDANCY_ANALIZE:
                        {
                            //Анализируем Хранилище на предмет избыточных функций
                            if (this.CurrentStorage.Controller.Storage.functions.EnumerateFunctions(Store.enFunctionKind.REAL).Any(f => f.Essential == Store.enFunctionEssentials.REDUNDANT))
                            {
                                //Отображаем предупреждение
                                Monitor.Log.Warning(MainForm.ObjectName, "В исходных текстах обнаружены избыточные функции.\nНеобходимо удалить избыточные функции из исходных текстов и повторить анализ заново, сформировав новый набор материалов.", true);

                                //Считаем рабочий процесс завершённым
                                isFinish = true;
                            }

                            break;
                        }
                    case WorkflowStopper.enNeed.GET_SRC_LAB:
                        {
                            //На данном этапе необходимо принудительно сформировать отчёты
                            //Если мы пришли сюда первый раз - генерируем отчёты, второй раз - обрабатываем стоппер
                            if (this.isNeedToGenerateReportBefore)
                            {
                                //Опускаем флаг
                                this.isNeedToGenerateReportBefore = false;

                                //Отображаем сообщение о том, что нужно указать директорию для формирования отчётов по формированию лабораторных исходных текстов
                                Monitor.Log.Information(MainForm.ObjectName, "Укажите директорию для генерации отчётов по формированию лабораторных исходных текстов.", true);

                                //Генерируем отчёты по формированию лабораторных исходных текстов
                                this.Plugin_Report_Generate_Launch(IA.Plugin.HandlersInitial.GetSomeByCondition(p => p.IsCompleted), this.Workflow_Buffer_Handler_Plugins_Report_Generate_Completed, true);

                                //Сообщаем о том, что генерация отчётов требуется
                                isNeedReports = true;

                                return;
                            }
                            //В противном случае
                            else
                                //Поднимаем флаг
                                this.isNeedToGenerateReportBefore = true;

                            //Принудительно получаем содержимое рабочей директории Хранилища (лабораторные исходные тексты)
                            this.Storage_GetWorkSubDirectoryContents(this.CurrentStorage, Store.WorkDirectory.enSubDirectories.SOURCES_LAB);

                            //Спрашиваем у пользователя, хочет ли он продолжить работу?
                            switch( MessageBox.Show(
                                        "Для продолжения работы требуется произвести сборку лабораторных исходных текстов и получить набор трасс для анализа. Хотите ли Вы продолжить анализ прямо сейчас?",
                                            "Вопрос",
                                                MessageBoxButtons.YesNo,
                                                    MessageBoxIcon.Question))
                            {
                                case DialogResult.Yes:

                                    break;
                                case DialogResult.No:
                                {
                                    //Считаем рабочий процесс завершённым
                                    isFinish = true;

                                    break;
                                }
                            }

                            break;
                        }
                    case WorkflowStopper.enNeed.FINISH:
                        {
                            //Считаем рабочий процесс завершённым
                            isFinish = true;

                            break;
                        }
                    default:
                        throw new Exception("Обнаружено необрабатываемое требование в рамках остановки рабочего процесса.");

                }
            }
            catch (Exception ex)
            {
#if DEBUG
                //Формируем исключение
                throw new Exception("Ошибка в ходе обработки элемента (стоппера) рабочего процесса.", ex);
#else
                //Пробрасываем исключение
                throw;
#endif
            }
        }

        /// <summary>
        /// Метод для запуска выполнения рабочего процесса
        /// </summary>
        /// <param name="materials">Набор материалов</param>
        /// <returns>Успешен ли запуск?</returns>
        private bool Workflow_Run_Launch(Materials materials)
        {
            try
            {
                //Проверка на разумность
                if(materials == null)
                    throw new Exception("Набор материалов не определён.");

                //Проверка на разумность
                if(this.workflowPluginsBuffer == null)
                    throw new Exception("Буфер плагинов в рамках рабочего процесса не определён.");

                //Формируем рабочий процесс
                this.CurrentWorkflow = IA.Config.Controller.GetSpecificGlobalWorkflow(new Config.Objects.Settings.Set(Project.GetByID(materials.ProjectID).Settings));

                //Если сформировать рабочий процесс не удалось
                this.CurrentWorkflow.ThrowIfNull("Не удалось сформировать рабочий процесс.");

                //Инициализируем прогресс в рамках рабочего процесса
                this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.WORKFLOW].SetProgress(0, (ulong)this.CurrentWorkflow.Elements.Where(e => e is WorkflowRunner).Count());

                //Подготовка рабочего процесса к выполнению. Отбрасывание уже выполненных плагинов.
                this.Workflow_Run_Prepare(materials);

                //Проверка на разумность
                this.CurrentWorkflow.ThrowIfNull("Рабочий процесс не определён.");

                //Принудительно очищаем буфер плагинов
                this.workflowPluginsBuffer.Clear();

                //Переходим к обработке первого элемента рабочего процесса
                this.Workflow_Run_Next();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка при запуске выполнения рабочего процесса.",
                    (message) => this.Workflow_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message))
                );

                return false;
            }

            return true;
        }

        /// <summary>
        /// Метод для запуска обработки следующего по очереди элемента рабочего процесса
        /// </summary>
        private void Workflow_Run_Next()
        {
            try
            {
                //Проверка на разумность
                if(this.CurrentWorkflow == null)
                    throw new Exception("Рабочий процесс не определён.");

                //Если больше нет элементов для обработки, ничего не делаем
                if (this.CurrentWorkflow.Elements.Count == 0)
                    throw new Exception("Недопустимая ситуация. Рабочий процесс не содержит ни одного элемента.");

                //Получаем элемент рабочего процесса для обработки (не удаляя его из очереди)
                WorkflowElement workflowElement = this.CurrentWorkflow.Elements.Peek();

                //Если элемент является раннером
                WorkflowRunner workflowRunner;
                if ((workflowRunner = workflowElement as WorkflowRunner) != null)
                {
                    bool isNeedUserIntervention;

                    //Обрабатываем раннер
                    this.Workflow_Run_Element_Processing(workflowRunner, out isNeedUserIntervention);

                    //Удаляем раннер из рабочего процесса
                    this.CurrentWorkflow.Elements.Dequeue();

                    //Если требуется вмешательство пользователя
                    if (isNeedUserIntervention)
                    {
                        //Запускаем обработку буфера плагинов
                        this.Workflow_Buffer_Process();
                    }
                    //В противном случае
                    else
                    {
                        //Переходем к обработке следующего элемента рабочего процесса
                        this.Workflow_Run_Next();
                    }

                    return;
                }

                //Если элемент является стоппером
                WorkflowStopper workflowStopper;
                if ((workflowStopper = workflowElement as WorkflowStopper) != null)
                {
                    //Проверка на разумность
                    if(this.workflowPluginsBuffer == null)
                        throw new Exception("Буфер плагинов в рамках рабочего процесса не определён");

                    //Если буффер плагинов не пуст
                    if (this.workflowPluginsBuffer.Count != 0)
                    {
                        //Запускаем обработку буфера плагинов
                        this.Workflow_Buffer_Process();
                        return;
                    }

                    bool isNeedReports, isFinish;

                    //Обрабатываем стоппер
                    this.Workflow_Run_Element_Processing(workflowStopper, out isNeedReports, out isFinish);

                    //Если требуется сгенерировать отчёты ничего не делаем
                    if (isNeedReports)
                        return;

                    //Удаляем стоппер из рабочего процесса
                    this.CurrentWorkflow.Elements.Dequeue();

                    //Если рабочий процесс считается завершённым, принудительно заканчиваем обработку.
                    if (isFinish)
                        this.Workflow_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.SUCCESS));
                    else
                        //Переходим к обработке следующего элемента рабочего процесса
                        this.Workflow_Run_Next();

                    return;
                }

                //Формируем исключение
                throw new Exception("Обнаружен необрабатываемый элемент рабочего процесса.");
            }
            catch (Exception ex)
            {
#if DEBUG
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка при обработке одного из элементов рабочего процесса.",
                    (message) => this.Workflow_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message))
                );
#else
                //Завершаем выполнение рабочего процесса
                this.Workflow_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, ex.Message));
#endif
            }
        }

        /// <summary>
        /// Метод, вызываемый после завершения выполнения рабочего процесса
        /// </summary>
        /// <param name="result">Результат выполнения рабочего процесса</param>
        private void Workflow_Run_Finish(ExecutionResultInfo result)
        {
            try
            {
                //Проверка на разумность
                if(result == null)
                    throw new Exception("Результат выполнения рабочего процесса не определён.");

                //Проверка на разумность
                if(this.workflowPluginsBuffer == null)
                    throw new Exception("Буфер плагинов в рамках рабочего процесса не определён");

                //Принудительно устанавливаем максимальный прогресс
                this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.WORKFLOW].SetMaximumProgress();

                //Очищаем буфер плагинов
                this.workflowPluginsBuffer.Clear();

                //Оцениваем результат
                switch (result.Type)
                {
                    case ExecutionResultInfo.ResultType.SUCCESS:
                        {
                            //Формируем сообщение
                            Monitor.Log.Information(MainForm.ObjectName, "Рабочий процесс успешно завершён.", true);

                            //Меняем состояние приложения
                            this.State = State.STORAGE_OPENED_USER_ACTION;

                            break;
                        }
                    case ExecutionResultInfo.ResultType.ERROR:
                        {
                            //Формируем ошибку
                            Monitor.Log.Error(MainForm.ObjectName, "Рабочий процесс завершён с ошибкой.\n" + (result.Message ?? "Неизвестная ошибка.") + "\nТекущий проект будет закрыт.", true);

                            //Закрываем текущий проект без сохранения
                            if (!this.Project_Close(this.CurrentStorage, false))
                                return;

                            break;
                        }
                    case ExecutionResultInfo.ResultType.CANCEL:
                        throw new Exception("Отмена выполнения рабочего процесса недопустима.");
                    default:
                        throw new Exception("Неизвестный тип результата выполнения какого-либо действия.");
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при завершении выполнения рабочего процесса.", (message) => Monitor.Log.Error(MainForm.ObjectName, message));
            }
        }

        /// <summary>
        /// Обработчик - Генерация отчётов по проекту
        /// </summary>
        /// <param name="result">Результат генерации отчётов по проекту</param>
        private void Project_Report_Generate_ActionAfter(ExecutionResultInfo result)
        {
            try
            {
                //Проверка на разумность
                if (result == null)
                    throw new Exception("Результат генерации отчётов по проекту не задан.");

                //Проверка на разумность
                if (this.workflowPluginsBuffer == null)
                    throw new Exception("Буфер плагинов в рамках рабочего процесса не определён");

                //Проверка на разумность
                if (this.CurrentWorkflow == null)
                    throw new Exception("Рабочий процесс не определён.");

                //Оцениваем результат
                switch (result.Type)
                {
                    case ExecutionResultInfo.ResultType.SUCCESS:
                        {
                            Monitor.Log.Information(MainForm.ObjectName, "Генерация отчётов по проекту завершилась успешно.", true);
                            break;
                        }
                    case ExecutionResultInfo.ResultType.ERROR:
                        {
                            Monitor.Log.Error(MainForm.ObjectName, "Генерация отчётов по проекту завершилась с ошибкой.\n" + result.Message, true);
                            break;
                        }
                    case ExecutionResultInfo.ResultType.CANCEL:
                        break;
                    default:
                        throw new Exception("Неизвестный тип результата выполнения какого-либо действия.");
                }
                    

                //Если буфер плагинов пуст
                if (this.workflowPluginsBuffer.Count == 0)
                {
                    //Остались ли необработанные элементы рабочего процесса?
                    if (this.CurrentWorkflow.Elements.Count != 0)
                        this.Workflow_Run_Next();
                    else
                        this.Workflow_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.SUCCESS));
                }
                //В противном случае
                else
                {
                    //Запускаем обработку буфера
                    this.Workflow_Buffer_Process();
                }

            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе выполнения рабочего процесса.",
                    (message) => this.Workflow_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message))
                );
            }
        } 
    }
}
