﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using IA.Extensions;
using IA.MainForm.Controls;
using IOController;

namespace IA.MainForm
{
    using Buttons = Dictionary<string, EventHandler>;

    using Plugin = IA.Plugin.Handler;
    using Plugins = IA.Plugin.Handlers;
    using PluginsQueue = Queue<IA.Plugin.Handler>;
    using IA.Controls;
    using IA.Plugin;
    using Windows7.DesktopIntegration.WindowsForms;
    using Windows7.DesktopIntegration;
    using System.IO;
    /// <summary>
    /// Часть класса основной формы, отвечающая за работу с плагинами
    /// </summary>
    public partial class MainForm
    {
        /// <summary>
        /// Класс, реализующий информацию о результате выполнения какого-либо действия
        /// </summary>
        private class ExecutionResultInfo
        {
            /// <summary>
            /// Всевозможные виды результатов выполнения
            /// </summary>
            internal enum ResultType
            {
                /// <summary>
                /// Успех
                /// </summary>
                SUCCESS,
                /// <summary>
                /// Ошибка
                /// </summary>
                ERROR,
                /// <summary>
                /// Отмена
                /// </summary>
                CANCEL
            }

            /// <summary>
            /// Успешно ли завершилось действие?
            /// </summary>
            internal ResultType Type { get; private set; }

            /// <summary>
            /// Поясняющее сообщение
            /// </summary>
            internal string Message { get; private set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="type">Вид результата</param>
            /// <param name="message">Поясняющее сообщение</param>
            internal ExecutionResultInfo(ResultType type, string message = null)
            {
                this.Type = type;
                this.Message = message;
            }
        }

        #region Поля
        /// <summary>
        /// Очередь плагинов для запуска. Не может быть Null.
        /// </summary>
        private PluginsQueue pluginsQueue_toRun = new PluginsQueue();

        public int pluginsQueue_Count
        {
            get 
            {
                if (pluginsQueue_toRun.Count != 0)
                    return pluginsQueue_toRun.Count;
                else
                    return pluginsQueue_toGenerateReports.Count;
            }
        }

        /// <summary>
        /// поток, в котором выполняются плагины
        /// </summary>
        public StoppableBackgroundWorker.StoppableBackgroundWorker worker;

        /// <summary>
        /// Очередь плагинов для генерации отчётов. Не может быть Null.
        /// </summary>
        private PluginsQueue pluginsQueue_toGenerateReports = new PluginsQueue(); 
        #endregion

        #region Делегаты и события
        /// <summary>
        /// Делегат для описания некого действия, которое необходимо произвести после завершения выполнения чего-либо
        /// </summary>
        /// <param name="result">Результат выполнения чего-либо</param>
        private delegate void ExecutionActionAfter(ExecutionResultInfo result);
        #endregion

        /// <summary>
        /// Обработчик - Изменился текущий выбранный плагин
        /// </summary>
        /// <param name="plugin">Плагин</param>
        private void Plugin_CurrentChoosedPluginChanged_EventHandler(Plugin plugin)
        {
            try
            {
                //Задаём текущий выбранный плагин
                this.CurrentChoosedPlugin = plugin;

                //Меняем доступность кнопок меню\панели иснтрументов
                Actions_Refresh();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Изменение текущего выбранного плагина.", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Изменился текущий корневой плагин
        /// </summary>
        /// <param name="plugin">Плагин</param>
        private void Plugin_CurrentRootPluginChanged_EventHandler(Plugin plugin)
        {
            try
            {
                //Задаём корневой плагин
                this.CurrentRootPlugin = plugin;

                //Меняем доступность кнопок меню\панели иснтрументов
                Actions_Refresh();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Изменение текущего корневого плагина.", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Добавили\убрали плагин-зависимость в процесс мультизапуска
        /// </summary>
        /// <param name="plugin">Плагин</param>
        private void Plugin_DependencyMultiRunConditionChanged_EventHandler(Plugin plugin)
        {
            try
            {
                //Меняем доступность кнопок меню\панели иснтрументов
                Actions_Refresh();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Добавление\\удаление плагина-зависимости из процесса мультзапуска.", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        #region Дополнительные проверки и операции над плагинами
        /// <summary>
        /// Метод для проверки настроек списка плагинов.
        /// </summary>
        /// <param name="plugins">Список плагинов.</param>
        /// <returns></returns>
        private bool Plugin_Settings_Check(Plugins plugins)
        {
            try
            {
                //Проверка на разумность
                if (plugins == null)
                    throw new Exception("Список плагинов не определён.");

                //Проверяем все плагины
                foreach (Plugin plugin in plugins)
                {
                    string message;

                    //Проверяем настройки плагина
                    if (!Plugins.settingsChecker.isSettingsCorrect(plugin, out message, true))
                    {
                        //Отображаем ошибку
                        Monitor.Log.Error(
                            MainForm.ObjectName,
                            Messages.Plugin.SettingsWindow_Check_Error(this.Mode, plugin.Name) + "\n" + message,
                            true
                        );

                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при проверке настроек плагина(ов).", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));
                
                return false;
            }

            return true;
        }

        /// <summary>
        /// Метод для сохранения настроек списка плагинов
        /// </summary>
        /// <param name="plugins">Список плагинов</param>
        private void Plugin_Settings_Save(Plugins plugins)
        {
            try
            {
                //Проверка на разумность
                if (this.CurrentStorage == null)
                    throw new Exception("Хранилище не определено.");

                //Проверка на разумность
                if (plugins == null)
                    throw new Exception("Список плагинов не определён.");

                //Сохраняем настройки плагинов
                foreach (Plugin plugin in plugins)
                    plugin.SaveSettings(this.CurrentStorage.Controller.Storage);

                //Переводим фокус на список плагинов
                pluginListControl.Focus();

                //Меняем доступность кнопок меню\панели иснтрументов
                Actions_Refresh();
                // обновим контекстное меню
                pluginListControl.Plugin_Click_EventHandler(null, null);


                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при сохранении настроек плагина(ов).", ex);
            }
        }

        /// <summary>
        /// Метод для проверки корректности завершения плагинов.
        /// </summary>
        /// <param name="plugins">Список плагинов.</param>
        /// <returns></returns>
        private bool Plugin_Check_SuccessfulCompletion(Plugins plugins)
        {
            try
            {
                //Проверка на разумность
                if (plugins == null)
                    throw new Exception("Список плагинов не определён.");

                //Проверяем все плагины
                foreach (Plugin plugin in plugins)
                {
                    //Проверяем что плагин успешно завершился
                    if (plugin.RunResult != Plugin.ExecutionResult.SUCCESS)
                    {
                        Monitor.Log.Error(MainForm.ObjectName, "Плагин <" + plugin.Name + "> не завершился или завершился некорректно.", true);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при проверке корректности завершения плагина(ов).", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));

                return false;
            }

            return true;
        }

        /// <summary>
        /// Метод для проверки возможности перезапуска плагинов.
        /// </summary>
        /// <param name="plugins">Список плагинов.</param>
        /// <returns></returns>
        private bool Plugin_Check_RerunAbility(Plugins plugins)
        {
            try
            {
                //Проверка на разумность
                if (plugins == null)
                    throw new Exception("Список плагинов не определён.");

                //Проверяем все плагины
                foreach (Plugin plugin in plugins)
                {
                    //Проверяем что плагин успешно завершился
                    if (!plugin.IsCapableTo(IA.Plugin.Capabilities.RERUN) && plugin.RunResult == Plugin.ExecutionResult.SUCCESS)
                    {
                        Monitor.Log.Error(MainForm.ObjectName, "Плагин <" + plugin.Name + "> не может быть запущен многократно.", true);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при проверке возможности перезапуска плагина(ов).", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));

                return false;
            }

            return true;
        }
        #endregion

        #region Окно настроек

        /// <summary>
        /// Метод для отображения настроек списка плагинов.
        /// </summary>
        /// <param name="plugins">Список плагинов.</param>
        /// <param name="isSettingsWindowExists">Есть ли у списка плагинов окно настроек.</param>
        /// <param name="settingsWindowButtons">Набор кнопок, соотвтетствующий окну настроек списка плагинов.</param>
        /// <returns></returns>
        private bool Plugin_SettingsWindow_Show(Plugins plugins, out bool isSettingsWindowExists, Buttons settingsWindowButtons)
        {
            //Изначально наличие окна настроек плагинов неизвестно
            isSettingsWindowExists = false;

            try
            {
                //Проверка на разумность
                if (plugins == null)
                    throw new Exception("Список плагинов не определён.");

                //Получаем тип настроек для отображения в зависимости от режима работы приложения
                IA.Plugin.SettingsType type;
                switch (this.Mode)
                {
                    case Mode.BASIC: type = IA.Plugin.SettingsType.SIMPLE; break;
                    case Mode.EXPERT: type = IA.Plugin.SettingsType.CUSTOM; break;
                    case Mode.NO:
                    default:
                        throw new Exception(IA.Exceptions.UNKNOWN_MODE.Description());
                }

                //Очищаем рабочее пространство
                workspaceControl.Clear();

                //Формируем список окон настроек плагинов
                List<Control> pluginSettingsWindowList = new List<Control>();

                //Добавляем настройки всех плагинов на рабочее пространство
                foreach (Plugin plugin in plugins)
                {
                    //Получаем необходимое окно с настройками плагина
                    Control pluginSettingsWindow;

                    switch (type)
                    {
                        case IA.Plugin.SettingsType.SIMPLE:
                            {
                                if (!plugin.IsCapableTo(IA.Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW))
                                    continue;

                                pluginSettingsWindow = plugin.SimpleSettingsWindow;

                                break;
                            }
                        case IA.Plugin.SettingsType.CUSTOM:
                            {
                                if (!plugin.IsCapableTo(IA.Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW))
                                    continue;

                                pluginSettingsWindow = plugin.CustomSettingsWindow;

                                break;
                            }
                        default:
                            throw new Exception(IA.Plugin.Exceptions.UNKNOWN_SETTINGS_TYPE.Description());
                    }

                    //Если мы до сюда дошли, значит окно с настройками есть хотя бы у одного плагина из списка
                    isSettingsWindowExists = true;

                    //Разворачиваем окно с настройкми плагина
                    pluginSettingsWindow.Dock = DockStyle.Fill;

                    //Задаём наименование элемента управления
                    pluginSettingsWindow.Text = Messages.Plugin.SettingsWindow_Header(this.Mode, plugin.Name);

                    //Добавляем групбокс в список окон настроек
                    pluginSettingsWindowList.Add(pluginSettingsWindow);
                }

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED_USER_ACTION;

                //Отображаем окна настроек всех плагинов на рабочем пространстве
                workspaceControl.SetControls(pluginSettingsWindowList);


                //Отображаем кнопки на рабочее пространство
                workspaceControl.SetButtons(settingsWindowButtons);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при отображении настроек плагина(ов).", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;

                return false;
            }

            return true;
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню показать настройки нескольких плагинов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="plugins">Список плагинов</param>
        private void Plugin_SettingsWindow_Show_ActionHandler(object sender, Action action, Plugins plugins)
        {
            try
            {
                //Изначально наличие окна настроек плагина неизвестно
                bool isSettingsWindowExists = false;

                //Отсеиваем завершённые плагины, недоступные для перезапуска
                plugins.RemoveAll(p => p.RunResult == Plugin.ExecutionResult.SUCCESS && !p.IsCapableTo(IA.Plugin.Capabilities.RERUN));

                //Формируем набор кнопок
                Buttons settingsWindowButtons = new Buttons()
                {
                    {"Сохранить",   (s, e) => this.Plugin_SettingsWindow_Save_Click(plugins) },
                    {"Отмена",      (s, e) => this.Plugin_SettingsWindow_Cancel_Click()             }
                };

                //Отображаем окно настроек плагина
                if (!Plugin_SettingsWindow_Show(plugins, out isSettingsWindowExists, settingsWindowButtons))
                    return;

                //Если окно результататов не определено, отображаем ошибку
                if (!isSettingsWindowExists)
                    Monitor.Log.Error(MainForm.ObjectName, plugins.Count == 1 ? "Плагин <" + plugins.First().Name + "> не имеет настроек." : "Выбранные плагины не имеют настроек.");
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню сохранить в окне настроек списка плагинов
        /// </summary>
        /// <param name="plugins">Список плагинов</param>
        private void Plugin_SettingsWindow_Save_Click(Plugins plugins)
        {
            try
            {
                //Проверяем настройки всех плагинов
                if (!this.Plugin_Settings_Check(plugins))
                    return;
                //Сохраняем настройки плагинов
                Plugin_Settings_Save(plugins);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Сохранение изменений при работе с настройками плагина", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню отменить в окне настроек списка плагинов
        /// </summary>
        private void Plugin_SettingsWindow_Cancel_Click()
        {
            try
            {
                //Переводим фокус на список плагинов
                pluginListControl.Focus();

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Отмена изменений при работе с настройками плагина", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
                
        }
        #endregion

        #region Выполнение

        static public int numbersOfPluginsToRun = 0;

        private void Run_plugin_Pack(Plugins plugins, string tracesPath, string ReportsPath)
        {
            try
            {
                //Удаляем выполненные плагины, не допускающие перезапуск, из списка
                plugins.RemoveAll(p => p.RunResult == Plugin.ExecutionResult.SUCCESS && !p.IsCapableTo(IA.Plugin.Capabilities.RERUN));

                //Запускаем выполнение
                this.Plugin_Run_Launch(plugins, null, true, tracesPath, ReportsPath);
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException("", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        private void Plugin_Run_GenerateReports_Next(ExecutionActionAfter executionActionAfter, string ReportsPath)
        {
            try
            {
                //Проверка на разумность
                if (this.pluginsQueue_toRun == null)
                    throw new Exception("Очередь плагинов для запуска выполнения не определена.");

                //Если плагинов для запуска больше нет
                if (this.pluginsQueue_toRun.Count == 0)
                {
                    //Завершаем выполнение списка плагинов
                    this.Plugin_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.SUCCESS), executionActionAfter);

                    runPack(ReportsPath);

                    return;
                }

                //Получаем плагин для запуска выполнения
                Plugin pluginToRun = this.pluginsQueue_toRun.Dequeue();

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED_COMPUTER_ACTION;

                //Отображаем окно выполнения плагина
                bool isRunWindowExists;
                if (!Plugin_RunWindow_Show(pluginToRun, out isRunWindowExists))
                    throw new Exception("Не удалось отобразить окно выполнения плагина <" + pluginToRun.Name + ">.");

                //Добавляем сообщение о запуске плагина в лог
                Monitor.Log.Information(pluginToRun.Name, Messages.Plugin.Run_Launch(this.Mode), false);

                tasksControl.AddRange(pluginToRun.TasksReport);

                //Запускаем выполнение плагина в асинхронном потоке
                worker = new StoppableBackgroundWorker.StoppableBackgroundWorker();
                worker.RunWorkerCompleted += (sender, e) => this.Plugin_Run_After_Pack(pluginToRun, executionActionAfter, ReportsPath);
                worker.DoWork += (sender, e) =>
                {
                    string projName = (currentProject.Name == "Синтез_ОС_7" || currentProject.Name == "Синтез_ОС_7_Part_2") ? currentMaterials.Comment : currentProject.Name;
                    projName = projName.Replace("<", "").Replace(">", "").Replace("?", "").Replace("*", "");
                    projName = Path.Combine(ReportsPath, projName);
                    //if (!Directory.Exists(projName))
                    {
                        this.Plugin_Run(pluginToRun);
                        this.Plugin_Report_Generate_Run(pluginToRun, projName);
                    }
                };
                worker.RunWorkerAsync();
            }
            catch (System.Threading.ThreadAbortException)
            {/* Игнорируем исключение, возникшее при попытке убить процесс нажатием на кнопку "Стоп"*/ }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе запуска выполнения одного из плагинов.",
                    (message) => this.Plugin_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message), executionActionAfter)
                );
            }
        }

        private void Plugin_Run_After_Pack(Plugin plugin, ExecutionActionAfter executionActionAfter, string ReportsPath = "")
        {
            string pluginName = "имя неизвестно";

            try
            {
                //Првоерка на разумность
                if (plugin == null)
                    throw new Exception("Плагин не определён.");

                //Получаем имя плагина
                pluginName = plugin.Name;

                //Отображаем максимальный прогресс
                this.progressControl.Elements[ProgressControl.ProgressType.PLUGIN].SetMaximumProgress();

                //Обрабатываем результат запуска плагина
                switch (plugin.RunResult)
                {
                    case Plugin.ExecutionResult.SUCCESS:
                        {
                            //Формируем сообщения
                            string message = Messages.Plugin.Run_Finish_Successfully(this.Mode, pluginName);
                            string messageNoName = Messages.Plugin.Run_Finish_Successfully(this.Mode);

                            //Добавляем сообщение о завершении плагина в лог
                            Monitor.Log.Information(pluginName, messageNoName);

                            //Отображаем сообщение в трее
                            trayIcon.ShowMessage(message);

                            break;
                        }
                    case Plugin.ExecutionResult.NO:
                        break;
                    case Plugin.ExecutionResult.ERROR:
                        {
                            //Формируем сообщения
                            string message = Messages.Plugin.Run_Finish_WithError(this.Mode, pluginName);
                            string messageNoName = Messages.Plugin.Run_Finish_WithError(this.Mode);

                            //Добавляем сообщение о завершении плагина с ошибкой в лог
                            Monitor.Log.Error(pluginName, messageNoName);

                            //Отображаем сообщение в трее
                            trayIcon.ShowMessage(message);

                            //Выбрасываем исключение
                            throw new Exception(message);
                        }
                    default:
                        throw new Exception(Plugin.Exceptions.UNKNOWN_EXECUTION_RESULT.Description());
                }

                //Очищаем строку статуса
                statusControl.Clear();

                //Увеличиваем общий прогресс на 1
                this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.PLUGINLIST].IncProgress();
                
                //Помечаем плагин, как выполненный
                plugin.IsCompleted = true;

                //Запускаем следующий плагин в очереди
                this.Plugin_Run_GenerateReports_Next(executionActionAfter, ReportsPath);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе постобработки плагина <" + pluginName + "> после завершения выполнения.",
                    (message) => this.Plugin_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message), executionActionAfter)
                );
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню запустить список плагинов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="plugins">Список плагинов</param>
        private void Plugin_Run_ActionHandler(object sender, Action action, Plugins plugins)
        {
            try
            {
                //Удаляем выполненные плагины, не допускающие перезапуск, из списка
                plugins.RemoveAll(p => p.RunResult == Plugin.ExecutionResult.SUCCESS && !p.IsCapableTo(IA.Plugin.Capabilities.RERUN));

                //Если среди плагинов есть выполненные, но допускающие перезапуск
                plugins.Where(p => p.RunResult == Plugin.ExecutionResult.SUCCESS && p.IsCapableTo(IA.Plugin.Capabilities.RERUN)).ToList().ForEach(p =>
                {
                    //Уточняем у пользователя, требуется ли перезапустить ранее выполненные плагины
                    switch (MessageBox.Show("Плагин <" + p.Name + "> может быть перезапущен. Перезапуск плагина приводит к автоматическому перезапуску всех последующих плагинов в очереди. Перезапустить указанный плагин?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        case DialogResult.Yes:
                            //Всё оставшиеся плагины перезапускаются автоматически
                            return;
                        case DialogResult.No:
                            //Удаляем плагин из списка для запуска
                            plugins.Remove(p);
                            break;
                    }
                });

                WindowsFormsExtensions.SetTaskbarProgressState(MainForm.thisForm, Windows7.DesktopIntegration.Windows7Taskbar.ThumbnailProgressState.NoProgress);

                //Запускаем выполнение
                this.Plugin_Run_Launch(plugins, null);
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Метод для запуска выполнения списка плагинов.
        /// </summary>
        /// <param name="plugins">Список плагинов</param>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после (успешного/неуспешного) завершения выполнения списка плагинов.</param>
        /// <returns>Успешно ли проведён запуск?</returns>
        private void Plugin_Run_Launch(Plugins plugins, ExecutionActionAfter executionActionAfter, bool inPack = false, string tracesPath = "", string ReportsPath = "")
        {
            try
            {
                //Проверка на разумность
                if (plugins == null)
                    throw new Exception("Список плагинов не определён.");

                if (!inPack)
                {
                    //Проверка на то, что все плагины настроены
                    if (!Plugin_Settings_Check(plugins))
                        throw new Exception("Один или несколько плагинов некорректно настроены.");

                    //Проверка на то, что все плагины можно запустить
                    if (!Plugin_Check_RerunAbility(plugins))
                        throw new Exception("Один или несколько плагинов не могут быть запущены повторно.");
                }
                //Очищаем рабочее пространство
                workspaceControl.Clear();

                //Очищаем список тасков
                tasksControl.Clear();

                //Задаём таски для отображения прогресса
                foreach (Plugin plugin in plugins)
                {
                    if (inPack)
                    {
                        if (plugin.ID == Store.Const.PluginIdentifiers.DYNAMIC_ANALYSIS_RESULTS_PROCESSING)
                            plugin.SetSimpleSettings(tracesPath);
                        else
                            plugin.SetSimpleSettings("");
                    }
                    tasksControl.AddRange(plugin.Tasks);
                }

                //Активируем таймер
                //timer.Enabled = true;

                //На данном этапе очередь должна быть пустой
                if (this.pluginsQueue_toRun.Count != 0)
                    throw new Exception("Недопустимая ситуация. Очередь плагинов для запуска выполнения не пуста.");

                //Инициализируем прогресс по списку плагинов
                progressControl.Elements[ProgressControl.ProgressType.PLUGINLIST].SetProgress(0, (ulong)plugins.Count());

                //Добавляем плагины в очередь для запуска
                this.pluginsQueue_toRun = new PluginsQueue(plugins);
                
                numbersOfPluginsToRun = plugins.Count;
                currentProgress = 0;
                
                //Запускаем первый плагин в очереди
                if (!inPack)
                    this.Plugin_Run_Next(executionActionAfter);                
                else
                    this.Plugin_Run_GenerateReports_Next(executionActionAfter, ReportsPath);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка при запуске выполнения списка плагинов.",
                    (message) => this.Plugin_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message), executionActionAfter)
                );

                WindowsFormsExtensions.SetTaskbarProgressState(MainForm.thisForm, Windows7Taskbar.ThumbnailProgressState.Error);
            }
        }



        /// <summary>
        /// Метод для запуска выполнения следующего по очереди плагина
        /// </summary>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после завершения выполнения списка плагинов</param>
        private void Plugin_Run_Next(ExecutionActionAfter executionActionAfter)
        {
            try
            {
                //Проверка на разумность
                if (this.pluginsQueue_toRun == null)
                    throw new Exception("Очередь плагинов для запуска выполнения не определена.");

                //Если плагинов для запуска больше нет
                if (this.pluginsQueue_toRun.Count == 0)
                {
                    //Завершаем выполнение списка плагинов
                    this.Plugin_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.SUCCESS), executionActionAfter);

                    //MessageBox.Show(this.ContainsFocus.ToString());

                    WindowsFormsExtensions.SetTaskbarProgress(MainForm.thisForm, 0);
                    WindowsFormsExtensions.SetTaskbarProgressState(MainForm.thisForm, Windows7Taskbar.ThumbnailProgressState.NoProgress);
                    if (!this.ContainsFocus)
                    {
                        FlashTaskBar.FlashWindowEx(this);
                    } 

                    return;
                }

                //Получаем плагин для запуска выполнения
                Plugin pluginToRun = this.pluginsQueue_toRun.Dequeue();

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED_COMPUTER_ACTION;

                //Отображаем окно выполнения плагина
                bool isRunWindowExists;
                if (!Plugin_RunWindow_Show(pluginToRun, out isRunWindowExists))
                    throw new Exception("Не удалось отобразить окно выполнения плагина <" + pluginToRun.Name + ">.");

                //Добавляем сообщение о запуске плагина в лог
                Monitor.Log.Information(pluginToRun.Name, Messages.Plugin.Run_Launch(this.Mode), false);

                //Запускаем выполнение плагина в асинхронном потоке
                worker = new StoppableBackgroundWorker.StoppableBackgroundWorker();
                worker.RunWorkerCompleted += (sender, e) => this.Plugin_Run_After(pluginToRun, executionActionAfter);
                worker.DoWork += (sender, e) =>
                    {      
                        this.Plugin_Run(pluginToRun);            
                    };                    
                worker.RunWorkerAsync();
            }
            catch (System.Threading.ThreadAbortException)
            {/* Игнорируем исключение, возникшее при попытке убить процесс нажатием на кнопку "Стоп"*/ }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе запуска выполнения одного из плагинов.",
                    (message) => this.Plugin_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message), executionActionAfter)
                );
            }
        }

        /// <summary>
        /// Метод для выполнения отдельного плагина. Метод запускается в отдельном асинхронном потоке.
        /// </summary>
        /// <param name="plugin">Плагин</param>
        private void Plugin_Run(Plugin plugin)
        {
            string pluginName = "имя неизвестно";

            try
            {
                //Проверка на разумность
                if (plugin == null)
                    throw new Exception("Плагин не определён.");

                //Получаем имя плагина
                pluginName = plugin.Name;

                //Запускаем плагин
                plugin.Run();
            }
            catch (System.Threading.ThreadAbortException)
            {/* Игнорируем исключение, возникшее при попытке убить процесс нажатием на кнопку "Стоп"*/ }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка в ходе выполнения плагина <" + pluginName + ">.", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));
            }
        }

        /// <summary>
        /// Метод, вызываемый после завершения выполнения каждого плагина из очереди
        /// </summary>
        /// <param name="plugin">Плагин</param>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после завершения выполнения списка плагинов</param>
        private void Plugin_Run_After(Plugin plugin, ExecutionActionAfter executionActionAfter)
        {
            string pluginName = "имя неизвестно";

            try
            {
                //Првоерка на разумность
                if (plugin == null)
                    throw new Exception("Плагин не определён.");

                //Получаем имя плагина
                pluginName = plugin.Name;

                //Отображаем максимальный прогресс
                this.progressControl.Elements[ProgressControl.ProgressType.PLUGIN].SetMaximumProgress();

                //Обрабатываем результат запуска плагина
                switch (plugin.RunResult)
                {
                    case Plugin.ExecutionResult.SUCCESS:
                        {
                            //Формируем сообщения
                            string message = Messages.Plugin.Run_Finish_Successfully(this.Mode, pluginName);
                            string messageNoName = Messages.Plugin.Run_Finish_Successfully(this.Mode);

                            //Добавляем сообщение о завершении плагина в лог
                            Monitor.Log.Information(pluginName, messageNoName);

                            //Отображаем сообщение в трее
                            trayIcon.ShowMessage(message);

                            break;
                        }
                    case Plugin.ExecutionResult.NO:
                        break;
                    case Plugin.ExecutionResult.ERROR:
                        {
                            //Формируем сообщения
                            string message = Messages.Plugin.Run_Finish_WithError(this.Mode, pluginName);
                            string messageNoName = Messages.Plugin.Run_Finish_WithError(this.Mode);

                            //Добавляем сообщение о завершении плагина с ошибкой в лог
                            Monitor.Log.Error(pluginName, messageNoName);

                            //Отображаем сообщение в трее
                            trayIcon.ShowMessage(message);

                            //Выбрасываем исключение
                            throw new Exception(message);
                        }
                    default:
                        throw new Exception(Plugin.Exceptions.UNKNOWN_EXECUTION_RESULT.Description());
                }

                //Очищаем строку статуса
                statusControl.Clear();

                //Увеличиваем общий прогресс на 1
                this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.PLUGINLIST].IncProgress();

                //Отображаем окно результатов плагина
                bool isResultWindowExists;
                if (!Plugin_ResultWindow_Show(plugin, out isResultWindowExists, executionActionAfter, true))
                    throw new Exception("Не удалось отобразить окно результатов плагина <" + pluginName + ">.");

                //Если у плагина нет окна результатов
                if (!isResultWindowExists)
                {
                    //Помечаем плагин, как выполненный
                    plugin.IsCompleted = true;

                    //Запускаем следующий плагин в очереди
                    this.Plugin_Run_Next(executionActionAfter);
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе постобработки плагина <" + pluginName + "> после завершения выполнения.",
                    (message) => this.Plugin_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message), executionActionAfter)
                );
            }
        }

        /// <summary>
        /// Метод, вызываемый после завершения выполнения всей очереди плагинов
        /// </summary>
        /// <param name="result">Результат выполнения всей очереди плагинов</param>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после завершения выполнения списка плагинов</param>
        private void Plugin_Run_Finish(ExecutionResultInfo result, ExecutionActionAfter executionActionAfter)
        {
            try
            {
                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;

                //Принудительно выставляем максимальный прогресс
                this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.PLUGINLIST].SetMaximumProgress();

                //Оцениваем результат
                switch (result.Type)
                {
                    case ExecutionResultInfo.ResultType.SUCCESS:
                        {
                            //Проверка на разумность
                            if (pluginsQueue_toRun.Count != 0)
                                throw new Exception("Недопустимая ситуация. Очередь плагинов для выполнения не пуста.");
                            break;
                        }
                    case ExecutionResultInfo.ResultType.ERROR:
                        {
                            //Очищаем очередь плагинов для запуска
                            pluginsQueue_toRun.Clear();

                            break;
                        }
                    case ExecutionResultInfo.ResultType.CANCEL:
                        {
                            //Очищаем очередь плагинов для запуска
                            pluginsQueue_toRun.Clear();

                            break;
                        }
                    default:
                        throw new Exception("Неизвестный тип результата выполнения какого-либо действия.");
                }

                //Выполняем необходимое действие после заврешнеия выполнения списка плагинов
                if (executionActionAfter != null)
                    executionActionAfter(result);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при завершении выполнения списка плагинов.", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;
            }
        }
        #endregion

        #region Окно выполнения плагина
        /// <summary>
        /// Метод для отображения окна выполнения плагина.
        /// </summary>
        /// <param name="plugin">Плагин.</param>
        /// <param name="isRunWindowExists">Есть ли у плагина окно выполнения.</param>
        private bool Plugin_RunWindow_Show(Plugin plugin, out bool isRunWindowExists)
        {
            //Изначально наличие окна выполнения плагина неизвестно
            isRunWindowExists = false;

            try
            {
                //Проверка на разумность
                if (plugin == null)
                    throw new Exception("Плагин не определён.");

                //Получаем имя плагина
                string pluginName = plugin.Name;

                //Узнаём, есть ли окно выполнения
                isRunWindowExists = plugin.IsCapableTo(IA.Plugin.Capabilities.RUN_WINDOW);

                //Получаем окно выполнения плагина (своё или стандартное)
                if (isRunWindowExists)
                {
                    //Очищаем рабочее пространство
                    workspaceControl.Clear();

                    //Формируем имя элемента управления
                    plugin.RunWindow.Text = Messages.Plugin.RunWindow_Header(this.Mode, pluginName);

                    //Отображаем окно выполнения на рабочем пространстве
                    workspaceControl.SetControl(plugin.RunWindow);
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при отображении окна выполнения работы плагина.", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));

                //Очищаем рабочее пространство
                workspaceControl.Clear();

                return false;
            }

            return true;
        }
        #endregion

        #region Окно результатов

        /// <summary>
        /// Метод для отображения окна результатов работы плагина.
        /// </summary>
        /// <param name="plugin">Плагин.</param>
        /// <param name="isResultWindowExists">Есть ли у плагина окно результатов.</param>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после завершения выполнения списка плагинов</param>
        /// <param name="isInAction">Отображается ли окно результатов в рамках выполнения списка плагинов?</param>
        private bool Plugin_ResultWindow_Show(Plugin plugin, out bool isResultWindowExists, ExecutionActionAfter executionActionAfter = null, bool isInAction = false)
        {
            //Изначально наличие окна реузультатов плагина неизвестно
            isResultWindowExists = false;

            try
            {
                //Проверка на разумность
                if (plugin == null)
                    throw new Exception("Плагин не определён.");

                //Получаем имя плагина
                string pluginName = plugin.Name;

                //Проверка на то, что плагин был завершён корректно
                if (plugin.RunResult != Plugin.ExecutionResult.SUCCESS)
                    throw new Exception("Работа плагина <" + pluginName + "> не была завершена или была завершена некорректно.");

                //Узнаём, есть ли окно с результатом
                isResultWindowExists = plugin.IsCapableTo(IA.Plugin.Capabilities.RESULT_WINDOW);

                //Если окно с результатами не определено, ничего не делаем
                if (!isResultWindowExists)
                    return true;

                WindowsFormsExtensions.SetTaskbarProgressState(MainForm.thisForm, Windows7Taskbar.ThumbnailProgressState.Paused);

                //Помечам плагин как невыполненный
                plugin.IsCompleted = false;

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED_USER_ACTION;

                //Получаем окно результатов плагина
                Control pluginResultWindow = plugin.ResultWindow;

                //Очищаем рабочее пространство
                workspaceControl.Clear();

                //Создаём кнопки
                Dictionary<string, EventHandler> buttons = new Dictionary<string, EventHandler>
                {
                    { "Готово", (sender, e) => this.Plugin_ResultWindow_Ready_Click(plugin, executionActionAfter, isInAction) }
                };

                //Формируем имя элемента управления
                pluginResultWindow.Text = Messages.Plugin.ResultWindow_Header(this.Mode, pluginName);

                //Отображаем окно результатов и кнопки на рабочем пространстве
                workspaceControl.SetControl(pluginResultWindow);
                workspaceControl.SetButtons(buttons);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при отображении окна результатов работы плагина.", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;

                return false;
            }

            return true;
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню показать окно результатов плагина.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="plugin">Плагин</param>
        private void Plugin_ResultWindow_Show_ActionHandler(object sender, Action action, Plugin plugin)
        {
            try
            {
                //Изначально наличие окна реузультатов плагина неизвестно
                bool isResultWindowExists = false;

                //Отображаем окно результатов плагина
                if (!Plugin_ResultWindow_Show(plugin, out isResultWindowExists))
                    return;

                //Если окно результататов не определено, отображаем ошибку
                if (!isResultWindowExists)
                    Monitor.Log.Error(MainForm.ObjectName, "Плагин <" + plugin.Name + "> не имеет результатов для обработки.");
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню готово в окне результатов плагина.
        /// </summary>
        /// <param name="plugin">Плагин</param>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после завершения выполнения списка плагинов</param>
        /// <param name="isInAction">Отображается ли окно результатов в рамках выполнения списка плагинов?</param>
        private void Plugin_ResultWindow_Ready_Click(Plugin plugin, ExecutionActionAfter executionActionAfter = null, bool isInAction = false)
        {
            try
            {
                //Сохраняем результаты работы плагина в Хранилище
                plugin.SaveResults();

                //Помечаем плагин как завершённый
                plugin.IsCompleted = true;

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;

                //Если окно с результатами было показано в рамках выполнения списка плагинов
                if (isInAction)
                    //Запускаем следующий плагин в очереди
                    this.Plugin_Run_Next(executionActionAfter);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Сохранение изменений при работе с результатами плагина", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        #endregion

        #region Стоп-пауза

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню Пауза/продолжить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="plugins">Список плагинов</param>        
        private void Plugin_Pause(object sender, Action action, Plugin plugin, ToolsControl toolsControl)
        {
            try
            {
                ToolStripButton button = toolsControl.GetToolsControlButtonByAction(actions[MultiOperation.PAUSE.FullName()]);

                if (worker.Suspended)
                {
                    worker.Resume();
                    button.Image = Properties.Resources.Plugin_Button_Pause;
                }
                else
                {
                    worker.Suspend();
                    button.Image = Properties.Resources.Plugin_Button_Pause_1;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню остановить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="plugins">Список плагинов</param>        
        private bool Plugin_Stop(object sender, Action action, Plugins plugins)
        {
            if (MessageBox.Show("Остановка плагина с большой вероятность необратимо повредит Хранилище. Продолжить?", "Предупреждение!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                try
                {
                    worker.Abort();
                }
                catch (System.Threading.ThreadStateException) { }

                try
                {
                    Plugin_Run_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.CANCEL), null);
                }
                catch (System.Threading.ThreadAbortException) { }

                MessageBox.Show("Хранилище с большой вероятностью необратимо повреждено! Рекомендуется закрыть его без сохранения и создать новый экземпляр!", "Предупреждение!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Генерация отчётов
        /// <summary>
        /// Обработчик - Нажали на кнопку\меню сгенерировать отчёты по списку плагинов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="plugins">Список плагинов</param>        
        private void Plugin_Report_Generate_ActionHandler(object sender, Action action, Plugins plugins)
        {
            try
            {
                this.Plugin_Report_Generate_Launch(plugins, null);
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Метод для запуска генерации отчётов для списка плагинов
        /// </summary>
        /// <param name="plugins">Список плагинов</param>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после (успешного/неуспешного) завершения генерации отчётов по списку плагинов.</param>
        /// <param name="isNoCancel">Можно ли отменить генерацию отчётов?</param>
        /// <returns>Успешно ли проведён запуск?</returns>
        private void Plugin_Report_Generate_Launch(Plugins plugins, ExecutionActionAfter executionActionAfter, bool isNoCancel = false)
        {
            try
            {
                //Проверка на разумность
                if (plugins == null)
                    throw new Exception("Список плагинов не определён.");

                //Проверка на то, что все плагины завершились корректно
                if (!Plugin_Check_SuccessfulCompletion(plugins))
                    throw new Exception("Один или несколько плагинов не завершился или завершился некорректно.");

                //Путь до директории для генерации отчётов по плагинам
                string reportsDirectoryPath = null;

                //Получаем путь до директории для генерации отчётов по плагинам
                reportsDirectoryPath = DirectoryController.SelectByUser(null, null, false, isNoCancel, enableCreateFolder:true);

                //Если путь не указан
                if (reportsDirectoryPath == null)
                {
                    //Если путь должен был быть обязательно указан
                    if (isNoCancel)
                        throw new Exception("Недопустимая ситуация. Директория для генерации отчётов для списка плагинов не указана.");
                    else
                    {
                        //Завершаем генерацию отчётов по списку плагинов
                        this.Plugin_Report_Generate_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.CANCEL), executionActionAfter);
                        return;
                    }
                }

                //Очищаем рабочее пространство
                workspaceControl.Clear();

                //Очищаем список тасков
                tasksControl.Clear();

                //Задаём таски для отображения прогресса
                plugins.ForEach(p => tasksControl.AddRange(p.TasksReport));

                //Активируем таймер
                //timer.Enabled = true;

                //На данном этапе очередь должна быть пустой
                if (this.pluginsQueue_toGenerateReports.Count != 0)
                    throw new Exception("Недопустимая ситуация. Очередь плагинов для генерации отчётов не пуста.");

                //Добавляем плагины в последовательность для запуска
                this.pluginsQueue_toGenerateReports = new PluginsQueue(plugins);

                numbersOfPluginsToRun = pluginsQueue_toGenerateReports.Count;
                WindowsFormsExtensions.SetTaskbarProgressState(this, Windows7Taskbar.ThumbnailProgressState.NoProgress);

                //Инициализируем прогресс по списку плагинов
                progressControl.Elements[ProgressControl.ProgressType.PLUGINLIST].SetProgress(0, (ulong)plugins.Count());


                //Запускаем генерацию отчётов по первому в очереди плагину
                this.Plugin_Report_Generate_Next(reportsDirectoryPath, executionActionAfter);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка при запуске генерации отчётов для списка плагинов.",
                    (message) => this.Plugin_Report_Generate_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message), executionActionAfter)
                );
            }
        }

        /// <summary>
        /// Метод для запуска генерации отчётов по следующему по очереди плагину
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до директории для генерации отчётов</param>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после завершения генерации отчётов по списку плагинов</param>
        private void Plugin_Report_Generate_Next(string reportsDirectoryPath, ExecutionActionAfter executionActionAfter)
        {
            try
            {
                //Проверка на разумность
                if (this.pluginsQueue_toGenerateReports == null)
                    throw new Exception("Очередь плагинов для запуска генерации отчётов не определена.");

                //Проверка на разумность
                if (reportsDirectoryPath == null)
                    throw new Exception("Путь до директории для генерации отчётов не определён.");

                //Если плагинов для запуска больше нет
                if (this.pluginsQueue_toGenerateReports.Count == 0)
                {
                    //Завершаем генерацию отчётов по списку плагинов
                    this.Plugin_Report_Generate_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.SUCCESS), executionActionAfter);

                    return;
                }

                //Получаем плагин для запуска генерации отчётов
                Plugin pluginToGenerateReports = this.pluginsQueue_toGenerateReports.Dequeue();

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED_COMPUTER_ACTION;

                //Проверка на то, что плагин был завершён корректно
                if (pluginToGenerateReports.RunResult != Plugin.ExecutionResult.SUCCESS)
                    throw new Exception("Работа плагина <" + pluginToGenerateReports.Name + "> не была завершена или была завершена некорректно.");

                //Если плагин не генерирует никаких отчётов
                if (!pluginToGenerateReports.IsCapableTo(IA.Plugin.Capabilities.REPORTS))
                {
                    //Переходим к генерации отчётов по следующему плагину в очереди
                    this.Plugin_Report_Generate_Next(reportsDirectoryPath, executionActionAfter);

                    return;
                }

                //Добавляем сообщение о запуске генерации отчётов по плагину в лог
                Monitor.Log.Information(pluginToGenerateReports.Name, Messages.Plugin.Report_Generate_Launch(this.Mode), false);

                //Запускаем генерацию отчётов по плагину в асинхронном потоке
                BackgroundWorker worker = new BackgroundWorker();
                worker.RunWorkerCompleted += (sender, e) => this.Plugin_Report_Generate_Run_After(pluginToGenerateReports, reportsDirectoryPath, executionActionAfter);
                worker.DoWork += (sender, e) => this.Plugin_Report_Generate_Run(pluginToGenerateReports, reportsDirectoryPath);
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе запуска генерации отчётов по одному из плагинов.",
                    (message) => this.Plugin_Report_Generate_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message), executionActionAfter)
                );
            }
        }

        /// <summary>
        /// Метод для генерации отчётов по отдельному плагину. Метод запускается в отдельном асинхронном потоке.
        /// </summary>
        /// <param name="plugin">Плагин</param>
        /// <param name="reportsDirectoryPath">Путь до директории для генерации отчётов</param>
        private void Plugin_Report_Generate_Run(Plugin plugin, string reportsDirectoryPath)
        {
            string pluginName = "имя неизвестно";

            try
            {
                //Проверка на разумность
                if (plugin == null)
                    throw new Exception("Плагин не определён.");

                //Проверка на разумность
                if (reportsDirectoryPath == null)
                    throw new Exception("Путь до директории для генерации отчётов не определена.");

                //Получаем имя плагина
                pluginName = plugin.Name;

                //Генерируем отчёты по плагину
                plugin.GenerateReports(reportsDirectoryPath);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка в ходе генерации отчётов по плагину <" + pluginName + ">.", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));
            }
        }

        /// <summary>
        ///  Метод, вызываемый после завершения генерации отчётов по каждому плагину из очереди
        /// </summary>
        /// <param name="plugin">Плагин</param>
        /// <param name="reportsDirectoryPath">Путь до директории для генерации отчётов</param>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после завершения генерации отчётов по списку плагинов</param>
        private void Plugin_Report_Generate_Run_After(Plugin plugin, string reportsDirectoryPath, ExecutionActionAfter executionActionAfter)
        {
            string pluginName = "имя неизвестно";
            string resultMessage = String.Empty;

            try
            {
                //Проверка на разумность
                if (plugin == null)
                    throw new Exception("Плагин не определён.");

                //Проверка на разумность
                if (reportsDirectoryPath == null)
                    throw new Exception("Путь до директории для генерации отчётов не определён.");

                //Получаем имя плагина
                pluginName = plugin.Name;

                //Отображаем максимальный прогресс
                this.progressControl.Elements[ProgressControl.ProgressType.PLUGIN].SetMaximumProgress();

                //Обрабатываем результат генерации отчётов по плагину
                switch (plugin.ReportGenerationResult)
                {
                    case Plugin.ExecutionResult.SUCCESS:
                        {
                            //Формируем сообщения
                            string message = Messages.Plugin.Report_Generate_Finish_Successfully(this.Mode, pluginName);
                            string messageNoName = Messages.Plugin.Report_Generate_Finish_Successfully(this.Mode);

                            //Добавляем сообщение о завершении генерации отчётов по плагину в лог
                            Monitor.Log.Information(pluginName, messageNoName);

                            //Отображаем сообщение в трее
                            trayIcon.ShowMessage(message);

                            break;
                        }
                    case Plugin.ExecutionResult.NO:
                    case Plugin.ExecutionResult.ERROR:
                        {
                            //Формируем сообщения
                            string message = Messages.Plugin.Report_Generate_Finish_WithError(this.Mode, pluginName);
                            string messageNoName = Messages.Plugin.Report_Generate_Finish_WithError(this.Mode);

                            //Добавляем сообщение о завершении генерации отчётов по плагину с ошибкой в лог
                            Monitor.Log.Error(pluginName, messageNoName);

                            //Отображаем сообщение в трее
                            trayIcon.ShowMessage(message);

                            //Выбрасываем исключение
                            throw new Exception(message);
                        }
                    default:
                        throw new Exception(Plugin.Exceptions.UNKNOWN_EXECUTION_RESULT.Description());
                }

                //Очищаем строку статуса
                statusControl.Clear();

                //Увеличиваем общий прогресс на 1
                this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.PLUGINLIST].IncProgress();

                //Запускаем генерацию отчётов по следующему плагину
                this.Plugin_Report_Generate_Next(reportsDirectoryPath, executionActionAfter);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage(
                    "Ошибка в ходе постобработки плагина <" + pluginName + "> после завершения генерации отчётов.",
                    (message) => this.Plugin_Report_Generate_Finish(new ExecutionResultInfo(ExecutionResultInfo.ResultType.ERROR, message), executionActionAfter)
                );
            }
        }

        /// <summary>
        /// Метод, вызываемый после завершения генерации отчётов по всем плагинам из очереди
        /// </summary>
        /// <param name="result">Результат генерации отчётов по всем плагином из очереди</param>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после завершения генерации отчётов по списку плагинов</param>
        private void Plugin_Report_Generate_Finish(ExecutionResultInfo result, ExecutionActionAfter executionActionAfter)
        {
            try
            {
                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;

                //Оцениваем результат
                switch (result.Type)
                {
                    case ExecutionResultInfo.ResultType.SUCCESS:
                        {
                            //Принудительно выставляем максимальный прогресс
                            this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.PLUGINLIST].SetMaximumProgress();

                            //Проверка на разумность
                            if (this.pluginsQueue_toGenerateReports.Count != 0)
                                throw new Exception("Недопустимая ситуация. Очередь плагинов для генерации отчётов не пуста.");

                            break;
                        }
                    case ExecutionResultInfo.ResultType.ERROR:
                        {
                            //Принудительно выставляем максимальный прогресс
                            this.progressControl.Elements[IA.MainForm.Controls.ProgressControl.ProgressType.PLUGINLIST].SetMaximumProgress();

                            //Очищаем очередь плагинов для запуска
                            this.pluginsQueue_toGenerateReports.Clear();

                            break;
                        }
                    case ExecutionResultInfo.ResultType.CANCEL:
                        break;
                    default:
                        throw new Exception("Неизвестный тип результата выполнения какого-либо действия.");
                }

                //Выполняем необходимое действие после заврешнеия генерации отчётов по очереди плагинов
                if (executionActionAfter != null)
                    executionActionAfter(result);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при завершении генерации отчётов по списку плагинов.", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;
            }
        }
        #endregion
    }
}
