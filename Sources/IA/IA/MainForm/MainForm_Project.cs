﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Objects;

namespace IA.MainForm
{
    using Plugins = IA.Plugin.Handlers;

    /// <summary>
    /// Часть класса основной формы, отвечающая за работу с проектами
    /// </summary>
    public partial class MainForm
    {
        /// <summary>
        /// Открытие проекта. По заданному проекту и материалам выгружается новое\последнее Хранилище.
        /// </summary>
        /// <param name="materials">Объект материалов из БД </param>
        /// <returns></returns>
        private bool Project_Open(Materials materials)
        {
            //Объект Хранилища для выгрузки
            Storage storage = null;

            try
            {
                //Проверка на то, что не ведётся работа с каким-то проектом ещё 
                if (this.CurrentProject != null || this.CurrentMaterials != null)
                    throw new Exception("Работа с предыдущим проектом не была завершена.");

                //Проверка на разумность
                if (materials == null)
                    throw new Exception("Набор материалов не определён.");

                //Получаем проект, с которым работаем
                Project project = Project.GetByID(materials.ProjectID);

                //Получаем список всех Хранилищ по выбранным материалам выбранного проекта
                List<Storage> allStorages = Storage.GetByMaterials(materials.ID);

                //Если Хранилищ в рамках текущего проекта и набора материалов ещё не было
                if (allStorages.Count == 0)
                {
                    //Автоматически добавляем пустое Хранилище от имени текущего пользователя
                    storage = Storage.Add(materials.ID, 0, this.CurrentUser.ID, Storage.enMode.BASIC, true, Storage.StorageAutoGeneratedComment);
                }
                else
                {
                    //Получаем список Хранилищ, созданных в основном режиме работы
                    List<Storage> userModeStorages = allStorages.Where(s => s.Mode == Storage.enMode.BASIC).ToList();

                    //Если Хранилища уже есть, но ни одно из них не создано в основном режиме работы
                    if (userModeStorages.Count == 0)
                    {
                        //Формируем сообщение
                        string message = new string[]
                        {
                            "Работа над проектом <" + project.Name+ "> уже была начата в режиме <" + Mode.EXPERT.Description() + ">.",
                            "Продолжить работу в режиме <" + Mode.BASIC.Description() + "> невозможно."
                        }.ToMultiLineString();

                        //Отображаем ошибку
                        Monitor.Log.Error(ObjectName, message, true);

                        return false;
                    }

                    //Получаем идентификатор пользователя, который ведёт работу над проектом
                    uint userID = userModeStorages.Select(s => s.CreatorID).First();

                    //Проверяем, что все Хранилища, созданное в основном режиме работы, принидлежат одному и тому же пользователю
                    if (!userModeStorages.All(s => s.CreatorID == userID))
                        throw new Exception("В рамках работы над проектом обнаружены автоматические Хранилища сгенерированные разными пользователями.");

                    //Проверяем что над проектом собирается продолжить работать правильный пользователь
                    if (this.CurrentUser.ID != userID)
                    {
                        //Формируем сообщение
                        string message = new string[]
                        {
                            "Работа над проектом <" + project.Name + "> уже была начата пользователем <" + User.GetByID(userID).Fullname + ">.",
                            "Параллельно работать над проектом можно только в режиме <" + Mode.EXPERT.Description() + ">."
                        }.ToMultiLineString();

                        //Отображаем ошибку
                        Monitor.Log.Error(ObjectName, message, true);

                        return false;
                    }

                    //Выбираем Хранилище, созданное в основном режиме работы, с максимальным идентификатором
                    storage = userModeStorages.First(s => s.ID == userModeStorages.Max(st => st.ID)); 
                }

                //Выгружаем и открываем Хранилище
                if (!this.Storage_Open(storage))
                    return false;

                //Запускаем выполнение рабочего процесса
                if (!this.Workflow_Run_Launch(materials))
                    return false;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при открытии проекта (" + ObjectName + ").");
#endif

                return false;
            }

            return true;
        }

        /// <summary>
        /// Закрытие проекта. Закрываем Хранилище с которым идёт работа. Если требуется, загружаем Хранилище на сервер.
        /// </summary>
        /// <param name="storage">Объект Хранилища из БД</param>
        /// <param name="isNeedToUpload">Необходимо ли загрузить Хранилище на сервер?</param>
        /// <returns></returns>
        private bool Project_Close(Storage storage, bool isNeedToUpload)
        {
            try
            {
                //Проверка на разумность
                if (storage == null)
                    throw new Exception("Хранилище не определено.");

                ////Прерываем выполнение рабочего процесса
                //if (thread_Workflow_Run.IsAlive)
                //    thread_Workflow_Run.Abort();

                //Закрываем и загружаем (если требуется) Хранилище на сервер
                if (!Storage_Close(storage, isNeedToUpload))
                    return false;
            }
            catch (Exception ex)
            {
#if DEBUG
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при закрытии проекта (" + ObjectName + ").");
#endif

                return false;
            }
            finally
            {
                //Текущий проект неизвестен
                this.CurrentProject = null;
            }

            return true;
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню открыть проект. Только для простого режима! 
        /// </summary>
        private void Project_Open_ActionHandler(object sender, Action action)
        {
            try
            {
                //Провера авторизации. Если проверка не пройдена - закрываем приложение.
                if (this.CurrentUser == null)
                    throw new Exception(Exceptions.NOT_ALLOWED_FOR_ANONYMOUS_USER.Description());

                //Провера на разумность. Если проверка не пройдена - закрываем приложение.
                if (this.CurrentProject != null || this.CurrentMaterials != null || this.CurrentStorage != null)
                    throw new Exception(Exceptions.ILLEGAL_PROJECT_OPEN.Description());

                //Открываем диалог выбора и настройки проекта
                using (IA.Controls.ProjectSelectControl projectSelectDialog =
                    new IA.Controls.ProjectSelectControl()
                    {
                        Icon = this.Icon,
                        StartPosition = FormStartPosition.CenterParent
                    })
                {
                    //Если ничего не выбрали, ничего не делаем
                    if (projectSelectDialog.ShowDialog(this) != DialogResult.OK)
                        return;

                    //Открываем окно выбора материалов
                    using (IA.Controls.MaterialsSelectControl materialsSelectDialog =
                        new IA.Controls.MaterialsSelectControl())
                    {
                        //Если ничего не выбрали,  ничего не делаем
                        throw new Exception("Нужна доработка!!!");
                        //if (materialsSelectDialog.ShowDialog(this) != DialogResult.OK)
                            return;

                        //Открываем заданные материалы заданного проекта
                        if (!Project_Open(materialsSelectDialog.Result))
                        {
#if !DEBUG
                            //Отображаем ошибку
                            Monitor.Log.Error(ObjectName, "Не удалось открыть проект.", true);
#endif
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню закрыть проект
        /// </summary>
        private void Project_Close_ActionHandler(object sender, Action action)
        {
            try
            {
                //Провера авторизации. Если проверка не пройдена - закрываем приложение.
                if (this.CurrentUser == null)
                    throw new Exception(Exceptions.NOT_ALLOWED_FOR_ANONYMOUS_USER.Description());

                //Провера на разумность. Если проверка не пройдена - закрываем приложение.
                if (this.CurrentProject == null || this.CurrentMaterials == null || this.CurrentStorage == null)
                    throw new Exception(Exceptions.ILLEGAL_PROJECT_CLOSE.Description());

                //Переменная для формирования ответа пользователя
                DialogResult answer;

                //Предупреждаем о том, что проект будет закрыт
                answer = MessageBox.Show("Текущий проект будет закрыт, вы уверены?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //Если пользователь, не согласен, ничего не делаем
                if (answer != DialogResult.Yes)
                    return;

                if (!Settings.AutonomyMode)
                    answer = MessageBox.Show("Сохранить изменения внесённые в проект?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //Закрываем проект и загружаем (если требуется) изменения на сервер
                if (!this.Project_Close(this.CurrentStorage, answer == DialogResult.Yes))
                {
#if !DEBUG
                    //Отображаем ошибку
                    Monitor.Log.Error(ObjectName, "Не удалось закрыть проект.", true);
#endif
                    return;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню запустить генерацию отчётов по проекту
        /// </summary>
        private void Project_Report_Generate_ActionHandler(object sender, Action action)
        {
            try
            {
                //Запускае генерацию отчётов по проекту
                Project_Report_Generate_Launch(this.Project_Report_Generate_ActionAfter);
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Метод для запуска генерации отчётов по проекту
        /// </summary>
        /// <param name="executionActionAfter">Действие, которое необходимо выполнить после завершения генерации отчётов по проекту</param>
        private void Project_Report_Generate_Launch(ExecutionActionAfter executionActionAfter)
        {
            try
            {
                //Получаем список выполненных плагинов
                Plugins completedPlugins = IA.Plugin.HandlersInitial.GetSomeByCondition(p => p.IsCompleted);

                //Если нечего генерировать, оповещаем пользователя, ничего не делаем
                if (completedPlugins.Count == 0 || completedPlugins.All(p => !p.IsCapableTo(Plugin.Capabilities.REPORTS)))
                {
                    //Формируем предупреждение
                    Monitor.Log.Warning(MainForm.ObjectName, "На данном этапе не может быть сгенерированно ни одного отчёта.", true);
                    return;
                }

                //Генерируем отчёты по всем завершённым плагинам
                this.Plugin_Report_Generate_Launch(completedPlugins, executionActionAfter);
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка в ходе запуска генерации отчётов по проекту.");
            }
        }
    }
}
