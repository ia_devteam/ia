﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using IA.Extensions;
using IA.Objects;

using IOController;
using System.Drawing;
using System.IO;
using IA.Plugin;
using System.Threading;
using System.ComponentModel;
using IAUtils;
using System.Linq;

namespace IA.MainForm
{
    /// <summary>
    /// Часть класса основной формы, отвечающая за работу с Хранилищами
    /// </summary>
    public partial class MainForm
    {
        /// <summary>
        /// форма для выбора проекта
        /// </summary>
        IA.Controls.ProjectSelectControl projectSelectDialog;

        private const string UnloadedStorageInfoFileName = "storageInfo.xml";

        /// <summary>
        /// Подготовка рабочей папки.
        /// </summary>
        /// <param name="storage">Хранилище. Если задано, то происходит его настройка на рабочий каталог.</param>
        /// <returns>путь до рабочего каталога. null - если пользователь не задал.</returns>
        private string createWorkDirectory(Storage storage = null)
        {
            if (!DirectoryController.IsExists(IA.Settings.CommonWorkDirectoryPath))
            {
                MessageBox.Show(
                    "Укажите директорию на жёстком диске, куда следует скопировать информацию необходимую для дальнейшей работы приложения.",
                    "Информация",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                );

                IA.Settings.CommonWorkDirectoryPath = Store.WorkDirectory.SelectWorkDirectoryPathFromUser(IA.Settings.CommonWorkDirectoryPath);

                if (IA.Settings.CommonWorkDirectoryPath == null)
                    return null;
            }

            // путь до рабочей папки лично этого экземпляра IA
            string path = Path.Combine(IA.Settings.CommonWorkDirectoryPath, IA.Settings.PrivateWorkDirectoryPath);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (storage == null)
                return path;

            storage.WorkDirectory = new Store.WorkDirectory(path);

            return path;
        }

        public delegate void Plugins_Pack_Finished(string ReportsPath);

        Plugins_Pack_Finished runPack;

        public void Plugins_Pack_Finished_Handler(string ReportsPath)
        {
            if (CurrentStorage != null)
            {
                Storage_Close(CurrentStorage, false);
            }
            if (StoragesToProcess.Count != 0)
            {
                StorageForPack storage = StoragesToProcess.Dequeue();
                Storage_Open(storage.storage, true);
                pluginsToRun.ForEach((x) => { x.RunResult = Handler.ExecutionResult.NO; });
                Run_plugin_Pack(pluginsToRun, storage.TracesPath, ReportsPath);
            }
        }

        Queue<StorageForPack> StoragesToProcess;

        Handlers pluginsToRun;

        /// <summary>
        /// пакетная обработка хранилища
        /// </summary>
        /// <param name="storages">хранилища</param>
        /// <param name="plugins">плагины</param>
        /// <returns></returns>
        public bool Storage_Pack_Start(Queue<StorageForPack> storages, Handlers plugins, string ReportsPath)
        {
            StoragesToProcess = storages;
            pluginsToRun = plugins;

            runPack += Plugins_Pack_Finished_Handler;

            runPack(ReportsPath);

            return true;
        }

        /// <summary>
        /// Открытие указанного Хранилища.
        /// Выгрузка Хранилища с сервера производится в рабочую директорию. 
        /// Возможны 4 варианта развития событий.
        /// 1. Если на сервере Хранилище пусто и рабочая директория не указана      => Запрашиваем рабочую директорию с создаём в ней новое Хранилище.
        /// 2. Если на сервере Хранилище пусто и рабочая директория указана         => Создаём Хранилище в рабочей директории.
        /// 3. Если на сервере Хранилище непусто и рабочая директория не указана    => Запрашиваем рабочую директорию, выгружаем в неё и открываем Хранилище.
        /// 4. Если на сервере Хранилище непусто и рабочая директория указана       => Выгружаем Хранилище в рабочую директорию и открываем его.
        /// </summary>
        /// <param name="storage">Объект Хранилища из БД</param>
        /// <param name="isInBackground">Использовать фоновый режим (без влияния на основную форму)?</param>
        /// <returns></returns>
        public bool Storage_Open(Storage storage, bool isInBackground = false)
        {
            var mode = storage?.IsEmpty ?? true ? Store.Storage.OpenMode.CREATE : Store.Storage.OpenMode.OPEN;

            try
            {
                if (storage == null)
                    throw new Exception("Хранилище не определено.");

                System.Action doStorageOpen = () =>
                {
                    createWorkDirectory(storage);
                    if (!Settings.AutonomyMode)
                        storage.Controller.Download();
                    storage.Controller.Open(mode);
                };

                if (isInBackground)
                {
                    Monitor.Log.Information(ObjectName, "Выгрузка данных с сервера проектов запущена.");
                    doStorageOpen();
                    Monitor.Log.Information(ObjectName, "Выгрузка данных с сервера проектов завершена.");
                }
                else
                {
                    WaitWindow.Show(
                        delegate (WaitWindow window)
                        {
                            doStorageOpen();
                        },
                        Messages.Storage.Open(this.Mode)
                    );
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                ex.ReThrow("Ошибка при " + (mode == Store.Storage.OpenMode.CREATE ? "создании" : "открытии") + " Хранилища (" + ObjectName + ").");
#endif

                return false;
            }

            this.CurrentStorage = storage;

            if (!isInBackground)
            {
                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;
            }

            workspaceControl.Clear();
            Monitor.Log.Information("Открытие хранилища", "Открытие хранилища осуществлено успешно");

            return true;
        }

        private bool Storage_Unload(Storage storage, string destination)
        {
            string targetfile;
            try
            {
                if (storage.WorkDirectory == null)
                    createWorkDirectory(storage);

                if (!Settings.AutonomyMode)
                    storage.Controller.Download();
                else
                {
                    if (storage.WorkDirectory.IsFolderUnpacked(Store.WorkDirectory.enSubDirectories.STORAGE))
                    {
                        FileOperations.ArchiveWriter.ArchiveDirectoryContents(storage.WorkDirectory.GetSubDirectoryPath(Store.WorkDirectory.enSubDirectories.STORAGE),
                            Path.Combine(storage.WorkDirectory.Path, "str"), true);
                        File.Move(Path.Combine(storage.WorkDirectory.Path, "str" + FileOperations.ArchiveWriter.ArchiveDefaultExtension),
                            Path.Combine(storage.WorkDirectory.GetSubDirectoryPath(Store.WorkDirectory.enSubDirectories.STORAGE), "str" + FileOperations.ArchiveWriter.ArchiveDefaultExtension));
                    }
                }

                var suifilename = Path.Combine(storage.WorkDirectory.Path, UnloadedStorageInfoFileName);

                var sui = StorageUnloadInfo.Create(storage);
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(StorageUnloadInfo));
                using (var stream = File.OpenWrite(suifilename))
                    ser.Serialize(stream, sui);
                if (!Settings.AutonomyMode) //в режиме автономности оффлайнБД уже лежит в правильном месте
                    Sql.SqlConnector.DumpToLocalDB(suifilename);

                targetfile = Path.Combine(destination, storage.ID.ToString());
                string fulltargetfile = targetfile + FileOperations.ArchiveWriter.ArchiveDefaultExtension;
                if (File.Exists(fulltargetfile))
                {
                    if (MessageBox.Show(
                        $"Файл {targetfile} существует. Заменить? Если нет - будет создан файл с именем {storage.ID.ToString()}_N, где N - уникальное число.",
                        "Перезапись файла выгрузки хранилища", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                        == DialogResult.Yes)
                    {
                        File.Delete(fulltargetfile);
                    }
                    else
                    {
                        int i = 1;
                        do
                        {
                            targetfile = Path.Combine(destination, $"{storage.ID.ToString()}_{i++}");
                        }
                        while (File.Exists(targetfile + FileOperations.ArchiveWriter.ArchiveDefaultExtension));
                    }
                }
                FileOperations.ArchiveWriter.ArchiveDirectoryContents(storage.WorkDirectory.Path, targetfile);
            }
            catch (Exception ex)
            {
                Monitor.Log.Error("Выгрузка хранилища", ex.Message);
                return false;
            }

            Monitor.Log.Information("Выгрузка хранилища", $"Хранилище успешно выгружено в файл <{targetfile + FileOperations.ArchiveWriter.ArchiveDefaultExtension}>.");
            return true;
        }

        private bool Storage_Open_Offline(string storagearchive)
        {
            var workdir = createWorkDirectory();
            FileOperations.ArchiveReader.ExtractAll(storagearchive, workdir);
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(StorageUnloadInfo));
            string storageUnloadInfoFilename = Path.Combine(workdir, UnloadedStorageInfoFileName);
            StorageUnloadInfo sui;
            using (var stream = File.OpenText(storageUnloadInfoFilename))
                sui = ser.Deserialize(stream) as StorageUnloadInfo;
            Sql.SqlConnector.UseSqlCompactDB(storageUnloadInfoFilename);
            var storage = sui.RestoreStorage();

            Storage_Open(storage);
            return true;
        }

        /// <summary>
        /// Закрытие указанного Хранилища. 
        /// Загрузка Хранилища на сервер (если требуется) производится из рабочей директории Хранилища.
        /// </summary>
        /// <param name="storage">Объект Хранилища из БД</param>
        /// <param name="isNeedToUpload">Требуется ли загрузить Хранилище на сервер?</param>
        /// <param name="isInBackground">Использовать фоновый режим (без влияния на основную форму)?</param>
        /// <returns></returns>
        private bool Storage_Close(Storage storage, bool isNeedToUpload = false, bool isInBackground = false)
        {
            //Monitor.Log.Information(ObjectName, "Закрытие хранилища запущено.");
            try
            {
                //Проверка на разумность
                if (storage == null)
                    return true;

                //FIXME - тут необходимо останавливать выполнение всех плагинов (если возможно, то плавно)
                //Если какой-то плагин запущен - прерываем его работу
                //if (PluginsRunThread != null && PluginsRunThread.IsAlive && !(PluginsRunThread.ThreadState == System.Threading.ThreadState.AbortRequested))
                //    PluginsRunThread.Abort();

                //Формируем режим работы с Хранилищем
                Storage.enMode storageMode;
                switch (this.Mode)
                {
                    case Mode.BASIC:
                        storageMode = Storage.enMode.BASIC;
                        break;
                    case Mode.EXPERT:
                        storageMode = Storage.enMode.EXPERT;
                        break;
                    default:
                        throw new Exception(Exceptions.UNKNOWN_MODE.Description());
                }

                System.Action doClose = () =>
                {
                    storage.Controller.Close();
                    if (!Settings.AutonomyMode)
                        if (isNeedToUpload)
                            storage.Controller.Upload(CurrentUser.ID, storageMode, Storage.StorageDefaultComment);
                };

                if (isInBackground)
                {
                    Monitor.Log.Information(ObjectName, "Загрузка данных на сервер проектов запущена.");
                    doClose();
                    Monitor.Log.Information(ObjectName, "Загрузка данных на сервер проектов завершена.");
                }
                else
                {
                    WaitWindow.Show(
                        delegate (WaitWindow window)
                        {
                            doClose();
                        },
                        Messages.Storage.Close(this.Mode)
                    );
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при закрытии Хранилища (" + ObjectName + ").");
#endif

                return false;
            }
            finally
            {
                //Текущее Хранилище неизвестно
                this.CurrentStorage = null;

                //Если мы не в фоновом режиме
                if (!isInBackground)
                {
                    //Меняем состояние приложения
                    this.State = State.STORAGE_CLOSED;
                }
            }
            

            if (Settings.AutonomyMode && isNeedToUpload)
            {
                using (var dialog = new FolderBrowserDialog() { ShowNewFolderButton = true })
                {
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        Storage_Unload(storage, dialog.SelectedPath);
                            
                    }
                    else
                    {
                        MessageBox.Show($"Из рабочего каталога (задаётся в настройках) будет произведено удаление всех файлов. Если необходимо сохранить результаты работы для последующего использования - необходимо не закрывая этого окна вручную выполнить архивацию содержимого каталога <{Path.Combine(Settings.CommonWorkDirectoryPath, Settings.PrivateWorkDirectoryPath)}>.", "Удаление содержимого рабочего каталога", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                }
            }

            WaitWindow.Show(
                delegate (WaitWindow window)
                {
                    string path = Path.Combine(IA.Settings.CommonWorkDirectoryPath, IA.Settings.PrivateWorkDirectoryPath);
                    try
                    {
                        if (DirectoryController.IsExists(path))
                            DirectoryController.Remove(path, true);
                    }
                    catch (System.IO.IOException)
                    {
                        MessageBox.Show("Не удается очистить рабочую директорию. Один или несколько файлов открыты в другой программе. Очистка не завершена. ");
                        Monitor.Log.Error("Очистка рабочей папки", "Не удается очистить рабочую директорию. Один или несколько файлов открыты в другой программе. Очистка не завершена. ");
                    }
                },
                "Очистка временной папки"
            );
            Monitor.Log.Information(ObjectName, "Закрытие хранилища завершено.");
            return true;
        }

        /// <summary>
        /// никакое хранилище не открыто - показываем доступный выбор
        /// </summary>
        private void Show_Projects_ActionHandler(object sender, Action action)
        {
            try
            {

                //Открываем диалог выбора и настройки проекта
                if (projectSelectDialog == null)
                {
                    projectSelectDialog = new IA.Controls.ProjectSelectControl()
                    { Dock = DockStyle.Fill, TopLevel = false, Location = new Point(0, 0), FormBorderStyle = FormBorderStyle.None, Visible = true };
                }
                if (workspaceControl.Controls[0].GetType() != projectSelectDialog.GetType())
                {
                    workspaceControl.Clear();
                    workspaceControl.SetControl(projectSelectDialog);
                    projectSelectDialog.ShakeForm();
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException("Отображение списка проектов", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        private void ShowSelectAutonomyStoragePanel()
        {
            var c = new IA.Controls.SelectSingleStorage() { Dock = DockStyle.Fill, Visible = true };
            c.StorageReadyToOpenEvent += (o, s) => Storage_Open_Offline(s);
            workspaceControl.Clear();
            workspaceControl.SetControl(c);
        }

        private void StoragePackProcessing_ActionHandler(object sender, Action action)
        {
            Form frm = new StoragePackProcessing();
            frm.ShowDialog();
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню загрузить Хранилище
        /// </summary>
        private void Storage_Close_ActionHandler(object sender, Action action)
        {
            try
            {
                if (!Settings.AutonomyMode)
                {
                    //Провера авторизации. Если проверка не пройдена - закрываем приложение.
                    if (this.CurrentUser == null)
                        throw new Exception(Exceptions.NOT_ALLOWED_FOR_ANONYMOUS_USER.Description());

                    //Провера на разумность. Если проверка не пройдена - закрываем приложение.
                    if (this.CurrentProject == null || this.CurrentMaterials == null || this.CurrentStorage == null)
                        throw new Exception(Exceptions.ILLEGAL_STORAGE_UPLOAD.Description());
                }
                //Переменная для формирования ответа пользователя
                DialogResult answer;

                //Предупреждаем о том, что Хранилище будет закрыто
                answer = MessageBox.Show("Текущее Хранилище будет закрыто, вы уверены?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //Если пользователь, не согласен, ничего не делаем
                if (answer != DialogResult.Yes)
                    return;

                //Спрашиваем требуется ли сохранить изменения внесённые в Хранилище?
                answer = MessageBox.Show("Сохранить изменения внесённые в Хранилище?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //Закрываем и загружаем (если требуется) Хранилище на сервер
                if (!this.Storage_Close(this.CurrentStorage, answer == DialogResult.Yes))
                {
#if !DEBUG
                    //Отображаем ошибку
                    Monitor.Log.Error(ObjectName, "Не удалось закрыть Хранилище.", true);
#endif
                    return;
                }

                //Текущий проект неизвестен
                this.CurrentProject = null;


            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }


        /// <summary>
        /// Обработчик - Меню - Хранилище - Содержимое Хранилища
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="viewMode"></param>
        private void Storage_Contents_Show_ActionHandler(object sender, Action action, Store.Controls.StorageContentsViewControl.ViewMode viewMode)
        {
            try
            {
                //Провера авторизации. Если проверка не пройдена - закрываем приложение.
                if (this.CurrentUser == null)
                    throw new Exception(Exceptions.NOT_ALLOWED_FOR_ANONYMOUS_USER.Description());

                //Проверка на разумность.
                if (this.currentStorage == null || this.currentStorage.Controller.Storage.isClosed)
                    throw new Exception("Ни открыто ни одного Хранилища в данный момент.");

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED_USER_ACTION;

                //Очищаем рабочее пространство
                workspaceControl.Clear();

                //Получаем элемент управления для отображения
                Control control = new Store.Controls.StorageContentsViewControl(viewMode, this.CurrentStorage.Controller.Storage);

                //Формируем имя элемента управления
                control.Text = "Содержимое Хранилища ";
                switch (viewMode)
                {
                    case Store.Controls.StorageContentsViewControl.ViewMode.Files: control.Text += "(Файлы)"; break;
                    case Store.Controls.StorageContentsViewControl.ViewMode.Functions: control.Text += "(Функции)"; break;
                    case Store.Controls.StorageContentsViewControl.ViewMode.Variables: control.Text += "(Переменные)"; break;
                    default:
                        throw new Exception("Обнаружен неизвестный тип содержимого в Хранилище.");
                }

                //Добавляем на него элемент отображения содержимого Хранилища
                workspaceControl.SetControl(control);

                //Добавляем кнопку "Закрыть"
                workspaceControl.SetButtons(
                    new Dictionary<string, EventHandler>()
                    {
                        {
                            "Закрыть",
                            delegate(object s, EventArgs e)
                            {
                                //Меняем состояние приложения
                                this.State = State.STORAGE_OPENED;
                            }
                        }
                    }
                );
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));

                //Меняем состояние приложения
                this.State = State.STORAGE_OPENED;
            }
        }

        /// <summary>
        /// Выгрузить содержимое поддиректории рабочей директории Хранилища
        /// </summary>
        /// <param name="storage">Объект Хранилища</param>
        /// <param name="subDirectory">Поддиректория рабочей директории Хранилища</param>
        private void Storage_GetWorkSubDirectoryContents(Storage storage, Store.WorkDirectory.enSubDirectories subDirectory)
        {
            try
            {
                //Формируем всевозможные сообщения
                string questionMessage = "Укажите директорию, куда следует поместить ";
                string waitWindowMessage = "Получение ";
                string resultMessage = String.Empty;

                switch (subDirectory)
                {
                    case Store.WorkDirectory.enSubDirectories.STORAGE:
                        questionMessage += "Хранилище.";
                        waitWindowMessage += "Хранилищ";
                        resultMessage += "Файлы Хранилища";
                        break;
                    case Store.WorkDirectory.enSubDirectories.SOURCES_ORIGINAL:
                        questionMessage += "оригинальные исходные тексты.";
                        waitWindowMessage += "оригинальных исходных текстов";
                        resultMessage += "Оригинальные исходные тексты";
                        break;
                    case Store.WorkDirectory.enSubDirectories.SOURCES_CLEAR:
                        questionMessage += "очищенные исходные тексты.";
                        waitWindowMessage += "очищенных исходных текстов";
                        resultMessage += "Очищенные исходные тексты";
                        break;
                    case Store.WorkDirectory.enSubDirectories.SOURCES_LAB:
                        questionMessage += "лабораторные исходные тексты.";
                        waitWindowMessage += "лабораторных исходных текстов";
                        resultMessage += "Лабораторные исходные тексты";
                        break;
                    default:
                        throw new Exception("Обнаружена неизвестная поддиректория рабочей директории Хранилища.");
                }

                //Получаем путь до директории, куда следует скопировать содержимое, от пользователя (не указывать директорию не допускается)
                string destinationDirectoryPath = DirectoryController.SelectByUser(null, questionMessage, true, true);

                resultMessage += " успешно помещены в директорию <" + destinationDirectoryPath + ">.";

                //Отображаем окно ожидания
                WaitWindow.Show(
                    delegate (WaitWindow window)
                    {
                        //Копируем содержимое поддиректории рабочей директории Хранилища
                        storage.Controller.CopyWorkSubDirectoryContents(subDirectory, destinationDirectoryPath);
                    },
                    waitWindowMessage
                );

                //Отображаем сообщение о завершении
                Monitor.Log.Information(ObjectName, resultMessage, true);
            }
            catch (Exception ex)
            {
                //Фомируем сообщение
                string message =
#if DUBEG
                    "Ошибка при получении содержимого поддиректории рабочей директории Хранилища.";
#else
                    "Ошибка при получении данных из Хранилища.";
#endif

                //Формируем исключение
                throw new Exception(message, ex);
            }
        }
    }
}
