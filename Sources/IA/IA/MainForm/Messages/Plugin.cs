﻿using System;

using IA.Extensions;

namespace IA.MainForm.Messages
{
    /// <summary>
    /// Сообщения связанные с плагинами
    /// </summary>
    internal static class Plugin
    {
        /// <summary>
        /// Сообщение о запуске плагина
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        internal static string Run_Launch(Mode mode, string pluginName = null)
        {
            string ret = String.Empty;

            switch (mode)
            {
                case Mode.BASIC:
                    ret += "Cтадия";
                    Plugin.AddPluginName(ref ret, pluginName);
                    ret += " запущена";
                    break;
                case Mode.EXPERT:
                    ret += "Плагин";
                    Plugin.AddPluginName(ref ret, pluginName);
                    ret += " запущен";
                    break;
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }

            ret += ".";

            return ret;
        }

        /// <summary>
        /// Сообщение о завершении плагина
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <param name="pluginName">Имя плагина. Если Null - не отображать.</param>
        /// <param name="time">Затраченное время. Если Null - не отображать.</param>
        /// <returns></returns>
        internal static string Run_Finish_Successfully(Mode mode, string pluginName = null, TimeSpan? time = null)
        {
            string ret = String.Empty;

            switch (mode)
            {
                case Mode.BASIC:
                    ret += "Cтадия";
                    Plugin.AddPluginName(ref ret, pluginName);
                    ret += " успешно завершена";
                    break;
                case Mode.EXPERT:
                    ret += "Плагин";
                    Plugin.AddPluginName(ref ret, pluginName);
                    ret += " успешно завершен";
                    break;
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }

            Plugin.AddRunTime(ref ret, time);
            ret += ".";

            return ret;
        }

        /// <summary>
        /// Сообщение о завершении плагина с ошибкой
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <param name="pluginName">Имя плагина. Если Null - не отображать.</param>
        /// <param name="time">Затраченное время. Если Null - не отображать.</param>
        /// <returns></returns>
        internal static string Run_Finish_WithError(Mode mode, string pluginName = null, TimeSpan? time = null)
        {
            string ret = String.Empty;

            switch (mode)
            {
                case Mode.BASIC:
                    ret += "Cтадия";
                    Plugin.AddPluginName(ref ret, pluginName);
                    ret += " завершена";
                    break;
                case Mode.EXPERT:
                    ret += "Плагин";
                    Plugin.AddPluginName(ref ret, pluginName);
                    ret += " завершён";
                    break;
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }

            ret += " с ошибкой";
            Plugin.AddRunTime(ref ret, time);
            ret += ".";

            return ret;
        }

        /// <summary>
        /// Сообщение о запуске генерации отчётов по плагину
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <param name="pluginName">Имя плагина. Если Null - не отображать.</param>
        /// <returns></returns>
        internal static string Report_Generate_Launch(Mode mode, string pluginName = null)
        {
            string ret = "Генерация отчётов";

            if (pluginName != null)
            {
                switch (mode)
                {
                    case Mode.BASIC:
                        ret += " по стадии";
                        break;
                    case Mode.EXPERT:
                        ret += " по плагину";
                        break;
                    default:
                        throw new Exception(Exceptions.UNKNOWN_MODE.Description());
                }
                Plugin.AddPluginName(ref ret, pluginName);
            }
            
            ret += " запущена.";

            return ret;
        }

        /// <summary>
        /// Сообщение о завершении генерации отчётов по плагину
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <param name="pluginName">Имя плагина. Если Null - не отображать.</param>
        /// <param name="time">Затраченное время. Если Null - не отображать.</param>
        /// <returns></returns>
        internal static string Report_Generate_Finish_Successfully(Mode mode, string pluginName = null, TimeSpan? time = null)
        {
            string ret = "Отчёты";

            if (pluginName != null)
            {
                switch (mode)
                {
                    case Mode.BASIC:
                        ret += " по стадии";
                        break;
                    case Mode.EXPERT:
                        ret += " по плагину";
                        break;
                    default:
                        throw new Exception(Exceptions.UNKNOWN_MODE.Description());
                }
                Plugin.AddPluginName(ref ret, pluginName);
            }

            ret += " успешно сгенерированы";
            Plugin.AddRunTime(ref ret, time);
            ret += ".";

            return ret;
        }

        /// <summary>
        /// Сообщение о завершении генерации отчётов по плагину с ошибкой
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <param name="pluginName">Имя плагина. Если Null - не отображать.</param>
        /// <param name="time">Затраченное время. Если Null - не отображать.</param>
        /// <returns></returns>
        internal static string Report_Generate_Finish_WithError(Mode mode, string pluginName = null, TimeSpan? time = null)
        {
            string ret = "Отчёты";

            if (pluginName != null)
            {
                switch (mode)
                {
                    case Mode.BASIC:
                        ret += " по стадии";
                        break;
                    case Mode.EXPERT:
                        ret += " по плагину";
                        break;
                    default:
                        throw new Exception(Exceptions.UNKNOWN_MODE.Description());
                }
                Plugin.AddPluginName(ref ret, pluginName);
            }

            ret += " сгенерированы с ошибкой";
            Plugin.AddRunTime(ref ret, time);
            ret += ".";

            return ret;
        }

        /// <summary>
        /// Добавить имя плагина к сообщению
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="pluginName">Имя плагина</param>
        private static void AddPluginName(ref string message, string pluginName)
        {
            if(pluginName != null)
                message += " \"" + pluginName + "\"";
        }

        /// <summary>
        /// Добавить время выполнения к сообщению
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="time">Время выполнения</param>
        private static void AddRunTime(ref string message, TimeSpan? time)
        {
            if (time != null)
                message += " за" + Plugin.TimeToString((TimeSpan)time);
        }

        /// <summary>
        /// Временная метка в виде строки
        /// </summary>
        /// <returns></returns>
        private static string TimeToString(TimeSpan time)
        {
            string ret = string.Empty;

            if (time.Hours != 0)
                ret = time.Hours.ToString() + " часов";

            if (time.Minutes != 0)
            {
                if (ret != String.Empty)
                    ret += ", ";

                ret += time.Minutes.ToString() + " минут";
            }

            if (ret != String.Empty)
                ret += ", ";

            ret += time.Seconds.ToString() + " секунд";

            return ret;
        }

        #region Заголовки окон
        /// <summary>
        /// Заголовок окна настроек плагина
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <param name="pluginName">Имя плагина</param>
        /// <returns></returns>
        internal static string SettingsWindow_Header(Mode mode, string pluginName)
        {
            if (pluginName == null)
                throw new Exception("Имя плагина не определено.");

            string ret = "Настройки ";

            switch (mode)
            {
                case Mode.BASIC:
                    ret += "стадии";
                    break;
                case Mode.EXPERT:
                    ret += "плагина";
                    break;
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }

            ret += " \"" + pluginName + "\"";

            return ret;
        }

        /// <summary>
        /// Заголовок окна выполнения плагина
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <param name="pluginName">Имя плагина</param>
        /// <returns></returns>
        internal static string RunWindow_Header(Mode mode, string pluginName)
        {
            if (pluginName == null)
                throw new Exception("Имя плагина не определено.");

            string ret = "Выполнение ";

            switch (mode)
            {
                case Mode.BASIC:
                    ret += "стадии";
                    break;
                case Mode.EXPERT:
                    ret += "плагина";
                    break;
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }

            ret += " \"" + pluginName + "\"";

            return ret;
        }

        /// <summary>
        /// Заголовок окна результатов плагина
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <param name="pluginName">Имя плагина</param>
        /// <returns></returns>
        internal static string ResultWindow_Header(Mode mode, string pluginName)
        {
            if (pluginName == null)
                throw new Exception("Имя плагина не определено.");

            string ret = "Результаты ";

            switch (mode)
            {
                case Mode.BASIC:
                    ret += "стадии";
                    break;
                case Mode.EXPERT:
                    ret += "плагина";
                    break;
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }

            ret += " \"" + pluginName + "\"";

            return ret;
        }  
        #endregion

        /// <summary>
        /// Сообщение об ошибке в настройках плагина
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <param name="pluginName">Имя плагина</param>
        /// <returns></returns>
        internal static string SettingsWindow_Check_Error(Mode mode, string pluginName)
        {
            if (pluginName == null)
                throw new Exception("Имя плагина не определено.");

            switch (mode)
            {
                case Mode.BASIC:
                    return "Стадия <" + pluginName + "> некорректно настроена!";
                case Mode.EXPERT:
                    return "Плагин <" + pluginName + "> некорректно настроен!";
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }
        }
    }
}
