﻿using System;

using IA.Extensions;

namespace IA.MainForm.Messages
{
    /// <summary>
    /// Сообщения связанные с Хранилищами
    /// </summary>
    internal static class Storage
    {
        /// <summary>
        /// Сообщение при открытии Хранилища
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <returns></returns>
        internal static string Open(Mode mode)
        {
            string ret = "Открытие ";

            switch (mode)
            {
                case Mode.BASIC:
                    ret += "проекта";
                    break;
                case Mode.EXPERT:
                    ret += "Хранилища";
                    break;
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }

            return ret;
        }

        /// <summary>
        /// Сообщение при закрытии Хранилища
        /// </summary>
        /// <param name="mode">Режим работы основной формы приложения</param>
        /// <returns></returns>
        internal static string Close(Mode mode)
        {
            string ret = "Закрытие ";

            switch (mode)
            {
                case Mode.BASIC:
                    ret += "проекта";
                    break;
                case Mode.EXPERT:
                    ret += "Хранилища";
                    break;
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }
            return ret;
        }
    }
}
