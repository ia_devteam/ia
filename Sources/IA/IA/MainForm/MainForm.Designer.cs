﻿using System;
using System.Windows.Forms;

namespace IA.MainForm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.main_tools_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.main_progress_status_tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.main_tableLayoutPanel.SuspendLayout();
            this.main_tools_tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_tableLayoutPanel
            // 
            this.main_tableLayoutPanel.ColumnCount = 1;
            this.main_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.Controls.Add(this.main_tools_tableLayoutPanel, 0, 1);
            this.main_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.main_tableLayoutPanel.Name = "main_tableLayoutPanel";
            this.main_tableLayoutPanel.RowCount = 3;
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.main_tableLayoutPanel.Size = new System.Drawing.Size(1356, 732);
            this.main_tableLayoutPanel.TabIndex = 0;
            // 
            // main_tools_tableLayoutPanel
            // 
            this.main_tools_tableLayoutPanel.ColumnCount = 2;
            this.main_tools_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.main_tools_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tools_tableLayoutPanel.Controls.Add(this.main_progress_status_tableLayoutPanel, 1, 0);
            this.main_tools_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_tools_tableLayoutPanel.Location = new System.Drawing.Point(3, 33);
            this.main_tools_tableLayoutPanel.Name = "main_tools_tableLayoutPanel";
            this.main_tools_tableLayoutPanel.RowCount = 1;
            this.main_tools_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_tools_tableLayoutPanel.Size = new System.Drawing.Size(1350, 666);
            this.main_tools_tableLayoutPanel.TabIndex = 0;
            // 
            // main_progress_status_tableLayoutPanel
            // 
            this.main_progress_status_tableLayoutPanel.ColumnCount = 1;
            this.main_progress_status_tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_progress_status_tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_progress_status_tableLayoutPanel.Location = new System.Drawing.Point(63, 3);
            this.main_progress_status_tableLayoutPanel.Name = "main_progress_status_tableLayoutPanel";
            this.main_progress_status_tableLayoutPanel.RowCount = 3;
            this.main_progress_status_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.main_progress_status_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.main_progress_status_tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.main_progress_status_tableLayoutPanel.Size = new System.Drawing.Size(1284, 660);
            this.main_progress_status_tableLayoutPanel.TabIndex = 0;

            main_splitContainer = new SplitContainer();
            main_splitContainer.Dock = DockStyle.Fill;
            main_splitContainer.Panel1MinSize = 20;
            main_splitContainer.SplitterDistance = 40;
            main_splitContainer.Panel2.BackgroundImage = Properties.Resources.Wallpaper;
            main_splitContainer.Panel2.BackgroundImageLayout = ImageLayout.Zoom;

            main_show_hide_tableLayoutPanel = new TableLayoutPanel();
            main_show_hide_tableLayoutPanel.Dock = DockStyle.Fill;
            main_show_hide_tableLayoutPanel.RowCount = 1;
            main_show_hide_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
            main_show_hide_tableLayoutPanel.ColumnCount = 2;
            main_show_hide_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
            main_show_hide_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20));

            main_show_hide_button = new Button();
            main_show_hide_button.Dock = DockStyle.Fill;
            main_show_hide_button.Text = String.Empty;
            main_show_hide_button.Image = Properties.Resources.MainForm_Button_Hide;
            main_show_hide_button.TextImageRelation = TextImageRelation.Overlay;

            main_pluginList_splitContainer = new SplitContainer();
            main_pluginList_splitContainer.Dock = DockStyle.Fill;
            main_pluginList_splitContainer.Orientation = Orientation.Horizontal;


            main_tasks_log_splitContainer = new SplitContainer();
            main_tasks_log_splitContainer.Dock = DockStyle.Fill;
            main_tasks_log_splitContainer.Orientation = Orientation.Horizontal;

            statusControl = new IA.MainForm.Controls.StatusControl();
            statusControl.Dock = DockStyle.Fill;

            pluginListControl = new IA.Controls.PluginListControl();
            pluginListControl.Dock = DockStyle.Fill;
            pluginListControl.PluginClickEvent += this.Plugin_CurrentChoosedPluginChanged_EventHandler;
            pluginListControl.LevelChangedEvent += this.Plugin_CurrentRootPluginChanged_EventHandler;
            pluginListControl.PluginCheckedChangedEvent += this.Plugin_DependencyMultiRunConditionChanged_EventHandler;
            pluginListControl.PluginSettingsEvent += this.Plugin_SettingsWindow_Show_ActionHandler;
            pluginListControl.PluginRunEvent += this.Plugin_Run_ActionHandler;
            pluginListControl.PluginReportEvent += this.Plugin_Report_Generate_ActionHandler;
            pluginListControl.PluginResultEvent += this.Plugin_ResultWindow_Show_ActionHandler;

            logControl = new IA.Controls.LogControl();
            logControl.Dock = DockStyle.Fill;

            workspaceControl = new IA.Controls.WorkspaceControl();
            workspaceControl.Dock = DockStyle.Fill;

            menuControl = new IA.Controls.MenuControl();
            menuControl.Anchor = AnchorStyles.Left | AnchorStyles.Right;

            toolsControl = new IA.Controls.ToolsControl();
            toolsControl.Dock = DockStyle.Fill;
            toolsControl.LayoutStyle = ToolStripLayoutStyle.VerticalStackWithOverflow;

            infoControl = new IA.Controls.InfoControl();
            infoControl.Anchor = AnchorStyles.Left | AnchorStyles.Right;

            main_tableLayoutPanel.Controls.Add(menuControl, 0, 0);
            main_tasks_log_splitContainer.Panel2.Controls.Add(logControl);
            main_progress_status_tableLayoutPanel.Controls.Add(main_splitContainer, 0, 0);
            main_show_hide_tableLayoutPanel.Controls.Add(main_show_hide_button, 1, 0);
            main_pluginList_splitContainer.Panel2.Controls.Add(main_tasks_log_splitContainer);
            main_pluginList_splitContainer.Panel1.Controls.Add(pluginListControl);
            main_show_hide_tableLayoutPanel.Controls.Add(main_pluginList_splitContainer, 0, 0);
            main_tools_tableLayoutPanel.Controls.Add(toolsControl, 0, 0);
            main_splitContainer.Panel1.Controls.Add(main_show_hide_tableLayoutPanel);
            main_tableLayoutPanel.Controls.Add(infoControl, 0, 2);
            main_splitContainer.Panel2.Controls.Add(workspaceControl);
            main_progress_status_tableLayoutPanel.Controls.Add(statusControl, 0, 2);

            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1356, 732);
            this.Controls.Add(this.main_tableLayoutPanel);
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_Closing);
            this.main_tableLayoutPanel.ResumeLayout(false);
            this.main_tools_tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

    }
}

