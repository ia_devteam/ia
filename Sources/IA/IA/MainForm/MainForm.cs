﻿/*
 * Главный интерфейс
 * Файл MainForm.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Лошкарёв, Юхтин
 * 
 * Основная форма приложения
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Controls;
using IA.Objects;
using IA.Plugin;

namespace IA.MainForm
{
    using Plugin = Handler;
    using AllPlugins = HandlersInitial;

    using Workflow = IA.Config.Objects.Workflows.SpecificWorkflow;
    using System.IO;
    using System.Reflection;
    using Sql;
    using Sql.DatabaseConnections;
    using IA.Controls;
    using IAUtils;
    using IA.Controls.MainSettings;
    using Controls;/// <summary>
                   /// Основная форма приложения
                   /// </summary>
    public partial class MainForm : Form
    {
        #region Константы

        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Основная форма";

        /// <summary>
        /// Версия базы данных для корректной работы приложения
        /// </summary>
        private const uint DatabaseVersion = 8;


        #endregion

        #region Свойства - текущие нестройки, необходимые всему приложению
        private User currentUser = null;
        /// <summary>
        /// Текущий пользователь
        /// </summary>
        internal User CurrentUser
        {
            get
            {
                if (Settings.AutonomyMode)
                    return User.GetOfflineUser();
                return currentUser;
            }
            set
            {
                //Если ничего не поменялось,  ничего не делаем
                if (currentUser == value)
                    return;

                //Сохраняем новое значение
                IACommon.CurrentUser = currentUser = value;

                //Сохраняем настройки для будущего запуска приложения
                IA.Settings.UserID = value == null ? 0 : value.ID;

                //Если пользователь не выбран
                if (value == null)
                    this.CurrentProject = null;

                //Обновляем информацию на информационной панели
                infoControl.Items[InfoType.USER.FullName()].Text = InfoType.USER.Description() + ": " + ((value == null) ? "Аноним" : value.Fullname);

                //Меняем состояние приложения
                this.State = value == null ? State.STORAGE_CLOSED_USER_NOT_AUTHORIZED : State.STORAGE_CLOSED;
            }
        }

        private Project currentProject = null;
        /// <summary>
        /// Текущий проект
        /// </summary>
        internal Project CurrentProject
        {
            get
            {
                return currentProject;
            }
            set
            {
                //Если ничего не поменялось,  ничего не делаем
                if (currentProject == value)
                    return;

                //Сохраняем новое значение
                currentProject = value;

                //Если проект не выбран
                if (value == null)
                    this.CurrentMaterials = null;

                //Обновляем информацию на информационной панели
                infoControl.Items[InfoType.PROJECT.FullName()].Text = InfoType.PROJECT.Description() + ": " + ((value == null) ? "не выбран" : value.Name);
            }
        }

        private Materials currentMaterials = null;
        /// <summary>
        /// Текущий набор материалов
        /// </summary>
        internal Materials CurrentMaterials
        {
            get
            {
                return currentMaterials;
            }
            set
            {
                //Если ничего не поменялось,  ничего не делаем
                if (currentMaterials == value)
                    return;

                //Сохраняем новое значение
                currentMaterials = value;

                //Если материалы не выбраны
                if (value == null)
                {
                    //Текущее Хранилище не задано
                    this.CurrentStorage = null;

                    //Текущий рабочий процес не задан
                    this.CurrentWorkflow = null;
                }
                else
                    this.CurrentProject = Project.GetByID(value.ProjectID);

                //Обновляем информацию на информационной панели
                infoControl.Items[InfoType.MATERIALS.FullName()].Text = InfoType.MATERIALS.Description() + (value == null ? ": не выбраны" : " ver. " + value.Version);
            }
        }

        private Storage currentStorage = null;
        /// <summary>
        /// Текущиее Хранилище
        /// </summary>
        internal Storage CurrentStorage
        {
            get
            {
                return currentStorage;
            }
            set
            {
                //Если ничего не поменялось,  ничего не делаем
                if (currentStorage == value)
                    return;

                //Если Хранилище не выбрано
                if (value == null || Settings.AutonomyMode)
                {
                    //Текущий выбранный и корневой плагин не заданы
                    this.CurrentChoosedPlugin = null;
                    this.CurrentRootPlugin = null;
                }
                else
                {
                    this.CurrentMaterials = Materials.GetByID(value.MaterialsID);
                }

                //Сохраняем новое значение
                currentStorage = value;

                //Обновляем информацию на информационной панели
                infoControl.Items[InfoType.STORAGE.FullName()].Text = InfoType.STORAGE.Description() + ": " + ((value == null) ? "не выбрано" : value.DateTime.ToString());
                infoControl.Items[InfoType.WORKDIRECTORY.FullName()].Text = InfoType.WORKDIRECTORY.Description() + ": " + ((value == null || value.WorkDirectory == null) ? "не выбрана" : value.WorkDirectory.Path);
            }
        }

        private Plugin currentChoosedPlugin = null;
        /// <summary>
        /// Текущий выбранный плагин в списке плагинов
        /// </summary>
        internal Plugin CurrentChoosedPlugin
        {
            get
            {
                return currentChoosedPlugin;
            }

            set
            {
                //Если ничего не поменялось,  ничего не делаем
                if (currentChoosedPlugin == value)
                    return;

                //Сохраняем новое значение
                currentChoosedPlugin = value;
            }
        }

        private Plugin currentRootPlugin = null;
        /// <summary>
        /// Текущий корневой плагин в списке плагинов
        /// </summary>
        internal Plugin CurrentRootPlugin
        {
            get
            {
                return currentRootPlugin;
            }

            set
            {
                //Если ничего не поменялось,  ничего не делаем
                if (currentRootPlugin == value)
                    return;

                //Сохраняем новое значение
                currentRootPlugin = value;
            }
        }

        private Workflow currentWorkflow = null;
        internal Workflow CurrentWorkflow
        {
            get
            {
                return currentWorkflow;
            }
            set
            {
                //Если ничего не поменялось,  ничего не делаем
                if (currentWorkflow == value)
                    return;

                //Сохраняем новое значение
                currentWorkflow = value;
            }
        }
        #endregion

        #region Элементы управления

        #region Таблицы
        private TableLayoutPanel main_tableLayoutPanel = null;
        private TableLayoutPanel main_tools_tableLayoutPanel = null;
        private TableLayoutPanel main_progress_status_tableLayoutPanel = null;
        private TableLayoutPanel main_show_hide_tableLayoutPanel = null;
        #endregion

        #region Разделители
        private SplitContainer main_splitContainer = null;
        private SplitContainer main_pluginList_splitContainer = null;
        private SplitContainer main_tasks_log_splitContainer = null;
        #endregion

        #region Кнопки
        private Button main_show_hide_button = null;
        #endregion

        /// <summary>
        /// Элемент упарвления - Меню
        /// </summary>
        IA.Controls.MenuControl menuControl = null;

        /// <summary>
        /// Элемент управления - Панель инструментов
        /// </summary>
        IA.Controls.ToolsControl toolsControl = null;

        /// <summary>
        /// Элемент управления - Список плагинов
        /// </summary>
        public PluginListControl pluginListControl = null;

        /// <summary>
        /// Элемент управления - Окно тасков
        /// </summary>
        Controls.TasksControl tasksControl = null;

        /// <summary>
        /// Элемент управления - Лог
        /// </summary>
        LogControl logControl = null;

        /// <summary>
        /// Элемент управления - Окно прогресса
        /// </summary>
        Controls.ProgressControl progressControl = null;

        /// <summary>
        /// Элемент упарвления - Рабочее пространство
        /// </summary>
        IA.Controls.WorkspaceControl workspaceControl = null;

        /// <summary>
        /// Элемент управления - Строка статуса
        /// </summary>
        Controls.StatusControl statusControl = null;

        /// <summary>
        /// Элемент управления - Информационная панель
        /// </summary>
        InfoControl infoControl = null;
        #endregion

        private Mode mode = Mode.NO;
        /// <summary>
        /// Текущий режим работы приложения
        /// </summary>
        private Mode Mode
        {
            get
            {
                return mode;
            }
            set
            {
                //Если ничего не поменялось, ничего не делаем
                if (mode == value)
                    return;

                //Сохраняем переданное значение
                mode = value;

                //Меняем режим работы приложения
                this.MainForm_SetMode(value);
            }
        }

        private State state = State.NO;
        /// <summary>
        /// Текущее состояние приложения
        /// </summary>
        private State State
        {
            get
            {
                return state;
            }
            set
            {
                //Если ничего не поменялось, ничего не делаем
                if (state == value)
                    return;

                //Сохраняем переданное значение
                state = value;

                if (Settings.AutonomyMode && (
                    state == State.STORAGE_CLOSED
                    || state == State.STORAGE_CLOSED_USER_NOT_AUTHORIZED
                    //|| state == State.STORAGE_CLOSED_USER_ACTION //?
                    ))
                    state = State.STORAGE_CLOSED_AUTONOMYMODE;


                //Меняем состояние приложения
                this.MainForm_SetState(state);
            }
        }

        /// <summary>
        /// Хранитель значения дистанции сплиттера
        /// </summary>
        private int splitterDistanceSaver = -1;

        /// <summary>
        /// Всевозможные действия для кнопок и пунктов меню
        /// </summary>
        private Dictionary<string, Action> actions = new Dictionary<string, Action>();

        /// <summary>
        /// Иконка в трее
        /// </summary>
        private TrayIcon trayIcon = null;

        /// <summary>
        /// Сетевой ресурс - Сервер проектов
        /// </summary>
        private Network.NetworkResource ProjectsServerNetworkResource = null;

        public static Form thisForm;

        public static int currentProgress;

        /// <summary>
        /// Конструктор
        /// </summary>
        public MainForm()
        {
            SqlConnector.MainForm = thisForm = this;

            currentProgress = 0;
            InitializeComponent();

            //Добавляем обработчик
            this.Shown += (sender, e) => this.MainForm_Shown();

            IAUtils.IACommon.StorageDoubleClick = Storage_Open;
            IAUtils.IACommon.StoragePackStart = Storage_Pack_Start;
            IAUtils.IACommon.StorageUnloadClick = Storage_Unload;
        }

        /// <summary>
        /// Появилось соединение с базой данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Проверяем версию БД
            Database_Version_Check();

            if (this.CurrentUser == null)
            {
                //Меняем состояние приложения
                //this.State = State.STORAGE_CLOSED_USER_NOT_AUTHORIZED;

                //Вспоминаем под кем мы входили в последний раз
                Database_User_Login(IA.Settings.UserID, true);
            }
            else
            {
                //Производим вход, под текущеим пользователем
                Database_User_Login(this.CurrentUser.ID, true);

                //Меняем состояние приложения
                this.State = State.STORAGE_CLOSED;
            }

            // теперь с окном ожидания можно работать, не боясь, что оно кого-то перекроет 
            WaitWindow.parent = this;
        }

        /// <summary>
        /// Пропало соединение с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Переключаемся в режим анонимного пользователя
            this.CurrentUser = null;
        }

        #region Главная форма
        /// <summary>
        /// Формирвоание основной архитектуры главной формы
        /// </summary>
        /// <returns></returns>
        private void MainForm_Initialization()
        {
            //Устанавливаем иконку приложения
            this.Icon = Icon.FromHandle(Properties.Resources.Application_Icon.GetHicon());

            splitterDistanceSaver = 40;

            main_show_hide_button.Click += (sender, e) => this.Main_Show_Hide_Button_Click();

            //Инициализация элемента управления - Окно прогресса
            progressControl = new IA.MainForm.Controls.ProgressControl() { Dock = DockStyle.Fill };

            Monitor.Status.Register(statusControl);

            //Инициализация элемента управления - Окно тасков
            tasksControl = new IA.MainForm.Controls.TasksControl() { Dock = DockStyle.Fill };
            tasksControl.PluginProgressChangedEvent += progressControl.Elements[ProgressControl.ProgressType.PLUGIN].SetProgress;
            Monitor.Tasks.Register(tasksControl);

            //Инициализация элемента управления - Лог
            logControl.LineClickedEvent += delegate (string line)
                {
                    if (line != null)
                        statusControl.ShowMessage(Monitor.Status.MessageType.TEMPORARY, "Двойное нажатие на строчку лога скопирует её в буфер обмена.");
                };
            logControl.LineDoubleClickedEvent += delegate (string line)
                {
                    if (line != null)
                    {
                        //Добавляем сообщение в буфер обмена
                        Clipboard.SetText(line);
                        statusControl.ShowMessage(Monitor.Status.MessageType.TEMPORARY, "Строка лога скопирована в буфер обмена.");
                    }
                };
            Monitor.Log.Register(logControl);

            //Инициализация иконки в трее
            trayIcon = new TrayIcon();
            trayIcon.SetMouseClickEventHandler(delegate (object sender, MouseEventArgs e)
                                                    {
                                                        if (e.Button == System.Windows.Forms.MouseButtons.Left)
                                                            this.Visible = !this.Visible;
                                                    });

            //Заполняем список всевозможных действий и устанавливаем их в значения по умолчанию
            Actions_Initialization();

            main_progress_status_tableLayoutPanel.Controls.Add(progressControl, 0, 1);

            //Подгружаем плагины
            this.LoadPluginsWithWaitWindow();

            main_tasks_log_splitContainer.Panel1.Controls.Add(tasksControl);

        }

        /// <summary>
        /// Обработчик события - Что необходимо сделать при первом отображении формы
        /// </summary>
        private void MainForm_Shown()
        {
            //Загружаем информацию из конфигурационного файла
            this.LoadConfigWithWaitWindow();

            //Инициализация всех элементов управления формы
            this.MainForm_Initialization();

            //Режим работы приложения по умолчанию
            this.Mode = Mode.EXPERT;

            string message = String.Empty;

            //Если это первый запуск приложения
            if (this.MainSettings_IsFirstRun())
            {
                //Формируем сообщение
                message = new string[]
                {
                    "Это первый запуск приложения <" + Constants.Application.ShortName + ">.",
                    "Пожалуйста, произведите настройку приложения.",
                    "Укажите параметры соединения с SQL сервером и сервером проектов."
                }.ToMultiLineString();
            }
            else
            {
                //Проверяем настройки приложения
                if (!this.MainSettings_Check())
                {
                    //Формируем сообщение
                    message = new string[]
                    {
                    "Приложение настроено некорректно.",
                    "Пожалуйста, произведите корректную настройку приложения.",
                    "Укажите параметры соединения с SQL сервером и сервером проектов."
                    }.ToMultiLineString();
                }
            }

            if (message != string.Empty)
            {
                //Если приложение настроено некорректно, отображаем предупреждение
                Monitor.Log.Warning(ObjectName, message, true);

                //Отображаем окно с настройками приложения
                this.MainSettings_Show(false);
            }
        }

        /// <summary>
        /// Обработчик события - основная форма закрывается
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Closing(object sender, FormClosingEventArgs e)
        {
            //Переменная для формирования ответа пользователя
            DialogResult answer = DialogResult.No;
            try
            {
                if (MessageBox.Show("Вы уверены, что хотите завершить работу?", "Завершить", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    e.Cancel = true;
                    return;
                }

                //Если мы находимся в процессе выполнения чего либо, предупредить! 
                if (this.State == State.STORAGE_OPENED_COMPUTER_ACTION)
                {
                    if (!Plugin_Stop(null, null, null))
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    if (currentStorage != null && !currentStorage.Controller.Storage.isClosed)
                    {
                        //Закрываем Хранилище/проект перед выходом
                        answer = MessageBox.Show("Текущий проект будет закрыт, вы уверены?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        //Если пользователь, не согласен, ничего не делаем
                        if (answer != DialogResult.Yes)
                        {
                            e.Cancel = true;
                            return;
                        }

                        if (!Settings.AutonomyMode)
                            answer = MessageBox.Show("Сохранить изменения внесённые в проект?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                }
                //Если Хранилище (Проект) не было закрыто

                switch (this.Mode)
                {
                    case Mode.BASIC:
                        {
                            //Закрываем проект и загружаем (если требуется) изменения на сервер
                            if (!this.Project_Close(this.CurrentStorage, answer == DialogResult.Yes))
                                throw new Exception("Проект не был закрыт корректно.");

                            break;
                        }
                    case Mode.EXPERT:
                        {
                            //Закрываем и загружаем (если требуется) Хранилище на сервер
                            if (!this.Storage_Close(this.CurrentStorage, answer == DialogResult.Yes))
                                return;

                            //Текущий проект неизвестен
                            this.CurrentProject = null;

                            break;
                        }
                    case Mode.NO:
                    default:
                        throw new Exception(Exceptions.UNKNOWN_MODE.Description());
                }

                //Сохраняем все изменённые настройки приложения
                Settings.SaveAll();

                trayIcon.Hide();

                // очистим временную директорию
                //Отображаем окно ожидания
                if (!Settings.AutonomyMode)
                    WaitWindow.Show(
                        delegate (WaitWindow window)
                        {
                            string path = Path.Combine(IA.Settings.CommonWorkDirectoryPath, IA.Settings.PrivateWorkDirectoryPath);
                            try
                            {
                                if (Directory.Exists(path))
                                    Directory.Delete(path, true);
                            }
                            catch (System.IO.IOException)
                            {
                                MessageBox.Show("Не удается очистить рабочую директорию. Один или несколько файлов открыты в другой программе. Очистка не завершена. ");
                                Monitor.Log.Error("Очистка рабочей папки", "Не удается очистить рабочую директорию. Один или несколько файлов открыты в другой программе. Очистка не завершена. ");
                            }
                        },
                        "Очистка временной папки"
                    );

            }
            catch (Exception ex)
            {
                //Формируем исключение
                //throw new Exception("Ошибка в ходе завершения работы приложения.", ex);
            }
        }

        /// <summary>
        /// Задать режим работы приложения
        /// </summary>
        /// <param name="mode">Режим работы</param>
        private void MainForm_SetMode(Mode mode)
        {
            //Меняем заголовок окна
            this.Text = Constants.Application.ShortName + " [Режим работы: " + mode.Description() + "]";

            //В зависимости от текущего режима работы, отображаем элементы управления
            switch (mode)
            {
                case Mode.BASIC:
                    {
                        //Не отображаем список плагинов
                        main_pluginList_splitContainer.Panel1Collapsed = true;

                        //Не отображаем список тасков
                        main_tasks_log_splitContainer.Panel1Collapsed = true;
                        break;
                    }
                case Mode.EXPERT:
                    {
                        //Отображаем список плагинов
                        main_pluginList_splitContainer.Panel1Collapsed = false;

                        //Отображаем список тасков
                        main_tasks_log_splitContainer.Panel1Collapsed = false;
                        break;
                    }
                case Mode.NO:
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }

            //Формируем меню
            Menu_SetMode(mode);

            //Формируем набор инструментов
            Tools_SetMode(mode);

            //Формируем информационную панель
            Info_SetMode(mode);

            //Формируем панель прогресса
            Progress_SetMode(mode);

            //Формируем иконку в трее
            Tray_Icon_SetMode(mode);
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку "Изменить режим работы"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="action"></param>
        /// <param name="mode">Режим работы</param>
        private void MainForm_SetMode_ActionHandler(object sender, Action action, Mode mode)
        {
            try
            {
                switch (mode)
                {
                    case Mode.EXPERT:
                        {
                            //Отображаем предупреждение
                            switch (MessageBox.Show("Данный режим предназначен только для опытных пользователей. Возврат к основному режиму будет невозможен в рамках текущего сеанса рыботы с приложением. Вы уверены?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                            {
                                case DialogResult.Yes:
                                    {
                                        //Если есть открытые Хранилища, значит мы работаем в основном режиме
                                        if (this.CurrentStorage != null)
                                        {
                                            //Рабочий процесс более не актуален
                                            this.CurrentWorkflow = null;

                                            //Буфер плагинов для обработки более не актуален
                                            this.workflowPluginsBuffer.Clear();

                                            //Закрываем Хранилище в фоновом режиме и загружаем его на сервер
                                            this.Storage_Close(this.CurrentStorage, true, true);

                                            //Получаем все Хранилища, созданные в основном режиме
                                            List<Storage> userModeStorages = Storage.GetByMaterials(this.CurrentMaterials.ID).Where(s => s.Mode == Storage.enMode.BASIC).ToList();

                                            //Открываем Хранилище, созданное в основном режиме, с максимальным идентификатором в фоновом режиме
                                            this.Storage_Open(userModeStorages.First(s => s.ID == userModeStorages.Max(st => st.ID)), true);
                                        }

                                        break;
                                    }
                                case DialogResult.No:
                                    return;
                            }
                            break;
                        }
                    case Mode.BASIC:
                        throw new Exception("Недопустимая ситуация. Попытка перейти из режима <" + Mode.EXPERT.Description() + "> в режим <" + Mode.BASIC.Description() + ">.");
                    default:
                        throw new Exception(Exceptions.UNKNOWN_MODE.Description());
                }

                //Устанавливаем требуемый режим работы
                this.Mode = mode;
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// запустить утилиту из контекстного меню
        /// </summary>
        /// <param name="FileName">путь к программе</param>
        private void RunUtil(string FileName)
        {
            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = "cmd";
            cmdStartInfo.Arguments = "/c " + FileName;
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = cmdStartInfo;
            p.Start();
        }

        /// <summary>
        /// Задать состояние приложения
        /// </summary>
        /// <param name="state">Состояние</param>
        private void MainForm_SetState(State state)
        {
            //Обновляем действия
            this.Actions_Refresh();

            switch (state)
            {
                case State.STORAGE_CLOSED_USER_NOT_AUTHORIZED:
                case State.STORAGE_CLOSED:
                    {
                        pluginListControl.Reset();
                        pluginListControl.IsBlocked = true;
                        pluginListControl.IsEnabled = false;
                        tasksControl.Clear();
                        statusControl.Clear();

                        this.progressControl.Reset();

                        // запускаем отображение выбора проекта-материалов-хранилища в рабочей области
                        Show_Projects_ActionHandler(null, null);

                        break;
                    }
                case State.STORAGE_CLOSED_AUTONOMYMODE:
                    {
                        pluginListControl.Reset();
                        pluginListControl.IsBlocked = true;
                        pluginListControl.IsEnabled = false;

                        tasksControl.Clear();
                        statusControl.Clear();

                        this.progressControl.Reset();

                        ShowSelectAutonomyStoragePanel();

                        break;
                    }
                case State.STORAGE_CLOSED_USER_ACTION:
                    {
                        break;
                    }
                case State.STORAGE_OPENED:
                    {
                        pluginListControl.IsBlocked = false;
                        pluginListControl.IsEnabled = true;

                        statusControl.Clear();
                        workspaceControl.Clear();

                        break;
                    }
                case State.STORAGE_OPENED_USER_ACTION:
                case State.STORAGE_OPENED_COMPUTER_ACTION:
                    {
                        pluginListControl.IsBlocked = true;
                        pluginListControl.IsEnabled = true;

                        break;
                    }
                case State.NO:
                default:
                    throw new Exception(Exceptions.UNKNOWN_STATE.Description());
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку "О программе"
        /// </summary>
        /// <param name="sender">Отправитель</param>
        /// <param name="action">Действие</param>
        private void MainForm_About_ActionHandler(object sender, Action action)
        {
            try
            {
                //Отображаем диалог с информацией о приложении
                using (
                    Dialogs.AboutDialog aboutDialog = new Dialogs.AboutDialog()
                    {
                        FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog,
                        MinimizeBox = false,
                        MaximizeBox = false,
                        StartPosition = FormStartPosition.CenterParent,
                    }
                )
                    aboutDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        #endregion

        /// <summary>
        /// Показать\скрыть информацию о плагинах, прогресс, лог
        /// </summary>
        private void Main_Show_Hide_Button_Click()
        {
            try
            {
                //Узнаём, нужно ли спрятать или показать панель?
                bool needToHide = main_splitContainer.SplitterDistance > 20;

                //Если прячем, то запоминаем дистанцию сплиттера
                if (needToHide)
                    splitterDistanceSaver = main_splitContainer.SplitterDistance;

                //Делаем необходимое действие
                main_splitContainer.SplitterDistance = /*main_splitContainer.Panel1MinSize =*/ needToHide ? 20 : splitterDistanceSaver;
                main_splitContainer.IsSplitterFixed = needToHide;
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Показать\\Скрыть", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        #region Основные настройки приложения
        /// <summary>
        /// Заданы ли настройки? Если нет, значит это первый запуск приложения.
        /// </summary>
        /// <returns></returns>
        private bool MainSettings_IsFirstRun()
        {
            try
            {
                return String.IsNullOrEmpty(IA.Settings.ProjectsServer.Name) &&
                            String.IsNullOrEmpty(IA.Settings.ProjectsServer.Login) &&
                                String.IsNullOrEmpty(IA.Settings.ProjectsServer.Password) &&
                                    String.IsNullOrEmpty(IA.Settings.SQLServer.Name) &&
                                        String.IsNullOrEmpty(IA.Settings.SQLServer.Login) &&
                                            String.IsNullOrEmpty(IA.Settings.SQLServer.Password);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при проверке наличия основных настроек приложения.", ex);
            }
        }

        /// <summary>
        /// Применить основные настройки приложения
        /// </summary>
        private void MainSettings_Apply()
        {
            try
            {
                if (Settings.AutonomyMode)
                    return;

                this.ProjectsServerNetworkResource = new Network.NetworkResource(IA.Settings.ProjectsServer.Name,
                                                                                IA.Settings.ProjectsServer.Login,
                                                                                    IA.Settings.ProjectsServer.Password);

                IA.Settings.SQLServer.Set(IA.Settings.SQLServer.Name,
                                            IA.Settings.SQLServer.Login,
                                                IA.Settings.SQLServer.Password,
                                                    IA.Settings.SQLServer.IsWindowsAuthentication);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при применении основных настроек приложения.", ex);
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню отображения основных настроек приложения
        /// </summary>
        private void MainSettings_Show_ActionHandler(object sender, Action action)
        {
            try
            {
                //Отображаем настройки
                this.MainSettings_Show(true);
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }

        }

        /// <summary>
        /// Отобразить основные настройки приложения
        /// </summary>
        private void MainSettings_Show(bool ContainsCancel)
        {
            try
            {
                //Меняем состояние приложения
                this.State = State.STORAGE_CLOSED_USER_ACTION;

                //Создаём элемент управления с основными настройками
                MainSettingsControl settings = new MainSettingsControl(MainSettingsControl.SettingsElements.projectsServerSettingsSubControl |
                                    MainSettingsControl.SettingsElements.sqlServerSettingsSubControl | MainSettingsControl.SettingsElements.otherSettingsControl);
                ((Control)settings).Text = IA.Actions.Global.SETTINGS.Description();

                //Очищаем рабочее пространоство
                workspaceControl.Clear();

                Dictionary<string, EventHandler> buttons = new Dictionary<string, EventHandler>
                {
                    { "Сохранить",  (EventHandler)((sender, e) => this.MainSettings_Save_Click(settings)) },
                    { "Проверить",  (EventHandler)((sender, e) => this.MainSettings_SingleCheck_Click(settings)) }
                };
                if (ContainsCancel)
                {
                    buttons.Add("Отмена", delegate (object sender, EventArgs e)
                    {
                        //Меняем состояние приложения
                        this.State = State.STORAGE_CLOSED;
                    });
                }


                workspaceControl.SetControl(settings);
                workspaceControl.SetButtons(buttons);
            }
            catch (Exception ex)
            {
                //Меняем состояние приложения
                this.State = State.STORAGE_CLOSED;

                //Формируем исключение
                throw new Exception("Ошибка при отображении основных настроек приложения.", ex);
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню разовой проверки настроек
        /// </summary>
        /// <param name="mainSettingsControl">Окно основных настроек приложения</param>
        private void MainSettings_SingleCheck_Click(IA.Controls.MainSettings.MainSettingsControl mainSettingsControl)
        {
            try
            {
                string message;

                //Провера на разумность
                if (mainSettingsControl == null)
                    throw new Exception("Элемент управления основных настроек приложения не определён.");

                //Проверяем настройки и в зависимости от результата выводим сообщение
                if (mainSettingsControl.Check(out message))
                    Monitor.Log.Information(MainForm.ObjectName, message, true);
                else
                    Monitor.Log.Error(MainForm.ObjectName, message, true);

            }
            catch (Exception)
            {
                //Обрабатываем исключение
            }
        }

        /// <summary>
        /// Проверка настроек приложения
        /// </summary>
        /// <returns>Результат проверки настроек приложения</returns>
        private bool MainSettings_Check()
        {
            try
            {
                if (!Settings.AutonomyMode)
                {
                    this.MainSettings_Apply();

                    if (!this.TestProjectsServerConnectionWithWaitWindow())
                        return false;

                    if (!this.TestDatabaseConnectionWithWaitWindow())
                        return false;

                    SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);

                }
                this.State = State.STORAGE_CLOSED_USER_NOT_AUTHORIZED;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при проверке основных настроек приложения.", ex);
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню сохранения основных настройки приложения
        /// </summary>
        /// <param name="mainSettingsControl">Окно основных настроек приложения</param>
        private void MainSettings_Save_Click(IA.Controls.MainSettings.MainSettingsControl mainSettingsControl)
        {
            try
            {
                if (mainSettingsControl == null)
                    throw new Exception("Элемент управления основных настроек приложения не определён.");

                mainSettingsControl.Save();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Сохранение основных настроек приложения", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
            finally
            {
                //По окончанию принудительно меняем состояние формы
                if (this.MainSettings_Check())
                {
                    this.State = (this.CurrentUser != null) ? State.STORAGE_CLOSED : State.STORAGE_CLOSED_USER_NOT_AUTHORIZED;
                }
            }
        }
        #endregion

        #region База данных
        /// <summary>
        /// Проверка версии базы данных, с которой работает приложение.
        /// Предполагается, что соединение с базой данных уже установлено.
        /// </summary>
        /// <returns></returns>
        private void Database_Version_Check()
        {
            try
            {
                //Текущая версия БД
                uint currentDatabaseVersion;

                //Если версия базы данных для корректной работы не совпадает с текущей
                if (!((IADatabaseConnection)SqlConnector.IADB.DatabaseConnection).CheckVersion(MainForm.DatabaseVersion, out currentDatabaseVersion))
                    throw new Exception("Текущая версия базы данных не совпадает с версией для корректной работы приложения.\n" +
                                            "Текущая версия базы данных: " + currentDatabaseVersion + ".\n" +
                                                "Версия базы данных для корректной работы: " + MainForm.DatabaseVersion + ".");
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleMessage("Ошибка при проверке версии базы данных.", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));

                //Принудительно завершаем приложение
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню выбора пользователя
        /// </summary>
        private void Database_User_Login_ActionHandler(object sender, Action action)
        {
            try
            {
                //Отображаем окно авторизайии
                this.Database_User_Login(0);
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Войти в приложение под указанным пользователем.
        /// Предполагается, что соединенеие с базой данных уже установлено.
        /// </summary>
        /// <param name="userID">Текущий идентификтор пользователя</param>
        /// <param name="isNoAnonymous">Давать возможность зайти как аноним?</param>
        private void Database_User_Login(uint userID, bool isNoAnonymous = false)
        {
            try
            {
                if (Settings.AutonomyMode)
                {
                    CurrentUser = User.GetOfflineUser();
                    return;
                }

                //Если пользователь не выбран показываем окно авторизации
                if (userID == 0)
                {
                    using (IA.Dialogs.AuthorizationDialog dialog =
                        new IA.Dialogs.AuthorizationDialog()
                        {
                            FormBorderStyle = FormBorderStyle.FixedDialog,
                            Icon = this.Icon,
                            MaximizeBox = false,
                            MinimizeBox = false,
                            StartPosition = FormStartPosition.CenterParent,
                            User = this.CurrentUser
                        })
                    {
                        //Показываем окно до тех пор, пока пользователь не будет указан
                        while (dialog.ShowDialog(this) != DialogResult.OK)
                        {
                            if (!isNoAnonymous)
                                return;

                            //Отображаем ошибку
                            Monitor.Log.Error(ObjectName, "Пользователь не указан. Требуется указать пользователя.", true);
                        }

                        //Сохраняем выбранного пользователя
                        this.CurrentUser = dialog.User;
                    }
                }
                //Если пользователь выбран, просто получаем его данные из БД
                else
                {
                    //Получаем объект пользователя для входа
                    try
                    {
                        User userToLogin = User.GetByID(userID);

                        //Если пользователь удалён
                        if (userToLogin.IsDeleted)
                        {
                            //Отображаем предупреждение
                            Monitor.Log.Warning(MainForm.ObjectName, "Пользователь <" + userToLogin.Fullname + "> был удалён. Требуется произвести вход под другим пользователем.", true);

                            //Показывам окно авторизации
                            this.Database_User_Login(0, isNoAnonymous);

                            return;
                        }

                        //Задаём текущего пользователя
                        this.CurrentUser = userToLogin;
                    }
                    catch (UserNotFoundException)
                    {
                        this.Database_User_Login(0, isNoAnonymous);
                    }
                }
            }
            //Если по каким-то причинам пользователя определить не удалось
            catch (Exception ex)
            {
                //Текущий пользователь неизвестен
                CurrentUser = null;

                //Обрабатываем исключение
                ex.HandleMessage("Ошибка авторизации.", (message) => Monitor.Log.Error(MainForm.ObjectName, message, true));
            }
        }
        #endregion

        #region Действия
        /// <summary>
        /// Инициализация всеовзможных действий
        /// </summary>
        private void Actions_Initialization()
        {
            //Очищаем список действий
            actions.Clear();

            //Серверные операции с проектами
            EnumLoop<Project.ServerOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Project.ServerOperation.OPEN:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Project_Button_Open, ActionHandler = this.Project_Open_ActionHandler });
                        break;
                    case Project.ServerOperation.CLOSE:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Project_Button_Close, ActionHandler = this.Project_Close_ActionHandler });
                        break;
                    default:
                        throw new Exception(Project.Exceptions.UNKNOWN_SERVER_OPERATION.Description());
                }
            });

            //Пользовательские операции с проектами
            EnumLoop<Project.UserOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Project.UserOperation.GET_REPORTS:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Project_Button_Reports, ActionHandler = this.Project_Report_Generate_ActionHandler });
                        break;
                    default:
                        throw new Exception(Project.Exceptions.UNKNOWN_USER_OPERATION.Description());
                }
            });

            //Серверные операции с Хранилищем
            EnumLoop<Storage.ServerOperation>.ForEach(operation =>
           {
               switch (operation)
               {
                   //case Storage.ServerOperation.DOWNLOAD:
                   //    actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Storage_Button_Open, ShortcutKeys = Keys.Control | Keys.L, ActionHandler = this.Show_Projects_ActionHandler });
                   //    break;
                   case Storage.ServerOperation.UPLOAD:
                       actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Storage_Button_Close, ShortcutKeys = Keys.Control | Keys.S, ActionHandler = this.Storage_Close_ActionHandler });
                       break;
                   case Storage.ServerOperation.PACKPROCESS:
                       actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Cawabunga, ActionHandler = this.StoragePackProcessing_ActionHandler });
                       break;
                   default:
                       throw new Exception(Storage.Exceptions.UNKNOWN_SERVER_OPERATION.Description());
               }
           });

            //Операции просмотра содержимого Хранилища
            EnumLoop<Storage.BrowseOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Storage.BrowseOperation.FILES:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), ActionHandler = (sender, action) => this.Storage_Contents_Show_ActionHandler(sender, action, Store.Controls.StorageContentsViewControl.ViewMode.Files) });
                        break;
                    case Storage.BrowseOperation.FUNCTIONS:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), ActionHandler = (sender, action) => this.Storage_Contents_Show_ActionHandler(sender, action, Store.Controls.StorageContentsViewControl.ViewMode.Functions) });
                        break;
                    case Storage.BrowseOperation.VARIABLES:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), ActionHandler = (sender, action) => this.Storage_Contents_Show_ActionHandler(sender, action, Store.Controls.StorageContentsViewControl.ViewMode.Variables) });
                        break;
                    default:
                        throw new Exception(Storage.Exceptions.UNKNOWN_BROWSE_OPERATION.Description());
                }
            });

            //Операции с плагином
            EnumLoop<IA.Plugin.Operation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case IA.Plugin.Operation.SETTINGS:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Plugin_Button_Settings, ActionHandler = (sender, action) => this.Plugin_SettingsWindow_Show_ActionHandler(sender, action, new Handlers() { this.CurrentChoosedPlugin }) });
                        break;
                    case IA.Plugin.Operation.RUN:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Plugin_Button_Run, ActionHandler = (sender, action) => this.Plugin_Run_ActionHandler(sender, action, new Handlers() { this.CurrentChoosedPlugin }) });
                        break;
                    case IA.Plugin.Operation.REPORT:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Plugin_Button_Report, ActionHandler = (sender, action) => this.Plugin_Report_Generate_ActionHandler(sender, action, new Handlers() { this.CurrentChoosedPlugin }) });
                        break;
                    case IA.Plugin.Operation.RESULT:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Plugin_Button_Result, ActionHandler = (sender, action) => this.Plugin_ResultWindow_Show_ActionHandler(sender, action, this.CurrentChoosedPlugin) });
                        break;
                    default:
                        throw new Exception(IA.Plugin.Exceptions.UNKNOWN_OPERATION.Description());
                }
            });

            //Мультиоперации для плагина
            EnumLoop<IA.Plugin.MultiOperation>.ForEach(operation =>
           {
               switch (operation)
               {
                   case IA.Plugin.MultiOperation.SETTINGS:
                       actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Plugin_Button_Settings_Multi, ActionHandler = (sender, action) => this.Plugin_SettingsWindow_Show_ActionHandler(sender, action, this.pluginListControl.SpecificMultiRunSequence) });
                       break;
                   case IA.Plugin.MultiOperation.RUN:
                       actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Plugin_Button_Run_Multi, ActionHandler = (sender, action) => this.Plugin_Run_ActionHandler(sender, action, this.pluginListControl.SpecificMultiRunSequence) });
                       break;
                   case IA.Plugin.MultiOperation.REPORT:
                       actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Plugin_Button_Report_Multi, ActionHandler = (sender, action) => this.Plugin_Report_Generate_ActionHandler(sender, action, this.pluginListControl.SpecificMultiRunSequence) });
                       break;
                   case IA.Plugin.MultiOperation.PAUSE:
                       actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Plugin_Button_Pause, ActionHandler = (sender, action) => this.Plugin_Pause(sender, action, null, toolsControl) });
                       break;
                   case IA.Plugin.MultiOperation.STOP:
                       actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Plugin_Button_Stop, ActionHandler = (sender, action) => this.Plugin_Stop(sender, action, null) });
                       break;
                   default:
                       throw new Exception(IA.Plugin.Exceptions.UNKNOWN_MULTI_OPERATION.Description());
               }
           });

            //Глобальные операции
            EnumLoop<IA.Actions.Global>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Actions.Global.SETTINGS:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Global_Button_Settings, ActionHandler = this.MainSettings_Show_ActionHandler });
                        break;
                    case Actions.Global.USER:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Global_Button_User, ActionHandler = this.Database_User_Login_ActionHandler });
                        break;
                    case Actions.Global.EXPERT_MODE:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Global_Button_ExpertMode, ActionHandler = (sender, action) => this.MainForm_SetMode_ActionHandler(sender, action, Mode.EXPERT) });
                        break;
                    case Actions.Global.ABOUT:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Global_Button_About, ActionHandler = this.MainForm_About_ActionHandler });
                        break;
                    default:
                        throw new Exception(IA.Actions.Exceptions.UNKNOWN_GLOBAL_ACTION.Description());
                }
            });

            // создаем под утилиты
            foreach (string fileName in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.exe", SearchOption.TopDirectoryOnly))
            {
                string curName = Application.ExecutablePath.ToLower();
                if (!fileName.ToLower().Contains(".vshost") && fileName != curName)
                {
                    Assembly assembly = Assembly.LoadFile(fileName);

                    actions.Add(fileName.ToLower(), new Action() { Key = fileName.ToLower(), Text = ((AssemblyTitleAttribute)assembly.GetCustomAttribute(new AssemblyTitleAttribute("").GetType())).Title, Image = null, ActionHandler = (sender, action) => this.RunUtil(fileName) });
                }
            }
        }

        /// <summary>
        /// Обновление всеовзможных действий
        /// </summary>
        private void Actions_Refresh()
        {
            //Узнаём есть ли соединение с базой данных
            bool isDatabaseConnected = SqlConnector.IADB?.DatabaseConnection.IsConnected != null ? SqlConnector.IADB.DatabaseConnection.IsConnected : false;

            //Узнаем выполнил ли пользователь вход
            bool isUserAuthorized = this.CurrentUser != null;

            //Узнаём открыто ли Хранилище
            bool isStorageOpened = this.CurrentStorage != null;

            //Узнаём есть ли незаконченное действие со стороны пользователя
            bool isInUserAction = this.State == State.STORAGE_CLOSED_USER_ACTION || this.state == State.STORAGE_OPENED_USER_ACTION;

            //Узнаём есть ли незаконченное действие со стороны комрьютера
            bool isInComputerAction = this.State == State.STORAGE_OPENED_COMPUTER_ACTION;

            //Доступность действий (Серверные операции с проектом)
            EnumLoop<Project.ServerOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Project.ServerOperation.OPEN:
                        actions[operation.FullName()].Enabled = isUserAuthorized && !isStorageOpened && !isInUserAction && !isInComputerAction;
                        break;
                    case Project.ServerOperation.CLOSE:
                        actions[operation.FullName()].Enabled = isUserAuthorized && isStorageOpened && isInUserAction && !isInComputerAction;
                        break;
                    default:
                        throw new Exception(Project.Exceptions.UNKNOWN_SERVER_OPERATION.Description());
                }
            });

            //Доступность действий (Пользовательские операции с проектом)
            EnumLoop<Project.UserOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Project.UserOperation.GET_REPORTS:
                        actions[operation.FullName()].Enabled = isUserAuthorized && isStorageOpened && isInUserAction && !isInComputerAction;
                        break;
                    default:
                        throw new Exception(Project.Exceptions.UNKNOWN_USER_OPERATION.Description());
                }
            });

            //Доступность действий (Операции с Хранилищем на сервере)
            EnumLoop<Storage.ServerOperation>.ForEach(operation =>
            {
                switch (operation)
                {
                    //case Storage.ServerOperation.DOWNLOAD:
                    //    actions[operation.FullName()].Enabled = isUserAuthorized && !isStorageOpened && !isInUserAction && !isInComputerAction;
                    //    break;
                    case Storage.ServerOperation.UPLOAD:
                        actions[operation.FullName()].Enabled = isUserAuthorized && isStorageOpened && !isInUserAction && !isInComputerAction;
                        break;
                    case Storage.ServerOperation.PACKPROCESS:
                        actions[operation.FullName()].Enabled = !Settings.AutonomyMode && isUserAuthorized && !isStorageOpened && !isInUserAction && !isInComputerAction;
                        break;
                    default:
                        throw new Exception(Storage.Exceptions.UNKNOWN_SERVER_OPERATION.Description());
                }
            });

            //Доступность действий (Операции просмотра содержимого Хранилища)
            EnumLoop<Storage.BrowseOperation>.ForEach(operation =>
            {
                actions[operation.FullName()].Enabled = isUserAuthorized && isStorageOpened && !isInUserAction && !isInComputerAction;
            });

            bool isReadyToRun = this.CurrentChoosedPlugin == null ? false : this.CurrentChoosedPlugin.IsReadyToRun;

            //Доступность действий (Операции с плагином)
            EnumLoop<IA.Plugin.Operation>.ForEach(operation =>
            {
                switch (operation)
                {
                    case IA.Plugin.Operation.SETTINGS:
                        actions[operation.FullName()].Enabled = isUserAuthorized && !isInUserAction && !isInComputerAction && this.CurrentChoosedPlugin != null && this.CurrentChoosedPlugin.IsReadyToShowCustomSettingsWindow;
                        break;
                    case IA.Plugin.Operation.RUN:
                        actions[operation.FullName()].Enabled = isUserAuthorized && !isInUserAction && !isInComputerAction && isReadyToRun;
                        break;
                    case IA.Plugin.Operation.REPORT:
                        actions[operation.FullName()].Enabled = isUserAuthorized && !isInUserAction && !isInComputerAction && this.CurrentChoosedPlugin != null && this.CurrentChoosedPlugin.IsReadyToGenerateReport;
                        break;
                    case IA.Plugin.Operation.RESULT:
                        actions[operation.FullName()].Enabled = isUserAuthorized && !isInUserAction && !isInComputerAction && this.CurrentChoosedPlugin != null && this.CurrentChoosedPlugin.IsReadyToShowResultWindow;
                        break;
                    default:
                        throw new Exception(IA.Plugin.Exceptions.UNKNOWN_OPERATION.Description());
                }
            });


            //Доступность действий (Мулти операции с плагинами)
            EnumLoop<IA.Plugin.MultiOperation>.ForEach(operation =>
            {
                IA.Plugin.Sequences.SpecificMultiRunSequence currentRootPluginMultiRunSequence = this.pluginListControl.SpecificMultiRunSequence;

                switch (operation)
                {
                    case IA.Plugin.MultiOperation.SETTINGS:
                        actions[operation.FullName()].Enabled = isUserAuthorized && !isInUserAction && !isInComputerAction && this.CurrentRootPlugin != null && currentRootPluginMultiRunSequence != null && currentRootPluginMultiRunSequence.IsReadyToShowMultiCustomSettingsWindow;
                        break;
                    case IA.Plugin.MultiOperation.RUN:
                        actions[operation.FullName()].Enabled = isUserAuthorized && !isInUserAction && !isInComputerAction && this.CurrentRootPlugin != null && currentRootPluginMultiRunSequence != null && currentRootPluginMultiRunSequence.IsReadyToMultiRun;
                        break;
                    case IA.Plugin.MultiOperation.REPORT:
                        actions[operation.FullName()].Enabled = isUserAuthorized && !isInUserAction && !isInComputerAction && this.CurrentRootPlugin != null && currentRootPluginMultiRunSequence != null && currentRootPluginMultiRunSequence.IsReadyToMultiGenerateReport;
                        break;
                    case IA.Plugin.MultiOperation.PAUSE:
                    case IA.Plugin.MultiOperation.STOP:
                        actions[operation.FullName()].Enabled = isInComputerAction;
                        break;
                    default:
                        throw new Exception(IA.Plugin.Exceptions.UNKNOWN_MULTI_OPERATION.Description());
                }
            });

            //Доступность действий (Глобальные операции)
            EnumLoop<Actions.Global>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Actions.Global.USER:
                        actions[operation.FullName()].Enabled = !Settings.AutonomyMode && isDatabaseConnected && !isStorageOpened && !isInUserAction && !isInComputerAction;
                        break;
                    case Actions.Global.SETTINGS:
                        actions[operation.FullName()].Enabled = !isStorageOpened && !isInUserAction && !isInComputerAction;
                        break;
                    case Actions.Global.EXPERT_MODE:
                        actions[operation.FullName()].Enabled = this.Mode == Mode.BASIC && isUserAuthorized && !isInComputerAction;
                        break;
                    case Actions.Global.ABOUT:
                        actions[operation.FullName()].Enabled = !isInComputerAction;
                        break;
                    default:
                        throw new Exception(Actions.Exceptions.UNKNOWN_GLOBAL_ACTION.Description());
                }
            });

        }
        #endregion

        #region Меню

        /// <summary>
        /// Формирование меню для заданного режима работы приложения
        /// </summary>
        /// <param name="mode"></param>
        private void Menu_SetMode(Mode mode)
        {
            menuControl.Items.Clear();

            ToolStripMenuItem root, tmp;

            switch (mode)
            {
                case Mode.BASIC:
                    {
                        //Проект
                        root = menuControl.Add_MenuItem(null, "Project", "Проект");
                        EnumLoop<Project.ServerOperation>.ForEach(operation => menuControl.Add_MenuItem(root, actions[operation.FullName()]));
                        menuControl.AddSeparator(root, "Project.Separator.1");
                        EnumLoop<Project.UserOperation>.ForEach(operation => menuControl.Add_MenuItem(root, actions[operation.FullName()]));

                        //Общее
                        root = menuControl.Add_MenuItem(null, "Common", "Общее");
                        EnumLoop<IA.Actions.Global>.ForEach(operation => menuControl.Add_MenuItem(root, actions[operation.FullName()]));

                        break;
                    }
                case Mode.EXPERT:
                    {
                        //Хранилище
                        root = menuControl.Add_MenuItem(null, "Storage", "Хранилище");
                        EnumLoop<Storage.ServerOperation>.ForEach(operation => menuControl.Add_MenuItem(root, actions[operation.FullName()]));
                        menuControl.AddSeparator(root, "Storage.Separator.1");
                        tmp = menuControl.Add_MenuItem(root, "Storage.Contents", "Содержимое");
                        EnumLoop<Storage.BrowseOperation>.ForEach(operation => menuControl.Add_MenuItem(tmp, actions[operation.FullName()]));

                        //Плагин
                        root = menuControl.Add_MenuItem(null, "Plugin.Operations", "Плагин");
                        EnumLoop<IA.Plugin.Operation>.ForEach(operation => menuControl.Add_MenuItem(root, actions[operation.FullName()]));
                        menuControl.AddSeparator(root, "Plugin.Operations.Separator");
                        EnumLoop<IA.Plugin.MultiOperation>.ForEach(operation => menuControl.Add_MenuItem(root, actions[operation.FullName()]));

                        //Общее
                        root = menuControl.Add_MenuItem(null, "Common", "Общее");
                        EnumLoop<IA.Actions.Global>.ForEach(operation =>
                        {
                            switch (operation)
                            {
                                case Actions.Global.USER:
                                case Actions.Global.SETTINGS:
                                case Actions.Global.ABOUT:
                                    menuControl.Add_MenuItem(root, actions[operation.FullName()]);
                                    break;
                                case Actions.Global.EXPERT_MODE:
                                    break;
                                default:
                                    throw new Exception(Actions.Exceptions.UNKNOWN_GLOBAL_ACTION.Description());
                            }

                        });

                        // утилиты
                        //Общее
                        root = menuControl.Add_MenuItem(null, "Utils", "Утилиты");

                        foreach (string fileName in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.exe", SearchOption.TopDirectoryOnly))
                        {
                            string curName = Application.ExecutablePath.ToLower();
                            if (!fileName.ToLower().Contains(".vshost") && fileName.ToLower() != curName)
                            {
                                menuControl.Add_MenuItem(root, actions[fileName.ToLower()]);
                            }
                        }
                        break;
                    }
            }


        }

        #endregion

        #region Панель инструментов
        /// <summary>
        /// Формирование панели инструметов для заданного режима работы приложения
        /// </summary>
        /// <param name="mode"></param>
        private void Tools_SetMode(Mode mode)
        {
            toolsControl.Items.Clear();

            switch (mode)
            {
                case Mode.BASIC:
                    {
                        toolsControl.AddSeparator("Project.ServerOperation.Begin");
                        EnumLoop<Project.ServerOperation>.ForEach(operation => toolsControl.AddButton(actions[operation.FullName()]));
                        toolsControl.AddSeparator("Project.ServerOperation.End");

                        toolsControl.AddSeparator("Project.UserOperation.Begin");
                        EnumLoop<Project.UserOperation>.ForEach(operation => toolsControl.AddButton(actions[operation.FullName()]));
                        toolsControl.AddSeparator("Project.UserOperation.End");

                        break;
                    }
                case Mode.EXPERT:
                    {
                        toolsControl.AddSeparator("Storage.Server.Operations.Begin");
                        EnumLoop<Storage.ServerOperation>.ForEach(operation => toolsControl.AddButton(actions[operation.FullName()]));
                        toolsControl.AddSeparator("Storage.Server.Operations.End");

                        toolsControl.AddSeparator("Plugin.Operations.Begin");
                        EnumLoop<IA.Plugin.Operation>.ForEach(operation => toolsControl.AddButton(actions[operation.FullName()]));
                        toolsControl.AddSeparator("Plugin.Operations.End");

                        toolsControl.AddSeparator("Plugin.MultiOperations.Begin");
                        EnumLoop<IA.Plugin.MultiOperation>.ForEach(operation => toolsControl.AddButton(actions[operation.FullName()]));
                        toolsControl.AddSeparator("Plugin.MultiOperations.End");

                        break;
                    }
                case Mode.NO:
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }
        }

        #endregion

        #region Информационная панель
        /// <summary>
        /// Формирование информационной панели для заданного режима работы приложения
        /// </summary>
        /// <param name="mode"></param>
        private void Info_SetMode(Mode mode)
        {
            switch (mode)
            {
                case Mode.BASIC:
                    {
                        infoControl.Items[IA.InfoType.STORAGE.FullName()].Visible = false;
                        break;
                    }
                case Mode.EXPERT:
                    {
                        infoControl.Items[IA.InfoType.STORAGE.FullName()].Visible = true;
                        break;
                    }
                case Mode.NO:
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }
        }
        #endregion

        #region Панель прогресса
        /// <summary>
        /// Формирование панели прогресса для заданного режима работы приложения
        /// </summary>
        /// <param name="mode"></param>
        private void Progress_SetMode(Mode mode)
        {
            switch (mode)
            {
                case Mode.BASIC:
                    {
                        this.progressControl.ShowSpecificElementsOnly(new ProgressControl.ProgressType[] { ProgressControl.ProgressType.PLUGIN, ProgressControl.ProgressType.WORKFLOW });
                        break;
                    }
                case Mode.EXPERT:
                    {
                        this.progressControl.ShowSpecificElementsOnly(new ProgressControl.ProgressType[] { ProgressControl.ProgressType.PLUGIN, ProgressControl.ProgressType.PLUGINLIST });
                        break;
                    }
                case Mode.NO:
                default:
                    throw new Exception(Exceptions.UNKNOWN_MODE.Description());
            }
        }
        #endregion

        #region Работа с треем
        /// <summary>
        /// Формирование иконки трея для заданного режима работы приложения
        /// </summary>
        private void Tray_Icon_SetMode(Mode mode)
        {
            trayIcon.SetText("Режим работы: " + mode.Description());
        }
        #endregion

        #region Полезные методы

        /// <summary>
        /// Загрузить информацию из конфигурационного файла
        /// </summary>
        private void LoadConfigWithWaitWindow()
        {
            try
            {
                //Отображаем окно ожидания
                WaitWindow.Show(
                    delegate (WaitWindow window)
                    {
                        //Читаем конфигурационный файл
                        IA.Config.Controller.Load();
                    },
                    "Чтение конфигурационного файла"
                );
            }
            catch (IA.Config.ConfigException ex)
            {
                string message = new string[]
                {
                    "Ошибка при работе с конфигурационным файлом.",
                    ex.ToFullMultiLineMessage(),
                    Constants.Message.ApplicationWillBeClosed,
                    Constants.Message.ReinstallIsNeeded
                }.ToMultiLineString();

                //Отображаем ошибку
                MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //Принудительно закрываем приложение
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Загрузить все плагины с отображением окна ожидания
        /// </summary>
        private void LoadPluginsWithWaitWindow()
        {
            try
            {
                //Отображаем окно ожидания
                //WaitWindow.Show(
                //    delegate(WaitWindow window)
                {
                    //Загружаем плагины
                    IA.Plugin.HandlersInitial.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Plugins"));

                    //Получаем все необходимые плагины для основного режима
                    //HashSet<uint> necessaryPluginIDs = new HashSet<uint>(IA.Config.Controller.GetNecessaryPlugins());
                    HashSet<uint> necessaryPluginIDs = new HashSet<uint>();

                    //Получаем список отсутствующих необходимых плагинов
                    necessaryPluginIDs.ExceptWith(AllPlugins.GetAll().Select(p => p.ID));

                    //Если часть небходимых плагинов отсутствует
                    if (necessaryPluginIDs.Count() != 0)
                        throw new Exception(
                            new string[]
                            {
                                    "Не найдена часть плагинов, необходимых для корректной работы приложения в режиме <" + Mode.BASIC.Description() +">.",
                                    "Идентификаторы отсутствующих плагинов: " + String.Join(",", necessaryPluginIDs) + "."
                            }.ToMultiLineString()
                        );

                    //Подгружаем их в рамках списка плагинов
                    this.pluginListControl.LoadPlugins();
                }
                //                );
            }
            catch (Exception ex)
            {
                string message = new string[]
                {
                    "Ошибка при работе с плагинами.",
                    ex.ToFullMultiLineMessage(),
                    Constants.Message.ApplicationWillBeClosed,
                    Constants.Message.ReinstallIsNeeded
                }.ToMultiLineString();

                //Отображаем ошибку
                MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //Принудительно закрываем приложение
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Проверить соединение с базой данных с отображение окна ожидания
        /// </summary>
        /// <returns>Результат проверки соединенеия с базой данных</returns>
        private bool TestDatabaseConnectionWithWaitWindow()
        {
            //Изначально результат проверки положительный
            bool ret = true;

            //Проверяем соединенеие с базой данных
            Sql.SqlExtensions.TestDatabaseConnectionsWithWaitWindow(
                new Sql.SQLConnection[]
                        {
                            Sql.SqlConnector.IADB
                        },
                delegate (Sql.DatabaseConnection[] databaseConnections)
                {
                    //Результат проверки отрицательный
                    ret = false;

                    //Проверка на разумность
                    if (databaseConnections == null || databaseConnections.Length != 1)
                        throw new ArgumentNullException("databaseConnections", "Список соединений с базами данных не соответствует ожидаемому.");

                    //Отображаем ошибку
                    Monitor.Log.Error(ObjectName, "Отсутствует соединение с базой данных <" + databaseConnections.First().DatabaseName + ">.", true);
                }
            );

            return ret;
        }

        /// <summary>
        ///  Проверить соединение с сервером проектов с отображением окна ожидания
        /// </summary>
        /// <returns>Результат проверки соединения с сервером проектов </returns>
        private bool TestProjectsServerConnectionWithWaitWindow()
        {
            if (Settings.AutonomyMode)
                return true;

            if (Network.NetworkExtensions.TestNetworkConnection(ProjectsServerNetworkResource, true, "Проверка соединения с сервером проектов <" + this.ProjectsServerNetworkResource.Name + ">"))
                return true;

            Monitor.Log.Error(ObjectName, "Отсутствует соединение с сервером проектов <" + this.ProjectsServerNetworkResource.Name + ">.", true);

            return false;
        }
        #endregion
    }

}
