﻿using System.ComponentModel;

namespace IA.MainForm
{
    /// <summary>
    /// Всевозможные режимы работы главной формы приложения
    /// </summary>
    internal enum Mode
    {
        /// <summary>
        /// Неизвестный
        /// </summary>
        [Description("Неизвестный")]
        NO,
        /// <summary>
        /// Основной
        /// </summary>
        [Description("Основной")]
        BASIC,
        /// <summary>
        /// Экспертный
        /// </summary>
        [Description("Экспертный")]
        EXPERT
    }
}
