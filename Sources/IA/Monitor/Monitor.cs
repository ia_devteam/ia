﻿/*
 * Монитор работы IA
 * Файл Monitor.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Юхтин
 * 
 * Обработчик ошибок выполнения, состояний плагинов, отображения хода выполнения работы
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using IA.Extensions;

namespace IA
{
    /// <summary>
    /// Обработчик ошибок выполнения, состояний плагинов, отображения хода выполнения работы
    /// </summary>
    public static class Monitor
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Monitor";

        /// <summary>
        /// Класс для работы с тасками приложения
        /// </summary>
        public static class Tasks
        {
            /// <summary>
            /// Описание отдельного таска
            /// </summary>
            public class Task
            {
                /// <summary>
                /// Тип возможного обновления таска
                /// </summary>
                public enum UpdateType
                {
                    /// <summary>
                    /// Обновить значение текущего этапа для заданного таска
                    /// </summary>
                    STAGE,
                    /// <summary>
                    /// Обновить общее число этапов для заданного таска
                    /// </summary>
                    STAGES
                }

                /// <summary>
                /// Время начала выполнения таска
                /// </summary>
                public TimeSpan StartExecutionTime { get; set; }

                /// <summary>
                /// Название таска
                /// </summary>
                public string Name { get; private set; }

                /// <summary>
                /// Количество этапов выполнения таска
                /// </summary>
                public ulong Stages { get; set; }

                /// <summary>
                /// Текущий этап выполнения таска
                /// </summary>
                public ulong Stage { get; set; }

                /// <summary>
                /// Плагин, к которму привзяан данный таск
                /// </summary>
                public string PluginName { get; private set; }

                /// <summary>
                /// Конструктор
                /// </summary>
                /// <param name="pluginName">Название плагина</param>
                /// <param name="name">Название таска</param>
                /// <param name="stages">Количество единиц работы таска</param>
                public Task(string pluginName, string name, ulong stages = 0)
                {
                    this.StartExecutionTime = System.DateTime.Now.TimeOfDay;
                    this.Name = name;
                    this.Stages = stages;
                    this.Stage = 0;
                    this.PluginName = pluginName;
                }

                public static Task FromEnum(string pluginName, Enum value)
                {
                    return new Task(pluginName, value.Description());
                }
            }

            /// <summary>
            /// Интерфейс слушателя
            /// </summary>
            public interface Interface
            {
                /// <summary>
                /// Обновить текущую информацию о заданном таске
                /// </summary>
                /// <param name="pluginName">Имя плагина</param>
                /// <param name="taskName">Имя таска</param>
                /// <param name="type">Тип обновления для таска</param>
                /// <param name="value">Значение</param>
                void Update(string pluginName, string taskName, Tasks.Task.UpdateType type, ulong value);
            }

            /// <summary>
            /// Список слушателей
            /// </summary>
            private static List<Interface> Audience = new List<Interface>();

            #region Делегаты
            private delegate void Delegate_Update(string pluginName, string taskName, Tasks.Task.UpdateType type, ulong value);
            #endregion

            /// <summary>
            /// Регистрация слушателя
            /// </summary>
            /// <param name="listener">Слушатель</param>
            public static void Register(Interface listener)
            {
                if (Audience.Contains(listener))
                    return;

                Audience.Add(listener);
            }

            /// <summary>
            /// Отмена регистрации слушателя
            /// </summary>
            /// <param name="listener"></param>
            public static void Unregister(Interface listener)
            {
                if (Audience.Contains(listener))
                    Audience.Remove(listener);
            }

            /// <summary>
            /// Обновить текущую информацию о заданном таске
            /// </summary>
            /// <param name="pluginName">Имя плагина</param>
            /// <param name="taskName">Имя таска</param>
            /// <param name="type">Тип обновления для таска</param>
            /// <param name="value">Значение</param>
            public static void Update(string pluginName, string taskName, Tasks.Task.UpdateType type, ulong value)
            {
                foreach (Interface listener in Tasks.Audience)
                {
                    Control control = listener as Control;

                    if (control != null && control.InvokeRequired)
                    {
                        control.Invoke(new Delegate_Update(listener.Update), new object[4] { pluginName, taskName, type, value });
                        return;
                    }

                    listener.Update(pluginName, taskName, type, value);
                }
            }
        }

        /// <summary>
        /// Класс для работы с логом приложения
        /// </summary>
        public static class Log
        {
            /// <summary>
            /// Интерфейс слушателя
            /// </summary>
            public interface Interface
            {
                /// <summary>
                /// Добавить запись в лог
                /// </summary>
                /// <param name="message">Сообщение</param>
                void AddMessage(Log.Message message);
            }

            /// <summary>
            /// Список слушателей
            /// </summary>
            private static HashSet<Interface> Audience = new HashSet<Interface>();

            /// <summary>
            /// Класс, реализующий отдельную запись лога.
            /// </summary>
            public class Message : IComparable<Message>
            {
                /// <summary>
                /// Тип сообщения лога
                /// </summary>
                public enum enType
                {
                    /// <summary>
                    /// Сообщение
                    /// </summary>
                    [Description("Информация")]
                    INFORMATION,
                    /// <summary>
                    /// Предупреждение
                    /// </summary>
                    [Description("Предупреждение")]
                    WARNING,
                    /// <summary>
                    /// Ошибка
                    /// </summary>
                    [Description("Ошибка")]
                    ERROR
                }

                /// <summary>
                /// Отправитель записи. Не может быть Null.
                /// </summary>
                public string Sender { get; private set; }

                /// <summary>
                /// Тип записи лога
                /// </summary>
                public enType Type { get; private set; }

                /// <summary>
                /// Текст записи лога. Не может быть Null.
                /// </summary>
                public string Text { get; private set; }

                /// <summary>
                /// Требуется ли дополнительное уведомление
                /// </summary>
                public bool IsNotificationNeeded { get; private set; }

                /// <summary>
                /// Дата и время создания записи лога
                /// </summary>
                public DateTime DateTime { get; private set; }

                /// <summary>
                /// Конструктор
                /// </summary>
                /// <param name="sender">Имя отправителя записи лога</param>
                /// <param name="type">Тип записи лога</param>
                /// <param name="text">Текст записи лога</param>
                /// <param name="isNotificationNeeded">Требуется ли дополнительное уведомление?</param>
                public Message(string sender, enType type, string text, bool isNotificationNeeded = false)
                {
                    try
                    {
                        //Проверка на разумность
                        if (sender == null)
                            throw new Exception("Отрпавитель записи лога не определён.");

                        //Проверка на разумность
                        if (text == null)
                            throw new Exception("Текст записи лога не определён.");

                        this.Sender = sender;
                        this.Type = type;
                        this.Text = text;
                        this.IsNotificationNeeded = isNotificationNeeded;
                        this.DateTime = DateTime.Now;
                    }
                    catch (Exception ex)
                    {
                        //Формируем исключение
                        throw new Exception("Ошибка в ходе создания экземпляра лога.", ex);
                    }
                }

                /// <summary>
                /// Представление сообщения в виде строки
                /// </summary>
                /// <returns></returns>
                public override string ToString()
                {
                    return this.DateTime.TimeOfDay.ToString(@"hh\:mm\:ss") + " <" + this.Sender + "> <" + this.Type.Description() + "> " + this.Text;
                }

                /// <summary>
                /// Сравнение текущего сообщения Лога с указанным
                /// </summary>
                /// <param name="other">Сообщение лога, с которым требуется сравнить текущее сообщение</param>
                /// <returns></returns>
                public int CompareTo(Message other)
                {
                    int ret;

                    if ((ret = this.Sender.CompareTo(other.Sender)) != 0)
                        return ret;

                    if ((ret = this.Type.CompareTo(other.Type)) != 0)
                        return ret;

                    if ((ret = this.IsNotificationNeeded.CompareTo(other.IsNotificationNeeded)) != 0)
                        return ret;

                    return this.Text.CompareTo(other.Text);
                }
            }

            #region Делегаты
            private delegate void Delegate_AddMessage(Log.Message message);
            #endregion

            /// <summary>
            /// Регистрация слушателя
            /// </summary>
            /// <param name="listener">Слушатель</param>
            public static void Register(Interface listener)
            {
                if (Audience.Contains(listener))
                    return;

                Audience.Add(listener);
            }

            /// <summary>
            /// Отмена регистрации слушателя
            /// </summary>
            /// <param name="listener"></param>
            public static void Unregister(Interface listener)
            {
                if (Audience.Contains(listener))
                    Audience.Remove(listener);
            }

            /// <summary>
            /// Отправить сообщение всем слушателям
            /// </summary>
            /// <param name="senderName">Имя отправителя</param>
            /// <param name="type">Тип сообщения</param>
            /// <param name="text">Текст сообщения</param>
            /// <param name="isNotificationNeeded">Требуется ли дополнительное уведомление?</param>
            private static void SendMessageToAudience(string senderName, Log.Message.enType type, string text, bool isNotificationNeeded)
            {
                try
                {
                    foreach (Interface listener in Log.Audience)
                    {
                        Control control = listener as Control;

                        if (control != null && control.InvokeRequired)
                            control.Invoke(new Delegate_AddMessage(listener.AddMessage), new object[1] { new Log.Message(senderName, type, text, isNotificationNeeded) });
                        else
                            listener.AddMessage(new Log.Message(senderName, type, text, isNotificationNeeded));
                    }
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new Exception("Ошибка при рассылке сообщения слушателям из Монитора.", ex);
                }
            }

            /// <summary>
            /// Вывести сообщение в лог.
            /// </summary>
            /// <param name="senderName">Имя отправителя</param>
            /// <param name="text">Текст сообщения</param>
            /// <param name="isNotificationNeeded">Требуется ли дополнительное уведомление?</param>
            public static void Information(string senderName, string text, bool isNotificationNeeded = false)
            {
                SendMessageToAudience(senderName, Log.Message.enType.INFORMATION, text, isNotificationNeeded);
            }

            /// <summary>
            /// Вывести предупреждение в лог. Поток, вызвавший данный метод, остаётся полностью функциональным.
            /// </summary>
            /// <param name="senderName">Имя отправителя</param>
            /// <param name="text">Текст предупреждения</param>
            /// <param name="isNotificationNeeded">Требуется ли дополнительное уведомление?</param>
            public static void Warning(string senderName, string text, bool isNotificationNeeded = false)
            {
                SendMessageToAudience(senderName, Log.Message.enType.WARNING, text, isNotificationNeeded);
            }

            /// <summary>
            /// Вывести ошибку в лог. Поток, вызвавший данный метод, остаётся полностью функциональным.
            /// </summary>
            /// <param name="senderName">Имя отправителя</param>
            /// <param name="text">Текст ошибки</param>
            /// <param name="isNotificationNeeded">Требуется ли дополнительное уведомление?</param>
            public static void Error(string senderName, string text, bool isNotificationNeeded = false)
            {
                SendMessageToAudience(senderName, Log.Message.enType.ERROR, text, isNotificationNeeded);
            }
        }

        /// <summary>
        /// Класс для работы с окном прогресса приложения
        /// </summary>
        public static class Progress
        {
        }

        /// <summary>
        /// Класс для работы со статусной строкой приложения
        /// </summary>
        public static class Status
        {
            /// <summary>
            /// Интерфейс слушателя
            /// </summary>
            public interface Interface
            {
                /// <summary>
                /// Показать сообщение в строки статуса
                /// </summary>
                /// <param name="type">Тип сообщения</param>
                /// <param name="text">Текст сообщения</param>
                void ShowMessage(Status.MessageType type, string text);

                /// <summary>
                /// Очистка строки статуса
                /// </summary>
                void Clear();
            }

            /// <summary>
            /// Список слушателей
            /// </summary>
            private static List<Interface> Audience = new List<Interface>();

            /// <summary>
            /// Тип сообщения строки статуса
            /// </summary>
            public enum MessageType
            {
                /// <summary>
                /// Сообщение показывается постоянно до тех пор, пока не будет подана команда показать другое сообщение.
                /// </summary>
                PERMANENT,
                /// <summary>
                /// Сообщение показываться несколько секунд, после чего показывается сообщение, отображаемое ранее.
                /// </summary>
                TEMPORARY,
            }

            #region Делегаты
            private delegate void Delegate_ShowMessage(Status.MessageType type, string text);
            private delegate void Delegate_Clear();
            #endregion

            /// <summary>
            /// Регистрация слушателя
            /// </summary>
            /// <param name="listener">Слушатель</param>
            public static void Register(Interface listener)
            {
                if (Audience.Contains(listener))
                    return;

                Audience.Add(listener);
            }

            /// <summary>
            /// Отмена регистрации слушателя
            /// </summary>
            /// <param name="listener"></param>
            public static void Unregister(Interface listener)
            {
                if (Audience.Contains(listener))
                    Audience.Remove(listener);
            }

            /// <summary>
            /// Показать сообщение в строке статуса
            /// </summary>
            /// <param name="type">Тип сообщения</param>
            /// <param name="text">Текст сообщения</param>
            public static void ShowMessage(Status.MessageType type, string text)
            {
                foreach (Interface listener in Status.Audience)
                {
                    Control control = listener as Control;

                    if (control != null && control.InvokeRequired)
                    {
                        control.Invoke(new Delegate_ShowMessage(listener.ShowMessage), new object[2] { type, text });
                        return;
                    }

                    listener.ShowMessage(type, text);
                }
            }

            /// <summary>
            /// Очистка строки статуса
            /// </summary>
            public static void Clear()
            {
                foreach (Interface listener in Status.Audience)
                {
                    Control control = listener as Control;

                    if (control != null && control.InvokeRequired)
                    {
                        control.Invoke(new Delegate_Clear(listener.Clear));
                        return;
                    }

                    listener.Clear();
                }
            }
        }

        /// <summary>
        /// Класс для работы с рабочим пространстом приложения
        /// </summary>
        public static class Workspace
        {
        }
    }
}

namespace IA.Extensions
{
    /// <summary>
    /// Класс реализует быстрое создание Task для монитора из значения перечисления.
    /// </summary>
    public static class EnumExtensionsToMonitor
    {
        /// <summary>
        /// Создать задачу для отображения в Мониторе из текущего значения.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public static Monitor.Tasks.Task ToMonitorTask(this Enum value, string pluginName)
        {
            return new Monitor.Tasks.Task(pluginName, value.Description());
        }
    }
}
