﻿namespace IA.Plugin
{
    /// <summary>
    /// Интерфейс настроек плагина
    /// </summary>
    public interface Settings
    {
        /// <summary>
        /// Сохранение настроек плагина
        /// </summary>
        void Save();
    }
}
