﻿/*
 * Интерфейс плагина
 * Файл PluginInterface.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Юхтин
 * 
 * Форма, предназначенная для хранения плагинов и задач
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using Store;
using Store.Table;

namespace IA.Plugin
{
    /// <summary>
    /// Список возможностей плагина
    /// </summary>
    [Flags]
    public enum Capabilities
    {
        /// <summary>
        /// У плагина нет никаких возможностей
        /// </summary>
        NONE = 0x00,
        /// <summary>
        /// У плагина есть окно настроек (самые необходимые настройки)
        /// </summary>
        SIMPLE_SETTINGS_WINDOW = 0x01,
        /// <summary>
        /// У плагина есть окно настроек (всевозможные настройки)
        /// </summary>
        CUSTOM_SETTINGS_WINDOW = 0x02,
        /// <summary>
        /// У плагина есть окно выполнения
        /// </summary>
        RUN_WINDOW = 0x04,
        /// <summary>
        /// У плагина есть окно с результатами
        /// </summary>
        RESULT_WINDOW = 0x08,
        /// <summary>
        /// В плагине предусмотрена возможность генерации отчётов
        /// </summary>
        REPORTS = 0x10,
        /// <summary>
        /// Плагин может быть перезапущен произвольное количество раз
        /// </summary>
        RERUN = 0x20,
        /// <summary>
        /// В плагине реализована возможность плавной остановки
        /// </summary>
        STOP_SMOOTHLY = 0x40
    }

    /// <summary>
    /// Список операций с плагином
    /// </summary>
    public enum Operation
    {
        /// <summary>
        /// Настроить
        /// </summary>
        [Description("Настроить")]
        SETTINGS,
        /// <summary>
        /// Запустить
        /// </summary>
        [Description("Запустить")]
        RUN,
        /// <summary>
        /// Сформировать отчёт
        /// </summary>
        [Description("Сформировать отчёт")]
        REPORT,
        /// <summary>
        /// Показать результат
        /// </summary>
        [Description("Показать результат")]
        RESULT
    }

    /// <summary>
    /// Список операций с несколькими плагинами
    /// </summary>
    public enum MultiOperation
    {
        /// <summary>
        /// Настроить все плагины
        /// </summary>
        [Description("Настроить все плагины")]
        SETTINGS,
        /// <summary>
        /// Запустить все плагины
        /// </summary>
        [Description("Запустить все плагины")]
        RUN,
        /// <summary>
        /// Сформировать отчёты по всем плагинам
        /// </summary>
        [Description("Сформировать отчёты по всем плагинам")]
        REPORT,
        /// <summary>
        /// Сформировать отчёты по всем плагинам
        /// </summary>
        [Description("Поставить выполнение на паузу/снять с паузы")]
        PAUSE,
        /// <summary>
        /// Сформировать отчёты по всем плагинам
        /// </summary>
        [Description("Прекратить выполнение плагина")]
        STOP
    }

    /// <summary>
    /// Тип возможных настроек плагина
    /// </summary>
    public enum SettingsType
    {
        /// <summary>
        /// Самые необходимые настройки плагина
        /// </summary>
        SIMPLE,
        /// <summary>
        /// Все возможные настройки плагина
        /// </summary>
        CUSTOM
    }

    /// <summary>
    /// Список самых распространённых исключений при работе с плагинами
    /// </summary>
    public enum Exceptions
    {
        /// <summary>
        /// Обнаружена неизвестная операция при работе с плагином
        /// </summary>
        [Description("Обнаружена неизвестная операция при работе с плагином.")]
        UNKNOWN_OPERATION,
        /// <summary>
        /// Обнаружена неизвестная операция c несколькими плагинами
        /// </summary>
        [Description("Обнаружена неизвестная операция c несколькими плагинами.")]
        UNKNOWN_MULTI_OPERATION,
        /// <summary>
        /// Обнаружен неивестный тип настроек плагина
        /// </summary>
        [Description("Обнаружен неивестный тип настроек плагина.")]
        UNKNOWN_SETTINGS_TYPE
    }

    /// <summary>
    /// Интерфейс, который должен реализовывать каждый плагин
    /// </summary>
    public interface Interface
    {
        /// <summary>
        /// Уникальный индетификатор плагина.
        /// </summary>
        uint ID { get; }

        /// <summary>
        /// Уникальное имя плагина. Используется как для отображения плагина пользователю, так и для внутренних целей.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Список возможностей плагина. Если плагин ничего не умеет - Null.
        /// </summary>
        Capabilities Capabilities { get; }

        /// <summary>
        /// Возвращает форму, используемую пользователем для настройки плагина. Null - если такая форма не требуется.
        /// В отличие от метода CustomSettingsWindow(), данный метод возвращаем форму только с самыми необходимыми настройками плагина и используется для основного режима работы.
        /// Когда форма драйверу больше не нужна, указатель на нее затирается, явное уничтожение не производится.
        /// </summary>
        UserControl SimpleSettingsWindow { get; }

        /// <summary>
        /// Возвращает форму, используемую пользователем для настройки плагина. Null - если такая форма не требуется.
        /// В отличие от метода SimpleSettingsWindow(), данный метод возвращаем форму со всеми возможными настройками плагина и используется для экспертного режима работы.
        /// Когда форма драйверу больше не нужна, указатель на нее затирается, явное уничтожение не производится.
        /// </summary>
        UserControl CustomSettingsWindow { get; }

        /// <summary>
        /// Возвращает форму, используемую пользователем для интерактивной работы с плагином. Null - если такая форма не требуется.
        /// Когда форма драйверу больше не нужна, указатель на нее затирается, явное уничтожение не производится.
        /// </summary>
        UserControl RunWindow { get; }

        /// <summary>
        /// Возвращает форму, используемую пользователем для обработки результатов работы плагина. Null - если такая форма не требуется.
        /// Когда форма драйверу больше не нужна, указатель на нее затирается, явное уничтожение не производится.
        /// </summary>
        UserControl ResultWindow { get; }

        /// <summary>
        /// Список задач, выполняемых в ходе работы плагина. Null - если список задач неизвестен.
        /// </summary>
        List<Monitor.Tasks.Task> Tasks { get; }

        /// <summary>
        /// Список задач, выполняемых в ходе генерации отчётов по плагину. Null - если список задач неизвестен
        /// </summary>
        List<Monitor.Tasks.Task> TasksReport { get; }

        /// <summary>
        /// Метод инициализации плагина.
        /// </summary>
        /// <param name="storage">Хранилище. Никогда не Null.</param>
        void Initialize(Storage storage);

        /// <summary>
        /// Метод, в рамках которого небходимо задать настройки плагина в соответствии с указанным буфером настроек.
        /// Настройкам, которых нет в буфере, должны устанавливаться значения по умолчанию.
        /// </summary>
        /// <param name="settingsBuffer">Буфер настроек плагина, получаемый из Хранилища. Никогда не Null.</param>
        void LoadSettings(IBufferReader settingsBuffer);

        /// <summary>
        /// Метод для задания настройки для работы плагина в основном режиме.
        /// В рамках данного метода необходимо:
        /// 1. Задать часть настроек плагина в соответствии с переданной строкой, хранящейся в конфигурационном файле приложения.
        /// 2. Проигнорировать часть настроек плагина, задаваемых через форму с самыми необходимыми настройками плагина.
        /// 3. Оставшиеся настройки плагина задать принудительно в соответствии с идеей функционирования плагина в основном режиме.
        /// </summary>
        /// <param name="settingsString">Строка настроек. Никогда не Null.</param>
        void SetSimpleSettings(string settingsString);

        /// <summary>
        /// Метод, в рамках которого небходимо сохранить настройки плагина в указанный буфер настроек.
        /// <param name="settingsBuffer">Буфер настроек плагина, помещаемый в Хранилище. Никогда не Null.</param>
        /// </summary>
        void SaveSettings(IBufferWriter settingsBuffer);

        /// <summary>
        /// Сохранение результатов работы плагина в Хранилище.
        /// </summary>
        void SaveResults();

        /// <summary>
        /// Проверка правильно ли заданы настройки плагина пользователем.
        /// </summary>
        /// <param name="message">Сообщение, необходимое для отображения, в случае, если настройки введены некорректно.</param>
        /// <returns>true - если настройки введены корректно, иначе false.</returns>
        bool IsSettingsCorrect(out string message);

        /// <summary>
        /// Запуск выполнения плагина.
        /// </summary>
        void Run();

        /// <summary>
        /// Запуск генерации отчетов по плагину.
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до директории для формирования отчётов по плагину. Гарантируется, что директория существует. Никогда не Null.</param>
        void GenerateReports(string reportsDirectoryPath);

        /// <summary>
        /// Реализация магкой остановки плагина.
        /// </summary>
        void StopRunning();
    }

    /// <summary>
    /// Реализация плагина по-умолчанию. Все методы делают максимально ничего. При наследовании требуется реализовать только те методы, которые нужны.
    /// </summary>
    public abstract class DefaultPlugin : Interface
    {
        /// 
        public virtual Capabilities Capabilities { get { return Capabilities.NONE; } }

        ///
        public virtual UserControl CustomSettingsWindow { get { return null; } }

        ///
        public abstract uint ID { get; }

        ///
        public abstract string Name { get; }

        ///
        public virtual UserControl ResultWindow { get { return null; } }

        ///
        public virtual UserControl RunWindow { get { return null; } }

        ///
        public virtual UserControl SimpleSettingsWindow { get { return null; } }

        ///
        public virtual List<Monitor.Tasks.Task> Tasks { get { return new List<Monitor.Tasks.Task>(); } }

        ///
        public virtual List<Monitor.Tasks.Task> TasksReport { get { return new List<Monitor.Tasks.Task>(); } }

        ///
        public virtual void GenerateReports(string reportsDirectoryPath) { }

        ///
        public virtual void Initialize(Storage storage) { }

        ///
        public virtual bool IsSettingsCorrect(out string message) { message = String.Empty; return true; }

        ///
        public virtual void LoadSettings(IBufferReader settingsBuffer) { }

        ///
        public virtual void Run() { }

        ///
        public virtual void SaveResults() { }

        ///
        public virtual void SaveSettings(IBufferWriter settingsBuffer) { }

        ///
        public virtual void SetSimpleSettings(string settingsString) { }

        ///
        public virtual void StopRunning() { }

        /// <summary>
        /// Для наследования классами, работающими только с настройками плагина для работы.
        /// </summary>
        [Obsolete("This class is not tested.", true)]
        public abstract class PluginSettingsClass
        {
            private static Dictionary<int, PluginSettingsClass> pluginSettings = new Dictionary<int, PluginSettingsClass>();

            /// <summary>
            /// Получить настройки плагина. Если настройки не были заданы - возвращает null.
            /// </summary>
            public static PluginSettingsClass PluginSettings
            {
                get
                {
                    var tId = System.Threading.Thread.CurrentThread.ManagedThreadId;
                    if (!pluginSettings.ContainsKey(tId))
                        return null;
                    return pluginSettings[tId];
                }

                set
                {
                    var tId = System.Threading.Thread.CurrentThread.ManagedThreadId;
                    if (!pluginSettings.ContainsKey(tId))
                        pluginSettings.Add(tId, null);
                    pluginSettings[tId] = value;
                }
            }
        }


    }
}
