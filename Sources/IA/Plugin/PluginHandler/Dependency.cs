﻿namespace IA.Plugin
{
    /// <summary>
    /// Класс, реализующий зависимость между двумя плагинами.
    /// </summary>
    internal class Dependency
    {
        /// <summary>
        /// Является ли зависимость опциональной?
        /// </summary>
        public bool IsOptional { get; private set; }

        /// <summary>
        /// Запускается ли плагин-зависимость до или после зависимого?
        /// </summary>
        public bool IsRunAfter { get; private set; }
     
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="isOptional">Является ли зависимость опциональной?</param>
        /// <param name="isRunAfter">Запускается ли плагин-зависимость до или после зависимого?</param>
        internal Dependency(bool isOptional, bool isRunAfter)
        {
            this.IsOptional = isOptional;
            this.IsRunAfter = isRunAfter;
        }
    }
}
