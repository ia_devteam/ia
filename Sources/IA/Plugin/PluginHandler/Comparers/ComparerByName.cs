﻿using System;
using System.Collections.Generic;

namespace IA.Plugin.Comparers
{
    /// <summary>
    /// Класс, реализующий сравнение двух плагинов в рамках их имён
    /// </summary>
    public class ComparerByName : IComparer<Handler>
    {
        /// <summary>
        /// Сравнение двух плагинов в рамках имён
        /// </summary>
        /// <param name="x">Плагин 1</param>
        /// <param name="y">Плагин 2</param>
        /// <returns></returns>
        public int Compare(Handler x, Handler y)
        {
            //Проверка на разумность
            if (x == null || y == null)
                throw new Exception("Один из плагинов для сравнения не определён.");

            return x.Name.CompareTo(y.Name);
        }
    }
}
