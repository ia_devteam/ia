﻿using System;
using System.Collections.Generic;
using System.Linq;

using IA.Extensions;

namespace IA.Plugin
{
    /// <summary>
    /// Класс, реализующий набор плагинов без повторений
    /// </summary>
    public class Handlers : List<Handler>
    {
        public static class settingsChecker
        {
            static Dictionary<uint, bool> PluginsSetedUp = new Dictionary<uint, bool>();

            public static void Reset()
            {
                PluginsSetedUp.Clear();
            }

            public static void Add(uint ID, bool value)
            {
                PluginsSetedUp.Add(ID, value);
            }

            public static bool isSettingsCorrect(Plugin.Handler plugin, out string Message, bool fullCheck)
            {
                string Mes = "";
                if (fullCheck)
                {
                    PluginsSetedUp[plugin.ID] = plugin.IsSettingsCorrect(out Mes);
                }
                Message = Mes;
                return PluginsSetedUp[plugin.ID];
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public Handlers() 
            : base()
        {
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="plugins">Последовательность плагинов</param>
        public Handlers(IEnumerable<Handler> plugins)
            : base(plugins)
        {
        }

        /// <summary>
        /// Получить плагин из списка плагинов по идентификтору
        /// </summary>
        /// <param name="pluginID">Идентификатор плагина</param>
        /// <returns>Плагин. Не может быть Null.</returns>
        public Handler GetOneByID(uint pluginID)
        {
            Handler ret = null;

            try
            {
                //Проверка на разумность
                if (pluginID == 0)
                    throw new Exception("Неверный идентификатор плагина: " + pluginID + ".");

                //Попытка получить плагин из списка
                ret = this.FirstOrDefault(h => h.ID == pluginID);
                
                //Если ничего не нашли
                if (ret == null)
                    throw new Exception("Плагин с идентификатором " + pluginID + " отсутствует.");
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при получении плагина по идентификатору из списка плагинов.");
            }

            return ret;
        }

        /// <summary>
        /// Получить подсписок плагинов из списка плагинов по списку идентификаторов
        /// </summary>
        /// <param name="pluginIDs">Список идентификаторов</param>
        /// <returns>Список плагинов. Не может быть Null.</returns>
        public Handlers GetSomeByIDs(IEnumerable<uint> pluginIDs)
        {
            Handlers ret = new Handlers();

            try
            {
                //Проверка на разумность
                if (pluginIDs == null)
                    throw new ArgumentNullException("pluginIDs", "Список идентификаторов плагинов не определён.");

                //Проходим по всем указанным идентификаторам плагинов
                pluginIDs.ForEach(pluginID => ret.Add(this.GetOneByID(pluginID)));
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при получении подсписка плагинов из списка плагинов по списку идентификаторов.");
            }

            return ret;
        }
    }
}
