﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Store;

using IOController;

namespace IA.Plugin
{
    /// <summary>
    /// Класс, описывающий держатель отдельного плагина.
    /// Данная часть представляет собой обёртку над интерфейсом плагина.
    /// </summary>
    public partial class Handler : IComparable
    {
        #region Поля
        /// <summary>
        /// Плагин
        /// </summary>
        private IA.Plugin.Interface plugin = null;
        #endregion

        private uint id = 0;
        /// <summary>
        /// Идентификатор плагина
        /// </summary>
        public uint ID
        {
            get
            {
                if (id == 0)
                {
                    try
                    {
                        id = this.plugin.ID;
                    }
                    catch (Exception ex)
                    {
                        //Формируем исключение
                        throw new Exception("Получение идентификатора плагина <" + this.Name + "> завершилось с ошибкой.", ex);
                    }
                }

                return id;
            }
        }

        private string name = null;
        /// <summary>
        /// Имя плагина
        /// </summary>
        public string Name
        {
            get
            {
                if (name == null)
                {
                    try
                    {
                        name = this.plugin.Name;
                    }
                    catch (Exception ex)
                    {
                        //Формируем исключение
                        throw new Exception("Получение имени плагина завершилось с ошибкой.", ex);
                    }
                }

                return name;
            }
        }

        private Capabilities? capabilities = null;
        /// <summary>
        /// Список возможностей плагина
        /// </summary>
        public Capabilities Capabilities
        {
            get
            {
                if (capabilities == null)
                {
                    try
                    {
                        capabilities = this.plugin.Capabilities;
                    }
                    catch (Exception ex)
                    {
                        //Формируем исключение
                        throw new Exception("Получение списка возможностей плагина завершилось с ошибкой.", ex);
                    }
                }

                return (Capabilities)capabilities;
            }
        }

        /// <summary>
        /// Окно основных настроек плагина
        /// </summary>
        public UserControl SimpleSettingsWindow
        {
            get
            {
                try
                {
                    return this.plugin.SimpleSettingsWindow;
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new Exception("Получение окна основных настроек плагина <" + this.Name + "> завершилось с ошибкой.", ex);
                }
            }
        }

        /// <summary>
        /// Окно всех настроек плагина
        /// </summary>
        public UserControl CustomSettingsWindow
        {
            get
            {
                try
                {
                    return this.plugin.CustomSettingsWindow;
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new Exception("Получение окна всех настроек плагина <" + this.Name + "> завершилось с ошибкой.", ex);
                }
            }
        }

        /// <summary>
        /// Окно выполнения плагина
        /// </summary>
        public UserControl RunWindow
        {
            get
            {
                try
                {
                    return this.plugin.RunWindow;
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new Exception("Получение окна выполнения плагина <" + this.Name + "> завершилось с ошибкой.", ex);
                }
            }
        }

        /// <summary>
        /// Окно результатов работы плагина
        /// </summary>
        public UserControl ResultWindow
        {
            get
            {
                try
                {
                    return this.plugin.ResultWindow;
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new Exception("Получение окна результатов работы плагина <" + this.Name + "> завершилось с ошибкой.", ex);
                }
            }
        }

        /// <summary>
        /// Список задач плагина в рамках выполнения
        /// </summary> 
        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                try
                {
                    return this.plugin.Tasks;
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new Exception("Получение списка задач плагина <" + this.Name + "> в рамках выполнения завершилось с ошибкой.", ex);
                }
            }
        }

        /// <summary>
        /// Список задач плагина в рамках генерации отчётов
        /// </summary> 
        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                try
                {
                    return this.plugin.TasksReport;
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new Exception("Получение списка задач плагина <" + this.Name + "> в рамках генерации отчётов завершилось с ошибкой.", ex);
                }
            }
        }

        /// <summary>
        /// Инициализация плагина
        /// </summary>
        /// <param name="storage">Хранилище</param>
        public void Initialize(Storage storage)
        {
            try
            {
                //Проверка на разумность
                if (storage == null)
                    throw new Exception("Хранилище не определено.");

                //Инициализация плагина
                this.plugin.Initialize(storage);

                //Задаём изначальный результат выполнения плагина
                this.RunResult = ExecutionResult.NO;

                //Задаём изначальный результат генерации отчётов
                this.ReportGenerationResult = ExecutionResult.NO;

                //Изначально работа с окном результатов плагина не проводилась
                this.IsCompleted = false;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Инициализация плагина <" + this.Name + "> завершилась с ошибкой.", ex);
            }
        }

        /// <summary>
        /// Установка настроек плагина в соответсвии с настройками, сохранёнными в Хранилище
        /// </summary>
        /// <param name="storage">Хранилище</param>
        public void LoadSettings(Storage storage)
        {
            try
            {
                //Проверка на разумность
                if (storage == null)
                    throw new Exception("Хранилище не определено.");

                //Получаем буфер настроек плагина из Хранилища
                Store.Table.IBufferReader buffer = storage.pluginSettings.LoadSettings(this.ID);

                //Если сохранённых настроек нет, ничего не делаем
                if (buffer == null)
                    return;

                //Устанавливаем настройки в соответствии с буфером настроек
                this.plugin.LoadSettings(buffer);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Установка настроек плагина <" + this.Name + "> в соответствии с настройками, сохранёнными в Хранилище, завершилась с ошибкой.", ex);
            }
        }

        /// <summary>
        /// Задание настроек плагина в основном режиме
        /// </summary>
        /// <param name="settingsString">Строка настроек</param>
        public void SetSimpleSettings(string settingsString)
        {
            try
            {
                //Проверка на разумность
                if (settingsString == null)
                    throw new Exception("Строка настроек не определена.");

                //Устанавливаем настройки в соответствии с буфером настроек
                this.plugin.SetSimpleSettings(settingsString);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Установка настроек плагина <" + this.Name + "> в соответствии со строкой настроек завершилась с ошибкой.", ex);
            }
        }

        /// <summary>
        /// Проверка настроек плагина
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns>Корректно ли настроен плагин?</returns>
        public bool IsSettingsCorrect(out string message)
        {
            bool result = false;
            message = String.Empty;

            try
            {
                result = this.plugin.IsSettingsCorrect(out message);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Проверка настроек плагина <" + this.Name + "> завершилась с ошибкой.", ex);
            }

            return result;
        }

        /// <summary>
        /// Cохранить настройки плагина в Хранилище
        /// </summary>
        public void SaveSettings(Storage storage)
        {
            try
            {
                //Проверка на разумность
                if (storage == null)
                    throw new Exception("Хранилище не определено.");

                //Формируем буфер для сохранения настроек
                Store.Table.IBufferWriter buffer = Store.Table.WriterPool.Get();

                //Сохраняем настройки в буфер
                this.plugin.SaveSettings(buffer);

                //Помещаем буфер в Хранилище
                storage.pluginSettings.SaveSettings(this.ID, buffer);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Сохранение настроек плагина <" + this.Name + "> завершилось с ошибкой.", ex);
            }
        }

        /// <summary>
        /// Запустить плагин
        /// </summary>
        public void Run()
        {
            try
            {

                //Выполняем плагин
                this.plugin.Run();

                //Задаём состояние выполнения плагина
                this.RunResult = ExecutionResult.SUCCESS;
            }
            catch(System.Threading.ThreadAbortException)
            {/* Игнорируем исключение, возникшее при попытке убить процесс нажатием на кнопку "Стоп"*/ }
            catch (Exception ex)
            {
                //Задаём состояние выполнения плагина
                this.RunResult = ExecutionResult.ERROR;

                //Формируем исключение
                throw new Exception("Плагин <" + this.Name + "> завершился с ошибкой.", ex);
            }
        }

        /// <summary>
        /// Сохранить результаты работы плагина в Хранилище
        /// </summary>
        public void SaveResults()
        {
            try
            {
                this.plugin.SaveResults();
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Сохранение результатов работы плагина <" + this.Name + "> завершилось с ошибкой.", ex);
            }
        }

        /// <summary>
        /// Сгенерировать отчёты по плагину
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь до директории для формирования отчётов.</param>
        public void GenerateReports(string reportsDirectoryPath)
        {
            try
            {
                //Проверка на разумность
                if (reportsDirectoryPath == null)
                    throw new Exception("Путь до директории для формирования отчётов не задан.");

                //Получаем путь к поддиректории для формирования отчётов по текущему плогину
                string pluginReportsDirectoryPath = System.IO.Path.Combine(reportsDirectoryPath, this.plugin.Name);

                //Если директория для формирования отчётов по плагину не существет, создаём её
                if (!DirectoryController.IsExists(pluginReportsDirectoryPath))
                    DirectoryController.Create(pluginReportsDirectoryPath);

                //Генерируем отчёт
                this.plugin.GenerateReports(pluginReportsDirectoryPath);

                //Задаём результат генерации отчётов
                this.ReportGenerationResult = ExecutionResult.SUCCESS;
            }
            catch (Exception ex)
            {
                //Задаём результат генерации отчётов
                this.ReportGenerationResult = ExecutionResult.ERROR;

                //Формируем исключение
                throw new Exception("Генерация отчётов по плагину <" + this.Name + "> завершилась с ошибкой.", ex);
            }
        }

        public int CompareTo(object obj)
        {
            return (int)(ID - ((Handler)obj).ID);
        }
    }
}
