﻿using System;
using System.ComponentModel;

using IA.Extensions;

using PluginConfiguration = IA.Config.Objects.Plugin;

namespace IA.Plugin
{
    /// <summary>
    /// Класс, описывающий держатель отдельного плагина
    /// </summary>
    public partial class Handler : IEquatable<Handler>
    {
        /// <summary>
        /// Возможные результаты выполнения какого-либо процесса
        /// </summary>
        public enum ExecutionResult
        {
            /// <summary>
            /// Процесс не был запущен
            /// </summary>
            NO,
            /// <summary>
            /// Процесс успешно завершился
            /// </summary>
            SUCCESS,
            /// <summary>
            /// Процесс завершился с ошибкой
            /// </summary>
            ERROR
        }

        /// <summary>
        /// Список самых распространённых исключений при работе с держателями плагинов
        /// </summary>
        public enum Exceptions
        {
            /// <summary>
            /// Обнаружен неизвестный результат выполненния процесса
            /// </summary>
            [Description("Обнаружен неизвестный результат выполненния процесса.")]
            UNKNOWN_EXECUTION_RESULT
        }

        #region Поля
        /// <summary>
        /// Конфигурация плагина
        /// </summary>
        private PluginConfiguration configuration = null;
        #endregion

        #region Свойства
        /// <summary>
        /// Категория плагина
        /// </summary>
        public string Category { get; private set; }

        /// <summary>
        /// Возможность многократного использования плагина
        /// </summary>
        public bool IsReusable { get; private set; }

        private ExecutionResult runResult;
        /// <summary>
        /// Результат выполнения плагина
        /// </summary>
        public ExecutionResult RunResult
        {
            get
            {
                return runResult;
            }
            set
            {
                //Если ничего не поменялось, ничего не делаем
                if (runResult == value)
                    return;

                //Сохраняем переданное значение
                runResult = value;

                //Формируем событие
                if (PluginRunResultChangedEvent != null)
                    PluginRunResultChangedEvent(value);
            }
        }

        private ExecutionResult reportGenerationResult;
        /// <summary>
        /// Результат генерации отчётов по плагину
        /// </summary>
        public ExecutionResult ReportGenerationResult
        {
            get
            {
                return reportGenerationResult;
            }
            set
            {
                //Если ничего не поменялось, ничего не делаем
                if (reportGenerationResult == value)
                    return;

                //Сохраняем переданное значение
                reportGenerationResult = value;
            }
        }

        /// <summary>
        /// Завершён ли плагин? (Заврешена ли работа с окном результатов плагина?)
        /// </summary>
        public bool IsCompleted { get; set; }

        /// <summary>
        /// Свойство - Готов ли плагин к показу окна со всевозможными настройками
        /// </summary>
        public bool IsReadyToShowCustomSettingsWindow
        {
            get
            {
                //Если у плагина нет окна с настройками
                if (!this.IsCapableTo(Capabilities.CUSTOM_SETTINGS_WINDOW))
                    return false;

                //Если плагин завершён и не может быть перезапущен
                if (this.RunResult == IA.Plugin.Handler.ExecutionResult.SUCCESS && !this.IsCapableTo(Capabilities.RERUN))
                    return false;

                return true;
            }
        }

        /// <summary>
        /// Свойство - Готов ли плагин к запуску?
        /// </summary>
        public bool IsReadyToRun
        {
            get
            {
                //Если плагин не настроен, запускать нельзя
                string message;
                if (!Handlers.settingsChecker.isSettingsCorrect(this, out message, false))
                    return false;

                //Если плагин был завершён корректно
                if (this.RunResult == ExecutionResult.SUCCESS)
                    //Проверяем можно ли его перезапускать и в зависимости от этого даём такую возможность
                    return this.IsCapableTo(Capabilities.RERUN);
                //Если плагин не был завершён корректно
                else
                    //Не даём запустить плагин, пока не запущены все обязательные плагины (до)
                    return this.IsAllCompulsoryAndBeforeCompleted;
            }
        }

        /// <summary>
        /// Свойство - Готов ли плагин к генерации отчёта
        /// </summary>
        public bool IsReadyToGenerateReport
        {
            get
            {
                //Если генерация отчётов не поддерживается плагином - не готов
                if (!this.IsCapableTo(Capabilities.REPORTS))
                    return false;

                //Если не завершился корректно, не готов
                if (this.RunResult != ExecutionResult.SUCCESS)
                    return false;

                return true;
            }
        }

        /// <summary>
        /// Свойство - Готов ли плагин к показу своего результата
        /// </summary>
        public bool IsReadyToShowResultWindow
        {
            get
            {
                //Готов, если он успешно завершён и имеет форму результата
                return this.RunResult == ExecutionResult.SUCCESS && this.IsCapableTo(Capabilities.RESULT_WINDOW);
            }
        }
        #endregion

        #region Делегаты и события
        /// <summary>
        /// Делегат для события - Состояние плагина было изменено (в плане запуска)
        /// </summary>
        /// <param name="runResult"></param>
        public delegate void PluginRunResultChangedEventHandler(ExecutionResult runResult);

        /// <summary>
        /// Событие - Состояние плагина было изменено (в плане запуска)
        /// </summary>
        public event PluginRunResultChangedEventHandler PluginRunResultChangedEvent;
        #endregion

        #region Реализуемые интерфейсы
        #region IEquatable
        /// <summary>
        /// Сравнение двух плагинов
        /// </summary>
        /// <param name="other">Плагин для сравнения</param>
        /// <returns></returns>
        public bool Equals(Handler other)
        {
            return other != null && this.id == other.id;
        }
        #endregion
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="plugin">Плагин</param>
        /// <param name="configuration">Конфигурация плагина</param>
        internal Handler(IA.Plugin.Interface plugin, PluginConfiguration configuration)
        {
            try
            {
                if (plugin == null)
                    throw new ArgumentNullException("plugin", "Плагин не определён.");

                if (configuration == null)
                    throw new ArgumentNullException("plugin", "Конфигурация плагина не определена.");

                if (plugin.ID != configuration.ID)
                    throw new Exception("Идентификатор плагина не совпадает с идентификатором переданной конфигурации.");

                //Запоминаем сам плагин
                this.plugin = plugin;

                //Запоминаем конфигурацию
                this.configuration = configuration;

                //Задаём категорию плагина
                this.Category = configuration.Category;

                //Задаём возможность многократного использования плагина
                this.IsReusable = configuration.IsReusable;
            }
            catch (Exception ex)
            {
                //Пробрасываем ошибку
                ex.ReThrow("Ошибка при создании экземпляра оболочки плагина.");
            }
        }

        /// <summary>
        /// Обладает ли плагин указанными возможностями?
        /// </summary>
        /// <param name="capabilities"></param>
        /// <returns></returns>
        public bool IsCapableTo(Capabilities capabilities)
        {
            return (this.Capabilities & capabilities) == capabilities;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
