﻿/*
 * Главный интерфейс
 * Файл PluginsHandler.cs
 * 
 * ЗАО "РНТ" (с)
 * Разработчики: Лошкарёв, Юхтин
 * 
 * Класс, осуществляющий безопасную работу со списком плагинов и групп
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Linq;

using IA.Extensions;

using IOController;

using PluginConfiguration = IA.Config.Objects.Plugin;

namespace IA.Plugin
{
    /// <summary>
    /// Класс, описывающий список доступных плагинов
    /// </summary>
    public static class HandlersInitial
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Список доступных плагинов";

        /// <summary>
        /// Словарь, реализующий список доступных плагинов
        /// </summary>
        private static Dictionary<uint, Handler> Plugins = new Dictionary<uint, Handler>();

        /// <summary>
        /// Загрузить все доступные плагины из директории
        /// </summary>
        /// <param name="directoryPath">Путь до директории</param>
        public static void Load(string directoryPath)
        {
            try
            {
                //Проверка на разумность
                if (directoryPath == null)
                    throw new Exception("Директория с плагинами не определена.");

                //Если директория с плагинами не существует, ничего не делаем
                if (!DirectoryController.IsExists(directoryPath))
                    throw new Exception("Директория с плагинами <" + directoryPath + "> не существует.");

                //Идём по всем файлам с расширением .dll в директории с плагинами
                foreach (string file in DirectoryController.GetFiles(directoryPath).Where(path => Path.GetExtension(path) == ".dll"))
                {
                    try
                    {
                        int i = 0;
                        //Идём по всем типам, определённым в сборке
                        foreach (Type type in Assembly.LoadFile(Path.GetFullPath(file)).GetTypes())
                            if (type.BaseType != null && type.GetInterface("IA.Plugin.Interface") != null && !type.IsAbstract)
                            {
                                //Получаем плагин
                                IA.Plugin.Interface plugin = (IA.Plugin.Interface)Activator.CreateInstance(type);

                                try
                                {
                                    //Узнаём есть ли конфигурация для данного плагина
                                    if (!Config.Controller.Plugins.ContainsID(plugin.ID))
                                        throw new Exception("Отсутствует конфигурация для плагина.");

                                    //Получаем конфигурацию плагина
                                    PluginConfiguration configuration = Config.Controller.Plugins[plugin.ID];

                                    //Проверяем совпадение имён
                                    if (plugin.Name != configuration.Name)
                                    {
                                        //Формируем сообщение
                                        string message = new string[]
                                        {
                                            "Имя плагина не сопадает с его именем в конфигурационном файле приложения.",
                                            "Имя плагина: <" + plugin.Name + ">.",
                                            "Имя плагина в конфигурационном файле приложения: <" + configuration.Name + ">."
                                        }.ToMultiLineString();

                                        //Отображаем предупреждение
                                        Monitor.Log.Warning(ObjectName, message, true);
                                    }

                                    //Получаем держатель плагина
                                    Handler handler = new Handler(plugin, configuration);

                                    //Если плагин с таким идентифиатором уже существует
                                    if (Plugins.ContainsKey(handler.ID))
                                        throw new Exception("Плагин с таким идентификатором уже существует.");

                                    //Добавляем его в список
                                    Plugins.Add(handler.ID, handler);
                                }
                                catch (Exception ex)
                                {
                                    //Формируем сообщение
                                    string message = new string[]
                                    {
                                        "Не удалось загрузить плагин.",
                                        ex.Message,
                                        "Идентификатор плагина: " + plugin.ID,
                                        "Имя плагина: " + plugin.Name,
                                        "Имя файла плагина: " + Path.GetFileName(file),
                                    }.ToMultiLineString();

                                    //Отображаем ошибку
                                    Monitor.Log.Error(ObjectName, message, true);

                                    //Продолжаем загрузку
                                    continue;
                                }
                            }
                    }
                    catch (BadImageFormatException)
                    {
                        continue;
                    }
                    catch (ReflectionTypeLoadException)
                    {
                        continue;
                    }
                }
                //Инициализируем зависимости всех плагинов
                Plugins.Values.ForEach(h => h.InitializeDependencies());

                //Инициализируем абстрактные последовательности для мультизапуска всех плагинов
                Plugins.Values.ForEach(h => h.InitializeAbstractMultiRunSequence());
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при загрузке плагинов.");
            }
        }

        /// <summary>
        /// Получить плагин из списка доступных плагинов по идентификтору
        /// </summary>
        /// <param name="pluginID">Идентификатор плагина</param>
        /// <param name="isMissAllowed">Допускается ли отсутствие плагина?</param>
        /// <returns>Плагин. Null, если искомый плагин не был найден.</returns>
        public static Handler GetOneByID(uint pluginID, bool isMissAllowed = false)
        {
            Handler ret = null;

            try
            {
                //Проверка на разумность
                if (pluginID == 0)
                    throw new Exception("Неверный идентификатор плагина: " + pluginID + ".");

                //Попытка получить плагин из списка
                if (!HandlersInitial.Plugins.TryGetValue(pluginID, out ret) && !isMissAllowed)
                    throw new Exception("Плагин с идентификатором " + pluginID + " отсутствует.");
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при получении плагина по идентификатору из списка доступных плагинов.");
            }

            return ret;
        }

        /// <summary>
        /// Получить список доступных плагинов
        /// </summary>
        /// <returns>Список всех плагинов. Не может быть Null.</returns>
        public static Handlers GetAll()
        {
            Handlers ret = new Handlers();

            try
            {
                //Добавляем все плагины из коллекции
                ret.AddRange(HandlersInitial.Plugins.Values);
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при получении списка доступных плагинов.");
            }

            return ret;
        }

        /// <summary>
        /// Получить список плагинов из списка доступных плагинов по списку идентификаторов
        /// </summary>
        /// <param name="pluginIDs">Список идентификаторов</param>
        /// <param name="isMissAllowedCondition">Условие при котором допускается пропускать ненайденные плагины</param>
        /// <returns>Список плагинов. Не может быть Null.</returns>
        public static Handlers GetSomeByIDs(IEnumerable<uint> pluginIDs, Func<uint, bool> isMissAllowedCondition = null)
        {
            Handlers ret = new Handlers();

            try
            {
                //Проверка на разумность
                if (pluginIDs == null)
                    throw new ArgumentNullException("pluginIDs", "Список идентификаторов плагинов не определён.");

                //Проходим по всем указанным идентификаторам плагинов
                pluginIDs.ForEach(
                    pluginID =>
                    {
                        //Ищем плагин в коллекции
                        Handler handlerToAdd =
                            HandlersInitial.GetOneByID(
                                pluginID, 
                                isMissAllowedCondition != null && isMissAllowedCondition(pluginID)
                            );

                        //Если плагин не нашли и это допускается, то ничего не делаем
                        if(handlerToAdd == null)
                            return;

                        //Добавляем плагин в результат
                        ret.Add(handlerToAdd);
                    }
                );
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при получении списка плагинов по списку идентификаторов из из списка доступных плагинов.");
            }

            return ret;
        }

        /// <summary>
        /// Получить все плагины из списка доступных плагинов, удовлетворяющие указанному условию
        /// </summary>
        /// <param name="condition">Условие</param>
        /// <returns>Список плагинов. Не может быть Null.</returns>
        public static Handlers GetSomeByCondition(Func<Handler, bool> condition)
        {
            Handlers ret = null;

            try
            {
                ret = new Handlers(HandlersInitial.Plugins.Values.Where(p => condition(p)));
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при получении списка плагинов из списка доступных плагинов, удовлетворяющих указанному условию.");
            }

            return ret;
        }

        /// <summary>
        /// Прокси класс для сохранения статуса выполнения плагинов при выгрузке.
        /// </summary>
        public class PluginRunInfo
        {
            public PluginRunInfo() { }
            [System.Xml.Serialization.XmlAttribute]
            public uint ID { get; set; }
            [System.Xml.Serialization.XmlAttribute]
            public bool IsCompleted { get; set; }
            [System.Xml.Serialization.XmlAttribute]
            public IA.Plugin.Handler.ExecutionResult RunResult { get; set; }

            private static PluginRunInfo FromHandler(Handler h)
            {
                return new PluginRunInfo() { ID = h.ID, IsCompleted = h.IsCompleted, RunResult = h.RunResult };
            }

            /// <summary>
            /// Получить дамп информации о выполненных плагинах (статус выполнения и результата).
            /// </summary>
            /// <returns></returns>
            public static IEnumerable<PluginRunInfo> DumpPluginRunInfo()
            {
                foreach (var h in HandlersInitial.GetAll().Where(h => h.RunResult != Handler.ExecutionResult.NO))
                    yield return PluginRunInfo.FromHandler(h);
            }

            /// <summary>
            /// По созданному ранее дампу восстановить состояние плагинов.
            /// </summary>
            /// <param name="input"></param>
            public static void SynchronizeFromDump(IEnumerable<PluginRunInfo> input)
            {
                foreach (var pri in input)
                {
                    var h = HandlersInitial.GetOneByID(pri.ID);
                    h.IsCompleted = pri.IsCompleted;
                    h.RunResult = pri.RunResult;
                }
            }
        }
    }
}
