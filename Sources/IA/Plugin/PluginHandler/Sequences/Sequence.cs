﻿using System;

namespace IA.Plugin.Sequences
{
    /// <summary>
    /// Базовый класс, реализующий последовательность плагинов.
    /// Последовательность плагинов отличается от обычного списка плагинов тем, что у неё есть корневой элемент.
    /// </summary>
    public class Sequence : Handlers
    {
        /// <summary>
        /// Держатель корневого плагина, по которому формируется последовательность
        /// </summary>
        public Handler RootHandler = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="rootHandler">Держатель корневого плагина</param>
        internal Sequence(Handler rootHandler)
            : base()
        {
            //Проверка на разумность
            if (rootHandler == null)
                throw new Exception("Корневой плагин не определён.");

            //Запоминаем корневой держатель плагина
            this.RootHandler = rootHandler;
        }
    }
}
