﻿using System.Linq;

namespace IA.Plugin.Sequences
{
    /// <summary>
    /// Класс реализующий определённую последовательность для мультизапуска отдельного плагина
    /// </summary>
    public class SpecificMultiRunSequence : Sequence
    {
        #region Свойства
        /// <summary>
        /// Готов ли плагин к показу мультиокна всевозможных настроек?
        /// </summary>
        public bool IsReadyToShowMultiCustomSettingsWindow
        {
            get
            {
                //Если корневой плагин завершён и не может быть перезапущен
                if (this.RootHandler.RunResult == Handler.ExecutionResult.SUCCESS && !this.RootHandler.IsCapableTo(Capabilities.RERUN))
                    return false;

                //Если у всех плигнов в последовательности нет окна со всеми возможными настройками
                if (this.All(h => !h.IsCapableTo(Capabilities.CUSTOM_SETTINGS_WINDOW)))
                    return false;

                return true;
            }
        }

        /// <summary>
        /// Готов ли плагин к мультизапуску?
        /// </summary>
        public bool IsReadyToMultiRun
        {
            get
            {
                //Если корневой плагин уже был выполнен и не допускает перезапуска, то мультизапуск запрещён
                if (this.RootHandler.RunResult == Handler.ExecutionResult.SUCCESS && !this.RootHandler.IsCapableTo(Capabilities.RERUN))
                    return false;

                //Если хоть один плагин в последовательности некорректно настроен, то мультизапуск запрещён
                string message;
                if (this.Any(h => !Handlers.settingsChecker.isSettingsCorrect(h, out message, false)))
                    return false;

                return true;
            }
        }

        /// <summary>
        /// Готов ли плагин к мультигенерации отчётов?
        /// </summary>
        public bool IsReadyToMultiGenerateReport
        {
            get
            {
                //Если генерация отчётов не поддерживается корневым плагином - мультизапуск генерации отчётов невозможен
                if (!this.RootHandler.IsCapableTo(Capabilities.REPORTS))
                    return false;

                //Если хоть один плагин в последовательности не был завершён корректно, то мультизапуск генерации отчётов невозможен
                if (this.Any(h => h.RunResult != Handler.ExecutionResult.SUCCESS))
                    return false;

                return true;
            }
        } 
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="rootHandler">Держатель корневого плагина</param>
        internal SpecificMultiRunSequence(Handler rootHandler)
            : base(rootHandler)
        {
        }
    }
}
