﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IA.Plugin.Sequences
{
    /// <summary>
    /// Класс реализующий абстрактную последовательность для мультизапуска отдельного плагина
    /// </summary>
    public class AbstractMultiRunSequence : Sequence
    {
        /// <summary>
        /// Таблица для временного хранения информации о расположении плагинов, запускаемых "после"
        /// </summary>
        private Dictionary<Handler, Handler> table = new Dictionary<Handler, Handler>();

        /// <summary>
        /// Текущий индекс для вставки очередного плагина в последовательность
        /// </summary>
        private int currentIndex = 0;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="rootHandler">Держатель корневого плагина</param>
        internal AbstractMultiRunSequence(Handler rootHandler)
            : base(rootHandler)
        {
            //Добавляем плагин и все его зависимости в последовательность
            this.InsertWithAllDependenciesAtCurrentIndex(rootHandler);

            //Подставляем плагины, запускаемые "после" в нужные места
            foreach (var pair in this.table)
            {
                this.currentIndex = this.IndexOf(pair.Value) + 1;
                this.InsertWithAllDependenciesAtCurrentIndex(pair.Key);
            }

            //Таблица нам больше не нужна. Последовательность сформирована.
            this.table.Clear();
        }

        /// <summary>
        /// Рекурсивный метод. Помещает плагин и его зависимости в последовательность по текущему индексу.
        /// Зависимости между плагинами учитываются
        /// </summary>
        /// <param name="handler"></param>
        private void InsertWithAllDependenciesAtCurrentIndex(Handler handler)
        {
            //Проверка на разумность
            if (handler == null)
                throw new ArgumentNullException("handler", "Плагин для добавления в последовательность не определён.");

            //Если плагин уже есть в последовательности
            if (this.Contains(handler))
                return;

            //Можно пропускать только опциональные зависимости
            Func<uint, bool> isMissAllowedCondition = (dependecyID) => handler.Dependencies[dependecyID].IsOptional;

            //Получаем список плагинов, выполняемых до заданного
            //Необязательные плагины могут физически отсутствовать
            Handlers dependencyHandlersToRunBefore =
                HandlersInitial.GetSomeByIDs(
                    handler.Dependencies.Where(pair => !pair.Value.IsRunAfter).Select(pair => pair.Key),
                    isMissAllowedCondition
                );

            //Рекурсивно обрабатываем все плагины, выполняемые до заданного
            foreach (Handler dependencyHandler in dependencyHandlersToRunBefore)
                this.InsertWithAllDependenciesAtCurrentIndex(dependencyHandler);

            //Помещаем сам плагин в последовательность
            this.Insert(this.currentIndex++, handler);

            //Получаем список плагинов, выполняемых после заданного
            Handlers dependencyHandlersToRunAfter = 
                HandlersInitial.GetSomeByIDs(
                    handler.Dependencies.Where(pair => pair.Value.IsRunAfter).Select(pair => pair.Key),
                    isMissAllowedCondition
                );

            //Обрабатываем все плагины, выполняемые после заданного
            //Происходит заполнение временной таблицы, где каждому плагину запускаемому "после" ставится в соответствие тот, за кем он должен быть запущен.
            //Пример: A -> B -> X, А -> C, B -> C.
            //В таблице после обработки A: table[C] = A, а после обработки B: table[C] = B. Таким образом мы понимаем, кто последний из плагинов требует запуска C после себя. 
            foreach (Handler dependency in dependencyHandlersToRunAfter)
                if (table.ContainsKey(dependency))
                    table[dependency] = handler;
                else
                    table.Add(dependency, handler);
        }

        /// <summary>
        /// Преобразовать абстрактную последовательность для мультизапуска плагина в определённую последовательность
        /// </summary>
        /// <param name="selectedPluginsIDs">Множество идентификаторов выбранных плагинов</param>
        /// <returns></returns>
        public SpecificMultiRunSequence ToSpecificMultiRunSequence(HashSet<uint> selectedPluginsIDs)
        {
            //Проверка на разумность
            if (selectedPluginsIDs == null)
                throw new ArgumentNullException("selectedPluginsIDs", "Множество идентификаторов выбранных плагинов не определено.");

            //Получаем пустую определённую последовательность
            SpecificMultiRunSequence ret = new SpecificMultiRunSequence(this.RootHandler);

            //Добавляем в неё только выбранные плагины
            ret.AddRange(this.Where(h => selectedPluginsIDs.Contains(h.ID)));

            return ret;
        }
    }
}
