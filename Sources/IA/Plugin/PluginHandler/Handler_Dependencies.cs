﻿using System;
using System.Collections.Generic;
using System.Linq;

using IA.Extensions;

using PluginConfiguration = IA.Config.Objects.Plugin;

namespace IA.Plugin
{
    /// <summary>
    /// Класс, описывающий держатель отдельного плагина.
    /// Данная часть класса отвечает за работу с зависимостями плагина.
    /// </summary>
    public partial class Handler
    {
        #region Свойства
        private Dictionary<uint, Dependency> dependencies = null;
        /// <summary>
        /// Набор зависимостей плагина
        /// </summary>
        internal Dictionary<uint, Dependency> Dependencies
        {
            get
            {
                //Проверка на инициализацию
                if (this.dependencies != null)
                    return this.dependencies;

                throw new Exception("Зависимости плагина <" + this.Name + "> не были проинициализированы.");
            }
        }

        private Sequences.AbstractMultiRunSequence abstractMultiRunSequence = null;
        /// <summary>
        /// Получить последовательность для мультизапуска плагина, учитывая все возможные опциональные зависимости.
        /// </summary>
        public Sequences.AbstractMultiRunSequence AbstractMultiRunSequence
        {
            get
            {
                //Проверка на инициализацию
                if (this.abstractMultiRunSequence != null)
                    return this.abstractMultiRunSequence;

                throw new Exception("Последовательность для мультизапуска плагина <" + this.Name + "> не была проинициализирована.");
            }
        }

        /// <summary>
        /// Есть ли у данного плагина зависимости
        /// </summary>
        public bool IsHasDependencies
        {
            get
            {
                return this.Dependencies.Count() > 0;
            }
        }

        /// <summary>
        /// Выполнены ли все плагины, которые должны быть запущены до этого
        /// </summary>
        public bool IsAllCompulsoryAndBeforeCompleted
        {
            get
            {
                //Получаем список всех обязательных зависимостей плагина
                Handlers compulsoryAndBeforeDependencies = HandlersInitial.GetSomeByIDs(
                    this.Dependencies.Where(pair => !pair.Value.IsOptional && !pair.Value.IsRunAfter).Select(pair => pair.Key)
                );

                //Проверяем, что все обязательные зависимости завершены корректно
                return compulsoryAndBeforeDependencies.All(p => p.RunResult == ExecutionResult.SUCCESS);
            }
        }
        #endregion

        /// <summary>
        /// Инициализация зависимостей плагина.
        /// Выполняется после инициализации всех плагинов.
        /// </summary>
        internal void InitializeDependencies()
        {
            try
            {
                //Инициализируем набор зависимостей плагина
                this.dependencies = new Dictionary<uint, Dependency>();

                //Проходим по всем зависимостям, указанных в конфигурации
                foreach (PluginConfiguration.Dependency dependency in this.configuration.Dependencies)
                {
                    //Если не нашли физически обязательную зависимость
                    if (HandlersInitial.GetOneByID(dependency.ID, true) == null && !dependency.IsOptional)
                        throw new Exception("Не найден обязательный плагин-зависимость с идентификатором " + dependency.ID + ".");

                    //Добавляем зависимость в набор
                    this.Dependencies.Add(dependency.ID, new Dependency(dependency.IsOptional, dependency.IsRunAfter));
                }
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при инициализации зависимостей плагина <" + this.Name + ">.");
            }
        }

        /// <summary>
        /// Инициализация абстрактной последовательности для мультизапуска плагина.
        /// Выполняеся после инициализации зависимостей плагина.
        /// </summary>
        internal void InitializeAbstractMultiRunSequence()
        {
            try
            {
                //Инициализируем последовательность для мультизапуска плагина
                this.abstractMultiRunSequence = new Sequences.AbstractMultiRunSequence(this);
            }
            catch (Exception ex)
            {
                //Пробрасываем исключение
                ex.ReThrow("Ошибка при инициализации последовательности для мультизапуска плагина <" + this.Name + ">.");
            }
        }

        /// <summary>
        /// Зависит ли плагин от плагина с указанным идентификатором.
        /// Исследуется прямая зависимость непосредственно от указанного плагина.
        /// </summary>
        /// <param name="pluginID">Идентификатор плагина</param>
        /// <returns></returns>
        public bool IsDirectlyDepends(uint pluginID)
        {
            return this.Dependencies.ContainsKey(pluginID);
        }

        /// <summary>
        /// Зависит ли плагин от плагина с указанным идентификатором.
        /// Исследуется прямая, обязательная зависимость непосредственно от указанного плагина.
        /// </summary>
        /// <param name="pluginID">Идентификатор плагина</param>
        /// <returns></returns>
        public bool IsDirectlyAndCompulsoryDepends(uint pluginID)
        {
            Dependency dependency = null;

            return this.Dependencies.TryGetValue(pluginID, out dependency) && !dependency.IsOptional;
        }

        /// <summary>
        /// Зависит ли плагин от плагина с указанным идентификатором.
        /// Исследуется любая зависимость от указанного плагина. В том числе и через другие плагины.
        /// </summary>
        /// <param name="pluginID">Идентификатор плагина</param>
        /// <returns></returns>
        public bool IsDepends(uint pluginID)
        {
            //Можно пропускать только опциональные зависимости
            Func<uint, bool> isMissAllowedCondition = (dependencyID) => this.Dependencies[dependencyID].IsOptional;

            return this.IsDirectlyDepends(pluginID) ||
                HandlersInitial.GetSomeByIDs(
                    this.Dependencies.Keys,
                    isMissAllowedCondition
                ).Any(h => h.IsDepends(pluginID));
        }

        /// <summary>
        /// Зависит ли плагин от плагина с указанным идентификатором.
        /// Исследуется любая, обязательная зависимость от указанного плагина. В том числе и через другие плагины.
        /// Непрямая зависимость является обязательной. Если все зависимости от текущего плагина, до указанного обязательные.
        /// В противном случае зависимость считается необязательной.
        /// </summary>
        /// <param name="pluginID">Идентификатор плагина</param>
        /// <returns></returns>
        public bool IsCompulsoryDepends(uint pluginID)
        {
            return this.IsDirectlyAndCompulsoryDepends(pluginID) || 
                HandlersInitial.GetSomeByIDs(
                    this.Dependencies.Where(pair => !pair.Value.IsOptional).Select(pair => pair.Key)
                ).Any(h => h.IsCompulsoryDepends(pluginID));
        }
    }
}
