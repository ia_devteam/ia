﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace IA.Config
{
    /// <summary>
    /// Класс, реализующий полезнфе функции для работы с тегами
    /// </summary>
    internal static class Tag
    {
        /// <summary>
        /// Класс, реализующий формирование различных исключений, связанных с работой с тегами
        /// </summary>
        internal static class Exceptions
        {
            /// <summary>
            /// Сформировать исключение - Неверный тег
            /// </summary>
            /// <param name="tagName">Имя тега</param>
            internal static void Wrong(string tagName)
            {
                throw new ConfigException("Неверный тег. Ожидаемый тег - <" + tagName + ">.");
            }

            /// <summary>
            /// Сформировать исключение - Неверный список атрибутов тега
            /// </summary>
            /// <param name="tagName">Имя тега</param>
            internal static void WrongAttributes(string tagName)
            {
                throw new ConfigException("Неверный список атрибутов тега <" + tagName + ">.");
            }

            /// <summary>
            /// Сформировать исключение - Неверное значение тега
            /// </summary>
            /// <param name="tagName">Имя тега</param>
            internal static void WrongValue(string tagName)
            {
                throw new ConfigException("Неверное значение тега <" + tagName + ">.");
            }

            /// <summary>
            /// Сформировать исключение - Неверная структура тега
            /// </summary>
            /// <param name="tagName">Имя тега</param>
            internal static void WrongStructure(string tagName)
            {
                throw new ConfigException("Неверная структура тега <" + tagName + ">.");
            }

            /// <summary>
            /// Сформировать исключение - Остуствует дочерний тег
            /// </summary>
            /// <param name="tagName">Имя тега</param>
            /// <param name="subTagName">Имя дочернего тега</param>
            internal static void NoSubTag(string tagName, string subTagName)
            {
                throw new ConfigException("В рамках структуры тега <" + tagName + "> отсутствует тег <" + subTagName + ">.");
            }

            /// <summary>
            /// Сформировать исключение - Отсутствует атрибут
            /// </summary>
            /// <param name="tagName">Имя тега</param>
            /// <param name="attributeName">Имя атрибута</param>
            internal static void NoAttribute(string tagName, string attributeName)
            {
                throw new ConfigException("Отсутствует обязательный атрибут '" + attributeName + "' тега <" + tagName + ">.");
            }

            /// <summary>
            /// Сформировать исключение - Недопустимое значение атрибута
            /// </summary>
            /// <param name="tagName">Имя тега</param>
            /// <param name="attributeName">Имя атрибута</param>
            internal static void WrongAttributeValue(string tagName, string attributeName)
            {
                throw new ConfigException("Недопустимое значение атрибута '" + attributeName + "' тега <" + tagName + ">.");
            }
        }

        /// <summary>
        /// Имя тега совпадает с указанным?
        /// </summary>
        /// <param name="element">Тег</param>
        /// <param name="name">Имя</param>
        /// <returns></returns>
        internal static bool IsName(XElement element, string name)
        {
            return element != null && String.Compare(element.Name.LocalName, name, true) == 0;
        }

        /// <summary>
        /// Получить значение атрибута
        /// </summary>
        /// <param name="element">Тег</param>
        /// <param name="attributeName">Имя атрибута</param>
        /// <param name="isCompulsory">Является ли данный атрибут обязательным?</param>
        /// <returns></returns>
        internal static string GetAttributeValue(XElement element, string attributeName, bool isCompulsory = true)
        {
            XAttribute attribute = element.Attribute(attributeName);

            if (attribute != null)
                return attribute.Value;

            if (isCompulsory)
                Exceptions.NoAttribute(element.Name.LocalName, attributeName);

            return null;
        }

        /// <summary>
        /// Получить дочерний тег
        /// </summary>
        /// <param name="element">Тег</param>
        /// <param name="subTagName">Имя дочернего тега</param>
        /// <returns></returns>
        internal static XElement GetSubTag(XElement element, string subTagName)
        {
            XElement subElement = element.Element(subTagName);

            if(subElement == null)
                Exceptions.NoSubTag(element.Name.LocalName, subTagName);

            return subElement;
        }

        /// <summary>
        /// Получить единственный дочерний тег
        /// </summary>
        /// <param name="element">Тег</param>
        /// <param name="subTagName">Имя дочернего тега</param>
        /// <returns></returns>
        internal static XElement GetOnlySubTag(XElement element, string subTagName)
        {
            XElement subElement = null;

            if (element.Elements().Count() != 1 || (subElement = (element.FirstNode as XElement)) == null || !Tag.IsName(subElement, subTagName))
                Exceptions.WrongStructure(element.Name.LocalName);

            return subElement;
        }
    }
}
