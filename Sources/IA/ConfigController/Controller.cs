﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;

using IA.Extensions;

using IOController;

namespace IA.Config
{
    /// <summary>
    /// Класс реализующий разбор конфигурационного файла и работу с ним
    /// </summary>
    public static class Controller
    {
        /// <summary>
        /// Имя класса
        /// </summary>
        private const string ObjectName = "Контроллер конфигурации";

        /// <summary>
        /// Коллекция конфигураций плагинов
        /// </summary>
        public static EntityCollection<IA.Config.Objects.Plugin> Plugins { get; private set; }

        /// <summary>
        /// Коллекция условий
        /// </summary>
        public static EntityCollection<IA.Config.Objects.Condition> Conditions { get; private set; }

        /// <summary>
        /// Коллекция стадий
        /// </summary>
        public static EntityCollection<IA.Config.Objects.Stage> Stages { get; private set; }

        /// <summary>
        /// Настройки проекта
        /// </summary>
        public static IA.Config.Objects.Settings Settings { get; private set; }

        /// <summary>
        /// Полный абстрактный рабочий процесс
        /// </summary>
        internal static Objects.Workflows.AbstractWorkflow AbstractGlobalWorkflow { get; private set; }

        /// <summary>
        /// Статический конструктор
        /// </summary>
        static Controller()
        {
            //Инициализируем коллекцию конфигураций плагинов
            Controller.Plugins = new EntityCollection<Objects.Plugin>();

            //Инициализируем коллекцию условий
            Controller.Conditions = new EntityCollection<Objects.Condition>();

            //Инициализируем колекцию стадий
            Controller.Stages = new EntityCollection<Objects.Stage>();

            //Информация о настройках проекта не определена
            Controller.Settings = null;
                
            //Глобальный рабочий процесс не определён
            Controller.AbstractGlobalWorkflow = null;
        }

        /// <summary>
        /// Выгрузка данных из конфигурационного файла
        /// </summary>
        public static void Load()
        {
            try
            {
                //Проверка на разумность
                if (String.IsNullOrWhiteSpace(IA.Settings.ConfigFilePath) || !FileController.IsExists(IA.Settings.ConfigFilePath))
                    throw new ConfigException("Файл не найден.");

                //Парсим конфигурационный файл
                XDocument document = XDocument.Load(IA.Settings.ConfigFilePath);

                //Получаем корневой элемент конфигурационного файла
                XElement rootElement = (document.FirstNode as XElement).ThrowIfNull("Недопустимый корневой элемент.");

                //Корневой элемент - это всегда тег <config>
                if (!Tag.IsName(rootElement, "config"))
                    Tag.Exceptions.Wrong("config");

                //Заполняем коллекцию конфигураций плагинов
                Controller.Plugins = new EntityCollection<Objects.Plugin>();
                foreach (XElement pluginElement in Tag.GetSubTag(rootElement, "plugins").Elements())
                {
                    //Получаем плагин
                    Objects.Plugin plugin = new Objects.Plugin(pluginElement);

                    //Добавляем плагин в коллекцию
                    try
                    {
                        Controller.Plugins.Add(plugin);
                    }
                    catch (DuplicateIDsException)
                    {
                        //Формируем исключение
                        throw new Exception("В конфигурационном файле описано несколько плагинов с идентификатором <" + plugin.ID + ">.");
                    }
                }

                //Заполняем коллекцию условий
                Controller.Conditions = new EntityCollection<Objects.Condition>();
                foreach (XElement conditionElement in Tag.GetSubTag(rootElement, "conditions").Elements())
                {
                    //Получаем условие
                    Objects.Condition condition = new Objects.Condition(conditionElement);

                    //Добавляем условие в коллекцию
                    try
                    {
                        Controller.Conditions.Add(condition);
                    }
                    catch (DuplicateIDsException)
                    {
                        //Формируем исключение
                        throw new Exception("В конфигурационном файле описано несколько условий с идентификатором <" + condition.ID + ">.");
                    }
                }

                //Заполняем колекцию стадий
                Controller.Stages = new EntityCollection<Objects.Stage>();
                foreach (XElement stageElement in Tag.GetSubTag(rootElement, "stages").Elements())
                {
                    //Получаем стадию
                    Objects.Stage stage = new Objects.Stage(stageElement);

                    //Добавляем стадию в коллекцию
                    try
                    {
                        Controller.Stages.Add(stage);
                    }
                    catch (DuplicateIDsException)
                    {
                        //Формируем исключение
                        throw new Exception("В конфигурационном файле описано несколько стадий с идентификатором <" + stage.ID + ">.");
                    }
                }

                //Формируем информацию о настройках проекта
                Controller.Settings = new Objects.Settings(Tag.GetSubTag(rootElement, "settings"));
                
                //Формируем глобальный рабочий процесс
                Controller.AbstractGlobalWorkflow = new Objects.Workflows.AbstractWorkflow(Tag.GetOnlySubTag(Tag.GetSubTag(rootElement, "global_workflow"), "workflow"));
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при чтении конфигурационного файла приложения.", ex);
            }
        }


        /// <summary>
        /// Получить определённый глобальный рабочий процесс, используя указанный набор настроек
        /// </summary>
        /// <param name="settings">Набор настроек</param>
        /// <returns></returns>
        public static Objects.Workflows.SpecificWorkflow GetSpecificGlobalWorkflow(Objects.Settings.Set settings)
        {
            try
            {
                //Формируем словарь информации о стадиях, помечая указанные в настройках стадии, как необходимые к запуску
                Dictionary<uint, Objects.Workflows.AbstractWorkflow.StageInfo> stagesInfo = new Dictionary<uint, Objects.Workflows.AbstractWorkflow.StageInfo>();
                foreach (uint stageID in Controller.Stages.Select(s => s.ID))
                    stagesInfo.Add(stageID, new Objects.Workflows.AbstractWorkflow.StageInfo(stageID));

                //Формируем словарь информации об условиях, помечая указанные в настройках условия, как выполненные
                Dictionary<uint, Objects.Workflows.AbstractWorkflow.ConditionInfo> conditionsInfo = new Dictionary<uint, Objects.Workflows.AbstractWorkflow.ConditionInfo>();
                foreach (uint conditionID in Controller.Conditions.Select(c => c.ID))
                    conditionsInfo.Add(conditionID, new Objects.Workflows.AbstractWorkflow.ConditionInfo(conditionID) { Value = settings.Elements.Keys.Contains(conditionID) || settings.Elements.Values.Any(e => e.Contains(conditionID)) });

                //Переменная необходимая для корректного рекурсивного построения определённого рабочего процесса
                bool isStop = false;

                //Преобразуем абстрактный глобальный рабочий процесс в определённый
                return Controller.AbstractGlobalWorkflow.ToSpecificWorkflow(stagesInfo, conditionsInfo, ref isStop);
            }
            catch (IA.Config.Objects.Workflows.Workflow.ContentErrorException)
            {
                //Пробрасываем исключение
                throw;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при преобразовании абстрактного глобального рабочего процесса в определённый.", ex);
            }
        }

        /// <summary>
        /// Получение множества идентификаторов плагинов, необходимых для возможности выполнения произвольного рабочего процесса
        /// </summary>
        /// <returns></returns>
        public static HashSet<uint> GetNecessaryPlugins()
        {
            return Controller.AbstractGlobalWorkflow.GetNecessaryPluginIDs();
        }
    }

    /// <summary>
    /// Класс, реализующий исключения, возникающие при работе с конфигурационным файлом
    /// </summary>
    public class ConfigException : Exception, ISerializable
    {
            /// <summary>
            /// Конструктор
            /// </summary>
            public ConfigException()
                : base()
            {
            }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="message">Текст</param>
            public ConfigException(string message)
                : base(message)
            {
            }

            /// <summary>
            /// Конустрктор
            /// </summary>
            /// <param name="message">Текст</param>
            /// <param name="inner">Вложенное исплючение</param>
            public ConfigException(string message, Exception inner)
                : base(message, inner)
            {
            }

            /// <summary>
            /// Констрктор
            /// </summary>
            /// <param name="info"></param>
            /// <param name="context"></param>
            protected ConfigException(SerializationInfo info, StreamingContext context)
                : base(info, context)
            {
            }
    }
}
