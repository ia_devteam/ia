﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace IA.Config.Objects
{
    /// <summary>
    /// Класс, реализующий отдельную стадию выполнения анализа
    /// </summary>
    public sealed class Stage : IEntity
    {
        /// <summary>
        /// Индентификатор стадии
        /// </summary>
        public uint ID { get; private set; }

        /// <summary>
        /// Наименование стадии
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Зависимость от других стадий. Не может быть - Null.
        /// </summary>
        public uint[] Dependencies { get; private set; }

        /// <summary>
        /// Рабочий процесс, связанный с данной стадией
        /// </summary>
        internal Workflows.AbstractWorkflow Workflow { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="element"></param>
        internal Stage(XElement element)
        {
            try
            {
                //Проверка на разумность
                if (element == null)
                    throw new ConfigException("Тег не определён.");

                //Проверяем имя тега
                if (!Tag.IsName(element, "stage"))
                    Tag.Exceptions.Wrong("stage");

                this.ID = Convert.ToUInt32(Tag.GetAttributeValue(element, "id"));

                if (this.ID == 0)
                    Tag.Exceptions.WrongAttributeValue("stage", "id");

                this.Name = Tag.GetAttributeValue(element, "name");

                Tag.GetAttributeValue(element, "dependsOnStages", false);

                string tmpValue = Tag.GetAttributeValue(element, "dependencies", false);
                this.Dependencies = tmpValue == null ? new uint[0] : tmpValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(uint.Parse).ToArray();

                this.Workflow = new Workflows.AbstractWorkflow(Tag.GetOnlySubTag(element, "workflow"));
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземляра стадии.", ex);
            }
        }

        /// <summary>
        /// Представление стадии в виде строки
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
