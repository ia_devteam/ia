﻿using System;
using System.Xml.Linq;

namespace IA.Config.Objects
{
    /// <summary>
    /// Класс реализующий условие
    /// </summary>
    public sealed class Condition : IEntity
    {
        /// <summary>
        /// Идентификатор условия
        /// </summary>
        public uint ID { get; private set; }

        /// <summary>
        /// Наименование условия
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Тип элемента управления связанного с данным свойством
        /// </summary>
        public Settings.Control Control { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="element"></param>
        internal Condition(XElement element)
        {
            try
            {
                //Проверка на разумность
                if (element == null)
                    throw new ConfigException("Тег не определён.");

                //Проверяем имя тега
                if (!Tag.IsName(element, "condition"))
                    Tag.Exceptions.Wrong("condition");

                this.ID = Convert.ToUInt32(Tag.GetAttributeValue(element, "id"));

                if (this.ID == 0)
                    Tag.Exceptions.WrongAttributeValue("condition", "id");

                this.Name = Tag.GetAttributeValue(element, "name");

                this.Control = (Settings.Control)Enum.Parse(typeof(Settings.Control), Tag.GetAttributeValue(element, "control"), true);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземпляра условия.", ex);
            }
        }
    }
}
