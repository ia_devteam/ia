﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using IA.Extensions;

namespace IA.Config.Objects
{
    /// <summary>
    /// Класс, реализующий структуру и набор пресетов настроек для проекта
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Тип элемента управления связанного с сущностью в рамках настроек
        /// </summary>
        public enum Control
        {
            /// <summary>
            /// Элемент управления не определён
            /// </summary>
            NOTHING,

            /// <summary>
            /// Чекбокс
            /// </summary>
            CHECKBOX,

            /// <summary>
            /// Радиобатон
            /// </summary>
            RADIOBUTTON
        }

        /// <summary>
        /// Класс, реализующий набор настроек
        /// </summary>
        public class Set
        {
            /// <summary>
            /// Список элементов вида {Стадия -> Набор условий для данной стадии}
            /// </summary>
            public Dictionary<uint, HashSet<uint>> Elements { get; private set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            internal Set()
            {
                this.Elements = new Dictionary<uint, HashSet<uint>>();
            }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="element"></param>
            internal Set(XElement element)
            {
                try
                {
                    //Проверка на разумность
                    if (element == null)
                        throw new ConfigException("Тег не определён.");

                    //Проверяем имя тега
                    if (!Tag.IsName(element, "set"))
                        Tag.Exceptions.Wrong("set");

                    if (element.HasAttributes)
                        Tag.Exceptions.WrongAttributes("set");

                    if (element.HasElements)
                        Tag.Exceptions.WrongStructure("set");

                    this.Elements = FromString(element.Value);
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new ConfigException("Ошибка при создании экземпляра набора настроек.", ex);
                }
            }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="value"></param>
            public Set(string value)
            {
                try
                {
                    this.Elements = FromString(value);
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new ConfigException("Ошибка при создании экземпляра набора настроек.", ex);
                }
            }

            /// <summary>
            /// Получение набора элементов набора настроек из строки
            /// </summary>
            /// <param name="value">Набор настроек в строково представлении</param>
            /// <returns></returns>
            private Dictionary<uint, HashSet<uint>> FromString(string value)
            {
                Dictionary<uint, HashSet<uint>> ret = new Dictionary<uint, HashSet<uint>>();

                if (String.IsNullOrWhiteSpace(value))
                    return ret;

                string[] semicolonSplit = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string pair in semicolonSplit)
                {
                    string[] colonSplit = pair.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                    switch (colonSplit.Length)
                    {
                        case 1:
                            {
                                ret.Add(UInt32.Parse(colonSplit[0]), new HashSet<uint>());
                                continue;
                            }
                        case 2:
                            {
                                ret.Add(UInt32.Parse(colonSplit[0]), new HashSet<uint>(colonSplit[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(uint.Parse)));
                                continue;
                            }
                        default:
                            {
                                Tag.Exceptions.WrongValue("set");
                                continue;
                            }

                    }
                }

                return ret;
            }

            /// <summary>
            /// Равны ли два набора настроек?
            /// </summary>
            /// <param name="other">Набор настроек</param>
            /// <returns></returns>
            public bool IsEqualsTo(Set other)
            {
                //Проверка на разумность
                if (other == null || other.Elements == null)
                    return false;

                //Если мы имеем дела с указателем на один и тот же набор элементов
                if(this.Elements == other.Elements)
                    return true;

                //Если количество стадий в наборах не совпадают
                if (this.Elements.Count != other.Elements.Count)
                    return false;

                //Проверяем на то, что все стадии и условия совпадают
                foreach (uint this_stageID in this.Elements.Keys)
                {
                    HashSet<uint> other_conditionsIDs;

                    if (!other.Elements.TryGetValue(this_stageID, out other_conditionsIDs))
                        return false;

                    if (!this.Elements[this_stageID].SetEquals(other_conditionsIDs))
                        return false;
                }

                return true;
            }

            /// <summary>
            /// Является ли данный набор настроек подмножеством указанного?
            /// </summary>
            /// <param name="other">Набор настроек</param>
            /// <returns></returns>
            public bool IsSubsetOf(Set other)
            {
                //Проверка на разумность
                if (other == null || other.Elements == null)
                    return false;

                //Если мы имеем дела с указателем на один и тот же набор элементов
                if (this.Elements == other.Elements)
                    return true;

                //Если количество стадий в наборах не совпадают
                if (this.Elements.Count > other.Elements.Count)
                    return false;

                return Elements.Keys.All(stageID =>
                {
                    HashSet<uint> other_conditionsIDs;

                    return other.Elements.TryGetValue(stageID, out other_conditionsIDs) && this.Elements[stageID].IsSubsetOf(other_conditionsIDs);
                });
            }

            /// <summary>
            /// Общее число стадий и условий в рамках набора настроек
            /// </summary>
            /// <returns></returns>
            public int ElementsCount()
            {
                int ret = 0;

                this.Elements.ForEach(el => ret += el.Value.Count + 1);

                return ret;
            }

            /// <summary>
            /// Представление набора настроек в виде строки
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return String.Join("; ", this.Elements.Select(el => el.Key + (el.Value.Count() == 0 ? String.Empty : ":" + String.Join(",", el.Value.ToArray()))));
            }
        }

        /// <summary>
        /// Класс, реализующий пресет
        /// </summary>
        public class Preset : IEntity
        {
            /// <summary>
            /// Идентификатор пресета
            /// </summary>
            public uint ID { get; private set; }

            /// <summary>
            /// Наименование пресета
            /// </summary>
            public string Name { get; private set; }

            /// <summary>
            /// Минимальный набор настроек для пресета
            /// </summary>
            public Settings.Set CompulsorySettingsSet { get; private set; }

            /// <summary>
            /// Рекомендованный набор настроек для пресета
            /// </summary>
            public Settings.Set DefaultSettingsSet { get; private set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="element"></param>
            internal Preset(XElement element)
            {
                try
                {
                    //Проверка на разумность
                    if (element == null)
                        throw new ConfigException("Тег не определён.");

                    //Проверяем имя тега
                    if (!Tag.IsName(element, "preset"))
                        Tag.Exceptions.Wrong("preset");

                    this.ID = Convert.ToUInt32(Tag.GetAttributeValue(element, "id"));

                    this.Name = Tag.GetAttributeValue(element, "name");

                    this.CompulsorySettingsSet = new Settings.Set(Tag.GetOnlySubTag(Tag.GetSubTag(element, "compulsory"), "set"));

                    this.DefaultSettingsSet = new Settings.Set(Tag.GetOnlySubTag(Tag.GetSubTag(element, "default"), "set"));
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new ConfigException("Ошибка при создании экземпляра пресета.", ex);
                }
            }

            /// <summary>
            /// Представления пресета в виде строки
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return this.Name ?? "<Unnamed preset>";
            }
        }

        /// <summary>
        /// Структура настроек проекта
        /// </summary>
        public Settings.Set Structure { get; private set; }

        /// <summary>
        /// Условия по умолчанию для отдельных стадий
        /// </summary>
        public Settings.Set Defaults { get; private set; }

        /// <summary>
        /// Список пресетов
        /// </summary>
        public EntityCollection<Preset> Presets { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="element"></param>
        internal Settings(XElement element)
        {
            try
            {
                //Проверка на разумность
                if (element == null)
                    throw new ConfigException("Тег не определён.");

                //Проверяем имя тега
                if (!Tag.IsName(element, "settings"))
                    Tag.Exceptions.Wrong("settings");

                this.Structure = new Settings.Set(Tag.GetOnlySubTag(Tag.GetSubTag(element, "structure"), "set"));

                this.Defaults = new Settings.Set(Tag.GetOnlySubTag(Tag.GetSubTag(element, "defaults"), "set"));

                this.Presets = new EntityCollection<Preset>();
                foreach (XElement presetElement in Tag.GetSubTag(element, "presets").Elements())
                {
                    //Получаем пресет
                    Preset preset = new Preset(presetElement);

                    //Добавляем пресет в коллекцию
                    try
                    {
                        this.Presets.Add(preset);
                    }
                    catch (DuplicateIDsException)
                    {
                        //Формируем исключение
                        throw new Exception("В конфигурационном файле описано несколько пресетов с идентификатором <" + preset.ID + ">.");
                    }
                }

            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземпляра настроек.", ex);
            }
        }
    }
}
