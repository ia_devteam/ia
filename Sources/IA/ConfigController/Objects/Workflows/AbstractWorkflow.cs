﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using IA.Extensions;

namespace IA.Config.Objects.Workflows
{
    /// <summary>
    /// Интерфейс, представляющий элемент абстрактного рабочего процесса
    /// </summary>
    internal interface IAbstractElement : Workflow.IElement
    {
        /// <summary>
        /// Преобразование элемента абстрактного рабочего процесса в последовательность элементов определённого рабочего процесса
        /// </summary>
        /// <param name="stagesInfo">Информация о стадиях</param>
        /// <param name="conditionsInfo">Информация об условиях</param>
        /// <param name="isStop">Требуется ли остановить рекурсивное формирование определённого рабочего процесса?</param>
        /// <returns></returns>
        Queue<ISpecificElement> ToSpecificElements(Dictionary<uint, AbstractWorkflow.StageInfo> stagesInfo, Dictionary<uint, AbstractWorkflow.ConditionInfo> conditionsInfo, ref bool isStop);

        /// <summary>
        /// Получение множества идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса
        /// </summary>
        /// <returns>Множество идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса. Не может быть Null.</returns>
        HashSet<uint> GetNecessaryPluginIDs();
    }

    /// <summary>
    /// Класс, реализующий абстрактный рабочий процесс выполнения анализа. В данном рабочем процессе могут присутствовать неразрешённые ветвления.
    /// </summary>
    internal class AbstractWorkflow : Workflow<IAbstractElement>
    {
        /// <summary>
        /// Информация об отдельной стадии (используется при преобразовании абстрактного рабочего процесса в определённый)
        /// </summary>
        internal class StageInfo
        {
            /// <summary>
            /// Идентификатор стадии
            /// </summary>
            internal uint ID { get; private set; }
            /// <summary>
            /// Была ли стадия уже учитана при построении определённого рабочего процесса?
            /// </summary>
            internal bool IsPassed { get; set; }

            /// <summary>
            /// Констркутор
            /// </summary>
            /// <param name="id">Идентификатор стадии</param>
            internal StageInfo(uint id)
            {
                this.ID = id;
                this.IsPassed = false;
            }
        }

        /// <summary>
        /// Информация об отдельном условии (используется при преобразовании абстрактного рабочего процесса в определённый)
        /// </summary>
        internal class ConditionInfo
        {
            /// <summary>
            /// Идентификатор условия
            /// </summary>
            internal uint ID { get; private set; }
            /// <summary>
            /// Значение условия
            /// </summary>
            internal bool Value { get; set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="id">Идентификатор условия</param>
            /// <param name="value">Значение условия</param>
            internal ConditionInfo(uint id, bool value = false)
            {
                this.ID = id;
                this.Value = value;
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="element"></param>
        internal AbstractWorkflow(XElement element)
        {
            try
            {
                //Проверка на разумность
                if (element == null)
                    throw new ConfigException("Тег не определён.");

                //Проверяем имя тега
                if (!Tag.IsName(element, "workflow"))
                    Tag.Exceptions.Wrong("workflow");

                //Проходим по всем вложенным элементам
                foreach (XElement child in element.Elements())
                    switch (child.Name.LocalName)
                    {
                        case "run":
                            {
                                this.Elements.Enqueue(new Run(child));
                                break;
                            }
                        case "if":
                            {
                                this.Elements.Enqueue(new If(child));
                                break;
                            }
                        case "stop":
                            {
                                this.Elements.Enqueue(new Stop(child));
                                break;
                            }
                        case "error":
                            {
                                this.Elements.Enqueue(new Error(child));
                                break;
                            }
                        default:
                            {
                                Tag.Exceptions.WrongStructure("workflow");
                                break;
                            }
                    }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземпляра абстрактного рабочего процесса.", ex);
            }
        }

        /// <summary>
        /// Преобразование абстрактного рабочего процесса в определённый рабочий процесс
        /// </summary>
        /// <param name="stagesInfo">Информация о стадиях</param>
        /// <param name="conditionsInfo">Информация об условиях</param>
        /// <param name="isStop">Требуется ли остановить рекурсивное формирование определённого рабочего процесса?</param>
        /// <returns></returns>
        internal SpecificWorkflow ToSpecificWorkflow(Dictionary<uint, AbstractWorkflow.StageInfo> stagesInfo, Dictionary<uint, AbstractWorkflow.ConditionInfo> conditionsInfo, ref bool isStop)
        {
            //Проверка на разумность
            if (stagesInfo == null || conditionsInfo == null)
                throw new ConfigException("Недостаточно данных для преобразования абстрактного рабочего процесса в определённый. Часть параметров равны Null.");

            //Инициализируем результат
            SpecificWorkflow ret = new SpecificWorkflow();

            //Если требуется закончить построение определённого рабочего процесса
            if (isStop)
                return ret;

            //Преобразуем все абстрактные элементы рабочего процесса в списки определённых элементов рабочего процесса и добавляем в результирующий рабочий процесс
            foreach (IAbstractElement element in this.Elements)
            {
                element.ToSpecificElements(stagesInfo, conditionsInfo, ref isStop).ForEach(ret.Elements.Enqueue);

                //Если требуется закончить построение определённого рабочего процесса
                if (isStop)
                    return ret;
            }

            return ret;
        }

        /// <summary>
        /// Получение множества идентификаторов плагинов, необходимых в рамках абстрактного рабочего процесса
        /// </summary>
        /// <returns>Множество идентификаторов плагинов, необходимых в рамках абстрактного рабочего процесса. Не может быть Null.</returns>
        internal HashSet<uint> GetNecessaryPluginIDs()
        {
            return new HashSet<uint>(this.Elements.SelectMany(e => e.GetNecessaryPluginIDs()));
        }
    }
}
