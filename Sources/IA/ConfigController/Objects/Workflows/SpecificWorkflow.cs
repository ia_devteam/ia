﻿namespace IA.Config.Objects.Workflows
{
    /// <summary>
    /// Интерфейс, представляющий элемент определённого рабочего процесса
    /// </summary>
    public interface ISpecificElement : Workflow.IElement
    {
    }

    /// <summary>
    /// Класс, реализующий определённый рабочий процесс выполнения анализа. В данном рабочем процессе не могут присутствовать неразрешённые ветвления.
    /// </summary>
    public class SpecificWorkflow : Workflow<ISpecificElement>
    {
    }
}
