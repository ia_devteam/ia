﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Linq;

using IA.Extensions;

namespace IA.Config.Objects.Workflows
{
    /// <summary>
    /// Класс, реализующиий раннер. Используется для запуска стадий или плагинов в рамках рабочего процесса.
    /// </summary>
    public class Run : IAbstractElement, ISpecificElement
    {
        /// <summary>
        /// Тип запускаемой сущности
        /// </summary>
        public enum enEntityType
        {
            /// <summary>
            /// Стадия
            /// </summary>
            [Description("Стадия")]
            STAGE,
            /// <summary>
            /// Плагин
            /// </summary>
            [Description("Плагин")]
            PLUGIN
        }

        /// <summary>
        /// Идентификатор запускаемой сущности
        /// </summary>
        public uint EntityID { get; private set; }

        /// <summary>
        /// Тип запускаемой сущности
        /// </summary>
        public enEntityType EntityType { get; private set; }

        /// <summary>
        /// Строка настроек для запускаемой сущности
        /// </summary>
        public string EntitySettings { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="element"></param>
        internal Run(XElement element)
        {
            try
            {
                //Проверка на разумность
                if (element == null)
                    throw new ConfigException("Тег не определён.");

                //Проверяем имя тега
                if (!Tag.IsName(element, "run"))
                    Tag.Exceptions.Wrong("run");

                this.EntityID = Convert.ToUInt32(Tag.GetAttributeValue(element, "entityID"));

                if (this.EntityID == 0)
                    Tag.Exceptions.WrongAttributeValue("run", "entityID");

                this.EntityType = (enEntityType)Enum.Parse(typeof(enEntityType), Tag.GetAttributeValue(element, "entityType"), true);

                this.EntitySettings = Tag.GetAttributeValue(element, "settings", false);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземпляра раннера.", ex);
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="entityID">Идентификатор сущности</param>
        /// <param name="entityType">Тип сущности</param>
        /// <param name="entitySettings">Строка настроек сущности</param>
        private Run(uint entityID, enEntityType entityType, string entitySettings = null)
        {
            try
            {
                //Проверка на разумность
                if (entityID == 0)
                    throw new ConfigException("Неверный идентификатор сущности.");

                this.EntityID = entityID;
                this.EntityType = entityType;
                this.EntitySettings = entitySettings;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземпляра раннера.", ex);
            }
        }

        /// <summary>
        /// Представление раннера в виде строки
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            switch (this.EntityType)
            {
                case enEntityType.PLUGIN:
                    return "Раннер. Сущность: <" + this.EntityType.Description() + ">. Имя: <" + IA.Config.Controller.Plugins[this.EntityID] +">." + (String.IsNullOrWhiteSpace(this.EntitySettings) ? String.Empty : " Настройки: <" + this.EntitySettings + ">.");
                case enEntityType.STAGE:
                    return "Раннер. Сущность: <" + this.EntityType.Description() + ">. Имя: <" + IA.Config.Controller.Stages[this.EntityID] + ">." + (String.IsNullOrWhiteSpace(this.EntitySettings) ? String.Empty : " Настройки: <" + this.EntitySettings + ">.");
                default:
                    throw new ConfigException("Обнаружен необрабатываемый тип запускаемой сущности.");
            }
        }

        /// <summary>
        /// Преобрахование элемента абстрактного рабочего процесса в последовательность элементов определённого рабочего процесса
        /// </summary>
        /// <param name="stagesInfo">Информация о стадиях</param>
        /// <param name="conditionsInfo">Информация об условиях</param>
        /// <param name="isStop">Требуется ли остановить рекурсивное формирование определённого рабочего процесса?</param>
        /// <returns></returns>
        Queue<ISpecificElement> IAbstractElement.ToSpecificElements(Dictionary<uint, AbstractWorkflow.StageInfo> stagesInfo, Dictionary<uint, AbstractWorkflow.ConditionInfo> conditionsInfo, ref bool isStop)
        {
            //Проверка на разумность
            if (stagesInfo == null || conditionsInfo == null)
                throw new ConfigException("Недостаточно данных для преобразования абстрактного элемента рабочего процесса в определённый. Часть параметров равны Null.");

            //Формируем результат
            Queue<ISpecificElement> ret = new Queue<ISpecificElement>();

            //Если требуется закончить построение определённого рабочего процесса
            if (isStop)
                return ret;

            switch (this.EntityType)
            {
                //Абстрактный элемент раннера плагина ничего не отличается от определённого
                case enEntityType.PLUGIN:
                    {
                        ret.Enqueue(this);
                        return ret;
                    }
                //Преобразуем абстрактный элемент раннера стадии в последовательность определённых элементов
                case enEntityType.STAGE:
                    {
                        //Если не треубется учитывать данную стадию или стадия уже была учтена
                        if (stagesInfo[this.EntityID].IsPassed)
                            return ret;

                        //Помечаем, что учли эту стадию
                        stagesInfo[this.EntityID].IsPassed = true;

                        //Получаем саму стадию
                        IA.Config.Objects.Stage stage = IA.Config.Controller.Stages[this.EntityID];

                        //Добавляем стадии от которых зависим в рабочий процесс
                        foreach (uint dependencyID in stage.Dependencies)
                        {
                            //Обрабатываем стадию от которой зависим
                            (new Run(dependencyID, enEntityType.STAGE) as IAbstractElement).ToSpecificElements(stagesInfo, conditionsInfo, ref isStop).ForEach(ret.Enqueue);

                            //Если требуется закончить построение определённого рабочего процесса
                            if (isStop)
                                return ret;
                        }

                        //Преобразуем рабочий процесс стадии в определённый рабочий процесс
                        stage.Workflow.ToSpecificWorkflow(stagesInfo, conditionsInfo, ref isStop).Elements.ForEach(ret.Enqueue);

                        return ret;
                    }
                default:
                    throw new ConfigException("Обнаружен необрабатываемый тип запускаемой сущности.");
            }
        }

        /// <summary>
        /// Получение множества идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса
        /// </summary>
        /// <returns>Множество идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса. Не может быть Null.</returns>
        HashSet<uint> IAbstractElement.GetNecessaryPluginIDs()
        {
            HashSet<uint> ret = new HashSet<uint>();

            switch (this.EntityType)
            {
                case enEntityType.PLUGIN:
                    {
                        ret.Add(this.EntityID);
                        break;
                    }
                case enEntityType.STAGE:
                    {
                        //Получаем саму стадию
                        IA.Config.Objects.Stage stage = IA.Config.Controller.Stages[this.EntityID];

                        //Обрабатываем стадии, от которой зависим
                        foreach (uint dependencyID in stage.Dependencies)
                            ret.UnionWith((new Run(dependencyID, enEntityType.STAGE) as IAbstractElement).GetNecessaryPluginIDs());

                        //Обрабатываем саму стадию
                        ret.UnionWith(stage.Workflow.GetNecessaryPluginIDs());

                        break;
                    }
                default:
                    throw new ConfigException("Обнаружен необрабатываемый тип запускаемой сущности.");
            }

            return ret;
        }
    }

    /// <summary>
    /// Класс, реализующий стоппер. Используется для прерывания рабочего процесса, в частности, чтобы создать новое Хранилище или новый набор материалов.
    /// </summary>
    public class Stop : IAbstractElement, ISpecificElement
    {
        /// <summary>
        /// Варианты, для чего стоит прерваться
        /// </summary>
        public enum enNeed
        {
            /// <summary>
            /// Требуется завершить выполнение
            /// </summary>
            [Description("Завершить выполнение")]
            FINISH,
            /// <summary>
            /// Требуется создать новое Хранилище для обработки очищенных исходных текстов
            /// </summary>
            [Description("Новое Хранилище для обработки очищенных исходных текстов")]
            STORAGE_NEW_SRC_CLEAR,
            /// <summary>
            /// Требуется анализ Хранилища на предмет наличия избыточных функций
            /// </summary>
            [Description("Анализ Хранилища на предмет избыточных функций")]
            STORAGE_FUNCTION_REDUNDANCY_ANALIZE,
            /// <summary>
            /// Требуется предоставление лабораторных ичсходных текстов пользователю
            /// </summary>
            [Description("Предоставление лабораторных исходных текстов")]
            GET_SRC_LAB
        };

        /// <summary>
        /// Что необходимо создать в рамках стоппера
        /// </summary>
        public enNeed Need { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="element"></param>
        internal Stop(XElement element)
        {
            try
            {
                //Проверка на разумность
                if (element == null)
                    throw new ConfigException("Тег не определён.");

                //Проверяем имя тега
                if (!Tag.IsName(element, "stop"))
                    Tag.Exceptions.Wrong("stop");

                this.Need = (enNeed)Enum.Parse(typeof(enNeed), Tag.GetAttributeValue(element, "need"), true);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземпляра стоппера.", ex);
            }
        }

        /// <summary>
        /// Представление стоппера в виде строки
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "Стоппер. Требуется: <" + this.Need.Description() + ">.";
        }

        /// <summary>
        /// Преобрахование элемента абстрактного рабочего процесса в последовательность элементов определённого рабочего процесса
        /// </summary>
        /// <param name="stagesInfo">Информация о стадиях</param>
        /// <param name="conditionsInfo">Информация об условиях</param>
        /// <param name="isStop">Требуется ли остановить рекурсивное формирование определённого рабочего процесса?</param>
        /// <returns></returns>
        Queue<ISpecificElement> IAbstractElement.ToSpecificElements(Dictionary<uint, AbstractWorkflow.StageInfo> stagesInfo, Dictionary<uint, AbstractWorkflow.ConditionInfo> conditionsInfo, ref bool isStop)
        {
            //Проверка на разумность
            if (stagesInfo == null || conditionsInfo == null)
                throw new ConfigException("Недостаточно данных для преобразования абстрактного элемента рабочего процесса в определённый. Часть параметров равны Null.");

            //Формируем результат
            Queue<ISpecificElement> ret = new Queue<ISpecificElement>();

            //Если требуется закончить построение определённого рабочего процесса
            if (isStop)
                return ret;

            //Если мы дошли до финального стоппера то, заканчиваем построение определённого рабочего процесса
            isStop = this.Need == enNeed.FINISH;

            //Добавляем себя в рабочий процесс
            ret.Enqueue(this);

            return ret;
        }

        /// <summary>
        /// Получение множества идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса
        /// </summary>
        /// <returns>Множество идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса. Не может быть Null.</returns>
        HashSet<uint> IAbstractElement.GetNecessaryPluginIDs()
        {
            return new HashSet<uint>();
        }
    }

    /// <summary>
    /// Класс, реализующий ветвление
    /// </summary>
    internal class If : IAbstractElement
    {
        /// <summary>
        /// Идентифиактор условия
        /// </summary>
        public uint ConditionID { get; private set; }

        /// <summary>
        /// Значение условия
        /// </summary>
        public bool ConditionValue { get; private set; }

        /// <summary>
        /// Рабочий процесс при указанном значении условия
        /// </summary>
        internal AbstractWorkflow Workflow { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="element"></param>
        internal If(XElement element)
        {
            try
            {
                //Проверка на разумность
                if (element == null)
                    throw new ConfigException("Тег не определён.");

                //Проверяем имя тега
                if (!Tag.IsName(element, "if"))
                    Tag.Exceptions.Wrong("if");

                this.ConditionID = Convert.ToUInt32(Tag.GetAttributeValue(element, "conditionID"));

                if (this.ConditionID == 0)
                    Tag.Exceptions.WrongAttributeValue("if", "conditionID");

                this.ConditionValue = Convert.ToBoolean(Tag.GetAttributeValue(element, "conditionValue"));

                this.Workflow = new AbstractWorkflow(Tag.GetOnlySubTag(element, "workflow"));
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземпляра ветвления.", ex);
            }
        }


        /// <summary>
        /// Преобразование элемента абстрактного рабочего процесса в последовательность элементов определённого рабочего процесса
        /// </summary>
        /// <param name="stagesInfo">Информация о стадиях</param>
        /// <param name="conditionsInfo">Информация об условиях</param>
        /// <param name="isStop">Требуется ли остановить рекурсивное формирование определённого рабочего процесса?</param>
        /// <returns></returns>
        Queue<ISpecificElement> IAbstractElement.ToSpecificElements(Dictionary<uint, AbstractWorkflow.StageInfo> stagesInfo, Dictionary<uint, AbstractWorkflow.ConditionInfo> conditionsInfo, ref bool isStop)
        {
            //Проверка на разумность
            if (stagesInfo == null || conditionsInfo == null)
                throw new ConfigException("Недостаточно данных для преобразования абстрактного элемента рабочего процесса в определённый. Часть параметров равны Null.");

            //Формируем результат
            Queue<ISpecificElement> ret = new Queue<ISpecificElement>();

            //Если требуется прервать построение определённого рабочего процесса
            if (isStop)
                return ret;

            //Если условие не выполнено, ничего не делаем
            if(this.ConditionValue != conditionsInfo[this.ConditionID].Value)
                return ret;

            //Преобразуем абстрактный элемент ветвления в последовательность определённых элементов
            return this.Workflow.ToSpecificWorkflow(stagesInfo, conditionsInfo, ref isStop).Elements;
        }

        /// <summary>
        /// Получение множества идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса
        /// </summary>
        /// <returns>Множество идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса. Не может быть Null.</returns>
        HashSet<uint> IAbstractElement.GetNecessaryPluginIDs()
        {
            return this.Workflow.GetNecessaryPluginIDs();
        }
    }

    /// <summary>
    /// Класс, реализующий ошибку
    /// </summary>
    internal class Error : IAbstractElement
    {
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="element"></param>
        internal Error(XElement element)
        {
            try
            {
                //Проверка на разумность
                if (element == null)
                    throw new ConfigException("Тег не определён.");

                //Проверяем имя тега
                if (!Tag.IsName(element, "error"))
                    Tag.Exceptions.Wrong("error");

                this.Text =  Tag.GetAttributeValue(element, "text");
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземпляра ошибки.", ex);
            }
        }

        /// <summary>
        /// Преобразование элемента абстрактного рабочего процесса в последовательность элементов определённого рабочего процесса
        /// </summary>
        /// <param name="stagesInfo">Информация о стадиях</param>
        /// <param name="conditionsInfo">Информация об условиях</param>
        /// <param name="isStop">Требуется ли остановить рекурсивное формирование определённого рабочего процесса?</param>
        /// <returns></returns>
        Queue<ISpecificElement> IAbstractElement.ToSpecificElements(Dictionary<uint, AbstractWorkflow.StageInfo> stagesInfo, Dictionary<uint, AbstractWorkflow.ConditionInfo> conditionsInfo, ref bool isStop)
        {
            //Проверка на разумность
            if (stagesInfo == null || conditionsInfo == null)
                throw new ConfigException("Недостаточно данных для преобразования абстрактного элемента рабочего процесса в определённый. Часть параметров равны Null.");

            //Формируем результат
            Queue<ISpecificElement> ret = new Queue<ISpecificElement>();

            //Если требуется прервать построение определённого рабочего процесса
            if (isStop)
                return ret;

            //Формируем исключение
            throw new Workflow.ContentErrorException(this.Text);
        }

        /// <summary>
        /// Получение множества идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса
        /// </summary>
        /// <returns>Множество идентификаторов плагинов, необходимых в рамках элемента абстрактного рабочего процесса. Не может быть Null.</returns>
        HashSet<uint> IAbstractElement.GetNecessaryPluginIDs()
        {
            return new HashSet<uint>();
        }
    }


    /// <summary>
    /// Абстрактный класс, представляющий рабочий процесс выполнения анализа
    /// </summary>
    public abstract class Workflow
    {
        /// <summary>
        /// Интерфейс, представляющий элемент рабочего процесса
        /// </summary>
        public interface IElement
        {
        }

        /// <summary>
        /// Класс, реализующий исключение связанное с некорректностью содержания рабочего процесса
        /// </summary>
        [Serializable]
        public class ContentErrorException : ConfigException, ISerializable
        {
            /// <summary>
            /// Конструктор
            /// </summary>
            public ContentErrorException()
                : base()
            {
            }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="message">Текст</param>
            public ContentErrorException(string message)
                : base(message)
            {
            }

            /// <summary>
            /// Конустрктор
            /// </summary>
            /// <param name="message">Текст</param>
            /// <param name="inner">Вложенное исплючение</param>
            public ContentErrorException(string message, Exception inner)
                : base(message, inner)
            {
            }

            /// <summary>
            /// Констрктор
            /// </summary>
            /// <param name="info"></param>
            /// <param name="context"></param>
            protected ContentErrorException(SerializationInfo info, StreamingContext context)
                : base(info, context)
            {
            }
        }
    }

    /// <summary>
    /// Абстрактный класс, представляющий рабочий процесс выполнения анализа
    /// </summary>
    /// <typeparam name="T">Тип элементов рабочего процесса</typeparam>
    public abstract class Workflow<T> where T : Workflow.IElement
    {
        private Queue<T> elements = new Queue<T>();
        /// <summary>
        /// Список элементов рабочего процесса
        /// </summary>
        public Queue<T> Elements
        {
            get
            {
                return elements;
            }
        }
    }
}
