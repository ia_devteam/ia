﻿using System;
using System.Xml.Linq;

namespace IA.Config.Objects
{
    /// <summary>
    /// Класс, реализующий конфигурацию отдельного плагина
    /// </summary>
    public sealed class Plugin : IEntity
    {
        /// <summary>
        /// Класс описывающий зависимость между плагинами
        /// </summary>
        public class Dependency : IEntity
        {
            /// <summary>
            /// Идентификатор зависимости
            /// </summary>
            public uint ID { get; private set; }

            /// <summary>
            /// Является ли зависимость опциональной?
            /// </summary>
            public bool IsOptional { get; private set; }

            /// <summary>
            /// Должен ли плагин запускаться после плагина, чьей зависимостью он является?
            /// </summary>
            public bool IsRunAfter { get; private set; }

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="element"></param>
            internal Dependency(XElement element)
            {
                try
                {
                    //Проверка на разумность
                    if (element == null)
                        throw new ConfigException("Тег не определён.");

                    //Проверка на разумность, что мы имеем дело с тегом <dependency>
                    if(!Tag.IsName(element, "dependency"))
                        Tag.Exceptions.Wrong("dependency");

                    //Получаем идентификатор зависимости
                    this.ID = Convert.ToUInt32(Tag.GetAttributeValue(element, "id"));

                    //Проверка на разумность
                    if (this.ID == 0)
                        Tag.Exceptions.WrongAttributeValue("dependency", "id");

                    //Узнаём, является ли зависимость опциональной?
                    this.IsOptional = Convert.ToBoolean(Tag.GetAttributeValue(element, "isOptional"));

                    //Узнаём, должен ли плагин запускаться после плагина, чьей зависимостью он является?
                    this.IsRunAfter = Convert.ToBoolean(Tag.GetAttributeValue(element, "isRunAfter"));
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new ConfigException("Ошибка при создании экземпляра зависимости между плагинами.", ex);
                }
            }
        }

        /// <summary>
        /// Идентификтор плагина
        /// </summary>
        public uint ID { get; private set; }

        /// <summary>
        /// Наименование плагина
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Категория плагина
        /// </summary>
        public string Category { get; private set; }

        /// <summary>
        /// Возможность многократного использования плагина
        /// </summary>
        public bool IsReusable { get; private set; }

        /// <summary>
        /// Список зависимостей плагина
        /// </summary>
        public EntityCollection<Dependency> Dependencies { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="element"></param>
        internal Plugin(XElement element)
        {
            try
            {
                //Проверка на разумность
                if (element == null)
                    throw new ConfigException("Тег не определён.");

                //Проверяем имя тега
                if (!Tag.IsName(element, "plugin"))
                    Tag.Exceptions.Wrong("plugin");

                this.ID = Convert.ToUInt32(Tag.GetAttributeValue(element, "id"));

                if (this.ID == 0)
                    Tag.Exceptions.WrongAttributeValue("plugin", "id");

                this.Name = Tag.GetAttributeValue(element, "name");

                this.Category = Tag.GetAttributeValue(element, "category");

                this.IsReusable = Convert.ToBoolean(Tag.GetAttributeValue(element, "isReusable"));

                //Получаем список зависимостей плагина
                this.Dependencies = new EntityCollection<Dependency>();
                foreach (XElement dependencyElement in Tag.GetSubTag(element, "dependencies").Elements())
                {
                    //Получаем зависимость
                    Dependency dependency = new Dependency(dependencyElement);

                    //Добавляем зависимость в коллекцию
                    try
                    {
                        this.Dependencies.Add(dependency);
                    }
                    catch (DuplicateIDsException)
                    {
                        //Формируем исключение
                        throw new Exception("В конфигурационном файле описано несколько зависимостей плагина <" + this.Name + "> с идентификатором <" + dependency.ID + ">.");
                    }
                }

                //Проверяем список зависимостей плагина
                this.ThrowIfWrongDependencies();
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка при создании экземпляра конфигурации плагина.", ex);
            }
        }

        /// <summary>
        /// Проверка списка зависимостей плагина.
        /// Если обнаружена ошибка, выдаётся исключение.
        /// </summary>
        private void ThrowIfWrongDependencies()
        {
            try
            {
                //Плагин не может зависить от себя
                if (this.Dependencies.ContainsID(this.ID))
                    throw new ConfigException("Плагин не может зависить от себя.");
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new ConfigException("Ошибка в списке зависимостей плагина <" + this.Name + ">.", ex);
            }
        }

        /// <summary>
        /// Представление плагина в виде строки
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
