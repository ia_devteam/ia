﻿using IA.Extensions;
using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;

namespace IA
{
    /// <summary>
    /// Перечень каталогов, в которых живёт IA во время исполнения. 
    /// Нужен, так как текущий каталог, в общем случае, произвольный в момент запуска IA.
    /// </summary>
    public static class RuntimeDirectories
    {
        private static string assemblyLocation = null;
        /// <summary>
        /// Каталог с ConfigController.dll, который должен быть корневым в структуре каталогов IA.
        /// </summary>
        /// <remarks>
        /// Дословное повторение кода из stackoverflow не имеет смысла, 
        /// так как в описании CodeBase сказано, что может возвращаться http://... 
        /// или вообще ничего не возвращаться, 
        /// а Location всегда возвращает путь к загруженной с hdd сборки
        /// </remarks>
        public static string MainAssemblyDirectory
        {
            get
            {
                if (assemblyLocation == null)
                {
                    string assemblyLocation1 = typeof(IA.Config.Controller).Assembly.Location;
                    assemblyLocation = Path.GetDirectoryName(assemblyLocation1);
                }
                return assemblyLocation;
            }
        }

        /// <summary>
        /// Наименования каталогов IA времени исполнения
        /// </summary>
        public enum IARuntimeDirs
        {
            /// <summary>
            /// Каталог с исполняемым файлом IA
            /// </summary>
            [Description("")]
            Main,
            /// <summary>
            /// Каталог Common.
            /// </summary>
            [Description("Common")]
            Common,
            /// <summary>
            /// Каталог Plugins
            /// </summary>
            [Description("Plugins")]
            Plugins
        }

        /// <summary>
        /// Получить путь к каталогу времени выполнения.
        /// </summary>
        /// <param name="dir">Интересующий подкаталог.</param>
        /// <returns></returns>
        public static string GetRTDir(IARuntimeDirs dir)
        {
            return Path.Combine(MainAssemblyDirectory, dir.Description());
        }
    }
}
