﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IA.Config
{
    /// <summary>
    /// Интерфейс, описывающий сущность конфигурационного файла приложения
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        uint ID { get; }
    }

    /// <summary>
    /// Класс, реализующий коллекцию сущностей конфигурационного файла приложения
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public class EntityCollection<T> : IEnumerable<T> where T : IEntity
    {
        /// <summary>
        /// Список сущностей в рамках коллекции
        /// </summary>
        private List<T> collection = new List<T>();

        /// <summary>
        /// Содержится ли сущность с заданным идентификтором в коллекции
        /// </summary>
        /// <param name="id">Идентификтор</param>
        /// <returns></returns>
        public bool ContainsID(uint id)
        {
            return collection.Any(e => e.ID == id);
        }

        /// <summary>
        /// Индексатор
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <returns></returns>
        public T this[uint id]
        {
            get
            {
                return collection.First(e => e.ID == id);
            }
        }

        /// <summary>
        /// Добавить сущность в коллекцию
        /// </summary>
        /// <param name="entity">Сущность</param>
        internal void Add(T entity)
        {
            if (this.ContainsID(entity.ID))
                throw new DuplicateIDsException();

            collection.Add(entity);
        }

        /// <summary>
        /// Очистить коллекцию
        /// </summary>
        internal void Clear()
        {
            collection.Clear();
        }

        /// <summary>
        /// Получить энумератор для коллекции
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator()
        {
            return this.collection.GetEnumerator();
        }

        /// <summary>
        /// Получить энумератор для коллекции
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }

    /// <summary>
    /// Класс, реализующий исключения, возникающие при работе с коллекцией сущностей конфигурационного файла приложения
    /// </summary>
    internal class EntityCollectionException : ConfigException, ISerializable
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public EntityCollectionException()
            : base()
        {
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message">Текст</param>
        public EntityCollectionException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Конустрктор
        /// </summary>
        /// <param name="message">Текст</param>
        /// <param name="inner">Вложенное исплючение</param>
        public EntityCollectionException(string message, Exception inner)
            : base(message, inner)
        {
        }

        /// <summary>
        /// Констрктор
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected EntityCollectionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Класс, реализующий исключения, возникающие при попытке добавить 
    /// </summary>
    internal class DuplicateIDsException : EntityCollectionException, ISerializable
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public DuplicateIDsException()
            : base("Элемент с таким идентификатором уже существует в коллекции сущностей.")
        {
        }

        /// <summary>
        /// Констрктор
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected DuplicateIDsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
