﻿using System.IO;

namespace IA
{
    /// <summary>
    /// Всевозможные настройки для доступа из различных частей приложения
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Текущий пользователь
        /// </summary>
        public static uint UserID = IA.Config.Properties.Settings.Default.UserID;

        /// <summary>
        /// Путь до общей для всех экземпляров IA рабочей директории приложения
        /// </summary>
        public static string CommonWorkDirectoryPath = IA.Config.Properties.Settings.Default.WorkDirectoryPath;
        /// <summary>
        /// Путь до xfcnyjq рабочей директории приложения
        /// </summary>
        public static string PrivateWorkDirectoryPath = Path.GetRandomFileName();

        /// <summary>
        /// Путь до конфигурационного файла приложения
        /// </summary>
        public static string ConfigFilePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, IA.Config.Properties.Settings.Default.ConfigFileName);

        /// <summary>
        /// Режим автономности
        /// </summary>
        public static bool AutonomyMode = IA.Config.Properties.Settings.Default.AutonomyMode;

        /// <summary>
        /// Настройки доступа к серверу проектов
        /// </summary>
        public static class ProjectsServer
        {
            /// <summary>
            /// Сетевое имя сервера
            /// </summary>
            public static string Name = IA.Config.Properties.Settings.Default.ProjectsServer_Name;

            /// <summary>
            /// Имя пользователя для входа на сервер
            /// </summary>
            public static string Login = IA.Config.Properties.Settings.Default.ProjectsServer_Login;

            /// <summary>
            /// Пароль пользователя для входа на сервер
            /// </summary>
            public static string Password = IA.Config.Properties.Settings.Default.ProjectsServer_Password;

            /// <summary>
            /// Путь до директории для хранения информации о проектах на сервере
            /// </summary>
            public static string ProjectsDirectoryPath = IA.Config.Properties.Settings.Default.ProjectsServer_ProjectsDirectoryPath;

            /// <summary>
            /// Установить значения настроек сервера
            /// </summary>
            /// <param name="name"></param>
            /// <param name="login"></param>
            /// <param name="password"></param>
            /// <param name="projectsDirectoryPath"></param>
            public static void Set(string name, string login, string password, string projectsDirectoryPath)
            {
                //Устанавливаем настройки
                ProjectsServer.Name = name;
                ProjectsServer.Login = login;
                ProjectsServer.Password = password;
                ProjectsServer.ProjectsDirectoryPath = projectsDirectoryPath;
            }

            /// <summary>
            /// Сохранить настройки, как настройки по умолчанию
            /// </summary>
            public static void Save()
            {
                IA.Config.Properties.Settings.Default.ProjectsServer_Name = Settings.ProjectsServer.Name;
                IA.Config.Properties.Settings.Default.ProjectsServer_Login = Settings.ProjectsServer.Login;
                IA.Config.Properties.Settings.Default.ProjectsServer_Password = Settings.ProjectsServer.Password;
                IA.Config.Properties.Settings.Default.ProjectsServer_ProjectsDirectoryPath = Settings.ProjectsServer.ProjectsDirectoryPath;
                IA.Config.Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Настройки базы данных SQL Server
        /// </summary>
        public static class SQLServer
        {
            /// <summary>
            /// Имя сервера
            /// </summary>
            public static string Name = IA.Config.Properties.Settings.Default.SQLServer_Name;

            /// <summary>
            /// Логин пользователя для входа на сервер
            /// </summary>
            public static string Login = IA.Config.Properties.Settings.Default.SQLServer_Login;

            /// <summary>
            /// Пароль пользователя для входа на сервер
            /// </summary>
            public static string Password = IA.Config.Properties.Settings.Default.SQLServer_Password;

            /// <summary>
            /// Метод аутентификации пользователя для входа на сервер
            /// </summary>
            public static bool IsWindowsAuthentication = IA.Config.Properties.Settings.Default.SQLServer_IsWindowsAuthentication;

            /// <summary>
            /// Установить значения настроек MS SQL сервера
            /// </summary>
            /// <param name="name"></param>
            /// <param name="login"></param>
            /// <param name="password"></param>
            /// <param name="isWindowsAuthentication"></param>
            public static void Set(string name, string login, string password, bool isWindowsAuthentication)
            {
                //Устанавливаем настройки
                SQLServer.Name = name;
                SQLServer.Login = login;
                SQLServer.Password = password;
                SQLServer.IsWindowsAuthentication = isWindowsAuthentication;                

                //Передаём информацию в сборку IASQL
                IA.Sql.ServerConfiguration.Set(name, login, password, isWindowsAuthentication);
            }

            /// <summary>
            /// Сохранить настройки, как настройки по умолчанию
            /// </summary>
            public static void Save()
            {
                IA.Config.Properties.Settings.Default.SQLServer_Name = Settings.SQLServer.Name;
                IA.Config.Properties.Settings.Default.SQLServer_Login = Settings.SQLServer.Login;
                IA.Config.Properties.Settings.Default.SQLServer_Password = Settings.SQLServer.Password;
                IA.Config.Properties.Settings.Default.SQLServer_IsWindowsAuthentication = Settings.SQLServer.IsWindowsAuthentication;
                
                IA.Config.Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Сохранение всех настроек
        /// </summary>
        public static void SaveAll()
        {
            IA.Config.Properties.Settings.Default.UserID = Settings.UserID;
            IA.Config.Properties.Settings.Default.WorkDirectoryPath = Settings.CommonWorkDirectoryPath;
            IA.Config.Properties.Settings.Default.AutonomyMode = Settings.AutonomyMode;

            //Сохраняем настройки сервера
            Settings.ProjectsServer.Save();

            //Сохраняем настройки MS SQL сервера
            Settings.SQLServer.Save();

            //Сохраняем все настройки как настройки по умолчанию
            IA.Config.Properties.Settings.Default.Save();
        }
    }
}
