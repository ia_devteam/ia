﻿using System.Drawing;

namespace IA.Extensions
{
    /// <summary>
    /// Расширения для работы с изображениями
    /// </summary>
    public static class ImageExtensions
    {
        /// <summary>
        /// Получить копию изображения заданного размера
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <param name="size">Размер</param>
        /// <returns></returns>
        public static Image GetResizedCopy(Image image, Size size)
        {
            return (Image)(new Bitmap(image, size));
        }
    }
}
