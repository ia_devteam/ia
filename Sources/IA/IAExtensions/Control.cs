﻿using System;
using System.Runtime.InteropServices;

namespace IA.Extensions
{
    /// <summary>
    /// Класс для задания дополнительных стилей для элемента управления
    /// </summary>
    public class ControlExtentions
    {
        /// <summary>
        /// Список дополнительных стилей для элемента управления
        /// </summary>
        public enum Styles
        {
            /// <summary>
            /// LVS_EX_GRIDLINES
            /// </summary>
            GridLines = 0x00000001,
            /// <summary>
            /// LVS_EX_SUBITEMIMAGES
            /// </summary>
            SubItemImages = 0x00000002,
            /// <summary>
            /// LVS_EX_CHECKBOXES
            /// </summary>
            CheckBoxes = 0x00000004,
            /// <summary>
            /// LVS_EX_TRACKSELECT
            /// </summary>
            TrackSelect = 0x00000008,
            /// <summary>
            /// LVS_EX_HEADERDRAGDROP
            /// </summary>
            HeaderDragDrop = 0x00000010,
            /// <summary>
            /// LVS_EX_FULLROWSELECT
            /// </summary>
            FullRowSelect = 0x00000020,
            /// <summary>
            /// LVS_EX_ONECLICKACTIVATE
            /// </summary>
            OneClickActivate = 0x00000040,
            /// <summary>
            /// LVS_EX_TWOCLICKACTIVATE
            /// </summary>
            TwoClickActivate = 0x00000080,
            /// <summary>
            /// LVS_EX_FLATSB
            /// </summary>
            FlatsB = 0x00000100,
            /// <summary>
            /// LVS_EX_REGIONAL
            /// </summary>
            Regional = 0x00000200,
            /// <summary>
            /// LVS_EX_INFOTIP
            /// </summary>
            InfoTip = 0x00000400,
            /// <summary>
            /// LVS_EX_UNDERLINEHOT
            /// </summary>
            UnderlineHot = 0x00000800,
            /// <summary>
            /// LVS_EX_UNDERLINECOLD
            /// </summary>
            UnderlineCold = 0x00001000,
            /// <summary>
            /// LVS_EX_MULTIWORKAREAS
            /// </summary>
            MultilWorkAreas = 0x00002000,
            /// <summary>
            /// LVS_EX_LABELTIP
            /// </summary>
            LabelTip = 0x00004000,
            /// <summary>
            /// LVS_EX_BORDERSELECT
            /// </summary>
            BorderSelect = 0x00008000,
            /// <summary>
            /// LVS_EX_DOUBLEBUFFER
            /// </summary>
            DoubleBuffer = 0x00010000,
            /// <summary>
            /// LVS_EX_HIDELABELS
            /// </summary>
            HideLabels = 0x00020000,
            /// <summary>
            /// LVS_EX_SINGLEROW
            /// </summary>
            SingleRow = 0x00040000,
            /// <summary>
            /// LVS_EX_SNAPTOGRID
            /// </summary>
            SnapToGrid = 0x00080000,
            /// <summary>
            /// LVS_EX_SIMPLESELECT
            /// </summary>
            SimpleSelect = 0x00100000
        }

        /// <summary>
        /// Список сообщения для элемента управления - список
        /// </summary>
        private enum Messages
        {
            First = 0x1000,
            SetExtendedStyle = (First + 54),
            GetExtendedStyle = (First + 55),
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr handle, int messg, int wparam, int lparam);

        /// <summary>
        /// Задать дополнительный стиль для элемента управления
        /// </summary>
        /// <param name="control"></param>
        /// <param name="exStyle"></param>
        public static void SetExtendedStyle(System.Windows.Forms.Control control, Styles exStyle)
        {
            Styles styles;
            styles = (Styles)SendMessage(control.Handle, (int)Messages.GetExtendedStyle, 0, 0);
            styles |= exStyle;
            SendMessage(control.Handle, (int)Messages.SetExtendedStyle, 0, (int)styles);
        }

        /// <summary>
        /// Включить двойную буферизацию для элемента управления
        /// </summary>
        /// <param name="control"></param>
        public static void EnableDoubleBuffer(System.Windows.Forms.Control control)
        {
            Styles styles;
            
            //Получаем текущий стиль
            styles = (Styles)SendMessage(control.Handle, (int)Messages.GetExtendedStyle, 0, 0);

            //Активируем двойную буферизацию и выбор границ
            styles |= Styles.DoubleBuffer | Styles.BorderSelect;
            
            //Записываем новый стиль
            SendMessage(control.Handle, (int)Messages.SetExtendedStyle, 0, (int)styles);
        }
        
        /// <summary>
        /// Отключить двойную буферизацию для элемента управления
        /// </summary>
        /// <param name="control"></param>
        public static void DisableDoubleBuffer(System.Windows.Forms.Control control)
        {
            Styles styles;

            //Получаем текущий стиль
            styles = (Styles)SendMessage(control.Handle, (int)Messages.GetExtendedStyle, 0, 0);
            
            //Отключаем двойную буферизацию и выбор границ
            styles -= styles & Styles.DoubleBuffer;
            styles -= styles & Styles.BorderSelect;

            //Записываем новый стиль
            SendMessage(control.Handle, (int)Messages.SetExtendedStyle, 0, (int)styles);
        }
    }
}
