﻿using System;

namespace IA.Extensions
{
    /// <summary>
    /// Расширения для работы с исключениями
    /// </summary>
    public static class ExceptionExtensions
    {
        #region События и делегаты
        /// <summary>
        /// Метод произвольно обрабатываеющий сообщение
        /// </summary>
        /// <param name="message"></param>
        public delegate void MessageHandler(string message);
	    #endregion

        /// <summary>
        /// Преобразовать исключение в многострочную строку.
        /// </summary>
        /// <typeparam name="T">Тип исключения</typeparam>
        /// <param name="exception">Исключение</param>
        /// <returns></returns>
        public static string ToFullMultiLineMessage<T>(this T exception) where T : Exception
        {
            //Прверка на разумность
            if (exception == null)
                throw new ArgumentNullException("exception", "Исключение не определено.");

            //Если внутреннего исключения нет
            if (exception.InnerException == null)
                return exception.Message;

            return new string[]
            {
                exception.Message,
                exception.InnerException.ToFullMultiLineMessage()
            }.ToMultiLineString();
        }

        /// <summary>
        /// Обработчик исключения, возникшего в обработчике какого-либо действия (Например: нажали на кнопку).
        /// </summary>
        /// <typeparam name="T">Тип исключения</typeparam>
        /// <param name="exception">Исключение. Не может быть Null.</param>
        /// <param name="actionName">Наименование действия. Не может быть Null.</param>
        /// <param name="messageHandler">Метод для обработки сообщения. Не может быть Null.</param>
        public static void HandleAsActionHandlerException<T>(this T exception, string actionName, MessageHandler messageHandler) where T : Exception
        {
            //Вызываем обработчик исключения со стандартным комментарием
            exception.Debug_HandleMessage_Release_ReThrow("Ошибка в обработчике действия <" + (actionName ?? ("Неизвестное действие")) + ">.", messageHandler);
        }

        /// <summary>
        /// Обработчик исключения. 
        /// Подразумевает формирование полного сообщения об ошибке с учётом указанного комментария и обработкой его с помощью указанного метода.
        /// </summary>
        /// <typeparam name="T">Тип исключения</typeparam>
        /// <param name="exception">Исключение. Не может быть Null.</param>
        /// <param name="comment">Комментарий. Не может быть Null.</param>
        /// <param name="messageHandler">Метод для обработки сообщения. Не может быть Null.</param>
        public static void HandleMessage<T>(this T exception, string comment, MessageHandler messageHandler) where T : Exception
        {
            //Проверка на разумность
            if (exception == null)
                throw new ArgumentNullException("exception", "Исключение не определено.");

            //Проверка на разумность
            if (comment == null)
                throw new ArgumentNullException("comment", "Комментарий не определён.");

            //Проверка на разумность
            if (messageHandler == null)
                throw new ArgumentNullException("messageHandler", "Метод для обработки сообщения не определён.");

            //Фомируем сообщение
            string message = new string[]
            {
                comment,
                exception.ToFullMultiLineMessage()
            }.ToMultiLineString();

            //Запускаем обработчик сообщения
            messageHandler(message);
        }

        /// <summary>
        /// Обработчик исключения. 
        /// Подразумевает проброс исключения с добавлением указанного комментария.
        /// </summary>
        /// <typeparam name="T">Тип исключения</typeparam>
        /// <param name="exception">Исключение. Не может быть Null.</param>
        /// <param name="comment">Комментарий. Не может быть Null.</param>
        public static void ReThrow<T>(this T exception, string comment) where T : Exception
        {
            //Проверка на разумность
            if (exception == null)
                throw new ArgumentNullException("exception", "Исключение не определено.");

            //Проверка на разумность
            if (comment == null)
                throw new ArgumentNullException("comment", "Комментарий не определён.");

            //Пробрасываем исключение
            throw new Exception(comment, exception);
        }

        /// <summary>
        /// Обработчик исключения. 
        /// Debug - формирование полного сообщения об ошибке с учётом указанного комментария и обработкой его с помощью указанного метода.
        /// Release - проброс исключения с добавлением указанного комментария.
        /// </summary>
        /// <typeparam name="T">Тип исключения</typeparam>
        /// <param name="exception">Исключение. Не может быть Null.</param>
        /// <param name="comment">Комментарий. Не может быть Null.</param>
        /// <param name="messageHandler">Метод для обработки сообщения. Не может быть Null.</param>
        public static void Debug_HandleMessage_Release_ReThrow<T>(this T exception, string comment, MessageHandler messageHandler) where T : Exception
        {
//#if DEBUG
//            //Обрабатываем сообщение
//            exception.HandleMessage(comment, messageHandler);
//#else
//            //Пробрасываем исключение
//            exception.ReThrow(comment);
//#endif
            DebugRelease(
                () => exception.HandleMessage(comment, messageHandler),
                () => exception.ReThrow(comment)
            );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="debugAction"></param>
        /// <param name="releaseAction"></param>
        public static void DebugRelease(Action debugAction, Action releaseAction)
        {
#if DEBUG
            if(debugAction != null)
                debugAction();
#else
            if(releaseAction != null)
                releaseAction();
#endif
        }
    }
}
