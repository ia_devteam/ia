﻿using System;

namespace IA.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class VariableExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static T ThrowIfNull<T>(this T value, string message) where T : class
        {
            if (value == null)
                throw new NullReferenceException(message);

            return value;
        }
    }
}
