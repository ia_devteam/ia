﻿using System;
using System.Runtime.InteropServices;

namespace IA.Extensions
{
    /// <summary>
    /// Расширения для класса TextBox
    /// </summary>
    public static class TextBoxExtensions
    {
        private const uint ECM_FIRST = 0x1500;
        private const uint EM_SETCUEBANNER = ECM_FIRST + 1;

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, uint wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);

        /// <summary>
        /// Отображение подсказки в момент, когда текстбокс пуст
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="watermarkText"></param>
        public static void SetWatermark(this System.Windows.Forms.TextBox textBox, string watermarkText)
        {
            SendMessage(textBox.Handle, EM_SETCUEBANNER, 0, watermarkText);
        }
    }
}
