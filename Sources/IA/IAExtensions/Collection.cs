﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IA.Extensions
{
    /// <summary>
    /// Класс, реализующий полезные методы для работы с коллекциями
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Метод, позволяющий пройти по всем элементам коллекции и сделать одно и тоже действие
        /// </summary>
        /// <typeparam name="T">Тип элементов коллекции</typeparam>
        /// <param name="collection">Коллекция</param>
        /// <param name="action">Действие</param>
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            try
            {
                if(collection == null)
                    throw new Exception("Коллекция не определена.");

                if(action == null)
                    throw new Exception("Действие не определено.");

                foreach (T element in collection)
                    action(element);
            }
            catch(Exception ex)
            {
                ExceptionExtensions.DebugRelease(
                    () => { ex.ReThrow("Ошибка при выполнении действия над всеми элементами коллекции."); },
                    () => { throw ex; }
                );
            }
        }

        /// <summary>
        /// Преобразование коллекции в многострочную строку
        /// </summary>
        /// <typeparam name="T">Тип элементов коллекции</typeparam>
        /// <param name="collection">Коллекция</param>
        /// <returns></returns>
        public static string ToMultiLineString<T>(this IEnumerable<T> collection)
        {
            try
            {
                if (collection == null)
                    throw new Exception("Коллекция не определена.");

                if (collection.First() == null)
                    return String.Empty;

                return collection.Cast<string>().Aggregate((result, next) => result + Environment.NewLine + next);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при преобразовании коллекции в многострочную строку.", ex);
            }
        }
    }
}
