﻿using System;
using System.ComponentModel;

namespace IA.Extensions
{
    /// <summary>
    /// Расширения для класса Enum
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Описание элемента перечисления в строковом формате
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Описание из аттрибута <c>Description</c> если задано, иначе <c>ToString</c>.</returns>
        public static string Description(this Enum value)
        {
            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length == 0 ? value.ToString() : ((DescriptionAttribute)attributes[0]).Description;
        }

        /// <summary>
        /// Полное имя элемента перечисления включая все имена классов и пространств имён
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FullName(this Enum value)
        {
            return value.GetType().ToString() + "." + value.ToString();
        }
    }

    /// <summary>
    /// Класс реализующий цикл по всем элементам перечисления
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EnumLoop<T> where T : struct, IConvertible
    {
        static readonly T[] arr = typeof(T).IsEnum ? (T[])Enum.GetValues(typeof(T)) : null;

        /// <summary>
        /// Метод реализующий цикл по всем элементам перечисления
        /// </summary>
        /// <param name="act"></param>
        static public void ForEach(Action<T> act)
        {
            if (arr == null)
                throw new ArgumentException("EnumLoop: Тип <" + typeof(T).FullName + "> не является Enum-типом. Перечисление значений невозможно.");

            for (int i = 0; i < arr.Length; i++)
            {
                act(arr[i]);
            }
        }
    }
}
