﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

using TestUtils;
using Store;

namespace TestCheckSum
{
    [TestClass]
    public class TestCheckSum
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        [TestMethod]
        public void CheckSum1()
        {
            const string sourcePrefixPart = @"Sources\CSharpBuilt\ComplexStatements\";
            testUtils.RunTest(
                sourcePrefixPart,                                                   // sourcePrefixPart
                new IA.Plugins.Analyses.CheckSum.Plugin(),                          // Plugin
                false,                                                              // isUseCachedStorage
                (storage, testMaterialsPath) => { },                                // prepareStorage
                (storage, testMaterialsPath) =>
                {
                    TestUtilsClass.Run_FillFileList(storage, System.IO.Path.Combine(testMaterialsPath, sourcePrefixPart));
                },                                                                  // customizeStorage
                (storage, testMaterialsPath) =>
                {
                    return Check(storage, testMaterialsPath);
                },                                                                  // checkStorage
                (reportsPath, testMaterialsPath) =>
                {
                    return true;
                },                                                                  // checkReportBeforeRerun
                false,                                                              // isUpdateReport
                (plugin, testMaterialsPath) => { },                                 // changeSettingsBeforeRerun
                (storage, testMaterialsPath) => { return true; },                   // checkStorageAfterRerun
                (reportsPath, testMaterialsPath) => { return true; }                // checkReportAfterRerun
                );
        }

        /// <summary>
        /// Проверка результатов теста
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="testMaterialsPath"></param>
        /// <returns></returns>
        private bool Check(Storage storage, string testMaterialsPath)
        {
            //Имена логов
            string tempFileName = "res.log";
            string tempLog = System.IO.Path.Combine(testUtils.TestRunDirectoryPath, tempFileName);

            //Пишем лог
            var stream = new System.IO.StreamWriter(tempLog, false);
            foreach(IFile f in storage.files.EnumerateFiles( enFileKind.anyFile))
                stream.WriteLine(f.FileNameForReports + " " + f.CheckSum.Aggregate("", (total, next) => total + System.Convert.ToString(next, 16))); 
            stream.Close();

            //сравниваем результат
            return TestUtils.TestUtilsClass.Reports_Lines_Compare(tempLog, System.IO.Path.Combine(testUtils.TestMatirialsDirectoryPath, "CheckSum" , tempFileName));
        }
    }
}
