﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Store;
using PerlAnalizer;
using System.IO;

using StorageImporter;
using System.Xml;
using System.Diagnostics;

namespace PerlAnalizer
{
    class Program
    {
        static void Main(string[] args)
        {
            string basePath = "E:\\creation\\perl_parser\\";
            string currentDirectory = Environment.CurrentDirectory;
            
            Console.WriteLine(string.Format("Current diretory = {0} \n", currentDirectory));
            // remove perl_analizer content
            String path = Path.Combine(basePath, "perl_analizer");
            String srcOriginalPath = Path.Combine(basePath, "perl_analizer", "str");
            
            // таким образом мы может создавать новые хранилища используя одну и туже копию 
            // исходников
            PerlAnalizerUtils.TestCleanDirectoryExceptSrcOriginal(path);

            Storage nStore;
            if (Directory.Exists(srcOriginalPath))
            {
                nStore = PerlAnalizerUtils.TestStorageCreator(basePath, "perl_analizer", "open");
            }
            else
            {
                nStore = PerlAnalizerUtils.TestStorageCreator(basePath, "perl_analizer");
            }

            

            // попытка запустить testPerlHandler который умеет генерировать xml         
            Dictionary<string, string> perlArgs = new Dictionary<string, string>();
            perlArgs["program"] = "E:\\creation\\ia\\Externals\\Perl\\t\\testPerlHandler.t";
            perlArgs["cwd"] = "E:\\creation\\ia\\Externals\\Perl";

            Dictionary<string, string> result = PerlAnalizerUtils.RunPerlAnalizer(perlArgs);

            Console.Write("Output:");
            Console.WriteLine(result["output"]);
            Console.Write("Errors:");
            Console.WriteLine(result["errors"]);

            // пытаемся загрузить сгенерированный xml
            XmlReader reader;
            reader = XmlReader.Create("E:\\creation\\ia\\Externals\\Perl\\struct_simple.xml");
            XmlDocument xml = new XmlDocument();
            xml.Load(reader);
            reader.Close();

            Console.WriteLine(String.Format("CurDir: {0}", Directory.GetCurrentDirectory()));

            XMLStorageImporterForParser xmlLoader = new XMLStorageImporterForParser();
            xmlLoader.ImportXML(xml, nStore);



            //nStore.appliedSettings.FileListPath
            //соответсвенно файлы с исходниками должны лежать в этой каталожке, но тогда нужно будет 
            // каждый раз копировать исходники, что не есть хорошо
            // E:\\creation\\perl_parser\\perl_analizer\\src_original
            PerlAnalizerUtils.TestStorageDestructor(nStore);

            Console.ReadLine();
        }
    }
}
