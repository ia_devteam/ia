﻿using System;
/*
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;
*/
using System.Text;
using System.IO;
using Store;

using System.Diagnostics;
using System.Collections.Generic;
using System.Threading;

namespace PerlAnalizer
{
    /* Утилиты используемые при 
     * анализе perl кода
     */
    class PerlAnalizerUtils
    {
        public static Storage TestStorageCreator(String DirPath = null, String postfix = null, String mode = null)
        {
            string currentDirectory = Environment.CurrentDirectory;
            Storage nStore = new Storage();
            String lDirPath;
            if (DirPath != null)
            {
                lDirPath = DirPath;
            }
            else
            {
                lDirPath = TestGetWorkingDirectory();
            }

            //Получаем путь до будующей рабочей директории Хранилища
            string storageWorkDirectoryPath = postfix == null ? lDirPath : Path.Combine(lDirPath, postfix);

            //Принудительно создаём рабочую директорию
            if (!Directory.Exists(storageWorkDirectoryPath))
                Directory.CreateDirectory(storageWorkDirectoryPath);

            //Создаём Хранилище
            if (mode == "open")
            {
                nStore.Open(storageWorkDirectoryPath, Storage.OpenMode.OPEN);
            }
            else
            {
                nStore.Open(storageWorkDirectoryPath, Storage.OpenMode.CREATE);
            }           
            return nStore;
        }

        //
        public static void TestStorageDestructor(Storage nStore)
        {
            if (!nStore.isClosed)
            {
                nStore.Close();
            }            
        }

        //
        public static string TestGetDirectory()
        {
            string currentDirectory = TestGetWorkingDirectory();
            Console.WriteLine("currentDirectory = %s \n", currentDirectory);
            return Path.Combine(currentDirectory, "perl_storage");
        }

        //
        public static string TestGetWorkingDirectory()
        {
            string currentDirectory = Environment.CurrentDirectory;
            if (currentDirectory.Contains("TestResults"))
                currentDirectory = Path.Combine(currentDirectory, "../..");
            return currentDirectory;
        }

        //
        public static void cleanDirectory(String directoryPath)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(directoryPath);
            if (!Directory.Exists(directoryPath)) return;
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }

        //
        public static void TestCleanDirectoryExceptSrcOriginal(String directoryPath)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(directoryPath);
            if (!Directory.Exists(directoryPath)) return;
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                if (!dir.FullName.Contains("src_original"))
                {
                    dir.Delete(true);
                }
            }
        }

        // запускает perl анализатор, 
        // получает путь до xml файла
        public static Dictionary<string, string> RunPerlAnalizer(Dictionary <string, string> args)
        {
            string arguments = "";
            string cwd = "";
            string tempCwd = Directory.GetCurrentDirectory(); ;
            string progPath = ""; // обязательное
            if (args.ContainsKey("arguments"))
            {
                arguments = args["arguments"];
            }
            if (args.ContainsKey("cwd"))
            {
                cwd = args["cwd"];
            }
            progPath = args["program"];
            

            // устанавливаем рабочий каталог
            if (cwd != "") 
                Directory.SetCurrentDirectory(cwd);            

            string fArguments = progPath + " " + arguments;

            StringBuilder output;
            StringBuilder error;

            Dictionary<string, string> result = _callPerlProcess(fArguments);            
            //string output = perl.StandardOutput.ReadToEnd();
            //string stderr = perl.StandardError.ReadToEnd();

            // восстанавливаем рабочий каталог
            if (cwd != "")            
                Directory.SetCurrentDirectory(tempCwd);            

            return result;          
        }

        // непосредственный запус perl программы
        public static Dictionary<string, string> _callPerlProcess(string arguments)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            using (Process perl = new Process())
            {

                ProcessStartInfo perlStartInfo = new ProcessStartInfo(@"C:\Strawberry\perl\bin\perl.exe");

                perlStartInfo.Arguments = arguments;

                Console.WriteLine(String.Format("arguments:{0}", arguments));

                perlStartInfo.UseShellExecute = false;
                perlStartInfo.RedirectStandardOutput = true;
                perlStartInfo.RedirectStandardError = true;
                perlStartInfo.CreateNoWindow = false;

                StringBuilder output = new StringBuilder();
                StringBuilder error = new StringBuilder();


                perl.StartInfo = perlStartInfo;

                // http://stackoverflow.com/questions/20182670/calling-perl-script-from-c-sharp-service
                // http://stackoverflow.com/questions/139593/processstartinfo-hanging-on-waitforexit-why

                int timeout = 100;

                using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
                using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
                {
                    perl.OutputDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            outputWaitHandle.Set();
                        }
                        else
                        {
                            output.AppendLine(e.Data);
                        }
                    };
                    perl.ErrorDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            errorWaitHandle.Set();
                        }
                        else
                        {
                            error.AppendLine(e.Data);
                        }
                    };


                    perl.Start();
                    Console.WriteLine("PerlProgram started...");

                    if (perl.WaitForExit(timeout) &&
                        outputWaitHandle.WaitOne(timeout) &&
                        errorWaitHandle.WaitOne(timeout))
                    {
                        // Process completed. Check process.ExitCode here.
                    }
                    else
                    {
                        // Timed out.
                    }
                }

                //perl.WaitForExit();
                Console.WriteLine("PerlProgram finised...");
                result["output"] = output.ToString() + perl.StandardOutput.ReadToEnd();
                result["errors"] = error.ToString() + perl.StandardError.ReadToEnd();
                return result;
            }
        }
    }
}
