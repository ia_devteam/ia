﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Store;
using PerlAnalizer;

namespace TestAnalizer
{
    class Program
    {
        static void Main(string[] args)
        {
            Storage nStore = PerlAnalizerUtils.TestStorageCreator("E:\\creation\\perl_parser\\", "perl_analizer");
            PerlAnalizerUtils.TestStorageDestructor(nStore);
            Console.ReadLine();
        }
    }
}
