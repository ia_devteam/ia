﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

using Store;


namespace PerlAnalizer
{
    /* Класс функции утилиты используемые при 
     * анализе perl кода
     */
    class PerlAnalizerUtils
    {
        public static Storage TestStorageCreator(String DirPath = null, String postfix = null)
        {
            string currentDirectory = Environment.CurrentDirectory;
            Storage nStore = new Storage();
            String lDirPath;
            if (DirPath != null)
            {
                lDirPath = DirPath;
            }
            else
            {
                lDirPath = TestGetWorkingDirectory();
            }

            //Получаем путь до будующей рабочей директории Хранилища
            string storageWorkDirectoryPath = postfix == null ? lDirPath : Path.Combine(lDirPath, postfix);

            //Принудительно создаём рабочую директорию
            if (!Directory.Exists(storageWorkDirectoryPath))
                Directory.CreateDirectory(storageWorkDirectoryPath);

            //Создаём Хранилище
            nStore.Open(storageWorkDirectoryPath, Storage.OpenMode.CREATE);
            return nStore;
        }

        public static void TestStorageDestructor(Storage nStore)
        {
            nStore.Close();
        }

        public static string TestGetDirectory()
        {
            string currentDirectory = TestGetWorkingDirectory();
            Console.WriteLine("currentDirectory = %s \n", currentDirectory);
            return Path.Combine(currentDirectory, "perl_storage");
        }


        private static string TestGetWorkingDirectory()
        {
            string currentDirectory = Environment.CurrentDirectory;
            if (currentDirectory.Contains("TestResults"))
                currentDirectory = Path.Combine(currentDirectory, "../..");
            return currentDirectory;
        }

    }
}
