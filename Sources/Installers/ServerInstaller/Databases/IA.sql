USE [ia]
GO
/****** Object:  Table [dbo].[materials]    Script Date: 05.03.2015 18:21:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[materials](
	[id] [bigint] IDENTITY(294,1) NOT NULL,
	[project] [bigint] NOT NULL CONSTRAINT [DF__materials__paren__7E6CC920]  DEFAULT (NULL),
	[version] [tinyint] NOT NULL,
	[datetime] [datetime] NOT NULL CONSTRAINT [DF__materials__datet__7F60ED59]  DEFAULT (getdate()),
	[comment] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_materials_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[projects]    Script Date: 05.03.2015 18:21:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[projects](
	[id] [bigint] IDENTITY(35,1) NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[path] [nvarchar](255) NOT NULL,
	[settings] [varchar](255) NOT NULL,
 CONSTRAINT [PK_projects_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[storages]    Script Date: 05.03.2015 18:21:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[storages](
	[id] [bigint] IDENTITY(1627,1) NOT NULL,
	[materials] [bigint] NOT NULL,
	[parent] [bigint] NOT NULL CONSTRAINT [DF__storages__parent__03317E3D]  DEFAULT (NULL),
	[datetime] [datetime] NOT NULL CONSTRAINT [DF__storages__dateti__0425A276]  DEFAULT (getdate()),
	[creator] [bigint] NOT NULL,
	[empty] [varchar](5) NOT NULL CONSTRAINT [DF__storages__empty__0519C6AF]  DEFAULT (N'true'),
	[comment] [nvarchar](1000) NOT NULL,
	[runned_plugins] [nvarchar](1000) NOT NULL CONSTRAINT [DF_storages_completed_plugins]  DEFAULT (''),
	[completed_plugins] [nvarchar](1000) NOT NULL CONSTRAINT [DF_storages_completed_plugins_1]  DEFAULT (''),
	[mode] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_storages_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users]    Script Date: 05.03.2015 18:21:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[id] [bigint] IDENTITY(16,1) NOT NULL,
	[surname] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[patronymic] [nvarchar](50) NOT NULL,
	[deleted] [varchar](5) NOT NULL CONSTRAINT [DF_users_deleted]  DEFAULT (N'false'),
 CONSTRAINT [PK_users_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[versions]    Script Date: 05.03.2015 18:21:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[versions](
	[version] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[storages]  WITH CHECK ADD  CONSTRAINT [CK__storages__mode__3D5E1FD2] CHECK  (([mode]='expert' OR [mode]='user'))
GO
ALTER TABLE [dbo].[storages] CHECK CONSTRAINT [CK__storages__mode__3D5E1FD2]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [CK__users__deleted__4D94879B] CHECK  (([deleted]='false' OR [deleted]='true'))
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [CK__users__deleted__4D94879B]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'ia.materials' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'materials'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'ia.projects' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'projects'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'ia.storages' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'storages'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'ia.users' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users'
GO

USE [ia]
GO
SET IDENTITY_INSERT [dbo].[versions] ON

INSERT [dbo].[versions] ([version]) VALUES (1)
INSERT [dbo].[versions] ([version]) VALUES (2)
INSERT [dbo].[versions] ([version]) VALUES (3)
INSERT [dbo].[versions] ([version]) VALUES (4)
INSERT [dbo].[versions] ([version]) VALUES (5)
INSERT [dbo].[versions] ([version]) VALUES (6)
INSERT [dbo].[versions] ([version]) VALUES (7)
INSERT [dbo].[versions] ([version]) VALUES (8)
SET IDENTITY_INSERT [dbo].[versions] OFF