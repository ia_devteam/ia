﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

using IA.Extensions;

namespace IA.Administration
{
    static class Program
    {
        #region Константы
        /// <summary>
        /// Наименование приложения
        /// </summary>
        internal const string ApplicationName = "Административный модуль приложения IA";

        /// <summary>
        /// Наименование разработчика приложения
        /// </summary>
        internal const string DeveloperName = "ЗАО РНТ";
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Проверка на отстутствие дубликатов приложения
            bool isNoDuplicates;
            Mutex mutex = new Mutex(true, "IA.IA.Admin", out isNoDuplicates);

            if (!isNoDuplicates)
            {
                //Отображаем ошибку
                MessageBox.Show("Приложение <" + Program.ApplicationName + "> уже запущено на данном компьютере. Будьте осторожны! ", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            try
            {
                Rectangle resolution = Screen.PrimaryScreen.Bounds;
                Size applicationSize = new Size(2 * resolution.Width / 3, 2 * resolution.Height / 3);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm() { Size = applicationSize });
            }
            catch (Exception ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    "Произошла критическая ошибка в ходе работы приложения.",
#if DEBUG
                    ex.ToFullMultiLineMessage(),
#endif
                    Constants.Message.ApplicationWillBeClosed,
                    Constants.Message.СontactTheManufacturer
                }.ToMultiLineString();

                //Отображаем ошибку
                MessageBox.Show(message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

#if !DEBUG
                //Записываем ошибку в файл
                string errorLogFilePath = System.IO.Path.Combine(Application.UserAppDataPath, Constants.Files.ErrorLogFileName);
                System.IO.File.WriteAllText(errorLogFilePath, ex.ToFullMultiLineMessage());
#endif

                //Принудительно закрываем приложение
                Environment.Exit(1);
            }
            finally
            {
                //Теперь этот объект больше не нужен, его можно уничтожить
                GC.KeepAlive(mutex);
            }
        }
    }
}
