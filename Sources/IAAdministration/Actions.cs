﻿using System.ComponentModel;

namespace IA.Administration.Actions
{
    /// <summary>
    /// Глобальные действия в рамках приложения
    /// </summary>
    internal enum Global
    {
        /// <summary>
        /// Настройки приложения
        /// </summary>
        [Description("Настройки приложения")]
        SETTINGS,
        [Description("Создать локальную БД")]
        CREATESTANDALONEDB
    }

    /// <summary>
    /// Всевозможные исключения в рамках приложения
    /// </summary>
    internal enum Exceptions
    {
        /// <summary>
        /// Обнаружена неизвестная глобальная операция в приложении
        /// </summary>
        [Description("Обнаружена неизвестная глобальная операция в приложении.")]
        UNKNOWN_GLOBAL_ACTION
    }
}


