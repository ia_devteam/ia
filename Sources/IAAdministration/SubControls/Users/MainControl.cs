﻿using System.Windows.Forms;

namespace IA.Administration.SubControls.Users
{
    /// <summary>
    /// Класс, реализующий дочерний элемент административного модуля "Пользователи"
    /// </summary>
    internal partial class MainControl : UserControl
    {
        /// <summary>
        /// Элемент управления - Список пользователей
        /// </summary>
        private IA.Controls.UserListControl userListControl = null;
        
        /// <summary>
        /// Элемент управления - Настройки пользователя
        /// </summary>
        private IA.Controls.UserSettingsControl userSettingsControl = null;

        /// <summary>
        /// Констркутор
        /// </summary>
        /// <param name="iaDatabaseConnection">Соединение с базой данных "ia"</param>
        internal MainControl()
            : base()
        {
            InitializeComponent();

            Name = "Users";
            Text = "Пользователи";

            //Инициализация элемента управления - Настройки пользователя
            this.userSettingsControl = new IA.Controls.UserSettingsControl()
            {
                Dock = DockStyle.Fill,
                IsBlocked = true,
                IsEnabled = false
            };

            //Инициализация элемента управления - Список пользователей
            this.userListControl = new IA.Controls.UserListControl(0)
            {
                Dock = DockStyle.Fill
            };

            this.userListControl.UserClickEvent += delegate(IA.Objects.User user)
            {
                //Определяем доступен ли список настроек
                userSettingsControl.IsEnabled = user != null;

                //Если пользователь не выбран, ничего не делаем
                if (user == null)
                    return;

                //Отображаем настройки пользователя
                this.userSettingsControl.User = user;
            };

            //Инициализация элемента управления - Сплит контейнер для закладки "Пользователи"
            SplitContainer users_splitContainer = new SplitContainer()
            {
                Dock = DockStyle.Fill,
                Orientation = Orientation.Vertical,
                SplitterDistance = 60,
            };
            users_splitContainer.Panel1.Controls.Add(this.userListControl);
            users_splitContainer.Panel2.Controls.Add(this.userSettingsControl);

            this.Controls.Add(users_splitContainer);
        }

        
        #region SqlDependentControl
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Обрабатываем дочерние элементы управления
            this.userSettingsControl.Enabled = false;

            //Делаем элемент управления доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Обрабатываем дочерние элементы управления
            this.userSettingsControl.Enabled = true;

            //Делаем его недоступным
            this.Enabled = false;
        }
        #endregion
    }
}
