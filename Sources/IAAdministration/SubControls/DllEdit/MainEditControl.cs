﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;

using IA.Extensions;
using IA.Sql;
using IA.Sql.DatabaseConnections;

namespace IA.Administration.SubControls.DllEdit
{
    /// <summary>
    /// Класс - Редактирование записей лавной формы
    /// </summary>
    internal partial class MainEditControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Элемент управления - Редактирование записей главной формы";

        #region Поля
        /// <summary>
        /// Режим работы с информацией о библиотеке
        /// </summary>
        EditMode mode;
        /// <summary>
        /// Идентификатор записи в таблице типов файлов
        /// </summary>
        string id;
        /// <summary>
        /// Таблица функций                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        /// </summary>
        List<string[]> lsFuncs = new List<string[]>();
        /// <summary>
        /// Таблица импортируемых функций
        /// </summary>
        List<string[]> lsImports = new List<string[]>();
        /// <summary>
        /// Исходная таблица функций
        /// </summary>
        List<string[]> lsBaseFuncs = new List<string[]>();
        /// <summary>
        /// Исходная таблица импортируемых функций
        /// </summary>
        List<string[]> lsBaseImports = new List<string[]>();
        /// <summary>
        /// Таблица функций из файла
        /// </summary>
        List<string> lsFileFuncs = new List<string>();
        /// <summary>
        /// Таблица импортируемых функций из файла
        /// </summary>
        List<string> lsFileImports = new List<string>();
        /// <summary>
        /// Содержимо файла
        /// </summary>
        byte[] ImageFile = null; 
        #endregion

        /// <summary>
        /// Констркутор
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="id"></param>
        internal MainEditControl(EditMode mode, string id)
            : this(mode, id, new Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection())
        {
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="id"></param>
        /// <param name="dllKnowledgeBaseDatabaseConnection">Cоединение с базой данных "DllKnowledgeBase"</param>
        internal MainEditControl(EditMode mode, string id, Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection dllKnowledgeBaseDatabaseConnection)
            : base()
        {
            InitializeComponent();

            this.mode = mode;
            this.id = id;

            ToolTip toolTip1 = new ToolTip();

            toolTip1.SetToolTip(this.buttonAddFunction, "Добавление новой экспортируемой функции");
            toolTip1.SetToolTip(this.buttonDeleteFunction, "Удаление выделенной экспортируемой функции");
            toolTip1.SetToolTip(this.buttonUpdateFunction, "Изменение выделенной экспортируемой функции");
            toolTip1.SetToolTip(this.buttonAddImport, "Добавление новой импортируемой функции");
            toolTip1.SetToolTip(this.buttonDeleteImport, "Удаление выделенной импортируемой функции");
            toolTip1.SetToolTip(this.buttonUpdateImport, "Изменение выделенной импортируемой функции");
            toolTip1.SetToolTip(this.buttonBrowse, "Обзор библиотечного файла");
            toolTip1.SetToolTip(this.buttonCopy, "Копирование данных в поля группы \"База данных\"\n" +
                                                 "для сохранения в базе данных");
            if (mode != EditMode.DELETE)
                toolTip1.SetToolTip(this.buttonSaveForm, "Сохранение данных в базе данных");
            else
                toolTip1.SetToolTip(this.buttonSaveForm, "Удаление данных из базы данных");
        }

        /// <summary>
        /// Заполнить элемент управления в соответствии с содержимым базы данных
        /// Предполагается, что соединение с базой данных было установлено ранее
        /// </summary>
        private void Generate()
        {
            //Очищаем элемент управления
            this.Clear();

            switch (this.mode)
            {
                case EditMode.NEW:
                    this.Text = "Заведение новой библиотеки";
                    break;
                case EditMode.UPDATE:
                    this.Text = "Изменение библиотеки";
                    break;
                case EditMode.DELETE:
                    this.Text = "Удаление библиотеки";
                    buttonSaveForm.Text = "Удалить";

                    buttonAddFunction.Enabled = false;
                    buttonUpdateFunction.Enabled = false;
                    buttonDeleteFunction.Enabled = false;
                    buttonAddImport.Enabled = false;
                    buttonUpdateImport.Enabled = false;
                    buttonDeleteImport.Enabled = false;
                    buttonBrowse.Enabled = false;
                    buttonCopy.Enabled = false;

                    textBoxBaseNumber.Enabled = false;
                    textBoxBaseName.Enabled = false;
                    textBoxBaseAuthor.Enabled = false;
                    textBoxBaseDescription.Enabled = false;
                    textBoxBaseMD5.Enabled = false;
                    textBoxBaseVersion.Enabled = false;

                    textBoxFileName.Enabled = false;
                    textBoxFileAuthor.Enabled = false;
                    textBoxFileDescription.Enabled = false;
                    textBoxFileMD5.Enabled = false;
                    textBoxFileVersion.Enabled = false;

                    checkBoxType.Enabled = false;

                    break;
            }

            if (this.mode != EditMode.NEW)
            {
                string[] sData = ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).DllBaseData(int.Parse(id)).FirstOrDefault();
                if (sData != null)
                {
                    textBoxBaseNumber.Text = sData[0];
                    textBoxBaseName.Text = sData[1];
                    textBoxBaseAuthor.Text = sData[2];
                    textBoxBaseDescription.Text = sData[3];
                    textBoxBaseMD5.Text = sData[4];
                    textBoxBaseVersion.Text = sData[5];
                    checkBoxType.Checked = sData[6] != "1";

                    lsFuncs.Clear();
                    lsImports.Clear();
                    lsBaseFuncs = ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).DllFunctions(int.Parse(id));
                    lsBaseImports = ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).DllImports(int.Parse(id));
                    int cnt = 1;
                    foreach (string[] sa in lsBaseFuncs)
                    {
                        if (sa[5] != "1")
                            sa[5] = "0";
                        string[] sb = new string[] { cnt++.ToString() }.Concat(sa).ToArray();
                        lsFuncs.Add(sb);
                    }
                    cnt = 1;
                    foreach (string[] sa in lsBaseImports)
                    {
                        if (sa[6] != "1")
                            sa[6] = "0";
                        string[] sb = new string[] { cnt++.ToString() }.Concat(sa).ToArray();
                        lsImports.Add(sb);
                    }

                    FillData(id);
                }
            }
        }

        /// <summary>
        /// Очистить элемент управления
        /// </summary>
        private void Clear()
        {
            //this
            this.Text = "Окно редактирования информации о библиотеках";

            //Button
            buttonSaveForm.Text = "Сохранить";

            //TextBox
            this.textBoxBaseAuthor.Text = String.Empty;
            this.textBoxBaseDescription.Text = String.Empty;
            this.textBoxBaseMD5.Text = String.Empty;
            this.textBoxBaseName.Text = String.Empty;
            this.textBoxBaseNumber.Text = String.Empty;
            this.textBoxBaseVersion.Text = String.Empty;
            this.textBoxFileAuthor.Text = String.Empty;
            this.textBoxFileDescription.Text = String.Empty;
            this.textBoxFileFullName.Text = String.Empty;
            this.textBoxFileMD5.Text = String.Empty;
            this.textBoxFileName.Text = String.Empty;
            this.textBoxFileVersion.Text = String.Empty;

            //DataGridView
            this.dataGridViewBaseFunctions.Rows.Clear();
            this.dataGridViewBaseImports.Rows.Clear();
            this.dataGridViewFileFunctions.Rows.Clear();
            this.dataGridViewFileImports.Rows.Clear();

            //CheckBox
            this.checkBoxType.Checked = false;
        }
        
        /// <summary>
        /// Заполнение полей формы значениями из базы
        /// </summary>
        /// <param name="id"></param>
        void FillData(string id)
        {
            dataGridViewBaseFunctions.Rows.Clear();
            dataGridViewBaseImports.Rows.Clear();

            foreach (string[] sa in lsFuncs)
            {
                dataGridViewBaseFunctions.Rows.Add(sa);
            }
            foreach (string[] sa in lsImports)
            {
                dataGridViewBaseImports.Rows.Add(sa);
            }
        }

        /// <summary>
        /// Заполнение полей формы значениями из файла
        /// </summary>
        /// <param name="lsFileFuncs"></param>
        /// <param name="lsFileImports"></param>
        void FillFileData(List<string> lsFileFuncs, List<string> lsFileImports)
        {
            int cnt=1;
            dataGridViewFileFunctions.Rows.Clear();
            dataGridViewFileImports.Rows.Clear();
            foreach(string s in lsFileFuncs)
            {
                string[] sa = s.Split('\t');
                dataGridViewFileFunctions.Rows.Add(new string[] { cnt++.ToString(), "0", "0", sa[1], "", sa[0], "0" });
            }
            cnt = 1;
            foreach (string s in lsFileImports)
            {
                string[] sa = s.Split('\t');
                dataGridViewFileImports.Rows.Add(new string[] { cnt++.ToString(), "0", "0", sa[0], sa[1], "", sa[2], "0" });
            }

        }

        #region Кнопки
        /// <summary>
        /// Обзор файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                string filename;
                OpenFileDialog of = new OpenFileDialog();
                if (of.ShowDialog() == DialogResult.OK)
                {
                    filename = of.FileName;
                    textBoxFileFullName.Text = filename;
                    textBoxFileName.Text = of.SafeFileName;
                    textBoxFileMD5.Text = MainControl.ComputeCtrlSum(filename);
                    List<string> ss = MainControl.AuthorDescription(filename);
                    textBoxFileAuthor.Text = ss[0];
                    textBoxFileDescription.Text = ss[1];
                    textBoxFileVersion.Text = ss[2];
                    lsFileFuncs = ImpExp.ImpExp.Export(filename);
                    lsFileImports = ImpExp.ImpExp.Import(filename);
                    ImageFile = File.ReadAllBytes(filename);
                    FillFileData(lsFileFuncs, lsFileImports);
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Добавление функции
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddFunction_Click(object sender, EventArgs e)
        {
            try
            {
                string[] srecord = new string[] { (lsFuncs.Count + 1).ToString(), "", "", "", "", "", "" };
                Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                FuncEditControl fec;
                fm.Controls.Add(fec = new FuncEditControl(EditMode.NEW, ref srecord) { Dock = DockStyle.Fill });
                if (fm.ShowDialog() == DialogResult.OK)
                {
                    lsFuncs.Add(srecord);
                }
                fm.Controls.Remove(fec);
                FillData(id);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Добавление импортируемой функции
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddImport_Click(object sender, EventArgs e)
        {
            try
            {
                string[] srecord = new string[] { (lsImports.Count + 1).ToString(), "", "", "", "", "", "", "" };
                Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                fm.Controls.Add(new ImportEditControl(EditMode.NEW, ref srecord) { Dock = DockStyle.Fill });
                if (fm.ShowDialog() == DialogResult.OK)
                {
                    lsImports.Add(srecord);
                }
                FillData(id);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Изменение импортируемой функции
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdateImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewBaseImports.SelectedCells.Count > 0)
                {
                    int index = dataGridViewBaseImports.SelectedCells[0].RowIndex;
                    string[] srecord = lsImports[index];

                    Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                    fm.Controls.Add(new ImportEditControl(EditMode.UPDATE, ref srecord) { Dock = DockStyle.Fill });
                    if (fm.ShowDialog() == DialogResult.OK)
                    {
                        lsImports[index] = srecord;
                    }
                    FillData(id);
                }
                else
                {
                    Monitor.Log.Warning(ObjectName, "Выделите запись в таблице \"Импортируемые функции\"", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Удаление импортируемой функции
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeleteImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewBaseImports.SelectedCells.Count > 0)
                {
                    int index = dataGridViewBaseImports.SelectedCells[0].RowIndex;
                    string[] srecord = lsImports[index];
                    Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                    fm.Controls.Add(new ImportEditControl(EditMode.DELETE, ref srecord) { Dock = DockStyle.Fill });
                    if (fm.ShowDialog() == DialogResult.OK)
                    {
                        lsImports.RemoveAt(index);
                    }
                    FillData(id);
                }
                else
                {
                    Monitor.Log.Warning(ObjectName, "Выделите запись в таблице \"Импортируемые функции\"", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Копирование данных, полученных из файла в форму
        /// Копируются только в те элементы формы, где нет значений
        /// Отдельно проверяестя контрольная сумма - для Image файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCopy_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(textBoxBaseName.Text) && !string.IsNullOrWhiteSpace(textBoxFileName.Text))
                {
                    textBoxBaseName.Text = textBoxFileName.Text;
                    textBoxBaseName.BackColor = Color.Yellow;
                    checkBoxType.Checked = true;
                }

                if (string.IsNullOrWhiteSpace(textBoxBaseAuthor.Text) && !string.IsNullOrWhiteSpace(textBoxFileAuthor.Text))
                {
                    textBoxBaseAuthor.Text = textBoxFileAuthor.Text;
                    textBoxBaseAuthor.BackColor = Color.Yellow;
                    checkBoxType.Checked = true;
                }

                if (string.IsNullOrWhiteSpace(textBoxBaseDescription.Text) && !string.IsNullOrWhiteSpace(textBoxFileDescription.Text))
                {
                    textBoxBaseDescription.Text = textBoxFileDescription.Text;
                    textBoxBaseDescription.BackColor = Color.Yellow;
                    checkBoxType.Checked = true;
                }

                if (string.IsNullOrWhiteSpace(textBoxBaseMD5.Text) && !string.IsNullOrWhiteSpace(textBoxFileMD5.Text))
                {
                    textBoxBaseMD5.Text = textBoxFileMD5.Text;
                    textBoxBaseMD5.BackColor = Color.Yellow;
                    checkBoxType.Checked = true;
                }
                else
                {
                    if (textBoxBaseMD5.Text != textBoxFileMD5.Text)
                        ImageFile = null;
                    checkBoxType.Checked = true;
                }

                if (string.IsNullOrWhiteSpace(textBoxBaseVersion.Text) && !string.IsNullOrWhiteSpace(textBoxFileVersion.Text))
                {
                    textBoxBaseVersion.Text = textBoxFileVersion.Text;
                    textBoxBaseVersion.BackColor = Color.Yellow;
                    checkBoxType.Checked = true;
                }

                if (lsFuncs.Count == 0 && lsFileFuncs.Count > 0)
                {
                    int cnt = 1;
                    foreach (string s in lsFileFuncs)
                    {
                        string[] sa = s.Split('\t');
                        dataGridViewBaseFunctions.Rows.Add(new string[] { cnt.ToString(), "0", "0", sa[1], "", sa[0], "0" });
                        lsFuncs.Add(new string[] { cnt++.ToString(), "0", "0", sa[1], "", sa[0], "0" });
                    }
                    groupBoxBaseFunctions.BackColor = Color.Yellow;
                    checkBoxType.Checked = true;
                }

                if (lsImports.Count == 0 && lsFileImports.Count > 0)
                {
                    int cnt = 1;
                    foreach (string s in lsFileImports)
                    {
                        string[] sa = s.Split('\t');
                        dataGridViewBaseImports.Rows.Add(new string[] { cnt.ToString(), "0", "0", sa[0], sa[1], "", sa[2], "0" });
                        lsImports.Add(new string[] { cnt++.ToString(), "0", "0", sa[0], sa[1], "", sa[2], "0" });
                    }
                    groupBoxBaseImports.BackColor = Color.Yellow;
                    checkBoxType.Checked = true;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Сохранение изменений
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSaveForm_Click(object sender, EventArgs e)
        {
            try
            {
                List<string[]> lsf;
                List<string[]> lsi;
                switch (mode)
                {
                    case EditMode.NEW:
                        lsf = lsFuncs.Select(x => x.Skip(3).ToArray()).ToList();
                        lsi = lsImports.Select(x => x.Skip(3).ToArray()).ToList();
                        string[] sa = new string[] { textBoxBaseName.Text, textBoxBaseAuthor.Text, textBoxBaseDescription.Text, textBoxBaseMD5.Text, textBoxBaseVersion.Text, checkBoxType.Checked ? "0" : "1" };
                        ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).InsertDllToBase(sa, ImageFile, lsf, lsi);
                        break;
                    case EditMode.UPDATE:
                        lsf = lsFuncs.Select(x => x.Skip(3).ToArray()).ToList();
                        lsi = lsImports.Select(x => x.Skip(3).ToArray()).ToList();
                        string[] sb = new string[] { id, textBoxBaseName.Text, textBoxBaseAuthor.Text, textBoxBaseDescription.Text, textBoxBaseMD5.Text, textBoxBaseVersion.Text, checkBoxType.Checked ? "0" : "1" };
                        ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).UpdateDll(sb);
                        ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).DeleteDllFunctions(int.Parse(id));
                        ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).DeleteDllImports(int.Parse(id));
                        ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).InsertDllFunctions(int.Parse(id), lsf);
                        ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).InsertDllImports(int.Parse(id), lsi);
                        break;
                    case EditMode.DELETE:
                        ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).DeleteDllFromBase(int.Parse(id));
                        break;
                }
                base.ParentForm.DialogResult = DialogResult.OK;
                base.ParentForm.Close();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }

            
        }

        /// <summary>
        /// Изменение функции
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdateFunction_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewBaseFunctions.SelectedCells.Count > 0)
                {
                    int index = dataGridViewBaseFunctions.SelectedCells[0].RowIndex;
                    string[] srecord = lsFuncs[index];
                    Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                    FuncEditControl fec;
                    fm.Controls.Add(fec = new FuncEditControl(EditMode.UPDATE, ref srecord) { Dock = DockStyle.Fill });
                    if (fm.ShowDialog() == DialogResult.OK)
                    {
                        lsFuncs[index] = srecord;
                    }
                    fm.Controls.Remove(fec);
                    FillData(id);
                }
                else
                {
                    Monitor.Log.Warning(ObjectName, "Выделите запись в таблице \"Экспортируемые функции\"", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Удаление функции
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeleteFunction_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewBaseFunctions.SelectedCells.Count > 0)
                {
                    int index = dataGridViewBaseFunctions.SelectedCells[0].RowIndex;
                    string[] srecord = lsFuncs[index];
                    Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                    FuncEditControl fec;
                    fm.Controls.Add(fec = new FuncEditControl(EditMode.DELETE, ref srecord) { Dock = DockStyle.Fill });
                    if (fm.ShowDialog() == DialogResult.OK)
                    {
                        lsFuncs.RemoveAt(index);
                    }
                    fm.Controls.Remove(fec);
                    FillData(id);
                }
                else
                {
                    Monitor.Log.Warning(ObjectName, "Выделите запись в таблице \"Экспортируемые функции\"", true);
                    return;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }

        } 
        #endregion

        #region SqlDependentControl
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления в соответствии с содержимым базы данных
            this.Generate();

            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Делаем его недоступным
            this.Enabled = false;
        }
        #endregion
    }
}
