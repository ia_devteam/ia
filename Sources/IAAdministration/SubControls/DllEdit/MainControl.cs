﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

using IA.Extensions;
using IA.Sql;
using IA.Sql.DatabaseConnections;

namespace IA.Administration.SubControls.DllEdit
{
    /// <summary>
    /// Режимы редактирования
    /// </summary>
    internal enum EditMode
    {
        /// <summary>
        /// Заведение новой библиотеки
        /// </summary>
        NEW,
        /// <summary>
        /// Изменение библиотеки
        /// </summary>
        UPDATE,
        /// <summary>
        /// Удаление библиотеки
        /// </summary>
        DELETE
    }

    /// <summary>
    /// Класс - Главная форма приложения Редактирования библиотек функций
    /// </summary>
    internal partial class MainControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private string ObjectName = "Элемент управления - Редактирование библиотек функций";

        #region Поля
        /// <summary>
        /// Таблица библиотек
        /// </summary>
        private List<string[]> listDlls = new List<string[]>();
        /// <summary>
        /// Таблица функций
        /// </summary>
        private List<string[]> listFuncs = new List<string[]>();
        /// <summary>
        /// Таблица импортируемых функций
        /// </summary>
        private List<string[]> listImports = new List<string[]>(); 
        #endregion



        /// <summary>
        /// Конструктор
        /// </summary>
        internal MainControl()
        {
            InitializeComponent();
            Name = "DllEdit";
            Text = "Библиотеки";

            ToolTip toolTip1 = new ToolTip();

            toolTip1.SetToolTip(this.buttonFind, "Отображение библиотек с учетом заданных фильтров");
            toolTip1.SetToolTip(this.buttonDelete, "Удаление выдеденной библиотеки");
            toolTip1.SetToolTip(this.buttonAddMany, "Добавление группы библиотек\n" +
                                                    "в автоматическом режиме");
            toolTip1.SetToolTip(this.buttonEditForm, "Включение режима редактирования таблицы \"Библиотеки\"");
            toolTip1.SetToolTip(this.buttonSaveForm, "Сохранение измененных данных и\n"+ 
                                                     "выключение режима редактирования");
            toolTip1.SetToolTip(this.buttonNew, "Добавление новой библиотеки");
            toolTip1.SetToolTip(this.buttonUpdate, "Изменение выдеденной библиотеки");
            toolTip1.SetToolTip(this.textBoxFilterAuthor, "Фильтр: Поле Автор содержит подстроку");
            toolTip1.SetToolTip(this.textBoxFilterDescription, "Фильтр: Поле Описание содержит подстроку");
            toolTip1.SetToolTip(this.textBoxFilterMD5, "Фильтр: Поле Контрольная сумма содержит подстроку");
            toolTip1.SetToolTip(this.textBoxFilterName, "Фильтр: Поле Наименование содержит подстроку");
            toolTip1.SetToolTip(this.textBoxFilterVersion, "Фильтр: Поле Версия содержит подстроку");
        }

        #region Кнопки
        /// <summary>
        /// Кнопка "Найти"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFind_Click(object sender, EventArgs e)
        {
            try
            {
                FillData();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка "Редактирование" - в таблице разрешается изменять поля "Автор", "Описание", "Версия", "Иссл." 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEditForm_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridViewBase.Columns[2].ReadOnly = false;
                dataGridViewBase.Columns[3].ReadOnly = false;
                dataGridViewBase.Columns[5].ReadOnly = false;
                dataGridViewBase.Columns[6].ReadOnly = false;
                dataGridViewBase.EditMode = DataGridViewEditMode.EditOnKeystroke;
                dataGridViewBase.AllowUserToOrderColumns = true;
                labelEditingMode.Text = "Режим редактирования";
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка "Сохранить" - сохранение изменений после редактирования таблицы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSaveForm_Click(object sender, EventArgs e)
        {
            try
            {
                dataGridViewBase.Columns[2].ReadOnly = true;
                dataGridViewBase.Columns[3].ReadOnly = true;
                dataGridViewBase.Columns[5].ReadOnly = true;
                dataGridViewBase.Columns[6].ReadOnly = true;
                dataGridViewBase.AllowUserToOrderColumns = false;
                for (int i = 0; i < dataGridViewBase.RowCount; i++)
                {
                    string ID = GetGridValue(dataGridViewBase, i, 0);
                    string Name = GetGridValue(dataGridViewBase, i, 1);
                    string Author = GetGridValue(dataGridViewBase, i, 2);
                    string Descr = GetGridValue(dataGridViewBase, i, 3);
                    string CheckSum = GetGridValue(dataGridViewBase, i, 4);
                    string Version = GetGridValue(dataGridViewBase, i, 5);
                    string TypeOf = GetGridValue(dataGridViewBase, i, 6);

                    if (IfEdited(ID, Author, Descr, Version, TypeOf))
                    {
                        ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).UpdateDll(new string[] { ID, Name, Author, Descr, CheckSum, Version, TypeOf });
                    }
                }
                labelEditingMode.Text = "";
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка "Изменить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewBase.SelectedCells != null && dataGridViewBase.SelectedCells.Count > 0)
                {
                    string sInd = GetGridValue(dataGridViewBase, dataGridViewBase.SelectedCells[0].RowIndex, 0);
                    if (sInd != string.Empty)
                    {
                        Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                        fm.Size = this.Size;
                        MainEditControl mec;
                        fm.Controls.Add(mec = new MainEditControl(EditMode.UPDATE, sInd, ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection)) { Dock = DockStyle.Fill });
                        if (fm.ShowDialog() == DialogResult.OK)
                        {
                        }
                        fm.Controls.Remove(mec);

                        FillData();
                    }
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка "Удалить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewBase.SelectedCells != null && dataGridViewBase.SelectedCells.Count > 0)
                {
                    string sInd = GetGridValue(dataGridViewBase, dataGridViewBase.SelectedCells[0].RowIndex, 0);
                    if (sInd != string.Empty)
                    {
                        Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                        fm.Size = this.Size;
                        MainEditControl mec;
                        fm.Controls.Add(mec = new MainEditControl(EditMode.DELETE, sInd, ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection)) { Dock = DockStyle.Fill });
                        if (fm.ShowDialog() == DialogResult.OK)
                        {
                        }
                        fm.Controls.Remove(mec);
                        FillData();
                    }
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка "Новая" 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNew_Click(object sender, EventArgs e)
        {
            try
            {
                Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                fm.Size = this.Size;
                MainEditControl mec;
                fm.Controls.Add(mec = new MainEditControl(EditMode.NEW, "0", ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection)) { Dock = DockStyle.Fill });
                if (fm.ShowDialog() == DialogResult.OK)
                {
                }
                fm.Controls.Remove(mec);

                FillData();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка "Добавить группу" файлов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddMany_Click(object sender, EventArgs e)
        {
            try
            {
                Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                fm.Size = this.Size;
                GroupDllsControl gdc;
                fm.Controls.Add(gdc = new GroupDllsControl(((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection)) { Dock = DockStyle.Fill });
                if (fm.ShowDialog() == DialogResult.OK)
                {
                }
                fm.Controls.Remove(gdc);
                FillData();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        } 
        #endregion

        #region Прочие элементы управления

        /// <summary>
        /// Завершение редактирования ячейки таблицы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewBase_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            List<int> allowToEditColumns = new List<int>() { 2, 3, 4, 5, 6 };
            try
            {
                if (allowToEditColumns.Contains(e.ColumnIndex))
                {
                    string sInd = dataGridViewBase[0, e.RowIndex].Value.ToString();
                    int ind = -1;
                    for (int i = 0; i < listDlls.Count; i++)
                    {
                        if (listDlls[i][0] == sInd)
                        {
                            ind = i;
                            break;
                        }
                    }
                    if (ind != -1)
                    {
                        if (dataGridViewBase[e.ColumnIndex, e.RowIndex].Value != null && dataGridViewBase[e.ColumnIndex, e.RowIndex].Value.ToString() != listDlls[ind][e.ColumnIndex])
                        {
                            dataGridViewBase[e.ColumnIndex, e.RowIndex].Style.BackColor = Color.Yellow;
                        }
                        else
                        {
                            dataGridViewBase[e.ColumnIndex, e.RowIndex].Style.BackColor = SystemColors.Window;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Редакатирование ячейки табицы", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Событие изменения выделения ячеек таблицы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewBase_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridViewFunctions.Rows.Clear();
                dataGridViewImports.Rows.Clear();
                groupBoxFunctions.Text = "Экспортируемые функции";
                if (dataGridViewBase.SelectedCells != null && dataGridViewBase.SelectedCells.Count > 0)
                {
                    int ID = int.Parse(GetGridValue(dataGridViewBase, dataGridViewBase.SelectedCells[0].RowIndex, 0));

                    listFuncs = ((DllKnowledgeBaseDatabaseConnection)SqlConnector.DllKnowledgeDB.DatabaseConnection).DllFunctions(ID);
                    int cnt = 1;
                    foreach (string[] sa in listFuncs)
                    {
                        if (sa[5] != "1")
                            sa[5] = "0";

                        string[] sb = new string[] { cnt++.ToString() }.Concat(sa).ToArray();
                        dataGridViewFunctions.Rows.Add(sb);
                    }
                    if (listFuncs.Count > 0)
                        groupBoxFunctions.Text = "Экспортируемые функции  " + listFuncs.Count;

                    groupBoxImports.Text = "Импортируемые функции";
                    cnt = 1;
                    listImports = ((DllKnowledgeBaseDatabaseConnection)SqlConnector.DllKnowledgeDB.DatabaseConnection).DllImports(ID);
                    foreach (string[] sa in listImports)
                    {
                        if (sa[6] != "1")
                            sa[6] = "0";
                        dataGridViewImports.Rows.Add(new string[] { cnt++.ToString() }.Concat(sa).ToArray());

                    }
                    if (listFuncs.Count > 0)
                        groupBoxImports.Text = "Импортируемые функции  " + listImports.Count;

                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Выбор ячейки таблицы", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        #endregion

        #region Внутренние полезные методы
        /// <summary>
        /// Очистка элемента управления
        /// </summary>
        private void Clear()
        {
            //Label
            label4.Text = String.Empty;
            label4.Refresh();
            labelEditingMode.Text = String.Empty;
            labelEditingMode.Refresh();

            //TextBox
            this.textBoxFilterAuthor.Text = String.Empty;
            this.textBoxFilterDescription.Text = String.Empty;
            this.textBoxFilterName.Text = String.Empty;
            this.textBoxFilterVersion.Text = String.Empty;

            //DataGridView
            this.dataGridViewBase.Rows.Clear();
            this.dataGridViewFunctions.Rows.Clear();
            this.dataGridViewImports.Rows.Clear();
        }

        /// <summary>
        /// Заполнение таблиц в соответствии с фильтрами
        /// </summary>
        private void FillData()
        {
            bool Nfilter = !string.IsNullOrWhiteSpace(textBoxFilterName.Text);
            bool Afilter = !string.IsNullOrWhiteSpace(textBoxFilterAuthor.Text);
            bool Dfilter = !string.IsNullOrWhiteSpace(textBoxFilterDescription.Text);
            bool Sfilter = !string.IsNullOrWhiteSpace(textBoxFilterMD5.Text);
            bool Vfilter = !string.IsNullOrWhiteSpace(textBoxFilterVersion.Text);
           

            dataGridViewBase.Rows.Clear();
            dataGridViewFunctions.Rows.Clear();
            dataGridViewImports.Rows.Clear();

            Cursor = Cursors.WaitCursor;
            listDlls = ((DllKnowledgeBaseDatabaseConnection)SqlConnector.DllKnowledgeDB.DatabaseConnection).DllBase.Where(x => (Nfilter ? x[1].ToLower().Contains(textBoxFilterName.Text.ToLower()) : true) && (Afilter ? x[2].ToLower().Contains(textBoxFilterAuthor.Text.ToLower()) : true) && (Dfilter ? x[3].ToLower().Contains(textBoxFilterDescription.Text.ToLower()) : true) && (Sfilter ? x[4].ToLower().Contains(textBoxFilterMD5.Text.ToLower()) : true) && (Vfilter ? x[5].ToLower().Contains(textBoxFilterVersion.Text.ToLower()) : true)).ToList();
            foreach (string[] sa in listDlls)
            {
                if (sa[6] != "1")
                    sa[6] = "0";
                dataGridViewBase.Rows.Add(sa);
            }
            if (dataGridViewBase.Rows.Count > 0)
                label4.Text = "Итого " + dataGridViewBase.Rows.Count + " записей";
            else
                label4.Text = "Нет записей";
            Cursor = Cursors.Default;
        }

        /// <summary>
        /// Получить значение ячейки таблицы
        /// </summary>
        /// <param name="dg"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        private static string GetGridValue(DataGridView dg, int row, int col)
        {
            if (row < 0 || dg.RowCount <= row)
                return string.Empty;
            if (col < 0 || dg.ColumnCount <= col)
                return string.Empty;
            return dg[col, row].Value != null ? dg[col, row].Value.ToString() : string.Empty;
        }

        /// <summary>
        /// Считать всю таблицу
        /// </summary>
        /// <param name="dg"></param>
        /// <returns></returns>
        private static List<string[]> GetTable(DataGridView dg)
        {
            List<string[]> ret = new List<string[]>();
            for (int i = 0; i < dg.Rows.Count; i++)
            {
                string[] sa = new string[dg.ColumnCount];
                for (int j = 0; j < dg.ColumnCount; j++)
                    sa[j] = GetGridValue(dg, i, j);
                ret.Add(sa);
            }
            return ret;
        }

        /// <summary>
        /// Считать строку из таблицы
        /// </summary>
        /// <param name="dg"></param>
        /// <param name="rowindex"></param>
        /// <returns></returns>
        private static string[] GetRow(DataGridView dg, int rowindex)
        {
            string[] ret = new string[dg.ColumnCount];
            for (int j = 0; j < dg.ColumnCount; j++)
                ret[j] = GetGridValue(dg, rowindex, j);
            return ret;
        }

        /// <summary>
        /// Установить новые значения в строке таблицы
        /// </summary>
        /// <param name="dg"></param>
        /// <param name="rowindex"></param>
        /// <param name="array"></param>
        private static void SetRow(DataGridView dg, int rowindex, string[] array)
        {
            for (int j = 0; j < dg.ColumnCount; j++)
                dg[j, rowindex].Value = array[j];
        }

        /// <summary>
        /// Проверка того, что поле таблицы было изменено
        /// </summary>
        /// <param name="id"></param>
        /// <param name="author"></param>
        /// <param name="descr"></param>
        /// <param name="version"></param>
        /// <param name="issl"></param>
        /// <returns></returns>
        private bool IfEdited(string id, string author, string descr, string version, string issl)
        {
            return listDlls.Any(x => x[0] == id && (x[2] != author || x[3] != descr || x[5] != version || x[6] != issl));
        }

        /// <summary>
        /// контрольная сумма файла
        /// </summary>
        /// <param name="file">имя файла</param>
        /// <returns>контрольная сумма</returns>
        internal static string ComputeCtrlSum(string file)
        {
            System.IO.FileStream fs = new System.IO.FileStream(file, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            byte[] contents = new byte[fs.Length];
            fs.Read(contents, 0, (int)fs.Length);
            return ComputeCtrlSum(contents);
        }

        /// <summary>
        /// Контрольная сумма байтового массива
        /// </summary>
        /// <param name="content">содержимое файла в виде байтового массива</param>
        /// <returns>контрольная сумма</returns>
        internal static string ComputeCtrlSum(byte[] content)
        {
            System.Security.Cryptography.MD5 MD5 = System.Security.Cryptography.MD5.Create();
            return BitConverter.ToString(MD5.ComputeHash(content)).Replace("-", "");
        }

        /// <summary>
        /// Контрольная сумма файла + содержимое файла
        /// </summary>
        /// <param name="file">имя файла </param>
        /// <param name="contents">выходной параметр - содержимое файла в виде байтового массива</param>
        /// <returns>контрольная сумма</returns>
        internal static string ComputeCtrlSum(string file, out byte[] contents)
        {
            System.IO.FileStream fs = new System.IO.FileStream(file, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            contents = new byte[fs.Length];
            fs.Read(contents, 0, (int)fs.Length);
            return ComputeCtrlSum(contents);
        }

        /// <summary>
        /// Автор и описание библиотеки
        /// </summary>
        /// <param name="filename">имя библиотеки</param>
        /// <returns>список: автор, описание, версия</returns>
        internal static List<string> AuthorDescription(string filename)
        {
            List<string> lsRet = new List<string>();
            Dictionary<string, string> fileExAttribs = new Dictionary<string, string>();
            Shell32.Shell objShell;
            Shell32.Folder objFolder;
            objShell = new Shell32.Shell();
            string dir_name = (new FileInfo(filename)).Directory.ToString();
            objFolder = objShell.NameSpace(dir_name);
            Shell32.FolderItem objFolderItem;
            objFolderItem = objFolder.ParseName(filename.Replace(dir_name, "").Trim(new char[] { '\\' }));
            for (int i = 0; i <= short.MaxValue; i++)
            {
                if (String.IsNullOrEmpty(objFolder.GetDetailsOf(null, i)))
                    break;
                if (!fileExAttribs.Keys.Contains(objFolder.GetDetailsOf(dir_name, i)))
                    fileExAttribs.Add(objFolder.GetDetailsOf(dir_name, i), objFolder.GetDetailsOf(objFolderItem, i));
                //srichTextBox1 += "№" + i + " " + objFolder.GetDetailsOf(dir_name, i) + "\t" + objFolder.GetDetailsOf(objFolderItem, i) + "\n";
            }
            if (fileExAttribs.Keys.Contains("Авторские права"))
            {
                lsRet.Add(fileExAttribs["Авторские права"]);
            }
            else
            {
                if (fileExAttribs.Keys.Contains("Copyright"))
                    lsRet.Add(fileExAttribs["Copyright"]);
                else
                    lsRet.Add("");
            }
            if (fileExAttribs.Keys.Contains("Описание файла"))
            {
                lsRet.Add(fileExAttribs["Описание файла"]);
            }
            else
            {
                if (fileExAttribs.Keys.Contains("Description"))
                    lsRet.Add(fileExAttribs["Description"]);
                else
                    lsRet.Add("");
            }
            if (fileExAttribs.Keys.Contains("Версия"))
            {
                lsRet.Add(fileExAttribs["Версия"]);
            }
            else
            {
                if (fileExAttribs.Keys.Contains("Version"))
                    lsRet.Add(fileExAttribs["Version"]);
                else
                    lsRet.Add("");
            }

            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(filename);
            if (lsRet[0] == "")
                lsRet[0] = fvi.CompanyName;
            else
            {
                if (lsRet[0] != fvi.CompanyName && fvi.CompanyName != "" && !lsRet[0].Contains(fvi.CompanyName))
                    lsRet[0] += " (" + fvi.CompanyName + ")";
            }
            if (lsRet[1] == "")
                lsRet[1] = fvi.FileDescription;
            else
            {
                if (lsRet[1] != fvi.FileDescription && fvi.FileDescription != "" && !lsRet[1].Contains(fvi.FileDescription))
                    lsRet[1] += " (" + fvi.FileDescription + ")";
            }
            if (lsRet[2] == "")
                lsRet[2] = fvi.FileVersion;
            else
            {
                if (lsRet[2] != fvi.FileVersion && fvi.FileVersion != "" && !lsRet[1].Contains(fvi.FileVersion))
                    lsRet[2] += " (" + fvi.FileVersion + ")";
            }
            return lsRet;
        }
        #endregion

        
        
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Элемент управления доступен
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Элемент управления доступен
            this.Enabled = false;
        }
    }

}
