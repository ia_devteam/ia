﻿using System;
using System.Windows.Forms;

using IA.Extensions;

namespace IA.Administration.SubControls.DllEdit
{
    /// <summary>
    /// Класс - Диалог модификации функции
    /// </summary>
    internal partial class FuncEditControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private string ObjectName = "Элемент управления - Модификация функций";

        #region Поля
        /// <summary>
        /// Режим редактирования
        /// </summary>
        EditMode mode;
        /// <summary>
        /// Идентификатор функии
        /// </summary>
        string functionID;
        /// <summary>
        /// Идентификатор библиотеки
        /// </summary>
        string dllID;
        /// <summary>
        /// Запись в таблице функций                                       
        /// </summary>
        string[] record; 
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="record"></param>
        internal FuncEditControl(EditMode mode, ref string[] record)
        {
            InitializeComponent();

            ToolTip toolTip1 = new ToolTip();

            toolTip1.SetToolTip(this.textBoxName, "Наименование экспортируемой функции");
            toolTip1.SetToolTip(this.textBoxDescription, "Описание экспортируемой функции");
            toolTip1.SetToolTip(this.textBoxOrdinal, "Номер экспортируемой функции");


            //Инициализация полей
            this.mode = mode;
            this.record = record;
            this.functionID = record[1];
            this.dllID = record[2];
            
            this.buttonSave.DialogResult = DialogResult.OK;
            textBoxName.Text = record[3];
            textBoxDescription.Text = record[4];
            textBoxOrdinal.Text = record[5];
            checkBoxToResearch.Checked = record[6] == "1" ? false : true;

            switch (mode)
            {
                case EditMode.NEW:
                    Text = "Заведение новой экспортируемой функции";
                    break;
                case EditMode.UPDATE:
                    Text = "Изменение экспортируемой функции";
                    break;
                case EditMode.DELETE:
                    Text = "Удаление экспортируемой функции";
                    buttonSave.Text = "Удалить";
                    textBoxName.Enabled = false;
                    textBoxDescription.Enabled = false;
                    textBoxOrdinal.Enabled = false;
                    checkBoxToResearch.Enabled = false;
                    break;
            }


        }

        #region Кнопки
        /// <summary>
        /// Сохранение изменений
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                switch (mode)
                {
                    case EditMode.NEW:
                        record[1] = "";
                        record[2] = dllID;
                        record[3] = textBoxName.Text;
                        record[4] = textBoxDescription.Text;
                        record[5] = textBoxOrdinal.Text;
                        record[6] = checkBoxToResearch.Checked ? "0" : "1";
                        break;
                    case EditMode.UPDATE:
                        record[3] = textBoxName.Text;
                        record[4] = textBoxDescription.Text;
                        record[5] = textBoxOrdinal.Text;
                        record[6] = checkBoxToResearch.Checked ? "0" : "1";
                        break;
                    case EditMode.DELETE:
                        break;
                }
                base.ParentForm.DialogResult = DialogResult.OK;
                base.ParentForm.Close();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        } 
        #endregion
    }
}
