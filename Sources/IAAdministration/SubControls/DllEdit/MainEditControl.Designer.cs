﻿namespace IA.Administration.SubControls.DllEdit
{
    partial class MainEditControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBoxFromBase = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxType = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxBaseNumber = new System.Windows.Forms.TextBox();
            this.textBoxBaseName = new System.Windows.Forms.TextBox();
            this.textBoxBaseAuthor = new System.Windows.Forms.TextBox();
            this.textBoxBaseDescription = new System.Windows.Forms.TextBox();
            this.textBoxBaseMD5 = new System.Windows.Forms.TextBox();
            this.textBoxBaseVersion = new System.Windows.Forms.TextBox();
            this.groupBoxBaseFunctions = new System.Windows.Forms.GroupBox();
            this.dataGridViewBaseFunctions = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBoxBaseImports = new System.Windows.Forms.GroupBox();
            this.dataGridViewBaseImports = new System.Windows.Forms.DataGridView();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAddFunction = new System.Windows.Forms.Button();
            this.buttonUpdateFunction = new System.Windows.Forms.Button();
            this.buttonDeleteFunction = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAddImport = new System.Windows.Forms.Button();
            this.buttonUpdateImport = new System.Windows.Forms.Button();
            this.buttonDeleteImport = new System.Windows.Forms.Button();
            this.buttonSaveForm = new System.Windows.Forms.Button();
            this.groupBoxFromFile = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxFileFullName = new System.Windows.Forms.TextBox();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.textBoxFileAuthor = new System.Windows.Forms.TextBox();
            this.textBoxFileDescription = new System.Windows.Forms.TextBox();
            this.textBoxFileMD5 = new System.Windows.Forms.TextBox();
            this.textBoxFileVersion = new System.Windows.Forms.TextBox();
            this.groupBoxFileFunctions = new System.Windows.Forms.GroupBox();
            this.dataGridViewFileFunctions = new System.Windows.Forms.DataGridView();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBoxFileImports = new System.Windows.Forms.GroupBox();
            this.dataGridViewFileImports = new System.Windows.Forms.DataGridView();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.buttonCopy = new System.Windows.Forms.Button();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBoxFromBase.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBoxBaseFunctions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBaseFunctions)).BeginInit();
            this.groupBoxBaseImports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBaseImports)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBoxFromFile.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBoxFileFunctions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFileFunctions)).BeginInit();
            this.groupBoxFileImports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFileImports)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBoxFromBase);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBoxFromFile);
            this.splitContainer1.Size = new System.Drawing.Size(1196, 609);
            this.splitContainer1.SplitterDistance = 594;
            this.splitContainer1.TabIndex = 1;
            // 
            // groupBoxFromBase
            // 
            this.groupBoxFromBase.Controls.Add(this.tableLayoutPanel1);
            this.groupBoxFromBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFromBase.Location = new System.Drawing.Point(0, 0);
            this.groupBoxFromBase.Name = "groupBoxFromBase";
            this.groupBoxFromBase.Size = new System.Drawing.Size(594, 609);
            this.groupBoxFromBase.TabIndex = 0;
            this.groupBoxFromBase.TabStop = false;
            this.groupBoxFromBase.Text = "База данных";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxType, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBoxBaseNumber, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBoxBaseName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxBaseAuthor, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxBaseDescription, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBoxBaseMD5, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBoxBaseVersion, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxBaseFunctions, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxBaseImports, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 8);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(588, 590);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "№";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 30);
            this.label2.TabIndex = 1;
            this.label2.Text = "Наименование";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 30);
            this.label3.TabIndex = 2;
            this.label3.Text = "Автор";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 30);
            this.label4.TabIndex = 3;
            this.label4.Text = "Описание";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 30);
            this.label5.TabIndex = 4;
            this.label5.Text = "Контрольная сумма";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBoxType
            // 
            this.checkBoxType.AutoSize = true;
            this.checkBoxType.Location = new System.Drawing.Point(123, 183);
            this.checkBoxType.Name = "checkBoxType";
            this.checkBoxType.Size = new System.Drawing.Size(142, 17);
            this.checkBoxType.TabIndex = 5;
            this.checkBoxType.Text = "Требует исследования";
            this.checkBoxType.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 30);
            this.label6.TabIndex = 6;
            this.label6.Text = "Версия";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxBaseNumber
            // 
            this.textBoxBaseNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBaseNumber.Location = new System.Drawing.Point(123, 3);
            this.textBoxBaseNumber.Name = "textBoxBaseNumber";
            this.textBoxBaseNumber.ReadOnly = true;
            this.textBoxBaseNumber.Size = new System.Drawing.Size(462, 20);
            this.textBoxBaseNumber.TabIndex = 7;
            // 
            // textBoxBaseName
            // 
            this.textBoxBaseName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBaseName.Location = new System.Drawing.Point(123, 33);
            this.textBoxBaseName.Name = "textBoxBaseName";
            this.textBoxBaseName.Size = new System.Drawing.Size(462, 20);
            this.textBoxBaseName.TabIndex = 8;
            // 
            // textBoxBaseAuthor
            // 
            this.textBoxBaseAuthor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBaseAuthor.Location = new System.Drawing.Point(123, 63);
            this.textBoxBaseAuthor.Name = "textBoxBaseAuthor";
            this.textBoxBaseAuthor.Size = new System.Drawing.Size(462, 20);
            this.textBoxBaseAuthor.TabIndex = 9;
            // 
            // textBoxBaseDescription
            // 
            this.textBoxBaseDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBaseDescription.Location = new System.Drawing.Point(123, 93);
            this.textBoxBaseDescription.Name = "textBoxBaseDescription";
            this.textBoxBaseDescription.Size = new System.Drawing.Size(462, 20);
            this.textBoxBaseDescription.TabIndex = 10;
            // 
            // textBoxBaseMD5
            // 
            this.textBoxBaseMD5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBaseMD5.Location = new System.Drawing.Point(123, 123);
            this.textBoxBaseMD5.Name = "textBoxBaseMD5";
            this.textBoxBaseMD5.Size = new System.Drawing.Size(462, 20);
            this.textBoxBaseMD5.TabIndex = 11;
            // 
            // textBoxBaseVersion
            // 
            this.textBoxBaseVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBaseVersion.Location = new System.Drawing.Point(123, 153);
            this.textBoxBaseVersion.Name = "textBoxBaseVersion";
            this.textBoxBaseVersion.Size = new System.Drawing.Size(462, 20);
            this.textBoxBaseVersion.TabIndex = 12;
            // 
            // groupBoxBaseFunctions
            // 
            this.groupBoxBaseFunctions.Controls.Add(this.dataGridViewBaseFunctions);
            this.groupBoxBaseFunctions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxBaseFunctions.Location = new System.Drawing.Point(123, 213);
            this.groupBoxBaseFunctions.Name = "groupBoxBaseFunctions";
            this.groupBoxBaseFunctions.Size = new System.Drawing.Size(462, 184);
            this.groupBoxBaseFunctions.TabIndex = 15;
            this.groupBoxBaseFunctions.TabStop = false;
            this.groupBoxBaseFunctions.Text = "Экспортируемые Функции";
            // 
            // dataGridViewBaseFunctions
            // 
            this.dataGridViewBaseFunctions.AllowUserToAddRows = false;
            this.dataGridViewBaseFunctions.AllowUserToDeleteRows = false;
            this.dataGridViewBaseFunctions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBaseFunctions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column4,
            this.Column2,
            this.Column3,
            this.Column5,
            this.Column6,
            this.Column7});
            this.dataGridViewBaseFunctions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewBaseFunctions.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewBaseFunctions.Name = "dataGridViewBaseFunctions";
            this.dataGridViewBaseFunctions.ReadOnly = true;
            this.dataGridViewBaseFunctions.RowHeadersVisible = false;
            this.dataGridViewBaseFunctions.Size = new System.Drawing.Size(456, 165);
            this.dataGridViewBaseFunctions.TabIndex = 14;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "№";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 40;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "ID";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "DLL_ID";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Visible = false;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Наименование";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Описание";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Номер (ординал)";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 120;
            // 
            // Column7
            // 
            this.Column7.FalseValue = "1";
            this.Column7.HeaderText = "Иссл";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column7.TrueValue = "0";
            this.Column7.Width = 40;
            // 
            // groupBoxBaseImports
            // 
            this.groupBoxBaseImports.Controls.Add(this.dataGridViewBaseImports);
            this.groupBoxBaseImports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxBaseImports.Location = new System.Drawing.Point(123, 403);
            this.groupBoxBaseImports.Name = "groupBoxBaseImports";
            this.groupBoxBaseImports.Size = new System.Drawing.Size(462, 184);
            this.groupBoxBaseImports.TabIndex = 16;
            this.groupBoxBaseImports.TabStop = false;
            this.groupBoxBaseImports.Text = "Импортируемые функции";
            // 
            // dataGridViewBaseImports
            // 
            this.dataGridViewBaseImports.AllowUserToAddRows = false;
            this.dataGridViewBaseImports.AllowUserToDeleteRows = false;
            this.dataGridViewBaseImports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBaseImports.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23});
            this.dataGridViewBaseImports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewBaseImports.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewBaseImports.Name = "dataGridViewBaseImports";
            this.dataGridViewBaseImports.ReadOnly = true;
            this.dataGridViewBaseImports.RowHeadersVisible = false;
            this.dataGridViewBaseImports.Size = new System.Drawing.Size(456, 165);
            this.dataGridViewBaseImports.TabIndex = 0;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "№";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Width = 40;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "ID";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Visible = false;
            // 
            // Column17
            // 
            this.Column17.HeaderText = "DLL_ID";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.Visible = false;
            // 
            // Column18
            // 
            this.Column18.HeaderText = "Библиотека";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            // 
            // Column19
            // 
            this.Column19.HeaderText = "Функция";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            // 
            // Column20
            // 
            this.Column20.HeaderText = "Описание";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            // 
            // Column21
            // 
            this.Column21.HeaderText = "Номер (ординал)";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Width = 120;
            // 
            // Column22
            // 
            this.Column22.FalseValue = "1";
            this.Column22.HeaderText = "Иссл";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column22.TrueValue = "0";
            this.Column22.Width = 40;
            // 
            // Column23
            // 
            this.Column23.HeaderText = "FuncID";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.buttonAddFunction, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonUpdateFunction, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.buttonDeleteFunction, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 213);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(114, 184);
            this.tableLayoutPanel2.TabIndex = 17;
            // 
            // buttonAddFunction
            // 
            this.buttonAddFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAddFunction.Location = new System.Drawing.Point(3, 3);
            this.buttonAddFunction.Name = "buttonAddFunction";
            this.buttonAddFunction.Size = new System.Drawing.Size(108, 24);
            this.buttonAddFunction.TabIndex = 0;
            this.buttonAddFunction.Text = "Добавить";
            this.buttonAddFunction.UseVisualStyleBackColor = true;
            this.buttonAddFunction.Click += new System.EventHandler(this.buttonAddFunction_Click);
            // 
            // buttonUpdateFunction
            // 
            this.buttonUpdateFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonUpdateFunction.Location = new System.Drawing.Point(3, 33);
            this.buttonUpdateFunction.Name = "buttonUpdateFunction";
            this.buttonUpdateFunction.Size = new System.Drawing.Size(108, 24);
            this.buttonUpdateFunction.TabIndex = 1;
            this.buttonUpdateFunction.Text = "Изменить";
            this.buttonUpdateFunction.UseVisualStyleBackColor = true;
            this.buttonUpdateFunction.Click += new System.EventHandler(this.buttonUpdateFunction_Click);
            // 
            // buttonDeleteFunction
            // 
            this.buttonDeleteFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDeleteFunction.Location = new System.Drawing.Point(3, 63);
            this.buttonDeleteFunction.Name = "buttonDeleteFunction";
            this.buttonDeleteFunction.Size = new System.Drawing.Size(108, 24);
            this.buttonDeleteFunction.TabIndex = 2;
            this.buttonDeleteFunction.Text = "Удалить";
            this.buttonDeleteFunction.UseVisualStyleBackColor = true;
            this.buttonDeleteFunction.Click += new System.EventHandler(this.buttonDeleteFunction_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.buttonAddImport, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.buttonUpdateImport, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.buttonDeleteImport, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.buttonSaveForm, 0, 4);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 403);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(114, 184);
            this.tableLayoutPanel3.TabIndex = 18;
            // 
            // buttonAddImport
            // 
            this.buttonAddImport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAddImport.Location = new System.Drawing.Point(3, 3);
            this.buttonAddImport.Name = "buttonAddImport";
            this.buttonAddImport.Size = new System.Drawing.Size(108, 24);
            this.buttonAddImport.TabIndex = 0;
            this.buttonAddImport.Text = "Добавить";
            this.buttonAddImport.UseVisualStyleBackColor = true;
            this.buttonAddImport.Click += new System.EventHandler(this.buttonAddImport_Click);
            // 
            // buttonUpdateImport
            // 
            this.buttonUpdateImport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonUpdateImport.Location = new System.Drawing.Point(3, 33);
            this.buttonUpdateImport.Name = "buttonUpdateImport";
            this.buttonUpdateImport.Size = new System.Drawing.Size(108, 24);
            this.buttonUpdateImport.TabIndex = 1;
            this.buttonUpdateImport.Text = "Изменить";
            this.buttonUpdateImport.UseVisualStyleBackColor = true;
            this.buttonUpdateImport.Click += new System.EventHandler(this.buttonUpdateImport_Click);
            // 
            // buttonDeleteImport
            // 
            this.buttonDeleteImport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDeleteImport.Location = new System.Drawing.Point(3, 63);
            this.buttonDeleteImport.Name = "buttonDeleteImport";
            this.buttonDeleteImport.Size = new System.Drawing.Size(108, 24);
            this.buttonDeleteImport.TabIndex = 2;
            this.buttonDeleteImport.Text = "Удалить";
            this.buttonDeleteImport.UseVisualStyleBackColor = true;
            this.buttonDeleteImport.Click += new System.EventHandler(this.buttonDeleteImport_Click);
            // 
            // buttonSaveForm
            // 
            this.buttonSaveForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSaveForm.Location = new System.Drawing.Point(3, 157);
            this.buttonSaveForm.Name = "buttonSaveForm";
            this.buttonSaveForm.Size = new System.Drawing.Size(108, 24);
            this.buttonSaveForm.TabIndex = 3;
            this.buttonSaveForm.Text = "Сохранить";
            this.buttonSaveForm.UseVisualStyleBackColor = true;
            this.buttonSaveForm.Click += new System.EventHandler(this.buttonSaveForm_Click);
            // 
            // groupBoxFromFile
            // 
            this.groupBoxFromFile.Controls.Add(this.tableLayoutPanel4);
            this.groupBoxFromFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFromFile.Location = new System.Drawing.Point(0, 0);
            this.groupBoxFromFile.Name = "groupBoxFromFile";
            this.groupBoxFromFile.Size = new System.Drawing.Size(598, 609);
            this.groupBoxFromFile.TabIndex = 0;
            this.groupBoxFromFile.TabStop = false;
            this.groupBoxFromFile.Text = "Файл";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.buttonBrowse, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.label10, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.label11, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.textBoxFileFullName, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.textBoxFileName, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.textBoxFileAuthor, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.textBoxFileDescription, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.textBoxFileMD5, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.textBoxFileVersion, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.groupBoxFileFunctions, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.groupBoxFileImports, 1, 8);
            this.tableLayoutPanel4.Controls.Add(this.buttonCopy, 0, 6);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 9;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(592, 590);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonBrowse.Location = new System.Drawing.Point(3, 3);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(114, 24);
            this.buttonBrowse.TabIndex = 0;
            this.buttonBrowse.Text = "Обзор";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 30);
            this.label7.TabIndex = 1;
            this.label7.Text = "Наименование";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 30);
            this.label8.TabIndex = 2;
            this.label8.Text = "Автор";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 30);
            this.label9.TabIndex = 3;
            this.label9.Text = "Описание";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(3, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 30);
            this.label10.TabIndex = 4;
            this.label10.Text = "Контрольная сумма";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(3, 150);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 30);
            this.label11.TabIndex = 5;
            this.label11.Text = "Версия";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxFileFullName
            // 
            this.textBoxFileFullName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFileFullName.Location = new System.Drawing.Point(123, 3);
            this.textBoxFileFullName.Name = "textBoxFileFullName";
            this.textBoxFileFullName.ReadOnly = true;
            this.textBoxFileFullName.Size = new System.Drawing.Size(466, 20);
            this.textBoxFileFullName.TabIndex = 6;
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFileName.Location = new System.Drawing.Point(123, 33);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.ReadOnly = true;
            this.textBoxFileName.Size = new System.Drawing.Size(466, 20);
            this.textBoxFileName.TabIndex = 7;
            // 
            // textBoxFileAuthor
            // 
            this.textBoxFileAuthor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFileAuthor.Location = new System.Drawing.Point(123, 63);
            this.textBoxFileAuthor.Name = "textBoxFileAuthor";
            this.textBoxFileAuthor.ReadOnly = true;
            this.textBoxFileAuthor.Size = new System.Drawing.Size(466, 20);
            this.textBoxFileAuthor.TabIndex = 8;
            // 
            // textBoxFileDescription
            // 
            this.textBoxFileDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFileDescription.Location = new System.Drawing.Point(123, 93);
            this.textBoxFileDescription.Name = "textBoxFileDescription";
            this.textBoxFileDescription.ReadOnly = true;
            this.textBoxFileDescription.Size = new System.Drawing.Size(466, 20);
            this.textBoxFileDescription.TabIndex = 9;
            // 
            // textBoxFileMD5
            // 
            this.textBoxFileMD5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFileMD5.Location = new System.Drawing.Point(123, 123);
            this.textBoxFileMD5.Name = "textBoxFileMD5";
            this.textBoxFileMD5.ReadOnly = true;
            this.textBoxFileMD5.Size = new System.Drawing.Size(466, 20);
            this.textBoxFileMD5.TabIndex = 10;
            // 
            // textBoxFileVersion
            // 
            this.textBoxFileVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxFileVersion.Location = new System.Drawing.Point(123, 153);
            this.textBoxFileVersion.Name = "textBoxFileVersion";
            this.textBoxFileVersion.ReadOnly = true;
            this.textBoxFileVersion.Size = new System.Drawing.Size(466, 20);
            this.textBoxFileVersion.TabIndex = 11;
            // 
            // groupBoxFileFunctions
            // 
            this.groupBoxFileFunctions.Controls.Add(this.dataGridViewFileFunctions);
            this.groupBoxFileFunctions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFileFunctions.Location = new System.Drawing.Point(123, 213);
            this.groupBoxFileFunctions.Name = "groupBoxFileFunctions";
            this.groupBoxFileFunctions.Size = new System.Drawing.Size(466, 184);
            this.groupBoxFileFunctions.TabIndex = 12;
            this.groupBoxFileFunctions.TabStop = false;
            this.groupBoxFileFunctions.Text = "Экспортируемые функции";
            // 
            // dataGridViewFileFunctions
            // 
            this.dataGridViewFileFunctions.AllowUserToAddRows = false;
            this.dataGridViewFileFunctions.AllowUserToDeleteRows = false;
            this.dataGridViewFileFunctions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFileFunctions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14});
            this.dataGridViewFileFunctions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFileFunctions.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewFileFunctions.Name = "dataGridViewFileFunctions";
            this.dataGridViewFileFunctions.ReadOnly = true;
            this.dataGridViewFileFunctions.RowHeadersVisible = false;
            this.dataGridViewFileFunctions.Size = new System.Drawing.Size(460, 165);
            this.dataGridViewFileFunctions.TabIndex = 0;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "№";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 40;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "ID";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Visible = false;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "DLL_ID";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Visible = false;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Наименование";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Описание";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Номер (ординал)";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 120;
            // 
            // Column14
            // 
            this.Column14.FalseValue = "1";
            this.Column14.HeaderText = "Иссл";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column14.TrueValue = "0";
            this.Column14.Width = 40;
            // 
            // groupBoxFileImports
            // 
            this.groupBoxFileImports.Controls.Add(this.dataGridViewFileImports);
            this.groupBoxFileImports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFileImports.Location = new System.Drawing.Point(123, 403);
            this.groupBoxFileImports.Name = "groupBoxFileImports";
            this.groupBoxFileImports.Size = new System.Drawing.Size(466, 184);
            this.groupBoxFileImports.TabIndex = 13;
            this.groupBoxFileImports.TabStop = false;
            this.groupBoxFileImports.Text = "Импортируемые функции";
            // 
            // dataGridViewFileImports
            // 
            this.dataGridViewFileImports.AllowUserToAddRows = false;
            this.dataGridViewFileImports.AllowUserToDeleteRows = false;
            this.dataGridViewFileImports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFileImports.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column28,
            this.Column29,
            this.Column30,
            this.Column31});
            this.dataGridViewFileImports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFileImports.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewFileImports.Name = "dataGridViewFileImports";
            this.dataGridViewFileImports.ReadOnly = true;
            this.dataGridViewFileImports.RowHeadersVisible = false;
            this.dataGridViewFileImports.Size = new System.Drawing.Size(460, 165);
            this.dataGridViewFileImports.TabIndex = 0;
            // 
            // Column24
            // 
            this.Column24.HeaderText = "№";
            this.Column24.Name = "Column24";
            this.Column24.ReadOnly = true;
            this.Column24.Width = 40;
            // 
            // Column25
            // 
            this.Column25.HeaderText = "ID";
            this.Column25.Name = "Column25";
            this.Column25.ReadOnly = true;
            this.Column25.Visible = false;
            // 
            // Column26
            // 
            this.Column26.HeaderText = "DLL_ID";
            this.Column26.Name = "Column26";
            this.Column26.ReadOnly = true;
            this.Column26.Visible = false;
            // 
            // Column27
            // 
            this.Column27.HeaderText = "Библиотека";
            this.Column27.Name = "Column27";
            this.Column27.ReadOnly = true;
            // 
            // Column28
            // 
            this.Column28.HeaderText = "Функция";
            this.Column28.Name = "Column28";
            this.Column28.ReadOnly = true;
            // 
            // Column29
            // 
            this.Column29.HeaderText = "Описание";
            this.Column29.Name = "Column29";
            this.Column29.ReadOnly = true;
            // 
            // Column30
            // 
            this.Column30.HeaderText = "Номер (ординал)";
            this.Column30.Name = "Column30";
            this.Column30.ReadOnly = true;
            this.Column30.Width = 120;
            // 
            // Column31
            // 
            this.Column31.FalseValue = "1";
            this.Column31.HeaderText = "Иссл";
            this.Column31.Name = "Column31";
            this.Column31.ReadOnly = true;
            this.Column31.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column31.TrueValue = "0";
            this.Column31.Width = 40;
            // 
            // buttonCopy
            // 
            this.buttonCopy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCopy.Location = new System.Drawing.Point(3, 183);
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.Size = new System.Drawing.Size(114, 24);
            this.buttonCopy.TabIndex = 14;
            this.buttonCopy.Text = "<< Копировать";
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
            // 
            // Column32
            // 
            this.Column32.Name = "Column32";
            // 
            // MainEditControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainEditControl";
            this.Size = new System.Drawing.Size(1196, 609);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBoxFromBase.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBoxBaseFunctions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBaseFunctions)).EndInit();
            this.groupBoxBaseImports.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBaseImports)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBoxFromFile.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBoxFileFunctions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFileFunctions)).EndInit();
            this.groupBoxFileImports.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFileImports)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBoxFromBase;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBoxType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxBaseNumber;
        private System.Windows.Forms.TextBox textBoxBaseName;
        private System.Windows.Forms.TextBox textBoxBaseAuthor;
        private System.Windows.Forms.TextBox textBoxBaseDescription;
        private System.Windows.Forms.TextBox textBoxBaseMD5;
        private System.Windows.Forms.TextBox textBoxBaseVersion;
        private System.Windows.Forms.GroupBox groupBoxBaseFunctions;
        private System.Windows.Forms.DataGridView dataGridViewBaseFunctions;
        private System.Windows.Forms.GroupBox groupBoxBaseImports;
        private System.Windows.Forms.DataGridView dataGridViewBaseImports;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button buttonAddFunction;
        private System.Windows.Forms.Button buttonUpdateFunction;
        private System.Windows.Forms.Button buttonDeleteFunction;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button buttonAddImport;
        private System.Windows.Forms.Button buttonUpdateImport;
        private System.Windows.Forms.Button buttonDeleteImport;
        private System.Windows.Forms.Button buttonSaveForm;
        private System.Windows.Forms.GroupBox groupBoxFromFile;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxFileFullName;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.TextBox textBoxFileAuthor;
        private System.Windows.Forms.TextBox textBoxFileDescription;
        private System.Windows.Forms.TextBox textBoxFileMD5;
        private System.Windows.Forms.TextBox textBoxFileVersion;
        private System.Windows.Forms.GroupBox groupBoxFileFunctions;
        private System.Windows.Forms.DataGridView dataGridViewFileFunctions;
        private System.Windows.Forms.GroupBox groupBoxFileImports;
        private System.Windows.Forms.DataGridView dataGridViewFileImports;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.Button buttonCopy;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column31;
    }
}
