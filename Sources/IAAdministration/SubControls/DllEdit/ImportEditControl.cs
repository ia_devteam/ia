﻿using System;
using System.Windows.Forms;

using IA.Extensions;

namespace IA.Administration.SubControls.DllEdit
{
    /// <summary>
    /// Класс - Диалог модификации импортируемой функции
    /// </summary>
    internal partial class ImportEditControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private string ObjectName = "Элемент управления - Модификация импортируемой функции";

        /// <summary>
        /// Идентификатор в таблице импортируемых функций
        /// </summary>
        string functionID;
        /// <summary>
        /// Идентификатор в таблице Библиотек
        /// </summary>
        string dllID;
        /// <summary>
        /// Режим редактирования
        /// </summary>
        EditMode mode;
        /// <summary>
        /// Запись в таблице импортируемых функций
        /// </summary>
        string[] record;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="record"></param>
        internal ImportEditControl(EditMode mode, ref string[] record)
        {
            InitializeComponent();

            ToolTip toolTip1 = new ToolTip();

            toolTip1.SetToolTip(this.textBoxLibrary, "Наименование внешней библиотеки");
            toolTip1.SetToolTip(this.textBoxFunction, "Наименование импортируемой функции");
            toolTip1.SetToolTip(this.textBoxDescription, "Описание импортируемой функции");
            toolTip1.SetToolTip(this.textBoxOrdinal, "Номер импортируемой функции");

            //Инициализация полей
            this.record = record;
            this.functionID = record[1];
            this.dllID = record[2];
            this.mode = mode;

            this.buttonSave.DialogResult = DialogResult.OK;

            textBoxLibrary.Text = record[3];
            textBoxFunction.Text = record[4];
            textBoxDescription.Text = record[5];
            textBoxOrdinal.Text = record[6];
            checkBoxToResearch.Checked = record[7] == "1" ? false : true;
            switch(mode)
            {
                case EditMode.NEW:
                    Text = "Заведение новой импортируемой функции";
                    break;
                case EditMode.UPDATE:
                    Text = "Изменение импортируемой функции";
                    break;
                case EditMode.DELETE:
                    Text = "Удаление импортируемой функции";
                    textBoxLibrary.Enabled = false;
                    textBoxFunction.Enabled = false;
                    textBoxOrdinal.Enabled = false;
                    textBoxDescription.Enabled = false;
                    buttonSave.Text = "Удалить";
                    break;
            }
        }

        /// <summary>
        /// Сохранение изменений
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                switch (mode)
                {
                    case EditMode.NEW:
                        record[3] = textBoxLibrary.Text;
                        record[4] = textBoxFunction.Text;
                        record[5] = textBoxDescription.Text;
                        record[6] = textBoxOrdinal.Text;
                        record[7] = checkBoxToResearch.Checked ? "0" : "1";
                        break;
                    case EditMode.UPDATE:
                        record[3] = textBoxLibrary.Text;
                        record[4] = textBoxFunction.Text;
                        record[5] = textBoxDescription.Text;
                        record[6] = textBoxOrdinal.Text;
                        record[7] = checkBoxToResearch.Checked ? "0" : "1";
                        break;
                    case EditMode.DELETE:
                        break;
                }
                base.ParentForm.DialogResult = DialogResult.OK;
                base.ParentForm.Close();

            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
    }
}
