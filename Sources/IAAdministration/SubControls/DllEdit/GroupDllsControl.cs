﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Sql;
using IA.Sql.DatabaseConnections;

namespace IA.Administration.SubControls.DllEdit
{
    /// <summary>
    /// Класс: Диалог обработки и внесения в базу данных всех файлов из папок
    /// </summary>
    internal partial class GroupDllsControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private const string ObjectName = "Элемент управления - Обработка и внесение в базу данных всех файлов и папок";

        /// <summary>
        /// Констркутор
        /// </summary>
        internal GroupDllsControl()
            : this(new Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection())
        {
        }

        /// <summary>
        /// Констркутор
        /// </summary>
        /// <param name="dllKnowledgeBaseDatabaseConnection">Соединение с базой данных "DllKnowledgeBase"</param>
        internal GroupDllsControl(Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection dllKnowledgeBaseDatabaseConnection)
            : base()
        {
            InitializeComponent();

            ToolTip toolTip1 = new ToolTip();

            toolTip1.SetToolTip(this.buttonBrowse, "Диалог обзора папок");
            toolTip1.SetToolTip(this.textBoxFolder, "Папка, содержащая файлы библиотек");
            toolTip1.SetToolTip(this.buttonStart, "Запуск автоматического добавления\n" +
                                                  "в базу данных файлов библиотек");
            toolTip1.SetToolTip(this.radioButtonAdd, "Добавление новой записи в базу данных\n" +
                                                     "при существовании имени библиотеки в базе данных");
            toolTip1.SetToolTip(this.radioButtonReplace, "Замена записи при существовании\n" +
                                                         "имени библиотеки в базе данных");
            toolTip1.SetToolTip(this.radioButtonIgnore, "Пропуск библиотеки при существовании\n" +
                                                         "имени библиотеки в базе данных");            
        }

        /// <summary>
        /// Заполнить элемент управления в соответствии с содержимым базы данных
        /// Предполагается, что соединение с базой данных было установлено ранее
        /// </summary>
        private void Generate()
        {
            //Очищаем элемент управления
            this.Clear();

            radioButtonReplace.Checked = true;
        }

        /// <summary>
        /// Очистить элемент управления
        /// </summary>
        private void Clear()
        {
            //TextBox
            this.textBoxFolder.Text = String.Empty;

            //RadioButton
            radioButtonReplace.Checked = false;
            radioButtonAdd.Checked = false;
            radioButtonIgnore.Checked = false;

            //ListBox
            listBoxLog.Items.Clear();
        }

        /// <summary>
        /// Вывод строки в окно лога - (в листбокс)
        /// </summary>
        /// <param name="str">строка</param>
        /// <param name="isNewLine">true - новая строка, false - добавление к последней строке</param>
        private void Log(string str, bool isNewLine)
        {
            // Если строка не задана - очистка лога
            if (str == null)
                listBoxLog.Items.Clear();
            else
            {
                if (isNewLine || listBoxLog.Items.Count == 0)
                    listBoxLog.Items.Add(str);
                else
                    listBoxLog.Items[listBoxLog.Items.Count - 1] = str;
                listBoxLog.SelectedIndex = listBoxLog.Items.Count - 1;
            }
        }

        #region Кнопки
        /// <summary>
        /// Кнопка - обзор папки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog fd = new FolderBrowserDialog();
                fd.Description = "Укажите каталог обрабатываемых библиотек";
                if (fd.ShowDialog() == DialogResult.OK)
                    textBoxFolder.Text = fd.SelectedPath;
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - запуск процесса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStart_Click(object sender, EventArgs e)
        {
            try
            {
                Log(null, true);
                groupBoxLog.Text = "";
                if (!Directory.Exists(textBoxFolder.Text))
                {
                    Monitor.Log.Warning(ObjectName, "Указанный каталог отсутствует", true);
                    return;
                }
                DirectoryInfo di = new DirectoryInfo(textBoxFolder.Text);
                FileInfo[] files = di.GetFiles("*.*", SearchOption.AllDirectories);
                if (files.Length == 0)
                {
                    Monitor.Log.Warning(ObjectName, "У источника нет файлов", true);
                    textBoxFolder.Focus();
                    return;
                }
                int nfile = 0, nfiles = files.Length;

                List<string[]> dllsFromBase = ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).DllBase;
                Cursor = Cursors.WaitCursor;
                foreach (FileInfo file in files)
                {
                    nfile++;
                    groupBoxLog.Text = "Обработка: " + nfile + "/" + nfiles;
                    Log(file.FullName, true);
                    Application.DoEvents();
                    if (file.Length <= 64)
                        continue;

                    //Проверка того, что тип файла подходит
                    byte[] ImageFile = File.ReadAllBytes(file.FullName);
                    if (ImageFile[0] != 0x4D || ImageFile[1] != 0x5A)  // MZ - magic number
                        continue;
                    int sg = BitConverter.ToInt32(ImageFile, 60);
                    if (file.Length <= sg)
                        continue;
                    uint sw = BitConverter.ToUInt32(ImageFile, sg);
                    if (sw != 0x00004550)
                        continue;

                    string dllName = file.Name;
                    string checksum = MainControl.ComputeCtrlSum(file.FullName);
                    List<string> lss = MainControl.AuthorDescription(file.FullName);
                    string author = lss[0];
                    string descr = lss[1];
                    string version = lss[2];

                    List<string[]> lsTemp = dllsFromBase.Where(x => x[4] == checksum && x[1] == file.Name).ToList();
                    if (radioButtonReplace.Checked && lsTemp.Count > 0)
                        foreach (string[] sa in lsTemp)
                            ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).DeleteDllFromBase(int.Parse(sa[0]));

                    if (radioButtonIgnore.Checked && lsTemp.Count > 0)
                        continue;

                    List<string[]> lsFunc = ImpExp.ImpExp.Export(file.FullName).Select(x => x.Split('\t')).ToArray().Select(y => new string[] { y[1], "", y[0], "0" }).ToList();
                    List<string[]> lsImports = ImpExp.ImpExp.Import(file.FullName).Select(x => x.Split('\t')).ToArray().Select(y => new string[] { y[0], y[1], "", y[2], "0" }).ToList();


                    ((DllKnowledgeBaseDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).InsertDllToBase(new string[] { file.Name, author, descr, checksum, version, "0" }, ImageFile, lsFunc, lsImports);

                    Log(file.FullName + "  " + lsFunc.Count + " " + lsImports.Count, false);
                    Application.DoEvents();
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        } 
        #endregion

        #region SqlDependentControl
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления в соответствии с содержимым базы данных
            this.Generate();

            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Делаем его недоступным
            this.Enabled = false;
        }
        #endregion
    }
}
