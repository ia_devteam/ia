﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Sql;
using IA.Sql.DatabaseConnections;

namespace IA.Administration.SubControls.SignatureFile
{
    /// <summary>
    /// Класс - Диалоговое окно повисших ссылок
    /// </summary>
    internal partial class FormHangingListControl : Form /*Sql.Controls.SqlDependentControl<Sql.DatabaseConnections.SignatureFileDatabaseConnection>*/
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private string ObjectName = "Элемент управленния - Повисшие ссылки";

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="signatureFileDatabaseConnection">Соединение с базой данных "SignatureFile"</param>
        internal FormHangingListControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Заполнить элемент управления в соответствии с содержимым базы данных
        /// Предполагается, что соединение с базой данных было установлено ранее
        /// </summary>
        private void Generate()
        {
            //Очищаем элемент управления
            this.Clear();

            //Заполняем элемент управления данными из базы данных
            List<string> WorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces.Select(x => x[0]).ToList();
            foreach (string[] sa in ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat.Where(x => !WorkPlaces.Contains(x[2])))
                dataGridViewHangingList.Rows.Add(sa);
        }

        /// <summary>
        /// Очистить элемент управления
        /// </summary>
        private void Clear()
        {
            dataGridViewHangingList.Rows.Clear();
        }

        #region Кнопки
        /// <summary>
        /// Кнопка - Очистка ссылок
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClear_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> WorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces.Select(x => x[0]).ToList();
                foreach (string[] sa in ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat.Where(x => !WorkPlaces.Contains(x[2])))
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatDelete(new string[] { sa[0] });

                //Заполняем элемент управления в соответствии с содержимым базы данных
                this.Generate();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        #endregion

        #region SqlDependentControl
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления в соответствии с содержимым базы данных
            this.Generate();

            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Делаем его недоступным
            this.Enabled = false;
        }
        #endregion
    }
}
