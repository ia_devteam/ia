﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Sql;
using IA.Sql.DatabaseConnections;

namespace IA.Administration.SubControls.SignatureFile
{
    /// <summary>
    /// Класс - Диалоговое окно - повисшие шаблоны
    /// </summary>
    internal partial class FormHangingPatternControl : UserControl /*Sql.Controls.SqlDependentControl<Sql.DatabaseConnections.SignatureFileDatabaseConnection>*/
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private string ObjectName = "Элемент управления - Повисшие шаблоны";

        private List<string[]> pattern = new List<string[]>();

        /// <summary>
        /// Конструктор
        /// </summary>
        internal FormHangingPatternControl()
            : this(new Sql.DatabaseConnections.SignatureFileDatabaseConnection())
        {
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="signatureFileDatabaseConnection">Соединение с базой данных "SignatureFile"</param>
        internal FormHangingPatternControl(Sql.DatabaseConnections.SignatureFileDatabaseConnection signatureFileDatabaseConnection)
            : base()
        {
            InitializeComponent();            
        }

        /// <summary>
        /// Заполнить элемент управления в соответствии с содержимым базы данных
        /// Предполагается, что соединение с базой данных было установлено ранее
        /// </summary>
        private void Generate()
        {
            //Очищаем элемент управления
            this.Clear();

            //Заполняем элемент управления данными из базы данных
            this.pattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern.Where
                (x => ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat.Where(y => y[1] == x[1] && y[2] == x[2]).Count() == 0).ToList();
            foreach (string[] sa in pattern)
                dataGridViewHangingPattern.Rows.Add(sa);
        }

        /// <summary>
        /// Очистить элемент управления
        /// </summary>
        private void Clear()
        {
            dataGridViewHangingPattern.Rows.Clear();
        }

        #region Кнопки
        /// <summary>
        /// Кнопка - Очистка повисших ссылок
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClear_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (string[] sa in ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern.Where
                    (x => ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat.Where(y => y[1] == x[1] && y[2] == x[2]).Count() == 0).ToList())
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternDeleteID(new string[] { sa[0] });

                //Заполняем элемент управления в соответствии с содержимым базы данных
                this.Generate();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        #endregion

        #region SqlDependentControl
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления в соответствии с содержимым базы данных
            this.Generate();

            //Делаем его доступным
            this.Enabled = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Делаем его недоступным
            this.Enabled = false;
        }
        #endregion
    }

}
