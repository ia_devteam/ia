﻿namespace IA.Administration.SubControls.SignatureFile
{
    partial class MainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listBoxTypesOrExtentions = new System.Windows.Forms.ListBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewSignature = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxFileTypes = new System.Windows.Forms.GroupBox();
            this.listBoxTypesOfFiles = new System.Windows.Forms.ListBox();
            this.groupBoxLanguages = new System.Windows.Forms.GroupBox();
            this.listBoxLanguages = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAddSignature = new System.Windows.Forms.Button();
            this.buttonAddToDefault = new System.Windows.Forms.Button();
            this.buttonSaveSignature = new System.Windows.Forms.Button();
            this.buttonDeleteSignature = new System.Windows.Forms.Button();
            this.buttonDeleteExceptDefault = new System.Windows.Forms.Button();
            this.buttonMoveToDefault = new System.Windows.Forms.Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonHangingLinks = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxFilters = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewPattern = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonDeletePattern = new System.Windows.Forms.Button();
            this.buttonSavePattern = new System.Windows.Forms.Button();
            this.buttonAddPattern = new System.Windows.Forms.Button();
            this.buttonHangingPatterns = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonDoubleNames = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonDoubleExtentions = new System.Windows.Forms.Button();
            this.buttonNoTypesNoPatterns = new System.Windows.Forms.Button();
            this.buttonNoPatterns = new System.Windows.Forms.Button();
            this.buttonUnknownTypes = new System.Windows.Forms.Button();
            this.buttonWorkPlaces = new System.Windows.Forms.Button();
            this.buttonDeleteWorkingPlace = new System.Windows.Forms.Button();
            this.buttonHasBinaryPattern = new System.Windows.Forms.Button();
            this.buttonAllNames = new System.Windows.Forms.Button();
            this.buttonAllExtentions = new System.Windows.Forms.Button();
            this.listBoxWorkingPlaces = new System.Windows.Forms.ListBox();
            this.checkBoxAllWithDefault = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.labelOperation = new System.Windows.Forms.Label();
            this.labelCurrentType = new System.Windows.Forms.Label();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSignature)).BeginInit();
            this.tableLayoutPanel8.SuspendLayout();
            this.groupBoxFileTypes.SuspendLayout();
            this.groupBoxLanguages.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPattern)).BeginInit();
            this.tableLayoutPanel11.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1121, 619);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.splitContainer1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 23);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1115, 593);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(163, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(949, 587);
            this.splitContainer1.SplitterDistance = 205;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listBoxTypesOrExtentions);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(205, 587);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Результаты запросов";
            // 
            // listBoxTypesOrExtentions
            // 
            this.listBoxTypesOrExtentions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxTypesOrExtentions.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.listBoxTypesOrExtentions.FormattingEnabled = true;
            this.listBoxTypesOrExtentions.Location = new System.Drawing.Point(3, 16);
            this.listBoxTypesOrExtentions.Name = "listBoxTypesOrExtentions";
            this.listBoxTypesOrExtentions.Size = new System.Drawing.Size(199, 568);
            this.listBoxTypesOrExtentions.TabIndex = 0;
            this.listBoxTypesOrExtentions.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBoxTypesOrExtentions_DrawItem);
            this.listBoxTypesOrExtentions.SelectedIndexChanged += new System.EventHandler(this.listBoxTypesOrExtentions_SelectedIndexChanged);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox4);
            this.splitContainer2.Size = new System.Drawing.Size(740, 587);
            this.splitContainer2.SplitterDistance = 330;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel5);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(740, 330);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Типы файлов";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel5.Controls.Add(this.dataGridViewSignature, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel10, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel7, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(734, 311);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // dataGridViewSignature
            // 
            this.dataGridViewSignature.AllowUserToAddRows = false;
            this.dataGridViewSignature.AllowUserToDeleteRows = false;
            this.dataGridViewSignature.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSignature.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column16});
            this.dataGridViewSignature.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewSignature.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewSignature.Name = "dataGridViewSignature";
            this.dataGridViewSignature.RowHeadersWidth = 5;
            this.dataGridViewSignature.Size = new System.Drawing.Size(568, 196);
            this.dataGridViewSignature.TabIndex = 0;
            this.dataGridViewSignature.SelectionChanged += new System.EventHandler(this.dataGridViewSignature_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "№";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 50;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Код места";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 50;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Тип файла";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 86;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Описание";
            this.Column5.Name = "Column5";
            this.Column5.Width = 180;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Полное описание";
            this.Column6.Name = "Column6";
            this.Column6.Width = 120;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Расширения";
            this.Column7.Name = "Column7";
            this.Column7.Width = 90;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "Язык";
            this.Column16.Name = "Column16";
            this.Column16.Width = 70;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Controls.Add(this.groupBoxFileTypes, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.groupBoxLanguages, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 205);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(568, 103);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // groupBoxFileTypes
            // 
            this.groupBoxFileTypes.Controls.Add(this.listBoxTypesOfFiles);
            this.groupBoxFileTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxFileTypes.Location = new System.Drawing.Point(3, 3);
            this.groupBoxFileTypes.Name = "groupBoxFileTypes";
            this.groupBoxFileTypes.Size = new System.Drawing.Size(278, 97);
            this.groupBoxFileTypes.TabIndex = 0;
            this.groupBoxFileTypes.TabStop = false;
            this.groupBoxFileTypes.Text = "Типы содержимого";
            // 
            // listBoxTypesOfFiles
            // 
            this.listBoxTypesOfFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxTypesOfFiles.FormattingEnabled = true;
            this.listBoxTypesOfFiles.Location = new System.Drawing.Point(3, 16);
            this.listBoxTypesOfFiles.Name = "listBoxTypesOfFiles";
            this.listBoxTypesOfFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxTypesOfFiles.Size = new System.Drawing.Size(272, 78);
            this.listBoxTypesOfFiles.TabIndex = 0;
            this.listBoxTypesOfFiles.SelectedIndexChanged += new System.EventHandler(this.listBoxTypeOfFiles_SelectedIndexChanged);
            // 
            // groupBoxLanguages
            // 
            this.groupBoxLanguages.Controls.Add(this.listBoxLanguages);
            this.groupBoxLanguages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxLanguages.Location = new System.Drawing.Point(287, 3);
            this.groupBoxLanguages.Name = "groupBoxLanguages";
            this.groupBoxLanguages.Size = new System.Drawing.Size(278, 97);
            this.groupBoxLanguages.TabIndex = 1;
            this.groupBoxLanguages.TabStop = false;
            this.groupBoxLanguages.Text = "Языки программирования";
            // 
            // listBoxLanguages
            // 
            this.listBoxLanguages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxLanguages.FormattingEnabled = true;
            this.listBoxLanguages.Location = new System.Drawing.Point(3, 16);
            this.listBoxLanguages.Name = "listBoxLanguages";
            this.listBoxLanguages.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxLanguages.Size = new System.Drawing.Size(272, 78);
            this.listBoxLanguages.TabIndex = 0;
            this.listBoxLanguages.SelectedIndexChanged += new System.EventHandler(this.listBoxLanguages_SelectedIndexChanged);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.buttonAddSignature, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.buttonAddToDefault, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.buttonSaveSignature, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.buttonDeleteSignature, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.buttonDeleteExceptDefault, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.buttonMoveToDefault, 0, 3);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(577, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 8;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(154, 196);
            this.tableLayoutPanel10.TabIndex = 11;
            // 
            // buttonAddSignature
            // 
            this.buttonAddSignature.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAddSignature.Location = new System.Drawing.Point(0, 0);
            this.buttonAddSignature.Margin = new System.Windows.Forms.Padding(0);
            this.buttonAddSignature.Name = "buttonAddSignature";
            this.buttonAddSignature.Size = new System.Drawing.Size(154, 24);
            this.buttonAddSignature.TabIndex = 7;
            this.buttonAddSignature.Text = "Добавить";
            this.buttonAddSignature.UseVisualStyleBackColor = true;
            this.buttonAddSignature.Click += new System.EventHandler(this.buttonAddSignature_Click);
            // 
            // buttonAddToDefault
            // 
            this.buttonAddToDefault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAddToDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddToDefault.Location = new System.Drawing.Point(0, 144);
            this.buttonAddToDefault.Margin = new System.Windows.Forms.Padding(0);
            this.buttonAddToDefault.Name = "buttonAddToDefault";
            this.buttonAddToDefault.Size = new System.Drawing.Size(154, 36);
            this.buttonAddToDefault.TabIndex = 9;
            this.buttonAddToDefault.Text = "Добавить шаб. к Базовому рабочему месту";
            this.buttonAddToDefault.UseVisualStyleBackColor = true;
            this.buttonAddToDefault.Click += new System.EventHandler(this.buttonAddToDefault_Click);
            // 
            // buttonSaveSignature
            // 
            this.buttonSaveSignature.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSaveSignature.Location = new System.Drawing.Point(0, 24);
            this.buttonSaveSignature.Margin = new System.Windows.Forms.Padding(0);
            this.buttonSaveSignature.Name = "buttonSaveSignature";
            this.buttonSaveSignature.Size = new System.Drawing.Size(154, 24);
            this.buttonSaveSignature.TabIndex = 0;
            this.buttonSaveSignature.Text = "Сохранить";
            this.buttonSaveSignature.UseVisualStyleBackColor = true;
            this.buttonSaveSignature.Click += new System.EventHandler(this.buttonSaveSignature_Click);
            // 
            // buttonDeleteSignature
            // 
            this.buttonDeleteSignature.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDeleteSignature.Location = new System.Drawing.Point(0, 48);
            this.buttonDeleteSignature.Margin = new System.Windows.Forms.Padding(0);
            this.buttonDeleteSignature.Name = "buttonDeleteSignature";
            this.buttonDeleteSignature.Size = new System.Drawing.Size(154, 24);
            this.buttonDeleteSignature.TabIndex = 1;
            this.buttonDeleteSignature.Text = "Удалить";
            this.buttonDeleteSignature.UseVisualStyleBackColor = true;
            this.buttonDeleteSignature.Click += new System.EventHandler(this.buttonDeleteSignature_Click);
            // 
            // buttonDeleteExceptDefault
            // 
            this.buttonDeleteExceptDefault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDeleteExceptDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDeleteExceptDefault.Location = new System.Drawing.Point(0, 108);
            this.buttonDeleteExceptDefault.Margin = new System.Windows.Forms.Padding(0);
            this.buttonDeleteExceptDefault.Name = "buttonDeleteExceptDefault";
            this.buttonDeleteExceptDefault.Size = new System.Drawing.Size(154, 36);
            this.buttonDeleteExceptDefault.TabIndex = 3;
            this.buttonDeleteExceptDefault.Text = "Удалить кроме Базового рабочего места";
            this.buttonDeleteExceptDefault.UseVisualStyleBackColor = true;
            this.buttonDeleteExceptDefault.Click += new System.EventHandler(this.buttonDeleteExceptDefault_Click);
            // 
            // buttonMoveToDefault
            // 
            this.buttonMoveToDefault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonMoveToDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonMoveToDefault.Location = new System.Drawing.Point(0, 72);
            this.buttonMoveToDefault.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMoveToDefault.Name = "buttonMoveToDefault";
            this.buttonMoveToDefault.Size = new System.Drawing.Size(154, 36);
            this.buttonMoveToDefault.TabIndex = 2;
            this.buttonMoveToDefault.Text = "Перенести в Базовое рабочее место";
            this.buttonMoveToDefault.UseVisualStyleBackColor = true;
            this.buttonMoveToDefault.Click += new System.EventHandler(this.buttonMoveToDefault_Click);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.buttonHangingLinks, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.checkBoxFilters, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(577, 205);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(154, 103);
            this.tableLayoutPanel7.TabIndex = 12;
            // 
            // buttonHangingLinks
            // 
            this.buttonHangingLinks.Location = new System.Drawing.Point(0, 79);
            this.buttonHangingLinks.Margin = new System.Windows.Forms.Padding(0);
            this.buttonHangingLinks.Name = "buttonHangingLinks";
            this.buttonHangingLinks.Size = new System.Drawing.Size(154, 21);
            this.buttonHangingLinks.TabIndex = 10;
            this.buttonHangingLinks.Text = "Повисшие типы";
            this.buttonHangingLinks.UseVisualStyleBackColor = true;
            this.buttonHangingLinks.Visible = false;
            this.buttonHangingLinks.Click += new System.EventHandler(this.buttonHangingLinks_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = " ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxFilters
            // 
            this.checkBoxFilters.AutoSize = true;
            this.checkBoxFilters.Location = new System.Drawing.Point(3, 58);
            this.checkBoxFilters.Name = "checkBoxFilters";
            this.checkBoxFilters.Size = new System.Drawing.Size(147, 17);
            this.checkBoxFilters.TabIndex = 11;
            this.checkBoxFilters.Text = "Использовать фильтры";
            this.checkBoxFilters.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel6);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(740, 253);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Шаблоны";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel6.Controls.Add(this.dataGridViewPattern, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel11, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(734, 234);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // dataGridViewPattern
            // 
            this.dataGridViewPattern.AllowUserToAddRows = false;
            this.dataGridViewPattern.AllowUserToDeleteRows = false;
            this.dataGridViewPattern.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPattern.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15});
            this.dataGridViewPattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPattern.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewPattern.Name = "dataGridViewPattern";
            this.dataGridViewPattern.RowHeadersWidth = 5;
            this.dataGridViewPattern.Size = new System.Drawing.Size(568, 228);
            this.dataGridViewPattern.TabIndex = 0;
            this.dataGridViewPattern.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPattern_CellValueChanged);
            this.dataGridViewPattern.SelectionChanged += new System.EventHandler(this.dataGridViewPattern_SelectionChanged);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.buttonDeletePattern, 0, 5);
            this.tableLayoutPanel11.Controls.Add(this.buttonSavePattern, 0, 4);
            this.tableLayoutPanel11.Controls.Add(this.buttonAddPattern, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.buttonHangingPatterns, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(577, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 6;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(154, 228);
            this.tableLayoutPanel11.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(3, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = " ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonDeletePattern
            // 
            this.buttonDeletePattern.Location = new System.Drawing.Point(0, 204);
            this.buttonDeletePattern.Margin = new System.Windows.Forms.Padding(0);
            this.buttonDeletePattern.Name = "buttonDeletePattern";
            this.buttonDeletePattern.Size = new System.Drawing.Size(154, 24);
            this.buttonDeletePattern.TabIndex = 6;
            this.buttonDeletePattern.Text = "Удалить";
            this.buttonDeletePattern.UseVisualStyleBackColor = true;
            this.buttonDeletePattern.Click += new System.EventHandler(this.buttonDeletePattern_Click);
            // 
            // buttonSavePattern
            // 
            this.buttonSavePattern.Location = new System.Drawing.Point(0, 180);
            this.buttonSavePattern.Margin = new System.Windows.Forms.Padding(0);
            this.buttonSavePattern.Name = "buttonSavePattern";
            this.buttonSavePattern.Size = new System.Drawing.Size(154, 24);
            this.buttonSavePattern.TabIndex = 5;
            this.buttonSavePattern.Text = "Сохранить";
            this.buttonSavePattern.UseVisualStyleBackColor = true;
            this.buttonSavePattern.Click += new System.EventHandler(this.buttonSavePattern_Click);
            // 
            // buttonAddPattern
            // 
            this.buttonAddPattern.Location = new System.Drawing.Point(0, 156);
            this.buttonAddPattern.Margin = new System.Windows.Forms.Padding(0);
            this.buttonAddPattern.Name = "buttonAddPattern";
            this.buttonAddPattern.Size = new System.Drawing.Size(154, 24);
            this.buttonAddPattern.TabIndex = 4;
            this.buttonAddPattern.Text = "Добавить";
            this.buttonAddPattern.UseVisualStyleBackColor = true;
            this.buttonAddPattern.Click += new System.EventHandler(this.buttonAddPattern_Click);
            // 
            // buttonHangingPatterns
            // 
            this.buttonHangingPatterns.Location = new System.Drawing.Point(1, 1);
            this.buttonHangingPatterns.Margin = new System.Windows.Forms.Padding(1);
            this.buttonHangingPatterns.Name = "buttonHangingPatterns";
            this.buttonHangingPatterns.Size = new System.Drawing.Size(152, 22);
            this.buttonHangingPatterns.TabIndex = 8;
            this.buttonHangingPatterns.Text = "Повисшие шаблоны";
            this.buttonHangingPatterns.UseVisualStyleBackColor = true;
            this.buttonHangingPatterns.Visible = false;
            this.buttonHangingPatterns.Click += new System.EventHandler(this.buttonHangingPatterns_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(154, 587);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Запросы";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.buttonDoubleNames, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 13);
            this.tableLayoutPanel3.Controls.Add(this.buttonDoubleExtentions, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.buttonNoTypesNoPatterns, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.buttonNoPatterns, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.buttonUnknownTypes, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.buttonWorkPlaces, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.buttonDeleteWorkingPlace, 0, 12);
            this.tableLayoutPanel3.Controls.Add(this.buttonHasBinaryPattern, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.buttonAllNames, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.buttonAllExtentions, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.listBoxWorkingPlaces, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.checkBoxAllWithDefault, 0, 10);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 14;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(148, 568);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // buttonDoubleNames
            // 
            this.buttonDoubleNames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDoubleNames.Location = new System.Drawing.Point(0, 48);
            this.buttonDoubleNames.Margin = new System.Windows.Forms.Padding(0);
            this.buttonDoubleNames.Name = "buttonDoubleNames";
            this.buttonDoubleNames.Size = new System.Drawing.Size(148, 24);
            this.buttonDoubleNames.TabIndex = 0;
            this.buttonDoubleNames.Text = "Дубликаты имен";
            this.buttonDoubleNames.UseVisualStyleBackColor = true;
            this.buttonDoubleNames.Click += new System.EventHandler(this.buttonDoubleNames_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(3, 548);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = " ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonDoubleExtentions
            // 
            this.buttonDoubleExtentions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDoubleExtentions.Location = new System.Drawing.Point(0, 72);
            this.buttonDoubleExtentions.Margin = new System.Windows.Forms.Padding(0);
            this.buttonDoubleExtentions.Name = "buttonDoubleExtentions";
            this.buttonDoubleExtentions.Size = new System.Drawing.Size(148, 24);
            this.buttonDoubleExtentions.TabIndex = 1;
            this.buttonDoubleExtentions.Text = "Дубликаты расширений";
            this.buttonDoubleExtentions.UseVisualStyleBackColor = true;
            this.buttonDoubleExtentions.Click += new System.EventHandler(this.buttonDoubleExtentions_Click);
            // 
            // buttonNoTypesNoPatterns
            // 
            this.buttonNoTypesNoPatterns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonNoTypesNoPatterns.Location = new System.Drawing.Point(0, 96);
            this.buttonNoTypesNoPatterns.Margin = new System.Windows.Forms.Padding(0);
            this.buttonNoTypesNoPatterns.Name = "buttonNoTypesNoPatterns";
            this.buttonNoTypesNoPatterns.Size = new System.Drawing.Size(148, 24);
            this.buttonNoTypesNoPatterns.TabIndex = 4;
            this.buttonNoTypesNoPatterns.Text = "Нет типов и шаблонов";
            this.buttonNoTypesNoPatterns.UseVisualStyleBackColor = true;
            this.buttonNoTypesNoPatterns.Click += new System.EventHandler(this.buttonNoTypesNoPatterns_Click);
            // 
            // buttonNoPatterns
            // 
            this.buttonNoPatterns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonNoPatterns.Location = new System.Drawing.Point(0, 120);
            this.buttonNoPatterns.Margin = new System.Windows.Forms.Padding(0);
            this.buttonNoPatterns.Name = "buttonNoPatterns";
            this.buttonNoPatterns.Size = new System.Drawing.Size(148, 24);
            this.buttonNoPatterns.TabIndex = 5;
            this.buttonNoPatterns.Text = "Нет шаблонов";
            this.buttonNoPatterns.UseVisualStyleBackColor = true;
            this.buttonNoPatterns.Click += new System.EventHandler(this.buttonNoPatterns_Click);
            // 
            // buttonUnknownTypes
            // 
            this.buttonUnknownTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonUnknownTypes.Location = new System.Drawing.Point(0, 144);
            this.buttonUnknownTypes.Margin = new System.Windows.Forms.Padding(0);
            this.buttonUnknownTypes.Name = "buttonUnknownTypes";
            this.buttonUnknownTypes.Size = new System.Drawing.Size(148, 24);
            this.buttonUnknownTypes.TabIndex = 6;
            this.buttonUnknownTypes.Text = "Неопределенные типы";
            this.buttonUnknownTypes.UseVisualStyleBackColor = true;
            this.buttonUnknownTypes.Click += new System.EventHandler(this.buttonUnknownTypes_Click);
            // 
            // buttonWorkPlaces
            // 
            this.buttonWorkPlaces.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonWorkPlaces.ForeColor = System.Drawing.Color.Blue;
            this.buttonWorkPlaces.Location = new System.Drawing.Point(0, 202);
            this.buttonWorkPlaces.Margin = new System.Windows.Forms.Padding(0);
            this.buttonWorkPlaces.Name = "buttonWorkPlaces";
            this.buttonWorkPlaces.Size = new System.Drawing.Size(148, 24);
            this.buttonWorkPlaces.TabIndex = 9;
            this.buttonWorkPlaces.Text = "Рабочие места";
            this.buttonWorkPlaces.UseVisualStyleBackColor = true;
            this.buttonWorkPlaces.Click += new System.EventHandler(this.buttonWorkPlaces_Click);
            // 
            // buttonDeleteWorkingPlace
            // 
            this.buttonDeleteWorkingPlace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDeleteWorkingPlace.Location = new System.Drawing.Point(0, 524);
            this.buttonDeleteWorkingPlace.Margin = new System.Windows.Forms.Padding(0);
            this.buttonDeleteWorkingPlace.Name = "buttonDeleteWorkingPlace";
            this.buttonDeleteWorkingPlace.Size = new System.Drawing.Size(148, 24);
            this.buttonDeleteWorkingPlace.TabIndex = 11;
            this.buttonDeleteWorkingPlace.Text = "Удалить рабочее место";
            this.buttonDeleteWorkingPlace.UseVisualStyleBackColor = true;
            this.buttonDeleteWorkingPlace.Click += new System.EventHandler(this.buttonDeleteWorkingPlace_Click);
            // 
            // buttonHasBinaryPattern
            // 
            this.buttonHasBinaryPattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonHasBinaryPattern.Location = new System.Drawing.Point(0, 168);
            this.buttonHasBinaryPattern.Margin = new System.Windows.Forms.Padding(0);
            this.buttonHasBinaryPattern.Name = "buttonHasBinaryPattern";
            this.buttonHasBinaryPattern.Size = new System.Drawing.Size(148, 24);
            this.buttonHasBinaryPattern.TabIndex = 12;
            this.buttonHasBinaryPattern.Text = "Есть бинарные шаблоны";
            this.buttonHasBinaryPattern.UseVisualStyleBackColor = true;
            this.buttonHasBinaryPattern.Click += new System.EventHandler(this.buttonHasBinaryPattern_Click);
            // 
            // buttonAllNames
            // 
            this.buttonAllNames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAllNames.Location = new System.Drawing.Point(0, 0);
            this.buttonAllNames.Margin = new System.Windows.Forms.Padding(0);
            this.buttonAllNames.Name = "buttonAllNames";
            this.buttonAllNames.Size = new System.Drawing.Size(148, 24);
            this.buttonAllNames.TabIndex = 2;
            this.buttonAllNames.Text = "Все имена";
            this.buttonAllNames.UseVisualStyleBackColor = true;
            this.buttonAllNames.Click += new System.EventHandler(this.buttonAllNames_Click);
            // 
            // buttonAllExtentions
            // 
            this.buttonAllExtentions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAllExtentions.Location = new System.Drawing.Point(0, 24);
            this.buttonAllExtentions.Margin = new System.Windows.Forms.Padding(0);
            this.buttonAllExtentions.Name = "buttonAllExtentions";
            this.buttonAllExtentions.Size = new System.Drawing.Size(148, 24);
            this.buttonAllExtentions.TabIndex = 3;
            this.buttonAllExtentions.Text = "Все расширения";
            this.buttonAllExtentions.UseVisualStyleBackColor = true;
            this.buttonAllExtentions.Click += new System.EventHandler(this.buttonAllExtentions_Click);
            // 
            // listBoxWorkingPlaces
            // 
            this.listBoxWorkingPlaces.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxWorkingPlaces.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.listBoxWorkingPlaces.FormattingEnabled = true;
            this.listBoxWorkingPlaces.Location = new System.Drawing.Point(3, 265);
            this.listBoxWorkingPlaces.Name = "listBoxWorkingPlaces";
            this.listBoxWorkingPlaces.Size = new System.Drawing.Size(142, 256);
            this.listBoxWorkingPlaces.TabIndex = 10;
            this.listBoxWorkingPlaces.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBoxWorkingPlaces_DrawItem);
            this.listBoxWorkingPlaces.SelectedIndexChanged += new System.EventHandler(this.listBoxWorkingPlaces_SelectedIndexChanged);
            // 
            // checkBoxAllWithDefault
            // 
            this.checkBoxAllWithDefault.AutoSize = true;
            this.checkBoxAllWithDefault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxAllWithDefault.Location = new System.Drawing.Point(3, 229);
            this.checkBoxAllWithDefault.Name = "checkBoxAllWithDefault";
            this.checkBoxAllWithDefault.Size = new System.Drawing.Size(142, 30);
            this.checkBoxAllWithDefault.TabIndex = 13;
            this.checkBoxAllWithDefault.Text = "Вместе с Базовым рабочим местом";
            this.checkBoxAllWithDefault.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 161F));
            this.tableLayoutPanel4.Controls.Add(this.labelOperation, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelCurrentType, 2, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1115, 14);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // labelOperation
            // 
            this.labelOperation.AutoSize = true;
            this.labelOperation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelOperation.ForeColor = System.Drawing.Color.Blue;
            this.labelOperation.Location = new System.Drawing.Point(163, 0);
            this.labelOperation.Name = "labelOperation";
            this.labelOperation.Size = new System.Drawing.Size(391, 14);
            this.labelOperation.TabIndex = 0;
            this.labelOperation.Text = " ";
            this.labelOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCurrentType
            // 
            this.labelCurrentType.AutoSize = true;
            this.labelCurrentType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCurrentType.ForeColor = System.Drawing.Color.Blue;
            this.labelCurrentType.Location = new System.Drawing.Point(560, 0);
            this.labelCurrentType.Name = "labelCurrentType";
            this.labelCurrentType.Size = new System.Drawing.Size(391, 14);
            this.labelCurrentType.TabIndex = 1;
            this.labelCurrentType.Text = " ";
            this.labelCurrentType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "ID";
            this.Column8.Name = "Column8";
            this.Column8.Visible = false;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "FFID";
            this.Column9.Name = "Column9";
            this.Column9.Visible = false;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "WID";
            this.Column10.Name = "Column10";
            this.Column10.Visible = false;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "Тип шаблона";
            this.Column11.Items.AddRange(new object[] {
            "XML",
            "TEXT",
            "Binary",
            "Executable"});
            this.Column11.Name = "Column11";
            this.Column11.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Заголовок";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column12.Width = 80;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Шаблон";
            this.Column13.Name = "Column13";
            this.Column13.Width = 200;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Заголовок2";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column14.Width = 80;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Шаблон2";
            this.Column15.Name = "Column15";
            this.Column15.Width = 160;
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(1121, 619);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSignature)).EndInit();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.groupBoxFileTypes.ResumeLayout(false);
            this.groupBoxLanguages.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPattern)).EndInit();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox listBoxTypesOrExtentions;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.DataGridView dataGridViewSignature;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.GroupBox groupBoxFileTypes;
        private System.Windows.Forms.ListBox listBoxTypesOfFiles;
        private System.Windows.Forms.GroupBox groupBoxLanguages;
        private System.Windows.Forms.ListBox listBoxLanguages;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.DataGridView dataGridViewPattern;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button buttonDoubleNames;
        private System.Windows.Forms.Button buttonDoubleExtentions;
        private System.Windows.Forms.Button buttonAllNames;
        private System.Windows.Forms.Button buttonAllExtentions;
        private System.Windows.Forms.Button buttonNoTypesNoPatterns;
        private System.Windows.Forms.Button buttonNoPatterns;
        private System.Windows.Forms.Button buttonUnknownTypes;
        private System.Windows.Forms.Button buttonWorkPlaces;
        private System.Windows.Forms.ListBox listBoxWorkingPlaces;
        private System.Windows.Forms.Button buttonDeleteWorkingPlace;
        private System.Windows.Forms.Button buttonHasBinaryPattern;
        private System.Windows.Forms.CheckBox checkBoxAllWithDefault;
        private System.Windows.Forms.Button buttonAddPattern;
        private System.Windows.Forms.Button buttonSavePattern;
        private System.Windows.Forms.Button buttonDeletePattern;
        private System.Windows.Forms.Button buttonDeleteExceptDefault;
        private System.Windows.Forms.Button buttonMoveToDefault;
        private System.Windows.Forms.Button buttonDeleteSignature;
        private System.Windows.Forms.Button buttonSaveSignature;
        private System.Windows.Forms.Button buttonAddSignature;
        private System.Windows.Forms.Button buttonHangingPatterns;
        private System.Windows.Forms.Button buttonAddToDefault;
        private System.Windows.Forms.Button buttonHangingLinks;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label labelOperation;
        private System.Windows.Forms.Label labelCurrentType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.CheckBox checkBoxFilters;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
    }
}
