﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;

using Store;
using IA.Sql;
using IA.Sql.DatabaseConnections;

namespace IA.Administration.SubControls.SignatureFile
{
    /// <summary>
    /// Класс - Дилог редактирования cигнатур файлов (Работа с базой данный SignatureFile)
    /// </summary>
    internal partial class MainControl : UserControl
    {
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        private string ObjectName = "Элемент управления - Редактирование сигнатур файлов";

        /// <summary>
        /// Список рабочих мест
        /// </summary>
        private List<string[]> lsWorkPlaces = null;
        /// <summary>
        /// Список новых правок
        /// </summary>
        private List<string> lsIDHistory = null;
        /// <summary>
        /// список рабочих мест с правками
        /// </summary>
        private List<string> lsWIDHistory = null;
        /// <summary>
        /// список рабочих мест для изменения цвета: накапливается между запросами
        /// </summary>
        private List<string> lsQIDHistory = new List<string>();
        /// <summary>
        /// таблица FileFormat
        /// </summary>
        private List<string[]> lsFileFormat = null;
        /// <summary>
        /// таблица Pattern
        /// </summary>
        private List<string[]> lsPattern = null;
        /// <summary>
        /// Список типов файлов по расширениям
        /// </summary>
        private List<string[]> lsExFileFormat = null;
        /// <summary>
        /// Временный список выбранных типов файлов
        /// </summary>
        private List<string[]> lsSelected = null;
        /// <summary>
        /// Временный список выбранных шаблонов
        /// </summary>
        private List<string[]> lsSelectedP = null;
        /// <summary>
        /// Код рабочего места
        /// </summary>
        private string WorkID = "0";
        /// <summary>
        /// Тип запроса к базе данных
        /// </summary>
        private int AnaType = 0;

        /// <summary>
        /// Таблица перекодировки enum тип содержимого -> в описание на русском 
        /// </summary>
        Dictionary<string, string> TypeOfFilesToDescription = new Dictionary<string, string>();

        /// <summary>
        /// Таблица перекодировки описание на русском -> в enum тип содержимого 
        /// </summary>
        Dictionary<string, string> DescriptionToTypeOfFiles = new Dictionary<string, string>();

        /// <summary>
        /// Получение аттрибута у Енума
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(Enum value)
        {
            DescriptionAttribute[] da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false));
            return da.Length > 0 ? da[0].Description : value.ToString();
        }

        /// <summary>
        /// Заполнение словарей Енум->Аттрибут и Аттрибут->Енум
        /// </summary>
        private void EnumAttributes()
        {
            this.TypeOfFilesToDescription.Clear();
            this.DescriptionToTypeOfFiles.Clear();

            foreach (enTypeOfContents en in Enum.GetValues(typeof(enTypeOfContents)))
            {
                TypeOfFilesToDescription.Add(en.ToString().Trim(), GetDescription(en).Trim());
                DescriptionToTypeOfFiles.Add(GetDescription(en).Trim(), en.ToString().Trim());
            }
        }

        /// <summary>
        /// Констркутор
        /// </summary>
        internal MainControl()
        {
            InitializeComponent();

            Name = "SignatureFile";
            Text = "Сигнатуры файлов";

            SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);

            //OnDatabaseConnect();

            ToolTip toolTip1 = new ToolTip();

            toolTip1.SetToolTip(this.checkBoxFilters, "Если отмечено, то запросы фильтруются \n" +
                                                       "по выделенныи типам содержимого \n" +
                                                       "и выделенным языкам программирования");
            toolTip1.SetToolTip(this.checkBoxAllWithDefault, "Если отмечено, то к результатам \n" +
                                                             "по выделенному рабочему месту\n"+
                                                             "добавляются результаты по Базовому рабочему месту");
            toolTip1.SetToolTip(this.buttonAllNames, "Выборка всех типов файлов, \n" +
                                                     "удовлетворяющих фильтрам \n" +
                                                     "и выделенным рабочим местам");
            toolTip1.SetToolTip(this.buttonAllExtentions, "Выборка всех расширений файлов, \n" +
                                                          "удовлетворяющих фильтрам \n" +
                                                          "и выделенным рабочим местам");
            toolTip1.SetToolTip(this.buttonDoubleNames, "Выборка не уникальных типов файлов,\n" +
                                                        "удовлетворяющих фильтрам и\n" +
                                                        "выделенным рабочим местам");
            toolTip1.SetToolTip(this.buttonDoubleExtentions, "Выборка не уникальных расширений файлов, \n" +
                                                             "удовлетворяющих фильтрам \n" +
                                                             "и выделенным рабочим местам");
            toolTip1.SetToolTip(this.buttonHasBinaryPattern, "Выборка всех типов файлов, \n" +
                                                     "удовлетворяющих фильтрам\n" +
                                                     "и выделенным рабочим местам,\n" +
                                                     "имеющим бинарные шаблоны");
            toolTip1.SetToolTip(this.buttonNoPatterns, "Выборка всех типов файлов, \n" +
                                                     "удовлетворяющих фильтрам\n" +
                                                     "и выделенным рабочим местам,\n" +
                                                     "не имеющим шаблонов");
            toolTip1.SetToolTip(this.buttonNoTypesNoPatterns, "Выборка всех типов файлов, \n" +
                                                     "удовлетворяющих фильтрам\n" +
                                                     "и выделенным рабочим местам,\n" +
                                                     "не имеющим шаблонов и типов содержимого");
            toolTip1.SetToolTip(this.buttonUnknownTypes, "Выборка всех типов файлов, \n" +
                                                     "удовлетворяющих фильтрам\n" +
                                                     "и выделенным рабочим местам,\n" +
                                                     "не имеющим типов содержимого");
            toolTip1.SetToolTip(this.buttonWorkPlaces, "Отобразить/скрыть рабочие места");
            toolTip1.SetToolTip(this.buttonDeleteWorkingPlace, "Удалить рабочее место");

            toolTip1.SetToolTip(this.buttonAddSignature, "Добавить новую запись \n" +
                                                         "в таблицу типов файлов");
            toolTip1.SetToolTip(this.buttonAddPattern, "Добавить новую запись \n" +
                                                       "в таблицу шаблонов");
            toolTip1.SetToolTip(this.buttonDeleteSignature, "Удалить запись из таблицы типов файлов\n" +
                                                            "и из базы данных (при наличии)");
            toolTip1.SetToolTip(this.buttonDeletePattern, "Удалить запись из таблицы шаблонов\n" +
                                                            "и из базы данных (при наличии)");
            toolTip1.SetToolTip(this.buttonSaveSignature, "Сохранить выделенную запись\n" +
                                                            "таблицы типов файлов в базе данных");
            toolTip1.SetToolTip(this.buttonSavePattern, "Сохранить выделенную запись\n" +
                                                         "таблицы шаблонов в базе данных");
            toolTip1.SetToolTip(this.buttonDeleteExceptDefault, "Удалить все записи из таблицы типов файлов\n"+
                                                                "и из базы данных (при наличии),\n"+
                                                                "кроме записей Базового рабочего места\n");
            toolTip1.SetToolTip(this.buttonMoveToDefault, "Заменить рабочее место\n" +
                                                          "выделенной записи таблицы типов файлов\n" +
                                                          "на Базовое рабочее место\n");
            toolTip1.SetToolTip(this.buttonAddToDefault,  "Добавить шаблоны выделенной записи таблицы типов файлов\n"+
                                                          "к записи, являющейся записью Базового рабочего места");
        }
        
        /// <summary>
        /// Очистка элемента управления
        /// </summary>
        private void Clear()
        {
            dataGridViewSignature.Rows.Clear();
            dataGridViewPattern.Rows.Clear();

            listBoxTypesOfFiles.Items.Clear();
            listBoxLanguages.Items.Clear();
            listBoxTypesOrExtentions.Items.Clear();
            listBoxWorkingPlaces.Items.Clear();

            labelOperation.Text = String.Empty;
            labelCurrentType.Text = String.Empty;

            checkBoxAllWithDefault.Checked = false;
        }

        /// <summary>
        /// Заполнение элемента управления информацией по умолчанию
        /// </summary>
        private void FillDefault()
        {
            this.Clear();

            var wh = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistory.Select(x => x[0]);
            var ff = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat.Select(x => x[0]);
            foreach (string s in wh)
            {
                if (!ff.Contains(s))
                {
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistoryRemove(new string[] { s });
                }
            }
        }

        /// <summary>
        /// Получить массив языков из enum языков
        /// </summary>
        /// <returns></returns>
        private string[] GetLanguages()
        {
            return Enum.GetNames(typeof(enLanguage));
        }

        /// <summary>
        /// Получть массив языков по маске
        /// </summary>
        /// <param name="mask"></param>
        /// <returns></returns>
        private string[] GetLanguagesByMask(ulong mask)
        {
            return ((enLanguage)mask).ToString().Split(',').Select(x => x.Trim()).ToArray();
        }

        /// <summary>
        /// Получить маску для выбранных языков
        /// </summary>
        /// <param name="listBox"></param>
        /// <returns></returns>
        private ulong GetLanguagesMask(ListBox listBox)
        {
            ulong mask = 0;
            foreach (string s in listBox.SelectedItems)
                mask |= (ulong)(enLanguage)Enum.Parse(typeof(enLanguage), s);
            return mask;
        }

        /// <summary>
        /// Получить массив типов содержимого файлов
        /// </summary>
        /// <returns></returns>
        private string[] GetTypesOfContents()
        {
            return Enum.GetNames(typeof(enTypeOfContents));
        }


        /// <summary>
        /// Получть массив типов содержимого файлов по маске
        /// </summary>
        /// <param name="mask"></param>
        /// <returns></returns>
        private string[] GetTypesOfContentsByMask(uint mask)
        {
            return ((enTypeOfContents)mask).ToString().Split(',').Select(x => x.Trim()).ToArray();
        }

        /// <summary>
        /// Получить маску для выбранных типов содержимого файлов
        /// </summary>
        /// <param name="listBox"></param>
        /// <returns></returns>
        private uint GetTypesOfContentsMask(ListBox listBox)
        {
            uint mask = 0;
            foreach (string s in listBox.SelectedItems)
                mask |= (uint)(enTypeOfContents)Enum.Parse(typeof(enTypeOfContents), DescriptionToTypeOfFiles[s]);
            return mask;
        }

        #region Кнопки
        /// <summary>
        /// Кнопка - Дубликаты имен
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDoubleNames_Click(object sender, EventArgs e)
        {
            try
            {
                labelOperation.Text = buttonDoubleNames.Text;
                AnaType = 1;
                FillData(AnaType);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Дубликаты расширений
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDoubleExtentions_Click(object sender, EventArgs e)
        {
            try
            {
                labelOperation.Text = buttonDoubleExtentions.Text;
                AnaType = 2;
                FillData(AnaType);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Все имена
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAllNames_Click(object sender, EventArgs e)
        {
            try
            {
                labelOperation.Text = buttonAllNames.Text;
                AnaType = 4;
                FillData(AnaType);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Все расширения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAllExtentions_Click(object sender, EventArgs e)
        {
            try
            {
                labelOperation.Text = buttonAllExtentions.Text;
                AnaType = 5;
                FillData(AnaType);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Нет типов и шаблонов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNoTypesNoPatterns_Click(object sender, EventArgs e)
        {
            try
            {
                labelOperation.Text = buttonNoTypesNoPatterns.Text;
                AnaType = 8;
                FillData(AnaType);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        
        /// <summary>
        /// Кнопка - Нет шаблонов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNoPatterns_Click(object sender, EventArgs e)
        {
            try
            {
                labelOperation.Text = buttonNoPatterns.Text;
                AnaType = 6;
                FillData(AnaType);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Неопределенные типы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUnknownTypes_Click(object sender, EventArgs e)
        {
            try
            {
                labelOperation.Text = buttonUnknownTypes.Text;
                AnaType = 3;
                FillData(AnaType);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Сохранить описание типа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSaveSignature_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewSignature.SelectedCells.Count > 0)
                {

                    int index = dataGridViewSignature.SelectedCells[0].RowIndex;
                    string ID = GetCellValue(dataGridViewSignature, index, 0);
                    string FFID = GetCellValue(dataGridViewSignature, index, 1);
                    string WID = GetCellValue(dataGridViewSignature, index, 2);
                    string TOF = GetCellValue(dataGridViewSignature, index, 3);
                    string DS = GetCellValue(dataGridViewSignature, index, 4).Trim();
                    string DL = GetCellValue(dataGridViewSignature, index, 5).Trim();
                    string EX = GetCellValue(dataGridViewSignature, index, 6).Trim();
                    string LG = GetCellValue(dataGridViewSignature, index, 7).Trim();
                    if (TOF == "0")
                    {
                        if (MessageBox.Show("Установите тип файла\nДля продолжения с пустым типом нажмите OK", "Вниание", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
                            return;
                    }
                    if (DS == String.Empty)
                    {
                        if (MessageBox.Show("Установите описание\nДля продолжения с пустым описанием OK", "Вниание", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
                            return;
                    }
                    EX = EX.Trim();

                    if (ID != String.Empty)
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatUpdate(new string[] { ID, FFID, WID, TOF, DS, DL, EX, LG });
                    else
                    {
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatInsert(new string[] { FFID, WID, TOF, DS, DL, EX, LG });
                    }
                    string savedItem = null;
                    if (AnaType == 2 || AnaType == 5)
                    {
                        if (!string.IsNullOrEmpty(EX))
                            savedItem = EX;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(DS))
                            savedItem = DS;
                    }
                    RefreshFrm(savedItem);
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Удалить описание типа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeleteSignature_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewSignature.SelectedCells.Count > 0)
                {
                    DialogResult dr = MessageBox.Show("Если вы действительно хотите удалить эту запись, нажмите OK", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    if (dr != DialogResult.OK)
                        return;

                    int index = dataGridViewSignature.SelectedCells[0].RowIndex;
                    string ID = GetCellValue(dataGridViewSignature, index, 0);
                    string FFID = GetCellValue(dataGridViewSignature, index, 1);
                    string WID = GetCellValue(dataGridViewSignature, index, 2);
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternDelete(new string[] { FFID, WID });
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatDelete(new string[] { ID });
                    if (lsQIDHistory.Contains(ID))
                    {
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistoryRemove(new string[] { ID });
                        lsQIDHistory.Remove(ID);
                    }
                    RefreshFrm(null);
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка/переключатель - Рабочие места/Все вместе
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonWorkPlaces_Click(object sender, EventArgs e)
        {
            try
            {
                listBoxWorkingPlaces.Items.Clear();
                listBoxTypesOrExtentions.Items.Clear();
                dataGridViewSignature.Rows.Clear();
                dataGridViewPattern.Rows.Clear();
                labelOperation.Text = String.Empty;
                labelCurrentType.Text = String.Empty;
                label5.Text = String.Empty;
                WorkID = "0";
                AnaType = 0;

                switch (buttonWorkPlaces.Text)
                {
                    case "Рабочие места":
                        lsWIDHistory = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WIDHistory;
                        lsWorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces;
                        foreach (string[] sa in lsWorkPlaces)
                            listBoxWorkingPlaces.Items.Add(sa[1]);
                        buttonWorkPlaces.Text = "Все вместе";
                        break;
                    case "Все вместе":
                        buttonWorkPlaces.Text = "Рабочие места";
                        break;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Удалить рабочее место
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeleteWorkingPlace_Click(object sender, EventArgs e)
        {
            try
            {
                int savesIndex = -1;
                if (listBoxWorkingPlaces.SelectedIndex != -1)
                {
                    savesIndex = listBoxWorkingPlaces.SelectedIndex;
                    WorkID = lsWorkPlaces[listBoxWorkingPlaces.SelectedIndex][0];
                    labelOperation.Text = WorkID;
                    listBoxTypesOrExtentions.Items.Clear();
                    dataGridViewSignature.Rows.Clear();
                    dataGridViewPattern.Rows.Clear();
                    labelOperation.Text = String.Empty;
                    labelCurrentType.Text = String.Empty;
                    int count = lsFileFormat.Where(x => x[2] == WorkID).Count();
                    if (count > 0)
                    {
                        Monitor.Log.Warning(ObjectName, "Нельзя удалить рабочее место, имеющее активные форматы", true);
                        return;
                    }
                    else
                    {
                        listBoxWorkingPlaces.Items.Clear();
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlacesDelete(new string[] { WorkID });
                        WorkID = "0";
                        AnaType = 0;
                        lsFileFormat = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat;
                        lsPattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern;
                        lsWorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces;
                        foreach (string[] sa in lsWorkPlaces)
                            listBoxWorkingPlaces.Items.Add(sa[1]);
                        if (savesIndex >= listBoxWorkingPlaces.Items.Count)
                            savesIndex = listBoxWorkingPlaces.Items.Count - 1;
                        listBoxWorkingPlaces.Text = listBoxWorkingPlaces.Items[savesIndex].ToString();
                    }
                }
                else
                    WorkID = "0";
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Перенести в Базовое рабочее место
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMoveToDefault_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewSignature.SelectedCells.Count > 0)
                {
                    int index = dataGridViewSignature.SelectedCells[0].RowIndex;
                    string ID = GetCellValue(dataGridViewSignature, index, 0);
                    string WID = GetCellValue(dataGridViewSignature, index, 2);
                    string FFID = GetCellValue(dataGridViewSignature, index, 1);
                    if (WID == "1")
                    {
                        Monitor.Log.Warning(ObjectName, "Данная запись уже находится в Базовом рабочем месте", true);
                        return;
                    }

                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternToDefault(new string[] { FFID, WID });
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatToDefault(new string[] { ID });
                    if (lsQIDHistory.Contains(ID))
                    {
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistoryRemove(new string[] { ID });
                        lsQIDHistory.Remove(ID);
                    }
                    string DS = GetCellValue(dataGridViewSignature, index, 4).Trim();
                    string EX = GetCellValue(dataGridViewSignature, index, 6).Trim();
                    string savedItem = null;
                    if (AnaType == 2 || AnaType == 5)
                    {
                        if (!string.IsNullOrEmpty(EX))
                            savedItem = EX;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(DS))
                            savedItem = DS;
                    }
                    RefreshFrm(savedItem);
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Удалить кроме Базового рабочего места
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeleteExceptDefault_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dr = MessageBox.Show("Если вы действительно хотите удалить эти записи, нажмите OK", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                if (dr != DialogResult.OK)
                    return;

                for (int i = 0; i < dataGridViewSignature.Rows.Count; i++)
                {
                    string ID = GetCellValue(dataGridViewSignature, i, 0);
                    string FFID = GetCellValue(dataGridViewSignature, i, 1);
                    string WID = GetCellValue(dataGridViewSignature, i, 2);
                    if (WID != "1")
                    {

                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternDelete(new string[] { FFID, WID });
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatDelete(new string[] { ID });
                        if (lsQIDHistory.Contains(ID))
                        {
                            ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistoryRemove(new string[] { ID });
                            lsQIDHistory.Remove(ID);
                        }

                    }

                }
                RefreshFrm(null);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Добавить шаблон
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddPattern_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewSignature.SelectedCells.Count > 0)
                {
                    int index = dataGridViewSignature.SelectedCells[0].RowIndex;
                    string ID = GetCellValue(dataGridViewSignature, index, 0);
                    if(ID==string.Empty)
                    {
                        Monitor.Log.Warning(ObjectName, "Для добавления шаблонов нужно сохранить новый тип", true);
                        return;
                    }
                    string FFID = GetCellValue(dataGridViewSignature, index, 1);
                    string WID = GetCellValue(dataGridViewSignature, index, 2);
                    dataGridViewPattern.Rows.Add(new string[] { String.Empty, FFID, WID, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty });
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Сохранить шаблон
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSavePattern_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewPattern.SelectedCells.Count > 0)
                {
                    int index = dataGridViewPattern.SelectedCells[0].RowIndex;
                    string ID = GetCellValue(dataGridViewPattern, index, 0);
                    string FFID = GetCellValue(dataGridViewPattern, index, 1);
                    string WID = GetCellValue(dataGridViewPattern, index, 2);
                    string TOF = GetCellValue(dataGridViewPattern, index, 3);
                    string PF = GetCellValue(dataGridViewPattern, index, 4);
                    string PFV = GetCellValue(dataGridViewPattern, index, 5);
                    string PS = GetCellValue(dataGridViewPattern, index, 6);
                    string PSV = GetCellValue(dataGridViewPattern, index, 7);
                    if (TOF.ToLower() != "binary" && TOF.ToLower() != "text" && TOF.ToLower() != "xml" && TOF.ToLower() != "executable")
                    {
                        Monitor.Log.Warning(ObjectName, "Неверный тип шаблона", true);
                        return;
                    }
                    switch (TOF.ToLower())
                    {
                        case "binary":
                            TOF = "Binary";
                            PF = "Begins";
                            PFV = PFV.ToUpper();
                            if (PS != String.Empty && PSV != String.Empty)
                            {
                                PS = "Ends";
                                PSV = PSV.ToUpper();
                            }
                            else
                            {
                                PS = String.Empty;
                                PSV = String.Empty;
                            }
                            break;
                        case "text":
                            TOF = "TEXT";
                            PF = "Contains";
                            PS = "Weight";
                            break;
                        case "xml":
                            TOF = "XML";
                            PF = "Contains";
                            PS = "Weight";
                            break;
                        case "executable":
                            TOF = "Executable";
                            PF = "Method";
                            PS = "CheckType";
                            break;
                    }

                    if (ID == String.Empty)
                    {
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternInsert(new string[] { FFID, WID, TOF, PF, PFV, PS, PSV });
                    }
                    else
                    {
                        ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternUpdate(new string[] { ID, FFID, WID, TOF, PF, PFV, PS, PSV });
                    }

                    string savedItem = null;
                    if (dataGridViewSignature.SelectedCells.Count > 0)
                    {
                        int index1 = dataGridViewSignature.SelectedCells[0].RowIndex;

                        string DS = GetCellValue(dataGridViewSignature, index1, 4).Trim();
                        string EX = GetCellValue(dataGridViewSignature, index1, 6).Trim();
                        if (AnaType == 2 || AnaType == 5)
                        {
                            if (!string.IsNullOrEmpty(EX))
                                savedItem = EX;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(DS))
                                savedItem = DS;
                        }
                    }
                    RefreshFrm(savedItem);
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Удалить шаблон
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDeletePattern_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewPattern.SelectedCells.Count > 0)
                {
                    DialogResult dr = MessageBox.Show("Если вы действительно хотите удалить эту запись, нажмите OK", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    if (dr != DialogResult.OK)
                        return;

                    int index = dataGridViewPattern.SelectedCells[0].RowIndex;
                    string ID = GetCellValue(dataGridViewPattern, index, 0);
                    if (ID == String.Empty)
                    {
                        dataGridViewPattern.Rows.RemoveAt(index);
                        return;
                    }

                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternDeleteID(new string[] { ID });
                    RefreshFrm(null);
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Добавить описание типа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddSignature_Click(object sender, EventArgs e)
        {
            try
            {
                if (WorkID == "0")
                    WorkID = "1";
                string FFID = (lsFileFormat.Select(x => int.Parse(x[1])).Max() + 1).ToString();
                dataGridViewSignature.Rows.Add(new string[] { String.Empty, FFID, WorkID, "0", String.Empty, String.Empty, String.Empty });
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Есть бинарные шаблоны
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonHasBinaryPattern_Click(object sender, EventArgs e)
        {
            try
            {
                labelOperation.Text = buttonHasBinaryPattern.Text;
                AnaType = 7;
                FillData(AnaType);
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Повисшие шаблоны
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonHangingPatterns_Click(object sender, EventArgs e)
        {
            try
            {
                Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                fm.Controls.Add(new FormHangingPatternControl(((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection)) { Dock = DockStyle.Fill });
                fm.ShowDialog();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Добавить шаблон к Базовомк рабочему месту
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddToDefault_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewSignature.SelectedCells.Count > 0)
                {
                    int index = dataGridViewSignature.SelectedCells[0].RowIndex;
                    string ID = GetCellValue(dataGridViewSignature, index, 0);
                    string WID = GetCellValue(dataGridViewSignature, index, 2);
                    string FFID = GetCellValue(dataGridViewSignature, index, 1);
                    if (WID == "1")
                    {
                        Monitor.Log.Error(ObjectName, "Данная запись уже находится в Базовом рабочем месте", true);
                        return;
                    }
                    List<string[]> IDs = new List<string[]>();

                    for (int i = 0; i < dataGridViewSignature.RowCount; i++)
                    {
                        string sid = GetCellValue(dataGridViewSignature, i, 0);
                        string swid = GetCellValue(dataGridViewSignature, i, 2);
                        IDs.Add(new string[] { sid, swid });
                    }
                    if (IDs.Where(x => x[1] == "1").Count() > 1)
                    {
                        if (MessageBox.Show("Присутствует несколько записей Базового рабочего места\nОперация будет выполнена с первой записью Базового рабочего места.\nЕсли согласны, нажмите OK", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
                            return;
                    }
                    else
                    {

                        if (IDs.Where(x => x[1] == "1").Count() == 0)
                        {
                            if (MessageBox.Show("Отсутствуют записи Базового рабочего места\nДействие эквивалентно перенести в Базовое рабочее место. Если согласны, нажмите OK", "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop) != DialogResult.OK)
                            {
                                return;
                            }
                            else
                            {

                                ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternToDefault(new string[] { FFID, WID });
                                ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormatToDefault(new string[] { ID });
                                string DS1 = GetCellValue(dataGridViewSignature, index, 4).Trim();
                                string EX1 = GetCellValue(dataGridViewSignature, index, 6).Trim();
                                string savedItem1 = null;
                                if (AnaType == 2 || AnaType == 5)
                                {
                                    if (!string.IsNullOrEmpty(EX1))
                                        savedItem1 = EX1;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(DS1))
                                        savedItem1 = DS1;
                                }
                                RefreshFrm(savedItem1);
                                return;
                            }
                        }
                    }
                    int indexDefault = IDs.IndexOf(IDs.Where(x => x[1] == "1").First());
                    string FFID1 = GetCellValue(dataGridViewSignature, indexDefault, 1);

                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).PatternCopyToDefault(new string[] { FFID, WID, FFID1 });
                    //sigFile.FileFormatToDefault(new List<string>() { ID });
                    string DS = GetCellValue(dataGridViewSignature, index, 4).Trim();
                    string EX = GetCellValue(dataGridViewSignature, index, 6).Trim();
                    string savedItem = null;
                    if (AnaType == 2 || AnaType == 5)
                    {
                        if (!string.IsNullOrEmpty(EX))
                            savedItem = EX;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(DS))
                            savedItem = DS;
                    }
                    RefreshFrm(savedItem);
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Кнопка - Повисшие ссылки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonHangingLinks_Click(object sender, EventArgs e)
        {
            try
            {
                Form fm = new Form() { StartPosition = FormStartPosition.CenterParent };
                fm.Controls.Add(new FormHangingListControl() { Dock = DockStyle.Fill });
                fm.ShowDialog();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException((sender as Button).Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        } 
        #endregion

        #region Списки
        /// <summary>
        /// Кнопка - Выбор рабочего места
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxWorkingPlaces_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listBoxWorkingPlaces.SelectedIndex != -1)
                {
                    WorkID = lsWorkPlaces[listBoxWorkingPlaces.SelectedIndex][0];
                    label5.Text = listBoxWorkingPlaces.Text;
                    listBoxTypesOrExtentions.Items.Clear();
                    dataGridViewSignature.Rows.Clear();
                    dataGridViewPattern.Rows.Clear();
                    labelOperation.Text = String.Empty;
                    labelCurrentType.Text = String.Empty;
                }
                else
                {
                    WorkID = "0";
                    label5.Text = String.Empty;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Выбор рабочего места", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Выделение элемента списка - Наименования типа файла/Расширения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxTypesOrExtentions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listBoxTypesOrExtentions.SelectedIndex != -1)
                {
                    labelCurrentType.Text = listBoxTypesOrExtentions.Text;
                    lsSelected = new List<string[]>();
                    dataGridViewSignature.Rows.Clear();
                    dataGridViewPattern.Rows.Clear();
                    List<string> listIDs = new List<string>();
                    switch (AnaType)
                    {
                        // Результат запроса - расширение
                        case 2:
                        case 5:
                            listIDs = lsExFileFormat.Where(x => x[6] == listBoxTypesOrExtentions.Items[listBoxTypesOrExtentions.SelectedIndex].ToString()).Select(x => x[0]).Distinct().ToList();
                            lsSelected = lsFileFormat.Where(x => listIDs.Contains(x[0])).ToList();
                            break;
                        // Результат запроса - наименование
                        default:
                            lsSelected = lsFileFormat.Where(x => x[4] == listBoxTypesOrExtentions.Items[listBoxTypesOrExtentions.SelectedIndex].ToString()).ToList();
                            break;
                    }
                    if (lsSelected.Where(x => lsIDHistory.Contains(x[0])).Count() > 0)
                    {
                        foreach (string s in lsSelected.Select(x => x[0]))
                            lsQIDHistory.Add(s);  // Добавляется в список очереди на смену цвета
                    }
                    //Заполнение таблицы типов файлов
                    foreach (string[] sa in lsSelected)
                        dataGridViewSignature.Rows.Add(sa);

                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Выбор наименования типа файла\\расширения", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Выделение элемента списка - типов содержимого файлов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxTypeOfFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewSignature.SelectedCells.Count > 0)
                {
                    int index = dataGridViewSignature.SelectedCells[0].RowIndex;
                    uint typeOfFile = GetTypesOfContentsMask(listBoxTypesOfFiles);
                    dataGridViewSignature.Rows[index].Cells[3].Value = typeOfFile.ToString();
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Выбор типа содержимого файлов", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Выделение элемента списка - языка программирования типа файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxLanguages_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewSignature.SelectedCells.Count > 0)
                {
                    int index = dataGridViewSignature.SelectedCells[0].RowIndex;
                    ulong nLanguage = GetLanguagesMask(listBoxLanguages);
                    dataGridViewSignature.Rows[index].Cells[7].Value = nLanguage.ToString();
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Выбор языка программирования", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Собственная прорисовка элемента списка (рабочие места) - для выделения цветом определенных строк по условию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxWorkingPlaces_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                if (e.Index == -1)
                    return;
                bool bColored = false;
                e.DrawBackground();
                if (lsWIDHistory == null || lsWIDHistory.Count() == 0)
                    bColored = false;
                else
                    bColored = (lsWorkPlaces.Where(x => x[1] == ((ListBox)sender).Items[e.Index].ToString() && lsWIDHistory.Contains(x[0])).Count() > 0);
                Color selColor = Color.Black;
                if (bColored)
                    selColor = Color.Red;
                e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(), e.Font, new SolidBrush(selColor), e.Bounds, StringFormat.GenericDefault);
                e.DrawFocusRectangle();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Отрисовка элемента списка рабочих мест", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Собственная прорисовка элемента списка (типы файлов или расширения) - для выделения цветом определенных строк по условию
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxTypesOrExtentions_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                if (e.Index == -1)
                    return;
                bool bColored = false;
                e.DrawBackground();
                switch (AnaType)
                {
                    case 0:
                        break;
                    case 2:
                    case 5:
                        bColored = (lsExFileFormat.Where(x => x[6] == ((ListBox)sender).Items[e.Index].ToString()).Where(x => lsIDHistory.Contains(x[0])).Count() > 0);
                        break;
                    default:
                        bColored = (lsFileFormat.Where(x => x[4] == ((ListBox)sender).Items[e.Index].ToString()).Where(x => lsIDHistory.Contains(x[0])).Count() > 0);
                        break;
                }
                Color selColor = Color.Black;
                if (bColored)
                    selColor = Color.Red;
                e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(), e.Font, new SolidBrush(selColor), e.Bounds, StringFormat.GenericDefault);
                e.DrawFocusRectangle();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Отрисовка элемента списка типов файлов\\расширений", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        } 
        #endregion

        #region Таблицы
        /// <summary>
        /// Изменение выделения ячейки таблицы сигнатур
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewSignature_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridViewPattern.Rows.Clear();
                if (dataGridViewSignature.SelectedCells.Count > 0)
                {
                    int index = dataGridViewSignature.SelectedCells[0].RowIndex;
                    string FileFormatID = GetCellValue(dataGridViewSignature, index, 1);
                    string SelectedWorkID = GetCellValue(dataGridViewSignature, index, 2);
                    lsSelectedP = lsPattern.Where(x => ((x[1] == FileFormatID) && (x[2] == SelectedWorkID))).ToList();
                    foreach (string[] sa in lsSelectedP)
                        dataGridViewPattern.Rows.Add(sa);
                    if (dataGridViewPattern.Rows.Count > 0)
                    {
                        dataGridViewPattern.Rows[0].Cells[3].Selected = false;
                        dataGridViewPattern.Rows[0].Cells[5].Selected = true;
                    }
                    this.listBoxTypesOfFiles.SelectedIndexChanged -= new System.EventHandler(this.listBoxTypeOfFiles_SelectedIndexChanged);
                    this.listBoxLanguages.SelectedIndexChanged -= new System.EventHandler(this.listBoxLanguages_SelectedIndexChanged);

                    listBoxTypesOfFiles.SelectedIndices.Clear();
                    listBoxLanguages.SelectedIndices.Clear();

                    uint nTypeOfFile;
                    if ((dataGridViewSignature.Rows[index].Cells[3].Value != null) && uint.TryParse(dataGridViewSignature.Rows[index].Cells[3].Value.ToString(), out nTypeOfFile))
                    {
                        string[] sa = GetTypesOfContentsByMask(nTypeOfFile).Select(x=>TypeOfFilesToDescription[x]).ToArray();
                        for (int i = 0; i < listBoxTypesOfFiles.Items.Count; i++)
                        {
                            if (sa.Contains(listBoxTypesOfFiles.Items[i].ToString()))
                            {
                                listBoxTypesOfFiles.SelectedIndices.Add(i);
                            }
                            else
                            {
                            }
                        }
                    }
                    ulong nLanguage;
                    if ((dataGridViewSignature.Rows[index].Cells[7].Value != null) && ulong.TryParse(dataGridViewSignature.Rows[index].Cells[7].Value.ToString(), out nLanguage))
                    {
                        string[] sa = GetLanguagesByMask(nLanguage);
                        for (int i = 0; i < listBoxLanguages.Items.Count; i++)
                        {
                            if (sa.Contains(listBoxLanguages.Items[i].ToString()))
                                listBoxLanguages.SelectedIndices.Add(i);
                        }
                    }


                    this.listBoxTypesOfFiles.SelectedIndexChanged += new System.EventHandler(this.listBoxTypeOfFiles_SelectedIndexChanged);
                    this.listBoxLanguages.SelectedIndexChanged += new System.EventHandler(this.listBoxLanguages_SelectedIndexChanged);


                    try
                    {
                        label4.Text = lsWorkPlaces.Where(x => x[0] == SelectedWorkID).Select(x => x[1]).First();
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Выбор элемента таблиц сигнатур", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Изменение выделения ячейки таблицы шаблонов типов файлов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPattern_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dataGridViewPattern.SelectedCells.Count > 0)
                {
                    int Rowindex = dataGridViewPattern.SelectedCells[0].RowIndex;
                    int Columnindex = dataGridViewPattern.SelectedCells[0].ColumnIndex;
                    if (Columnindex == 5)
                    {
                        string sBegin = GetCellValue(dataGridViewPattern, Rowindex, Columnindex - 1);
                        if (sBegin.ToLower() == "begins")
                        {
                            string sres = String.Empty;
                            string stringValue;
                            string sShablon = GetCellValue(dataGridViewPattern, Rowindex, Columnindex);
                            for (int i = 0; i < sShablon.Length / 2; i++)
                            {
                                string hex = sShablon.Substring(i * 2, 2);
                                int value = 0;
                                try
                                {
                                    value = Convert.ToInt32(hex, 16);
                                }
                                catch (Exception)
                                {
                                }
                                if (value == 0)
                                    stringValue = "?";
                                else
                                    stringValue = Char.ConvertFromUtf32(value);
                                sres += stringValue;
                            }
                            label3.Text = sres;
                        }
                        else
                            label3.Text = String.Empty;
                    }
                    if (Columnindex == 7)
                    {
                        string sBegin = GetCellValue(dataGridViewPattern, Rowindex, Columnindex - 1);
                        if (sBegin.ToLower() == "ends")
                        {
                            string sres = String.Empty;
                            string stringValue;
                            string sShablon = GetCellValue(dataGridViewPattern, Rowindex, Columnindex);
                            for (int i = 0; i < sShablon.Length / 2; i++)
                            {
                                string hex = sShablon.Substring(i * 2, 2);
                                int value = Convert.ToInt32(hex, 16);
                                if (value == 0)
                                    stringValue = "?";
                                else
                                    stringValue = Char.ConvertFromUtf32(value);
                                sres += stringValue;
                            }
                            label3.Text = sres;
                        }
                        else
                        {
                            label3.Text = String.Empty;
                        }
                    }
                }
                else
                    label3.Text = String.Empty;
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Выбор элемента таблицы шаблонов типов файлов", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Изменение содержимого ячейки таблицы шаблонов типов файлов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPattern_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int row = e.RowIndex;
                int column = e.ColumnIndex;
                if (row < 0 || column < 0)
                    return;
                string value = String.Empty;
                if (dataGridViewPattern.Rows[row].Cells[column].Value != null)
                    value = dataGridViewPattern.Rows[row].Cells[column].Value.ToString();

                switch (column)
                {
                    case 3:
                        switch (value)
                        {
                            case "Binary":
                                dataGridViewPattern.Rows[row].Cells[4].Value = "Begins";
                                dataGridViewPattern.Rows[row].Cells[6].Value = "Ends";
                                break;
                            case "TEXT":
                            case "XML":
                                dataGridViewPattern.Rows[row].Cells[4].Value = "Contains";
                                dataGridViewPattern.Rows[row].Cells[6].Value = "Weight";
                                break;
                            case "Executable":
                                dataGridViewPattern.Rows[row].Cells[4].Value = "Method";
                                dataGridViewPattern.Rows[row].Cells[6].Value = "CheckType";
                                break;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Изменение элемента таблицы шаблонов типов файлов", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        } 
        #endregion

        /// <summary>
        /// Заполнение listBoxTypesOrExtentions результатами запроса
        /// </summary>
        /// <param name="anatype"></param>
        /// <param name="bClearQueryHistory"></param>
        private void FillData(int anatype, bool bClearQueryHistory = true)
        {
            dataGridViewSignature.Rows.Clear();
            dataGridViewPattern.Rows.Clear();
            listBoxTypesOrExtentions.Items.Clear();
            if (bClearQueryHistory)
            {
                foreach (string s in lsQIDHistory)
                    ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkHistoryRemove(new string[] { s });
                lsQIDHistory = new List<string>();
            }

            lsFileFormat = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat;
            lsPattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern;
            lsWorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces;
            lsIDHistory = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).IDHistory;
            if (WorkID != "0")
            {
                if (checkBoxAllWithDefault.Checked)
                {
                    List<string> lsFFSHD = lsFileFormat.Where(x => x[2] == WorkID).Select(x => x[4]).ToList();
                    lsFileFormat = lsFileFormat.Where(x => lsFFSHD.Contains(x[4]) && (x[2] == WorkID || x[2] == "1")).ToList();
                }
                else
                {
                    lsFileFormat = lsFileFormat.Where(x => x[2] == WorkID).ToList();
                    lsPattern = lsPattern.Where(x => x[2] == WorkID).ToList();
                }
            }

            // Если отмечено "Использовать фильтры", то запросы фильтруются по выделенныи типам содержимого и выделенным языкам программирования
            if (checkBoxFilters.Checked) 
            {
                uint tcm = GetTypesOfContentsMask(listBoxTypesOfFiles);
                uint itmp;
                if (tcm != 0 && tcm != 0x3f)
                {
                    lsFileFormat = lsFileFormat.Where(x => ((uint.TryParse(x[3], out itmp) ? uint.Parse(x[3]) : 0) & tcm) != 0).ToList();
                }
                ulong lcm = GetLanguagesMask(listBoxLanguages);
                ulong ltmp;
                if (lcm != 0)
                {
                    lsFileFormat = lsFileFormat.Where(x => ((ulong.TryParse(x[7], out ltmp) ? ulong.Parse(x[7]) : 0) & lcm) != 0).ToList();
                }
            }


            List<string> lsShortName = new List<string>();
            List<string> lsDoubles = new List<string>();
            lsExFileFormat = new List<string[]>();
            List<string> lsExtension = new List<string>();
            List<string> listIDs = new List<string>();
            string sperv = String.Empty;

            switch (anatype)
            {
                case 1:
                    lsShortName = lsFileFormat.OrderBy(x => x[4]).Select(x => x[4]).ToList();
                    foreach (string s in lsShortName)
                    {
                        if (s == sperv && !lsDoubles.Contains(s))
                            lsDoubles.Add(s);
                        sperv = s;
                    }
                    foreach (string s in lsDoubles)
                        listBoxTypesOrExtentions.Items.Add(s);
                    break;
                case 2:
                    foreach (string[] sa in lsFileFormat)
                    {
                        if (sa[6] == String.Empty)
                        {
                            lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], String.Empty });
                        }
                        else
                        {
                            string ss6 = sa[6].ToLower();
                            
                            if (ss6.IndexOf('.') >= 0)
                            {
                                if (ss6.StartsWith("."))
                                    ss6 = ss6.Substring(1);
                                if (ss6.EndsWith("."))
                                    ss6 = ss6.Substring(0, ss6.Length - 1);

                                string[] sb = ss6.Split('.');
                                foreach (string s in sb)
                                {
                                    lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], s });
                                }
                            }
                            else
                                lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], ss6 });
                        }
                    }
                    lsExtension = lsExFileFormat.OrderBy(x => x[6]).Select(x => x[6]).ToList();
                    foreach (string s in lsExtension)
                    {
                        if (s == sperv && !lsDoubles.Contains(s))
                            lsDoubles.Add(s);
                        sperv = s;
                    }
                    foreach (string s in lsDoubles)
                        listBoxTypesOrExtentions.Items.Add(s);
                    break;
                case 3:
                    lsShortName = lsFileFormat.Where(x => x[3] == "0").OrderBy(x => x[4]).Select(x => x[4]).Distinct().ToList();
                    foreach (string s in lsShortName)
                        listBoxTypesOrExtentions.Items.Add(s);
                    break;
                case 4:
                    lsShortName = lsFileFormat.OrderBy(x => x[4]).Select(x => x[4]).Distinct().ToList();
                    foreach (string s in lsShortName)
                        listBoxTypesOrExtentions.Items.Add(s);
                    break;
                case 5:
                    foreach (string[] sa in lsFileFormat)
                    {
                        if (sa[6] == String.Empty)
                        {
                            lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], String.Empty });
                        }
                        else
                        {
                            string ss6 = sa[6].ToLower();
                            if (ss6.IndexOf('.') >= 0)
                            {
                                if (ss6.StartsWith("."))
                                    ss6 = ss6.Substring(1);
                                if (ss6.EndsWith("."))
                                    ss6 = ss6.Substring(0, ss6.Length - 1);

                                string[] sb = ss6.Split('.');
                                foreach (string s in sb)
                                {
                                    lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], s });
                                }
                            }
                            else
                                lsExFileFormat.Add(new string[] { sa[0], sa[1], sa[2], sa[3], sa[4], sa[5], ss6 });
                        }
                    }
                    lsExtension = lsExFileFormat.OrderBy(x => x[6]).Select(x => x[6]).Distinct().ToList();
                    foreach (string s in lsExtension)
                        listBoxTypesOrExtentions.Items.Add(s);
                    break;
                case 6:
                    listIDs = lsPattern.Select(x => x[1]).Distinct().ToList();
                    List<string> lsNotIDs = lsFileFormat.Where(x => !listIDs.Contains(x[1])).Distinct().Select(x => x[1]).ToList();
                    lsShortName = lsFileFormat.Where(x => lsNotIDs.Contains(x[1])).OrderBy(x => x[4]).Select(x => x[4]).Distinct().ToList();
                    foreach (string s in lsShortName)
                        listBoxTypesOrExtentions.Items.Add(s);
                    break;
                case 7:
                    listIDs = lsPattern.Where(x => x[4].ToLower() == "begins").Select(x => x[1]).Distinct().ToList();
                    lsShortName = lsFileFormat.Where(x => listIDs.Contains(x[1])).OrderBy(x => x[4]).Select(x => x[4]).Distinct().ToList();
                    foreach (string s in lsShortName)
                        listBoxTypesOrExtentions.Items.Add(s);
                    break;
                case 8:
                    listIDs = lsPattern.Select(x => x[1]).Distinct().ToList();
                    lsShortName = lsFileFormat.Where(x => x[3] == "0" && !listIDs.Contains(x[1])).OrderBy(x => x[4]).Select(x => x[4]).Distinct().ToList();
                    foreach (string s in lsShortName)
                        listBoxTypesOrExtentions.Items.Add(s);
                    break;
                case 9:
                    listIDs = lsPattern.Where(x => x[6].ToLower() == "ends").Select(x => x[1]).Distinct().ToList();
                    lsShortName = lsFileFormat.Where(x => listIDs.Contains(x[1])).OrderBy(x => x[4]).Select(x => x[4]).Distinct().ToList();
                    foreach (string s in lsShortName)
                        listBoxTypesOrExtentions.Items.Add(s);
                    break;
            }
        }

        /// <summary>
        /// Обновить данные формы (после сохранения)
        /// </summary>
        /// <param name="savedItem"></param>
        private void RefreshFrm(string savedItem)
        {
            dataGridViewSignature.Rows.Clear();
            dataGridViewPattern.Rows.Clear();
            int selectedIndex = (listBoxTypesOrExtentions.SelectedIndex < 0) ? 0 : listBoxTypesOrExtentions.SelectedIndex;
            listBoxTypesOrExtentions.Items.Clear();
            FillData(AnaType, false);
            if (!string.IsNullOrEmpty(savedItem))
            {
                selectedIndex = 0;
                for (selectedIndex = 0; selectedIndex < listBoxTypesOrExtentions.Items.Count; selectedIndex++)
                {
                    if (listBoxTypesOrExtentions.Items[selectedIndex].ToString() == savedItem)
                        break;
                }
            }
            if (selectedIndex < listBoxTypesOrExtentions.Items.Count)
                listBoxTypesOrExtentions.SelectedIndex = selectedIndex;
            else
                listBoxTypesOrExtentions.SelectedIndex = listBoxTypesOrExtentions.Items.Count - 1;
        }

        /// <summary>
        /// Получить значение ячейки таблицы
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        string GetCellValue(DataGridView dataGridView, int row, int column)
        {
            object value = dataGridView.Rows[row].Cells[column].Value;

            return value == null ? String.Empty : value.ToString();
        }
       
        /// <summary>
        /// Действие при подключении к базе данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Заполняем элемент управления значениями по умолчанию
            this.FillDefault();

            //Элемент управления доступен
            this.Enabled = true;

            EnumAttributes();

            //Заполняем элемент управления значениями по умолчнию
            List<string> lsTypeOfPatterns = new List<string>() { "TEXT", "Binary", "XML", "Executable" };
            DataGridViewComboBoxColumn cmb = dataGridViewPattern.Columns[3] as DataGridViewComboBoxColumn;
            foreach (string s in lsTypeOfPatterns)
                cmb.Items.Add(s);

            foreach (string s in GetTypesOfContents())
                listBoxTypesOfFiles.Items.Add(TypeOfFilesToDescription[s]);

            foreach (string s in this.GetLanguages())
                listBoxLanguages.Items.Add(s);

            lsFileFormat = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).FileFormat;
            lsPattern = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).Pattern;
            lsWorkPlaces = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WorkPlaces;
            lsIDHistory = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).IDHistory;
            lsWIDHistory = ((SignatureFileDatabaseConnection)SqlConnector.SignatureDB.DatabaseConnection).WIDHistory;
            checkBoxAllWithDefault.Checked = true;
        }

        /// <summary>
        /// Действие при потере соединения с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
            //Очищаем элемент управления
            this.Clear();

            //Элемент управления доступен
            this.Enabled = false;
        }
    }
}
