﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using IA.Extensions;
using IA.Sql;
using IA.Controls.MainSettings;

namespace IA.Administration
{
    /// <summary>
    /// Класс, реализующий главную форму административного модуля
    /// </summary>
    internal partial class MainForm : Form, Monitor.Log.Interface
    {
        #region Константы
        /// <summary>
        /// Наименование экземпляра класса
        /// </summary>
        internal static string ObjectName = "Основная форма";

        /// <summary>
        /// Версия базы данных для корректной работы приложения
        /// </summary>
        internal const uint DatabaseVersion = 8;
        #endregion

        /// <summary>
        /// Всевозможные действия для кнопок и пунктов меню
        /// </summary>
        private Dictionary<string, Action> actions = new Dictionary<string, Action>();

        /// <summary>
        /// Конструктор
        /// </summary>
        internal MainForm()
        {
            //Инициализация основных компонентов
            InitializeComponent();

            SqlConnector.MainForm = this;
            SqlConnector.DllKnowledgeDB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);

            //Заполняем список всевозможных действий и устанавливаем их в значения по умолчанию
            this.Actions_Initialization();

            //Инициализация всех элементов управления формы
            this.MainForm_Initialization();

            //Добавляем обработчики
            this.Shown += (sender, e) => this.MainForm_Shown();
        }

        /// <summary>
        /// Обработчик - Что нужно сделать при первом отображении формы
        /// </summary>
        private void MainForm_Shown()
        {
            //Если это первый запуск приложения
            if (this.MainSettings_IsFirstRun())
            {
                //Отображаем предупреждение
                Monitor.Log.Warning(
                    ObjectName,
                    "Это первый запуск приложения <" + Program.ApplicationName + ">.\n" +
                        "Пожалуйста, произведите настройку приложения.\n" +
                            "Укажите параметры соединения с SQL сервером.",
                    true
                );
            }
            else
            {
                //Проверяем настройки приложения
                if (this.MainSettings_Check())
                    return;

                //Если приложение настроено некорректно, отображаем предупреждение
                Monitor.Log.Warning(
                    ObjectName,
                    "Приложение настроено некорректно.\n" +
                        "Пожалуйста, произведите корректную настройку приложения.\n" +
                            "Укажите параметры соединения с SQL сервером.",
                    true
                );
            }

            //Отображаем окно с настройками приложения
            this.MainSettings_Show();
        }

        /// <summary>
        /// Формирвоание основной архитектуры главного элемента управления
        /// </summary>
        /// <returns></returns>
        private void MainForm_Initialization()
        {
            SqlConnector.MainForm = this;
            SqlConnector.IADB.SynchronizeWithDatabaseConnection(OnDatabaseConnect, OnDatabaseDisconnect);

            //Устанавливаем заголовок формы
            this.Text = Program.ApplicationName;

            //Регистрируем основную форму в качествве слушателя Лога
            Monitor.Log.Register(this);

            #region Меню
            //Инициализация элемента управления - Меню
            IA.Controls.MenuControl menuControl = new IA.Controls.MenuControl()
            {
                Anchor = AnchorStyles.Left | AnchorStyles.Right
            };
            ToolStripMenuItem root = menuControl.Add_MenuItem(null, "Common", "Общее");
            EnumLoop<Actions.Global>.ForEach(operation => menuControl.Add_MenuItem(root, this.actions[operation.FullName()]));
            #endregion

            var controls = new Control[] {
                new SubControls.Users.MainControl(),
                new SubControls.SignatureFile.MainControl(),
#if !CERTIFIED
                new SubControls.DllEdit.MainControl(),
#endif
            };

            //Инициализация элемента управления - Основная панель с вкладками
            var main_tabControl = new TabControl() { Dock = DockStyle.Fill };
            controls.ForEach(subControl =>
            {
                subControl.Dock = DockStyle.Fill;
                TabPage tab = new TabPage(subControl.Text);
                tab.Controls.Add(subControl);
                main_tabControl.TabPages.Add(tab);
            });

            //Инициализация элемента управления - Основная таблица
            TableLayoutPanel main_tableLayoutPanel = new TableLayoutPanel() { Dock = DockStyle.Fill };
            main_tableLayoutPanel.RowCount = 2;
            main_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 30));
            main_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
            main_tableLayoutPanel.ColumnCount = 1;
            main_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
            main_tableLayoutPanel.Controls.Add(menuControl, 0, 0);
            main_tableLayoutPanel.Controls.Add(main_tabControl, 0, 1);

            this.Controls.Add(main_tableLayoutPanel);
        }

        #region Действия
        /// <summary>
        /// Инициализация всеовзможных действий
        /// </summary>
        private void Actions_Initialization()
        {
            //Глобальные операции
            EnumLoop<Actions.Global>.ForEach(operation =>
            {
                switch (operation)
                {
                    case Actions.Global.SETTINGS:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Global_Button_Settings, ActionHandler = this.MainSettings_Show_ActionHandler });
                        break;
                    case Actions.Global.CREATESTANDALONEDB:
                        actions.Add(operation.FullName(), new Action() { Key = operation.FullName(), Text = operation.Description(), Image = Properties.Resources.Global_Button_Settings, ActionHandler = this.DBUnload_ActionHandler, Enabled = false, Visible = false }); //disable this action because of migration functionality to main form UnloadStorage action.
                        break;
                    default:
                        throw new Exception(Actions.Exceptions.UNKNOWN_GLOBAL_ACTION.Description());
                }
            });
        }


        #endregion

        #region Основные настройки приложения
        /// <summary>
        /// Заданы ли настройки? Если нет, значит это первый запуск приложения.
        /// </summary>
        /// <returns></returns>
        private bool MainSettings_IsFirstRun()
        {
            try
            {
                return String.IsNullOrEmpty(IA.Settings.SQLServer.Name) &&
                     String.IsNullOrEmpty(IA.Settings.SQLServer.Login) &&
                         String.IsNullOrEmpty(IA.Settings.SQLServer.Password);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при проверке наличия основных настроек приложения.", ex);
            }
        }

        /// <summary>
        /// Применить основные настройки приложения
        /// </summary>
        private void MainSettings_Apply()
        {
            try
            {
                //Устанавливаем параметры для подключения к MS SQL серверу
                IA.Settings.SQLServer.Set(IA.Settings.SQLServer.Name,
                                            IA.Settings.SQLServer.Login,
                                                IA.Settings.SQLServer.Password,
                                                    IA.Settings.SQLServer.IsWindowsAuthentication);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при применении основных настроек приложения.", ex);
            }
        }

        #region Обработчики действий
        /// <summary>
        /// Обработчик - Нажали на кнопку\меню отображения основных настроек приложения
        /// </summary>
        private void MainSettings_Show_ActionHandler(object sender, Action action)
        {
            try
            {
                //Отображаем настройки
                this.MainSettings_Show();
            }
            catch (Exception ex)
            {
                //Обрабатываем ошибку
                ex.HandleAsActionHandlerException(action.Text, (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        private void DBUnload_ActionHandler(object sender, Action action)
        {
            using (var dialog = new OpenFileDialog() { ValidateNames = true, CheckFileExists = false, DefaultExt = ".sdf" })
            {
                dialog.Title = "Выберите имя файла локальной БД. К имени будет добавлено название БД.";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string file = dialog.FileName;
                    try
                    {
                        Sql.SqlConnector.DumpToLocalDB(file);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "Create file error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Отобразить основные настройки приложения
        /// </summary>
        private void MainSettings_Show()
        {
            try
            {
                //Формируем диалог с настройкими
                using (Form dialog = new Form()
                {
                    StartPosition = FormStartPosition.CenterParent,
                    Text = Actions.Global.SETTINGS.Description(),
                    Width = 700,
                    Height = 420,
                })
                {
                    //Создаём элемент управления - Основные настройки приложения
                    MainSettingsControl mainSettingsControl = new MainSettingsControl
                        (MainSettingsControl.SettingsElements.sqlServerSettingsSubControl)
                    {
                        Dock = DockStyle.Fill,
                    };

                    //Создаём элемент управления - Панель с кнопками
                    IA.Controls.Common.ButtonsPanel buttonsPanel = new IA.Controls.Common.ButtonsPanel()
                    {
                        Dock = DockStyle.Fill
                    };

                    Dictionary<string, EventHandler> buttons = new Dictionary<string, EventHandler>
                    {
                        { "Сохранить",  (sender, e) => this.MainSettings_Save_Click(dialog, mainSettingsControl)},
                        { "Проверить",  (sender, e) => this.MainSettings_SingleCheck_Click(mainSettingsControl) },
                        { "Отмена",     delegate(object sender, EventArgs e)
                                        {
                                            dialog.DialogResult = DialogResult.Cancel;
                                            dialog.Close();
                                        }
                        }
                    };
                    buttonsPanel.SetButtons(buttons);

                    //Создаём основную таблицу диалога
                    TableLayoutPanel dialog_main_tableLayoutPanel = new TableLayoutPanel()
                    {
                        ColumnCount = 1,
                        Dock = DockStyle.Fill,
                        RowCount = 2,
                    };
                    dialog_main_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
                    dialog_main_tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, IA.Controls.Common.ButtonsPanel.ButtonHeight + 20));
                    dialog_main_tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

                    dialog_main_tableLayoutPanel.Controls.Add(mainSettingsControl, 0, 0);
                    dialog_main_tableLayoutPanel.Controls.Add(buttonsPanel, 0, 1);

                    dialog.Controls.Add(dialog_main_tableLayoutPanel);
                    dialog.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при отображении основных настроек приложения.", ex);
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню разовой проверки настроек
        /// </summary>
        /// <param name="mainSettingsControl">Окно основных настроек приложения</param>
        private void MainSettings_SingleCheck_Click(IA.Controls.MainSettings.MainSettingsControl mainSettingsControl)
        {
            try
            {
                string message;

                //Провера на разумность
                if (mainSettingsControl == null)
                    throw new Exception("Элемент управления основных настроек приложения не определён.");

                //Проверяем настройки и в зависимости от результата выводим сообщение
                if (mainSettingsControl.Check(out message))
                    Monitor.Log.Information(ObjectName, message, true);
                else
                    Monitor.Log.Error(ObjectName, message, true);

            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Разовая проверка основных настроек приложения", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }

        /// <summary>
        /// Проверка настроек приложения
        /// </summary>
        /// <returns>Результат проверки настроек приложения</returns>
        private bool MainSettings_Check()
        {
            try
            {
                //Применяем текущие настройки
                this.MainSettings_Apply();

                //Изначально результат проверки положительный
                bool ret = true;

                //Проверям подключение к базам данных
                Sql.SqlExtensions.TestDatabaseConnectionsWithWaitWindow(
                    new Sql.SQLConnection[]
                        {
                           SqlConnector.SignatureDB,
                            SqlConnector.IADB,
#if !CERTIFIED
                            SqlConnector.DllKnowledgeDB
#endif
                        },
                    delegate (Sql.DatabaseConnection[] databaseConnections)
                    {
                        //Результат проверки отрицительный
                        ret = false;

                        //Проверка на разумность
                        if (databaseConnections == null)
                            throw new ArgumentNullException("databaseConnections", "Список соединений с базами данных не определён.");

                        //Формируем сообщение об ошибке   
                        string message = "Отсутствует соединение со следующими базами данных:";

                        foreach (string databaseName in databaseConnections.Select(db => db.DatabaseName))
                            message += "\n- " + databaseName;

                        //Отображаем ошибку
                        Monitor.Log.Error(ObjectName, message, true);
                    }
                );

                return ret;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new Exception("Ошибка при проверке основных настроек приложения.", ex);
            }
        }

        /// <summary>
        /// Обработчик - Нажали на кнопку\меню сохранения основных настройки приложения
        /// </summary>
        /// <param name="mainSettingsDialog">диалоговое окно основных настроек приложения</param>
        /// <param name="mainSettingsControl">Окно основных настроек приложения</param>
        private void MainSettings_Save_Click(Form mainSettingsDialog, IA.Controls.MainSettings.MainSettingsControl mainSettingsControl)
        {
            try
            {
                //Провера на разумность
                if (mainSettingsControl == null)
                    throw new Exception("Элемент управления основных настроек приложения не определён.");

                //Сохраняем введённые настройки
                mainSettingsControl.Save();

                //Закрываем диалог
                mainSettingsDialog.DialogResult = DialogResult.OK;
                mainSettingsDialog.Close();

                //Проверяем настройки приложения
                this.MainSettings_Check();
            }
            catch (Exception ex)
            {
                //Обрабатываем исключение
                ex.HandleAsActionHandlerException("Сохранение основных настроек приложения", (message) => Monitor.Log.Error(ObjectName, message, true));
            }
        }
        #endregion

        /// <summary>
        /// Появилось соединение с базой данных
        /// </summary>
        protected void OnDatabaseConnect()
        {
            //Проверяем версию базы данных
            this.Database_Version_Check();
        }

        /// <summary>
        /// Пропало соединение с базой данных
        /// </summary>
        protected void OnDatabaseDisconnect()
        {
        }

        /// <summary>
        /// Проверка версии базы данных, с которой работает приложение.
        /// Предполагается, что соденинение с базой данных уже установлено ранее.
        /// </summary>
        /// <returns></returns>
        private void Database_Version_Check()
        {
            try
            {
                //Текущая версия БД
                uint currentDatabaseVersion;

                //Если версия базы данных для корректной работы не совпадает с текущей
                if (!(SqlConnector.IADB.DatabaseConnection as IA.Sql.DatabaseConnections.IADatabaseConnection).CheckVersion(MainForm.DatabaseVersion, out currentDatabaseVersion))
                    throw new Exception("Текущая версия базы данных не совпадает с версией для корректной работы приложения.\n" +
                                            "Текущая версия базы данных: " + currentDatabaseVersion + ".\n" +
                                                "Версия базы данных для корректной работы: " + MainForm.DatabaseVersion + ".");
            }
            catch (Exception ex)
            {
                //Формируем сообщение
                string message = new string[]
                {
                    "Ошибка при проверке версии базы данных.",
                    ex.ToFullMultiLineMessage(),
                    Constants.Message.ApplicationWillBeClosed
                }.ToMultiLineString();

                //Формируем ошибку и завершаем приложение
                Monitor.Log.Error(ObjectName, message, true);

                //Принудительно закрываем приложение
                Environment.Exit(1);
            }
        }

        #region Monitor.Log.Interface
        /// <summary>
        /// Получили сообщение из монитора
        /// </summary>
        /// <param name="message"></param>
        public void AddMessage(Monitor.Log.Message message)
        {
            //Если не требуется оповещение, ничего не делаем
            if (!message.IsNotificationNeeded)
                return;

            //Обрабатываем сообщение в зависимости от его типа
            switch (message.Type)
            {
                case Monitor.Log.Message.enType.INFORMATION:
                    MessageBox.Show(message.Text, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Monitor.Log.Message.enType.WARNING:
                    MessageBox.Show(message.Text, "Предупреждение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                case Monitor.Log.Message.enType.ERROR:
                    MessageBox.Show(message.Text, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                default:
                    throw new Exception("Получено сообщение из монитора неизвестного типа.");
            }
        }
        #endregion
    }
}
