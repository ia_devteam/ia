﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Store;
using Store.Table;
using IA.Extensions;

namespace IA.Plugins.Analyses.RestoreSources
{

    /// <summary>
    /// Класс настроек плагина
    /// </summary>
    internal static class PluginSettings
    {
        /// <summary>
        /// Наименование плагина
        /// </summary>
        internal const string Name = "Восстановление исходных файлов после очистки";

        /// <summary>
        /// Иденитификатор плагина
        /// </summary>
        internal const uint ID = Store.Const.PluginIdentifiers.RESTORE_SOURCES;

        #region Settings

        /// <summary>
        /// Использование проектных файлов Microsoft Visual Studio для восстановления исходных кодов на языках C, C++, C#
        /// </summary>
        internal static bool useVisualStudioProjects;

        /// <summary>
        /// Использование Make файлов для восстановления исходных кодов на языках C, C++
        /// </summary>
        internal static bool useMakeFiles;

        /// <summary>
        /// Использование проектных файлов Borland Delphi для восстановления исходных кодов на языках C, C++, Pascal
        /// </summary>
        internal static bool useDelphiProjects;

        /// <summary>
        /// Интерактивное восстановление по типам файлов
        /// </summary>
        internal static bool useIdentifyFileTypes;

        ///// <summary>
        ///// Полный путь до папки с текстами
        ///// </summary>
        //internal static string dirtyFolder;

        #endregion
        /// <summary>
        /// Наименования задач плагина
        /// </summary>
        internal enum Tasks
        {
            [Description("Восстановление исходных файлов из проектов Visual Studio")]
            RSTORE_FROM_VISUALSTUDIO_PROJECTS,
            [Description("Восстановление исходных файлов из Make файлов")]
            RSTORE_FROM_MAKE_FILES,
            [Description("Восстановление исходных файлов из проектов Delphi")]
            RSTORE_FROM_DELPHI_PROJECTS,
            [Description("Восстановление исходных файлов в интерактивном режиме")]
            RSTORE_INTERACTIVE
        };
    }


    /// <summary>
    /// Основной класс плагина
    /// </summary>
    public class RestoreSources : IA.Plugin.Interface
    {
        /// <summary>
        /// Хранилище
        /// </summary>
        Storage storage;

        Settings settings;

        #region plugin inerface

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ID"/>
        /// </summary>
        public uint ID
        {
            get
            {
                return PluginSettings.ID;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Name"/>
        /// </summary>
        public string Name
        {
            get
            {
                return PluginSettings.Name;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Capabilities"/>
        /// </summary>
        public Plugin.Capabilities Capabilities
        {
            get
            {
                return Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW |
                    //Plugin.Capabilities.RESULT_WINDOW |
                                    Plugin.Capabilities.REPORTS |
                                        Plugin.Capabilities.RERUN;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SimpleSettingsWindow"/>
        /// </summary>
        public System.Windows.Forms.UserControl SimpleSettingsWindow
        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.CustomSettingsWindow"/>
        /// </summary>
        public System.Windows.Forms.UserControl CustomSettingsWindow
        {
            get
            {
                settings = new Settings();
                return settings;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.RunWindow"/>
        /// </summary>
        public System.Windows.Forms.UserControl RunWindow
        {
            get { return null; }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.ResultWindow"/>
        /// </summary>
        public System.Windows.Forms.UserControl ResultWindow
        {
            get { return null; }
        }

        public List<Monitor.Tasks.Task> Tasks
        {
            get
            {
                // 06.04.2015. Гарифулин М. Метод переписан.
                // Метод описан плохо:
                // 1. По логике на выходе может быть любая комбинация данных настроек (на форме Settings они реализованы CheckBox, а не RadioButton), а тут возвращается только одна настройка.
                // 2. Пустые настройки (не выбран ни один режим) тоже должны быть валидными.
                // 3. В старой логике ветка else относилась только к последнему if, хотя по общей логике процесса должны была быть общей для всех 4х проверок.

                var result = new List<Monitor.Tasks.Task>();

                if (PluginSettings.useDelphiProjects)
                    result.Add(PluginSettings.Tasks.RSTORE_FROM_DELPHI_PROJECTS.ToMonitorTask(Name));

                if (PluginSettings.useMakeFiles)
                    result.Add(PluginSettings.Tasks.RSTORE_FROM_MAKE_FILES.ToMonitorTask(Name));

                if (PluginSettings.useVisualStudioProjects)
                    result.Add(PluginSettings.Tasks.RSTORE_FROM_VISUALSTUDIO_PROJECTS.ToMonitorTask(Name));

                if (PluginSettings.useIdentifyFileTypes)
                    result.Add(PluginSettings.Tasks.RSTORE_INTERACTIVE.ToMonitorTask(Name));

                return result;
                //if (PluginSettings.useDelphiProjects)
                //{
                //    return new List<Monitor.Tasks.Task>()
                //    {
                //        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.RSTORE_FROM_DELPHI_PROJECTS.Description(),1)
                //    };
                //}
                //if (PluginSettings.useMakeFiles)
                //{
                //    return new List<Monitor.Tasks.Task>()
                //    {
                //        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.RSTORE_FROM_MAKE_FILES.Description(),1)
                //    };
                //}
                //if (PluginSettings.useVisualStudioProjects)
                //{
                //    return new List<Monitor.Tasks.Task>()
                //    {
                //        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.RSTORE_FROM_VISUALSTUDIO_PROJECTS.Description(),1)
                //    };
                //}
                //if (PluginSettings.useIdentifyFileTypes)
                //{
                //    return new List<Monitor.Tasks.Task>()
                //    {
                //        new Monitor.Tasks.Task(PluginSettings.Name, PluginSettings.Tasks.RSTORE_INTERACTIVE.Description(),1)
                //    };
                //}
                //else
                //{
                //    throw new NotImplementedException();
                //}

            }
        }

        public List<Monitor.Tasks.Task> TasksReport
        {
            get
            {
                return null;
            }
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.Initialize"/>
        /// </summary>
        /// <param name="storage"><see cref="IA.Plugin.Interface.Initialize"/></param>
        public void Initialize(Storage storage)
        {
            this.storage = storage;

            PluginSettings.useVisualStudioProjects = Properties.Settings.Default.useVisualStudioProjects;
            PluginSettings.useMakeFiles = Properties.Settings.Default.useMakeFiles;
            PluginSettings.useDelphiProjects = Properties.Settings.Default.useDelphiProjects;
            PluginSettings.useIdentifyFileTypes = Properties.Settings.Default.useIdentifyFileTypes;
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.LoadSettings"/>
        /// </summary>
        /// <param name="settingsBuffer"><see cref="IA.Plugin.Interface.LoadSettings"/></param>
        public void LoadSettings(IBufferReader settingsBuffer)
        {
            settingsBuffer.GetBool(ref PluginSettings.useVisualStudioProjects);
            settingsBuffer.GetBool(ref PluginSettings.useMakeFiles);
            settingsBuffer.GetBool(ref PluginSettings.useDelphiProjects);
            settingsBuffer.GetBool(ref PluginSettings.useIdentifyFileTypes);
        }

        /// <summary> 
        /// <see cref="IA.Plugin.Interface.SetSimpleSettings"/>
        /// </summary>
        /// <param name="settingsString"><see cref="IA.Plugin.Interface.SetSimpleSettings"/></param>
        public void SetSimpleSettings(string settingsString)
        {
            //Задаём настройки, не показываемые пользователю
            PluginSettings.useVisualStudioProjects = true;
            PluginSettings.useMakeFiles = false;
            PluginSettings.useDelphiProjects = true;
            PluginSettings.useIdentifyFileTypes = false;

            //Задаём настройки, переданные через строку
            switch (settingsString)
            {
                case "":
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public void SaveSettings(IBufferWriter settingsBuffer)
        {
            //Сохраняем настройки в XML-файл настроек приложения
            Properties.Settings.Default.useVisualStudioProjects = PluginSettings.useVisualStudioProjects;
            Properties.Settings.Default.useMakeFiles = PluginSettings.useMakeFiles;
            Properties.Settings.Default.useDelphiProjects = PluginSettings.useDelphiProjects;
            Properties.Settings.Default.useIdentifyFileTypes = PluginSettings.useIdentifyFileTypes;
            Properties.Settings.Default.Save();

            //Сохраняем настройки в буфер настроек
            settingsBuffer.Add(PluginSettings.useVisualStudioProjects);
            settingsBuffer.Add(PluginSettings.useMakeFiles);
            settingsBuffer.Add(PluginSettings.useDelphiProjects);
            settingsBuffer.Add(PluginSettings.useIdentifyFileTypes);

        }

        public void SaveResults()
        {

        }

        public bool IsSettingsCorrect(out string message)
        {
            message = string.Empty;

            if (settings != null)
                settings.Save();

            // 06.04.2015 Гарифулин М. Пустые настройки плагина - валидные. Он должен штатно "отработать", даже если ничего не задано
            //if (PluginSettings.useDelphiProjects == false && PluginSettings.useMakeFiles == false && PluginSettings.useVisualStudioProjects == false
            //    && PluginSettings.useIdentifyFileTypes == false)
            //{
            //    message = "Пустые настройки плагина";
            //    return false;
            //}

            return true;
        }

        public void Run()
        {
            string sourcePath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_ORIGINAL);
            
            List<string> listCompileFiles = new List<string>();

            List<string> listSourceFiles = storage.files.EnumerateFiles(enFileKind.anyFile).Select(x => x.FullFileName_Original).ToList();
            List<string> listClearFiles = storage.files.EnumerateFiles(enFileKind.anyFile)
                .Where(x => !x.FileEssential.Equals(enFileEssential.SUPERFLUOUS))
                .Select(x => x.FullFileName_Original).ToList();

            if (PluginSettings.useVisualStudioProjects)
                listCompileFiles.AddRange(GetVisualStudioProjectFiles(storage, listCompileFiles));
            if (PluginSettings.useMakeFiles)
                listCompileFiles.AddRange(GetMakeFileFiles(sourcePath, listCompileFiles));
            if (PluginSettings.useDelphiProjects)
                listCompileFiles.AddRange(GetDelphiProjectFiles(sourcePath, listCompileFiles));
            if (PluginSettings.useIdentifyFileTypes)
                listCompileFiles.AddRange(GetAllFiles(sourcePath, listCompileFiles));
            List<string> restoringFiles = listCompileFiles.Except(listClearFiles).ToList();
        }

        public void GenerateReports(string reportsDirectoryPath)
        {
            //Проверка на разумность
            if (this.storage == null)
                throw new Exception("Хранилище не определено.");
        }

        public void StopRunning()
        {

        }
        #endregion
        #region plugin functions

        List<string> GetVCXFilesPaths(IA.SlnResolver.VCX.ProjectBase project)
        {
            List<string> filePaths = new List<string>();
            foreach (var file in project.SourceFiles)
            {
                filePaths.Add(file.Location);
            }
            foreach (var file in project.ResourceFiles)
            {
                filePaths.Add(file.Location);
            }
            foreach (var file in project.HeaderFiles)
            {
                filePaths.Add(file.Location);
            }
            return filePaths;
        }

        List<string> GetVCFilesPaths(IA.SlnResolver.VC.ProjectBase project)
        {
            List<string> filePaths = new List<string>();
            foreach (var file in project.SourceFiles)
            {
                filePaths.Add(file.Location);
            }
            foreach (var file in project.ResourceFiles)
            {
                filePaths.Add(file.Location);
            }
            foreach (var file in project.HeaderFiles)
            {
                filePaths.Add(file.Location);
            }
            return filePaths;
        }

        List<string> GetCSFilesPaths(IA.SlnResolver.CS.ProjectBase project)
        {
            List<string> filePaths = new List<string>();
            foreach (var file in project.SourceFiles)
            {
                filePaths.Add(file.Location);
            }
            foreach (var file in project.ResourceFiles)
            {
                filePaths.Add(file.Location);
            }
            return filePaths;
        }

        List<string> GetVisualStudioProjectFiles(Storage storage, List<string> listClearFiles)
        {
            string sourcePath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_ORIGINAL);
            string clearPath = storage.WorkDirectory.GetSubDirectoryPathWithUnpack(WorkDirectory.enSubDirectories.SOURCES_CLEAR);
            sourcePath = IOController.DirectoryController.CanonizePath(sourcePath);
            clearPath = IOController.DirectoryController.CanonizePath(clearPath);

            List<string> vcproj = System.IO.Directory.EnumerateFiles(clearPath, "*.vcproj", System.IO.SearchOption.AllDirectories).Select(g => g.Replace(clearPath, sourcePath)).ToList();
            List<string> vcxproj = System.IO.Directory.EnumerateFiles(clearPath, "*.vcxproj", System.IO.SearchOption.AllDirectories).Select(g => g.Replace(clearPath, sourcePath)).ToList();
            List<string> csproj = System.IO.Directory.EnumerateFiles(clearPath, "*.csproj", System.IO.SearchOption.AllDirectories).Select(g => g.Replace(clearPath, sourcePath)).ToList();

            List<string> restore = new List<string>();
            foreach (string file in vcxproj)
            {
                try
                {
                    IA.SlnResolver.VCX.VCXProjectReader reader = new IA.SlnResolver.VCX.VCXProjectReader();
                    var proj = reader.ReadProject(file);

                    restore.AddRange(GetVCXFilesPaths(proj));
                }
                catch (Exception ex)
                {
                }
            }
            foreach (string file in vcproj)
            {
                try
                {
                    IA.SlnResolver.VC.VCProjectReader reader = new IA.SlnResolver.VC.VCProjectReader();
                    var proj = reader.ReadProject(file);

                    restore.AddRange(GetVCFilesPaths(proj));
                }
                catch (Exception ex)
                {
                }
            }
            foreach (string file in csproj)
            {
                try
                {
                    IA.SlnResolver.CS.CSProjectReader reader = new IA.SlnResolver.CS.CSProjectReader();
                    var proj = reader.ReadProject(file);

                    restore.AddRange(GetCSFilesPaths(proj));
                }
                catch (Exception ex)
                {
                }
            }

            //List<string> projects = IA.SlnResolver.SolutionVS.GetAllProjects(sourcePath);

            return restore;
        }
        List<string> GetMakeFileFiles(string sourcePath, List<string> listClearFiles)
        {
            return new List<string>();
        }
        List<string> GetDelphiProjectFiles(string sourcePath, List<string> listClearFiles)
        {
            return new List<string>();
        }
        List<string> GetAllFiles(string sourcePath, List<string> listClearFiles)
        {
            return new List<string>();
        }
        #endregion
    }

}
