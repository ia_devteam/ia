﻿using System.Windows.Forms;

namespace IA.Plugins.Analyses.RestoreSources
{
    public partial class Settings : UserControl, IA.Plugin.Settings
    {
        public Settings()
        {
            InitializeComponent();
         }
        /// <summary>
        /// Сохранение настроек
        /// </summary>
        public void Save()
        {   

            PluginSettings.useVisualStudioProjects = checkBoxVisualStudioProject.Checked;
            PluginSettings.useMakeFiles = checkBoxMakeFile.Checked;
            PluginSettings.useDelphiProjects = checkBoxDelphiProject.Checked;
            PluginSettings.useIdentifyFileTypes = checkBoxIdentifyFileTypes.Checked;

        }

      
    }
}
