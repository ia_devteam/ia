﻿namespace IA.Plugins.Analyses.RestoreSources
{
    partial class Settings
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.checkBoxIdentifyFileTypes = new System.Windows.Forms.CheckBox();
            this.checkBoxDelphiProject = new System.Windows.Forms.CheckBox();
            this.checkBoxMakeFile = new System.Windows.Forms.CheckBox();
            this.checkBoxVisualStudioProject = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBoxIdentifyFileTypes
            // 
            resources.ApplyResources(this.checkBoxIdentifyFileTypes, "checkBoxIdentifyFileTypes");
            this.checkBoxIdentifyFileTypes.Name = "checkBoxIdentifyFileTypes";
            this.checkBoxIdentifyFileTypes.UseVisualStyleBackColor = true;           
            // 
            // checkBoxDelphiProject
            // 
            resources.ApplyResources(this.checkBoxDelphiProject, "checkBoxDelphiProject");
            this.checkBoxDelphiProject.Name = "checkBoxDelphiProject";
            this.checkBoxDelphiProject.UseVisualStyleBackColor = true;
            // 
            // checkBoxMakeFile
            // 
            resources.ApplyResources(this.checkBoxMakeFile, "checkBoxMakeFile");
            this.checkBoxMakeFile.Name = "checkBoxMakeFile";
            this.checkBoxMakeFile.UseVisualStyleBackColor = true;
            // 
            // checkBoxVisualStudioProject
            // 
            resources.ApplyResources(this.checkBoxVisualStudioProject, "checkBoxVisualStudioProject");
            this.checkBoxVisualStudioProject.Name = "checkBoxVisualStudioProject";
            this.checkBoxVisualStudioProject.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.checkBoxVisualStudioProject, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxMakeFile, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxDelphiProject, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBoxIdentifyFileTypes, 0, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // Settings
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Settings";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxIdentifyFileTypes;
        private System.Windows.Forms.CheckBox checkBoxDelphiProject;
        private System.Windows.Forms.CheckBox checkBoxMakeFile;
        private System.Windows.Forms.CheckBox checkBoxVisualStudioProject;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

    }
}
