#!/usr/bin/python python

# Requirements: Python version 2 or 3
#
# This script is dedicated to update version, build IA, 
# make distrib archive, push changes to remote repo, apply tag,
# upload distrib
# 
# launch: MasterIA.py <repo> <path> <branch> <url>
# <repo> - local: /c/PROJECTS/Project_IA/ ИЛИ remote: ​https://UserName@bitbucket.org/ia_devteam/ia.git
# <path> - рабочий каталог, задается как привычный Win-путь: c:\PROJECTS\Project_IA_mirror\
# <branch> - feature/IA_Improve
# <url> - путь до удаленного репозитория в формате: ​https://UserName:password@bitbucket.org/ia_devteam/ia.git

# import modules used here -- sys is a very standard one
import sys
import codecs
import pickle           # serialize a list to disk
import subprocess       # for launch bat file
import os
import datetime
import tempfile
import os.path
import re               # regex
import os
import errno
import ntpath

## pathSln - full path 'IABuild.sln'
## create bat file and return path
def produceBat(IApth):
    f_cnt = [] # the content of bat for build IA
    f_cnt.append('call "%vs140comntools%vsvars32.bat"\n')
    f_cnt.append('MSBuild ' + IApth + ' /p:Configuration=Release\n')
    fBat = tempfile.mktemp() + '.bat'
    with open(fBat, 'wt') as fp: 
        for f_line in f_cnt:
            fp.write(f_line)
    return fBat
## execute bat, FIXME: error handling ness
def executeBat(pthBat):
    p = subprocess.Popen(pthBat, shell=True) #stdout = subprocess.PIPE)
    stdout, stderr = p.communicate()
    #os.system(pthBat)
    if (p.returncode != 0):
        print ('Error building project IABuild.sln') # is 0 if success
        sys.exit(1)
    os.system('del ' + pthBat)
## compress to zip builded version
## pth7z - path for 7z (gets from IA)
## IAbin - path for IA\Bin
## wFold - working dir for zip
def compress7z(pth7z, IAbin, wFold, verIA):
    cmd = pth7z + " a "
    now = datetime.datetime.now()
    spl = str.split(wFold, "\\")
    n0 = ""
    iT = len(spl)-1
    while len(spl[iT]) == 0:        
        iT = iT - 1
    n0 = spl[iT]
    n2 = n0 + "_arch"
    wFldNew = wFold.replace(n0, n2)
    os.system("rmdir /s /q " + wFldNew) 
    os.system("mkdir " + wFldNew)
    d = wFldNew + "ia_distrib_" + verIA + "_" + str(datetime.date.today()) 
    d = d + "_" + str(now.hour) + "-" + str(now.minute) + ".zip"
    os.system('del ' + d)
    cmd = cmd + d + " " + IAbin + "*"
    os.system(cmd)
    return d
## clear folder for Test, *.pdb, *.xml (except 'config.xml')
def washFolder(IAbin, IAtst): 
    os.system("rmdir /s /q " + IAtst)    
    os.system("del /s /q " + IAbin + "*.pdb")
    os.system("for /R %f in (*.xml) do if %~nxf!=config.xml del \"%f\"")
## get version from 'IAVersion.ver'
def getIAversion(verPath):
    if os.path.isfile(verPath): 
        with open(verPath, 'r') as c_file:
            return c_file.read()    #.replace('\n', '')
    return ""
## update IA version before(!) build 
def updateIAversion(verPath, verNum, rootFold):
    lVerIA = verNum.split('.')
    lVerIA[3] = str(int(lVerIA[3]) + 1)
    verNum = lVerIA[0] + '.' + lVerIA[1] + '.' + lVerIA[2] + '.' +  lVerIA[3]
    print('searching for AssemblyInfo.cs')
    lf = list_filesall(rootFold, "", ["AssemblyInfo.cs"])
    for fname in lf:
        print(fname)
        fCont = ""
        with codecs.open(fname, 'r', 'utf-8') as f:
            fCont = f.read()
        fCont = re.sub("\d+\.\d+\.\d+\.\d+", verNum, fCont)    
        with codecs.open(fname, 'w', 'utf-8') as w_file:
            w_file.write(fCont)
    with codecs.open(verPath, 'w', 'utf-8') as v_file:
        v_file.write(verNum)
    return verNum     

## walk through all folders and subfolders
# and return full file list
# 'root_folder'
# 'ex_list' - extensions for search
# 'ex_name' - exact file name
def list_filesall(root_folder, ex_list, ex_names):
    f_list = []
    skey = 0        # search all files        
    if len(ex_list) > 0:
        skey = 1    # search by extensions
    if len(ex_names) > 0:
        skey = 2    # search by exact file name
    # FIXME: if len(ex_list) > 0 and len(ex_name) > 0:
    for path, subdirs, files in os.walk(root_folder):
        for name in files:
            if skey == 0:   # all files
                f_list.append(os.path.join(path, name))
            elif skey == 1: # by extension
                for c_ex in ex_list:                
                    if fnmatch(name, c_ex):
                        f_list.append(os.path.join(path, name))
            elif skey == 2: # by exact name
                for c_name in ex_names:
                    if name == c_name:
                        f_list.append(os.path.join(path, name))
    return f_list
## working with git via cmd
def git_commands(gitIA, wrkIA, brhIA, urlIA):
    os.system("rmdir /s /q " + wrkIA.rstrip('\\'))
    make_sure_path_exists(wrkIA)

    cd = "cd " + gitIA

    cmd = "git remote set-url origin " + urlIA
    os.system(cd + " && " + cmd) 


    cmd = cd + " && git pull -v" 
    os.system(cmd) 


    cmd = "git clone --progress -b " + brhIA + " " + gitIA + " " + wrkIA        
    os.system(cmd) 

    cd = "cd " + gitIA

    cmd = cd + " && " + "git checkout " + brhIA
    os.system(cmd) 


## correct work path anyway
def correct_wdir(wrkIA):
    fname = tempfile.mktemp()
    IABuild = ""
    cmd = "cd " + wrkIA + " & dir /b /s IABuild.sln > " + fname
    os.system(cmd)
    with codecs.open(fname, 'r', 'utf-8') as f:
        IABuild = f.read()
        f.close    
    IABuild = IABuild.rstrip('\r\n')
    wrkIA = IABuild[0:len(IABuild)-len('Sources\IABuild.sln')]
    return wrkIA
## 
def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
## commit & push our changes by script
def commit_push(gitIA, wrkIA, brhIA, urlIA, verIA, urlAr, msgAD):
    cd = "cd " + wrkIA + " && "
    cmd = cd + "git remote set-url origin " + urlIA
    cmd = cmd + " && git add . -v && "
    cmd = cmd + "git commit --message=\"" + str(verIA) + " - autobuild " + msgAD + " " + urlAr + " \""
    cmd = cmd + " && " + "git tag " + str(verIA) + "_auto"
    os.system(cmd)
    os.system(cd + " git push && git push --tags")
## post our arch to bitbucket.org
def post_download(pthAr, e, w):
    upath = "https://bitbucket.org/ia_devteam/ia/downloads/" + ntpath.basename(pthAr)
    sh = "curl -u " + e + ":" + w
    sh = sh + " -X POST https://api.bitbucket.org/2.0/repositories/ia_devteam/ia/downloads/ -F files=@"
    sh = sh + '\"' + pthAr + '\"' 
    sh = "#!/bin/bash\n" + sh
    f_sh = tempfile.mktemp() + ".sh"
    with codecs.open(f_sh, 'w', 'utf-8') as w_file:
        w_file.write(sh)
    gb = "C:\\Program Files (x86)\\Git\\git-bash.exe"
    if os.path.isfile(gb) == False:
        gb = "C:\\Program Files\\Git\\git-bash.exe"
        if os.path.isfile(gb) == False:
            print('Could not find git-bash.exe')
            gb = ""
    if len(gb) > 0:
        cmd = "\"" + gb + "\" " + "\"" + f_sh + "\""
        p = subprocess.Popen(cmd, shell=True)
        os.system('del ' + f_sh)
    # pass    
    return upath
# print's info for user and exit
def help_print():
    print('Not valid arguments number')
    print('launch this way:\n    MasterIA.py <repo> <folder> [<branch>]')
    print('for all local paths use UNIX style')
    print('    example: /c/PROJECTS/Project_IA/')
    print('path for remote repo for example:')
    print('     https://UserName@bitbucket.org/ia_devteam/ia.git')
    print('[<branch>] optional, use this way for example:')
    print('    feature/IA_Improve')        
    sys.exit(1)
# extract url ew
def parse_ew(urlIA):
    e = ""
    w = ""
    h = "https://"
    a = "@"
    i = urlIA.find(h) + len(h)
    j = urlIA.find(a)
    s = urlIA[i:j]
    p = s.split(':')
    if len(p) == 2:
        e = p[0]
        w = p[1] 
    return e,w
# read all input args from file
def read_file_args(f_path):
    if os.path.isfile(f_path): 
        with open(f_path, 'r') as c_file:
            rf = c_file.readlines()
            return rf[0].strip(), rf[1].strip(), rf[2].strip(), rf[3].strip(), rf[4].strip()
    return ["", "", "", "", ""]
## sys.argv[1] - repopath OR path to file with all args. Each one on new line.
# local: /c/PROJECTS/Project_IA/ remote: https://UserName@bitbucket.org/ia_devteam/ia.git
## sys.argv[2] - working dir
## sys.argv[3] - branch, ex. feature/IA_Improve
## sys.argv[4] - remote repo to push
## sys.argv[5] - url to upload file
## sys.argv[6] - message for commit from user (optional)
def main():
    print('Starting work at ' + str(datetime.datetime.now()))
    gitIA = ""                          # 'c:\PROJECTS\Project_IA\'
    wrkIA = ""                          # working directory
    brhIA = ""                          # branch name for clone
    urlIA = ""                          # https://login:password@bitbucket.org/ia_devteam/ia.git
    msgAD = ""                          # message for commit from input
    ##
    IApth = '\Sources\IABuild.sln'      # full path for solutions 'IABuild.sln'
    IAbin = "\Binaries" + "\\"          # 'c:\PROJECTS\Project_IA\Binaries\'    
    IAtst = '\Binaries\Tests' + "\\"    # '\Binaries\Tests'
    pth7z = "\\Externals\\7z\\7z.exe"   # 
    pthVr = '\Sources\IAVersion.ver'    # path
    verIA = ""                          # IAVersion.ver
    pthAr = ""                          # full path for arch with IA for export
    urlAr = ""                          # url of arch in bitbucket.org/ia_devteam/ia/downloads/
    if len(sys.argv) == 2:
       gitIA, wrkIA, brhIA, urlIA, msgAD = read_file_args(sys.argv[1])
    elif len(sys.argv) > 5:
        gitIA = sys.argv[1]
        wrkIA = sys.argv[2]
        brhIA = sys.argv[3]
        urlIA = sys.argv[4]
        if len(sys.argv) > 6: 
            msgAD = sys.argv[5]
    else:
        help_print
    # working region
    e, w = parse_ew(urlIA)
    # working with git via cmd
    git_commands(gitIA, wrkIA, brhIA, urlIA)   #
    #wrkIA = correct_wdir(wrkIA)    # NOT IN USE
    # make paths
    IAtst  = wrkIA + IAtst
    IApth  = wrkIA + IApth
    IAbin  = wrkIA + IAbin    
    pthVr  = wrkIA + pthVr
    pth7z  = wrkIA + pth7z
    verIA  = getIAversion(pthVr)
    if (len(verIA) == 0):
        print("No IAVersion.ver in project folder")
        sys.exit(1)   
    washFolder(IAbin, IAtst)
    pthBat = produceBat(IApth)
    verIA = updateIAversion(pthVr, verIA, wrkIA + "Sources\\")  #
    os.system("rmdir /s /q " + IAbin.rstrip('\\')) #
    executeBat(pthBat)  #    
    washFolder(IAbin, IAtst) ## clear Binaries Folder    
    pthAr = compress7z(pth7z, IAbin, wrkIA, verIA) ## compress to zip-arch
    urlAr = post_download(pthAr, e, w)    #
    commit_push(gitIA, wrkIA, brhIA, urlIA, verIA, urlAr, msgAD) # commit & push our changes by script    
    pass
    print('\nFinished work at ' + str(datetime.datetime.now()))
    print ('Good Bye from IA master')    

# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
    main()
    
   
    