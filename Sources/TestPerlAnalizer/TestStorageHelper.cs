﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;
using TestUtils;

namespace TestPerlAnalizer
{
    [TestClass]
    public class TestStorageHelper
    {

        private TestUtilsClass testUtils;

        Storage storage;

        private TestContext testContextInstance;


        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            string str = System.IO.Directory.GetCurrentDirectory();

            storage = testUtils.Storage_CreateAndOpen();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            testUtils.Storage_Remove(ref storage);
        }


        [TestMethod]
        public void TestMethod1()
        {
            String prefix = storage.appliedSettings.FileListPath;
        }

        [TestMethod]
        public void TestMethod2()
        {
        }
    }
}
