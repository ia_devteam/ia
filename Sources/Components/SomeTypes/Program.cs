﻿using System;
using System.Linq;
using System.IO;

namespace SomeTypes
{
    class Program
    {
        /// <summary>
        /// Кансольная программа, вызываемая плагином Идентификация файлов
        /// </summary>
        /// <param name="args">две строки: имя файла и код типа файла (например, "SVN")</param>
        /// <returns>0 - тип файла соответствует коду типа файла, иначе - нет</returns>
        static int Main(string[] args)
        {
            if (args.Length < 2)
                return -1;
            string fileName = args[0];
            string checkName = args[1];
            string[] sa = GetCheckTypes();
            if (!sa.Contains(checkName))
                return -2;

            // Для каждого кода типа файла свой обработчик
            switch (SetCheckTypes(checkName))
            {
                case enCheckTypes.None:
                    return -3;
                case enCheckTypes.SVN:
                    return CheckSVN(fileName);
                default:
                    return -4;
            }
        }

        /// <summary>
        /// Коды типов файлов
        /// </summary>
        [Flags]
        public enum enCheckTypes : ulong
        {
            None = 0x0,
            SVN = 0x1
        }

        /// <summary>
        /// Перечень кодов типов файлов
        /// </summary>
        /// <returns></returns>
        public static string[] GetCheckTypes()
        {
            return Enum.GetNames(typeof(enCheckTypes));
        }

        /// <summary>
        /// Получить код по имени
        /// </summary>
        /// <param name="cht"></param>
        /// <returns></returns>
        public static enCheckTypes SetCheckTypes(string cht)
        {
            return (enCheckTypes)Enum.Parse(typeof(enCheckTypes), cht);
        }

        /// <summary>
        /// Обработка типов файлов SVN
        /// </summary>
        /// <param name="fileName">Полный путь к файлу</param>
        /// <returns></returns>
        public static int CheckSVN(string fileName)
        {
            //1. В пути есть .svn
            string[] sa = fileName.Split(new char[] { '\\', '/' }).Select(x => x.ToLower()).ToArray();
            int index = 0;
            bool bFound = false;
            for (; index < sa.Count(); index++)
            {
                if (sa[index] == ".svn")
                {
                    bFound = true;
                    break;
                }
            }
            if (!bFound)
                return 1;
            index++;
            string basePath = sa.Take(index).Aggregate((a, b) => a + "\\" + b);

            //2. В базовой папке есть нужные файлы

            DirectoryInfo di = new DirectoryInfo(basePath);
            FileInfo[] files = di.GetFiles("*.*", SearchOption.TopDirectoryOnly);
            string[] fNames = files.Select(x => x.Name.ToLower()).ToArray();
            if (!fNames.Contains("entries") || !fNames.Contains("format") || !fNames.Contains("wc.db"))
                return 2;

            //3. Файлы нужного формата
            string wc = System.Text.Encoding.ASCII.GetString(ReadFile(basePath + "\\" + "wc.db", "SQLite format".Length));
            if (wc != "SQLite format")
                return 3;
            int num;
            int num1;
            {
                StreamReader sr = new StreamReader(basePath + "\\" + "entries");
                string s = sr.ReadLine();
                if (s == null)
                    return 4;
                if (!int.TryParse(s, out num))
                    return 5;
                s = sr.ReadLine();
                if (s != null)
                    return 6;
            }
            {
                StreamReader sr = new StreamReader(basePath + "\\" + "format");
                string s = sr.ReadLine();
                if (s == null)
                    return 7;
                if (!int.TryParse(s, out num1))
                    return 8;
                s = sr.ReadLine();
                if (s != null)
                    return 9;
            }
            if (num != num1)
                return 10;
            return 0;
        }

        /// <summary>
        /// Чтение файла в массив байтов
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static byte[] ReadFile(string filePath, int len)
        {
            byte[] buffer = null;
            System.IO.FileStream fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;
                if (length > len)
                    length = len;
                buffer = new byte[length];
                if (length != fileStream.Read(buffer, 0, length))
                {
                    throw new Exception("Не удалось прочитать файл " + filePath);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }

    }
}
