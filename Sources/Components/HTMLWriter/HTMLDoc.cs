﻿using System;
using System.Collections.Generic;
using System.IO;


namespace HTMLWriter
{
    /// <summary>
    /// HTML-документ
    /// </summary>
    public class HTMLDoc
    {
        StreamWriter w;
        int nCols;
        /// <summary>
        /// шрифт
        /// </summary>
        public Font font = new Font(14,new Style());
        bool RowStarted = false;
        bool TableStarted = false;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="path">путь</param>
        /// <param name="title">заголовок</param>
        public HTMLDoc(string path, string title)
        {
            if (w != null)
            {
                w.Close();
            }
            w = new StreamWriter(path+".html");
            w.WriteLine("<HTML>");
            w.WriteLine("<meta charset=utf-8>");
            w.WriteLine("<head>");
            w.WriteLine("<title> " + title + "</title>");
            w.WriteLine("</head>");
            w.WriteLine("<BODY>");
        }

        /// <summary>
        /// добавить текст
        /// </summary>
        /// <param name="text">текст</param>
        public void AddText(string text)
        {
            w.WriteLine("<div>");
            w.WriteLine("<FONT SIZE=" + font.size +">");
            if (font.style.Bold || font.style.Italic || font.style.Underlined)
            {
                w.WriteLine((font.style.Bold ? "<b>" : "") + (font.style.Italic? "<i>" : "") + (font.style.Underlined ? "<u>" : ""));
            }
            w.WriteLine(text);
            if (font.style.Bold || font.style.Italic || font.style.Underlined)
            {
                w.WriteLine((font.style.Bold ? "</b>" : "") + (font.style.Italic ? "</i>" : "") + (font.style.Underlined ? "</u>" : ""));
            }
            w.WriteLine("</FONT>");
            w.WriteLine("</div>");
        }

        /// <summary>
        /// добавить таблицу
        /// </summary>
        /// <param name="nCols">количество столбцов</param>
        public void AddTable(int nCols)
        {
            if (TableStarted)
            {
                w.WriteLine("</TABLE>");
            }
            w.WriteLine("<TABLE BORDER=1 width=100%>");
            this.nCols = nCols;
            TableStarted = true;
        }

        /// <summary>
        /// добавить таблицу
        /// </summary>
        /// <param name="captions">Список заголовков</param>
        public void AddTable(List<string> captions)
        {
            if (RowStarted)
            {
                w.WriteLine("</tr>");
                RowStarted = false;
            }
            if (TableStarted)
            {
                w.WriteLine("</TABLE>");
            }
            w.WriteLine("<TABLE BORDER=1 width=100%>");
            foreach (string Caption in captions)
            {
                w.WriteLine("<TH><FONT SIZE=\"6\">"+ Caption + "</FONT></TH>");
            }
            nCols = captions.Count;
            TableStarted = true;
        }

        /// <summary>
        /// добавить строку в таблицу
        /// </summary>
        /// <param name="values">значения</param>
        public void AddRow(List<string> values)
        {
            if (RowStarted)
            {
                w.WriteLine("</tr>");
            }
            if (values.Count != nCols)
            {
                throw new Exception("Количество элементов в добавляемой строке не совпадает с количеством элементов в созданной таблице.");
            }
            w.WriteLine("<tr>");
            foreach (string value in values)
            {
                w.WriteLine("<TD>");
                AddText(value);
                w.WriteLine("</TD>");
            }
            w.WriteLine("</tr>");
            RowStarted = true;
        }

        /// <summary>
        /// начать строку
        /// </summary>
        public void AddRow()
        {
            if (RowStarted)
            {
                w.WriteLine("</tr>");
            } 
            w.WriteLine("<tr>");
            RowStarted = true;
        }

        /// <summary>
        /// закончить строку
        /// </summary>
        public void FinishRow()
        {
            w.WriteLine("</tr>");
            RowStarted = false;
        }

        /// <summary>
        /// добавить ячейку
        /// </summary>
        /// <param name="value"></param>
        /// <param name="colSpan"></param>
        /// <param name="rowSpan"></param>
        public void AddCell(string value, int colSpan, int rowSpan)
        {
            w.WriteLine("<TD colspan=\"" + colSpan.ToString() + "\" rowspan=\"" + rowSpan.ToString() +  "\">");
            AddText(value);
            w.WriteLine("</TD>");
        }

        /// <summary>
        /// завершить таблицу
        /// </summary>
        public void FinishTable()
        {
            if (RowStarted)
            {
                w.WriteLine("</tr>");
                RowStarted = false;
            }
            w.WriteLine("</TABLE>");
            TableStarted = false;
        }
        
        
        /// <summary>
        /// завершить документ
        /// </summary>
        public void Close()
        {
            if (RowStarted)
            {
                w.WriteLine("</tr>");
            }
            if (TableStarted)
            {
                w.WriteLine("</table>");
            }
            w.WriteLine("</BODY>");
            w.WriteLine("</HTML>");
            w.Close();
        }

        /// <summary>
        /// шрифт по умолчанию
        /// </summary>
        public void ResetFont()
        {
            font.size = 5;
            font.style.Bold = font.style.Italic = font.style.Underlined = false;
        }

    }

    /// <summary>
    /// шрифт
    /// </summary>
    public class Font
    {
        /// <summary>
        /// размер
        /// </summary>
        public int size;

        /// <summary>
        /// стиль
        /// </summary>
        public Style style;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="size"></param>
        /// <param name="style"></param>
        public Font(int size, Style style)
        {
            this.size = size;
            this.style = style;
        }
    }

    public class Style
    {
        /// <summary>
        /// полужирный? 
        /// </summary>
        public bool Bold;

        /// <summary>
        /// курсив
        /// </summary>
        public bool Italic;
        /// <summary>
        /// подчеркнутый
        /// </summary>
        public bool Underlined;

        /// <summary>
        /// конструктор
        /// </summary>
        public Style()
        {
            Bold = Italic = Underlined = false;
        }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="bold"></param>
        /// <param name="italic"></param>
        /// <param name="underlined"></param>
        public Style(bool bold, bool italic, bool underlined)
        {
            Bold = bold;
            Italic = italic;
            Underlined = underlined;
        }
    }
}
