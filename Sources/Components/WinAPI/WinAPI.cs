﻿using System;
using System.Runtime.InteropServices;

namespace WinAPI
{
    [Obsolete("Будет заменён на AlphaFS")]
    public class WinAPI
    {
        /// <summary>
        /// Атрибуты файла
        /// </summary>
        [Flags]
        [CLSCompliant(false)]
        public enum FileAttributes : uint
        {
            /// <summary>
            /// Архив
            /// </summary>
            Archive = 0x00000020,

            /// <summary>
            /// Сжатый файл
            /// </summary>
            Compressed = 0x00000800,

            /// <summary>
            /// Устройство
            /// </summary>
            Device = 0x00000040,

            /// <summary>
            /// Каталог
            /// </summary>
            Directory = 0x00000010,

            /// <summary>
            /// Зашифрованный файл
            /// </summary>
            Encrypted = 0x00004000,

            /// <summary>
            /// Скрытый файл
            /// </summary>
            Hidden = 0x00000002,

            /// <summary>
            /// 
            /// </summary>
            IntegrityStream = 0x00008000,

            /// <summary>
            /// Без дополнительных атрибутов
            /// </summary>
            Normal = 0x00000080,

            /// <summary>
            /// Непроиндексированный файл
            /// </summary>
            NotContentIndexed = 0x00002000,

            /// <summary>
            /// 
            /// </summary>
            NoScrubData = 0x00020000,

            /// <summary>
            /// Отключенный сетевой файл
            /// </summary>
            Offline = 0x00001000,

            /// <summary>
            /// Только чтение
            /// </summary>
            Readonly = 0x00000001,

            /// <summary>
            /// Точка повторной обработки (символическая ссылка)
            /// </summary>
            ReparsePoint = 0x00000400,
            
            /// <summary>
            /// Разреженный файл
            /// </summary>
            SparseFile = 0x00000200,

            /// <summary>
            /// Системный файл
            /// </summary>
            System = 0x00000004,

            /// <summary>
            /// Временный файл
            /// </summary>
            Temporary = 0x00000100,

            /// <summary>
            /// Зарезервированный файл
            /// </summary>
            Virtual = 0x00010000,

            /// <summary>
            /// Ошибка
            /// </summary>
            Invalid = 0xFFFFFFFF
        }

        /// <summary>
        /// Структура данных для пойска дочерних элементов директории
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct WIN32_FIND_DATA
        {
            public FileAttributes dwFileAttributes;
            public FILETIME ftCreationTime;
            public FILETIME ftLastAccessTime;
            public FILETIME ftLastWriteTime;
            public uint nFileSizeHigh;
            public uint nFileSizeLow;
            public uint dwReserved0;
            public uint dwReserved1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 300)]
            public string cFileName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
            public string cAlternate;
        }

        /// <summary>
        /// Получить атрибуты файла или директории. Присутствует поддержка длинных путей.
        /// </summary>
        /// <param name="lpFileName">Путь до файла или директории</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern FileAttributes GetFileAttributes([MarshalAs(UnmanagedType.LPWStr)] string lpFileName);

        /// <summary>
        /// Задать атрибуты файла или директории. Присутствует поддержка длинных путей.
        /// </summary>
        /// <param name="lpFileName">Путь до файла или директории</param>
        /// <param name="dwFileAttributes">Атрибуты файла или директории</param>
        /// <returns>True - успех, False - иначе</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool SetFileAttributes([MarshalAs(UnmanagedType.LPWStr)] string lpFileName, FileAttributes dwFileAttributes);

        /// <summary>
        /// Поиск первого дочернего элемента директории (файла или каталога). Необходима для инициализации дескриптора поиска
        /// </summary>
        /// <param name="lpFileName">Путь до директории</param>
        /// <param name="lpFindFileData">Структура данных поиска дочерних элементов директории</param>
        /// <returns>Дескриптор поиска дочерних элементов директории</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern IntPtr FindFirstFile([MarshalAs(UnmanagedType.LPWStr)] string lpFileName, out WIN32_FIND_DATA lpFindFileData);

        /// <summary>
        /// Поиск следующего дочернего элемента директории по дескриптору поиска
        /// </summary>
        /// <param name="hFindFile">Дескриптор поиска дочерних элементов директории</param>
        /// <param name="lpFindFileData">Структура данных поиска дочерних элементов директории</param>
        /// <returns>True - успех, False - иначе</returns>
        [DllImport("kernel32", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern bool FindNextFile(IntPtr hFindFile, out WIN32_FIND_DATA lpFindFileData);

        /// <summary>
        /// Закрытие дескриптора поиска дочерних элементов директории
        /// </summary>
        /// <param name="hFindFile"></param>
        /// <returns>True - успех, False - иначе</returns>
        [DllImport("kernel32.dll")]
        public static extern bool FindClose(IntPtr hFindFile);

        /// <summary>
        /// Копирование файла. Присутствует поддержка длинных путей.
        /// </summary>
        /// <param name="lpExistingFileName">Путь до файла, который копируется (строка преобразуется в 2-байтовую последовательность Юникода)</param>
        /// <param name="lpNewFileName">Путь до файла, куда следует скопировать (строка преобразуется в 2-байтовую последовательность Юникода)</param>
        /// <param name="bFailIfExists">Выдавать ошибку, если такой файл уже существует?</param>
        /// <returns>True - успех, False - иначе</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CopyFile([MarshalAs(UnmanagedType.LPWStr)] string lpExistingFileName, [MarshalAs(UnmanagedType.LPWStr)] string lpNewFileName, [MarshalAs(UnmanagedType.Bool)] bool bFailIfExists);

        /// <summary>
        /// Удаление файла. Присутствует поддержка длинных путей
        /// </summary>
        /// <param name="lpFileName">Путь до файла (строка преобразуется в 2-байтовую последовательность Юникода)</param>        
        /// <returns>True - успех, False - иначе</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteFile([MarshalAs(UnmanagedType.LPWStr)] string lpFileName);

        /// <summary>
        /// Создание диерктории. Присутствует поддержка длинных путей
        /// </summary>
        /// <param name="lpPathName">Путь до директории (строка преобразуется в 2-байтовую последовательность Юникода)</param>
        /// <param name="lpSecurityAttributes">Атрибуты доступа к директории</param>
        /// <returns>True - успех, False - иначе</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CreateDirectory([MarshalAs(UnmanagedType.LPWStr)] string lpPathName, IntPtr lpSecurityAttributes);

        /// <summary>
        /// Удаление директории. Присутствует поддрежка длинных путей
        /// </summary>
        /// <param name="lpPathName">Путь до директории (строка преобразуется в 2-байтовую последовательность Юникода)</param>        
        /// <returns>True - успех, False - иначе</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool RemoveDirectory([MarshalAs(UnmanagedType.LPWStr)] string lpPathName);
    }
}
