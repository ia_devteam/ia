﻿using System.Collections.Generic;

namespace IA.SlnResolver
{
    /// <summary>
    /// Класс, предоставляющий методы импорта настроек, а также импорта и экспорта конфигураций проектных файлов C# и VC++.
    /// </summary>
    public class ProjectVS
    {
        /// <summary>
        /// Проверка существования проектного файла.
        /// </summary>
        /// <param name="filename">Путь до проектного файла.</param>
        /// <returns>"true" - файл существует.</returns>
        private static bool CanOpen(string filename)
        {
            return System.IO.File.Exists(filename);
        }

        /// <summary>
        /// Импорт настроек проекта C#.
        /// </summary>
        /// <param name="filename">Путь до проектного файла.</param>
        /// <returns>Настройки проекта.</returns>
        public static CS.ProjectBase ParseCSProject(string filename)
        {
            if (!CanOpen(filename)) 
                return null;

            CS.CSProjectReader csProjectReader = new CS.CSProjectReader();
            return csProjectReader.ReadProject(filename, true);
        }

        /// <summary>
        /// Импорт настроек проекта VC++ до VS2010 (.vcproj).
        /// </summary>
        /// <param name="filename">Путь до проектного файла.</param>
        /// <returns>Настройки проекта.</returns>
        public static VC.ProjectBase ParseVCProject(string filename)
        {
            if (!CanOpen(filename))
                return null;

            VC.VCProjectReader vcProjectReader = new VC.VCProjectReader();
            return vcProjectReader.ReadProject(filename);
        }

        /// <summary>
        /// Импорт настроек проекта VC++ c VS 2010 (.vcxproj).
        /// </summary>
        /// <param name="filename">Путь до проектного файла.</param>
        /// <returns>Настройки проекта.</returns>
        public static VCX.ProjectBase ParseVCXCProject (string filename)
        {
            if (!CanOpen(filename))
                return null;

            VCX.VCXProjectReader vcxProjectReader = new VCX.VCXProjectReader();
            return vcxProjectReader.ReadProject(filename);
        }



        /// <summary>
        /// Импорт текущей конфигурации проекта С#.
        /// </summary>
        /// <param name="project">Проект.</param>
        /// <returns>Текущая конфигурация проекта.</returns>
        public static CS.ProjectConfiguration GetCurrentConfiguration(CS.ProjectBase project)
        {
            foreach (CS.ProjectConfiguration projectConfiguration in project.Settings.ConfigurationArray)
                if (projectConfiguration.isCurrent)
                    return projectConfiguration;

            return null;
        }

        /// <summary>
        /// Импорт текущей конфигурации проекта VC++ до VS2010 (.vcproj).
        /// </summary>
        /// <param name="project">Проект.</param>
        /// <returns>Текущая конфигурация проекта.</returns>
        public static VC.ProjectConfiguration GetCurrentConfiguration(VC.ProjectBase project)
        {
            foreach (VC.ProjectConfiguration projectConfiguration in project.Settings.ConfigurationArray)
                if (projectConfiguration.isCurrent)
                    return projectConfiguration;

            return null;
        }


        /// <summary>
        /// Импорт текущей конфигурации проекта VC++ c VS2010 (.vcxproj).
        /// </summary>
        /// <param name="project">Проект.</param>
        /// <returns>Текущая конфигурация проекта.</returns>
        public static VCX.ProjectConfiguration GetCurrentConfiguration(VCX.ProjectBase project)
        {
            foreach (VCX.ProjectConfiguration projectConfiguration in project.Settings.ConfigurationArray)
                if (projectConfiguration.isCurrent)
                    return projectConfiguration;

            return null;
        }





        /// <summary>
        /// Установка(Изменение) текущей конфигурации проекта C# .
        /// Если такой конфигурации нет, оставляет ту что и была по умолчанию.
        /// </summary>
        /// <param name="project">Проект.</param>
        /// <param name="configName">Имя конфигурации.</param>
        /// <returns>Текущая конфигурация проекта.</returns>
        public static CS.ProjectConfiguration SetCurrentConfiguration(CS.ProjectBase project, string configName)
        {
            int i;
            for (i = 0; i < project.Settings.ConfigurationArray.Count; i++ )
                if (project.Settings.ConfigurationArray[i].ConfigurationName == configName)
                    break;

            //не нашли такой конфигурации
            if (i == project.Settings.ConfigurationArray.Count)
                return GetCurrentConfiguration(project);

            for (int j = 0; j < project.Settings.ConfigurationArray.Count; j++)
                project.Settings.ConfigurationArray[j].isCurrent = j == i;

            return project.Settings.ConfigurationArray[i];
        }

        /// <summary>
        /// Установка текущей конфигурации проекта VC++.
        /// Если такой конфигурации нет, оставляет ту что и была по умолчанию.
        /// </summary>
        /// <param name="project">Проект.</param>
        /// <param name="configName">Имя конфигурации.</param>
        /// <returns>Текущая конфигурация проекта.</returns>
        public static VC.ProjectConfiguration SetCurrentConfiguration(VC.ProjectBase project, string configName)
        {
            int i;
            for (i = 0; i < project.Settings.ConfigurationArray.Count; i++)
                if (project.Settings.ConfigurationArray[i].ConfigurationName.Split('|')[0] == configName)
                    break;

            //не нашли такой конфигурации
            if (i == project.Settings.ConfigurationArray.Count)
                return GetCurrentConfiguration(project);

            for (int j = 0; j < project.Settings.ConfigurationArray.Count; j++)
                project.Settings.ConfigurationArray[j].isCurrent = j == i;

            return project.Settings.ConfigurationArray[i];
        }

        /// <summary>
        /// Экспорт проекта по пути к файлу проекта C#.
        /// </summary>
        /// <param name="projects">Список проектов.</param>
        /// <param name="path">Путь до проекта.</param>
        /// <returns>Найденный проект.</returns>
        public static CS.ProjectBase GetProjectByPath (List<CS.ProjectBase> projects, string path)
        {
            foreach (CS.ProjectBase project in projects)
                if (project != null && project.Path == path)
                    return project;

            return null;
        }
    }
}
