﻿using System.Collections.Generic;

namespace IA.SlnResolver.VC
{
    /// <summary>
    /// Класс хранит всю информацию, импортированноую из файла прокета (Visual C++)
    /// </summary>
    public class ProjectBase
    {
        /// <summary>
        /// Имя проекта
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// Путь к файлу проекта
        /// </summary>
        public string Path = string.Empty;

        /// <summary>
        /// Основные настройки проекта
        /// </summary>
        public ProjectSettings Settings = new ProjectSettings();

        /// <summary>
        /// Ссылки на сторонние библиотеки
        /// </summary>
        public List<ProjectReferencedDll> ReferencedProjects = new List<ProjectReferencedDll>();

        /// <summary>
        /// 
        /// </summary>
        public List<ProjectSourceFile> SourceFiles = new List<ProjectSourceFile>();
        /// <summary>
        /// 
        /// </summary>
        public List<ProjectSourceFile> HeaderFiles = new List<ProjectSourceFile>();
        /// <summary>
        /// 
        /// </summary>
        public List<ProjectSourceFile> ResourceFiles = new List<ProjectSourceFile>();

    }

    /// <summary>
    /// Класс, экземпляры которого хранят информацию о файлах исходного кода в проекте (C#)
    /// </summary>
    public class ProjectSourceFile
    {
        /// <summary>
        /// Имя файла
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public bool CopyToOutputDirectory;

        /// <summary>
        /// Путь до каталога, содержащего данный файл
        /// </summary>
        public string Location = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string DependentUpon = string.Empty;
    }

    /// <summary>
    /// Класс, экземпляры которого хранят настройки отдельных проектов (Visual C++)
    /// </summary>
    public class ProjectSettings
    {
        /// <summary>
        /// Имя сборки на выходе
        /// </summary>
        public string AssemblyName = string.Empty;


        /// <summary>
        /// Список доступных конфигурация для сборки
        /// </summary>
        public List<ProjectConfiguration> ConfigurationArray = new List<ProjectConfiguration>();
    }

    /// <summary>
    /// Класс, экземпляры которого хранят настройки отдельной конфигурации проекта (Visual C++)
    /// </summary>
    public class ProjectConfiguration
    {

        /// <summary>
        /// Наименование конфигурации
        /// </summary>
        public string ConfigurationName = string.Empty;

        /// <summary>
        /// Имя платформы
        /// </summary>
        public string ConfigurationPlatform = string.Empty;

        /// <summary>
        /// Путь к коталогу, куда будут помещены собранные файлы для данной конфигурации
        /// </summary>
        public string OutputDirectory = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string IntermediateDirectory = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string ConfigurationType = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string CharacterSet = string.Empty;

        /// <summary>
        /// Данное поле указывает на то, является ли данная конфинурация текущей выбранной конфигурацией для проекта
        /// </summary>
        public bool isCurrent = false;
    }

    /// <summary>
    /// Класс, экземпляры которого хранят внешние ссылки на сторонние библиотеки в проекте (Visual C++)
    /// </summary>
    public class ProjectReferencedDll
    {
        /// <summary>
        /// Путь к файлу с библиотекой
        /// </summary>
        public string RelativePath = string.Empty;

        /// <summary>
        /// Имя сторонней сборки
        /// </summary>
        public string AssemblyName = string.Empty;

        /// <summary>
        /// Минимальная версия framework'a для работы библиотеки
        /// </summary>
        public string MinFrameworkVersion = string.Empty;
    }

    ///// <summary>
    ///// Класс, экземпляры которого хранят информацию о файлах исходного кода в проекте (Visual C++)
    ///// </summary>
    //public class ProjectFiles
    //{
    //    public List<string> SourceFiles = new List<string>();
    //    public List<string> HeaderFiles = new List<string>();
    //    public List<string> ResourceFiles = new List<string>();
    //}


  
}
