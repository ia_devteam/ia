﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Linq;


namespace IA.SlnResolver.VCX
{

    /// <summary>
    /// Класс импорта настроек из конфигурационного файла проекта C#
    /// </summary>
    public class VCXProjectReader
    {
        private string projectPath;
        string DefaultConfgurationName = "Release";
        string DefaultPlatform = "Win32";
        bool lostKey = true;

        /// <summary>
        /// Основная функция импорта настроек из файла проекта.
        /// </summary>
        /// <param name="filePath">Путь к файлу проекта</param>
        /// <param name="resolvePath"></param>
        /// <returns>Экземпляр класса, содержащий всю информацию о настройках проекта</returns>          
        public ProjectBase ReadProject(string filePath, bool resolvePath = false)
        {
            try
            {
                projectPath = Path.GetFullPath(filePath.Replace(Path.GetFileName(filePath), ""));
                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlNamespaceManager mgr = new XmlNamespaceManager(doc.NameTable);
                mgr.AddNamespace("x", "http://schemas.microsoft.com/developer/msbuild/2003");
                ProjectBase basicProject = new ProjectBase
                {
                    Name = filePath.Substring(filePath.LastIndexOf('\\') + 1),
                    Path = filePath,
                    Settings = GetSettings(doc, mgr),
                    SourceFiles = GetSourceFiles(doc, mgr),
                    ResourceFiles = GetResourceFiles(doc, mgr),
                    HeaderFiles = GetHeaderFiles(doc, mgr),
                    ReferencedProjects = getReferencedProjects(doc, mgr),

                };

                basicProject.ReferencedDlls = GetReferenceDlls(doc, mgr, basicProject.Settings.ConfigurationArray);


                ReplaceEnvironmentVariables(basicProject);
                return basicProject;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
                return null;
            }
        }

        private List<ProjectReferencedDll> GetReferenceDlls(XmlDocument doc, XmlNamespaceManager mgr, List<ProjectConfiguration> ConfArray)
        {
            List<ProjectReferencedDll> prjdlls = new List<ProjectReferencedDll>();

            foreach (ProjectConfiguration conf in ConfArray)
            {
                XmlNode definitionGroup = GetLinkNode(doc, mgr, conf);
                if (definitionGroup != null)
                {
                    string str;
                    str = GetCrtConfigurationValue(definitionGroup, mgr, "AdditionalDependencies");
                    if (str != string.Empty)
                    {

                        if (str.Contains("$(SolutionDir)") == true)
                        {
                            str = str.Replace("$(SolutionDir)", projectPath);
                        }
                        if (str.Contains("$(Configuration)") == true)
                        {
                            str = str.Replace("$(Configuration)", conf.ConfigurationName);
                        }
                        if (str.Contains(";%(AdditionalDependencies)"))
                        {
                            str = str.Replace(";%(AdditionalDependencies)", string.Empty);
                        }

                        str = Path.GetFullPath(str);
                        ProjectReferencedDll newDll = new ProjectReferencedDll
                        {
                            Name = Path.GetFileName(str),
                            Location = str
                        };

                        ProjectReferencedDll find;
                        if ((find = prjdlls.Find(f => (f.Location == newDll.Location))) == null)
                        {
                            prjdlls.Add(newDll);
                        }

                    };

                }
            }

            return prjdlls;
        }
        
        private ProjectSettings GetSettings(XmlDocument doc, XmlNamespaceManager mgr)
        {

            XmlNode projectSetNode = GetSettingsNode(doc, mgr);
            ProjectSettings settings = new ProjectSettings
            {
                ProjectGuid = GetCrtConfigurationValue(projectSetNode, mgr, "ProjectGuid"),
                RootNamespace = GetCrtConfigurationValue(projectSetNode, mgr, "RootNamespace"),
                ToolVersion = GetToolVersion(doc, mgr),
                ConfigurationArray = GetProjectConfigurationArray(doc, mgr)
            };
            return settings;
        }

        private List<ProjectConfiguration> GetConfigurationList(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectConfiguration> prjConfigurationList = new List<ProjectConfiguration>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup", mgr);

            if (nodes != null)
                foreach (XmlNode node in nodes)
                {
                    if (node.Attributes == null)
                        continue;
                    XmlAttribute attribute = node.Attributes["Label"];

                    if (attribute == null || string.IsNullOrEmpty(attribute.Value))
                        continue;

                    if (attribute.Value == "ProjectConfigurations")
                    {
                        XmlNodeList childs = node.ChildNodes;
                        foreach (XmlNode child in childs)
                        {
                            ProjectConfiguration conf = new ProjectConfiguration
                            {
                                ConfigurationName = GetCrtConfigurationValue(child, mgr, "Configuration"),
                                ConfigurationPlatform = GetCrtConfigurationValue(child, mgr, "Platform")
                            };
                            prjConfigurationList.Add(conf);
                        }
                    }
                }
            return prjConfigurationList;
        }

        private List<ProjectConfiguration> GetProjectConfigurationArray(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectConfiguration> prjConfigurationList = GetConfigurationList(doc, mgr);

            foreach (ProjectConfiguration conf in prjConfigurationList)
            {
                XmlNode definitionGroup = GetDefinitionNode(doc, mgr, conf);
                XmlNode propertyGroup = GetCnfPropertyNode(doc, mgr, conf);
                List<XmlNode> confProperties = getPropertyCnfNodes(doc, mgr, conf);

                conf.ConfigurationType = GetCrtConfigurationValue(propertyGroup, mgr, "ConfigurationType");
                conf.PlatformToolset = GetCrtConfigurationValue(propertyGroup, mgr, "PlatformToolset");
                conf.CharacterSet = GetCrtConfigurationValue(propertyGroup, mgr, "CharacterSet");
                conf.Optimize = GetCrtConfigurationValue(definitionGroup, mgr, "Optimization");
                conf.WarningLevel = GetCrtConfigurationValue(definitionGroup, mgr, "WarningLevel");
                conf.WholeProgrammOptimization = GetCrtConfigurationValue(definitionGroup, mgr, "WholeProgramOptimization");
                conf.UseDebugLibraries = GetCrtConfigurationValue(definitionGroup, mgr, "UseDebugLibraries");
                conf.UseOfMfc = GetCrtConfigurationValue(definitionGroup, mgr, "UseOfMfc");
                //conf.UseOfATL = GetCrtConfigurationValue(definitionGroup, mgr, "UseOfATL");
                conf.AssemblyName = GetPropertyFromList(confProperties, "TargetName");
                conf.TargetExt = GetPropertyFromList(confProperties, "TargetExt");
                conf.OutputPath = GetOutputPath(confProperties, conf);

            }
            SetCurrentConfiguration(prjConfigurationList);

            return prjConfigurationList;

        }

        private List<ProjectSourceFile> GetSourceFiles(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectSourceFile> files = new List<ProjectSourceFile>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup/x:ClCompile", mgr);

            if (nodes != null)
            {
                foreach (XmlNode child in nodes)
                {
                    files.Add(CreateFile(child));
                    //ProjectSourceFile file = new ProjectSourceFile();
                    //if (child.Attributes != null)
                    //{
                    //    string f = child.Attributes["Include"].InnerText;
                    //    file.Location = Path.GetFullPath(Path.Combine(projectPath, f));
                    //    file.Name = Path.GetFileName(file.Location);
                    //    files.Add(file);
                    //}
                }
            }

            return files;
        }

        private List<ProjectSourceFile> GetHeaderFiles(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectSourceFile> files = new List<ProjectSourceFile>();

            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup/x:ClInclude", mgr);
            if (nodes != null)
            {
                foreach (XmlNode child in nodes)
                {
                    files.Add(CreateFile(child));
                }
            }
            return files;
        }

        private ProjectSourceFile CreateFile(XmlNode node)
        {
            ProjectSourceFile file = new ProjectSourceFile();
            if (node.Attributes != null)
            {
                string f = node.Attributes["Include"].InnerText;
                file.Name = Path.GetFileName(f);
                file.Location = f.Replace(file.Name, "");
            }
            return file;
        }

        private List<ProjectSourceFile> GetResourceFiles(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectSourceFile> files = new List<ProjectSourceFile>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup", mgr);

            if (nodes != null)
            {
                foreach (XmlNode child in nodes)
                {
                    if (child.Attributes.Count == 0)
                    {
                        XmlNodeList n = child.ChildNodes;
                        foreach (XmlNode a in n)
                        {
                            if (a.Name != "ClInclude" && a.Name != "ClCompile" && a.Name != "ProjectReference" && a.Name != "None")
                            {
                                files.Add(CreateFile(a));
                                //ProjectSourceFile file = new ProjectSourceFile();
                                //if (a.Attributes["Include"] != null)
                                //{
                                //    string f = a.Attributes["Include"].InnerText;
                                //    file.Location = Path.GetFullPath(Path.Combine(projectPath, f));
                                //    file.Name = Path.GetFileName(file.Location);
                                //    files.Add(file);
                                //}
                            }
                        }
                    }
                }
            }
            return files;
        }

        private List<ProjectReferencedProject> getReferencedProjects(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectReferencedProject> files = new List<ProjectReferencedProject>();
            try
            {
                XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup/x:ProjectReference", mgr);

                if (nodes != null)
                    foreach (XmlNode child in nodes)
                    {
                        if (child.Attributes == null)
                            continue;

                        ProjectReferencedProject file = new ProjectReferencedProject { Location = child.Attributes["Include"].InnerText };
                        file.Name = Path.GetFileName(file.Location);
                        XmlNode node = child.SelectSingleNode("./x:Project", mgr);

                        if (node != null)
                            file.ProjectGuid = node.InnerText;

                        files.Add(file);
                    }
                return files;
            }
            catch (Exception)
            {
                return files;
            }
        }

        private XmlNode GetSettingsNode(XmlDocument doc, XmlNamespaceManager mgr)
        {
            XmlNode res = null;
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:PropertyGroup", mgr);

            foreach (XmlNode node in nodes)
            {
                if (node.Attributes != null)
                {
                    if (node.Attributes["Label"].Value == "Globals")
                    {
                        return node;
                    }
                }
            }
            return res;
        }

        private List<XmlNode> getPropertyCnfNodes(XmlDocument doc, XmlNamespaceManager mgr, ProjectConfiguration conf)
        {
            List<XmlNode> result = new List<XmlNode>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:PropertyGroup", mgr);
            if (nodes != null)
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.Attributes.Count == 0)
                    {
                        result.AddRange(GetFromEmptyPropGroup(node.ChildNodes, mgr, conf));
                    }
                    else
                    {
                        if (node.Attributes != null && node.Attributes["Label"] == null && node.Attributes["Condition"] != null)
                        {
                            result.AddRange(GetFromUserPropGroup(node, mgr, conf));
                        }
                    }
                }
            }
            return result;
        }

        private XmlNode GetDefinitionNode(XmlDocument doc, XmlNamespaceManager mgr, ProjectConfiguration conf)
        {
            string confName = "'" + conf.ConfigurationName + "|" + conf.ConfigurationPlatform + "'";
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemDefinitionGroup", mgr);
            if (nodes != null)
                foreach (XmlNode node in nodes)
                {
                    if (node.Attributes == null)
                        continue;

                    XmlAttribute attribute = node.Attributes["Condition"];

                    if (attribute == null || string.IsNullOrEmpty(attribute.Value))
                        continue;

                    if (attribute.Value.Contains(confName))
                    {
                        XmlNodeList childs = node.ChildNodes;
                        foreach (XmlNode child in childs)
                        {
                            if (child.Name == "ClCompile")
                            {
                                return child;
                            }
                        }
                    }
                }
            return doc;
        }

        private XmlNode GetLinkNode(XmlDocument doc, XmlNamespaceManager mgr, ProjectConfiguration conf)
        {
            string confName = "'" + conf.ConfigurationName + "|" + conf.ConfigurationPlatform + "'";
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemDefinitionGroup", mgr);
            if (nodes != null)
                foreach (XmlNode node in nodes)
                {
                    if (node.Attributes == null)
                        continue;

                    XmlAttribute attribute = node.Attributes["Condition"];

                    if (attribute == null || string.IsNullOrEmpty(attribute.Value))
                        continue;

                    if (attribute.Value.Contains(confName))
                    {
                        XmlNodeList childs = node.ChildNodes;
                        foreach (XmlNode child in childs)
                        {
                            if (child.Name == "Link")
                            {
                                return child;
                            }
                        }
                    }
                }
            return doc;
        }

        private XmlNode GetCnfPropertyNode(XmlDocument doc, XmlNamespaceManager mgr, ProjectConfiguration conf)
        {

            string confName = "'" + conf.ConfigurationName + "|" + conf.ConfigurationPlatform + "'";
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:PropertyGroup", mgr);
            if (nodes != null)
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.Attributes == null)
                        continue;

                    if (node.Attributes["Label"] != null && node.Attributes["Condition"] != null)
                    {
                        if (node.Attributes["Label"].Value == "Configuration")
                        {
                            if (node.Attributes["Condition"].Value.Contains(confName))
                            {
                                return node;
                            }
                        }
                    }
                }
            }
            return doc;
        }

        private XmlNode[] GetFromUserPropGroup(XmlNode node, XmlNamespaceManager mgr, ProjectConfiguration conf)
        {
            List<XmlNode> result = new List<XmlNode>();
            if (node != null && CheckToAccessCnf(node, conf) == true)
            {
                XmlNodeList childs = node.ChildNodes;

                foreach (XmlNode child in childs)
                {
                    result.Add(child);
                }
            }
            return result.ToArray();
        }

        private XmlNode[] GetFromEmptyPropGroup(XmlNodeList nodes, XmlNamespaceManager mgr, ProjectConfiguration conf)
        {
            List<XmlNode> result = new List<XmlNode>();
            if (nodes != null)
            {
                foreach (XmlNode node in nodes)
                {
                    if (CheckToAccessCnf(node, conf) == true)
                    {
                        result.Add(node);
                    }
                }
            }
            return result.ToArray();
        }

        private string GetPropertyValue(XmlDocument doc, XmlNamespaceManager mgr, string property)
        {
            XmlNode node = doc.SelectSingleNode(string.Format("/x:Project/x:PropertyGroup/x:{0}", property), mgr);
            return node == null ? string.Empty : node.InnerText;
        }

        private string GetCrtConfigurationValue(XmlNode crtConfigurationNode, XmlNamespaceManager mgr, string property)
        {
            XmlNode node = crtConfigurationNode.SelectSingleNode(string.Format("./x:{0}", property), mgr);
            return node == null ? string.Empty : node.InnerText;
        }

        private string GetPropertyFromList(List<XmlNode> nodes, string property)
        {
            XmlNode node;
            if ((node = nodes.Find(f => f.Name == property)) != null)
            {
                return node.InnerText;
            }
            else
            {
                return string.Empty;
            }
        }

        private bool SetCurrentConfiguration(List<ProjectConfiguration> configurations)
        {
            try
            {

                List<ProjectConfiguration> AllDefaulconfigurations = configurations.FindAll(s => s.ConfigurationName == DefaultConfgurationName);
                if (AllDefaulconfigurations.Count != 0 && AllDefaulconfigurations != null)
                {
                    ProjectConfiguration q = AllDefaulconfigurations.Find(s => s.ConfigurationPlatform == DefaultPlatform);
                    if (q != null)
                    {
                        q.isCurrent = true;
                    }

                    else AllDefaulconfigurations.First().isCurrent = true;
                }
                else configurations.First().isCurrent = true;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return true;
        }


        //private void AddToEnVDic (IDictionary environmentVariables, string key, string value)
        //{
        //    if (value!=string.Empty)
        //    {
        //        environmentVariables.Add (key, value);
        //    }
        //}

        private IDictionary ToFormEnvVarDictionary(ProjectBase project, ProjectConfiguration config)
        {
            string SolutionDir = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(project.Path), @"..\"));
            IDictionary environmentVariables = Environment.GetEnvironmentVariables();
            environmentVariables.Add("SolutionDir", SolutionDir);
            environmentVariables.Add("Configuration", config.ConfigurationName);
            environmentVariables.Add("Platform", config.ConfigurationPlatform);
            environmentVariables.Add("CharacterSet", config.CharacterSet);
            environmentVariables.Add("ConfigurationType", config.ConfigurationType);
            environmentVariables.Add("PlatformName", config.ConfigurationPlatform);
            environmentVariables.Add("PlatformToolset", config.PlatformToolset);
            environmentVariables.Add("ProjectDir", projectPath);
            environmentVariables.Add("ProjectExt", config.TargetExt);
            environmentVariables.Add("ProjectName", project.Settings.RootNamespace);
            environmentVariables.Add("ProjectPath", project.Path);
            environmentVariables.Add("RootNamespace", project.Settings.RootNamespace);
            environmentVariables.Add("SolutionName", project.Name);
            environmentVariables.Add("SolutionFileName", project.Settings.RootNamespace);
            environmentVariables.Add("TargetExt", config.TargetExt);
            environmentVariables.Add("TargetName", config.AssemblyName);

            return environmentVariables;
        }

        private void ReplaceEnvironmentVariables(ProjectBase project)
        {
            foreach (ProjectConfiguration cnf in project.Settings.ConfigurationArray)
            {
                IDictionary envVarDic = ToFormEnvVarDictionary(project, cnf);
                cnf.OutputPath = CheckPresentEnvVarAndReplace(cnf.OutputPath, envVarDic, true);
                cnf.AssemblyName = CheckPresentEnvVarAndReplace(cnf.AssemblyName, envVarDic, false);
            }
        }

        private string CheckPresentEnvVarAndReplace(string value, IDictionary dic, bool path)
        {
            lostKey = true;
            while (isPresentEnvironmentVars(value) && lostKey)
            {
                value = Replacing(value, value, dic);
                if (path == true)
                {
                    value = Path.GetFullPath(value);
                }
            }
            return value;
        }

        private string Replacing(string replacingStr, string strWithEnvVars, IDictionary envVar)
        {
            int f_index = strWithEnvVars.IndexOf("$");
            int l_index = strWithEnvVars.IndexOf(")");
            if (f_index != -1 && l_index != -1)
            {
                string environmentVar = strWithEnvVars.Substring(f_index, l_index - f_index + 1);
                string key = environmentVar.Substring(2, environmentVar.Length - 3);

                if (envVar.Contains(key))
                {
                    string newPath = envVar[key].ToString();
                    replacingStr = strWithEnvVars.Replace(environmentVar, newPath);
                }
                else
                {
                    lostKey = false;
                }
            }
            return replacingStr;
        }

        private bool isPresentEnvironmentVars(string str)
        {
            int f, l;
            if ((f = str.IndexOf("$(")) != -1 && (l = str.IndexOf(")")) != -1 && l > f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckToAccessCnf(XmlNode node, ProjectConfiguration conf)
        {
            string confName = "'" + conf.ConfigurationName + "|" + conf.ConfigurationPlatform + "'";
            if (node.Attributes["Condition"] != null)
            {
                if (node.Attributes["Condition"].Value.Contains(confName))
                {
                    return true;
                }
            }
            return false;
        }

        private string GetToolVersion(XmlDocument doc, XmlNamespaceManager mgr)
        {
            XmlNode node = doc.SelectSingleNode(string.Format("./x:{0}", "Project"), mgr);
            if (node != null)
            {
                if (node.Attributes != null && node.Attributes["ToolsVersion"] != null)
                {
                    return node.Attributes["ToolsVersion"].Value;
                }
            }
            return string.Empty;
        }

        private string GetOutputPath(List<XmlNode> nodes, ProjectConfiguration config)
        {

            string outputPath = string.Empty;
            string definePath = @"$(SolutionDir)\$(Configuration)";
            outputPath = definePath;
            string OutDir = GetPropertyFromList(nodes, "OutDir");

            if (OutDir != string.Empty)
            {
                outputPath = OutDir;
            }
            return outputPath;
        }






    }

}

