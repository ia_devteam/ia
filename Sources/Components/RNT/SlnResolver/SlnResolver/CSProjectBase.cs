﻿using System.Collections.Generic;

namespace IA.SlnResolver.CS
{  /// <summary>
    /// Класс хранит всю информацию, импортированноую из файла прокета (C#)
    /// </summary>
    public class ProjectBase
    {
        /// <summary>
        /// Имя проекта
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// Путь к файлу проекта
        /// </summary>
        public string Path = string.Empty;

        /// <summary>
        /// Основные настроейки проекта
        /// </summary>
        public ProjectSettings Settings = new ProjectSettings();

        /// <summary>
        /// 
        /// </summary>
        public List<ProjectContentFile> ContentFiles = new List<ProjectContentFile>();

        /// <summary>
        /// Список файлов с исходными текстами
        /// </summary>
        public List<ProjectSourceFile> SourceFiles = new List<ProjectSourceFile>();

        /// <summary>
        /// Список файлов с ресурсами
        /// </summary>
        public List<ProjectResourceFile> ResourceFiles = new List<ProjectResourceFile>();

        /// <summary>
        /// Список конфигурационных файлов
        /// </summary>
        public List<ProjectConfigFile> ConfigFiles = new List<ProjectConfigFile>();

        /// <summary>
        /// Список ссылок на сторонние проекты (C#, Visual C++)
        /// </summary>
        public List<ProjectReferencedProject> ReferencedProjects = new List<ProjectReferencedProject>();

        /// <summary>
        /// Список ссылок на сторонние бибилиотеки
        /// </summary>
        public List<ProjectReferencedDll> ReferencedDlls = new List<ProjectReferencedDll>();

    }

    /// <summary>
    /// Класс, экземпляры которого хранят настройки отдельных проектов (C#)
    /// </summary>
    public class ProjectSettings
    {
        /// <summary>
        /// Версия среды разработки
        /// </summary>
        public string ProductVersion = string.Empty;

        /// <summary>
        /// Версия VisualStudio
        /// </summary>
        public string ToolVersion = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string SchemaVersion = string.Empty;
        
        /// <summary>
        /// 
        /// </summary>
        public string ProjectGuid = string.Empty;
        
        /// <summary>
        /// Параметр указывает на тип вывода. В результате сборки может получатся как исполняемый файл так и библиотека.
        /// </summary>
        public string OutputType = string.Empty;
        
        /// <summary>
        /// 
        /// </summary>
        public string AppDesignerFolder = string.Empty;
        
        /// <summary>
        /// 
        /// </summary>
        public string RootNamespace = string.Empty;
        
        /// <summary>
        /// Имя сборки на выходе
        /// </summary>
        public string AssemblyName = string.Empty;
        
        /// <summary>
        /// Версия framework'a необходимая для сборки проекта
        /// </summary>
        public string TargetFrameworkVersion = "v4.0";
        
        /// <summary>
        /// 
        /// </summary>
        public string FileAlignment = "512";
        
        /// <summary>
        /// 
        /// </summary>
        public string WarningLevel = string.Empty;
        
        /// <summary>
        /// Список доступных конфигураций
        /// </summary>
        public List<ProjectConfiguration> ConfigurationArray = new List<ProjectConfiguration>();
    }

    /// <summary>
    /// Класс, экземпляры которого хранят настройки отдельной конфигурации проекта (C#)
    /// </summary>
    public class ProjectConfiguration
    {

         /// <summary>
        /// Наименование конфигурации
        /// </summary>
        public string ConfigurationName = string.Empty;
        
        /// <summary>
        /// Платформа сборки
        /// </summary>
        public string ConfigurationPlatform = string.Empty;

        public string PlatformTarget = string.Empty;
        
        /// <summary>
        /// 
        /// </summary>
        public string CompilationConstants = string.Empty; //
        
        /// <summary>
        /// 
        /// </summary>
        public string Optimize = string.Empty; //

        /// <summary>
        /// Путь до XML-файла документации проекта
        /// </summary>
        public string DocumentationPath = string.Empty;

        /// <summary>
        /// Путь к коталогу, куда будут помещены собранные файлы для данной конфигурации
        /// </summary>
        public string OutputPath = string.Empty; //
        
        /// <summary>
        /// Данное поле указывает на то, является ли данная конфинурация текущей выбранной конфигурацией для проекта
        /// </summary>
        public bool isCurrent;
    }

    public abstract class ProjectBaseFile
    {
        public string Name = string.Empty;
        public string Location = string.Empty;
    }

    public abstract class ProjectBaseFileWithOutputDir : ProjectBaseFile
    {
        public bool CopyToOutputDirectory;
    }

    public abstract class ProjectBaseFileWithOutputDirAndDependent: ProjectBaseFileWithOutputDir
    {
        public string DependentUpon = string.Empty;
    }

    /// <summary>
    /// 
    /// </summary>
    public class ProjectContentFile : ProjectBaseFileWithOutputDir
    {
    }

    /// <summary>
    /// Класс, экземпляры которого хранят информацию о файлах ресурсов в проекте (C#)
    /// </summary>
    public class ProjectResourceFile : ProjectBaseFileWithOutputDirAndDependent
    {
    }

    /// <summary>
    /// Класс, экземпляры которого хранят информацию о файлах исходного кода в проекте (C#)
    /// </summary>
    public class ProjectSourceFile : ProjectBaseFileWithOutputDirAndDependent
    {
    }

    /// <summary>
    /// Класс, экземпляры которого хранят информацию о конфигурационных файлах в проекте (C#)
    /// </summary>
    public class ProjectConfigFile : ProjectBaseFileWithOutputDir
    {
    }

    /// <summary>
    /// Класс, экземпляры которого хранят внешние ссылки на сторонние проекты (C#)
    /// </summary>
    public class ProjectReferencedProject : ProjectBaseFile
    {
    }

    //FIXME - учитывать версию фреймворка
    /// <summary>
    /// Класс, экземпляры которого хранят внешние ссылки на сторонние библиотеки в проекте (C#)
    /// </summary>
    public class ProjectReferencedDll : ProjectBaseFile
    {
        /// <summary>
        /// Полный путь к файлу
        /// </summary>
        public string FullName = string.Empty;
    }   
}

