﻿using System.Collections.Generic;

namespace IA.SlnResolver.VCX
{

    public class ProjectBase
    {

        /// <summary>
        /// Имя проекта
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// Путь к файлу проекта
        /// </summary>
        public string Path = string.Empty;

        /// <summary>
        /// Основные настроейки проекта
        /// </summary>
        public ProjectSettings Settings = new ProjectSettings();
        
        /// <summary>
        /// Список файлов с исходными текстами
        /// </summary>
        public List<ProjectSourceFile> SourceFiles = new List<ProjectSourceFile>();

        /// <summary>
        /// Список файлов с ресурсами
        /// </summary>
        public List<ProjectSourceFile> ResourceFiles = new List<ProjectSourceFile>();

        /// <summary>
        /// Список конфигурационных файлов
        /// </summary>
        public List<ProjectSourceFile> HeaderFiles = new List<ProjectSourceFile>();

        /// <summary>
        /// Список ссылок на сторонние проекты (C#, Visual C++)
        /// </summary>
        public List<ProjectReferencedProject> ReferencedProjects = new List<ProjectReferencedProject>();

        /// <summary>
        /// Список ссылок на сторонние бибилиотеки
        /// </summary>
        public List<ProjectReferencedDll> ReferencedDlls = new List<ProjectReferencedDll>();

    }

    /// <summary>
    /// Класс, экземпляры которого хранят настройки отдельных проектов (C#)
    /// </summary>
    public class ProjectSettings
    {
        /// <summary>
        /// Версия VisualStudio
        /// </summary>
        public string ToolVersion = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string ProjectGuid = string.Empty;

        /// <summary>
        /// Имя сборки на выходе
        /// </summary>
        public string RootNamespace = string.Empty;

        /// <summary>
        /// Список доступных конфигураций
        /// </summary>
        public List<ProjectConfiguration> ConfigurationArray = new List<ProjectConfiguration>();
    }

    /// <summary>
    /// Класс, экземпляры которого хранят настройки отдельной конфигурации проекта (C#)
    /// </summary>
    public class ProjectConfiguration
    {

        /// <summary>
        /// Наименование конфигурации
        /// </summary>
        public string ConfigurationName = string.Empty;

        /// <summary>
        /// Платформа сборки
        /// </summary>
        public string ConfigurationPlatform = string.Empty;



        /// <summary>
        /// Параметр указывает на тип вывода. В результате сборки может получатся как исполняемый файл так и библиотека.
        /// </summary>
        public string ConfigurationType = string.Empty;


        /// <summary>
        /// Версия студии
        /// </summary>
        public string PlatformToolset = string.Empty;


        /// <summary>
        /// То же, что Optimization
        /// </summary>
        public string Optimize = string.Empty; //

        /// <summary>
        /// Путь к коталогу, куда будут помещены собранные файлы для данной конфигурации
        /// </summary>
        public string OutputPath = string.Empty; //

        /// <summary>
        /// Имя проекта на выходе
        /// </summary>
        public string AssemblyName = string.Empty;

        /// <summary>
        /// Расширение проекта на выходе
        /// </summary>
        public string TargetExt = string.Empty;

        /// <summary>
        /// Данное поле указывает на то, является ли данная конфинурация текущей выбранной конфигурацией для проекта
        /// </summary>
        public bool isCurrent = false;

        /// <summary>
        /// 
        /// </summary>
        public string WarningLevel = string.Empty;

        /// <summary>
        /// набор символов
        /// </summary>
        public string CharacterSet = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public string WholeProgrammOptimization = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public string UseDebugLibraries = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public string UseOfMfc = "false";

        



    }

    /// <summary>
    /// 
    /// </summary>
    public class ProjectContentFile
    {
        /// <summary>
        /// Имя файла
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public bool CopyToOutputDirectory;

        /// <summary>
        /// Путь до каталога, содержащего данный файл
        /// </summary>
        public string Location = string.Empty;
    }

    ///// <summary>
    ///// Класс, экземпляры которого хранят информацию о файлах ресурсов в проекте (C#)
    ///// </summary>
    //public class ProjectResourceFile
    //{
    //    /// <summary>
    //    /// Имя файла
    //    /// </summary>
    //    public string Name = string.Empty;

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public bool CopyToOutputDirectory;

    //    /// <summary>
    //    /// Путь до каталога, содержащего данный файл
    //    /// </summary>
    //    public string Location = string.Empty;

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public string DependentUpon = string.Empty;
    //}

    /// <summary>
    /// Класс, экземпляры которого хранят информацию о файлах исходного кода в проекте (C#)
    /// </summary>
    public class ProjectSourceFile
    {
        /// <summary>
        /// Имя файла
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public bool CopyToOutputDirectory;

        /// <summary>
        /// Путь до каталога, содержащего данный файл
        /// </summary>
        public string Location = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string DependentUpon = string.Empty;
    }

    /// <summary>
    /// Класс, экземпляры которого хранят информацию о конфигурационных файлах в проекте (C#)
    /// </summary>
    public class ProjectConfigFile
    {
        /// <summary>
        /// Имя файла
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public bool CopyToOutputDirectory;

        /// <summary>
        /// Путь до каталога, содержащего данный файл
        /// </summary>
        public string Location = string.Empty;
    }

    /// <summary>
    /// Класс, экземпляры которого хранят внешние ссылки на сторонние проекты (C#)
    /// </summary>
    public class ProjectReferencedProject
    {
        /// <summary>
        /// Имя файла проекта
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// Путь до каталога, содержащего данный файл
        /// </summary>
        public string Location = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public string ProjectGuid = string.Empty;
    }

    //FIXME - учитывать версию фреймворка
    /// <summary>
    /// Класс, экземпляры которого хранят внешние ссылки на сторонние библиотеки в проекте (C#)
    /// </summary>
    public class ProjectReferencedDll
    {
        /// <summary>
        /// Имя файла библиотеки
        /// </summary>
        public string Name = string.Empty;

        /// <summary>
        /// Полный путь к файлу
        /// </summary>

        string FullPath;
        /// <summary>
        /// Путь до каталога, содержащего данный файл
        /// </summary>
        public string Location = string.Empty;
    }
}
