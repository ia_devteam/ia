﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Linq;

namespace IA.SlnResolver.VC
{
    /// <summary>
    /// Класс импорта настроек из файла прокета (Visual C++)
    /// </summary>
    public class VCProjectReader
    {
        private string projectPath;
        string DefaultConfgurationName = "Release";
        string DefaultPlatform = "Win32";
        bool lostKey = false;
        enum FileType
        {
            SourceFile,
            HeaderFile,
            ResourceFile
        }

        /// <summary>
        /// Основная функция импорта настроек из файла проекта.
        /// </summary>
        /// <param name="projectPath">Путь к файлу проекта</param>
        /// <returns>Экземпляр класса, содержащий всю информацию о настройках проекта</returns>
        public ProjectBase ReadProject(string projectPath) //пока второй параметр не используется
        {
            try
            {
                this.projectPath = Path.GetFullPath(projectPath.Replace(Path.GetFileName(projectPath), ""));
                XmlDocument doc = new XmlDocument();
                doc.Load(projectPath);
                XmlNamespaceManager mgr = new XmlNamespaceManager(doc.NameTable);


                List<ProjectSourceFile> files = GetAllFiles(doc, mgr);
                ProjectBase projectBase = new ProjectBase
                {
                    Name = projectPath.Substring(projectPath.LastIndexOf('\\') + 1),
                    Path = projectPath,
                    Settings = GetSettings(doc, mgr),
                    ReferencedProjects = GetReferences(doc, mgr),

                    SourceFiles = isSource(files),
                    HeaderFiles = isHeader(files),
                    ResourceFiles = isResource(files)

                };

                return projectBase;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
                return null;
            }
        }

        private ProjectSettings GetSettings(XmlDocument doc, XmlNamespaceManager mgr)
        {
            XmlNodeList settings_node = doc.SelectNodes("/VisualStudioProject", mgr);
            ProjectSettings settings = new ProjectSettings
            {
                ConfigurationArray = GetConfigurations(doc, mgr),
                AssemblyName = GetAttributeValue(settings_node[0], mgr, "Name")
            };

            return settings;
        }


        private List<ProjectConfiguration> GetConfigurations(XmlDocument doc, XmlNamespaceManager mgr)
        {


            List<ProjectConfiguration> Configurations = new List<ProjectConfiguration>();

            XmlNodeList configurations_node = doc.SelectNodes("/VisualStudioProject/Configurations", mgr);

            if (configurations_node != null && configurations_node[0] != null)
            {
                foreach (XmlNode child in configurations_node[0].ChildNodes)
                {
                    ProjectConfiguration configuration = new ProjectConfiguration();

                    if (child.Attributes != null)
                    {
                        configuration.ConfigurationName = GetConfigurationName(child, mgr);
                        configuration.ConfigurationPlatform = GetConfigurationPlatform(child, mgr);
                        configuration.IntermediateDirectory = GetIntermediateDirectory(child, mgr, configuration.ConfigurationName);
                        configuration.ConfigurationType = GetAttributeValue(child, mgr, "ConfigurationType");
                        configuration.CharacterSet = GetAttributeValue(child, mgr, "CharacterSet");
                        configuration.OutputDirectory = GetOutputDirectory(child, mgr, configuration);

                    }

                    Configurations.Add(configuration);
                }
            }
            SetCurrentConfiguration(Configurations);
            return Configurations;
        }



        private bool SetCurrentConfiguration(List<ProjectConfiguration> configurations)
        {
            try
            {

                List<ProjectConfiguration> AllDefaulconfigurations = configurations.FindAll(s => s.ConfigurationName == DefaultConfgurationName);
                if (AllDefaulconfigurations.Count != 0 && AllDefaulconfigurations != null)
                {
                    ProjectConfiguration q = AllDefaulconfigurations.Find(s => s.ConfigurationPlatform == DefaultPlatform);
                    if (q != null)
                    {
                        q.isCurrent = true;
                    }
                    else AllDefaulconfigurations.First().isCurrent = true;
                }
                else configurations.First().isCurrent = true;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return true;
        }








        private string GetAttributeValue(XmlNode node, XmlNamespaceManager mgr, string nameAttribute)
        {
            XmlAttributeCollection attr = node.Attributes;
            return attr[nameAttribute] == null ? string.Empty : attr[nameAttribute].Value;
        }

        private string GetIntermediateDirectory(XmlNode node, XmlNamespaceManager mgr, string nameConfiguration)
        {
            string intermediateDirectory = " ";
            if (GetAttributeValue(node, mgr, "IntermediateDirectory") != " ")
            {
                intermediateDirectory = GetAttributeValue(node, mgr, "IntermediateDirectory");
                intermediateDirectory = intermediateDirectory.Replace("$(ConfigurationName)", nameConfiguration + "\\");
            }
            return intermediateDirectory;
        }

        private string GetOutputDirectory(XmlNode node, XmlNamespaceManager mgr, ProjectConfiguration config)
        {


            string outputDirectory = GetAttributeValue(node, mgr, "OutputDirectory");
            if (outputDirectory != string.Empty)
            {
                while (isPresentEnvironmentVars(outputDirectory) && !lostKey)
                {
                    var outputDirectoryWithReplaces = ReplaceEnvVars(outputDirectory, outputDirectory, ToFormEnvironmentVariables(config));
                    if (outputDirectory.Equals(outputDirectoryWithReplaces))
                        //подстановки ничего не дали, нечего в цикле зависать. Отдать результат как есть и топать дальше.
                        break;
                    outputDirectory = outputDirectoryWithReplaces;
                }
                lostKey = false;
                outputDirectory = Path.GetFullPath(outputDirectory);
            }

            return outputDirectory;
        }



        private string ReplaceEnvVars(string replacingStr, string strWithEnvVars, IDictionary envVar)
        {
            int f_index = strWithEnvVars.IndexOf("$");
            int l_index = strWithEnvVars.IndexOf(")");
            if (f_index != -1 && l_index != -1)
            {
                string environmentVar = strWithEnvVars.Substring(f_index, l_index - f_index + 1);
                string key = environmentVar.Substring(2, environmentVar.Length - 3);

                if (envVar.Contains(key))
                {
                    string newPath = envVar[key].ToString();
                    replacingStr = strWithEnvVars.Replace(environmentVar, newPath);

                }
                else lostKey = true;
            }
            return replacingStr;
        }


        private bool isPresentEnvironmentVars(string str)
        {
            if (str.IndexOf("$(") != -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        private IDictionary ToFormEnvironmentVariables(ProjectConfiguration config)
        {
            string SolutionDir = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(projectPath), @"..\"));
            IDictionary environmentVariables = Environment.GetEnvironmentVariables();
            environmentVariables.Add("SolutionDir", SolutionDir);
            environmentVariables.Add("ConfigurationName", config.ConfigurationName);
            environmentVariables.Add("Platform", config.ConfigurationPlatform);
            environmentVariables.Add("CharacterSet", config.CharacterSet);
            environmentVariables.Add("ConfigurationType", config.ConfigurationType);
            environmentVariables.Add("PlatformName", config.ConfigurationPlatform);
            //environmentVariables.Add("PlatformToolset", config.PlatformToolset);
            environmentVariables.Add("ProjectDir", projectPath);
            /*environmentVariables.Add("ProjectExt");
             environmentVariables.Add("ProjectFileName");
             environmentVariables.Add("ProjectPath");
             environmentVariables.Add("RootNamespace");
             environmentVariables.Add("SolutionName");
             environmentVariables.Add("SolutionFileName");
             environmentVariables.Add("TargetExt");
             environmentVariables.Add("TargetName");*/


            return environmentVariables;
        }




        private string GetConfigurationName(XmlNode node, XmlNamespaceManager mgr)
        {
            string configurationName = GetAttributeValue(node, mgr, "Name");
            if (configurationName != null && configurationName != string.Empty)
            {
                try
                {
                    if (configurationName.IndexOf("|") > 0)
                        return configurationName.Substring(0, configurationName.IndexOf('|'));
                    else return configurationName;
                }
                catch (Exception)
                {
                    throw new Exception("Error in node while retrieving it's name");
                }
            }

            throw new Exception("Error in node while retrieving it's name");
        }

        private string GetConfigurationPlatform(XmlNode node, XmlNamespaceManager mgr)
        {
            if (node.Attributes["Name"] != null)
            {
                string configurationPlatform = GetAttributeValue(node, mgr, "Name");
                if (configurationPlatform != null)
                {
                    try
                    {
                        if (configurationPlatform.IndexOf("|") > 0)
                            return configurationPlatform.Substring(configurationPlatform.IndexOf('|') + 1);
                        else return " ";
                    }
                    catch (Exception)
                    {
                        throw new Exception("Error in node while retrieving it's name");
                    }
                }

            }
            throw new Exception("Error in node while retrieving it's name");
        }


        private List<ProjectReferencedDll> GetReferences(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectReferencedDll> References = new List<ProjectReferencedDll>();

            XmlNodeList references_node = doc.SelectNodes("/VisualStudioProject/References", mgr);

            if (references_node != null && references_node[0] != null)
                foreach (XmlNode child in references_node[0].ChildNodes)
                {
                    ProjectReferencedDll reference = new ProjectReferencedDll();

                    if (child.Attributes != null)
                    {
                        if (child.Attributes["RelativePath"] != null)
                            reference.RelativePath = child.Attributes["RelativePath"].InnerText;
                        if (child.Attributes["AssemblyName"] != null)
                            reference.AssemblyName = child.Attributes["AssemblyName"].InnerText;
                        if (child.Attributes["MinFrameworkVersion"] != null)
                            reference.MinFrameworkVersion = child.Attributes["MinFrameworkVersion"].InnerText;
                    }

                    References.Add(reference);
                }

            return References;
        }

        private List<ProjectSourceFile> GetAllFiles(XmlDocument doc, XmlNamespaceManager mgr)
        {

            List<ProjectSourceFile> prjSourceFiles = new List<ProjectSourceFile>();

            XmlNodeList FilterNodes = doc.SelectNodes("/VisualStudioProject/Files/Filter/File", mgr);

            prjSourceFiles.AddRange(GetFilesFromNodes(FilterNodes, mgr).ToArray());

            XmlNodeList FileNodes = doc.SelectNodes("/VisualStudioProject/Files/File", mgr);

            prjSourceFiles.AddRange(GetFilesFromNodes(FileNodes, mgr).ToArray());

            return prjSourceFiles;
        }



        private List<ProjectSourceFile> GetFilesFromNodes(XmlNodeList nodes, XmlNamespaceManager mgr)
        {

            List<ProjectSourceFile> files = new List<ProjectSourceFile>();

            if (nodes != null && nodes[0] != null)
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.Attributes["RelativePath"] != null)
                    {
                        string path = node.Attributes["RelativePath"].InnerText;
                        ProjectSourceFile file = new ProjectSourceFile
                        {
                            Name = Path.GetFileName(path),
                            Location = path.Replace(Path.GetFileName(path), "")
                        };
                        files.Add(file);
                    }
                }
            }
            return files;
        }


        private List<ProjectSourceFile> isSource(List<ProjectSourceFile> allFiles)
        {
            string[] SourceExts = new string[] { ".cpp", ".c", ".cc", ".cxx", ".def", ".odl", ".idl", ".hpj", ".bat", ".asm", ".asmx" };
            var sourceFiles = new List<ProjectSourceFile>();
            foreach (var file in allFiles)
            {
                if (SourceExts.Contains(Path.GetExtension(file.Name)))
                {
                    sourceFiles.Add(file);
                }
            }
            return sourceFiles;
        }
        private List<ProjectSourceFile> isHeader(List<ProjectSourceFile> allFiles)
        {
            string[] HeaderExts = new string[] { ".h", ".hpp", ".hxx", ".hm", ".inl", ".inc", ".xsd" };
            var headerFiles = new List<ProjectSourceFile>();
            foreach (var file in allFiles)
            {
                if (HeaderExts.Contains(Path.GetExtension(file.Name)))
                {
                    headerFiles.Add(file);
                }
            }
            return headerFiles;
        }

        private List<ProjectSourceFile> isResource(List<ProjectSourceFile> allFiles)
        {
            string[] ResourceExts = new string[] { ".rc",".ico",".cur",".bmp",".dlg",".rc2",".rct",".bin",".rgs",".gif",".jpg",".jpeg",".jpe",
                ".resx",".tiff",".tif",".png",".wav" };
            var resourceFiles = new List<ProjectSourceFile>();
            foreach (var file in allFiles)
            {
                if (ResourceExts.Contains(Path.GetExtension(file.Name)))
                {
                    resourceFiles.Add(file);
                }
            }
            return resourceFiles;
        }






        private string GetResName(string FileName)
        {
            string fileName = FileName;
            string substr = "./";
            int indx = fileName.IndexOf(substr);
            if (indx != -1)
            {
                fileName.Substring(indx + (substr.Length - 1), fileName.Length - substr.Length);
            }
            return fileName;
        }


    }
}
