﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Linq;

namespace IA.SlnResolver.CS
{
    /// <summary>
    /// Класс импорта настроек из конфигурационного файла проекта C#
    /// </summary>
    public class CSProjectReader
    {
        string DefaultConfgurationName = "release";
        string DefaultPlatform = "anycpu";
        string projectPath;

        private string GetPropertyValue(XmlDocument doc, XmlNamespaceManager mgr, string property)
        {
            XmlNode node = doc.SelectSingleNode(string.Format("/x:Project/x:PropertyGroup/x:{0}", property), mgr);
            return node == null ? string.Empty : node.InnerText;
        }

        private string GetToolVersion(XmlDocument doc, XmlNamespaceManager mgr, string property = "ToolsVersion")
        {
            XmlNode node = doc.SelectSingleNode("/x:Project", mgr);
            string attribute = " ";
            if (node != null && node.Attributes != null && node.Attributes[property] != null)
            {
                attribute = node.Attributes[property].Value;
            }
            return attribute;
        }

        //private XmlNode GetCrtConfigurationNode(XmlDocument doc, XmlNamespaceManager mgr, string configuration, string platform)
        //{
        //    string lookForValue = string.Format("'$(Configuration)|$(Platform)' == '{0}|{1}'", configuration, platform);
        //    XmlNodeList nodes = doc.SelectNodes("/x:Project/x:PropertyGroup", mgr);

        //    if (nodes != null)
        //        foreach (XmlNode node in nodes)
        //        {
        //            if (node.Attributes == null)
        //                continue;

        //            XmlAttribute attribute = node.Attributes["Condition"];

        //            if (attribute == null || string.IsNullOrEmpty(attribute.Value))
        //                continue;

        //            if (attribute.Value.Trim() == lookForValue)
        //                return node;
        //        }

        //    return null;
        //}

        private IEnumerable<XmlNode> GetAllConfigurationNodes(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<XmlNode> ret = new List<XmlNode>();

            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:PropertyGroup", mgr);
            if (nodes != null)
                foreach (XmlNode node in nodes)
                {
                    if (node.Attributes == null)
                        continue;

                    XmlAttribute attribute = node.Attributes["Condition"];

                    if (attribute == null || string.IsNullOrEmpty(attribute.Value))
                        continue;

                    if (attribute.Value.Contains("$(Configuration)"))
                        ret.Add(node);
                }
            return ret;
        }



        private string GetConfigurationNodeName(XmlNode node, XmlNamespaceManager mgr)
        {
            if (node.Attributes != null)
            {
                XmlAttribute attribute = node.Attributes["Condition"];
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value))
                {
                    try
                    {
                        string temp = attribute.Value.Substring(attribute.Value.IndexOf("==") + 2).Trim().Trim('\'');
                        if (attribute.Value.IndexOf("|") > 0)
                            return temp.Substring(0, temp.IndexOf('|')).ToLower();
                        else return temp.ToLower();
                    }
                    catch (Exception)
                    {
                        throw new Exception("Error in node while retrieving it's name");
                    }
                }

            }
            throw new Exception("Error in node while retrieving it's name");
        }

        private string GetConfigurationNodePlatform(XmlNode node)
        {
            if (node.Attributes != null)
            {
                XmlAttribute attribute = node.Attributes["Condition"];
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value))
                {
                    if (attribute.Value.IndexOf("|") > 0)
                    {
                        try
                        {
                            string temp = attribute.Value.Substring(attribute.Value.IndexOf("==") + 2).Trim().Trim('\'');
                            return temp.Substring(temp.IndexOf('|') + 1).ToLower();
                        }
                        catch (Exception)
                        {
                            throw new Exception("Error in node while retrieving it's name");
                        }
                    }
                    return DefaultPlatform;
                }

            }
            throw new Exception("Error in node while retrieving it's configuration");
        }






        private string GetCrtConfigurationValue(XmlNode crtConfigurationNode, XmlNamespaceManager mgr, string property)
        {
            XmlNode node = crtConfigurationNode.SelectSingleNode(string.Format("./x:{0}", property), mgr);
            return node == null ? string.Empty : node.InnerText;
        }


        private List<ProjectReferencedDll> GetReferencedDlls(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectReferencedDll> references = new List<ProjectReferencedDll>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup/x:Reference", mgr);
            if (nodes != null)
                foreach (XmlNode child in nodes)
                {
                    ProjectReferencedDll refDll = new ProjectReferencedDll();

                    if (child.Attributes != null)
                    {
                        refDll.Name = child.Attributes["Include"].InnerText.Split(',')[0];
                        refDll.FullName = child.Attributes["Include"].InnerText;
                    }

                    XmlNode node = child.SelectSingleNode("./x:HintPath", mgr);

                    if (node != null)
                        refDll.Location = node.InnerText;

                    references.Add(refDll);
                }

            if (references.Count == 0)
            {
                ProjectReferencedDll refDll = new ProjectReferencedDll();
                refDll.Name = "System";
                refDll.FullName = "System";
            }

            return references;
        }

        private List<ProjectContentFile> GetContentFiles(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectContentFile> files = new List<ProjectContentFile>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup/x:Content", mgr);
            if (nodes != null)
                foreach (XmlNode child in nodes)
                {
                    ProjectContentFile file = MakeFileFromInclude<ProjectContentFile>(mgr, child);
                    files.Add(file);
                }
            return files;
        }

        private List<ProjectSourceFile> GetSourceFiles(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectSourceFile> files = new List<ProjectSourceFile>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup/x:Compile", mgr);

            if (nodes != null)
                foreach (XmlNode child in nodes)
                {
                    ProjectSourceFile file = MakeFileFromInclude<ProjectSourceFile>(mgr, child);

                    if (file.Name.Contains("?") || file.Name.Contains("*"))
                    {
                        var mask = Path.GetFileName(file.Name);
                        var projFilePath = Path.GetDirectoryName(doc.BaseURI.Substring("fileL///".Length));
                        var filesWOmask = Directory.GetFiles(Path.Combine(projFilePath, file.Location), mask, SearchOption.TopDirectoryOnly);
                        files.AddRange(filesWOmask.Select(x =>
                            new ProjectSourceFile()
                            {
                                Name = x,
                                Location = file.Location,
                                DependentUpon = file.DependentUpon,
                                CopyToOutputDirectory = file.CopyToOutputDirectory
                            }));
                    }
                    else
                        files.Add(file);
                }
            return files;
        }

        private List<ProjectReferencedProject> GetReferencedProjects(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectReferencedProject> files = new List<ProjectReferencedProject>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup/x:ProjectReference", mgr);

            if (nodes != null)
                foreach (XmlNode child in nodes)
                {
                    if (child.Attributes == null)
                        continue;

                    ProjectReferencedProject file = new ProjectReferencedProject { Location = child.Attributes["Include"].InnerText };
                    XmlNode node = child.SelectSingleNode("./x:Name", mgr);

                    if (node != null)
                        file.Name = node.InnerText;

                    files.Add(file);
                }
            return files;
        }

        private List<ProjectResourceFile> GetResourceFiles(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectResourceFile> files = new List<ProjectResourceFile>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup/x:EmbeddedResource", mgr);

            if (nodes != null)
                foreach (XmlNode child in nodes)
                {
                    ProjectResourceFile file = MakeFileFromInclude<ProjectResourceFile>(mgr, child);

                    files.Add(file);
                }
            return files;
        }


        private bool SetCurrentConfiguration(List<ProjectConfiguration> configurations)
        {
            try
            {

                List<ProjectConfiguration> AllDefaulconfigurations = configurations.FindAll(s => s.ConfigurationName == DefaultConfgurationName);
                if (AllDefaulconfigurations.Count != 0 && AllDefaulconfigurations != null)
                {
                    ProjectConfiguration q = AllDefaulconfigurations.Find(s => s.ConfigurationPlatform == DefaultPlatform);
                    if (q != null)
                    {
                        q.isCurrent = true;
                    }
                    else AllDefaulconfigurations.First().isCurrent = true;
                }
                else
                    if (configurations.Count > 0)
                        configurations.First().isCurrent = true;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return true;
        }



        private List<ProjectConfigFile> GetConfigFiles(XmlDocument doc, XmlNamespaceManager mgr)
        {
            List<ProjectConfigFile> files = new List<ProjectConfigFile>();
            XmlNodeList nodes = doc.SelectNodes("/x:Project/x:ItemGroup/x:None", mgr);

            if (nodes != null)
                foreach (XmlNode child in nodes)
                {
                    ProjectConfigFile file = MakeFileFromInclude<ProjectConfigFile>(mgr, child);

                    files.Add(file);
                }
            return files;
        }

        private int GetExistingConf(ProjectConfiguration newConf, List<ProjectConfiguration> prjCnfList)
        {
            return prjCnfList.FindIndex(s => (s.ConfigurationName == newConf.ConfigurationName && s.ConfigurationPlatform == newConf.ConfigurationPlatform));
        }

        private ProjectSettings GetSettings(XmlDocument doc, XmlNamespaceManager mgr)
        {
            ProjectSettings settings = new ProjectSettings();


            IEnumerable<XmlNode> allConfigurationNodes = GetAllConfigurationNodes(doc, mgr);
            // XmlNode crtConfigurationNode = GetCrtConfigurationNode(doc, mgr, configuration, platform);

            List<ProjectConfiguration> prjConfigurationList = new List<ProjectConfiguration>();

            foreach (XmlNode node in allConfigurationNodes)
            {

                string configuration = GetConfigurationNodeName(node, mgr);
                string platform = GetConfigurationNodePlatform(node);


                ProjectConfiguration newConf = new ProjectConfiguration
                {
                    ConfigurationName = GetConfigurationNodeName(node, mgr),
                    ConfigurationPlatform = GetConfigurationNodePlatform(node),
                    CompilationConstants = GetCrtConfigurationValue(node, mgr, "DefineConstants"),
                    Optimize = GetCrtConfigurationValue(node, mgr, "Optimize"),
                    OutputPath = GetCrtConfigurationValue(node, mgr, "OutputPath"),
                    DocumentationPath = GetCrtConfigurationValue(node, mgr, "DocumentationFile"),
                    PlatformTarget = GetCrtConfigurationValue(node, mgr, "PlatformTarget")
                };

                int ExistConf = GetExistingConf(newConf, prjConfigurationList);
                if (ExistConf != -1)
                {

                    prjConfigurationList[ExistConf] = newConf;
                }
                else
                {
                    prjConfigurationList.Add(newConf);
                }


            }

            SetCurrentConfiguration(prjConfigurationList);
            settings.ConfigurationArray = prjConfigurationList;
            settings.AppDesignerFolder = GetPropertyValue(doc, mgr, "AppDesignerFolder");
            settings.AssemblyName = GetPropertyValue(doc, mgr, "AssemblyName");

            //FileAlignment was added after .Net 2.0, so it is missing 
            //in projects created with Visual Studio 2005. The default 
            //(512) should be used.
            settings.FileAlignment = GetPropertyValue(doc, mgr, "FileAlignment");
            if (string.IsNullOrEmpty(settings.FileAlignment)) settings.FileAlignment = "512";
            settings.OutputType = GetPropertyValue(doc, mgr, "OutputType");
            settings.ProductVersion = GetPropertyValue(doc, mgr, "ProductVersion");
            settings.ProjectGuid = GetPropertyValue(doc, mgr, "ProjectGuid");
            settings.RootNamespace = GetPropertyValue(doc, mgr, "RootNamespace");
            settings.SchemaVersion = GetPropertyValue(doc, mgr, "SchemaVersion");
            settings.TargetFrameworkVersion = GetPropertyValue(doc, mgr, "TargetFrameworkVersion");
            if (string.IsNullOrEmpty(settings.TargetFrameworkVersion)) settings.TargetFrameworkVersion = "v4.0";
            //settings.WarningLevel = GetCrtConfigurationValue(crtConfigurationNode, mgr, "WarningLevel");
            settings.ToolVersion = GetToolVersion(doc, mgr);
            return settings;
        }

        /// <summary>
        /// Основная функция импорта настроек из файла проекта.
        /// </summary>
        /// <param name="filePath">Путь к файлу проекта</param>
        /// <param name="resolvePath"></param>
        /// <returns>Экземпляр класса, содержащий всю информацию о настройках проекта</returns>
        public ProjectBase ReadProject(string filePath, bool resolvePath = false)
        {
            try
            {
                projectPath = Path.GetDirectoryName(filePath);
                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlNamespaceManager mgr = new XmlNamespaceManager(doc.NameTable);
                mgr.AddNamespace("x", "http://schemas.microsoft.com/developer/msbuild/2003");
                ProjectBase basicProject = new ProjectBase
                {
                    Name = filePath.Substring(filePath.LastIndexOf('\\') + 1),
                    Path = filePath,
                    Settings = GetSettings(doc, mgr),
                    ReferencedDlls = GetReferencedDlls(doc, mgr),
                    ContentFiles = GetContentFiles(doc, mgr),
                    SourceFiles = GetSourceFiles(doc, mgr),
                    ResourceFiles = GetResourceFiles(doc, mgr),
                    ConfigFiles = GetConfigFiles(doc, mgr),
                    ReferencedProjects = GetReferencedProjects(doc, mgr)
                };

                //добавляем самого себя
                if (basicProject.Settings.OutputType == "Library")
                    basicProject.ReferencedProjects.Add(new ProjectReferencedProject { Name = basicProject.Settings.AssemblyName, Location = filePath });

                if (resolvePath)
                {
                    foreach (ProjectReferencedDll reference in basicProject.ReferencedDlls)
                        reference.Location = resPath(filePath, reference.Location);

                    foreach (ProjectSourceFile reference in basicProject.SourceFiles)
                        if (!reference.Name.Contains(":"))
                            reference.Location = resPath(filePath, reference.Location != string.Empty ? reference.Location + "\\" + reference.Name : reference.Name);
                        else
                        {
                            reference.Location = Path.GetFullPath(reference.Name);
                            if (reference.Name.Contains("\\"))
                                reference.Name = reference.Name.Substring(reference.Name.LastIndexOf("\\") + 1);
                        }

                    foreach (ProjectReferencedProject reference in basicProject.ReferencedProjects)
                        reference.Location = resPath(filePath, reference.Location);

                    foreach (ProjectConfiguration conf in basicProject.Settings.ConfigurationArray)
                        conf.OutputPath = resPath(filePath, conf.OutputPath);
                }

                return basicProject;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
                return null;
            }
        }

        private string resPath(string projectPath, string additivePath)
        {
            if (additivePath.Contains(":"))
                return additivePath;

            return additivePath == "" ? string.Empty : Path.GetFullPath(projectPath.Replace("/", "\\").Substring(0, projectPath.LastIndexOf("\\") + 1) + additivePath);
        }

        private static T MakeFileFromInclude<T>(XmlNamespaceManager mgr, XmlNode child) where T : ProjectBaseFile, new()
        {
            T file = new T();

            if (child.Attributes != null)
            {
                string filename = child.Attributes["Include"].InnerText;

                int index = -1;

                if (filename.Contains(Path.DirectorySeparatorChar.ToString()))
                    index = filename.LastIndexOf(Path.DirectorySeparatorChar);

                file.Name = filename.Substring(index + 1);
                file.Location = filename.Substring(0, index + 1);
            }

            var node = child.SelectSingleNode("./x:CopyToOutputDirectory", mgr);

            if (node != null && file is ProjectBaseFileWithOutputDir)
                (file as ProjectBaseFileWithOutputDir).CopyToOutputDirectory = true;

            node = child.SelectSingleNode("./x:DependentUpon", mgr);

            if (node != null && file is ProjectBaseFileWithOutputDirAndDependent)
                (file as ProjectBaseFileWithOutputDirAndDependent).DependentUpon = node.InnerText;

            return file;
        }

    }
}
