﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;


namespace IA.SlnResolver
{
    /// <summary>
    /// 
    /// </summary>
    public class SolutionVS
    {
        
    /// <summary>
    /// Импорт списка проектов C# в решении
    /// </summary>
    /// <param name="fileName">Путь к файлу решения</param>
    /// <returns>Список C# проектов в решении</returns>
        public List<string> GetProjects(string fileName)
        {
            //возращаемый список проектов
            List<string> projectList = new List<string>();

            //если такого файла нет возращаем пустой список
            if (!System.IO.File.Exists(fileName))
                return projectList;

            DirectoryInfo solutionDirectory = Directory.GetParent(fileName);
            
            string solutionDirectoryPath = solutionDirectory + @"\";

            FileInfo solutionFile = new FileInfo(fileName);

            //если это не файл решения, то фозвращаем пустой список
            if (solutionFile.Extension != ".sln")
                return projectList;

            using (StreamReader sr = new StreamReader(fileName))
            {
                string str;
                while ((str = sr.ReadLine()) != null)
                {
                    if (!str.StartsWith("Project"))
                        continue;

                    string[] sa = str.Split('=');

                    //проверка на разумность
                    if (sa.Length < 2)
                        continue;

                    string[] sa1 = sa[1].Split(',');

                    //проверка на разумность
                    if (sa1.Length < 2)
                        continue;

                    string s1 = sa1[1].Trim().Replace("\"", "");
                    string sext = new string(s1.Skip(s1.LastIndexOf('.')).ToArray());

                    //если речь идёт не о проекте С# то продолжаем
                    if (sext != ".csproj")
                        continue;

                    DirectoryInfo directory = Directory.GetParent(solutionDirectoryPath + s1);
                    FileInfo file = new FileInfo(solutionDirectoryPath + s1);
                    projectList.Add(directory + @"\" + file.Name);
                }
            }
            return projectList;
        }

        /// <summary>
        /// Импорт списка проектов C# и VC++ в решении
        /// </summary>
        /// <param name="fileName">Путь к файлу решения</param>
        /// <returns>список проектов C# VC++ </returns>
        /// 
        public List<string> GetAllProjects(string fileName)
        {
            //возращаемый список проектов
            List<string> projectList = new List<string>();

            //если такого файла нет возращаем пустой список
            if (!System.IO.File.Exists(fileName))
                return projectList;
            

            DirectoryInfo solutionDirectory = Directory.GetParent(fileName);

            string solutionDirectoryPath = solutionDirectory + @"\";

            FileInfo solutionFile = new FileInfo(fileName);

            if (solutionFile.Extension != ".sln")
                return projectList;

            using (StreamReader sr = new StreamReader(fileName))
            {
                string str;
                while ((str = sr.ReadLine()) != null)
                {
                    if (!str.StartsWith("Project"))
                        continue;

                    string[] sa = str.Split('=');

                    //проверка на разумность
                    if (sa.Length < 2)
                        continue;

                    string[] sa1 = sa[1].Split(',');

                    //проверка на разумность
                    if (sa1.Length < 2)
                        continue;

                    string s1 = sa1[1].Trim().Replace("\"", "");
                    string sext = new string(s1.Skip(s1.LastIndexOf('.')).ToArray());

                    //если речь идёт не о проекте С# или VC++ то продолжаем
                    if (sext != ".csproj" && sext != ".vcxproj" && sext!=".vcproj")
                        continue;

                    DirectoryInfo directory = Directory.GetParent(solutionDirectoryPath + s1);
                    FileInfo file = new FileInfo(solutionDirectoryPath + s1);
                    projectList.Add(directory + @"\" + file.Name);
                }
            }
            return projectList;
        }
    }

    #region Старый парсинг проектов
    public class ProjectVS_Old
    {
        public string ProjectName;
        public ProjectVS_Old(string projectName)
        {
            ProjectName = projectName;
        }
        public List<string> GetDefines
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                xr.SearchByPattern("Project/PropertyGroup/DefineConstants/#text", false);
                return xr.GetResults;
            }
        }
        public string GetDefinesConf
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                xr.SearchByPattern("Project/PropertyGroup/DefineConstants/#text", false);
                String res = "";
                XmlResolver_Old xr0 = new XmlResolver_Old(ProjectName);
                String defConf = "";
                xr0.SearchByPattern("Project/PropertyGroup/Configuration/#text", false);
                defConf = xr0.GetResults[0] + "|";
                xr0.SearchByPattern("Project/PropertyGroup/Platform/#text", false);
                defConf += xr0.GetResults[0];
                //находим все конфигурации
                XmlResolver_Old xr3 = new XmlResolver_Old(ProjectName);
                xr3.SearchByPatternWithChilds("Project/PropertyGroup:Condition", false);
                foreach (string confString in xr3.GetResults)
                {
                    if (confString.Contains(defConf))
                    {
                        //необходимая конфигурация
                        foreach (string defs in xr.GetResults)
                        {
                            if (confString.Contains(defs) && defs.Length > res.Length)
                                res = defs;
                        }
                    }
                }
                return res;
            }
        }
        public List<string> GetConfigurations
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                xr.SearchByPatternWithChilds("Project/PropertyGroup:Condition", false);
                return xr.GetResults;
            }
        }
        public List<string> GetLibraries
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                xr.SearchByPattern("Project/ItemGroup/Reference:Include", false);
                return xr.GetResults;
            }
        }
        public List<string> GetLibrariesAdd
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                List<string> ret = new List<string>();
                xr.SearchByPatternWithChilds("Project/ItemGroup/Reference:Include", false);
                foreach (string outStr in xr.GetResults)
                {
                    //фича, если указаны нормальные пути, но ВС очень неадекватна на такие вещи
                    if (outStr.Contains("~") && outStr.Split('~').Length == 2 && outStr.Contains(":")) //FIXME самая дурацкая проверка
                    {
                        string[] strArr = outStr.Split('~');
                        string resStr = strArr[0] + "~" + Path.GetFullPath(ProjectName.Substring(0, ProjectName.LastIndexOf("\\") + 1) + strArr[1]);
                        ret.Add(resStr);
                    }
                    else
                    {
                        ret.Add(outStr);
                    }
                }


                List<string> lsOutPaths = GetOutputsConf;
                XmlResolver_Old xr2 = new XmlResolver_Old(ProjectName);
                xr2.SearchByPattern("Project/PropertyGroup/AssemblyName/#text", false);
                string sProject = xr2.GetResults[0];
                ret.Add(sProject + "~" + lsOutPaths[0] + sProject + ".dll");

                return ret;
                //return xr.GetResults;
            }
        }
        public List<string> GetModules
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                xr.SearchByPattern("Project/ItemGroup/Compile:Include", true);
                return xr.GetResults;
            }
        }
        public List<string> GetOutputs
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                xr.SearchByPattern("Project/PropertyGroup/OutputPath/#text", true);
                return xr.GetResults;
            }
        }
        public List<string> GetOutputsConf
        {
            get
            {
                String loc = "";
                //FIXME !!! проблема индексов
                XmlResolver_Old xr0 = new XmlResolver_Old(ProjectName);
                String defConf = "";
                xr0.SearchByPattern("Project/PropertyGroup/Configuration/#text", false);
                defConf = xr0.GetResults[0] + "|";
                xr0.SearchByPattern("Project/PropertyGroup/Platform/#text", false);
                defConf += xr0.GetResults[0];
                //находим все конфигурации
                XmlResolver_Old xr3 = new XmlResolver_Old(ProjectName);
                xr3.SearchByPatternWithChilds("Project/PropertyGroup:Condition", false);
                foreach (string confString in xr3.GetResults)
                {
                    if (confString.Contains(defConf) && (confString.Contains("\\") || confString.Contains("/")))
                    {
                        loc = confString.Split('~')[confString.Split('~').Length - 1];
                        break;
                    }
                }
                
                if (loc != "")
                {
                    //нашли строку
                    string prjPath = ProjectName.Substring(0, ProjectName.LastIndexOf("\\")+1);
                    loc = System.IO.Path.GetFullPath(prjPath + loc);
                    List<string> ret = new List<string>();
                    ret.Add(loc);
                    return ret;
                }
                else
                {
                    XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                    xr.SearchByPattern("Project/PropertyGroup/OutputPath/#text", true);
                    return xr.GetResults;
                }
            }
        }
        public List<string> GetRefProjects
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                xr.SearchByPattern("Project/ItemGroup/ProjectReference:Include", true);
                return xr.GetResults;
            }
        }
        public List<string> getCurrentConf
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                List<string> res = new List<string>();
                xr.SearchByPattern("Project/PropertyGroup/Configuration/#text", false);
                res.AddRange(xr.GetResults);
                xr.SearchByPattern("Project/PropertyGroup/Platform/#text", false);
                res.AddRange(xr.GetResults);
                return res;
            }
        }
        public List<string> GetRefDlls
        {
            get
            {

                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                List<string> lsRefProj = GetRefProjects;
                List<string> lsRet = new List<string>();
                foreach (string s in lsRefProj)
                {
                    XmlResolver_Old xr1 = new XmlResolver_Old(s);
                    xr1.SearchByPattern("Project/PropertyGroup/OutputPath/#text", true);
                    List<string> lsOutPaths = xr1.GetResults;
                    XmlResolver_Old xr2 = new XmlResolver_Old(s);
                    xr2.SearchByPattern("Project/PropertyGroup/AssemblyName/#text", false);
                    string sProject = xr2.GetResults[0];
                    foreach (string s1 in lsOutPaths)
                    {
                        string stmp = s1 + sProject;
                        DirectoryInfo di = Directory.GetParent(s1 + sProject);
                        FileInfo fi = new FileInfo(stmp);
                        stmp = di.ToString() + @"\" + fi.Name;
                        if (!lsRet.Contains(stmp))
                        {
                            lsRet.Add(stmp);
                        }

                    }
                }
                return lsRet;
            }
        }
        public List<string> GetRefDllsConf
        {
            get
            {

                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                List<string> lsRefProj = GetRefProjects;
                List<string> lsRet = new List<string>();
                foreach (string s in lsRefProj)
                {
                    string loc = "";
                    //находим текущую конфигурацию
                    //FIXME !!! проблема индексов
                    XmlResolver_Old xr0 = new XmlResolver_Old(s);
                    String defConf = "";
                    xr0.SearchByPattern("Project/PropertyGroup/Configuration/#text", false);
                    defConf = xr0.GetResults[0] + "|";
                    xr0.SearchByPattern("Project/PropertyGroup/Platform/#text", false);
                    defConf += xr0.GetResults[0];
                    //находим все конфигурации
                    XmlResolver_Old xr3 = new XmlResolver_Old(s);
                    xr3.SearchByPatternWithChilds("Project/PropertyGroup:Condition", false);
                    foreach (string confString in xr3.GetResults)
                    {
                        if (confString.Contains(defConf) && (confString.Contains("\\") || confString.Contains("/")))
                        {
                            loc = confString.Split('~')[confString.Split('~').Length - 1];
                            break;
                        }
                    }
                    if (loc == "")
                        continue;//если не нашли
                    if (loc.StartsWith(".."))
                        loc = Path.GetFullPath(Directory.GetParent(s).ToString() + "\\" + loc);
                    List<string> lsOutPaths = new List<string>();
                    lsOutPaths.Add(loc);
                    XmlResolver_Old xr2 = new XmlResolver_Old(s);
                    xr2.SearchByPattern("Project/PropertyGroup/AssemblyName/#text", false);
                    string sProject = xr2.GetResults[0];
                    foreach (string s1 in lsOutPaths)
                    {
                        string stmp = s1 + sProject;
                        DirectoryInfo di = Directory.GetParent(s1 + sProject);
                        FileInfo fi = new FileInfo(stmp);
                        stmp = sProject + "~" + di.ToString() + @"\" + fi.Name + ".dll";
                        if (!lsRet.Contains(stmp))
                        {
                            lsRet.Add(stmp);
                        }

                    }
                }
                return lsRet;
            }
        }
        public List<string> GetProjFramework
        {
            get
            {
                XmlResolver_Old xr = new XmlResolver_Old(ProjectName);
                xr.SearchByPattern("Project/PropertyGroup/TargetFrameworkVersion/#text", false);
                if(xr.GetResults.Count == 0)
                    return new List<string>{"v4.0"};
                return xr.GetResults;
            }
        }
    }
    public class XmlResolver_Old
    {
        string XmlName;
        List<string> SearchPattern = new List<string>();
        List<string> SearchResult = new List<string>();
        List<string> AttributeResult = new List<string>();

        public XmlResolver_Old(string xn)
        {
            XmlName = xn;
        }

        public void SearchByPatternWithChilds(string sPattern, bool bAddPath)
        {
            string fileName = XmlName;
            string selectedPath = "";
            if (bAddPath)
            {
                DirectoryInfo di = Directory.GetParent(fileName);
                selectedPath = di.ToString() + @"\";
            }

            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.LoadXml(System.IO.File.ReadAllText(fileName));
            }
            catch (Exception)
            {
                return;
            }
            XmlNode root = xDoc.DocumentElement;
            if (root == null)
                return;
            SearchResult.Clear();
            AttributeResult.Clear();
            List<string> sSP = new List<string>();
            List<string> sSA = new List<string>();
            string SearchAttribute = "";
            sSP.Add(root.Name);
            string[] st = sPattern.Split(':');
            SearchPattern = st[0].Split('/').ToList();
            if (st.Length > 1)
                SearchAttribute = st[1];

            FindFromAllChildWithChilds(root, sSP, sSA, SearchAttribute,"");
            if (SearchResult.Count != AttributeResult.Count)
            {
                return;
            }
            if (bAddPath)
                for (int i = 0; i < SearchResult.Count; i++)
                {
                    string stmp = SearchResult[i];
                    stmp = selectedPath + SearchResult[i];
                    DirectoryInfo di1 = Directory.GetParent(stmp);
                    FileInfo fi = new FileInfo(stmp);
                    SearchResult[i] = di1 + @"\" + fi.Name;
                }

        }
        public void SearchByPattern(string sPattern, bool bAddPath)
        {
            string fileName = XmlName;
            string selectedPath = "";
            if (bAddPath)
            {
                DirectoryInfo di = Directory.GetParent(fileName);
                selectedPath = di.ToString() + @"\";
            }

            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.LoadXml(System.IO.File.ReadAllText(fileName));
            }
            catch (Exception)
            {
                return;
            }
            XmlNode root = xDoc.DocumentElement;
            if (root == null)
                return;
            SearchResult.Clear();
            AttributeResult.Clear();
            List<string> sSP = new List<string>();
            List<string> sSA = new List<string>();
            string SearchAttribute="";
            sSP.Add(root.Name);
            string[] st = sPattern.Split(':');
            SearchPattern = st[0].Split('/').ToList();
            if (st.Length > 1)
                SearchAttribute = st[1];
            FindFromAllChild(root, sSP, sSA, SearchAttribute);
            if (SearchResult.Count != AttributeResult.Count)
            {
                return;
            }
            if(bAddPath)
            for (int i = 0; i < SearchResult.Count; i++)
            {
                string stmp = SearchResult[i];
                if (stmp.Contains("*") || stmp.Contains("?"))
                {
                    var dir = Path.GetDirectoryName(selectedPath);
                    var subdir = Path.GetDirectoryName(stmp);
                    var mask = Path.GetFileName(stmp);
                    var files = Directory.EnumerateFiles(Path.Combine(dir, subdir), mask, SearchOption.TopDirectoryOnly);
                    SearchResult.AddRange(files.Select(x => Path.GetFullPath(Path.Combine(dir, x))));
                    SearchResult.RemoveAt(i);
                    i--;
                    continue;
                }
                if (File.Exists(stmp)) continue;
                stmp = selectedPath + SearchResult[i];
                DirectoryInfo di1 = Directory.GetParent(stmp);
                FileInfo fi = new FileInfo(stmp);
                SearchResult[i] = di1 + @"\" + fi.Name;
            }
        }
        public List<string> GetResults
        {
            get
            {
                return SearchResult;
            }
        }
        public List<string> GetAttributes
        {
            get
            {
                return AttributeResult;
            }
        }


        public void FindFromAllChild(XmlNode xNode, List<string> sSP, List<string> sSA, string sAttr)
        {

            if (SearchPattern.SequenceEqual(sSP))
            {
                //Теперь нужно взять значение или атрибут
                if (xNode.Value != null)
                    SearchResult.Add(xNode.Value);
                else
                {
                    if (xNode.Attributes != null)
                    {
                        foreach (XmlAttribute attr in xNode.Attributes)
                        {
                            if (attr.Name == sAttr)
                                SearchResult.Add(attr.Value);
                        }
                    }
                }
                if (sSA.Count > 0)
                    AttributeResult.Add(sSA.Aggregate((first, second) => first + "~" + second));
                return;
            }
            else
            {
                if (xNode.HasChildNodes)
                {
                    foreach (XmlNode xN in xNode.ChildNodes)
                    {
                        sSP.Add(xN.Name);
                        if (xNode.Value != null)
                            sSA.Add(xNode.Value);
                        else
                        {
                            if (xNode.Attributes != null && xNode.Attributes.Count != 0)
                            {
                                List<string> ls = new List<string>();
                                foreach (XmlAttribute attr in xNode.Attributes)
                                {
                                    ls.Add(attr.Name + ":" + attr.Value);
                                }
                                sSA.Add(ls.Aggregate((first, second) => first + "+" + second));
                            }
                            else
                                sSA.Add("");
                        }
                        FindFromAllChild(xN, sSP, sSA, sAttr);
                        sSP.RemoveAt(sSP.Count - 1);
                        sSA.RemoveAt(sSA.Count - 1);
                    }
                }

            }
        }
        public void FindFromAllChildWithChilds(XmlNode xNode, List<string> sSP, List<string> sSA, string sAttr, string sCurrResult)
        {

            if (SearchPattern.SequenceEqual(sSP.Take(SearchPattern.Count())))
            {
                if (xNode.Value != null)
                    if(sCurrResult!="")
                        sCurrResult += "~" + xNode.Value;
                    else
                        sCurrResult +=  xNode.Value;
                else
                {
                    if (xNode.Attributes != null)
                    {
                        foreach (XmlAttribute attr in xNode.Attributes)
                        {
                            if (attr.Name == sAttr)
                                if (sCurrResult != "")
                                    sCurrResult +="~"+ attr.Value;
                                else
                                    sCurrResult += attr.Value;
                        }
                    }
                }

                if (xNode.HasChildNodes)
                {
                    foreach (XmlNode xN in xNode.ChildNodes)
                    {
                        sSP.Add(xN.Name);
                        if (xNode.Value != null)
                            sSA.Add(xNode.Value);
                        else
                        {
                            if (xNode.Attributes != null && xNode.Attributes.Count != 0)
                            {
                                List<string> ls = new List<string>();
                                foreach (XmlAttribute attr in xNode.Attributes)
                                {
                                    ls.Add(attr.Name + ":" + attr.Value);
                                }
                                sSA.Add(ls.Aggregate((first, second) => first + "+" + second));
                            }
                            else
                                sSA.Add("");
                        }
                        FindFromAllChildWithChilds(xN, sSP, sSA, sAttr,sCurrResult);
                        sSP.RemoveAt(sSP.Count - 1);
                        sSA.RemoveAt(sSA.Count - 1);
                    }
                }
                else
                {

                    SearchResult.Add(sCurrResult);
/*
                    //Теперь нужно взять значение или атрибут
                    if (xNode.Value != null)
                        SearchResult.Add(sCurrResult + "~" + xNode.Value);
                    else
                    {
                        if (xNode.Attributes != null)
                        {
                            foreach (XmlAttribute attr in xNode.Attributes)
                            {
                                if (attr.Name == sAttr)
                                    SearchResult.Add(sCurrResult + "~"+attr.Value);
                            }
                        }
                    }
 */ 
                    if (sSA.Count > 0)
                        AttributeResult.Add(sSA.Aggregate((first, second) => first + "~" + second));
                    return;
                }
            }
            else
            {
                if (xNode.HasChildNodes)
                {
                    foreach (XmlNode xN in xNode.ChildNodes)
                    {
                        sSP.Add(xN.Name);
                        if (xNode.Value != null)
                            sSA.Add(xNode.Value);
                        else
                        {
                            if (xNode.Attributes != null && xNode.Attributes.Count != 0)
                            {
                                List<string> ls = new List<string>();
                                foreach (XmlAttribute attr in xNode.Attributes)
                                {
                                    ls.Add(attr.Name + ":" + attr.Value);
                                }
                                sSA.Add(ls.Aggregate((first, second) => first + "+" + second));
                            }
                            else
                                sSA.Add("");
                        }
                        FindFromAllChildWithChilds(xN, sSP, sSA, sAttr, sCurrResult);
                        sSP.RemoveAt(sSP.Count - 1);
                        sSA.RemoveAt(sSA.Count - 1);
                    }
                }

            }
        }

    }
    #endregion
}
