﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;
using IA.SlnResolver.CS;
using IA.SlnResolver.VC;
using IA.SlnResolver.VCX;
using IOController;

using SlnResolver = IA.SlnResolver;

namespace TestSlnResolver
{


    /// <summary>
    /// Данный класс тестов направлен на проверку правильности определния настроек конфигураций
    /// для разных типов проектов, а именно на проекты, созданные в разных студиях
    /// VS2008 VS2010 VS2011 VS2012 VS2013
    /// </summary>
    [TestClass]
    public class TestDefineCurrentConfiguration
    {
                     

        #region переменные для тестирования CSPROJ
        /// <summary>
        /// имя конфигурации Debug
        /// </summary>
        private static string cnfDebug = "debug";

        /// <summary>
        /// имя конфигурации Release
        /// </summary>
        private static string cnfRealease = "release";
        
        /// <summary>
        /// платформа AnyCPU
        /// </summary>
        private static string platformAnyCPU = "anycpu";
        /// <summary>
        /// платформа x64
        /// </summary>
        private static string platform_x64 = "x64";
        /// <summary>
        /// платформа x86
        /// </summary>
        private static string platform_x86 = "x86";

        /// <summary>
        /// соответвие версий VisualStudio
        /// </summary>
        private static string VS2008 = "3.5";
        private static string VS2010 = "4.0";
        private static string VS2011 = "4.5";
        private static string VS2012_2013 = "12.0";
        /// <summary>
        /// Список версий студий
        /// </summary>
        private static string[] VS_ToolVersion = { VS2008, VS2010, VS2011, VS2012_2013 };




        #endregion


        #region Для запуска теста

        /// <summary>
        /// Путь к файлу теста
        /// </summary>   
        private string sourceFile = string.Empty;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;
              
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private static TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        
        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
            //Путь к файлу теста
            sourceFile = testUtils.TestMatirialsDirectoryPath;
            //Проверяем существует ли такой
            bool exist = Directory.Exists(sourceFile);
            Assert.IsTrue(exist, "Directory not exist");      
        }

       #endregion


        #region  тесты на верное определение конфигураций С# Reader

        


        /// <summary>
        /// Данный тест проверяет верное чтение файла проекта
        /// при отсутствии заданных конфигураций
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2013
        ///_[Конфигурация в шапке]= нет
        ///_[Платформа в шапке или 0 - если нет]= нет
        ///_[CrtConfig]= нет
        ///_[CrtPltf] = нет
        ///_[явное задание платформы в описании конфигурации]= не заданы
        ///_[Количество конфигураций]= 0
        ///_[порядок конфигурации Release|AnyCPU  ] = 0    
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration1()
        {  
           //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
           string expectedConfiguration = cnfRealease;
           string expectedPlatform = platformAnyCPU;   
      
           //файл C# проекта
           string source_file = Path.Combine(sourceFile, @"TestSlnResolver\test5.csproj");
           //читаем
           CSProjectReader reader = new CSProjectReader();          
           var proj =  reader.ReadProject(source_file,true);
           //если файл проекта удалось прочитать, проверяем верно ли прочитал.
           Assert.IsNotNull(proj);
           if (proj != null)
           {
               SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
               Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
               Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
           }
           
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
               DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// Данный тест проверяет верное чтение файла проекта
        /// при отсутвии явно заданных платформ и отсутвию ReleseAnyCPU
        /// 
        /// комментарии к содержанию тестового файла:
        /// Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= нет
        ///_[Платформа в шапке или 0 - если нет]= нет
        ///_[CrtConfig]= Release
        ///_[CrtPltf] = AnyCPU
        ///_[явное задание платформы в описании конфигурации]= 0
        ///_[Количество конфигураций]= 4
        ///_[порядок конфигурации Release|AnyCPU  ] = 0 
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration2()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platform_x64;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\AistImporter.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2013
        ///_[Конфигурация в шапке]= debud
        ///_[Платформа в шапке или 0 - если нет]= AnyCPU
        ///_[CrtConfig]= Release
        ///_[CrtPltf] = x64
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 4
        ///_[порядок конфигурации Release|AnyCPU  ] = 0 
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration3()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platform_x64;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\AistReportsReader.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= anyCpu
        ///_[CrtConfig]= Release
        ///_[CrtPltf] = AnyCpu
        ///_[явное задание платформы в описании конфигурации]= test  и release  не заданы
        ///_[Количество конфигураций]= 5
        ///_[порядок конфигурации Release|AnyCPU  ] = 2 
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration4()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\BinaryRightsChecker.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= x86
        ///_[CrtConfig]= Release
        ///_[CrtPltf] = x86
        ///_[явное задание платформы в описании конфигурации]= ok
        ///_[Количество конфигураций]= 2
        ///_[порядок конфигурации Release|AnyCPU  ] = 2 
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration5()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platform_x86;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\ConsoleClient.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2008
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= any cpu
        ///_[CrtConfig]= Release
        ///_[CrtPltf] = AnyCPU
        ///_[явное задание платформы в описании конфигурации]= ok
        ///_[Количество конфигураций]= 2
        ///_[порядок конфигурации Release|AnyCPU  ] = 2 
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration6()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\ConsumingDb4oPerformanceCounters.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }


        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2008
        ///_[Конфигурация в шапке]= no
        ///_[Платформа в шапке или 0 - если нет]= no
        ///_[CrtConfig]= DebugSilverlight
        ///_[CrtPltf] = AnyCPU
        ///_[явное задание платформы в описании конфигурации]= release x86 |release AnyCPU
        ///_[Количество конфигураций]= 3
        ///_[порядок конфигурации Release|AnyCPU  ] = 3
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration7()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = "debugsilverlight";
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\Core (Silverlight).csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= no
        ///_[Платформа в шапке или 0 - если нет]= no
        ///_[CrtConfig]= ReleaseWithoutTests
        ///_[CrtPltf] = x64
        ///_[явное задание платформы в описании конфигурации]= debux - | - anycpu|release -
        ///_[Количество конфигураций]= 5
        ///_[порядок конфигурации Release|null  ] = 5
        /// </summary>        
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration8()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfDebug;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\CSharpParser.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= anycpu
        ///_[CrtConfig]= debug
        ///_[CrtPltf] = x86
        ///_[явное задание платформы в описании конфигурации]= debug не задан
        ///_[Количество конфигураций]= 3
        ///_[порядок конфигурации Release|null  ] = 1
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration9()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = "develog";
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\EmitMapper.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= anycpu
        ///_[CrtConfig]= release
        ///_[CrtPltf] = anyCpu
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 4
        ///_[порядок конфигурации Release|null  ] = 1
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration10()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfDebug;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\FileStatistics.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= anycpu
        ///_[CrtConfig]= release
        ///_[CrtPltf] = x64
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 5
        ///_[порядок конфигурации Release|null  ] = 2
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration11()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platform_x64;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\HTMLWriter.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2013
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= x86
        ///_[CrtConfig]= release
        ///_[CrtPltf] = anyCpu
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 3
        ///_[порядок конфигурации Release|null  ] = 1
        /// </summary>        
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration12()
        {
             //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\IAWaitWindow.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }


        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= anycpu
        ///_[CrtConfig]= release
        ///_[CrtPltf] = anyCpu
        ///_[явное задание платформы в описании конфигурации]= 2
        ///_[Количество конфигураций]= 5
        ///_[порядок конфигурации Release|null  ] = 2
        /// </summary>
        [TestMethod]      
        public void SlnResolver_TestCheckConfiguration13()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\ICSharpCode.NRefactory.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= 0
        ///_[Платформа в шапке или 0 - если нет]= 0
        ///_[CrtConfig]= release
        ///_[CrtPltf] = X64
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 4
        ///_[порядок конфигурации Release|null  ] = 1
        /// </summary>       
        [TestMethod]       
        public void SlnResolver_TestCheckConfiguration14()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platform_x64;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\ICSharpCode.SharpDevelop.Dom.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= anycpu
        ///_[CrtConfig]= release
        ///_[CrtPltf] = anyCpu
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 4
        ///_[порядок конфигурации Release|null  ] = 1
        /// </summary>        
        [TestMethod]        
        public void SlnResolver_TestCheckConfiguration15()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform =platform_x64;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\IdentifyFileTypes.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= anycpu
        ///_[CrtConfig]= debug
        ///_[CrtPltf] = anyCpu
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 4
        ///_[порядок конфигурации Release|null  ] = 1
        /// </summary>
      
        [TestMethod]       
        public void SlnResolver_TestCheckConfiguration16()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\itextsharp.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }


        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2008
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= anycpu
        ///_[CrtConfig]= release
        ///_[CrtPltf] = anyCpu
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 2
        ///_[порядок конфигурации Release|null  ] = 2
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration17()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\LoggingBlock.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= anycpu
        ///_[CrtConfig]= release
        ///_[CrtPltf] = anyCpu
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 2
        ///_[порядок конфигурации Release|null  ] = 2
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration18()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\LoggingBlock-2010.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2008
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= х86
        ///_[CrtConfig]= release
        ///_[CrtPltf] = х86
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 2
        ///_[порядок конфигурации Release|null  ] = 2
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration19()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platform_x86;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\NewCecilPerfTest.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2008
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= AnyCPU
        ///_[CrtConfig]= release
        ///_[CrtPltf] = AnyCPU
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 2
        ///_[порядок конфигурации Release|null  ] = 2
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration20()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\OMAddin.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }


        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= AnyCPU
        ///_[CrtConfig]= release
        ///_[CrtPltf] = AnyCPU
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 2
        ///_[порядок конфигурации Release|null  ] = 2
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration21()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\OMAddin-2010.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }
        
        /// <summary>
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2010
        ///_[Конфигурация в шапке]= debug
        ///_[Платформа в шапке или 0 - если нет]= х86
        ///_[CrtConfig]= release
        ///_[CrtPltf] = AnyCPU
        ///_[явное задание платформы в описании конфигурации]= 1
        ///_[Количество конфигураций]= 4
        ///_[порядок конфигурации Release|null  ] = 2
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestCheckConfiguration22()
        {
            //задаем значения конфигурации и платформы, которые должны быть текущими (определено вручную из файла)
            string expectedConfiguration = cnfRealease;
            string expectedPlatform = platformAnyCPU;

            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\Phalanger.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {
                SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(expectedConfiguration, currentConf.ConfigurationName, "Not Correct");
                Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }
        
        /// <summary>
        /// проверка чтения файла с отсутсвием описанием конфигураций - такой проект не должен запускаться
        /// комментарии к содержанию тестового файла:
        /// //Test_[Версия студии]=2008
        ///_[Конфигурация в шапке]= 0
        ///_[Платформа в шапке или 0 - если нет]= 0
        ///_[CrtConfig]= 0
        ///_[CrtPltf] = 0
        ///_[явное задание платформы в описании конфигурации]= 0
        ///_[Количество конфигураций]= 0
        ///_[порядок конфигурации Release|null  ] = 0
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestReadWrongConfigFile()
        {
            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\no config\no config.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj);
            if (proj != null)
            {

                Assert.IsNull(proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true));
               
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// Сравнение чтения двух файлов в разных версиях
        /// </summary>        
        [TestMethod]
        public void SlnResolver_TestComparisonTwoFilesInDiffVersions()
        {

            //файл C# проекта в 2008
            string source_file1 = Path.Combine(sourceFile, @"TestSlnResolver\comparison\OMPlus.csproj");
            //файл C# проекта в 2010
            string source_file2 = Path.Combine(sourceFile, @"TestSlnResolver\comparison\OMPlus-2010.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj1 = reader.ReadProject(source_file1, true);
            var proj2 = reader.ReadProject(source_file2, true);
            //если файлы проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsNotNull(proj1);
            Assert.IsNotNull(proj2);
            if (proj1 != null && proj2!=null)
            {
                //проверяем совпадают ли текущие конфигурации
                SlnResolver.CS.ProjectConfiguration currentConf1 = proj1.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                SlnResolver.CS.ProjectConfiguration currentConf2 = proj2.Settings.ConfigurationArray.Find(s => s.isCurrent == true);
                Assert.AreEqual(currentConf1.ConfigurationName, currentConf2.ConfigurationName, "Not Correct");
                Assert.AreEqual(currentConf1.ConfigurationPlatform, currentConf2.ConfigurationPlatform, "Not Correct");
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }


        List<string> TakeProjectFilesFromtestFolder()
        {
            List<string> projectFilesPaths = new List<string>();

            string[] dirs = Directory.GetFiles( Path.Combine(sourceFile , @"TestSlnResolver"), "*.csproj");

            foreach (string dir in dirs)
            {
                projectFilesPaths.Add(dir);
            }
            string[] dirs2 = Directory.GetFiles(Path.Combine(sourceFile , @"TestSlnResolver\tests"), "*.csproj");
            foreach (string dir in dirs2)
            {
                projectFilesPaths.Add(dir);
            }
            return projectFilesPaths;
        }

        /// <summary>
        ///Тест на чтение набора тестовых файлов проектов и способность опредления конфигураций
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestReadingTestProjectsFiles()
        {
            
            CSProjectReader reader = new CSProjectReader();
            List<string> projectFilesPaths = TakeProjectFilesFromtestFolder();
            int expectNumFiles = 210;
            Assert.AreEqual(expectNumFiles, projectFilesPaths.Count);

            Dictionary<SlnResolver.CS.ProjectBase, List<SlnResolver.CS.ProjectConfiguration>> projectDictionary = new Dictionary<SlnResolver.CS.ProjectBase, List<SlnResolver.CS.ProjectConfiguration>>();
            foreach (string project in projectFilesPaths)
            {
               var readPr = reader.ReadProject(project,true);
               projectDictionary.Add(readPr, readPr.Settings.ConfigurationArray);
               Assert.IsNotNull(readPr.Settings.ConfigurationArray.Find(s => s.isCurrent == true));
               Assert.IsNotNull(readPr.Settings.ConfigurationArray.Find(s => s.isCurrent == true));
            }
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);        
        }

        /// <summary>
        /// Тест на проверку чтения конфигурационного файла c#  проекта
        /// с двумя одинаковыми конфигурациями ( в одну были дополнены изменения) - одинаковые
        /// означают совпадение по именам конфигураций и платформам
        /// 
        /// Две одинаковые конфигурации - это одна, с изменениями
        /// В данном тесте проверяем совпадение по количеству прочитанных платформ
        /// по имени и платформе текущей
        /// по выходному пути текущей платформы
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestReadingConfigFileWithTwosameConf()
        {
            //файл C# проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\cSharperParser.csproj");
            //читаем
            CSProjectReader reader = new CSProjectReader();
            var proj = reader.ReadProject(source_file, true);
            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            int expectCountCnf = 2;
            string expectedPath = Path.GetFullPath(Path.Combine(sourceFile, @"..\..\..\Binaries\"));
            string expectedName = cnfRealease;
            string expectedPlatform = platformAnyCPU;

             SlnResolver.CS.ProjectConfiguration currentConf = proj.Settings.ConfigurationArray.Find(s => s.isCurrent == true);

             Assert.AreEqual(expectCountCnf, proj.Settings.ConfigurationArray.Count);
             Assert.AreEqual(expectedPath, currentConf.OutputPath);
             Assert.AreEqual(expectedName, currentConf.ConfigurationName);
             Assert.AreEqual(expectedPlatform, currentConf.ConfigurationPlatform);
             
            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                 DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);

        }

        #endregion


        #region тесты на чтение настроек C++  проектов     

        #region Тесты для проверки чтения Slnresolver_VCXProjects

        /// <summary>
        /// Формирование исходных файлов для VCХ проектов
        /// </summary>
        /// <param name="nameFiles">массив имен файлов</param>
        /// <returns>список файлов</returns>
        List<SlnResolver.VCX.ProjectSourceFile> ToFormVCXProjectFiles(string[] nameFiles)
        {
            List<SlnResolver.VCX.ProjectSourceFile> prjFiles = new List<SlnResolver.VCX.ProjectSourceFile>();

            foreach (string file in nameFiles)
            {
                SlnResolver.VCX.ProjectSourceFile newFile = new SlnResolver.VCX.ProjectSourceFile
                {
                    Name = Path.GetFileName(file),
                    Location = file.Replace(Path.GetFileName(file), "")
                };
                prjFiles.Add(newFile);
            }
            return prjFiles;
        }
        /// <summary>
        /// Проверка совпадения проекта с ожидаемым
        /// </summary>
        /// <param name="readPrj">проект для проверки</param>
        /// <param name="expectedPrj">ожидаемый проект</param>
        /// <returns>результат сравнения</returns>
        bool CompareVCXProjects(SlnResolver.VCX.ProjectBase readPrj, SlnResolver.VCX.ProjectBase expectedPrj)
        {         
                Assert.AreEqual(readPrj.Name, expectedPrj.Name, "Не совпадает имя");
                Assert.AreEqual(readPrj.Path, expectedPrj.Path, "Не совпадает путь");                
                Assert.IsTrue(CompareVCXSetting(readPrj.Settings, expectedPrj.Settings), "Не совпадают настройки");
                Assert.IsTrue(CompareVCXFiles(readPrj.SourceFiles, expectedPrj.SourceFiles), "Не совпадают Source файлы");
                Assert.IsTrue(CompareVCXFiles(readPrj.HeaderFiles, expectedPrj.HeaderFiles), "Не совпадают Header файлы");
                Assert.IsTrue(CompareVCXFiles(readPrj.ResourceFiles, expectedPrj.ResourceFiles), "Не совпадают Resource файлы");
                Assert.IsTrue(CompareVCXRefPrj(readPrj.ReferencedProjects, expectedPrj.ReferencedProjects), "Не совпадают зависимые проекты");
                Assert.IsTrue(CompareVCXRefDllsPrj(readPrj.ReferencedDlls, expectedPrj.ReferencedDlls), "Не совпадают ReferenceDLLs");
                return true;           
        }
        /// <summary>
        /// Сравнение VC настроек
        /// </summary>
        /// <param name="readPrj">настройки сформированные SlnResolver</param>
        /// <param name="expectedPrj">эталонные настройки </param>
        /// <returns></returns>
        bool CompareVCXSetting(SlnResolver.VCX.ProjectSettings readPrj, SlnResolver.VCX.ProjectSettings expectedPrj)
        {
                Assert.AreEqual(readPrj.ToolVersion, expectedPrj.ToolVersion, "Не совпадает ToolVersion");
                Assert.AreEqual(readPrj.ProjectGuid, expectedPrj.ProjectGuid, "Не совпадает projectGuid");
                Assert.AreEqual(readPrj.RootNamespace, expectedPrj.RootNamespace, "Не совпадает RootNamespace");
                Assert.IsTrue(CompareVCXConfigurations(readPrj.ConfigurationArray, expectedPrj.ConfigurationArray), "Неверно определены конфигурации");
                return true;          
        }
        /// <summary>
        /// Сравнение VC настроек
        /// </summary>
        /// <param name="readPrj">настройки сформированные SlnResolver</param>
        /// <param name="expectedPrj">эталонные настройки </param>
        /// <returns></returns>
        bool CompareVCXConfigurations(List<SlnResolver.VCX.ProjectConfiguration> readPrj, List<SlnResolver.VCX.ProjectConfiguration> expectedPrj)
        {
             Assert.AreEqual(readPrj.Count(), expectedPrj.Count(), "Не совпадает количество конфигураций");

                foreach (SlnResolver.VCX.ProjectConfiguration cnfg in expectedPrj)
                {
                       SlnResolver.VCX.ProjectConfiguration curCnfg = readPrj.Find(f => (f.ConfigurationName == cnfg.ConfigurationName && f.ConfigurationPlatform == cnfg.ConfigurationPlatform));
                        Assert.AreEqual(curCnfg.ConfigurationType, cnfg.ConfigurationType, "Не совпадает ConfigurationtеType");
                        Assert.AreEqual(curCnfg.PlatformToolset, cnfg.PlatformToolset, "Не совпадает PlatrformToolset");
                        Assert.AreEqual(curCnfg.Optimize, cnfg.Optimize, "Не совпадает Optimize");
                        Assert.AreEqual(curCnfg.OutputPath, cnfg.OutputPath, "Не совпадает OutputPath");
                        Assert.AreEqual(curCnfg.AssemblyName, cnfg.AssemblyName, "Не совпадает AssemblyName");
                        Assert.AreEqual(curCnfg.TargetExt, cnfg.TargetExt, "Не совпадает TargetExt");
                        Assert.AreEqual(curCnfg.isCurrent, cnfg.isCurrent, "Не совпадает метка текущей конфигурации");
                        Assert.AreEqual(curCnfg.WarningLevel, cnfg.WarningLevel, "Не совпадает WarningLevel");
                        Assert.AreEqual(curCnfg.CharacterSet, cnfg.CharacterSet, "Не совпадает CharacterSet");
                        Assert.AreEqual(curCnfg.UseDebugLibraries, cnfg.UseDebugLibraries, "Не совпадает useDebugLibraries");
                        Assert.AreEqual(curCnfg.UseOfMfc, cnfg.UseOfMfc, "Не совпадает UseOfMfc");                   
                }
                return true;          
        }
        /// <summary>
        /// Сравнение VC настроек
        /// </summary>
        /// <param name="readPrj">настройки сформированные SlnResolver</param>
        /// <param name="expectedPrj">эталонные настройки </param>
        /// <returns></returns>
        bool CompareVCXFiles(List<SlnResolver.VCX.ProjectSourceFile> readPrj, List<SlnResolver.VCX.ProjectSourceFile> expectedPrj)
        {

                Assert.AreEqual(readPrj.Count(), expectedPrj.Count(), "Не совпадает количество файлов");
                foreach (SlnResolver.VCX.ProjectSourceFile expFile in expectedPrj)
                {
                        SlnResolver.VCX.ProjectSourceFile curFile = readPrj.Find(f => (f.Name == expFile.Name));
                        Assert.AreEqual(curFile.Location, expFile.Location, "Не совпадают пути к файлу");                
                                                          
                }
                return true;
        }
        /// <summary>
        /// Сравнение VC настроек
        /// </summary>
        /// <param name="readPrj">настройки сформированные SlnResolver</param>
        /// <param name="expectedPrj">эталонные настройки </param>
        /// <returns></returns>
        bool CompareVCXRefPrj(List<SlnResolver.VCX.ProjectReferencedProject> readPrj, List<SlnResolver.VCX.ProjectReferencedProject> expectedPrj)
        {
            if (readPrj.Count() == expectedPrj.Count())
            {
                return true;
            }
            Assert.Fail("Не совпадает количество зависимых проектов");
            return false;
        }
        /// <summary>
        /// Сравнение VC настроек
        /// </summary>
        /// <param name="readPrj">настройки сформированные SlnResolver</param>
        /// <param name="expectedPrj">эталонные настройки </param>
        /// <returns></returns>
        bool CompareVCXRefDllsPrj(List<SlnResolver.VCX.ProjectReferencedDll> readPrj, List<SlnResolver.VCX.ProjectReferencedDll> expectedPrj)
        {
            if (readPrj.Count() == expectedPrj.Count())
            {

                return true;
            }
            Assert.Fail("Не совпадает количество зависимых проектов");
            return false;
        }

       
        /// <summary>
        /// Тест на правильность чтения настроек VCX-проекта 
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestingReadingVCXProject1()
        {        
            //файл C++ проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\VCXPROJ\MinH\build\VC10\libMinHook.vcxproj");
            string source_path = Path.GetFullPath(source_file.Replace(Path.GetFileName(source_file), ""));
            //читаем
            VCXProjectReader reader = new VCXProjectReader();
            var readingPrj = reader.ReadProject(source_file);

            #region Исходные данные

            #region Исходные конфигурации
              
            SlnResolver.VCX.ProjectConfiguration expectedCnf1  = new SlnResolver.VCX.ProjectConfiguration
              {
                ConfigurationName = "Debug",
                ConfigurationPlatform = "Win32",
                ConfigurationType = "StaticLibrary",
                PlatformToolset = string.Empty,
                Optimize = "Disabled",
                OutputPath = Path.GetFullPath(Path.Combine(source_file, @"..\..\lib\Debug\")),
                AssemblyName = "libMinHook.x86",
                TargetExt = string.Empty,
                isCurrent = false,
                WarningLevel = "Level3",
                CharacterSet = "Unicode",
                WholeProgrammOptimization = "false",
                UseDebugLibraries = string.Empty,
                UseOfMfc = string.Empty
              };

              SlnResolver.VCX.ProjectConfiguration expectedCnf2  = new SlnResolver.VCX.ProjectConfiguration
              {
                ConfigurationName = "Debug",
                ConfigurationPlatform = "x64",
                ConfigurationType = "StaticLibrary",
                PlatformToolset = string.Empty,
                Optimize = "Disabled",
                OutputPath= Path.GetFullPath(Path.Combine(source_file, @"..\..\lib\Debug\")),
                AssemblyName = "libMinHook.x64",
                TargetExt = string.Empty,
                isCurrent = false,
                WarningLevel = "Level3",
                CharacterSet = "Unicode",
                WholeProgrammOptimization = "false",
                UseDebugLibraries = string.Empty,
                UseOfMfc = string.Empty
              };

              SlnResolver.VCX.ProjectConfiguration expectedCnf3 = new SlnResolver.VCX.ProjectConfiguration
              {
                  ConfigurationName = "Release",
                  ConfigurationPlatform = "Win32",
                  ConfigurationType = "StaticLibrary",
                  PlatformToolset = string.Empty,
                  Optimize = "MinSpace",
                  OutputPath = Path.GetFullPath(Path.Combine(source_file, @"..\..\lib\Release\")),
                  AssemblyName = "libMinHook.x86",
                  TargetExt = string.Empty,
                  isCurrent = true,
                  WarningLevel = "Level3",
                  CharacterSet = "Unicode",
                  WholeProgrammOptimization = "true",
                  UseDebugLibraries = string.Empty,
                  UseOfMfc = string.Empty
              };

              SlnResolver.VCX.ProjectConfiguration expectedCnf4 = new SlnResolver.VCX.ProjectConfiguration
              {
                  ConfigurationName = "Release",
                  ConfigurationPlatform = "x64",
                  ConfigurationType = "StaticLibrary",
                  PlatformToolset = string.Empty,
                  Optimize = "MinSpace",
                  OutputPath = Path.GetFullPath(Path.Combine(source_file, @"..\..\lib\Release\")),
                  AssemblyName = "libMinHook.x64",
                  TargetExt = string.Empty,
                  isCurrent = false,
                  WarningLevel = "Level3",
                  CharacterSet = "Unicode",
                  WholeProgrammOptimization = "true",
                  UseDebugLibraries = string.Empty,
                  UseOfMfc = string.Empty
              };


             List<SlnResolver.VCX.ProjectConfiguration> expectedCnfArray = new List<SlnResolver.VCX.ProjectConfiguration>();
             expectedCnfArray.Add(expectedCnf1);
             expectedCnfArray.Add(expectedCnf2);
             expectedCnfArray.Add(expectedCnf3);
             expectedCnfArray.Add(expectedCnf4);
            #endregion

            #region Исходные файлы

            string[] sources = new string[]
            { @"..\..\src\buffer.c",
              @"..\..\src\hook.c",
              @"..\..\src\trampoline.c",     
              @"..\..\src\HDE\hde32.c",  
              @"..\..\src\HDE\hde64.c",
                      
            };

            string[] headers = new string[]
            {
              @"..\..\src\buffer.h",
              @"..\..\src\trampoline.h",
              @"..\..\src\HDE\hde32.h",  
              @"..\..\src\HDE\pstdint.h",
              @"..\..\src\HDE\table32.h",
              @"..\..\src\HDE\table64.h",
              @"..\..\src\HDE\table64.h",
              @"..\..\include\MinHook.h"  
            };

            string[] resourses = new string []
            {

            };

         List<SlnResolver.VCX.ProjectSourceFile> expectedSourceFiles = ToFormVCXProjectFiles (sources);
         List<SlnResolver.VCX.ProjectSourceFile> expectedHeaderFiles = ToFormVCXProjectFiles (headers);
         List<SlnResolver.VCX.ProjectSourceFile> expectedResourceFiles = ToFormVCXProjectFiles (resourses);
         
         #endregion       

            #region ReferencePrj
            List<SlnResolver.VCX.ProjectReferencedProject> expectedReferencePrj = new List<SlnResolver.VCX.ProjectReferencedProject>();
            #endregion       

            #region ReferenceDLLs
            List<SlnResolver.VCX.ProjectReferencedDll> expectedReferenceDLLs = new List<SlnResolver.VCX.ProjectReferencedDll>();
            #endregion      

            #region Settings
            SlnResolver.VCX.ProjectSettings expectedSettings = new SlnResolver.VCX.ProjectSettings
            {
                ToolVersion = "4.0",
                ProjectGuid = "{F142A341-5EE0-442D-A15F-98AE9B48DBAE}",
                RootNamespace = "libMinHook",
                ConfigurationArray = expectedCnfArray
            };
            #endregion                
            
            SlnResolver.VCX.ProjectBase excpectedPrj = new SlnResolver.VCX.ProjectBase 
            {
                Name = "libMinHook.vcxproj",
                Path = source_file,
                Settings = expectedSettings,
                ReferencedProjects = expectedReferencePrj,
                SourceFiles = expectedSourceFiles,
                HeaderFiles = expectedHeaderFiles,
                ResourceFiles = expectedResourceFiles,
                ReferencedDlls = expectedReferenceDLLs
            };

            #endregion

            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsTrue(CompareVCXProjects(readingPrj,excpectedPrj));

        }

        /// <summary>
        /// Тест на правильность чтения настроек VCX-проекта 
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestingReadingVCXProject2()
        {

            //файл C++ проекта
            
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\VCXPROJ\MinH\build\VC11\MinHook.vcxproj");
            string source_path = Path.GetFullPath(source_file.Replace(Path.GetFileName(source_file), ""));
            //читаем
            VCXProjectReader reader = new VCXProjectReader();
            var readingPrj = reader.ReadProject(source_file);

            #region Исходные данные

            #region Исходные конфигурации
            SlnResolver.VCX.ProjectConfiguration expectedCnf1 = new SlnResolver.VCX.ProjectConfiguration
            {
                ConfigurationName = "Debug",
                ConfigurationPlatform = "Win32",
                ConfigurationType = "DynamicLibrary",
                PlatformToolset = "v110_xp",
                Optimize = "Disabled",
                OutputPath =  Path.GetFullPath(Path.Combine(source_file, @"..\..\bin\Debug\")),
                AssemblyName = "MinHook.x86",
                TargetExt = string.Empty,
                isCurrent = false,
                WarningLevel = "Level3",
                CharacterSet = "Unicode",
                WholeProgrammOptimization = "",
                UseDebugLibraries = string.Empty,
                UseOfMfc = string.Empty

            };
            SlnResolver.VCX.ProjectConfiguration expectedCnf2 = new SlnResolver.VCX.ProjectConfiguration
            {

                ConfigurationName = "Debug",
                ConfigurationPlatform = "x64",
                ConfigurationType = "DynamicLibrary",
                PlatformToolset = "v110_xp",
                Optimize = "Disabled",
                OutputPath =  Path.GetFullPath(Path.Combine(source_file, @"..\..\bin\Debug\")),
                AssemblyName = "MinHook.x64",
                TargetExt = string.Empty,
                isCurrent = false,
                WarningLevel = "Level3",
                CharacterSet = "Unicode",
                WholeProgrammOptimization = "",
                UseDebugLibraries = string.Empty,
                UseOfMfc = string.Empty
            };
            SlnResolver.VCX.ProjectConfiguration expectedCnf3 = new SlnResolver.VCX.ProjectConfiguration
            {

                ConfigurationName = "Release",
                ConfigurationPlatform = "Win32",
                ConfigurationType = "DynamicLibrary",
                PlatformToolset = "v110_xp",
                Optimize = "MinSpace",
                OutputPath = Path.GetFullPath(Path.Combine(source_file, @"..\..\bin\Release\")),
                AssemblyName = "MinHook.x86",
                TargetExt = string.Empty,
                isCurrent = true,
                WarningLevel = "Level3",
                CharacterSet = "Unicode",
                WholeProgrammOptimization = "true",
                UseDebugLibraries = string.Empty,
                UseOfMfc = string.Empty
            };
            SlnResolver.VCX.ProjectConfiguration expectedCnf4 = new SlnResolver.VCX.ProjectConfiguration
            {

                ConfigurationName = "Release",
                ConfigurationPlatform = "x64",
                ConfigurationType = "DynamicLibrary",
                PlatformToolset = "v110_xp",
                Optimize = "MinSpace",
                OutputPath = Path.GetFullPath(Path.Combine(source_file, @"..\..\bin\Release\")),
                AssemblyName = "MinHook.x64",
                TargetExt = string.Empty,
                isCurrent = false,
                WarningLevel = "Level3",
                CharacterSet = "Unicode",
                WholeProgrammOptimization = "true",
                UseDebugLibraries = string.Empty,
                UseOfMfc = string.Empty
            };


            List<SlnResolver.VCX.ProjectConfiguration> expectedCnfArray = new List<SlnResolver.VCX.ProjectConfiguration>();
            expectedCnfArray.Add(expectedCnf1);
            expectedCnfArray.Add(expectedCnf2);
            expectedCnfArray.Add(expectedCnf3);
            expectedCnfArray.Add(expectedCnf4);
            #endregion

            #region Исходные файлы
            string[] sources = new string[]
            {
            //@"..\..\src\buffer.c",
            //  @"..\..\src\hook.c",
            //  @"..\..\src\trampoline.c",     
            //  @"..\..\src\HDE\hde32.c",  
            //  @"..\..\src\HDE\hde64.c",                      
            };

            string[] headers = new string[]
            {
              //@"..\..\src\buffer.h",
              //@"..\..\src\trampoline.h",
              //@"..\..\src\HDE\hde32.h",  
              //@"..\..\src\HDE\pstdint.h",
              //@"..\..\src\HDE\table32.h",
              //@"..\..\src\HDE\table64.h",
              //@"..\..\src\HDE\table64.h",
              //@"..\..\include\MinHook.h"  
            };

            string[] resourses = new string[]
            {
                @"..\..\dll_resources\MinHook.rc"
            };

            List<SlnResolver.VCX.ProjectSourceFile> expectedSourceFiles = ToFormVCXProjectFiles(sources);
            List<SlnResolver.VCX.ProjectSourceFile> expectedHeaderFiles = ToFormVCXProjectFiles(headers);
            List<SlnResolver.VCX.ProjectSourceFile> expectedResourceFiles = ToFormVCXProjectFiles(resourses);
            #endregion

            #region ReferencePrj

            List<SlnResolver.VCX.ProjectReferencedProject> expectedReferencePrj = new List<SlnResolver.VCX.ProjectReferencedProject>();

            #endregion

            #region ReferenceDLLs

            List<SlnResolver.VCX.ProjectReferencedDll> expectedReferenceDLLs = new List<SlnResolver.VCX.ProjectReferencedDll>();
            expectedReferenceDLLs.Add(new SlnResolver.VCX.ProjectReferencedDll { Name = "", Location = "" });
            expectedReferenceDLLs.Add(new SlnResolver.VCX.ProjectReferencedDll { Name = "", Location = "" });
            expectedReferenceDLLs.Add(new SlnResolver.VCX.ProjectReferencedDll { Name = "", Location = "" });
            expectedReferenceDLLs.Add(new SlnResolver.VCX.ProjectReferencedDll { Name = "", Location = "" });
            #endregion

            #region Settings
            SlnResolver.VCX.ProjectSettings expectedSettings = new SlnResolver.VCX.ProjectSettings
            {
                ToolVersion = "4.0",
                ProjectGuid = "{027FAC75-3FDB-4044-8DD0-BC297BD4C461}",
                RootNamespace = "MinHook",
                ConfigurationArray = expectedCnfArray
            };

            #endregion

            SlnResolver.VCX.ProjectBase excpectedPrj = new SlnResolver.VCX.ProjectBase
            {
                Name = "MinHook.vcxproj",
                Path = source_file,
                Settings = expectedSettings,
                ReferencedProjects = expectedReferencePrj,
                SourceFiles = expectedSourceFiles,
                HeaderFiles = expectedHeaderFiles,
                ResourceFiles = expectedResourceFiles,
                ReferencedDlls = expectedReferenceDLLs
            };

            #endregion

            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsTrue(CompareVCXProjects(readingPrj, excpectedPrj));
        }


        #endregion

        #region Тесты для проверки чтения Slnresolver_VCProject
         /// <summary>
        /// Формирование исходных файлов для VC проектов
        /// </summary>
        /// <param name="nameFiles">массив имен файлов</param>
        /// <returns>список файлов</returns>
        List<SlnResolver.VC.ProjectSourceFile> ToFormProjectFiles (string [] nameFiles)
        {
            List<SlnResolver.VC.ProjectSourceFile> prjFiles= new List<SlnResolver.VC.ProjectSourceFile>();

            foreach (string file in nameFiles)
            {
                SlnResolver.VC.ProjectSourceFile newFile = new SlnResolver.VC.ProjectSourceFile
                {
                    Name = Path.GetFileName(file),
                    Location = file.Replace(Path.GetFileName(file),"")
                };
                prjFiles.Add(newFile);
            }
            return prjFiles;
        }     

        /// <summary>
        /// Сравнение VC проектов
        /// </summary>
        /// <param name="readPrj">проект сформированный SlnResolver</param>
        /// <param name="expectedPrj">эталонный проект</param>
        /// <returns></returns>
        bool CompareVCProjects (SlnResolver.VC.ProjectBase readPrj, SlnResolver.VC.ProjectBase expectedPrj)
        {         
            Assert.AreEqual(readPrj.Name,expectedPrj.Name,"Не совпадает имя");
            Assert.AreEqual(readPrj.Path,expectedPrj.Path,"Не совпадает путь");
            Assert.IsTrue (CompareVCSetting (readPrj.Settings, expectedPrj.Settings),"Не совпадают настрйоки");
            Assert.IsTrue(CompareVCFiles(readPrj.SourceFiles, expectedPrj.SourceFiles),"Не совпадают Source файлы");
            Assert.IsTrue(CompareVCFiles (readPrj.HeaderFiles, expectedPrj.HeaderFiles),"Не совпадают Header файлы");
            Assert.IsTrue (CompareVCFiles(readPrj.ResourceFiles, expectedPrj.ResourceFiles ),"Не совпадают Resource файлы" );
            Assert.IsTrue (CompareVCRefPrj(readPrj.ReferencedProjects, expectedPrj.ReferencedProjects),"Не совпадают зависимые проекты");
            return true;                     
        }

        /// <summary>
        /// Сравнение VC настроек
        /// </summary>
        /// <param name="readPrj">настройки сформированные SlnResolver</param>
        /// <param name="expectedPrj">эталонные настройки </param>
        /// <returns></returns>
        bool CompareVCSetting (SlnResolver.VC.ProjectSettings readPrj, SlnResolver.VC.ProjectSettings expectedPrj)
        {           
            Assert.AreEqual (readPrj.AssemblyName,expectedPrj.AssemblyName,"Не совпадают AssemblyName");
            Assert.IsTrue(CompareVCConfigurations( readPrj.ConfigurationArray, expectedPrj.ConfigurationArray),"Неверно определены конфигурации");
                return true;            
        }

        /// <summary>
        /// Сравнение VC конфигураций
        /// </summary>
        /// <param name="readPrj">конфигурации сформированные SlnResolver</param>
        /// <param name="expectedPrj">эталонные конфигурации</param>
        /// <returns></returns>
        bool CompareVCConfigurations(List<SlnResolver.VC.ProjectConfiguration> readPrj, List<SlnResolver.VC.ProjectConfiguration> expectedPrj)
        {

            Assert.AreEqual(readPrj.Count(), expectedPrj.Count(), "Не совпадает количество конфигураций");

            foreach (SlnResolver.VC.ProjectConfiguration cnfg in expectedPrj)
            {
                SlnResolver.VC.ProjectConfiguration curCnfg = readPrj.Find(f => (f.ConfigurationName == cnfg.ConfigurationName && f.ConfigurationPlatform == cnfg.ConfigurationPlatform));
                Assert.AreEqual(curCnfg.CharacterSet, cnfg.CharacterSet, "Не совпадает CharacterSet");
                Assert.AreEqual(curCnfg.ConfigurationType, cnfg.ConfigurationType, "Не совпадает ConfigurationType");
                //Assert.AreEqual(curCnfg.IntermediateDirectory,cnfg.IntermediateDirectory,"Не совпадает IntermediateDirectory");
                Assert.AreEqual(curCnfg.OutputDirectory, cnfg.OutputDirectory, "Не совпадает OutputDirectory");
            }
            return true;
        }

        /// <summary>
        /// Сравнение VC файлов
        /// </summary>
        /// <param name="readPrj">файлы сформированные SlnResolver</param>
        /// <param name="expectedPrj">эталонные файлы</param>
        /// <returns></returns>
        bool CompareVCFiles (List<SlnResolver.VC.ProjectSourceFile> readPrj, List<SlnResolver.VC.ProjectSourceFile> expectedPrj)
        {
                 Assert.AreEqual (readPrj.Count(),expectedPrj.Count(), "Не совпадает количество файлов");
                 foreach (SlnResolver.VC.ProjectSourceFile expFile in expectedPrj)
                 {
                        SlnResolver.VC.ProjectSourceFile curFile = readPrj.Find(f=> (f.Name==expFile.Name));
                        Assert.AreEqual(curFile.Location, expFile.Location,"Не совпадают путиr к файлу");
                    
                 }
                 return true;  
        }

        /// <summary>
        /// Сравнение VC зависимых преоктов
        /// </summary>
        /// <param name="readPrj">зависимые проекты сформированные SlnResolver</param>
        /// <param name="expectedPrj">эталонные зависимые проекты</param>
        /// <returns></returns>
        bool CompareVCRefPrj (List<SlnResolver.VC.ProjectReferencedDll> readPrj, List<SlnResolver.VC.ProjectReferencedDll> expectedPrj)
        {
            if (readPrj.Count()==expectedPrj.Count())
            {
                
                return true;
            }
            Assert.Fail("Не совпадает количество зависимых проектов");
            return false;
        }

        /// <summary>
        /// Тест на правильность чтения настроек VC-проекта
        /// </summary>
        [TestMethod]
        public void SlnResolver_TestReadingConfigurationVCPrj()
        {

            //файл C++ проекта
            string source_file = Path.Combine(sourceFile, @"TestSlnResolver\VCXPROJ\MinH\build\VC9\libMinHook.vcproj");
            string source_path = Path.GetFullPath(source_file.Replace(Path.GetFileName(source_file), ""));
            //читаем
            VCProjectReader reader = new VCProjectReader();
            var readingPrj = reader.ReadProject(source_file);

            #region Исходные данные

            #region Исходные конфигурации
            SlnResolver.VC.ProjectConfiguration expectedCnf1 = new SlnResolver.VC.ProjectConfiguration
            {
                ConfigurationName = "Debug",
                ConfigurationPlatform = "Win32",
                OutputDirectory = Path.GetFullPath(Path.Combine(source_file, @"..\..\lib\Debug")),
                IntermediateDirectory = @"$(PlatformName)\$(ConfigurationName)\$(ProjectName)",
                CharacterSet = "1",
                ConfigurationType = "4",
                isCurrent = false
            };
            SlnResolver.VC.ProjectConfiguration expectedCnf2 = new SlnResolver.VC.ProjectConfiguration
            {
                ConfigurationName = "Debug",
                ConfigurationPlatform = "x64",
                OutputDirectory = Path.GetFullPath(Path.Combine(source_file, @"..\..\lib\Debug")),
                IntermediateDirectory = @"x64\Debug\libMinHook",
                CharacterSet = "1",
                ConfigurationType = "4",
                isCurrent = false
            };
            SlnResolver.VC.ProjectConfiguration expectedCnf3 = new SlnResolver.VC.ProjectConfiguration
            {
                ConfigurationName = "Release",
                ConfigurationPlatform = "Win32",
                OutputDirectory = Path.GetFullPath(Path.Combine(source_file, @"..\..\lib\Release")),
                IntermediateDirectory = String.Empty,
                CharacterSet = "1",
                ConfigurationType = "4",
                isCurrent = true
            };
            SlnResolver.VC.ProjectConfiguration expectedCnf4 = new SlnResolver.VC.ProjectConfiguration
            {
                ConfigurationName = "Release",
                ConfigurationPlatform = "x64",
                OutputDirectory = Path.GetFullPath(Path.Combine(source_file, @"..\..\lib\Release")),
                IntermediateDirectory = @"x64\Release\libMinHook",
                CharacterSet = "1",
                ConfigurationType = "4",
                isCurrent = false
            };


            List<SlnResolver.VC.ProjectConfiguration> expectedCnfArray = new List<SlnResolver.VC.ProjectConfiguration>();
            expectedCnfArray.Add(expectedCnf1);
            expectedCnfArray.Add(expectedCnf2);
            expectedCnfArray.Add(expectedCnf3);
            expectedCnfArray.Add(expectedCnf4);
            #endregion

            #region Исходные файлы
            string[] sources = new string[]
            { @"..\..\src\buffer.c",
              @"..\..\src\hook.c",
              @"..\..\src\trampoline.c",     
              @"..\..\src\HDE\hde32.c",  
              @"..\..\src\HDE\hde64.c",
                      
            };


            string[] headers = new string[]
            {
              @"..\..\src\buffer.h",
              @"..\..\src\trampoline.h",
              @"..\..\src\HDE\hde32.h",  
              @"..\..\src\HDE\pstdint.h",
              @"..\..\src\HDE\table32.h",
              @"..\..\src\HDE\table64.h",
              @"..\..\src\HDE\table64.h",
              @"..\..\include\MinHook.h"  
            };

            string[] resourses = new string[]
            {

            };

            List<SlnResolver.VC.ProjectSourceFile> expectedSourceFiles = ToFormProjectFiles(sources);
            List<SlnResolver.VC.ProjectSourceFile> expectedHeaderFiles = ToFormProjectFiles(headers);
            List<SlnResolver.VC.ProjectSourceFile> expectedResourceFiles = ToFormProjectFiles(resourses);
            #endregion

            #region ReferencePrj

            List<SlnResolver.VC.ProjectReferencedDll> expectedReferencePrj = new List<SlnResolver.VC.ProjectReferencedDll>();

            #endregion


            #region Settings
            SlnResolver.VC.ProjectSettings expectedSettings = new SlnResolver.VC.ProjectSettings
            {
                AssemblyName = "libMinHook",
                ConfigurationArray = expectedCnfArray
            };

            #endregion
            SlnResolver.VC.ProjectBase excpectedPrj = new SlnResolver.VC.ProjectBase
            {
                Name = "libMinHook.vcproj",
                Path = source_file,
                Settings = expectedSettings,
                ReferencedProjects = expectedReferencePrj,
                SourceFiles = expectedSourceFiles,
                HeaderFiles = expectedHeaderFiles,
                ResourceFiles = expectedResourceFiles
            };



            #endregion

            //если файл проекта удалось прочитать, проверяем верно ли прочитал.
            Assert.IsTrue(CompareVCProjects(readingPrj, excpectedPrj));
           
        }

        #endregion

        #endregion


    }
}
