﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

using IOController;

namespace TestIOController
{
    /// <summary>
    /// Тесты для класса, работающего с путями
    /// </summary>
    [TestClass]
    public partial class TestPathController
    {
        #region Константы
        private const string DirectoryPostfix_TestMaterials = @"IOController\PathController";
        #endregion
        
        /// <summary>
        /// Проверка успешного и правильного определения признака каталога у корня ивда "С:".
        /// </summary>
        [TestMethod]
        public void IOController_WinAPI_GetAttributes()
        {
            var drives = System.IO.DriveInfo.GetDrives();
            Assert.IsTrue(drives.Length > 0, "Get attributes fail with no drives to test.");
            var driveLetterWithColon = drives[0].ToString().TrimEnd(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
            WinAPI.WinAPI.FileAttributes attributes = WinAPI.WinAPI.GetFileAttributes(PathController.ExtendPath(driveLetterWithColon));
            Assert.IsTrue((attributes & WinAPI.WinAPI.FileAttributes.Invalid) != WinAPI.WinAPI.FileAttributes.Invalid, "GetAttributes fail with C: - Invalid.");
            Assert.IsTrue((attributes & WinAPI.WinAPI.FileAttributes.Directory) != 0, "GetAttributes fail with C: - Not a Dir.");
        }
    }
}
