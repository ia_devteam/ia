﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

using TestUtils;

using IOController;

namespace TestIOController
{
    /// <summary>
    /// Тесты для класса, работающего с директориями
    /// </summary>
    [TestClass]
    public partial class TestDirectoryController
    {
        #region Константы
        internal const string DirectoryPostfix_TestMaterials = @"IOController\DirectoryController";

        internal const string DirectoryName_Temp = "TempDirectory";

        internal const string DirectoryName_Empty = "Empty";
        internal const string DirectoryName_FilesAndSubDirectories = "Files_And_SubDirectories";
        internal const string DirectoryName_FilesOnly = "Files_Only";
        internal const string DirectoryName_SubDirectoriesOnly = "SubDirectories_Only"; 
        #endregion

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Делаем то, что необходимо сделать после запуска каждого теста
        /// </summary>
        [TestCleanup]
        public void EachTest_CleanUp()
        {
        }

        

        #region GetFiles
        /// <summary>
        /// Подсчёт количества файлов в пустой директории
        /// </summary>
        [TestMethod]
        public void IOController_Directory_GetFiles_Empty()
        {
            string path = Path.Combine(testUtils.TestMatirialsDirectoryPath, DirectoryPostfix_TestMaterials, DirectoryName_Empty);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            int filesCount = DirectoryController.GetFiles(path).Count;
            int filesCountRecursive = DirectoryController.GetFiles(path, true).Count;

            Assert.IsTrue(filesCount == 0);
            Assert.IsTrue(filesCountRecursive == 0);
        }

        /// <summary>
        /// Подсчёт количества файлов в директории, содержащей только файлы
        /// </summary>
        [TestMethod]
        public void IOController_Directory_GetFiles_FilesOnly()
        {
            string path = Path.Combine(testUtils.TestMatirialsDirectoryPath, DirectoryPostfix_TestMaterials, DirectoryName_FilesOnly);

            int filesCount = DirectoryController.GetFiles(path).Count;
            int filesCountRecursive = DirectoryController.GetFiles(path, true).Count;

            Assert.IsTrue(filesCount == 2);
            Assert.IsTrue(filesCountRecursive == 2);
        }

        /// <summary>
        /// Подсчёт количества файлов в директории, содержащей только поддиректории
        /// </summary>
        [TestMethod]
        public void IOController_Directory_GetFiles_SubDirectoriesOnly()
        {
            string path = Path.Combine(testUtils.TestMatirialsDirectoryPath, DirectoryPostfix_TestMaterials, DirectoryName_SubDirectoriesOnly);

            int filesCount = DirectoryController.GetFiles(path).Count;
            int filesCountRecursive = DirectoryController.GetFiles(path, true).Count;

            Assert.IsTrue(filesCount == 0);
            Assert.IsTrue(filesCountRecursive == 4);
        }

        /// <summary>
        /// Подсчёт количества файлов в директории, содержащей как файлы так и поддиректории
        /// </summary>
        [TestMethod]
        public void IOController_Directory_GetFiles_FilesAndSubDirectories()
        {
            string path = Path.Combine(testUtils.TestMatirialsDirectoryPath, DirectoryPostfix_TestMaterials, DirectoryName_FilesAndSubDirectories);

            int filesCount = DirectoryController.GetFiles(path).Count;
            int filesCountRecursive = DirectoryController.GetFiles(path, true).Count;

            Assert.IsTrue(filesCount == 2);
            Assert.IsTrue(filesCountRecursive == 6);
        } 
        #endregion

        #region GetSubDirectories
        /// <summary>
        /// Подсчёт количества поддиректорий в пустой директории
        /// </summary>
        [TestMethod]
        public void IOController_Directory_GetSubDirectories_Empty()
        {
            string path = Path.Combine(testUtils.TestMatirialsDirectoryPath, DirectoryPostfix_TestMaterials, DirectoryName_Empty);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            int filesCount = DirectoryController.GetSubDirectories(path).Count;
            int filesCountRecursive = DirectoryController.GetSubDirectories(path, true).Count;

            Assert.IsTrue(filesCount == 0);
            Assert.IsTrue(filesCountRecursive == 0);
        }

        /// <summary>
        /// Подсчёт количества поддиректорий в директории, содержащей только файлы
        /// </summary>
        [TestMethod]
        public void IOController_Directory_GetSubDirectories_FilesOnly()
        {
            string path = Path.Combine(testUtils.TestMatirialsDirectoryPath, DirectoryPostfix_TestMaterials, DirectoryName_FilesOnly);

            int filesCount = DirectoryController.GetSubDirectories(path).Count;
            int filesCountRecursive = DirectoryController.GetSubDirectories(path, true).Count;

            Assert.IsTrue(filesCount == 0);
            Assert.IsTrue(filesCountRecursive == 0);
        }

        /// <summary>
        /// Подсчёт количества поддиректорий в директории, содержащей только поддиректории
        /// </summary>
        [TestMethod]
        public void IOController_Directory_GetSubDirectories_SubDirectoriesOnly()
        {
            string path = Path.Combine(testUtils.TestMatirialsDirectoryPath, DirectoryPostfix_TestMaterials, DirectoryName_SubDirectoriesOnly);

            int filesCount = DirectoryController.GetSubDirectories(path).Count;
            int filesCountRecursive = DirectoryController.GetSubDirectories(path, true).Count;

            Assert.IsTrue(filesCount == 2);
            Assert.IsTrue(filesCountRecursive == 2);
        }

        /// <summary>
        /// Подсчёт количества поддиректорий в директории, содержащей как файлы так и поддиректории
        /// </summary>
        [TestMethod]
        public void IOController_Directory_GetSubDirectories_FilesAndSubDirectories()
        {
            string path = Path.Combine(testUtils.TestMatirialsDirectoryPath, DirectoryPostfix_TestMaterials, DirectoryName_FilesAndSubDirectories);

            int filesCount = DirectoryController.GetSubDirectories(path).Count;
            int filesCountRecursive = DirectoryController.GetSubDirectories(path, true).Count;

            Assert.IsTrue(filesCount == 2);
            Assert.IsTrue(filesCountRecursive == 2);
        }
        #endregion
    }
}
