﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.IO;

using IOController;

using TestUtils;

namespace TestIOController
{
    /// <summary>
    /// Тесты для класса, работающего с путями
    /// </summary>
    public partial class TestPathController
    {
        /// <summary>
        /// Тесты для метода подменяющего префикс пути
        /// </summary>
        [TestClass]
        public class TestChangePrefix
        {
            #region Константы
            private const string DirectoryName_TestMaterials = @"ChangePrefix";
            private const string DirectoryName_Prefix1 = @"Prefix1";
            private const string DirectoryName_Prefix2 = @"Prefix2";
            private const string FileName = "1.txt";
	        #endregion

            /// <summary>
            /// Экземпляр инфраструктуры тестирования для каждого теста
            /// </summary>
            private TestUtilsClass testUtils;

            /// <summary>
            /// Путь до материалов тестирования
            /// </summary>
            private string testChangePrefixMaterialsPath = null; 
            
            /// <summary>
            /// Исходный префикс
            /// </summary>
            private string prefix1 = null;

            /// <summary>
            /// Конечный префикс
            /// </summary>
            private string prefix2 = null;

            /// <summary>
            /// Исходный путь к файлу
            /// </summary>
            private string path1 = null;

            /// <summary>
            /// Конечный путь к файлу
            /// </summary>
            private string path2 = null;

            private TestContext testContextInstance;
            /// <summary>
            /// Информация о текущем тесте
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            /// <summary>
            /// Делаем то, что необходимо сделать перед запуском каждого теста
            /// </summary>
            [TestInitialize]
            public void EachTest_Initialize()
            {
                //Иницииализация инфраструктуры тестирования для каждого теста
                testUtils = new TestUtilsClass(testContextInstance.TestName);

                this.testChangePrefixMaterialsPath = 
                    Path.Combine(
                        testUtils.TestMatirialsDirectoryPath,
                        TestPathController.DirectoryPostfix_TestMaterials,
                        TestChangePrefix.DirectoryName_TestMaterials
                    );

                this.prefix1 = 
                    Path.Combine(
                        this.testChangePrefixMaterialsPath,
                        TestChangePrefix.DirectoryName_Prefix1
                    );

                this.prefix2 = 
                    Path.Combine(
                        this.testChangePrefixMaterialsPath,
                        TestChangePrefix.DirectoryName_Prefix2
                    );

                this.path1 = 
                    Path.Combine(
                        this.prefix1,
                        TestChangePrefix.FileName
                    );

                this.path2 = String.Empty;
            }

            /// <summary>
            /// Делаем то, что необходимо сделать после запуска каждого теста
            /// </summary>
            [TestCleanup]
            public void EachTest_CleanUp()
            {
            }

            /// <summary>
            /// Проверка замены префиксов, когда оба префикса не заканчиваются на разделитель (Например: '\').
            /// </summary>
            [TestMethod]
            public void IOController_Path_ChangePrefix_Prefix1_Prefix2()
            {
                Assert.IsTrue(FileController.IsExists(path1), "Исходный файл не найден.");

                path2 = PathController.ChangePrefix(path1, prefix1, prefix2);

                Assert.IsTrue(
                    FileController.IsExists(path2),
                    "Файл после замены префикса с <" + prefix1 + "> на <" + prefix2 + "> не найден."
                );
            }

            /// <summary>
            /// Проверка замены префиксов, когда исходный префикс заканчивается на разделитель (Например: '\').
            /// </summary>
            [TestMethod]
            public void IOController_Path_ChangePrefix_Prefix1WithSeparator_Prefix2()
            {
                Assert.IsTrue(FileController.IsExists(path1), "Исходный файл не найден.");

                foreach (char directorySeparator in PathController.DirectorySeparators)
                {
                    string prefix1_And_Separator = prefix1 + directorySeparator;

                    path2 = PathController.ChangePrefix(path1, prefix1_And_Separator, prefix2);
                    
                    Assert.IsTrue(
                        FileController.IsExists(path2),
                        "Файл после замены префикса с <" + prefix1_And_Separator + "> на <" + prefix2 + "> не найден."
                    );
                }
            }

            /// <summary>
            /// Проверка замены префиксов, когда конечный префикс заканчивается на разделитель (Например: '\').
            /// </summary>
            [TestMethod]
            public void IOController_Path_ChangePrefix_Prefix1_Prefix2WithSeparator()
            {
                Assert.IsTrue(FileController.IsExists(path1), "Исходный файл не найден.");

                foreach (char directorySeparator in PathController.DirectorySeparators)
                {
                    string prefix2_And_Separator = prefix2 + directorySeparator;

                    path2 = PathController.ChangePrefix(path1, prefix1, prefix2_And_Separator);

                    Assert.IsTrue(
                        FileController.IsExists(path2),
                        "Файл после замены префикса с <" + prefix1 + "> на <" + prefix2_And_Separator + "> не найден."
                    );
                }
            }

            /// <summary>
            /// Проверка замены префиксов, когда оба префикса заканчиваются на разделитель (Например: '\').
            /// </summary>
            [TestMethod]
            public void IOController_Path_ChangePrefix_Prefix1WithSeparator_Prefix2WithSeparator()
            {
                Assert.IsTrue(FileController.IsExists(path1), "Исходный файл не найден.");

                foreach (char directorySeparator in PathController.DirectorySeparators)
                {
                    string prefix1_And_Separator = prefix1 + directorySeparator;
                    string prefix2_And_Separator = prefix2 + directorySeparator;

                    path2 = PathController.ChangePrefix(path1, prefix1_And_Separator, prefix2_And_Separator);

                    Assert.IsTrue(
                        FileController.IsExists(path2),
                        "Файл после замены префикса с <" + prefix1_And_Separator + "> на <" + prefix2_And_Separator + "> не найден."
                    );
                }
            }


            /// <summary>
            /// Проверка замены префиксов, когда оба префикса заканчиваются на разделитель (Например: '\').
            /// </summary>
            [TestMethod]
            public void IOController_CanonizePath1()
            {
                string path = @"\aaa\.\";
                Assert.IsTrue(DirectoryController.CanonizePath(path) == @"\aaa");

                path = @"\111\aaa\..\11";
                Assert.IsTrue(DirectoryController.CanonizePath(path) == @"\111\11");

                path = @"aaa\..\11";
                Assert.IsTrue(DirectoryController.CanonizePath(path) == @"\11");

                path = @"AAA";
                Assert.IsTrue(DirectoryController.CanonizePath(path) == @"aaa");

                path = @"aaa/../11";
                Assert.IsTrue(DirectoryController.CanonizePath(path) == @"\11");
            }

        }
    }
}
