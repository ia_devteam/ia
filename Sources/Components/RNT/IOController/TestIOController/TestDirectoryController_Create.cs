﻿using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using IOController;

namespace TestIOController
{
    /// <summary>
    /// Тесты для класса, работающего с директориями
    /// </summary>
    public partial class TestDirectoryController
    {
        /// <summary>
        /// Создание и удаление директории
        /// </summary>
        [TestMethod]
        public void IOController_Directory_CreateAndDelete()
        {
            string path = Path.Combine(this.testUtils.TestRunDirectoryPath, "TestDirectory\\Test1");

            Assert.IsFalse(DirectoryController.IsExists(path));

            DirectoryController.Create(path);

            Assert.IsTrue(DirectoryController.IsExists(path));

            DirectoryController.Remove(path.Substring(0, path.LastIndexOf("\\")));

            Assert.IsFalse(DirectoryController.IsExists(path));
        }

        /// <summary>
        /// Создание и удаление директории с учетом различных комбинаций разделителей директорий
        /// </summary>
        [TestMethod]
        public void IOController_Directory_CreateWithSeparators()
        {
            foreach (char separator1 in PathController.DirectorySeparators)
                foreach(char separator2 in PathController.DirectorySeparators)
                {
                    string path = this.testUtils.TestRunDirectoryPath + separator1 + "Test1" + separator2 + "Test2";

                    Assert.IsFalse(DirectoryController.IsExists(path));

                    DirectoryController.Create(path);

                    Assert.IsTrue(DirectoryController.IsExists(path));

                    DirectoryController.Remove(this.testUtils.TestRunDirectoryPath + separator1 + "Test1");
                }

            DirectoryController.Remove(this.testUtils.TestRunDirectoryPath);
        }
    }
}
