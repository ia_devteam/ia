﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;
using System.IO;

using IOController;

using TestUtils;

namespace TestIOController
{
    /// <summary>
    /// Тесты для класса, работающего с путями
    /// </summary>
    public partial class TestPathController
    {
        /// <summary>
        /// Тесты для метода, вычисляющего общую часть пути для списка путей
        /// </summary>
        [TestClass]
        public partial class TestGetCommonDirectoryPath
        {
            /// <summary>
            /// Некоторые произвольные имена директорий
            /// </summary>
            string[] someDirectoryNames = 
            {
                "Windows",
                "System32",
                "drivers"
            };
            /// <summary>
            /// Некоторые произвольные имена директорий
            /// </summary>
            string[] someDirectoryNames2 = 
            {
                "Windaws",
                "System32",
                "drivers"
            };

            /// <summary>
            /// Экземпляр инфраструктуры тестирования для каждого теста
            /// </summary>
            private TestUtilsClass testUtils;

            private TestContext testContextInstance;
            /// <summary>
            /// Информация о текущем тесте
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            /// <summary>
            /// Делаем то, что необходимо сделать перед запуском каждого теста
            /// </summary>
            [TestInitialize]
            public void EachTest_Initialize()
            {
                //Иницииализация инфраструктуры тестирования для каждого теста
                testUtils = new TestUtilsClass(testContextInstance.TestName);
            }

            /// <summary>
            /// Делаем то, что необходимо сделать после запуска каждого теста
            /// </summary>
            [TestCleanup]
            public void EachTest_CleanUp()
            {
            }

            /// <summary>
            /// Входная последовательность путей не определена
            /// </summary>
            [TestMethod]
            public void IOController_Path_GetCommonDirectoryPath_NullInput()
            {
                try
                {
                    PathController.GetCommonDirectoryPrefix(null);
                }
                catch (IOControllerException)
                {
                    return;
                }

                Assert.Fail("Метод не сгенерировал ислючение типа <" + typeof(IOControllerException).Name + ">");
            }

            /// <summary>
            /// Входная последовательность путей содержит элемент Null.
            /// </summary>
            [TestMethod]
            public void IOController_Path_GetCommonDirectoryPath_NullElement()
            {
                string[] correctOutput = null;

                List<string> input = new List<string>();

                input.AddRange(this.SomePathsListGenerator('C', Path.DirectorySeparatorChar));
                input.Add(null);

                string output = PathController.GetCommonDirectoryPrefix(input);

                this.CheckOutput(output, correctOutput);
            }

            /// <summary>
            /// Входная последовательность путей содержит элемент String.Empty.
            /// </summary>
            [TestMethod]
            public void IOController_Path_GetCommonDirectoryPath_EmptyElement()
            {
                string[] correctOutput = new string[0];

                List<string> input = new List<string>();

                input.AddRange(this.SomePathsListGenerator('C', Path.DirectorySeparatorChar));
                input.Add(String.Empty);

                string output = PathController.GetCommonDirectoryPrefix(input);

                this.CheckOutput(output, correctOutput);
            }

            /// <summary>
            /// Входная последовательность путей содержит элементы не имеющие общего префикса
            /// </summary>
            [TestMethod]
            public void IOController_Path_GetCommonDirectoryPath_NoCommonPrefix()
            {
                string[] correctOutput = new string[0];

                List<string> input = new List<string>();

                input.AddRange(this.SomePathsListGenerator('C', Path.DirectorySeparatorChar));
                input.AddRange(this.SomePathsListGenerator('D', Path.DirectorySeparatorChar));

                string output = PathController.GetCommonDirectoryPrefix(input);

                this.CheckOutput(output, correctOutput);
            }

            /// <summary>
            /// Входная последовательность путей элементы которой, содержат одинаковые разделители директорий в пути
            /// </summary>
            [TestMethod]
            public void IOController_Path_GetCommonDirectoryPath_DifferentDirectorySeparators()
            {
                string[] correctOutput = new string[] { "C:", "Windows" };

                foreach (char directorySeparator in PathController.DirectorySeparators)
                {
                    string[] input = this.SomePathsListGenerator('C', directorySeparator);

                    string output = PathController.GetCommonDirectoryPrefix(input);

                    this.CheckOutput(output, correctOutput);
                }
            }

            /// <summary>
            /// Входная последовательность путей элементы которой содержат различные разделители директорий в пути
            /// </summary>
            [TestMethod]
            public void IOController_Path_GetCommonDirectoryPath_AllDirectorySeparators()
            {
                string[] correctOutput = new string[] { "C:", "Windows" };

                List<string> input = new List<string>();

                foreach (char directorySeparator in PathController.DirectorySeparators)
                    input.AddRange(this.SomePathsListGenerator('C', directorySeparator));

                string output = PathController.GetCommonDirectoryPrefix(input);

                this.CheckOutput(output, correctOutput);
            }

            /// <summary>
            /// Входная последовательность путей элементы которой содержат пути в разных регистрах
            /// </summary>
            [TestMethod]
            public void IOController_Path_GetCommonDirectoryPath_RandomCase()
            {
                string[] correctOutput = new string[] { "C:", "WINDOWS" };

                List<string> input = new List<string>();
                
                Random random = new Random();
                string[] paths = this.SomePathsListGenerator('C', Path.DirectorySeparatorChar);

                foreach(string path in paths)
                {
                    string pathWithRandomCase = String.Empty;

                    for (int j = 0; j < path.Length; j++)
                        pathWithRandomCase += (random.Next() % 2 == 0) ? Char.ToLower(path[j]) : Char.ToUpper(path[j]);
                    
                    input.Add(pathWithRandomCase);
                }

                string output = PathController.GetCommonDirectoryPrefix(input, true);

                this.CheckOutput(output, correctOutput, true);
            }

            /// <summary>
            /// Генерация набора путей из некоторого произвольного набора имён директорий
            /// </summary>
            /// <param name="diskLetter">Буква диска</param>
            /// <param name="directorySeparatorChar">Разделитель между именами директорий в пути</param>
            /// <returns></returns>
            private string[] SomePathsListGenerator(char diskLetter, char directorySeparatorChar)
            {
                string[] ret = new string[someDirectoryNames.Length];

                for (int i = 0; i < someDirectoryNames.Length; i++)
                {
                    string path = diskLetter + ":" + directorySeparatorChar;

                    for (int j = 0; j <= i; j++)
                        path += someDirectoryNames[j] + directorySeparatorChar;

                    ret[i] = path;
                }

                return ret;
            }

            /// <summary>
            /// Проверка результата
            /// </summary>
            /// <param name="currentOutput">Текущий результат</param>
            /// <param name="correctDirectoriesSequence">Последовательность директорий содержащаяся в правильном результате</param>
            private void CheckOutput(string currentOutput, string[] correctDirectoriesSequence = null, bool caseInvariant = false)
            {
                if (String.IsNullOrEmpty(currentOutput))
                {
                    if ((currentOutput == null && correctDirectoriesSequence == null) ||
                            (currentOutput == String.Empty && 
                                (correctDirectoriesSequence.Length == 0 || correctDirectoriesSequence[0] == String.Empty)
                            )
                        )
                        return;
                }
                else
                {
                    if (correctDirectoriesSequence != null)
                    {
                        if (caseInvariant)
                        {
                            currentOutput = Store.Files.CanonizeFileName(currentOutput);
                            for (int i = 0; i < correctDirectoriesSequence.Length; i++)
                            {
                                correctDirectoriesSequence[i] = Store.Files.CanonizeFileName(correctDirectoriesSequence[i]);
                            }
                        }

                        foreach (char directorySeparator in PathController.DirectorySeparators)
                        {
                            string correctOutput = String.Join(directorySeparator.ToString(), correctDirectoriesSequence);

                            if (currentOutput.Equals(correctOutput))
                                return;
                        }
                    }
                }

                Assert.Fail(
                    String.Format(
                        "Результат отличен от ожидаемого. Результат: <{0}>. Ожидаемая последовательной директорий <{1}>.",
                        currentOutput ?? "NULL",
                        correctDirectoriesSequence != null ? String.Join(" -> ", correctDirectoriesSequence) : "NULL"
                        )
                );
            }
        }
    }
}
