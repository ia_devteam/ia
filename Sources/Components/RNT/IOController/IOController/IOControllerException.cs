﻿using System.IO;

namespace IOController
{
    /// <summary>
    /// Класс, реализующий исключение при работе со сборкой "IOController"
    /// </summary>
    public class IOControllerException : IOException
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public IOControllerException() : base() { }
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message"></param>
        public IOControllerException(string message) : base(message) { }
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public IOControllerException(string message, System.Exception inner) : base(message, inner) { }
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected IOControllerException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
