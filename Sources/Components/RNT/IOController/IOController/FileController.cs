﻿using System;
using System.Diagnostics;
using System.IO;

namespace IOController
{
    /// <summary>
    /// Класс, реализующий различные методы для работы с файлами
    /// </summary>
    public static class FileController
    {
        /// <summary>
        /// Проверка существования файла по указанному пути
        /// </summary>
        /// <param name="path">Путь до файла</param>
        /// <returns></returns>
        public static bool IsExists(string path)
        {
            try
            {
                //Если путь не задан
                if (String.IsNullOrWhiteSpace(path))
                    return false;

                //Получаем аттрибуты файла
                WinAPI.WinAPI.FileAttributes attributes = WinAPI.WinAPI.GetFileAttributes(PathController.ExtendPath(path));

                //По атрибутам проверяем существование
                return ((attributes & WinAPI.WinAPI.FileAttributes.Invalid) != WinAPI.WinAPI.FileAttributes.Invalid) &&
                            ((attributes & WinAPI.WinAPI.FileAttributes.Directory) != WinAPI.WinAPI.FileAttributes.Directory);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при проверке существования файла <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Копирование файла
        /// </summary>
        /// <param name="fromPath">Путь до файла для копирования</param>
        /// <param name="toPath">
        /// Путь до файла, создаваемого в процессе копирования.
        /// Если файл уже существует, он перезаписывается.
        /// </param>
        /// <returns></returns>
        public static void Copy(string fromPath, string toPath)
        {
            try
            {
                //Проверка на разумность
                if (fromPath == null)
                    throw new ArgumentNullException("fromPath", "Путь до файла для копирования не определен.");

                //Проверка на то что файл для копирования существует
                if (!FileController.IsExists(fromPath))
                    throw new FileNotFoundException("Файл для копирования не существует.");

                //Проверка на разумность
                if (toPath == null)
                    throw new ArgumentNullException("toPath", "Путь до файла, создаваемого в процессе копирования, не определён.");

                //Копируем файл
                if (!WinAPI.WinAPI.CopyFile(PathController.ExtendPath(fromPath), PathController.ExtendPath(toPath), false))
                    throw new IOControllerException("Неизвестная ошибка.");
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при копировании файла <" + (fromPath ?? "путь неизвестен") + "> по пути <" + (toPath ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Удаление файла
        /// </summary>
        /// <param name="path">Путь до файла</param>
        /// <param name="isResetAttributes">Необходимо ли сбросить аттрибуты файла перед удалением?</param>
        public static void Remove(string path, bool isResetAttributes = false)
        {
            try
            {
                //Проверка на разумность
                if (path == null)
                    throw new ArgumentNullException("path", "Путь до файла не определён.");

                //Проверка на существование файла
                if (!FileController.IsExists(path))
                {
                    //Сообщеаем разработчику о попытке удалить несуществующий файл.
                    Debug.Assert(false, "Файл не существует.");
                    return;
                }

                //Сбрасываем аттрибуты файла
                if (isResetAttributes)
                    FileController.SetAttributes(path, FileAttributes.Normal);

                //Удаляем файл
                if (!WinAPI.WinAPI.DeleteFile(PathController.ExtendPath(path)))
                    throw new IOControllerException("Неизвестная ошибка.");
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при удалении файла <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Задать атрибуты файла
        /// </summary>
        /// <param name="path">Путь до файла</param>
        /// <param name="attributes">Атрибуты</param>
        public static void SetAttributes(string path, FileAttributes attributes)
        {
            try
            {
                //Проверка на разумность
                if (path == null)
                    throw new ArgumentNullException("path", "Путь до файла не определён.");

                //Проверка на существование файла
                if (!FileController.IsExists(path))
                    throw new FileNotFoundException();

                //Задаём атрибуты файла
                if (!WinAPI.WinAPI.SetFileAttributes(PathController.ExtendPath(path), IOExtensions.ConvertAttributes(attributes)))
                    throw new IOControllerException("Неизвестная ошибка.");
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при задании атрибутов файла <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }
    }
}
