﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;

namespace IOController
{
    /// <summary>
    /// Класс, реализующий различные методы для работы с директориями
    /// </summary>
    public static class DirectoryController
    {
        /// <summary>
        /// Получить список полных путей поддиректорий, содержащихся в директории по указанному пути
        /// Исследуется содержимое только указанной директории, содержимое поддиректорий не учитывается
        /// </summary>
        /// <param name="path">Путь до директории</param>
        /// <param name="isRecursive">Обрабатывать ли поддиректории?</param>
        /// <returns>Список полных путей поддиректорий</returns>
        public static List<string> GetSubDirectories(string path, bool isRecursive = false)
        {
            try
            {
                //Формируем результат
                List<string> ret = new List<string>();

                //Ищем первый дочерний элемент
                WinAPI.WinAPI.WIN32_FIND_DATA findData;
                IntPtr findHandle = WinAPI.WinAPI.FindFirstFile(PathController.ExtendPath(Path.Combine(path, "*")), out findData);

                //Если ничего не нашли
                if (findHandle == new IntPtr(-1))
                    throw new System.ComponentModel.Win32Exception();

                //Обрабатываем все файлы и поддиректории
                do
                {
                    //Получаем путь до дочернего элемента
                    string subPath = Path.Combine(path, findData.cFileName);

                    //Если дочерний элемент - это файл
                    if ((findData.dwFileAttributes & WinAPI.WinAPI.FileAttributes.Directory) == 0)
                        continue;

                    //Если дочерний элемент - это сама и родительская директория
                    if (findData.cFileName == "." || findData.cFileName == "..")
                        continue;

                    //Если требуется, обрабатываем поддиректорию
                    if (isRecursive)
                        ret.AddRange(DirectoryController.GetSubDirectories(subPath, isRecursive));

                    //Добавляем поддиректорию в результирующий список
                    ret.Add(subPath);
                }
                while (WinAPI.WinAPI.FindNextFile(findHandle, out findData));

                //Закрываем дескриптор поиска
                if (!WinAPI.WinAPI.FindClose(findHandle))
                    throw new IOControllerException("Ошибка при закрытии дескриптора поиска.");

                return ret;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при получении списка поддиректорий директории <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Получить список полных путей файлов, содержащихся в директории по указанному пути
        /// </summary>
        /// <param name="path">Путь до директории</param>
        /// <param name="isRecursive">Обрабатывать ли поддиректории?</param>
        /// <returns>Список полных путей файлов</returns>
        public static List<string> GetFiles(string path, bool isRecursive = false)
        {
            try
            {
                //Формируем результат
                List<string> ret = new List<string>();

                //Ищем первый дочерний элемент
                WinAPI.WinAPI.WIN32_FIND_DATA findData;
                IntPtr findHandle = WinAPI.WinAPI.FindFirstFile(PathController.ExtendPath(Path.Combine(path, "*")), out findData);

                //Если ничего не нашли
                if (findHandle == new IntPtr(-1))
                    throw new System.ComponentModel.Win32Exception();

                //Удаляем все файлы и поддиректории
                do
                {
                    //Получаем путь до дочернего элемента
                    string subPath = Path.Combine(path, findData.cFileName);

                    //Если дочерний элемент - это директория
                    if ((findData.dwFileAttributes & WinAPI.WinAPI.FileAttributes.Directory) != 0)
                    {
                        //Если дочерний элемент - это сама и родительская директория
                        if (findData.cFileName == "." || findData.cFileName == "..")
                            continue;

                        //Если требуется, обрабатываем поддиректорию
                        if (isRecursive)
                            ret.AddRange(DirectoryController.GetFiles(subPath, isRecursive));
                    }
                    //Если дочерний элемент - это файл
                    else
                    {
                        //Добавляем файл в результирующий список
                        ret.Add(subPath);
                    }
                }
                while (WinAPI.WinAPI.FindNextFile(findHandle, out findData));

                //Закрываем дескриптор поиска
                if (!WinAPI.WinAPI.FindClose(findHandle))
                    throw new IOControllerException("Ошибка при закрытии дескриптора поиска.");

                return ret;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при получении списка файлов директории <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Проверка существования директории по указанному пути
        /// </summary>
        /// <param name="path">Путь до директории</param>
        /// <returns>Существует ли директория?</returns>
        public static bool IsExists(string path)
        {
            try
            {
                path = PathController.CanonizePath(path);

                //Если путь не задан
                if (String.IsNullOrWhiteSpace(path))
                    return false;

                //Получаем аттрибуты директории
                WinAPI.WinAPI.FileAttributes attributes = WinAPI.WinAPI.GetFileAttributes(PathController.ExtendPath(path));

                //По атрибутам проверяем существование
                return ((attributes & WinAPI.WinAPI.FileAttributes.Invalid) != WinAPI.WinAPI.FileAttributes.Invalid) &&
                            ((attributes & WinAPI.WinAPI.FileAttributes.Directory) == WinAPI.WinAPI.FileAttributes.Directory);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException("Ошибка при проверке существования директории <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Создать директорию
        /// </summary>
        /// <param name="path">Путь до директории</param>
        public static void Create(string path)
        {
            try
            {
                path = PathController.CanonizePath(path);

                //Добрались до диска, корректируем путь
                if (path[path.Length - 1] == ':')
                {
                    if (!DirectoryController.IsExists(path))
                        throw new IOControllerException("Диск не существует.");

                    return;
                }

                //Проверка на то, что директория уже существует
                if (DirectoryController.IsExists(path))
                    return;

                //Создаем родительские директории                
                DirectoryController.Create(path.Substring(0, path.LastIndexOf(PathController.DirectorySeparators[0])));

                //Создаём директорию
                if (!WinAPI.WinAPI.CreateDirectory(PathController.ExtendPath(path), IntPtr.Zero))
                    throw new IOControllerException("Неизвестная ошибка.");
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при создании директории <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Скопировать одну директорию в другую
        /// </summary>
        /// <param name="fromPath">Путь до директории для копирования</param>
        /// <param name="toPath">
        /// Путь до директории, куда неоходимо произвести копирование.
        /// Если директория не пуста, её содержимое не уничтожается. Файлы с одинаковыми именами перезаписываются.
        /// </param>
        /// <param name="isRecursive">Обрабатывать ли поддиректории?</param>
        public static void Copy(string fromPath, string toPath, bool isRecursive = true)
        {
            try
            {
                //Проверка на разумность
                if (fromPath == null)
                    throw new ArgumentNullException("fromPath", "Путь до директории для копирования не определен.");

                //Проверка на то что директория для копирования существует
                if (!DirectoryController.IsExists(fromPath))
                    throw new DirectoryNotFoundException("Директория для копирования не существует.");

                //Проверка на разумность
                if (toPath == null)
                    throw new ArgumentNullException("toPath", "Путь до директории, куда необходимо произвести копирование, не определён.");

                //Если директория для копирования не существует
                if (!DirectoryController.IsExists(toPath))
                    //Создаём её
                    DirectoryController.Create(toPath);

                //Копируем файлы
                foreach (string filePath in DirectoryController.GetFiles(fromPath))
                    FileController.Copy(filePath, PathController.ChangePrefix(filePath, fromPath, toPath));

                //Копируем поддиректории, если требуется
                if (isRecursive)
                    foreach (string subDirectoryPath in DirectoryController.GetSubDirectories(fromPath))
                    {
                        //Получаем новый путь до поддиректории
                        string subDirectoryNewPath = PathController.ChangePrefix(subDirectoryPath, fromPath, toPath);

                        //Копируем 
                        DirectoryController.Copy(subDirectoryPath, PathController.ChangePrefix(subDirectoryPath, fromPath, toPath));
                    }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при копировании информации из директории <" + (fromPath ?? "путь неизвестен") + "> в директорию <" + (toPath ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Очистка директории по указанному пути
        /// </summary>
        /// <param name="path">Путь до директории</param>
        /// <param name="isResetAttributes">Необходимо ли сбросить аттрибуты содержимого директории перед удалением?</param>
        public static void Clear(string path, bool isResetAttributes = false)
        {
            try
            {
                //Проверка на разумность
                if (path == null)
                    throw new ArgumentNullException("path", "Путь до директории не определён.");

                //Проверка на существование директории
                if (!DirectoryController.IsExists(path))
                    throw new DirectoryNotFoundException();

                //Сбрасываем аттрибуты содержимого директории
                if (isResetAttributes)
                    DirectoryController.SetAttributes(path, FileAttributes.Normal, false, true);

                //Удаляем файлы
                foreach (string filePath in DirectoryController.GetFiles(path))
                    FileController.Remove(filePath);

                //Удаляем поддиректории
                foreach (string subDirectoryPath in DirectoryController.GetSubDirectories(path))
                    DirectoryController.Remove(subDirectoryPath, isResetAttributes);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при очистке директории <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Удаление директории со всеми поддиректориями
        /// </summary>
        /// <param name="path">Путь до директории</param>
        /// <param name="isResetAttributes">Необходимо ли сбросить аттрибуты содержимого директории перед удалением?</param>
        public static void Remove(string path, bool isResetAttributes = false)
        {
            try
            {
                path = PathController.CanonizePath(path);

                //Проверка на разумность
                if (String.IsNullOrWhiteSpace(path))
                    throw new DirectoryNotFoundException("Путь до директории не определён.");

                //Проверка на существование директории
                if (!DirectoryController.IsExists(path))
                {
                    //Сообщеаем разработчику о попытке удалить несуществующую директорию.
                    Debug.Assert(false, "Директория не существует.");
                    return;
                }

                //Удаляем содержимое директории
                DirectoryController.Clear(path, isResetAttributes);

                //Сбрасываем аттрибуты директории
                if (isResetAttributes)
                    DirectoryController.SetAttributes(path, FileAttributes.Normal, true, false);

                //Удаляем саму директорию
                if (!WinAPI.WinAPI.RemoveDirectory(PathController.ExtendPath(path)))
                    throw new IOControllerException("Неизвестная ошибка.");
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при удалении директории <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Проверка на то, пуста ли директория
        /// </summary>
        /// <param name="path">Путь к директории</param>
        /// <returns>Пуста ли директория?</returns>
        public static bool IsEmpty(string path)
        {
            try
            {
                //Проверка на разумность
                if (path == null)
                    throw new ArgumentNullException("path", "Путь до директории не определён.");

                //Проверка на то, что директория существует
                if (!DirectoryController.IsExists(path))
                    throw new DirectoryNotFoundException();

                //Директория пуста, если в ней нет поддиректорий и файлов
                return DirectoryController.GetSubDirectories(path).Count == 0 && DirectoryController.GetFiles(path).Count == 0;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при проверке директории <" + (path ?? "путь неизвестен") + "> на пустоту.",
                    ex
                );
            }
        }

        /// <summary>
        /// Получить путь к директории, выбранной пользователем
        /// 
        /// NB! Если параметр enableComment равен true, то возвращаемое значение имеет вид "Путь?комментарий" !!!!!!!!!!
        /// </summary>
        /// <param name="defaultPath">Путь для отображения по умолчанию</param>
        /// <param name="message">Сообщение для пользователя</param>
        /// <param name="isNeedToBeEmpty">Необходимо ли, чтобы выбранная директория была пустой?</param>
        /// <param name="isNoCancel">Есть ли возможность отменить выбор путь до директории?</param>
        /// <param name="emptyPathPossible">Допустим ли пустой путь</param>
        /// <param name="enableCreateFolder">Разрешить создание папки</param>
        /// <param name="enableComment">Добавить возможность указания комментария к папке (для материалов)</param>
        /// <returns>Путь к выбранной директории, null - если директория не выбрана. В случае поднятого флага isNoCancel - не может быть null. Через символ "?" может быть дописан комментарий</returns>
        public static string SelectByUser(string defaultPath = null, string message = null, bool isNeedToBeEmpty = false, bool isNoCancel = false, bool emptyPathPossible = false, bool enableCreateFolder = false, bool enableComment = false)
        {
            try
            {
                //Отображаем сообщение
                //if (message != null)
                //    MessageBox.Show(message, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);

                using (FolderSelectDialog dialog = new FolderSelectDialog(message, emptyPathPossible, enableCreateFolder, enableComment, defaultPath) { StartPosition = FormStartPosition.CenterParent })
                {
                    dialog.Text = message;
                    do
                    {
                        //Отображаем диалог выбор директории
                        while (dialog.ShowDialog() != DialogResult.OK)
                        {
                            //Если отменить выбор пути до директории возможно, ничего не делаем
                            if (!isNoCancel)
                                return null;

                            MessageBox.Show("Директория не была указана. Данная ситуация недопустима. Пожалуйста, попробуйте снова.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }


                        //Если не требуется пустота, выходим из цикла
                        if (!isNeedToBeEmpty)
                            break;

                        //Если директория не пуста
                        if (!DirectoryController.IsEmpty(dialog.SelectedPath))
                        {
                            //Отображаем вопрос
                            switch (MessageBox.Show("Директория не пуста. Удалить содержимое?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                            {
                                case DialogResult.Yes:
                                    DirectoryController.Clear(dialog.SelectedPath);
                                    break;
                                case DialogResult.No:
                                    continue;
                            }
                            break;
                        }

                    } while (!DirectoryController.IsEmpty(dialog.SelectedPath));

                    //Возвращаем путь к выбранной директории
                    return dialog.SelectedPath + (enableComment ? "?" + dialog.comment : "");
                }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при выборе директории пользователем.",
                    ex
                );
            }
        }

        /// <summary>
        /// Проверка на то, совпадают ли пути заданных директорий
        /// </summary>
        /// <param name="directoryPath1">Путь к директории 1</param>
        /// <param name="directoryPath2">Путь к директории 2</param>
        /// <returns>Совпадают ли пути?</returns>
        public static bool IsEqualPaths(string directoryPath1, string directoryPath2)
        {
            try
            {
                return String.Equals(
                            PathController.TrimEndSeparator(Path.GetFullPath(directoryPath1)),
                            PathController.TrimEndSeparator(Path.GetFullPath(directoryPath2)),
                            StringComparison.InvariantCultureIgnoreCase);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при сравнении пути директории <" + (directoryPath1 ?? "путь неизвестен") + "> с путём директории <" + (directoryPath2 ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Нормализация пути до директории или до файла.
        /// С конца пути отсекается разделитель (Например: '\').
        /// Путь переводится в нижний регистр.
        /// </summary>
        /// <param name="path">Путь до директории</param>
        /// <returns>Нормализованный путь до директории</returns>
        public static string CanonizePath(string path)
        {
            if (path == null)
                return null;
            else
                if (path == "")
                return "";
            else
            {
                //Боремся с разнообразием слешей и букв
                string res = PathController.TrimEndSeparator(path.Replace('/', '\\').ToLowerInvariant());

                //Боремся с ./
                res = res.Replace(@"\.\", "\\");
                if (res.EndsWith(@"\."))
                    res = res.Substring(0, res.Length - 2);

                if (res.StartsWith("..\\"))
                    throw new IOControllerException("Невозможно канонизировать путь, начинающийся с ..\\  Путь:" + path);

                //Боремся с \..\
                int idx;
                while ((idx = res.IndexOf(@"\..\")) != -1)
                {
                    int idx1;
                    for (idx1 = idx - 1; idx1 >= 0 && res[idx1] != '\\'; idx1--);

                    if (idx1 == -1)
                        idx1 = 0;

                    res = res.Remove(idx1, idx + 3 - idx1);

                }

                return res;
            }
        }

        /// <summary>
        /// Задать атриубуты директории
        /// </summary>
        /// <param name="path">Путь до директории</param>
        /// <param name="attributes">Атрибуты</param>
        /// <param name="isNeetToProcessRoot">Задать указанные атрибуты для самой директории?</param>
        /// <param name="isNeedToProcessContents">Задать указанные атрибуты для содержимого директории?</param>
        public static void SetAttributes(string path, FileAttributes attributes, bool isNeetToProcessRoot = true, bool isNeedToProcessContents = true)
        {
            try
            {
                //Проверка на разумность
                if (path == null)
                    throw new ArgumentNullException("path", "Путь до директории не определён.");

                //Проверка на то, что директория существует
                if (!DirectoryController.IsExists(path))
                    throw new DirectoryNotFoundException();

                //Если требуется обработка самой директории
                if (isNeetToProcessRoot)
                {
                    //Задаём атрибуты директории
                    if (!WinAPI.WinAPI.SetFileAttributes(PathController.ExtendPath(path), IOExtensions.ConvertAttributes(attributes)))
                        throw new IOControllerException("Неизвестная ошибка.");
                }

                //Если требуется обработка содержимого директории
                if (isNeedToProcessContents)
                {
                    //Проставляем атрибуты для всех файлов
                    foreach (string file in DirectoryController.GetFiles(path))
                        FileController.SetAttributes(file, attributes);

                    //Проставляем атрибуты для всех поддиректорий рекурсивно
                    foreach (string subDirectory in DirectoryController.GetSubDirectories(path))
                        DirectoryController.SetAttributes(subDirectory, attributes, true, true);
                }
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при задании атрибутов директории <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }
    }
}
