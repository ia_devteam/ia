﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace IOController
{
    /// <summary>
    /// Класс, реализующий различные методы для работы с файлами
    /// </summary>
    public class PathController
    {
        /// <summary>
        /// Возможные разделители директорий в пути
        /// </summary>
        public static char[] DirectorySeparators = 
        { 
            Path.DirectorySeparatorChar,
            Path.AltDirectorySeparatorChar
        };

        /// <summary>
        /// Префикс расширения длины локального пути
        /// </summary>
        public const string ExtendedLocalPathPrefix = @"\\?\";

        /// <summary>
        /// Префикс расширения длины сетевого пути
        /// </summary>
        public const string ExtendedNetworkPathPrefix = @"\\?\UNC\";

        /// <summary>
        /// Канонизация пути к файлу или директории.
        /// Разделители каталогов приводятся к общему виду через обратную косую черту
        /// </summary>
        /// <param name="path">Путь к файлу или директории.</param>
        /// <returns>Канонизированный путь.</returns>
        public static string CanonizePath(string path)
        {
            if (path == null)
                throw new ArgumentNullException("path", "Путь до файла или директории не определён.");

            if (path.Contains(DirectorySeparators[1]))
                path = path.Replace(DirectorySeparators[1], DirectorySeparators[0]);
            
            return path;
        }

        /// <summary>
        /// Метод для расширения длины пути. Служит для передачи путей в методы WinAPI.
        /// </summary>
        /// <param name="fullPath">Полный путь до файла или директории</param>
        /// <returns></returns>
        public static string ExtendPath(string fullPath)
        {
            //Если длина пути уже расширена
            if (fullPath.StartsWith(ExtendedLocalPathPrefix) || fullPath.StartsWith(ExtendedNetworkPathPrefix))
                return fullPath;

            if (fullPath.Length <= 3 && fullPath.Length >= 2) // валидные пути C: C:\ //fix #10
                return fullPath;

            //Если путь сетевой
            if (fullPath.StartsWith(@"\\"))
                return ExtendedNetworkPathPrefix + fullPath.Substring(2);

            //Если путь локальный
            return ExtendedLocalPathPrefix + fullPath;
        }

        /// <summary>
        /// Замена префикса пути до файла или директории
        /// </summary>
        /// <param name="path">Путь до файла или директории</param>
        /// <param name="oldPrefix">Старый префикс пути до файла или директории</param>
        /// <param name="newPrefix">Новый префикс пути до файла или директории</param>
        /// <returns>Путь до файла или директории с новым префиксом</returns>
        public static string ChangePrefix(string path, string oldPrefix, string newPrefix)
        {
            try
            {
                //Проверка на разумность
                if (path == null)
                    throw new ArgumentNullException("path", "Путь до файла или директории не определён.");

                //Проверка на разумность
                if (oldPrefix == null)
                    throw new ArgumentNullException("oldPrefix", "Старый префикс не определён.");

                //Проверка на разумность
                if (newPrefix == null)
                    throw new ArgumentNullException("newPrefix", "Новый префикс не определён.");

                //Нормализуем префиксы
                string oldPrefixNoDirectorySeparator = PathController.TrimEndSeparator(new Uri(oldPrefix).LocalPath);
                string newPrefixNoDirectorySeparator = PathController.TrimEndSeparator(new Uri(newPrefix).LocalPath);

                //Проверка на то, что путь до файла или директории начинается с указанного профикса
                if (!path.StartsWith(oldPrefixNoDirectorySeparator, StringComparison.OrdinalIgnoreCase))
                    throw new IOControllerException("Путь до файла или директории не начинается с указанного префикса.");

                return Path.Combine(newPrefixNoDirectorySeparator, path.Substring(oldPrefixNoDirectorySeparator.Length + 1)); //path.Replace(oldPrefixNoDirectorySeparator, newPrefixNoDirectorySeparator);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при изменении префикса пути до файла или директории <" + (path ?? "путь неизвестен") + "> c <" + (oldPrefix ?? "префикс неизвестен") + "> на <" + (newPrefix ?? "префикс неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Удаление разделителя в конце пути до файла или директории
        /// </summary>
        /// <param name="path">Путь до файла или директории</param>
        /// <returns></returns>
        public static string TrimEndSeparator(string path)
        {
            try
            {
                //Проверка на разумность
                if (path == null)
                    throw new ArgumentNullException("path", "Путь до файла или директории не определён.");

                return path.TrimEnd(DirectorySeparators);
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при удалении разделителя в конце пути до файла или директории <" + (path ?? "путь неизвестен") + ">.",
                    ex
                );
            }
        }

        /// <summary>
        /// Сравнение двух путей без учёта регистра и культуры (см. <c>StringComparison.OrdinalIgnoreCase</c>)
        /// </summary>
        /// <param name="path1"></param>
        /// <param name="path2"></param>
        /// <returns></returns>
        public static bool IsPathsEqualAsInvariants(string path1, string path2)
        {
            return path1.Equals(path2, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Вычисление общего префикса для набора путей.
        /// Элемент набора, представляющий из себя путь до директории, должен заканчиваться на символы "\" или "/".
        /// </summary>
        /// <param name="paths">Набор путей.</param>
        /// <param name="isIgnoreCase">Игнорировать ли регистр символов? Стоит включать для путей формата Unix.</param>
        /// <remarks>
        /// Если рассматривать функцию в контексте Unix-like путей, то возможна ситуация вроде "/etc/\ sss", где строка "\ " это один символ пробела.
        /// Такие случаи корректно не обрабатываются.
        /// </remarks>
        /// <returns>
        /// Общий префикс для набора путей.
        /// Если исходный набор путей пуст => String.Empty
        /// Если исходный набор путей содержит хоть один элемент Null => Null.
        /// Если исходный набор путей содержит хоть один элемент String.Empty => String.Empty
        /// </returns>
        public static string GetCommonDirectoryPrefix(IEnumerable<string> paths, bool isIgnoreCase = false)
        {
            try
            {
                //Проверка на разумность
                if (paths == null)
                    throw new IOControllerException("Набор строк не определён.");

                foreach (string path in paths)
                {
                    //Если один из элементов Null => Null
                    //Если один из элементов String.Empty => String.Empty
                    if (String.IsNullOrEmpty(path))
                        return path;
                }

                var arr = paths.ToArray(); //в этот момент всё перечисление уже в памяти, почти бесплатное преобразование.

                //Если список путей пуст
                if(arr.Length == 0)
                    return String.Empty;

                //Если список путей состоит из одного элемента
                if(arr.Length == 1)
                    return Path.GetDirectoryName(arr[0]) ?? arr[0];

                CultureInfo culture = CultureInfo.GetCultureInfo("ru-RU"); //значит, что с испанскими или украинскими путями работать будет плохо

                //Нормализация путей в списке
                var arrNormalized = arr
                    .AsParallel()
                    .Select(s =>
                        {
                            string tmp = s;

                            if (tmp.Contains(Path.AltDirectorySeparatorChar))
                                tmp = tmp.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                            
                            string doubleDirectorySeparatorChar = new String(Path.DirectorySeparatorChar, 2);

                            while (tmp.Contains(doubleDirectorySeparatorChar))
                                tmp = tmp.Replace(doubleDirectorySeparatorChar, Path.DirectorySeparatorChar.ToString());

                            if(isIgnoreCase)
                                tmp = tmp.ToUpper(culture);

                            return tmp;
                        }
                    );

                string res = arrNormalized.First();

                int commonPart = res.Length;
                foreach (var path in arrNormalized)
                {
                    commonPart = GetStringsCommonPartLength(res, path, commonPart);
                    if (commonPart == 0)
                        return String.Empty;
                }

                res = arr[0].Substring(0, commonPart); //возвращаем обрезок строки из оригинального набора с сохранением регистра букв. Кому надо - сами приведут в нужный регистр.

                return Path.GetDirectoryName(res) ?? res;
            }
            catch (Exception ex)
            {
                //Формируем исключение
                throw new IOControllerException(
                    "Ошибка при вычислении общего префикса для набора путей.",
                    ex
                );
            }
        }

        /// <summary>
        /// Получение длины наибольшей общей начальной части двух строк.
        /// </summary>
        /// <param name="str1">Первая строка.</param>
        /// <param name="str2">Вторая строка.</param>
        /// <param name="maxLength">Максимальная допустимая длина общей начальной части двух подстрок.</param>
        /// <returns></returns>
        private static int GetStringsCommonPartLength(string str1, string str2, int maxLength = int.MaxValue)
        {
            if (String.IsNullOrEmpty(str1) || String.IsNullOrEmpty(str2))
                return 0;

            for (int i = 0; i < str1.Length; i++)
                if (i >= str2.Length || str2[i] != str1[i] || i == maxLength)
                    return i;

            return str1.Length;
        }
    }
}
