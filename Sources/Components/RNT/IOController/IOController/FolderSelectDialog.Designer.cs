﻿namespace IOController
{
    partial class FolderSelectDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.CloseButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.addFolderGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.addFolderButton = new System.Windows.Forms.Button();
            this.folderNameTextBox = new System.Windows.Forms.TextBox();
            this.createFolderButton = new System.Windows.Forms.Button();
            this.cancelNewFolderButton = new System.Windows.Forms.Button();
            this.BrowseGroupBox = new System.Windows.Forms.GroupBox();
            this.FolderPathTextBox = new System.Windows.Forms.TextBox();
            this.folderBrowseTreeView = new System.Windows.Forms.TreeView();
            this.EmptyFolderCheckBox = new System.Windows.Forms.CheckBox();
            this.commentGroupBox = new System.Windows.Forms.GroupBox();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.addFolderGroupBox.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.BrowseGroupBox.SuspendLayout();
            this.commentGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 99F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 107F));
            this.tableLayoutPanel1.Controls.Add(this.CloseButton, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.OkButton, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.addFolderGroupBox, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.BrowseGroupBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.folderBrowseTreeView, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.EmptyFolderCheckBox, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.commentGroupBox, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(685, 461);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // CloseButton
            // 
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CloseButton.Location = new System.Drawing.Point(581, 432);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(101, 26);
            this.CloseButton.TabIndex = 1;
            this.CloseButton.Text = "Отмена";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OkButton.Location = new System.Drawing.Point(482, 432);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(93, 26);
            this.OkButton.TabIndex = 2;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // addFolderGroupBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.addFolderGroupBox, 3);
            this.addFolderGroupBox.Controls.Add(this.tableLayoutPanel4);
            this.addFolderGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addFolderGroupBox.Location = new System.Drawing.Point(3, 375);
            this.addFolderGroupBox.Name = "addFolderGroupBox";
            this.addFolderGroupBox.Size = new System.Drawing.Size(679, 51);
            this.addFolderGroupBox.TabIndex = 3;
            this.addFolderGroupBox.TabStop = false;
            this.addFolderGroupBox.Text = "Добавить папку";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.Controls.Add(this.addFolderButton, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.folderNameTextBox, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.createFolderButton, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.cancelNewFolderButton, 3, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(673, 32);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // addFolderButton
            // 
            this.addFolderButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addFolderButton.Location = new System.Drawing.Point(3, 3);
            this.addFolderButton.Name = "addFolderButton";
            this.addFolderButton.Size = new System.Drawing.Size(94, 26);
            this.addFolderButton.TabIndex = 2;
            this.addFolderButton.Text = "Новая папка";
            this.addFolderButton.UseVisualStyleBackColor = true;
            this.addFolderButton.Click += new System.EventHandler(this.addFolderButton_Click);
            // 
            // folderNameTextBox
            // 
            this.folderNameTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.folderNameTextBox.Location = new System.Drawing.Point(103, 3);
            this.folderNameTextBox.Name = "folderNameTextBox";
            this.folderNameTextBox.Size = new System.Drawing.Size(367, 20);
            this.folderNameTextBox.TabIndex = 0;
            this.folderNameTextBox.Visible = false;
            // 
            // createFolderButton
            // 
            this.createFolderButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createFolderButton.Location = new System.Drawing.Point(476, 3);
            this.createFolderButton.Name = "createFolderButton";
            this.createFolderButton.Size = new System.Drawing.Size(94, 26);
            this.createFolderButton.TabIndex = 1;
            this.createFolderButton.Text = "Создать";
            this.createFolderButton.UseVisualStyleBackColor = true;
            this.createFolderButton.Visible = false;
            this.createFolderButton.Click += new System.EventHandler(this.createFolderButton_Click);
            // 
            // cancelNewFolderButton
            // 
            this.cancelNewFolderButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancelNewFolderButton.Location = new System.Drawing.Point(576, 3);
            this.cancelNewFolderButton.Name = "cancelNewFolderButton";
            this.cancelNewFolderButton.Size = new System.Drawing.Size(94, 26);
            this.cancelNewFolderButton.TabIndex = 3;
            this.cancelNewFolderButton.Text = "Не создавать";
            this.cancelNewFolderButton.UseVisualStyleBackColor = true;
            this.cancelNewFolderButton.Visible = false;
            this.cancelNewFolderButton.Click += new System.EventHandler(this.cancelNewFolderButton_Click);
            // 
            // BrowseGroupBox
            // 
            this.BrowseGroupBox.Controls.Add(this.FolderPathTextBox);
            this.BrowseGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BrowseGroupBox.Location = new System.Drawing.Point(3, 3);
            this.BrowseGroupBox.Name = "BrowseGroupBox";
            this.BrowseGroupBox.Size = new System.Drawing.Size(473, 42);
            this.BrowseGroupBox.TabIndex = 0;
            this.BrowseGroupBox.TabStop = false;
            this.BrowseGroupBox.Text = "Введите путь";
            // 
            // FolderPathTextBox
            // 
            this.FolderPathTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FolderPathTextBox.Location = new System.Drawing.Point(3, 16);
            this.FolderPathTextBox.Name = "FolderPathTextBox";
            this.FolderPathTextBox.Size = new System.Drawing.Size(467, 20);
            this.FolderPathTextBox.TabIndex = 0;
            this.FolderPathTextBox.TextChanged += new System.EventHandler(this.FolderPathTextBox_TextChanged);
            // 
            // folderBrowseTreeView
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.folderBrowseTreeView, 3);
            this.folderBrowseTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.folderBrowseTreeView.HideSelection = false;
            this.folderBrowseTreeView.Location = new System.Drawing.Point(3, 51);
            this.folderBrowseTreeView.Name = "folderBrowseTreeView";
            this.folderBrowseTreeView.Size = new System.Drawing.Size(679, 318);
            this.folderBrowseTreeView.TabIndex = 6;
            this.folderBrowseTreeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.folderBrowseTreeView_AfterExpand);
            this.folderBrowseTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.folderBrowseTreeView_AfterSelect);
            // 
            // EmptyFolderCheckBox
            // 
            this.EmptyFolderCheckBox.AutoSize = true;
            this.EmptyFolderCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EmptyFolderCheckBox.Location = new System.Drawing.Point(3, 432);
            this.EmptyFolderCheckBox.Name = "EmptyFolderCheckBox";
            this.EmptyFolderCheckBox.Size = new System.Drawing.Size(473, 26);
            this.EmptyFolderCheckBox.TabIndex = 1;
            this.EmptyFolderCheckBox.Text = "Пустая папка (предполагается использование только плагинов-утилит";
            this.EmptyFolderCheckBox.UseVisualStyleBackColor = true;
            this.EmptyFolderCheckBox.CheckedChanged += new System.EventHandler(this.EmptyFolderCheckBox_CheckedChanged);
            // 
            // commentGroupBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.commentGroupBox, 2);
            this.commentGroupBox.Controls.Add(this.commentTextBox);
            this.commentGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commentGroupBox.Location = new System.Drawing.Point(482, 3);
            this.commentGroupBox.Name = "commentGroupBox";
            this.commentGroupBox.Size = new System.Drawing.Size(200, 42);
            this.commentGroupBox.TabIndex = 7;
            this.commentGroupBox.TabStop = false;
            this.commentGroupBox.Text = "Комментарий";
            this.commentGroupBox.Visible = false;
            // 
            // commentTextBox
            // 
            this.commentTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commentTextBox.Location = new System.Drawing.Point(3, 16);
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.Size = new System.Drawing.Size(194, 20);
            this.commentTextBox.TabIndex = 0;
            this.commentTextBox.TextChanged += new System.EventHandler(this.commentTextBox_TextChanged);
            // 
            // FolderSelectDialog
            // 
            this.AcceptButton = this.OkButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(685, 461);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FolderSelectDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "FolderSelectDialog";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.addFolderGroupBox.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.BrowseGroupBox.ResumeLayout(false);
            this.BrowseGroupBox.PerformLayout();
            this.commentGroupBox.ResumeLayout(false);
            this.commentGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox BrowseGroupBox;
        private System.Windows.Forms.TextBox FolderPathTextBox;
        private System.Windows.Forms.CheckBox EmptyFolderCheckBox;
        private new System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.TreeView folderBrowseTreeView;
        private System.Windows.Forms.Button addFolderButton;
        private System.Windows.Forms.GroupBox addFolderGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox folderNameTextBox;
        private System.Windows.Forms.Button createFolderButton;
        private System.Windows.Forms.Button cancelNewFolderButton;
        private System.Windows.Forms.GroupBox commentGroupBox;
        private System.Windows.Forms.TextBox commentTextBox;
    }
}