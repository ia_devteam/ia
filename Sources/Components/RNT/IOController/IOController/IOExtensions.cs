﻿using System.IO;

namespace IOController
{
    /// <summary>
    /// Класс, реализующий различные методы для работы как с директориями, так и с файлами
    /// </summary>
    public static class IOExtensions
    {
        /// <summary>
        /// Преобразование атрибутов
        /// </summary>
        /// <param name="attributes">Атрибуты в формате .NET Framework</param>
        /// <returns>Атрибуты в формате WinAPI</returns>
        public static WinAPI.WinAPI.FileAttributes ConvertAttributes(FileAttributes attributes)
        {
            //Формируем результат
            WinAPI.WinAPI.FileAttributes ret;

            switch (attributes)
            {
                case FileAttributes.Archive:            ret = WinAPI.WinAPI.FileAttributes.Archive;             break;
                case FileAttributes.Compressed:         ret = WinAPI.WinAPI.FileAttributes.Compressed;          break;
                case FileAttributes.Device:             ret = WinAPI.WinAPI.FileAttributes.Device;              break;
                case FileAttributes.Directory:          ret = WinAPI.WinAPI.FileAttributes.Directory;           break;
                case FileAttributes.Encrypted:          ret = WinAPI.WinAPI.FileAttributes.Encrypted;           break;
                case FileAttributes.Hidden:             ret = WinAPI.WinAPI.FileAttributes.Hidden;              break;
                case FileAttributes.IntegrityStream:    ret = WinAPI.WinAPI.FileAttributes.IntegrityStream;     break;
                case FileAttributes.Normal:             ret = WinAPI.WinAPI.FileAttributes.Normal;              break;
                case FileAttributes.NoScrubData:        ret = WinAPI.WinAPI.FileAttributes.NoScrubData;         break;
                case FileAttributes.NotContentIndexed:  ret = WinAPI.WinAPI.FileAttributes.NotContentIndexed;   break;
                case FileAttributes.Offline:            ret = WinAPI.WinAPI.FileAttributes.Offline;             break;
                case FileAttributes.ReadOnly:           ret = WinAPI.WinAPI.FileAttributes.Readonly;            break;
                case FileAttributes.ReparsePoint:       ret = WinAPI.WinAPI.FileAttributes.ReparsePoint;        break;
                case FileAttributes.SparseFile:         ret = WinAPI.WinAPI.FileAttributes.SparseFile;          break;
                case FileAttributes.System:             ret = WinAPI.WinAPI.FileAttributes.System;              break;
                case FileAttributes.Temporary:          ret = WinAPI.WinAPI.FileAttributes.Temporary;           break;
                default:
                    throw new IOControllerException("Ошибка преобразования атрибутов. Обнаружен неизвестный атрибут.");
            }

            return ret;
        }
    }
}
