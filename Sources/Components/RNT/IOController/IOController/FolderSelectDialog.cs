﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace IOController
{
    /// <summary>
    /// Диалог для выбора папки
    /// </summary>
    public partial class FolderSelectDialog : Form
    {
        #region Для работы с сетевыми папками
        [DllImport("Netapi32.dll", CharSet = CharSet.Unicode)]
        private static extern int NetShareEnum(
             StringBuilder ServerName,
             int level,
             ref IntPtr bufPtr,
             uint prefmaxlen,
             ref int entriesread,
             ref int totalentries,
             ref int resume_handle
             );
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct SHARE_INFO_0
        {
            public string shi0_netname;
        }
        const uint MAX_PREFERRED_LENGTH = 0xFFFFFFFF;
        const int NERR_Success = 0;

        [DllImport("Netapi32.dll", SetLastError = true)]
        static extern int NetApiBufferFree(IntPtr Buffer);

        #endregion

        /// <summary>
        /// выбранный при помощи данного диалога путь
        /// </summary>
        public string SelectedPath;

        /// <summary>
        /// комментарий, введенный пользователем
        /// </summary>
        public string comment;

        private bool enableCreateFolder;

        /// <summary>
        /// 
        /// </summary>
        public FolderSelectDialog(string Caption = "", bool emptyStringPossible = false, bool enableCreateFolder = false, bool enableComment = false, string selectedPath = "")
        {
            InitializeComponent();
            Text = Caption == "" ? "Укажите директорию" : Caption;
            this.enableCreateFolder = enableCreateFolder;
            EmptyFolderCheckBox.Visible = emptyStringPossible;
            commentGroupBox.Visible = enableComment;
            BrowseGroupBox.Text += emptyStringPossible ? " (несуществующая часть пути будет создана)" : "";

            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in drives)
            {
                TreeNode node = new TreeNode(drive.Name);
                try
                {
                    if (Directory.EnumerateDirectories(drive.Name, "*", SearchOption.TopDirectoryOnly).Count() != 0)
                    {
                        node.Nodes.Add("");
                    }
                }
                catch (IOException) { }

                node.Name = drive.Name;

                folderBrowseTreeView.Nodes.Add(node);
            }

            addFolderButton.Enabled = enableCreateFolder;
            FolderPathTextBox.Text = selectedPath;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(FolderPathTextBox.Text) || EmptyFolderCheckBox.Checked || enableCreateFolder)
            {
                if (enableCreateFolder && !Directory.Exists(FolderPathTextBox.Text))
                {
                    Directory.CreateDirectory(FolderPathTextBox.Text);
                }
                DialogResult = DialogResult.OK;
                if (EmptyFolderCheckBox.Checked)
                {
                    SelectedPath = "";
                }
                else
                {
                    SelectedPath = FolderPathTextBox.Text;
                }
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void EmptyFolderCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            BrowseGroupBox.Enabled = folderBrowseTreeView.Enabled = !EmptyFolderCheckBox.Checked;
        }

        private void folderBrowseTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (!changing)
            {
                changing = true;
                Expand(e.Node);
                FolderPathTextBox.Text = e.Node.FullPath;
                changing = false;
            }
        }

        bool changing = false;

        private void Expand(TreeNode Node)
        {            
            IEnumerable<string> dirs;

            int count;
            try
            {
                dirs = Directory.EnumerateDirectories(Node.FullPath, "*", SearchOption.TopDirectoryOnly);
                count = dirs.Count();
            }
            catch
            {
                dirs = getNetFolders(Node.FullPath);
                count = dirs.Count();
            }

            if (Node.Nodes.Count != 0 && Node.Nodes[0].Text == "" || count != Node.Nodes.Count)
            {
                Node.Nodes.Clear();
                try
                {
                    foreach (string dir in dirs)
                    {
                        TreeNode node = new TreeNode(Path.GetFileName(dir));
                        try
                        {
                            if (Directory.GetDirectories(dir, "*", SearchOption.TopDirectoryOnly).Count() != 0)
                            {
                                node.Nodes.Add("");
                            }
                        }
                        catch (IOException) { }
                        catch (UnauthorizedAccessException) { }
                        node.Name = node.Text;
                        Node.Nodes.Add(node);
                    }
                }
                catch (IOException) { }
                catch (UnauthorizedAccessException) { }
            }
        }

        private void folderBrowseTreeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            if (!changing)
            {
                changing = true;
                Expand(e.Node);
                changing = false;
            }
        }

        private List<string> getNetFolders(string server)
        {
            List<string> res = new List<string>();
            int entriesRead = 0;
            int totalEntries = 0;
            int resume_handle = 0;
            int nStructSize = Marshal.SizeOf(typeof(SHARE_INFO_0));
            IntPtr bufPtr = IntPtr.Zero;
            StringBuilder Server = new StringBuilder(server);
            int ret = NetShareEnum(Server, 0, ref bufPtr, MAX_PREFERRED_LENGTH, ref entriesRead, ref totalEntries, ref resume_handle);
            if (ret == NERR_Success)
            {
                IntPtr currentPtr = bufPtr;
                for (int i = 0; i < entriesRead; i++)
                {
                    SHARE_INFO_0 shi0 = (SHARE_INFO_0)Marshal.PtrToStructure(currentPtr, typeof(SHARE_INFO_0));
                    if (!shi0.shi0_netname.EndsWith("$"))
                        res.Add("\\\\" + server + "\\" + shi0.shi0_netname);
                    currentPtr = new IntPtr(currentPtr.ToInt32() + nStructSize);
                }
                NetApiBufferFree(bufPtr);
            }
            return res;
        }

        private void FolderPathTextBox_TextChanged(object sender, EventArgs e)
        {
            FolderPathTextBox.Text = FolderPathTextBox.Text.Replace(@":\\", @":\");
            if (!changing)
            {
                string[] names = FolderPathTextBox.Text.Split(new char[] { '\\' });
                while (names[0] == "")
                {
                    names = names.Skip(1).ToArray();
                    if (names.Length == 0)
                        return;
                }
                if (names.Length > 0)
                {
                    changing = true;
                    if (FolderPathTextBox.Text.StartsWith("\\\\"))
                    {
                        // сетевой путь
                        names[0] = "\\\\" + names[0];
                        // проверим наличие имени или адреса хоста
                        if (folderBrowseTreeView.Nodes.Find("\\\\" + names[0], false).Count() == 0)
                        {
                            if (getNetFolders(names[0]).Count != 0)
                            {
                                TreeNode node = new TreeNode(names[0]);
                                node.Name = names[0];

                                folderBrowseTreeView.Nodes.Add(node);
                                Expand(node);
                            }
                        }
                    }
                    else
                    {
                        names[0] += "\\";
                    }
                    if (Directory.Exists(FolderPathTextBox.Text))
                    {
                        TreeNode node = null;
                        node = folderBrowseTreeView.Nodes.Find(names[0], false).First();
                        Expand(node);
                        names = names.Skip(1).ToArray();
                        foreach (string name in names)
                        {
                            if (name != "")
                            {
                                node = node.Nodes.Find(name, false).First();
                                if (node == null)
                                    node = node.Nodes.Find("\\\\" + name, false).First();
                                Expand(node);
                            }
                        }

                        folderBrowseTreeView.SelectedNode = node;
                    }
                }
                changing = false;
            }
        }

        private void addFolderButton_Click(object sender, EventArgs e)
        {
            folderNameTextBox.Visible = createFolderButton.Visible = cancelNewFolderButton.Visible = true;
            addFolderButton.Enabled = false;
        }

        private void cancelNewFolderButton_Click(object sender, EventArgs e)
        {
            folderNameTextBox.Visible = createFolderButton.Visible = cancelNewFolderButton.Visible = false;
            addFolderButton.Enabled = true;
            folderNameTextBox.Text = "";
        }

        /// <summary>
        /// результат проверки валидности имени создаваемой директории
        /// </summary>
        public enum validationFolderResult
        {
            folderExists, forbiddenSymbol, ok
        }

        /// <summary>
        /// проверка корректности имени создаваемой папки. Она не должна существовать ранее, имя не должно содержать запрещенных символов
        /// </summary>
        /// <param name="path">путь к месту создания</param>
        /// <param name="name">имя папки</param>
        /// <returns></returns>
        private validationFolderResult ValidateFolderName(string path, string name)
        {
            char[] forbidden = {'\\', '/', ':', '*', '>', '<', '?', '|', '"'};
            if (Directory.Exists(Path.Combine(path, name)))
                return validationFolderResult.folderExists;

            if (name.IndexOfAny(forbidden) >= 0)
                return validationFolderResult.forbiddenSymbol;

            return validationFolderResult.ok;
        }

        private void createFolderButton_Click(object sender, EventArgs e)
        {
            validationFolderResult result = ValidateFolderName((string)folderBrowseTreeView.SelectedNode.FullPath, folderNameTextBox.Text);
            if (result != validationFolderResult.ok)
            {
                MessageBox.Show(result == validationFolderResult.folderExists? "Папка уже существует" : "В имени папки запрещены следующие символы:\n \\ / : * > < ? | \"", "Ошибка создания папки");
                return;
            }
            Directory.CreateDirectory(Path.Combine((string)folderBrowseTreeView.SelectedNode.FullPath, folderNameTextBox.Text));
            folderNameTextBox.Visible = createFolderButton.Visible = cancelNewFolderButton.Visible = false;
            addFolderButton.Enabled = true;
            FolderPathTextBox.Text = Path.Combine((string)folderBrowseTreeView.SelectedNode.FullPath, folderNameTextBox.Text);
            folderNameTextBox.Text = "";

        }

        private void commentTextBox_TextChanged(object sender, EventArgs e)
        {
            comment = commentTextBox.Text;
        }

    }
}
