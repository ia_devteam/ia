﻿/*

 Copyright (c) 2005-2006 Tomas Matousek, Ladislav Prosek, Vaclav Novak, Pavel Novak, Jan Benda and Martin Maly.

 The use and distribution terms for this software are contained in the file named License.txt, 
 which can be found in the root of the distribution. By using this software 
 in any fashion, you are agreeing to be bound by the terms of this license.
 
 You must not remove this notice from this software.

*/

using System.Reflection;

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("The Project Team")]
[assembly: AssemblyProduct("PHP Compiler")]
[assembly: AssemblyCopyright("Copyright (c) 2004-2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
//[assembly: CLSCompliant(true)]

#if !SILVERLIGHT
[assembly: AssemblyTitle("Core")]
[assembly: AssemblyDescription("Core Functionality")]
[assembly: AssemblyVersion("3.0.2.4")]
//[assembly: AllowPartiallyTrustedCallers]
#else
[assembly: AssemblyTitle("Core (Silverlight)")]
[assembly: AssemblyDescription("Core Functionality (Silverlight)")]
[assembly: AssemblyVersion("3.0.2.4")]
#endif
