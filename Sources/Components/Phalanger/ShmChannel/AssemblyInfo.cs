﻿/*

 Copyright (c) 2004-2006 Ladislav Prosek.

 The use and distribution terms for this software are contained in the file named License.txt, 
 which can be found in the root of the distribution. By using this software 
 in any fashion, you are agreeing to be bound by the terms of this license.
 
 You must not remove this notice from this software.

*/

using System.Reflection;

[assembly: AssemblyTitle("ShmChannel")]
[assembly: AssemblyDescription("Shared Memory Channel")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("The  Project Team")]
[assembly: AssemblyProduct("PHPComp")]
[assembly: AssemblyCopyright("Copyright *")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("3.0.2.4")]
[assembly: AssemblyKeyName("")]
