﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;


namespace ImpExp
{
    /// <summary>
    /// кдасс для разбора бинарного файла
    /// </summary>
    public class ImpExp
    {
        [DllImport("dbghelp.dll", SetLastError = true, PreserveSig = true)]
        static extern int UnDecorateSymbolName([In] [MarshalAs(UnmanagedType.LPStr)] string DecoratedName, [Out] StringBuilder UnDecoratedName, [In] [MarshalAs(UnmanagedType.U4)] int UndecoratedLength, [In] [MarshalAs(UnmanagedType.U4)] UnDecorateFlags Flags);
        [Flags]
        enum UnDecorateFlags
        {
            UNDNAME_COMPLETE = (0x0000),  // Enable full undecoration
            UNDNAME_NO_LEADING_UNDERSCORES = (0x0001),  // Remove leading underscores from MS extended keywords
            UNDNAME_NO_MS_KEYWORDS = (0x0002),  // Disable expansion of MS extended keywords
            UNDNAME_NO_FUNCTION_RETURNS = (0x0004),  // Disable expansion of return type for primary declaration
            UNDNAME_NO_ALLOCATION_MODEL = (0x0008),  // Disable expansion of the declaration model
            UNDNAME_NO_ALLOCATION_LANGUAGE = (0x0010),  // Disable expansion of the declaration language specifier
            UNDNAME_NO_MS_THISTYPE = (0x0020),  // NYI Disable expansion of MS keywords on the 'this' type for primary declaration
            UNDNAME_NO_CV_THISTYPE = (0x0040),  // NYI Disable expansion of CV modifiers on the 'this' type for primary declaration
            UNDNAME_NO_THISTYPE = (0x0060),  // Disable all modifiers on the 'this' type
            UNDNAME_NO_ACCESS_SPECIFIERS = (0x0080),  // Disable expansion of access specifiers for members
            UNDNAME_NO_THROW_SIGNATURES = (0x0100),  // Disable expansion of 'throw-signatures' for functions and pointers to functions
            UNDNAME_NO_MEMBER_TYPE = (0x0200),  // Disable expansion of 'static' or 'virtual'ness of members
            UNDNAME_NO_RETURN_UDT_MODEL = (0x0400),  // Disable expansion of MS model for UDT returns
            UNDNAME_32_BIT_DECODE = (0x0800),  // Undecorate 32-bit decorated names
            UNDNAME_NAME_ONLY = (0x1000),  // Crack only the name for primary declaration;
            // return just [scope::]name.  Does expand template params
            UNDNAME_NO_ARGUMENTS = (0x2000),  // Don't undecorate arguments to function
            UNDNAME_NO_SPECIAL_SYMS = (0x4000),  // Don't undecorate special names (v-table, vcall, vector xxx, metatype, etc)
        }



        /// <summary>
        /// Экспортируемые функции в библиотеке
        /// </summary>
        /// <param name="fname">имя библиотеки</param>
        /// <returns>список экспортируемых функций</returns>
        public static List<string> Export(string fname)
        {
            List<string> lse = new List<string>();
            //делаем попытку прочитать ilDasm'ом
            string stdout = ExecuteilDasm(fname, "/pub");
            //если получилось - это .NET
            if (stdout != "")
            {
                Encoding enc3 = System.Text.Encoding.UTF8;
                MemoryStream ms = new MemoryStream(enc3.GetBytes(stdout));
                StreamReader sr = new StreamReader(ms);

                string s;
                Encoding enc = sr.CurrentEncoding;
                while ((s = sr.ReadLine()) != null)
                {
                    if (s.Contains("// end of"))
                    {
                        Encoding enc1 = System.Text.Encoding.GetEncoding(1251);
                        byte[] b = enc1.GetBytes(s);
                        Encoding enc2 = System.Text.Encoding.GetEncoding(866);
                        string ss = enc2.GetString(b);
                        Regex re = new Regex(" ");
                        List<string> ls = new List<string>();
                        foreach (String sub in re.Split(ss))
                        {
                            if (sub != null && sub != "")
                            {
                                ls.Add("0\t" + sub);
                            }
                        }
                        lse.Add(ls[ls.Count - 1]);
                    }
                }
                sr.Close();
            }
            else
            {
                //иначе - Native
                List<string[]> log = new PE().exports(fname);
                lse = log.Select(x => x.Aggregate((a, b) => a + '\t' + b)).ToList();

                /*
                //иначе - Native
                stdout = ExecuteExtProg("tdump.exe", "/ee " + "\"" + fname + "\"");
                if (stdout != null)
                {
                    Encoding enc3 = System.Text.Encoding.UTF8;
                    MemoryStream ms = new MemoryStream(enc3.GetBytes(stdout));
                    StreamReader sr = new StreamReader(ms);

                    string s;
                    Encoding enc = sr.CurrentEncoding;
                    while ((s = sr.ReadLine()) != null)
                    {
                        if (!s.Contains("EXPORT"))
                        {
                            continue;
                        }
                        Encoding enc1 = System.Text.Encoding.GetEncoding(1251);
                        byte[] b = enc1.GetBytes(s);
                        Encoding enc2 = System.Text.Encoding.GetEncoding(866);
                        string ss = enc2.GetString(b);
                        string[] sa = ss.Split(new char[] { ':', '=' });
                        string ord = "0";
                        int nord;
                        if (sa.Length >= 1)
                            ord = sa[1];
                        if (int.TryParse(ord, out nord))
                        {
                            ord = nord.ToString();
                        }

                        ss = ss.Substring(ss.IndexOf("=") + 1).Trim('\'');
                        if (ss[0] == '?')
                        {
                            string sub1 = ExecuteExtProg("undname.exe", ss);
                            int ind = sub1.IndexOf("is :- ") + "is :- ".Length;
                            string sub2 = sub1.Substring(ind);
                            lse.Add(sub1.Substring(ind).Trim(new char[] { '\"', ' ', '\r', '\n' }));
                        }
                        else
                        {
                            lse.Add(ord + "\t" + ss);
                        }
                    }
                    sr.Close();
                }


                // Если ничего не нашли при помощи TDump и ilDasm, по пробуем Dumpbin
                if (lse.Count == 0)
                {
                    stdout = ExecuteExtProg("dumpbin.exe", "/EXPORTS " + "\"" + fname + "\"");
                    if (stdout != null)
                    {
                        Encoding enc3 = System.Text.Encoding.UTF8;
                        MemoryStream ms = new MemoryStream(enc3.GetBytes(stdout));
                        StreamReader sr = new StreamReader(ms);

                        string s;
                        Encoding enc = sr.CurrentEncoding;

                        s = sr.ReadLine();
                        // доходим до описаний функций. 
                        while (s!= null && !s.StartsWith("    ordinal"))
                        { 
                            s = sr.ReadLine(); 
                        }
                        if (s == null)
                        {
                            sr.Close();
                            return lse;
                        }
                        sr.ReadLine();

                        int ord = 0;

                        while ((s = sr.ReadLine()) != null)
                        {
                            if (s == "")
                            {
                                break;
                            }
                            ++ord;
                            s = s.Substring(26);
                            Encoding enc1 = System.Text.Encoding.GetEncoding(1251);
                            byte[] b = enc1.GetBytes(s);
                            Encoding enc2 = System.Text.Encoding.GetEncoding(866);
                            string ss = enc2.GetString(b);

                            if (ss[0] == '?')
                            {
                                // есть декорирование
                                string sub1 = ExecuteExtProg("undname.exe", ss);
                                int ind = sub1.IndexOf("is :- ") + "is :- ".Length;
                                string sub2 = sub1.Substring(ind);
                                lse.Add(ord + "\t" + sub1.Substring(ind).Trim(new char[] { '\"', ' ', '\r', '\n' }));
                            }
                            else
                            {
                                lse.Add(ord + "\t" + ss);
                            }
                        }
                        sr.Close();
                    }
                }
                 */ 
            }

            return lse;
        }
        
        /// <summary>
        /// Импортируемые функции в библиотеке
        /// </summary>
        /// <param name="fname">имя библиотеки</param>
        /// <returns>список библиотек и импортируемых функций, разделенные табуляцией </returns>
        public static List<string> Import(string fname)
        {

            List<string> lse = new List<string>();
            // пробуем прочитать при помощи ilDasm. Если удалось - это .NET
            string stdout = ExecuteilDasm(fname, "");
            if (stdout != "")
            {
                Encoding enc3 = System.Text.Encoding.UTF8;
                MemoryStream ms = new MemoryStream(enc3.GetBytes(stdout));
                StreamReader sr = new StreamReader(ms);

                string s;
                string ImpDllName = "";
                string ImpFuncName = "";

                Encoding enc = sr.CurrentEncoding;


                string tts;
                List<string> Added = new List<string>();


                while ((s = sr.ReadLine()) != null)
                {
                    if (s.Contains("// =============== CLASS MEMBERS DECLARATION ==================="))
                    {
                        break;
                    }
                    if (s.Contains(".assembly ") && !s.Contains(".assembly extern "))
                    {
                        ImpDllName = s.Substring(s.LastIndexOf(' ') + 1);
                    }
                }

                while ((s = sr.ReadLine()) != null)
                {
                    //если в дампе есть указание на вызов функции,
                    if ((s.Contains(":  call ") || s.Contains(":  callvirt ") || s.Contains(":  newobj ")) && !s.EndsWith("\""))
                    {
                        //то вытаскиваем имя
                        tts = killForCalledMethod(s);
                        if (tts.IndexOf('[') >= 0)
                        {
                            if (!Added.Contains(tts))
                            {
                                Added.Add(tts);
                                //если указано имя сборки, из которой импортируется функция, то запомнить ее, если раньше не запоминали
                                ImpDllName = tts.Substring(tts.IndexOf('[') + 1, tts.IndexOf(']') - tts.IndexOf('[') - 1);
                                ImpFuncName = tts.Substring(tts.IndexOf(']') + 1);
                                if (!lse.Contains(ImpDllName + "\t" + ImpFuncName + "\t0"))
                                    lse.Add(ImpDllName + "\t" + ImpFuncName + "\t0");
                            }
                        }
                    }
                }
                sr.Close();
            }
            else
            {
                // иначе - Native 
                List<string[]> log = new PE().imports(fname);
                lse = log.Select(x => x.Aggregate((a, b) => a + '\t' + b)).ToList();

                // Если ничего не нашли при помощи TDump и ilDasm, по пробуем Dumpbin
                if (lse.Count == 0)
                {
                    string ImpDllName = "";
                    string ImpFuncName = "";
                    string ImpOrdNum = "";
                    stdout = ExecuteExtProg("Common\\dumpbin.exe", "/IMPORTS " + "\"" + fname + "\"");
                    if (stdout != null)
                    {
                        // конвертация кодировки
                        Encoding enc3 = System.Text.Encoding.UTF8;
                        MemoryStream ms = new MemoryStream(enc3.GetBytes(stdout));
                        StreamReader sr = new StreamReader(ms);

                        string s = null;

                        // заголовок
                        for (int i = 0; i < 10; ++i)
                            s = sr.ReadLine();

                        while ((s = sr.ReadLine()) != null)
                        {
                            // именя библиотек начинаются с 4 пробелов
                            if (s.StartsWith("    "))
                            {
                                s = s.Substring(4);
                                if (!s.StartsWith(" "))
                                {
                                    ImpDllName = s;
                                }
                            }

                            // имена функций начинаются с кучи пробелов. Меньше нельзя - описание сегментов тогда подойдет тоже
                            if (s.StartsWith("                ") && !(s.Contains("Import Name Table") || s.Contains("Import Address Table") || s.Contains("time date stamp") || s.Contains("Index of first forwarder reference")))
                            {
                                s = s.Trim();
                                string[] ss = s.Split(new char[1] { ' ' });
                                ImpFuncName = ss[1];
                                ImpOrdNum = ss[0];

                                lse.Add(ImpDllName + "\t" + ImpFuncName + "\t" + ImpOrdNum);
                            }
                        }
                        sr.Close();
                    }

                    // если и Dumpbin ничего не нашел - пробуем objdump.exe (портированная из Linux утилита для анализа Elf-файлов)
                    if (lse.Count == 0)
                    {
                        Dictionary<string, List<string>> libs = new Dictionary<string, List<string>>();
                        string libStart = "  NEEDED               ";
                        string libStart2 = "  required from ";

                        // получаем библиотеки
                        stdout = ExecuteExtProg("Common\\objdump.exe", "-p -C " + "\"" + fname + "\"");
                        if (stdout != null)
                        {
                            // конвертация кодировки
                            Encoding enc3 = System.Text.Encoding.UTF8;
                            MemoryStream ms = new MemoryStream(enc3.GetBytes(stdout));
                            StreamReader sr = new StreamReader(ms);

                            string s = null;

                            // заголовок
                            while ((s = sr.ReadLine()) != null)
                            {
                                if (s.StartsWith(libStart))
                                {
                                    libs.Add(s.Substring(libStart.Length), new List<string>());
                                }

                                if (s == "Version References:")
                                    break;
                            }
                            libs.Add("", new List<string>());
                            string name = "";
                            while ((s = sr.ReadLine()) != null)
                            {
                                if (s.StartsWith(libStart2))
                                {
                                    name = s.Substring(libStart2.Length);
                                    name = name.Substring(0, name.Length - 1);
                                }
                                else
                                {                                    
                                    if (libs.ContainsKey(name))
                                    {
                                        string[] ss = s.Split(new char[] { ' ', '\t' });
                                        libs[name].Add(ss[ss.Count() - 1]);
                                    }
                                }
                            }
                        }

                        // получаем функции
                        stdout = ExecuteExtProg("Common\\objdump.exe", "-T -C " + "\"" + fname + "\"");
                        if (stdout != null)
                        {
                            // конвертация кодировки
                            Encoding enc3 = System.Text.Encoding.UTF8;
                            MemoryStream ms = new MemoryStream(enc3.GetBytes(stdout));
                            StreamReader sr = new StreamReader(ms);

                            string s = null;

                            // заголовок
                            for (int i = 0; i < 4; ++i)
                                s = sr.ReadLine();

                            while ((s = sr.ReadLine()) != null)
                            {
                                if (string.IsNullOrEmpty(s))
                                    continue;
                                
                                s = s.Substring(s.IndexOf("\t") + 1);
                                string[] ss = s.Split(new char[] { ' ', '\t' });

                                ImpDllName = "";
                                foreach(string lib in libs.Keys)
                                {
                                    if (ss.Length > 2)
                                    {
                                        if (libs[lib].Contains(ss[2]))
                                            ImpDllName = lib;
                                    }
                                    else
                                    {
                                        ImpDllName = "";
                                    }
                                }
                                ImpFuncName = "";

                                ss = ss.Skip(ss.Length > 2 ? 3 : 1).ToArray();

                                foreach (string elem in ss)
                                {
                                    if (!string.IsNullOrEmpty(elem))
                                        ImpFuncName += elem + " ";
                                }
                                ImpOrdNum = "0";

                                lse.Add(ImpDllName + "\t" + ImpFuncName + "\t" + ImpOrdNum);
                            }
                            sr.Close();
                        }
                    }
                }
            }
            return lse;
        }
        
        /// <summary>
        /// вынимаем из строки описания имя функции
        /// </summary>
        /// <param name="l">исходная строка</param>
        /// <returns>результат</returns>
        public static string killForCalledMethod(string l)
        {
            int i = l.LastIndexOf('(') - 1;
            int j = i;
            int counter = 0;
            while (l[i] != ' ' || counter > 0)
            {
                if (l[i] == '>')
                {
                    ++counter;
                }
                if (l[i] == '<')
                {
                    --counter;
                }
                --i;
            }
            ++i;
            return l.Substring(i, j - i + 1);
        }

        /// <summary>
        /// Запуск внешней программы с параметрами
        /// </summary>
        /// <param name="ProgName">имя внешней программы</param>
        /// <param name="progargs">аргументы внешней программы</param>
        /// <returns>список библиотек и импортируемых функций, разделенные табуляцией </returns>
        public static string ExecuteExtProg(string ProgName, string progargs)
        {
            string strStdout = "";
            string tfile;
            if (!System.IO.File.Exists(ProgName))
            {
                throw new NotImplementedException("Программа " + ProgName + " не найдена!");
            }
            tfile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

            FileStream f = new FileStream(tfile, FileMode.Create);
            f.Close();

            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = "cmd";
            cmdStartInfo.Arguments = "/c " + ProgName + " " + progargs + " >" + "\"" + tfile + "\"";
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = cmdStartInfo;

            p.Start();
            p.WaitForExit();
            if (p.ExitCode != 0)
                return null;

            if (File.Exists(tfile))
            {
                strStdout = File.ReadAllText(tfile);
                File.Delete(tfile);
            }
            return strStdout;
        }

        /// <summary>
        /// Выполнить ilDasm
        /// </summary>
        /// <param name="DllName">файл dll-библиотеки</param>
        /// <param name="args">дополнительные параметры</param>
        /// <returns>протокол</returns>
        public static string ExecuteilDasm(string DllName, string args)
        {
            string strStdout = "";
            string tfile;
            int ret = -1;
            if (!System.IO.File.Exists("Common\\ilDasm.exe"))
            {
                throw new NotImplementedException("Программа " + "ilDasm.exe" + " не найдена!");
            }
            tfile = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

            FileStream f = new FileStream(tfile, FileMode.Create);
            f.Close();

            ProcessStartInfo cmdStartInfo = new ProcessStartInfo();
            cmdStartInfo.FileName = "Common\\ilDasm.exe";
            cmdStartInfo.Arguments = " /text " + args + " " + "\"" + DllName + "\"" + " " + "/out=\"" + tfile + "\"";
            cmdStartInfo.UseShellExecute = false;
            cmdStartInfo.CreateNoWindow = true;
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = cmdStartInfo;

            p.Start();
            p.WaitForExit();
            ret = p.ExitCode;
            if (ret != 0)
                return "";
            else
            {
                if (File.Exists(tfile))
                {
                    strStdout = File.ReadAllText(tfile);
                    File.Delete(tfile);
                }
                return strStdout;
            }
        }
 
    }

    /// <summary>
    /// Класс для прямого чтения сеций экспорта и импорта файлов PE
    /// </summary>
    public class PE
    {
        /// <summary>
        /// DOS заголовок файла PE
        /// </summary>
        public struct IMAGE_DOS_HEADER
        {
            // DOS .EXE header 
            /// <summary>
            /// Magic number
            /// </summary>
            public UInt16 e_magic;         
            /// <summary>
            /// Bytes on last page of file 
            /// </summary>
            public UInt16 e_cblp;         
            /// <summary>
            /// Pages in file
            /// </summary>
            public UInt16 e_cp;         
            /// <summary>
            /// Relocations
            /// </summary>
            public UInt16 e_crlc;         
            /// <summary>
            /// Size of header in paragraphs
            /// </summary>  
            public UInt16 e_cparhdr;       
            /// <summary>
            /// Minimum extra paragraphs needed
            /// </summary>
            public UInt16 e_minalloc;         
            /// <summary>
            /// Maximum extra paragraphs needed
            /// </summary>  
            public UInt16 e_maxalloc;         
            /// <summary>
            /// Initial (relative) SS value
            /// </summary>  
            public UInt16 e_ss;         
            /// <summary>
            /// Initial SP value
            /// </summary>  
            public UInt16 e_sp;         
            /// <summary>
            /// Checksum
            /// </summary>  
            public UInt16 e_csum;         
            /// <summary>
            /// Initial IP value
            /// </summary>  
            public UInt16 e_ip;         
            /// <summary>
            /// Initial (relative) CS value
            /// </summary>  
            public UInt16 e_cs;         
            /// <summary>
            /// File address of relocation table
            /// </summary>  
            public UInt16 elfarlc;         
            /// <summary>
            /// Overlay number
            /// </summary>  
            public UInt16 e_ovno;         
            /// <summary>
            /// Reserved words
            /// </summary>
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public ushort[] e_res;       
            /// <summary>
            /// OEM identifier (for e_oeminfo) 
            /// </summary>  
            public UInt16 e_oemid;        
            /// <summary>
            /// OEM information; e_oemid specific
            /// </summary> 
            public UInt16 e_oeminfo;         
            /// <summary>
            /// Reserved words
            /// </summary>
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public ushort[] e_res2;         
            /// <summary>
            /// File address of new exe header
            /// </summary>
            public Int32 elfanew;         
        }
        /// <summary>
        /// NT заголовок файла PE для 32 разрядных модулей
        /// </summary>
        public struct IMAGE_NT_HEADERS32
        {
            /// <summary>
            /// 
            /// </summary>
            public UInt32 Signature;
            /// <summary>
            /// 
            /// </summary>
            public IMAGE_FILE_HEADER FileHeader;
            /// <summary>
            /// 
            /// </summary>
            public IMAGE_OPTIONAL_HEADER32 OptionalHeader;
        }
        /// <summary>
        /// NT заголовок файла PE для 64 разрядных модулей
        /// </summary>
        public struct IMAGE_NT_HEADERS64
        {
            /// <summary>
            /// 
            /// </summary>
            public UInt32 Signature;
            /// <summary>
            /// 
            /// </summary>
            public IMAGE_FILE_HEADER FileHeader;
            /// <summary>
            /// 
            /// </summary>
            public IMAGE_OPTIONAL_HEADER64 OptionalHeader;
        }
        /// <summary>
        /// Файловый заголовок
        /// </summary>
        public struct IMAGE_FILE_HEADER
        {
            public FHMachine Machine;
            public UInt16 NumberOfSections;
            public UInt32 TimeDateStamp;
            public UInt32 PointerToSymbolTable;
            public UInt32 NumberOfSymbols;
            public UInt16 SizeOfOptionalHeader;
            public UInt16 Characteristics;
        }
        /// <summary>
        /// Опциональный заголовок для 32 разрядных модулей
        /// </summary>
        public struct IMAGE_OPTIONAL_HEADER32
        {
            //
            // Standard fields.
            //

            public UInt16 Magic; // поле указывает на основное предназначение программы. абсолютно всем наплевать в него 
            public byte MajorLinkerVersion; //старший номер версии использовавшегося при создании линкера 
            public byte MinorLinkerVersion; //младший номер версии использовавшегося при создании линкера (эти 2 поля загрузчик пока игнорирует)
            public UInt32 SizeOfCode; //размер именно программного кода в файле, KERNEL использует это значение для фактического отведения памяти под загружаемую программу, установка этого значения слишком маленьким приведет к выдаче идиотского сообщения о нехватке памяти, хотя ее может быть валом 
            public UInt32 SizeOfInitializedData; //размер секции инициализированных данных, очевидно не используется в Windows'95, но используется в NT, назначение аналогично приведенному выше 
            public UInt32 SizeOfUninitializedData; //размер секции неинициализированных данных, сложно сказать, как эти 3 поля корреспондируют между собой, но лучше с ними по честному ;-) явно видно, что формат разрабатывали одни, а реализовывали его другие. Рекомендую изучить регионы памяти и VirtualXXX функции
            public UInt32 AddressOfEntryPoint; //адрес, относительно Image Base по которому передается управление при запуске программы или адрес инициализации/завершения библиотеки 
            public UInt32 BaseOfCode; //RVA секции, которая содержит программный код (как будто бы она одна единственная ;-) ) судя по всему никем не используется (но установлено верно) 
            public UInt32 BaseOfData; //RVA секции содержащей якобы данные, в реальных экзешниках указывает и на .data и на .bss и еще бог знает куда, вряд ли кем-нибудь используется 

            //
            // NT additional fields.
            //

            public UInt32 ImageBase; //виртуальный начальный адрес загрузки программы (ее первого байта). Должен быть на границе 64 Кб (связано с системой памяти Windows'95) 
            public UInt32 SectionAlignment; //выравнивание программных секций, должен быть степенью 2 между 512 и 256М включительно, так же связано с системой памяти. При использовании других значений программа не загрузится
            public UInt32 FileAlignment; //фактор используемый для выравнивания секций в программном файле. В байтовом значении указывает на границу на которую секции дополняются 0 при размещении в файле. Большое значение приводит к нерациональному использованию дискового пространства, маленькое увеличивает компактность, но и снижает скорость загрузки. Должен быть степенью 2 в диапазоне от 512 до 64К включительно. Прочие значения вызовут ошибку загрузки файла. Я так думаю, что размер файла штука более важная. 
            public UInt16 MajorOperatingSystemVersion; //старший номер версии операционки необходимый для запуска программы. (нулевое значение не позволяет запустить программу, остальные игнорируются проверялось на OSR2) 
            public UInt16 MinorOperatingSystemVersion; //младший номер версии операц. 
            public UInt16 MajorImageVersion; //пользовательский номер версии, задается пользователем при линковке программы и им же и используется
            public UInt16 MinorImageVersion; // аналогично, младший номер 
            public UInt16 MajorSubsystemVersion; //старший номер версии подсистемы, черт его знает как он использается, по моему всяких версий уже через край
            public UInt16 MinorSubsystemVersion; // аналогично, младший номер  
            public UInt32 Win32VersionValue;
            public UInt32 SizeOfImage; //виртуальный размер в байтах всего загружаемого образа, вместе с заголовками, кратен Object align
            public UInt32 SizeOfHeaders; //общий размер всех заголовков: DOS Stub + PE Header + Object Table 
            public UInt32 CheckSum; //контрольная сумма всего файла, опять же как и в DOS'е ее никто не контролирует, а линкер ее ставит в 0 при линковке Предполагалось ее рассчитывать как инверсию суммы всех байтов файла. 
            public UInt16 Subsystem; //операционная подсистема необходимая для запуска данного файла (GUI, консоль...) 
            public UInt16 DllCharacteristics; //указывает на специальные потребности при загрузке, начиная с NT 3.5 устарел и не используется 
            public UInt32 SizeOfStackReserve; // память требуемая для стека приложения, память резервируется, но выделяется только Stack Commit Size байтов, следующая страница является охранной. Когда приложение достигает этой страницы, то страница становится доступной, а следующая страница - охранной, и так до достижения нижней границы, после чего Windows'95 убивает программу с воплями об исключении у нее в стеке 
            public UInt32 SizeOfStackCommit; //объем памяти отводимой в стеке немедленно после загрузки 
            public UInt32 SizeOfHeapReserve; //максимальный возможный размер локального хипа 
            public UInt32 SizeOfHeapCommit; //отводимый при загрузке хип 
            public UInt32 LoaderFlags; //? начиная с NT 3.5 объявлено неиспользуемым, назначение неясно, но в целом связано с поддержкой отладки
            public UInt32 NumberOfRvaAndSizes; //указывает размер массива VA/Size который следует ниже, данная фича зарезервирована под будущие расширения формата. В данный момент его значение всегда равно 10h 
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = IMAGE_NUMBEROF_DIRECTORY_ENTRIES)]
            public IMAGE_DATA_DIRECTORY[] DataDirectory;
        }
        /// <summary>
        /// Опциональный заголовок для 64 разрядных модулей
        /// </summary>
        public struct IMAGE_OPTIONAL_HEADER64
        {
            public UInt16 Magic;
            public byte MajorLinkerVersion;
            public byte MinorLinkerVersion;
            public UInt32 SizeOfCode;
            public UInt32 SizeOfInitializedData;
            public UInt32 SizeOfUninitializedData;
            public UInt32 AddressOfEntryPoint;
            public UInt32 BaseOfCode;
            public UInt64 ImageBase;
            public UInt32 SectionAlignment;
            public UInt32 FileAlignment;
            public UInt16 MajorOperatingSystemVersion;
            public UInt16 MinorOperatingSystemVersion;
            public UInt16 MajorImageVersion;
            public UInt16 MinorImageVersion;
            public UInt16 MajorSubsystemVersion;
            public UInt16 MinorSubsystemVersion;
            public UInt32 Win32VersionValue;
            public UInt32 SizeOfImage;
            public UInt32 SizeOfHeaders;
            public UInt32 CheckSum;
            public UInt16 Subsystem;
            public UInt16 DllCharacteristics;
            public UInt64 SizeOfStackReserve;
            public UInt64 SizeOfStackCommit;
            public UInt64 SizeOfHeapReserve;
            public UInt64 SizeOfHeapCommit;
            public UInt32 LoaderFlags;
            public UInt32 NumberOfRvaAndSizes;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = IMAGE_NUMBEROF_DIRECTORY_ENTRIES)]
            public IMAGE_DATA_DIRECTORY[] DataDirectory;
        }
        /// <summary>
        /// Заголовок секций
        /// </summary>
        public struct IMAGE_SECTION_HEADER
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = IMAGE_SIZEOF_SHORT_NAME)]
            public byte[] Name; //Имя объекта, остаток заполнен нулями, если имя объекта имеет длину 8 символов, то заключительного 0 нет. Некоторые PE дамперы падают на этом факте. Имя - штука отфонарная и никого ни к чему не обязывает
            public UInt32 VirtualSize; //Union //виртуальный размер секции, именно столько памяти будет отведено под секцию. Если Virtual Size превышает Physical Size, то разница заполняется нулями, так определяются секции неинициализированных данных (Physical Size = 0) 
            public UInt32 VirtualAddress; //размещение секции в памяти, виртуальный ее адрес относительно Image Base. Позиция каждой секции выравнена на границу Object align (степень 2 от 512 до 256М включительно, по умолчанию 64К) и секции упакованы впритык друг к другу, впрочем, можно это не соблюдать
            public UInt32 SizeOfRawData; //размер секции (ее инициализированной части) в файле, кратно полю File align в заголовке PE Header, должно быть меньше или равно Virtual Size. Играя с этим полем можно добиться некоторых результатов ;-) загрузчик по идее хлопает всю секцию в отведенное ОЗУ 
            public UInt32 PointerToRawData; //физическое смещение относительно начала EXE файла, выровнено на границу File align поля заголовка PE Header. Смещение используется загрузчиком как seek значение
            public UInt32 PointerToRelocations;
            public UInt32 PointerToLinenumbers;
            public UInt16 NumberOfRelocations;
            public UInt16 NumberOfLinenumbers;
            public UInt32 Characteristics;
        }
        /// <summary>
        /// Список директорных входов
        /// </summary>
        public enum DirectoryEntries
        {
            IMAGE_DIRECTORY_ENTRY_EXPORT = 0,   // Export Directory
            IMAGE_DIRECTORY_ENTRY_IMPORT = 1,   // Import Directory
            IMAGE_DIRECTORY_ENTRY_RESOURCE = 2,   // Resource Directory
            IMAGE_DIRECTORY_ENTRY_EXCEPTION = 3,   // Exception Directory
            IMAGE_DIRECTORY_ENTRY_SECURITY = 4,   // Security Directory
            IMAGE_DIRECTORY_ENTRY_BASERELOC = 5,   // Base Relocation Table
            IMAGE_DIRECTORY_ENTRY_DEBUG = 6,   // Debug Directory
            //      IMAGE_DIRECTORY_ENTRY_COPYRIGHT       7   // (X86 usage)
            IMAGE_DIRECTORY_ENTRY_ARCHITECTURE = 7,   // Architecture Specific Data
            IMAGE_DIRECTORY_ENTRY_GLOBALPTR = 8,   // RVA of GP
            IMAGE_DIRECTORY_ENTRY_TLS = 9,   // TLS Directory
            IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG = 10,   // Load Configuration Directory
            IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT = 11,   // Bound Import Directory in headers
            IMAGE_DIRECTORY_ENTRY_IAT = 12,   // Import Address Table
            IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT = 13,   // Delay Load Import Descriptors
            IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR = 14   // COM Runtime descriptor

        }
        /// <summary>
        /// Список типов машин
        /// </summary>
        public enum FHMachine : ushort
        {
            IMAGE_FILE_MACHINE_UNKNOWN = 0,
            IMAGE_FILE_MACHINE_I386 = 0x014c,  // Intel 386.
            IMAGE_FILE_MACHINE_R3000 = 0x0162,  // MIPS little-endian, 0x160 big-endian
            IMAGE_FILE_MACHINE_R4000 = 0x0166,  // MIPS little-endian
            IMAGE_FILE_MACHINE_R10000 = 0x0168,  // MIPS little-endian
            IMAGE_FILE_MACHINE_WCEMIPSV2 = 0x0169,  // MIPS little-endian WCE v2
            IMAGE_FILE_MACHINE_ALPHA = 0x0184,  // Alpha_AXP
            IMAGE_FILE_MACHINE_SH3 = 0x01a2,  // SH3 little-endian
            IMAGE_FILE_MACHINE_SH3DSP = 0x01a3,
            IMAGE_FILE_MACHINE_SH3E = 0x01a4,  // SH3E little-endian
            IMAGE_FILE_MACHINE_SH4 = 0x01a6,  // SH4 little-endian
            IMAGE_FILE_MACHINE_SH5 = 0x01a8,  // SH5
            IMAGE_FILE_MACHINE_ARM = 0x01c0,  // ARM Little-Endian
            IMAGE_FILE_MACHINE_THUMB = 0x01c2,
            IMAGE_FILE_MACHINE_AM33 = 0x01d3,
            IMAGE_FILE_MACHINE_POWERPC = 0x01F0,  // IBM PowerPC Little-Endian
            IMAGE_FILE_MACHINE_POWERPCFP = 0x01f1,
            IMAGE_FILE_MACHINE_IA64 = 0x0200,  // Intel 64
            IMAGE_FILE_MACHINE_MIPS16 = 0x0266,  // MIPS
            IMAGE_FILE_MACHINE_ALPHA64 = 0x0284,  // ALPHA64
            IMAGE_FILE_MACHINE_MIPSFPU = 0x0366,  // MIPS
            IMAGE_FILE_MACHINE_MIPSFPU16 = 0x0466,  // MIPS
            IMAGE_FILE_MACHINE_AXP64 = IMAGE_FILE_MACHINE_ALPHA64,
            IMAGE_FILE_MACHINE_TRICORE = 0x0520,  // Infineon
            IMAGE_FILE_MACHINE_CEF = 0x0CEF,
            IMAGE_FILE_MACHINE_EBC = 0x0EBC,  // EFI Byte Code
            IMAGE_FILE_MACHINE_AMD64 = 0x8664,  // AMD64 (K8)
            IMAGE_FILE_MACHINE_M32R = 0x9041,  // M32R little-endian
            IMAGE_FILE_MACHINE_CEE = 0xC0EE
        }
        /// <summary>
        /// Структура директорных входов
        /// </summary>
        public struct IMAGE_DATA_DIRECTORY
        {
            public UInt32 VirtualAddress;
            public UInt32 Size;
        }
        /// <summary>
        /// Структура директории Экспорта
        /// </summary>
        public struct IMAGE_EXPORT_DIRECTORY
        {
            public UInt32 Characteristics;
            public UInt32 TimeDateStamp; //время и дата создания экспортных данных 
            public UInt16 MajorVersion; //опять для нас, блин, старший номер версии таблицы экспорта как хочешь, так и используй 
            public UInt16 MinorVersion; //аналогично, младший 
            public UInt32 Name; //RVA строки указывающей на имя нашей библиотеки
            public UInt32 Base; //начальный номер экспорта, для функций нашей библиотеки, обычно установлено в 1, но не факт 
            public UInt32 NumberOfFunctions; //количество функций экспортируемых нашим модулем, является числом элементов массива Address Table см.ниже
            public UInt32 NumberOfNames; //число указателей на имена, обычно равно числу функций, но это не так, если у нас есть функции экспортируемые только по номеру 
            public UInt32 AddressOfFunctions;     // RVA from base of image //указатель на таблицу адресов (RVA) экспорта 
            public UInt32 AddressOfNames;         // RVA from base of image //указатель на таблицу указателей на имена экспорта 
            public UInt32 AddressOfNameOrdinals;  // RVA from base of image //указатель на таблицу ординалов экспорта, данный массив по индексам параллелен Name Pointers, элементами являются слова 
        }
        
        /// <summary>
        /// Структура директории Импорта
        /// </summary>
        public struct IMAGE_IMPORT_DESCRIPTOR  //Import Directory Table //Каталог импорта
        {
            public UInt32 OriginalFirstThunk;  //union  DWORD   Characteristics; DWORD   OriginalFirstThunk;
            //Ранее это поле называлось Characteristics; в целях сохранения 
            //совместимости кода это название поддерживается и сейчас, но не
            //отражает содержание поля. Далее мы на нем остановимся подробнее
            //Содержит ссылку на табличку RVA указывающих на соответствующие Hint-Name's или непосредственно ординал ипортируемого входа

            // 0 for terminating null import descriptor
            // RVA to original unbound IAT (PIMAGE_THUNK_DATA)
            public UInt32 TimeDateStamp;                  // 0 if not bound,
            // -1 if bound, and real date\time stamp
            //     in IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT (new BIND)
            // O.W. date/time stamp of DLL bound to (Old BIND)
            //0, если импортирование осуществляется без привязки (binding - см. далее)
            //При импортировании с привязкой содержит отметку времени файла, из которого
            // импортируем, но:
            //Если -1, то здесь использовался новый стиль привязки
            //Отметка о времени создания, часто содержит 0 (У-уф) 

            public UInt32 ForwarderChain;                 // -1 if no forwarders //? связано с возможностью передачи экспорта в другие библиотеки. Обычно равно 0FFFFFFFFh 
            public UInt32 Name;         //Виртуальный адрес ASCIIZ-строки с именем файла, из которого импортируем //Ссылка на библиотеку с которой нам необходимо поиметь вызовы представлена в виде ASCIZ. 
            public UInt32 FirstThunk;                     // RVA to IAT (if bound this IAT has actual addresses) //Ссылка на табличку адресов импорта, заполняется системой при связывании 
            //Виртуальный адрес подтаблицы импортируемых символов

        }

        /// <summary>
        /// Сигатура PE
        /// </summary>
        public const UInt32 IMAGE_NT_SIGNATURE = 0x00004550; // PE00
        /// <summary>
        /// Сигнатура DOS
        /// </summary>
        public const UInt16 IMAGE_DOS_SIGNATURE = 0x5A4D; // MZ
        /// <summary>
        /// Количество директорий
        /// </summary>
        public const int IMAGE_NUMBEROF_DIRECTORY_ENTRIES = 16;
        /// <summary>
        /// Размер короткого имени в секция PE
        /// </summary>
        public const int IMAGE_SIZEOF_SHORT_NAME = 8;
        /// <summary>
        /// Флаг наличия ORDINAL в таблице импорта для 64 разрядных файлов
        /// </summary>
        public const UInt64 IMAGE_ORDINAL_FLAG64 = 0x8000000000000000;
        /// <summary>
        /// Флаг наличия ORDINAL в таблице импорта для 32 разрядных файлов
        /// </summary>
        public const UInt32 IMAGE_ORDINAL_FLAG32 = 0x80000000;

        /// <summary>
        /// тело файла PE
        /// </summary>
        byte[] body = new byte[0];
        /// <summary>
        /// Заголовок DOS
        /// </summary>
        IMAGE_DOS_HEADER dos = new IMAGE_DOS_HEADER();
        /// <summary>
        /// заголовок NT для 32 разрядных файлов 
        /// </summary>
        IMAGE_NT_HEADERS32 nt = new IMAGE_NT_HEADERS32();
        /// <summary>
        /// заголовок NT для 64 разрядных файлов 
        /// </summary>
        IMAGE_NT_HEADERS64 nt64 = new IMAGE_NT_HEADERS64();
        /// <summary>
        /// Заголовок файла PE
        /// </summary>
        IMAGE_FILE_HEADER fileheader = new IMAGE_FILE_HEADER();
        /// <summary>
        /// Массив заголовков секций
        /// </summary>
        IMAGE_SECTION_HEADER[] sections;

        /// <summary>
        /// Экспортная функция разбора и выдачи секции экспорта
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public List<string[]> exports(string filename)
        {
            List<string[]> log = new List<string[]>();
            body = File.ReadAllBytes(filename);
            if (SizeOf(dos) > body.Length)
            {
                return log;
            }
            dos = ReadStruct<IMAGE_DOS_HEADER>(body);
            if (dos.e_magic != IMAGE_DOS_SIGNATURE)
            {
                return log;
            }

            //Начало заголовка самого PE-файла (IMAGE_NT_HEADERS) должно быть
            //выровнено на величину двойного слова (DWORD)
            //убедимся, что это так
            if ((dos.elfanew % 4) != 0)
            {
                //а иначе наш PE-файл некорректен
                return log;
            }
            if (dos.elfanew + SizeOf(nt) > body.Length)
            {
                return log;
            }
            nt = ReadStruct<IMAGE_NT_HEADERS32>(body, dos.elfanew);
            if (nt.Signature != IMAGE_NT_SIGNATURE)
            {
                return log;
            }
            sections = new IMAGE_SECTION_HEADER[(int)(nt.FileHeader.NumberOfSections)];
            for (UInt32 i = 0; i < nt.FileHeader.NumberOfSections; i++)
            {
                int first_section = dos.elfanew + nt.FileHeader.SizeOfOptionalHeader + SizeOf(fileheader) + 4;
                int delta = SizeOf(new IMAGE_SECTION_HEADER());
                sections[i] = ReadStruct<IMAGE_SECTION_HEADER>(body, first_section + (int)i * delta);
            }

            //Проверяем, что это PE32
            if (IsImage32pe())
            {
                log = ReadExport32(log);
                return log;
            }
            else
            {
                log = ReadExport64(log);
                return log;
            }

        }
        /// <summary>
        /// Функция разбора и выдачи секции экспорта для 32 разрядных файлов
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        List<string[]> ReadExport32(List<string[]> log)
        {
            if (has_exports())
            {
                UInt32 xxx = rvaToOffset(nt.OptionalHeader.DataDirectory[(int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);

                IMAGE_EXPORT_DIRECTORY export = ReadStruct<IMAGE_EXPORT_DIRECTORY>(body, (int)rvaToOffset(nt.OptionalHeader.DataDirectory[(int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress));

                if (export.NumberOfFunctions == 0)
                {
                    return log;
                }

                UInt32 address_table = rvaToOffset(export.AddressOfFunctions);
                UInt32 name_table = rvaToOffset(export.AddressOfNames);
                UInt32 ordinals_table = rvaToOffset(export.AddressOfNameOrdinals);


                for (int i = 0; i < export.NumberOfFunctions; i++)
                {
                    UInt16 hint = 0;
                    string name = "";
                    UInt32 addr = 0;
                    addr = BitConverter.ToUInt32(body, (int)address_table);
                    UInt32 names = 0;
                    if (name_table != 0 && export.NumberOfNames > i)
                    {
                        names = BitConverter.ToUInt32(body, (int)name_table);
                        if (names < body.Length)
                            name = UnDecorate(ByteArrayToString(body, (int)rvaToOffset(names)));
                        name_table += 4;
                    }
                    if (ordinals_table != 0)
                    {
                        hint = BitConverter.ToUInt16(body, (int)(ordinals_table));
                        ordinals_table += 2;
                    }
                    else
                        hint = (ushort)i;

                    log.Add(new string[] { ((ushort)(hint + export.Base)).ToString(), name });

                    address_table += 4;
                }


            }
            return log;
        }
        /// <summary>
        /// Функция разбора и выдачи секции экспорта для 64 разрядных файлов
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        List<string[]> ReadExport64(List<string[]> log)
        {
            nt64 = ReadStruct<IMAGE_NT_HEADERS64>(body, dos.elfanew);

            if (has_exports64())
            {

                UInt32 xxx = rvaToOffset(nt64.OptionalHeader.DataDirectory[(int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);

                IMAGE_EXPORT_DIRECTORY export = ReadStruct<IMAGE_EXPORT_DIRECTORY>(body, (int)rvaToOffset(nt64.OptionalHeader.DataDirectory[(int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress));
                if (export.NumberOfFunctions == 0)
                {
                    return log;
                }

                UInt32 address_table = rvaToOffset(export.AddressOfFunctions);
                UInt32 name_table = rvaToOffset(export.AddressOfNames);
                UInt32 ordinals_table = rvaToOffset(export.AddressOfNameOrdinals);


                for (int i = 0; i < export.NumberOfFunctions; i++)
                {
                    ushort hint = 0;
                    string name = "";
                    UInt32 addr = 0;
                    addr = BitConverter.ToUInt32(body, (int)address_table);
                    UInt32 names = 0;
                    if (name_table != 0 && export.NumberOfNames > i)
                    {
                        names = BitConverter.ToUInt32(body, (int)name_table);
                        if (names < body.Length)
                            name = UnDecorate(ByteArrayToString(body, (int)rvaToOffset(names)));
                        name_table += 4;
                    }
                    if (ordinals_table != 0)
                    {
                        hint = BitConverter.ToUInt16(body, (int)(ordinals_table));
                        ordinals_table += 2;
                    }
                    else
                        hint = (ushort)i;

                    log.Add(new string[] { ((ushort)(hint + export.Base)).ToString(), name });

                    address_table += 4;

                }

            }
            return log;
        }

        /// <summary>
        /// Экспортная функция разбора и выдачи секции Импорта
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public List<string[]> imports(string filename)
        {
            List<string[]> log = new List<string[]>();

            body = File.ReadAllBytes(filename);

            if (SizeOf(dos) > body.Length)
            {
                return log;
            }
            dos = ReadStruct<IMAGE_DOS_HEADER>(body);
            if (dos.e_magic != IMAGE_DOS_SIGNATURE)
            {
                return log;
            }

            //Начало заголовка самого PE-файла (IMAGE_NT_HEADERS) должно быть
            //выровнено на величину двойного слова (DWORD)
            //убедимся, что это так
            if ((dos.elfanew % 4) != 0)
            {
                //а иначе наш PE-файл некорректен
                return log;
            }
            if (dos.elfanew + SizeOf(nt) > body.Length)
            {
                return log;
            }
            nt = ReadStruct<IMAGE_NT_HEADERS32>(body, dos.elfanew);
            if (nt.Signature != IMAGE_NT_SIGNATURE)
            {
                return log;
            }
            sections = new IMAGE_SECTION_HEADER[(int)(nt.FileHeader.NumberOfSections)];
            for (UInt32 i = 0; i < nt.FileHeader.NumberOfSections; i++)
            {
                int first_section = dos.elfanew + nt.FileHeader.SizeOfOptionalHeader + SizeOf(fileheader) + 4;
                int delta = SizeOf(new IMAGE_SECTION_HEADER());
                sections[i] = ReadStruct<IMAGE_SECTION_HEADER>(body, first_section + (int)i * delta);
            }
            //Проверяем, что это PE32
            if (IsImage32pe())
            {
                log = ReadImports32(log);
                return log;
            }
            else
            {
                log = ReadImports64(log);
                return log;
            }
        }
        /// <summary>
        /// Функция разбора и выдачи секции Импорта для 32 разрядных файлов
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        List<string[]> ReadImports32(List<string[]> log)
        {
            if (has_imports())
            {
                int cnt = 0;
                int delta = SizeOf(new IMAGE_IMPORT_DESCRIPTOR());
                while (true)
                {
                    IMAGE_IMPORT_DESCRIPTOR import = ReadStruct<IMAGE_IMPORT_DESCRIPTOR>(body, delta * cnt + (int)rvaToOffset(nt.OptionalHeader.DataDirectory[(int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress));
                    cnt++;
                    if (import.OriginalFirstThunk == 0)
                        break;


                    UInt32 import_address_table = rvaToOffset(import.FirstThunk);
                    UInt32 import_lookup_table = (import.OriginalFirstThunk == 0) ? import_address_table : rvaToOffset(import.OriginalFirstThunk);
                    UInt32 address_table = import.FirstThunk;


                    UInt16 hint = 0;
                    string name = "";
                    UInt16 ordinal = 0;
                    if (BitConverter.ToUInt32(body, (int)import_lookup_table) != 0 && BitConverter.ToUInt32(body, (int)import_address_table) != 0)
                    {
                        while (true)
                        {
                            //Тут стоило бы добавить дополнительные проверки, т.к. указатель для кривого exe
                            //может оказаться невалидным, но этот пример демонстрационный
                            //и не стремится быть идеально правильным
                            UInt32 address = BitConverter.ToUInt32(body, (int)import_address_table);
                            import_address_table += 4;

                            //Если мы достигли конца списка импортируемых функций, то переходим
                            //к следующей библиотеке
                            if (address == 0)
                                break;

                            UInt32 lookup = BitConverter.ToUInt32(body, (int)import_lookup_table);
                            import_lookup_table += 4;
                            if (IMAGE_SNAP_BY_ORDINAL32((uint)lookup))
                            {
                                ordinal = (ushort)IMAGE_ORDINAL32(lookup);
                                hint = 0;
                                name = "";
                                log.Add(new string[] { ByteArrayToString(body, (int)rvaToOffset(import.Name)), name, ordinal.ToString() });
                            }
                            else
                            {
                                //В противном случае выведем ее имя
                                name = UnDecorate(ByteArrayToString(body, (int)rvaToOffset(lookup + 2)));
                                hint = BitConverter.ToUInt16(body, (int)rvaToOffset((uint)lookup));
                                log.Add(new string[] { ByteArrayToString(body, (int)rvaToOffset(import.Name)), name, hint.ToString() });
                            }

                            address_table += 4;

                        }
                    }

                    //Переходим к следующей библиотеке

                }
            }
            return log;
        }
        /// <summary>
        /// Функция разбора и выдачи секции Импорта для 64 разрядных файлов
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        List<string[]> ReadImports64(List<string[]> log)
        {

            nt64 = ReadStruct<IMAGE_NT_HEADERS64>(body, dos.elfanew);
            if (has_imports64())
            {
                int cnt = 0;
                int delta = SizeOf(new IMAGE_IMPORT_DESCRIPTOR());
                while (true)
                {

                    IMAGE_IMPORT_DESCRIPTOR import = ReadStruct<IMAGE_IMPORT_DESCRIPTOR>(body, delta * cnt + (int)rvaToOffset(nt64.OptionalHeader.DataDirectory[(int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress));
                    cnt++;
                    if (import.OriginalFirstThunk == 0)
                        break;

                    UInt64 import_address_table = rvaToOffset(import.FirstThunk);
                    UInt64 import_lookup_table = (import.OriginalFirstThunk == 0) ? import_address_table : rvaToOffset(import.OriginalFirstThunk);
                    UInt64 address_table = import.FirstThunk;

                    UInt16 hint = 0;
                    string name = "";
                    UInt16 ordinal = 0;
                    if (BitConverter.ToUInt64(body, (int)import_lookup_table) != 0 && BitConverter.ToUInt64(body, (int)import_address_table) != 0)
                    {
                        while (true)
                        {
                            //Тут стоило бы добавить дополнительные проверки, т.к. указатель для кривого exe
                            //может оказаться невалидным, но этот пример демонстрационный
                            //и не стремится быть идеально правильным

                            UInt64 address = BitConverter.ToUInt64(body, (int)import_address_table);
                            import_address_table += 8;

                            //Если мы достигли конца списка импортируемых функций, то переходим
                            //к следующей библиотеке
                            if (address == 0)
                                break;

                            UInt64 lookup = BitConverter.ToUInt64(body, (int)import_lookup_table);
                            import_lookup_table += 8;
                            if (IMAGE_SNAP_BY_ORDINAL64(lookup))
                            {
                                ordinal = (ushort)IMAGE_ORDINAL64(lookup);
                                hint = 0;
                                name = "";
                                log.Add(new string[] { ByteArrayToString(body, (int)rvaToOffset(import.Name)), name, ordinal.ToString() });
                            }
                            else
                            {
                                //В противном случае выведем ее имя
                                name = UnDecorate(ByteArrayToString(body, (int)rvaToOffset((uint)lookup + 2)));
                                hint = BitConverter.ToUInt16(body, (int)rvaToOffset((uint)lookup));
                                log.Add(new string[] { ByteArrayToString(body, (int)rvaToOffset(import.Name)), name, hint.ToString() });
                            }

                            address_table += 8;

                        }
                    }

                    //Переходим к следующей библиотеке

                }
            }
            return log;
        }

        /// <summary>
        /// Чтение произвольной стрктуры из байтового массива
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="b"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        T ReadStruct<T>(byte[] b, int pos)
        {
            return ReadStruct<T>(b.Skip(pos).ToArray());
        }
        /// <summary>
        /// Чтение произвольной стрктуры из байтового массива
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="b"></param>
        /// <returns></returns>
        T ReadStruct<T>(byte[] b)
        {
            byte[] buffer = b.Take(Marshal.SizeOf(typeof(T))).ToArray();
            GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            T temp = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            handle.Free();
            return temp;
        }

        /// <summary>
        /// Размер обекта в байтах
        /// </summary>
        /// <param name="ob"></param>
        /// <returns></returns>
        int SizeOf(object ob)
        {
            return Marshal.SizeOf(ob);
        }

        /// <summary>
        /// Копирование в String строки, заканчивающейся нулем
        /// </summary>
        /// <param name="x"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        string ByteArrayToString(byte[] x, int pos)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = pos; i < x.Length && x[i] != 0; i++)
            {
                sb.Append((char)x[i]);
            }
            return sb.ToString();
        }
        /// <summary>
        /// Проверка того, что файл является 32 разрядным
        /// </summary>
        /// <returns></returns>
        bool IsImage32pe()
        {
            if (nt.FileHeader.SizeOfOptionalHeader == 224)
                return true;
            return false;
            // Для 64 длина =240
            // if (nt.FileHeader.Machine == FHMachine.IMAGE_FILE_MACHINE_AMD64 || nt.FileHeader.Machine == FHMachine.IMAGE_FILE_MACHINE_IA64)
            //      return false;
            // return true;
        }

        /// <summary>
        /// Проверка того, что директорный вход существует для 32 разрядного файла
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool directory_exists(int id)
        {
            return (((nt.OptionalHeader.NumberOfRvaAndSizes - 1) >= id) && (nt.OptionalHeader.DataDirectory[id].VirtualAddress != 0));
        }
        /// <summary>
        /// Проверка того, что директорный вход существует для 64 разрядного файла
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool directory_exists64(int id)
        {
            return (((nt64.OptionalHeader.NumberOfRvaAndSizes - 1) >= id) && (nt64.OptionalHeader.DataDirectory[id].VirtualAddress != 0));
        }
        /// <summary>
        /// Проверка того, что есть секция импорта для 32 разрядных файлов
        /// </summary>
        /// <returns></returns>
        bool has_imports()
        {
            return directory_exists((int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_IMPORT);
        }
        /// <summary>
        /// Проверка того, что есть секция импорта для 64 разрядных файлов
        /// </summary>
        /// <returns></returns>
        bool has_imports64()
        {
            return directory_exists64((int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_IMPORT);
        }

        /// <summary>
        /// Проверка того, что есть секция экспорта для 32 разрядных файлов
        /// </summary>
        /// <returns></returns>
        bool has_exports()
        {
            return directory_exists((int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_EXPORT);
        }
        /// <summary>
        /// Проверка того, что есть секция экспорта для 64 разрядных файлов
        /// </summary>
        /// <returns></returns>
        bool has_exports64()
        {
            return directory_exists64((int)DirectoryEntries.IMAGE_DIRECTORY_ENTRY_EXPORT);
        }

        /// <summary>
        /// Получить номер секции по относительному виртуальному адресу
        /// </summary>
        /// <param name="rva"></param>
        /// <returns></returns>
        UInt32 GetSectionWithRva(UInt32 rva)
        {
            for (UInt32 i = 0; i < sections.Length; i++)
            {
                UInt32 max = sections[i].VirtualSize >= sections[i].SizeOfRawData ? sections[i].VirtualSize : sections[i].SizeOfRawData;
                if (sections[i].VirtualAddress <= rva && sections[i].VirtualAddress + max > rva)
                    return i;
            }

            return 0xffff;
        }
        /// <summary>
        /// Получить смещение в файле по относительному виртуальному адресу
        /// </summary>
        /// <param name="rva"></param>
        /// <returns></returns>
        UInt32 rvaToOffset(UInt32 rva)
        {
            // XXX: Not correct
            if (rva < 0x1000)
                return rva;

            uint uiSecnr = GetSectionWithRva(rva);

            if (uiSecnr == 0xFFFF || rva > sections[uiSecnr].VirtualAddress + sections[uiSecnr].SizeOfRawData)
            {
                return 0xffff;
            }

            return sections[uiSecnr].PointerToRawData + rva - sections[uiSecnr].VirtualAddress;
        }
        /// <summary>
        /// Подключение функции Восстановления декорированного имени из внешней библиотеки 
        /// </summary>
        /// <param name="DecoratedName"></param>
        /// <param name="UnDecoratedName"></param>
        /// <param name="UndecoratedLength"></param>
        /// <param name="Flags"></param>
        /// <returns></returns>
        [DllImport("dbghelp.dll", SetLastError = true, PreserveSig = true)]
        static extern int UnDecorateSymbolName([In] [MarshalAs(UnmanagedType.LPStr)] string DecoratedName, [Out] StringBuilder UnDecoratedName, [In] [MarshalAs(UnmanagedType.U4)] int UndecoratedLength, [In] [MarshalAs(UnmanagedType.U4)] UnDecorateFlags Flags);
        [Flags]
        enum UnDecorateFlags
        {
            UNDNAME_COMPLETE = (0x0000),  // Enable full undecoration
            UNDNAME_NO_LEADING_UNDERSCORES = (0x0001),  // Remove leading underscores from MS extended keywords
            UNDNAME_NO_MS_KEYWORDS = (0x0002),  // Disable expansion of MS extended keywords
            UNDNAME_NO_FUNCTION_RETURNS = (0x0004),  // Disable expansion of return type for primary declaration
            UNDNAME_NO_ALLOCATION_MODEL = (0x0008),  // Disable expansion of the declaration model
            UNDNAME_NO_ALLOCATION_LANGUAGE = (0x0010),  // Disable expansion of the declaration language specifier
            UNDNAME_NO_MS_THISTYPE = (0x0020),  // NYI Disable expansion of MS keywords on the 'this' type for primary declaration
            UNDNAME_NO_CV_THISTYPE = (0x0040),  // NYI Disable expansion of CV modifiers on the 'this' type for primary declaration
            UNDNAME_NO_THISTYPE = (0x0060),  // Disable all modifiers on the 'this' type
            UNDNAME_NO_ACCESS_SPECIFIERS = (0x0080),  // Disable expansion of access specifiers for members
            UNDNAME_NO_THROW_SIGNATURES = (0x0100),  // Disable expansion of 'throw-signatures' for functions and pointers to functions
            UNDNAME_NO_MEMBER_TYPE = (0x0200),  // Disable expansion of 'static' or 'virtual'ness of members
            UNDNAME_NO_RETURN_UDT_MODEL = (0x0400),  // Disable expansion of MS model for UDT returns
            UNDNAME_32_BIT_DECODE = (0x0800),  // Undecorate 32-bit decorated names
            UNDNAME_NAME_ONLY = (0x1000),  // Crack only the name for primary declaration;
            // return just [scope::]name.  Does expand template params
            UNDNAME_NO_ARGUMENTS = (0x2000),  // Don't undecorate arguments to function
            UNDNAME_NO_SPECIAL_SYMS = (0x4000),  // Don't undecorate special names (v-table, vcall, vector xxx, metatype, etc)
        }
        /// <summary>
        /// Восстановление декорированного имени
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        internal string UnDecorate(string s)
        {
            if (s.Length > 0 && s[0] == '?')
            {
                StringBuilder builder = new StringBuilder(8000);
                UnDecorateSymbolName(s, builder, builder.Capacity, UnDecorateFlags.UNDNAME_COMPLETE);
                return builder.ToString();
            }
            return s;
        }

        /// <summary>
        /// Проверка того, что процедура зарегестрирована по номеру для 32 разрядных файлов
        /// </summary>
        /// <param name="Ordinal"></param>
        /// <returns></returns>
        bool IMAGE_SNAP_BY_ORDINAL32(uint Ordinal)
        {
            return ((Ordinal & IMAGE_ORDINAL_FLAG32) != 0);
        }
        /// <summary>
        /// Проверка того, что процедура зарегестрирована по номеру для 64 разрядных файлов
        /// </summary>
        /// <param name="Ordinal"></param>
        /// <returns></returns>
        bool IMAGE_SNAP_BY_ORDINAL64(ulong Ordinal)
        {
            return ((Ordinal & IMAGE_ORDINAL_FLAG64) != 0);
        }
        /// <summary>
        /// Получение номера функции для 32 разрядных файлов
        /// </summary>
        /// <param name="Ordinal"></param>
        /// <returns></returns>
        UInt64 IMAGE_ORDINAL64(UInt64 Ordinal)
        {
            return (Ordinal & 0xffff);
        }
        /// <summary>
        /// Получение номера функции для 64 разрядных файлов
        /// </summary>
        /// <param name="Ordinal"></param>
        /// <returns></returns>
        UInt32 IMAGE_ORDINAL32(UInt32 Ordinal)
        {
            return (Ordinal & 0xffff);
        }


    }

}
