﻿using System.Windows.Forms;

namespace IA.AistSubsystem
{
    /// <summary>
    /// Контрол для настройки парсера отчётов АИСТ-С.
    /// </summary>
    public partial class AistReportReaderSettingsControl : UserControl
    {
        /// <summary>
        /// Конструктор по-умолчанию
        /// </summary>
        public AistReportReaderSettingsControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Текст поля с путём до каталога отчётов АИСТ-С.
        /// </summary>
        public string PathToAistReportDirectory
        {
            get { return txtPathToAistReport.Text; }
            set { txtPathToAistReport.Text = value; }
        }

        /// <summary>
        /// Текст поля с путём до каталога с исходными текстами во время работы АИСТ-С.
        /// </summary>
        public string PathToOldSourcesDirectory
        {
            get { return txtPathToOldSources.Text; }
            set { txtPathToOldSources.Text = value; }
        }
    }
}
