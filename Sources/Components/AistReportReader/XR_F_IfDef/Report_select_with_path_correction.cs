﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace IA.AistSubsystem.XR_F_IfDef
{
    /// <summary>
    /// Контрол для составления списка протоколов утилиты XR_F_IfDef из состава АИСТ-С.
    /// </summary>
    public partial class Report_select_with_path_correction : UserControl
    {
        private OpenFileDialog dlg;
        /// <summary>
        /// Список протоколов.
        /// </summary>
        public BindingList<XR_F_IfDefProtocol> protocolList { get; private set; }

        /// <summary>
        /// Простой конструктор.
        /// </summary>
        public Report_select_with_path_correction()
        {
            InitializeComponent();

            Init();
        }

        void Init()
        {
            dlg = new OpenFileDialog()
            {
                Filter = "XR_F_IfDef reports (*1_Din_.Rep)|*1_Din_.Rep|All files (*.*)|*.*",
                FilterIndex = 1,
                CheckFileExists = true,
                Multiselect = true
            };

            protocolList = new BindingList<XR_F_IfDefProtocol>();

            protocols_listbox.DataSource = protocolList;

            pathExisted_textbox.TextBox.TextChanged += txtPathExisted_Leave;
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (dlg.ShowDialog() != DialogResult.OK)
                return;
            Dictionary<string, string> troubles = new Dictionary<string, string>();
            foreach (var filepath in dlg.FileNames)
            {
                try
                {
                    XR_F_IfDefProtocol currentProtocol = new XR_F_IfDefProtocol(filepath);
                    if (currentProtocol.IsProtocolFormatAccepted)
                        protocolList.Add(currentProtocol);
                    else
                        troubles.Add(filepath, "В протоколе не найдены указания на файлы исходных текстов.");
                }
                catch (Exception ex)
                {
                    troubles.Add(filepath, ex.Message);
                }
            }

            if (troubles.Count != 0)
            {
                string resultmessage = String.Empty;
                resultmessage = String.Join(Environment.NewLine, troubles.Select(kvp => String.Format("Проблема в файле <{0}>: {1}", kvp.Key, kvp.Value)));
                MessageBox.Show("При обработке файлов возникли нижеуказанные ошибки. Упомянутые файлы не были добавлены в список." + Environment.NewLine + resultmessage,
                    "При обработке возникли ошибки", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            protocols_listbox.SelectedIndex = protocols_listbox.Items.Count - 1;
            //lstProtocols_SelectedIndexChanged(null, null);
        }

        private void lstProtocols_SelectedIndexChanged(object sender, EventArgs e)
        {
            badFilesInProtocol_listbox.Items.Clear();

            if (protocols_listbox.SelectedIndex == -1)
            {
                pathExisted_textbox.Text = string.Empty;
                pathCalculated_textBox.Text = string.Empty;
                return;
            }

            XR_F_IfDefProtocol selectedProtocol = (XR_F_IfDefProtocol)protocols_listbox.SelectedItem;
            pathExisted_textbox.Text = selectedProtocol.CorrectedFilesInReportPrefix;
            pathCalculated_textBox.Text = selectedProtocol.OriginalFilesInReportPrefix;

            badFilesInProtocol_listbox.Items.AddRange(selectedProtocol.FilesInReportWithCorrectedPaths
                .Where(f => !IOController.FileController.IsExists(f))
                .Take(5)
                .ToArray());
        }


        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (protocols_listbox.SelectedIndex == -1)
                return;
            XR_F_IfDefProtocol selectedProtocol = (XR_F_IfDefProtocol)protocols_listbox.SelectedItem;
            protocolList.Remove(selectedProtocol);

            protocols_listbox.SelectedIndex = protocols_listbox.Items.Count - 1;
        }

        private void txtPathExisted_Leave(object sender, EventArgs e)
        {
            if (protocols_listbox.SelectedIndex == -1)
                return;
            XR_F_IfDefProtocol selectedProtocol = (XR_F_IfDefProtocol)protocols_listbox.SelectedItem;
            selectedProtocol.CorrectedFilesInReportPrefix = pathExisted_textbox.Text;
            lstProtocols_SelectedIndexChanged(null, null);
        }

    }
}
