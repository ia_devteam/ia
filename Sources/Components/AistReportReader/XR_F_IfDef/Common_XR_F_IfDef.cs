﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace IA.AistSubsystem.XR_F_IfDef
{
    /// <summary>
    /// Класс позволяет хранить протокол вставки датчиков утилитой АИСТ-С XR_F_IfDef и опрашивать его на предмет встречаемых файлов.
    /// </summary>
    /// <remarks>
    /// Вносить внутрь функционал полного разбора/сборки протокола нет необходимости: 
    /// слишком много особенностей надо учитывать, проще по месту вытаскивать то, что нужно.
    /// </remarks>
    public class XR_F_IfDefProtocol
    {
        /// <summary>
        /// Полный путь до файла протокола.
        /// </summary>
        public string ReportPath { get; private set; }

        /// <summary>
        /// Общая часть пути всех файлов исходных текстов, в которые были вставлены датчики (до самой вставки датчиков).
        /// </summary>
        public string OriginalFilesInReportPrefix { get; private set; }

        private string correctedFilesInReportPrefix;
        /// <summary>
        /// Путь в протоколе до обработанных файлов. Может не совпадать с путём до файлов, включённых в анализатор.
        /// </summary>
        public string CorrectedFilesInReportPrefix
        {
            get
            { return correctedFilesInReportPrefix; }
            set
            {
                if (value == correctedFilesInReportPrefix)
                    return;
                correctedFilesInReportPrefix = value;
                if (String.IsNullOrWhiteSpace(OriginalFilesInReportPrefix) && String.IsNullOrWhiteSpace(CorrectedFilesInReportPrefix))
                {
                    filesInReportWithCorrectedPaths = FilesInReport;
                    return;
                }

                if (String.IsNullOrWhiteSpace(OriginalFilesInReportPrefix))
                {
                    filesInReportWithCorrectedPaths = FilesInReport.Select(l => l.Insert(0, CorrectedFilesInReportPrefix));
                    return;
                }

                if (String.IsNullOrWhiteSpace(CorrectedFilesInReportPrefix))
                {
                    filesInReportWithCorrectedPaths = FilesInReport.Select(l => l.Remove(0, OriginalFilesInReportPrefix.Length));
                    return;
                }

                filesInReportWithCorrectedPaths = FilesInReport.Select(l => l.ReplaceAtStartIgnoreCase(OriginalFilesInReportPrefix, CorrectedFilesInReportPrefix));
            }
        }

        private List<string> _FilesInReport;
        /// <summary>
        /// Перечень полных путей файлов, обнаруженных в отчёте.
        /// </summary>
        public List<string> FilesInReport
        {
            get
            {
                if (_FilesInReport != null && _FilesInReport.Count == 0)
                {
                    ParseProjectForFiles(ReportPath);

                }
                return _FilesInReport;
            }
            private set
            {
                _FilesInReport = value;
            }
        }

        /// <summary>
        /// Выкусить из файла проекта полные пути обработанных файлов. Если формат нарушается - список очищается для сигнализации проблемы.
        /// </summary>
        /// <param name="reportPath">Полный путь к файлу протокола.</param>
        private void ParseProjectForFiles(string reportPath)
        {
            if (!String.IsNullOrWhiteSpace(reportPath) && IOController.FileController.IsExists(reportPath))
            {
                foreach (string line in File.ReadLines(reportPath, Encoding.GetEncoding(1251)))
                {
                    if (String.IsNullOrWhiteSpace(line) || line[0] == ' ' || line[0] == '=')
                        continue;
                    if (FilePathHasInvalidChars(line))
                    {
                        IA.Monitor.Log.Error(AistReportsReader.ObjectName,
                            String.Format("Файл протокола <{0}> содержит строку неверного формата: <{1}>.", reportPath, line));
                        _FilesInReport.Clear();
                        break;
                    }
                    _FilesInReport.Add(line);
                }

                CorrectedFilesInReportPrefix = OriginalFilesInReportPrefix = IOController.PathController.GetCommonDirectoryPrefix(_FilesInReport, true);

                IsProtocolFormatAccepted = _FilesInReport.Count != 0;
            }
            else
            {
                IA.Monitor.Log.Warning(AistReportsReader.ObjectName, "Файл протокола <" + reportPath + "> не найден!");
                CorrectedFilesInReportPrefix = OriginalFilesInReportPrefix = String.Empty;
            }
        }

        private IEnumerable<string> filesInReportWithCorrectedPaths;
        /// <summary>
        /// Перечень полных путей файлов, у которых был заменён <c>OriginalFilesInReportPrefix</c> на <c>CorrectedFilesInReportPrefix</c>.
        /// </summary>
        public IEnumerable<string> FilesInReportWithCorrectedPaths
        {
            get
            {
                return filesInReportWithCorrectedPaths;
            }
        }

        /// <summary>
        /// Протокол может быть обработан формата.
        /// </summary>
        public bool IsProtocolFormatAccepted
        {
            get;
            private set;
        }

        /// <summary>
        /// Объект протокола XR_F_IfDef.
        /// </summary>
        /// <param name="filepath">Полный путь до файла протокола.</param>
        public XR_F_IfDefProtocol(string filepath)
        {
            FilesInReport = new List<string>();

            ParseProjectForFiles(filepath);
            
            ReportPath = filepath;
        }

        /// <summary>
        /// Путь к файлу протокола. Аналогично <c>SelfPath</c>.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ReportPath;
        }

        private static bool FilePathHasInvalidChars(string path)
        { // http://stackoverflow.com/questions/2435894/net-how-do-i-check-for-illegal-characters-in-a-path
            if (!string.IsNullOrEmpty(path))
            {
                try
                {
                    var spaces = 0;
                    while (path[spaces] == ' ')
                        spaces++;
                    var dotdot = path.IndexOf(':');
                    var slash = path.IndexOf('\\', dotdot < 0 ? 0 : dotdot);
                    if (dotdot == -1 || dotdot - spaces != 1 || dotdot + 1 != slash)
                        return true;
                    // Careful!
                    //    Path.GetDirectoryName("C:\Directory\SubDirectory")
                    //    returns "C:\Directory", which may not be what you want in
                    //    this case. You may need to explicitly add a trailing \
                    //    if path is a directory and not a file path. As written, 
                    //    this function just assumes path is a file path.

                    string fileName = System.IO.Path.GetFileName(path);

                    // we don't need to do anything else,
                    // if we got here without throwing an 
                    // exception, then the path does not
                    // contain invalid characters
                }
                catch (ArgumentException)
                {
                    // Path functions will throw this 
                    // if path contains invalid chars
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// Класс-расширение для строк.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Замена начала строки на другую строку вне зависимости от регистра.
        /// </summary>
        /// <param name="input">Входная строка.</param>
        /// <param name="oldPart">Часть, которую заменить.</param>
        /// <param name="newPart">Строка на которую менять.</param>
        /// <returns></returns>
        public static string ReplaceAtStartIgnoreCase(this string input, string oldPart, string newPart)
        {
            if (input.IndexOf(oldPart, StringComparison.CurrentCultureIgnoreCase) == 0)
                return input.Remove(0, oldPart.Length).Insert(0, newPart);
            return input;
        }
    }
}
