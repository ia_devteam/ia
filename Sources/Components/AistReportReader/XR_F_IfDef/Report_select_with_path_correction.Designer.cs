﻿namespace IA.AistSubsystem.XR_F_IfDef
{
    partial class Report_select_with_path_correction
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.protocolList_groupbox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.protocols_listbox = new System.Windows.Forms.ListBox();
            this.pathRealToFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.pathExisted_textbox = new IA.Controls.TextBoxWithDialogButton();
            this.badFileList_groupbox = new System.Windows.Forms.GroupBox();
            this.badFilesInProtocol_listbox = new System.Windows.Forms.ListBox();
            this.pathProtocolToFiles_groupBox = new System.Windows.Forms.GroupBox();
            this.pathCalculated_textBox = new System.Windows.Forms.TextBox();
            this.protocolList_groupbox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.pathRealToFiles_groupBox.SuspendLayout();
            this.badFileList_groupbox.SuspendLayout();
            this.pathProtocolToFiles_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // protocolList_groupbox
            // 
            this.protocolList_groupbox.Controls.Add(this.tableLayoutPanel1);
            this.protocolList_groupbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.protocolList_groupbox.Location = new System.Drawing.Point(0, 0);
            this.protocolList_groupbox.Name = "protocolList_groupbox";
            this.protocolList_groupbox.Size = new System.Drawing.Size(405, 360);
            this.protocolList_groupbox.TabIndex = 0;
            this.protocolList_groupbox.TabStop = false;
            this.protocolList_groupbox.Text = "Формирование списка обрабатываемых отчётов XR_F_IfDef";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.protocols_listbox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pathRealToFiles_groupBox, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.badFileList_groupbox, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.pathProtocolToFiles_groupBox, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(399, 341);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btnAdd, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnRemove, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 103);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(393, 34);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAdd.Location = new System.Drawing.Point(3, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(140, 28);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Добавить протокол";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnRemove.Location = new System.Drawing.Point(250, 3);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(140, 28);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "Удалить протокол";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // protocols_listbox
            // 
            this.protocols_listbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.protocols_listbox.FormattingEnabled = true;
            this.protocols_listbox.Location = new System.Drawing.Point(3, 3);
            this.protocols_listbox.Name = "protocols_listbox";
            this.protocols_listbox.Size = new System.Drawing.Size(393, 94);
            this.protocols_listbox.TabIndex = 1;
            this.protocols_listbox.SelectedIndexChanged += new System.EventHandler(this.lstProtocols_SelectedIndexChanged);
            // 
            // pathRealToFiles_groupBox
            // 
            this.pathRealToFiles_groupBox.Controls.Add(this.pathExisted_textbox);
            this.pathRealToFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pathRealToFiles_groupBox.Location = new System.Drawing.Point(3, 193);
            this.pathRealToFiles_groupBox.Name = "pathRealToFiles_groupBox";
            this.pathRealToFiles_groupBox.Size = new System.Drawing.Size(393, 44);
            this.pathRealToFiles_groupBox.TabIndex = 2;
            this.pathRealToFiles_groupBox.TabStop = false;
            this.pathRealToFiles_groupBox.Text = "Путь до корня обработанных файлов";
            // 
            // pathExisted_textbox
            // 
            this.pathExisted_textbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pathExisted_textbox.Location = new System.Drawing.Point(3, 16);
            this.pathExisted_textbox.MaximumSize = new System.Drawing.Size(0, 24);
            this.pathExisted_textbox.MinimumSize = new System.Drawing.Size(150, 24);
            this.pathExisted_textbox.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.pathExisted_textbox.Name = "pathExisted_textbox";
            this.pathExisted_textbox.Size = new System.Drawing.Size(387, 24);
            this.pathExisted_textbox.TabIndex = 0;
            this.pathExisted_textbox.Leave += new System.EventHandler(this.txtPathExisted_Leave);
            // 
            // badFileList_groupbox
            // 
            this.badFileList_groupbox.Controls.Add(this.badFilesInProtocol_listbox);
            this.badFileList_groupbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.badFileList_groupbox.Location = new System.Drawing.Point(3, 243);
            this.badFileList_groupbox.Name = "badFileList_groupbox";
            this.badFileList_groupbox.Size = new System.Drawing.Size(393, 94);
            this.badFileList_groupbox.TabIndex = 3;
            this.badFileList_groupbox.TabStop = false;
            this.badFileList_groupbox.Text = "Файлы с исправленными путями, не найденные на диске";
            // 
            // badFilesInProtocol_listbox
            // 
            this.badFilesInProtocol_listbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.badFilesInProtocol_listbox.FormattingEnabled = true;
            this.badFilesInProtocol_listbox.Location = new System.Drawing.Point(3, 16);
            this.badFilesInProtocol_listbox.Name = "badFilesInProtocol_listbox";
            this.badFilesInProtocol_listbox.Size = new System.Drawing.Size(387, 75);
            this.badFilesInProtocol_listbox.TabIndex = 0;
            // 
            // pathProtocolToFiles_groupBox
            // 
            this.pathProtocolToFiles_groupBox.Controls.Add(this.pathCalculated_textBox);
            this.pathProtocolToFiles_groupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pathProtocolToFiles_groupBox.Location = new System.Drawing.Point(3, 143);
            this.pathProtocolToFiles_groupBox.Name = "pathProtocolToFiles_groupBox";
            this.pathProtocolToFiles_groupBox.Size = new System.Drawing.Size(393, 44);
            this.pathProtocolToFiles_groupBox.TabIndex = 4;
            this.pathProtocolToFiles_groupBox.TabStop = false;
            this.pathProtocolToFiles_groupBox.Text = "Вычисленная общая часть пути файлов в протоколе";
            // 
            // pathCalculated_textBox
            // 
            this.pathCalculated_textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pathCalculated_textBox.Location = new System.Drawing.Point(3, 16);
            this.pathCalculated_textBox.Name = "pathCalculated_textBox";
            this.pathCalculated_textBox.ReadOnly = true;
            this.pathCalculated_textBox.Size = new System.Drawing.Size(387, 20);
            this.pathCalculated_textBox.TabIndex = 0;
            // 
            // Report_select_with_path_correction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.protocolList_groupbox);
            this.MinimumSize = new System.Drawing.Size(405, 360);
            this.Name = "Report_select_with_path_correction";
            this.Size = new System.Drawing.Size(405, 360);
            this.protocolList_groupbox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.pathRealToFiles_groupBox.ResumeLayout(false);
            this.badFileList_groupbox.ResumeLayout(false);
            this.pathProtocolToFiles_groupBox.ResumeLayout(false);
            this.pathProtocolToFiles_groupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox protocolList_groupbox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.ListBox protocols_listbox;
        private System.Windows.Forms.GroupBox pathRealToFiles_groupBox;
        private System.Windows.Forms.GroupBox badFileList_groupbox;
        private System.Windows.Forms.ListBox badFilesInProtocol_listbox;
        private Controls.TextBoxWithDialogButton pathExisted_textbox;
        private System.Windows.Forms.GroupBox pathProtocolToFiles_groupBox;
        private System.Windows.Forms.TextBox pathCalculated_textBox;
    }
}
