﻿using System.Collections.Generic;
using System.IO;
using System.Text;


namespace IA.AistSubsystem.SingleReport
{

    /// <summary>
    /// Обработка файла _xxxxxxx.8ep.
    /// </summary>
    internal static class Reader_xxxxxxx8ep
    {
        //Обозначения:
        //Г - головная функция (main)
        //* - явно вызываемая функция
        //. - не вызываемая функция
        //- - функция вызывается в невызываемых функциях
        //# - многажды определенная функция
        //@ - метод объекта
        //x - функция не вошла из-за IfDef
        //X - Метод не вошел из-за IfDef
        //! - Имеется обращение к не вошедшей функции

        /// <summary>
        /// Вычитать файл _xxxxxxx.8ep для составления списка функций, 
        /// включая номер порядковый (используется в _Report), номер абсолютный, 
        /// имя, признак висячести, номер файла с определением.
        /// </summary>
        /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по грозди.</param>
        /// <param name="files">Словарь файлов.</param>
        public static List<AistReportEntity> ReadFuncList(string bunchDirectoryPath, Dictionary<int, string> files)
        {
            string file = Path.Combine(bunchDirectoryPath, "_xxxxxxx.8ep");
            if (!File.Exists(file))
            {
                return null;
            }

            List<AistReportEntity> funcList = null;

            using (StreamReader reader = new StreamReader(file, Encoding.Default))
            {
                string curLine = Common.SkipLines(reader, @"Имена функций:");
                if (curLine == null)
                {
                    return null;
                }
                funcList = new List<AistReportEntity>();
                reader.ReadLine(); //пустая строка
                reader.ReadLine(); //шапка таблицы

                while ((curLine = reader.ReadLine()) != null)
                {//1    1    1 * CHelloWorldWnd
                    if (string.IsNullOrWhiteSpace(curLine)) break;
                    AistReportEntity item = ParseLineForFunc(curLine);
                    if (item != null)
                    {
                        item.SourceFile = files[item.NFile];
                        funcList.Add(item);
                    }
                }
            }
            return funcList;
        }

        static readonly List<char> goodEssentialFlags = new List<char> { '*', '.', '-', 'Г' };
        private static AistReportEntity ParseLineForFunc(string line)
        {
            char essentialFlag = line[15];
            if (!goodEssentialFlags.Contains(essentialFlag))
                return null; //остальные флаги избыточности неинтересны
            int nPP = int.Parse(line.Substring(0, 4));
            int nFUN = int.Parse(line.Substring(4, 5));
            int nFILE = int.Parse(line.Substring(9, 5));
            return new AistReportEntity() { NPP = nPP, NFun = nFUN, NFile = nFILE, EssentialFlagByAist = essentialFlag };
        }
    }

}
