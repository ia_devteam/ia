﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IA.AistSubsystem.SingleReport
{

    /// <summary>
    /// Обработка файла _Report.csv.
    /// </summary>
    internal static class Reader_Reportcsv
    {
        /// <summary>
        /// Вычитать файл _Report.csv для получения номера строки определения функции, а так же её имени (пригодится для уточнения по андестенду). Лежит в каталоге грозди отчёта АИСТ-С.
        /// </summary>
        /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по грозди.</param>
        /// <param name="functions">Список функций.</param>
        public static void Read(string bunchDirectoryPath, List<AistReportEntity> functions)
        {
            string file = Path.Combine(bunchDirectoryPath, "_Report.csv");

            if (!File.Exists(file))
                return;

            using (StreamReader reader = new StreamReader(file, Encoding.Default))
            {
                string curLine = Common.SkipLines(reader, @"Nпп,Имя функции,Имя  файла,N1");
                if (curLine == null)
                    return;

                reader.ReadLine(); //пустая строка

                while (!String.IsNullOrWhiteSpace(curLine = reader.ReadLine()))
                {
                    //1,GetRunDirectory,makestub.cpp,13
                    var splitted = curLine.Split(new char[] { ',' }, 4);
                    if (splitted.Length < 2)
                        continue;
                    int nPP = int.Parse(splitted[0]);
                    string funcName = splitted[1].TrimStart(':', '-');
                    int lineNUM = -1;
                    if (splitted.Length >= 4)
                        lineNUM = int.Parse(splitted[3].Substring(0, splitted[3].IndexOf(','))); //чего только не бывает в отчётах АИСТ-С...

                    AistReportEntity currentItem = functions.Find(f => f.NPP == nPP);
                    if (currentItem != null)
                    {
                        currentItem.Name = funcName;
                        if (lineNUM != -1)
                            currentItem.AistLineNumber = lineNUM;
                    }
                }
            }
        }
    }

}
