﻿using System.Collections.Generic;
using System.IO;
using System.Text;


namespace IA.AistSubsystem.SingleReport
{

    /// <summary>
    /// Обработка файла _xxxxxxx.2ep.
    /// </summary>
    internal static class Reader_xxxxxxx2ep
    {
        /// <summary>
        /// Вычитать _xxxxxxx.2ep для контрольного выдёргивания признака висячести (0 в определённом столбце).
        /// </summary>
        /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по грозди.</param>
        /// <param name="functions">Список функций.</param>
        public static void Read(string bunchDirectoryPath, List<AistReportEntity> functions)
        {
            string file = Path.Combine(bunchDirectoryPath, "_xxxxxxx.2ep");
            if (!File.Exists(file))
            {
                return;
            }

            using (StreamReader reader = new StreamReader(file, Encoding.Default))
            {
                string line = Common.SkipLines(reader, @"Nпп   Имя функции");
                if (line == null)
                {
                    return;
                }

                while ((line = reader.ReadLine()) != null)
                {
                    var item = ParseLine(line);
                    AistReportEntity currentItem = functions.Find(f => f.NFun == item.Key);
                    if (currentItem != null)
                        currentItem.NumberOfCallsByAist = item.Value;
                    else
                        break;//отчёт построен таким образом, что интересующие функции идут подряд, а уже потом все остальные.
                }
            }
        }

        private static KeyValuePair<int, int> ParseLine(string line)
        { //выдернуть из строки количество вызовов с номером функции
            if (string.IsNullOrWhiteSpace(line)) return new KeyValuePair<int, int>();
            int pos = line.LastIndexOf(' ', 71);
            int fNum = int.Parse(line.Substring(pos, 72 - pos));
            pos = line.LastIndexOfAny(new char[] { ' ', ',' }, 101);
            int calls = int.Parse(line.Substring(pos + 1, 102 - pos));
            return new KeyValuePair<int, int>(fNum, calls);
        }
    }

}
