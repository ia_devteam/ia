﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IA.AistSubsystem.SingleReport
{

    /// <summary>
    /// Обработка файла _xxxxxxx.4ep.
    /// </summary>
    internal static class Reader_xxxxxxx4ep
    {
        /// <summary>
        /// Вычитать _xxxxxxx.4ep для выдёргивания списка переменных.
        /// </summary>
        /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по грозди.</param>
        /// <param name="files">Словарь файлов, проанализированных в грозди.</param>
        /// <returns>Список переменных.</returns>
        static public List<AistReportEntity> Read(string bunchDirectoryPath, Dictionary<int, string> files)
        {
            List<AistReportEntity> varList = null;

            string file = Path.Combine(bunchDirectoryPath, "_xxxxxxx.4ep");
            if (!File.Exists(file))
            {
                return varList;
            }

            using (StreamReader reader = new StreamReader(file, Encoding.Default))
            {
                string curLine = Common.SkipLines(reader, @"Nпп  Имя переменной                    N");
                if (curLine == null)
                {
                    return null;
                }

                List<string> skippedStrings = new List<string>();
                bool isMatrixChoosed = false;

                while (!isMatrixChoosed && (curLine = reader.ReadLine()) != null)
                { //отдельно вычитываем одну строку, чтоб понять, какого вида отчёт: с матричным или с численным представлением.
                    //в теории может так случиться, что первая строка будет "битой". Поэтому добавил обвязку "до пока не определимся".
                    skippedStrings.Add(curLine);
                    int posTmp = curLine.LastIndexOf(')');
                    if (posTmp != -1)
                    {
                        string tmp = curLine.Substring(posTmp);
                        isMatrixReport = tmp.Contains(".") || tmp.Contains("*");
                        isMatrixChoosed = true;
                    }
                }

                if (!isMatrixChoosed)
                    return null; //вычитав весь файл не смог определить вид таблицы - ухожу расстроенный

                varList = new List<AistReportEntity>();

                foreach (var line in skippedStrings)
                { //обработка пропущенных строк
                    var item = ParseLine(curLine);
                    if (item == null)
                        continue;
                    item.SourceFile = files[item.NFile];
                    varList.Add(item);
                }

                while ((curLine = reader.ReadLine()) != null)
                { //обработка невычитанных строк
                    var item = ParseLine(curLine);
                    if (item == null)
                        continue;
                    item.SourceFile = files[item.NFile];
                    varList.Add(item);
                }
            }

            return varList;
        }
        /// <summary>
        /// Отчёт выполнен в виде матрицы, а не в виде последовательности чисел
        /// </summary>
        static bool isMatrixReport = false;

        static char[] delimiter = new char[] { ' ' };
        static private AistReportEntity ParseLine(string line)
        {
            try
            {//иногда формат строки СИЛЬНО другой, АИСТ запросто может оставить имя и количество вызовов без всяких дополнительных номеров. Поэтому в try catch
                string NAME = line.Substring(6, 31).Trim();
                if (NAME.StartsWith("(")) //бывает у макросов (#d), отсеивает существенную их часть.
                    return null;
                int FNUM = Int32.Parse(line.Substring(79, 4));
                int LNUM = Int32.Parse(line.Substring(87, 5));

                int posEnd = line.LastIndexOf('-');
                int USENUM = Int32.Parse(line.Substring(posEnd + 1));

                AistReportEntity result = new AistReportEntity()
                {
                    NFile = FNUM,
                    AistLineNumber = LNUM,
                    Name = NAME,
                    NumberOfCallsByAist = USENUM
                };

                int posStart = line.LastIndexOf(')');
                if (posStart != -1)
                {
                    string functionsThatUseThisVariable = line.Substring(posStart + 1, posEnd - posStart - 1);
                    if (isMatrixReport)
                    {//........*,.........,.........*.........,.........,.........,........*,...
                        var trimmed = functionsThatUseThisVariable.Trim();
                        for (int i = trimmed.IndexOf('*'); i > -1; i = trimmed.IndexOf('*', i + 1))
                            result.AistCalledBy.Add(i + 1);
                    }
                    else
                    {//70 91 106 116 124 125 145 206 210
                        string[] functions = functionsThatUseThisVariable.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

                        foreach (var num in functions)
                        {
                            int curnum;
                            if (int.TryParse(num, out curnum))
                                result.AistCalledBy.Add(curnum);
                        }
                    }

                }

                return result;
            }
            catch
            {
                return null;
            }
        }
    }

}
