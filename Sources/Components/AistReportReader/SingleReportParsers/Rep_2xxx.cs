﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace IA.AistSubsystem.SingleReport
{

    /// <summary>
    /// Обработка файла __Rep_2.xxx.
    /// </summary>
    internal static class Reader__Rep_2xxx
    {
        /// <summary>
        /// Вычитать __Rep_2.xxx для установления связей между функциями.
        /// </summary>
        /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по грозди.</param>
        /// <param name="functions">Список функций.</param>
        public static void Read(string bunchDirectoryPath, List<AistReportEntity> functions)
        {
            string file = Path.Combine(bunchDirectoryPath, "__Rep_2.xxx");
            if (!File.Exists(file) || functions == null || functions.Count == 0)
            {
                return;
            }

            using (StreamReader reader = new StreamReader(file, Encoding.Default))
            {
                string line = Common.SkipLines(reader, @"Имеют вызовы других прикладных функций");
                if (line == null)
                    return;

                int countbyAist = int.Parse(line.Substring(line.IndexOf('-') + 1).TrimEnd(':'));
                int countReal = 0;

                #region Заполнение AistCalls
                AistReportEntity curfunc = null;
                while ((line = reader.ReadLine()) != null && countReal < countbyAist)
                {
                    if (line.Contains("Функция"))
                    {
                        countReal++;
                        var funcInLine = GetFuncNumberFirstLine(line);
                        curfunc = functions.Find(f => f.NFun == funcInLine);
                        if (curfunc == null)
                        {
                            string errorMessage = "Не удалось восстановить связи между функциями в отчёте АИСТ-С <" + file + ">. Обработка остановлена. ";
                            errorMessage += string.Format("Порядковый номер функции <{0}>.", funcInLine);
                            Monitor.Log.Error(AistReportsReader.ObjectName, errorMessage);
                            return;
                        }
                    }
                    else
                    {
                        if (curfunc != null)
                        {
                            var funcInLine = GetFuncNumberFromEnumeration(line);
                            if (functions.Any(f => f.NFun == funcInLine))
                                curfunc.AistCalls.Add(funcInLine);
                        }
                    }

                }
                #endregion

                line = Common.SkipLines(reader, @"Вызываются из других функций");
                if (line == null)
                    return;

                #region Заполнение AistCalledBy

                countbyAist = int.Parse(line.Substring(line.IndexOf('-') + 1).TrimEnd(':'));
                countReal = 0;

                curfunc = null;
                while ((line = reader.ReadLine()) != null && countReal < countbyAist)
                {
                    if (line.Contains("Функция"))
                    {
                        countReal++;
                        var funcInLine = GetFuncNumberFirstLine(line);
                        curfunc = functions.Find(f => f.NFun == funcInLine);
                        if (curfunc == null)
                        {
                            string errorMessage = "Не удалось восстановить связи между функциями в отчёте АИСТ-С <" + file + ">. Обработка остановлена. ";
                            errorMessage += string.Format("Порядковый номер функции <{0}>.", funcInLine);
                            Monitor.Log.Error(AistReportsReader.ObjectName, errorMessage);
                            return;
                        }
                    }
                    else
                    {
                        if (curfunc != null)
                        {
                            var funcInLine = GetFuncNumberFromEnumeration(line);
                            if (functions.Any(f => f.NFun == funcInLine))
                                curfunc.AistCalledBy.Add(funcInLine);
                        }
                    }
                #endregion

                }
            }
        }

        #region Пример
        //40  Функция CheckWord N= 1098 вызывает:
        //1 1105 CheckWithWord
        //2 1106 CheckUpdateWord
        //3 1107 CheckDeleteWord
        //4 1108 CheckInsertWord
        //5 1109 CheckMergeWord
        //6 1110 CheckSelectWord
        #endregion
        private static int GetFuncNumberFirstLine(string line)
        {
            int curfuncNum = int.Parse(line.Substring(line.LastIndexOf("N=") + 2, 5));
            return curfuncNum;
        }

        private static int GetFuncNumberFromEnumeration(string line)
        {
            int curfuncNum = int.Parse(line.Substring(9, 5));
            return curfuncNum;
        }
    }

}
