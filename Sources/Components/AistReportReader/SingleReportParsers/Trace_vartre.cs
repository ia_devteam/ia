﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace IA.AistSubsystem.SingleReport
{

    /// <summary>
    /// Обработать файл __Trace_Var.tre.
    /// </summary>
    internal static class Reader_Trace_Vartre
    {
        /// <summary>
        /// Вычитать __Trace_Var.tre для уточнения имён переменных (в остальных отчётах имена обрезаются свыше 29 символов).
        /// </summary>
        /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по грозди.</param>
        /// <param name="variables">Список переменных.</param>
        /// <param name="files">Словарь файлов, проанализированных в грозди.</param>
        static public void Read(string bunchDirectoryPath, List<AistReportEntity> variables, Dictionary<int, string> files)
        {
            string file = Path.Combine(bunchDirectoryPath, "__Trace_Var.tre");

            if (!File.Exists(file) || variables == null || variables.Count == 0)
            {
                return;
            }

            var varsToProcess = variables.Where(v => v.Name.Length >= 29).OrderBy(v => v.NFile);

            using (StreamReader reader = new StreamReader(file, Encoding.Default))
            {
                foreach (var variable in varsToProcess)
                {
                    string curLine;
                    curLine = Common.SkipLines(reader, files[variable.NFile]);

                    while (curLine != null && !curLine.EndsWith("N=" + variable.AistLineNumber.ToString()))
                    {
                        curLine = Common.SkipLines(reader, variable.Name);
                    }
                    if (curLine != null)
                    {
                        variable.UpdateName(curLine.Substring(2, curLine.IndexOf("N=") - 3));
                    }

                    reader.DiscardBufferedData();//http://msdn.microsoft.com/en-us/library/system.io.streamreader.basestream%28v=vs.110%29.aspx
                    reader.BaseStream.Seek(0, SeekOrigin.Begin);
                }
            }
        }
    }
}
