﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IA.AistSubsystem.SingleReport
{

    /// <summary>
    /// Обработка файла xxxxxxx.rep.
    /// </summary>
    internal static class Reader_xxxxxxxrep
    {
        /// <summary>
        /// Вычитать файл _xxxxxxx.rep для получения списка файлов в правильной нумерации. Лежит в каталоге грозди отчёта АИСТ-С.
        /// </summary>
        /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по конкретной грозди.</param>
        /// <returns>Словарь "номер файла - полный путь к файлу".</returns>
        public static Dictionary<int, string> GetFiles(string bunchDirectoryPath)
        {
            Dictionary<int, string> ret = null;

            string file = Path.Combine(bunchDirectoryPath, "_xxxxxxx.rep");

            if (File.Exists(file))
            {
                using (StreamReader reader = new StreamReader(file, Encoding.Default))
                {
                    string curLine;
                    // пропустить ненужные строки со статистикой
                    curLine = Common.SkipLines(reader, @"Обработанные файлы (");
                    if (curLine != null) //мало ли.. вдруг отчёт битый и в это место не попадёшь никогда...
                    {
                        int pos = curLine.IndexOf(')');
                        if (pos == -1)
                        {
                            //ЗДЕСЬ И ДАЛЕЕ: в бытность частью плагина здесь писались всякие ругательства в лог. После переработки и доработки пустой результат стал иметь смысл и учитывается в процессе обработки. Ошибки выдавать тут нецелесообразно.
                            return ret;
                        }
                        string s_numOfFiles = curLine.Substring(26, pos - 26); //формат фиксирован по этому полю.
                        int numOfFiles;
                        if (!Int32.TryParse(s_numOfFiles, out numOfFiles)) //в этом месте может быть ошибка. Если появилась, значит файл отчёта не просто бит. А убит.
                        {
                            return ret;
                        }
                        ret = new Dictionary<int, string>(numOfFiles);

                        while ((curLine = reader.ReadLine()) != null && !String.IsNullOrWhiteSpace(curLine))
                        { //покуда строки с номерами и именами файлов не кончатся - вычитываем
                            var item = ParseLine(curLine);
                            if (item.Key == -1)
                                continue;
                            ret.Add(item.Key, item.Value.ToLowerInvariant());
                        }
                    }
                    else
                    { //curLine == null
                        reader.Close();
                        return ret;
                    }
                }
            }
            return ret;
        }

        private static KeyValuePair<int, string> ParseLine(string line)
        {
            int pos = line.IndexOf(')');

            if (pos == -1)
                return new KeyValuePair<int, string>(-1, null);

            int num = Int32.Parse(line.Substring(0, pos));
            string s = line.Substring(pos + 2);
            return new KeyValuePair<int, string>(num, s);
        }
    }

}
