﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IA.AistSubsystem.SingleReport
{

    /// <summary>
    /// Обработка файлов _ListFil.tre либо _ListFiles.tre.
    /// </summary>
    internal static class Reader_Listfil
    {//Вычитать файл _ListFil.tre (либо _ListFiles.tre) для получения списка файлов. Нумерации в файле нет, но обычно она совпадает с оной в _xxxxxxx.rep. Лежит в каталоге грозди отчёта АИСТ-С.
        /// <summary>
        /// Проверить существование файла _ListFil.tre (либо _ListFiles.tre) и попытать вычитать его строки как имена файлов с естественной нумерацией.
        /// </summary>
        /// <param name="bunchDirectoryPath">Имя каталога грозди для обработки.</param>
        /// <returns>Словарь номер файла - полное имя файла. null в случае ошибки.</returns>
        public static Dictionary<int, string> Read(string bunchDirectoryPath)
        {
            Dictionary<int, string> ret = null;

            string file = Path.Combine(bunchDirectoryPath, @"_ListFil.tre");

            if (!File.Exists(file))
                file = Path.Combine(bunchDirectoryPath, @"_ListFiles.tre");

            if (!File.Exists(file))
                return null;

            if (new FileInfo(file).Length == 0) //бывают почти папки с пустым искомым файлом
                return null;

            using (StreamReader reader = new StreamReader(file, Encoding.Default))
            {
                string curline;
                int i = 1;
                ret = new Dictionary<int, string>();
                while (!String.IsNullOrWhiteSpace(curline = reader.ReadLine()))
                {
                    ret.Add(i++, curline);
                }
            }
            return ret;
        }
    }

}
