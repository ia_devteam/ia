﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;

namespace IA.AistSubsystem
{

    /// <summary>
    /// Класс может проанализировать один отчёт АИСТ-С и перевести информацию о функциях и переменных во внутреннее представление.
    /// </summary>
    public class AistReportsReader
    {
        /// <summary>
        /// Название модуля
        /// </summary>
        internal const string ObjectName = "Модуль анализа отчётов АИСТ-С";

        /// <summary>
        /// Создать объект читателя и проанализировать состав отчёта АИСТ-С.
        /// </summary>
        /// <param name="aistReportsDirectoryPath">Полный путь до корня каталога отчёта АИСТ-С.</param>
        /// <param name="needFunctionRelations">Флаг необходимости вычитывания зависимостей между функциями.</param>
        /// <param name="oldFilePrefix">Путь до файлов исходных текстов, которые обрабатывались АИСТ-С (та часть пути, что будет заменена на актуальную).</param>
        /// <param name="newFilePrefix">Актуальный путь до файлов исходных текстов.</param>
        public AistReportsReader(string aistReportsDirectoryPath, bool needFunctionRelations = false, string oldFilePrefix = "", string newFilePrefix = "")
        {
            if (!Directory.Exists(aistReportsDirectoryPath))
                throw new AistReportReaderException("Ошибка при создании экземпляра класса, отвечающего за чтение отчётов АИСТа. Путь до директории с отчётами не существует.");

            this.AistReportsDirectoryPath = aistReportsDirectoryPath;
            AistReportsBunchReader.needFunctionRelations = needFunctionRelations;
            PrepareBunches(oldFilePrefix, newFilePrefix);
        }

        /// <summary>
        /// Конструктор для десериализации
        /// </summary>
        private AistReportsReader()
        {

        }

        #region Поля
        /// <summary>
        /// Путь к каталогу, содержащему ветви отчётов АИСТ-С.
        /// </summary>
        public string AistReportsDirectoryPath { get; private set; }

        /// <summary>
        /// Получить количество анализируемых гроздей.
        /// </summary>
        public int BunchesCount
        {
            get { return bunchList.Count; }
        }

        /// <summary>
        /// Список гроздей отчёта. Внутри всё, что может понадобиться.
        /// </summary>
        private List<AistBunch> bunchList = new List<AistBunch>();
        #endregion

        #region Методы
        /// <summary>
        /// Получить перечисление всех гроздей отчёта.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AistBunch> EnumerateBunches()
        {
            foreach (var aistbunch in bunchList)
                yield return aistbunch;
        }

        /// <summary>
        /// Запустить анализ содержательности гроздей, составляющих отчёт АИСТ-С.
        /// </summary>
        /// <param name="oldFilePrefix">Путь до файлов исходных текстов, которые обрабатывались АИСТ-С (та часть пути, что будет заменена на актуальную).</param>
        /// <param name="newFilePrefix">Актуальный путь до файлов исходных текстов.</param>
        private void PrepareBunches(string oldFilePrefix = "", string newFilePrefix = "")
        {
            //блок выковыривания списка гроздей данного отчёта. 
            //В зависимости от настроек АИСТ-С порождает разное количетво файлов и каталогов,
            //приходится изголяться, чтобы работать в большинстве случаев.
            var bunchFileNamesDictionary = Reader_Protokoltxt.Read(Path.Combine(this.AistReportsDirectoryPath, "Protokol.txt")) 
                ?? new Dictionary<string, Dictionary<int, string>>();
            var bunchNames = Directory.EnumerateDirectories(this.AistReportsDirectoryPath, "*.*", SearchOption.TopDirectoryOnly).Select(d => Path.GetFileName(d));

            //ситуация, что в Protokol.txt не попали какие-то грозди
            foreach (var name in bunchNames.Where(n => !bunchFileNamesDictionary.ContainsKey(n)))
                bunchFileNamesDictionary.Add(name, null);

            foreach (var name in bunchNames)
            {
                //для каждой грозди пытаемся добыть список входящих в неё файлов
                if (bunchFileNamesDictionary[name] == null)
                    bunchFileNamesDictionary[name] = AistReportsBunchReader.GetFiles(Path.Combine(this.AistReportsDirectoryPath, name));

                if (bunchFileNamesDictionary[name] == null)
                    bunchFileNamesDictionary.Remove(name);//если для данной грозди неудалось получить список файлов, то и содержательно она пуста
            }

            if (!String.IsNullOrWhiteSpace(oldFilePrefix) && !String.IsNullOrWhiteSpace(newFilePrefix))
            {
                oldFilePrefix = oldFilePrefix.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                newFilePrefix = newFilePrefix.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                foreach (var bunch in bunchFileNamesDictionary.Values)
                {
                    var bunch2 = new Dictionary<int, string>(bunch.Count);
                    foreach (var key in bunch.Keys)
                        bunch2[key] = bunch[key].Replace(oldFilePrefix, newFilePrefix); //производится подмена понятий
                    foreach (var key in bunch2.Keys)
                        bunch[key] = bunch2[key];
                }
            }

            foreach (var bunch in bunchFileNamesDictionary)
            {
                //для каждой грозди со списком проанализированных файлов создаём объект-всезнайку
                bunchList.Add(new AistBunch(Path.Combine(this.AistReportsDirectoryPath, bunch.Key), bunch.Value));
            }
        }

        #region Сериализация в XML
        /// <summary>
        /// Сохранить информацию в файл для последующего восстановления.
        /// </summary>
        /// <param name="filePath">Путь к файлу.</param>
        public void ToFile(string filePath)
        {
            XElement root = new XElement("SerializedAistReport");
            
            root.Add(new XElement("AistReportsDirectoryPath", AistReportsDirectoryPath));
   
            foreach (var entity in bunchList)
                root.Add(entity.Serialize());

            try
            {
                (new XDocument(root)).Save(filePath);
            }
            catch (Exception ex)
            {
                throw new AistReportReaderException("Не удалось записать данные в файл <" + filePath + ">.", ex);
            }
        }

        /// <summary>
        /// Создать объект читателя используя ранее сохранённые данные.
        /// </summary>
        /// <param name="filePath">Путь к файлу.</param>
        /// <returns></returns>
        public static AistReportsReader FromFile(string filePath)
        {
            if (!IOController.FileController.IsExists(filePath))
                return null;

            try
            {
                XDocument xdoc = XDocument.Load(filePath);

                XElement root = xdoc.Element("SerializedAistReport");
                AistReportsReader ret = new AistReportsReader()
                {
                    AistReportsDirectoryPath = root.Element("AistReportsDirectoryPath").Value,
                    bunchList = root.Elements("Bunch").Select(b => AistBunch.Deserialize(b)).ToList()
                };

                return ret;
            }
            catch (Exception ex)
            {
                throw new AistReportReaderException("Не удалось прочитать данные из файла <" + filePath + ">.", ex);
            }
        }
        #endregion
        #endregion

        #region Внутренние вспомогательные классы
        #region Вычитывание файлов в корне отчёта
        /// <summary>
        /// Вычитать файл Protokol.txt для получения списка гроздей вместе со списком файлов в каждой грозди. Файл иногда может не содержать нужной информации или вовсе несуществовать. Лежит в корне отчёта АИСТ-С.
        /// </summary>
        internal static class Reader_Protokoltxt
        {
            /// <summary>
            /// Проверить существование и формат файла Protokol.txt. Вычитать содержимое на предмет названия гроздей и входящих в каждую гроздь файлов.
            /// </summary>
            /// <param name="fullpath">Полный путь до файла Protokol.txt.</param>
            /// <returns>Словарь, где ключ - имя грозди, значение - словарь (номер файла, полный путь). В случае ошибки - null.</returns>
            public static Dictionary<string, Dictionary<int, string>> Read(string fullpath)
            {
                Dictionary<string, Dictionary<int, string>> ret = null;

                if (!File.Exists(fullpath))
                    return null;

                using (StreamReader reader = new StreamReader(fullpath, Encoding.Default))
                {
                    if (reader.ReadLine() != @"   Версия программы: Версия от 30.01.2001") //в других версиях возможны изменения форматов
                        return ret;

                    ret = new Dictionary<string, Dictionary<int, string>>();

                    while (!reader.EndOfStream && !String.IsNullOrWhiteSpace(reader.ReadLine()))
                    {
                        string line = Common.SkipLines(reader, @"  Список файлов, составляющих гроздь [");
                        //Список файлов, составляющих гроздь [...\001_makestub]

                        if (line == null)
                            break;

                        string bunchName;
                        try
                        {
                            int bunchNameStart = line.IndexOf('[') + 5;
                            int bunchNameEnd = line.LastIndexOf(']') - 1;

                            bunchName = line.Substring(bunchNameStart, bunchNameEnd - bunchNameStart + 1);

                            ret.Add(bunchName, new Dictionary<int, string>());
                        }
                        catch (Exception ex)
                        {
                            throw new AistReportReaderException("Файл Protokol.txt: название грозди вне формата.", ex);
                        }

                        while (!String.IsNullOrWhiteSpace(line = reader.ReadLine()) && line != "  Конец списка")
                        {
                            try
                            {
                                string curlineTrimmed = line.Trim();

                                int posOfWhitespaceDelimiter = 0;

                                while (Char.IsDigit(curlineTrimmed[posOfWhitespaceDelimiter++])) ;

                                int filenum = int.Parse(curlineTrimmed.Substring(0, posOfWhitespaceDelimiter - 1));

                                string filename = curlineTrimmed.Substring(posOfWhitespaceDelimiter);

                                ret[bunchName].Add(filenum, filename);
                            }
                            catch (Exception ex)
                            {
                                throw new AistReportReaderException("Файл Protokol.txt: описание файла грозди вне формата.", ex);
                            }
                        }
                    }
                }
                return ret;
            }
        }
        #endregion

        #region Вычитывание файлов в отдельной грозди
        /// <summary>
        /// Анализ одного частного каталога отчёта АИСТ-С по "грозди". Вспомогательный характер, поэтому наружу ничего не показыbunchDirectoryPathваем.
        /// </summary>
        internal static class AistReportsBunchReader
        {
            /// <summary>
            /// Флаг необходимости вычитывания зависимостей между функциями.
            /// </summary>
            static public bool needFunctionRelations = false;

            /// <summary>
            /// Прочитать каталог отчёта АИСТ-С и получить список функций (в т.ч. "висячесть", файл определения, позиция в файле). Некоторые аттрибуты могут быть недоступны или содержать ложную информацию.
            /// </summary>
            /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по конкретной грозди.</param>
            /// <param name="files">Список исходных файлов, по которой сформирована данная гроздь.</param>
            /// <returns>Список функций (содержит в себе все доступные данные). null в случае ошибки.</returns>
            public static List<AistReportEntity> GetFunctions(string bunchDirectoryPath, Dictionary<int, string> files)
            {
                if (files == null)
                    return null;

                List<AistReportEntity> functions = SingleReport.Reader_xxxxxxx8ep.ReadFuncList(bunchDirectoryPath, files);

                if (functions == null)
                    return null;

                SingleReport.Reader_Reportcsv.Read(bunchDirectoryPath, functions);

                SingleReport.Reader_xxxxxxx2ep.Read(bunchDirectoryPath, functions);

                if (needFunctionRelations)
                    TryGetFunctionsRelations(bunchDirectoryPath, functions);

                return functions;
            }

            //пока по сути это единственный вызов вычитывания __Rep_2.xxx, но, возможно, потребуется дополнение вычиткой других файлов
            /// <summary>
            /// Попытаться вычитать из отчёта АИСТ-С информацио о зависимостях между функциями (карту вызовов).
            /// </summary>
            /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по конкретной грозди.</param>
            /// <param name="functions">Список функций.</param>
            private static void TryGetFunctionsRelations(string bunchDirectoryPath, List<AistReportEntity> functions)
            {
                SingleReport.Reader__Rep_2xxx.Read(bunchDirectoryPath, functions);
            }

            /// <summary>
            /// Прочитать каталог отчёта АИСТ-С и получить список переменных (в т.ч. файл определения, позиция в файле). Некоторые аттрибуты могут быть недоступны или содержать ложную информацию.
            /// </summary>
            /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по конкретной грозди.</param>
            /// <param name="files">Список исходных файлов, по которой сформирована данная гроздь.</param>
            /// <returns>Список переменных (содержит в себе все доступные данные). null в случае ошибки.</returns>
            public static List<AistReportEntity> GetVariables(string bunchDirectoryPath, Dictionary<int, string> files)
            {
                if (files == null)
                    return null;

                List<AistReportEntity> result = SingleReport.Reader_xxxxxxx4ep.Read(bunchDirectoryPath, files);

                SingleReport.Reader_Trace_Vartre.Read(bunchDirectoryPath, result, files);

                return result;
            }

            /// <summary>
            /// Вытащить из каталога с отчётом по грозди АИСТ-С список обработанных файлов. В качестве источника файлы xxxxxxx.rep, _ListFil.tre и _ListFiles.tre.
            /// </summary>
            /// <param name="bunchDirectoryPath">Полный путь до каталога с отчётом по конкретной грозди.</param>
            /// <returns>Словарь "номер файла - полный путь к файлу".</returns>
            public static Dictionary<int, string> GetFiles(string bunchDirectoryPath)
            {
                return SingleReport.Reader_xxxxxxxrep.GetFiles(bunchDirectoryPath) ?? SingleReport.Reader_Listfil.Read(bunchDirectoryPath);
            }


        }
        #endregion

        #endregion

    }

    /// <summary>
    /// Общие для всех классов поля и методы.
    /// </summary>
    internal static class Common
    {
        /// <summary>
        /// Пропустить все строки до указанной.
        /// </summary>
        /// <param name="sr">Ридер, в котором пропускать строки.</param>
        /// <param name="stopSubString">Часть строки, до которой пропускать (проверка по Contains).</param>
        /// <returns>Строка, содержащая указанную часть либо null при достижении конца.</returns>
        static internal string SkipLines(StreamReader sr, String stopSubString)
        {
            string line;

            while ((line = sr.ReadLine()) != null && !line.Contains(stopSubString))
                ;

            return line;
        }
    }


}
