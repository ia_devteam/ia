﻿namespace IA.AistSubsystem
{
    partial class AistReportReaderSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpAistRepoerParserSettings = new System.Windows.Forms.GroupBox();
            this.tblLayout2Rows = new System.Windows.Forms.TableLayoutPanel();
            this.grpAistReportDirectory = new System.Windows.Forms.GroupBox();
            this.txtPathToAistReport = new IA.Controls.TextBoxWithDialogButton();
            this.grpOldSourcesDirectory = new System.Windows.Forms.GroupBox();
            this.txtPathToOldSources = new IA.Controls.TextBoxWithDialogButton();
            this.grpAistRepoerParserSettings.SuspendLayout();
            this.tblLayout2Rows.SuspendLayout();
            this.grpAistReportDirectory.SuspendLayout();
            this.grpOldSourcesDirectory.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpAistRepoerParserSettings
            // 
            this.grpAistRepoerParserSettings.Controls.Add(this.tblLayout2Rows);
            this.grpAistRepoerParserSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpAistRepoerParserSettings.Location = new System.Drawing.Point(0, 0);
            this.grpAistRepoerParserSettings.Name = "grpAistRepoerParserSettings";
            this.grpAistRepoerParserSettings.Size = new System.Drawing.Size(420, 120);
            this.grpAistRepoerParserSettings.TabIndex = 0;
            this.grpAistRepoerParserSettings.TabStop = false;
            this.grpAistRepoerParserSettings.Text = "Настройка путей для анализа отчётов АИСТ-С";
            // 
            // tblLayout2Rows
            // 
            this.tblLayout2Rows.ColumnCount = 1;
            this.tblLayout2Rows.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayout2Rows.Controls.Add(this.grpAistReportDirectory, 0, 0);
            this.tblLayout2Rows.Controls.Add(this.grpOldSourcesDirectory, 0, 1);
            this.tblLayout2Rows.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblLayout2Rows.Location = new System.Drawing.Point(3, 16);
            this.tblLayout2Rows.Name = "tblLayout2Rows";
            this.tblLayout2Rows.RowCount = 2;
            this.tblLayout2Rows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayout2Rows.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblLayout2Rows.Size = new System.Drawing.Size(414, 101);
            this.tblLayout2Rows.TabIndex = 0;
            // 
            // grpAistReportDirectory
            // 
            this.grpAistReportDirectory.Controls.Add(this.txtPathToAistReport);
            this.grpAistReportDirectory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpAistReportDirectory.Location = new System.Drawing.Point(3, 3);
            this.grpAistReportDirectory.Name = "grpAistReportDirectory";
            this.grpAistReportDirectory.Size = new System.Drawing.Size(408, 44);
            this.grpAistReportDirectory.TabIndex = 0;
            this.grpAistReportDirectory.TabStop = false;
            this.grpAistReportDirectory.Text = "Путь до каталога с отчётами АИСТ-С";
            // 
            // txtPathToAistReport
            // 
            this.txtPathToAistReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPathToAistReport.Location = new System.Drawing.Point(3, 16);
            this.txtPathToAistReport.MaximumSize = new System.Drawing.Size(0, 25);
            this.txtPathToAistReport.MinimumSize = new System.Drawing.Size(400, 25);
            this.txtPathToAistReport.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.txtPathToAistReport.Name = "txtPathToAistReport";
            this.txtPathToAistReport.Size = new System.Drawing.Size(402, 25);
            this.txtPathToAistReport.TabIndex = 0;
            // 
            // grpOldSourcesDirectory
            // 
            this.grpOldSourcesDirectory.Controls.Add(this.txtPathToOldSources);
            this.grpOldSourcesDirectory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpOldSourcesDirectory.Location = new System.Drawing.Point(3, 53);
            this.grpOldSourcesDirectory.Name = "grpOldSourcesDirectory";
            this.grpOldSourcesDirectory.Size = new System.Drawing.Size(408, 45);
            this.grpOldSourcesDirectory.TabIndex = 1;
            this.grpOldSourcesDirectory.TabStop = false;
            this.grpOldSourcesDirectory.Text = "Путь до каталога с исходными текстами при обработке АИСТ-С";
            // 
            // txtPathToOldSources
            // 
            this.txtPathToOldSources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPathToOldSources.Location = new System.Drawing.Point(3, 16);
            this.txtPathToOldSources.MaximumSize = new System.Drawing.Size(0, 25);
            this.txtPathToOldSources.MinimumSize = new System.Drawing.Size(400, 25);
            this.txtPathToOldSources.Mode = IA.Controls.TextBoxWithDialogButton.DialogTypes.FolderBrowserDialog;
            this.txtPathToOldSources.Name = "txtPathToOldSources";
            this.txtPathToOldSources.Size = new System.Drawing.Size(402, 25);
            this.txtPathToOldSources.TabIndex = 0;
            // 
            // AistReportReaderSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpAistRepoerParserSettings);
            this.MaximumSize = new System.Drawing.Size(0, 120);
            this.MinimumSize = new System.Drawing.Size(420, 120);
            this.Name = "AistReportReaderSettingsControl";
            this.Size = new System.Drawing.Size(420, 120);
            this.grpAistRepoerParserSettings.ResumeLayout(false);
            this.tblLayout2Rows.ResumeLayout(false);
            this.grpAistReportDirectory.ResumeLayout(false);
            this.grpOldSourcesDirectory.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpAistRepoerParserSettings;
        private System.Windows.Forms.TableLayoutPanel tblLayout2Rows;
        private System.Windows.Forms.GroupBox grpAistReportDirectory;
        private IA.Controls.TextBoxWithDialogButton txtPathToAistReport;
        private System.Windows.Forms.GroupBox grpOldSourcesDirectory;
        private IA.Controls.TextBoxWithDialogButton txtPathToOldSources;

    }
}
