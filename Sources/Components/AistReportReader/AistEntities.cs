﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using IOController;

namespace IA.AistSubsystem
{
    /// <summary>
    /// Подробное представление функции (переменной) из отчётов АИСТ-С. Включает возможность записи значений из Хранилища ИА.
    /// </summary>
    public class AistReportEntity
    {
        /// <summary>
        /// Конструктор по-умолчанию. Инициализирует поля, способные быть null (для правильной сериализации в xml).
        /// </summary>
        internal AistReportEntity()
        {
            SourceFile = String.Empty;
            StorageEntityName = String.Empty;
            CallPlaceFileName = String.Empty;
            StorageEssentialFlag = Essential.UNKNOWN;
            CallPlaceFunctionName = String.Empty;

            AistCalls = new List<int>();
            AistCalledBy = new List<int>();
        }

        #region Поля
        /// <summary>
        /// Перечисление возможных состояний избыточности в терминах хранилища
        /// </summary>
        public enum Essential
        {
            /// <summary>
            /// Избыточность неустановлена
            /// </summary>
            UNKNOWN,
            /// <summary>
            /// Избыточность установлена
            /// </summary>
            REDUNANT,
            /// <summary>
            /// Значимость установлена
            /// </summary>
            ESSENTIAL,
            /// <summary>
            /// Избыточность неприменима, так как является информационным объектом
            /// </summary>
            INFORMOBJECT
        }

        #region AistPart
        private int nPP = -1;
        /// <summary>
        /// В отчётах АИСТ-С порядковый номер функции в рамках одной грозди, в разных отчётах может смещаться из-за "дублей" других функций. Устанавливается единожды.
        /// </summary>
        public int NPP
        {
            get
            {
                return this.nPP;
            }
            set
            {
                if (this.nPP == -1)
                    this.nPP = value;
                else
                    Warning();

            }
        }

        private int nFun = -1;
        /// <summary>
        /// В отчётах АИСТ-С абсолютный номер функции в рамках одной грозди. Устанавливается единожды.
        /// </summary>
        public int NFun
        {
            get
            {
                return this.nFun;
            }
            set
            {
                if (this.nFun == -1)
                    this.nFun = value;
                else
                    Warning();

            }
        }

        private int nFile = -1;
        /// <summary>
        /// В отчётах АИСТ-С номер файла исходных текстов, в которых была найдена функция. Устанавливается единожды.
        /// </summary>
        public int NFile
        {
            get
            {
                return this.nFile;
            }
            set
            {
                if (this.nFile == -1)
                    this.nFile = value;
                else
                    Warning();
            }
        }

        private char essentialFlagByAist = ' ';
        /// <summary>
        /// В отчётах АИСТ-С символ избыточности: . - избыточная, - - вызов из избыточной, Г - входная функция (main). Устанавливается единожды.
        /// </summary>
        public char EssentialFlagByAist
        {
            get
            {
                return this.essentialFlagByAist;
            }
            set
            {
                if (this.essentialFlagByAist == ' ')
                    this.essentialFlagByAist = value;
                else
                    Warning();
            }
        }

        private string name = null;
        /// <summary>
        /// В отчётах АИСТ-С имя функции. Устанавливается единожды.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set
            {
                if (this.name == null)
                    this.name = value;
                else
                    Warning();
            }
        }

        private int numberOfCallsByAist = -1;
        /// <summary>
        /// Число прямых вызовов функций. Устанавливается единожды.
        /// </summary>
        public int NumberOfCallsByAist
        {
            get
            {
                return this.numberOfCallsByAist;
            }
            set
            {
                if (this.numberOfCallsByAist == -1)
                    this.numberOfCallsByAist = value;
                else
                    Warning();
            }
        }

        /// <summary>
        /// Флаг избыточности в терминах АИСТ-С. Может быть только REDUNANT ИЛИ ESSENTIAL. Управляется numberOfCallsByAist и (если не задан первый) essentialFlagByAist.
        /// </summary>
        public Essential AistEssentialFlag
        {
            get
            {
                if (this.NumberOfCallsByAist < 0)
                {
                    switch (this.EssentialFlagByAist)
                    {
                        case '*':
                            return Essential.ESSENTIAL;
                        default:
                            return Essential.REDUNANT;
                    }
                }
                else
                {
                    if (this.NumberOfCallsByAist == 0)
                        return Essential.REDUNANT;
                    else
                        return Essential.ESSENTIAL;
                }
            }
        }

        private int aistLineNumber = -1;
        /// <summary>
        /// Строка, в которой была найдена функция (открывающая фигурная скобка?). Устанавливается единожды.
        /// </summary>
        public int AistLineNumber
        {
            get
            {
                return this.aistLineNumber;
            }
            set
            {
                if (this.aistLineNumber == -1)
                    this.aistLineNumber = value;
                else
                    Warning();
            }
        }

        /// <summary>
        /// Список номеров (в терминах АИСТ-С - NFUN) функций, вызываемых из данной.
        /// </summary>
        public List<int> AistCalls { get; private set; }

        /// <summary>
        /// Список номеров (в терминах АИСТ-С - NFUN) функций, вызывающих данную.
        /// </summary>
        public List<int> AistCalledBy { get; private set; }

        /// <summary>
        /// Имя файла исходных текстов, в котором была найдена функция.
        /// </summary>
        public string SourceFile { get; set; }
        #endregion

        #region StoragePart
        /// <summary>
        /// Флаг избыточности в терминах хранилища
        /// </summary>
        public Essential StorageEssentialFlag { get; set; }

        private int storageLineNumber = -1;
        /// <summary>
        /// Строка, в которой происходит вызов функции / использование информ. объекта. Устанавливается единожды.
        /// </summary>
        public int StorageLineNumber
        {
            get
            {
                return this.storageLineNumber < 0 ? this.AistLineNumber : this.storageLineNumber;
            }
            set
            {
                if (this.storageLineNumber == -1)
                    this.storageLineNumber = value;
                else
                    Warning();
            }
        }

        /// <summary>
        /// Имя сущности в хранилище, совпадение с которой определено
        /// </summary>
        public string StorageEntityName { get; set; }

        /// <summary>
        /// Имя файла, из которого происходит вызов данной функции. Имеет смысл только при storageEssentialflag == ESSENTIAL.
        /// </summary>
        public string CallPlaceFileName { get; set; }

        /// <summary>
        /// Имя функции, из которой происходит вызов данной функции. Имеет смысл только при storageEssentialflag == ESSENTIAL.
        /// </summary>
        public string CallPlaceFunctionName { get; set; }

        private int callPlaceLineNumber = -1;
        /// <summary>
        /// Номер строки, в которой происходит вызов данной функции. Имеет смысл только при storageEssentialflag == ESSENTIAL. Устанавливается единожды.
        /// </summary>
        public int CallPlaceLineNumber
        {
            get
            {
                return this.callPlaceLineNumber;
            }
            set
            {
                if (this.callPlaceLineNumber == -1)
                    this.callPlaceLineNumber = value;
                else
                    Warning();
            }
        }
        #endregion

        /// <summary>
        /// Отчёт о значимости данной функции с точки зрения Хранилища. Имеет смысл только после обработки отчётов АИСТ-С.
        /// </summary>
        public string EssentialReport
        {
            get
            {
                StorageEntityName = String.IsNullOrWhiteSpace(StorageEntityName) ? "<неизвестно>" : StorageEntityName;
                SourceFile = String.IsNullOrWhiteSpace(SourceFile) ? "<неизвестно>" : SourceFile;
                CallPlaceFunctionName = String.IsNullOrWhiteSpace(CallPlaceFunctionName) ? "<неизвестно>" : CallPlaceFunctionName;
                CallPlaceFileName = String.IsNullOrWhiteSpace(CallPlaceFileName) ? "<неизвестно>" : CallPlaceFileName;
                switch (StorageEssentialFlag)
                {
                    case Essential.REDUNANT:
                        return String.Format("Избыточная функция. Полное имя \"{0}\". Определена в файле <{1}> на строке {2}.",
                            this.StorageEntityName,
                            this.SourceFile,
                            this.StorageLineNumber);
                    case Essential.INFORMOBJECT:
                        return String.Format("Информационный объект. Полное имя \"{0}\". Используется в файле <{1}> на строке {2}.",
                            this.StorageEntityName,
                            this.SourceFile,
                            this.StorageLineNumber);
                    case Essential.ESSENTIAL:
                        return String.Format("Значимая функция. Полное имя \"{0}\". Определена в файле <{1}> на строке {2}. Вызывается функцией \"{3}\" в файле <{4}> на строке {5}.",
                            this.StorageEntityName,
                            this.SourceFile,
                            this.StorageLineNumber,
                            this.CallPlaceFunctionName,
                            this.CallPlaceFileName,
                            this.CallPlaceLineNumber
                            );
                    default:
                        return "В хранилище не найдено объектов, соотносящихся с данным.";
                }
            }
        }
        #endregion

        #region Методы
        /// <summary>
        /// Откорректировать имя в сторону увеличения.
        /// </summary>
        /// <param name="name">Новое имя должно начинаться со старого.</param>
        /// <returns>Успешность обновления имени.</returns>
        internal bool UpdateName(string name)
        {
            if (this.Name == null || !name.StartsWith(this.Name))
                return false;

            this.Name = name;

            return true;
        }

        /// <summary>
        /// Для дебага.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("OrigName: {0}, FullName: {1}, EssOrig: {2}, EssStor: {3}.", (this.Name ?? "<неизвестно>"), (this.StorageEntityName ?? "<неизвестно>"), this.EssentialFlagByAist, this.StorageEssentialFlag);
        }

        /// <summary>
        /// Метод для отображения предупреждения о попытке повторно установить значение какого-либо поля
        /// </summary>
        /// <param name="memberName">Имя поля</param>
        internal static void Warning([System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            Monitor.Log.Warning(AistReportsReader.ObjectName, "Произошла попытка повторно установить значение поля <" + memberName + ">.");
        }

        #region Сериализация в XML
        /// <summary>
        /// Сериализовать данный объект в XML.
        /// </summary>
        /// <returns>System.Xml.Linq.XElement содержащий описание объекта.</returns>
        internal XElement ToXElement()
        {
            XElement ret = new XElement("AistReportEntity");

            XElement AistPart = new XElement("AistPart");
            XElement StoragePart = new XElement("StoragePart");

            AistPart.Add(
                new XAttribute("name", name),
                new XAttribute("aistLineNumber", aistLineNumber),
                new XAttribute("essentialFlagByAist", essentialFlagByAist),
                new XAttribute("nFile", nFile),
                new XAttribute("nFun", nFun),
                new XAttribute("nPP", nPP),
                new XAttribute("numberOfCallsByAist", numberOfCallsByAist),
                new XElement("AistCalls",
                                String.Join(";", AistCalls.ToArray())),
                new XElement("AistCalledBy",
                                String.Join(";", AistCalledBy.ToArray()))
                );

            StoragePart.Add(
                new XAttribute("StorageEntityName", StorageEntityName),
                new XAttribute("StorageEssentialFlag", StorageEssentialFlag),
                new XAttribute("storageLineNumber", storageLineNumber),
                new XAttribute("CallPlaceFunctionName", CallPlaceFunctionName),
                new XAttribute("callPlaceLineNumber", callPlaceLineNumber),
                new XAttribute("CallPlaceFileName", CallPlaceFileName)
                );

            ret.Add(
                new XElement("SourceFile", SourceFile),
                AistPart,
                StoragePart
                );

            return ret;
        }

        /// <summary>
        /// Получить объект из XML.
        /// </summary>
        /// <param name="xElement">System.Xml.Linq.XElement содержащий описание объекта.</param>
        /// <returns>Восстановленный объект.</returns>
        internal static AistReportEntity FromXElement(XElement xElement)
        {
            AistReportEntity ret = new AistReportEntity()
            {
                SourceFile = xElement.Element("SourceFile").Value,
            };

            XElement AistPart = xElement.Element("AistPart");
            XElement StoragePart = xElement.Element("StoragePart");

            ret.name = AistPart.Attribute("name").Value;
            ret.essentialFlagByAist = AistPart.Attribute("essentialFlagByAist").Value[0];

            int.TryParse(AistPart.Attribute("aistLineNumber").Value, out ret.aistLineNumber);
            int.TryParse(AistPart.Attribute("nFile").Value, out ret.nFile);
            int.TryParse(AistPart.Attribute("nFun").Value, out ret.nFun);
            int.TryParse(AistPart.Attribute("nPP").Value, out ret.nPP);
            int.TryParse(AistPart.Attribute("numberOfCallsByAist").Value, out ret.numberOfCallsByAist);

            if (!AistPart.Element("AistCalls").IsEmpty)
            {
                var aistCalls = AistPart.Element("AistCalls").Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(s =>
                {
                    int result = 0;
                    int.TryParse(s, out result);
                    return result;
                });
                ret.AistCalls.AddRange(aistCalls);
            }

            if (!AistPart.Element("AistCalledBy").IsEmpty)
            {
                var aistCalls = AistPart.Element("AistCalledBy").Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(s =>
                {
                    int result = 0;
                    int.TryParse(s, out result);
                    return result;
                });
                ret.AistCalledBy.AddRange(aistCalls);
            }

            ret.StorageEntityName = StoragePart.Attribute("StorageEntityName").Value;
            ret.CallPlaceFileName = StoragePart.Attribute("CallPlaceFileName").Value;
            ret.CallPlaceFunctionName = StoragePart.Attribute("CallPlaceFunctionName").Value;
            ret.StorageEssentialFlag = (Essential)Enum.Parse(typeof(Essential), StoragePart.Attribute("StorageEssentialFlag").Value);

            int.TryParse(StoragePart.Attribute("storageLineNumber").Value, out ret.storageLineNumber);
            int.TryParse(StoragePart.Attribute("callPlaceLineNumber").Value, out ret.callPlaceLineNumber);

            return ret;
        }
        #endregion
        #endregion
    }

    /// <summary>
    /// Класс представляет сущность одной "грозди" (поддиректории отчётов) АИСТ-С.
    /// </summary>
    public class AistBunch
    {
        /// <summary>
        /// Конструктор с заданием каталога отчёта и списка файлов отчёта.
        /// </summary>
        /// <param name="directoryPath">Полный путь до каталога отчёта.</param>
        /// <param name="files">Словарь со списком файлов под нумерацией как ключ.</param>
        internal AistBunch(string directoryPath, Dictionary<int, string> files)
        {
            try
            {
                //Проверка на разумность
                if (String.IsNullOrWhiteSpace(directoryPath))
                    throw new AistReportReaderException("Директория грозди не определена.");

                //Проверка на разумность
                if (!DirectoryController.IsExists(directoryPath))
                    throw new AistReportReaderException("Директория грозди <" + directoryPath + "> не существует.");

                //Проверка на разумность
                if (files == null)
                    throw new AistReportReaderException("Словарь файлов не определён.");

                this.DirectoryPath = directoryPath;
                this.Name = Path.GetFileName(this.DirectoryPath.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar));
                this.Files = files;
            }
            catch (Exception ex)
            {
                throw new AistReportReaderException("Ошибка при создании экземпляра \"грозди\" отчётов АИСТ-С.", ex);
            }
        }

        #region Поля
        /// <summary>
        /// Путь до каталога с отчётом по грозди.
        /// </summary>
        private string DirectoryPath { get; set; }

        /// <summary>
        /// Список файлов грозди. Полные пути.
        /// </summary>
        private Dictionary<int, string> Files { get; set; }

        private List<AistReportEntity> functions = null;
        /// <summary>
        /// Список функций
        /// </summary>
        public List<AistReportEntity> Functions
        {
            get
            {
                return this.functions ?? (this.functions = AistReportsReader.AistReportsBunchReader.GetFunctions(this.DirectoryPath, this.Files));
            }
        }

        List<AistReportEntity> variables = null;
        /// <summary>
        /// Список переменных
        /// </summary>
        /// 
        public List<AistReportEntity> Variables
        {
            get
            {
                return this.variables ?? (this.variables = AistReportsReader.AistReportsBunchReader.GetVariables(this.DirectoryPath, this.Files));
            }
        }

        /// <summary>
        /// Имя грозди по АИСТ-С. Вычисляется на основе пути до "грозди".
        /// </summary>
        public string Name { get; private set; }

        private string shortName = null;
        /// <summary>
        /// Имя грозди только в части названия каталога (для ###_$__FOLDERNAME_New и для ###_FOLDERNAME вернётся FOLDERNAME) на основании BranchName. В случае ошибки - пустая строка.
        /// </summary>
        public string ShortName
        {
            get
            {
                if (this.shortName != null)
                    return this.shortName;

                if (!this.Name.Contains('_'))
                    return (this.shortNumber = String.Empty);

                string tmp = Name.Substring(4);

                if (tmp.Contains(@"$__") && tmp.EndsWith(@"_New"))
                    tmp = tmp.Remove(tmp.Length - 4).Substring(3);

                return (this.shortName = tmp);
            }
        }

        private string shortNumber = null;
        /// <summary>
        /// Имя грозди только в части номера (для ###_$__FOLDERNAME_New и для ###_FOLDERNAME вернётся ###) на основнии BranchName. В случае ошибки - пустая строка.
        /// </summary>
        public string ShortNumber
        {
            get
            {
                if (this.shortNumber != null)
                    return this.shortNumber;

                if (!Name.Contains('_'))
                    return (this.shortNumber = String.Empty);

                return (this.shortNumber = Name.Substring(0, Name.IndexOf('_')));
            }
        }

        /// <summary>
        /// Путь до каталога грозди по АИСТ-С
        /// </summary>
        private string filesPath = null;

        /// <summary>
        /// Путь до каталога грозди по АИСТ-С. Вычисляется автоматически, если заполнен filelist, как пересечение всех путей. В случае ошибки - пустая строка.
        /// </summary>
        public string SourcesDirectoryPath
        {
            get
            {
                return this.filesPath ?? (this.filesPath = PathController.GetCommonDirectoryPrefix(Files.Values, true));
            }
        }
        #endregion

        #region Методы
        #region Сериализация в XML
        /// <summary>
        /// Перевести ветвь в XML представление.
        /// </summary>
        /// <returns></returns>
        internal XElement Serialize()
        {
            XElement thisNode = new XElement("Bunch");
            XElement settingsNode = new XElement("Settings");
            XElement filesNode = new XElement("Files");
            XElement functionsNode = new XElement("Functions");
            XElement variablesNode = new XElement("Variables");

            if (DirectoryPath != null)
                settingsNode.Add(new XElement("BunchDir", DirectoryPath));

            filesNode.Add(new XAttribute("prefix", SourcesDirectoryPath));
            if (Files != null)
                foreach (var pair in Files)
                {
                    filesNode.Add(
                        new XElement("File", pair.Key + ";" + pair.Value.Substring(SourcesDirectoryPath.Length)));
                }

            if (functions != null)
                foreach (var entity in functions)
                {
                    functionsNode.Add(entity.ToXElement());
                }

            if (variables != null)
                foreach (var entity in variables)
                {
                    variablesNode.Add(entity.ToXElement());
                }

            thisNode.Add
                (
                    new XAttribute("Name", this.Name),
                    settingsNode,
                    filesNode,
                    functionsNode,
                    variablesNode
                );

            return thisNode;
        }

        /// <summary>
        /// Создать ветвь на основе XML представления.
        /// </summary>
        /// <param name="serialized"></param>
        /// <returns></returns>
        internal static AistBunch Deserialize(XElement serialized)
        {
            XElement settingsNode = serialized.Element("Settings");
            XElement filesNode = serialized.Element("Files");
            XElement functionsNode = serialized.Element("Functions");
            XElement variablesNode = serialized.Element("Variables");

            string dirPath = settingsNode.Element("BunchDir").Value;

            Dictionary<int, string> files = null;

            if (!filesNode.IsEmpty)
            {
                files = new Dictionary<int, string>();
                foreach (var file in filesNode.Elements())
                {
                    string[] splitted = file.Value.Split(';');
                    files.Add(int.Parse(splitted[0]), splitted[1]);
                }
            }

            AistBunch ret = new AistBunch(dirPath, files);

            if (!functionsNode.IsEmpty)
            {
                ret.functions = new List<AistReportEntity>();
                foreach (var entity in functionsNode.Elements())
                {
                    ret.functions.Add(AistReportEntity.FromXElement(entity));
                }
            }

            if (!variablesNode.IsEmpty)
            {
                ret.variables = new List<AistReportEntity>();
                foreach (var entity in variablesNode.Elements())
                {
                    ret.variables.Add(AistReportEntity.FromXElement(entity));
                }
            }

            return ret;
        }
        #endregion
        #endregion

    }

    /// <summary>
    /// Внутренний класс ошибки. Простое наследование.
    /// </summary>
    public class AistReportReaderException : Exception
    {
        /// <summary>
        /// Конструктор с сообщением.
        /// </summary>
        /// <param name="message">Сообщение для отображения.</param>
        public AistReportReaderException(string message)
            : base(message)
        { }

        /// <summary>
        /// Конструктор с сообщением и внутренней ошибкой.
        /// </summary>
        /// <param name="message">Сообщение для отображения.</param>
        /// <param name="inner">Внутренняя ошибка.</param>
        public AistReportReaderException(string message, Exception inner)
            : base(message, inner)
        { }
    }

}
