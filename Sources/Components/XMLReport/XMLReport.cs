﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace XMLReport
{
    public class XMLReport
    {
        /// <summary>
        /// Вывод массива данных в формате XML Spreadsheet, который открывается в MS Excel
        /// </summary>
        /// <param name="FileName">Имя XML файла</param>
        /// <param name="HeaderReport">Заголовок отчета</param>
        /// <param name="Data">Массив данных, первая строка - заголовки столбцов</param>
        public static void Report(string FileName, string HeaderReport, List<string[]> Data)
        {

            if (Data.Count == 0)
                return;
            // Определение полей, являющимися числами, размеров полей (для первых 1000 записей)
            int CellCnt = Data[0].Length;
            bool[] bint = new bool[CellCnt];
            int[] arrSize = new int[CellCnt];
            double itmp;
            if (Data.Count > 0)
            {
                for (int i = 0; i < CellCnt; i++)
                {
                    arrSize[i] = CalculateColumnWidth(Data[0][i], new Font("Arial Cyr", 10, FontStyle.Bold));
                    if (Data.Count > 1)
                    {
                        bint[i] = Data.Skip(1).Take(Math.Min(1000, Data.Count - 1)).Select(x => double.TryParse(x[i], out itmp)).Aggregate((a, b) => a & b);
                        arrSize[i] = Math.Max(Data.Skip(1).Take(Math.Min(1000, Data.Count - 1)).Select(x => CalculateColumnWidth(x[i], new Font("Arial Cyr", 10))).Max(), arrSize[i]);
                    }
                }
            }

            // Ограничение на заголовок Worksheet
            if (HeaderReport.Length > 30)
                HeaderReport = HeaderReport.Substring(0, 30);


            // Этот отчет генерируеться в файл XML Spreadsheet, который открывается в MS Excel.
            // Справочная информация содержится на http://msdn.microsoft.com/en-us/library/aa140066%28office.10%29.aspx

            string header = @"<?xml version='1.0'?><Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet'
                                xmlns:o='urn:schemas-microsoft-com:office:office'
                                xmlns:x='urn:schemas-microsoft-com:office:excel'
                                xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet'
                                xmlns:html='http://www.w3.org/TR/REC-html40'>
                                <ExcelWorkbook xmlns='urn:schemas-microsoft-com:office:excel'>
                                </ExcelWorkbook>";
            string styleDefault = @"<Styles>  <Style ss:ID='Default' ss:Name='Normal'>
                                <Alignment ss:Vertical='Bottom'/>
                                <Borders/>
                                <Font ss:FontName='Arial Cyr' x:CharSet='204'/>
                                <Interior/><NumberFormat/><Protection/></Style>";
            // Строка отвечающая за форматирование текста. 
            //У каждого стиля свой идентификатор, который используется для применения к конкретной ячейке.
            string styleOther = @"<Style ss:ID='s21'>
                                <Alignment ss:Horizontal='Center' ss:Vertical='Distributed'/>
                                <Font ss:FontName='Arial Cyr' x:CharSet='204' ss:Size='10' ss:Bold ='1'/>
                                </Style>
                                <Style ss:ID='s22'>
                                <Alignment ss:Horizontal='Center' ss:Vertical='Center'/>
                                <Font ss:FontName='Arial Cyr' x:CharSet='204' ss:Size='10'/>
                                </Style>
                                <Style ss:ID='s23'>
                                <Alignment ss:Horizontal='Left' ss:Vertical='Distributed'/>
                                <Font ss:FontName='Arial Cyr' x:CharSet='204' ss:Size='10'/>
                                </Style>
                                <Style ss:ID='s24'>
                                <Alignment ss:Horizontal='Right' ss:Vertical='Distributed'/>
                                <Font ss:FontName='Arial Cyr' x:CharSet='204' ss:Size='10'/>
                                </Style></Styles>";
            //Заполняем заголовок таблицы.
            string WorksheetBegin = @"<Worksheet ss:Name='" + HeaderReport + @"'>
                               <Table>";
            for (int i = 0; i < CellCnt; i++)
            {
                WorksheetBegin += @"<Column ss:AutoFitWidth='1' ss:Width='" + (arrSize[i]).ToString() + @"'/>" + Environment.NewLine;
            }

            WorksheetBegin += @"<Row>
                               <Cell ss:StyleID='s21'><Data ss:Type='String'>" + Data[0].Aggregate((a, b) => a + @"</Data></Cell>
                               <Cell ss:StyleID='s21'><Data ss:Type='String'>" + b) + @"</Data></Cell>
                               </Row>";
            string WorksheetEnd = @"</Table></Worksheet></Workbook>";


            System.IO.StreamWriter theWriter = null;
            try
            {
                theWriter = new System.IO.StreamWriter(FileName, false, Encoding.Unicode);
                theWriter.WriteLine(header);
                theWriter.WriteLine(styleDefault);
                theWriter.WriteLine(styleOther);
                theWriter.WriteLine(WorksheetBegin);

                for (int i = 1; i < Data.Count; i++)
                {
                    string ResultStr = @"<Row>" + Environment.NewLine;
                    for (int j = 0; j < bint.Length; j++)
                    {
                        if (bint[j])
                            ResultStr += @"<Cell ss:StyleID='s24'><Data ss:Type='Number'>" + Data[i][j] + @"</Data></Cell>" + Environment.NewLine;
                        else
                            ResultStr += @"<Cell ss:StyleID='s23'><Data ss:Type='String'>" + Data[i][j].Replace(">", "-").Replace("<", "-") + @"</Data></Cell>" + Environment.NewLine;
                    }
                    ResultStr += @"</Row>";
                    theWriter.WriteLine(ResultStr);

                }
                theWriter.WriteLine(WorksheetEnd);
            }
            finally
            {
                theWriter.Close();
            }
        }
        private static int CalculateColumnWidth(string s, Font font)
        {
            return Math.Min((int)Math.Truncate(Graphics.FromImage(new Bitmap(100, 100)).MeasureString(s, font).Width),680);
        } 
    }
}
