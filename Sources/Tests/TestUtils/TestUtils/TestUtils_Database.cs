﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace TestUtils
{
    /// <summary>
    /// Часть класса инфраструктуры тестирования. Содержит статические методы для работы с базой данных.
    /// </summary>
    public partial class TestUtilsClass
    {
        /// <summary>
        /// Установить соединение с базой данных. Информация о параметрах соединения хранится в файле настроек тестового окружения.
        /// </summary>
        //public static void SQL_SetupConnection()
        //{
        //    //Если соединение уже установлено, ничего не делаем (проверяем на БД "ia");
        //    if ((new IA.Sql.DatabaseConnection("ia")).TestConnection())
        //        return;

        //    var SqlConnection = XDocument.Load(TestRunConfigurationFileName).Descendants("SQLconnection").First();
        //    var Server = SqlConnection.Descendants("Server").First();
        //    var ServerName = Server.FirstAttribute.Value;
        //    var InstanceName = Server.LastAttribute.Value;

        //    var SQLUser = SqlConnection.Descendants("SQLUser").First();
        //    var UserName = SQLUser.FirstAttribute.Value;
        //    var Password = SQLUser.LastAttribute.Value;

        //    IA.Sql.ServerConfiguration.Set(ServerName + "\\" + InstanceName, UserName, Password, false);
        //}

        /// <summary>
        /// Установить соединение с базой данных. Информация о параметрах соединения хранится в файле настроек тестового окружения.
        /// </summary>
        public static void SQL_SetupTestConnection()
        {

            string TestRunConfigurationFileName = System.IO.Path.GetFullPath(System.IO.Path.Combine(GetWorkingDirectory(), TestRunConfigurationFileNameRelativePath));

            var SqlConnection = XDocument.Load(TestRunConfigurationFileName).Descendants("SQLTestconnection").First();
            var Server = SqlConnection.Descendants("TestServer").First();
            var ServerName = Server.FirstAttribute.Value;
            var InstanceName = Server.LastAttribute.Value;

            var SQLUser = SqlConnection.Descendants("SQLUser").First();
            var UserName = SQLUser.FirstAttribute.Value;
            var Password = SQLUser.LastAttribute.Value;
            if(string.IsNullOrWhiteSpace(InstanceName))
                IA.Sql.ServerConfiguration.Set(ServerName, UserName, Password, false);
            else
                IA.Sql.ServerConfiguration.Set(ServerName + "\\" + InstanceName, UserName, Password, false);
        }


        public static void SetSqlTestData()
        {
            IA.Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection mSql = new IA.Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection();
            mSql.InsertDllToBase(new string[] { "Dll001.dll", "RntTest", "Temp record in database for tests", "", "1.1.", "1" }, null, new List<string[]> { new string[] { "Func001", "test func", "1", "1" }, new string[] { "Func002", "test func", "2", "1" }, new string[] { "Func003", "test func", "3", "1" }, }, null);
            mSql.InsertDllToBase(new string[] { "Dll002.dll", "RntTest", "Temp record in database for tests", "", "1.1.", "1" }, null, new List<string[]> { new string[] { "Func001", "test func", "1", "1" }, new string[] { "Func002", "test func", "2", "1" }, new string[] { "Func003", "test func", "3", "1" }, }, null);
            mSql.InsertDllToBase(new string[] { "Dll003.dll", "RntTest", "Temp record in database for tests", "", "1.1.", "1" }, null, new List<string[]> { new string[] { "Func001", "test func", "1", "1" }, new string[] { "Func002", "test func", "2", "1" }, new string[] { "Func003", "test func", "3", "1" }, }, null);
        }
        public static void RemoveSqlTestData()
        {
            IA.Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection mSql = new IA.Sql.DatabaseConnections.DllKnowledgeBaseDatabaseConnection();
            foreach (int num in mSql.DllBase.Where(x => new string[] { "Dll001.dll", "Dll002.dll", "Dll003.dll" }.Contains(x[1])).Select(x => int.Parse(x[0])).Distinct())
            {
                mSql.DeleteDllFromBase(num);
            }
        }

    }
}
