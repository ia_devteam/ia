﻿using System.IO;

namespace TestUtils
{
    /// <summary>
    /// Часть класса инфраструктуры тестирования. Содержит статические методы для работы с файлами.
    /// </summary>
    public partial class TestUtilsClass
    {
        /// <summary>
        /// Создать копию файла с заменой указанной подстроки
        /// </summary>
        /// <param name="originalFilePath">Путь к оригинальному файлу</param>
        /// <param name="copyFilePath">Путь к файлу, в который выполняется копирование</param>
        /// <param name="searchString">Заменяемая строка</param>
        /// <param name="replaceString">Заменяющая строка</param>
        public static void File_CopyAndReplace(string originalFilePath, string copyFilePath, string searchString, string replaceString)
        {
            using (StreamReader reader = new StreamReader(originalFilePath))
            using (StreamWriter writer = new StreamWriter(copyFilePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    writer.WriteLine(line.Replace(searchString.ToLower(), replaceString).Replace(searchString, replaceString));
            }
        }
    }
}
