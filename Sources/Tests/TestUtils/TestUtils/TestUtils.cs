﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Store;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

using IOController;

namespace TestUtils
{
    /// <summary>
    /// Часть класса инфраструктуры тестирования. Содержит основные методы для тестирования плагинов.
    /// </summary>
    public partial class TestUtilsClass
    {
        /// <summary>
        /// Относительный путь к неизменяемым материалам для тестов
        /// </summary>
        const string TestMaterialsDirectoryRelativePath = "../../Tests/Materials";

        /// <summary>
        /// Относительный путь файла, из которого читаем настройки окружения при тестировании
        /// </summary>
        const string TestRunConfigurationFileNameRelativePath = "../../Tests/Materials/TestRunConfiguration.xml";

        /// <summary>
        /// Относительный путь к временным папкам, в которых производится тестирование
        /// </summary>
        const string TestRunDirectoryRelativePath = "../../Tests/RunTemp";

        /// <summary>
        /// Путь к кэшу Хранилищ
        /// </summary>
        const string CacheDirectoryRelativePath = "../../Tests/Cache";

        /// <summary>
        /// УСТАРЕЛО!!! Константа, на которую заменяется локальный путь в эталонных файлах. Требуется для того, чтобы тестировать по разным путям.
        /// </summary>
        [Obsolete("Use PathConstant instead!")]
        public const string PathConstant_old = "$$##@@!!RNT";

        /// <summary>
        /// Константа, на которую заменяется локальный путь в эталонных файлах. Требуется для того, чтобы тестировать по разным путям.
        /// </summary>
        public const string PathConstant = "@#$%^&*";

        /// <summary>
        /// Константа, необходимая для формирования "уникального" пути к отчётам выполняемых плагинов.
        /// </summary>
        public const string ReportsDirectoryConstant = "$$##@@!!RNT_Reports";

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="testUniqName">Уникальное имя теста</param>
        public TestUtilsClass(string testUniqName)
        {
            string currentDirectory = GetWorkingDirectory();

            this.TestName = testUniqName;
            this.TestMatirialsDirectoryPath = Path.GetFullPath(Path.Combine(currentDirectory, TestMaterialsDirectoryRelativePath));
            this.TestRunConfigurationFileName = Path.GetFullPath(Path.Combine(currentDirectory, TestRunConfigurationFileNameRelativePath));
            this.TestRunDirectoryPath = Path.GetFullPath(Path.Combine(currentDirectory, TestRunDirectoryRelativePath, testUniqName));
            this.TestTempStorageCopyDirectoryPath = Path.GetFullPath(Path.Combine(currentDirectory, TestRunDirectoryRelativePath, testUniqName + "_temp"));
            this.TestCachedStorageDirectoryPath = Path.GetFullPath(Path.Combine(currentDirectory, CacheDirectoryRelativePath, testUniqName));

            //Очищаем рабочую деректорию теста, гарантируя при этом её наличие на жёстком диске
            if (Directory.Exists(this.TestRunDirectoryPath))
                DirectoryController.Clear(this.TestRunDirectoryPath, true);
            else
                Directory.CreateDirectory(this.TestRunDirectoryPath);
        }

        /// <summary>
        /// Получить текущую директорию, относитеьлно которой строить выполнение тестов
        /// </summary>
        /// <returns></returns>
        private static string GetWorkingDirectory()
        {
            string currentDirectory = Environment.CurrentDirectory;
            if (currentDirectory.Contains("TestResults"))
                currentDirectory = Path.Combine(currentDirectory, "../..");
            return currentDirectory;
        }

        #region Инфраструктура запуска тестов плагинов целиком

        #region Делегаты
        /// <summary>
        /// Выполнить проверку содержимого Хранилища после отработки теста
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="testMaterialsDirectoryPath">Путь к постоянным материалам тестирования</param>
        /// <returns></returns>
        public delegate bool CheckStorage(Storage storage, string testMaterialsDirectoryPath);

        /// <summary>
        /// Проверить правильность сгенерированных отчётов
        /// </summary>
        /// <param name="reportsDirectoryPath">Путь к сгенерированным отчётам</param>
        /// <param name="testMaterialsDirectoryPath">Путь к постоянным материалам тестирования</param>
        /// <returns></returns>
        public delegate bool CheckReports(string reportsDirectoryPath, string testMaterialsDirectoryPath);
        
        /// <summary>
        /// Изменить данные в Хранилище перед запуском теста
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="testMaterialsDirectoryPath">Путь к постоянным материалам тестирования</param>
        public delegate void CustomizeStorage(Storage storage, string testMaterialsDirectoryPath);
        #endregion

        #region Свойства
        /// <summary>
        /// Имя теста
        /// </summary>
        public string TestName { get; private set; }

        /// <summary>
        /// Путь до директории с постоянными материалами тестирования
        /// </summary>
        public string TestMatirialsDirectoryPath { get; private set; }

        /// <summary>
        /// Путь до директории, с которой взаимодействует тест в процессе работы
        /// </summary>
        public string TestRunDirectoryPath { get; private set; }

        /// <summary>
        /// Путь файла, из которого читаем настройки окружения при тестировании
        /// </summary>
        public string TestRunConfigurationFileName { get; private set; }

        /// <summary>
        /// Временная директория теста
        /// </summary>
        private string TestTempStorageCopyDirectoryPath { get; set; }

        /// <summary>
        /// Путь до директории с кэшированным Хранилищем
        /// </summary>
        private string TestCachedStorageDirectoryPath { get; set; }
        #endregion

        /// <summary>
        /// Метод автоматизации тестирования одного выделенного плагина. 
        /// Применение совместно с вспомогательным классом конфигурации. 
        /// Данная перегрузка служит для случая, когда у вспомогательного класса в конструктор необходимо передавать параметры.
        /// </summary>
        /// <param name="config">Экземпляр вспомогательного класса.</param>
        public void RunTest(RunTestConfig config)
        {
            var t = config;

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsNotNull(t.Stage01SourcesPostfix, "При запуске теста не задано значение Stage01SourcesPostfix.");
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsNotNull(t.Stage02Plugin, "При запуске теста не задан экземпляр плагина.");

            RunTest(
                t.Stage01SourcesPostfix, t.Stage02Plugin, t.Stage03IsUseCachedStorage, t.Stage04PrepareStorageDelegate,
                t.Stage11PrepareStorageDelegate, t.Stage12CheckStorageDelegate, t.Stage13CheckReportsAfterRunDelegate,
                t.Stage21IsRunTwice, t.Stage22PrepareStorageForReRunDelegate, t.Stage23CheckStorageAfterReRunDelegate, t.Stage24CheckReportsAfterReRunDelegate,
                t.Stage000IsSettingsCorrectChecking
                );
        }

        /// <summary>
        /// Метод автоматизации тестирования одного выделенного плагина. 
        /// Применение совместно с вспомогательным классом конфигурации. 
        /// Данная перегрузка используется отсутствии параметров у конструктора вспомогательного класса.
        /// </summary>
        /// <typeparam name="T">Класс тестирования, описывающий необходимые действия.</typeparam>
        public void RunTest<T>() where T: RunTestConfig, new()
        {
            var t = new T();

            RunTest(t);
        }

        /// <summary>
        /// Метод автоматизации тестирования одного выделенного плагина. 
        /// Применение совместно с вспомогательным классом конфигурации. 
        /// Данная перегрузка используется отсутствии параметров у конструктора вспомогательного класса и отсутствии необходимости в передаче конкретного экземпляра плагина.
        /// </summary>
        /// <typeparam name="P">Класс тестируемого плагина. Создаётся автоматически.</typeparam>
        /// <typeparam name="T">Класс тестирования, описывающий необходимые действия.</typeparam>
        public void RunTest<P, T>() where P: class, IA.Plugin.Interface, new() where T: RunTestConfig,new()
        {
            var plugin = new P();
            var t = new T();

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsNotNull(t.Stage01SourcesPostfix, "При запуске теста не задано значение Stage01SourcesPostfix.");

            RunTest(
                t.Stage01SourcesPostfix, t.Stage02Plugin ?? plugin, t.Stage03IsUseCachedStorage, t.Stage04PrepareStorageDelegate,
                t.Stage11PrepareStorageDelegate, t.Stage12CheckStorageDelegate, t.Stage13CheckReportsAfterRunDelegate,
                t.Stage21IsRunTwice, t.Stage22PrepareStorageForReRunDelegate, t.Stage23CheckStorageAfterReRunDelegate, t.Stage24CheckReportsAfterReRunDelegate,
                t.Stage000IsSettingsCorrectChecking
                );
        }

        /// <summary>
        /// Метод для автоматизации тестирования одного выделенного плагина. Позволяет тестировать плагин как на новосгенерированном Хранилище, так и содержать Хранилище в КЭШе.
        /// 
        /// </summary>
        /// <param name="sourcesPostfix">Костыль для подмены путей к исходникам - в ходе открытия хранилища путь подменяется на временный, этим параметром он устанавливается в указанный в
        /// в нем. Должен полностью соответствовать пути FillFileList, если тот запускается в ходе формирования хранилища. Часть пути к исходникам в неизменяемых материалах. Например, если исходники лежат в папке e:\IATFS\Tests\Materials\Sources\JavaScript\, 
        ///                                 то в данном параметре должно быть Sources\JavaScript. Используется для установки префикса к файлам при работе раздела Files Хранилища </param>
        /// <param name="plugin">Ссылка на экземпляр плагина. Ни один метод данного экземпляра не должен быть вызван (кроме конструктора, разумеется)</param>
        /// <param name="isUseCachedStorage">Используется ли для данного плагина КЭШ Хранилищ</param>
        /// <param name="prepareStorage">Имеет смысл, только если isUseCachedStorage=true. Вызывается для подготовки Хранилища до его помещения в КЭШ. Вызывается, только если требуется обновить или создать КЭШ</param>
        /// <param name="customizeStorage">Вызывается для донастройки Хранилища после его вытаскивания из КЭШа и когда КЭШ не используется</param>
        /// <param name="checkStorage">Вызывается после отработки плагина для проверки того, что плагин корректно отработал с Хранилищем</param>
        /// <param name="checkReportsBeforeRerun">Вызывается после генерации отчетов для проверки сгенерированных отчётов</param>
        /// <param name="isRunTwice">true, если тесту требуется выполнить донастройку Хранилища и его перезапуск плагина</param>
        /// <param name="changeSettingsBeforRerun">вызывается, только если isRunTwice = true. Выполнить донастройку Хранилища и окружения до перезапуска плагина</param>
        /// <param name="checkStorageAfterRerun">вызывается, только если isRunTwice = true. Вызывается для проверки содержимого Хранилища после перезапуска плагина</param>
        /// <param name="checkReportsAfterRerun">вызывается, только если isRunTwice = true. Вызывается после генерации отчетов по результатам перезапуска плагина</param>
        public void RunTest(string sourcesPostfix, IA.Plugin.Interface plugin, bool isUseCachedStorage, CustomizeStorage prepareStorage, CustomizeStorage customizeStorage,
            CheckStorage checkStorage, CheckReports checkReportsBeforeRerun, bool isRunTwice, CustomizeStorage changeSettingsBeforRerun, CheckStorage checkStorageAfterRerun,
            CheckReports checkReportsAfterRerun, bool isSettingsCorrectChecking = false)
        {
            string execPath = (new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
            execPath = execPath.Substring(0, execPath.IndexOf("TestUtils.DLL"));
            System.IO.Directory.SetCurrentDirectory(execPath);

            //Некоторые плагины работают с серверными БД. Настраиваем соединение
            SQL_SetupTestConnection();

            //Получаем путь к исходным тестам, с которыми (возможно) будет вестиль работа. Применяется для задания префикса в Хранилище.
            string testSourcesDirectoryPath = Path.Combine(this.TestMatirialsDirectoryPath, sourcesPostfix);

            Storage storage = null;

            //Смотрим, надо ли работать с КЭШем. В зависимости от генерируем Хранилище и выполняем
            if (isUseCachedStorage) //Пользователь указал, что работа с КЭШем присутствует
            {
                if (XDocument.Load(TestRunConfigurationFileName).Descendants("isRefreshCache").First().Value.ToLower().Contains("true")) //Настройки тестирования заставляют перегенерировать КЭШ
                {
                    IARunTestException message = null;
                    try
                    {
                        storage = GenerateStorageWithCopy(prepareStorage, testSourcesDirectoryPath);
                    }
                    catch (Exception ex)
                    {
                        message = new IARunTestException("Исключение при перегенерировании Хранилища: " + ex.Message, ex);
                    }

                    if (message == null) //Если генерация Хранилища удалась
                    {
                        //Прогоняем тест на новом Хранилище
                        try
                        {
                            RunTest(plugin, customizeStorage, checkStorage, checkReportsBeforeRerun, isRunTwice, changeSettingsBeforRerun, checkStorageAfterRerun, checkReportsAfterRerun,
                              this.TestMatirialsDirectoryPath, storage, isSettingsCorrectChecking);
                        }
                        catch (IARunTestException ex)
                        {
                            message = new IARunTestException("Исключение при закпуске плагина на перегенерированном Хранилище: " + ex.Message, ex);
                        }

                        //Тестирование Хранилища закончилось. Закрываем его
                        storage.Close();

                        if (message == null)
                        {
                            //Тест удался. Надо занести новое Хранилище в КЭШ.
                            SaveStorageCopyToCache();
                        }
                        else
                            //Если на новом Хранилище отработал с ошибкой, то резервную копию Хранилища удаляем как мусор
                            DirectoryController.Remove(this.TestTempStorageCopyDirectoryPath, true);
                    }

                    if (message != null) //Либо не удалось инициировать Хранилище, либо запуск теста на новом Хранилище провалился
                    {
                        //На вновь сгенерированном Хранилище тест падает. Пытаемся взять Хранилище из КЭШа и попробовать на нём.
                        if ((storage = OpenCachedStorage()) == null)
                            throw new IARunTestException("В КЭШе Хранилище отсутствует" + message, message);  //Прерываем работу теста

                        //Хранилище взято из КЭШа
                        //В КЭШе могут быть другие префиксы. Правим.
                        try
                        {
                            Storage_ChangeFolderPrefix(storage, testSourcesDirectoryPath);
                        }
                        catch (Exception ex)
                        {
                            //Принудительно закрываем Хранилище
                            if (!storage.isClosed)
                                storage.Close();

                            throw new IARunTestException("Исключение при запуске ChangeFolderPrefix: " + ex.Message + " Тест не проходит на сгенерированном Хранилище " + message.Message, message);   //Прерываем работу теста
                        }

                        //Запускаем тест
                        try
                        {
                            RunTest(plugin, customizeStorage, checkStorage, checkReportsBeforeRerun, isRunTwice, changeSettingsBeforRerun, checkStorageAfterRerun, checkReportsAfterRerun, this.TestMatirialsDirectoryPath, storage, isSettingsCorrectChecking);
                        }
                        catch (IARunTestException ex)
                        {
                            //Принудительно закрываем Хранилище
                            if (!storage.isClosed)
                                storage.Close();

                            throw new IARunTestException("Тест не проходит на Хранилище, размещённом в КЭШе. " + ex.Message + " Тест не проходит на сгенерированном Хранилище " + message.Message, message);   //Прерываем работу теста
                        }

                        //В любом случае прерываем работу теста, так как он не прошел на сгенерированном Хранилище

                        //Принудительно закрываем Хранилище
                        if (!storage.isClosed)
                            storage.Close();

                        throw new IARunTestException("Тест проходит на Хранилище из КЭШа. Но тест не проходит на сгенерированном Хранилище " + message, message);   //Прерываем работу теста
                    }
                }
                else
                {
                    //Пользуемся КЭШем, но перегенерирование настройками не затребовано
                    if ((storage = OpenCachedStorage()) == null)
                    {
                        //Перегенерируем Хранилище для КЭШа, так как его там нет
                        try
                        {
                            storage = GenerateStorageWithCopy(prepareStorage, testSourcesDirectoryPath);
                        }
                        catch (Exception ex)
                        {
                            //Принудительно закрываем Хранилище
                            if (storage != null && !storage.isClosed)
                                storage.Close();

                            throw new IARunTestException("Хранилища в КЭШе не было. Исключение при перегенерировании Хранилища: " + ex.Message, ex);    //Прерываем работу теста
                        }

                        try
                        {
                            RunTest(plugin, customizeStorage, checkStorage, checkReportsBeforeRerun, isRunTwice, changeSettingsBeforRerun, checkStorageAfterRerun, checkReportsAfterRerun, this.TestMatirialsDirectoryPath, storage, isSettingsCorrectChecking);
                        }
                        catch (IARunTestException ex)
                        {
                            //Принудительно закрываем Хранилище
                            if (!storage.isClosed)
                                storage.Close();

                            DirectoryController.Remove(this.TestRunDirectoryPath, true);

                            throw new IARunTestException("Хранилища в КЭШе не было. Исключение при работе на перегенерированном Хранилище: " + ex.Message, ex);    //Прерываем работу теста
                        }

                        //Закрываем Хранилище
                        storage.Close();

                        //Помещаем Хранилище в КЭШ
                        SaveStorageCopyToCache();
                    }
                    else //Хранилище открылось
                    {
                        //В КЭШе могут быть другие префиксы. Правим.
                        try
                        {
                            Storage_ChangeFolderPrefix(storage, testSourcesDirectoryPath);
                        }
                        catch (Exception ex)
                        {
                            //Принудительно закрываем Хранилище
                            if (!storage.isClosed)
                                storage.Close();

                            throw new IARunTestException("Работа на Хранилище из КЭШа. Исключение при изменении префиксов в Хранилище: " + ex.Message, ex);    //Прерываем работу теста
                        }

                        try
                        {
                            RunTest(plugin, customizeStorage, checkStorage, checkReportsBeforeRerun, isRunTwice, changeSettingsBeforRerun, checkStorageAfterRerun, checkReportsAfterRerun, this.TestMatirialsDirectoryPath, storage, isSettingsCorrectChecking);
                        }
                        catch (IARunTestException ex)
                        {
                            //Принудительно закрываем Хранилище
                            if (!storage.isClosed)
                                storage.Close();

                            throw new IARunTestException("Работа на Хранилище из КЭШа. Исключение при выполнении теста: " + ex.Message, ex);    //Прерываем работу теста
                        }

                        //Закрываем Хранилище
                        storage.Close();
                    }
                }
            }
            else //Пользователь указал, что работа с КЭШем отсутствует
            {
                //Создаём Хранилище
                storage = Storage_CreateAndOpen(this.TestRunDirectoryPath);

                //Устанавливаем префикс
                Storage_ChangeFolderPrefix(storage, testSourcesDirectoryPath);

                //Запускаем плагин
                try
                {
                    RunTest(plugin, customizeStorage, checkStorage, checkReportsBeforeRerun, isRunTwice, changeSettingsBeforRerun, checkStorageAfterRerun, checkReportsAfterRerun, this.TestMatirialsDirectoryPath, storage, isSettingsCorrectChecking);
                }
                catch (IARunTestException ex)
                {
                    //Принудительно закрываем Хранилище
                    if (!storage.isClosed)
                        storage.Close();

                    throw new IARunTestException("Работа без КЭШа. Исключение при выполнении теста: " + ex.Message, ex);    //Прерываем работу теста
                }

                //Закрываем Хранилище
                storage.Close();
            }

            //Если мы дошли до конца, то тест прошёл нормально, можно удалять сгенерированную в ходе теста информацию
            DirectoryController.Remove(this.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// Создаёт новое Хранилище. Заполняет его, вызвав пользовательскую функцию <c>prepareStorage</c>. Затем Хранилище архивируется. После этого открытое Хранилище возвращается пользователю.
        /// </summary>
        /// <param name="prepareStorage">Функция заполнения Хранилища</param>
        /// <param name="testSourcesDirectoryPath">Часть пути к исходникам в неизменяемых материалах. Например, если исходники лежат в папке e:\IATFS\Tests\Materials\Sources\JavaScript\, 
        ///                                 то в данном параметре должно быть Sources\JavaScript </param>
        /// <returns>Открытое Хранилище. Если в процессе обнаруживается проблема, то выдаётся исключение</returns>
        private Storage GenerateStorageWithCopy(CustomizeStorage prepareStorage, string testSourcesDirectoryPath)
        {
            //Создаём Хранилище
            Storage storage = Storage_CreateAndOpen(this.TestRunDirectoryPath);

            //Выставляем префикс, иначе будет префикс по умолчанию, ведущий в Хранилище
            Storage_ChangeFolderPrefix(storage, testSourcesDirectoryPath);

            //Заполняем Хранилище
            try
            {
                prepareStorage(storage, this.TestMatirialsDirectoryPath);
            }
            catch (Exception ex)
            {
                throw new IARunTestException("При вызове функции prepareStorage возникло исключение: " + ex.Message, ex);
            }

            //Закрываем Хранилище
            storage.Close();

            //Временно сохраняем сгенерированное Хранилище
            DirectoryController.Copy(this.TestRunDirectoryPath, this.TestTempStorageCopyDirectoryPath);

            //Снова открываем Хранилище, чтобы запустить на нём тест
            storage = new Storage();
            storage.Open(this.TestRunDirectoryPath, Storage.OpenMode.OPEN);

            //Выставляем префикс, иначе будет префикс по умолчанию, ведущий в Хранилище
            //Вроде бы этот вызов избыточный и дублирует вызов выше. Однако префикс имеет свойство восстанавливаться. Повторяется для уверенности.
            Storage_ChangeFolderPrefix(storage, testSourcesDirectoryPath);

            return storage;
        }

        /// <summary>
        /// Прогнать тестирование на открытом Хранилище. Параметры аналогичны параметрам <c>RunTest</c>
        /// </summary>
        /// <param name="plugin"></param>
        /// <param name="customizeStorage"></param>
        /// <param name="checkStorage"></param>
        /// <param name="checkReportBeforRerun"></param>
        /// <param name="isUpdateReport"></param>
        /// <param name="changeSettingsBeforRerun"></param>
        /// <param name="checkStorageAfterRerun"></param>
        /// <param name="checkReportAfterRerun"></param>
        /// <param name="testMatirialsFolder"></param>
        /// <param name="storage"></param>
        /// <param name="isSettingsCorrectChecking"></param>
        private void RunTest(IA.Plugin.Interface plugin, CustomizeStorage customizeStorage, CheckStorage checkStorage, CheckReports checkReportBeforRerun, bool isUpdateReport,
            CustomizeStorage changeSettingsBeforRerun, CheckStorage checkStorageAfterRerun, CheckReports checkReportAfterRerun, string testMatirialsFolder, Storage storage, bool isSettingsCorrectChecking)
        {
            //Донастраиваем Хранилище
            try
            {
                customizeStorage(storage, testMatirialsFolder);
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове customizeStorage: " + ex.Message);
            }

            //Инициализируем плагин
            try
            {
                plugin.Initialize(storage);
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове plugin.Initialize: " + ex.Message);
            }

            //Получаем настройки плагина из Хранилища
            try
            {
                Store.Table.IBufferReader settingsBuffer = storage.pluginSettings.LoadSettings(plugin.ID);

                if (settingsBuffer != null)
                    plugin.LoadSettings(settingsBuffer);
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове plugin.LoadSettings(IBufferReader settingsBuffer): " + ex.Message);
            }

            //Проверяем настройки плагина
            string message;
            bool res = false;
            try
            {
                res = plugin.IsSettingsCorrect(out message);
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове plugin.IsSettingsCorrect: " + ex.Message);
            }

            if (res == false)
            {
                if (isSettingsCorrectChecking)
                {
                    IA.Monitor.Log.Error(plugin.Name, message);
                    return;
                }
                else
                {
                    throw new IARunTestException("Ошибка при вызове plugin.IsSettingsCorrect: " + message);
                }
            }

            //Проверяем, что плагин сгенерирует задачи
            try
            {
                List<IA.Monitor.Tasks.Task> tasks = plugin.Tasks;
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове plugin.Tasks: " + ex.Message);
            }

            //Запускаем плагин
            try
            {

                plugin.Run();
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове plugin.Run: " + ex.Message);
            }

            // Сохраняем результаты работы
            try
            {
                plugin.SaveResults();
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове plugin.SaveResults: " + ex.Message);
            }

            //Проверяем отработку плагина
            try
            {
                res = checkStorage(storage, testMatirialsFolder);
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове checkStorage: " + ex.Message);
            }
            if (!res)
                throw new IARunTestException("checkStorage вернул false. ");

            //Получаем путь до директории для генерации отчётов по плагину
            string pluginReportsDirectoryPath = Path.Combine(this.TestRunDirectoryPath, TestUtilsClass.ReportsDirectoryConstant, plugin.ID.ToString());

            //Если директория для генерации отчётов уже существует, очищаем её, если нет, то создаём
            if (Directory.Exists(pluginReportsDirectoryPath))
                DirectoryController.Clear(pluginReportsDirectoryPath, true);
            else
                Directory.CreateDirectory(pluginReportsDirectoryPath);

            //Запускаем построение отчётов
            try
            {
                plugin.GenerateReports(pluginReportsDirectoryPath);
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове plugin.GenerateReports: " + ex.Message);
            }

            //Проверить отчёт
            try
            {
                res = checkReportBeforRerun(pluginReportsDirectoryPath, testMatirialsFolder);
            }
            catch (Exception ex)
            {
                throw new IARunTestException("Исключение при вызове checkReportBeforRerun: " + ex.Message);
            }
            if (!res)
                throw new IARunTestException("checkReportBeforRerun вернул false. ");

            //Если надо, повторяем запуск
            if (isUpdateReport)
            {
                //Меняем настройки
                try
                {
                    changeSettingsBeforRerun(storage, testMatirialsFolder);
                }
                catch (Exception ex)
                {
                    throw new IARunTestException("Исключение при вызове changeSettingsBeforRerun: " + ex.Message);
                }

                //Повторяем запуск
                try
                {
                    res = plugin.IsSettingsCorrect(out message);
                }
                catch (Exception ex)
                {
                    throw new IARunTestException("Исключение при вызове IsSettingsCorrect при повторном запуске: " + ex.Message);
                }
                if (!res)
                    throw new IARunTestException("plugin.IsSettingsCorrect вернул false: " + message);

                //Запускаем плагин
                try
                {
                    plugin.Run();
                }
                catch (Exception ex)
                {
                    throw new IARunTestException("Исключение при вызове plugin.Run при повторном запуске: " + ex.Message);
                }

                // Сохраняем результаты работы
                try
                {
                    plugin.SaveResults();
                }
                catch (Exception ex)
                {
                    throw new IARunTestException("Исключение при вызове plugin.SaveResults при повторном запуске: " + ex.Message);
                }

                //Проверяем результат
                try
                {
                    res = checkStorageAfterRerun(storage, testMatirialsFolder);
                }
                catch (Exception ex)
                {
                    throw new IARunTestException("Исключение при вызове checkReportAfterRerun при повторном запуске: " + ex.Message);
                }
                if (!res)
                    throw new IARunTestException("checkStorageAfterRerun вернул false: " + message);

                //Очищаем созданную при первом запуске директорию для генерации отчётов
                DirectoryController.Clear(pluginReportsDirectoryPath, true);

                //Генерируем отчёты
                try
                {
                    plugin.GenerateReports(pluginReportsDirectoryPath);
                }
                catch (Exception ex)
                {
                    throw new IARunTestException("Исключение при вызове plugin.GenerateReports при повторном запуске: " + ex.Message);
                }

                //Проверяем результат
                try
                {
                    res = checkReportAfterRerun(pluginReportsDirectoryPath, testMatirialsFolder);
                }
                catch (Exception ex)
                {
                    throw new IARunTestException("Исключение при вызове checkReportAfterRerun при повторном запуске: " + ex.Message);
                }
                if (!res)
                    throw new IARunTestException("checkReportAfterRerun вернул false: " + message);
            }

        }

        /// <summary>
        /// Сохраняем дубликат Хранилища в КЭШ
        /// </summary>
        private void SaveStorageCopyToCache()
        {
            //Очищаем папку со старым кэшированным Хранилищем
            if (Directory.Exists(this.TestCachedStorageDirectoryPath))
                DirectoryController.Clear(this.TestCachedStorageDirectoryPath, true);

            //Основное копирование
            DirectoryController.Copy(this.TestTempStorageCopyDirectoryPath, this.TestCachedStorageDirectoryPath);

            //Удаляем мусор
            DirectoryController.Remove(this.TestTempStorageCopyDirectoryPath, true);
        }

        /// <summary>
        /// Открыть Хранилище, размещённое к кэше.
        /// При этом используется локальный кэш.
        /// Хранилище копируется в локальную папку выполнения теста и открывается только там.
        /// </summary>
        /// <returns></returns>
        private Storage OpenCachedStorage()
        {
            Storage storage = null;

            try
            {
                //Копируем в рабочую папку
                DirectoryController.Copy(this.TestCachedStorageDirectoryPath, this.TestRunDirectoryPath);

                //Открываем скопированное Хранилище
                storage = new Storage();
                storage.Open(this.TestRunDirectoryPath, Storage.OpenMode.OPEN);
            }
            catch
            {
                return null;
            }

            return storage;
        }

        #endregion

        #region Ошибки выполнения теста
        /// <summary>
        /// Ошибка при выполнении запуска TestUtils.TestUtilsClass.RunTest().
        /// </summary>
        public class IARunTestException : Exception
        {
            public IARunTestException() : base() { }
            public IARunTestException(string message) : base(message) { }
            public IARunTestException(string message, Exception inner) : base(message, inner) { }
        }
        #endregion

        #region Операции над Хранилищем
        /// <summary>
        /// Меняет префикс путей к директории с исходными текстами в Хранилище
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="prefix">Новый префикс</param>
        public void Storage_ChangeFolderPrefix(Storage storage, string prefix)
        {
            storage.appliedSettings.SetFileListPath(prefix);
        }

        /// <summary>
        /// Создать и открыть Хранилище
        /// </summary>
        /// <param name="postfix">Постфикс в рамках рабочей директории теста</param>
        /// <returns>Открытое Хранилище</returns>
        public Storage Storage_CreateAndOpen(string postfix = null)
        {
            //Инициализируем Хранилище
            Storage storage = new Storage();

            //Получаем путь до будующей рабочей директории Хранилища
            string storageWorkDirectoryPath = postfix == null ? this.TestRunDirectoryPath : Path.Combine(this.TestRunDirectoryPath, postfix);

            //Принудительно создаём рабочую директорию
            if (!Directory.Exists(storageWorkDirectoryPath))
                Directory.CreateDirectory(storageWorkDirectoryPath);

            //Создаём Хранилище
            storage.Open(storageWorkDirectoryPath, Storage.OpenMode.CREATE);

            return storage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="postfix"></param>
        /// <returns></returns>
        public Storage Storage_Open(string postfix = null)
        {
            //Инициализируем Хранилище
            Storage storage = new Storage();

            //Получаем путь до будующей рабочей директории Хранилища
            string storageWorkDirectoryPath = postfix == null ? this.TestRunDirectoryPath : Path.Combine(this.TestRunDirectoryPath, postfix);

            //Создаём Хранилище
            storage.Open(storageWorkDirectoryPath, Storage.OpenMode.OPEN);

            return storage;
        }

        /// <summary>
        /// Закрыть открытое Хранилище и открыть повторно.
        /// </summary>
        /// <param name="storage">Хранилище</param>
        public void Storage_Reopen(ref Storage storage)
        {
            if (storage == null)
                throw new IARunTestException("Хранилище не определено.");

            //Получаем путь до рабочей директории Хранилища
            string storageWorkDirectoryPath = storage.WorkDirectory.Path;

            //Принудительно закрываем Хранилище
            if (!storage.isClosed)
                storage.Close();

            storage = null;
            GC.WaitForPendingFinalizers();

            //Инициализируем Хранилище
            storage = new Storage();

            //Открываем Хранилище
            storage.Open(storageWorkDirectoryPath, Storage.OpenMode.OPEN);
        }

        /// <summary>
        /// Удалить Хранилище. При необходмости Хранилище закрывается и удаляется с жёсткого диска.
        /// </summary>
        /// <param name="storage">Хранилище</param>
        public void Storage_Remove(ref Storage storage)
        {
            if (storage == null)
                throw new IARunTestException("Хранилище не определено.");

            //Получаем путь до рабочей директории Хранилища
            string storageWorkDirectoryPath = storage.WorkDirectory.Path;

            //Принудительно закрываем Хранилище
            if (!storage.isClosed)
                storage.Close();

            //Удаляем рабочую директорию Хранилища
            DirectoryController.Remove(storageWorkDirectoryPath, true);

            storage = null;
            GC.WaitForPendingFinalizers();
        }
        #endregion

        #region Вспомогательный класс тестирования TestRunConfigСlass     

        /// <summary>
        /// Вспомогательный класс для проведения тестирования плагинов. Позволяет задавать только необходимые обработчики перед вызовом TestUtil.RunTest.
        /// </summary>
        public abstract class RunTestConfig
        {
            /// <summary>
            /// Путь в материалах к тестовому набору файлов, на котором будет выполняеться текущий тест. По-умолчанию - пустая строка.
            /// </summary>
            public virtual string Stage01SourcesPostfix { get { return String.Empty; } }
            /// <summary>
            /// Экземпляр плагина, который будет проходить тестирование.
            /// </summary>
            public virtual IA.Plugin.Interface Stage02Plugin { get { return null; } }
            /// <summary>
            /// Использовать ли кешированное хранилище. Имеет смысл при "тяжёлой" подготовке к тестированию, которую можно выполнить один раз затем только используя уже подготовленное хранилище. По-умолчанию - нет.
            /// </summary>
            public virtual bool Stage03IsUseCachedStorage { get {return false;} }
            /// <summary>
            /// Функция подготовки кешированного хранилища. Имеет смысл только если <c>IsUseCachedStorage</c> = true. По-умолчанию - пустая.
            /// </summary>
            public virtual void Stage04PrepareStorageDelegate(Storage storage, string testMaterialsDirectoryPath) { }
            /// <summary>
            /// Функция подготовки хранилища перед выполнением тестового запуска плагина. По-умолчанию - пустая.
            /// </summary>
            public virtual void Stage11PrepareStorageDelegate(Storage storage, string testMaterialsDirectoryPath) { }
            /// <summary>
            /// Функция проверки содержимого хранилища после выполнения тестового запуска плагина. По-умолчанию - возвращает true.
            /// </summary>
            public virtual bool Stage12CheckStorageDelegate(Storage storage, string testMaterialsDirectoryPath) { return true; }
            /// <summary>
            /// Функция проверки отчётов плагина после выполнения его тестового запуска. По-умолчанию - возвращает true
            /// </summary>
            public virtual bool Stage13CheckReportsAfterRunDelegate(string reportsDirectoryPath, string testMaterialsDirectoryPath) { return true; }
            /// <summary>
            /// Необходим ли повторный запуск плагина. По-умолчанию - нет.
            /// </summary>
            public virtual bool Stage21IsRunTwice { get { return false; } }
            /// <summary>
            /// Функция подготовки хранилища к повторному запуску плагина. Обычно производится некоторое переконфигурирование плагина. По-умолчанию - пустая.
            /// </summary>
            public virtual void Stage22PrepareStorageForReRunDelegate(Storage storage, string testMaterialsDirectoryPath) { }
            /// <summary>
            /// Делегат проверки содержимого хранилища после выполнения повторного запуска плагина. По-умолчанию - возвращает true.
            /// </summary>
            public virtual bool Stage23CheckStorageAfterReRunDelegate(Storage storage, string testMaterialsDirectoryPath) { return true; }
            /// <summary>
            /// Делегат проверки отчётов плагина после выполнения его повторного запуска. По-умолчанию - возвращает true.
            /// </summary>
            public virtual bool Stage24CheckReportsAfterReRunDelegate(string reportsDirectoryPath, string testMaterialsDirectoryPath) { return true; }
            /// <summary>
            /// Выдавать ли ошибку тестирования, если вызов метода IsSettingsCorrect тестируемого плагина вернул false. По-умолчанию - false.
            /// </summary>
            public virtual bool Stage000IsSettingsCorrectChecking { get { return false; } }
        }
        #endregion
    }

}
