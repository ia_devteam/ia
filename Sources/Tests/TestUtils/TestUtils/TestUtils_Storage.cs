﻿
using Store;

namespace TestUtils
{
    /// <summary>
    /// Часть класса инфраструктуры тестирования. Содержит статические методы для работы к Хранилищем.
    /// </summary>
    public partial class TestUtilsClass
    {
        /// <summary>
        /// Задать всем файлам, занесённым в Хранилище, указанный тип
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="type">Тип файла</param>
        public static void Storage_Files_SetType(Storage storage, FileType type)
        {
            foreach (IFile file in storage.files.EnumerateFiles(enFileKind.anyFile))
                file.fileType = type;
        }        
    }
}
