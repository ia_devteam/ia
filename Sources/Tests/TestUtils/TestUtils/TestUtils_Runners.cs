﻿using System;
using System.Collections.Generic;

using Store;
using Store.Table;

namespace TestUtils
{
    /// <summary>
    /// Часть класса инфраструктуры тестирования. Содержит статические методы для запуска отдельных плагинов.
    /// </summary>
    public partial class TestUtilsClass
    {
        /// <summary>
        /// Автоматический запуск указанного плагина на указанном Хранилище с указанным буффером настроек
        /// </summary>
        /// <param name="plugin">Плагин</param>
        /// <param name="storage">Хранилище</param>
        /// <param name="buffer">Буффер с настройками плагина</param>
        private static void RunPlugin(IA.Plugin.Interface plugin, Storage storage, IBufferWriter buffer = null)
        {
            try
            {
                //Если буфер с настройками задан
                if (buffer != null)
                    //Сохраняем настройки в Хранилище
                    storage.pluginSettings.SaveSettings(plugin.ID, buffer);

                //Инициализируем плагин
                plugin.Initialize(storage);

                //Получаем настройки из Хранилища
                if(buffer != null)
                    plugin.LoadSettings(storage.pluginSettings.LoadSettings(plugin.ID));

                //Запускаем плагин
                plugin.Run();
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка при автоматическом запуске плагина" + (plugin == null ? String.Empty : "<" + plugin.Name + ">") + ".", ex);
            }
        }

        /// <summary>
        /// Запустить плагин FillFileList
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="path">Путь к анализируемой директории</param>
        public static void Run_FillFileList(
            Storage storage,
            string path = null
            )
        {
            //Задаём указанный путь к анализируемой директории
            if (path != null)
                storage.appliedSettings.SetFileListPath(path);

            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Importers.FillFileList.FillFileList(), storage);
        }

        /// <summary>
        /// Запустить плагин - Идентификация файлов
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="workSpace">Рабочее пространство</param>
        /// <param name="workSpaceDescription">Описание рабочего пространства</param>
        public static void Run_IdentifyFileTypes(
            Storage storage,
            string workSpace = "Базовое рабочее место",
            string workSpaceDescription = null
            )
        {
            //Устанавливаем соединение с базой данных
            TestUtilsClass.SQL_SetupTestConnection();

            //Формируем буфер настроек плагина
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(workSpace);
            buffer.Add(workSpaceDescription);

            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Analyses.IdentifyFileTypes.IdentifyFileTypes(), storage, buffer);
        }

        /// <summary>
        /// Запустить плагин - Индексирвоание файлов
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="isProcessBinaries">Включать файлы с исполняемым кодом?</param>
        /// <param name="isProcessBinarySources">Включать бинарные файлы, содержащие исходный код?</param>
        /// <param name="isProcessData">Включать файлы с данными?</param>
        /// <param name="isProcessSources">Включать файлы с исходным кодом?</param>
        /// <param name="isProcessUnknown">Включать неразобранные файлы?</param>
        /// <param name="symbols">Список символов</param>
        public static void Run_IndexingFileContent(
            Storage storage,
            bool isProcessBinaries = false,
            bool isProcessBinarySources = false,
            bool isProcessData = true,
            bool isProcessSources = true,
            bool isProcessUnknown = true,
            bool isIgnoreCase = false,
            string symbols = ""
            )
        {
            //Формируем буфер настроек плагина
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(isProcessBinaries);
            buffer.Add(isProcessBinarySources);
            buffer.Add(isProcessData);
            buffer.Add(isProcessSources);
            buffer.Add(isProcessUnknown);
            buffer.Add(symbols);
            buffer.Add(isIgnoreCase);


            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Analyses.IndexingFileContent.IndexingFileContent(), storage, buffer);
        }

        /// <summary>
        /// Запустить плагин - Анализ указателей
        /// </summary>
        /// <param name="storage">Хранилище</param>
        public static void Run_PointsToAnalysesSimple(
            Storage storage
            )
        {
            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Analyses.PointsToAnalysesSimple.PointsToAnalysesSimple(), storage);
        }

        /// <summary>
        /// Запустить плагин - Анализ указателей
        /// </summary>
        /// <param name="storage">Хранилище</param>
        public static void Run_FileSaver(
            Storage storage
            )
        {
            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Analyses.FileSaver.FileSaver(), storage);
        }


        #region Парсеры
        /// <summary>
        /// Запустить плагин - Парсер C#
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="buildedSourceFolder">Путь до директории с собранными исходными текстами</param>
        /// <param name="firstSensorNumber">Номер первого датчика</param>
        /// <param name="level2">Уровень НДВ - 2?</param>
        public static void Run_CSharpParser(
            Storage storage,
            string buildedSourceFolder,
            UInt64 firstSensorNumber = 1,
            bool level2 = false
            )
        {
            //Формируем буфер настроек плагина
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(buildedSourceFolder);
            buffer.Add(firstSensorNumber);
            buffer.Add(level2);
            buffer.Add(false);
            buffer.Add(false);
            buffer.Add("");

            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Parsers.CSharpParser.CSharpParser(), storage, buffer);
        }

        /// <summary>
        /// Запустить плагин - Парсер PHP
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="isLevel2">Уровень НДВ - 2?</param>
        /// <param name="isLevel3">Уровень НДВ - 3?</param>
        /// <param name="isNoSensors">Не вставлять датчики?</param>
        /// <param name="firstSensorNumber">Номер первого датчика</param>
        /// <param name="sensorFunctionText_Level2">Текст функции датчика (НДВ 2)</param>
        /// <param name="sensorText_Level2">Текст датчика (НДВ 2)</param>
        /// <param name="sensorFunctionText_Level3">Текст функции датчика (НДВ 3)</param>
        /// <param name="sensorText_Level3">Текст датчика (НДВ 3)</param>
        /// <param name="isSkipErrors">Пропускать ли ошибки?</param>
        public static void Run_JavaScriptParser(
            Storage storage,
            bool isLevel2 = true,
            bool isLevel3 = false,
            bool isNoSensors = false,
            UInt64 firstSensorNumber = 1,
            string sensorFunctionText_Level2 = "function getXmlHttp() { var xmlhttp; xmlhttp = new XMLHttpRequest();return xmlhttp;} function SensorRnt(a) { var oReq = getXmlHttp(); oReq.open(\"GET\", \"http://localhost:8080/\"+a,true); oReq.send(null);resp=oReq.responseText; return resp;} ",
            string sensorText_Level2 = "SensorRnt(#);",
            string sensorFunctionText_Level3 = "",
            string sensorText_Level3 = "var RNTfileWITHsensors_FileSystemObject, RNTfileWITHsensors_File; RNTfileWITHsensors_FileSystemObject = new ActiveXObject(\"Scripting.FileSystemObject\"); RNTfileWITHsensors_File = RNTfileWITHsensors_FileSystemObject.OpenTextFile(\"c:\\\\RNTSensorlog_JavaScript.log\", 8, true); RNTfileWITHsensors_File.WriteLine(#);RNTfileWITHsensors_File.Close();",
            bool isSkipErrors = false
            )
        {
            //Формируем буфер настроек плагина
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(isLevel2, isLevel3, isNoSensors);
            buffer.Add(firstSensorNumber);
            buffer.Add(sensorFunctionText_Level2);
            buffer.Add(sensorText_Level2);
            buffer.Add(sensorFunctionText_Level3);
            buffer.Add(sensorText_Level3);
            buffer.Add(isSkipErrors);

            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Parsers.JavaScriptParser.JavaScriptParser(), storage, buffer);
        }

        /// <summary>
        /// Запустить плагин - Парсер PHP
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="sensorText">Текст датчика</param>
        /// <param name="firstSensorNumber">Номер первого датчика</param>
        public static void Run_PhpParser(
            Storage storage,
            string sensorText = " $RNThandle = fopen(\"c:\\RNTSens.txt\", \"a\"); fputs($RNThandle,\"#\\n\"); fclose($RNThandle); ",
            ulong firstSensorNumber = 1,
            ulong ndvLevel = 2
            )
        {
            //Формируем буфер настроек плагина
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(sensorText);
            buffer.Add(firstSensorNumber);
            buffer.Add(ndvLevel);

            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Parsers.PhpParser.PHPParser(), storage, buffer);
        }

        /// <summary>
        /// Запустить плагин - Парсер VB(S)
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="level">Уровень НДВ</param>
        /// <param name="firstSensorNumber">Номер первого датчика</param>
        /// <param name="sensorText">Текст датчика</param>
        /// <param name="isVB">Вставлять датчики в исходные тексты на языке (VisualBasic)?</param>
        /// <param name="vbSensorFunctionText">Текст функции датчика (VisualBasic)</param>
        /// <param name="isVBS">Вставлять датчики в исходные тексты на языке (VisualBasicScript)?</param>
        /// <param name="vbsSensorFunctionText">Текст функции датчика (VisualBasicScript)</param>
        /// <param name="isDefault_systemFunctionsFilePath">Использовать стандартный путь к файлу со списком системных функций?</param>
        /// <param name="systemFunctionsFilePath">Путь к файлу со списком системных функций</param>
        /// <param name="isDefault_systemVariablesFilePath">Использовать стандартный путь к файлу со списком системных переменных?</param>
        /// <param name="systemVariablesFilePath">Путь к файлу со списком системных переменных</param>
        public static void Run_VBParser(
            Storage storage,
            uint level = 2,
            ulong firstSensorNumber = 1,
            string sensorText = "sensor(#)",
            bool isVB = true,
            string vbSensorFunctionText = "Private Sub sensor(n)\nDim MyFile\nMyFile = FreeFile\nOpen (\"C:\\vb_log_\\\" & App.ThreadID & \".log\") For Append As #MyFile\nWrite #MyFile, n\nClose #MyFile\nEnd Sub",
            bool isVBS = true,
            string vbsSensorFunctionText = "Private Sub sensor(n)\nDim WinAPI: Set WinAPI = CreateObject(&quot;DynamicWrapperX&quot;)\nWinAPI.Register &quot;KERNEL32.DLL&quot;, &quot;GetCurrentThreadId&quot;, &quot;r=u&quot;\nDim fso: Set fso = CreateObject(&quot;Scripting.FileSystemObject&quot;)\nDim tf: Set tf = fso.OpenTextFile(&quot;c:\vbs_traces_&quot; &amp; WinAPI.GetCurrentThreadId &amp; &quot;.txt&quot;,8,true)\ntf.WriteLine(n)\ntf.Close\nEnd Sub",
            bool isDefault_systemFunctionsFilePath = true,
            string systemFunctionsFilePath = null,
            bool isDefault_systemVariablesFilePath = true,
            string systemVariablesFilePath = null
            )
        {
            //Формируем буфер настроек плагина
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(level);
            buffer.Add(firstSensorNumber);
            buffer.Add(sensorText);
            buffer.Add(isVB);
            buffer.Add(vbSensorFunctionText);
            buffer.Add(isVBS);
            buffer.Add(vbsSensorFunctionText);
            buffer.Add(isDefault_systemFunctionsFilePath);
            buffer.Add(systemFunctionsFilePath);
            buffer.Add(isDefault_systemVariablesFilePath);
            buffer.Add(systemVariablesFilePath);

            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Parsers.VBParser.VBParser(), storage, buffer);
        }
                
        #endregion

        /// <summary>
        /// Запустить плагин - Генератор статических маршрутов
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="isLevel2">НДВ 2?</param>
        public static void Run_WayGenerator(
            Storage storage,
            bool isLevel2)
        {
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(isLevel2);

            //Запускаем плагин
            TestUtilsClass.RunPlugin(new IA.Plugins.Analyses.WayGenerator.WayGenerator(), storage, buffer);
        }

        /// <summary>
        /// Запустить плагин - Вставка датчиков в исходные тексты Pascal (2-ой уровень)
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="understandSourcePath">Путь к исходникам, на которых отработал Understand. Если не задан - подразумевается, что Understand отработал в рабочем каталоге Хранилища.</param>
        public static void Run_DelphiSensors(
            Storage storage, string understandSourcePath = null)
        {
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(understandSourcePath);
            TestUtilsClass.RunPlugin(new IA.Plugins.Analyses.DelphiSensors.DelphiSensors(), storage, buffer);
        }

        /// <summary>
        /// Запустить плагин - Вставка датчиков в исходники С C++ C# Delphi
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="FirstSensorNumber">Номер первого датчика</param>
        /// <param name="SensorText_C">Датчик C</param>
        /// <param name="SensorText_Cpp">Датчик C++</param>
        /// <param name="SensorInclude_Cpp">Строка включения заголовочного файла датчика в C/C++</param>
        /// <param name="SensorText_Delphi">Датчки Delphi</param>
        /// <param name="SensorText_Delphi_Text">Текстовый датчик Delphi</param>
        /// <param name="SensorInclude_Delphi">Строка включения библиотеки датчика Delphi</param>
        /// <param name="IsDelphiSensors_Text">Вставлять текстовые датчики в Delphi?</param>
        public static void Run_CS_Sensors(
            Storage storage,     
            Int32 FirstSensorNumber = 1,
            string SensorText_C = "int rnt_sensorcall = ____Din_Go(\" \",%DAT%);",
            string SensorText_Cpp = "____Din_Go(\" \",%DAT%);",
            string SensorInclude_Cpp = "#include \"c:/sensor.h\"",
            string SensorText_Delphi = "____Din_Go(' ',%DAT%);",
            string SensorText_Delphi_Text = "System.AssignFile(rnt_sensor,'c:\\Delphifile.txt'); if not System.FileExists('c:\\Delphifile.txt') then begin System.Rewrite(rnt_sensor); System.CloseFile(rnt_sensor); end; System.Append(rnt_sensor); System.Writeln(rnt_sensor,'%DAT%'); System.Flush(rnt_sensor); System.CloseFile(rnt_sensor);",
            string SensorInclude_Delphi = "function ____Din_Go(ff: PChar; a: cardinal) : integer; stdcall; external 'c:\\Windows\\system32\\sensor.dll'",
            bool IsDelphiSensors_Text = false)
        {
            //Формируем буфер настроек плагина
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(FirstSensorNumber);
            buffer.Add(SensorText_C ?? "int rnt_sensorcall = ____Din_Go(\" \",%DAT%);");
            buffer.Add(SensorText_Cpp ?? "____Din_Go(\" \",%DAT%);");
            buffer.Add(SensorInclude_Cpp ?? "#include \"c:/sensor.h\"");
            buffer.Add(SensorText_Delphi ?? "____Din_Go(' ',%DAT%);");
            buffer.Add(SensorText_Delphi_Text ?? "System.AssignFile(rnt_sensor,'c:\\Delphifile.txt'); if not System.FileExists('c:\\Delphifile.txt') then begin System.Rewrite(rnt_sensor); System.CloseFile(rnt_sensor); end; System.Append(rnt_sensor); System.Writeln(rnt_sensor,'%DAT%'); System.Flush(rnt_sensor); System.CloseFile(rnt_sensor);");
            buffer.Add(SensorInclude_Delphi ?? "function ____Din_Go(ff: PChar; a: cardinal) : integer; stdcall; external 'c:\\Windows\\system32\\sensor.dll'");
            buffer.Add(IsDelphiSensors_Text);

            TestUtilsClass.RunPlugin(new IA.Plugins.Analyses.CS_Sensors.CS_Sensors(), storage, buffer);
        }

        /// <summary>
        /// Запуск импорта Understand'а
        /// </summary>
        /// <param name="storage">Хранилище</param>
        /// <param name="reportsDirectoryPath">Папка с отчетами</param>
        /// <param name="sourcesDirectoryPath">Папка с исходниками</param>
        /// <param name="IsLoadVariables">Загрузить переменные</param>
        /// <param name="IsIgnoreMacroLines">Игнорировать строки с макровызовами</param>
        /// <param name="UnderstandPath">Путь к Understand</param>
        /// <param name="isCPP">Анализировать языки программирования С/С++</param>
        /// <param name="isPascal">Анализировать язык программирования Pascal</param>
        /// <param name="isWeb">Анализировать языки Web-программирования (PHP и JavaScript)</param>
        /// <param name="isJava">Анализировать язык программирования Java</param>
        /// <param name="isCS">Анализировать язык программирования C#</param>
        public static void Run_UnderstandImporter(
            Storage storage,
            string reportsDirectoryPath = null,
            string sourcesDirectoryPath = null,
            bool IsLoadVariables = false,
            bool IsIgnoreMacroLines = false,
            string UnderstandPath = null,
            bool isCPP = true,
            bool isPascal = true,
            bool isWeb = false,
            bool isJava = true,
            bool isCS = false
            )
        {
            IBufferWriter buffer = WriterPool.Get();
            buffer.Add(reportsDirectoryPath ?? String.Empty);
            buffer.Add(sourcesDirectoryPath ?? String.Empty);
            buffer.Add(false);
            buffer.Add(IsIgnoreMacroLines);
            buffer.Add(IsLoadVariables);
            buffer.Add(UnderstandPath ?? String.Empty);
            buffer.Add(isCPP);
            buffer.Add(isPascal);
            buffer.Add(isWeb);
            buffer.Add(isJava);
            buffer.Add(isCS);

            TestUtilsClass.RunPlugin(new IA.Plugins.Importers.UnderstandImporter.UnderstandImporter(), storage, buffer);
        }

        /* Rusakov 19.02.2014 Убираем возможность в тестировании заполнять тестовые данные из АИСТа, ST, и заданных вручную
                 * 
                        public void Run_AistImporter(Storage storage, string folder)
                        {
                            //Заполняем настройки плагина
                            IBufferWriter buf = WriterPool.Get();
                            buf.Add(folder);
                            storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.im_AISTIMPORTER, buf);

                            //Запускаем плагин
                            Importer.AistImporter plug = new Importer.AistImporter();
                            plug.Initialize(storage, storage.pluginSettings.LoadSettings(Store.Const.PluginIdentifiers.im_AISTIMPORTER));
                            if (!plug.Run())
                                throw new Exception("RunAistImporter failed");
                        }

                        public void Run_XR_F_IfDef_Importer(Storage storage, string reportFileName)
                        {
                            //Заполняем настройки плагина
                            IBufferWriter buf = WriterPool.Get();
                            buf.Add(reportFileName);
                            storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.im_XR_F_IFDEF_IMPORTER, buf);

                            //Запускаем плагин
                            Importer.Importer_XR_F_IfDef plug = new Importer.Importer_XR_F_IfDef();
                            plug.Initialize(storage, storage.pluginSettings.LoadSettings(Store.Const.PluginIdentifiers.im_XR_F_IFDEF_IMPORTER));
                            if (!plug.Run())
                                throw new Exception("Run_XR_F_IfDef_Importer failed");
                        }

                        public void Run_HandMadeCallGraphImporter(Storage storage, string fileName)
                        {
                            //Заполняем настройки плагина
                            IBufferWriter buf = WriterPool.Get();
                            buf.Add(1);
                            buf.Add(fileName);
                            storage.pluginSettings.SaveSettings(Store.Const.PluginIdentifiers.im_HANDMADECALLGRAPHIMPORTER, buf);

                            //Запускаем плагин
                            Importer.HandMadeCallGraphImporter plug = new Importer.HandMadeCallGraphImporter();
                            plug.Initialize(storage, storage.pluginSettings.LoadSettings(Store.Const.PluginIdentifiers.im_HANDMADECALLGRAPHIMPORTER));
                            plug.Run();
                        }

                        public void Run_STImporter(Storage storage, string fileName)
                        {
                            UInt64 ID = Store.Const.PluginIdentifiers.im_ST;

                            //Заполняем настройки плагина
                            IBufferWriter buf = WriterPool.Get();
                            buf.Add(fileName);
                            storage.pluginSettings.SaveSettings(ID, buf);

                            //Запускаем плагин
                            IA.Plugin.Interface plug = new Importer.ST_Importer();
                            plug.Initialize(storage, storage.pluginSettings.LoadSettings(ID));
                            plug.Run();
                        }
        Rusakov 19.02.2014*/
        /// <summary>
        /// Запустить плагин "Импорт вручную вставленных датчиков"
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="fileName"></param>
        public static void Run_HandMadeSensors(Storage storage, string fileName)
        {
            //Заполняем настройки плагина
            IBufferWriter buf = WriterPool.Get();
            buf.Add(1);
            buf.Add(fileName);

            TestUtilsClass.RunPlugin(new IA.Plugins.Importers.HandMadeSensorsImporter.HandMadeSensorsImporter(), storage, buf);
        }


        /// <summary>
        /// Запуск плагина Анализ результатов динамического анализа
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="importFolder"></param>
        /// <param name="mode"></param>
        /// <param name="keyWord"></param>
        /// <param name="rangeFrom"></param>
        /// <param name="rangeTo"></param>
        /// <param name="sensorOffset"></param>
        /// <param name="isMarkNotCallableEntriesAsProgramEntry"></param>
        /// <param name="isMarkAllEntriesAsProgramEntry"></param>
        /// <param name="isMarkCalledFunctions"></param>
        /// <param name="necessaryFunctionsList"></param>
        /// <param name="internalFunctionsFiles"></param>
        /// <param name="controlFunctionsFile"></param>
        /// <param name="isGenerateRestoredTraces"></param>
        public static void Run_DynamicAnalysisResultsProcessing(Storage storage, string importFolder, int mode = 3, string keyWord = "", long rangeFrom = -1, 
            long rangeTo = -1, UInt64 sensorOffset = 0, bool isMarkNotCallableEntriesAsProgramEntry = false, bool isMarkAllEntriesAsProgramEntry = false,
            bool isMarkCalledFunctions = false, List<string> necessaryFunctionsList = null, List<string> internalFunctionsFiles = null, string controlFunctionsFile = "", bool isGenerateRestoredTraces = true)
        {

            //Заполняем настройки плагина
            IBufferWriter buf = WriterPool.Get();

            buf.Add(false); //режим работы всегда обычный
            buf.Add(importFolder);
            buf.Add(mode); //mode = 2 уровень НДВ
            buf.Add(!String.IsNullOrEmpty(keyWord));
            buf.Add(keyWord);
            buf.Add(!((rangeFrom == -1) && (rangeTo == -1)));
            buf.Add(rangeFrom);
            buf.Add(rangeTo);
            buf.Add(sensorOffset != 0);
            buf.Add(sensorOffset);
            buf.Add(isMarkNotCallableEntriesAsProgramEntry);
            buf.Add(isMarkAllEntriesAsProgramEntry);
            buf.Add(isMarkCalledFunctions);

            if (necessaryFunctionsList != null)
            {
                buf.Add(necessaryFunctionsList.Count);
                foreach (string s in necessaryFunctionsList)
                    buf.Add(s);
            }
            else
            {
                buf.Add(0);
            }

            buf.Add(controlFunctionsFile);

            if (internalFunctionsFiles != null)
            {
                buf.Add(internalFunctionsFiles.Count);
                foreach (string s in internalFunctionsFiles)
                    buf.Add(s);
            }
            else
            {
                buf.Add(0);
            }

            buf.Add(isGenerateRestoredTraces);

            RunPlugin(new IA.Plugins.Analyses.DynamicAnalysisResultsProcessing.DynamicAnalysisResultsProcessing(), storage, buf);
        }
                
        /// <summary>
        /// Запуск плагина Изменения типа датчиков
        /// </summary>
        /// <param name="storage">Хранилище</param>
        public static void Run_SensorTypeFixer(Storage storage)
        {
            RunPlugin(new IA.Plugins.Analyses.SensorTypeFixer.SensorTypeFixer(), storage);
        }


        /// <summary>
        /// Запуск плагина подсчета контрольных сумм
        /// </summary>
        /// <param name="storage">Хранилище</param>
        public static void Run_CheckSum(Storage storage)
        {
            RunPlugin(new IA.Plugins.Analyses.CheckSum.Plugin(), storage);
        }
    }
}
