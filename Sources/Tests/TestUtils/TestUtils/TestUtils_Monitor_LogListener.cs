﻿
using System;
using System.Collections.Generic;
using System.Linq;

using IA.Extensions;

namespace TestUtils
{
    /// <summary>
    /// Часть класса инфраструктуры тестирования. Содержит статические методы для котороля обращения к Логу из Монитора
    /// </summary>
    public partial class TestUtilsClass
    {
        /// <summary>
        /// Класс, реализующий слушателя сообщений, поступающих в Лог из Монитора
        /// </summary>
        public class LogListener : IA.Monitor.Log.Interface, IDisposable
        {
            /// <summary>
            /// Список типов отслеживаемых сообщений
            /// </summary>
            private HashSet<IA.Monitor.Log.Message.enType> messageTypes;

            /// <summary>
            /// Очередь отслеживаемых сообщений
            /// </summary>
            private List<IA.Monitor.Log.Message> messages;

            /// <summary>
            /// Произошла ли ошибка в процессе "жизни" слушателя сообщений
            /// </summary>
            private bool isErrorOccurred = false;

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="messageTypes">Список типов отслеживаемых сообщений</param>
            /// <param name="messageQueue">Очередь отслеживаемых сообщений</param>
            public LogListener(HashSet<IA.Monitor.Log.Message.enType> messageTypes, List<IA.Monitor.Log.Message> messageQueue)
            {
                try
                {
                    //Проверка на разумность
                    if (messageTypes == null)
                        throw new Exception("Список типов отслеживаемых сообщений не определён.");

                    //Проверка на разумность
                    if (messageQueue == null)
                        throw new Exception("Очередь остлеживаемых сообщений не определена.");

                    //Запоминаем типы отслеживаемых сообщений
                    this.messageTypes = messageTypes;

                    //Запоминаем очередь сообщений
                    this.messages = messageQueue;

                    //Проверка на то, что типы переданных сообщений есть среди отслеживаемых типов
                    if (!messageQueue.Select(m => m.Type).Distinct().All(t => messageTypes.Contains(t)))
                        throw new Exception("Среди отслеживаемых сообщений есть сообщения неотслеживаемого типа.");

                    //Регистрируем себя в качестве слушателя монитора
                    IA.Monitor.Log.Register(this);
                }
                catch (Exception ex)
                {
                    //Формируем исключение
                    throw new Exception("Ошибка при создании экземпляра класса слушателя сообщений, поступающих в Лог из Монитора.", ex);
                }
            }

            /// <summary>
            /// Финализатор
            /// </summary>
            ~LogListener()
            {
                //Формируем исключение
                throw new Exception("Для экземпляра слушателя сообщений, поступающих в Лог из Монитора, не был вызван метод Dispose().");
            }

            /// <summary>
            /// Метод интерфейса слушателя. Обработчик получаемых сообщений в Лог из Монитора.
            /// Контроль поступления сообщений в рамках указанной очереди.
            /// </summary>
            /// <param name="message">Переданное сообщение в Лог из Монитора</param>
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                //Если сообщение такого типа мы не отслеживаем
                if (!this.messageTypes.Contains(message.Type))
                    return;

                try
                {
                    //Если сообщений более не ожидалось
                    if (this.messages == null || this.messages.Count == 0)
                        throw new Exception("Сообщений более не ожидалось.\nПолученное сообщение: " + message);

                    //Проверяем, что полученное сообщение совпадает с ожидаемым
                    if (this.messages[0].CompareTo(message) != 0)
                        throw new Exception("Полученное сообщение не совпадает с ожидаемым.\nПолученное сообщение: " + message + "\nОжидаемое сообщение: " + messages[0]);

                    //Удаляем проверенное сообщение
                    this.messages.RemoveAt(0);
                }
                catch (Exception ex)
                {
                    //На этапе "жизни" слушателя сообщений возникла ошибка
                    this.isErrorOccurred = true;

                    //Формируем сообщение
                    string errorMessage = new string[]
                    {
                        "Ошибка при отслеживании сообщений, поступающих в Лог из Монитора.",
                        ex.ToFullMultiLineMessage()
                    }.ToMultiLineString();

                    //Формируем исключение
                    throw new Exception(errorMessage);
                }
            }

            /// <summary>
            /// Принудительное освобождение неуправляемых ресурсов
            /// </summary>
            public void Dispose()
            {
                //Отменяем регистрацию
                IA.Monitor.Log.Unregister(this);

                //Отключаем необходимость вызова финализатора
                GC.SuppressFinalize(this);

                //В случае, если на этапе "жизни" слушателя сообщений не возникло ошибок, проверяем, что отслеживаемых сообщений не осталось
                if (!this.isErrorOccurred && this.messages.Count != 0)
                    throw new Exception("Ошибка при отслеживании сообщений, поступающих в Лог из Монитора.\nНе все ожидаемые сообщения были получены.");
            }
        }

        /// <summary>
        /// Класс, реализующий простого слушателя монитора. Не содержит строгих проверок. Рассчитан на однократное применение при выполнении одного теста.
        /// </summary>
        public class LogSimpleListenerForTests : IA.Monitor.Log.Interface
        {
            readonly List<IA.Monitor.Log.Message> log;
            /// <summary>
            /// Простой конструктор. Регистрирует себя как слушателя лога.
            /// </summary>
            public LogSimpleListenerForTests()
            {
                log = new List<IA.Monitor.Log.Message>();
                IA.Monitor.Log.Register(this);
            }

            /// <summary>
            /// Очистка внутреннего списка сообщений.
            /// </summary>
            public void Clear()
            {
                log.Clear();
            }

            /// <summary>
            /// Подсчёт количества сообщений, удовлетворяющих заданному условию.
            /// </summary>
            /// <param name="action"></param>
            /// <returns></returns>
            public int Count(Func<IA.Monitor.Log.Message, bool> action = null)
            {
                if (action == null)
                    return log.Count;

                return log.Count(action);
            }

            /// <summary>
            /// Проверка списка сообщений на предмет наличия среди них заданного.
            /// </summary>
            /// <param name="message">Текст искомого сообщения.</param>
            /// <param name="type">Тип искомого сообщения.</param>
            /// <param name="messageIsPartial">Является ли текст искомого сообщения частичным? True - искать как часть текста, False - искать полное совпадение.</param>
            /// <returns></returns>
            public bool CheckMessage(string message, IA.Monitor.Log.Message.enType type, bool messageIsPartial = true)
            {
                return log.Any(m => (m.Type == type && messageIsPartial ? m.Text.Contains(message) : String.Equals(m.Text, message, StringComparison.Ordinal)));
            }
            
            public void AddMessage(IA.Monitor.Log.Message message)
            {
                log.Add(message);
            }
        }
    }
}
