﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

using IOController;
using FileOperations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestUtils
{
    /// <summary>
    /// Часть класса инфраструктуры тестирования. Содержит статические методы для работы с отчётами.
    /// </summary>
    public partial class TestUtilsClass
    {
        /// <summary>
        /// 
        /// </summary>
        public class TestUtilsException : Exception
        {
            public TestUtilsException(string ex) : base(ex)
            { }
        }


        /// <summary>
        /// Сравнение двух отчётов (в текстовом формате).
        /// </summary>
        /// <param name="report1_filePath">Путь к файлу первого отчёта</param>
        /// <param name="report2_filePath">Путь к файлу второго отчёта</param>
        /// <param name="isIgnoreCase">Игнорировать ли регистр символов?</param>
        /// <param name="isIgnoreLinesOrder">Не учитывать порядок строк в файлах?</param>
        /// <param name="materialsDirectoryPathPrefix">Префикс путь до директории с обрабатываемыми материалами</param>
        /// <param name="isIgnoreLineEndings">Игнорировать различия в переносах строк</param>
        /// <returns>Совпадают ли отчёты?</returns>
        public static bool Reports_TXT_Compare(string report1_filePath, string report2_filePath, bool isIgnoreCase = false, bool isIgnoreLinesOrder = false, string materialsDirectoryPathPrefix = null, bool isIgnoreLineEndings = true)
        {

            using (StreamReader reader1 = new StreamReader(report1_filePath))
            using (StreamReader reader2 = new StreamReader(report2_filePath))
            {
                if (!isIgnoreLinesOrder)
                {
                    string contents1 = reader1.ReadToEnd();
                    string contents2 = reader2.ReadToEnd();

                    if (isIgnoreLineEndings)
                    {
                        contents1 = contents1.Replace("\r\n", "\n").Replace("\n\r", "\n");
                        contents2 = contents2.Replace("\r\n", "\n").Replace("\n\r", "\n");
                    }

                    if (materialsDirectoryPathPrefix != null)
                    {
                        contents1 = contents1.Replace(PathConstant_old, materialsDirectoryPathPrefix);
                        contents2 = contents2.Replace(PathConstant_old, materialsDirectoryPathPrefix);
                    }

                    return String.Compare(contents1, contents2, isIgnoreCase) == 0;
                }

                List<string> lines1 = new List<string>();

                string line1;
                while ((line1 = reader1.ReadLine()) != null)
                {
                    if (materialsDirectoryPathPrefix != null)
                        line1 = line1.Replace(PathConstant_old, materialsDirectoryPathPrefix);

                    lines1.Add(isIgnoreCase ? line1.ToLower() : line1);
                }

                string line2;
                while ((line2 = reader2.ReadLine()) != null)
                {
                    if (materialsDirectoryPathPrefix != null)
                        line2 = line2.Replace(PathConstant_old, materialsDirectoryPathPrefix);

                    if (isIgnoreCase)
                        line2 = line2.ToLower();

                    if (lines1.Contains(line2))
                        lines1.Remove(line2);
                    else
                        return false;
                }

                if (lines1.Count != 0)
                    return false;
            }

            return true;
        }

        /// <summary>
        ///  Сравнение двух директорий с отчётами в текстовом формате и формате Microsoft Word.
        /// </summary>
        /// <param name="reports1_directoryPath">Путь до первой директории с отчётами</param>
        /// <param name="reports2_directoryPath">Путь до второй директории с отчётами</param>
        /// <param name="isIgnoreCase">Игнорировать ли регистр символов?</param>
        /// <param name="isIgnoreLinesOrder">Не учитывать порядок строк в файлах?</param>
        /// <param name="materialsDirectoryPathPrefix">Префикс путь до директории с обрабатываемыми материалами</param>
        /// <returns>Совпадают ли директории с отчётами?</returns>
        public static bool Reports_Directory_TXT_Compare(string reports1_directoryPath, string reports2_directoryPath, bool isIgnoreCase = false, bool isIgnoreLinesOrder = false, string materialsDirectoryPathPrefix = null)
        {
            //Проверяем, что количество файлов в обоих директориях совпадают
            if (Directory.EnumerateFiles(reports1_directoryPath, "*.*", SearchOption.AllDirectories).Count() != Directory.EnumerateFiles(reports2_directoryPath, "*.*", SearchOption.AllDirectories).Count())
                return false;

            //Сравниваем все отчёты по очереди
            return Directory.EnumerateFiles(reports1_directoryPath, "*.*", SearchOption.AllDirectories)
                                .All(f =>
                                    {
                                        string secondPath = PathController.ChangePrefix(f, reports1_directoryPath, reports2_directoryPath);
                                        if (System.IO.Path.GetExtension(f) == ".doc" || System.IO.Path.GetExtension(f) == ".docx")
                                            return TestUtilsClass.Reports_DOC_Compare(f, secondPath);
                                        else
                                            return TestUtilsClass.Reports_TXT_Compare(f, secondPath, isIgnoreCase, isIgnoreLinesOrder, materialsDirectoryPathPrefix);

                                    }
                                        );
        }

        /// <summary>
        /// Сравнение двух отчётов (в формате Microsoft Word).
        /// </summary>
        /// <param name="report1_filePath">Путь к файлу первого отчёта</param>
        /// <param name="report2_filePath">Путь к файлу второго отчёта</param>
        /// <param name="isIgnoreCase">Игнорировать ли регистр символов?</param>
        /// <returns>Совпадают ли отчёты?</returns>
        public static bool Reports_DOC_Compare(string report1_filePath, string report2_filePath, bool isIgnoreCase = false)
        {
            var lst1 = (from p in Process.GetProcessesByName("WINWORD") select p.Id).ToList();

            object report1_filePath_object = report1_filePath;
            object report2_filePath_object = report2_filePath;
            object doNotSaveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;

            //Приложения Microsoft Word
            Microsoft.Office.Interop.Word.Application application = new Microsoft.Office.Interop.Word.Application();

            application.DisplayAlerts = Microsoft.Office.Interop.Word.WdAlertLevel.wdAlertsNone;

            //Документ Microsoft Word
            Microsoft.Office.Interop.Word.Document document = null;

            document = application.Documents.Open(ref report1_filePath_object);
            string contents1 = document.Content.Text;
            ((Microsoft.Office.Interop.Word._Document)document).Close(ref doNotSaveChanges);

            document = application.Documents.Open(ref report2_filePath_object);
            string contents2 = document.Content.Text;
            ((Microsoft.Office.Interop.Word._Document)document).Close(ref doNotSaveChanges);

            var lst2 = from p in Process.GetProcessesByName("WINWORD") select p.Id;
            var pid = lst2.Where(i => !lst1.Contains(i)).ToList()[0];//(from p in lst2 where !lst1.Contains(p) select p).ToList()[0];
            ((Microsoft.Office.Interop.Word._Application)application).Quit(ref doNotSaveChanges);
            try
            {
                Process WordProcess = System.Diagnostics.Process.GetProcessById(pid);
                WordProcess.Kill();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(application);
                System.Threading.Thread.Sleep(1000);
            }
            catch { }


            return String.Compare(contents1, contents2, isIgnoreCase) == 0;
        }

        /// <summary>
        ///  Сравнение двух директорий с отчётами в формате Microsoft Word.
        /// </summary>
        /// <param name="reports1_directoryPath">Путь до первой директории с отчётами</param>
        /// <param name="reports2_directoryPath">Путь до второй директории с отчётами</param>
        /// <param name="isIgnoreCase">Игнорировать ли регистр символов?</param>
        /// <returns>Совпадают ли директории с отчётами?</returns>
        public static bool Reports_Directory_DOC_Compare(string reports1_directoryPath, string reports2_directoryPath, bool isIgnoreCase = false)
        {
            //Проверяем, что количество файлов в обоих директориях совпадают
            if (Directory.EnumerateFiles(reports1_directoryPath, "*.*", SearchOption.AllDirectories).Count() != Directory.EnumerateFiles(reports2_directoryPath, "*.*", SearchOption.AllDirectories).Count())
                return false;

            //Сравниваем все отчёты по очереди
            return Directory.EnumerateFiles(reports1_directoryPath, "*.*", SearchOption.AllDirectories).All(f => TestUtilsClass.Reports_DOC_Compare(f, PathController.ChangePrefix(f, reports1_directoryPath, reports2_directoryPath), isIgnoreCase));
        }

        static Microsoft.XmlDiffPatch.XmlDiff diff = new Microsoft.XmlDiffPatch.XmlDiff(
                Microsoft.XmlDiffPatch.XmlDiffOptions.IgnoreChildOrder
                | Microsoft.XmlDiffPatch.XmlDiffOptions.IgnoreWhitespace)
        { Algorithm = Microsoft.XmlDiffPatch.XmlDiffAlgorithm.Precise };

        /// <summary>
        /// Сравнение двух отчётов в формате XML.
        /// </summary>
        /// <param name="report1_filePath">Путь к файлу первого отчёта</param>
        /// <param name="report2_filePath">Путь к файлу второго отчёта</param>
        /// <param name="isIgnoreCase">Игнорировать ли регистр символов?</param>
        /// <param name="materialsDirectoryPathPrefix">Префикс путь до директории с обрабатываемыми материалами</param>
        /// <returns>Совпадают ли отчёты?</returns>
        public static bool Reports_XML_Compare(string report1_filePath, string report2_filePath, bool isIgnoreCase = false, string materialsDirectoryPathPrefix = null)
        {

            var contents1 = File.ReadLines(report1_filePath)
                .Select(x => x.StartsWith(@"<Column ss:AutoFitWidth='1' ss:Width=") ? @"<Column ss:AutoFitWidth='1' ss:Width='300'/>" : x)
                .Select(x => x.Contains(@"<Table><Column ss:AutoFitWidth='1' ss:Width=") ? @"<Table><Column ss:AutoFitWidth='1' ss:Width='300'/>" : x)
                .Select(x => (materialsDirectoryPathPrefix != null) ? x.Replace(PathConstant_old, materialsDirectoryPathPrefix).Replace(PathConstant, materialsDirectoryPathPrefix) : x)
                .Select(x => (isIgnoreCase) ? x.ToLower() : x);

            var contents2 = File.ReadLines(report2_filePath)
                .Select(x => x.StartsWith(@"<Column ss:AutoFitWidth='1' ss:Width=") ? @"<Column ss:AutoFitWidth='1' ss:Width='300'/>" : x)
                .Select(x => x.Contains(@"<Table><Column ss:AutoFitWidth='1' ss:Width=") ? @"<Table><Column ss:AutoFitWidth='1' ss:Width='300'/>" : x)
                .Select(x => (materialsDirectoryPathPrefix != null) ? x.Replace(PathConstant_old, materialsDirectoryPathPrefix).Replace(PathConstant, materialsDirectoryPathPrefix) : x)
                .Select(x => (isIgnoreCase) ? x.ToLower() : x);
            
            var doc1 = new System.Xml.XmlDocument();
            doc1.Load(new StringReader(string.Join(Environment.NewLine, contents1)));
            var doc2 = new System.Xml.XmlDocument();
            doc2.Load(new StringReader(string.Join(Environment.NewLine, contents2)));

            var result = diff.Compare(doc1, doc2);

            if (!result)
            {
                string fileWithDiffs = System.IO.Path.GetTempFileName();
                var wr = System.Xml.XmlWriter.Create(File.OpenWrite(fileWithDiffs));
                diff.Compare(doc1, doc2, wr);
                wr.Close();
                Assert.Fail("Различия xml сохранены в <{0}>", fileWithDiffs);
            }

            return true;
        }        

        /// <summary>
        ///  Сравнение двух директорий с отчётами в формате XML.
        /// </summary>
        /// <param name="reports1_directoryPath">Путь до первой директории с отчётами</param>
        /// <param name="reports2_directoryPath">Путь до второй директории с отчётами</param>
        /// <param name="isIgnoreCase">Игнорировать ли регистр символов?</param>
        /// <param name="materialsDirectoryPathPrefix">Префикс путь до директории с обрабатываемыми материалами</param>
        /// <returns>Совпадают ли директории с отчётами?</returns>
        public static bool Reports_Directory_XML_Compare(string reports1_directoryPath, string reports2_directoryPath, bool isIgnoreCase = false, string materialsDirectoryPathPrefix = null)
        {
            //Проверяем, что количество файлов в обоих директориях совпадают
            if (Directory.EnumerateFiles(reports1_directoryPath, "*.*", SearchOption.AllDirectories).Count() != Directory.EnumerateFiles(reports2_directoryPath, "*.*", SearchOption.AllDirectories).Count())
                return false;

            //Сравниваем все отчёты по очереди
            return Directory.EnumerateFiles(reports1_directoryPath, "*.*", SearchOption.AllDirectories).All(f => TestUtilsClass.Reports_XML_Compare(f, PathController.ChangePrefix(f, reports1_directoryPath, reports2_directoryPath), isIgnoreCase, materialsDirectoryPathPrefix));
        }

        /// <summary>
        /// Сравнение двух отчётов в формате текстовых строк.
        /// </summary>
        /// <param name="report1_filePath">Путь к файлу первого отчёта</param>
        /// <param name="report2_filePath">Путь к файлу второго отчёта</param>
        /// <param name="isIgnoreCase">Игнорировать ли регистр символов?</param>
        /// <param name="isIgnoreLinesOrder">Не учитывать порядок строк в файлах?</param>
        /// <param name="materialsDirectoryPathPrefix">Префикс путь до директории с обрабатываемыми материалами</param>
        /// <returns>Совпадают ли отчёты?</returns>
        public static bool Reports_Lines_Compare(string report1_filePath, string report2_filePath, bool isIgnoreCase = false, bool isIgnoreLinesOrder = false, string materialsDirectoryPathPrefix = null)
        {
            List<string> contents1 = File.ReadAllLines(report1_filePath).Select(x => (materialsDirectoryPathPrefix != null) ? x.Replace(PathConstant_old, materialsDirectoryPathPrefix) : x).ToList();
            List<string> contents2 = File.ReadAllLines(report2_filePath).Select(x => (materialsDirectoryPathPrefix != null) ? x.Replace(PathConstant_old, materialsDirectoryPathPrefix) : x).ToList();

            if (!isIgnoreLinesOrder)
                return contents1.Zip(contents2, (x, y) => string.Compare(x, y, isIgnoreCase) == 0).All(x => x);

            foreach (string s in contents2)
                if (contents1.Contains(s))
                    contents1.Remove(s);

            return contents1.Count == 0;
        }

        /// <summary>
        ///  Сравнение двух директорий с отчётами в формате текстовых строк.
        /// </summary>
        /// <param name="reports1_directoryPath">Путь до первой директории с отчётами</param>
        /// <param name="reports2_directoryPath">Путь до второй директории с отчётами</param>
        /// <param name="isIgnoreCase">Игнорировать ли регистр символов?</param>
        /// <param name="isIgnoreLinesOrder">Не учитывать порядок строк в файлах?</param>
        /// <param name="materialsDirectoryPathPrefix">Префикс путь до директории с обрабатываемыми материалами</param>
        /// <returns>Совпадают ли директории с отчётами?</returns>
        public static bool Reports_Directory_Lines_Compare(string reports1_directoryPath, string reports2_directoryPath, bool isIgnoreCase = false, bool isIgnoreLinesOrder = false, string materialsDirectoryPathPrefix = null)
        {
            //Проверяем, что количество файлов в обоих директориях совпадают
            if (Directory.EnumerateFiles(reports1_directoryPath, "*.*", SearchOption.AllDirectories).Count() != Directory.EnumerateFiles(reports2_directoryPath, "*.*", SearchOption.AllDirectories).Count())
                return false;

            //Сравниваем все отчёты по очереди
            return Directory.EnumerateFiles(reports1_directoryPath, "*.*", SearchOption.AllDirectories).All(f => TestUtilsClass.Reports_Lines_Compare(f, PathController.ChangePrefix(f, reports1_directoryPath, reports2_directoryPath), isIgnoreCase, isIgnoreLinesOrder, materialsDirectoryPathPrefix));
        }



        /// <summary>
        /// Проверка элемента
        /// </summary>
        /// <typeparam name="T">Тип проверяемого элемента</typeparam>
        /// <param name="arg">Проверяемый элемент</param>
        /// <param name="correct">Корректный элемент</param>
        /// <returns>True - если элементы совпадают</returns>
        public delegate bool Checker<T>(T arg, T correct);


        /// <summary>
        /// Проверка коллекции элементов
        /// </summary>
        /// <typeparam name="T">Тип элементов коллекции</typeparam>
        /// <param name="checker">Функционал проверки отдельного элемента</param>
        /// <param name="collection">Коллекция элементов для проверки</param>
        /// <param name="correctResults">Корректная коллекция элементов</param>
        public static void CheckArray<T>(Checker<T> checker, IEnumerable<T> collection, params T[] correctResults)
        {
            int i = 0;

            foreach (T t in collection)
            {
                if (!checker(t, correctResults[i]))
                    throw new TestUtilsException("CheckArray fail");

                i++;
            }

            if (i != correctResults.Length)
                throw new TestUtilsException("CheckArray fail 1");
        }

        /// <summary>
        /// Проверка распакованных из двух проверяемых архивов файлов на идентичность.
        /// </summary>
        /// <param name="file1">Полный путь до первого файла.</param>
        /// <param name="file2">Полный путь до второго файла.</param>
        /// <returns></returns>
        public delegate bool UnarchivedFilesComparer(string file1, string file2);


        /// <summary>
        /// Метод для сравнения двух архивов 7z по содержимому.
        /// </summary>
        /// <param name="archive7z_path1">Полный путь до первого архива.</param>
        /// <param name="archive7z_path2">Полный путь до второго архива.</param>
        ///<param name="storage">Хранилище, у которого запрашивать временные каталоги.</param>
        ///<param name="pluginId">Идентификатор плагина, от имени которого запрашивать временные каталоги.</param>
        /// <param name="comparer">Функция сравнения распакованных файлов.</param>

        public static bool Report_7z_Compare(string archive7z_path1, string archive7z_path2, Store.Storage storage, ulong pluginId, UnarchivedFilesComparer comparer)
        {
            //если списки файлов внутри архивов отличаются - они заведомо не равны.
            CollectionAssert.AreEquivalent(ArchiveReader.ReadFileList(archive7z_path1), ArchiveReader.ReadFileList(archive7z_path2),
                String.Format("Списки файлов внутри сверяемых архивов <{0}> и <{1}> не совпадают.", archive7z_path1, archive7z_path2));

            string tempdir = storage.GetTempDirectory(pluginId).Path;

            string tempdir1 = Path.Combine(tempdir, "1");
            DirectoryController.Create(tempdir1);
            string tempdir2 = Path.Combine(tempdir, "2");
            DirectoryController.Create(tempdir2);

            //сравнение пофайлово
            var files1 = ArchiveReader.ExtractAll(archive7z_path1, tempdir1);
            var files2 = ArchiveReader.ExtractAll(archive7z_path2, tempdir2);

            foreach (var file1 in files1)
            {
                string file2 = file1.Remove(0, tempdir1.Length).Insert(0, tempdir2);
                if (!File.Exists(file2))
                    throw new TestUtilsException(string.Format("Файл <{0}> из архива <{1}> не имеет соответсвия в архиве <{2}>", file1, archive7z_path1, archive7z_path2));

                if (!comparer(file1, file2))
                    throw new TestUtilsException(string.Format("Сравнение файлов <{0}> и <{1}> по содержимому показало их различие.", file1, file2));
            }

            return true;
        }

        public static bool compareSets<T>(IEnumerable<T> set1, IEnumerable<T> set2)
        {
            T[] set1Arr = set1.ToArray();
            T[] set2Arr = set2.ToArray();

            if (set1Arr.Intersect(set2Arr).Count() != set1Arr.Count())
                return false;

            return set2Arr.Intersect(set1Arr).Count() == set1Arr.Count();
        }
    }
}
