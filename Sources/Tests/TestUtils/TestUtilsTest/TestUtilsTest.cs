﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Store;

using TestUtils;
using IOController;

namespace TestUtilsTest
{
    /// <summary>
    /// Класс проверяющий работу инфраструктуры тестирования
    /// </summary>
    [TestClass]
    public class TestUtilsTest
    {
        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);
        }

        /// <summary>
        /// Делаем то, что необходимо сделать после запуска каждого теста
        /// </summary>
        [TestCleanup]
        public void EachTest_CleanUp()
        {
        }

        /// <summary>
        /// Проверка работоспособности раннеров для всех плагинов (на настройках по умолчанию)
        /// </summary>
        [TestMethod]
        public void TestUtilsTest_Runners()
        {
            //Создаём Хранилище
            Storage storage = testUtils.Storage_CreateAndOpen();

            try
            {
                //Запускаем плагин - FillFileList
                TestUtilsClass.Run_FillFileList(storage, Path.Combine(testUtils.TestMatirialsDirectoryPath, @"TestUtilsTest\Runners\src_clear\"));

                //Запускаем плагин - Идентификация файлов
                TestUtilsClass.Run_IdentifyFileTypes(storage);

                //Запускаем плагин - Индексирование файлов
                TestUtilsClass.Run_IndexingFileContent(storage);

                //Запускаем плагин - Анализ указателей
                TestUtilsClass.Run_PointsToAnalysesSimple(storage);

                ////Запускаем плагин - Парсер C#
                //TestUtilsClass.Run_CSharpParser(storage, Path.Combine(testUtils.TestMatirialsDirectoryPath, @"TestUtilsTest\Runners\src_builded\C#"));

                //Запускаем плагин - Парсер VB(S)
                //TestUtilsClass.Run_VBParser(storage);

                //Запускаем плагин - Парсер PHP
                TestUtilsClass.Run_PhpParser(storage);

                //Запускаем плагин - Прасер JavaScript
                //TestUtilsClass.Run_JavaScriptParser(storage);
            }
            finally
            {
                //Закрываем Хранилище
                storage.Close();
            }

            //Удаляем (мусор) папку теста!
            DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }
    }
}
