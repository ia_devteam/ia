﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Windows.Forms;

using Store;
using Store.Table;

using TestUtils;

using IOController;

namespace IA.Tests.GlobalTestInterface
{
    /// <summary>
    /// Класс, реализующий универсальный тест набора плагинов
    /// </summary>
    [TestClass]
    public class TestPluginUniversal
    {
        private TestContext testContextInstance;
        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Список всех плагинов в виде их типов
        /// </summary>
        private static List<Type> pluginTypes = new List<Type>();

        /// <summary>
        /// Список всех плагинов
        /// </summary>
        private List<Plugin.Interface> plugins = new List<Plugin.Interface>();

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском всех тестов
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize]
        public static void AllTests_Initialize(TestContext testContext)
        {
            //Устанавливаем соединения с базой данных
            TestUtilsClass.SQL_SetupTestConnection();

            //Заполняем список типов плагинов
            foreach (string file in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll"))
            {
                try
                {
                    pluginTypes.AddRange(Assembly.LoadFile(file).GetTypes().Where(type => type.BaseType != null && type.GetInterface("IA.Plugin.Interface") != null));
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать после запуска всех тестов
        /// </summary>
        [ClassCleanup]
        public static void AllTests_Cleanup()
        {
            //Очищаем список типов плагинов
            pluginTypes.Clear();
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Инициализация экземпляра тестирования для набора тестов в рамках данного класса
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            //Формируем список всех плагинов
            foreach (Type pluginType in pluginTypes.Where(x => !x.IsAbstract))
                //Добавляем плагин в список плагинов
                plugins.Add(Activator.CreateInstance(pluginType) as Plugin.Interface);
        }

        /// <summary>
        /// Делаем то, что необходимо сделать после запуска каждого
        /// </summary>
        [TestCleanup]
        public void EachTest_Cleanup()
        {
            //Очищаем список плагинов
            plugins.Clear();

            //Удаляем рабочую директорию теста
            DirectoryController.Remove(testUtils.TestRunDirectoryPath);
        }

        /// <summary>
        /// Проверка сценария {ID}
        /// </summary>
        [TestMethod]
        public void Plugin_ID()
        {
            bool result = true;

            foreach (Plugin.Interface plugin in plugins)
            {
                try
                {
                    var id = plugin.ID;
                }
                catch (Exception ex)
                {
                    //Отображаем ошибку
                    Trace.WriteLine(ex.Message, plugin.Name);

                    result = false;
                }
            }

            Assert.IsTrue(result);
        }

        /// <summary>
        /// Проверка сценария {Name}
        /// </summary>
        [TestMethod]
        public void Plugin_Name()
        {
            bool result = true;

            foreach (Plugin.Interface plugin in plugins)
            {
                try
                {
                    var name = plugin.Name;
                }
                catch (Exception ex)
                {
                    //Отображаем ошибку
                    Trace.WriteLine(ex.Message, plugin.Name);

                    result = false;
                }
            }

            Assert.IsTrue(result);
        }

        /// <summary>
        /// Проверка сценария {Capabilities}
        /// </summary>
        [TestMethod]
        public void Plugin_Capabilities()
        {
            bool result = true;

            foreach (Plugin.Interface plugin in plugins)
            {
                try
                {
                    var capabilities = plugin.Capabilities;
                }
                catch (Exception ex)
                {
                    //Отображаем ошибку
                    Trace.WriteLine(ex.Message, plugin.Name);

                    result = false;
                }
            }

            Assert.IsTrue(result);
        }

        /// <summary>
        /// Проверка сценария {Initialize -> SaveSettings -> Initialize}
        /// </summary>
        [TestMethod]
        public void Plugin_Initialize_SaveSettings_Initialize()
        {
            bool result = true;

            foreach (Plugin.Interface plugin in plugins)
            {
                //Формируем Хранилище
                Storage storage = testUtils.Storage_CreateAndOpen(plugin.ID.ToString());
                
                //Формируем буфер чтения
                IBufferReader bufferReader = null;

                //Формируем буфер записи
                IBufferWriter bufferWriter = WriterPool.Get();

                try
                {
                    //Инициализируем плагин
                    plugin.Initialize(storage);

                    //Сохраняем настройки в Хранилище
                    plugin.SaveSettings(bufferWriter);
                    storage.pluginSettings.SaveSettings(plugin.ID, bufferWriter);

                    //Выгружаем настройки из Хранилища
                    bufferReader = storage.pluginSettings.LoadSettings(plugin.ID);
                    plugin.LoadSettings(bufferReader);       
                }
                catch (Exception ex)
                {
                    //Отображаем ошибку
                    Trace.WriteLine(ex.Message, plugin.Name);

                    result = false;
                }
                finally
                {
                    //Удаляем Хранилище
                    testUtils.Storage_Remove(ref storage);

                    //Уничтожаем буфер чтения настроек плагина
                    if (bufferReader != null)
                        bufferReader.Dispose();

                    //Уничтожаем буфер записи настроек плагина
                    if (bufferWriter != null)
                        bufferWriter.Dispose();
                }
            }

            Assert.IsTrue(result);
        }

        /// <summary>
        /// Проверка сценария {Initialize -> IsSettingsCorrect -> Tasks}
        /// </summary>
        [TestMethod]
        public void Plugin_Initialize_IsSettingsCorrect_Tasks()
        {
            bool result = true;

            foreach (Plugin.Interface plugin in plugins)
            {
                //Формируем Хранилище
                Storage storage = testUtils.Storage_CreateAndOpen(plugin.ID.ToString());

                try
                {
                    //Инициализируем плагин
                    plugin.Initialize(storage);

                    //Проверяем настройки плагина (формально на то, что функция не падает)
                    string message;
                    plugin.IsSettingsCorrect(out message);

                    //Получаем задачи плагина при выполнении
                    List<IA.Monitor.Tasks.Task> tasks = plugin.Tasks;
                }
                catch (Exception ex)
                {
                    //Отображаем ошибку
                    Trace.WriteLine(ex.Message, plugin.Name);

                    result = false;
                }
                finally
                {
                    //Удаляем Хранилище
                    testUtils.Storage_Remove(ref storage);
                }
            }

            Assert.IsTrue(result);
        }

        /// <summary>
        /// Проверка сценария {Initialize -> CustomSettingsWindow -> IsSettingsCorrect -> SaveSettings -> Tasks}
        /// </summary>
        [TestMethod]
        public void Plugin_Initialize_CustomSettingsWindow_IsSettingsCorrect_SaveSettings_Tasks()
        {
            bool result = true;

            foreach (Plugin.Interface plugin in plugins)
            {
                //Формируем Хранилище
                Storage storage = testUtils.Storage_CreateAndOpen(plugin.ID.ToString());

                //Формируем буфер записи
                IBufferWriter bufferWriter = WriterPool.Get();

                try
                {
                    //Инициализируем плагин
                    plugin.Initialize(storage);

                    //Получаем окно основных настроек плагина
                    UserControl customSettingsWindow = plugin.CustomSettingsWindow;

                    //Проверка на разумность
                    if ((customSettingsWindow != null) != plugin.Capabilities.HasFlag(Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW))
                        throw new Exception("Ошибка в списке возможностей плагина: " + Plugin.Capabilities.CUSTOM_SETTINGS_WINDOW.ToString());

                    //Проверяем настройки плагина (формально на то, что функция не падает)
                    string message;
                    plugin.IsSettingsCorrect(out message);

                    //Сохраняем настройки плагина
                    plugin.SaveSettings(bufferWriter);

                    //Получаем задачи плагина при выполнении
                    List<Monitor.Tasks.Task> tasks = plugin.Tasks;
                }
                catch (Exception ex)
                {
                    //Отображаем ошибку
                    Trace.WriteLine(ex.Message, plugin.Name);

                    result = false;
                }
                finally
                {
                    //Удаляем Хранилище
                    testUtils.Storage_Remove(ref storage);

                    //Уничтожаем буфер записи настроек плагина
                    if (bufferWriter != null)
                        bufferWriter.Dispose();
                }
            }

            Assert.IsTrue(result);
        }

        /// <summary>
        /// Проверка сценария {Initialize -> SimpleSettingsWindow -> IsSettingsCorrect -> SaveSettings -> Tasks}
        /// </summary>
        [TestMethod]
        public void Plugin_Initialize_SimpleSettingsWindow_IsSettingsCorrect_SaveSettings_Tasks()
        {
            bool result = true;

            foreach (Plugin.Interface plugin in plugins)
            {
                //Формируем Хранилище
                Storage storage = testUtils.Storage_CreateAndOpen(plugin.ID.ToString());

                //Формируем буфер записи
                IBufferWriter bufferWriter = WriterPool.Get();

                try
                {
                    //Инициализируем плагин
                    plugin.Initialize(storage);

                    //Получаем окно основных настроек плагина
                    UserControl simpleSettingsWindow = plugin.SimpleSettingsWindow;

                    //Проверка на разумность
                    if ((simpleSettingsWindow != null) != plugin.Capabilities.HasFlag(Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW))
                        throw new Exception("Ошибка в списке возможностей плагина: " + Plugin.Capabilities.SIMPLE_SETTINGS_WINDOW.ToString());

                    //Проверяем настройки плагина (формально на то, что функция не падает)
                    string message;
                    plugin.IsSettingsCorrect(out message);

                    //Сохраняем настройки плагина
                    plugin.SaveSettings(bufferWriter);

                    //Получаем задачи плагина при выполнении
                    List<Monitor.Tasks.Task> tasks = plugin.Tasks;
                }
                catch (Exception ex)
                {
                    //Отображаем ошибку
                    Trace.WriteLine(ex.Message, plugin.Name);

                    result = false;
                }
                finally
                {
                    //Удаляем Хранилище
                    testUtils.Storage_Remove(ref storage);

                    //Уничтожаем буфер записи настроек плагина
                    if (bufferWriter != null)
                        bufferWriter.Dispose();
                }
            }

            Assert.IsTrue(result);
        }
    }
}
