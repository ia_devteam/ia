﻿using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using IOController;
using TestUtils;
using IA.SlnResolver;
using IA.Extensions;
using IA.SlnResolver.CS;

namespace TestProjectsValidation
{
    /// <summary>
    /// Класс тестирования настроек проектов
    /// </summary>
    [TestClass]
    public class TestProjectsValidation
    {
        /// <summary>
        /// Список конфигураций
        /// </summary>
        private static string[] configurationSet = new string[] { "debug", "release" };

        /// <summary>
        /// Список платформ для основного проекта IA
        /// </summary>
        private static string[] platformSetIA = new string[] { "x64" };

        /// <summary>
        /// Список платформ для основного проекта IA
        /// </summary>
        private static string[] platformSetWrapper = new string[] { "x86" };

        /// <summary>
        /// Список платформ
        /// </summary>
        private static string[] platformSetCommon = new string[] { "anycpu" };

        /// <summary>
        /// Путь до каталога с исходными текстами
        /// </summary>
        private static string sourcesDirectoryPath;

        /// <summary>
        /// Путь до каталога с бинарными файлами
        /// </summary>
        private static string binariesDirectoryPath;

        /// <summary>
        /// Путь до файла решения
        /// </summary>
        private static string solutionFilePath;

        /// <summary>
        /// Экземпляр инфраструктуры тестирования для каждого теста
        /// </summary>
        private TestUtilsClass testUtils;

        /// <summary>
        /// Парсер проектов
        /// </summary>
        private static SolutionVS solutionParser;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        private static TestContext testContextInstance;

        /// <summary>
        /// Информация о текущем тесте
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Делаем то, что необходимо сделать перед запуском каждого теста
        /// </summary>
        [TestInitialize]
        public void EachTest_Initialize()
        {
            //Иницииализация инфраструктуры тестирования для каждого теста
            testUtils = new TestUtilsClass(testContextInstance.TestName);

            string rootOfProjectBuild = Path.GetFullPath(Path.Combine(testUtils.TestMatirialsDirectoryPath, @"..\.."));

            solutionFilePath = Directory.EnumerateFiles(rootOfProjectBuild, "IABuild.sln", SearchOption.AllDirectories).FirstOrDefault();
            Assert.IsNotNull(solutionFilePath, "Не найдено основное решение ПО.");
            sourcesDirectoryPath = Path.GetDirectoryName(solutionFilePath);

            binariesDirectoryPath = Path.GetFullPath(Path.Combine(sourcesDirectoryPath, @"..\Binaries"));
            Assert.IsTrue(Directory.Exists(binariesDirectoryPath), "Не найден каталог с результатами сборки ПО (<" + binariesDirectoryPath + ">)");

            solutionParser = new SolutionVS();
        }


        /// <summary>
        /// Возвращает словарь настроек конфигураций для проектов.
        /// </summary>
        /// <param name="fail">true - возникла ошибка парсинга проектов.</param>
        /// <returns>Словарь настроек конфигураций.</returns>
        private Dictionary<ProjectBase, List<ProjectConfiguration>> GetCSConfigurations(out bool fail)
        {
            fail = false;
            Dictionary<ProjectBase, List<ProjectConfiguration>> projectDictionary = new Dictionary<ProjectBase, List<ProjectConfiguration>>();

            if (!string.IsNullOrEmpty(solutionFilePath) && File.Exists(solutionFilePath))
            {
                foreach (ProjectBase project in GetCSProjectSettings(solutionFilePath, out fail))
                {
                    projectDictionary.Add(project, project.Settings.ConfigurationArray);
                }
            }

            return projectDictionary;
        }

        /// <summary>
        /// Возвращает настройки всех проектов решения.
        /// </summary>
        /// <param name="solutionFilePath">Путь к файлу решения. Не может быть пустым или null.</param>
        /// <param name="fail">true - возникла ошибка парсинга проектов.</param>
        /// <returns>Список настроек.</returns>
        private List<ProjectBase> GetCSProjectSettings(string solutionFilePath, out bool fail)
        {
            fail = false;
            List<ProjectBase> projects = new List<ProjectBase>();

            List<string> projectFilesPaths = solutionParser.GetAllProjects(solutionFilePath);

            foreach (string projectFilePath in projectFilesPaths)
            {
                if (Path.GetExtension(projectFilePath).Equals(".csproj"))
                {
                    try
                    {
                        projects.Add(ProjectVS.ParseCSProject(projectFilePath));
                    }
                    catch
                    {
                        Trace.WriteLine("Ошибка в настройках проекта: <" + projectFilePath + ">.");

                        fail = true;
                    }
                }
            }

            return projects;

            //FIXME - будущая заделка для VC++
            //if (Path.GetExtension(projectPath).Equals(".vcxproj"))
            //{
            //    SlnResolver.VC.ProjectBase project = ProjectVS.ParseVCProject(projectPath);
            //    List<SlnResolver.VC.ProjectConfiguration> configurations = project.Configurations;
            //    Assert.IsTrue(configurations.Count != configurationSet.Length, "Неверное количество конфигураций: <" + project.Name + ">.");            
            //}
        }

        /// <summary>
        /// Тест на существование проектных файлов
        /// </summary>
        [TestMethod]
        public void ProjectsValidation_Projects()
        {
            List<string> projectPaths = solutionParser.GetAllProjects(solutionFilePath);

            bool areNotNullAndExist = projectPaths != null && projectPaths.Count != 0 && projectPaths.All(proj => File.Exists(proj));

            Assert.IsTrue(areNotNullAndExist, "Не найдены проектные файлы.");

            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// Проверка присутствия необходимых конфигураций
        /// </summary>
        [TestMethod]
        public void ProjectsValidation_MissingConfigurations()
        {
            bool fail = false;
            Dictionary<ProjectBase, List<ProjectConfiguration>> projectDictionary = GetCSConfigurations(out fail);

            foreach (var project in projectDictionary.Keys)
            {
                IEnumerable<string> projectConfigurationNamesEnum = projectDictionary[project].Select(proj => proj.ConfigurationName);

                List<string> missingConfigurations = new List<string>();

                configurationSet.ForEach(elem =>
                {
                    if (!projectConfigurationNamesEnum.Contains(elem))
                        missingConfigurations.Add(elem);
                });

                if (missingConfigurations.Count == 0)
                    continue;

                Trace.WriteLine("Нет необходимых конфигураций проекта: <" + string.Join("|", missingConfigurations) + ">. Проект: <" + project.Name + ">.");

                fail = true;
            }

            Assert.IsFalse(fail);

            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// Проверка имен конфигураций
        /// </summary>
        [TestMethod]
        public void ProjectsValidation_InvalidConfigurationNames()
        {
            bool fail = false;
            Dictionary<ProjectBase, List<ProjectConfiguration>> projectDictionary = GetCSConfigurations(out fail);

            foreach (var project in projectDictionary.Keys)
            {
                foreach (ProjectConfiguration configuration in projectDictionary[project])
                {
                    if (configurationSet.Any(set => configuration.ConfigurationName.Equals(set)))
                        continue;

                    if ((project.Name.Contains("IAControls") || project.Name.Contains("IAAdministration")) && configuration.ConfigurationName.Equals("release_certified"))
                        continue;

                    Trace.WriteLine("Присутствует неверная конфигурация проекта: <" + configuration.ConfigurationName + ">. Проект <" + project.Name + ">.");

                    fail = true;
                }
            }

            Assert.IsFalse(fail);

            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// Проверка платформ сборки
        /// </summary>
        [TestMethod]
        public void ProjectsValidation_InvalidConfigurationPlatforms()
        {
            bool fail = false;
            Dictionary<ProjectBase, List<ProjectConfiguration>> projectDictionary = GetCSConfigurations(out fail);

            foreach (var project in projectDictionary.Keys)
            {
                foreach (ProjectConfiguration configuration in projectDictionary[project])
                {
                    if (project.Name == "GraphVizWrapper.csproj")
                    {
                        if (platformSetWrapper.Any(set => configuration.ConfigurationPlatform.Equals(set)))
                            continue;
                    }
                    else
                        if (project.Name.Contains("IA.csproj"))
                        {
                            if (platformSetIA.Any(set => configuration.ConfigurationPlatform.Equals(set)))
                                continue;
                        }
                        else
                            if (platformSetCommon.Any(set => configuration.ConfigurationPlatform.Equals(set)))
                                continue;

                    Trace.WriteLine("Присутствует неверная платформа сборки проекта: <" + configuration.ConfigurationPlatform + ">. Проект <" + project.Name + ">, конфигурация: <" + configuration.ConfigurationName + ">.");

                    fail = true;
                }
            }

            Assert.IsFalse(fail);

            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// Проверка выходного каталога
        /// </summary>
        [TestMethod]
        public void ProjectsValidation_InvalidConfigurationOutputDirectory()
        {
            bool fail = false;
            Dictionary<ProjectBase, List<ProjectConfiguration>> projectDictionary = GetCSConfigurations(out fail);

            foreach (var project in projectDictionary.Keys)
            {
                foreach (ProjectConfiguration configuration in projectDictionary[project])
                {
                    string outputDirectory = Path.GetDirectoryName(Path.Combine(project.Path, configuration.OutputPath));
                    if (outputDirectory.ToLower().StartsWith(binariesDirectoryPath.ToLower()))
                        continue;

                    Trace.WriteLine("Неверных выходной каталог: <" + configuration.OutputPath + ">. Проект: <" + project.Name + ">, конфигурация: <" + configuration.ConfigurationName + ">, платформа: <" + configuration.ConfigurationPlatform + ">.");

                    fail = true;
                }
            }

            Assert.IsFalse(fail);

            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }

        /// <summary>
        /// Тест на присутствие документации к проекту (XML-файл) только в режиме DEBUG
        /// </summary>
        [TestMethod]
        public void ProjectsValidation_DocumentationPath()
        {
            bool fail = false;
            Dictionary<ProjectBase, List<ProjectConfiguration>> projectDictionary = GetCSConfigurations(out fail);

            foreach (var project in projectDictionary.Keys)
            {
                foreach (ProjectConfiguration configuration in projectDictionary[project])
                {
                    switch (configuration.ConfigurationName)
                    {
                        case "debug":
                            if (!string.IsNullOrEmpty(configuration.DocumentationPath))
                                continue;

                            Trace.WriteLine("В проекте <" + project.Name + "> не указан путь до документации проекта в конфигурации DEBUG.");

                            break;
                        case "release":
                            if (string.IsNullOrEmpty(configuration.DocumentationPath))
                                continue;

                            Trace.WriteLine("В проекте <" + project.Name + "> присутствует путь до документации проекта в конфигурации RELEASE.");

                            break;
                        default:
                            if ((project.Name.Contains("IAControls") || project.Name.Contains("IAAdministration")) && configuration.ConfigurationName.Equals("release_certified"))
                                continue;

                            Trace.WriteLine("В проекте <" + project.Name + "> присутствует неверная конфигурация: <" + configuration.ConfigurationName + ">.");
                            break;
                    }

                    fail = true;
                }
            }

            Assert.IsFalse(fail);

            if (DirectoryController.IsExists(testUtils.TestRunDirectoryPath))
                DirectoryController.Remove(testUtils.TestRunDirectoryPath, true);
        }
    }
}
