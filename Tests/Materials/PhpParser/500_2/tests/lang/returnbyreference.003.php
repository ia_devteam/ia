
<?php
function returnConstantByValue() {
	$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2877\n"); fclose($RNThandle); return $RNTRNTRNT;
}

function &returnConstantByRef() {
	$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2878\n"); fclose($RNThandle); return $RNTRNTRNT;
}

function &returnVariableByRef() {
	$RNTRNTRNT = $GLOBALS['a']; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2879\n"); fclose($RNThandle); return $RNTRNTRNT;
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2875\n"); fclose($RNThandle); echo "\n---> 1. Trying to assign by reference the return value of a function that returns by value:\n";
unset($a, $b);
$a = 4;
$b = &returnConstantByValue();
$a++;
var_dump($a, $b);

echo "\n---> 2. Trying to assign by reference the return value of a function that returns a constant by ref:\n";
unset($a, $b);
$a = 4;
$b = &returnConstantByRef();
$a++;
var_dump($a, $b);

echo "\n---> 3. Trying to assign by reference the return value of a function that returns by ref:\n";
unset($a, $b);
$a = 4;
$b = &returnVariableByRef();
$a++;
var_dump($a, $b); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2876\n"); fclose($RNThandle); 

?>
