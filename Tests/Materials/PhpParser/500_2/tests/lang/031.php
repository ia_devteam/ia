
<?php
// reported by php.net@alienbill.com
$arrayOuter = array("key1","key2");
$arrayInner = array("0","1");

print "Correct - with inner loop reset.\n";

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1854\n"); fclose($RNThandle); while(list(,$o) = each($arrayOuter)){ $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1855\n"); fclose($RNThandle); 
	reset($arrayInner);
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1857\n"); fclose($RNThandle); while(list(,$i) = each($arrayInner)){ $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1856\n"); fclose($RNThandle); 
	    	print "inloop $i for $o\n";
	}
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1858\n"); fclose($RNThandle); reset($arrayOuter);
reset($arrayInner);

print "What happens without inner loop reset.\n";

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1861\n"); fclose($RNThandle); while(list(,$o) = each($arrayOuter)){
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1859\n"); fclose($RNThandle); while(list(,$i) = each($arrayInner)){ $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1860\n"); fclose($RNThandle); 
		print "inloop $i for $o\n";
	}
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1862\n"); fclose($RNThandle); reset($arrayOuter);
reset($arrayInner);

print "What happens without inner loop reset but copy.\n";

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1865\n"); fclose($RNThandle); while(list(,$o) = each($arrayOuter)){
	$placeholder = $arrayInner;
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1863\n"); fclose($RNThandle); while(list(,$i) = each($arrayInner)){ $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1864\n"); fclose($RNThandle); 
		print "inloop $i for $o\n";
	}
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1866\n"); fclose($RNThandle); reset($arrayOuter);
reset($arrayInner);

print "What happens with inner loop reset over copy.\n";

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1869\n"); fclose($RNThandle); while(list(,$o) = each($arrayOuter)){
	$placeholder = $arrayInner;
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1867\n"); fclose($RNThandle); while(list(,$i) = each($placeholder)){ $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1868\n"); fclose($RNThandle); 
		print "inloop $i for $o\n";
	}
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1870\n"); fclose($RNThandle); reset($arrayOuter);
reset($arrayInner); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1871\n"); fclose($RNThandle); 
?>
