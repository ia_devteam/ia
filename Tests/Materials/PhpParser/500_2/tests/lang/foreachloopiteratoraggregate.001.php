
<?php
class EnglishMealIterator implements Iterator {
	private $pos=0;
	private $myContent=array("breakfast", "dinner", "tea");
	
	public function valid() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = $this->pos < count($this->myContent); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2529\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	public function next() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2530\n"); fclose($RNThandle); global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$this->pos++; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2531\n"); fclose($RNThandle); 
	}
	
	public function rewind() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2532\n"); fclose($RNThandle); global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$this->pos=0; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2533\n"); fclose($RNThandle); 
	}

	public function current() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = $this->myContent[$this->pos]; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2534\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	public function key() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = "meal " . $this->pos; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2535\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
}

class FrenchMealIterator implements Iterator {
	private $pos=0;
	private $myContent=array("petit dejeuner", "dejeuner", "gouter", "dinner");
	
	public function valid() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = $this->pos < count($this->myContent); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2536\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	public function next() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2537\n"); fclose($RNThandle); global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$this->pos++; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2538\n"); fclose($RNThandle); 
	}
	
	public function rewind() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2539\n"); fclose($RNThandle); global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$this->pos=0; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2540\n"); fclose($RNThandle); 
	}

	public function current() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = $this->myContent[$this->pos]; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2541\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	public function key() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = "meal " . $this->pos; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2542\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
}


Class EuropeanMeals implements IteratorAggregate {
	
	private $storedEnglishMealIterator;
	private $storedFrenchMealIterator;
	
	public function __construct() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2543\n"); fclose($RNThandle); $this->storedEnglishMealIterator = new EnglishMealIterator;
		$this->storedFrenchMealIterator = new FrenchMealIterator; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2544\n"); fclose($RNThandle); 
	}
	
	public function getIterator() {
		global $indent;
		echo "$indent--> " . __METHOD__  . "\n";
		
		//Alternate between English and French meals
		static $i = 0;
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2545\n"); fclose($RNThandle); if ($i++%2 == 0) {
			$RNTRNTRNT = $this->storedEnglishMealIterator; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2546\n"); fclose($RNThandle); return $RNTRNTRNT;
		} else {
			$RNTRNTRNT = $this->storedFrenchMealIterator; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2547\n"); fclose($RNThandle); return $RNTRNTRNT;
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2548\n"); fclose($RNThandle); 
	}
	
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2520\n"); fclose($RNThandle); $f = new EuropeanMeals;
var_dump($f);

echo "-----( Simple iteration 1: )-----\n";
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2522\n"); fclose($RNThandle); foreach ($f as $k=>$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2521\n"); fclose($RNThandle); echo "$k => $v\n";	
}
echo "-----( Simple iteration 2: )-----\n";
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2524\n"); fclose($RNThandle); foreach ($f as $k=>$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2523\n"); fclose($RNThandle); echo "$k => $v\n";	
}


$indent = " ";
echo "\n\n\n-----( Nested iteration: )-----\n";
$count=1;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2527\n"); fclose($RNThandle); foreach ($f as $k=>$v) {
	echo "\nTop level "  .  $count++ . ": \n"; 
	echo "$k => $v\n";
	$indent = "     ";
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2525\n"); fclose($RNThandle); foreach ($f as $k=>$v) {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2526\n"); fclose($RNThandle); echo "     $k => $v\n";	
	}
	$indent = " ";
} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2528\n"); fclose($RNThandle); 


?>
