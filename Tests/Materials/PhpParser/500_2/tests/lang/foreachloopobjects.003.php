
<?php

class C {
	public $a = "Original a";
	public $b = "Original b";
	public $c = "Original c";
	protected $d = "Original d";
	private $e = "Original e";
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2640\n"); fclose($RNThandle); echo "\nDirectly changing object values.\n";
$obj = new C;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2642\n"); fclose($RNThandle); foreach ($obj as $k=>$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2641\n"); fclose($RNThandle); $obj->$k="changed.$k";
	var_dump($v);
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2643\n"); fclose($RNThandle); var_dump($obj);

echo "\nModifying the foreach \$value.\n";
$obj = new C;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2645\n"); fclose($RNThandle); foreach ($obj as $k=>$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2644\n"); fclose($RNThandle); $v="changed.$k";
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2646\n"); fclose($RNThandle); var_dump($obj);


echo "\nModifying the foreach &\$value.\n";
$obj = new C;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2648\n"); fclose($RNThandle); foreach ($obj as $k=>&$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2647\n"); fclose($RNThandle); $v="changed.$k";
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2649\n"); fclose($RNThandle); var_dump($obj);

echo "\nAdding properties to an an object.\n";
$obj = new C;
$counter=0;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2653\n"); fclose($RNThandle); foreach ($obj as $v) {
	$newPropName = "new$counter";
	$obj->$newPropName = "Added property $counter";
     $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2650\n"); fclose($RNThandle); if ($counter++>10) {
    	echo "Loop detected\n";
    	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2651\n"); fclose($RNThandle); break;
    }
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2652\n"); fclose($RNThandle); var_dump($v);    
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2654\n"); fclose($RNThandle); var_dump($obj);

echo "\nAdding properties to an an object, using &\$value.\n";
$obj = new C;
$counter=0;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2658\n"); fclose($RNThandle); foreach ($obj as &$v) {
	$newPropName = "new$counter";
	$obj->$newPropName = "Added property $counter";
     $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2655\n"); fclose($RNThandle); if ($counter++>10) {
    	echo "Loop detected\n";
    	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2656\n"); fclose($RNThandle); break;    	
    }
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2657\n"); fclose($RNThandle); var_dump($v);    
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2659\n"); fclose($RNThandle); var_dump($obj);

echo "\nRemoving properties from an object.\n";
$obj = new C;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2661\n"); fclose($RNThandle); foreach ($obj as $v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2660\n"); fclose($RNThandle); unset($obj->a);
	unset($obj->b);
	unset($obj->c);	
	var_dump($v);
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2662\n"); fclose($RNThandle); var_dump($obj);

echo "\nRemoving properties from an object, using &\$value.\n";
$obj = new C;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2664\n"); fclose($RNThandle); foreach ($obj as &$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2663\n"); fclose($RNThandle); unset($obj->a);
	unset($obj->b);
	unset($obj->c);
	var_dump($v);
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2665\n"); fclose($RNThandle); var_dump($obj); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2666\n"); fclose($RNThandle); 

?>
