
<?php
class C {
	function returnConstantByValue() {
		$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2904\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	function &returnConstantByRef() {
		$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2905\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	function &returnVariableByRef() {
		$RNTRNTRNT = $GLOBALS['a']; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2906\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	function &returnFunctionCallByRef($functionToCall) {
		$RNTRNTRNT = $this->$functionToCall(); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2907\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2902\n"); fclose($RNThandle); $c = new C;

echo "\n---> 1. Via a return by ref function call, assign by reference the return value of a function that returns by value:\n";
unset($a, $b);
$a = 4;
$b = &$c->returnFunctionCallByRef('returnConstantByValue');
$a++;
var_dump($a, $b);

echo "\n---> 2. Via a return by ref function call, assign by reference the return value of a function that returns a constant by ref:\n";
unset($a, $b);
$a = 4;
$b = &$c->returnFunctionCallByRef('returnConstantByRef');
$a++;
var_dump($a, $b);

echo "\n---> 3. Via a return by ref function call, assign by reference the return value of a function that returns by ref:\n";
unset($a, $b);
$a = 4;
$b = &$c->returnFunctionCallByRef('returnVariableByRef');
$a++;
var_dump($a, $b); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2903\n"); fclose($RNThandle); 

?>
