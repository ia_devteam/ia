
<?php
function returnConstantByValue() {
	$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2892\n"); fclose($RNThandle); return $RNTRNTRNT;
}

function &returnConstantByRef() {
	$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2893\n"); fclose($RNThandle); return $RNTRNTRNT;
}

function &returnVariableByRef() {
	$RNTRNTRNT = $GLOBALS['a']; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2894\n"); fclose($RNThandle); return $RNTRNTRNT;
}

function &returnFunctionCallByRef($functionToCall) {
	$RNTRNTRNT = $functionToCall(); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2895\n"); fclose($RNThandle); return $RNTRNTRNT;
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2890\n"); fclose($RNThandle); echo "\n---> 1. Via a return by ref function call, assign by reference the return value of a function that returns by value:\n";
unset($a, $b);
$a = 4;
$b = &returnFunctionCallByRef('returnConstantByValue');
$a++;
var_dump($a, $b);

echo "\n---> 2. Via a return by ref function call, assign by reference the return value of a function that returns a constant by ref:\n";
unset($a, $b);
$a = 4;
$b = &returnFunctionCallByRef('returnConstantByRef');
$a++;
var_dump($a, $b);

echo "\n---> 3. Via a return by ref function call, assign by reference the return value of a function that returns by ref:\n";
unset($a, $b);
$a = 4;
$b = &returnFunctionCallByRef('returnVariableByRef');
$a++;
var_dump($a, $b); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2891\n"); fclose($RNThandle); 

?>
