
<?php

echo "\nDirectly changing array values.\n";
$a = array("original.1","original.2","original.3");
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2343\n"); fclose($RNThandle); foreach ($a as $k=>$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2344\n"); fclose($RNThandle); $a[$k]="changed.$k";
	var_dump($v);
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2345\n"); fclose($RNThandle); var_dump($a);

echo "\nModifying the foreach \$value.\n";
$a = array("original.1","original.2","original.3");
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2347\n"); fclose($RNThandle); foreach ($a as $k=>$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2346\n"); fclose($RNThandle); $v="changed.$k";
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2348\n"); fclose($RNThandle); var_dump($a);


echo "\nModifying the foreach &\$value.\n";
$a = array("original.1","original.2","original.3");
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2350\n"); fclose($RNThandle); foreach ($a as $k=>&$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2349\n"); fclose($RNThandle); $v="changed.$k";
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2351\n"); fclose($RNThandle); var_dump($a);

echo "\nPushing elements onto an unreferenced array.\n";
$a = array("original.1","original.2","original.3");
$counter=0;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2355\n"); fclose($RNThandle); foreach ($a as $v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2352\n"); fclose($RNThandle); array_push($a, "new.$counter");

	//avoid infinite loop if test is failing
     $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2354\n"); fclose($RNThandle); if ($counter++>10) {
    	echo "Loop detected\n";
    	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2353\n"); fclose($RNThandle); break;    	
    }
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2356\n"); fclose($RNThandle); var_dump($a);

echo "\nPushing elements onto an unreferenced array, using &\$value.\n";
$a = array("original.1","original.2","original.3");
$counter=0;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2360\n"); fclose($RNThandle); foreach ($a as &$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2357\n"); fclose($RNThandle); array_push($a, "new.$counter");

	//avoid infinite loop if test is failing
     $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2359\n"); fclose($RNThandle); if ($counter++>10) {
    	echo "Loop detected\n";
    	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2358\n"); fclose($RNThandle); break;    	
    }	
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2361\n"); fclose($RNThandle); var_dump($a);

echo "\nPopping elements off an unreferenced array.\n";
$a = array("original.1","original.2","original.3");
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2363\n"); fclose($RNThandle); foreach ($a as $v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2362\n"); fclose($RNThandle); array_pop($a);
	var_dump($v);	
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2364\n"); fclose($RNThandle); var_dump($a);

echo "\nPopping elements off an unreferenced array, using &\$value.\n";
$a = array("original.1","original.2","original.3");
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2366\n"); fclose($RNThandle); foreach ($a as &$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2365\n"); fclose($RNThandle); array_pop($a);
	var_dump($v);
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2367\n"); fclose($RNThandle); var_dump($a); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2368\n"); fclose($RNThandle); 

?>
