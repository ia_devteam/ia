
<?php $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2427\n"); fclose($RNThandle); 

define('MAX_LOOPS',5);

function withRefValue($elements, $transform) {
	echo "\n---( Array with $elements element(s): )---\n";
	//Build array:
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2429\n"); fclose($RNThandle); for ($i=0; $i<$elements; $i++) { $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2430\n"); fclose($RNThandle); 
		$a[] = "v.$i";
	}
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2431\n"); fclose($RNThandle); $counter=0;
	
	echo "--> State of array before loop:\n";
	var_dump($a);
	
	echo "--> Do loop:\n";	
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2434\n"); fclose($RNThandle); foreach ($a as $k=>&$v) {
		echo "     iteration $counter:  \$k=$k; \$v=$v\n";
		eval($transform);
		$counter++;
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2432\n"); fclose($RNThandle); if ($counter>MAX_LOOPS) {
			echo "  ** Stuck in a loop! **\n";
			 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2433\n"); fclose($RNThandle); break;
		}
	}
	
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2435\n"); fclose($RNThandle); echo "--> State of array after loop:\n";
	var_dump($a); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2436\n"); fclose($RNThandle); 
}


echo "\nPopping elements off end of an unreferenced array, using &\$value.";
$transform = 'array_pop($a);';
withRefValue(1, $transform);
withRefValue(2, $transform);
withRefValue(3, $transform);
withRefValue(4, $transform);

echo "\n\n\nShift elements off start of an unreferenced array, using &\$value.";
$transform = 'array_shift($a);';
withRefValue(1, $transform);
withRefValue(2, $transform);
withRefValue(3, $transform);
withRefValue(4, $transform);

echo "\n\n\nRemove current element of an unreferenced array, using &\$value.";
$transform = 'unset($a[$k]);';
withRefValue(1, $transform);
withRefValue(2, $transform);
withRefValue(3, $transform);
withRefValue(4, $transform);

echo "\n\n\nAdding elements to the end of an unreferenced array, using &\$value.";
$transform = 'array_push($a, "new.$counter");';
withRefValue(1, $transform);
withRefValue(2, $transform);
withRefValue(3, $transform);
withRefValue(4, $transform);

echo "\n\n\nAdding elements to the start of an unreferenced array, using &\$value.";
$transform = 'array_unshift($a, "new.$counter");';
withRefValue(1, $transform);
withRefValue(2, $transform);
withRefValue(3, $transform);
withRefValue(4, $transform); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2428\n"); fclose($RNThandle); 

?>
