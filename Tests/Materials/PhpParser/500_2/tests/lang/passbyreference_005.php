
<?php

function v($val) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2806\n"); fclose($RNThandle); $val = "Val changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2807\n"); fclose($RNThandle); 
}

function r(&$ref) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2808\n"); fclose($RNThandle); $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2809\n"); fclose($RNThandle); 
}


function vv($val1, $val2) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2810\n"); fclose($RNThandle); $val1 = "Val1 changed";
  $val2 = "Val2 changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2811\n"); fclose($RNThandle); 
}

function vr($val, &$ref) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2812\n"); fclose($RNThandle); $val = "Val changed";
  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2813\n"); fclose($RNThandle); 
}

function rv(&$ref, $val) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2814\n"); fclose($RNThandle); $val = "Val changed";
  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2815\n"); fclose($RNThandle); 
}

function rr(&$ref1, &$ref2) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2816\n"); fclose($RNThandle); $ref1 = "Ref1 changed";
  $ref2 = "Ref2 changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2817\n"); fclose($RNThandle); 
}


class C {

	function __construct($val, &$ref) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2818\n"); fclose($RNThandle); $val = "Val changed";
	  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2819\n"); fclose($RNThandle); 
	}

	function v($val) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2820\n"); fclose($RNThandle); $val = "Val changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2821\n"); fclose($RNThandle); 
	}
	
	function r(&$ref) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2822\n"); fclose($RNThandle); $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2823\n"); fclose($RNThandle); 
	}
	
	function vv($val1, $val2) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2824\n"); fclose($RNThandle); $val1 = "Val1 changed";
	  $val2 = "Val2 changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2825\n"); fclose($RNThandle); 
	}
	
	function vr($val, &$ref) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2826\n"); fclose($RNThandle); $val = "Val changed";
	  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2827\n"); fclose($RNThandle); 
	}
	
	function rv(&$ref, $val) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2828\n"); fclose($RNThandle); $val = "Val changed";
	  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2829\n"); fclose($RNThandle); 
	}
	
	function rr(&$ref1, &$ref2) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2830\n"); fclose($RNThandle); $ref1 = "Ref1 changed";
	  $ref2 = "Ref2 changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2831\n"); fclose($RNThandle); 
	}

} 

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2804\n"); fclose($RNThandle); echo "\n ---- Pass by ref / pass by val: functions ----\n";
unset($u1, $u2);
v($u1);
r($u2);
var_dump($u1, $u2);

unset($u1, $u2);
vv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
vr($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
rv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
rr($u1, $u2);
var_dump($u1, $u2);


echo "\n\n ---- Pass by ref / pass by val: static method calls ----\n";
unset($u1, $u2);
C::v($u1);
C::r($u2);
var_dump($u1, $u2);

unset($u1, $u2);
C::vv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
C::vr($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
C::rv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
C::rr($u1, $u2);
var_dump($u1, $u2);

echo "\n\n ---- Pass by ref / pass by val: instance method calls ----\n";
unset($u1, $u2);
$c = new C($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->v($u1);
$c->r($u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->vv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->vr($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->rv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->rr($u1, $u2);
var_dump($u1, $u2); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2805\n"); fclose($RNThandle); 

?>
