
<?php
// From php.net/foreach:
// "Unless the array is referenced, foreach operates on a copy of the specified array."

echo "\nRemove elements from a referenced array during loop\n";
$refedArray=array("original.0", "original.1", "original.2");
$ref=&$refedArray;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2393\n"); fclose($RNThandle); foreach ($refedArray as $k=>$v1) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2394\n"); fclose($RNThandle); array_pop($refedArray);
	echo "key: $k; value: $v1\n";
}

echo "\nRemove elements from a referenced array during loop, using &\$value\n";
$refedArray=array("original.0", "original.1", "original.2");
$ref=&$refedArray;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2396\n"); fclose($RNThandle); foreach ($refedArray as $k=>&$v2) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2395\n"); fclose($RNThandle); array_pop($refedArray);
	echo "key: $k; value: $v2\n";
}

echo "\nAdd elements to a referenced array during loop\n";
$refedArray=array("original.0", "original.1", "original.2");
$ref=&$refedArray;
$count=0;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2400\n"); fclose($RNThandle); foreach ($refedArray as $k=>$v3) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2397\n"); fclose($RNThandle); array_push($refedArray, "new.$k");
	echo "key: $k; value: $v3\n";
	
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2399\n"); fclose($RNThandle); if ($count++>5) {
		echo "Loop detected, as expected.\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2398\n"); fclose($RNThandle); break;
	}
}

echo "\nAdd elements to a referenced array during loop, using &\$value\n";
$refedArray=array("original.0", "original.1", "original.2");
$ref=&$refedArray;
$count=0;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2404\n"); fclose($RNThandle); foreach ($refedArray as $k=>&$v4) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2401\n"); fclose($RNThandle); array_push($refedArray, "new.$k");
	echo "key: $k; value: $v4\n";
	
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2403\n"); fclose($RNThandle); if ($count++>5) {
		echo "Loop detected, as expected.\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2402\n"); fclose($RNThandle); break;
	}
} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2405\n"); fclose($RNThandle); 

?>
