
<?php

echo "\nSame variable used as static and non static.\n";
function staticNonStatic() {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2926\n"); fclose($RNThandle); echo "---------\n";	
	$a=0;
	echo "$a\n";	
	static $a=10;
	echo "$a\n";
	$a++; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2927\n"); fclose($RNThandle); 	
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2922\n"); fclose($RNThandle); staticNonStatic();
staticNonStatic();
staticNonStatic();

echo "\nLots of initialisations in the same statement.\n";
function manyInits() {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2928\n"); fclose($RNThandle); static $counter=0;
	echo "------------- Call $counter --------------\n";
	static $a, $b=10, $c=20, $d, $e=30;
	echo "Unitialised      : $a\n";
	echo "Initialised to 10: $b\n";
	echo "Initialised to 20: $c\n";
	echo "Unitialised      : $d\n";
	echo "Initialised to 30: $e\n";
	$a++;
	$b++;
	$c++;
	$d++;
	$e++;
	$counter++; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2929\n"); fclose($RNThandle); 
}
manyInits();
manyInits();
manyInits();

echo "\nUsing static keyword at global scope\n";
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2924\n"); fclose($RNThandle); for ($i=0; $i<3; $i++) { $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2923\n"); fclose($RNThandle); 
   static $s, $k=10;
   echo "$s $k\n";
   $s++;
   $k++;
} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2925\n"); fclose($RNThandle); 
?>
