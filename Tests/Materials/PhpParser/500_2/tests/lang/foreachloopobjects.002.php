
<?php

class C {
	public $a = "Original a";
	public $b = "Original b";
	public $c = "Original c";
	protected $d = "Original d";
	private $e = "Original e";

	function doForEachC() {
		echo "in C::doForEachC\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2619\n"); fclose($RNThandle); foreach ($this as $k=>&$v) {
			 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2620\n"); fclose($RNThandle); var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2621\n"); fclose($RNThandle); 
	}
		
	static function doForEach($obj) {
		echo "in C::doForEach\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2622\n"); fclose($RNThandle); foreach ($obj as $k=>&$v) {
			 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2623\n"); fclose($RNThandle); var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2624\n"); fclose($RNThandle); 
	}
	
	function doForEachOnThis() {
		echo "in C::doForEachOnThis\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2625\n"); fclose($RNThandle); foreach ($this as $k=>&$v) {
			 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2626\n"); fclose($RNThandle); var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2627\n"); fclose($RNThandle); 
	}
	
}

class D extends C {
	
	private $f = "Original f";
	protected $g = "Original g";
	
	static function doForEach($obj) {
		echo "in D::doForEach\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2628\n"); fclose($RNThandle); foreach ($obj as $k=>&$v) {
			 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2629\n"); fclose($RNThandle); var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2630\n"); fclose($RNThandle); 
	}

	function doForEachOnThis() {
		echo "in D::doForEachOnThis\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2631\n"); fclose($RNThandle); foreach ($this as $k=>&$v) {
			 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2632\n"); fclose($RNThandle); var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2633\n"); fclose($RNThandle); 
	}
}

class E extends D {
	public $a = "Overridden a";
	public $b = "Overridden b";
	public $c = "Overridden c";
	protected $d = "Overridden d";
	private $e = "Overridden e";	

	static function doForEach($obj) {
		echo "in E::doForEach\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2634\n"); fclose($RNThandle); foreach ($obj as $k=>&$v) {
			 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2635\n"); fclose($RNThandle); var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2636\n"); fclose($RNThandle); 
	}

	function doForEachOnThis() {
		echo "in E::doForEachOnThis\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2637\n"); fclose($RNThandle); foreach ($this as $k=>&$v) {
			 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2638\n"); fclose($RNThandle); var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2639\n"); fclose($RNThandle); 
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2608\n"); fclose($RNThandle); echo "\n\nIterate over various generations from within overridden methods:\n";
echo "\n--> Using instance of C:\n";
$myC = new C;
$myC->doForEachOnThis();
var_dump($myC);
echo "\n--> Using instance of D:\n";
$myD = new D;
$myD->doForEachOnThis();
var_dump($myD);
echo "\n--> Using instance of E:\n";
$myE = new E;
$myE->doForEachOnThis();
var_dump($myE);

echo "\n\nIterate over various generations from within an inherited method:\n";
echo "\n--> Using instance of C:\n";
$myC = new C;
$myC->doForEachC();
var_dump($myC);
echo "\n--> Using instance of D:\n";
$myD = new D;
$myD->doForEachC();
var_dump($myD);
echo "\n--> Using instance of E:\n";
$myE = new E;
$myE->doForEachC();
var_dump($myE);

echo "\n\nIterate over various generations from within an overridden static method:\n";
echo "\n--> Using instance of C:\n";
$myC = new C;
C::doForEach($myC);
var_dump($myC);
$myC = new C;
D::doForEach($myC);
var_dump($myC);
$myC = new C;
E::doForEach($myC);
var_dump($myC);
echo "\n--> Using instance of D:\n";
$myD = new D;
C::doForEach($myD);
var_dump($myD);
$myD = new D;
D::doForEach($myD);
var_dump($myD);
$myD = new D;
E::doForEach($myD);
var_dump($myD);
echo "\n--> Using instance of E:\n";
$myE = new E;
C::doForEach($myE);
var_dump($myE);
$myE = new E;
D::doForEach($myE);
var_dump($myE);
$myE = new E;
E::doForEach($myE);
var_dump($myE);


echo "\n\nIterate over various generations from outside the object:\n";
echo "\n--> Using instance of C:\n";
$myC = new C;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2610\n"); fclose($RNThandle); foreach ($myC as $k=>&$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2609\n"); fclose($RNThandle); var_dump($v);
	$v="changed.$k";
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2611\n"); fclose($RNThandle); var_dump($myC);
echo "\n--> Using instance of D:\n";
$myD = new D;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2613\n"); fclose($RNThandle); foreach ($myD as $k=>&$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2612\n"); fclose($RNThandle); var_dump($v);
	$v="changed.$k";
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2614\n"); fclose($RNThandle); var_dump($myD);
echo "\n--> Using instance of E:\n";
$myE = new E;
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2616\n"); fclose($RNThandle); foreach ($myE as $k=>&$v) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2615\n"); fclose($RNThandle); var_dump($v);
	$v="changed.$k";
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2617\n"); fclose($RNThandle); var_dump($myE); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2618\n"); fclose($RNThandle); 
?>
