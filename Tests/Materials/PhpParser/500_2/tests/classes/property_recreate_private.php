
<?php
class C {
	private $p = 'test';
	function unsetPrivate() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1300\n"); fclose($RNThandle); unset($this->p); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1301\n"); fclose($RNThandle); 		
	}
	function setPrivate() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1302\n"); fclose($RNThandle); $this->p = 'changed'; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1303\n"); fclose($RNThandle); 		
	}
}

class D extends C {
	function setP() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1304\n"); fclose($RNThandle); $this->p = 'changed in D'; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1305\n"); fclose($RNThandle); 
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1298\n"); fclose($RNThandle); echo "Unset and recreate a superclass's private property:\n";
$d = new D;
$d->unsetPrivate();
$d->setPrivate();
var_dump($d);

echo "\nUnset superclass's private property, and recreate it as public in subclass:\n";
$d = new D;
$d->unsetPrivate();
$d->setP();
var_dump($d);

echo "\nUnset superclass's private property, and recreate it as public at global scope:\n";
$d = new D;
$d->unsetPrivate();
$d->p = 'this will create a public property';
var_dump($d);


echo "\n\nUnset and recreate a private property:\n";
$c = new C;
$c->unsetPrivate();
$c->setPrivate();
var_dump($c);

echo "\nUnset a private property, and attempt to recreate at global scope (expecting failure):\n";
$c = new C;
$c->unsetPrivate();
$c->p = 'this will fail';
var_dump($c); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1299\n"); fclose($RNThandle); 
?>
