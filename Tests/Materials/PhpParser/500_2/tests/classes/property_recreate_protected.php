
<?php
class C {
	protected $p = 'test';
	function unsetProtected() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1308\n"); fclose($RNThandle); unset($this->p); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1309\n"); fclose($RNThandle); 		
	}
	function setProtected() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1310\n"); fclose($RNThandle); $this->p = 'changed'; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1311\n"); fclose($RNThandle); 		
	}
}

class D extends C {
	function setP() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1312\n"); fclose($RNThandle); $this->p = 'changed in D'; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1313\n"); fclose($RNThandle); 
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1306\n"); fclose($RNThandle); $d = new D;
echo "Unset and recreate a protected property from property's declaring class scope:\n";
$d->unsetProtected();
$d->setProtected();
var_dump($d);

echo "\nUnset and recreate a protected property from subclass:\n";
$d = new D;
$d->unsetProtected();
$d->setP();
var_dump($d);

echo "\nUnset a protected property, and attempt to recreate it outside of scope (expected failure):\n";
$d->unsetProtected();
$d->p = 'this will fail';
var_dump($d); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1307\n"); fclose($RNThandle); 
?>
