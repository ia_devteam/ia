
<?php 

// NOTE: This will become part of SPL

interface ArrayProxyAccess extends ArrayAccess
{
	function proxyGet($element);
	function proxySet($element, $index, $value);
	function proxyUnset($element, $index);
}

class ArrayProxy implements ArrayAccess
{
	private $object;
	private $element;
	
	function __construct(ArrayProxyAccess $object, $element)
	{
		echo __METHOD__ . "($element)\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"258\n"); fclose($RNThandle); if (!$object->offsetExists($element))
		{ $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"259\n"); fclose($RNThandle); 
			$object[$element] = array();
		}
		$this->object = $object;
		$this->element = $element; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"260\n"); fclose($RNThandle); 
	}

	function offsetExists($index) {
		echo __METHOD__ . "($this->element, $index)\n";
		$RNTRNTRNT = array_key_exists($index, $this->object->proxyGet($this->element)); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"261\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetGet($index) {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"262\n"); fclose($RNThandle); echo __METHOD__ . "($this->element, $index)\n";
		$tmp = $this->object->proxyGet($this->element);
		$RNTRNTRNT = isset($tmp[$index]) ? $tmp[$index] : NULL; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"263\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetSet($index, $value) {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"264\n"); fclose($RNThandle); echo __METHOD__ . "($this->element, $index, $value)\n";
		$this->object->proxySet($this->element, $index, $value); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"265\n"); fclose($RNThandle); 
	}

	function offsetUnset($index) {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"266\n"); fclose($RNThandle); echo __METHOD__ . "($this->element, $index)\n";
		$this->object->proxyUnset($this->element, $index); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"267\n"); fclose($RNThandle); 
	}
}

class Peoples implements ArrayProxyAccess
{
	public $person;
	
	function __construct()
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"268\n"); fclose($RNThandle); $this->person = array(array('name'=>'Foo')); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"269\n"); fclose($RNThandle); 
	}

	function offsetExists($index)
	{
		$RNTRNTRNT = array_key_exists($index, $this->person); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"270\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetGet($index)
	{
		$RNTRNTRNT = new ArrayProxy($this, $index); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"271\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetSet($index, $value)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"272\n"); fclose($RNThandle); $this->person[$index] = $value; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"273\n"); fclose($RNThandle); 
	}

	function offsetUnset($index)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"274\n"); fclose($RNThandle); unset($this->person[$index]); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"275\n"); fclose($RNThandle); 
	}

	function proxyGet($element)
	{
		$RNTRNTRNT = $this->person[$element]; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"276\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function proxySet($element, $index, $value)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"277\n"); fclose($RNThandle); $this->person[$element][$index] = $value; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"278\n"); fclose($RNThandle); 
	}
	
	function proxyUnset($element, $index)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"279\n"); fclose($RNThandle); unset($this->person[$element][$index]); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"280\n"); fclose($RNThandle); 
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"256\n"); fclose($RNThandle); $people = new Peoples;

var_dump($people->person[0]['name']);
$people->person[0]['name'] = $people->person[0]['name'] . 'Bar';
var_dump($people->person[0]['name']);
$people->person[0]['name'] .= 'Baz';
var_dump($people->person[0]['name']);

echo "===ArrayOverloading===\n";

$people = new Peoples;

var_dump($people[0]);
var_dump($people[0]['name']);
$people[0]['name'] = 'FooBar';
var_dump($people[0]['name']);
$people[0]['name'] = $people->person[0]['name'] . 'Bar';
var_dump($people[0]['name']);
$people[0]['name'] .= 'Baz';
var_dump($people[0]['name']);
unset($people[0]['name']);
var_dump($people[0]);
var_dump($people[0]['name']);
$people[0]['name'] = 'BlaBla';
var_dump($people[0]['name']); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"257\n"); fclose($RNThandle); 

?>
