--TEST--
Ensure an interface may not shadow an inherited constant. 
--FILE--
<?php $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"851\n"); fclose($RNThandle); 
interface I1 {
	const FOO = 10;
}

interface I2 extends I1 {
	const FOO = 10;
}

echo "Done\n"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"852\n"); fclose($RNThandle); 
?>
