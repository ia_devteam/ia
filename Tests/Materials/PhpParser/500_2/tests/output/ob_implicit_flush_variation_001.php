
<?php
/* Prototype  : void ob_implicit_flush([int flag])
 * Description: Turn implicit flush on/off and is equivalent to calling flush() after every output call 
 * Source code: main/output.c
 * Alias to functions: 
 */

echo "*** Testing ob_implicit_flush() : usage variation ***\n";

// Define error handler
function test_error_handler($err_no, $err_msg, $filename, $linenum, $vars) {
	 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3409\n"); fclose($RNThandle); if (error_reporting() != 0) { $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3410\n"); fclose($RNThandle); 
		// report non-silenced errors
		echo "Error: $err_no - $err_msg, $filename($linenum)\n";
	} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3411\n"); fclose($RNThandle); 
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3405\n"); fclose($RNThandle); set_error_handler('test_error_handler');

// Initialise function arguments not being substituted (if any)

//get an unset variable
$unset_var = 10;
unset ($unset_var);

// define some classes
class classWithToString
{
	public function __toString() {
		$RNTRNTRNT = "Class A object"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3412\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
}

class classWithoutToString
{
}

// heredoc string
$heredoc = <<<EOT
hello world
EOT;

// add arrays
$index_array = array (1, 2, 3);
$assoc_array = array ('one' => 1, 'two' => 2);

//array of values to iterate over
$inputs = array(

      // float data
      'float 10.5' => 10.5,
      'float -10.5' => -10.5,
      'float 12.3456789000e10' => 12.3456789000e10,
      'float -12.3456789000e10' => -12.3456789000e10,
      'float .5' => .5,

      // array data
      'empty array' => array(),
      'int indexed array' => $index_array,
      'associative array' => $assoc_array,
      'nested arrays' => array('foo', $index_array, $assoc_array),

      // null data
      'uppercase NULL' => NULL,
      'lowercase null' => null,

      // boolean data
      'lowercase true' => true,
      'lowercase false' =>false,
      'uppercase TRUE' =>TRUE,
      'uppercase FALSE' =>FALSE,

      // empty data
      'empty string DQ' => "",
      'empty string SQ' => '',

      // string data
      'string DQ' => "string",
      'string SQ' => 'string',
      'mixed case string' => "sTrInG",
      'heredoc' => $heredoc,

      // object data
      'instance of classWithToString' => new classWithToString(),
      'instance of classWithoutToString' => new classWithoutToString(),

      // undefined data
      'undefined var' => @$undefined_var,

      // unset data
      'unset var' => @$unset_var,
);

// loop through each element of the array for flag

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3407\n"); fclose($RNThandle); foreach($inputs as $key =>$value) {
       $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3406\n"); fclose($RNThandle); echo "\n--$key--\n";
      var_dump( ob_implicit_flush($value) );
}; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3408\n"); fclose($RNThandle); 

?>
