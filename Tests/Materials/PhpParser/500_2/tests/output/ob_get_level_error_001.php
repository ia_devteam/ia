
<?php $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3395\n"); fclose($RNThandle); 
/* Prototype  : proto int ob_get_level(void)
 * Description: Return the nesting level of the output buffer 
 * Source code: main/output.c
 * Alias to functions: 
 */

echo "*** Testing ob_get_level() : error conditions ***\n";

// One argument
echo "\n-- Testing ob_get_level() function with one argument --\n";
$extra_arg = 10;;
var_dump( ob_get_level($extra_arg) );

echo "Done"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"3396\n"); fclose($RNThandle); 
?>
