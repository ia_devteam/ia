
<?php
class C {
	static function returnConstantByValue() {
		$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2070\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	static function &returnConstantByRef() {
		$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2071\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	static function &returnVariableByRef() {
		$RNTRNTRNT = $GLOBALS['a']; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2072\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	static function &returnFunctionCallByRef($functionToCall) {
		$RNTRNTRNT = C::$functionToCall(); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2073\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2068\n"); fclose($RNThandle); echo "\n---> 1. Via a return by ref function call, assign by reference the return value of a function that returns by value:\n";
unset($a, $b);
$a = 4;
$b = &C::returnFunctionCallByRef('returnConstantByValue');
$a++;
var_dump($a, $b);

echo "\n---> 2. Via a return by ref function call, assign by reference the return value of a function that returns a constant by ref:\n";
unset($a, $b);
$a = 4;
$b = &C::returnFunctionCallByRef('returnConstantByRef');
$a++;
var_dump($a, $b);

echo "\n---> 3. Via a return by ref function call, assign by reference the return value of a function that returns by ref:\n";
unset($a, $b);
$a = 4;
$b = &C::returnFunctionCallByRef('returnVariableByRef');
$a++;
var_dump($a, $b); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2069\n"); fclose($RNThandle); 

?>
