
<?php

function v($val) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1978\n"); fclose($RNThandle); $val = "Val changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1979\n"); fclose($RNThandle); 
}

function r(&$ref) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1980\n"); fclose($RNThandle); $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1981\n"); fclose($RNThandle); 
}


function vv($val1, $val2) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1982\n"); fclose($RNThandle); $val1 = "Val1 changed";
  $val2 = "Val2 changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1983\n"); fclose($RNThandle); 
}

function vr($val, &$ref) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1984\n"); fclose($RNThandle); $val = "Val changed";
  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1985\n"); fclose($RNThandle); 
}

function rv(&$ref, $val) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1986\n"); fclose($RNThandle); $val = "Val changed";
  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1987\n"); fclose($RNThandle); 
}

function rr(&$ref1, &$ref2) {
   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1988\n"); fclose($RNThandle); $ref1 = "Ref1 changed";
  $ref2 = "Ref2 changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1989\n"); fclose($RNThandle); 
}


class C {

	function __construct($val, &$ref) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1990\n"); fclose($RNThandle); $val = "Val changed";
	  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1991\n"); fclose($RNThandle); 
	}

	function v($val) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1992\n"); fclose($RNThandle); $val = "Val changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1993\n"); fclose($RNThandle); 
	}
	
	function r(&$ref) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1994\n"); fclose($RNThandle); $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1995\n"); fclose($RNThandle); 
	}
	
	function vv($val1, $val2) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1996\n"); fclose($RNThandle); $val1 = "Val1 changed";
	  $val2 = "Val2 changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1997\n"); fclose($RNThandle); 
	}
	
	function vr($val, &$ref) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1998\n"); fclose($RNThandle); $val = "Val changed";
	  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1999\n"); fclose($RNThandle); 
	}
	
	function rv(&$ref, $val) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2000\n"); fclose($RNThandle); $val = "Val changed";
	  $ref = "Ref changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2001\n"); fclose($RNThandle); 
	}
	
	function rr(&$ref1, &$ref2) {
	   $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2002\n"); fclose($RNThandle); $ref1 = "Ref1 changed";
	  $ref2 = "Ref2 changed"; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2003\n"); fclose($RNThandle); 
	}

} 

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1976\n"); fclose($RNThandle); echo "\n ---- Pass by ref / pass by val: functions ----\n";
unset($u1, $u2);
v($u1);
r($u2);
var_dump($u1, $u2);

unset($u1, $u2);
vv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
vr($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
rv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
rr($u1, $u2);
var_dump($u1, $u2);


echo "\n\n ---- Pass by ref / pass by val: static method calls ----\n";
unset($u1, $u2);
C::v($u1);
C::r($u2);
var_dump($u1, $u2);

unset($u1, $u2);
C::vv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
C::vr($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
C::rv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
C::rr($u1, $u2);
var_dump($u1, $u2);

echo "\n\n ---- Pass by ref / pass by val: instance method calls ----\n";
unset($u1, $u2);
$c = new C($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->v($u1);
$c->r($u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->vv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->vr($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->rv($u1, $u2);
var_dump($u1, $u2);

unset($u1, $u2);
$c->rr($u1, $u2);
var_dump($u1, $u2); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1977\n"); fclose($RNThandle); 

?>
