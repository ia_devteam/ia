
<?php
Class C {
	function returnConstantByValue() {
		$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2059\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	function &returnConstantByRef() {
		$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2060\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	static function &returnVariableByRef() {
		$RNTRNTRNT = $GLOBALS['a']; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2061\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
}
 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2057\n"); fclose($RNThandle); $c = new C;

echo "\n---> 1. Trying to assign by reference the return value of a function that returns by value:\n";
unset($a, $b);
$a = 4;
$b = &$c->returnConstantByValue();
$a++;
var_dump($a, $b);

echo "\n---> 2. Trying to assign by reference the return value of a function that returns a constant by ref:\n";
unset($a, $b);
$a = 4;
$b = &$c->returnConstantByRef();
$a++;
var_dump($a, $b);

echo "\n---> 3. Trying to assign by reference the return value of a function that returns by ref:\n";
unset($a, $b);
$a = 4;
$b = &$c->returnVariableByRef();
$a++;
var_dump($a, $b); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2058\n"); fclose($RNThandle); 

?>
