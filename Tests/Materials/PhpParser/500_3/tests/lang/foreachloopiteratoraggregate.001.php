
<?php
class EnglishMealIterator implements Iterator {
	private $pos=0;
	private $myContent=array("breakfast", "dinner", "tea");
	
	public function valid() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = $this->pos < count($this->myContent); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1812\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	public function next() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1813\n"); fclose($RNThandle); global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$this->pos++; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1814\n"); fclose($RNThandle); 
	}
	
	public function rewind() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1815\n"); fclose($RNThandle); global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$this->pos=0; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1816\n"); fclose($RNThandle); 
	}

	public function current() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = $this->myContent[$this->pos]; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1817\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	public function key() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = "meal " . $this->pos; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1818\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
}

class FrenchMealIterator implements Iterator {
	private $pos=0;
	private $myContent=array("petit dejeuner", "dejeuner", "gouter", "dinner");
	
	public function valid() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = $this->pos < count($this->myContent); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1819\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	public function next() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1820\n"); fclose($RNThandle); global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$this->pos++; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1821\n"); fclose($RNThandle); 
	}
	
	public function rewind() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1822\n"); fclose($RNThandle); global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$this->pos=0; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1823\n"); fclose($RNThandle); 
	}

	public function current() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = $this->myContent[$this->pos]; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1824\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	public function key() {
		global $indent;
		echo "$indent--> " . __METHOD__ . " ($this->pos)\n";
		$RNTRNTRNT = "meal " . $this->pos; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1825\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
}


Class EuropeanMeals implements IteratorAggregate {
	
	private $storedEnglishMealIterator;
	private $storedFrenchMealIterator;
	
	public function __construct() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1826\n"); fclose($RNThandle); $this->storedEnglishMealIterator = new EnglishMealIterator;
		$this->storedFrenchMealIterator = new FrenchMealIterator; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1827\n"); fclose($RNThandle); 
	}
	
	public function getIterator() {
		global $indent;
		echo "$indent--> " . __METHOD__  . "\n";
		
		//Alternate between English and French meals
		static $i = 0;
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1828\n"); fclose($RNThandle); if ($i++%2 == 0) {
			$RNTRNTRNT = $this->storedEnglishMealIterator; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1829\n"); fclose($RNThandle); return $RNTRNTRNT;
		} else {
			$RNTRNTRNT = $this->storedFrenchMealIterator; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1830\n"); fclose($RNThandle); return $RNTRNTRNT;
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1831\n"); fclose($RNThandle); 
	}
	
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1810\n"); fclose($RNThandle); $f = new EuropeanMeals;
var_dump($f);

echo "-----( Simple iteration 1: )-----\n";
foreach ($f as $k=>$v) {
	echo "$k => $v\n";	
}
echo "-----( Simple iteration 2: )-----\n";
foreach ($f as $k=>$v) {
	echo "$k => $v\n";	
}


$indent = " ";
echo "\n\n\n-----( Nested iteration: )-----\n";
$count=1;
foreach ($f as $k=>$v) {
	echo "\nTop level "  .  $count++ . ": \n"; 
	echo "$k => $v\n";
	$indent = "     ";
	foreach ($f as $k=>$v) {
		echo "     $k => $v\n";	
	}
	$indent = " ";
} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1811\n"); fclose($RNThandle); 


?>
