
<?php
Class C {
	static function returnConstantByValue() {
		$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2054\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	static function &returnConstantByRef() {
		$RNTRNTRNT = 100; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2055\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
	
	static function &returnVariableByRef() {
		$RNTRNTRNT = $GLOBALS['a']; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2056\n"); fclose($RNThandle); return $RNTRNTRNT;
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2052\n"); fclose($RNThandle); echo "\n---> 1. Trying to assign by reference the return value of a function that returns by value:\n";
unset($a, $b);
$a = 4;
$b = &C::returnConstantByValue();
$a++;
var_dump($a, $b);

echo "\n---> 2. Trying to assign by reference the return value of a function that returns a constant by ref:\n";
unset($a, $b);
$a = 4;
$b = &C::returnConstantByRef();
$a++;
var_dump($a, $b);

echo "\n---> 3. Trying to assign by reference the return value of a function that returns by ref:\n";
unset($a, $b);
$a = 4;
$b = &C::returnVariableByRef();
$a++;
var_dump($a, $b); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"2053\n"); fclose($RNThandle); 

?>
