
<?php

class C {
	public $a = "Original a";
	public $b = "Original b";
	public $c = "Original c";
	protected $d = "Original d";
	private $e = "Original e";

	function doForEachC() {
		echo "in C::doForEachC\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1872\n"); fclose($RNThandle); foreach ($this as $k=>&$v) {
			var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1873\n"); fclose($RNThandle); 
	}
		
	static function doForEach($obj) {
		echo "in C::doForEach\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1874\n"); fclose($RNThandle); foreach ($obj as $k=>&$v) {
			var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1875\n"); fclose($RNThandle); 
	}
	
	function doForEachOnThis() {
		echo "in C::doForEachOnThis\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1876\n"); fclose($RNThandle); foreach ($this as $k=>&$v) {
			var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1877\n"); fclose($RNThandle); 
	}
	
}

class D extends C {
	
	private $f = "Original f";
	protected $g = "Original g";
	
	static function doForEach($obj) {
		echo "in D::doForEach\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1878\n"); fclose($RNThandle); foreach ($obj as $k=>&$v) {
			var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1879\n"); fclose($RNThandle); 
	}

	function doForEachOnThis() {
		echo "in D::doForEachOnThis\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1880\n"); fclose($RNThandle); foreach ($this as $k=>&$v) {
			var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1881\n"); fclose($RNThandle); 
	}
}

class E extends D {
	public $a = "Overridden a";
	public $b = "Overridden b";
	public $c = "Overridden c";
	protected $d = "Overridden d";
	private $e = "Overridden e";	

	static function doForEach($obj) {
		echo "in E::doForEach\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1882\n"); fclose($RNThandle); foreach ($obj as $k=>&$v) {
			var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1883\n"); fclose($RNThandle); 
	}

	function doForEachOnThis() {
		echo "in E::doForEachOnThis\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1884\n"); fclose($RNThandle); foreach ($this as $k=>&$v) {
			var_dump($v);
			$v="changed.$k";
		} $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1885\n"); fclose($RNThandle); 
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1870\n"); fclose($RNThandle); echo "\n\nIterate over various generations from within overridden methods:\n";
echo "\n--> Using instance of C:\n";
$myC = new C;
$myC->doForEachOnThis();
var_dump($myC);
echo "\n--> Using instance of D:\n";
$myD = new D;
$myD->doForEachOnThis();
var_dump($myD);
echo "\n--> Using instance of E:\n";
$myE = new E;
$myE->doForEachOnThis();
var_dump($myE);

echo "\n\nIterate over various generations from within an inherited method:\n";
echo "\n--> Using instance of C:\n";
$myC = new C;
$myC->doForEachC();
var_dump($myC);
echo "\n--> Using instance of D:\n";
$myD = new D;
$myD->doForEachC();
var_dump($myD);
echo "\n--> Using instance of E:\n";
$myE = new E;
$myE->doForEachC();
var_dump($myE);

echo "\n\nIterate over various generations from within an overridden static method:\n";
echo "\n--> Using instance of C:\n";
$myC = new C;
C::doForEach($myC);
var_dump($myC);
$myC = new C;
D::doForEach($myC);
var_dump($myC);
$myC = new C;
E::doForEach($myC);
var_dump($myC);
echo "\n--> Using instance of D:\n";
$myD = new D;
C::doForEach($myD);
var_dump($myD);
$myD = new D;
D::doForEach($myD);
var_dump($myD);
$myD = new D;
E::doForEach($myD);
var_dump($myD);
echo "\n--> Using instance of E:\n";
$myE = new E;
C::doForEach($myE);
var_dump($myE);
$myE = new E;
D::doForEach($myE);
var_dump($myE);
$myE = new E;
E::doForEach($myE);
var_dump($myE);


echo "\n\nIterate over various generations from outside the object:\n";
echo "\n--> Using instance of C:\n";
$myC = new C;
foreach ($myC as $k=>&$v) {
	var_dump($v);
	$v="changed.$k";
}
var_dump($myC);
echo "\n--> Using instance of D:\n";
$myD = new D;
foreach ($myD as $k=>&$v) {
	var_dump($v);
	$v="changed.$k";
}
var_dump($myD);
echo "\n--> Using instance of E:\n";
$myE = new E;
foreach ($myE as $k=>&$v) {
	var_dump($v);
	$v="changed.$k";
}
var_dump($myE); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1871\n"); fclose($RNThandle); 
?>
