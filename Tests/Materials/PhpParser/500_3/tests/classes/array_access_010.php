
<?php 

// NOTE: This will become part of SPL

class ArrayReferenceProxy implements ArrayAccess
{
	private $object;
	private $element;
	
	function __construct(ArrayAccess $object, array &$element)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"244\n"); fclose($RNThandle); echo __METHOD__ . "(Array)\n";
		$this->object = $object;
		$this->element = &$element; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"245\n"); fclose($RNThandle); 
	}

	function offsetExists($index) {
		echo __METHOD__ . "($this->element, $index)\n";
		$RNTRNTRNT = array_key_exists($index, $this->element); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"246\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetGet($index) {
		echo __METHOD__ . "(Array, $index)\n";
		$RNTRNTRNT = isset($this->element[$index]) ? $this->element[$index] : NULL; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"247\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetSet($index, $value) {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"248\n"); fclose($RNThandle); echo __METHOD__ . "(Array, $index, $value)\n";
		$this->element[$index] = $value; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"249\n"); fclose($RNThandle); 
	}

	function offsetUnset($index) {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"250\n"); fclose($RNThandle); echo __METHOD__ . "(Array, $index)\n";
		unset($this->element[$index]); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"251\n"); fclose($RNThandle); 
	}
}

class Peoples implements ArrayAccess
{
	public $person;
	
	function __construct()
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"252\n"); fclose($RNThandle); $this->person = array(array('name'=>'Foo')); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"253\n"); fclose($RNThandle); 
	}

	function offsetExists($index)
	{
		$RNTRNTRNT = array_key_exists($index, $this->person); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"254\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetGet($index)
	{
		$RNTRNTRNT = new ArrayReferenceProxy($this, $this->person[$index]); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"255\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetSet($index, $value)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"256\n"); fclose($RNThandle); $this->person[$index] = $value; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"257\n"); fclose($RNThandle); 
	}

	function offsetUnset($index)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"258\n"); fclose($RNThandle); unset($this->person[$index]); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"259\n"); fclose($RNThandle); 
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"242\n"); fclose($RNThandle); $people = new Peoples;

var_dump($people->person[0]['name']);
$people->person[0]['name'] = $people->person[0]['name'] . 'Bar';
var_dump($people->person[0]['name']);
$people->person[0]['name'] .= 'Baz';
var_dump($people->person[0]['name']);

echo "===ArrayOverloading===\n";

$people = new Peoples;

var_dump($people[0]);
var_dump($people[0]['name']);
$people[0]['name'] = 'FooBar';
var_dump($people[0]['name']);
$people[0]['name'] = $people->person[0]['name'] . 'Bar';
var_dump($people[0]['name']);
$people[0]['name'] .= 'Baz';
var_dump($people[0]['name']);
unset($people[0]['name']);
var_dump($people[0]);
var_dump($people[0]['name']);
$people[0]['name'] = 'BlaBla';
var_dump($people[0]['name']); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"243\n"); fclose($RNThandle); 

?>
