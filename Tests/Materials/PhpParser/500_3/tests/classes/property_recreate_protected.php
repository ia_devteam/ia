
<?php
class C {
	protected $p = 'test';
	function unsetProtected() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1061\n"); fclose($RNThandle); unset($this->p); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1062\n"); fclose($RNThandle); 		
	}
	function setProtected() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1063\n"); fclose($RNThandle); $this->p = 'changed'; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1064\n"); fclose($RNThandle); 		
	}
}

class D extends C {
	function setP() {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1065\n"); fclose($RNThandle); $this->p = 'changed in D'; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1066\n"); fclose($RNThandle); 
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1059\n"); fclose($RNThandle); $d = new D;
echo "Unset and recreate a protected property from property's declaring class scope:\n";
$d->unsetProtected();
$d->setProtected();
var_dump($d);

echo "\nUnset and recreate a protected property from subclass:\n";
$d = new D;
$d->unsetProtected();
$d->setP();
var_dump($d);

echo "\nUnset a protected property, and attempt to recreate it outside of scope (expected failure):\n";
$d->unsetProtected();
$d->p = 'this will fail';
var_dump($d); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"1060\n"); fclose($RNThandle); 
?>
