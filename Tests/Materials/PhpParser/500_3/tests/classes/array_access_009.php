
<?php 

// NOTE: This will become part of SPL

interface ArrayProxyAccess extends ArrayAccess
{
	function proxyGet($element);
	function proxySet($element, $index, $value);
	function proxyUnset($element, $index);
}

class ArrayProxy implements ArrayAccess
{
	private $object;
	private $element;
	
	function __construct(ArrayProxyAccess $object, $element)
	{
		echo __METHOD__ . "($element)\n";
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"220\n"); fclose($RNThandle); if (!$object->offsetExists($element))
		{
			$object[$element] = array();
		}
		$this->object = $object;
		$this->element = $element; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"221\n"); fclose($RNThandle); 
	}

	function offsetExists($index) {
		echo __METHOD__ . "($this->element, $index)\n";
		$RNTRNTRNT = array_key_exists($index, $this->object->proxyGet($this->element)); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"222\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetGet($index) {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"223\n"); fclose($RNThandle); echo __METHOD__ . "($this->element, $index)\n";
		$tmp = $this->object->proxyGet($this->element);
		$RNTRNTRNT = isset($tmp[$index]) ? $tmp[$index] : NULL; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"224\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetSet($index, $value) {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"225\n"); fclose($RNThandle); echo __METHOD__ . "($this->element, $index, $value)\n";
		$this->object->proxySet($this->element, $index, $value); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"226\n"); fclose($RNThandle); 
	}

	function offsetUnset($index) {
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"227\n"); fclose($RNThandle); echo __METHOD__ . "($this->element, $index)\n";
		$this->object->proxyUnset($this->element, $index); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"228\n"); fclose($RNThandle); 
	}
}

class Peoples implements ArrayProxyAccess
{
	public $person;
	
	function __construct()
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"229\n"); fclose($RNThandle); $this->person = array(array('name'=>'Foo')); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"230\n"); fclose($RNThandle); 
	}

	function offsetExists($index)
	{
		$RNTRNTRNT = array_key_exists($index, $this->person); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"231\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetGet($index)
	{
		$RNTRNTRNT = new ArrayProxy($this, $index); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"232\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function offsetSet($index, $value)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"233\n"); fclose($RNThandle); $this->person[$index] = $value; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"234\n"); fclose($RNThandle); 
	}

	function offsetUnset($index)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"235\n"); fclose($RNThandle); unset($this->person[$index]); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"236\n"); fclose($RNThandle); 
	}

	function proxyGet($element)
	{
		$RNTRNTRNT = $this->person[$element]; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"237\n"); fclose($RNThandle); return $RNTRNTRNT;
	}

	function proxySet($element, $index, $value)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"238\n"); fclose($RNThandle); $this->person[$element][$index] = $value; $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"239\n"); fclose($RNThandle); 
	}
	
	function proxyUnset($element, $index)
	{
		 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"240\n"); fclose($RNThandle); unset($this->person[$element][$index]); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"241\n"); fclose($RNThandle); 
	}
}

 $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"218\n"); fclose($RNThandle); $people = new Peoples;

var_dump($people->person[0]['name']);
$people->person[0]['name'] = $people->person[0]['name'] . 'Bar';
var_dump($people->person[0]['name']);
$people->person[0]['name'] .= 'Baz';
var_dump($people->person[0]['name']);

echo "===ArrayOverloading===\n";

$people = new Peoples;

var_dump($people[0]);
var_dump($people[0]['name']);
$people[0]['name'] = 'FooBar';
var_dump($people[0]['name']);
$people[0]['name'] = $people->person[0]['name'] . 'Bar';
var_dump($people[0]['name']);
$people[0]['name'] .= 'Baz';
var_dump($people[0]['name']);
unset($people[0]['name']);
var_dump($people[0]);
var_dump($people[0]['name']);
$people[0]['name'] = 'BlaBla';
var_dump($people[0]['name']); $RNThandle = fopen("c:\RNTSens.txt", "a"); fputs($RNThandle,"219\n"); fclose($RNThandle); 

?>
