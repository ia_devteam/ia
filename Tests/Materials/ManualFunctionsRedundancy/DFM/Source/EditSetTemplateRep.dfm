object EditSet: TEditSet
  Left = 620
  Top = 56
  Width = 225
  Height = 380
  AutoSize = True
  Caption = 'EditSet'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object l1: TLabel
    Left = 64
    Top = 0
    Width = 86
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1090#1095#1077#1090#1072
  end
  object Label1: TLabel
    Left = 0
    Top = 56
    Width = 68
    Height = 13
    Caption = #1055#1077#1088#1074#1099#1081' '#1101#1090#1072#1078
  end
  object Label2: TLabel
    Left = 0
    Top = 80
    Width = 84
    Height = 13
    Caption = #1055#1086#1089#1083#1077#1076#1085#1080#1081' '#1101#1090#1072#1078
  end
  object Label3: TLabel
    Left = 0
    Top = 208
    Width = 39
    Height = 13
    Caption = #1064#1072#1073#1083#1086#1085
  end
  object Label4: TLabel
    Left = 80
    Top = 240
    Width = 69
    Height = 13
    Caption = #1082#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
  end
  object cb1: TCheckBox
    Left = 0
    Top = 104
    Width = 73
    Height = 17
    Caption = #1056#1091#1089#1089#1082#1080#1081
    Checked = True
    State = cbChecked
    TabOrder = 0
  end
  object cb2: TCheckBox
    Left = 0
    Top = 128
    Width = 73
    Height = 17
    Caption = #1042#1080#1076#1080#1084#1099#1081
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
  object cb3: TCheckBox
    Left = 0
    Top = 152
    Width = 121
    Height = 17
    Caption = #1042#1099#1074#1086#1076#1080#1090#1100' '#1087#1083#1072#1085#1099
    TabOrder = 2
  end
  object cb4: TCheckBox
    Left = 0
    Top = 176
    Width = 137
    Height = 17
    Caption = #1041#1088#1072#1090#1100' '#1096#1072#1073#1083#1086#1085' '#1080#1079' '#1073#1072#1079#1099
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object e1: TEdit
    Left = 0
    Top = 17
    Width = 217
    Height = 21
    TabOrder = 4
  end
  object SE1: TSpinEdit
    Left = 96
    Top = 49
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
  end
  object SE2: TSpinEdit
    Left = 96
    Top = 73
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 6
    Value = 0
  end
  object Button1: TButton
    Left = 48
    Top = 204
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 7
    OnClick = Button1Click
  end
  object BitBtn1: TBitBtn
    Left = 16
    Top = 328
    Width = 81
    Height = 25
    Caption = #1055#1088#1080#1085#1103#1090#1100
    TabOrder = 8
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 104
    Top = 328
    Width = 83
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1080#1090#1100
    TabOrder = 9
    OnClick = BitBtn2Click
    Kind = bkCancel
  end
  object Memo1: TMemo
    Left = 0
    Top = 256
    Width = 217
    Height = 65
    TabOrder = 10
  end
  object OD1: TOpenDialog
    Left = 200
    Top = 104
  end
end
