unit EditSetTemplateRep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Spin, StdCtrls, ADODB, Buttons, Mytypes;

type
  TEditSet = class(TForm)
    l1: TLabel;
    Label1: TLabel;
    cb1: TCheckBox;
    Label2: TLabel;
    cb2: TCheckBox;
    cb3: TCheckBox;
    cb4: TCheckBox;
    e1: TEdit;
    SE1: TSpinEdit;
    SE2: TSpinEdit;
    OD1: TOpenDialog;
    Label3: TLabel;
    Button1: TButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label4: TLabel;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Query: TADOQuery;
    new: boolean;
    FileName: string;
    ReportTable, QuaryTable: string;
    constructor MyCreate(AOwner: TComponent; _Query: TADOQuery; _ReportTable, _QuaryTable: string; _new: boolean);
    destructor Destroy; override;
  end;

implementation

uses DB;

{$R *.dfm}

constructor TEditSet.MyCreate(AOwner: TComponent; _Query: TADOQuery; _ReportTable, _QuaryTable: string; _new: boolean);
begin
  inherited create(AOwner);
  ReportTable := _ReportTable;
  QuaryTable := _QuaryTable;
  Query := _Query;
  new := _new;
  if not new then
  begin
    e1.Text := Query.fieldbyname('namereport').AsString;
    se1.Value := Query.fieldbyname('firstflour').AsInteger;
    se2.Value := Query.fieldbyname('lastflour').AsInteger;
    cb1.Checked := Query.fieldbyname('rus').AsBoolean;
    cb2.Checked := Query.fieldbyname('visible').AsBoolean;
    cb3.Checked := Query.fieldbyname('havepicture').AsBoolean;
    cb4.Checked := Query.fieldbyname('getfrombase').AsBoolean;
    Filename := '';
    self.Caption := '�������������� ������';
  end
  else
  begin
    e1.Text := '';
    se1.Value := 0;
    se2.Value := 0;
    cb1.Checked := true;
    cb2.Checked := true;
    cb3.Checked := false;
    cb4.Checked := true;
    Filename := '';
    self.Caption := '����� �����';
  end;
end;

destructor TEditSet.Destroy;
begin
  inherited;
end;

procedure TEditSet.Button1Click(Sender: TObject);
begin
  if od1.Execute then
    self.FileName := od1.FileName;
end;

procedure TEditSet.BitBtn2Click(Sender: TObject);
begin
  self.Close;
end;

procedure TEditSet.BitBtn1Click(Sender: TObject);
var
  Q1: TADOQuery;
begin
  Q1 := TADOQuery.Create(self);
  Q1.Connection := QUERY.Connection;
  if new then
  begin
    Q1.SQL.Add(format(Sql_Add_Report, [self.ReportTable]));
    Q1.Parameters.ParamByName('namereport').Value := e1.Text;
    Q1.Parameters.ParamByName('comment').Value := memo1.Text;
    Q1.Parameters.ParamByName('visible').Value := cb2.Checked;
    Q1.Parameters.ParamByName('rus').Value := cb1.Checked;
    Q1.Parameters.ParamByName('firstflour').Value := se1.Value;
    Q1.Parameters.ParamByName('lastflour').Value := se2.Value;
    Q1.Parameters.ParamByName('havepicture').Value := cb3.Checked;
    Q1.Parameters.ParamByName('getfrombase').Value := cb4.Checked;
    if filename <> '' then
      Q1.Parameters.ParamByName('report').LoadFromFile(fileName, ftBlob);
    try
      Q1.ExecSQL;
      query.Close;
      query.Open;
    except
      on e: exception do
      begin
        Messagedlg(e.Message, MtError, [mbOk], 0);
      end;
    end;
  end
  else
  begin
    if filename <> '' then
      Q1.SQL.Add(format(Sql_Update_Report_all, [self.ReportTable]))
    else
      Q1.SQL.Add(format(Sql_Update_Report, [self.ReportTable]));
    Q1.Parameters.ParamByName('namereport').Value := e1.Text;
    Q1.Parameters.ParamByName('comment').Value := memo1.Text;
    Q1.Parameters.ParamByName('visible').Value := cb2.Checked;
    Q1.Parameters.ParamByName('rus').Value := cb1.Checked;
    Q1.Parameters.ParamByName('firstflour').Value := se1.Value;
    Q1.Parameters.ParamByName('lastflour').Value := se2.Value;
    Q1.Parameters.ParamByName('havepicture').Value := cb3.Checked;
    Q1.Parameters.ParamByName('getfrombase').Value := cb4.Checked;
    if filename <> '' then
      Q1.Parameters.ParamByName('report').LoadFromFile(fileName, ftBlob);
    Q1.Parameters.ParamByName('id').Value := Query.fieldbyname('id').AsInteger;
    try
      Q1.ExecSQL;
      query.Close;
      query.Open;
    except
      on e: exception do
      begin
        Messagedlg(e.Message, MtError, [mbOk], 0);
      end;
    end;
  end;
  q1.Free;
end;

end.

