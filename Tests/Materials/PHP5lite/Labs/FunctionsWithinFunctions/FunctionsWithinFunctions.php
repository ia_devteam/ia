<?php $hFNTX = fopen("/var/tmp/FNTXSens_1024.txt", "a"); fputs($hFNTX, "3\n"); fclose($hFNTX);
function foo() 
{ $hFNTX = fopen("/var/tmp/FNTXSens_1024.txt", "a"); fputs($hFNTX, "2\n"); fclose($hFNTX);
  function bar() 
  { $hFNTX = fopen("/var/tmp/FNTXSens_1024.txt", "a"); fputs($hFNTX, "1\n"); fclose($hFNTX);
    echo "I don't exist until foo() is called.\n";
  }
}

/* We can't call bar() yet
   since it doesn't exist. */

foo();

/* Now we can call bar(),
   foo()'s processing has
   made it accessible. */

bar();

?>
