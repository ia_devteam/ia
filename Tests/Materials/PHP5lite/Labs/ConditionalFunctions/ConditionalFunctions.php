
<?php $hFNTX = fopen("/var/tmp/FNTXSens_1024.txt", "a"); fputs($hFNTX, "3\n"); fclose($hFNTX);

$makefoo = true;

/* We can't call foo() from here 
   since it doesn't exist yet,
   but we can call bar() */

bar();

if ($makefoo) {
  function foo()
  { $hFNTX = fopen("/var/tmp/FNTXSens_1024.txt", "a"); fputs($hFNTX, "2\n"); fclose($hFNTX);
    echo "I don't exist until program execution reaches me.\n";
  }
}

/* Now we can safely call foo()
   since $makefoo evaluated to true */

if ($makefoo) foo();

function bar() 
{ $hFNTX = fopen("/var/tmp/FNTXSens_1024.txt", "a"); fputs($hFNTX, "1\n"); fclose($hFNTX);
  echo "I exist immediately upon program start.\n";
}

?>
