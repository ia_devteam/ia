function help(){
	alert(
		"Dinocube\n\n"+
		"This is the Dinocube puzzle. Click to the\n"+
		"left or right of a corner to turn it in\n"+
		"either direction. The aim is of course to\n"+
		"rearrange it so that each face has only one\n"+
		"colour.\n\n"+
		"Buttons:\n"+
		"Mix:   Mixes up the cube.\n"+
		"Reset: Resets the cube to solved position.\n"+
		"Solve: Calculates the shortest solution. Each\n"+
		"      time you click this button, one more move\n"+
		"      of the solution is performed.\n"+
		"Play:  Calculates the shortest solution, and\n"+
		"      plays it back automatically.\n"+
		"Edit:  Edit the puzzle. The pieces are cleared\n"+
		"      and then you can click where each of them\n"+
		"      (shown in miniature) is supposed to be.\n"+
		"Mode:  Switch between the 6 colour and the 4\n"+
		"      colour version of the dino cube.\n"+
		"Help:  Shows this help screen.");
}

function dostyle(){
	var s='<STYLE TYPE="text/css">';
	for(var i=0;i<4;i++){
		s+="#l"+(i*6  )+"{position:absolute; left:"+(i*47+ 5)+"; top:5; width:1;}";
		s+="#l"+(i*6+1)+"{position:absolute; left:"+(i*47+ 5)+"; top:29; width:1;}";
		s+="#l"+(i*6+2)+"{position:absolute; left:"+(i*47+ 5)+"; top:29; width:1;}";
		s+="#l"+(i*6+3)+"{position:absolute; left:"+(i*47+28)+"; top:29; width:1;}";
		s+="#l"+(i*6+4)+"{position:absolute; left:"+(i*47+ 5)+"; top:52; width:1;}";
		s+="#l"+(i*6+5)+"{position:absolute; left:"+(i*47+ 5)+"; top:76; width:1;}";
	}
	s+='#prev1{position:absolute; left:91; top:5; width:1; visibility:hidden;}';
	s+='#prev2{position:absolute; left:91; top:14; width:1; visibility:hidden;}';
	s+='#buttons{position:absolute; left:004; top:110; width:192;}';
	s+='<\/STYLE>';
	return s;
}
document.write(dostyle());