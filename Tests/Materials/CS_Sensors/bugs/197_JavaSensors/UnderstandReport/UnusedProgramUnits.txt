                          Unused Program Units Report
===============================================================================

@#$%^&*\InMemoryNamingStore.java
    org.jboss.as.naming.InMemoryNamingStore.addNamingListener           250
    org.jboss.as.naming.InMemoryNamingStore.bind                        121
    org.jboss.as.naming.InMemoryNamingStore.close                       234
    org.jboss.as.naming.InMemoryNamingStore.createSubcontext            222
    org.jboss.as.naming.InMemoryNamingStore.getBaseName                 116
    org.jboss.as.naming.InMemoryNamingStore.InMemoryNamingStore          79
    org.jboss.as.naming.InMemoryNamingStore.InMemoryNamingStore          88
    org.jboss.as.naming.InMemoryNamingStore.InMemoryNamingStore          97
    org.jboss.as.naming.InMemoryNamingStore.list                        205
    org.jboss.as.naming.InMemoryNamingStore.listBindings                217
    org.jboss.as.naming.InMemoryNamingStore.lookup                      193
    org.jboss.as.naming.InMemoryNamingStore.rebind                      140
    org.jboss.as.naming.InMemoryNamingStore.removeNamingListener        262
    org.jboss.as.naming.InMemoryNamingStore.unbind                      164

@#$%^&*\public_enum.java
    org.wildfly.extension.batch.job.repository.JobRepositoryType.(Anon_1).toString 36
    org.wildfly.extension.batch.job.repository.JobRepositoryType.(Anon_2).toString 42
    org.wildfly.extension.batch.job.repository.JobRepositoryType.of      55
    org.wildfly.extension.batch.job.repository.JobRepositoryType.valueOf 33
    org.wildfly.extension.batch.job.repository.JobRepositoryType.values  33

@#$%^&*\super_must_be_first.java
    org.jboss.as.appclient.component.ApplicationClientComponentDescription.ApplicationClientComponentDescription 45
    org.jboss.as.appclient.component.ApplicationClientComponentDescription.createConfiguration 57
    org.jboss.as.appclient.component.ApplicationClientComponentDescription.createConfiguration.(Anon_1).create 61
    org.jboss.as.appclient.component.ApplicationClientComponentDescription.createConfiguration.(Anon_1).create.(Anon_2).getInstance 69
    org.jboss.as.appclient.component.ApplicationClientComponentDescription.createConfiguration.(Anon_1).create.(Anon_2).release 64
    org.jboss.as.appclient.component.ApplicationClientComponentDescription.isIntercepted 52

@#$%^&*\this_must_be_first.java
    org.jboss.as.clustering.controller.MetricHandler.MetricHandler       39
    org.jboss.as.clustering.controller.MetricHandler.MetricHandler       43
    org.jboss.as.clustering.controller.MetricHandler.MetricHandler       47
    org.jboss.as.clustering.controller.MetricHandler.MetricHandler.(lambda_expr_1) 48
    org.jboss.as.clustering.controller.MetricHandler.register            53
    org.jboss.as.clustering.controller.MetricHandler.register.(lambda_expr_1) 54

@#$%^&*\x1.java
    org.jboss.as.ee.component.AbstractComponentConfigurator.mergeInjectionsForClass 71
    org.jboss.as.ee.component.AbstractComponentConfigurator.weaved       36

@#$%^&*\x2.java
    org.wildfly.iiop.openjdk.naming.Name.baseNameComponent               90
    org.wildfly.iiop.openjdk.naming.Name.components                     102
    org.wildfly.iiop.openjdk.naming.Name.ctxName                        110
    org.wildfly.iiop.openjdk.naming.Name.equals                         122
    org.wildfly.iiop.openjdk.naming.Name.fullName                       130
    org.wildfly.iiop.openjdk.naming.Name.hashCode                       134
    org.wildfly.iiop.openjdk.naming.Name.kind                            94
    org.wildfly.iiop.openjdk.naming.Name.Name                            41
    org.wildfly.iiop.openjdk.naming.Name.Name                            69
    org.wildfly.iiop.openjdk.naming.Name.Name                            77

@#$%^&*\x4_not_validFor.java
    org.jboss.as.ejb3.component.stateful.StatefulSessionBeanSerializabilityChecker.isSerializable 56
    org.jboss.as.ejb3.component.stateful.StatefulSessionBeanSerializabilityChecker.StatefulSessionBeanSerializabilityChecker 41

@#$%^&*\x5_badSuper.java
    org.wildfly.extension.undertow.LocationDefinition.getAttributes      71
    org.wildfly.extension.undertow.LocationDefinition.getChildren        76
    org.wildfly.extension.undertow.LocationDefinition.LocationDefinition.(Anon_1).serviceName 59

@#$%^&*\x6_badThis.java
    org.jboss.as.clustering.jgroups.subsystem.XMLAttribute.forName       88
    org.jboss.as.clustering.jgroups.subsystem.XMLAttribute.valueOf       30
    org.jboss.as.clustering.jgroups.subsystem.XMLAttribute.XMLAttribute  60
    org.jboss.as.clustering.jgroups.subsystem.XMLAttribute.XMLAttribute  64

@#$%^&*\x7.java
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.ArrayDequeExternalizer.ArrayDequeExternalizer 86
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.ArrayListExternalizer.ArrayListExternalizer 92
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.ConcurrentLinkedDequeExternalizer.ConcurrentLinkedDequeExternalizer 98
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.ConcurrentLinkedDequeExternalizer.ConcurrentLinkedDequeExternalizer.(lambda_expr_1) 99
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.ConcurrentLinkedQueueExternalizer.ConcurrentLinkedQueueExternalizer 104
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.ConcurrentLinkedQueueExternalizer.ConcurrentLinkedQueueExternalizer.(lambda_expr_2) 105
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.getTargetClass 81
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.HashSetExternalizer.HashSetExternalizer 110
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.LinkedHashSetExternalizer.LinkedHashSetExternalizer 116
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.LinkedListExternalizer.LinkedListExternalizer 122
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.LinkedListExternalizer.LinkedListExternalizer.(lambda_expr_3) 123
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.readObject 68
    org.wildfly.clustering.marshalling.jboss.CollectionExternalizer.writeObject 56

@#$%^&*\x8.java
    org.wildfly.clustering.marshalling.jboss.SimpleMarshallingConfigurationRepository.getCurrentMarshallingVersion 69
    org.wildfly.clustering.marshalling.jboss.SimpleMarshallingConfigurationRepository.getMarshallingConfiguration 74
    org.wildfly.clustering.marshalling.jboss.SimpleMarshallingConfigurationRepository.SimpleMarshallingConfigurationRepository 46
    org.wildfly.clustering.marshalling.jboss.SimpleMarshallingConfigurationRepository.SimpleMarshallingConfigurationRepository 54
    org.wildfly.clustering.marshalling.jboss.SimpleMarshallingConfigurationRepository.SimpleMarshallingConfigurationRepository 63
    org.wildfly.clustering.marshalling.jboss.SimpleMarshallingConfigurationRepository.SimpleMarshallingConfigurationRepository.(lambda_expr_1) 47

@#$%^&*\x81.java
    org.jboss.as.clustering.controller.AddStepHandler.AddStepHandler     57
    org.jboss.as.clustering.controller.AddStepHandler.AddStepHandler.(Anon_1).finishModelStage 64
    org.jboss.as.clustering.controller.AddStepHandler.execute            93
    org.jboss.as.clustering.controller.AddStepHandler.execute.(lambda_expr_1) 110
    org.jboss.as.clustering.controller.AddStepHandler.getDescriptor      88
    org.jboss.as.clustering.controller.AddStepHandler.performRuntime    146
    org.jboss.as.clustering.controller.AddStepHandler.populateModel     128
    org.jboss.as.clustering.controller.AddStepHandler.populateModel.(lambda_expr_1) 135
    org.jboss.as.clustering.controller.AddStepHandler.populateModel.(lambda_expr_2) 136
    org.jboss.as.clustering.controller.AddStepHandler.recordCapabilitiesAndRequirements 164
    org.jboss.as.clustering.controller.AddStepHandler.recordCapabilitiesAndRequirements.(lambda_expr_1) 167
    org.jboss.as.clustering.controller.AddStepHandler.recordCapabilitiesAndRequirements.(lambda_expr_2) 171
    org.jboss.as.clustering.controller.AddStepHandler.recordCapabilitiesAndRequirements.(lambda_expr_3) 172
    org.jboss.as.clustering.controller.AddStepHandler.register          176
    org.jboss.as.clustering.controller.AddStepHandler.register.(lambda_expr_1) 178
    org.jboss.as.clustering.controller.AddStepHandler.register.(lambda_expr_2) 179
    org.jboss.as.clustering.controller.AddStepHandler.register.(lambda_expr_3) 182
    org.jboss.as.clustering.controller.AddStepHandler.rollbackRuntime   153
