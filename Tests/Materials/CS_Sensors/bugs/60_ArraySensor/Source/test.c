#include <math.h>
#include "geoconcept.h"
#include "cpl_conv.h"
#include "cpl_string.h"
#include "ogr_core.h"

GCIO_CVSID("$Id: geoconcept.c,v 1.0.0 2007-11-03 20:58:19 drichard Exp $")

#define kItemSize_GCIO      256
#define kExtraSize_GCIO    4096
#define kIdSize_GCIO         12
#define UNDEFINEDID_GCIO 199901L

static char* gkGCCharset[]=
{
/* 0 */ "",
/* 1 */ "ANSI",
/* 2 */ "DOS",
/* 3 */ "MAC"
};

static char* gkGCAccess[]=
{
/* 0 */ "",
/* 1 */ "NO",
/* 2 */ "READ",
/* 3 */ "UPDATE",
/* 4 */ "WRITE"
};