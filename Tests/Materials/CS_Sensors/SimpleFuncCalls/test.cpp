// ConsoleApplication2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int a;

void A();
void B();

void A()
{
	B();
}

void B()
{
	while (a < 2)
	{
		a++;
		A();
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc > 1)
		a = 1;
	else
		a = 2;
	A();

	return 0;
}

