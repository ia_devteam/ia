#include "var/tmp/sensor.h"
/*    universal.c
 *
 *    Copyright (C) 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
 *    2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 * '"The roots of those mountains must be roots indeed; there must be
 *   great secrets buried there which have not been discovered since the
 *   beginning."'                   --Gandalf, relating Gollum's history
 *
 *     [p.54 of _The Lord of the Rings_, I/ii: "The Shadow of the Past"]
 */

/* This file contains the code that implements the functions in Perl's
 * UNIVERSAL package, such as UNIVERSAL->can().
 *
 * It is also used to store XS functions that need to be present in
 * miniperl for a lack of a better place to put them. It might be
 * clever to move them to separate XS files which would then be pulled
 * in by some to-be-written build process.
 */

#include "EXTERN.h"
#define PERL_IN_UNIVERSAL_C
#include "perl.h"

#ifdef USE_PERLIO
#include "perliol.h" /* For the PERLIO_F_XXX */
#endif

/*
 * Contributed by Graham Barr  <Graham.Barr@tiuk.ti.com>
 * The main guts of traverse_isa was actually copied from gv_fetchmeth
 */

STATIC bool
S_isa_lookup(pTHX_ HV *stash, const char * const name, STRLEN len, U32 flags)
{
SensorCall();    dVAR;
    const struct mro_meta *const meta = HvMROMETA(stash);
    HV *isa = meta->isa;
    const HV *our_stash;

    PERL_ARGS_ASSERT_ISA_LOOKUP;

    SensorCall();if (!isa) {
	SensorCall();(void)mro_get_linear_isa(stash);
	isa = meta->isa;
    }

    SensorCall();if (hv_common(isa, NULL, name, len, ( flags & SVf_UTF8 ? HVhek_UTF8 : 0),
		  HV_FETCH_ISEXISTS, NULL, 0)) {
	/* Direct name lookup worked.  */
	{_Bool  ReplaceReturn = TRUE; SensorCall(); return ReplaceReturn;}
    }

    /* A stash/class can go by many names (ie. User == main::User), so 
       we use the HvENAME in the stash itself, which is canonical, falling
       back to HvNAME if necessary.  */
    SensorCall();our_stash = gv_stashpvn(name, len, flags);

    SensorCall();if (our_stash) {
	SensorCall();HEK *canon_name = HvENAME_HEK(our_stash);
	SensorCall();if (!canon_name) canon_name = HvNAME_HEK(our_stash);

	SensorCall();if (hv_common(isa, NULL, HEK_KEY(canon_name), HEK_LEN(canon_name),
		      HEK_FLAGS(canon_name),
		      HV_FETCH_ISEXISTS, NULL, HEK_HASH(canon_name))) {
	    {_Bool  ReplaceReturn = TRUE; SensorCall(); return ReplaceReturn;}
	}
    }

    {_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
}

/*
=head1 SV Manipulation Functions

=for apidoc sv_derived_from_pvn

Returns a boolean indicating whether the SV is derived from the specified class
I<at the C level>.  To check derivation at the Perl level, call C<isa()> as a
normal Perl method.

Currently, the only significant value for C<flags> is SVf_UTF8.

=cut

=for apidoc sv_derived_from_sv

Exactly like L</sv_derived_from_pvn>, but takes the name string in the form
of an SV instead of a string/length pair.

=cut

*/

bool
Perl_sv_derived_from_sv(pTHX_ SV *sv, SV *namesv, U32 flags)
{
    SensorCall();char *namepv;
    STRLEN namelen;
    PERL_ARGS_ASSERT_SV_DERIVED_FROM_SV;
    namepv = SvPV(namesv, namelen);
    SensorCall();if (SvUTF8(namesv))
       flags |= SVf_UTF8;
    {_Bool  ReplaceReturn = sv_derived_from_pvn(sv, namepv, namelen, flags); SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc sv_derived_from

Exactly like L</sv_derived_from_pv>, but doesn't take a C<flags> parameter.

=cut
*/

bool
Perl_sv_derived_from(pTHX_ SV *sv, const char *const name)
{
    PERL_ARGS_ASSERT_SV_DERIVED_FROM;
    {_Bool  ReplaceReturn = sv_derived_from_pvn(sv, name, strlen(name), 0); SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc sv_derived_from_pv

Exactly like L</sv_derived_from_pvn>, but takes a nul-terminated string 
instead of a string/length pair.

=cut
*/


bool
Perl_sv_derived_from_pv(pTHX_ SV *sv, const char *const name, U32 flags)
{
    PERL_ARGS_ASSERT_SV_DERIVED_FROM_PV;
    {_Bool  ReplaceReturn = sv_derived_from_pvn(sv, name, strlen(name), flags); SensorCall(); return ReplaceReturn;}
}

bool
Perl_sv_derived_from_pvn(pTHX_ SV *sv, const char *const name, const STRLEN len, U32 flags)
{
SensorCall();    dVAR;
    HV *stash;

    PERL_ARGS_ASSERT_SV_DERIVED_FROM_PVN;

    SvGETMAGIC(sv);

    SensorCall();if (SvROK(sv)) {
	SensorCall();const char *type;
        sv = SvRV(sv);
        type = sv_reftype(sv,0);
	SensorCall();if (type && strEQ(type,name))
	    return TRUE;
	SensorCall();stash = SvOBJECT(sv) ? SvSTASH(sv) : NULL;
    }
    else {
        SensorCall();stash = gv_stashsv(sv, 0);
    }

    {_Bool  ReplaceReturn = stash ? isa_lookup(stash, name, len, flags) : FALSE; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc sv_does_sv

Returns a boolean indicating whether the SV performs a specific, named role.
The SV can be a Perl object or the name of a Perl class.

=cut
*/

#include "XSUB.h"

bool
Perl_sv_does_sv(pTHX_ SV *sv, SV *namesv, U32 flags)
{
    SensorCall();SV *classname;
    bool does_it;
    SV *methodname;
    dSP;

    PERL_ARGS_ASSERT_SV_DOES_SV;
    PERL_UNUSED_ARG(flags);

    ENTER;
    SAVETMPS;

    SvGETMAGIC(sv);

    SensorCall();if (!SvOK(sv) || !(SvROK(sv) || (SvPOK(sv) && SvCUR(sv))
	    || (SvGMAGICAL(sv) && SvPOKp(sv) && SvCUR(sv)))) {
	LEAVE;
	{_Bool  ReplaceReturn = FALSE; SensorCall(); return ReplaceReturn;}
    }

    SensorCall();if (sv_isobject(sv)) {
	SensorCall();classname = sv_ref(NULL,SvRV(sv),TRUE);
    } else {
	SensorCall();classname = sv;
    }

    SensorCall();if (sv_eq(classname, namesv)) {
	LEAVE;
	{_Bool  ReplaceReturn = TRUE; SensorCall(); return ReplaceReturn;}
    }

    PUSHMARK(SP);
    EXTEND(SP, 2);
    PUSHs(sv);
    PUSHs(namesv);
    PUTBACK;

    SensorCall();methodname = newSVpvs_flags("isa", SVs_TEMP);
    /* ugly hack: use the SvSCREAM flag so S_method_common
     * can figure out we're calling DOES() and not isa(),
     * and report eventual errors correctly. --rgs */
    SvSCREAM_on(methodname);
    call_sv(methodname, G_SCALAR | G_METHOD);
    SPAGAIN;

    does_it = SvTRUE( TOPs );
    FREETMPS;
    LEAVE;

    {_Bool  ReplaceReturn = does_it; SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc sv_does

Like L</sv_does_pv>, but doesn't take a C<flags> parameter.

=cut
*/

bool
Perl_sv_does(pTHX_ SV *sv, const char *const name)
{
    PERL_ARGS_ASSERT_SV_DOES;
    {_Bool  ReplaceReturn = sv_does_sv(sv, newSVpvn_flags(name, strlen(name), SVs_TEMP), 0); SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc sv_does_pv

Like L</sv_does_sv>, but takes a nul-terminated string instead of an SV.

=cut
*/


bool
Perl_sv_does_pv(pTHX_ SV *sv, const char *const name, U32 flags)
{
    PERL_ARGS_ASSERT_SV_DOES_PV;
    {_Bool  ReplaceReturn = sv_does_sv(sv, newSVpvn_flags(name, strlen(name), SVs_TEMP | flags), flags); SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc sv_does_pvn

Like L</sv_does_sv>, but takes a string/length pair instead of an SV.

=cut
*/

bool
Perl_sv_does_pvn(pTHX_ SV *sv, const char *const name, const STRLEN len, U32 flags)
{
    PERL_ARGS_ASSERT_SV_DOES_PVN;

    {_Bool  ReplaceReturn = sv_does_sv(sv, newSVpvn_flags(name, len, flags | SVs_TEMP), flags); SensorCall(); return ReplaceReturn;}
}

/*
=for apidoc croak_xs_usage

A specialised variant of C<croak()> for emitting the usage message for xsubs

    croak_xs_usage(cv, "eee_yow");

works out the package name and subroutine name from C<cv>, and then calls
C<croak()>. Hence if C<cv> is C<&ouch::awk>, it would call C<croak> as:

    Perl_croak(aTHX_ "Usage: %"SVf"::%"SVf"(%s)", "ouch" "awk", "eee_yow");

=cut
*/

void
Perl_croak_xs_usage(pTHX_ const CV *const cv, const char *const params)
{
    SensorCall();const GV *const gv = CvGV(cv);

    PERL_ARGS_ASSERT_CROAK_XS_USAGE;

    SensorCall();if (gv) {
	SensorCall();const HV *const stash = GvSTASH(gv);

	SensorCall();if (HvNAME_get(stash))
	    {/*1*/SensorCall();Perl_croak(aTHX_ "Usage: %"HEKf"::%"HEKf"(%s)",
                                HEKfARG(HvNAME_HEK(stash)),
                                HEKfARG(GvNAME_HEK(gv)),
                                params);/*2*/}
	else
	    {/*3*/SensorCall();Perl_croak(aTHX_ "Usage: %"HEKf"(%s)",
                                HEKfARG(GvNAME_HEK(gv)), params);/*4*/}
    } else {
	/* Pants. I don't think that it should be possible to get here. */
	SensorCall();Perl_croak(aTHX_ "Usage: CODE(0x%"UVxf")(%s)", PTR2UV(cv), params);
    }
SensorCall();}

XS(XS_UNIVERSAL_isa)
{
SensorCall();    dVAR;
    dXSARGS;

    SensorCall();if (items != 2)
	croak_xs_usage(cv, "reference, kind");
    else {
	SensorCall();SV * const sv = ST(0);

	SvGETMAGIC(sv);

	SensorCall();if (!SvOK(sv) || !(SvROK(sv) || (SvPOK(sv) && SvCUR(sv))
		    || (SvGMAGICAL(sv) && SvPOKp(sv) && SvCUR(sv))))
	    XSRETURN_UNDEF;

	ST(0) = boolSV(sv_derived_from_sv(sv, ST(1), 0));
	XSRETURN(1);
    }
SensorCall();}

XS(XS_UNIVERSAL_can)
{
SensorCall();    dVAR;
    dXSARGS;
    SV   *sv;
    SV   *rv;
    HV   *pkg = NULL;

    SensorCall();if (items != 2)
	croak_xs_usage(cv, "object-ref, method");

    SensorCall();sv = ST(0);

    SvGETMAGIC(sv);

    SensorCall();if (!SvOK(sv) || !(SvROK(sv) || (SvPOK(sv) && SvCUR(sv))
		|| (SvGMAGICAL(sv) && SvPOKp(sv) && SvCUR(sv))))
	XSRETURN_UNDEF;

    SensorCall();rv = &PL_sv_undef;

    SensorCall();if (SvROK(sv)) {
        SensorCall();sv = MUTABLE_SV(SvRV(sv));
        SensorCall();if (SvOBJECT(sv))
            pkg = SvSTASH(sv);
    }
    else {
        SensorCall();pkg = gv_stashsv(sv, 0);
    }

    SensorCall();if (pkg) {
	SensorCall();GV * const gv = gv_fetchmethod_sv_flags(pkg, ST(1), 0);
        SensorCall();if (gv && isGV(gv))
	    rv = sv_2mortal(newRV(MUTABLE_SV(GvCV(gv))));
    }

    ST(0) = rv;
    XSRETURN(1);
}

XS(XS_UNIVERSAL_DOES)
{
SensorCall();    dVAR;
    dXSARGS;
    PERL_UNUSED_ARG(cv);

    SensorCall();if (items != 2)
	{/*5*/SensorCall();Perl_croak(aTHX_ "Usage: invocant->DOES(kind)");/*6*/}
    else {
	SensorCall();SV * const sv = ST(0);
	SensorCall();if (sv_does_sv( sv, ST(1), 0 ))
	    XSRETURN_YES;

	XSRETURN_NO;
    }
SensorCall();}

XS(XS_UNIVERSAL_VERSION)
{
SensorCall();    dVAR;
    dXSARGS;
    HV *pkg;
    GV **gvp;
    GV *gv;
    SV *sv;
    const char *undef;
    PERL_UNUSED_ARG(cv);

    SensorCall();if (SvROK(ST(0))) {
        SensorCall();sv = MUTABLE_SV(SvRV(ST(0)));
        SensorCall();if (!SvOBJECT(sv))
            {/*7*/SensorCall();Perl_croak(aTHX_ "Cannot find version of an unblessed reference");/*8*/}
        SensorCall();pkg = SvSTASH(sv);
    }
    else {
        SensorCall();pkg = gv_stashsv(ST(0), 0);
    }

    SensorCall();gvp = pkg ? (GV**)hv_fetchs(pkg, "VERSION", FALSE) : NULL;

    SensorCall();if (gvp && isGV(gv = *gvp) && (sv = GvSV(gv)) && SvOK(sv)) {
        SensorCall();SV * const nsv = sv_newmortal();
        sv_setsv(nsv, sv);
        sv = nsv;
	SensorCall();if ( !sv_isobject(sv) || !sv_derived_from(sv, "version"))
	    upg_version(sv, FALSE);

        SensorCall();undef = NULL;
    }
    else {
        SensorCall();sv = &PL_sv_undef;
        undef = "(undef)";
    }

    SensorCall();if (items > 1) {
	SensorCall();SV *req = ST(1);

	SensorCall();if (undef) {
	    SensorCall();if (pkg) {
		SensorCall();const HEK * const name = HvNAME_HEK(pkg);
		Perl_croak(aTHX_
			   "%"HEKf" does not define $%"HEKf
			   "::VERSION--version check failed",
			   HEKfARG(name), HEKfARG(name));
	    } else {
		SensorCall();Perl_croak(aTHX_
			     "%"SVf" defines neither package nor VERSION--version check failed",
			     SVfARG(ST(0)) );
	     }
	}

	SensorCall();if ( !sv_isobject(req) || !sv_derived_from(req, "version")) {
	    /* req may very well be R/O, so create a new object */
	    SensorCall();req = sv_2mortal( new_version(req) );
	}

	SensorCall();if ( vcmp( req, sv ) > 0 ) {
	    SensorCall();if ( hv_exists(MUTABLE_HV(SvRV(req)), "qv", 2 ) ) {
		SensorCall();Perl_croak(aTHX_ "%"HEKf" version %"SVf" required--"
		       "this is only version %"SVf"",
                       HEKfARG(HvNAME_HEK(pkg)),
		       SVfARG(sv_2mortal(vnormal(req))),
		       SVfARG(sv_2mortal(vnormal(sv))));
	    } else {
		SensorCall();Perl_croak(aTHX_ "%"HEKf" version %"SVf" required--"
		       "this is only version %"SVf,
                       HEKfARG(HvNAME_HEK(pkg)),
		       SVfARG(sv_2mortal(vstringify(req))),
		       SVfARG(sv_2mortal(vstringify(sv))));
	    }
	}

    }

    SensorCall();if ( SvOK(sv) && sv_derived_from(sv, "version") ) {
	ST(0) = sv_2mortal(vstringify(sv));
    } else {
	ST(0) = sv;
    }

    XSRETURN(1);
}

XS(XS_version_new)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items > 3)
	croak_xs_usage(cv, "class, version");
    SP -= items;
    {
        SensorCall();SV *vs = ST(1);
	SV *rv;
        STRLEN len;
        const char *classname;
        U32 flags;
        SensorCall();if ( sv_isobject(ST(0)) ) { /* get the class if called as an object method */
            SensorCall();const HV * stash = SvSTASH(SvRV(ST(0)));
            classname = HvNAME(stash);
            len       = HvNAMELEN(stash);
            flags     = HvNAMEUTF8(stash) ? SVf_UTF8 : 0;
        }
        else {
	    SensorCall();classname = SvPV(ST(0), len);
            flags     = SvUTF8(ST(0));
        }

	SensorCall();if ( items == 1 || ! SvOK(vs) ) { /* no param or explicit undef */
	    /* create empty object */
	    SensorCall();vs = sv_newmortal();
	    sv_setpvs(vs, "0");
	}
	else {/*9*/SensorCall();if ( items == 3 ) {
	    SensorCall();vs = sv_newmortal();
	    Perl_sv_setpvf(aTHX_ vs,"v%s",SvPV_nolen_const(ST(2)));
	;/*10*/}}

	SensorCall();rv = new_version(vs);
	SensorCall();if ( strnNE(classname,"version", len) ) /* inherited new() */
	    sv_bless(rv, gv_stashpvn(classname, len, GV_ADD | flags));

	mPUSHs(rv);
	PUTBACK;
	SensorCall();return;
    }
}

XS(XS_version_stringify)
{
SensorCall();     dVAR;
     dXSARGS;
     SensorCall();if (items < 1)
	 croak_xs_usage(cv, "lobj, ...");
     SP -= items;
     {
	  SensorCall();SV *	lobj = ST(0);

	  SensorCall();if (sv_isobject(lobj) && sv_derived_from(lobj, "version")) {
	       SensorCall();lobj = SvRV(lobj);
	  }
	  else
	       {/*11*/SensorCall();Perl_croak(aTHX_ "lobj is not of type version");/*12*/}

	  mPUSHs(vstringify(lobj));

	  PUTBACK;
	  SensorCall();return;
     }
}

XS(XS_version_numify)
{
SensorCall();     dVAR;
     dXSARGS;
     SensorCall();if (items < 1)
	 croak_xs_usage(cv, "lobj, ...");
     SP -= items;
     {
	  SensorCall();SV *	lobj = ST(0);

	  SensorCall();if (sv_isobject(lobj) && sv_derived_from(lobj, "version")) {
	       SensorCall();lobj = SvRV(lobj);
	  }
	  else
	       {/*13*/SensorCall();Perl_croak(aTHX_ "lobj is not of type version");/*14*/}

	  mPUSHs(vnumify(lobj));

	  PUTBACK;
	  SensorCall();return;
     }
}

XS(XS_version_normal)
{
SensorCall();     dVAR;
     dXSARGS;
     SensorCall();if (items < 1)
	 croak_xs_usage(cv, "lobj, ...");
     SP -= items;
     {
	  SensorCall();SV *	lobj = ST(0);

	  SensorCall();if (sv_isobject(lobj) && sv_derived_from(lobj, "version")) {
	       SensorCall();lobj = SvRV(lobj);
	  }
	  else
	       {/*15*/SensorCall();Perl_croak(aTHX_ "lobj is not of type version");/*16*/}

	  mPUSHs(vnormal(lobj));

	  PUTBACK;
	  SensorCall();return;
     }
}

XS(XS_version_vcmp)
{
SensorCall();     dVAR;
     dXSARGS;
     SensorCall();if (items < 1)
	 croak_xs_usage(cv, "lobj, ...");
     SP -= items;
     {
	  SensorCall();SV *	lobj = ST(0);

	  SensorCall();if (sv_isobject(lobj) && sv_derived_from(lobj, "version")) {
	       SensorCall();lobj = SvRV(lobj);
	  }
	  else
	       {/*17*/SensorCall();Perl_croak(aTHX_ "lobj is not of type version");/*18*/}

	  {
	       SensorCall();SV	*rs;
	       SV	*rvs;
	       SV * robj = ST(1);
	       const IV	 swap = (IV)SvIV(ST(2));

	       SensorCall();if ( !sv_isobject(robj) || !sv_derived_from(robj, "version") )
	       {
		    SensorCall();robj = new_version(SvOK(robj) ? robj : newSVpvs_flags("0", SVs_TEMP));
		    sv_2mortal(robj);
	       }
	       SensorCall();rvs = SvRV(robj);

	       SensorCall();if ( swap )
	       {
		    SensorCall();rs = newSViv(vcmp(rvs,lobj));
	       }
	       else
	       {
		    SensorCall();rs = newSViv(vcmp(lobj,rvs));
	       }

	       mPUSHs(rs);
	  }

	  PUTBACK;
	  SensorCall();return;
     }
}

XS(XS_version_boolean)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items < 1)
	croak_xs_usage(cv, "lobj, ...");
    SP -= items;
    SensorCall();if (sv_isobject(ST(0)) && sv_derived_from(ST(0), "version")) {
	SensorCall();SV * const lobj = SvRV(ST(0));
	SV * const rs =
	    newSViv( vcmp(lobj,
			  sv_2mortal(new_version(
					sv_2mortal(newSVpvs("0"))
				    ))
			 )
		   );
	mPUSHs(rs);
	PUTBACK;
	SensorCall();return;
    }
    else
	{/*19*/SensorCall();Perl_croak(aTHX_ "lobj is not of type version");/*20*/}
SensorCall();}

XS(XS_version_noop)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items < 1)
	croak_xs_usage(cv, "lobj, ...");
    SensorCall();if (sv_isobject(ST(0)) && sv_derived_from(ST(0), "version"))
	{/*21*/SensorCall();Perl_croak(aTHX_ "operation not supported with version object");/*22*/}
    else
	{/*23*/SensorCall();Perl_croak(aTHX_ "lobj is not of type version");/*24*/}
#ifndef HASATTRIBUTE_NORETURN
    XSRETURN_EMPTY;
#endif
SensorCall();}

XS(XS_version_is_alpha)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items != 1)
	croak_xs_usage(cv, "lobj");
    SP -= items;
    SensorCall();if (sv_isobject(ST(0)) && sv_derived_from(ST(0), "version")) {
	SensorCall();SV * const lobj = ST(0);
	SensorCall();if ( hv_exists(MUTABLE_HV(SvRV(lobj)), "alpha", 5 ) )
	    XSRETURN_YES;
	else
	    XSRETURN_NO;
	PUTBACK;
	SensorCall();return;
    }
    else
	{/*25*/SensorCall();Perl_croak(aTHX_ "lobj is not of type version");/*26*/}
SensorCall();}

XS(XS_version_qv)
{
SensorCall();    dVAR;
    dXSARGS;
    PERL_UNUSED_ARG(cv);
    SP -= items;
    {
	SV * ver = ST(0);
	SV * rv;
        STRLEN len = 0;
        const char * classname = "";
        U32 flags = 0;
        SensorCall();if ( items == 2 && SvOK(ST(1)) ) {
            SensorCall();ver = ST(1);
            SensorCall();if ( sv_isobject(ST(0)) ) { /* class called as an object method */
                SensorCall();const HV * stash = SvSTASH(SvRV(ST(0)));
                classname = HvNAME(stash);
                len       = HvNAMELEN(stash);
                flags     = HvNAMEUTF8(stash) ? SVf_UTF8 : 0;
            }
            else {
	       SensorCall();classname = SvPV(ST(0), len);
                flags     = SvUTF8(ST(0));
            }
        }
	SensorCall();if ( !SvVOK(ver) ) { /* not already a v-string */
	    SensorCall();rv = sv_newmortal();
	    sv_setsv(rv,ver); /* make a duplicate */
	    upg_version(rv, TRUE);
	} else {
	    SensorCall();rv = sv_2mortal(new_version(ver));
	}
	SensorCall();if ( items == 2
                && strnNE(classname,"version", len) ) { /* inherited new() */
	    sv_bless(rv, gv_stashpvn(classname, len, GV_ADD | flags));
        }
	PUSHs(rv);
    }
    PUTBACK;
    SensorCall();return;
}

XS(XS_version_is_qv)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items != 1)
	croak_xs_usage(cv, "lobj");
    SP -= items;
    SensorCall();if (sv_isobject(ST(0)) && sv_derived_from(ST(0), "version")) {
	SensorCall();SV * const lobj = ST(0);
	SensorCall();if ( hv_exists(MUTABLE_HV(SvRV(lobj)), "qv", 2 ) )
	    XSRETURN_YES;
	else
	    XSRETURN_NO;
	PUTBACK;
	SensorCall();return;
    }
    else
	{/*27*/SensorCall();Perl_croak(aTHX_ "lobj is not of type version");/*28*/}
SensorCall();}

XS(XS_utf8_is_utf8)
{
SensorCall();     dVAR;
     dXSARGS;
     SensorCall();if (items != 1)
	 croak_xs_usage(cv, "sv");
     else {
	SensorCall();SV * const sv = ST(0);
	SvGETMAGIC(sv);
	    SensorCall();if (SvUTF8(sv))
		XSRETURN_YES;
	    else
		XSRETURN_NO;
     }
     XSRETURN_EMPTY;
}

XS(XS_utf8_valid)
{
SensorCall();     dVAR;
     dXSARGS;
     SensorCall();if (items != 1)
	 croak_xs_usage(cv, "sv");
    else {
	SensorCall();SV * const sv = ST(0);
	STRLEN len;
	const char * const s = SvPV_const(sv,len);
	SensorCall();if (!SvUTF8(sv) || is_utf8_string((const U8*)s,len))
	    XSRETURN_YES;
	else
	    XSRETURN_NO;
    }
     XSRETURN_EMPTY;
}

XS(XS_utf8_encode)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items != 1)
	croak_xs_usage(cv, "sv");
    sv_utf8_encode(ST(0));
    XSRETURN_EMPTY;
}

XS(XS_utf8_decode)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items != 1)
	croak_xs_usage(cv, "sv");
    else {
	SensorCall();SV * const sv = ST(0);
	bool RETVAL;
	SvPV_force_nolen(sv);
	RETVAL = sv_utf8_decode(sv);
	ST(0) = boolSV(RETVAL);
    }
    XSRETURN(1);
}

XS(XS_utf8_upgrade)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items != 1)
	croak_xs_usage(cv, "sv");
    else {
	SensorCall();SV * const sv = ST(0);
	STRLEN	RETVAL;
	dXSTARG;

	RETVAL = sv_utf8_upgrade(sv);
	XSprePUSH; PUSHi((IV)RETVAL);
    }
    XSRETURN(1);
}

XS(XS_utf8_downgrade)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items < 1 || items > 2)
	croak_xs_usage(cv, "sv, failok=0");
    else {
	SensorCall();SV * const sv = ST(0);
        const bool failok = (items < 2) ? 0 : (int)SvIV(ST(1));
        const bool RETVAL = sv_utf8_downgrade(sv, failok);

	ST(0) = boolSV(RETVAL);
    }
    XSRETURN(1);
}

XS(XS_utf8_native_to_unicode)
{
SensorCall(); dVAR;
 dXSARGS;
 const UV uv = SvUV(ST(0));

 SensorCall();if (items > 1)
     croak_xs_usage(cv, "sv");

 ST(0) = sv_2mortal(newSViv(NATIVE_TO_UNI(uv)));
 XSRETURN(1);
}

XS(XS_utf8_unicode_to_native)
{
SensorCall(); dVAR;
 dXSARGS;
 const UV uv = SvUV(ST(0));

 SensorCall();if (items > 1)
     croak_xs_usage(cv, "sv");

 ST(0) = sv_2mortal(newSViv(UNI_TO_NATIVE(uv)));
 XSRETURN(1);
}

XS(XS_Internals_SvREADONLY)	/* This is dangerous stuff. */
{
SensorCall();    dVAR;
    dXSARGS;
    SV * const svz = ST(0);
    SV * sv;
    PERL_UNUSED_ARG(cv);

    /* [perl #77776] - called as &foo() not foo() */
    SensorCall();if (!SvROK(svz))
        croak_xs_usage(cv, "SCALAR[, ON]");

    SensorCall();sv = SvRV(svz);

    SensorCall();if (items == 1) {
	 SensorCall();if (SvREADONLY(sv) && !SvIsCOW(sv))
	     XSRETURN_YES;
	 else
	     XSRETURN_NO;
    }
    else {/*29*/SensorCall();if (items == 2) {
	SensorCall();if (SvTRUE(ST(1))) {
	    SensorCall();if (SvIsCOW(sv)) sv_force_normal(sv);
	    SvREADONLY_on(sv);
	    XSRETURN_YES;
	}
	else {
	    /* I hope you really know what you are doing. */
	    SensorCall();if (!SvIsCOW(sv)) SvREADONLY_off(sv);
	    XSRETURN_NO;
	}
    ;/*30*/}}
    XSRETURN_UNDEF; /* Can't happen. */
}

XS(XS_Internals_SvREFCNT)	/* This is dangerous stuff. */
{
SensorCall();    dVAR;
    dXSARGS;
    SV * const svz = ST(0);
    SV * sv;
    PERL_UNUSED_ARG(cv);

    /* [perl #77776] - called as &foo() not foo() */
    SensorCall();if (!SvROK(svz))
        croak_xs_usage(cv, "SCALAR[, REFCOUNT]");

    SensorCall();sv = SvRV(svz);

    SensorCall();if (items == 1)
	 XSRETURN_UV(SvREFCNT(sv) - 1); /* Minus the ref created for us. */
    else {/*31*/SensorCall();if (items == 2) {
         /* I hope you really know what you are doing. */
	 SvREFCNT(sv) = SvUV(ST(1)) + 1; /* we free one ref on exit */
	 XSRETURN_UV(SvREFCNT(sv) - 1);
    ;/*32*/}}
    XSRETURN_UNDEF; /* Can't happen. */
}

XS(XS_Internals_hv_clear_placehold)
{
SensorCall();    dVAR;
    dXSARGS;

    SensorCall();if (items != 1 || !SvROK(ST(0)))
	croak_xs_usage(cv, "hv");
    else {
	SensorCall();HV * const hv = MUTABLE_HV(SvRV(ST(0)));
	hv_clear_placeholders(hv);
	XSRETURN(0);
    }
SensorCall();}

XS(XS_PerlIO_get_layers)
{
SensorCall();    dVAR;
    dXSARGS;
    SensorCall();if (items < 1 || items % 2 == 0)
	croak_xs_usage(cv, "filehandle[,args]");
#ifdef USE_PERLIO
    {
	SensorCall();SV *	sv;
	GV *	gv;
	IO *	io;
	bool	input = TRUE;
	bool	details = FALSE;

	SensorCall();if (items > 1) {
	     SensorCall();SV * const *svp;
	     SensorCall();for (svp = MARK + 2; svp <= SP; svp += 2) {
		  SensorCall();SV * const * const varp = svp;
		  SV * const * const valp = svp + 1;
		  STRLEN klen;
		  const char * const key = SvPV_const(*varp, klen);

		  SensorCall();switch (*key) {
		  case 'i':
		       SensorCall();if (klen == 5 && memEQ(key, "input", 5)) {
			    SensorCall();input = SvTRUE(*valp);
			    SensorCall();break;
		       }
		       SensorCall();goto fail;
		  case 'o': 
		       SensorCall();if (klen == 6 && memEQ(key, "output", 6)) {
			    SensorCall();input = !SvTRUE(*valp);
			    SensorCall();break;
		       }
		       SensorCall();goto fail;
		  case 'd':
		       SensorCall();if (klen == 7 && memEQ(key, "details", 7)) {
			    SensorCall();details = SvTRUE(*valp);
			    SensorCall();break;
		       }
		       SensorCall();goto fail;
		  default:
		  fail:
		       SensorCall();Perl_croak(aTHX_
				  "get_layers: unknown argument '%s'",
				  key);
		  }
	     }

	     SP -= (items - 1);
	}

	SensorCall();sv = POPs;
	gv = MAYBE_DEREF_GV(sv);

	SensorCall();if (!gv && !SvROK(sv))
	    gv = gv_fetchsv_nomg(sv, 0, SVt_PVIO);

	SensorCall();if (gv && (io = GvIO(gv))) {
	     SensorCall();AV* const av = PerlIO_get_layers(aTHX_ input ?
					IoIFP(io) : IoOFP(io));
	     I32 i;
	     const I32 last = av_len(av);
	     I32 nitem = 0;
	     
	     SensorCall();for (i = last; i >= 0; i -= 3) {
		  SensorCall();SV * const * const namsvp = av_fetch(av, i - 2, FALSE);
		  SV * const * const argsvp = av_fetch(av, i - 1, FALSE);
		  SV * const * const flgsvp = av_fetch(av, i,     FALSE);

		  const bool namok = namsvp && *namsvp && SvPOK(*namsvp);
		  const bool argok = argsvp && *argsvp && SvPOK(*argsvp);
		  const bool flgok = flgsvp && *flgsvp && SvIOK(*flgsvp);

		  SensorCall();if (details) {
		      /* Indents of 5? Yuck.  */
		      /* We know that PerlIO_get_layers creates a new SV for
			 the name and flags, so we can just take a reference
			 and "steal" it when we free the AV below.  */
		       XPUSHs(namok
			      ? sv_2mortal(SvREFCNT_inc_simple_NN(*namsvp))
			      : &PL_sv_undef);
		       XPUSHs(argok
			      ? newSVpvn_flags(SvPVX_const(*argsvp),
					       SvCUR(*argsvp),
					       (SvUTF8(*argsvp) ? SVf_UTF8 : 0)
					       | SVs_TEMP)
			      : &PL_sv_undef);
		       XPUSHs(flgok
			      ? sv_2mortal(SvREFCNT_inc_simple_NN(*flgsvp))
			      : &PL_sv_undef);
		       SensorCall();nitem += 3;
		  }
		  else {
		       SensorCall();if (namok && argok)
			    XPUSHs(sv_2mortal(Perl_newSVpvf(aTHX_ "%"SVf"(%"SVf")",
						 SVfARG(*namsvp),
						 SVfARG(*argsvp))));
		       else if (namok)
			   XPUSHs(sv_2mortal(SvREFCNT_inc_simple_NN(*namsvp)));
		       else
			    XPUSHs(&PL_sv_undef);
		       SensorCall();nitem++;
		       SensorCall();if (flgok) {
			    SensorCall();const IV flags = SvIVX(*flgsvp);

			    SensorCall();if (flags & PERLIO_F_UTF8) {
				 XPUSHs(newSVpvs_flags("utf8", SVs_TEMP));
				 SensorCall();nitem++;
			    }
		       }
		  }
	     }

	     SvREFCNT_dec(av);

	     XSRETURN(nitem);
	}
    }
#endif

    XSRETURN(0);
}

XS(XS_Internals_hash_seed)
{
SensorCall();    dVAR;
    /* Using dXSARGS would also have dITEM and dSP,
     * which define 2 unused local variables.  */
    dAXMARK;
    PERL_UNUSED_ARG(cv);
    PERL_UNUSED_VAR(mark);
    XSRETURN_UV(PERL_HASH_SEED);
}

XS(XS_Internals_rehash_seed)
{
SensorCall();    dVAR;
    /* Using dXSARGS would also have dITEM and dSP,
     * which define 2 unused local variables.  */
    dAXMARK;
    PERL_UNUSED_ARG(cv);
    PERL_UNUSED_VAR(mark);
    XSRETURN_UV(PL_rehash_seed);
}

XS(XS_Internals_HvREHASH)	/* Subject to change  */
{
SensorCall();    dVAR;
    dXSARGS;
    PERL_UNUSED_ARG(cv);
    SensorCall();if (SvROK(ST(0))) {
	SensorCall();const HV * const hv = (const HV *) SvRV(ST(0));
	SensorCall();if (items == 1 && SvTYPE(hv) == SVt_PVHV) {
	    SensorCall();if (HvREHASH(hv))
		XSRETURN_YES;
	    else
		XSRETURN_NO;
	}
    }
    SensorCall();Perl_croak(aTHX_ "Internals::HvREHASH $hashref");
SensorCall();}

XS(XS_re_is_regexp)
{
SensorCall();    dVAR; 
    dXSARGS;
    PERL_UNUSED_VAR(cv);

    SensorCall();if (items != 1)
	croak_xs_usage(cv, "sv");

    SensorCall();if (SvRXOK(ST(0))) {
        XSRETURN_YES;
    } else {
        XSRETURN_NO;
    }
SensorCall();}

XS(XS_re_regnames_count)
{
    SensorCall();REGEXP *rx = PL_curpm ? PM_GETRE(PL_curpm) : NULL;
    SV * ret;
    dVAR; 
    dXSARGS;

    SensorCall();if (items != 0)
	croak_xs_usage(cv, "");

    SP -= items;
    PUTBACK;

    SensorCall();if (!rx)
        XSRETURN_UNDEF;

    SensorCall();ret = CALLREG_NAMED_BUFF_COUNT(rx);

    SPAGAIN;
    PUSHs(ret ? sv_2mortal(ret) : &PL_sv_undef);
    XSRETURN(1);
}

XS(XS_re_regname)
{
SensorCall();    dVAR;
    dXSARGS;
    REGEXP * rx;
    U32 flags;
    SV * ret;

    SensorCall();if (items < 1 || items > 2)
	croak_xs_usage(cv, "name[, all ]");

    SP -= items;
    PUTBACK;

    SensorCall();rx = PL_curpm ? PM_GETRE(PL_curpm) : NULL;

    SensorCall();if (!rx)
        XSRETURN_UNDEF;

    SensorCall();if (items == 2 && SvTRUE(ST(1))) {
        SensorCall();flags = RXapif_ALL;
    } else {
        SensorCall();flags = RXapif_ONE;
    }
    SensorCall();ret = CALLREG_NAMED_BUFF_FETCH(rx, ST(0), (flags | RXapif_REGNAME));

    SPAGAIN;
    PUSHs(ret ? sv_2mortal(ret) : &PL_sv_undef);
    XSRETURN(1);
}


XS(XS_re_regnames)
{
SensorCall();    dVAR;
    dXSARGS;
    REGEXP * rx;
    U32 flags;
    SV *ret;
    AV *av;
    I32 length;
    I32 i;
    SV **entry;

    SensorCall();if (items > 1)
	croak_xs_usage(cv, "[all]");

    SensorCall();rx = PL_curpm ? PM_GETRE(PL_curpm) : NULL;

    SensorCall();if (!rx)
        XSRETURN_UNDEF;

    SensorCall();if (items == 1 && SvTRUE(ST(0))) {
        SensorCall();flags = RXapif_ALL;
    } else {
        SensorCall();flags = RXapif_ONE;
    }

    SP -= items;
    PUTBACK;

    SensorCall();ret = CALLREG_NAMED_BUFF_ALL(rx, (flags | RXapif_REGNAMES));

    SPAGAIN;

    SensorCall();if (!ret)
        XSRETURN_UNDEF;

    SensorCall();av = MUTABLE_AV(SvRV(ret));
    length = av_len(av);

    SensorCall();for (i = 0; i <= length; i++) {
        SensorCall();entry = av_fetch(av, i, FALSE);
        
        SensorCall();if (!entry)
            {/*33*/SensorCall();Perl_croak(aTHX_ "NULL array element in re::regnames()");/*34*/}

        mXPUSHs(SvREFCNT_inc_simple_NN(*entry));
    }

    SvREFCNT_dec(ret);

    PUTBACK;
    SensorCall();return;
}

XS(XS_re_regexp_pattern)
{
SensorCall();    dVAR;
    dXSARGS;
    REGEXP *re;

    SensorCall();if (items != 1)
	croak_xs_usage(cv, "sv");

    SP -= items;

    /*
       Checks if a reference is a regex or not. If the parameter is
       not a ref, or is not the result of a qr// then returns false
       in scalar context and an empty list in list context.
       Otherwise in list context it returns the pattern and the
       modifiers, in scalar context it returns the pattern just as it
       would if the qr// was stringified normally, regardless as
       to the class of the variable and any stringification overloads
       on the object.
    */

    SensorCall();if ((re = SvRX(ST(0)))) /* assign deliberate */
    {
        /* Houston, we have a regex! */
        SensorCall();SV *pattern;

        SensorCall();if ( GIMME_V == G_ARRAY ) {
	    SensorCall();STRLEN left = 0;
	    char reflags[sizeof(INT_PAT_MODS) + MAX_CHARSET_NAME_LENGTH];
            const char *fptr;
            char ch;
            U16 match_flags;

            /*
               we are in list context so stringify
               the modifiers that apply. We ignore "negative
               modifiers" in this scenario, and the default character set
            */

	    SensorCall();if (get_regex_charset(RX_EXTFLAGS(re)) != REGEX_DEPENDS_CHARSET) {
		SensorCall();STRLEN len;
		const char* const name = get_regex_charset_name(RX_EXTFLAGS(re),
								&len);
		Copy(name, reflags + left, len, char);
		left += len;
	    }
            SensorCall();fptr = INT_PAT_MODS;
            match_flags = (U16)((RX_EXTFLAGS(re) & RXf_PMf_COMPILETIME)
                                    >> RXf_PMf_STD_PMMOD_SHIFT);

            SensorCall();while((ch = *fptr++)) {
                SensorCall();if(match_flags & 1) {
                    SensorCall();reflags[left++] = ch;
                }
                SensorCall();match_flags >>= 1;
            }

            SensorCall();pattern = newSVpvn_flags(RX_PRECOMP(re),RX_PRELEN(re),
				     (RX_UTF8(re) ? SVf_UTF8 : 0) | SVs_TEMP);

            /* return the pattern and the modifiers */
            XPUSHs(pattern);
            XPUSHs(newSVpvn_flags(reflags, left, SVs_TEMP));
            XSRETURN(2);
        } else {
            /* Scalar, so use the string that Perl would return */
            /* return the pattern in (?msix:..) format */
#if PERL_VERSION >= 11
            SensorCall();pattern = sv_2mortal(newSVsv(MUTABLE_SV(re)));
#else
            pattern = newSVpvn_flags(RX_WRAPPED(re), RX_WRAPLEN(re),
				     (RX_UTF8(re) ? SVf_UTF8 : 0) | SVs_TEMP);
#endif
            XPUSHs(pattern);
            XSRETURN(1);
        }
    } else {
        /* It ain't a regexp folks */
        SensorCall();if ( GIMME_V == G_ARRAY ) {
            /* return the empty list */
            XSRETURN_UNDEF;
        } else {
            /* Because of the (?:..) wrapping involved in a
               stringified pattern it is impossible to get a
               result for a real regexp that would evaluate to
               false. Therefore we can return PL_sv_no to signify
               that the object is not a regex, this means that one
               can say

                 if (regex($might_be_a_regex) eq '(?:foo)') { }

               and not worry about undefined values.
            */
            XSRETURN_NO;
        }
    }
    /* NOT-REACHED */
SensorCall();}

struct xsub_details {
    const char *name;
    XSUBADDR_t xsub;
    const char *proto;
};

struct xsub_details details[] = {
    {"UNIVERSAL::isa", XS_UNIVERSAL_isa, NULL},
    {"UNIVERSAL::can", XS_UNIVERSAL_can, NULL},
    {"UNIVERSAL::DOES", XS_UNIVERSAL_DOES, NULL},
    {"UNIVERSAL::VERSION", XS_UNIVERSAL_VERSION, NULL},
    {"version::()", XS_version_noop, NULL},
    {"version::new", XS_version_new, NULL},
    {"version::parse", XS_version_new, NULL},
    {"version::(\"\"", XS_version_stringify, NULL},
    {"version::stringify", XS_version_stringify, NULL},
    {"version::(0+", XS_version_numify, NULL},
    {"version::numify", XS_version_numify, NULL},
    {"version::normal", XS_version_normal, NULL},
    {"version::(cmp", XS_version_vcmp, NULL},
    {"version::(<=>", XS_version_vcmp, NULL},
    {"version::vcmp", XS_version_vcmp, NULL},
    {"version::(bool", XS_version_boolean, NULL},
    {"version::boolean", XS_version_boolean, NULL},
    {"version::(+", XS_version_noop, NULL},
    {"version::(-", XS_version_noop, NULL},
    {"version::(*", XS_version_noop, NULL},
    {"version::(/", XS_version_noop, NULL},
    {"version::(+=", XS_version_noop, NULL},
    {"version::(-=", XS_version_noop, NULL},
    {"version::(*=", XS_version_noop, NULL},
    {"version::(/=", XS_version_noop, NULL},
    {"version::(abs", XS_version_noop, NULL},
    {"version::(nomethod", XS_version_noop, NULL},
    {"version::noop", XS_version_noop, NULL},
    {"version::is_alpha", XS_version_is_alpha, NULL},
    {"version::qv", XS_version_qv, NULL},
    {"version::declare", XS_version_qv, NULL},
    {"version::is_qv", XS_version_is_qv, NULL},
    {"utf8::is_utf8", XS_utf8_is_utf8, NULL},
    {"utf8::valid", XS_utf8_valid, NULL},
    {"utf8::encode", XS_utf8_encode, NULL},
    {"utf8::decode", XS_utf8_decode, NULL},
    {"utf8::upgrade", XS_utf8_upgrade, NULL},
    {"utf8::downgrade", XS_utf8_downgrade, NULL},
    {"utf8::native_to_unicode", XS_utf8_native_to_unicode, NULL},
    {"utf8::unicode_to_native", XS_utf8_unicode_to_native, NULL},
    {"Internals::SvREADONLY", XS_Internals_SvREADONLY, "\\[$%@];$"},
    {"Internals::SvREFCNT", XS_Internals_SvREFCNT, "\\[$%@];$"},
    {"Internals::hv_clear_placeholders", XS_Internals_hv_clear_placehold, "\\%"},
    {"PerlIO::get_layers", XS_PerlIO_get_layers, "*;@"},
    {"Internals::hash_seed", XS_Internals_hash_seed, ""},
    {"Internals::rehash_seed", XS_Internals_rehash_seed, ""},
    {"Internals::HvREHASH", XS_Internals_HvREHASH, "\\%"},
    {"re::is_regexp", XS_re_is_regexp, "$"},
    {"re::regname", XS_re_regname, ";$$"},
    {"re::regnames", XS_re_regnames, ";$"},
    {"re::regnames_count", XS_re_regnames_count, ""},
    {"re::regexp_pattern", XS_re_regexp_pattern, "$"},
};

void
Perl_boot_core_UNIVERSAL(pTHX)
{
SensorCall();    dVAR;
    static const char file[] = __FILE__;
    struct xsub_details *xsub = details;
    const struct xsub_details *end
	= details + sizeof(details) / sizeof(details[0]);

    SensorCall();do {
	newXS_flags(xsub->name, xsub->xsub, file, xsub->proto, 0);
    } while (++xsub < end);

    /* register the overloading (type 'A') magic */
    PL_amagic_generation++;

    /* Providing a Regexp::DESTROY fixes #21347. See test in t/op/ref.t  */
    {
	SensorCall();CV * const cv =
	    newCONSTSUB(get_hv("Regexp::", GV_ADD), "DESTROY", NULL);
	Safefree(CvFILE(cv));
	CvFILE(cv) = (char *)file;
	CvDYNFILE_off(cv);
    }
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
