#include "var/tmp/sensor.h"
/*    pp_ctl.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 *      Now far ahead the Road has gone,
 *          And I must follow, if I can,
 *      Pursuing it with eager feet,
 *          Until it joins some larger way
 *      Where many paths and errands meet.
 *          And whither then?  I cannot say.
 *
 *     [Bilbo on p.35 of _The Lord of the Rings_, I/i: "A Long-Expected Party"]
 */

/* This file contains control-oriented pp ("push/pop") functions that
 * execute the opcodes that make up a perl program. A typical pp function
 * expects to find its arguments on the stack, and usually pushes its
 * results onto the stack, hence the 'pp' terminology. Each OP structure
 * contains a pointer to the relevant pp_foo() function.
 *
 * Control-oriented means things like pp_enteriter() and pp_next(), which
 * alter the flow of control of the program.
 */


#include "EXTERN.h"
#define PERL_IN_PP_CTL_C
#include "perl.h"

#define DOCATCH(o) ((CATCH_GET == TRUE) ? docatch(o) : (o))

#define dopoptosub(plop)	dopoptosub_at(cxstack, (plop))

PP(pp_wantarray)
{
SensorCall(13062);    dVAR;
    dSP;
    I32 cxix;
    const PERL_CONTEXT *cx;
    EXTEND(SP, 1);

    SensorCall(13067);if (PL_op->op_private & OPpOFFBYONE) {
	SensorCall(13063);if (!(cx = caller_cx(1,NULL))) RETPUSHUNDEF;
    }
    else {
      SensorCall(13064);cxix = dopoptosub(cxstack_ix);
      SensorCall(13065);if (cxix < 0)
	RETPUSHUNDEF;
      SensorCall(13066);cx = &cxstack[cxix];
    }

    SensorCall(13068);switch (cx->blk_gimme) {
    case G_ARRAY:
	RETPUSHYES;
    case G_SCALAR:
	RETPUSHNO;
    default:
	RETPUSHUNDEF;
    }
SensorCall(13069);}

PP(pp_regcreset)
{
SensorCall(13070);    dVAR;
    /* XXXX Should store the old value to allow for tie/overload - and
       restore in regcomp, where marked with XXXX. */
    PL_reginterp_cnt = 0;
    TAINT_NOT;
    {OP * ReplaceReturn1095 = NORMAL; SensorCall(13071); return ReplaceReturn1095;}
}

PP(pp_regcomp)
{
SensorCall(13072);    dVAR;
    dSP;
    register PMOP *pm = (PMOP*)cLOGOP->op_other;
    SV *tmpstr;
    REGEXP *re = NULL;

    /* prevent recompiling under /o and ithreads. */
#if defined(USE_ITHREADS)
    SensorCall(13074);if (pm->op_pmflags & PMf_KEEP && PM_GETRE(pm)) {
	SensorCall(13073);if (PL_op->op_flags & OPf_STACKED) {
	    dMARK;
	    SP = MARK;
	}
	else
	    (void)POPs;
	RETURN;
    }
#endif

#define tryAMAGICregexp(rx)			\
    STMT_START {				\
	SvGETMAGIC(rx);				\
	if (SvROK(rx) && SvAMAGIC(rx)) {	\
	    SV *sv = AMG_CALLunary(rx, regexp_amg); \
	    if (sv) {				\
		if (SvROK(sv))			\
		    sv = SvRV(sv);		\
		if (SvTYPE(sv) != SVt_REGEXP)	\
		    Perl_croak(aTHX_ "Overloaded qr did not return a REGEXP"); \
		rx = sv;			\
	    }					\
	}					\
    } STMT_END
	    

    SensorCall(13083);if (PL_op->op_flags & OPf_STACKED) {
	/* multiple args; concatenate them */
	dMARK; dORIGMARK;
	SensorCall(13075);tmpstr = PAD_SV(ARGTARG);
	sv_setpvs(tmpstr, "");
	SensorCall(13081);while (++MARK <= SP) {
	    SensorCall(13076);SV *msv = *MARK;
	    SV *sv;

	    tryAMAGICregexp(msv);

	    SensorCall(13078);if ((SvAMAGIC(tmpstr) || SvAMAGIC(msv)) &&
		(sv = amagic_call(tmpstr, msv, concat_amg, AMGf_assign)))
	    {
	       sv_setsv(tmpstr, sv);
	       SensorCall(13077);continue;
	    }

	    SensorCall(13080);if (SvROK(msv) && SvTYPE(SvRV(msv)) == SVt_REGEXP) {
		SensorCall(13079);msv = SvRV(msv);
		PL_reginterp_cnt +=
		    RX_SEEN_EVALS((REGEXP *)MUTABLE_PTR(msv));
	    }

	    sv_catsv_nomg(tmpstr, msv);
	}
    	SvSETMAGIC(tmpstr);
	SP = ORIGMARK;
    }
    else {
	SensorCall(13082);tmpstr = POPs;
	tryAMAGICregexp(tmpstr);
    }

#undef tryAMAGICregexp

    SensorCall(13089);if (SvROK(tmpstr)) {
	SensorCall(13084);SV * const sv = SvRV(tmpstr);
	SensorCall(13086);if (SvTYPE(sv) == SVt_REGEXP)
	    {/*249*/SensorCall(13085);re = (REGEXP*) sv;/*250*/}
    }
    else {/*251*/SensorCall(13087);if (SvTYPE(tmpstr) == SVt_REGEXP)
	{/*253*/SensorCall(13088);re = (REGEXP*) tmpstr;/*254*/}/*252*/}

    SensorCall(13107);if (re) {
	/* The match's LHS's get-magic might need to access this op's reg-
	   exp (as is sometimes the case with $';  see bug 70764).  So we
	   must call get-magic now before we replace the regexp. Hopeful-
	   ly this hack can be replaced with the approach described at
	   http://www.nntp.perl.org/group/perl.perl5.porters/2007/03
	   /msg122415.html some day. */
	SensorCall(13090);if(pm->op_type == OP_MATCH) {
	 SensorCall(13091);SV *lhs;
	 const bool was_tainted = PL_tainted;
	 SensorCall(13092);if (pm->op_flags & OPf_STACKED)
	    lhs = TOPs;
	 else if (pm->op_private & OPpTARGET_MY)
	    lhs = PAD_SV(pm->op_targ);
	 else lhs = DEFSV;
	 SvGETMAGIC(lhs);
	 /* Restore the previous value of PL_tainted (which may have been
	    modified by get-magic), to avoid incorrectly setting the
	    RXf_TAINTED flag further down. */
	 PL_tainted = was_tainted;
	}

	SensorCall(13093);re = reg_temp_copy(NULL, re);
	ReREFCNT_dec(PM_GETRE(pm));
	PM_SETRE(pm, re);
    }
    else {
	SensorCall(13094);STRLEN len = 0;
	const char *t = SvOK(tmpstr) ? SvPV_nomg_const(tmpstr, len) : "";

	re = PM_GETRE(pm);
	assert (re != (REGEXP*) &PL_sv_undef);

	/* Check against the last compiled regexp. */
	SensorCall(13106);if (!re || !RX_PRECOMP(re) || RX_PRELEN(re) != len ||
	    memNE(RX_PRECOMP(re), t, len))
	{
	    SensorCall(13095);const regexp_engine *eng = re ? RX_ENGINE(re) : NULL;
            U32 pm_flags = pm->op_pmflags & RXf_PMf_COMPILETIME;
	    SensorCall(13099);if (re) {
	        ReREFCNT_dec(re);
#ifdef USE_ITHREADS
		PM_SETRE(pm, (REGEXP*) &PL_sv_undef);
#else
		PM_SETRE(pm, NULL);	/* crucial if regcomp aborts */
#endif
	    } else {/*255*/SensorCall(13096);if (PL_curcop->cop_hints_hash) {
	        SensorCall(13097);SV *ptr = cop_hints_fetch_pvs(PL_curcop, "regcomp", 0);
                SensorCall(13098);if (ptr && SvIOK(ptr) && SvIV(ptr))
                    eng = INT2PTR(regexp_engine*,SvIV(ptr));
	    ;/*256*/}}

	    SensorCall(13100);if (PL_op->op_flags & OPf_SPECIAL)
		PL_reginterp_cnt = I32_MAX; /* Mark as safe.  */

	    SensorCall(13104);if (!DO_UTF8(tmpstr) && SvUTF8(tmpstr)) {
		/* Not doing UTF-8, despite what the SV says. Is this only if
		   we're trapped in use 'bytes'?  */
		/* Make a copy of the octet sequence, but without the flag on,
		   as the compiler now honours the SvUTF8 flag on tmpstr.  */
		SensorCall(13101);STRLEN len;
		const char *const p = SvPV(tmpstr, len);
		tmpstr = newSVpvn_flags(p, len, SVs_TEMP);
	    }
	    else {/*257*/SensorCall(13102);if (SvAMAGIC(tmpstr) || SvGMAGICAL(tmpstr)) {
		/* make a copy to avoid extra stringifies */
		SensorCall(13103);tmpstr = newSVpvn_flags(t, len, SVs_TEMP | SvUTF8(tmpstr));
	    ;/*258*/}}

	    SensorCall(13105);if (eng)
	        PM_SETRE(pm, CALLREGCOMP_ENG(eng, tmpstr, pm_flags));
	    else
	        PM_SETRE(pm, CALLREGCOMP(tmpstr, pm_flags));

	    PL_reginterp_cnt = 0;	/* XXXX Be extra paranoid - needed
					   inside tie/overload accessors.  */
	}
    }
    
    SensorCall(13108);re = PM_GETRE(pm);

#ifndef INCOMPLETE_TAINTS
    SensorCall(13110);if (PL_tainting) {
	SensorCall(13109);if (PL_tainted) {
	    SvTAINTED_on((SV*)re);
	    RX_EXTFLAGS(re) |= RXf_TAINTED;
	}
    }
#endif

    SensorCall(13111);if (!RX_PRELEN(PM_GETRE(pm)) && PL_curpm)
	pm = PL_curpm;


#if !defined(USE_ITHREADS)
    /* can't change the optree at runtime either */
    /* PMf_KEEP is handled differently under threads to avoid these problems */
    if (pm->op_pmflags & PMf_KEEP) {
	pm->op_private &= ~OPpRUNTIME;	/* no point compiling again */
	cLOGOP->op_first->op_next = PL_op->op_next;
    }
#endif
    RETURN;
}

PP(pp_substcont)
{
SensorCall(13112);    dVAR;
    dSP;
    register PERL_CONTEXT *cx = &cxstack[cxstack_ix];
    register PMOP * const pm = (PMOP*) cLOGOP->op_other;
    register SV * const dstr = cx->sb_dstr;
    register char *s = cx->sb_s;
    register char *m = cx->sb_m;
    char *orig = cx->sb_orig;
    register REGEXP * const rx = cx->sb_rx;
    SV *nsv = NULL;
    REGEXP *old = PM_GETRE(pm);

    PERL_ASYNC_CHECK();

    SensorCall(13114);if(old != rx) {
	SensorCall(13113);if(old)
	    ReREFCNT_dec(old);
	PM_SETRE(pm,ReREFCNT_inc(rx));
    }

    rxres_restore(&cx->sb_rxres, rx);
    RX_MATCH_UTF8_set(rx, DO_UTF8(cx->sb_targ));

    SensorCall(13135);if (cx->sb_iters++) {
	SensorCall(13115);const I32 saviters = cx->sb_iters;
	SensorCall(13116);if (cx->sb_iters > cx->sb_maxiters)
	    DIE(aTHX_ "Substitution loop");

	SvGETMAGIC(TOPs); /* possibly clear taint on $1 etc: #67962 */

    	/* See "how taint works" above pp_subst() */
	SensorCall(13117);if (SvTAINTED(TOPs))
	    cx->sb_rxtainted |= SUBST_TAINT_REPL;
	sv_catsv_nomg(dstr, POPs);
	/* XXX: adjust for positive offsets of \G for instance s/(.)\G//g with positive pos() */
	SensorCall(13118);s -= RX_GOFS(rx);

	/* Are we done */
	/* I believe that we can't set REXEC_SCREAM here if
	   SvSCREAM(cx->sb_targ) is true because SvPVX(cx->sb_targ) isn't always
	   equal to s.  [See the comment before Perl_re_intuit_start(), which is
	   called from Perl_regexec_flags(), which says that it should be when
	   SvSCREAM() is true.]  s, cx->sb_strend and orig will be consistent
	   with SvPVX(cx->sb_targ), as substconst doesn't modify cx->sb_targ
	   during the match.  */
	SensorCall(13133);if (CxONCE(cx) || s < orig ||
		!CALLREGEXEC(rx, s, cx->sb_strend, orig,
			     (s == m) + RX_GOFS(rx), cx->sb_targ, NULL,
			     ((cx->sb_rflags & REXEC_COPY_STR)
			      ? (REXEC_IGNOREPOS|REXEC_NOT_FIRST)
			      : (REXEC_COPY_STR|REXEC_IGNOREPOS|REXEC_NOT_FIRST))))
	{
	    SensorCall(13119);SV *targ = cx->sb_targ;

	    assert(cx->sb_strend >= s);
	    SensorCall(13121);if(cx->sb_strend > s) {
		 SensorCall(13120);if (DO_UTF8(dstr) && !SvUTF8(targ))
		      sv_catpvn_utf8_upgrade(dstr, s, cx->sb_strend - s, nsv);
		 else
		      sv_catpvn(dstr, s, cx->sb_strend - s);
	    }
	    SensorCall(13122);if (RX_MATCH_TAINTED(rx)) /* run time pattern taint, eg locale */
		cx->sb_rxtainted |= SUBST_TAINT_PAT;

	    SensorCall(13127);if (pm->op_pmflags & PMf_NONDESTRUCT) {
		PUSHs(dstr);
		/* From here on down we're using the copy, and leaving the
		   original untouched.  */
		SensorCall(13123);targ = dstr;
	    }
	    else {
		SensorCall(13124);if (SvIsCOW(targ)) {
		    sv_force_normal_flags(targ, SV_COW_DROP_PV);
		} else
		{
		    SvPV_free(targ);
		}
		SvPV_set(targ, SvPVX(dstr));
		SvCUR_set(targ, SvCUR(dstr));
		SvLEN_set(targ, SvLEN(dstr));
		SensorCall(13125);if (DO_UTF8(dstr))
		    SvUTF8_on(targ);
		SvPV_set(dstr, NULL);

		mPUSHi(saviters - 1);

		SensorCall(13126);(void)SvPOK_only_UTF8(targ);
	    }

	    /* update the taint state of various various variables in
	     * preparation for final exit.
	     * See "how taint works" above pp_subst() */
	    SensorCall(13131);if (PL_tainting) {
		SensorCall(13128);if ((cx->sb_rxtainted & SUBST_TAINT_PAT) ||
		    ((cx->sb_rxtainted & (SUBST_TAINT_STR|SUBST_TAINT_RETAINT))
				    == (SUBST_TAINT_STR|SUBST_TAINT_RETAINT))
		)
		    {/*277*/SensorCall(13129);(RX_MATCH_TAINTED_on(rx));/*278*/} /* taint $1 et al */

		SensorCall(13130);if (!(cx->sb_rxtainted & SUBST_TAINT_BOOLRET)
		    && (cx->sb_rxtainted & (SUBST_TAINT_STR|SUBST_TAINT_PAT))
		)
		    SvTAINTED_on(TOPs);  /* taint return value */
		/* needed for mg_set below */
		PL_tainted = cBOOL(cx->sb_rxtainted &
			    (SUBST_TAINT_STR|SUBST_TAINT_PAT|SUBST_TAINT_REPL));
		SvTAINT(TARG);
	    }
	    /* PL_tainted must be correctly set for this mg_set */
	    SvSETMAGIC(TARG);
	    TAINT_NOT;
	    LEAVE_SCOPE(cx->sb_oldsave);
SensorCall(13132);	    POPSUBST(cx);
	    PERL_ASYNC_CHECK();
	    RETURNOP(pm->op_next);
	    /* NOTREACHED */
	}
	SensorCall(13134);cx->sb_iters = saviters;
    }
    SensorCall(13137);if (RX_MATCH_COPIED(rx) && RX_SUBBEG(rx) != orig) {
	SensorCall(13136);m = s;
	s = orig;
	cx->sb_orig = orig = RX_SUBBEG(rx);
	s = orig + (m - s);
	cx->sb_strend = s + (cx->sb_strend - m);
    }
    SensorCall(13138);cx->sb_m = m = RX_OFFS(rx)[0].start + orig;
    SensorCall(13140);if (m > s) {
	SensorCall(13139);if (DO_UTF8(dstr) && !SvUTF8(cx->sb_targ))
	    sv_catpvn_utf8_upgrade(dstr, s, m - s, nsv);
	else
	    sv_catpvn(dstr, s, m-s);
    }
    SensorCall(13141);cx->sb_s = RX_OFFS(rx)[0].end + orig;
    { /* Update the pos() information. */
	SV * const sv
	    = (pm->op_pmflags & PMf_NONDESTRUCT) ? cx->sb_dstr : cx->sb_targ;
	MAGIC *mg;
	SvUPGRADE(sv, SVt_PVMG);
	SensorCall(13143);if (!(mg = mg_find(sv, PERL_MAGIC_regex_global))) {
#ifdef PERL_OLD_COPY_ON_WRITE
	    if (SvIsCOW(sv))
		sv_force_normal_flags(sv, 0);
#endif
	    SensorCall(13142);mg = sv_magicext(sv, NULL, PERL_MAGIC_regex_global, &PL_vtbl_mglob,
			     NULL, 0);
	}
	SensorCall(13144);mg->mg_len = m - orig;
    }
    SensorCall(13145);if (old != rx)
	(void)ReREFCNT_inc(rx);
    /* update the taint state of various various variables in preparation
     * for calling the code block.
     * See "how taint works" above pp_subst() */
    SensorCall(13150);if (PL_tainting) {
	SensorCall(13146);if (RX_MATCH_TAINTED(rx)) /* run time pattern taint, eg locale */
	    cx->sb_rxtainted |= SUBST_TAINT_PAT;

	SensorCall(13148);if ((cx->sb_rxtainted & SUBST_TAINT_PAT) ||
	    ((cx->sb_rxtainted & (SUBST_TAINT_STR|SUBST_TAINT_RETAINT))
			    == (SUBST_TAINT_STR|SUBST_TAINT_RETAINT))
	)
	    {/*279*/SensorCall(13147);(RX_MATCH_TAINTED_on(rx));/*280*/} /* taint $1 et al */

	SensorCall(13149);if (cx->sb_iters > 1 && (cx->sb_rxtainted & 
			(SUBST_TAINT_STR|SUBST_TAINT_PAT|SUBST_TAINT_REPL)))
	    SvTAINTED_on((pm->op_pmflags & PMf_NONDESTRUCT)
			 ? cx->sb_dstr : cx->sb_targ);
	TAINT_NOT;
    }
    rxres_save(&cx->sb_rxres, rx);
    PL_curpm = pm;
    RETURNOP(pm->op_pmstashstartu.op_pmreplstart);
}

void
Perl_rxres_save(pTHX_ void **rsp, REGEXP *rx)
{
    SensorCall(13151);UV *p = (UV*)*rsp;
    U32 i;

    PERL_ARGS_ASSERT_RXRES_SAVE;
    PERL_UNUSED_CONTEXT;

    SensorCall(13155);if (!p || p[1] < RX_NPARENS(rx)) {
#ifdef PERL_OLD_COPY_ON_WRITE
	i = 7 + RX_NPARENS(rx) * 2;
#else
	SensorCall(13152);i = 6 + RX_NPARENS(rx) * 2;
#endif
	SensorCall(13153);if (!p)
	    Newx(p, i, UV);
	else
	    Renew(p, i, UV);
	SensorCall(13154);*rsp = (void*)p;
    }

    SensorCall(13156);*p++ = PTR2UV(RX_MATCH_COPIED(rx) ? RX_SUBBEG(rx) : NULL);
    RX_MATCH_COPIED_off(rx);

#ifdef PERL_OLD_COPY_ON_WRITE
    *p++ = PTR2UV(RX_SAVED_COPY(rx));
    RX_SAVED_COPY(rx) = NULL;
#endif

    *p++ = RX_NPARENS(rx);

    *p++ = PTR2UV(RX_SUBBEG(rx));
    *p++ = (UV)RX_SUBLEN(rx);
    SensorCall(13158);for (i = 0; i <= RX_NPARENS(rx); ++i) {
	SensorCall(13157);*p++ = (UV)RX_OFFS(rx)[i].start;
	*p++ = (UV)RX_OFFS(rx)[i].end;
    }
SensorCall(13159);}

static void
S_rxres_restore(pTHX_ void **rsp, REGEXP *rx)
{
    SensorCall(13160);UV *p = (UV*)*rsp;
    U32 i;

    PERL_ARGS_ASSERT_RXRES_RESTORE;
    PERL_UNUSED_CONTEXT;

    RX_MATCH_COPY_FREE(rx);
    RX_MATCH_COPIED_set(rx, *p);
    *p++ = 0;

#ifdef PERL_OLD_COPY_ON_WRITE
    if (RX_SAVED_COPY(rx))
	SvREFCNT_dec (RX_SAVED_COPY(rx));
    RX_SAVED_COPY(rx) = INT2PTR(SV*,*p);
    *p++ = 0;
#endif

    RX_NPARENS(rx) = *p++;

    RX_SUBBEG(rx) = INT2PTR(char*,*p++);
    RX_SUBLEN(rx) = (I32)(*p++);
    SensorCall(13161);for (i = 0; i <= RX_NPARENS(rx); ++i) {
	RX_OFFS(rx)[i].start = (I32)(*p++);
	RX_OFFS(rx)[i].end = (I32)(*p++);
    }
SensorCall(13162);}

static void
S_rxres_free(pTHX_ void **rsp)
{
    SensorCall(13163);UV * const p = (UV*)*rsp;

    PERL_ARGS_ASSERT_RXRES_FREE;
    PERL_UNUSED_CONTEXT;

    SensorCall(13165);if (p) {
#ifdef PERL_POISON
	void *tmp = INT2PTR(char*,*p);
	Safefree(tmp);
	if (*p)
	    PoisonFree(*p, 1, sizeof(*p));
#else
	Safefree(INT2PTR(char*,*p));
#endif
#ifdef PERL_OLD_COPY_ON_WRITE
	if (p[1]) {
	    SvREFCNT_dec (INT2PTR(SV*,p[1]));
	}
#endif
	Safefree(p);
	SensorCall(13164);*rsp = NULL;
    }
SensorCall(13166);}

#define FORM_NUM_BLANK (1<<30)
#define FORM_NUM_POINT (1<<29)

PP(pp_formline)
{
SensorCall(13167);    dVAR; dSP; dMARK; dORIGMARK;
    register SV * const tmpForm = *++MARK;
    SV *formsv;		    /* contains text of original format */
    register U32 *fpc;	    /* format ops program counter */
    register char *t;	    /* current append position in target string */
    const char *f;	    /* current position in format string */
    register I32 arg;
    register SV *sv = NULL; /* current item */
    const char *item = NULL;/* string value of current item */
    I32 itemsize  = 0;	    /* length of current item, possibly truncated */
    I32 fieldsize = 0;	    /* width of current field */
    I32 lines = 0;	    /* number of lines that have been output */
    bool chopspace = (strchr(PL_chopset, ' ') != NULL); /* does $: have space */
    const char *chophere = NULL; /* where to chop current item */
    STRLEN linemark = 0;    /* pos of start of line in output */
    NV value;
    bool gotsome = FALSE;   /* seen at least one non-blank item on this line */
    STRLEN len;
    STRLEN linemax;	    /* estimate of output size in bytes */
    bool item_is_utf8 = FALSE;
    bool targ_is_utf8 = FALSE;
    const char *fmt;
    MAGIC *mg = NULL;
    U8 *source;		    /* source of bytes to append */
    STRLEN to_copy;	    /* how may bytes to append */
    char trans;		    /* what chars to translate */

    mg = doparseform(tmpForm);

    fpc = (U32*)mg->mg_ptr;
    /* the actual string the format was compiled from.
     * with overload etc, this may not match tmpForm */
    formsv = mg->mg_obj;


    SvPV_force(PL_formtarget, len);
    SensorCall(13168);if (SvTAINTED(tmpForm) || SvTAINTED(formsv))
	SvTAINTED_on(PL_formtarget);
    SensorCall(13169);if (DO_UTF8(PL_formtarget))
	targ_is_utf8 = TRUE;
    SensorCall(13170);linemax = (SvCUR(formsv) * (IN_BYTES ? 1 : 3) + 1);
    t = SvGROW(PL_formtarget, len + linemax + 1);
    /* XXX from now onwards, SvCUR(PL_formtarget) is invalid */
    t += len;
    f = SvPV_const(formsv, len);

    SensorCall(13360);for (;;) {
	DEBUG_f( {
	    const char *name = "???";
	    arg = -1;
	    switch (*fpc) {
	    case FF_LITERAL:	arg = fpc[1]; name = "LITERAL";	break;
	    case FF_BLANK:	arg = fpc[1]; name = "BLANK";	break;
	    case FF_SKIP:	arg = fpc[1]; name = "SKIP";	break;
	    case FF_FETCH:	arg = fpc[1]; name = "FETCH";	break;
	    case FF_DECIMAL:	arg = fpc[1]; name = "DECIMAL";	break;

	    case FF_CHECKNL:	name = "CHECKNL";	break;
	    case FF_CHECKCHOP:	name = "CHECKCHOP";	break;
	    case FF_SPACE:	name = "SPACE";		break;
	    case FF_HALFSPACE:	name = "HALFSPACE";	break;
	    case FF_ITEM:	name = "ITEM";		break;
	    case FF_CHOP:	name = "CHOP";		break;
	    case FF_LINEGLOB:	name = "LINEGLOB";	break;
	    case FF_NEWLINE:	name = "NEWLINE";	break;
	    case FF_MORE:	name = "MORE";		break;
	    case FF_LINEMARK:	name = "LINEMARK";	break;
	    case FF_END:	name = "END";		break;
	    case FF_0DECIMAL:	name = "0DECIMAL";	break;
	    case FF_LINESNGL:	name = "LINESNGL";	break;
	    }
	    if (arg >= 0)
		PerlIO_printf(Perl_debug_log, "%-16s%ld\n", name, (long) arg);
	    else
		PerlIO_printf(Perl_debug_log, "%-16s\n", name);
	} );
	SensorCall(13171);switch (*fpc++) {
	case FF_LINEMARK:
	    SensorCall(13172);linemark = t - SvPVX(PL_formtarget);
	    lines++;
	    gotsome = FALSE;
	    SensorCall(13173);break;

	case FF_LITERAL:
	    SensorCall(13174);to_copy = *fpc++;
	    source = (U8 *)f;
	    f += to_copy;
	    trans = '~';
	    item_is_utf8 = targ_is_utf8 ? !!DO_UTF8(formsv) : !!SvUTF8(formsv);
	    SensorCall(13175);goto append;

	case FF_SKIP:
	    SensorCall(13176);f += *fpc++;
	    SensorCall(13177);break;

	case FF_FETCH:
	    SensorCall(13178);arg = *fpc++;
	    f += arg;
	    fieldsize = arg;

	    SensorCall(13180);if (MARK < SP)
		sv = *++MARK;
	    else {
		SensorCall(13179);sv = &PL_sv_no;
		Perl_ck_warner(aTHX_ packWARN(WARN_SYNTAX), "Not enough format arguments");
	    }
	    SensorCall(13181);if (SvTAINTED(sv))
		SvTAINTED_on(PL_formtarget);
	    SensorCall(13182);break;

	case FF_CHECKNL:
	    {
		SensorCall(13183);const char *send;
		const char *s = item = SvPV_const(sv, len);
		itemsize = len;
		SensorCall(13198);if (DO_UTF8(sv)) {
		    SensorCall(13184);itemsize = sv_len_utf8(sv);
		    SensorCall(13197);if (itemsize != (I32)len) {
			SensorCall(13185);I32 itembytes;
			SensorCall(13188);if (itemsize > fieldsize) {
			    SensorCall(13186);itemsize = fieldsize;
			    itembytes = itemsize;
			    sv_pos_u2b(sv, &itembytes, 0);
			}
			else
			    {/*169*/SensorCall(13187);itembytes = len;/*170*/}
			SensorCall(13189);send = chophere = s + itembytes;
			SensorCall(13194);while (s < send) {
			    SensorCall(13190);if (*s & ~31)
				gotsome = TRUE;
			    else {/*171*/SensorCall(13191);if (*s == '\n')
				{/*173*/SensorCall(13192);break;/*174*/}/*172*/}
			    SensorCall(13193);s++;
			}
			SensorCall(13195);item_is_utf8 = TRUE;
			itemsize = s - item;
			sv_pos_b2u(sv, &itemsize);
			SensorCall(13196);break;
		    }
		}
		SensorCall(13199);item_is_utf8 = FALSE;
		SensorCall(13201);if (itemsize > fieldsize)
		    {/*175*/SensorCall(13200);itemsize = fieldsize;/*176*/}
		SensorCall(13202);send = chophere = s + itemsize;
		SensorCall(13207);while (s < send) {
		    SensorCall(13203);if (*s & ~31)
			gotsome = TRUE;
		    else {/*177*/SensorCall(13204);if (*s == '\n')
			{/*179*/SensorCall(13205);break;/*180*/}/*178*/}
		    SensorCall(13206);s++;
		}
		SensorCall(13208);itemsize = s - item;
		SensorCall(13209);break;
	    }

	case FF_CHECKCHOP:
	    {
		SensorCall(13210);const char *s = item = SvPV_const(sv, len);
		itemsize = len;
		SensorCall(13235);if (DO_UTF8(sv)) {
		    SensorCall(13211);itemsize = sv_len_utf8(sv);
		    SensorCall(13234);if (itemsize != (I32)len) {
			SensorCall(13212);I32 itembytes;
			SensorCall(13231);if (itemsize <= fieldsize) {
			    SensorCall(13213);const char *send = chophere = s + itemsize;
			    SensorCall(13218);while (s < send) {
				SensorCall(13214);if (*s == '\r') {
				    SensorCall(13215);itemsize = s - item;
				    chophere = s;
				    SensorCall(13216);break;
				}
				SensorCall(13217);if (*s++ & ~31)
				    gotsome = TRUE;
			    }
			}
			else {
			    SensorCall(13219);const char *send;
			    itemsize = fieldsize;
			    itembytes = itemsize;
			    sv_pos_u2b(sv, &itembytes, 0);
			    send = chophere = s + itembytes;
			    SensorCall(13229);while (s < send || (s == send && isSPACE(*s))) {
				SensorCall(13220);if (isSPACE(*s)) {
				    SensorCall(13221);if (chopspace)
					{/*181*/SensorCall(13222);chophere = s;/*182*/}
				    SensorCall(13224);if (*s == '\r')
					{/*183*/SensorCall(13223);break;/*184*/}
				}
				else {
				    SensorCall(13225);if (*s & ~31)
					gotsome = TRUE;
				    SensorCall(13227);if (strchr(PL_chopset, *s))
					{/*185*/SensorCall(13226);chophere = s + 1;/*186*/}
				}
				SensorCall(13228);s++;
			    }
			    SensorCall(13230);itemsize = chophere - item;
			    sv_pos_b2u(sv, &itemsize);
			}
			SensorCall(13232);item_is_utf8 = TRUE;
			SensorCall(13233);break;
		    }
		}
		SensorCall(13236);item_is_utf8 = FALSE;
		SensorCall(13255);if (itemsize <= fieldsize) {
		    SensorCall(13237);const char *const send = chophere = s + itemsize;
		    SensorCall(13242);while (s < send) {
			SensorCall(13238);if (*s == '\r') {
			    SensorCall(13239);itemsize = s - item;
			    chophere = s;
			    SensorCall(13240);break;
			}
			SensorCall(13241);if (*s++ & ~31)
			    gotsome = TRUE;
		    }
		}
		else {
		    SensorCall(13243);const char *send;
		    itemsize = fieldsize;
		    send = chophere = s + itemsize;
		    SensorCall(13253);while (s < send || (s == send && isSPACE(*s))) {
			SensorCall(13244);if (isSPACE(*s)) {
			    SensorCall(13245);if (chopspace)
				{/*187*/SensorCall(13246);chophere = s;/*188*/}
			    SensorCall(13248);if (*s == '\r')
				{/*189*/SensorCall(13247);break;/*190*/}
			}
			else {
			    SensorCall(13249);if (*s & ~31)
				gotsome = TRUE;
			    SensorCall(13251);if (strchr(PL_chopset, *s))
				{/*191*/SensorCall(13250);chophere = s + 1;/*192*/}
			}
			SensorCall(13252);s++;
		    }
		    SensorCall(13254);itemsize = chophere - item;
		}
		SensorCall(13256);break;
	    }

	case FF_SPACE:
	    SensorCall(13257);arg = fieldsize - itemsize;
	    SensorCall(13261);if (arg) {
		SensorCall(13258);fieldsize -= arg;
		SensorCall(13260);while (arg-- > 0)
		    {/*193*/SensorCall(13259);*t++ = ' ';/*194*/}
	    }
	    SensorCall(13262);break;

	case FF_HALFSPACE:
	    SensorCall(13263);arg = fieldsize - itemsize;
	    SensorCall(13267);if (arg) {
		SensorCall(13264);arg /= 2;
		fieldsize -= arg;
		SensorCall(13266);while (arg-- > 0)
		    {/*195*/SensorCall(13265);*t++ = ' ';/*196*/}
	    }
	    SensorCall(13268);break;

	case FF_ITEM:
	    SensorCall(13269);to_copy = itemsize;
	    source = (U8 *)item;
	    trans = 1;
	    SensorCall(13273);if (item_is_utf8) {
		/* convert to_copy from chars to bytes */
		SensorCall(13270);U8 *s = source;
		SensorCall(13271);while (to_copy--)
		   s += UTF8SKIP(s);
		SensorCall(13272);to_copy = s - source;
	    }
	    SensorCall(13274);goto append;

	case FF_CHOP:
	    {
		SensorCall(13275);const char *s = chophere;
		SensorCall(13278);if (chopspace) {
		    SensorCall(13276);while (isSPACE(*s))
			{/*197*/SensorCall(13277);s++;/*198*/}
		}
		sv_chop(sv,s);
		SvSETMAGIC(sv);
		SensorCall(13279);break;
	    }

	case FF_LINESNGL:
	    SensorCall(13280);chopspace = 0;
	case FF_LINEGLOB:
	    {
		SensorCall(13281);const bool oneline = fpc[-1] == FF_LINESNGL;
		const char *s = item = SvPV_const(sv, len);
		const char *const send = s + len;

		item_is_utf8 = DO_UTF8(sv);
		SensorCall(13283);if (!len)
		    {/*199*/SensorCall(13282);break;/*200*/}
		SensorCall(13284);trans = 0;
		gotsome = TRUE;
		chophere = s + len;
		source = (U8 *) s;
		to_copy = len;
		SensorCall(13292);while (s < send) {
		    SensorCall(13285);if (*s++ == '\n') {
			SensorCall(13286);if (oneline) {
			    SensorCall(13287);to_copy = s - SvPVX_const(sv) - 1;
			    chophere = s;
			    SensorCall(13288);break;
			} else {
			    SensorCall(13289);if (s == send) {
				SensorCall(13290);to_copy--;
			    } else
				{/*201*/SensorCall(13291);lines++;/*202*/}
			}
		    }
		}
	    }

	append:
	    /* append to_copy bytes from source to PL_formstring.
	     * item_is_utf8 implies source is utf8.
	     * if trans, translate certain characters during the copy */
	    {
		SensorCall(13293);U8 *tmp = NULL;
		STRLEN grow = 0;

		SvCUR_set(PL_formtarget,
			  t - SvPVX_const(PL_formtarget));

		SensorCall(13299);if (targ_is_utf8 && !item_is_utf8) {
		    SensorCall(13294);source = tmp = bytes_to_utf8(source, &to_copy);
		} else {
		    SensorCall(13295);if (item_is_utf8 && !targ_is_utf8) {
			SensorCall(13296);U8 *s;
			/* Upgrade targ to UTF8, and then we reduce it to
			   a problem we have a simple solution for.
			   Don't need get magic.  */
			sv_utf8_upgrade_nomg(PL_formtarget);
			targ_is_utf8 = TRUE;
			/* re-calculate linemark */
			s = (U8*)SvPVX(PL_formtarget);
			/* the bytes we initially allocated to append the
			 * whole line may have been gobbled up during the
			 * upgrade, so allocate a whole new line's worth
			 * for safety */
			grow = linemax;
			SensorCall(13297);while (linemark--)
			    s += UTF8SKIP(s);
			SensorCall(13298);linemark = s - (U8*)SvPVX(PL_formtarget);
		    }
		    /* Easy. They agree.  */
		    assert (item_is_utf8 == targ_is_utf8);
		}
		SensorCall(13301);if (!trans)
		    /* @* and ^* are the only things that can exceed
		     * the linemax, so grow by the output size, plus
		     * a whole new form's worth in case of any further
		     * output */
		    {/*203*/SensorCall(13300);grow = linemax + to_copy;/*204*/}
		SensorCall(13302);if (grow)
		    SvGROW(PL_formtarget, SvCUR(PL_formtarget) + grow + 1);
		SensorCall(13303);t = SvPVX(PL_formtarget) + SvCUR(PL_formtarget);

		Copy(source, t, to_copy, char);
		SensorCall(13310);if (trans) {
		    /* blank out ~ or control chars, depending on trans.
		     * works on bytes not chars, so relies on not
		     * matching utf8 continuation bytes */
		    SensorCall(13304);U8 *s = (U8*)t;
		    U8 *send = s + to_copy;
		    SensorCall(13309);while (s < send) {
			SensorCall(13305);const int ch = *s;
			SensorCall(13307);if (trans == '~' ? (ch == '~') :
#ifdef EBCDIC
			       iscntrl(ch)
#else
			       (!(ch & ~31))
#endif
			)
			    {/*205*/SensorCall(13306);*s = ' ';/*206*/}
			SensorCall(13308);s++;
		    }
		}

		SensorCall(13311);t += to_copy;
		SvCUR_set(PL_formtarget, SvCUR(PL_formtarget) + to_copy);
		SensorCall(13312);if (tmp)
		    Safefree(tmp);
		SensorCall(13313);break;
	    }

	case FF_0DECIMAL:
	    SensorCall(13314);arg = *fpc++;
#if defined(USE_LONG_DOUBLE)
	    fmt = (const char *)
		((arg & FORM_NUM_POINT) ?
		 "%#0*.*" PERL_PRIfldbl : "%0*.*" PERL_PRIfldbl);
#else
	    fmt = (const char *)
		((arg & FORM_NUM_POINT) ?
		 "%#0*.*f"              : "%0*.*f");
#endif
	    SensorCall(13315);goto ff_dec;
	case FF_DECIMAL:
	    SensorCall(13316);arg = *fpc++;
#if defined(USE_LONG_DOUBLE)
 	    fmt = (const char *)
		((arg & FORM_NUM_POINT) ? "%#*.*" PERL_PRIfldbl : "%*.*" PERL_PRIfldbl);
#else
            fmt = (const char *)
		((arg & FORM_NUM_POINT) ? "%#*.*f"              : "%*.*f");
#endif
	ff_dec:
	    /* If the field is marked with ^ and the value is undefined,
	       blank it out. */
	    SensorCall(13321);if ((arg & FORM_NUM_BLANK) && !SvOK(sv)) {
		SensorCall(13317);arg = fieldsize;
		SensorCall(13319);while (arg--)
		    {/*207*/SensorCall(13318);*t++ = ' ';/*208*/}
		SensorCall(13320);break;
	    }
	    SensorCall(13322);gotsome = TRUE;
	    value = SvNV(sv);
	    /* overflow evidence */
	    SensorCall(13327);if (num_overflow(value, fieldsize, arg)) {
	        SensorCall(13323);arg = fieldsize;
		SensorCall(13325);while (arg--)
		    {/*209*/SensorCall(13324);*t++ = '#';/*210*/}
		SensorCall(13326);break;
	    }
	    /* Formats aren't yet marked for locales, so assume "yes". */
	    {
		STORE_NUMERIC_STANDARD_SET_LOCAL();
		SensorCall(13328);arg &= ~(FORM_NUM_POINT|FORM_NUM_BLANK);
		my_snprintf(t, SvLEN(PL_formtarget) - (t - SvPVX(PL_formtarget)), fmt, (int) fieldsize, (int) arg, value);
		RESTORE_NUMERIC_STANDARD();
	    }
	    t += fieldsize;
	    SensorCall(13329);break;

	case FF_NEWLINE:
	    SensorCall(13330);f++;
	    SensorCall(13331);while (t-- > (SvPVX(PL_formtarget) + linemark) && *t == ' ') ;
	    SensorCall(13332);t++;
	    *t++ = '\n';
	    SensorCall(13333);break;

	case FF_BLANK:
	    SensorCall(13334);arg = *fpc++;
	    SensorCall(13339);if (gotsome) {
		SensorCall(13335);if (arg) {		/* repeat until fields exhausted? */
		    SensorCall(13336);fpc--;
		    SensorCall(13337);goto end;
		}
	    }
	    else {
		SensorCall(13338);t = SvPVX(PL_formtarget) + linemark;
		lines--;
	    }
	    SensorCall(13340);break;

	case FF_MORE:
	    {
		SensorCall(13341);const char *s = chophere;
		const char *send = item + len;
		SensorCall(13344);if (chopspace) {
		    SensorCall(13342);while (isSPACE(*s) && (s < send))
			{/*213*/SensorCall(13343);s++;/*214*/}
		}
		SensorCall(13355);if (s < send) {
		    SensorCall(13345);char *s1;
		    arg = fieldsize - itemsize;
		    SensorCall(13349);if (arg) {
			SensorCall(13346);fieldsize -= arg;
			SensorCall(13348);while (arg-- > 0)
			    {/*215*/SensorCall(13347);*t++ = ' ';/*216*/}
		    }
		    SensorCall(13350);s1 = t - 3;
		    SensorCall(13353);if (strnEQ(s1,"   ",3)) {
			SensorCall(13351);while (s1 > SvPVX_const(PL_formtarget) && isSPACE(s1[-1]))
			    {/*217*/SensorCall(13352);s1--;/*218*/}
		    }
		    SensorCall(13354);*s1++ = '.';
		    *s1++ = '.';
		    *s1++ = '.';
		}
		SensorCall(13356);break;
	    }
	case FF_END:
	end:
	    assert(t < SvPVX_const(PL_formtarget) + SvLEN(PL_formtarget));
	    SensorCall(13357);*t = '\0';
	    SvCUR_set(PL_formtarget, t - SvPVX_const(PL_formtarget));
	    SensorCall(13358);if (targ_is_utf8)
		SvUTF8_on(PL_formtarget);
	    FmLINES(PL_formtarget) += lines;
	    SP = ORIGMARK;
	    SensorCall(13359);if (fpc[-1] == FF_BLANK)
		RETURNOP(cLISTOP->op_first);
	    else
		RETPUSHYES;
	}
    }
SensorCall(13361);}

PP(pp_grepstart)
{
SensorCall(13362);    dVAR; dSP;
    SV *src;

    SensorCall(13365);if (PL_stack_base + *PL_markstack_ptr == SP) {
	SensorCall(13363);(void)POPMARK;
	SensorCall(13364);if (GIMME_V == G_SCALAR)
	    mXPUSHi(0);
	RETURNOP(PL_op->op_next->op_next);
    }
    PL_stack_sp = PL_stack_base + *PL_markstack_ptr + 1;
    SensorCall(13366);Perl_pp_pushmark(aTHX);				/* push dst */
    Perl_pp_pushmark(aTHX);				/* push src */
    ENTER_with_name("grep");					/* enter outer scope */

    SAVETMPS;
    SensorCall(13367);if (PL_op->op_private & OPpGREP_LEX)
	SAVESPTR(PAD_SVl(PL_op->op_targ));
    else
	SAVE_DEFSV;
    ENTER_with_name("grep_item");					/* enter inner scope */
    SAVEVPTR(PL_curpm);

    SensorCall(13368);src = PL_stack_base[*PL_markstack_ptr];
    SvTEMP_off(src);
    SensorCall(13369);if (PL_op->op_private & OPpGREP_LEX)
	PAD_SVl(PL_op->op_targ) = src;
    else
	DEFSV_set(src);

    PUTBACK;
    SensorCall(13371);if (PL_op->op_type == OP_MAPSTART)
	{/*237*/SensorCall(13370);Perl_pp_pushmark(aTHX);/*238*/}			/* push top */
    {OP * ReplaceReturn1094 = ((LOGOP*)PL_op->op_next)->op_other; SensorCall(13372); return ReplaceReturn1094;}
}

PP(pp_mapwhile)
{
SensorCall(13373);    dVAR; dSP;
    const I32 gimme = GIMME_V;
    I32 items = (SP - PL_stack_base) - *PL_markstack_ptr; /* how many new items */
    I32 count;
    I32 shift;
    SV** src;
    SV** dst;

    /* first, move source pointer to the next item in the source list */
    ++PL_markstack_ptr[-1];

    /* if there are new items, push them into the destination list */
    SensorCall(13392);if (items && gimme != G_VOID) {
	/* might need to make room back there first */
	SensorCall(13374);if (items > PL_markstack_ptr[-1] - PL_markstack_ptr[-2]) {
	    /* XXX this implementation is very pessimal because the stack
	     * is repeatedly extended for every set of items.  Is possible
	     * to do this without any stack extension or copying at all
	     * by maintaining a separate list over which the map iterates
	     * (like foreach does). --gsar */

	    /* everything in the stack after the destination list moves
	     * towards the end the stack by the amount of room needed */
	    SensorCall(13375);shift = items - (PL_markstack_ptr[-1] - PL_markstack_ptr[-2]);

	    /* items to shift up (accounting for the moved source pointer) */
	    count = (SP - PL_stack_base) - (PL_markstack_ptr[-1] - 1);

	    /* This optimization is by Ben Tilly and it does
	     * things differently from what Sarathy (gsar)
	     * is describing.  The downside of this optimization is
	     * that leaves "holes" (uninitialized and hopefully unused areas)
	     * to the Perl stack, but on the other hand this
	     * shouldn't be a problem.  If Sarathy's idea gets
	     * implemented, this optimization should become
	     * irrelevant.  --jhi */
            SensorCall(13377);if (shift < count)
                {/*241*/SensorCall(13376);shift = count;/*242*/} /* Avoid shifting too often --Ben Tilly */

	    EXTEND(SP,shift);
	    SensorCall(13378);src = SP;
	    dst = (SP += shift);
	    PL_markstack_ptr[-1] += shift;
	    *PL_markstack_ptr += shift;
	    SensorCall(13380);while (count--)
		{/*243*/SensorCall(13379);*dst-- = *src--;/*244*/}
	}
	/* copy the new items down to the destination list */
	SensorCall(13381);dst = PL_stack_base + (PL_markstack_ptr[-2] += items) - 1;
	SensorCall(13391);if (gimme == G_ARRAY) {
	    /* add returned items to the collection (making mortal copies
	     * if necessary), then clear the current temps stack frame
	     * *except* for those items. We do this splicing the items
	     * into the start of the tmps frame (so some items may be on
	     * the tmps stack twice), then moving PL_tmps_floor above
	     * them, then freeing the frame. That way, the only tmps that
	     * accumulate over iterations are the return values for map.
	     * We have to do to this way so that everything gets correctly
	     * freed if we die during the map.
	     */
	    SensorCall(13382);I32 tmpsbase;
	    I32 i = items;
	    /* make space for the slice */
	    EXTEND_MORTAL(items);
	    tmpsbase = PL_tmps_floor + 1;
	    Move(PL_tmps_stack + tmpsbase,
		 PL_tmps_stack + tmpsbase + items,
		 PL_tmps_ix - PL_tmps_floor,
		 SV*);
	    PL_tmps_ix += items;

	    SensorCall(13386);while (i-- > 0) {
		SensorCall(13383);SV *sv = POPs;
		SensorCall(13384);if (!SvTEMP(sv))
		    sv = sv_mortalcopy(sv);
		SensorCall(13385);*dst-- = sv;
		PL_tmps_stack[tmpsbase++] = SvREFCNT_inc_simple(sv);
	    }
	    /* clear the stack frame except for the items */
	    PL_tmps_floor += items;
	    FREETMPS;
	    /* FREETMPS may have cleared the TEMP flag on some of the items */
	    SensorCall(13387);i = items;
	    SensorCall(13388);while (i-- > 0)
		SvTEMP_on(PL_tmps_stack[--tmpsbase]);
	}
	else {
	    /* scalar context: we don't care about which values map returns
	     * (we use undef here). And so we certainly don't want to do mortal
	     * copies of meaningless values. */
	    SensorCall(13389);while (items-- > 0) {
		SensorCall(13390);(void)POPs;
		*dst-- = &PL_sv_undef;
	    }
	    FREETMPS;
	}
    }
    else {
	FREETMPS;
    }
    LEAVE_with_name("grep_item");					/* exit inner scope */

    /* All done yet? */
    SensorCall(13400);if (PL_markstack_ptr[-1] > *PL_markstack_ptr) {

	SensorCall(13393);(void)POPMARK;				/* pop top */
	LEAVE_with_name("grep");					/* exit outer scope */
	(void)POPMARK;				/* pop src */
	items = --*PL_markstack_ptr - PL_markstack_ptr[-1];
	(void)POPMARK;				/* pop dst */
	SP = PL_stack_base + POPMARK;		/* pop original mark */
	SensorCall(13397);if (gimme == G_SCALAR) {
	    SensorCall(13394);if (PL_op->op_private & OPpGREP_LEX) {
		SensorCall(13395);SV* sv = sv_newmortal();
		sv_setiv(sv, items);
		PUSHs(sv);
	    }
	    else {
		dTARGET;
		XPUSHi(items);
	    }
	}
	else {/*245*/SensorCall(13396);if (gimme == G_ARRAY)
	    SP += items;/*246*/}
	RETURN;
    }
    else {
	SensorCall(13398);SV *src;

	ENTER_with_name("grep_item");					/* enter inner scope */
	SAVEVPTR(PL_curpm);

	/* set $_ to the new source item */
	src = PL_stack_base[PL_markstack_ptr[-1]];
	SvTEMP_off(src);
	SensorCall(13399);if (PL_op->op_private & OPpGREP_LEX)
	    PAD_SVl(PL_op->op_targ) = src;
	else
	    DEFSV_set(src);

	RETURNOP(cLOGOP->op_other);
    }
SensorCall(13401);}

/* Range stuff. */

PP(pp_range)
{
SensorCall(13402);    dVAR;
    SensorCall(13403);if (GIMME == G_ARRAY)
	return NORMAL;
    SensorCall(13405);if (SvTRUEx(PAD_SV(PL_op->op_targ)))
	{/*247*/{OP * ReplaceReturn1093 = cLOGOP->op_other; SensorCall(13404); return ReplaceReturn1093;}/*248*/}
    else
	return NORMAL;
SensorCall(13406);}

PP(pp_flip)
{
SensorCall(13407);    dVAR;
    dSP;

    SensorCall(13417);if (GIMME == G_ARRAY) {
	RETURNOP(((LOGOP*)cUNOP->op_first)->op_other);
    }
    else {
	dTOPss;
	SensorCall(13408);SV * const targ = PAD_SV(PL_op->op_targ);
	int flip = 0;

	SensorCall(13414);if (PL_op->op_private & OPpFLIP_LINENUM) {
	    SensorCall(13409);if (GvIO(PL_last_in_gv)) {
		SensorCall(13410);flip = SvIV(sv) == (IV)IoLINES(GvIOp(PL_last_in_gv));
	    }
	    else {
		SensorCall(13411);GV * const gv = gv_fetchpvs(".", GV_ADD|GV_NOTQUAL, SVt_PV);
		SensorCall(13412);if (gv && GvSV(gv))
		    flip = SvIV(sv) == SvIV(GvSV(gv));
	    }
	} else {
	    SensorCall(13413);flip = SvTRUE(sv);
	}
	SensorCall(13416);if (flip) {
	    sv_setiv(PAD_SV(cUNOP->op_first->op_targ), 1);
	    SensorCall(13415);if (PL_op->op_flags & OPf_SPECIAL) {
		sv_setiv(targ, 1);
		SETs(targ);
		RETURN;
	    }
	    else {
		sv_setiv(targ, 0);
		SP--;
		RETURNOP(((LOGOP*)cUNOP->op_first)->op_other);
	    }
	}
	sv_setpvs(TARG, "");
	SETs(targ);
	RETURN;
    }
SensorCall(13418);}

/* This code tries to decide if "$left .. $right" should use the
   magical string increment, or if the range is numeric (we make
   an exception for .."0" [#18165]). AMS 20021031. */

#define RANGE_IS_NUMERIC(left,right) ( \
	SvNIOKp(left)  || (SvOK(left)  && !SvPOKp(left))  || \
	SvNIOKp(right) || (SvOK(right) && !SvPOKp(right)) || \
	(((!SvOK(left) && SvOK(right)) || ((!SvOK(left) || \
          looks_like_number(left)) && SvPOKp(left) && *SvPVX_const(left) != '0')) \
         && (!SvOK(right) || looks_like_number(right))))

PP(pp_flop)
{
SensorCall(13419);    dVAR; dSP;

    SensorCall(13442);if (GIMME == G_ARRAY) {
	dPOPPOPssrl;

	SvGETMAGIC(left);
	SvGETMAGIC(right);

	SensorCall(13433);if (RANGE_IS_NUMERIC(left,right)) {
	    SensorCall(13420);register IV i, j;
	    IV max;
	    SensorCall(13421);if ((SvOK(left) && SvNV_nomg(left) < IV_MIN) ||
		(SvOK(right) && SvNV_nomg(right) > IV_MAX))
		DIE(aTHX_ "Range iterator outside integer range");
	    SensorCall(13422);i = SvIV_nomg(left);
	    max = SvIV_nomg(right);
	    SensorCall(13425);if (max >= i) {
		SensorCall(13423);j = max - i + 1;
		EXTEND_MORTAL(j);
		EXTEND(SP, j);
	    }
	    else
		{/*165*/SensorCall(13424);j = 0;/*166*/}
	    SensorCall(13427);while (j--) {
		SensorCall(13426);SV * const sv = sv_2mortal(newSViv(i++));
		PUSHs(sv);
	    }
	}
	else {
	    SensorCall(13428);STRLEN len, llen;
	    const char * const lpv = SvPV_nomg_const(left, llen);
	    const char * const tmps = SvPV_nomg_const(right, len);

	    SV *sv = newSVpvn_flags(lpv, llen, SvUTF8(left)|SVs_TEMP);
	    SensorCall(13432);while (!SvNIOKp(sv) && SvCUR(sv) <= len) {
		XPUSHs(sv);
	        SensorCall(13430);if (strEQ(SvPVX_const(sv),tmps))
	            {/*167*/SensorCall(13429);break;/*168*/}
		SensorCall(13431);sv = sv_2mortal(newSVsv(sv));
		sv_inc(sv);
	    }
	}
    }
    else {
	dTOPss;
	SensorCall(13434);SV * const targ = PAD_SV(cUNOP->op_first->op_targ);
	int flop = 0;
	sv_inc(targ);

	SensorCall(13440);if (PL_op->op_private & OPpFLIP_LINENUM) {
	    SensorCall(13435);if (GvIO(PL_last_in_gv)) {
		SensorCall(13436);flop = SvIV(sv) == (IV)IoLINES(GvIOp(PL_last_in_gv));
	    }
	    else {
		SensorCall(13437);GV * const gv = gv_fetchpvs(".", GV_ADD|GV_NOTQUAL, SVt_PV);
		SensorCall(13438);if (gv && GvSV(gv)) flop = SvIV(sv) == SvIV(GvSV(gv));
	    }
	}
	else {
	    SensorCall(13439);flop = SvTRUE(sv);
	}

	SensorCall(13441);if (flop) {
	    sv_setiv(PAD_SV(((UNOP*)cUNOP->op_first)->op_first->op_targ), 0);
	    sv_catpvs(targ, "E0");
	}
	SETs(targ);
    }

    RETURN;
}

/* Control. */

static const char * const context_name[] = {
    "pseudo-block",
    NULL, /* CXt_WHEN never actually needs "block" */
    NULL, /* CXt_BLOCK never actually needs "block" */
    NULL, /* CXt_GIVEN never actually needs "block" */
    NULL, /* CXt_LOOP_FOR never actually needs "loop" */
    NULL, /* CXt_LOOP_PLAIN never actually needs "loop" */
    NULL, /* CXt_LOOP_LAZYSV never actually needs "loop" */
    NULL, /* CXt_LOOP_LAZYIV never actually needs "loop" */
    "subroutine",
    "format",
    "eval",
    "substitution",
};

STATIC I32
S_dopoptolabel(pTHX_ const char *label, STRLEN len, U32 flags)
{
SensorCall(13443);    dVAR;
    register I32 i;

    PERL_ARGS_ASSERT_DOPOPTOLABEL;

    SensorCall(13454);for (i = cxstack_ix; i >= 0; i--) {
	SensorCall(13444);register const PERL_CONTEXT * const cx = &cxstack[i];
	SensorCall(13453);switch (CxTYPE(cx)) {
	case CXt_SUBST:
	case CXt_SUB:
	case CXt_FORMAT:
	case CXt_EVAL:
	case CXt_NULL:
	    /* diag_listed_as: Exiting subroutine via %s */
	    SensorCall(13445);Perl_ck_warner(aTHX_ packWARN(WARN_EXITING), "Exiting %s via %s",
			   context_name[CxTYPE(cx)], OP_NAME(PL_op));
	    SensorCall(13447);if (CxTYPE(cx) == CXt_NULL)
		{/*135*/{I32  ReplaceReturn1092 = -1; SensorCall(13446); return ReplaceReturn1092;}/*136*/}
	    SensorCall(13448);break;
	case CXt_LOOP_LAZYIV:
	case CXt_LOOP_LAZYSV:
	case CXt_LOOP_FOR:
	case CXt_LOOP_PLAIN:
	  {
            SensorCall(13449);STRLEN cx_label_len = 0;
            U32 cx_label_flags = 0;
	    const char *cx_label = CxLABEL_len_flags(cx, &cx_label_len, &cx_label_flags);
	    SensorCall(13451);if (!cx_label || !(
                    ( (cx_label_flags & SVf_UTF8) != (flags & SVf_UTF8) ) ?
                        (flags & SVf_UTF8)
                            ? (bytes_cmp_utf8(
                                        (const U8*)cx_label, cx_label_len,
                                        (const U8*)label, len) == 0)
                            : (bytes_cmp_utf8(
                                        (const U8*)label, len,
                                        (const U8*)cx_label, cx_label_len) == 0)
                    : (len == cx_label_len && ((cx_label == label)
                                    || memEQ(cx_label, label, len))) )) {
		DEBUG_l(Perl_deb(aTHX_ "(poptolabel(): skipping label at cx=%ld %s)\n",
			(long)i, cx_label));
		SensorCall(13450);continue;
	    }
	    DEBUG_l( Perl_deb(aTHX_ "(poptolabel(): found label at cx=%ld %s)\n", (long)i, label));
	    {I32  ReplaceReturn1091 = i; SensorCall(13452); return ReplaceReturn1091;}
	  }
	}
    }
    {I32  ReplaceReturn1090 = i; SensorCall(13455); return ReplaceReturn1090;}
}



I32
Perl_dowantarray(pTHX)
{
SensorCall(13456);    dVAR;
    const I32 gimme = block_gimme();
    {I32  ReplaceReturn1089 = (gimme == G_VOID) ? G_SCALAR : gimme; SensorCall(13457); return ReplaceReturn1089;}
}

I32
Perl_block_gimme(pTHX)
{
SensorCall(13458);    dVAR;
    const I32 cxix = dopoptosub(cxstack_ix);
    SensorCall(13459);if (cxix < 0)
	return G_VOID;

    SensorCall(13465);switch (cxstack[cxix].blk_gimme) {
    case G_VOID:
	{I32  ReplaceReturn1088 = G_VOID; SensorCall(13460); return ReplaceReturn1088;}
    case G_SCALAR:
	{I32  ReplaceReturn1087 = G_SCALAR; SensorCall(13461); return ReplaceReturn1087;}
    case G_ARRAY:
	{I32  ReplaceReturn1086 = G_ARRAY; SensorCall(13462); return ReplaceReturn1086;}
    default:
	SensorCall(13463);Perl_croak(aTHX_ "panic: bad gimme: %d\n", cxstack[cxix].blk_gimme);
	/* NOTREACHED */
	{I32  ReplaceReturn1085 = 0; SensorCall(13464); return ReplaceReturn1085;}
    }
SensorCall(13466);}

I32
Perl_is_lvalue_sub(pTHX)
{
SensorCall(13467);    dVAR;
    const I32 cxix = dopoptosub(cxstack_ix);
    assert(cxix >= 0);  /* We should only be called from inside subs */

    SensorCall(13469);if (CxLVAL(cxstack + cxix) && CvLVALUE(cxstack[cxix].blk_sub.cv))
	return CxLVAL(cxstack + cxix);
    else
	{/*17*/{I32  ReplaceReturn1084 = 0; SensorCall(13468); return ReplaceReturn1084;}/*18*/}
SensorCall(13470);}

/* only used by PUSHSUB */
I32
Perl_was_lvalue_sub(pTHX)
{
SensorCall(13471);    dVAR;
    const I32 cxix = dopoptosub(cxstack_ix-1);
    assert(cxix >= 0);  /* We should only be called from inside subs */

    SensorCall(13473);if (CxLVAL(cxstack + cxix) && CvLVALUE(cxstack[cxix].blk_sub.cv))
	return CxLVAL(cxstack + cxix);
    else
	{/*25*/{I32  ReplaceReturn1083 = 0; SensorCall(13472); return ReplaceReturn1083;}/*26*/}
SensorCall(13474);}

STATIC I32
S_dopoptosub_at(pTHX_ const PERL_CONTEXT *cxstk, I32 startingblock)
{
SensorCall(13475);    dVAR;
    I32 i;

    PERL_ARGS_ASSERT_DOPOPTOSUB_AT;

    SensorCall(13480);for (i = startingblock; i >= 0; i--) {
	SensorCall(13476);register const PERL_CONTEXT * const cx = &cxstk[i];
	SensorCall(13479);switch (CxTYPE(cx)) {
	default:
	    SensorCall(13477);continue;
	case CXt_EVAL:
	case CXt_SUB:
	case CXt_FORMAT:
	    DEBUG_l( Perl_deb(aTHX_ "(dopoptosub_at(): found sub at cx=%ld)\n", (long)i));
	    {I32  ReplaceReturn1082 = i; SensorCall(13478); return ReplaceReturn1082;}
	}
    }
    {I32  ReplaceReturn1081 = i; SensorCall(13481); return ReplaceReturn1081;}
}

STATIC I32
S_dopoptoeval(pTHX_ I32 startingblock)
{
SensorCall(13482);    dVAR;
    I32 i;
    SensorCall(13487);for (i = startingblock; i >= 0; i--) {
	SensorCall(13483);register const PERL_CONTEXT *cx = &cxstack[i];
	SensorCall(13486);switch (CxTYPE(cx)) {
	default:
	    SensorCall(13484);continue;
	case CXt_EVAL:
	    DEBUG_l( Perl_deb(aTHX_ "(dopoptoeval(): found eval at cx=%ld)\n", (long)i));
	    {I32  ReplaceReturn1080 = i; SensorCall(13485); return ReplaceReturn1080;}
	}
    }
    {I32  ReplaceReturn1079 = i; SensorCall(13488); return ReplaceReturn1079;}
}

STATIC I32
S_dopoptoloop(pTHX_ I32 startingblock)
{
SensorCall(13489);    dVAR;
    I32 i;
    SensorCall(13497);for (i = startingblock; i >= 0; i--) {
	SensorCall(13490);register const PERL_CONTEXT * const cx = &cxstack[i];
	SensorCall(13496);switch (CxTYPE(cx)) {
	case CXt_SUBST:
	case CXt_SUB:
	case CXt_FORMAT:
	case CXt_EVAL:
	case CXt_NULL:
	    /* diag_listed_as: Exiting subroutine via %s */
	    SensorCall(13491);Perl_ck_warner(aTHX_ packWARN(WARN_EXITING), "Exiting %s via %s",
			   context_name[CxTYPE(cx)], OP_NAME(PL_op));
	    SensorCall(13493);if ((CxTYPE(cx)) == CXt_NULL)
		{/*137*/{I32  ReplaceReturn1078 = -1; SensorCall(13492); return ReplaceReturn1078;}/*138*/}
	    SensorCall(13494);break;
	case CXt_LOOP_LAZYIV:
	case CXt_LOOP_LAZYSV:
	case CXt_LOOP_FOR:
	case CXt_LOOP_PLAIN:
	    DEBUG_l( Perl_deb(aTHX_ "(dopoptoloop(): found loop at cx=%ld)\n", (long)i));
	    {I32  ReplaceReturn1077 = i; SensorCall(13495); return ReplaceReturn1077;}
	}
    }
    {I32  ReplaceReturn1076 = i; SensorCall(13498); return ReplaceReturn1076;}
}

STATIC I32
S_dopoptogiven(pTHX_ I32 startingblock)
{
SensorCall(13499);    dVAR;
    I32 i;
    SensorCall(13507);for (i = startingblock; i >= 0; i--) {
	SensorCall(13500);register const PERL_CONTEXT *cx = &cxstack[i];
	SensorCall(13506);switch (CxTYPE(cx)) {
	default:
	    SensorCall(13501);continue;
	case CXt_GIVEN:
	    DEBUG_l( Perl_deb(aTHX_ "(dopoptogiven(): found given at cx=%ld)\n", (long)i));
	    {I32  ReplaceReturn1075 = i; SensorCall(13502); return ReplaceReturn1075;}
	case CXt_LOOP_PLAIN:
	    assert(!CxFOREACHDEF(cx));
	    SensorCall(13503);break;
	case CXt_LOOP_LAZYIV:
	case CXt_LOOP_LAZYSV:
	case CXt_LOOP_FOR:
	    SensorCall(13504);if (CxFOREACHDEF(cx)) {
		DEBUG_l( Perl_deb(aTHX_ "(dopoptogiven(): found foreach at cx=%ld)\n", (long)i));
		{I32  ReplaceReturn1074 = i; SensorCall(13505); return ReplaceReturn1074;}
	    }
	}
    }
    {I32  ReplaceReturn1073 = i; SensorCall(13508); return ReplaceReturn1073;}
}

STATIC I32
S_dopoptowhen(pTHX_ I32 startingblock)
{
SensorCall(13509);    dVAR;
    I32 i;
    SensorCall(13514);for (i = startingblock; i >= 0; i--) {
	SensorCall(13510);register const PERL_CONTEXT *cx = &cxstack[i];
	SensorCall(13513);switch (CxTYPE(cx)) {
	default:
	    SensorCall(13511);continue;
	case CXt_WHEN:
	    DEBUG_l( Perl_deb(aTHX_ "(dopoptowhen(): found when at cx=%ld)\n", (long)i));
	    {I32  ReplaceReturn1072 = i; SensorCall(13512); return ReplaceReturn1072;}
	}
    }
    {I32  ReplaceReturn1071 = i; SensorCall(13515); return ReplaceReturn1071;}
}

void
Perl_dounwind(pTHX_ I32 cxix)
{
SensorCall(13516);    dVAR;
    I32 optype;

    SensorCall(13518);if (!PL_curstackinfo) /* can happen if die during thread cloning */
	{/*9*/SensorCall(13517);return;/*10*/}

    SensorCall(13528);while (cxstack_ix > cxix) {
	SensorCall(13519);SV *sv;
        register PERL_CONTEXT *cx = &cxstack[cxstack_ix];
	DEBUG_CX("UNWIND");						\
	/* Note: we don't need to restore the base context info till the end. */
	SensorCall(13527);switch (CxTYPE(cx)) {
	case CXt_SUBST:
SensorCall(13520);	    POPSUBST(cx);
	    SensorCall(13521);continue;  /* not break */
	case CXt_SUB:
	    POPSUB(cx,sv);
	    LEAVESUB(sv);
	    SensorCall(13522);break;
	case CXt_EVAL:
	    POPEVAL(cx);
	    SensorCall(13523);break;
	case CXt_LOOP_LAZYIV:
	case CXt_LOOP_LAZYSV:
	case CXt_LOOP_FOR:
	case CXt_LOOP_PLAIN:
	    POPLOOP(cx);
	    SensorCall(13524);break;
	case CXt_NULL:
	    SensorCall(13525);break;
	case CXt_FORMAT:
	    POPFORMAT(cx);
	    SensorCall(13526);break;
	}
	cxstack_ix--;
    }
    PERL_UNUSED_VAR(optype);
}

void
Perl_qerror(pTHX_ SV *err)
{
SensorCall(13529);    dVAR;

    PERL_ARGS_ASSERT_QERROR;

    SensorCall(13534);if (PL_in_eval) {
	SensorCall(13530);if (PL_in_eval & EVAL_KEEPERR) {
		SensorCall(13531);Perl_ck_warner(aTHX_ packWARN(WARN_MISC), "\t(in cleanup) %"SVf,
                                                    SVfARG(err));
	}
	else
	    sv_catsv(ERRSV, err);
    }
    else {/*19*/SensorCall(13532);if (PL_errors)
	sv_catsv(PL_errors, err);
    else
	{/*21*/SensorCall(13533);Perl_warn(aTHX_ "%"SVf, SVfARG(err));/*22*/}/*20*/}
    SensorCall(13536);if (PL_parser)
	{/*23*/SensorCall(13535);++PL_parser->error_count;/*24*/}
SensorCall(13537);}

void
Perl_die_unwind(pTHX_ SV *msv)
{
SensorCall(13538);    dVAR;
    SV *exceptsv = sv_mortalcopy(msv);
    U8 in_eval = PL_in_eval;
    PERL_ARGS_ASSERT_DIE_UNWIND;

    SensorCall(13554);if (in_eval) {
	SensorCall(13539);I32 cxix;
	I32 gimme;

	/*
	 * Historically, perl used to set ERRSV ($@) early in the die
	 * process and rely on it not getting clobbered during unwinding.
	 * That sucked, because it was liable to get clobbered, so the
	 * setting of ERRSV used to emit the exception from eval{} has
	 * been moved to much later, after unwinding (see just before
	 * JMPENV_JUMP below).	However, some modules were relying on the
	 * early setting, by examining $@ during unwinding to use it as
	 * a flag indicating whether the current unwinding was caused by
	 * an exception.  It was never a reliable flag for that purpose,
	 * being totally open to false positives even without actual
	 * clobberage, but was useful enough for production code to
	 * semantically rely on it.
	 *
	 * We'd like to have a proper introspective interface that
	 * explicitly describes the reason for whatever unwinding
	 * operations are currently in progress, so that those modules
	 * work reliably and $@ isn't further overloaded.  But we don't
	 * have one yet.  In its absence, as a stopgap measure, ERRSV is
	 * now *additionally* set here, before unwinding, to serve as the
	 * (unreliable) flag that it used to.
	 *
	 * This behaviour is temporary, and should be removed when a
	 * proper way to detect exceptional unwinding has been developed.
	 * As of 2010-12, the authors of modules relying on the hack
	 * are aware of the issue, because the modules failed on
	 * perls 5.13.{1..7} which had late setting of $@ without this
	 * early-setting hack.
	 */
	SensorCall(13540);if (!(in_eval & EVAL_KEEPERR)) {
	    SvTEMP_off(exceptsv);
	    sv_setsv(ERRSV, exceptsv);
	}

	SensorCall(13541);while ((cxix = dopoptoeval(cxstack_ix)) < 0
	       && PL_curstackinfo->si_prev)
	{
	    dounwind(-1);
	    POPSTACK;
	}

	SensorCall(13553);if (cxix >= 0) {
	    SensorCall(13542);I32 optype;
	    SV *namesv;
	    register PERL_CONTEXT *cx;
	    SV **newsp;
	    COP *oldcop;
	    JMPENV *restartjmpenv;
	    OP *restartop;

	    SensorCall(13543);if (cxix < cxstack_ix)
		dounwind(cxix);

SensorCall(13544);	    POPBLOCK(cx,PL_curpm);
	    SensorCall(13546);if (CxTYPE(cx) != CXt_EVAL) {
		SensorCall(13545);STRLEN msglen;
		const char* message = SvPVx_const(exceptsv, msglen);
		PerlIO_write(Perl_error_log, (const char *)"panic: die ", 11);
		PerlIO_write(Perl_error_log, message, msglen);
		my_exit(1);
	    }
	    POPEVAL(cx);
	    SensorCall(13547);namesv = cx->blk_eval.old_namesv;
	    oldcop = cx->blk_oldcop;
	    restartjmpenv = cx->blk_eval.cur_top_env;
	    restartop = cx->blk_eval.retop;

	    SensorCall(13548);if (gimme == G_SCALAR)
		*++newsp = &PL_sv_undef;
	    PL_stack_sp = newsp;

	    LEAVE;

	    /* LEAVE could clobber PL_curcop (see save_re_context())
	     * XXX it might be better to find a way to avoid messing with
	     * PL_curcop in save_re_context() instead, but this is a more
	     * minimal fix --GSAR */
	    PL_curcop = oldcop;

	    SensorCall(13550);if (optype == OP_REQUIRE) {
                SensorCall(13549);(void)hv_store(GvHVn(PL_incgv),
                               SvPVX_const(namesv),
                               SvUTF8(namesv) ? -(I32)SvCUR(namesv) : (I32)SvCUR(namesv),
                               &PL_sv_undef, 0);
		/* note that unlike pp_entereval, pp_require isn't
		 * supposed to trap errors. So now that we've popped the
		 * EVAL that pp_require pushed, and processed the error
		 * message, rethrow the error */
		Perl_croak(aTHX_ "%"SVf"Compilation failed in require",
			   SVfARG(exceptsv ? exceptsv : newSVpvs_flags("Unknown error\n",
                                                                    SVs_TEMP)));
	    }
	    SensorCall(13552);if (in_eval & EVAL_KEEPERR) {
		SensorCall(13551);Perl_ck_warner(aTHX_ packWARN(WARN_MISC), "\t(in cleanup) %"SVf,
			       SVfARG(exceptsv));
	    }
	    else {
		sv_setsv(ERRSV, exceptsv);
	    }
	    PL_restartjmpenv = restartjmpenv;
	    PL_restartop = restartop;
	    JMPENV_JUMP(3);
	    /* NOTREACHED */
	}
    }

    write_to_stderr(exceptsv);
    my_failure_exit();
    /* NOTREACHED */
}

PP(pp_xor)
{
SensorCall(13555);    dVAR; dSP; dPOPTOPssrl;
    SensorCall(13556);if (SvTRUE(left) != SvTRUE(right))
	RETSETYES;
    else
	RETSETNO;
SensorCall(13557);}

/*
=for apidoc caller_cx

The XSUB-writer's equivalent of L<caller()|perlfunc/caller>. The
returned C<PERL_CONTEXT> structure can be interrogated to find all the
information returned to Perl by C<caller>. Note that XSUBs don't get a
stack frame, so C<caller_cx(0, NULL)> will return information for the
immediately-surrounding Perl code.

This function skips over the automatic calls to C<&DB::sub> made on the
behalf of the debugger. If the stack frame requested was a sub called by
C<DB::sub>, the return value will be the frame for the call to
C<DB::sub>, since that has the correct line number/etc. for the call
site. If I<dbcxp> is non-C<NULL>, it will be set to a pointer to the
frame for the sub call itself.

=cut
*/

const PERL_CONTEXT *
Perl_caller_cx(pTHX_ I32 count, const PERL_CONTEXT **dbcxp)
{
    SensorCall(13558);register I32 cxix = dopoptosub(cxstack_ix);
    register const PERL_CONTEXT *cx;
    register const PERL_CONTEXT *ccstack = cxstack;
    const PERL_SI *top_si = PL_curstackinfo;

    SensorCall(13567);for (;;) {
	/* we may be in a higher stacklevel, so dig down deeper */
	SensorCall(13559);while (cxix < 0 && top_si->si_type != PERLSI_MAIN) {
	    SensorCall(13560);top_si = top_si->si_prev;
	    ccstack = top_si->si_cxstack;
	    cxix = dopoptosub_at(ccstack, top_si->si_cxix);
	}
	SensorCall(13561);if (cxix < 0)
	    return NULL;
	/* caller() should not report the automatic calls to &DB::sub */
	SensorCall(13563);if (PL_DBsub && GvCV(PL_DBsub) && cxix >= 0 &&
		ccstack[cxix].blk_sub.cv == GvCV(PL_DBsub))
	    {/*1*/SensorCall(13562);count++;/*2*/}
	SensorCall(13565);if (!count--)
	    {/*3*/SensorCall(13564);break;/*4*/}
	SensorCall(13566);cxix = dopoptosub_at(ccstack, cxix - 1);
    }

    SensorCall(13568);cx = &ccstack[cxix];
    SensorCall(13570);if (dbcxp) {/*5*/SensorCall(13569);*dbcxp = cx;/*6*/}

    SensorCall(13574);if (CxTYPE(cx) == CXt_SUB || CxTYPE(cx) == CXt_FORMAT) {
        SensorCall(13571);const I32 dbcxix = dopoptosub_at(ccstack, cxix - 1);
	/* We expect that ccstack[dbcxix] is CXt_SUB, anyway, the
	   field below is defined for any cx. */
	/* caller() should not report the automatic calls to &DB::sub */
	SensorCall(13573);if (PL_DBsub && GvCV(PL_DBsub) && dbcxix >= 0 && ccstack[dbcxix].blk_sub.cv == GvCV(PL_DBsub))
	    {/*7*/SensorCall(13572);cx = &ccstack[dbcxix];/*8*/}
    }

    {const PERL_CONTEXT * ReplaceReturn1070 = cx; SensorCall(13575); return ReplaceReturn1070;}
}

PP(pp_caller)
{
SensorCall(13576);    dVAR;
    dSP;
    register const PERL_CONTEXT *cx;
    const PERL_CONTEXT *dbcx;
    I32 gimme;
    const HEK *stash_hek;
    I32 count = 0;
    bool has_arg = MAXARG && TOPs;

    SensorCall(13578);if (MAXARG) {
      SensorCall(13577);if (has_arg)
	count = POPi;
      else (void)POPs;
    }

    SensorCall(13579);cx = caller_cx(count + !!(PL_op->op_private & OPpOFFBYONE), &dbcx);
    SensorCall(13581);if (!cx) {
	SensorCall(13580);if (GIMME != G_ARRAY) {
	    EXTEND(SP, 1);
	    RETPUSHUNDEF;
	}
	RETURN;
    }

    SensorCall(13582);stash_hek = HvNAME_HEK((HV*)CopSTASH(cx->blk_oldcop));
    SensorCall(13584);if (GIMME != G_ARRAY) {
        EXTEND(SP, 1);
	SensorCall(13583);if (!stash_hek)
	    PUSHs(&PL_sv_undef);
	else {
	    dTARGET;
	    sv_sethek(TARG, stash_hek);
	    PUSHs(TARG);
	}
	RETURN;
    }

    EXTEND(SP, 11);

    SensorCall(13585);if (!stash_hek)
	PUSHs(&PL_sv_undef);
    else {
	dTARGET;
	sv_sethek(TARG, stash_hek);
	PUSHTARG;
    }
    mPUSHs(newSVpv(OutCopFILE(cx->blk_oldcop), 0));
    mPUSHi((I32)CopLINE(cx->blk_oldcop));
    SensorCall(13586);if (!has_arg)
	RETURN;
    SensorCall(13590);if (CxTYPE(cx) == CXt_SUB || CxTYPE(cx) == CXt_FORMAT) {
	SensorCall(13587);GV * const cvgv = CvGV(dbcx->blk_sub.cv);
	/* So is ccstack[dbcxix]. */
	SensorCall(13589);if (isGV(cvgv)) {
	    SensorCall(13588);SV * const sv = newSV(0);
	    gv_efullname3(sv, cvgv, NULL);
	    mPUSHs(sv);
	    PUSHs(boolSV(CxHASARGS(cx)));
	}
	else {
	    PUSHs(newSVpvs_flags("(unknown)", SVs_TEMP));
	    PUSHs(boolSV(CxHASARGS(cx)));
	}
    }
    else {
	PUSHs(newSVpvs_flags("(eval)", SVs_TEMP));
	mPUSHi(0);
    }
    SensorCall(13591);gimme = (I32)cx->blk_gimme;
    SensorCall(13592);if (gimme == G_VOID)
	PUSHs(&PL_sv_undef);
    else
	PUSHs(boolSV((gimme & G_WANT) == G_ARRAY));
    SensorCall(13595);if (CxTYPE(cx) == CXt_EVAL) {
	/* eval STRING */
	SensorCall(13593);if (CxOLD_OP_TYPE(cx) == OP_ENTEREVAL) {
	    PUSHs(cx->blk_eval.cur_text);
	    PUSHs(&PL_sv_no);
	}
	/* require */
	else {/*151*/SensorCall(13594);if (cx->blk_eval.old_namesv) {
	    mPUSHs(newSVsv(cx->blk_eval.old_namesv));
	    PUSHs(&PL_sv_yes);
	}
	/* eval BLOCK (try blocks have old_namesv == 0) */
	else {
	    PUSHs(&PL_sv_undef);
	    PUSHs(&PL_sv_undef);
	;/*152*/}}
    }
    else {
	PUSHs(&PL_sv_undef);
	PUSHs(&PL_sv_undef);
    }
    SensorCall(13598);if (CxTYPE(cx) == CXt_SUB && CxHASARGS(cx)
	&& CopSTASH_eq(PL_curcop, PL_debstash))
    {
	SensorCall(13596);AV * const ary = cx->blk_sub.argarray;
	const int off = AvARRAY(ary) - AvALLOC(ary);

	Perl_init_dbargs(aTHX);

	SensorCall(13597);if (AvMAX(PL_dbargs) < AvFILLp(ary) + off)
	    av_extend(PL_dbargs, AvFILLp(ary) + off);
	Copy(AvALLOC(ary), AvARRAY(PL_dbargs), AvFILLp(ary) + 1 + off, SV*);
	AvFILLp(PL_dbargs) = AvFILLp(ary) + off;
    }
    /* XXX only hints propagated via op_private are currently
     * visible (others are not easily accessible, since they
     * use the global PL_hints) */
    mPUSHi(CopHINTS_get(cx->blk_oldcop));
    {
	SensorCall(13599);SV * mask ;
	STRLEN * const old_warnings = cx->blk_oldcop->cop_warnings ;

	SensorCall(13600);if  (old_warnings == pWARN_NONE ||
		(old_warnings == pWARN_STD && (PL_dowarn & G_WARN_ON) == 0))
            mask = newSVpvn(WARN_NONEstring, WARNsize) ;
        else if (old_warnings == pWARN_ALL ||
		  (old_warnings == pWARN_STD && PL_dowarn & G_WARN_ON)) {
	    /* Get the bit mask for $warnings::Bits{all}, because
	     * it could have been extended by warnings::register */
	    SV **bits_all;
	    HV * const bits = get_hv("warnings::Bits", 0);
	    if (bits && (bits_all=hv_fetchs(bits, "all", FALSE))) {
		mask = newSVsv(*bits_all);
	    }
	    else {
		mask = newSVpvn(WARN_ALLstring, WARNsize) ;
	    }
	}
        else
            mask = newSVpvn((char *) (old_warnings + 1), old_warnings[0]);
        mPUSHs(mask);
    }

    PUSHs(cx->blk_oldcop->cop_hints_hash ?
	  sv_2mortal(newRV_noinc(MUTABLE_SV(cop_hints_2hv(cx->blk_oldcop, 0))))
	  : &PL_sv_undef);
    RETURN;
}

PP(pp_reset)
{
SensorCall(13601);    dVAR;
    dSP;
    const char * const tmps =
	(MAXARG < 1 || (!TOPs && !POPs)) ? (const char *)"" : POPpconstx;
    sv_reset(tmps, CopSTASH(PL_curcop));
    PUSHs(&PL_sv_yes);
    RETURN;
}

/* like pp_nextstate, but used instead when the debugger is active */

PP(pp_dbstate)
{
SensorCall(13602);    dVAR;
    PL_curcop = (COP*)PL_op;
    TAINT_NOT;		/* Each statement is presumed innocent */
    PL_stack_sp = PL_stack_base + cxstack[cxstack_ix].blk_oldsp;
    FREETMPS;

    PERL_ASYNC_CHECK();

    SensorCall(13611);if (PL_op->op_flags & OPf_SPECIAL /* breakpoint */
	    || SvIV(PL_DBsingle) || SvIV(PL_DBsignal) || SvIV(PL_DBtrace))
    {
	dSP;
	SensorCall(13603);register PERL_CONTEXT *cx;
	const I32 gimme = G_ARRAY;
	U8 hasargs;
	GV * const gv = PL_DBgv;
	register CV * const cv = GvCV(gv);

	SensorCall(13604);if (!cv)
	    DIE(aTHX_ "No DB::DB routine defined");

	SensorCall(13605);if (CvDEPTH(cv) >= 1 && !(PL_debug & DEBUG_DB_RECURSE_FLAG))
	    /* don't do recursive DB::DB call */
	    return NORMAL;

	ENTER;
	SAVETMPS;

	SAVEI32(PL_debug);
	SAVESTACK_POS();
	PL_debug = 0;
	SensorCall(13606);hasargs = 0;
	SPAGAIN;

	SensorCall(13610);if (CvISXSUB(cv)) {
	    CvDEPTH(cv)++;
	    PUSHMARK(SP);
	    SensorCall(13607);(void)(*CvXSUB(cv))(aTHX_ cv);
	    CvDEPTH(cv)--;
	    FREETMPS;
	    LEAVE;
	    {OP * ReplaceReturn1069 = NORMAL; SensorCall(13608); return ReplaceReturn1069;}
	}
	else {
	    PUSHBLOCK(cx, CXt_SUB, SP);
SensorCall(13609);	    PUSHSUB_DB(cx);
	    cx->blk_sub.retop = PL_op->op_next;
	    CvDEPTH(cv)++;
	    SAVECOMPPAD();
	    PAD_SET_CUR_NOSAVE(CvPADLIST(cv), 1);
	    RETURNOP(CvSTART(cv));
	}
    }
    else
	return NORMAL;
SensorCall(13612);}

STATIC SV **
S_adjust_stack_on_leave(pTHX_ SV **newsp, SV **sp, SV **mark, I32 gimme, U32 flags)
{
SensorCall(13613);    bool padtmp = 0;
    PERL_ARGS_ASSERT_ADJUST_STACK_ON_LEAVE;

    SensorCall(13615);if (flags & SVs_PADTMP) {
	SensorCall(13614);flags &= ~SVs_PADTMP;
	padtmp = 1;
    }
    SensorCall(13623);if (gimme == G_SCALAR) {
	SensorCall(13616);if (MARK < SP)
	    *++newsp = ((SvFLAGS(*SP) & flags) || (padtmp && SvPADTMP(*SP)))
			    ? *SP : sv_mortalcopy(*SP);
	else {
	    /* MEXTEND() only updates MARK, so reuse it instead of newsp. */
	    MARK = newsp;
	    MEXTEND(MARK, 1);
	    SensorCall(13617);*++MARK = &PL_sv_undef;
	    {SV ** ReplaceReturn1068 = MARK; SensorCall(13618); return ReplaceReturn1068;}
	}
    }
    else {/*27*/SensorCall(13619);if (gimme == G_ARRAY) {
	/* in case LEAVE wipes old return values */
	SensorCall(13620);while (++MARK <= SP) {
	    SensorCall(13621);if ((SvFLAGS(*MARK) & flags) || (padtmp && SvPADTMP(*MARK)))
		*++newsp = *MARK;
	    else {
		SensorCall(13622);*++newsp = sv_mortalcopy(*MARK);
		TAINT_NOT;	/* Each item is independent */
	    }
	}
	/* When this function was called with MARK == newsp, we reach this
	 * point with SP == newsp. */
    ;/*28*/}}

    {SV ** ReplaceReturn1067 = newsp; SensorCall(13624); return ReplaceReturn1067;}
}

PP(pp_enter)
{
SensorCall(13625);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    I32 gimme = GIMME_V;

    ENTER_with_name("block");

    SAVETMPS;
    PUSHBLOCK(cx, CXt_BLOCK, SP);

    RETURN;
}

PP(pp_leave)
{
SensorCall(13626);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    SV **newsp;
    PMOP *newpm;
    I32 gimme;

    SensorCall(13628);if (PL_op->op_flags & OPf_SPECIAL) {
	SensorCall(13627);cx = &cxstack[cxstack_ix];
	cx->blk_oldpm = PL_curpm;	/* fake block should preserve $1 et al */
    }

SensorCall(13629);    POPBLOCK(cx,newpm);

    gimme = OP_GIMME(PL_op, (cxstack_ix >= 0) ? gimme : G_SCALAR);

    TAINT_NOT;
    SP = adjust_stack_on_leave(newsp, SP, newsp, gimme, SVs_PADTMP|SVs_TEMP);
    PL_curpm = newpm;	/* Don't pop $1 et al till now */

    LEAVE_with_name("block");

    RETURN;
}

PP(pp_enteriter)
{
SensorCall(13630);    dVAR; dSP; dMARK;
    register PERL_CONTEXT *cx;
    const I32 gimme = GIMME_V;
    void *itervar; /* location of the iteration variable */
    U8 cxtype = CXt_LOOP_FOR;

    ENTER_with_name("loop1");
    SAVETMPS;

    SensorCall(13634);if (PL_op->op_targ) {			 /* "my" variable */
	SensorCall(13631);if (PL_op->op_private & OPpLVAL_INTRO) {        /* for my $x (...) */
	    SvPADSTALE_off(PAD_SVl(PL_op->op_targ));
	    SAVESETSVFLAGS(PAD_SVl(PL_op->op_targ),
		    SVs_PADSTALE, SVs_PADSTALE);
	}
	SAVEPADSVANDMORTALIZE(PL_op->op_targ);
#ifdef USE_ITHREADS
	SensorCall(13632);itervar = PL_comppad;
#else
	itervar = &PAD_SVl(PL_op->op_targ);
#endif
    }
    else {					/* symbol table variable */
	SensorCall(13633);GV * const gv = MUTABLE_GV(POPs);
	SV** svp = &GvSV(gv);
	save_pushptrptr(gv, SvREFCNT_inc(*svp), SAVEt_GVSV);
	*svp = newSV(0);
	itervar = (void *)gv;
    }

    SensorCall(13635);if (PL_op->op_private & OPpITER_DEF)
	cxtype |= CXp_FOR_DEF;

    ENTER_with_name("loop2");

    PUSHBLOCK(cx, cxtype, SP);
SensorCall(13636);    PUSHLOOP_FOR(cx, itervar, MARK);
    SensorCall(13652);if (PL_op->op_flags & OPf_STACKED) {
	SensorCall(13637);SV *maybe_ary = POPs;
	SensorCall(13647);if (SvTYPE(maybe_ary) != SVt_PVAV) {
	    dPOPss;
	    SensorCall(13638);SV * const right = maybe_ary;
	    SvGETMAGIC(sv);
	    SvGETMAGIC(right);
	    SensorCall(13645);if (RANGE_IS_NUMERIC(sv,right)) {
		SensorCall(13639);cx->cx_type &= ~CXTYPEMASK;
		cx->cx_type |= CXt_LOOP_LAZYIV;
		/* Make sure that no-one re-orders cop.h and breaks our
		   assumptions */
		assert(CxTYPE(cx) == CXt_LOOP_LAZYIV);
#ifdef NV_PRESERVES_UV
		if ((SvOK(sv) && ((SvNV_nomg(sv) < (NV)IV_MIN) ||
				  (SvNV_nomg(sv) > (NV)IV_MAX)))
			||
		    (SvOK(right) && ((SvNV_nomg(right) > (NV)IV_MAX) ||
				     (SvNV_nomg(right) < (NV)IV_MIN))))
#else
		SensorCall(13640);if ((SvOK(sv) && ((SvNV_nomg(sv) <= (NV)IV_MIN)
				  ||
		                  ((SvNV_nomg(sv) > 0) &&
					((SvUV_nomg(sv) > (UV)IV_MAX) ||
					 (SvNV_nomg(sv) > (NV)UV_MAX)))))
			||
		    (SvOK(right) && ((SvNV_nomg(right) <= (NV)IV_MIN)
				     ||
				     ((SvNV_nomg(right) > 0) &&
					((SvUV_nomg(right) > (UV)IV_MAX) ||
					 (SvNV_nomg(right) > (NV)UV_MAX))
				     ))))
#endif
		    DIE(aTHX_ "Range iterator outside integer range");
		SensorCall(13641);cx->blk_loop.state_u.lazyiv.cur = SvIV_nomg(sv);
		cx->blk_loop.state_u.lazyiv.end = SvIV_nomg(right);
#ifdef DEBUGGING
		/* for correct -Dstv display */
		cx->blk_oldsp = sp - PL_stack_base;
#endif
	    }
	    else {
		SensorCall(13642);cx->cx_type &= ~CXTYPEMASK;
		cx->cx_type |= CXt_LOOP_LAZYSV;
		/* Make sure that no-one re-orders cop.h and breaks our
		   assumptions */
		assert(CxTYPE(cx) == CXt_LOOP_LAZYSV);
		cx->blk_loop.state_u.lazysv.cur = newSVsv(sv);
		cx->blk_loop.state_u.lazysv.end = right;
		SvREFCNT_inc(right);
		(void) SvPV_force_nolen(cx->blk_loop.state_u.lazysv.cur);
		/* This will do the upgrade to SVt_PV, and warn if the value
		   is uninitialised.  */
		(void) SvPV_nolen_const(right);
		/* Doing this avoids a check every time in pp_iter in pp_hot.c
		   to replace !SvOK() with a pointer to "".  */
		SensorCall(13644);if (!SvOK(right)) {
		    SvREFCNT_dec(right);
		    SensorCall(13643);cx->blk_loop.state_u.lazysv.end = &PL_sv_no;
		}
	    }
	}
	else /* SvTYPE(maybe_ary) == SVt_PVAV */ {
	    SensorCall(13646);cx->blk_loop.state_u.ary.ary = MUTABLE_AV(maybe_ary);
	    SvREFCNT_inc(maybe_ary);
	    cx->blk_loop.state_u.ary.ix =
		(PL_op->op_private & OPpITER_REVERSED) ?
		AvFILL(cx->blk_loop.state_u.ary.ary) + 1 :
		-1;
	}
    }
    else { /* iterating over items on the stack */
	SensorCall(13648);cx->blk_loop.state_u.ary.ary = NULL; /* means to use the stack */
	SensorCall(13651);if (PL_op->op_private & OPpITER_REVERSED) {
	    SensorCall(13649);cx->blk_loop.state_u.ary.ix = cx->blk_oldsp + 1;
	}
	else {
	    SensorCall(13650);cx->blk_loop.state_u.ary.ix = MARK - PL_stack_base;
	}
    }

    RETURN;
}

PP(pp_enterloop)
{
SensorCall(13653);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    const I32 gimme = GIMME_V;

    ENTER_with_name("loop1");
    SAVETMPS;
    ENTER_with_name("loop2");

    PUSHBLOCK(cx, CXt_LOOP_PLAIN, SP);
    PUSHLOOP_PLAIN(cx, SP);

    RETURN;
}

PP(pp_leaveloop)
{
SensorCall(13654);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    I32 gimme;
    SV **newsp;
    PMOP *newpm;
    SV **mark;

    POPBLOCK(cx,newpm);
    assert(CxTYPE_is_LOOP(cx));
    mark = newsp;
    newsp = PL_stack_base + cx->blk_loop.resetsp;

    TAINT_NOT;
    SP = adjust_stack_on_leave(newsp, SP, MARK, gimme, 0);
    PUTBACK;

    POPLOOP(cx);	/* Stack values are safe: release loop vars ... */
    PL_curpm = newpm;	/* ... and pop $1 et al */

    LEAVE_with_name("loop2");
    LEAVE_with_name("loop1");

    {OP * ReplaceReturn1066 = NORMAL; SensorCall(13655); return ReplaceReturn1066;}
}

STATIC void
S_return_lvalues(pTHX_ SV **mark, SV **sp, SV **newsp, I32 gimme,
                       PERL_CONTEXT *cx, PMOP *newpm)
{
    SensorCall(13656);const bool ref = !!(CxLVAL(cx) & OPpENTERSUB_INARGS);
    SensorCall(13678);if (gimme == G_SCALAR) {
	SensorCall(13657);if (CxLVAL(cx) && !ref) {     /* Leave it as it is if we can. */
	    SensorCall(13658);SV *sv;
	    const char *what = NULL;
	    SensorCall(13663);if (MARK < SP) {
		assert(MARK+1 == SP);
		SensorCall(13659);if ((SvPADTMP(TOPs) ||
		     (SvFLAGS(TOPs) & (SVf_READONLY | SVf_FAKE))
		       == SVf_READONLY
		    ) &&
		    !SvSMAGICAL(TOPs)) {
		    SensorCall(13660);what =
			SvREADONLY(TOPs) ? (TOPs == &PL_sv_undef) ? "undef"
			: "a readonly value" : "a temporary";
		}
		else {/*281*/SensorCall(13661);goto copy_sv;/*282*/}
	    }
	    else {
		/* sub:lvalue{} will take us here. */
		SensorCall(13662);what = "undef";
	    }
	    LEAVE;
	    cxstack_ix--;
	    POPSUB(cx,sv);
	    PL_curpm = newpm;
	    LEAVESUB(sv);
	    SensorCall(13664);Perl_croak(aTHX_
	              "Can't return %s from lvalue subroutine", what
	    );
	}
	SensorCall(13670);if (MARK < SP) {
	      copy_sv:
		SensorCall(13665);if (cx->blk_sub.cv && CvDEPTH(cx->blk_sub.cv) > 1) {
		    SensorCall(13666);if (!SvPADTMP(*SP)) {
			SensorCall(13667);*++newsp = SvREFCNT_inc(*SP);
			FREETMPS;
			sv_2mortal(*newsp);
		    }
		    else {
			/* FREETMPS could clobber it */
			SensorCall(13668);SV *sv = SvREFCNT_inc(*SP);
			FREETMPS;
			*++newsp = sv_mortalcopy(sv);
			SvREFCNT_dec(sv);
		    }
		}
		else
		    *++newsp =
		      SvPADTMP(*SP)
		       ? sv_mortalcopy(*SP)
		       : !SvTEMP(*SP)
		          ? sv_2mortal(SvREFCNT_inc_simple_NN(*SP))
		          : *SP;
	}
	else {
	    EXTEND(newsp,1);
	    SensorCall(13669);*++newsp = &PL_sv_undef;
	}
	SensorCall(13672);if (CxLVAL(cx) & OPpDEREF) {
	    SvGETMAGIC(TOPs);
	    SensorCall(13671);if (!SvOK(TOPs)) {
		TOPs = vivify_ref(TOPs, CxLVAL(cx) & OPpDEREF);
	    }
	}
    }
    else {/*283*/SensorCall(13673);if (gimme == G_ARRAY) {
	assert (!(CxLVAL(cx) & OPpDEREF));
	SensorCall(13674);if (ref || !CxLVAL(cx))
	    while (++MARK <= SP)
		*++newsp =
		       SvFLAGS(*MARK) & SVs_PADTMP
		           ? sv_mortalcopy(*MARK)
		     : SvTEMP(*MARK)
		           ? *MARK
		           : sv_2mortal(SvREFCNT_inc_simple_NN(*MARK));
	else {/*285*/SensorCall(13675);while (++MARK <= SP) {
	    SensorCall(13676);if (*MARK != &PL_sv_undef
		    && (SvPADTMP(*MARK)
		       || (SvFLAGS(*MARK) & (SVf_READONLY|SVf_FAKE))
		             == SVf_READONLY
		       )
	    ) {
		    SensorCall(13677);SV *sv;
		    /* Might be flattened array after $#array =  */
		    PUTBACK;
		    LEAVE;
		    cxstack_ix--;
		    POPSUB(cx,sv);
		    PL_curpm = newpm;
		    LEAVESUB(sv);
	       /* diag_listed_as: Can't return %s from lvalue subroutine */
		    Perl_croak(aTHX_
			"Can't return a %s from lvalue subroutine",
			SvREADONLY(TOPs) ? "readonly value" : "temporary");
	    }
	    else
		*++newsp =
		    SvTEMP(*MARK)
		       ? *MARK
		       : sv_2mortal(SvREFCNT_inc_simple_NN(*MARK));
	;/*286*/}}
    ;/*284*/}}
    PL_stack_sp = newsp;
}

PP(pp_return)
{
SensorCall(13679);    dVAR; dSP; dMARK;
    register PERL_CONTEXT *cx;
    bool popsub2 = FALSE;
    bool clear_errsv = FALSE;
    bool lval = FALSE;
    I32 gimme;
    SV **newsp;
    PMOP *newpm;
    I32 optype = 0;
    SV *namesv;
    SV *sv;
    OP *retop = NULL;

    const I32 cxix = dopoptosub(cxstack_ix);

    SensorCall(13682);if (cxix < 0) {
	SensorCall(13680);if (CxMULTICALL(cxstack)) { /* In this case we must be in a
				     * sort block, which is a CXt_NULL
				     * not a CXt_SUB */
	    dounwind(0);
	    PL_stack_base[1] = *PL_stack_sp;
	    PL_stack_sp = PL_stack_base + 1;
	    {OP * ReplaceReturn1065 = 0; SensorCall(13681); return ReplaceReturn1065;}
	}
	else
	    DIE(aTHX_ "Can't return outside a subroutine");
    }
    SensorCall(13683);if (cxix < cxstack_ix)
	dounwind(cxix);

    SensorCall(13688);if (CxMULTICALL(&cxstack[cxix])) {
	SensorCall(13684);gimme = cxstack[cxix].blk_gimme;
	SensorCall(13686);if (gimme == G_VOID)
	    PL_stack_sp = PL_stack_base;
	else {/*269*/SensorCall(13685);if (gimme == G_SCALAR) {
	    PL_stack_base[1] = *PL_stack_sp;
	    PL_stack_sp = PL_stack_base + 1;
	;/*270*/}}
	{OP * ReplaceReturn1064 = 0; SensorCall(13687); return ReplaceReturn1064;}
    }

SensorCall(13689);    POPBLOCK(cx,newpm);
    SensorCall(13701);switch (CxTYPE(cx)) {
    case CXt_SUB:
	SensorCall(13690);popsub2 = TRUE;
	lval = !!CvLVALUE(cx->blk_sub.cv);
	retop = cx->blk_sub.retop;
	cxstack_ix++; /* preserve cx entry on stack for use by POPSUB */
	SensorCall(13691);break;
    case CXt_EVAL:
	SensorCall(13692);if (!(PL_in_eval & EVAL_KEEPERR))
	    clear_errsv = TRUE;
	POPEVAL(cx);
	SensorCall(13693);namesv = cx->blk_eval.old_namesv;
	retop = cx->blk_eval.retop;
	SensorCall(13695);if (CxTRYBLOCK(cx))
	    {/*271*/SensorCall(13694);break;/*272*/}
	SensorCall(13697);if (optype == OP_REQUIRE &&
	    (MARK == SP || (gimme == G_SCALAR && !SvTRUE(*SP))) )
	{
	    /* Unassume the success we assumed earlier. */
	    SensorCall(13696);(void)hv_delete(GvHVn(PL_incgv),
			    SvPVX_const(namesv),
                            SvUTF8(namesv) ? -(I32)SvCUR(namesv) : (I32)SvCUR(namesv),
			    G_DISCARD);
	    DIE(aTHX_ "%"SVf" did not return a true value", SVfARG(namesv));
	}
	SensorCall(13698);break;
    case CXt_FORMAT:
	POPFORMAT(cx);
	SensorCall(13699);retop = cx->blk_sub.retop;
	SensorCall(13700);break;
    default:
	DIE(aTHX_ "panic: return, type=%u", (unsigned) CxTYPE(cx));
    }

    TAINT_NOT;
    SensorCall(13713);if (lval) {/*273*/SensorCall(13702);S_return_lvalues(aTHX_ MARK, SP, newsp, gimme, cx, newpm);/*274*/}
    else {
      SensorCall(13703);if (gimme == G_SCALAR) {
	SensorCall(13704);if (MARK < SP) {
	    SensorCall(13705);if (popsub2) {
		SensorCall(13706);if (cx->blk_sub.cv && CvDEPTH(cx->blk_sub.cv) > 1) {
		    SensorCall(13707);if (SvTEMP(TOPs) && SvREFCNT(TOPs) == 1
			 && !SvMAGICAL(TOPs)) {
			SensorCall(13708);*++newsp = SvREFCNT_inc(*SP);
			FREETMPS;
			sv_2mortal(*newsp);
		    }
		    else {
			SensorCall(13709);sv = SvREFCNT_inc(*SP);	/* FREETMPS could clobber it */
			FREETMPS;
			*++newsp = sv_mortalcopy(sv);
			SvREFCNT_dec(sv);
		    }
		}
		else if (SvTEMP(*SP) && SvREFCNT(*SP) == 1
			  && !SvMAGICAL(*SP)) {
		    *++newsp = *SP;
		}
		else
		    *++newsp = sv_mortalcopy(*SP);
	    }
	    else
		*++newsp = sv_mortalcopy(*SP);
	}
	else
	    *++newsp = &PL_sv_undef;
      }
      else {/*275*/SensorCall(13710);if (gimme == G_ARRAY) {
	SensorCall(13711);while (++MARK <= SP) {
	    SensorCall(13712);*++newsp = popsub2 && SvTEMP(*MARK) && SvREFCNT(*MARK) == 1
			       && !SvGMAGICAL(*MARK)
			? *MARK : sv_mortalcopy(*MARK);
	    TAINT_NOT;		/* Each item is independent */
	}
      ;/*276*/}}
      PL_stack_sp = newsp;
    }

    LEAVE;
    /* Stack values are safe: */
    SensorCall(13714);if (popsub2) {
	cxstack_ix--;
	POPSUB(cx,sv);	/* release CV and @_ ... */
    }
    else
	sv = NULL;
    PL_curpm = newpm;	/* ... and pop $1 et al */

    LEAVESUB(sv);
    SensorCall(13715);if (clear_errsv) {
	CLEAR_ERRSV();
    }
    {OP * ReplaceReturn1063 = retop; SensorCall(13716); return ReplaceReturn1063;}
}

/* This duplicates parts of pp_leavesub, so that it can share code with
 * pp_return */
PP(pp_leavesublv)
{
SensorCall(13717);    dVAR; dSP;
    SV **newsp;
    PMOP *newpm;
    I32 gimme;
    register PERL_CONTEXT *cx;
    SV *sv;

    SensorCall(13719);if (CxMULTICALL(&cxstack[cxstack_ix]))
	{/*239*/{OP * ReplaceReturn1062 = 0; SensorCall(13718); return ReplaceReturn1062;}/*240*/}

SensorCall(13720);    POPBLOCK(cx,newpm);
    cxstack_ix++; /* temporarily protect top context */

    TAINT_NOT;

    S_return_lvalues(aTHX_ newsp, SP, newsp, gimme, cx, newpm);

    LEAVE;
    cxstack_ix--;
    POPSUB(cx,sv);	/* Stack values are safe: release CV and @_ ... */
    PL_curpm = newpm;	/* ... and pop $1 et al */

    LEAVESUB(sv);
    {OP * ReplaceReturn1061 = cx->blk_sub.retop; SensorCall(13721); return ReplaceReturn1061;}
}

PP(pp_last)
{
SensorCall(13722);    dVAR; dSP;
    I32 cxix;
    register PERL_CONTEXT *cx;
    I32 pop2 = 0;
    I32 gimme;
    I32 optype;
    OP *nextop = NULL;
    SV **newsp;
    PMOP *newpm;
    SV **mark;
    SV *sv = NULL;


    SensorCall(13727);if (PL_op->op_flags & OPf_SPECIAL) {
	SensorCall(13723);cxix = dopoptoloop(cxstack_ix);
	SensorCall(13724);if (cxix < 0)
	    DIE(aTHX_ "Can't \"last\" outside a loop block");
    }
    else {
        SensorCall(13725);cxix = dopoptolabel(cPVOP->op_pv, strlen(cPVOP->op_pv),
                           (cPVOP->op_private & OPpPV_IS_UTF8) ? SVf_UTF8 : 0);
	SensorCall(13726);if (cxix < 0)
	    DIE(aTHX_ "Label not found for \"last %"SVf"\"",
                                        SVfARG(newSVpvn_flags(cPVOP->op_pv,
                                                    strlen(cPVOP->op_pv),
                                                    ((cPVOP->op_private & OPpPV_IS_UTF8)
                                                    ? SVf_UTF8 : 0) | SVs_TEMP)));
    }
    SensorCall(13728);if (cxix < cxstack_ix)
	dounwind(cxix);

SensorCall(13729);    POPBLOCK(cx,newpm);
    cxstack_ix++; /* temporarily protect top context */
    mark = newsp;
    SensorCall(13738);switch (CxTYPE(cx)) {
    case CXt_LOOP_LAZYIV:
    case CXt_LOOP_LAZYSV:
    case CXt_LOOP_FOR:
    case CXt_LOOP_PLAIN:
	SensorCall(13730);pop2 = CxTYPE(cx);
	newsp = PL_stack_base + cx->blk_loop.resetsp;
	nextop = cx->blk_loop.my_op->op_lastop->op_next;
	SensorCall(13731);break;
    case CXt_SUB:
	SensorCall(13732);pop2 = CXt_SUB;
	nextop = cx->blk_sub.retop;
	SensorCall(13733);break;
    case CXt_EVAL:
	POPEVAL(cx);
	SensorCall(13734);nextop = cx->blk_eval.retop;
	SensorCall(13735);break;
    case CXt_FORMAT:
	POPFORMAT(cx);
	SensorCall(13736);nextop = cx->blk_sub.retop;
	SensorCall(13737);break;
    default:
	DIE(aTHX_ "panic: last, type=%u", (unsigned) CxTYPE(cx));
    }

    TAINT_NOT;
    SP = adjust_stack_on_leave(newsp, SP, MARK, gimme,
				pop2 == CXt_SUB ? SVs_TEMP : 0);
    PUTBACK;

    LEAVE;
    cxstack_ix--;
    /* Stack values are safe: */
    SensorCall(13741);switch (pop2) {
    case CXt_LOOP_LAZYIV:
    case CXt_LOOP_PLAIN:
    case CXt_LOOP_LAZYSV:
    case CXt_LOOP_FOR:
	POPLOOP(cx);	/* release loop vars ... */
	LEAVE;
	SensorCall(13739);break;
    case CXt_SUB:
	POPSUB(cx,sv);	/* release CV and @_ ... */
	SensorCall(13740);break;
    }
    PL_curpm = newpm;	/* ... and pop $1 et al */

    LEAVESUB(sv);
    PERL_UNUSED_VAR(optype);
    PERL_UNUSED_VAR(gimme);
    {OP * ReplaceReturn1060 = nextop; SensorCall(13742); return ReplaceReturn1060;}
}

PP(pp_next)
{
SensorCall(13743);    dVAR;
    I32 cxix;
    register PERL_CONTEXT *cx;
    I32 inner;

    SensorCall(13748);if (PL_op->op_flags & OPf_SPECIAL) {
	SensorCall(13744);cxix = dopoptoloop(cxstack_ix);
	SensorCall(13745);if (cxix < 0)
	    DIE(aTHX_ "Can't \"next\" outside a loop block");
    }
    else {
	SensorCall(13746);cxix = dopoptolabel(cPVOP->op_pv, strlen(cPVOP->op_pv),
                           (cPVOP->op_private & OPpPV_IS_UTF8) ? SVf_UTF8 : 0);
 	SensorCall(13747);if (cxix < 0)
	    DIE(aTHX_ "Label not found for \"next %"SVf"\"",
                                        SVfARG(newSVpvn_flags(cPVOP->op_pv, 
                                                    strlen(cPVOP->op_pv),
                                                    ((cPVOP->op_private & OPpPV_IS_UTF8)
                                                    ? SVf_UTF8 : 0) | SVs_TEMP)));
    }
    SensorCall(13749);if (cxix < cxstack_ix)
	dounwind(cxix);

    /* clear off anything above the scope we're re-entering, but
     * save the rest until after a possible continue block */
    SensorCall(13750);inner = PL_scopestack_ix;
    TOPBLOCK(cx);
    SensorCall(13751);if (PL_scopestack_ix < inner)
	leave_scope(PL_scopestack[PL_scopestack_ix]);
    PL_curcop = cx->blk_oldcop;
    PERL_ASYNC_CHECK();
    {OP * ReplaceReturn1059 = (cx)->blk_loop.my_op->op_nextop; SensorCall(13752); return ReplaceReturn1059;}
}

PP(pp_redo)
{
SensorCall(13753);    dVAR;
    I32 cxix;
    register PERL_CONTEXT *cx;
    I32 oldsave;
    OP* redo_op;

    SensorCall(13758);if (PL_op->op_flags & OPf_SPECIAL) {
	SensorCall(13754);cxix = dopoptoloop(cxstack_ix);
	SensorCall(13755);if (cxix < 0)
	    DIE(aTHX_ "Can't \"redo\" outside a loop block");
    }
    else {
	SensorCall(13756);cxix = dopoptolabel(cPVOP->op_pv, strlen(cPVOP->op_pv),
                           (cPVOP->op_private & OPpPV_IS_UTF8) ? SVf_UTF8 : 0);
 	SensorCall(13757);if (cxix < 0)
	    DIE(aTHX_ "Label not found for \"redo %"SVf"\"",
                                        SVfARG(newSVpvn_flags(cPVOP->op_pv,
                                                    strlen(cPVOP->op_pv),
                                                    ((cPVOP->op_private & OPpPV_IS_UTF8)
                                                    ? SVf_UTF8 : 0) | SVs_TEMP)));
    }
    SensorCall(13759);if (cxix < cxstack_ix)
	dounwind(cxix);

    SensorCall(13760);redo_op = cxstack[cxix].blk_loop.my_op->op_redoop;
    SensorCall(13762);if (redo_op->op_type == OP_ENTER) {
	/* pop one less context to avoid $x being freed in while (my $x..) */
	cxstack_ix++;
	assert(CxTYPE(&cxstack[cxstack_ix]) == CXt_BLOCK);
	SensorCall(13761);redo_op = redo_op->op_next;
    }

SensorCall(13763);    TOPBLOCK(cx);
    oldsave = PL_scopestack[PL_scopestack_ix - 1];
    LEAVE_SCOPE(oldsave);
    FREETMPS;
    PL_curcop = cx->blk_oldcop;
    PERL_ASYNC_CHECK();
    {OP * ReplaceReturn1058 = redo_op; SensorCall(13764); return ReplaceReturn1058;}
}

STATIC OP *
S_dofindlabel(pTHX_ OP *o, const char *label, STRLEN len, U32 flags, OP **opstack, OP **oplimit)
{
SensorCall(13765);    dVAR;
    OP **ops = opstack;
    static const char too_deep[] = "Target of goto is too deeply nested";

    PERL_ARGS_ASSERT_DOFINDLABEL;

    SensorCall(13767);if (ops >= oplimit)
	{/*79*/SensorCall(13766);Perl_croak(aTHX_ too_deep);/*80*/}
    SensorCall(13771);if (o->op_type == OP_LEAVE ||
	o->op_type == OP_SCOPE ||
	o->op_type == OP_LEAVELOOP ||
	o->op_type == OP_LEAVESUB ||
	o->op_type == OP_LEAVETRY)
    {
	SensorCall(13768);*ops++ = cUNOPo->op_first;
	SensorCall(13770);if (ops >= oplimit)
	    {/*81*/SensorCall(13769);Perl_croak(aTHX_ too_deep);/*82*/}
    }
    SensorCall(13772);*ops = 0;
    SensorCall(13790);if (o->op_flags & OPf_KIDS) {
	SensorCall(13773);OP *kid;
	/* First try all the kids at this level, since that's likeliest. */
	SensorCall(13778);for (kid = cUNOPo->op_first; kid; kid = kid->op_sibling) {
	    SensorCall(13774);if (kid->op_type == OP_NEXTSTATE || kid->op_type == OP_DBSTATE) {
                SensorCall(13775);STRLEN kid_label_len;
                U32 kid_label_flags;
		const char *kid_label = CopLABEL_len_flags(kCOP,
                                                    &kid_label_len, &kid_label_flags);
		SensorCall(13777);if (kid_label && (
                    ( (kid_label_flags & SVf_UTF8) != (flags & SVf_UTF8) ) ?
                        (flags & SVf_UTF8)
                            ? (bytes_cmp_utf8(
                                        (const U8*)kid_label, kid_label_len,
                                        (const U8*)label, len) == 0)
                            : (bytes_cmp_utf8(
                                        (const U8*)label, len,
                                        (const U8*)kid_label, kid_label_len) == 0)
                    : ( len == kid_label_len && ((kid_label == label)
                                    || memEQ(kid_label, label, len)))))
		    {/*83*/{OP * ReplaceReturn1057 = kid; SensorCall(13776); return ReplaceReturn1057;}/*84*/}
	    }
	}
	SensorCall(13789);for (kid = cUNOPo->op_first; kid; kid = kid->op_sibling) {
	    SensorCall(13779);if (kid == PL_lastgotoprobe)
		{/*85*/SensorCall(13780);continue;/*86*/}
	    SensorCall(13786);if (kid->op_type == OP_NEXTSTATE || kid->op_type == OP_DBSTATE) {
	        SensorCall(13781);if (ops == opstack)
		    {/*87*/SensorCall(13782);*ops++ = kid;/*88*/}
		else {/*89*/SensorCall(13783);if (ops[-1]->op_type == OP_NEXTSTATE ||
		         ops[-1]->op_type == OP_DBSTATE)
		    {/*91*/SensorCall(13784);ops[-1] = kid;/*92*/}
		else
		    {/*93*/SensorCall(13785);*ops++ = kid;/*94*/}/*90*/}
	    }
	    SensorCall(13788);if ((o = dofindlabel(kid, label, len, flags, ops, oplimit)))
		{/*95*/{OP * ReplaceReturn1056 = o; SensorCall(13787); return ReplaceReturn1056;}/*96*/}
	}
    }
    SensorCall(13791);*ops = 0;
    {OP * ReplaceReturn1055 = 0; SensorCall(13792); return ReplaceReturn1055;}
}

PP(pp_goto)
{
SensorCall(13793);    dVAR; dSP;
    OP *retop = NULL;
    I32 ix;
    register PERL_CONTEXT *cx;
#define GOTO_DEPTH 64
    OP *enterops[GOTO_DEPTH];
    const char *label = NULL;
    STRLEN label_len = 0;
    U32 label_flags = 0;
    const bool do_dump = (PL_op->op_type == OP_DUMP);
    static const char must_have_label[] = "goto must have label";

    SensorCall(13861);if (PL_op->op_flags & OPf_STACKED) {
	SensorCall(13794);SV * const sv = POPs;

	/* This egregious kludge implements goto &subroutine */
	SensorCall(13857);if (SvROK(sv) && SvTYPE(SvRV(sv)) == SVt_PVCV) {
	    SensorCall(13795);I32 cxix;
	    register PERL_CONTEXT *cx;
	    CV *cv = MUTABLE_CV(SvRV(sv));
	    SV** mark;
	    I32 items = 0;
	    I32 oldsave;
	    bool reified = 0;

	retry:
	    SensorCall(13805);if (!CvROOT(cv) && !CvXSUB(cv)) {
		SensorCall(13796);const GV * const gv = CvGV(cv);
		SensorCall(13804);if (gv) {
		    SensorCall(13797);GV *autogv;
		    SV *tmpstr;
		    /* autoloaded stub? */
		    SensorCall(13799);if (cv != GvCV(gv) && (cv = GvCV(gv)))
			{/*219*/SensorCall(13798);goto retry;/*220*/}
		    SensorCall(13800);autogv = gv_autoload_pvn(GvSTASH(gv), GvNAME(gv),
					  GvNAMELEN(gv),
                                          GvNAMEUTF8(gv) ? SVf_UTF8 : 0);
		    SensorCall(13802);if (autogv && (cv = GvCV(autogv)))
			{/*221*/SensorCall(13801);goto retry;/*222*/}
		    SensorCall(13803);tmpstr = sv_newmortal();
		    gv_efullname3(tmpstr, gv, NULL);
		    DIE(aTHX_ "Goto undefined subroutine &%"SVf"", SVfARG(tmpstr));
		}
		DIE(aTHX_ "Goto undefined subroutine");
	    }

	    /* First do some returnish stuff. */
	    SvREFCNT_inc_simple_void(cv); /* avoid premature free during unwind */
	    FREETMPS;
	    SensorCall(13806);cxix = dopoptosub(cxstack_ix);
	    SensorCall(13807);if (cxix < 0)
		DIE(aTHX_ "Can't goto subroutine outside a subroutine");
	    SensorCall(13808);if (cxix < cxstack_ix)
		dounwind(cxix);
SensorCall(13809);	    TOPBLOCK(cx);
	    SPAGAIN;
	    /* ban goto in eval: see <20050521150056.GC20213@iabyn.com> */
	    SensorCall(13812);if (CxTYPE(cx) == CXt_EVAL) {
		SensorCall(13810);if (CxREALEVAL(cx))
		/* diag_listed_as: Can't goto subroutine from an eval-%s */
		    DIE(aTHX_ "Can't goto subroutine from an eval-string");
		else
		/* diag_listed_as: Can't goto subroutine from an eval-%s */
		    DIE(aTHX_ "Can't goto subroutine from an eval-block");
	    }
	    else {/*223*/SensorCall(13811);if (CxMULTICALL(cx))
		DIE(aTHX_ "Can't goto subroutine from a sort sub (or similar callback)");/*224*/}
	    SensorCall(13818);if (CxTYPE(cx) == CXt_SUB && CxHASARGS(cx)) {
		/* put @_ back onto stack */
		SensorCall(13813);AV* av = cx->blk_sub.argarray;

		items = AvFILLp(av) + 1;
		EXTEND(SP, items+1); /* @_ could have been extended. */
		Copy(AvARRAY(av), SP + 1, items, SV*);
		SvREFCNT_dec(GvAV(PL_defgv));
		GvAV(PL_defgv) = cx->blk_sub.savearray;
		CLEAR_ARGARRAY(av);
		/* abandon @_ if it got reified */
		SensorCall(13815);if (AvREAL(av)) {
		    SensorCall(13814);reified = 1;
		    SvREFCNT_dec(av);
		    av = newAV();
		    av_extend(av, items-1);
		    AvREIFY_only(av);
		    PAD_SVl(0) = MUTABLE_SV(cx->blk_sub.argarray = av);
		}
	    }
	    else {/*225*/SensorCall(13816);if (CvISXSUB(cv)) {	/* put GvAV(defgv) back onto stack */
		SensorCall(13817);AV* const av = GvAV(PL_defgv);
		items = AvFILLp(av) + 1;
		EXTEND(SP, items+1); /* @_ could have been extended. */
		Copy(AvARRAY(av), SP + 1, items, SV*);
	    ;/*226*/}}
	    SensorCall(13819);mark = SP;
	    SP += items;
	    SensorCall(13820);if (CxTYPE(cx) == CXt_SUB &&
		!(CvDEPTH(cx->blk_sub.cv) = cx->blk_sub.olddepth))
		SvREFCNT_dec(cx->blk_sub.cv);
	    SensorCall(13821);oldsave = PL_scopestack[PL_scopestack_ix - 1];
	    LEAVE_SCOPE(oldsave);

	    /* A destructor called during LEAVE_SCOPE could have undefined
	     * our precious cv.  See bug #99850. */
	    SensorCall(13825);if (!CvROOT(cv) && !CvXSUB(cv)) {
		SensorCall(13822);const GV * const gv = CvGV(cv);
		SensorCall(13824);if (gv) {
		    SensorCall(13823);SV * const tmpstr = sv_newmortal();
		    gv_efullname3(tmpstr, gv, NULL);
		    DIE(aTHX_ "Goto undefined subroutine &%"SVf"",
			       SVfARG(tmpstr));
		}
		DIE(aTHX_ "Goto undefined subroutine");
	    }

	    /* Now do some callish stuff. */
	    SAVETMPS;
	    SAVEFREESV(cv); /* later, undo the 'avoid premature free' hack */
	    SensorCall(13854);if (CvISXSUB(cv)) {
		SensorCall(13826);OP* const retop = cx->blk_sub.retop;
		SV **newsp PERL_UNUSED_DECL;
		I32 gimme PERL_UNUSED_DECL;
		SensorCall(13829);if (reified) {
		    SensorCall(13827);I32 index;
		    SensorCall(13828);for (index=0; index<items; index++)
			sv_2mortal(SP[-index]);
		}

		/* XS subs don't have a CxSUB, so pop it */
SensorCall(13830);		POPBLOCK(cx, PL_curpm);
		/* Push a mark for the start of arglist */
		PUSHMARK(mark);
		PUTBACK;
		(void)(*CvXSUB(cv))(aTHX_ cv);
		LEAVE;
		PERL_ASYNC_CHECK();
		{OP * ReplaceReturn1054 = retop; SensorCall(13831); return ReplaceReturn1054;}
	    }
	    else {
		SensorCall(13832);AV* const padlist = CvPADLIST(cv);
		SensorCall(13834);if (CxTYPE(cx) == CXt_EVAL) {
		    PL_in_eval = CxOLD_IN_EVAL(cx);
		    PL_eval_root = cx->blk_eval.old_eval_root;
		    SensorCall(13833);cx->cx_type = CXt_SUB;
		}
		SensorCall(13835);cx->blk_sub.cv = cv;
		cx->blk_sub.olddepth = CvDEPTH(cv);

		CvDEPTH(cv)++;
		SensorCall(13837);if (CvDEPTH(cv) < 2)
		    SvREFCNT_inc_simple_void_NN(cv);
		else {
		    SensorCall(13836);if (CvDEPTH(cv) == PERL_SUB_DEPTH_WARN && ckWARN(WARN_RECURSION))
			sub_crush_depth(cv);
		    pad_push(padlist, CvDEPTH(cv));
		}
		PL_curcop = cx->blk_oldcop;
		SAVECOMPPAD();
		PAD_SET_CUR_NOSAVE(padlist, CvDEPTH(cv));
		SensorCall(13848);if (CxHASARGS(cx))
		{
		    SensorCall(13838);AV *const av = MUTABLE_AV(PAD_SVl(0));

		    cx->blk_sub.savearray = GvAV(PL_defgv);
		    GvAV(PL_defgv) = MUTABLE_AV(SvREFCNT_inc_simple(av));
		    CX_CURPAD_SAVE(cx->blk_sub);
		    cx->blk_sub.argarray = av;

		    SensorCall(13842);if (items >= AvMAX(av) + 1) {
			SensorCall(13839);SV **ary = AvALLOC(av);
			SensorCall(13840);if (AvARRAY(av) != ary) {
			    AvMAX(av) += AvARRAY(av) - AvALLOC(av);
			    AvARRAY(av) = ary;
			}
			SensorCall(13841);if (items >= AvMAX(av) + 1) {
			    AvMAX(av) = items - 1;
			    Renew(ary,items+1,SV*);
			    AvALLOC(av) = ary;
			    AvARRAY(av) = ary;
			}
		    }
		    SensorCall(13843);++mark;
		    Copy(mark,AvARRAY(av),items,SV*);
		    AvFILLp(av) = items - 1;
		    assert(!AvREAL(av));
		    SensorCall(13844);if (reified) {
			/* transfer 'ownership' of refcnts to new @_ */
			AvREAL_on(av);
			AvREIFY_off(av);
		    }
		    SensorCall(13847);while (items--) {
			SensorCall(13845);if (*mark)
			    SvTEMP_off(*mark);
			SensorCall(13846);mark++;
		    }
		}
		SensorCall(13853);if (PERLDB_SUB) {	/* Checking curstash breaks DProf. */
		    SensorCall(13849);Perl_get_db_sub(aTHX_ NULL, cv);
		    SensorCall(13852);if (PERLDB_GOTO) {
			SensorCall(13850);CV * const gotocv = get_cvs("DB::goto", 0);
			SensorCall(13851);if (gotocv) {
			    PUSHMARK( PL_stack_sp );
			    call_sv(MUTABLE_SV(gotocv), G_SCALAR | G_NODEBUG);
			    PL_stack_sp--;
			}
		    }
		}
		PERL_ASYNC_CHECK();
		RETURNOP(CvSTART(cv));
	    }
	}
	else {
	    SensorCall(13855);label       = SvPV_const(sv, label_len);
            label_flags = SvUTF8(sv);
	    SensorCall(13856);if (!(do_dump || *label))
		DIE(aTHX_ must_have_label);
	}
    }
    else {/*227*/SensorCall(13858);if (PL_op->op_flags & OPf_SPECIAL) {
	SensorCall(13859);if (! do_dump)
	    DIE(aTHX_ must_have_label);
    }
    else {
 	SensorCall(13860);label       = cPVOP->op_pv;
        label_flags = (cPVOP->op_private & OPpPV_IS_UTF8) ? SVf_UTF8 : 0;
        label_len   = strlen(label);
    ;/*228*/}}

    PERL_ASYNC_CHECK();

    SensorCall(13907);if (label && *label) {
	SensorCall(13862);OP *gotoprobe = NULL;
	bool leaving_eval = FALSE;
	bool in_block = FALSE;
	PERL_CONTEXT *last_eval_cx = NULL;

	/* find label */

	PL_lastgotoprobe = NULL;
	*enterops = 0;
	SensorCall(13889);for (ix = cxstack_ix; ix >= 0; ix--) {
	    SensorCall(13863);cx = &cxstack[ix];
	    SensorCall(13880);switch (CxTYPE(cx)) {
	    case CXt_EVAL:
		SensorCall(13864);leaving_eval = TRUE;
                SensorCall(13867);if (!CxTRYBLOCK(cx)) {
		    SensorCall(13865);gotoprobe = (last_eval_cx ?
				last_eval_cx->blk_eval.old_eval_root :
				PL_eval_root);
		    last_eval_cx = cx;
		    SensorCall(13866);break;
                }
                /* else fall through */
	    case CXt_LOOP_LAZYIV:
	    case CXt_LOOP_LAZYSV:
	    case CXt_LOOP_FOR:
	    case CXt_LOOP_PLAIN:
	    case CXt_GIVEN:
	    case CXt_WHEN:
		SensorCall(13868);gotoprobe = cx->blk_oldcop->op_sibling;
		SensorCall(13869);break;
	    case CXt_SUBST:
		SensorCall(13870);continue;
	    case CXt_BLOCK:
		SensorCall(13871);if (ix) {
		    SensorCall(13872);gotoprobe = cx->blk_oldcop->op_sibling;
		    in_block = TRUE;
		} else
		    gotoprobe = PL_main_root;
		SensorCall(13873);break;
	    case CXt_SUB:
		SensorCall(13874);if (CvDEPTH(cx->blk_sub.cv) && !CxMULTICALL(cx)) {
		    SensorCall(13875);gotoprobe = CvROOT(cx->blk_sub.cv);
		    SensorCall(13876);break;
		}
		/* FALL THROUGH */
	    case CXt_FORMAT:
	    case CXt_NULL:
		DIE(aTHX_ "Can't \"goto\" out of a pseudo block");
	    default:
		SensorCall(13877);if (ix)
		    DIE(aTHX_ "panic: goto, type=%u, ix=%ld",
			CxTYPE(cx), (long) ix);
		SensorCall(13878);gotoprobe = PL_main_root;
		SensorCall(13879);break;
	    }
	    SensorCall(13888);if (gotoprobe) {
		SensorCall(13881);retop = dofindlabel(gotoprobe, label, label_len, label_flags,
				    enterops, enterops + GOTO_DEPTH);
		SensorCall(13883);if (retop)
		    {/*229*/SensorCall(13882);break;/*230*/}
		SensorCall(13887);if (gotoprobe->op_sibling &&
			gotoprobe->op_sibling->op_type == OP_UNSTACK &&
			gotoprobe->op_sibling->op_sibling) {
		    SensorCall(13884);retop = dofindlabel(gotoprobe->op_sibling->op_sibling,
					label, label_len, label_flags, enterops,
					enterops + GOTO_DEPTH);
		    SensorCall(13886);if (retop)
			{/*231*/SensorCall(13885);break;/*232*/}
		}
	    }
	    PL_lastgotoprobe = gotoprobe;
	}
	SensorCall(13890);if (!retop)
	    DIE(aTHX_ "Can't find label %"SVf,
                            SVfARG(newSVpvn_flags(label, label_len,
                                        SVs_TEMP | label_flags)));

	/* if we're leaving an eval, check before we pop any frames
           that we're not going to punt, otherwise the error
	   won't be caught */

	SensorCall(13894);if (leaving_eval && *enterops && enterops[1]) {
	    SensorCall(13891);I32 i;
            SensorCall(13893);for (i = 1; enterops[i]; i++)
                {/*233*/SensorCall(13892);if (enterops[i]->op_type == OP_ENTERITER)
                    DIE(aTHX_ "Can't \"goto\" into the middle of a foreach loop");/*234*/}
	}

	SensorCall(13897);if (*enterops && enterops[1]) {
	    SensorCall(13895);I32 i = enterops[1]->op_type == OP_ENTER && in_block ? 2 : 1;
	    SensorCall(13896);if (enterops[i])
		deprecate("\"goto\" to jump into a construct");
	}

	/* pop unwanted frames */

	SensorCall(13902);if (ix < cxstack_ix) {
	    SensorCall(13898);I32 oldsave;

	    SensorCall(13900);if (ix < 0)
		{/*235*/SensorCall(13899);ix = 0;/*236*/}
	    dounwind(ix);
SensorCall(13901);	    TOPBLOCK(cx);
	    oldsave = PL_scopestack[PL_scopestack_ix];
	    LEAVE_SCOPE(oldsave);
	}

	/* push wanted frames */

	SensorCall(13906);if (*enterops && enterops[1]) {
	    SensorCall(13903);OP * const oldop = PL_op;
	    ix = enterops[1]->op_type == OP_ENTER && in_block ? 2 : 1;
	    SensorCall(13905);for (; enterops[ix]; ix++) {
		PL_op = enterops[ix];
		/* Eventually we may want to stack the needed arguments
		 * for each op.  For now, we punt on the hard ones. */
		SensorCall(13904);if (PL_op->op_type == OP_ENTERITER)
		    DIE(aTHX_ "Can't \"goto\" into the middle of a foreach loop");
		PL_op->op_ppaddr(aTHX);
	    }
	    PL_op = oldop;
	}
    }

    SensorCall(13909);if (do_dump) {
#ifdef VMS
	if (!retop) retop = PL_main_start;
#endif
	PL_restartop = retop;
	SensorCall(13908);PL_do_undump = TRUE;

	my_unexec();

	PL_restartop = 0;		/* hmm, must be GNU unexec().. */
	PL_do_undump = FALSE;
    }

    PERL_ASYNC_CHECK();
    RETURNOP(retop);
}

PP(pp_exit)
{
SensorCall(13910);    dVAR;
    dSP;
    I32 anum;

    SensorCall(13915);if (MAXARG < 1)
	{/*161*/SensorCall(13911);anum = 0;/*162*/}
    else {/*163*/SensorCall(13912);if (!TOPs) {
	SensorCall(13913);anum = 0; (void)POPs;
    }
    else {
	SensorCall(13914);anum = SvIVx(POPs);
#ifdef VMS
        if (anum == 1 && (PL_op->op_private & OPpEXIT_VMSISH))
	    anum = 0;
        VMSISH_HUSHED  = VMSISH_HUSHED || (PL_op->op_private & OPpHUSH_VMSISH);
#endif
    ;/*164*/}}
    PL_exit_flags |= PERL_EXIT_EXPECTED;
#ifdef PERL_MAD
    /* KLUDGE: disable exit 0 in BEGIN blocks when we're just compiling */
    if (anum || !(PL_minus_c && PL_madskills))
	my_exit(anum);
#else
    my_exit(anum);
#endif
    PUSHs(&PL_sv_undef);
    RETURN;
}

/* Eval. */

STATIC void
S_save_lines(pTHX_ AV *array, SV *sv)
{
    SensorCall(13916);const char *s = SvPVX_const(sv);
    const char * const send = SvPVX_const(sv) + SvCUR(sv);
    I32 line = 1;

    PERL_ARGS_ASSERT_SAVE_LINES;

    SensorCall(13922);while (s && s < send) {
	SensorCall(13917);const char *t;
	SV * const tmpstr = newSV_type(SVt_PVMG);

	t = (const char *)memchr(s, '\n', send - s);
	SensorCall(13920);if (t)
	    {/*147*/SensorCall(13918);t++;/*148*/}
	else
	    {/*149*/SensorCall(13919);t = send;/*150*/}

	sv_setpvn(tmpstr, s, t - s);
	av_store(array, line++, tmpstr);
	SensorCall(13921);s = t;
    }
SensorCall(13923);}

/*
=for apidoc docatch

Check for the cases 0 or 3 of cur_env.je_ret, only used inside an eval context.

0 is used as continue inside eval,

3 is used for a die caught by an inner eval - continue inner loop

See cop.h: je_mustcatch, when set at any runlevel to TRUE, means eval ops must
establish a local jmpenv to handle exception traps.

=cut
*/
STATIC OP *
S_docatch(pTHX_ OP *o)
{
SensorCall(13924);    dVAR;
    int ret;
    OP * const oldop = PL_op;
    dJMPENV;

#ifdef DEBUGGING
    assert(CATCH_GET == TRUE);
#endif
    PL_op = o;

    JMPENV_PUSH(ret);
    SensorCall(13928);switch (ret) {
    case 0:
	assert(cxstack_ix >= 0);
	assert(CxTYPE(&cxstack[cxstack_ix]) == CXt_EVAL);
	cxstack[cxstack_ix].blk_eval.cur_top_env = PL_top_env;
 redo_body:
	CALLRUNOPS(aTHX);
	SensorCall(13925);break;
    case 3:
	/* die caught by an inner eval - continue inner loop */
	SensorCall(13926);if (PL_restartop && PL_restartjmpenv == PL_top_env) {
	    PL_restartjmpenv = NULL;
	    PL_op = PL_restartop;
	    PL_restartop = 0;
	    SensorCall(13927);goto redo_body;
	}
	/* FALL THROUGH */
    default:
	JMPENV_POP;
	PL_op = oldop;
	JMPENV_JUMP(ret);
	/* NOTREACHED */
    }
    JMPENV_POP;
    PL_op = oldop;
    {OP * ReplaceReturn1053 = NULL; SensorCall(13929); return ReplaceReturn1053;}
}

/* James Bond: Do you expect me to talk?
   Auric Goldfinger: No, Mr. Bond. I expect you to die.

   This code is an ugly hack, doesn't work with lexicals in subroutines that are
   called more than once, and is only used by regcomp.c, for (?{}) blocks.

   Currently it is not used outside the core code. Best if it stays that way.

   Hence it's now deprecated, and will be removed.
*/
OP *
Perl_sv_compile_2op(pTHX_ SV *sv, OP** startop, const char *code, PAD** padp)
/* sv Text to convert to OP tree. */
/* startop op_free() this to undo. */
/* code Short string id of the caller. */
{
    PERL_ARGS_ASSERT_SV_COMPILE_2OP;
    {OP * ReplaceReturn1052 = Perl_sv_compile_2op_is_broken(aTHX_ sv, startop, code, padp); SensorCall(13930); return ReplaceReturn1052;}
}

/* Don't use this. It will go away without warning once the regexp engine is
   refactored not to use it.  */
OP *
Perl_sv_compile_2op_is_broken(pTHX_ SV *sv, OP **startop, const char *code,
			      PAD **padp)
{
SensorCall(13931);    dVAR; dSP;				/* Make POPBLOCK work. */
    PERL_CONTEXT *cx;
    SV **newsp;
    I32 gimme = G_VOID;
    I32 optype;
    OP dummy;
    char tbuf[TYPE_DIGITS(long) + 12 + 10];
    char *tmpbuf = tbuf;
    char *safestr;
    int runtime;
    CV* runcv = NULL;	/* initialise to avoid compiler warnings */
    STRLEN len;
    bool need_catch;

    PERL_ARGS_ASSERT_SV_COMPILE_2OP_IS_BROKEN;

    ENTER_with_name("eval");
    lex_start(sv, NULL, LEX_START_SAME_FILTER);
    SAVETMPS;
    /* switch to eval mode */

    SensorCall(13932);if (IN_PERL_COMPILETIME) {
	SAVECOPSTASH_FREE(&PL_compiling);
	CopSTASH_set(&PL_compiling, PL_curstash);
    }
    SensorCall(13934);if (PERLDB_NAMEEVAL && CopLINE(PL_curcop)) {
	SensorCall(13933);SV * const sv = sv_newmortal();
	Perl_sv_setpvf(aTHX_ sv, "_<(%.10seval %lu)[%s:%"IVdf"]",
		       code, (unsigned long)++PL_evalseq,
		       CopFILE(PL_curcop), (IV)CopLINE(PL_curcop));
	tmpbuf = SvPVX(sv);
	len = SvCUR(sv);
    }
    else
	len = my_snprintf(tmpbuf, sizeof(tbuf), "_<(%.10s_eval %lu)", code,
			  (unsigned long)++PL_evalseq);
    SAVECOPFILE_FREE(&PL_compiling);
    CopFILE_set(&PL_compiling, tmpbuf+2);
    SAVECOPLINE(&PL_compiling);
    CopLINE_set(&PL_compiling, 1);
    /* XXX For C<eval "...">s within BEGIN {} blocks, this ends up
       deleting the eval's FILEGV from the stash before gv_check() runs
       (i.e. before run-time proper). To work around the coredump that
       ensues, we always turn GvMULTI_on for any globals that were
       introduced within evals. See force_ident(). GSAR 96-10-12 */
    SensorCall(13935);safestr = savepvn(tmpbuf, len);
    SAVEDELETE(PL_defstash, safestr, len);
    SAVEHINTS();
#ifdef OP_IN_REGISTER
    PL_opsave = op;
#else
    SAVEVPTR(PL_op);
#endif

    /* we get here either during compilation, or via pp_regcomp at runtime */
    runtime = IN_PERL_RUNTIME;
    SensorCall(13938);if (runtime)
    {
	SensorCall(13936);runcv = find_runcv(NULL);

	/* At run time, we have to fetch the hints from PL_curcop. */
	PL_hints = PL_curcop->cop_hints;
	SensorCall(13937);if (PL_hints & HINT_LOCALIZE_HH) {
	    /* SAVEHINTS created a new HV in PL_hintgv, which we
	       need to GC */
	    SvREFCNT_dec(GvHV(PL_hintgv));
	    GvHV(PL_hintgv) =
	     refcounted_he_chain_2hv(PL_curcop->cop_hints_hash, 0);
	    hv_magic(GvHV(PL_hintgv), NULL, PERL_MAGIC_hints);
	}
	SAVECOMPILEWARNINGS();
	PL_compiling.cop_warnings = DUP_WARNINGS(PL_curcop->cop_warnings);
	cophh_free(CopHINTHASH_get(&PL_compiling));
	/* XXX Does this need to avoid copying a label? */
	PL_compiling.cop_hints_hash
	 = cophh_copy(PL_curcop->cop_hints_hash);
    }

    PL_op = &dummy;
    PL_op->op_type = OP_ENTEREVAL;
    PL_op->op_flags = 0;			/* Avoid uninit warning. */
    PUSHBLOCK(cx, CXt_EVAL|(IN_PERL_COMPILETIME ? 0 : CXp_REAL), SP);
    PUSHEVAL(cx, 0);
    SensorCall(13939);need_catch = CATCH_GET;
    CATCH_SET(TRUE);

    SensorCall(13940);if (runtime)
	(void) doeval(G_SCALAR, startop, runcv, PL_curcop->cop_seq, NULL);
    else
	(void) doeval(G_SCALAR, startop, PL_compcv, PL_cop_seqmax, NULL);
    CATCH_SET(need_catch);
SensorCall(13941);    POPBLOCK(cx,PL_curpm);
    POPEVAL(cx);

    (*startop)->op_type = OP_NULL;
    (*startop)->op_ppaddr = PL_ppaddr[OP_NULL];
    /* XXX DAPM do this properly one year */
    *padp = MUTABLE_AV(SvREFCNT_inc_simple(PL_comppad));
    LEAVE_with_name("eval");
    SensorCall(13942);if (IN_PERL_COMPILETIME)
	CopHINTS_set(&PL_compiling, PL_hints);
#ifdef OP_IN_REGISTER
    op = PL_opsave;
#endif
    PERL_UNUSED_VAR(newsp);
    PERL_UNUSED_VAR(optype);

    {OP * ReplaceReturn1051 = PL_eval_start; SensorCall(13943); return ReplaceReturn1051;}
}


/*
=for apidoc find_runcv

Locate the CV corresponding to the currently executing sub or eval.
If db_seqp is non_null, skip CVs that are in the DB package and populate
*db_seqp with the cop sequence number at the point that the DB:: code was
entered. (allows debuggers to eval in the scope of the breakpoint rather
than in the scope of the debugger itself).

=cut
*/

CV*
Perl_find_runcv(pTHX_ U32 *db_seqp)
{
SensorCall(13944);    dVAR;
    PERL_SI	 *si;

    SensorCall(13946);if (db_seqp)
	{/*11*/SensorCall(13945);*db_seqp = PL_curcop->cop_seq;/*12*/}
    SensorCall(13958);for (si = PL_curstackinfo; si; si = si->si_prev) {
        SensorCall(13947);I32 ix;
	SensorCall(13957);for (ix = si->si_cxix; ix >= 0; ix--) {
	    SensorCall(13948);const PERL_CONTEXT *cx = &(si->si_cxstack[ix]);
	    SensorCall(13956);if (CxTYPE(cx) == CXt_SUB || CxTYPE(cx) == CXt_FORMAT) {
		SensorCall(13949);CV * const cv = cx->blk_sub.cv;
		/* skip DB:: code */
		SensorCall(13952);if (db_seqp && PL_debstash && CvSTASH(cv) == PL_debstash) {
		    SensorCall(13950);*db_seqp = cx->blk_oldcop->cop_seq;
		    SensorCall(13951);continue;
		}
		{CV * ReplaceReturn1050 = cv; SensorCall(13953); return ReplaceReturn1050;}
	    }
	    else {/*13*/SensorCall(13954);if (CxTYPE(cx) == CXt_EVAL && !CxTRYBLOCK(cx))
		{/*15*/{CV * ReplaceReturn1049 = cx->blk_eval.cv; SensorCall(13955); return ReplaceReturn1049;}/*16*/}/*14*/}
	}
    }
    {CV * ReplaceReturn1048 = PL_main_cv; SensorCall(13959); return ReplaceReturn1048;}
}


/* Run yyparse() in a setjmp wrapper. Returns:
 *   0: yyparse() successful
 *   1: yyparse() failed
 *   3: yyparse() died
 */
STATIC int
S_try_yyparse(pTHX_ int gramtype)
{
    SensorCall(13960);int ret;
    dJMPENV;

    assert(CxTYPE(&cxstack[cxstack_ix]) == CXt_EVAL);
    JMPENV_PUSH(ret);
    SensorCall(13964);switch (ret) {
    case 0:
	SensorCall(13961);ret = yyparse(gramtype) ? 1 : 0;
	SensorCall(13962);break;
    case 3:
	SensorCall(13963);break;
    default:
	JMPENV_POP;
	JMPENV_JUMP(ret);
	/* NOTREACHED */
    }
    JMPENV_POP;
    {int  ReplaceReturn1047 = ret; SensorCall(13965); return ReplaceReturn1047;}
}


/* Compile a require/do, an eval '', or a /(?{...})/.
 * In the last case, startop is non-null, and contains the address of
 * a pointer that should be set to the just-compiled code.
 * outside is the lexically enclosing CV (if any) that invoked us.
 * Returns a bool indicating whether the compile was successful; if so,
 * PL_eval_start contains the first op of the compiled ocde; otherwise,
 * pushes undef (also croaks if startop != NULL).
 */

/* This function is called from three places, sv_compile_2op, pp_require
 * and pp_entereval.  These can be distinguished as follows:
 *    sv_compile_2op - startop is non-null
 *    pp_require     - startop is null; saveop is not entereval
 *    pp_entereval   - startop is null; saveop is entereval
 */

STATIC bool
S_doeval(pTHX_ int gimme, OP** startop, CV* outside, U32 seq, HV *hh)
{
SensorCall(13966);    dVAR; dSP;
    OP * const saveop = PL_op;
    COP * const oldcurcop = PL_curcop;
    bool in_require = (saveop && saveop->op_type == OP_REQUIRE);
    int yystatus;
    CV *evalcv;

    PL_in_eval = (in_require
		  ? (EVAL_INREQUIRE | (PL_in_eval & EVAL_INEVAL))
		  : EVAL_INEVAL);

    PUSHMARK(SP);

    evalcv = MUTABLE_CV(newSV_type(SVt_PVCV));
    CvEVAL_on(evalcv);
    assert(CxTYPE(&cxstack[cxstack_ix]) == CXt_EVAL);
    cxstack[cxstack_ix].blk_eval.cv = evalcv;
    cxstack[cxstack_ix].blk_gimme = gimme;

    CvOUTSIDE_SEQ(evalcv) = seq;
    CvOUTSIDE(evalcv) = MUTABLE_CV(SvREFCNT_inc_simple(outside));

    /* set up a scratch pad */

    CvPADLIST(evalcv) = pad_new(padnew_SAVE);
    PL_op = NULL; /* avoid PL_op and PL_curpad referring to different CVs */


    SensorCall(13967);if (!PL_madskills)
	SAVEMORTALIZESV(evalcv);	/* must remain until end of current statement */

    /* make sure we compile in the right package */

    SensorCall(13968);if (CopSTASH_ne(PL_curcop, PL_curstash)) {
	SAVEGENERICSV(PL_curstash);
	PL_curstash = (HV *)SvREFCNT_inc_simple(CopSTASH(PL_curcop));
    }
    /* XXX:ajgo do we really need to alloc an AV for begin/checkunit */
    SAVESPTR(PL_beginav);
    PL_beginav = newAV();
    SAVEFREESV(PL_beginav);
    SAVESPTR(PL_unitcheckav);
    PL_unitcheckav = newAV();
    SAVEFREESV(PL_unitcheckav);

#ifdef PERL_MAD
    SAVEBOOL(PL_madskills);
    PL_madskills = 0;
#endif

    SensorCall(13969);if (!startop) ENTER_with_name("evalcomp");
    SAVESPTR(PL_compcv);
    PL_compcv = evalcv;

    /* try to compile it */

    PL_eval_root = NULL;
    PL_curcop = &PL_compiling;
    SensorCall(13970);if (saveop && (saveop->op_type != OP_REQUIRE) && (saveop->op_flags & OPf_SPECIAL))
	PL_in_eval |= EVAL_KEEPERR;
    else
	CLEAR_ERRSV();

    SensorCall(13977);if (!startop) {
SensorCall(13971);	bool clear_hints = saveop->op_type != OP_ENTEREVAL;
	SAVEHINTS();
	SensorCall(13973);if (clear_hints) {
	    PL_hints = 0;
	    hv_clear(GvHV(PL_hintgv));
	}
	else {
	    PL_hints = saveop->op_private & OPpEVAL_COPHH
			 ? oldcurcop->cop_hints : saveop->op_targ;
	    SensorCall(13972);if (hh) {
		/* SAVEHINTS created a new HV in PL_hintgv, which we need to GC */
		SvREFCNT_dec(GvHV(PL_hintgv));
		GvHV(PL_hintgv) = hh;
	    }
	}
	SAVECOMPILEWARNINGS();
	SensorCall(13976);if (clear_hints) {
	    SensorCall(13974);if (PL_dowarn & G_WARN_ALL_ON)
	        PL_compiling.cop_warnings = pWARN_ALL ;
	    else if (PL_dowarn & G_WARN_ALL_OFF)
	        PL_compiling.cop_warnings = pWARN_NONE ;
	    else
	        PL_compiling.cop_warnings = pWARN_STD ;
	}
	else {
	    PL_compiling.cop_warnings =
		DUP_WARNINGS(oldcurcop->cop_warnings);
	    cophh_free(CopHINTHASH_get(&PL_compiling));
	    SensorCall(13975);if (Perl_cop_fetch_label(aTHX_ oldcurcop, NULL, NULL)) {
		/* The label, if present, is the first entry on the chain. So rather
		   than writing a blank label in front of it (which involves an
		   allocation), just use the next entry in the chain.  */
		PL_compiling.cop_hints_hash
		    = cophh_copy(oldcurcop->cop_hints_hash->refcounted_he_next);
		/* Check the assumption that this removed the label.  */
		assert(Perl_cop_fetch_label(aTHX_ &PL_compiling, NULL, NULL) == NULL);
	    }
	    else
		PL_compiling.cop_hints_hash = cophh_copy(oldcurcop->cop_hints_hash);
	}
    }

    CALL_BLOCK_HOOKS(bhk_eval, saveop);

    /* note that yyparse() may raise an exception, e.g. C<BEGIN{die}>,
     * so honour CATCH_GET and trap it here if necessary */

    SensorCall(13978);yystatus = (!in_require && CATCH_GET) ? S_try_yyparse(aTHX_ GRAMPROG) : yyparse(GRAMPROG);

    SensorCall(13995);if (yystatus || PL_parser->error_count || !PL_eval_root) {
	SensorCall(13979);SV **newsp;			/* Used by POPBLOCK. */
	PERL_CONTEXT *cx;
	I32 optype;			/* Used by POPEVAL. */
	SV *namesv;

	cx = NULL;
	namesv = NULL;
	PERL_UNUSED_VAR(newsp);
	PERL_UNUSED_VAR(optype);

	/* note that if yystatus == 3, then the EVAL CX block has already
	 * been popped, and various vars restored */
	PL_op = saveop;
	SensorCall(13983);if (yystatus != 3) {
	    SensorCall(13980);if (PL_eval_root) {
		op_free(PL_eval_root);
		PL_eval_root = NULL;
	    }
	    SP = PL_stack_base + POPMARK;	/* pop original mark */
	    SensorCall(13982);if (!startop) {
SensorCall(13981);		POPBLOCK(cx,PL_curpm);
		POPEVAL(cx);
		namesv = cx->blk_eval.old_namesv;
	    }
	    /* POPBLOCK renders LEAVE_with_name("evalcomp") unnecessary. */
	    LEAVE_with_name("eval"); /* pp_entereval knows about this LEAVE.  */
	}

	SensorCall(13992);if (in_require) {
	    SensorCall(13984);if (!cx) {
		/* If cx is still NULL, it means that we didn't go in the
		 * POPEVAL branch. */
		SensorCall(13985);cx = &cxstack[cxstack_ix];
		assert(CxTYPE(cx) == CXt_EVAL);
		namesv = cx->blk_eval.old_namesv;
	    }
	    SensorCall(13986);(void)hv_store(GvHVn(PL_incgv),
			   SvPVX_const(namesv),
                           SvUTF8(namesv) ? -(I32)SvCUR(namesv) : (I32)SvCUR(namesv),
			   &PL_sv_undef, 0);
	    Perl_croak(aTHX_ "%"SVf"Compilation failed in require",
		       SVfARG(ERRSV
                                ? ERRSV
                                : newSVpvs_flags("Unknown error\n", SVs_TEMP)));
	}
	else {/*77*/SensorCall(13987);if (startop) {
	    SensorCall(13988);if (yystatus != 3) {
SensorCall(13989);		POPBLOCK(cx,PL_curpm);
		POPEVAL(cx);
	    }
	    SensorCall(13990);Perl_croak(aTHX_ "%"SVf"Compilation failed in regexp",
		       SVfARG(ERRSV
                                ? ERRSV
                                : newSVpvs_flags("Unknown error\n", SVs_TEMP)));
	}
	else {
	    SensorCall(13991);if (!*(SvPVx_nolen_const(ERRSV))) {
	        sv_setpvs(ERRSV, "Compilation error");
	    }
	;/*78*/}}
	SensorCall(13993);if (gimme != G_ARRAY) PUSHs(&PL_sv_undef);
	PUTBACK;
	{_Bool  ReplaceReturn1046 = FALSE; SensorCall(13994); return ReplaceReturn1046;}
    }
    else if (!startop) LEAVE_with_name("evalcomp");
    CopLINE_set(&PL_compiling, 0);
    SensorCall(13997);if (startop) {
	SensorCall(13996);*startop = PL_eval_root;
    } else
	SAVEFREEOP(PL_eval_root);

    DEBUG_x(dump_eval());

    /* Register with debugger: */
    SensorCall(14000);if (PERLDB_INTER && saveop && saveop->op_type == OP_REQUIRE) {
	SensorCall(13998);CV * const cv = get_cvs("DB::postponed", 0);
	SensorCall(13999);if (cv) {
	    dSP;
	    PUSHMARK(SP);
	    XPUSHs(MUTABLE_SV(CopFILEGV(&PL_compiling)));
	    PUTBACK;
	    call_sv(MUTABLE_SV(cv), G_DISCARD);
	}
    }

    SensorCall(14002);if (PL_unitcheckav) {
	SensorCall(14001);OP *es = PL_eval_start;
	call_list(PL_scopestack_ix, PL_unitcheckav);
	PL_eval_start = es;
    }

    /* compiled okay, so do it */

    CvDEPTH(evalcv) = 1;
    SP = PL_stack_base + POPMARK;		/* pop original mark */
    PL_op = saveop;			/* The caller may need it. */
    PL_parser->lex_state = LEX_NOTPARSING;	/* $^S needs this. */

    PUTBACK;
    {_Bool  ReplaceReturn1045 = TRUE; SensorCall(14003); return ReplaceReturn1045;}
}

STATIC PerlIO *
S_check_type_and_open(pTHX_ SV *name)
{
SensorCall(14004);    Stat_t st;
    const char *p = SvPV_nolen_const(name);
    const int st_rc = PerlLIO_stat(p, &st);

    PERL_ARGS_ASSERT_CHECK_TYPE_AND_OPEN;

    SensorCall(14006);if (st_rc < 0 || S_ISDIR(st.st_mode) || S_ISBLK(st.st_mode)) {
	{PerlIO * ReplaceReturn1044 = NULL; SensorCall(14005); return ReplaceReturn1044;}
    }

#if !defined(PERLIO_IS_STDIO) && !defined(USE_SFIO)
    {PerlIO * ReplaceReturn1043 = PerlIO_openn(aTHX_ ":", PERL_SCRIPT_MODE, -1, 0, 0, NULL, 1, &name); SensorCall(14007); return ReplaceReturn1043;}
#else
    return PerlIO_open(p, PERL_SCRIPT_MODE);
#endif
}

#ifndef PERL_DISABLE_PMC
STATIC PerlIO *
S_doopen_pm(pTHX_ SV *name)
{
    SensorCall(14008);STRLEN namelen;
    const char *p = SvPV_const(name, namelen);

    PERL_ARGS_ASSERT_DOOPEN_PM;

    SensorCall(14011);if (namelen > 3 && memEQs(p + namelen - 3, 3, ".pm")) {
	SensorCall(14009);SV *const pmcsv = sv_newmortal();
	Stat_t pmcstat;

	SvSetSV_nosteal(pmcsv,name);
	sv_catpvn(pmcsv, "c", 1);

	SensorCall(14010);if (PerlLIO_stat(SvPV_nolen_const(pmcsv), &pmcstat) >= 0)
	    return check_type_and_open(pmcsv);
    }
    {PerlIO * ReplaceReturn1042 = check_type_and_open(name); SensorCall(14012); return ReplaceReturn1042;}
}
#else
#  define doopen_pm(name) check_type_and_open(name)
#endif /* !PERL_DISABLE_PMC */

PP(pp_require)
{
SensorCall(14013);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    SV *sv;
    const char *name;
    STRLEN len;
    char * unixname;
    STRLEN unixlen;
#ifdef VMS
    int vms_unixname = 0;
#endif
    const char *tryname = NULL;
    SV *namesv = NULL;
    const I32 gimme = GIMME_V;
    int filter_has_file = 0;
    PerlIO *tryrsfp = NULL;
    SV *filter_cache = NULL;
    SV *filter_state = NULL;
    SV *filter_sub = NULL;
    SV *hook_sv = NULL;
    SV *encoding;
    OP *op;

    sv = POPs;
    SensorCall(14024);if ( (SvNIOKp(sv) || SvVOK(sv)) && PL_op->op_type != OP_DOFILE) {
	SensorCall(14014);sv = sv_2mortal(new_version(sv));
	SensorCall(14015);if (!sv_derived_from(PL_patchlevel, "version"))
	    upg_version(PL_patchlevel, TRUE);
	SensorCall(14023);if (cUNOP->op_first->op_type == OP_CONST && cUNOP->op_first->op_private & OPpCONST_NOVER) {
	    SensorCall(14016);if ( vcmp(sv,PL_patchlevel) <= 0 )
		DIE(aTHX_ "Perls since %"SVf" too modern--this is %"SVf", stopped",
		    SVfARG(sv_2mortal(vnormal(sv))),
		    SVfARG(sv_2mortal(vnormal(PL_patchlevel)))
		);
	}
	else {
	    SensorCall(14017);if ( vcmp(sv,PL_patchlevel) > 0 ) {
		SensorCall(14018);I32 first = 0;
		AV *lav;
		SV * const req = SvRV(sv);
		SV * const pv = *hv_fetchs(MUTABLE_HV(req), "original", FALSE);

		/* get the left hand term */
		lav = MUTABLE_AV(SvRV(*hv_fetchs(MUTABLE_HV(req), "version", FALSE)));

		first  = SvIV(*av_fetch(lav,0,0));
		SensorCall(14022);if (   first > (int)PERL_REVISION    /* probably 'use 6.0' */
		    || hv_exists(MUTABLE_HV(req), "qv", 2 ) /* qv style */
		    || av_len(lav) > 1               /* FP with > 3 digits */
		    || strstr(SvPVX(pv),".0")        /* FP with leading 0 */
		   ) {
		    DIE(aTHX_ "Perl %"SVf" required--this is only "
		    	"%"SVf", stopped",
			SVfARG(sv_2mortal(vnormal(req))),
			SVfARG(sv_2mortal(vnormal(PL_patchlevel)))
		    );
		}
		else { /* probably 'use 5.10' or 'use 5.8' */
		    SensorCall(14019);SV *hintsv;
		    I32 second = 0;

		    SensorCall(14020);if (av_len(lav)>=1) 
			second = SvIV(*av_fetch(lav,1,0));

		    SensorCall(14021);second /= second >= 600  ? 100 : 10;
		    hintsv = Perl_newSVpvf(aTHX_ "v%d.%d.0",
					   (int)first, (int)second);
		    upg_version(hintsv, TRUE);

		    DIE(aTHX_ "Perl %"SVf" required (did you mean %"SVf"?)"
		    	"--this is only %"SVf", stopped",
			SVfARG(sv_2mortal(vnormal(req))),
			SVfARG(sv_2mortal(vnormal(sv_2mortal(hintsv)))),
			SVfARG(sv_2mortal(vnormal(PL_patchlevel)))
		    );
		}
	    }
	}

	RETPUSHYES;
    }
    SensorCall(14025);name = SvPV_const(sv, len);
    SensorCall(14026);if (!(name && len > 0 && *name))
	DIE(aTHX_ "Null filename used");
    TAINT_PROPER("require");


#ifdef VMS
    /* The key in the %ENV hash is in the syntax of file passed as the argument
     * usually this is in UNIX format, but sometimes in VMS format, which
     * can result in a module being pulled in more than once.
     * To prevent this, the key must be stored in UNIX format if the VMS
     * name can be translated to UNIX.
     */
    if ((unixname = tounixspec(name, NULL)) != NULL) {
	unixlen = strlen(unixname);
	vms_unixname = 1;
    }
    else
#endif
    {
        /* if not VMS or VMS name can not be translated to UNIX, pass it
	 * through.
	 */
	SensorCall(14027);unixname = (char *) name;
	unixlen = len;
    }
    SensorCall(14031);if (PL_op->op_type == OP_REQUIRE) {
	SensorCall(14028);SV * const * const svp = hv_fetch(GvHVn(PL_incgv),
					  unixname, unixlen, 0);
	SensorCall(14030);if ( svp ) {
	    SensorCall(14029);if (*svp != &PL_sv_undef)
		RETPUSHYES;
	    else
		DIE(aTHX_ "Attempt to reload %s aborted.\n"
			    "Compilation failed in require", unixname);
	}
    }

    /* prepare to compile file */

    SensorCall(14033);if (path_is_absolute(name)) {
	/* At this point, name is SvPVX(sv)  */
	SensorCall(14032);tryname = name;
	tryrsfp = doopen_pm(sv);
    }
    SensorCall(14090);if (!tryrsfp) {
	SensorCall(14034);AV * const ar = GvAVn(PL_incgv);
	I32 i;
#ifdef VMS
	if (vms_unixname)
#endif
	{
	    namesv = newSV_type(SVt_PV);
	    SensorCall(14089);for (i = 0; i <= AvFILL(ar); i++) {
		SensorCall(14035);SV * const dirsv = *av_fetch(ar, i, TRUE);

		SensorCall(14036);if (SvTIED_mg((const SV *)ar, PERL_MAGIC_tied))
		    mg_get(dirsv);
		SensorCall(14088);if (SvROK(dirsv)) {
		    SensorCall(14037);int count;
		    SV **svp;
		    SV *loader = dirsv;

		    SensorCall(14039);if (SvTYPE(SvRV(loader)) == SVt_PVAV
			&& !sv_isobject(loader))
		    {
			SensorCall(14038);loader = *av_fetch(MUTABLE_AV(SvRV(loader)), 0, TRUE);
		    }

		    SensorCall(14040);Perl_sv_setpvf(aTHX_ namesv, "/loader/0x%"UVxf"/%s",
				   PTR2UV(SvRV(dirsv)), name);
		    tryname = SvPVX_const(namesv);
		    tryrsfp = NULL;

		    ENTER_with_name("call_INC");
		    SAVETMPS;
		    EXTEND(SP, 2);

		    PUSHMARK(SP);
		    PUSHs(dirsv);
		    PUSHs(sv);
		    PUTBACK;
		    SensorCall(14041);if (sv_isobject(loader))
			count = call_method("INC", G_ARRAY);
		    else
			count = call_sv(loader, G_ARRAY);
		    SPAGAIN;

		    SensorCall(14062);if (count > 0) {
			SensorCall(14042);int i = 0;
			SV *arg;

			SP -= count - 1;
			arg = SP[i++];

			SensorCall(14046);if (SvROK(arg) && (SvTYPE(SvRV(arg)) <= SVt_PVLV)
			    && !isGV_with_GP(SvRV(arg))) {
			    SensorCall(14043);filter_cache = SvRV(arg);
			    SvREFCNT_inc_simple_void_NN(filter_cache);

			    SensorCall(14045);if (i < count) {
				SensorCall(14044);arg = SP[i++];
			    }
			}

			SensorCall(14048);if (SvROK(arg) && isGV_with_GP(SvRV(arg))) {
			    SensorCall(14047);arg = SvRV(arg);
			}

			SensorCall(14055);if (isGV_with_GP(arg)) {
			    SensorCall(14049);IO * const io = GvIO((const GV *)arg);

			    ++filter_has_file;

			    SensorCall(14052);if (io) {
				SensorCall(14050);tryrsfp = IoIFP(io);
				SensorCall(14051);if (IoOFP(io) && IoOFP(io) != IoIFP(io)) {
				    PerlIO_close(IoOFP(io));
				}
				IoIFP(io) = NULL;
				IoOFP(io) = NULL;
			    }

			    SensorCall(14054);if (i < count) {
				SensorCall(14053);arg = SP[i++];
			    }
			}

			SensorCall(14059);if (SvROK(arg) && SvTYPE(SvRV(arg)) == SVt_PVCV) {
			    SensorCall(14056);filter_sub = arg;
			    SvREFCNT_inc_simple_void_NN(filter_sub);

			    SensorCall(14058);if (i < count) {
				SensorCall(14057);filter_state = SP[i];
				SvREFCNT_inc_simple_void(filter_state);
			    }
			}

			SensorCall(14061);if (!tryrsfp && (filter_cache || filter_sub)) {
			    SensorCall(14060);tryrsfp = PerlIO_open(BIT_BUCKET,
						  PERL_SCRIPT_MODE);
			}
			SP--;
		    }

		    PUTBACK;
		    FREETMPS;
		    LEAVE_with_name("call_INC");

		    /* Adjust file name if the hook has set an %INC entry.
		       This needs to happen after the FREETMPS above.  */
		    SensorCall(14063);svp = hv_fetch(GvHVn(PL_incgv), name, len, 0);
		    SensorCall(14064);if (svp)
			tryname = SvPV_nolen_const(*svp);

		    SensorCall(14067);if (tryrsfp) {
			SensorCall(14065);hook_sv = dirsv;
			SensorCall(14066);break;
		    }

		    SensorCall(14068);filter_has_file = 0;
		    SensorCall(14070);if (filter_cache) {
			SvREFCNT_dec(filter_cache);
			SensorCall(14069);filter_cache = NULL;
		    }
		    SensorCall(14072);if (filter_state) {
			SvREFCNT_dec(filter_state);
			SensorCall(14071);filter_state = NULL;
		    }
		    SensorCall(14074);if (filter_sub) {
			SvREFCNT_dec(filter_sub);
			SensorCall(14073);filter_sub = NULL;
		    }
		}
		else {
		  SensorCall(14075);if (!path_is_absolute(name)
		  ) {
		    SensorCall(14076);const char *dir;
		    STRLEN dirlen;

		    SensorCall(14079);if (SvOK(dirsv)) {
			SensorCall(14077);dir = SvPV_const(dirsv, dirlen);
		    } else {
			SensorCall(14078);dir = "";
			dirlen = 0;
		    }

#ifdef VMS
		    char *unixdir;
		    if ((unixdir = tounixpath(dir, NULL)) == NULL)
			continue;
		    sv_setpv(namesv, unixdir);
		    sv_catpv(namesv, unixname);
#else
#  ifdef __SYMBIAN32__
		    if (PL_origfilename[0] &&
			PL_origfilename[1] == ':' &&
			!(dir[0] && dir[1] == ':'))
		        Perl_sv_setpvf(aTHX_ namesv,
				       "%c:%s\\%s",
				       PL_origfilename[0],
				       dir, name);
		    else
		        Perl_sv_setpvf(aTHX_ namesv,
				       "%s\\%s",
				       dir, name);
#  else
		    /* The equivalent of		    
		       Perl_sv_setpvf(aTHX_ namesv, "%s/%s", dir, name);
		       but without the need to parse the format string, or
		       call strlen on either pointer, and with the correct
		       allocation up front.  */
		    {
			SensorCall(14080);char *tmp = SvGROW(namesv, dirlen + len + 2);

			memcpy(tmp, dir, dirlen);
			tmp +=dirlen;
			*tmp++ = '/';
			/* name came from an SV, so it will have a '\0' at the
			   end that we can copy as part of this memcpy().  */
			memcpy(tmp, name, len + 1);

			SvCUR_set(namesv, dirlen + len + 1);
			SvPOK_on(namesv);
		    }
#  endif
#endif
		    TAINT_PROPER("require");
		    tryname = SvPVX_const(namesv);
		    tryrsfp = doopen_pm(namesv);
		    SensorCall(14087);if (tryrsfp) {
			SensorCall(14081);if (tryname[0] == '.' && tryname[1] == '/') {
			    SensorCall(14082);++tryname;
			    SensorCall(14083);while (*++tryname == '/');
			}
			SensorCall(14084);break;
		    }
		    else {/*261*/SensorCall(14085);if (errno == EMFILE)
			/* no point in trying other paths if out of handles */
			{/*263*/SensorCall(14086);break;/*264*/}/*262*/}
		  }
		}
	    }
	}
    }
    sv_2mortal(namesv);
    SensorCall(14096);if (!tryrsfp) {
	SensorCall(14091);if (PL_op->op_type == OP_REQUIRE) {
	    SensorCall(14092);if(errno == EMFILE) {
		/* diag_listed_as: Can't locate %s */
		DIE(aTHX_ "Can't locate %s:   %s", name, Strerror(errno));
	    } else {
	        SensorCall(14093);if (namesv) {			/* did we lookup @INC? */
		    SensorCall(14094);AV * const ar = GvAVn(PL_incgv);
		    I32 i;
		    SV *const inc = newSVpvs_flags("", SVs_TEMP);
		    SensorCall(14095);for (i = 0; i <= AvFILL(ar); i++) {
			sv_catpvs(inc, " ");
			sv_catsv(inc, *av_fetch(ar, i, TRUE));
		    }

		    /* diag_listed_as: Can't locate %s */
		    DIE(aTHX_
			"Can't locate %s in @INC%s%s (@INC contains:%" SVf ")",
			name,
			(memEQ(name + len - 2, ".h", 3)
			 ? " (change .h to .ph maybe?) (did you run h2ph?)" : ""),
			(memEQ(name + len - 3, ".ph", 4)
			 ? " (did you run h2ph?)" : ""),
			inc
			);
		}
	    }
	    DIE(aTHX_ "Can't locate %s", name);
	}

	CLEAR_ERRSV();
	RETPUSHUNDEF;
    }
    else
	SETERRNO(0, SS_NORMAL);

    /* Assume success here to prevent recursive requirement. */
    /* name is never assigned to again, so len is still strlen(name)  */
    /* Check whether a hook in @INC has already filled %INC */
    SensorCall(14100);if (!hook_sv) {
	SensorCall(14097);(void)hv_store(GvHVn(PL_incgv),
		       unixname, unixlen, newSVpv(tryname,0),0);
    } else {
	SensorCall(14098);SV** const svp = hv_fetch(GvHVn(PL_incgv), unixname, unixlen, 0);
	SensorCall(14099);if (!svp)
	    (void)hv_store(GvHVn(PL_incgv),
			   unixname, unixlen, SvREFCNT_inc_simple(hook_sv), 0 );
    }

    ENTER_with_name("eval");
    SAVETMPS;
    SAVECOPFILE_FREE(&PL_compiling);
    CopFILE_set(&PL_compiling, tryname);
    lex_start(NULL, tryrsfp, 0);

    SensorCall(14102);if (filter_sub || filter_cache) {
	/* We can use the SvPV of the filter PVIO itself as our cache, rather
	   than hanging another SV from it. In turn, filter_add() optionally
	   takes the SV to use as the filter (or creates a new SV if passed
	   NULL), so simply pass in whatever value filter_cache has.  */
	SensorCall(14101);SV * const datasv = filter_add(S_run_user_filter, filter_cache);
	IoLINES(datasv) = filter_has_file;
	IoTOP_GV(datasv) = MUTABLE_GV(filter_state);
	IoBOTTOM_GV(datasv) = MUTABLE_GV(filter_sub);
    }

    /* switch to eval mode */
    PUSHBLOCK(cx, CXt_EVAL, SP);
    PUSHEVAL(cx, name);
    SensorCall(14103);cx->blk_eval.retop = PL_op->op_next;

    SAVECOPLINE(&PL_compiling);
    CopLINE_set(&PL_compiling, 0);

    PUTBACK;

    /* Store and reset encoding. */
    encoding = PL_encoding;
    PL_encoding = NULL;

    SensorCall(14106);if (doeval(gimme, NULL, NULL, PL_curcop->cop_seq, NULL))
	{/*265*/SensorCall(14104);op = DOCATCH(PL_eval_start);/*266*/}
    else
	{/*267*/SensorCall(14105);op = PL_op->op_next;/*268*/}

    /* Restore encoding. */
    PL_encoding = encoding;

    {OP * ReplaceReturn1041 = op; SensorCall(14107); return ReplaceReturn1041;}
}

/* This is a op added to hold the hints hash for
   pp_entereval. The hash can be modified by the code
   being eval'ed, so we return a copy instead. */

PP(pp_hintseval)
{
SensorCall(14108);    dVAR;
    dSP;
    mXPUSHs(MUTABLE_SV(hv_copy_hints_hv(MUTABLE_HV(cSVOP_sv))));
    RETURN;
}


PP(pp_entereval)
{
SensorCall(14109);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    SV *sv;
    const I32 gimme = GIMME_V;
    const U32 was = PL_breakable_sub_gen;
    char tbuf[TYPE_DIGITS(long) + 12];
    bool saved_delete = FALSE;
    char *tmpbuf = tbuf;
    STRLEN len;
    CV* runcv;
    U32 seq, lex_flags = 0;
    HV *saved_hh = NULL;
    const bool bytes = PL_op->op_private & OPpEVAL_BYTES;

    SensorCall(14113);if (PL_op->op_private & OPpEVAL_HAS_HH) {
	SensorCall(14110);saved_hh = MUTABLE_HV(SvREFCNT_inc(POPs));
    }
    else {/*153*/SensorCall(14111);if (PL_hints & HINT_LOCALIZE_HH || (
	        PL_op->op_private & OPpEVAL_COPHH
	     && PL_curcop->cop_hints & HINT_LOCALIZE_HH
	    )) {
	SensorCall(14112);saved_hh = cop_hints_2hv(PL_curcop, 0);
	hv_magic(saved_hh, NULL, PERL_MAGIC_hints);
    ;/*154*/}}
    SensorCall(14114);sv = POPs;
    SensorCall(14119);if (!SvPOK(sv)) {
	/* make sure we've got a plain PV (no overload etc) before testing
	 * for taint. Making a copy here is probably overkill, but better
	 * safe than sorry */
	SensorCall(14115);STRLEN len;
	const char * const p = SvPV_const(sv, len);

	sv = newSVpvn_flags(p, len, SVs_TEMP | SvUTF8(sv));
	lex_flags |= LEX_START_COPIED;

	SensorCall(14116);if (bytes && SvUTF8(sv))
	    SvPVbyte_force(sv, len);
    }
    else {/*155*/SensorCall(14117);if (bytes && SvUTF8(sv)) {
	/* Don't modify someone else's scalar */
	SensorCall(14118);STRLEN len;
	sv = newSVsv(sv);
	(void)sv_2mortal(sv);
	SvPVbyte_force(sv,len);
	lex_flags |= LEX_START_COPIED;
    ;/*156*/}}

    TAINT_IF(SvTAINTED(sv));
    TAINT_PROPER("eval");

    ENTER_with_name("eval");
    lex_start(sv, NULL, lex_flags | (PL_op->op_private & OPpEVAL_UNICODE
			   ? LEX_IGNORE_UTF8_HINTS
			   : bytes ? LEX_EVALBYTES : LEX_START_SAME_FILTER
			)
	     );
    SAVETMPS;

    /* switch to eval mode */

    SensorCall(14121);if (PERLDB_NAMEEVAL && CopLINE(PL_curcop)) {
	SensorCall(14120);SV * const temp_sv = sv_newmortal();
	Perl_sv_setpvf(aTHX_ temp_sv, "_<(eval %lu)[%s:%"IVdf"]",
		       (unsigned long)++PL_evalseq,
		       CopFILE(PL_curcop), (IV)CopLINE(PL_curcop));
	tmpbuf = SvPVX(temp_sv);
	len = SvCUR(temp_sv);
    }
    else
	len = my_snprintf(tmpbuf, sizeof(tbuf), "_<(eval %lu)", (unsigned long)++PL_evalseq);
    SAVECOPFILE_FREE(&PL_compiling);
    CopFILE_set(&PL_compiling, tmpbuf+2);
    SAVECOPLINE(&PL_compiling);
    CopLINE_set(&PL_compiling, 1);
    /* special case: an eval '' executed within the DB package gets lexically
     * placed in the first non-DB CV rather than the current CV - this
     * allows the debugger to execute code, find lexicals etc, in the
     * scope of the code being debugged. Passing &seq gets find_runcv
     * to do the dirty work for us */
    SensorCall(14122);runcv = find_runcv(&seq);

    PUSHBLOCK(cx, (CXt_EVAL|CXp_REAL), SP);
    PUSHEVAL(cx, 0);
    cx->blk_eval.retop = PL_op->op_next;

    /* prepare to compile string */

    SensorCall(14124);if ((PERLDB_LINE || PERLDB_SAVESRC) && PL_curstash != PL_debstash)
	save_lines(CopFILEAV(&PL_compiling), PL_parser->linestr);
    else {
	/* XXX For C<eval "...">s within BEGIN {} blocks, this ends up
	   deleting the eval's FILEGV from the stash before gv_check() runs
	   (i.e. before run-time proper). To work around the coredump that
	   ensues, we always turn GvMULTI_on for any globals that were
	   introduced within evals. See force_ident(). GSAR 96-10-12 */
	SensorCall(14123);char *const safestr = savepvn(tmpbuf, len);
	SAVEDELETE(PL_defstash, safestr, len);
	saved_delete = TRUE;
    }
    
    PUTBACK;

    SensorCall(14133);if (doeval(gimme, NULL, runcv, seq, saved_hh)) {
	SensorCall(14125);if (was != PL_breakable_sub_gen /* Some subs defined here. */
	    ? (PERLDB_LINE || PERLDB_SAVESRC)
	    :  PERLDB_SAVESRC_NOSUBS) {
	    /* Retain the filegv we created.  */
	} else {/*157*/SensorCall(14126);if (!saved_delete) {
	    SensorCall(14127);char *const safestr = savepvn(tmpbuf, len);
	    SAVEDELETE(PL_defstash, safestr, len);
	;/*158*/}}
	{OP * ReplaceReturn1040 = DOCATCH(PL_eval_start); SensorCall(14128); return ReplaceReturn1040;}
    } else {
	/* We have already left the scope set up earlier thanks to the LEAVE
	   in doeval().  */
	SensorCall(14129);if (was != PL_breakable_sub_gen /* Some subs defined here. */
	    ? (PERLDB_LINE || PERLDB_SAVESRC)
	    :  PERLDB_SAVESRC_INVALID) {
	    /* Retain the filegv we created.  */
	} else {/*159*/SensorCall(14130);if (!saved_delete) {
	    SensorCall(14131);(void)hv_delete(PL_defstash, tmpbuf, len, G_DISCARD);
	;/*160*/}}
	{OP * ReplaceReturn1039 = PL_op->op_next; SensorCall(14132); return ReplaceReturn1039;}
    }
SensorCall(14134);}

PP(pp_leaveeval)
{
SensorCall(14135);    dVAR; dSP;
    SV **newsp;
    PMOP *newpm;
    I32 gimme;
    register PERL_CONTEXT *cx;
    OP *retop;
    const U8 save_flags = PL_op -> op_flags;
    I32 optype;
    SV *namesv;
    CV *evalcv;

    PERL_ASYNC_CHECK();
    POPBLOCK(cx,newpm);
    POPEVAL(cx);
    namesv = cx->blk_eval.old_namesv;
    retop = cx->blk_eval.retop;
    evalcv = cx->blk_eval.cv;

    TAINT_NOT;
    SP = adjust_stack_on_leave((gimme == G_VOID) ? SP : newsp, SP, newsp,
				gimme, SVs_TEMP);
    PL_curpm = newpm;	/* Don't pop $1 et al till now */

#ifdef DEBUGGING
    assert(CvDEPTH(evalcv) == 1);
#endif
    CvDEPTH(evalcv) = 0;

    SensorCall(14138);if (optype == OP_REQUIRE &&
	!(gimme == G_SCALAR ? SvTRUE(*SP) : SP > newsp))
    {
	/* Unassume the success we assumed earlier. */
	SensorCall(14136);(void)hv_delete(GvHVn(PL_incgv),
			SvPVX_const(namesv),
                        SvUTF8(namesv) ? -(I32)SvCUR(namesv) : (I32)SvCUR(namesv),
			G_DISCARD);
	retop = Perl_die(aTHX_ "%"SVf" did not return a true value",
			       SVfARG(namesv));
	/* die_unwind() did LEAVE, or we won't be here */
    }
    else {
	LEAVE_with_name("eval");
	SensorCall(14137);if (!(save_flags & OPf_SPECIAL)) {
	    CLEAR_ERRSV();
	}
    }

    RETURNOP(retop);
}

/* Common code for Perl_call_sv and Perl_fold_constants, put here to keep it
   close to the related Perl_create_eval_scope.  */
void
Perl_delete_eval_scope(pTHX)
{
    SensorCall(14139);SV **newsp;
    PMOP *newpm;
    I32 gimme;
    register PERL_CONTEXT *cx;
    I32 optype;
	
    POPBLOCK(cx,newpm);
    POPEVAL(cx);
    PL_curpm = newpm;
    LEAVE_with_name("eval_scope");
    PERL_UNUSED_VAR(newsp);
    PERL_UNUSED_VAR(gimme);
    PERL_UNUSED_VAR(optype);
}

/* Common-ish code salvaged from Perl_call_sv and pp_entertry, because it was
   also needed by Perl_fold_constants.  */
PERL_CONTEXT *
Perl_create_eval_scope(pTHX_ U32 flags)
{
    SensorCall(14140);PERL_CONTEXT *cx;
    const I32 gimme = GIMME_V;
	
    ENTER_with_name("eval_scope");
    SAVETMPS;

    PUSHBLOCK(cx, (CXt_EVAL|CXp_TRYBLOCK), PL_stack_sp);
    PUSHEVAL(cx, 0);

    PL_in_eval = EVAL_INEVAL;
    SensorCall(14141);if (flags & G_KEEPERR)
	PL_in_eval |= EVAL_KEEPERR;
    else
	CLEAR_ERRSV();
    SensorCall(14142);if (flags & G_FAKINGEVAL) {
	PL_eval_root = PL_op; /* Only needed so that goto works right. */
    }
    {PERL_CONTEXT * ReplaceReturn1038 = cx; SensorCall(14143); return ReplaceReturn1038;}
}
    
PP(pp_entertry)
{
SensorCall(14144);    dVAR;
    PERL_CONTEXT * const cx = create_eval_scope(0);
    cx->blk_eval.retop = cLOGOP->op_other->op_next;
    {OP * ReplaceReturn1037 = DOCATCH(PL_op->op_next); SensorCall(14145); return ReplaceReturn1037;}
}

PP(pp_leavetry)
{
SensorCall(14146);    dVAR; dSP;
    SV **newsp;
    PMOP *newpm;
    I32 gimme;
    register PERL_CONTEXT *cx;
    I32 optype;

    PERL_ASYNC_CHECK();
    POPBLOCK(cx,newpm);
    POPEVAL(cx);
    PERL_UNUSED_VAR(optype);

    TAINT_NOT;
    SP = adjust_stack_on_leave(newsp, SP, newsp, gimme, SVs_PADTMP|SVs_TEMP);
    PL_curpm = newpm;	/* Don't pop $1 et al till now */

    LEAVE_with_name("eval_scope");
    CLEAR_ERRSV();
    RETURN;
}

PP(pp_entergiven)
{
SensorCall(14147);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    const I32 gimme = GIMME_V;
    
    ENTER_with_name("given");
    SAVETMPS;

    SAVECLEARSV(PAD_SVl(PL_op->op_targ));
    sv_setsv_mg(PAD_SV(PL_op->op_targ), POPs);

    PUSHBLOCK(cx, CXt_GIVEN, SP);
    PUSHGIVEN(cx);

    RETURN;
}

PP(pp_leavegiven)
{
SensorCall(14148);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    I32 gimme;
    SV **newsp;
    PMOP *newpm;
    PERL_UNUSED_CONTEXT;

    POPBLOCK(cx,newpm);
    assert(CxTYPE(cx) == CXt_GIVEN);

    TAINT_NOT;
    SP = adjust_stack_on_leave(newsp, SP, newsp, gimme, SVs_PADTMP|SVs_TEMP);
    PL_curpm = newpm;	/* Don't pop $1 et al till now */

    LEAVE_with_name("given");
    RETURN;
}

/* Helper routines used by pp_smartmatch */
STATIC PMOP *
S_make_matcher(pTHX_ REGEXP *re)
{
SensorCall(14149);    dVAR;
    PMOP *matcher = (PMOP *) newPMOP(OP_MATCH, OPf_WANT_SCALAR | OPf_STACKED);

    PERL_ARGS_ASSERT_MAKE_MATCHER;

    PM_SETRE(matcher, ReREFCNT_inc(re));

    SAVEFREEOP((OP *) matcher);
    ENTER_with_name("matcher"); SAVETMPS;
    SAVEOP();
    {PMOP * ReplaceReturn1036 = matcher; SensorCall(14150); return ReplaceReturn1036;}
}

STATIC bool
S_matcher_matches_sv(pTHX_ PMOP *matcher, SV *sv)
{
SensorCall(14151);    dVAR;
    dSP;

    PERL_ARGS_ASSERT_MATCHER_MATCHES_SV;
    
    PL_op = (OP *) matcher;
    XPUSHs(sv);
    PUTBACK;
    (void) Perl_pp_match(aTHX);
    SPAGAIN;
    {_Bool  ReplaceReturn1035 = (SvTRUEx(POPs)); SensorCall(14152); return ReplaceReturn1035;}
}

STATIC void
S_destroy_matcher(pTHX_ PMOP *matcher)
{
SensorCall(14153);    dVAR;

    PERL_ARGS_ASSERT_DESTROY_MATCHER;
    PERL_UNUSED_ARG(matcher);

    FREETMPS;
    LEAVE_with_name("matcher");
}

/* Do a smart match */
PP(pp_smartmatch)
{
    DEBUG_M(Perl_deb(aTHX_ "Starting smart match resolution\n"));
    {OP * ReplaceReturn1034 = do_smartmatch(NULL, NULL, 0); SensorCall(14154); return ReplaceReturn1034;}
}

/* This version of do_smartmatch() implements the
 * table of smart matches that is found in perlsyn.
 */
STATIC OP *
S_do_smartmatch(pTHX_ HV *seen_this, HV *seen_other, const bool copied)
{
SensorCall(14155);    dVAR;
    dSP;
    
    bool object_on_left = FALSE;
    SV *e = TOPs;	/* e is for 'expression' */
    SV *d = TOPm1s;	/* d is for 'default', as in PL_defgv */

    /* Take care only to invoke mg_get() once for each argument.
     * Currently we do this by copying the SV if it's magical. */
    SensorCall(14157);if (d) {
	SensorCall(14156);if (!copied && SvGMAGICAL(d))
	    d = sv_mortalcopy(d);
    }
    else
	d = &PL_sv_undef;

    assert(e);
    SensorCall(14158);if (SvGMAGICAL(e))
	e = sv_mortalcopy(e);

    /* First of all, handle overload magic of the rightmost argument */
    SensorCall(14162);if (SvAMAGIC(e)) {
	SensorCall(14159);SV * tmpsv;
	DEBUG_M(Perl_deb(aTHX_ "    applying rule Any-Object\n"));
	DEBUG_M(Perl_deb(aTHX_ "        attempting overload\n"));

	tmpsv = amagic_call(d, e, smart_amg, AMGf_noleft);
	SensorCall(14161);if (tmpsv) {
	    SPAGAIN;
	    SensorCall(14160);(void)POPs;
	    SETs(tmpsv);
	    RETURN;
	}
	DEBUG_M(Perl_deb(aTHX_ "        failed to run overload method; continuing...\n"));
    }

    SP -= 2;	/* Pop the values */


    /* ~~ undef */
    SensorCall(14164);if (!SvOK(e)) {
	DEBUG_M(Perl_deb(aTHX_ "    applying rule Any-undef\n"));
	SensorCall(14163);if (SvOK(d))
	    RETPUSHNO;
	else
	    RETPUSHYES;
    }

    SensorCall(14166);if (sv_isobject(e) && (SvTYPE(SvRV(e)) != SVt_REGEXP)) {
	DEBUG_M(Perl_deb(aTHX_ "    applying rule Any-Object\n"));
	SensorCall(14165);Perl_croak(aTHX_ "Smart matching a non-overloaded object breaks encapsulation");
    }
    SensorCall(14167);if (sv_isobject(d) && (SvTYPE(SvRV(d)) != SVt_REGEXP))
	object_on_left = TRUE;

    /* ~~ sub */
    SensorCall(14286);if (SvROK(e) && SvTYPE(SvRV(e)) == SVt_PVCV) {
	SensorCall(14168);I32 c;
	SensorCall(14190);if (object_on_left) {
	    SensorCall(14169);goto sm_any_sub; /* Treat objects like scalars */
	}
	else {/*29*/SensorCall(14170);if (SvROK(d) && SvTYPE(SvRV(d)) == SVt_PVHV) {
	    /* Test sub truth for each key */
	    SensorCall(14171);HE *he;
	    bool andedresults = TRUE;
	    HV *hv = (HV*) SvRV(d);
	    I32 numkeys = hv_iterinit(hv);
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Hash-CodeRef\n"));
	    SensorCall(14172);if (numkeys == 0)
		RETPUSHYES;
	    SensorCall(14176);while ( (he = hv_iternext(hv)) ) {
		DEBUG_M(Perl_deb(aTHX_ "        testing hash key...\n"));
		ENTER_with_name("smartmatch_hash_key_test");
		SAVETMPS;
		PUSHMARK(SP);
		PUSHs(hv_iterkeysv(he));
		PUTBACK;
		SensorCall(14173);c = call_sv(e, G_SCALAR);
		SPAGAIN;
		SensorCall(14175);if (c == 0)
		    andedresults = FALSE;
		else
		    {/*31*/SensorCall(14174);andedresults = SvTRUEx(POPs) && andedresults;/*32*/}
		FREETMPS;
		LEAVE_with_name("smartmatch_hash_key_test");
	    }
	    SensorCall(14177);if (andedresults)
		RETPUSHYES;
	    else
		RETPUSHNO;
	}
	else {/*33*/SensorCall(14178);if (SvROK(d) && SvTYPE(SvRV(d)) == SVt_PVAV) {
	    /* Test sub truth for each element */
	    SensorCall(14179);I32 i;
	    bool andedresults = TRUE;
	    AV *av = (AV*) SvRV(d);
	    const I32 len = av_len(av);
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Array-CodeRef\n"));
	    SensorCall(14180);if (len == -1)
		RETPUSHYES;
	    SensorCall(14186);for (i = 0; i <= len; ++i) {
		SensorCall(14181);SV * const * const svp = av_fetch(av, i, FALSE);
		DEBUG_M(Perl_deb(aTHX_ "        testing array element...\n"));
		ENTER_with_name("smartmatch_array_elem_test");
		SAVETMPS;
		PUSHMARK(SP);
		SensorCall(14182);if (svp)
		    PUSHs(*svp);
		PUTBACK;
		SensorCall(14183);c = call_sv(e, G_SCALAR);
		SPAGAIN;
		SensorCall(14185);if (c == 0)
		    andedresults = FALSE;
		else
		    {/*35*/SensorCall(14184);andedresults = SvTRUEx(POPs) && andedresults;/*36*/}
		FREETMPS;
		LEAVE_with_name("smartmatch_array_elem_test");
	    }
	    SensorCall(14187);if (andedresults)
		RETPUSHYES;
	    else
		RETPUSHNO;
	}
	else {
	  sm_any_sub:
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Any-CodeRef\n"));
	    ENTER_with_name("smartmatch_coderef");
	    SAVETMPS;
	    PUSHMARK(SP);
	    PUSHs(d);
	    PUTBACK;
	    SensorCall(14188);c = call_sv(e, G_SCALAR);
	    SPAGAIN;
	    SensorCall(14189);if (c == 0)
		PUSHs(&PL_sv_no);
	    else if (SvTEMP(TOPs))
		SvREFCNT_inc_void(TOPs);
	    FREETMPS;
	    LEAVE_with_name("smartmatch_coderef");
	    RETURN;
	;/*34*/}/*30*/}}
    }
    /* ~~ %hash */
    else {/*37*/SensorCall(14191);if (SvROK(e) && SvTYPE(SvRV(e)) == SVt_PVHV) {
	SensorCall(14192);if (object_on_left) {
	    SensorCall(14193);goto sm_any_hash; /* Treat objects like scalars */
	}
	else {/*39*/SensorCall(14194);if (!SvOK(d)) {
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Any-Hash ($a undef)\n"));
	    RETPUSHNO;
	}
	else {/*41*/SensorCall(14195);if (SvROK(d) && SvTYPE(SvRV(d)) == SVt_PVHV) {
	    /* Check that the key-sets are identical */
	    SensorCall(14196);HE *he;
	    HV *other_hv = MUTABLE_HV(SvRV(d));
	    bool tied = FALSE;
	    bool other_tied = FALSE;
	    U32 this_key_count  = 0,
	        other_key_count = 0;
	    HV *hv = MUTABLE_HV(SvRV(e));

	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Hash-Hash\n"));
	    /* Tied hashes don't know how many keys they have. */
	    SensorCall(14200);if (SvTIED_mg((SV*)hv, PERL_MAGIC_tied)) {
		SensorCall(14197);tied = TRUE;
	    }
	    else {/*43*/SensorCall(14198);if (SvTIED_mg((const SV *)other_hv, PERL_MAGIC_tied)) {
		SensorCall(14199);HV * const temp = other_hv;
		other_hv = hv;
		hv = temp;
		tied = TRUE;
	    ;/*44*/}}
	    SensorCall(14201);if (SvTIED_mg((const SV *)other_hv, PERL_MAGIC_tied))
		other_tied = TRUE;
	    
	    SensorCall(14202);if (!tied && HvUSEDKEYS((const HV *) hv) != HvUSEDKEYS(other_hv))
	    	RETPUSHNO;

	    /* The hashes have the same number of keys, so it suffices
	       to check that one is a subset of the other. */
	    SensorCall(14203);(void) hv_iterinit(hv);
	    SensorCall(14207);while ( (he = hv_iternext(hv)) ) {
		SensorCall(14204);SV *key = hv_iterkeysv(he);

		DEBUG_M(Perl_deb(aTHX_ "        comparing hash key...\n"));
	    	++ this_key_count;
	    	
	    	SensorCall(14206);if(!hv_exists_ent(other_hv, key, 0)) {
	    	    SensorCall(14205);(void) hv_iterinit(hv);	/* reset iterator */
		    RETPUSHNO;
	    	}
	    }
	    
	    SensorCall(14211);if (other_tied) {
		SensorCall(14208);(void) hv_iterinit(other_hv);
		SensorCall(14210);while ( hv_iternext(other_hv) )
		    {/*45*/SensorCall(14209);++other_key_count;/*46*/}
	    }
	    else
		other_key_count = HvUSEDKEYS(other_hv);
	    
	    SensorCall(14212);if (this_key_count != other_key_count)
		RETPUSHNO;
	    else
		RETPUSHYES;
	}
	else {/*47*/SensorCall(14213);if (SvROK(d) && SvTYPE(SvRV(d)) == SVt_PVAV) {
	    SensorCall(14214);AV * const other_av = MUTABLE_AV(SvRV(d));
	    const I32 other_len = av_len(other_av) + 1;
	    I32 i;
	    HV *hv = MUTABLE_HV(SvRV(e));

	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Array-Hash\n"));
	    SensorCall(14218);for (i = 0; i < other_len; ++i) {
		SensorCall(14215);SV ** const svp = av_fetch(other_av, i, FALSE);
		DEBUG_M(Perl_deb(aTHX_ "        checking for key existence...\n"));
		SensorCall(14217);if (svp) {	/* ??? When can this not happen? */
		    SensorCall(14216);if (hv_exists_ent(hv, *svp, 0))
		        RETPUSHYES;
		}
	    }
	    RETPUSHNO;
	}
	else {/*49*/SensorCall(14219);if (SvROK(d) && SvTYPE(SvRV(d)) == SVt_REGEXP) {
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Regex-Hash\n"));
	  sm_regex_hash:
	    {
		SensorCall(14220);PMOP * const matcher = make_matcher((REGEXP*) SvRV(d));
		HE *he;
		HV *hv = MUTABLE_HV(SvRV(e));

		(void) hv_iterinit(hv);
		SensorCall(14223);while ( (he = hv_iternext(hv)) ) {
		    DEBUG_M(Perl_deb(aTHX_ "        testing key against pattern...\n"));
		    SensorCall(14221);if (matcher_matches_sv(matcher, hv_iterkeysv(he))) {
			SensorCall(14222);(void) hv_iterinit(hv);
			destroy_matcher(matcher);
			RETPUSHYES;
		    }
		}
		destroy_matcher(matcher);
		RETPUSHNO;
	    }
	}
	else {
	  sm_any_hash:
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Any-Hash\n"));
	    SensorCall(14224);if (hv_exists_ent(MUTABLE_HV(SvRV(e)), d, 0))
		RETPUSHYES;
	    else
		RETPUSHNO;
	;/*50*/}/*48*/}/*42*/}/*40*/}}
    }
    /* ~~ @array */
    else {/*51*/SensorCall(14225);if (SvROK(e) && SvTYPE(SvRV(e)) == SVt_PVAV) {
	SensorCall(14226);if (object_on_left) {
	    SensorCall(14227);goto sm_any_array; /* Treat objects like scalars */
	}
	else {/*53*/SensorCall(14228);if (SvROK(d) && SvTYPE(SvRV(d)) == SVt_PVHV) {
	    SensorCall(14229);AV * const other_av = MUTABLE_AV(SvRV(e));
	    const I32 other_len = av_len(other_av) + 1;
	    I32 i;

	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Hash-Array\n"));
	    SensorCall(14233);for (i = 0; i < other_len; ++i) {
		SensorCall(14230);SV ** const svp = av_fetch(other_av, i, FALSE);

		DEBUG_M(Perl_deb(aTHX_ "        testing for key existence...\n"));
		SensorCall(14232);if (svp) {	/* ??? When can this not happen? */
		    SensorCall(14231);if (hv_exists_ent(MUTABLE_HV(SvRV(d)), *svp, 0))
		        RETPUSHYES;
		}
	    }
	    RETPUSHNO;
	;/*54*/}}
	SensorCall(14266);if (SvROK(d) && SvTYPE(SvRV(d)) == SVt_PVAV) {
	    SensorCall(14234);AV *other_av = MUTABLE_AV(SvRV(d));
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Array-Array\n"));
	    SensorCall(14248);if (av_len(MUTABLE_AV(SvRV(e))) != av_len(other_av))
		RETPUSHNO;
	    else {
	    	SensorCall(14235);I32 i;
	    	const I32 other_len = av_len(other_av);

		SensorCall(14237);if (NULL == seen_this) {
		    SensorCall(14236);seen_this = newHV();
		    (void) sv_2mortal(MUTABLE_SV(seen_this));
		}
		SensorCall(14239);if (NULL == seen_other) {
		    SensorCall(14238);seen_other = newHV();
		    (void) sv_2mortal(MUTABLE_SV(seen_other));
		}
		SensorCall(14247);for(i = 0; i <= other_len; ++i) {
		    SensorCall(14240);SV * const * const this_elem = av_fetch(MUTABLE_AV(SvRV(e)), i, FALSE);
		    SV * const * const other_elem = av_fetch(other_av, i, FALSE);

		    SensorCall(14246);if (!this_elem || !other_elem) {
			SensorCall(14241);if ((this_elem && SvOK(*this_elem))
				|| (other_elem && SvOK(*other_elem)))
			    RETPUSHNO;
		    }
		    else {/*55*/SensorCall(14242);if (hv_exists_ent(seen_this,
				sv_2mortal(newSViv(PTR2IV(*this_elem))), 0) ||
			    hv_exists_ent(seen_other,
				sv_2mortal(newSViv(PTR2IV(*other_elem))), 0))
		    {
			SensorCall(14243);if (*this_elem != *other_elem)
			    RETPUSHNO;
		    }
		    else {
			SensorCall(14244);(void)hv_store_ent(seen_this,
				sv_2mortal(newSViv(PTR2IV(*this_elem))),
				&PL_sv_undef, 0);
			(void)hv_store_ent(seen_other,
				sv_2mortal(newSViv(PTR2IV(*other_elem))),
				&PL_sv_undef, 0);
			PUSHs(*other_elem);
			PUSHs(*this_elem);
			
			PUTBACK;
			DEBUG_M(Perl_deb(aTHX_ "        recursively comparing array element...\n"));
			(void) do_smartmatch(seen_this, seen_other, 0);
			SPAGAIN;
			DEBUG_M(Perl_deb(aTHX_ "        recursion finished\n"));
			
			SensorCall(14245);if (!SvTRUEx(POPs))
			    RETPUSHNO;
		    ;/*56*/}}
		}
		RETPUSHYES;
	    }
	}
	else {/*57*/SensorCall(14249);if (SvROK(d) && SvTYPE(SvRV(d)) == SVt_REGEXP) {
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Regex-Array\n"));
	  sm_regex_array:
	    {
		SensorCall(14250);PMOP * const matcher = make_matcher((REGEXP*) SvRV(d));
		const I32 this_len = av_len(MUTABLE_AV(SvRV(e)));
		I32 i;

		SensorCall(14253);for(i = 0; i <= this_len; ++i) {
		    SensorCall(14251);SV * const * const svp = av_fetch(MUTABLE_AV(SvRV(e)), i, FALSE);
		    DEBUG_M(Perl_deb(aTHX_ "        testing element against pattern...\n"));
		    SensorCall(14252);if (svp && matcher_matches_sv(matcher, *svp)) {
			destroy_matcher(matcher);
			RETPUSHYES;
		    }
		}
		destroy_matcher(matcher);
		RETPUSHNO;
	    }
	}
	else {/*59*/SensorCall(14254);if (!SvOK(d)) {
	    /* undef ~~ array */
	    SensorCall(14255);const I32 this_len = av_len(MUTABLE_AV(SvRV(e)));
	    I32 i;

	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Undef-Array\n"));
	    SensorCall(14258);for (i = 0; i <= this_len; ++i) {
		SensorCall(14256);SV * const * const svp = av_fetch(MUTABLE_AV(SvRV(e)), i, FALSE);
		DEBUG_M(Perl_deb(aTHX_ "        testing for undef element...\n"));
		SensorCall(14257);if (!svp || !SvOK(*svp))
		    RETPUSHYES;
	    }
	    RETPUSHNO;
	}
	else {
	  sm_any_array:
	    {
		SensorCall(14259);I32 i;
		const I32 this_len = av_len(MUTABLE_AV(SvRV(e)));

		DEBUG_M(Perl_deb(aTHX_ "    applying rule Any-Array\n"));
		SensorCall(14265);for (i = 0; i <= this_len; ++i) {
		    SensorCall(14260);SV * const * const svp = av_fetch(MUTABLE_AV(SvRV(e)), i, FALSE);
		    SensorCall(14262);if (!svp)
			{/*61*/SensorCall(14261);continue;/*62*/}

		    PUSHs(d);
		    PUSHs(*svp);
		    PUTBACK;
		    /* infinite recursion isn't supposed to happen here */
		    DEBUG_M(Perl_deb(aTHX_ "        recursively testing array element...\n"));
		    SensorCall(14263);(void) do_smartmatch(NULL, NULL, 1);
		    SPAGAIN;
		    DEBUG_M(Perl_deb(aTHX_ "        recursion finished\n"));
		    SensorCall(14264);if (SvTRUEx(POPs))
			RETPUSHYES;
		}
		RETPUSHNO;
	    }
	;/*60*/}/*58*/}}
    }
    /* ~~ qr// */
    else {/*63*/SensorCall(14267);if (SvROK(e) && SvTYPE(SvRV(e)) == SVt_REGEXP) {
	SensorCall(14268);if (!object_on_left && SvROK(d) && SvTYPE(SvRV(d)) == SVt_PVHV) {
	    SensorCall(14269);SV *t = d; d = e; e = t;
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Hash-Regex\n"));
	    SensorCall(14270);goto sm_regex_hash;
	}
	else {/*65*/SensorCall(14271);if (!object_on_left && SvROK(d) && SvTYPE(SvRV(d)) == SVt_PVAV) {
	    SensorCall(14272);SV *t = d; d = e; e = t;
	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Array-Regex\n"));
	    SensorCall(14273);goto sm_regex_array;
	}
	else {
	    SensorCall(14274);PMOP * const matcher = make_matcher((REGEXP*) SvRV(e));

	    DEBUG_M(Perl_deb(aTHX_ "    applying rule Any-Regex\n"));
	    PUTBACK;
	    PUSHs(matcher_matches_sv(matcher, d)
		    ? &PL_sv_yes
		    : &PL_sv_no);
	    destroy_matcher(matcher);
	    RETURN;
	;/*66*/}}
    }
    /* ~~ scalar */
    /* See if there is overload magic on left */
    else {/*67*/SensorCall(14275);if (object_on_left && SvAMAGIC(d)) {
	SensorCall(14276);SV *tmpsv;
	DEBUG_M(Perl_deb(aTHX_ "    applying rule Object-Any\n"));
	DEBUG_M(Perl_deb(aTHX_ "        attempting overload\n"));
	PUSHs(d); PUSHs(e);
	PUTBACK;
	tmpsv = amagic_call(d, e, smart_amg, AMGf_noright);
	SensorCall(14278);if (tmpsv) {
	    SPAGAIN;
	    SensorCall(14277);(void)POPs;
	    SETs(tmpsv);
	    RETURN;
	}
	SP -= 2;
	DEBUG_M(Perl_deb(aTHX_ "        failed to run overload method; falling back...\n"));
	SensorCall(14279);goto sm_any_scalar;
    }
    else {/*69*/SensorCall(14280);if (!SvOK(d)) {
	/* undef ~~ scalar ; we already know that the scalar is SvOK */
	DEBUG_M(Perl_deb(aTHX_ "    applying rule undef-Any\n"));
	RETPUSHNO;
    }
    else
  sm_any_scalar:
    {/*71*/SensorCall(14281);if (SvNIOK(e) || (SvPOK(e) && looks_like_number(e) && SvNIOK(d))) {
	DEBUG_M(if (SvNIOK(e))
		    Perl_deb(aTHX_ "    applying rule Any-Num\n");
		else
		    Perl_deb(aTHX_ "    applying rule Num-numish\n");
	);
	/* numeric comparison */
	PUSHs(d); PUSHs(e);
	PUTBACK;
	SensorCall(14284);if (CopHINTS_get(PL_curcop) & HINT_INTEGER)
	    {/*73*/SensorCall(14282);(void) Perl_pp_i_eq(aTHX);/*74*/}
	else
	    {/*75*/SensorCall(14283);(void) Perl_pp_eq(aTHX);/*76*/}
	SPAGAIN;
	SensorCall(14285);if (SvTRUEx(POPs))
	    RETPUSHYES;
	else
	    RETPUSHNO;
    ;/*72*/}/*70*/}/*68*/}/*64*/}/*52*/}/*38*/}}
    
    /* As a last resort, use string comparison */
    DEBUG_M(Perl_deb(aTHX_ "    applying rule Any-Any\n"));
    PUSHs(d); PUSHs(e);
    PUTBACK;
    {OP * ReplaceReturn1033 = Perl_pp_seq(aTHX); SensorCall(14287); return ReplaceReturn1033;}
}

PP(pp_enterwhen)
{
SensorCall(14288);    dVAR; dSP;
    register PERL_CONTEXT *cx;
    const I32 gimme = GIMME_V;

    /* This is essentially an optimization: if the match
       fails, we don't want to push a context and then
       pop it again right away, so we skip straight
       to the op that follows the leavewhen.
       RETURNOP calls PUTBACK which restores the stack pointer after the POPs.
    */
    SensorCall(14289);if ((0 == (PL_op->op_flags & OPf_SPECIAL)) && !SvTRUEx(POPs))
	RETURNOP(cLOGOP->op_other->op_next);

    ENTER_with_name("when");
    SAVETMPS;

    PUSHBLOCK(cx, CXt_WHEN, SP);
SensorCall(14290);    PUSHWHEN(cx);

    RETURN;
}

PP(pp_leavewhen)
{
SensorCall(14291);    dVAR; dSP;
    I32 cxix;
    register PERL_CONTEXT *cx;
    I32 gimme;
    SV **newsp;
    PMOP *newpm;

    cxix = dopoptogiven(cxstack_ix);
    SensorCall(14292);if (cxix < 0)
	/* diag_listed_as: Can't "when" outside a topicalizer */
	DIE(aTHX_ "Can't \"%s\" outside a topicalizer",
	           PL_op->op_flags & OPf_SPECIAL ? "default" : "when");

SensorCall(14293);    POPBLOCK(cx,newpm);
    assert(CxTYPE(cx) == CXt_WHEN);

    TAINT_NOT;
    SP = adjust_stack_on_leave(newsp, SP, newsp, gimme, SVs_PADTMP|SVs_TEMP);
    PL_curpm = newpm;   /* pop $1 et al */

    LEAVE_with_name("when");

    SensorCall(14294);if (cxix < cxstack_ix)
        dounwind(cxix);

    SensorCall(14295);cx = &cxstack[cxix];

    SensorCall(14299);if (CxFOREACH(cx)) {
	/* clear off anything above the scope we're re-entering */
	SensorCall(14296);I32 inner = PL_scopestack_ix;

	TOPBLOCK(cx);
	SensorCall(14297);if (PL_scopestack_ix < inner)
	    leave_scope(PL_scopestack[PL_scopestack_ix]);
	PL_curcop = cx->blk_oldcop;

	PERL_ASYNC_CHECK();
	{OP * ReplaceReturn1032 = cx->blk_loop.my_op->op_nextop; SensorCall(14298); return ReplaceReturn1032;}
    }
    else {
	PERL_ASYNC_CHECK();
	RETURNOP(cx->blk_givwhen.leave_op);
    }
SensorCall(14300);}

PP(pp_continue)
{
SensorCall(14301);    dVAR; dSP;
    I32 cxix;
    register PERL_CONTEXT *cx;
    I32 gimme;
    SV **newsp;
    PMOP *newpm;

    PERL_UNUSED_VAR(gimme);
    
    cxix = dopoptowhen(cxstack_ix); 
    SensorCall(14302);if (cxix < 0)   
	DIE(aTHX_ "Can't \"continue\" outside a when block");

    SensorCall(14303);if (cxix < cxstack_ix)
        dounwind(cxix);
    
SensorCall(14304);    POPBLOCK(cx,newpm);
    assert(CxTYPE(cx) == CXt_WHEN);

    SP = newsp;
    PL_curpm = newpm;   /* pop $1 et al */

    LEAVE_with_name("when");
    RETURNOP(cx->blk_givwhen.leave_op->op_next);
}

PP(pp_break)
{
SensorCall(14305);    dVAR;   
    I32 cxix;
    register PERL_CONTEXT *cx;

    cxix = dopoptogiven(cxstack_ix); 
    SensorCall(14306);if (cxix < 0)
	DIE(aTHX_ "Can't \"break\" outside a given block");

    SensorCall(14307);cx = &cxstack[cxix];
    SensorCall(14308);if (CxFOREACH(cx))
	DIE(aTHX_ "Can't \"break\" in a loop topicalizer");

    SensorCall(14309);if (cxix < cxstack_ix)
        dounwind(cxix);

    /* Restore the sp at the time we entered the given block */
SensorCall(14310);    TOPBLOCK(cx);

    {OP * ReplaceReturn1031 = cx->blk_givwhen.leave_op; SensorCall(14311); return ReplaceReturn1031;}
}

static MAGIC *
S_doparseform(pTHX_ SV *sv)
{
    SensorCall(14312);STRLEN len;
    register char *s = SvPV(sv, len);
    register char *send;
    register char *base = NULL; /* start of current field */
    register I32 skipspaces = 0; /* number of contiguous spaces seen */
    bool noblank   = FALSE; /* ~ or ~~ seen on this line */
    bool repeat    = FALSE; /* ~~ seen on this line */
    bool postspace = FALSE; /* a text field may need right padding */
    U32 *fops;
    register U32 *fpc;
    U32 *linepc = NULL;	    /* position of last FF_LINEMARK */
    register I32 arg;
    bool ischop;	    /* it's a ^ rather than a @ */
    bool unchopnum = FALSE; /* at least one @ (i.e. non-chop) num field seen */
    int maxops = 12; /* FF_LINEMARK + FF_END + 10 (\0 without preceding \n) */
    MAGIC *mg = NULL;
    SV *sv_copy;

    PERL_ARGS_ASSERT_DOPARSEFORM;

    SensorCall(14314);if (len == 0)
	{/*97*/SensorCall(14313);Perl_croak(aTHX_ "Null picture in formline");/*98*/}

    SensorCall(14316);if (SvTYPE(sv) >= SVt_PVMG) {
	/* This might, of course, still return NULL.  */
	SensorCall(14315);mg = mg_find(sv, PERL_MAGIC_fm);
    } else {
	sv_upgrade(sv, SVt_PVMG);
    }

    SensorCall(14322);if (mg) {
	/* still the same as previously-compiled string? */
	SensorCall(14317);SV *old = mg->mg_obj;
	SensorCall(14319);if ( !(!!SvUTF8(old) ^ !!SvUTF8(sv))
	      && len == SvCUR(old)
	      && strnEQ(SvPVX(old), SvPVX(sv), len)
	) {
	    DEBUG_f(PerlIO_printf(Perl_debug_log,"Re-using compiled format\n"));
	    {MAGIC * ReplaceReturn1030 = mg; SensorCall(14318); return ReplaceReturn1030;}
	}

	DEBUG_f(PerlIO_printf(Perl_debug_log, "Re-compiling format\n"));
	Safefree(mg->mg_ptr);
	SensorCall(14320);mg->mg_ptr = NULL;
	SvREFCNT_dec(old);
	mg->mg_obj = NULL;
    }
    else {
	DEBUG_f(PerlIO_printf(Perl_debug_log, "Compiling format\n"));
	SensorCall(14321);mg = sv_magicext(sv, NULL, PERL_MAGIC_fm, &PL_vtbl_fm, NULL, 0);
    }

    SensorCall(14323);sv_copy = newSVpvn_utf8(s, len, SvUTF8(sv));
    s = SvPV(sv_copy, len); /* work on the copy, not the original */
    send = s + len;


    /* estimate the buffer size needed */
    SensorCall(14326);for (base = s; s <= send; s++) {
	SensorCall(14324);if (*s == '\n' || *s == '@' || *s == '^')
	    {/*99*/SensorCall(14325);maxops += 10;/*100*/}
    }
    SensorCall(14327);s = base;
    base = NULL;

    Newx(fops, maxops, U32);
    fpc = fops;

    SensorCall(14329);if (s < send) {
	SensorCall(14328);linepc = fpc;
	*fpc++ = FF_LINEMARK;
	noblank = repeat = FALSE;
	base = s;
    }

    SensorCall(14413);while (s <= send) {
	SensorCall(14330);switch (*s++) {
	default:
	    SensorCall(14331);skipspaces = 0;
	    SensorCall(14332);continue;

	case '~':
	    SensorCall(14333);if (*s == '~') {
		SensorCall(14334);repeat = TRUE;
		skipspaces++;
		s++;
	    }
	    SensorCall(14335);noblank = TRUE;
	    /* FALL THROUGH */
	case ' ': case '\t':
	    SensorCall(14336);skipspaces++;
	    SensorCall(14337);continue;
        case 0:
	    SensorCall(14338);if (s < send) {
	        SensorCall(14339);skipspaces = 0;
                SensorCall(14340);continue;
            } /* else FALL THROUGH */
	case '\n':
	    SensorCall(14341);arg = s - base;
	    skipspaces++;
	    arg -= skipspaces;
	    SensorCall(14344);if (arg) {
		SensorCall(14342);if (postspace)
		    *fpc++ = FF_SPACE;
		SensorCall(14343);*fpc++ = FF_LITERAL;
		*fpc++ = (U32)arg;
	    }
	    SensorCall(14345);postspace = FALSE;
	    SensorCall(14347);if (s <= send)
		{/*101*/SensorCall(14346);skipspaces--;/*102*/}
	    SensorCall(14349);if (skipspaces) {
		SensorCall(14348);*fpc++ = FF_SKIP;
		*fpc++ = (U32)skipspaces;
	    }
	    SensorCall(14350);skipspaces = 0;
	    SensorCall(14351);if (s <= send)
		*fpc++ = FF_NEWLINE;
	    SensorCall(14357);if (noblank) {
		SensorCall(14352);*fpc++ = FF_BLANK;
		SensorCall(14355);if (repeat)
		    {/*103*/SensorCall(14353);arg = fpc - linepc + 1;/*104*/}
		else
		    {/*105*/SensorCall(14354);arg = 0;/*106*/}
		SensorCall(14356);*fpc++ = (U32)arg;
	    }
	    SensorCall(14360);if (s < send) {
		SensorCall(14358);linepc = fpc;
		*fpc++ = FF_LINEMARK;
		noblank = repeat = FALSE;
		base = s;
	    }
	    else
		{/*107*/SensorCall(14359);s++;/*108*/}
	    SensorCall(14361);continue;

	case '@':
	case '^':
	    SensorCall(14362);ischop = s[-1] == '^';

	    SensorCall(14364);if (postspace) {
		SensorCall(14363);*fpc++ = FF_SPACE;
		postspace = FALSE;
	    }
	    SensorCall(14365);arg = (s - base) - 1;
	    SensorCall(14367);if (arg) {
		SensorCall(14366);*fpc++ = FF_LITERAL;
		*fpc++ = (U32)arg;
	    }

	    SensorCall(14368);base = s - 1;
	    *fpc++ = FF_FETCH;
	    SensorCall(14410);if (*s == '*') { /*  @* or ^*  */
		SensorCall(14369);s++;
		*fpc++ = 2;  /* skip the @* or ^* */
		SensorCall(14371);if (ischop) {
		    SensorCall(14370);*fpc++ = FF_LINESNGL;
		    *fpc++ = FF_CHOP;
		} else
		    *fpc++ = FF_LINEGLOB;
	    }
	    else {/*109*/SensorCall(14372);if (*s == '#' || (*s == '.' && s[1] == '#')) { /* @###, ^### */
		SensorCall(14373);arg = ischop ? FORM_NUM_BLANK : 0;
		base = s - 1;
		SensorCall(14375);while (*s == '#')
		    {/*111*/SensorCall(14374);s++;/*112*/}
		SensorCall(14380);if (*s == '.') {
                    SensorCall(14376);const char * const f = ++s;
		    SensorCall(14378);while (*s == '#')
			{/*113*/SensorCall(14377);s++;/*114*/}
		    SensorCall(14379);arg |= FORM_NUM_POINT + (s - f);
		}
		SensorCall(14381);*fpc++ = s - base;		/* fieldsize for FETCH */
		*fpc++ = FF_DECIMAL;
                *fpc++ = (U32)arg;
                unchopnum |= ! ischop;
            }
            else {/*115*/SensorCall(14382);if (*s == '0' && s[1] == '#') {  /* Zero padded decimals */
                SensorCall(14383);arg = ischop ? FORM_NUM_BLANK : 0;
		base = s - 1;
                s++;                                /* skip the '0' first */
                SensorCall(14385);while (*s == '#')
                    {/*117*/SensorCall(14384);s++;/*118*/}
                SensorCall(14390);if (*s == '.') {
                    SensorCall(14386);const char * const f = ++s;
                    SensorCall(14388);while (*s == '#')
                        {/*119*/SensorCall(14387);s++;/*120*/}
                    SensorCall(14389);arg |= FORM_NUM_POINT + (s - f);
                }
                SensorCall(14391);*fpc++ = s - base;                /* fieldsize for FETCH */
                *fpc++ = FF_0DECIMAL;
		*fpc++ = (U32)arg;
                unchopnum |= ! ischop;
	    }
	    else {				/* text field */
		SensorCall(14392);I32 prespace = 0;
		bool ismore = FALSE;

		SensorCall(14401);if (*s == '>') {
		    SensorCall(14393);while (*++s == '>') ;
		    SensorCall(14394);prespace = FF_SPACE;
		}
		else {/*123*/SensorCall(14395);if (*s == '|') {
		    SensorCall(14396);while (*++s == '|') ;
		    SensorCall(14397);prespace = FF_HALFSPACE;
		    postspace = TRUE;
		}
		else {
		    SensorCall(14398);if (*s == '<')
			{/*127*/SensorCall(14399);while (*++s == '<') ;/*128*/}
		    SensorCall(14400);postspace = TRUE;
		;/*124*/}}
		SensorCall(14403);if (*s == '.' && s[1] == '.' && s[2] == '.') {
		    SensorCall(14402);s += 3;
		    ismore = TRUE;
		}
		SensorCall(14404);*fpc++ = s - base;		/* fieldsize for FETCH */

		*fpc++ = ischop ? FF_CHECKCHOP : FF_CHECKNL;

		SensorCall(14406);if (prespace)
		    {/*131*/SensorCall(14405);*fpc++ = (U32)prespace;/*132*/} /* add SPACE or HALFSPACE */
		SensorCall(14407);*fpc++ = FF_ITEM;
		SensorCall(14408);if (ismore)
		    *fpc++ = FF_MORE;
		SensorCall(14409);if (ischop)
		    *fpc++ = FF_CHOP;
	    ;/*116*/}/*110*/}}
	    SensorCall(14411);base = s;
	    skipspaces = 0;
	    SensorCall(14412);continue;
	}
    }
    SensorCall(14414);*fpc++ = FF_END;

    assert (fpc <= fops + maxops); /* ensure our buffer estimate was valid */
    arg = fpc - fops;

    mg->mg_ptr = (char *) fops;
    mg->mg_len = arg * sizeof(U32);
    mg->mg_obj = sv_copy;
    mg->mg_flags |= MGf_REFCOUNTED;

    SensorCall(14416);if (unchopnum && repeat)
        {/*133*/SensorCall(14415);Perl_die(aTHX_ "Repeated format line will never terminate (~~ and @#)");/*134*/}

    {MAGIC * ReplaceReturn1029 = mg; SensorCall(14417); return ReplaceReturn1029;}
}


STATIC bool
S_num_overflow(NV value, I32 fldsize, I32 frcsize)
{
    /* Can value be printed in fldsize chars, using %*.*f ? */
    SensorCall(14418);NV pwr = 1;
    NV eps = 0.5;
    bool res = FALSE;
    int intsize = fldsize - (value < 0 ? 1 : 0);

    SensorCall(14420);if (frcsize & FORM_NUM_POINT)
        {/*139*/SensorCall(14419);intsize--;/*140*/}
    SensorCall(14421);frcsize &= ~(FORM_NUM_POINT|FORM_NUM_BLANK);
    intsize -= frcsize;

    SensorCall(14423);while (intsize--) {/*141*/SensorCall(14422);pwr *= 10.0;/*142*/}
    SensorCall(14425);while (frcsize--) {/*143*/SensorCall(14424);eps /= 10.0;/*144*/}

    SensorCall(14428);if( value >= 0 ){
        SensorCall(14426);if (value + eps >= pwr)
	    res = TRUE;
    } else {
        SensorCall(14427);if (value - eps <= -pwr)
	    res = TRUE;
    }
    {_Bool  ReplaceReturn1028 = res; SensorCall(14429); return ReplaceReturn1028;}
}

static I32
S_run_user_filter(pTHX_ int idx, SV *buf_sv, int maxlen)
{
SensorCall(14430);    dVAR;
    SV * const datasv = FILTER_DATA(idx);
    const int filter_has_file = IoLINES(datasv);
    SV * const filter_state = MUTABLE_SV(IoTOP_GV(datasv));
    SV * const filter_sub = MUTABLE_SV(IoBOTTOM_GV(datasv));
    int status = 0;
    SV *upstream;
    STRLEN got_len;
    char *got_p = NULL;
    char *prune_from = NULL;
    bool read_from_cache = FALSE;
    STRLEN umaxlen;

    PERL_ARGS_ASSERT_RUN_USER_FILTER;

    assert(maxlen >= 0);
    umaxlen = maxlen;

    /* I was having segfault trouble under Linux 2.2.5 after a
       parse error occured.  (Had to hack around it with a test
       for PL_parser->error_count == 0.)  Solaris doesn't segfault --
       not sure where the trouble is yet.  XXX */

    {
	SV *const cache = datasv;
	SensorCall(14443);if (SvOK(cache)) {
	    SensorCall(14431);STRLEN cache_len;
	    const char *cache_p = SvPV(cache, cache_len);
	    STRLEN take = 0;

	    SensorCall(14437);if (umaxlen) {
		/* Running in block mode and we have some cached data already.
		 */
		SensorCall(14432);if (cache_len >= umaxlen) {
		    /* In fact, so much data we don't even need to call
		       filter_read.  */
		    SensorCall(14433);take = umaxlen;
		}
	    } else {
		SensorCall(14434);const char *const first_nl =
		    (const char *)memchr(cache_p, '\n', cache_len);
		SensorCall(14436);if (first_nl) {
		    SensorCall(14435);take = first_nl + 1 - cache_p;
		}
	    }
	    SensorCall(14439);if (take) {
		sv_catpvn(buf_sv, cache_p, take);
		sv_chop(cache, cache_p + take);
		/* Definitely not EOF  */
		{I32  ReplaceReturn1027 = 1; SensorCall(14438); return ReplaceReturn1027;}
	    }

	    sv_catsv(buf_sv, cache);
	    SensorCall(14441);if (umaxlen) {
		SensorCall(14440);umaxlen -= cache_len;
	    }
	    SvOK_off(cache);
	    SensorCall(14442);read_from_cache = TRUE;
	}
    }

    /* Filter API says that the filter appends to the contents of the buffer.
       Usually the buffer is "", so the details don't matter. But if it's not,
       then clearly what it contains is already filtered by this filter, so we
       don't want to pass it in a second time.
       I'm going to use a mortal in case the upstream filter croaks.  */
    SensorCall(14444);upstream = ((SvOK(buf_sv) && sv_len(buf_sv)) || SvGMAGICAL(buf_sv))
	? sv_newmortal() : buf_sv;
    SvUPGRADE(upstream, SVt_PV);
	
    SensorCall(14446);if (filter_has_file) {
	SensorCall(14445);status = FILTER_READ(idx+1, upstream, 0);
    }

    SensorCall(14454);if (filter_sub && status >= 0) {
	dSP;
	SensorCall(14447);int count;

	ENTER_with_name("call_filter_sub");
	SAVE_DEFSV;
	SAVETMPS;
	EXTEND(SP, 2);

	DEFSV_set(upstream);
	PUSHMARK(SP);
	mPUSHi(0);
	SensorCall(14448);if (filter_state) {
	    PUSHs(filter_state);
	}
	PUTBACK;
	SensorCall(14449);count = call_sv(filter_sub, G_SCALAR);
	SPAGAIN;

	SensorCall(14453);if (count > 0) {
	    SensorCall(14450);SV *out = POPs;
	    SensorCall(14452);if (SvOK(out)) {
		SensorCall(14451);status = SvIV(out);
	    }
	}

	PUTBACK;
	FREETMPS;
	LEAVE_with_name("call_filter_sub");
    }

    SensorCall(14462);if(SvOK(upstream)) {
	SensorCall(14455);got_p = SvPV(upstream, got_len);
	SensorCall(14461);if (umaxlen) {
	    SensorCall(14456);if (got_len > umaxlen) {
		SensorCall(14457);prune_from = got_p + umaxlen;
	    }
	} else {
	    SensorCall(14458);char *const first_nl = (char *)memchr(got_p, '\n', got_len);
	    SensorCall(14460);if (first_nl && first_nl + 1 < got_p + got_len) {
		/* There's a second line here... */
		SensorCall(14459);prune_from = first_nl + 1;
	    }
	}
    }
    SensorCall(14469);if (prune_from) {
	/* Oh. Too long. Stuff some in our cache.  */
	SensorCall(14463);STRLEN cached_len = got_p + got_len - prune_from;
	SV *const cache = datasv;

	SensorCall(14464);if (SvOK(cache)) {
	    /* Cache should be empty.  */
	    assert(!SvCUR(cache));
	}

	sv_setpvn(cache, prune_from, cached_len);
	/* If you ask for block mode, you may well split UTF-8 characters.
	   "If it breaks, you get to keep both parts"
	   (Your code is broken if you  don't put them back together again
	   before something notices.) */
	SensorCall(14465);if (SvUTF8(upstream)) {
	    SvUTF8_on(cache);
	}
	SvCUR_set(upstream, got_len - cached_len);
	SensorCall(14466);*prune_from = 0;
	/* Can't yet be EOF  */
	SensorCall(14468);if (status == 0)
	    {/*145*/SensorCall(14467);status = 1;/*146*/}
    }

    /* If they are at EOF but buf_sv has something in it, then they may never
       have touched the SV upstream, so it may be undefined.  If we naively
       concatenate it then we get a warning about use of uninitialised value.
    */
    SensorCall(14470);if (upstream != buf_sv && (SvOK(upstream) || SvGMAGICAL(upstream))) {
	sv_catsv(buf_sv, upstream);
    }

    SensorCall(14473);if (status <= 0) {
	IoLINES(datasv) = 0;
	SensorCall(14471);if (filter_state) {
	    SvREFCNT_dec(filter_state);
	    IoTOP_GV(datasv) = NULL;
	}
	SensorCall(14472);if (filter_sub) {
	    SvREFCNT_dec(filter_sub);
	    IoBOTTOM_GV(datasv) = NULL;
	}
	filter_del(S_run_user_filter);
    }
    SensorCall(14475);if (status == 0 && read_from_cache) {
	/* If we read some data from the cache (and by getting here it implies
	   that we emptied the cache) then we aren't yet at EOF, and mustn't
	   report that to our caller.  */
	{I32  ReplaceReturn1026 = 1; SensorCall(14474); return ReplaceReturn1026;}
    }
    {I32  ReplaceReturn1025 = status; SensorCall(14476); return ReplaceReturn1025;}
}

/* perhaps someone can come up with a better name for
   this?  it is not really "absolute", per se ... */
static bool
S_path_is_absolute(const char *name)
{
    PERL_ARGS_ASSERT_PATH_IS_ABSOLUTE;

    SensorCall(14477);if (PERL_FILE_IS_ABSOLUTE(name)
#ifdef WIN32
	|| (*name == '.' && ((name[1] == '/' ||
			     (name[1] == '.' && name[2] == '/'))
			 || (name[1] == '\\' ||
			     ( name[1] == '.' && name[2] == '\\')))
	    )
#else
	|| (*name == '.' && (name[1] == '/' ||
			     (name[1] == '.' && name[2] == '/')))
#endif
	 )
    {
	{_Bool  ReplaceReturn1024 = TRUE; SensorCall(14478); return ReplaceReturn1024;}
    }
    else
    	return FALSE;
SensorCall(14479);}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
