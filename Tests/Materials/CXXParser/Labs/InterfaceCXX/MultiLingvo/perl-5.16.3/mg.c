#include "var/tmp/sensor.h"
/*    mg.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 *  Sam sat on the ground and put his head in his hands.  'I wish I had never
 *  come here, and I don't want to see no more magic,' he said, and fell silent.
 *
 *     [p.363 of _The Lord of the Rings_, II/vii: "The Mirror of Galadriel"]
 */

/*
=head1 Magical Functions

"Magic" is special data attached to SV structures in order to give them
"magical" properties.  When any Perl code tries to read from, or assign to,
an SV marked as magical, it calls the 'get' or 'set' function associated
with that SV's magic. A get is called prior to reading an SV, in order to
give it a chance to update its internal value (get on $. writes the line
number of the last read filehandle into to the SV's IV slot), while
set is called after an SV has been written to, in order to allow it to make
use of its changed value (set on $/ copies the SV's new value to the
PL_rs global variable).

Magic is implemented as a linked list of MAGIC structures attached to the
SV. Each MAGIC struct holds the type of the magic, a pointer to an array
of functions that implement the get(), set(), length() etc functions,
plus space for some flags and pointers. For example, a tied variable has
a MAGIC structure that contains a pointer to the object associated with the
tie.

*/

#include "EXTERN.h"
#define PERL_IN_MG_C
#include "perl.h"

#if defined(HAS_GETGROUPS) || defined(HAS_SETGROUPS)
#  ifdef I_GRP
#    include <grp.h>
#  endif
#endif

#if defined(HAS_SETGROUPS)
#  ifndef NGROUPS
#    define NGROUPS 32
#  endif
#endif

#ifdef __hpux
#  include <sys/pstat.h>
#endif

#ifdef HAS_PRCTL_SET_NAME
#  include <sys/prctl.h>
#endif

#if defined(HAS_SIGACTION) && defined(SA_SIGINFO)
Signal_t Perl_csighandler(int sig, siginfo_t *, void *);
#else
Signal_t Perl_csighandler(int sig);
#endif

#ifdef __Lynx__
/* Missing protos on LynxOS */
void setruid(uid_t id);
void seteuid(uid_t id);
void setrgid(uid_t id);
void setegid(uid_t id);
#endif

/*
 * Use the "DESTRUCTOR" scope cleanup to reinstate magic.
 */

struct magic_state {
    SV* mgs_sv;
    I32 mgs_ss_ix;
    U32 mgs_magical;
    bool mgs_readonly;
    bool mgs_bumped;
};
/* MGS is typedef'ed to struct magic_state in perl.h */

STATIC void
S_save_magic(pTHX_ I32 mgs_ix, SV *sv)
{
SensorCall(4917);    dVAR;
    MGS* mgs;
    bool bumped = FALSE;

    PERL_ARGS_ASSERT_SAVE_MAGIC;

    /* we shouldn't really be called here with RC==0, but it can sometimes
     * happen via mg_clear() (which also shouldn't be called when RC==0,
     * but it can happen). Handle this case gracefully(ish) by not RC++
     * and thus avoiding the resultant double free */
    SensorCall(4919);if (SvREFCNT(sv) > 0) {
    /* guard against sv getting freed midway through the mg clearing,
     * by holding a private reference for the duration. */
	SvREFCNT_inc_simple_void_NN(sv);
	SensorCall(4918);bumped = TRUE;
    }

    assert(SvMAGICAL(sv));
    /* Turning READONLY off for a copy-on-write scalar (including shared
       hash keys) is a bad idea.  */
    SensorCall(4920);if (SvIsCOW(sv))
      sv_force_normal_flags(sv, 0);

    SAVEDESTRUCTOR_X(S_restore_magic, INT2PTR(void*, (IV)mgs_ix));

    SensorCall(4921);mgs = SSPTR(mgs_ix, MGS*);
    mgs->mgs_sv = sv;
    mgs->mgs_magical = SvMAGICAL(sv);
    mgs->mgs_readonly = SvREADONLY(sv) != 0;
    mgs->mgs_ss_ix = PL_savestack_ix;   /* points after the saved destructor */
    mgs->mgs_bumped = bumped;

    SvMAGICAL_off(sv);
    SvREADONLY_off(sv);
    SensorCall(4922);if (!(SvFLAGS(sv) & (SVf_IOK|SVf_NOK|SVf_POK))) {
	/* No public flags are set, so promote any private flags to public.  */
	SvFLAGS(sv) |= (SvFLAGS(sv) & (SVp_IOK|SVp_NOK|SVp_POK)) >> PRIVSHIFT;
    }
SensorCall(4923);}

/*
=for apidoc mg_magical

Turns on the magical status of an SV.  See C<sv_magic>.

=cut
*/

void
Perl_mg_magical(pTHX_ SV *sv)
{
    SensorCall(4924);const MAGIC* mg;
    PERL_ARGS_ASSERT_MG_MAGICAL;
    PERL_UNUSED_CONTEXT;

    SvMAGICAL_off(sv);
    SensorCall(4932);if ((mg = SvMAGIC(sv))) {
	SensorCall(4925);do {
	    SensorCall(4926);const MGVTBL* const vtbl = mg->mg_virtual;
	    SensorCall(4930);if (vtbl) {
		SensorCall(4927);if (vtbl->svt_get && !(mg->mg_flags & MGf_GSKIP))
		    SvGMAGICAL_on(sv);
		SensorCall(4928);if (vtbl->svt_set)
		    SvSMAGICAL_on(sv);
		SensorCall(4929);if (vtbl->svt_clear)
		    SvRMAGICAL_on(sv);
	    }
	} while ((mg = mg->mg_moremagic));
	SensorCall(4931);if (!(SvFLAGS(sv) & (SVs_GMG|SVs_SMG)))
	    SvRMAGICAL_on(sv);
    }
SensorCall(4933);}

/*
=for apidoc mg_get

Do magic before a value is retrieved from the SV.  See C<sv_magic>.

=cut
*/

int
Perl_mg_get(pTHX_ SV *sv)
{
SensorCall(4934);    dVAR;
    const I32 mgs_ix = SSNEW(sizeof(MGS));
    bool have_new = 0;
    MAGIC *newmg, *head, *cur, *mg;

    PERL_ARGS_ASSERT_MG_GET;

    SensorCall(4936);if (PL_localizing == 1 && sv == DEFSV) {/*127*/{int  ReplaceReturn2082 = 0; SensorCall(4935); return ReplaceReturn2082;}/*128*/}

    save_magic(mgs_ix, sv);

    /* We must call svt_get(sv, mg) for each valid entry in the linked
       list of magic. svt_get() may delete the current entry, add new
       magic to the head of the list, or upgrade the SV. AMS 20010810 */

    SensorCall(4937);newmg = cur = head = mg = SvMAGIC(sv);
    SensorCall(4952);while (mg) {
	SensorCall(4938);const MGVTBL * const vtbl = mg->mg_virtual;
	MAGIC * const nextmg = mg->mg_moremagic;	/* it may delete itself */

	SensorCall(4945);if (!(mg->mg_flags & MGf_GSKIP) && vtbl && vtbl->svt_get) {
	    SensorCall(4939);vtbl->svt_get(aTHX_ sv, mg);

	    /* guard against magic having been deleted - eg FETCH calling
	     * untie */
	    SensorCall(4942);if (!SvMAGIC(sv)) {
		SensorCall(4940);(SSPTR(mgs_ix, MGS *))->mgs_magical = 0; /* recalculate flags */
		SensorCall(4941);break;
	    }

	    /* recalculate flags if this entry was deleted. */
	    SensorCall(4944);if (mg->mg_flags & MGf_GSKIP)
		{/*129*/SensorCall(4943);(SSPTR(mgs_ix, MGS *))->mgs_magical = 0;/*130*/}
	}

	SensorCall(4946);mg = nextmg;

	SensorCall(4949);if (have_new) {
	    /* Have we finished with the new entries we saw? Start again
	       where we left off (unless there are more new entries). */
	    SensorCall(4947);if (mg == head) {
		SensorCall(4948);have_new = 0;
		mg   = cur;
		head = newmg;
	    }
	}

	/* Were any new entries added? */
	SensorCall(4951);if (!have_new && (newmg = SvMAGIC(sv)) != head) {
	    SensorCall(4950);have_new = 1;
	    cur = mg;
	    mg  = newmg;
	    (SSPTR(mgs_ix, MGS *))->mgs_magical = 0; /* recalculate flags */
	}
    }

    restore_magic(INT2PTR(void *, (IV)mgs_ix));
    {int  ReplaceReturn2081 = 0; SensorCall(4953); return ReplaceReturn2081;}
}

/*
=for apidoc mg_set

Do magic after a value is assigned to the SV.  See C<sv_magic>.

=cut
*/

int
Perl_mg_set(pTHX_ SV *sv)
{
SensorCall(4954);    dVAR;
    const I32 mgs_ix = SSNEW(sizeof(MGS));
    MAGIC* mg;
    MAGIC* nextmg;

    PERL_ARGS_ASSERT_MG_SET;

    SensorCall(4956);if (PL_localizing == 2 && sv == DEFSV) {/*137*/{int  ReplaceReturn2080 = 0; SensorCall(4955); return ReplaceReturn2080;}/*138*/}

    save_magic(mgs_ix, sv);

    SensorCall(4964);for (mg = SvMAGIC(sv); mg; mg = nextmg) {
        SensorCall(4957);const MGVTBL* vtbl = mg->mg_virtual;
	nextmg = mg->mg_moremagic;	/* it may delete itself */
	SensorCall(4959);if (mg->mg_flags & MGf_GSKIP) {
	    SensorCall(4958);mg->mg_flags &= ~MGf_GSKIP;	/* setting requires another read */
	    (SSPTR(mgs_ix, MGS*))->mgs_magical = 0;
	}
	SensorCall(4961);if (PL_localizing == 2
	    && PERL_MAGIC_TYPE_IS_VALUE_MAGIC(mg->mg_type))
	    {/*139*/SensorCall(4960);continue;/*140*/}
	SensorCall(4963);if (vtbl && vtbl->svt_set)
	    {/*141*/SensorCall(4962);vtbl->svt_set(aTHX_ sv, mg);/*142*/}
    }

    restore_magic(INT2PTR(void*, (IV)mgs_ix));
    {int  ReplaceReturn2079 = 0; SensorCall(4965); return ReplaceReturn2079;}
}

/*
=for apidoc mg_length

Report on the SV's length.  See C<sv_magic>.

=cut
*/

U32
Perl_mg_length(pTHX_ SV *sv)
{
SensorCall(4966);    dVAR;
    MAGIC* mg;
    STRLEN len;

    PERL_ARGS_ASSERT_MG_LENGTH;

    SensorCall(4971);for (mg = SvMAGIC(sv); mg; mg = mg->mg_moremagic) {
        SensorCall(4967);const MGVTBL * const vtbl = mg->mg_virtual;
	SensorCall(4970);if (vtbl && vtbl->svt_len) {
            SensorCall(4968);const I32 mgs_ix = SSNEW(sizeof(MGS));
	    save_magic(mgs_ix, sv);
	    /* omit MGf_GSKIP -- not changed here */
	    len = vtbl->svt_len(aTHX_ sv, mg);
	    restore_magic(INT2PTR(void*, (IV)mgs_ix));
	    {U32  ReplaceReturn2078 = len; SensorCall(4969); return ReplaceReturn2078;}
	}
    }

    {
	/* You can't know whether it's UTF-8 until you get the string again...
	 */
        SensorCall(4972);const U8 *s = (U8*)SvPV_const(sv, len);

	SensorCall(4974);if (DO_UTF8(sv)) {
	    SensorCall(4973);len = utf8_length(s, s + len);
	}
    }
    {U32  ReplaceReturn2077 = len; SensorCall(4975); return ReplaceReturn2077;}
}

I32
Perl_mg_size(pTHX_ SV *sv)
{
    SensorCall(4976);MAGIC* mg;

    PERL_ARGS_ASSERT_MG_SIZE;

    SensorCall(4981);for (mg = SvMAGIC(sv); mg; mg = mg->mg_moremagic) {
        SensorCall(4977);const MGVTBL* const vtbl = mg->mg_virtual;
	SensorCall(4980);if (vtbl && vtbl->svt_len) {
            SensorCall(4978);const I32 mgs_ix = SSNEW(sizeof(MGS));
            I32 len;
	    save_magic(mgs_ix, sv);
	    /* omit MGf_GSKIP -- not changed here */
	    len = vtbl->svt_len(aTHX_ sv, mg);
	    restore_magic(INT2PTR(void*, (IV)mgs_ix));
	    {I32  ReplaceReturn2076 = len; SensorCall(4979); return ReplaceReturn2076;}
	}
    }

    SensorCall(4985);switch(SvTYPE(sv)) {
	case SVt_PVAV:
	    {I32  ReplaceReturn2075 = AvFILLp((const AV *) sv); SensorCall(4982); return ReplaceReturn2075;} /* Fallback to non-tied array */
	case SVt_PVHV:
	    /* FIXME */
	default:
	    SensorCall(4983);Perl_croak(aTHX_ "Size magic not implemented");
	    SensorCall(4984);break;
    }
    {I32  ReplaceReturn2074 = 0; SensorCall(4986); return ReplaceReturn2074;}
}

/*
=for apidoc mg_clear

Clear something magical that the SV represents.  See C<sv_magic>.

=cut
*/

int
Perl_mg_clear(pTHX_ SV *sv)
{
    SensorCall(4987);const I32 mgs_ix = SSNEW(sizeof(MGS));
    MAGIC* mg;
    MAGIC *nextmg;

    PERL_ARGS_ASSERT_MG_CLEAR;

    save_magic(mgs_ix, sv);

    SensorCall(4991);for (mg = SvMAGIC(sv); mg; mg = nextmg) {
        SensorCall(4988);const MGVTBL* const vtbl = mg->mg_virtual;
	/* omit GSKIP -- never set here */

	nextmg = mg->mg_moremagic; /* it may delete itself */

	SensorCall(4990);if (vtbl && vtbl->svt_clear)
	    {/*125*/SensorCall(4989);vtbl->svt_clear(aTHX_ sv, mg);/*126*/}
    }

    restore_magic(INT2PTR(void*, (IV)mgs_ix));
    {int  ReplaceReturn2073 = 0; SensorCall(4992); return ReplaceReturn2073;}
}

static MAGIC*
S_mg_findext_flags(pTHX_ const SV *sv, int type, const MGVTBL *vtbl, U32 flags)
{
SensorCall(4993);    PERL_UNUSED_CONTEXT;

    assert(flags <= 1);

    SensorCall(4998);if (sv) {
	SensorCall(4994);MAGIC *mg;

	SensorCall(4997);for (mg = SvMAGIC(sv); mg; mg = mg->mg_moremagic) {
	    SensorCall(4995);if (mg->mg_type == type && (!flags || mg->mg_virtual == vtbl)) {
		{MAGIC * ReplaceReturn2072 = mg; SensorCall(4996); return ReplaceReturn2072;}
	    }
	}
    }

    {MAGIC * ReplaceReturn2071 = NULL; SensorCall(4999); return ReplaceReturn2071;}
}

/*
=for apidoc mg_find

Finds the magic pointer for type matching the SV.  See C<sv_magic>.

=cut
*/

MAGIC*
Perl_mg_find(pTHX_ const SV *sv, int type)
{
    {MAGIC * ReplaceReturn2070 = S_mg_findext_flags(aTHX_ sv, type, NULL, 0); SensorCall(5000); return ReplaceReturn2070;}
}

/*
=for apidoc mg_findext

Finds the magic pointer of C<type> with the given C<vtbl> for the C<SV>.  See
C<sv_magicext>.

=cut
*/

MAGIC*
Perl_mg_findext(pTHX_ const SV *sv, int type, const MGVTBL *vtbl)
{
    {MAGIC * ReplaceReturn2069 = S_mg_findext_flags(aTHX_ sv, type, vtbl, 1); SensorCall(5001); return ReplaceReturn2069;}
}

/*
=for apidoc mg_copy

Copies the magic from one SV to another.  See C<sv_magic>.

=cut
*/

int
Perl_mg_copy(pTHX_ SV *sv, SV *nsv, const char *key, I32 klen)
{
    SensorCall(5002);int count = 0;
    MAGIC* mg;

    PERL_ARGS_ASSERT_MG_COPY;

    SensorCall(5009);for (mg = SvMAGIC(sv); mg; mg = mg->mg_moremagic) {
        SensorCall(5003);const MGVTBL* const vtbl = mg->mg_virtual;
	SensorCall(5008);if ((mg->mg_flags & MGf_COPY) && vtbl->svt_copy){
	    SensorCall(5004);count += vtbl->svt_copy(aTHX_ sv, mg, nsv, key, klen);
	}
	else {
	    SensorCall(5005);const char type = mg->mg_type;
	    SensorCall(5007);if (isUPPER(type) && type != PERL_MAGIC_uvar) {
		sv_magic(nsv,
		     (type == PERL_MAGIC_tied)
			? SvTIED_obj(sv, mg)
			: (type == PERL_MAGIC_regdata && mg->mg_obj)
			    ? sv
			    : mg->mg_obj,
		     toLOWER(type), key, klen);
		SensorCall(5006);count++;
	    }
	}
    }
    {int  ReplaceReturn2068 = count; SensorCall(5010); return ReplaceReturn2068;}
}

/*
=for apidoc mg_localize

Copy some of the magic from an existing SV to new localized version of that
SV. Container magic (eg %ENV, $1, tie) gets copied, value magic doesn't (eg
taint, pos).

If setmagic is false then no set magic will be called on the new (empty) SV.
This typically means that assignment will soon follow (e.g. 'local $x = $y'),
and that will handle the magic.

=cut
*/

void
Perl_mg_localize(pTHX_ SV *sv, SV *nsv, bool setmagic)
{
SensorCall(5011);    dVAR;
    MAGIC *mg;

    PERL_ARGS_ASSERT_MG_LOCALIZE;

    SensorCall(5013);if (nsv == DEFSV)
	{/*131*/SensorCall(5012);return;/*132*/}

    SensorCall(5020);for (mg = SvMAGIC(sv); mg; mg = mg->mg_moremagic) {
	SensorCall(5014);const MGVTBL* const vtbl = mg->mg_virtual;
	SensorCall(5016);if (PERL_MAGIC_TYPE_IS_VALUE_MAGIC(mg->mg_type))
	    {/*133*/SensorCall(5015);continue;/*134*/}
		
	SensorCall(5018);if ((mg->mg_flags & MGf_LOCAL) && vtbl->svt_local)
	    {/*135*/SensorCall(5017);(void)vtbl->svt_local(aTHX_ nsv, mg);/*136*/}
	else
	    sv_magicext(nsv, mg->mg_obj, mg->mg_type, vtbl,
			    mg->mg_ptr, mg->mg_len);

	/* container types should remain read-only across localization */
	SensorCall(5019);if (!SvIsCOW(sv)) SvFLAGS(nsv) |= SvREADONLY(sv);
    }

    SensorCall(5022);if (SvTYPE(nsv) >= SVt_PVMG && SvMAGIC(nsv)) {
	SvFLAGS(nsv) |= SvMAGICAL(sv);
	SensorCall(5021);if (setmagic) {
	    PL_localizing = 1;
	    SvSETMAGIC(nsv);
	    PL_localizing = 0;
	}
    }	    
SensorCall(5023);}

#define mg_free_struct(sv, mg) S_mg_free_struct(aTHX_ sv, mg)
static void
S_mg_free_struct(pTHX_ SV *sv, MAGIC *mg)
{
    SensorCall(5024);const MGVTBL* const vtbl = mg->mg_virtual;
    SensorCall(5026);if (vtbl && vtbl->svt_free)
	{/*157*/SensorCall(5025);vtbl->svt_free(aTHX_ sv, mg);/*158*/}
    SensorCall(5028);if (mg->mg_ptr && mg->mg_type != PERL_MAGIC_regex_global) {
	SensorCall(5027);if (mg->mg_len > 0 || mg->mg_type == PERL_MAGIC_utf8)
	    Safefree(mg->mg_ptr);
	else if (mg->mg_len == HEf_SVKEY)
	    SvREFCNT_dec(MUTABLE_SV(mg->mg_ptr));
    }
    SensorCall(5029);if (mg->mg_flags & MGf_REFCOUNTED)
	SvREFCNT_dec(mg->mg_obj);
    Safefree(mg);
}

/*
=for apidoc mg_free

Free any magic storage used by the SV.  See C<sv_magic>.

=cut
*/

int
Perl_mg_free(pTHX_ SV *sv)
{
    SensorCall(5030);MAGIC* mg;
    MAGIC* moremagic;

    PERL_ARGS_ASSERT_MG_FREE;

    SensorCall(5032);for (mg = SvMAGIC(sv); mg; mg = moremagic) {
	SensorCall(5031);moremagic = mg->mg_moremagic;
	mg_free_struct(sv, mg);
	SvMAGIC_set(sv, moremagic);
    }
    SvMAGIC_set(sv, NULL);
    SvMAGICAL_off(sv);
    {int  ReplaceReturn2067 = 0; SensorCall(5033); return ReplaceReturn2067;}
}

/*
=for apidoc Am|void|mg_free_type|SV *sv|int how

Remove any magic of type I<how> from the SV I<sv>.  See L</sv_magic>.

=cut
*/

void
Perl_mg_free_type(pTHX_ SV *sv, int how)
{
    SensorCall(5034);MAGIC *mg, *prevmg, *moremg;
    PERL_ARGS_ASSERT_MG_FREE_TYPE;
    SensorCall(5040);for (prevmg = NULL, mg = SvMAGIC(sv); mg; prevmg = mg, mg = moremg) {
	SensorCall(5035);MAGIC *newhead;
	moremg = mg->mg_moremagic;
	SensorCall(5039);if (mg->mg_type == how) {
	    /* temporarily move to the head of the magic chain, in case
	       custom free code relies on this historical aspect of mg_free */
	    SensorCall(5036);if (prevmg) {
		SensorCall(5037);prevmg->mg_moremagic = moremg;
		mg->mg_moremagic = SvMAGIC(sv);
		SvMAGIC_set(sv, mg);
	    }
	    SensorCall(5038);newhead = mg->mg_moremagic;
	    mg_free_struct(sv, mg);
	    SvMAGIC_set(sv, newhead);
	    mg = prevmg;
	}
    }
    mg_magical(sv);
}

#include <signal.h>

U32
Perl_magic_regdata_cnt(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5041);    dVAR;
    PERL_UNUSED_ARG(sv);

    PERL_ARGS_ASSERT_MAGIC_REGDATA_CNT;

    SensorCall(5050);if (PL_curpm) {
	SensorCall(5042);register const REGEXP * const rx = PM_GETRE(PL_curpm);
	SensorCall(5049);if (rx) {
	    SensorCall(5043);if (mg->mg_obj) {			/* @+ */
		/* return the number possible */
		{U32  ReplaceReturn2066 = RX_NPARENS(rx); SensorCall(5044); return ReplaceReturn2066;}
	    } else {				/* @- */
		SensorCall(5045);I32 paren = RX_LASTPAREN(rx);

		/* return the last filled */
		SensorCall(5047);while ( paren >= 0
			&& (RX_OFFS(rx)[paren].start == -1
			    || RX_OFFS(rx)[paren].end == -1) )
		    {/*43*/SensorCall(5046);paren--;/*44*/}
		{U32  ReplaceReturn2065 = (U32)paren; SensorCall(5048); return ReplaceReturn2065;}
	    }
	}
    }

    {U32  ReplaceReturn2064 = (U32)-1; SensorCall(5051); return ReplaceReturn2064;}
}

int
Perl_magic_regdatum_get(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5052);    dVAR;

    PERL_ARGS_ASSERT_MAGIC_REGDATUM_GET;

    SensorCall(5066);if (PL_curpm) {
	SensorCall(5053);register const REGEXP * const rx = PM_GETRE(PL_curpm);
	SensorCall(5065);if (rx) {
	    SensorCall(5054);register const I32 paren = mg->mg_len;
	    register I32 s;
	    register I32 t;
	    SensorCall(5056);if (paren < 0)
		{/*45*/{int  ReplaceReturn2063 = 0; SensorCall(5055); return ReplaceReturn2063;}/*46*/}
	    SensorCall(5064);if (paren <= (I32)RX_NPARENS(rx) &&
		(s = RX_OFFS(rx)[paren].start) != -1 &&
		(t = RX_OFFS(rx)[paren].end) != -1)
		{
		    SensorCall(5057);register I32 i;
		    SensorCall(5060);if (mg->mg_obj)		/* @+ */
			{/*47*/SensorCall(5058);i = t;/*48*/}
		    else			/* @- */
			{/*49*/SensorCall(5059);i = s;/*50*/}

		    SensorCall(5063);if (i > 0 && RX_MATCH_UTF8(rx)) {
			SensorCall(5061);const char * const b = RX_SUBBEG(rx);
			SensorCall(5062);if (b)
			    i = utf8_length((U8*)b, (U8*)(b+i));
		    }

		    sv_setiv(sv, i);
		}
	}
    }
    {int  ReplaceReturn2062 = 0; SensorCall(5067); return ReplaceReturn2062;}
}

int
Perl_magic_regdatum_set(pTHX_ SV *sv, MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_REGDATUM_SET;
    PERL_UNUSED_ARG(sv);
    PERL_UNUSED_ARG(mg);
    Perl_croak_no_modify(aTHX);
    NORETURN_FUNCTION_END;
SensorCall(5068);}

U32
Perl_magic_len(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5069);    dVAR;
    register I32 paren;
    register I32 i;
    register const REGEXP * rx;
    const char * const remaining = mg->mg_ptr + 1;

    PERL_ARGS_ASSERT_MAGIC_LEN;

    SensorCall(5104);switch (*mg->mg_ptr) {
    case '\020':		
      SensorCall(5070);if (*remaining == '\0') { /* ^P */
          SensorCall(5071);break;
      } else {/*29*/SensorCall(5072);if (strEQ(remaining, "REMATCH")) { /* $^PREMATCH */
          SensorCall(5073);goto do_prematch;
      } else {/*31*/SensorCall(5074);if (strEQ(remaining, "OSTMATCH")) { /* $^POSTMATCH */
          SensorCall(5075);goto do_postmatch;
      ;/*32*/}/*30*/}}
      SensorCall(5076);break;
    case '\015': /* $^MATCH */
	SensorCall(5077);if (strEQ(remaining, "ATCH")) {
        SensorCall(5078);goto do_match;
    } else {
        SensorCall(5079);break;
    }
    case '`':
      do_prematch:
      SensorCall(5080);paren = RX_BUFF_IDX_PREMATCH;
      SensorCall(5081);goto maybegetparen;
    case '\'':
      do_postmatch:
      SensorCall(5082);paren = RX_BUFF_IDX_POSTMATCH;
      SensorCall(5083);goto maybegetparen;
    case '&':
      do_match:
      SensorCall(5084);paren = RX_BUFF_IDX_FULLMATCH;
      SensorCall(5085);goto maybegetparen;
    case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
      SensorCall(5086);paren = atoi(mg->mg_ptr);
    maybegetparen:
	SensorCall(5093);if (PL_curpm && (rx = PM_GETRE(PL_curpm))) {
      getparen:
        SensorCall(5087);i = CALLREG_NUMBUF_LENGTH((REGEXP * const)rx, sv, paren);

		SensorCall(5089);if (i < 0)
		    {/*33*/SensorCall(5088);Perl_croak(aTHX_ "panic: magic_len: %"IVdf, (IV)i);/*34*/}
		{U32  ReplaceReturn2061 = i; SensorCall(5090); return ReplaceReturn2061;}
	} else {
		SensorCall(5091);if (ckWARN(WARN_UNINITIALIZED))
		    report_uninit(sv);
		{U32  ReplaceReturn2060 = 0; SensorCall(5092); return ReplaceReturn2060;}
	}
    case '+':
	SensorCall(5094);if (PL_curpm && (rx = PM_GETRE(PL_curpm))) {
	    SensorCall(5095);paren = RX_LASTPAREN(rx);
	    SensorCall(5097);if (paren)
		{/*35*/SensorCall(5096);goto getparen;/*36*/}
	}
	{U32  ReplaceReturn2059 = 0; SensorCall(5098); return ReplaceReturn2059;}
    case '\016': /* ^N */
	SensorCall(5099);if (PL_curpm && (rx = PM_GETRE(PL_curpm))) {
	    SensorCall(5100);paren = RX_LASTCLOSEPAREN(rx);
	    SensorCall(5102);if (paren)
		{/*37*/SensorCall(5101);goto getparen;/*38*/}
	}
	{U32  ReplaceReturn2058 = 0; SensorCall(5103); return ReplaceReturn2058;}
    }
    magic_get(sv,mg);
    SensorCall(5105);if (!SvPOK(sv) && SvNIOK(sv)) {
	sv_2pv(sv, 0);
    }
    SensorCall(5106);if (SvPOK(sv))
	return SvCUR(sv);
    {U32  ReplaceReturn2057 = 0; SensorCall(5107); return ReplaceReturn2057;}
}

#define SvRTRIM(sv) STMT_START { \
    if (SvPOK(sv)) { \
        STRLEN len = SvCUR(sv); \
        char * const p = SvPVX(sv); \
	while (len > 0 && isSPACE(p[len-1])) \
	   --len; \
	SvCUR_set(sv, len); \
	p[len] = '\0'; \
    } \
} STMT_END

void
Perl_emulate_cop_io(pTHX_ const COP *const c, SV *const sv)
{
    PERL_ARGS_ASSERT_EMULATE_COP_IO;

    SensorCall(5108);if (!(CopHINTS_get(c) & (HINT_LEXICAL_IO_IN|HINT_LEXICAL_IO_OUT)))
	sv_setsv(sv, &PL_sv_undef);
    else {
	sv_setpvs(sv, "");
	SvUTF8_off(sv);
	SensorCall(5110);if ((CopHINTS_get(c) & HINT_LEXICAL_IO_IN)) {
	    SensorCall(5109);SV *const value = cop_hints_fetch_pvs(c, "open<", 0);
	    assert(value);
	    sv_catsv(sv, value);
	}
	sv_catpvs(sv, "\0");
	SensorCall(5112);if ((CopHINTS_get(c) & HINT_LEXICAL_IO_OUT)) {
	    SensorCall(5111);SV *const value = cop_hints_fetch_pvs(c, "open>", 0);
	    assert(value);
	    sv_catsv(sv, value);
	}
    }
SensorCall(5113);}

int
Perl_magic_get(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5114);    dVAR;
    register I32 paren;
    register const char *s = NULL;
    register REGEXP *rx;
    const char * const remaining = mg->mg_ptr + 1;
    const char nextchar = *remaining;

    PERL_ARGS_ASSERT_MAGIC_GET;

    SensorCall(5211);switch (*mg->mg_ptr) {
    case '\001':		/* ^A */
	sv_setsv(sv, PL_bodytarget);
	SensorCall(5115);if (SvTAINTED(PL_bodytarget))
	    SvTAINTED_on(sv);
	SensorCall(5116);break;
    case '\003':		/* ^C, ^CHILD_ERROR_NATIVE */
	SensorCall(5117);if (nextchar == '\0') {
	    sv_setiv(sv, (IV)PL_minus_c);
	}
	else {/*7*/SensorCall(5118);if (strEQ(remaining, "HILD_ERROR_NATIVE")) {
	    sv_setiv(sv, (IV)STATUS_NATIVE);
        ;/*8*/}}
	SensorCall(5119);break;

    case '\004':		/* ^D */
	sv_setiv(sv, (IV)(PL_debug & DEBUG_MASK));
	SensorCall(5120);break;
    case '\005':  /* ^E */
	 SensorCall(5121);if (nextchar == '\0') {
#if defined(VMS)
	     {
#	          include <descrip.h>
#	          include <starlet.h>
		  char msg[255];
		  $DESCRIPTOR(msgdsc,msg);
		  sv_setnv(sv,(NV) vaxc$errno);
		  if (sys$getmsg(vaxc$errno,&msgdsc.dsc$w_length,&msgdsc,0,0) & 1)
		       sv_setpvn(sv,msgdsc.dsc$a_pointer,msgdsc.dsc$w_length);
		  else
		       sv_setpvs(sv,"");
	     }
#elif defined(OS2)
	     if (!(_emx_env & 0x200)) {	/* Under DOS */
		  sv_setnv(sv, (NV)errno);
		  sv_setpv(sv, errno ? Strerror(errno) : "");
	     } else {
		  if (errno != errno_isOS2) {
		       const int tmp = _syserrno();
		       if (tmp)	/* 2nd call to _syserrno() makes it 0 */
			    Perl_rc = tmp;
		  }
		  sv_setnv(sv, (NV)Perl_rc);
		  sv_setpv(sv, os2error(Perl_rc));
	     }
#elif defined(WIN32)
	     {
		  const DWORD dwErr = GetLastError();
		  sv_setnv(sv, (NV)dwErr);
		  if (dwErr) {
		       PerlProc_GetOSError(sv, dwErr);
		  }
		  else
		       sv_setpvs(sv, "");
		  SetLastError(dwErr);
	     }
#else
	     {
		 dSAVE_ERRNO;
		 sv_setnv(sv, (NV)errno);
		 sv_setpv(sv, errno ? Strerror(errno) : "");
		 RESTORE_ERRNO;
	     }
#endif
	     SvRTRIM(sv);
	     SvNOK_on(sv);	/* what a wonderful hack! */
	 }
	 else if (strEQ(remaining, "NCODING"))
	      sv_setsv(sv, PL_encoding);
	 SensorCall(5122);break;
    case '\006':		/* ^F */
	sv_setiv(sv, (IV)PL_maxsysfd);
	SensorCall(5123);break;
    case '\007':		/* ^GLOBAL_PHASE */
	SensorCall(5124);if (strEQ(remaining, "LOBAL_PHASE")) {
	    sv_setpvn(sv, PL_phase_names[PL_phase],
		      strlen(PL_phase_names[PL_phase]));
	}
	SensorCall(5125);break;
    case '\010':		/* ^H */
	sv_setiv(sv, (IV)PL_hints);
	SensorCall(5126);break;
    case '\011':		/* ^I */ /* NOT \t in EBCDIC */
	sv_setpv(sv, PL_inplace); /* Will undefine sv if PL_inplace is NULL */
	SensorCall(5127);break;
    case '\017':		/* ^O & ^OPEN */
	SensorCall(5128);if (nextchar == '\0') {
	    sv_setpv(sv, PL_osname);
	    SvTAINTED_off(sv);
	}
	else {/*9*/SensorCall(5129);if (strEQ(remaining, "PEN")) {
	    SensorCall(5130);Perl_emulate_cop_io(aTHX_ &PL_compiling, sv);
	;/*10*/}}
	SensorCall(5131);break;
    case '\020':
	SensorCall(5132);if (nextchar == '\0') {       /* ^P */
	    sv_setiv(sv, (IV)PL_perldb);
	} else {/*11*/SensorCall(5133);if (strEQ(remaining, "REMATCH")) { /* $^PREMATCH */
	    SensorCall(5134);goto do_prematch_fetch;
	} else {/*13*/SensorCall(5135);if (strEQ(remaining, "OSTMATCH")) { /* $^POSTMATCH */
	    SensorCall(5136);goto do_postmatch_fetch;
	;/*14*/}/*12*/}}
	SensorCall(5137);break;
    case '\023':		/* ^S */
	SensorCall(5138);if (nextchar == '\0') {
	    SensorCall(5139);if (PL_parser && PL_parser->lex_state != LEX_NOTPARSING)
		SvOK_off(sv);
	    else if (PL_in_eval)
 		sv_setiv(sv, PL_in_eval & ~(EVAL_INREQUIRE));
	    else
		sv_setiv(sv, 0);
	}
	SensorCall(5140);break;
    case '\024':		/* ^T */
	SensorCall(5141);if (nextchar == '\0') {
#ifdef BIG_TIME
            sv_setnv(sv, PL_basetime);
#else
            sv_setiv(sv, (IV)PL_basetime);
#endif
        }
	else if (strEQ(remaining, "AINT"))
            sv_setiv(sv, PL_tainting
		    ? (PL_taint_warn || PL_unsafe ? -1 : 1)
		    : 0);
        SensorCall(5142);break;
    case '\025':		/* $^UNICODE, $^UTF8LOCALE, $^UTF8CACHE */
	SensorCall(5143);if (strEQ(remaining, "NICODE"))
	    sv_setuv(sv, (UV) PL_unicode);
	else if (strEQ(remaining, "TF8LOCALE"))
	    sv_setuv(sv, (UV) PL_utf8locale);
	else if (strEQ(remaining, "TF8CACHE"))
	    sv_setiv(sv, (IV) PL_utf8cache);
        SensorCall(5144);break;
    case '\027':		/* ^W  & $^WARNING_BITS */
	SensorCall(5145);if (nextchar == '\0')
	    sv_setiv(sv, (IV)((PL_dowarn & G_WARN_ON) ? TRUE : FALSE));
	else {/*15*/SensorCall(5146);if (strEQ(remaining, "ARNING_BITS")) {
	    SensorCall(5147);if (PL_compiling.cop_warnings == pWARN_NONE) {
	        sv_setpvn(sv, WARN_NONEstring, WARNsize) ;
	    }
	    else {/*17*/SensorCall(5148);if (PL_compiling.cop_warnings == pWARN_STD) {
		sv_setsv(sv, &PL_sv_undef);
		SensorCall(5149);break;
	    }
            else {/*19*/SensorCall(5150);if (PL_compiling.cop_warnings == pWARN_ALL) {
		/* Get the bit mask for $warnings::Bits{all}, because
		 * it could have been extended by warnings::register */
		SensorCall(5151);HV * const bits=get_hv("warnings::Bits", 0);
		SensorCall(5154);if (bits) {
		    SensorCall(5152);SV ** const bits_all = hv_fetchs(bits, "all", FALSE);
		    SensorCall(5153);if (bits_all)
			sv_setsv(sv, *bits_all);
		}
	        else {
		    sv_setpvn(sv, WARN_ALLstring, WARNsize) ;
		}
	    }
            else {
	        sv_setpvn(sv, (char *) (PL_compiling.cop_warnings + 1),
			  *PL_compiling.cop_warnings);
	    ;/*20*/}/*18*/}}
	    SvPOK_only(sv);
	;/*16*/}}
	SensorCall(5155);break;
    case '\015': /* $^MATCH */
	SensorCall(5156);if (strEQ(remaining, "ATCH")) {
    case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9': case '&':
	    SensorCall(5157);if (PL_curpm && (rx = PM_GETRE(PL_curpm))) {
		/*
		 * Pre-threads, this was paren = atoi(GvENAME((const GV *)mg->mg_obj));
		 * XXX Does the new way break anything?
		 */
		SensorCall(5158);paren = atoi(mg->mg_ptr); /* $& is in [0] */
		CALLREG_NUMBUF_FETCH(rx,paren,sv);
		SensorCall(5159);break;
	    }
	    sv_setsv(sv,&PL_sv_undef);
	}
	SensorCall(5160);break;
    case '+':
	SensorCall(5161);if (PL_curpm && (rx = PM_GETRE(PL_curpm))) {
	    SensorCall(5162);if (RX_LASTPAREN(rx)) {
	        CALLREG_NUMBUF_FETCH(rx,RX_LASTPAREN(rx),sv);
	        SensorCall(5163);break;
	    }
	}
	sv_setsv(sv,&PL_sv_undef);
	SensorCall(5164);break;
    case '\016':		/* ^N */
	SensorCall(5165);if (PL_curpm && (rx = PM_GETRE(PL_curpm))) {
	    SensorCall(5166);if (RX_LASTCLOSEPAREN(rx)) {
	        CALLREG_NUMBUF_FETCH(rx,RX_LASTCLOSEPAREN(rx),sv);
	        SensorCall(5167);break;
	    }

	}
	sv_setsv(sv,&PL_sv_undef);
	SensorCall(5168);break;
    case '`':
      do_prematch_fetch:
	SensorCall(5169);if (PL_curpm && (rx = PM_GETRE(PL_curpm))) {
	    CALLREG_NUMBUF_FETCH(rx,-2,sv);
	    SensorCall(5170);break;
	}
	sv_setsv(sv,&PL_sv_undef);
	SensorCall(5171);break;
    case '\'':
      do_postmatch_fetch:
	SensorCall(5172);if (PL_curpm && (rx = PM_GETRE(PL_curpm))) {
	    CALLREG_NUMBUF_FETCH(rx,-1,sv);
	    SensorCall(5173);break;
	}
	sv_setsv(sv,&PL_sv_undef);
	SensorCall(5174);break;
    case '.':
	SensorCall(5175);if (GvIO(PL_last_in_gv)) {
	    sv_setiv(sv, (IV)IoLINES(GvIOp(PL_last_in_gv)));
	}
	SensorCall(5176);break;
    case '?':
	{
	    sv_setiv(sv, (IV)STATUS_CURRENT);
#ifdef COMPLEX_STATUS
	    SvUPGRADE(sv, SVt_PVLV);
	    LvTARGOFF(sv) = PL_statusvalue;
	    LvTARGLEN(sv) = PL_statusvalue_vms;
#endif
	}
	SensorCall(5177);break;
    case '^':
	SensorCall(5178);if (GvIOp(PL_defoutgv))
		s = IoTOP_NAME(GvIOp(PL_defoutgv));
	SensorCall(5179);if (s)
	    sv_setpv(sv,s);
	else {
	    sv_setpv(sv,GvENAME(PL_defoutgv));
	    sv_catpvs(sv,"_TOP");
	}
	SensorCall(5180);break;
    case '~':
	SensorCall(5181);if (GvIOp(PL_defoutgv))
	    s = IoFMT_NAME(GvIOp(PL_defoutgv));
	SensorCall(5182);if (!s)
	    s = GvENAME(PL_defoutgv);
	sv_setpv(sv,s);
	SensorCall(5183);break;
    case '=':
	SensorCall(5184);if (GvIO(PL_defoutgv))
	    sv_setiv(sv, (IV)IoPAGE_LEN(GvIOp(PL_defoutgv)));
	SensorCall(5185);break;
    case '-':
	SensorCall(5186);if (GvIO(PL_defoutgv))
	    sv_setiv(sv, (IV)IoLINES_LEFT(GvIOp(PL_defoutgv)));
	SensorCall(5187);break;
    case '%':
	SensorCall(5188);if (GvIO(PL_defoutgv))
	    sv_setiv(sv, (IV)IoPAGE(GvIOp(PL_defoutgv)));
	SensorCall(5189);break;
    case ':':
	SensorCall(5190);break;
    case '/':
	SensorCall(5191);break;
    case '[':
	sv_setiv(sv, 0);
	SensorCall(5192);break;
    case '|':
	SensorCall(5193);if (GvIO(PL_defoutgv))
	    sv_setiv(sv, (IV)(IoFLAGS(GvIOp(PL_defoutgv)) & IOf_FLUSH) != 0 );
	SensorCall(5194);break;
    case '\\':
	SensorCall(5195);if (PL_ors_sv)
	    sv_copypv(sv, PL_ors_sv);
	SensorCall(5196);break;
    case '$': /* $$ */
	{
	    SensorCall(5197);IV const pid = (IV)PerlProc_getpid();
	    SensorCall(5198);if (isGV(mg->mg_obj) || SvIV(mg->mg_obj) != pid) {
		/* never set manually, or at least not since last fork */
		sv_setiv(sv, pid);
		/* never unsafe, even if reading in a tainted expression */
		SvTAINTED_off(sv);
	    }
	    /* else a value has been assigned manually, so do nothing */
	}
	SensorCall(5199);break;

    case '!':
	{
	dSAVE_ERRNO;
#ifdef VMS
	sv_setnv(sv, (NV)((errno == EVMSERR) ? vaxc$errno : errno));
#else
	sv_setnv(sv, (NV)errno);
#endif
#ifdef OS2
	if (errno == errno_isOS2 || errno == errno_isOS2_set)
	    sv_setpv(sv, os2error(Perl_rc));
	else
#endif
	sv_setpv(sv, errno ? Strerror(errno) : "");
	SensorCall(5200);if (SvPOKp(sv))
	    SvPOK_on(sv);    /* may have got removed during taint processing */
	RESTORE_ERRNO;
	}

	SvRTRIM(sv);
	SvNOK_on(sv);	/* what a wonderful hack! */
	SensorCall(5201);break;
    case '<':
	sv_setiv(sv, (IV)PerlProc_getuid());
	SensorCall(5202);break;
    case '>':
	sv_setiv(sv, (IV)PerlProc_geteuid());
	SensorCall(5203);break;
    case '(':
	sv_setiv(sv, (IV)PerlProc_getgid());
	SensorCall(5204);goto add_groups;
    case ')':
	sv_setiv(sv, (IV)PerlProc_getegid());
      add_groups:
#ifdef HAS_GETGROUPS
	{
	    Groups_t *gary = NULL;
	    SensorCall(5205);I32 i, num_groups = getgroups(0, gary);
            Newx(gary, num_groups, Groups_t);
            num_groups = getgroups(num_groups, gary);
	    SensorCall(5207);for (i = 0; i < num_groups; i++)
		{/*21*/SensorCall(5206);Perl_sv_catpvf(aTHX_ sv, " %"IVdf, (IV)gary[i]);/*22*/}
            Safefree(gary);
	}
	SensorCall(5208);(void)SvIOK_on(sv);	/* what a wonderful hack! */
#endif
	SensorCall(5209);break;
    case '0':
	SensorCall(5210);break;
    }
    {int  ReplaceReturn2056 = 0; SensorCall(5212); return ReplaceReturn2056;}
}

int
Perl_magic_getuvar(pTHX_ SV *sv, MAGIC *mg)
{
    SensorCall(5213);struct ufuncs * const uf = (struct ufuncs *)mg->mg_ptr;

    PERL_ARGS_ASSERT_MAGIC_GETUVAR;

    SensorCall(5215);if (uf && uf->uf_val)
	{/*27*/SensorCall(5214);(*uf->uf_val)(aTHX_ uf->uf_index, sv);/*28*/}
    {int  ReplaceReturn2055 = 0; SensorCall(5216); return ReplaceReturn2055;}
}

int
Perl_magic_setenv(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5217);    dVAR;
    STRLEN len = 0, klen;
    const char *s = SvOK(sv) ? SvPV_const(sv,len) : "";
    const char * const ptr = MgPV_const(mg,klen);
    my_setenv(ptr, s);

    PERL_ARGS_ASSERT_MAGIC_SETENV;

#ifdef DYNAMIC_ENV_FETCH
     /* We just undefd an environment var.  Is a replacement */
     /* waiting in the wings? */
    if (!len) {
	SV ** const valp = hv_fetch(GvHVn(PL_envgv), ptr, klen, FALSE);
	if (valp)
	    s = SvOK(*valp) ? SvPV_const(*valp, len) : "";
    }
#endif

#if !defined(OS2) && !defined(AMIGAOS) && !defined(WIN32) && !defined(MSDOS)
			    /* And you'll never guess what the dog had */
			    /*   in its mouth... */
    SensorCall(5224);if (PL_tainting) {
	MgTAINTEDDIR_off(mg);
#ifdef VMS
	if (s && klen == 8 && strEQ(ptr, "DCL$PATH")) {
	    char pathbuf[256], eltbuf[256], *cp, *elt;
	    int i = 0, j = 0;

	    my_strlcpy(eltbuf, s, sizeof(eltbuf));
	    elt = eltbuf;
	    do {          /* DCL$PATH may be a search list */
		while (1) {   /* as may dev portion of any element */
		    if ( ((cp = strchr(elt,'[')) || (cp = strchr(elt,'<'))) ) {
			if ( *(cp+1) == '.' || *(cp+1) == '-' ||
			     cando_by_name(S_IWUSR,0,elt) ) {
			    MgTAINTEDDIR_on(mg);
			    return 0;
			}
		    }
		    if ((cp = strchr(elt, ':')) != NULL)
			*cp = '\0';
		    if (my_trnlnm(elt, eltbuf, j++))
			elt = eltbuf;
		    else
			break;
		}
		j = 0;
	    } while (my_trnlnm(s, pathbuf, i++) && (elt = pathbuf));
	}
#endif /* VMS */
	SensorCall(5223);if (s && klen == 4 && strEQ(ptr,"PATH")) {
	    SensorCall(5218);const char * const strend = s + len;

	    SensorCall(5222);while (s < strend) {
		SensorCall(5219);char tmpbuf[256];
		Stat_t st;
		I32 i;
#ifdef VMS  /* Hmm.  How do we get $Config{path_sep} from C? */
		const char path_sep = '|';
#else
		const char path_sep = ':';
#endif
		s = delimcpy(tmpbuf, tmpbuf + sizeof tmpbuf,
			     s, strend, path_sep, &i);
		s++;
		SensorCall(5221);if (i >= (I32)sizeof tmpbuf   /* too long -- assume the worst */
#ifdef VMS
		      || !strchr(tmpbuf, ':') /* no colon thus no device name -- assume relative path */
#else
		      || *tmpbuf != '/'       /* no starting slash -- assume relative path */
#endif
		      || (PerlLIO_stat(tmpbuf, &st) == 0 && (st.st_mode & 2)) ) {
		    MgTAINTEDDIR_on(mg);
		    {int  ReplaceReturn2054 = 0; SensorCall(5220); return ReplaceReturn2054;}
		}
	    }
	}
    }
#endif /* neither OS2 nor AMIGAOS nor WIN32 nor MSDOS */

    {int  ReplaceReturn2053 = 0; SensorCall(5225); return ReplaceReturn2053;}
}

int
Perl_magic_clearenv(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5226);    PERL_ARGS_ASSERT_MAGIC_CLEARENV;
    PERL_UNUSED_ARG(sv);
    my_setenv(MgPV_nolen_const(mg),NULL);
    {int  ReplaceReturn2052 = 0; SensorCall(5227); return ReplaceReturn2052;}
}

int
Perl_magic_set_all_env(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5228);    dVAR;
    PERL_ARGS_ASSERT_MAGIC_SET_ALL_ENV;
    PERL_UNUSED_ARG(mg);
#if defined(VMS)
    Perl_die(aTHX_ "Can't make list assignment to %%ENV on this system");
#else
    SensorCall(5232);if (PL_localizing) {
	SensorCall(5229);HE* entry;
	my_clearenv();
	hv_iterinit(MUTABLE_HV(sv));
	SensorCall(5231);while ((entry = hv_iternext(MUTABLE_HV(sv)))) {
	    SensorCall(5230);I32 keylen;
	    my_setenv(hv_iterkey(entry, &keylen),
		      SvPV_nolen_const(hv_iterval(MUTABLE_HV(sv), entry)));
	}
    }
#endif
    {int  ReplaceReturn2051 = 0; SensorCall(5233); return ReplaceReturn2051;}
}

int
Perl_magic_clear_all_env(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5234);    dVAR;
    PERL_ARGS_ASSERT_MAGIC_CLEAR_ALL_ENV;
    PERL_UNUSED_ARG(sv);
    PERL_UNUSED_ARG(mg);
#if defined(VMS)
    Perl_die(aTHX_ "Can't make list assignment to %%ENV on this system");
#else
    my_clearenv();
#endif
    {int  ReplaceReturn2050 = 0; SensorCall(5235); return ReplaceReturn2050;}
}

#ifndef PERL_MICRO
#ifdef HAS_SIGPROCMASK
static void
restore_sigmask(pTHX_ SV *save_sv)
{
    SensorCall(5236);const sigset_t * const ossetp = (const sigset_t *) SvPV_nolen_const( save_sv );
    (void)sigprocmask(SIG_SETMASK, ossetp, NULL);
SensorCall(5237);}
#endif
int
Perl_magic_getsig(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5238);    dVAR;
    /* Are we fetching a signal entry? */
    int i = (I16)mg->mg_private;

    PERL_ARGS_ASSERT_MAGIC_GETSIG;

    SensorCall(5240);if (!i) {
        SensorCall(5239);STRLEN siglen;
        const char * sig = MgPV_const(mg, siglen);
        mg->mg_private = i = whichsig_pvn(sig, siglen);
    }

    SensorCall(5244);if (i > 0) {
    	SensorCall(5241);if(PL_psig_ptr[i])
    	    sv_setsv(sv,PL_psig_ptr[i]);
    	else {
	    SensorCall(5242);Sighandler_t sigstate = rsignal_state(i);
#ifdef FAKE_PERSISTENT_SIGNAL_HANDLERS
	    if (PL_sig_handlers_initted && PL_sig_ignoring[i])
		sigstate = SIG_IGN;
#endif
#ifdef FAKE_DEFAULT_SIGNAL_HANDLERS
	    if (PL_sig_handlers_initted && PL_sig_defaulting[i])
		sigstate = SIG_DFL;
#endif
    	    /* cache state so we don't fetch it again */
    	    SensorCall(5243);if(sigstate == (Sighandler_t) SIG_IGN)
    	    	sv_setpvs(sv,"IGNORE");
    	    else
    	    	sv_setsv(sv,&PL_sv_undef);
	    PL_psig_ptr[i] = SvREFCNT_inc_simple_NN(sv);
    	    SvTEMP_off(sv);
    	}
    }
    {int  ReplaceReturn2049 = 0; SensorCall(5245); return ReplaceReturn2049;}
}
int
Perl_magic_clearsig(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5246);    PERL_ARGS_ASSERT_MAGIC_CLEARSIG;

    magic_setsig(NULL, mg);
    {int  ReplaceReturn2048 = sv_unmagic(sv, mg->mg_type); SensorCall(5247); return ReplaceReturn2048;}
}

Signal_t
#if defined(HAS_SIGACTION) && defined(SA_SIGINFO)
Perl_csighandler(int sig, siginfo_t *sip PERL_UNUSED_DECL, void *uap PERL_UNUSED_DECL)
#else
Perl_csighandler(int sig)
#endif
{
#ifdef PERL_GET_SIG_CONTEXT
    dTHXa(PERL_GET_SIG_CONTEXT);
#else
    dTHX;
#endif
#ifdef FAKE_PERSISTENT_SIGNAL_HANDLERS
    (void) rsignal(sig, PL_csighandlerp);
    if (PL_sig_ignoring[sig]) return;
#endif
#ifdef FAKE_DEFAULT_SIGNAL_HANDLERS
    if (PL_sig_defaulting[sig])
#ifdef KILL_BY_SIGPRC
            exit((Perl_sig_to_vmscondition(sig)&STS$M_COND_ID)|STS$K_SEVERE|STS$M_INHIB_MSG);
#else
            exit(1);
#endif
#endif
    SensorCall(5253);if (
#ifdef SIGILL
	   sig == SIGILL ||
#endif
#ifdef SIGBUS
	   sig == SIGBUS ||
#endif
#ifdef SIGSEGV
	   sig == SIGSEGV ||
#endif
	   (PL_signals & PERL_SIGNALS_UNSAFE_FLAG))
	/* Call the perl level handler now--
	 * with risk we may be in malloc() or being destructed etc. */
#if defined(HAS_SIGACTION) && defined(SA_SIGINFO)
	{/*159*/SensorCall(5248);(*PL_sighandlerp)(sig, NULL, NULL);/*160*/}
#else
	(*PL_sighandlerp)(sig);
#endif
    else {
	SensorCall(5249);if (!PL_psig_pend) {/*161*/SensorCall(5250);return;/*162*/}
	/* Set a flag to say this signal is pending, that is awaiting delivery after
	 * the current Perl opcode completes */
	PL_psig_pend[sig]++;

#ifndef SIG_PENDING_DIE_COUNT
#  define SIG_PENDING_DIE_COUNT 120
#endif
	/* Add one to say _a_ signal is pending */
	SensorCall(5252);if (++PL_sig_pending >= SIG_PENDING_DIE_COUNT)
	    {/*163*/SensorCall(5251);Perl_croak(aTHX_ "Maximal count of pending signals (%lu) exceeded",
		       (unsigned long)SIG_PENDING_DIE_COUNT);/*164*/}
    }
SensorCall(5254);}

#if defined(FAKE_PERSISTENT_SIGNAL_HANDLERS) || defined(FAKE_DEFAULT_SIGNAL_HANDLERS)
void
Perl_csighandler_init(void)
{
    int sig;
    if (PL_sig_handlers_initted) return;

    for (sig = 1; sig < SIG_SIZE; sig++) {
#ifdef FAKE_DEFAULT_SIGNAL_HANDLERS
        dTHX;
        PL_sig_defaulting[sig] = 1;
        (void) rsignal(sig, PL_csighandlerp);
#endif
#ifdef FAKE_PERSISTENT_SIGNAL_HANDLERS
        PL_sig_ignoring[sig] = 0;
#endif
    }
    PL_sig_handlers_initted = 1;
}
#endif

#if defined HAS_SIGPROCMASK
static void
unblock_sigmask(pTHX_ void* newset)
{
    SensorCall(5255);sigprocmask(SIG_UNBLOCK, (sigset_t*)newset, NULL);
SensorCall(5256);}
#endif

void
Perl_despatch_signals(pTHX)
{
SensorCall(5257);    dVAR;
    int sig;
    PL_sig_pending = 0;
    SensorCall(5264);for (sig = 1; sig < SIG_SIZE; sig++) {
	SensorCall(5258);if (PL_psig_pend[sig]) {
	    dSAVE_ERRNO;
#ifdef HAS_SIGPROCMASK
	    /* From sigaction(2) (FreeBSD man page):
	     * | Signal routines normally execute with the signal that
	     * | caused their invocation blocked, but other signals may
	     * | yet occur.
	     * Emulation of this behavior (from within Perl) is enabled
	     * using sigprocmask
	     */
	    SensorCall(5259);int was_blocked;
	    sigset_t newset, oldset;

	    sigemptyset(&newset);
	    sigaddset(&newset, sig);
	    sigprocmask(SIG_BLOCK, &newset, &oldset);
	    was_blocked = sigismember(&oldset, sig);
	    SensorCall(5261);if (!was_blocked) {
		SensorCall(5260);SV* save_sv = newSVpvn((char *)(&newset), sizeof(sigset_t));
		ENTER;
		SAVEFREESV(save_sv);
		SAVEDESTRUCTOR_X(unblock_sigmask, SvPV_nolen(save_sv));
	    }
#endif
 	    PL_psig_pend[sig] = 0;
#if defined(HAS_SIGACTION) && defined(SA_SIGINFO)
	    SensorCall(5262);(*PL_sighandlerp)(sig, NULL, NULL);
#else
	    (*PL_sighandlerp)(sig);
#endif
#ifdef HAS_SIGPROCMASK
	    SensorCall(5263);if (!was_blocked)
		LEAVE;
#endif
	    RESTORE_ERRNO;
	}
    }
SensorCall(5265);}

/* sv of NULL signifies that we're acting as magic_clearsig.  */
int
Perl_magic_setsig(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5266);    dVAR;
    I32 i;
    SV** svp = NULL;
    /* Need to be careful with SvREFCNT_dec(), because that can have side
     * effects (due to closures). We must make sure that the new disposition
     * is in place before it is called.
     */
    SV* to_dec = NULL;
    STRLEN len;
#ifdef HAS_SIGPROCMASK
    sigset_t set, save;
    SV* save_sv;
#endif
    register const char *s = MgPV_const(mg,len);

    PERL_ARGS_ASSERT_MAGIC_SETSIG;

    SensorCall(5287);if (*s == '_') {
        SensorCall(5267);if (memEQs(s, len, "__DIE__"))
	    svp = &PL_diehook;
	else {/*99*/SensorCall(5268);if (memEQs(s, len, "__WARN__")
		 && (sv ? 1 : PL_warnhook != PERL_WARNHOOK_FATAL)) {
	    /* Merge the existing behaviours, which are as follows:
	       magic_setsig, we always set svp to &PL_warnhook
	       (hence we always change the warnings handler)
	       For magic_clearsig, we don't change the warnings handler if it's
	       set to the &PL_warnhook.  */
	    SensorCall(5269);svp = &PL_warnhook;
        } else {/*101*/SensorCall(5270);if (sv) {
            SensorCall(5271);SV *tmp = sv_newmortal();
            Perl_croak(aTHX_ "No such hook: %s",
                                pv_pretty(tmp, s, len, 0, NULL, NULL, 0));
        ;/*102*/}/*100*/}}
	SensorCall(5272);i = 0;
	SensorCall(5276);if (svp && *svp) {
	    SensorCall(5273);if (*svp != PERL_WARNHOOK_FATAL)
		{/*103*/SensorCall(5274);to_dec = *svp;/*104*/}
	    SensorCall(5275);*svp = NULL;
	}
    }
    else {
	SensorCall(5277);i = (I16)mg->mg_private;
	SensorCall(5279);if (!i) {
	    SensorCall(5278);i = whichsig_pvn(s, len);   /* ...no, a brick */
	    mg->mg_private = (U16)i;
	}
	SensorCall(5283);if (i <= 0) {
	    SensorCall(5280);if (sv) {
                SensorCall(5281);SV *tmp = sv_newmortal();
		Perl_ck_warner(aTHX_ packWARN(WARN_SIGNAL), "No such signal: SIG%s",
                                            pv_pretty(tmp, s, len, 0, NULL, NULL, 0));
            }
	    {int  ReplaceReturn2047 = 0; SensorCall(5282); return ReplaceReturn2047;}
	}
#ifdef HAS_SIGPROCMASK
	/* Avoid having the signal arrive at a bad time, if possible. */
	SensorCall(5284);sigemptyset(&set);
	sigaddset(&set,i);
	sigprocmask(SIG_BLOCK, &set, &save);
	ENTER;
	save_sv = newSVpvn((char *)(&save), sizeof(sigset_t));
	SAVEFREESV(save_sv);
	SAVEDESTRUCTOR_X(restore_sigmask, save_sv);
#endif
	PERL_ASYNC_CHECK();
#if defined(FAKE_PERSISTENT_SIGNAL_HANDLERS) || defined(FAKE_DEFAULT_SIGNAL_HANDLERS)
	if (!PL_sig_handlers_initted) Perl_csighandler_init();
#endif
#ifdef FAKE_PERSISTENT_SIGNAL_HANDLERS
	PL_sig_ignoring[i] = 0;
#endif
#ifdef FAKE_DEFAULT_SIGNAL_HANDLERS
	PL_sig_defaulting[i] = 0;
#endif
	to_dec = PL_psig_ptr[i];
	SensorCall(5286);if (sv) {
	    PL_psig_ptr[i] = SvREFCNT_inc_simple_NN(sv);
	    SvTEMP_off(sv); /* Make sure it doesn't go away on us */

	    /* Signals don't change name during the program's execution, so once
	       they're cached in the appropriate slot of PL_psig_name, they can
	       stay there.

	       Ideally we'd find some way of making SVs at (C) compile time, or
	       at least, doing most of the work.  */
	    SensorCall(5285);if (!PL_psig_name[i]) {
		PL_psig_name[i] = newSVpvn(s, len);
		SvREADONLY_on(PL_psig_name[i]);
	    }
	} else {
	    SvREFCNT_dec(PL_psig_name[i]);
	    PL_psig_name[i] = NULL;
	    PL_psig_ptr[i] = NULL;
	}
    }
    SensorCall(5302);if (sv && (isGV_with_GP(sv) || SvROK(sv))) {
	SensorCall(5288);if (i) {
	    SensorCall(5289);(void)rsignal(i, PL_csighandlerp);
	}
	else
	    *svp = SvREFCNT_inc_simple_NN(sv);
    } else {
	SensorCall(5290);if (sv && SvOK(sv)) {
	    SensorCall(5291);s = SvPV_force(sv, len);
	} else {
	    SensorCall(5292);sv = NULL;
	}
	SensorCall(5301);if (sv && memEQs(s, len,"IGNORE")) {
	    SensorCall(5293);if (i) {
#ifdef FAKE_PERSISTENT_SIGNAL_HANDLERS
		PL_sig_ignoring[i] = 1;
		(void)rsignal(i, PL_csighandlerp);
#else
		SensorCall(5294);(void)rsignal(i, (Sighandler_t) SIG_IGN);
#endif
	    }
	}
	else {/*105*/SensorCall(5295);if (!sv || memEQs(s, len,"DEFAULT") || !len) {
	    SensorCall(5296);if (i) {
#ifdef FAKE_DEFAULT_SIGNAL_HANDLERS
		PL_sig_defaulting[i] = 1;
		(void)rsignal(i, PL_csighandlerp);
#else
		SensorCall(5297);(void)rsignal(i, (Sighandler_t) SIG_DFL);
#endif
	    }
	}
	else {
	    /*
	     * We should warn if HINT_STRICT_REFS, but without
	     * access to a known hint bit in a known OP, we can't
	     * tell whether HINT_STRICT_REFS is in force or not.
	     */
	    SensorCall(5298);if (!strchr(s,':') && !strchr(s,'\''))
		{/*107*/SensorCall(5299);Perl_sv_insert_flags(aTHX_ sv, 0, 0, STR_WITH_LEN("main::"),
				     SV_GMAGIC);/*108*/}
	    SensorCall(5300);if (i)
		(void)rsignal(i, PL_csighandlerp);
	    else
		*svp = SvREFCNT_inc_simple_NN(sv);
	;/*106*/}}
    }

#ifdef HAS_SIGPROCMASK
    SensorCall(5303);if(i)
	LEAVE;
#endif
    SvREFCNT_dec(to_dec);
    {int  ReplaceReturn2046 = 0; SensorCall(5304); return ReplaceReturn2046;}
}
#endif /* !PERL_MICRO */

int
Perl_magic_setisa(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5305);    dVAR;
    PERL_ARGS_ASSERT_MAGIC_SETISA;
    PERL_UNUSED_ARG(sv);

    /* Skip _isaelem because _isa will handle it shortly */
    SensorCall(5307);if (PL_delaymagic & DM_ARRAY_ISA && mg->mg_type == PERL_MAGIC_isaelem)
	{/*79*/{int  ReplaceReturn2045 = 0; SensorCall(5306); return ReplaceReturn2045;}/*80*/}

    {int  ReplaceReturn2044 = magic_clearisa(NULL, mg); SensorCall(5308); return ReplaceReturn2044;}
}

/* sv of NULL signifies that we're acting as magic_setisa.  */
int
Perl_magic_clearisa(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5309);    dVAR;
    HV* stash;

    PERL_ARGS_ASSERT_MAGIC_CLEARISA;

    /* Bail out if destruction is going on */
    SensorCall(5311);if(PL_phase == PERL_PHASE_DESTRUCT) {/*1*/{int  ReplaceReturn2043 = 0; SensorCall(5310); return ReplaceReturn2043;}/*2*/}

    SensorCall(5312);if (sv)
	av_clear(MUTABLE_AV(sv));

    SensorCall(5313);if (SvTYPE(mg->mg_obj) != SVt_PVGV && SvSMAGICAL(mg->mg_obj))
	/* This occurs with setisa_elem magic, which calls this
	   same function. */
	mg = mg_find(mg->mg_obj, PERL_MAGIC_isa);

    SensorCall(5319);if (SvTYPE(mg->mg_obj) == SVt_PVAV) { /* multiple stashes */
	SensorCall(5314);SV **svp = AvARRAY((AV *)mg->mg_obj);
	I32 items = AvFILLp((AV *)mg->mg_obj) + 1;
	SensorCall(5317);while (items--) {
	    SensorCall(5315);stash = GvSTASH((GV *)*svp++);
	    SensorCall(5316);if (stash && HvENAME(stash)) mro_isa_changed_in(stash);
	}

	{int  ReplaceReturn2042 = 0; SensorCall(5318); return ReplaceReturn2042;}
    }

    SensorCall(5320);stash = GvSTASH(
        (const GV *)mg->mg_obj
    );

    /* The stash may have been detached from the symbol table, so check its
       name before doing anything. */
    SensorCall(5321);if (stash && HvENAME_get(stash))
	mro_isa_changed_in(stash);

    {int  ReplaceReturn2041 = 0; SensorCall(5322); return ReplaceReturn2041;}
}

int
Perl_magic_setamagic(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5323);    dVAR;
    PERL_ARGS_ASSERT_MAGIC_SETAMAGIC;
    PERL_UNUSED_ARG(sv);
    PERL_UNUSED_ARG(mg);
    PL_amagic_generation++;

    {int  ReplaceReturn2040 = 0; SensorCall(5324); return ReplaceReturn2040;}
}

int
Perl_magic_getnkeys(pTHX_ SV *sv, MAGIC *mg)
{
    SensorCall(5325);HV * const hv = MUTABLE_HV(LvTARG(sv));
    I32 i = 0;

    PERL_ARGS_ASSERT_MAGIC_GETNKEYS;
    PERL_UNUSED_ARG(mg);

    SensorCall(5330);if (hv) {
         SensorCall(5326);(void) hv_iterinit(hv);
         SensorCall(5329);if (! SvTIED_mg((const SV *)hv, PERL_MAGIC_tied))
	     i = HvUSEDKEYS(hv);
         else {
	     SensorCall(5327);while (hv_iternext(hv))
	         {/*25*/SensorCall(5328);i++;/*26*/}
         }
    }

    sv_setiv(sv, (IV)i);
    {int  ReplaceReturn2039 = 0; SensorCall(5331); return ReplaceReturn2039;}
}

int
Perl_magic_setnkeys(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5332);    PERL_ARGS_ASSERT_MAGIC_SETNKEYS;
    PERL_UNUSED_ARG(mg);
    SensorCall(5333);if (LvTARG(sv)) {
	hv_ksplit(MUTABLE_HV(LvTARG(sv)), SvIV(sv));
    }
    {int  ReplaceReturn2038 = 0; SensorCall(5334); return ReplaceReturn2038;}
}

/*
=for apidoc magic_methcall

Invoke a magic method (like FETCH).

C<sv> and C<mg> are the tied thingy and the tie magic.

C<meth> is the name of the method to call.

C<argc> is the number of args (in addition to $self) to pass to the method.

The C<flags> can be:

    G_DISCARD     invoke method with G_DISCARD flag and don't
                  return a value
    G_UNDEF_FILL  fill the stack with argc pointers to
                  PL_sv_undef

The arguments themselves are any values following the C<flags> argument.

Returns the SV (if any) returned by the method, or NULL on failure.


=cut
*/

SV*
Perl_magic_methcall(pTHX_ SV *sv, const MAGIC *mg, const char *meth, U32 flags,
		    U32 argc, ...)
{
SensorCall(5335);    dVAR;
    dSP;
    SV* ret = NULL;

    PERL_ARGS_ASSERT_MAGIC_METHCALL;

    ENTER;

    SensorCall(5336);if (flags & G_WRITING_TO_STDERR) {
	SAVETMPS;

	save_re_context();
	SAVESPTR(PL_stderrgv);
	PL_stderrgv = NULL;
    }

    PUSHSTACKi(PERLSI_MAGIC);
    PUSHMARK(SP);

    EXTEND(SP, argc+1);
    PUSHs(SvTIED_obj(sv, mg));
    SensorCall(5342);if (flags & G_UNDEF_FILL) {
	SensorCall(5337);while (argc--) {
	    PUSHs(&PL_sv_undef);
	}
    } else {/*39*/SensorCall(5338);if (argc > 0) {
	SensorCall(5339);va_list args;
	va_start(args, argc);

	SensorCall(5341);do {
	    SensorCall(5340);SV *const sv = va_arg(args, SV *);
	    PUSHs(sv);
	} while (--argc);

	va_end(args);
    ;/*40*/}}
    PUTBACK;
    SensorCall(5345);if (flags & G_DISCARD) {
	call_method(meth, G_SCALAR|G_DISCARD);
    }
    else {
	SensorCall(5343);if (call_method(meth, G_SCALAR))
	    {/*41*/SensorCall(5344);ret = *PL_stack_sp--;/*42*/}
    }
    POPSTACK;
    SensorCall(5346);if (flags & G_WRITING_TO_STDERR)
	FREETMPS;
    LEAVE;
    {SV * ReplaceReturn2037 = ret; SensorCall(5347); return ReplaceReturn2037;}
}


/* wrapper for magic_methcall that creates the first arg */

STATIC SV*
S_magic_methcall1(pTHX_ SV *sv, const MAGIC *mg, const char *meth, U32 flags,
    int n, SV *val)
{
SensorCall(5348);    dVAR;
    SV* arg1 = NULL;

    PERL_ARGS_ASSERT_MAGIC_METHCALL1;

    SensorCall(5353);if (mg->mg_ptr) {
	SensorCall(5349);if (mg->mg_len >= 0) {
	    SensorCall(5350);arg1 = newSVpvn_flags(mg->mg_ptr, mg->mg_len, SVs_TEMP);
	}
	else if (mg->mg_len == HEf_SVKEY)
	    arg1 = MUTABLE_SV(mg->mg_ptr);
    }
    else {/*153*/SensorCall(5351);if (mg->mg_type == PERL_MAGIC_tiedelem) {
	SensorCall(5352);arg1 = newSViv((IV)(mg->mg_len));
	sv_2mortal(arg1);
    ;/*154*/}}
    SensorCall(5355);if (!arg1) {
	{SV * ReplaceReturn2036 = Perl_magic_methcall(aTHX_ sv, mg, meth, flags, n - 1, val); SensorCall(5354); return ReplaceReturn2036;}
    }
    {SV * ReplaceReturn2035 = Perl_magic_methcall(aTHX_ sv, mg, meth, flags, n, arg1, val); SensorCall(5356); return ReplaceReturn2035;}
}

STATIC int
S_magic_methpack(pTHX_ SV *sv, const MAGIC *mg, const char *meth)
{
SensorCall(5357);    dVAR;
    SV* ret;

    PERL_ARGS_ASSERT_MAGIC_METHPACK;

    ret = magic_methcall1(sv, mg, meth, 0, 1, NULL);
    SensorCall(5358);if (ret)
	sv_setsv(sv, ret);
    {int  ReplaceReturn2034 = 0; SensorCall(5359); return ReplaceReturn2034;}
}

int
Perl_magic_getpack(pTHX_ SV *sv, MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_GETPACK;

    SensorCall(5360);if (mg->mg_type == PERL_MAGIC_tiedelem)
	mg->mg_flags |= MGf_GSKIP;
    magic_methpack(sv,mg,"FETCH");
    {int  ReplaceReturn2033 = 0; SensorCall(5361); return ReplaceReturn2033;}
}

int
Perl_magic_setpack(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5362);    dVAR;
    MAGIC *tmg;
    SV    *val;

    PERL_ARGS_ASSERT_MAGIC_SETPACK;

    /* in the code C<$tied{foo} = $val>, the "thing" that gets passed to
     * STORE() is not $val, but rather a PVLV (the sv in this call), whose
     * public flags indicate its value based on copying from $val. Doing
     * mg_set() on the PVLV temporarily does SvMAGICAL_off(), then calls us.
     * So STORE()'s $_[2] arg is a temporarily disarmed PVLV. This goes
     * wrong if $val happened to be tainted, as sv hasn't got magic
     * enabled, even though taint magic is in the chain. In which case,
     * fake up a temporary tainted value (this is easier than temporarily
     * re-enabling magic on sv). */

    SensorCall(5365);if (PL_tainting && (tmg = mg_find(sv, PERL_MAGIC_taint))
	&& (tmg->mg_len & 1))
    {
	SensorCall(5363);val = sv_mortalcopy(sv);
	SvTAINTED_on(val);
    }
    else
	{/*81*/SensorCall(5364);val = sv;/*82*/}

    magic_methcall1(sv, mg, "STORE", G_DISCARD, 2, val);
    {int  ReplaceReturn2032 = 0; SensorCall(5366); return ReplaceReturn2032;}
}

int
Perl_magic_clearpack(pTHX_ SV *sv, MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_CLEARPACK;

    SensorCall(5367);if (mg->mg_type == PERL_MAGIC_tiedscalar) {/*3*/{int  ReplaceReturn2031 = 0; SensorCall(5368); return ReplaceReturn2031;}/*4*/}
    {int  ReplaceReturn2030 = magic_methpack(sv,mg,"DELETE"); SensorCall(5369); return ReplaceReturn2030;}
}


U32
Perl_magic_sizepack(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5370);    dVAR;
    I32 retval = 0;
    SV* retsv;

    PERL_ARGS_ASSERT_MAGIC_SIZEPACK;

    retsv = magic_methcall1(sv, mg, "FETCHSIZE", 0, 1, NULL);
    SensorCall(5374);if (retsv) {
	SensorCall(5371);retval = SvIV(retsv)-1;
	SensorCall(5373);if (retval < -1)
	    {/*123*/SensorCall(5372);Perl_croak(aTHX_ "FETCHSIZE returned a negative value");/*124*/}
    }
    {U32  ReplaceReturn2029 = (U32) retval; SensorCall(5375); return ReplaceReturn2029;}
}

int
Perl_magic_wipepack(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5376);    dVAR;

    PERL_ARGS_ASSERT_MAGIC_WIPEPACK;

    Perl_magic_methcall(aTHX_ sv, mg, "CLEAR", G_DISCARD, 0);
    {int  ReplaceReturn2028 = 0; SensorCall(5377); return ReplaceReturn2028;}
}

int
Perl_magic_nextpack(pTHX_ SV *sv, MAGIC *mg, SV *key)
{
SensorCall(5378);    dVAR;
    SV* ret;

    PERL_ARGS_ASSERT_MAGIC_NEXTPACK;

    ret = SvOK(key) ? Perl_magic_methcall(aTHX_ sv, mg, "NEXTKEY", 0, 1, key)
	: Perl_magic_methcall(aTHX_ sv, mg, "FIRSTKEY", 0, 0);
    SensorCall(5379);if (ret)
	sv_setsv(key,ret);
    {int  ReplaceReturn2027 = 0; SensorCall(5380); return ReplaceReturn2027;}
}

int
Perl_magic_existspack(pTHX_ SV *sv, const MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_EXISTSPACK;

    {int  ReplaceReturn2026 = magic_methpack(sv,mg,"EXISTS"); SensorCall(5381); return ReplaceReturn2026;}
}

SV *
Perl_magic_scalarpack(pTHX_ HV *hv, MAGIC *mg)
{
SensorCall(5382);    dVAR;
    SV *retval;
    SV * const tied = SvTIED_obj(MUTABLE_SV(hv), mg);
    HV * const pkg = SvSTASH((const SV *)SvRV(tied));
   
    PERL_ARGS_ASSERT_MAGIC_SCALARPACK;

    SensorCall(5387);if (!gv_fetchmethod_autoload(pkg, "SCALAR", FALSE)) {
        SensorCall(5383);SV *key;
        SensorCall(5384);if (HvEITER_get(hv))
            /* we are in an iteration so the hash cannot be empty */
            return &PL_sv_yes;
        /* no xhv_eiter so now use FIRSTKEY */
        SensorCall(5385);key = sv_newmortal();
        magic_nextpack(MUTABLE_SV(hv), mg, key);
        HvEITER_set(hv, NULL);     /* need to reset iterator */
        {SV * ReplaceReturn2025 = SvOK(key) ? &PL_sv_yes : &PL_sv_no; SensorCall(5386); return ReplaceReturn2025;}
    }
   
    /* there is a SCALAR method that we can call */
    SensorCall(5388);retval = Perl_magic_methcall(aTHX_ MUTABLE_SV(hv), mg, "SCALAR", 0, 0);
    SensorCall(5389);if (!retval)
	retval = &PL_sv_undef;
    {SV * ReplaceReturn2024 = retval; SensorCall(5390); return ReplaceReturn2024;}
}

int
Perl_magic_setdbline(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5391);    dVAR;
    GV * const gv = PL_DBline;
    const I32 i = SvTRUE(sv);
    SV ** const svp = av_fetch(GvAV(gv),
		     atoi(MgPV_nolen_const(mg)), FALSE);

    PERL_ARGS_ASSERT_MAGIC_SETDBLINE;

    SensorCall(5395);if (svp && SvIOKp(*svp)) {
	SensorCall(5392);OP * const o = INT2PTR(OP*,SvIVX(*svp));
	SensorCall(5394);if (o) {
	    /* set or clear breakpoint in the relevant control op */
	    SensorCall(5393);if (i)
		o->op_flags |= OPf_SPECIAL;
	    else
		o->op_flags &= ~OPf_SPECIAL;
	}
    }
    {int  ReplaceReturn2023 = 0; SensorCall(5396); return ReplaceReturn2023;}
}

int
Perl_magic_getarylen(pTHX_ SV *sv, const MAGIC *mg)
{
SensorCall(5397);    dVAR;
    AV * const obj = MUTABLE_AV(mg->mg_obj);

    PERL_ARGS_ASSERT_MAGIC_GETARYLEN;

    SensorCall(5398);if (obj) {
	sv_setiv(sv, AvFILL(obj));
    } else {
	SvOK_off(sv);
    }
    {int  ReplaceReturn2022 = 0; SensorCall(5399); return ReplaceReturn2022;}
}

int
Perl_magic_setarylen(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5400);    dVAR;
    AV * const obj = MUTABLE_AV(mg->mg_obj);

    PERL_ARGS_ASSERT_MAGIC_SETARYLEN;

    SensorCall(5402);if (obj) {
	av_fill(obj, SvIV(sv));
    } else {
	SensorCall(5401);Perl_ck_warner(aTHX_ packWARN(WARN_MISC),
		       "Attempt to set length of freed array");
    }
    {int  ReplaceReturn2021 = 0; SensorCall(5403); return ReplaceReturn2021;}
}

int
Perl_magic_freearylen_p(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5404);    dVAR;

    PERL_ARGS_ASSERT_MAGIC_FREEARYLEN_P;
    PERL_UNUSED_ARG(sv);

    /* during global destruction, mg_obj may already have been freed */
    SensorCall(5406);if (PL_in_clean_all)
	{/*5*/{int  ReplaceReturn2020 = 0; SensorCall(5405); return ReplaceReturn2020;}/*6*/}

    SensorCall(5407);mg = mg_find (mg->mg_obj, PERL_MAGIC_arylen);

    SensorCall(5409);if (mg) {
	/* arylen scalar holds a pointer back to the array, but doesn't own a
	   reference. Hence the we (the array) are about to go away with it
	   still pointing at us. Clear its pointer, else it would be pointing
	   at free memory. See the comment in sv_magic about reference loops,
	   and why it can't own a reference to us.  */
	SensorCall(5408);mg->mg_obj = 0;
    }
    {int  ReplaceReturn2019 = 0; SensorCall(5410); return ReplaceReturn2019;}
}

int
Perl_magic_getpos(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5411);    dVAR;
    SV* const lsv = LvTARG(sv);

    PERL_ARGS_ASSERT_MAGIC_GETPOS;
    PERL_UNUSED_ARG(mg);

    SensorCall(5417);if (SvTYPE(lsv) >= SVt_PVMG && SvMAGIC(lsv)) {
	SensorCall(5412);MAGIC * const found = mg_find(lsv, PERL_MAGIC_regex_global);
	SensorCall(5416);if (found && found->mg_len >= 0) {
	    SensorCall(5413);I32 i = found->mg_len;
	    SensorCall(5414);if (DO_UTF8(lsv))
		sv_pos_b2u(lsv, &i);
	    sv_setiv(sv, i);
	    {int  ReplaceReturn2018 = 0; SensorCall(5415); return ReplaceReturn2018;}
	}
    }
    SvOK_off(sv);
    {int  ReplaceReturn2017 = 0; SensorCall(5418); return ReplaceReturn2017;}
}

int
Perl_magic_setpos(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5419);    dVAR;
    SV* const lsv = LvTARG(sv);
    SSize_t pos;
    STRLEN len;
    STRLEN ulen = 0;
    MAGIC* found;

    PERL_ARGS_ASSERT_MAGIC_SETPOS;
    PERL_UNUSED_ARG(mg);

    SensorCall(5420);if (SvTYPE(lsv) >= SVt_PVMG && SvMAGIC(lsv))
	found = mg_find(lsv, PERL_MAGIC_regex_global);
    else
	found = NULL;
    SensorCall(5427);if (!found) {
	SensorCall(5421);if (!SvOK(sv))
	    {/*83*/{int  ReplaceReturn2016 = 0; SensorCall(5422); return ReplaceReturn2016;}/*84*/}
#ifdef PERL_OLD_COPY_ON_WRITE
    if (SvIsCOW(lsv))
        sv_force_normal_flags(lsv, 0);
#endif
	SensorCall(5423);found = sv_magicext(lsv, NULL, PERL_MAGIC_regex_global, &PL_vtbl_mglob,
			    NULL, 0);
    }
    else {/*85*/SensorCall(5424);if (!SvOK(sv)) {
	SensorCall(5425);found->mg_len = -1;
	{int  ReplaceReturn2015 = 0; SensorCall(5426); return ReplaceReturn2015;}
    ;/*86*/}}
    SensorCall(5428);len = SvPOK(lsv) ? SvCUR(lsv) : sv_len(lsv);

    pos = SvIV(sv);

    SensorCall(5432);if (DO_UTF8(lsv)) {
	SensorCall(5429);ulen = sv_len_utf8(lsv);
	SensorCall(5431);if (ulen)
	    {/*87*/SensorCall(5430);len = ulen;/*88*/}
    }

    SensorCall(5438);if (pos < 0) {
	SensorCall(5433);pos += len;
	SensorCall(5435);if (pos < 0)
	    {/*89*/SensorCall(5434);pos = 0;/*90*/}
    }
    else {/*91*/SensorCall(5436);if (pos > (SSize_t)len)
	{/*93*/SensorCall(5437);pos = len;/*94*/}/*92*/}

    SensorCall(5440);if (ulen) {
	SensorCall(5439);I32 p = pos;
	sv_pos_u2b(lsv, &p, 0);
	pos = p;
    }

    SensorCall(5441);found->mg_len = pos;
    found->mg_flags &= ~MGf_MINMATCH;

    {int  ReplaceReturn2014 = 0; SensorCall(5442); return ReplaceReturn2014;}
}

int
Perl_magic_getsubstr(pTHX_ SV *sv, MAGIC *mg)
{
    SensorCall(5443);STRLEN len;
    SV * const lsv = LvTARG(sv);
    const char * const tmps = SvPV_const(lsv,len);
    STRLEN offs = LvTARGOFF(sv);
    STRLEN rem = LvTARGLEN(sv);
    const bool negoff = LvFLAGS(sv) & 1;
    const bool negrem = LvFLAGS(sv) & 2;

    PERL_ARGS_ASSERT_MAGIC_GETSUBSTR;
    PERL_UNUSED_ARG(mg);

    SensorCall(5446);if (!translate_substr_offsets(
	    SvUTF8(lsv) ? sv_len_utf8(lsv) : len,
	    negoff ? -(IV)offs : (IV)offs, !negoff,
	    negrem ? -(IV)rem  : (IV)rem,  !negrem, &offs, &rem
    )) {
	SensorCall(5444);Perl_ck_warner(aTHX_ packWARN(WARN_SUBSTR), "substr outside of string");
	sv_setsv_nomg(sv, &PL_sv_undef);
	{int  ReplaceReturn2013 = 0; SensorCall(5445); return ReplaceReturn2013;}
    }

    SensorCall(5447);if (SvUTF8(lsv))
	offs = sv_pos_u2b_flags(lsv, offs, &rem, SV_CONST_RETURN);
    sv_setpvn(sv, tmps + offs, rem);
    SensorCall(5448);if (SvUTF8(lsv))
        SvUTF8_on(sv);
    {int  ReplaceReturn2012 = 0; SensorCall(5449); return ReplaceReturn2012;}
}

int
Perl_magic_setsubstr(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5450);    dVAR;
    STRLEN len, lsv_len, oldtarglen, newtarglen;
    const char * const tmps = SvPV_const(sv, len);
    SV * const lsv = LvTARG(sv);
    STRLEN lvoff = LvTARGOFF(sv);
    STRLEN lvlen = LvTARGLEN(sv);
    const bool negoff = LvFLAGS(sv) & 1;
    const bool neglen = LvFLAGS(sv) & 2;

    PERL_ARGS_ASSERT_MAGIC_SETSUBSTR;
    PERL_UNUSED_ARG(mg);

    SvGETMAGIC(lsv);
    SensorCall(5452);if (SvROK(lsv))
	{/*109*/SensorCall(5451);Perl_ck_warner(aTHX_ packWARN(WARN_SUBSTR),
			    "Attempt to use reference as lvalue in substr"
	);/*110*/}
    SensorCall(5453);if (SvUTF8(lsv)) lsv_len = sv_len_utf8(lsv);
    else (void)SvPV_nomg(lsv,lsv_len);
    SensorCall(5455);if (!translate_substr_offsets(
	    lsv_len,
	    negoff ? -(IV)lvoff : (IV)lvoff, !negoff,
	    neglen ? -(IV)lvlen : (IV)lvlen, !neglen, &lvoff, &lvlen
    ))
	{/*111*/SensorCall(5454);Perl_croak(aTHX_ "substr outside of string");/*112*/}
    SensorCall(5456);oldtarglen = lvlen;
    SensorCall(5461);if (DO_UTF8(sv)) {
	sv_utf8_upgrade(lsv);
	SensorCall(5457);lvoff = sv_pos_u2b_flags(lsv, lvoff, &lvlen, SV_CONST_RETURN);
	sv_insert_flags(lsv, lvoff, lvlen, tmps, len, 0);
	newtarglen = sv_len_utf8(sv);
	SvUTF8_on(lsv);
    }
    else {/*113*/SensorCall(5458);if (lsv && SvUTF8(lsv)) {
	SensorCall(5459);const char *utf8;
	lvoff = sv_pos_u2b_flags(lsv, lvoff, &lvlen, SV_CONST_RETURN);
	newtarglen = len;
	utf8 = (char*)bytes_to_utf8((U8*)tmps, &len);
	sv_insert_flags(lsv, lvoff, lvlen, utf8, len, 0);
	Safefree(utf8);
    }
    else {
	sv_insert_flags(lsv, lvoff, lvlen, tmps, len, 0);
	SensorCall(5460);newtarglen = len;
    ;/*114*/}}
    SensorCall(5462);if (!neglen) LvTARGLEN(sv) = newtarglen;
    SensorCall(5463);if (negoff)  LvTARGOFF(sv) += newtarglen - oldtarglen;

    {int  ReplaceReturn2011 = 0; SensorCall(5464); return ReplaceReturn2011;}
}

int
Perl_magic_gettaint(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5465);    dVAR;

    PERL_ARGS_ASSERT_MAGIC_GETTAINT;
    PERL_UNUSED_ARG(sv);

    TAINT_IF((PL_localizing != 1) && (mg->mg_len & 1));
    {int  ReplaceReturn2010 = 0; SensorCall(5466); return ReplaceReturn2010;}
}

int
Perl_magic_settaint(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5467);    dVAR;

    PERL_ARGS_ASSERT_MAGIC_SETTAINT;
    PERL_UNUSED_ARG(sv);

    /* update taint status */
    SensorCall(5470);if (PL_tainted)
	{/*115*/SensorCall(5468);mg->mg_len |= 1;/*116*/}
    else
	{/*117*/SensorCall(5469);mg->mg_len &= ~1;/*118*/}
    {int  ReplaceReturn2009 = 0; SensorCall(5471); return ReplaceReturn2009;}
}

int
Perl_magic_getvec(pTHX_ SV *sv, MAGIC *mg)
{
    SensorCall(5472);SV * const lsv = LvTARG(sv);

    PERL_ARGS_ASSERT_MAGIC_GETVEC;
    PERL_UNUSED_ARG(mg);

    SensorCall(5473);if (lsv)
	sv_setuv(sv, do_vecget(lsv, LvTARGOFF(sv), LvTARGLEN(sv)));
    else
	SvOK_off(sv);

    {int  ReplaceReturn2008 = 0; SensorCall(5474); return ReplaceReturn2008;}
}

int
Perl_magic_setvec(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5475);    PERL_ARGS_ASSERT_MAGIC_SETVEC;
    PERL_UNUSED_ARG(mg);
    do_vecset(sv);	/* XXX slurp this routine */
    {int  ReplaceReturn2007 = 0; SensorCall(5476); return ReplaceReturn2007;}
}

int
Perl_magic_setvstring(pTHX_ SV *sv, MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_SETVSTRING;

    SensorCall(5477);if (SvPOKp(sv)) {
	SensorCall(5478);SV * const vecsv = sv_newmortal();
	scan_vstring(mg->mg_ptr, mg->mg_ptr + mg->mg_len, vecsv);
	SensorCall(5480);if (sv_eq_flags(vecsv, sv, 0 /*nomg*/)) {/*121*/{int  ReplaceReturn2006 = 0; SensorCall(5479); return ReplaceReturn2006;}/*122*/}
    }
    {int  ReplaceReturn2005 = sv_unmagic(sv, mg->mg_type); SensorCall(5481); return ReplaceReturn2005;}
}

int
Perl_magic_getdefelem(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5482);    dVAR;
    SV *targ = NULL;

    PERL_ARGS_ASSERT_MAGIC_GETDEFELEM;

    SensorCall(5491);if (LvTARGLEN(sv)) {
	SensorCall(5483);if (mg->mg_obj) {
	    SensorCall(5484);SV * const ahv = LvTARG(sv);
	    HE * const he = hv_fetch_ent(MUTABLE_HV(ahv), mg->mg_obj, FALSE, 0);
            SensorCall(5485);if (he)
                targ = HeVAL(he);
	}
	else {
	    SensorCall(5486);AV *const av = MUTABLE_AV(LvTARG(sv));
	    SensorCall(5488);if ((I32)LvTARGOFF(sv) <= AvFILL(av))
		{/*23*/SensorCall(5487);targ = AvARRAY(av)[LvTARGOFF(sv)];/*24*/}
	}
	SensorCall(5490);if (targ && (targ != &PL_sv_undef)) {
	    /* somebody else defined it for us */
	    SvREFCNT_dec(LvTARG(sv));
	    LvTARG(sv) = SvREFCNT_inc_simple_NN(targ);
	    LvTARGLEN(sv) = 0;
	    SvREFCNT_dec(mg->mg_obj);
	    SensorCall(5489);mg->mg_obj = NULL;
	    mg->mg_flags &= ~MGf_REFCOUNTED;
	}
    }
    else
	targ = LvTARG(sv);
    sv_setsv(sv, targ ? targ : &PL_sv_undef);
    {int  ReplaceReturn2004 = 0; SensorCall(5492); return ReplaceReturn2004;}
}

int
Perl_magic_setdefelem(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5493);    PERL_ARGS_ASSERT_MAGIC_SETDEFELEM;
    PERL_UNUSED_ARG(mg);
    SensorCall(5494);if (LvTARGLEN(sv))
	vivify_defelem(sv);
    SensorCall(5495);if (LvTARG(sv)) {
	sv_setsv(LvTARG(sv), sv);
	SvSETMAGIC(LvTARG(sv));
    }
    {int  ReplaceReturn2003 = 0; SensorCall(5496); return ReplaceReturn2003;}
}

void
Perl_vivify_defelem(pTHX_ SV *sv)
{
SensorCall(5497);    dVAR;
    MAGIC *mg;
    SV *value = NULL;

    PERL_ARGS_ASSERT_VIVIFY_DEFELEM;

    SensorCall(5499);if (!LvTARGLEN(sv) || !(mg = mg_find(sv, PERL_MAGIC_defelem)))
	{/*143*/SensorCall(5498);return;/*144*/}
    SensorCall(5509);if (mg->mg_obj) {
	SensorCall(5500);SV * const ahv = LvTARG(sv);
	HE * const he = hv_fetch_ent(MUTABLE_HV(ahv), mg->mg_obj, TRUE, 0);
        SensorCall(5501);if (he)
            value = HeVAL(he);
	SensorCall(5503);if (!value || value == &PL_sv_undef)
	    {/*145*/SensorCall(5502);Perl_croak(aTHX_ PL_no_helem_sv, SVfARG(mg->mg_obj));/*146*/}
    }
    else {
	SensorCall(5504);AV *const av = MUTABLE_AV(LvTARG(sv));
	SensorCall(5508);if ((I32)LvTARGLEN(sv) < 0 && (I32)LvTARGOFF(sv) > AvFILL(av))
	    LvTARG(sv) = NULL;	/* array can't be extended */
	else {
	    SensorCall(5505);SV* const * const svp = av_fetch(av, LvTARGOFF(sv), TRUE);
	    SensorCall(5507);if (!svp || (value = *svp) == &PL_sv_undef)
		{/*147*/SensorCall(5506);Perl_croak(aTHX_ PL_no_aelem, (I32)LvTARGOFF(sv));/*148*/}
	}
    }
    SvREFCNT_inc_simple_void(value);
    SvREFCNT_dec(LvTARG(sv));
    LvTARG(sv) = value;
    LvTARGLEN(sv) = 0;
    SvREFCNT_dec(mg->mg_obj);
    SensorCall(5510);mg->mg_obj = NULL;
    mg->mg_flags &= ~MGf_REFCOUNTED;
SensorCall(5511);}

int
Perl_magic_killbackrefs(pTHX_ SV *sv, MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_KILLBACKREFS;
    SensorCall(5512);Perl_sv_kill_backrefs(aTHX_ sv, MUTABLE_AV(mg->mg_obj));
    {int  ReplaceReturn2002 = 0; SensorCall(5513); return ReplaceReturn2002;}
}

int
Perl_magic_setmglob(pTHX_ SV *sv, MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_SETMGLOB;
    PERL_UNUSED_CONTEXT;
    PERL_UNUSED_ARG(sv);
    mg->mg_len = -1;
    {int  ReplaceReturn2001 = 0; SensorCall(5514); return ReplaceReturn2001;}
}

int
Perl_magic_setuvar(pTHX_ SV *sv, MAGIC *mg)
{
    SensorCall(5515);const struct ufuncs * const uf = (struct ufuncs *)mg->mg_ptr;

    PERL_ARGS_ASSERT_MAGIC_SETUVAR;

    SensorCall(5517);if (uf && uf->uf_set)
	{/*119*/SensorCall(5516);(*uf->uf_set)(aTHX_ uf->uf_index, sv);/*120*/}
    {int  ReplaceReturn2000 = 0; SensorCall(5518); return ReplaceReturn2000;}
}

int
Perl_magic_setregexp(pTHX_ SV *sv, MAGIC *mg)
{
    SensorCall(5519);const char type = mg->mg_type;

    PERL_ARGS_ASSERT_MAGIC_SETREGEXP;

    SensorCall(5523);if (type == PERL_MAGIC_qr) {
    } else {/*95*/SensorCall(5520);if (type == PERL_MAGIC_bm) {
	SvTAIL_off(sv);
	SvVALID_off(sv);
    } else {/*97*/SensorCall(5521);if (type == PERL_MAGIC_study) {
	SensorCall(5522);if (!isGV_with_GP(sv))
	    SvSCREAM_off(sv);
    } else {
	assert(type == PERL_MAGIC_fm);
    ;/*98*/}/*96*/}}
    {int  ReplaceReturn1999 = sv_unmagic(sv, type); SensorCall(5524); return ReplaceReturn1999;}
}

#ifdef USE_LOCALE_COLLATE
int
Perl_magic_setcollxfrm(pTHX_ SV *sv, MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_SETCOLLXFRM;

    /*
     * RenE<eacute> Descartes said "I think not."
     * and vanished with a faint plop.
     */
    PERL_UNUSED_CONTEXT;
    PERL_UNUSED_ARG(sv);
    SensorCall(5526);if (mg->mg_ptr) {
	Safefree(mg->mg_ptr);
	SensorCall(5525);mg->mg_ptr = NULL;
	mg->mg_len = -1;
    }
    {int  ReplaceReturn1998 = 0; SensorCall(5527); return ReplaceReturn1998;}
}
#endif /* USE_LOCALE_COLLATE */

/* Just clear the UTF-8 cache data. */
int
Perl_magic_setutf8(pTHX_ SV *sv, MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_SETUTF8;
    PERL_UNUSED_CONTEXT;
    PERL_UNUSED_ARG(sv);
    Safefree(mg->mg_ptr);	/* The mg_ptr holds the pos cache. */
    mg->mg_ptr = NULL;
    mg->mg_len = -1;		/* The mg_len holds the len cache. */
    {int  ReplaceReturn1997 = 0; SensorCall(5528); return ReplaceReturn1997;}
}

int
Perl_magic_set(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5529);    dVAR;
    register const char *s;
    register I32 paren;
    register const REGEXP * rx;
    const char * const remaining = mg->mg_ptr + 1;
    I32 i;
    STRLEN len;
    MAGIC *tmg;

    PERL_ARGS_ASSERT_MAGIC_SET;

    SensorCall(5666);switch (*mg->mg_ptr) {
    case '\015': /* $^MATCH */
      SensorCall(5530);if (strEQ(remaining, "ATCH"))
          {/*51*/SensorCall(5531);goto do_match;/*52*/}
    case '`': /* ${^PREMATCH} caught below */
      do_prematch:
      SensorCall(5532);paren = RX_BUFF_IDX_PREMATCH;
      SensorCall(5533);goto setparen;
    case '\'': /* ${^POSTMATCH} caught below */
      do_postmatch:
      SensorCall(5534);paren = RX_BUFF_IDX_POSTMATCH;
      SensorCall(5535);goto setparen;
    case '&':
      do_match:
      SensorCall(5536);paren = RX_BUFF_IDX_FULLMATCH;
      SensorCall(5537);goto setparen;
    case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
      SensorCall(5538);paren = atoi(mg->mg_ptr);
      setparen:
	SensorCall(5541);if (PL_curpm && (rx = PM_GETRE(PL_curpm))) {
            CALLREG_NUMBUF_STORE((REGEXP * const)rx,paren,sv);
	} else {
            /* Croak with a READONLY error when a numbered match var is
             * set without a previous pattern match. Unless it's C<local $1>
             */
            SensorCall(5539);if (!PL_localizing) {
                SensorCall(5540);Perl_croak_no_modify(aTHX);
            }
        }
        SensorCall(5542);break;
    case '\001':	/* ^A */
	sv_setsv(PL_bodytarget, sv);
	FmLINES(PL_bodytarget) = 0;
	SensorCall(5546);if (SvPOK(PL_bodytarget)) {
	    SensorCall(5543);char *s = SvPVX(PL_bodytarget);
	    SensorCall(5545);while ( ((s = strchr(s, '\n'))) ) {
		FmLINES(PL_bodytarget)++;
		SensorCall(5544);s++;
	    }
	}
	/* mg_set() has temporarily made sv non-magical */
	SensorCall(5548);if (PL_tainting) {
	    SensorCall(5547);if ((tmg = mg_find(sv,PERL_MAGIC_taint)) && tmg->mg_len & 1)
		SvTAINTED_on(PL_bodytarget);
	    else
		SvTAINTED_off(PL_bodytarget);
	}
	SensorCall(5549);break;
    case '\003':	/* ^C */
	PL_minus_c = cBOOL(SvIV(sv));
	SensorCall(5550);break;

    case '\004':	/* ^D */
#ifdef DEBUGGING
	s = SvPV_nolen_const(sv);
	PL_debug = get_debug_opts(&s, 0) | DEBUG_TOP_FLAG;
	if (DEBUG_x_TEST || DEBUG_B_TEST)
	    dump_all_perl(!DEBUG_B_TEST);
#else
	PL_debug = (SvIV(sv)) | DEBUG_TOP_FLAG;
#endif
	SensorCall(5551);break;
    case '\005':  /* ^E */
	SensorCall(5552);if (*(mg->mg_ptr+1) == '\0') {
#ifdef VMS
	    set_vaxc_errno(SvIV(sv));
#else
#  ifdef WIN32
	    SetLastError( SvIV(sv) );
#  else
#    ifdef OS2
	    os2_setsyserrno(SvIV(sv));
#    else
	    /* will anyone ever use this? */
	    SETERRNO(SvIV(sv), 4);
#    endif
#  endif
#endif
	}
	else {/*53*/SensorCall(5553);if (strEQ(mg->mg_ptr+1, "NCODING")) {
	    SvREFCNT_dec(PL_encoding);
	    SensorCall(5554);if (SvOK(sv) || SvGMAGICAL(sv)) {
		PL_encoding = newSVsv(sv);
	    }
	    else {
		PL_encoding = NULL;
	    }
	;/*54*/}}
	SensorCall(5555);break;
    case '\006':	/* ^F */
	PL_maxsysfd = SvIV(sv);
	SensorCall(5556);break;
    case '\010':	/* ^H */
	PL_hints = SvIV(sv);
	SensorCall(5557);break;
    case '\011':	/* ^I */ /* NOT \t in EBCDIC */
	Safefree(PL_inplace);
	PL_inplace = SvOK(sv) ? savesvpv(sv) : NULL;
	SensorCall(5558);break;
    case '\017':	/* ^O */
	SensorCall(5559);if (*(mg->mg_ptr+1) == '\0') {
	    Safefree(PL_osname);
	    PL_osname = NULL;
	    SensorCall(5560);if (SvOK(sv)) {
		TAINT_PROPER("assigning to $^O");
		PL_osname = savesvpv(sv);
	    }
	}
	else {/*55*/SensorCall(5561);if (strEQ(mg->mg_ptr, "\017PEN")) {
	    SensorCall(5562);STRLEN len;
	    const char *const start = SvPV(sv, len);
	    const char *out = (const char*)memchr(start, '\0', len);
	    SV *tmp;


	    PL_compiling.cop_hints |= HINT_LEXICAL_IO_IN | HINT_LEXICAL_IO_OUT;
	    PL_hints |= HINT_LEXICAL_IO_IN | HINT_LEXICAL_IO_OUT;

	    /* Opening for input is more common than opening for output, so
	       ensure that hints for input are sooner on linked list.  */
	    tmp = out ? newSVpvn_flags(out + 1, start + len - out - 1,
				       SvUTF8(sv))
		: newSVpvs_flags("", SvUTF8(sv));
	    (void)hv_stores(GvHV(PL_hintgv), "open>", tmp);
	    mg_set(tmp);

	    tmp = newSVpvn_flags(start, out ? (STRLEN)(out - start) : len,
				        SvUTF8(sv));
	    (void)hv_stores(GvHV(PL_hintgv), "open<", tmp);
	    mg_set(tmp);
	;/*56*/}}
	SensorCall(5563);break;
    case '\020':	/* ^P */
      SensorCall(5564);if (*remaining == '\0') { /* ^P */
          PL_perldb = SvIV(sv);
          SensorCall(5565);if (PL_perldb && !PL_DBsingle)
              init_debugger();
          SensorCall(5566);break;
      } else {/*57*/SensorCall(5567);if (strEQ(remaining, "REMATCH")) { /* $^PREMATCH */
          SensorCall(5568);goto do_prematch;
      } else {/*59*/SensorCall(5569);if (strEQ(remaining, "OSTMATCH")) { /* $^POSTMATCH */
          SensorCall(5570);goto do_postmatch;
      ;/*60*/}/*58*/}}
      SensorCall(5571);break;
    case '\024':	/* ^T */
#ifdef BIG_TIME
	PL_basetime = (Time_t)(SvNOK(sv) ? SvNVX(sv) : sv_2nv(sv));
#else
	PL_basetime = (Time_t)SvIV(sv);
#endif
	SensorCall(5572);break;
    case '\025':	/* ^UTF8CACHE */
	 SensorCall(5573);if (strEQ(mg->mg_ptr+1, "TF8CACHE")) {
	     PL_utf8cache = (signed char) sv_2iv(sv);
	 }
	 SensorCall(5574);break;
    case '\027':	/* ^W & $^WARNING_BITS */
	SensorCall(5575);if (*(mg->mg_ptr+1) == '\0') {
	    SensorCall(5576);if ( ! (PL_dowarn & G_WARN_ALL_MASK)) {
	        SensorCall(5577);i = SvIV(sv);
	        PL_dowarn = (PL_dowarn & ~G_WARN_ON)
		    		| (i ? G_WARN_ON : G_WARN_OFF) ;
	    }
	}
	else {/*61*/SensorCall(5578);if (strEQ(mg->mg_ptr+1, "ARNING_BITS")) {
	    SensorCall(5579);if ( ! (PL_dowarn & G_WARN_ALL_MASK)) {
		SensorCall(5580);if (!SvPOK(sv)) {
	            PL_compiling.cop_warnings = pWARN_STD;
		    SensorCall(5581);break;
		}
		{
		    SensorCall(5582);STRLEN len, i;
		    int accumulate = 0 ;
		    int any_fatals = 0 ;
		    const char * const ptr = SvPV_const(sv, len) ;
		    SensorCall(5584);for (i = 0 ; i < len ; ++i) {
		        SensorCall(5583);accumulate |= ptr[i] ;
		        any_fatals |= (ptr[i] & 0xAA) ;
		    }
		    SensorCall(5590);if (!accumulate) {
		        SensorCall(5585);if (!specialWARN(PL_compiling.cop_warnings))
			    PerlMemShared_free(PL_compiling.cop_warnings);
			PL_compiling.cop_warnings = pWARN_NONE;
		    }
		    /* Yuck. I can't see how to abstract this:  */
		    else {/*63*/SensorCall(5586);if (isWARN_on(((STRLEN *)SvPV_nolen_const(sv)) - 1,
				       WARN_ALL) && !any_fatals) {
			SensorCall(5587);if (!specialWARN(PL_compiling.cop_warnings))
			    PerlMemShared_free(PL_compiling.cop_warnings);
	                PL_compiling.cop_warnings = pWARN_ALL;
	                PL_dowarn |= G_WARN_ONCE ;
	            }
                    else {
			SensorCall(5588);STRLEN len;
			const char *const p = SvPV_const(sv, len);

			PL_compiling.cop_warnings
			    = Perl_new_warnings_bitfield(aTHX_ PL_compiling.cop_warnings,
							 p, len);

	                SensorCall(5589);if (isWARN_on(PL_compiling.cop_warnings, WARN_ONCE))
	                    PL_dowarn |= G_WARN_ONCE ;
	            ;/*64*/}}

		}
	    }
	;/*62*/}}
	SensorCall(5591);break;
    case '.':
	SensorCall(5592);if (PL_localizing) {
	    SensorCall(5593);if (PL_localizing == 1)
		SAVESPTR(PL_last_in_gv);
	}
	else if (SvOK(sv) && GvIO(PL_last_in_gv))
	    IoLINES(GvIOp(PL_last_in_gv)) = SvIV(sv);
	SensorCall(5594);break;
    case '^':
	Safefree(IoTOP_NAME(GvIOp(PL_defoutgv)));
	SensorCall(5595);s = IoTOP_NAME(GvIOp(PL_defoutgv)) = savesvpv(sv);
	IoTOP_GV(GvIOp(PL_defoutgv)) =  gv_fetchsv(sv, GV_ADD, SVt_PVIO);
	SensorCall(5596);break;
    case '~':
	Safefree(IoFMT_NAME(GvIOp(PL_defoutgv)));
	SensorCall(5597);s = IoFMT_NAME(GvIOp(PL_defoutgv)) = savesvpv(sv);
	IoFMT_GV(GvIOp(PL_defoutgv)) =  gv_fetchsv(sv, GV_ADD, SVt_PVIO);
	SensorCall(5598);break;
    case '=':
	IoPAGE_LEN(GvIOp(PL_defoutgv)) = (SvIV(sv));
	SensorCall(5599);break;
    case '-':
	IoLINES_LEFT(GvIOp(PL_defoutgv)) = (SvIV(sv));
	SensorCall(5600);if (IoLINES_LEFT(GvIOp(PL_defoutgv)) < 0L)
		IoLINES_LEFT(GvIOp(PL_defoutgv)) = 0L;
	SensorCall(5601);break;
    case '%':
	IoPAGE(GvIOp(PL_defoutgv)) = (SvIV(sv));
	SensorCall(5602);break;
    case '|':
	{
	    SensorCall(5603);IO * const io = GvIO(PL_defoutgv);
	    SensorCall(5605);if(!io)
	      {/*65*/SensorCall(5604);break;/*66*/}
	    SensorCall(5608);if ((SvIV(sv)) == 0)
		IoFLAGS(io) &= ~IOf_FLUSH;
	    else {
		SensorCall(5606);if (!(IoFLAGS(io) & IOf_FLUSH)) {
		    PerlIO *ofp = IoOFP(io);
		    SensorCall(5607);if (ofp)
			(void)PerlIO_flush(ofp);
		    IoFLAGS(io) |= IOf_FLUSH;
		}
	    }
	}
	SensorCall(5609);break;
    case '/':
	SvREFCNT_dec(PL_rs);
	PL_rs = newSVsv(sv);
	SensorCall(5610);break;
    case '\\':
	SvREFCNT_dec(PL_ors_sv);
	SensorCall(5611);if (SvOK(sv) || SvGMAGICAL(sv)) {
	    PL_ors_sv = newSVsv(sv);
	}
	else {
	    PL_ors_sv = NULL;
	}
	SensorCall(5612);break;
    case '[':
	SensorCall(5613);if (SvIV(sv) != 0)
	    {/*67*/SensorCall(5614);Perl_croak(aTHX_ "Assigning non-zero to $[ is no longer possible");/*68*/}
	SensorCall(5615);break;
    case '?':
#ifdef COMPLEX_STATUS
	if (PL_localizing == 2) {
	    SvUPGRADE(sv, SVt_PVLV);
	    PL_statusvalue = LvTARGOFF(sv);
	    PL_statusvalue_vms = LvTARGLEN(sv);
	}
	else
#endif
#ifdef VMSISH_STATUS
	if (VMSISH_STATUS)
	    STATUS_NATIVE_CHILD_SET((U32)SvIV(sv));
	else
#endif
	    STATUS_UNIX_EXIT_SET(SvIV(sv));
	SensorCall(5616);break;
    case '!':
        {
#ifdef VMS
#   define PERL_VMS_BANG vaxc$errno
#else
#   define PERL_VMS_BANG 0
#endif
	SETERRNO(SvIOK(sv) ? SvIVX(sv) : SvOK(sv) ? sv_2iv(sv) : 0,
		 (SvIV(sv) == EVMSERR) ? 4 : PERL_VMS_BANG);
	}
	SensorCall(5617);break;
    case '<':
	{
	SensorCall(5618);const IV new_uid = SvIV(sv);
	PL_delaymagic_uid = new_uid;
	SensorCall(5620);if (PL_delaymagic) {
	    PL_delaymagic |= DM_RUID;
	    SensorCall(5619);break;				/* don't do magic till later */
	}
#ifdef HAS_SETRUID
	(void)setruid((Uid_t)new_uid);
#else
#ifdef HAS_SETREUID
	SensorCall(5621);(void)setreuid((Uid_t)new_uid, (Uid_t)-1);
#else
#ifdef HAS_SETRESUID
      (void)setresuid((Uid_t)new_uid, (Uid_t)-1, (Uid_t)-1);
#else
	if (new_uid == PerlProc_geteuid()) {		/* special case $< = $> */
#ifdef PERL_DARWIN
	    /* workaround for Darwin's setuid peculiarity, cf [perl #24122] */
	    if (new_uid != 0 && PerlProc_getuid() == 0)
		(void)PerlProc_setuid(0);
#endif
	    (void)PerlProc_setuid(new_uid);
	} else {
	    Perl_croak(aTHX_ "setruid() not implemented");
	}
#endif
#endif
#endif
	SensorCall(5622);break;
	}
    case '>':
	{
	SensorCall(5623);const UV new_euid = SvIV(sv);
	PL_delaymagic_euid = new_euid;
	SensorCall(5625);if (PL_delaymagic) {
	    PL_delaymagic |= DM_EUID;
	    SensorCall(5624);break;				/* don't do magic till later */
	}
#ifdef HAS_SETEUID
	SensorCall(5626);(void)seteuid((Uid_t)new_euid);
#else
#ifdef HAS_SETREUID
	(void)setreuid((Uid_t)-1, (Uid_t)new_euid);
#else
#ifdef HAS_SETRESUID
	(void)setresuid((Uid_t)-1, (Uid_t)new_euid, (Uid_t)-1);
#else
	if (new_euid == PerlProc_getuid())		/* special case $> = $< */
	    PerlProc_setuid(new_euid);
	else {
	    Perl_croak(aTHX_ "seteuid() not implemented");
	}
#endif
#endif
#endif
	SensorCall(5627);break;
	}
    case '(':
	{
	SensorCall(5628);const UV new_gid = SvIV(sv);
	PL_delaymagic_gid = new_gid;
	SensorCall(5630);if (PL_delaymagic) {
	    PL_delaymagic |= DM_RGID;
	    SensorCall(5629);break;				/* don't do magic till later */
	}
#ifdef HAS_SETRGID
	(void)setrgid((Gid_t)new_gid);
#else
#ifdef HAS_SETREGID
	SensorCall(5631);(void)setregid((Gid_t)new_gid, (Gid_t)-1);
#else
#ifdef HAS_SETRESGID
      (void)setresgid((Gid_t)new_gid, (Gid_t)-1, (Gid_t) -1);
#else
	if (new_gid == PerlProc_getegid())			/* special case $( = $) */
	    (void)PerlProc_setgid(new_gid);
	else {
	    Perl_croak(aTHX_ "setrgid() not implemented");
	}
#endif
#endif
#endif
	SensorCall(5632);break;
	}
    case ')':
	{
	SensorCall(5633);UV new_egid;
#ifdef HAS_SETGROUPS
	{
	    const char *p = SvPV_const(sv, len);
            Groups_t *gary = NULL;
#ifdef _SC_NGROUPS_MAX
           int maxgrp = sysconf(_SC_NGROUPS_MAX);

           SensorCall(5634);if (maxgrp < 0)
               maxgrp = NGROUPS;
#else
           int maxgrp = NGROUPS;
#endif

            SensorCall(5636);while (isSPACE(*p))
                {/*69*/SensorCall(5635);++p;/*70*/}
            SensorCall(5637);new_egid = Atol(p);
            SensorCall(5646);for (i = 0; i < maxgrp; ++i) {
                SensorCall(5638);while (*p && !isSPACE(*p))
                    {/*71*/SensorCall(5639);++p;/*72*/}
                SensorCall(5641);while (isSPACE(*p))
                    {/*73*/SensorCall(5640);++p;/*74*/}
                SensorCall(5643);if (!*p)
                    {/*75*/SensorCall(5642);break;/*76*/}
                SensorCall(5644);if(!gary)
                    Newx(gary, i + 1, Groups_t);
                else
                    Renew(gary, i + 1, Groups_t);
                SensorCall(5645);gary[i] = Atol(p);
            }
            SensorCall(5648);if (i)
                {/*77*/SensorCall(5647);(void)setgroups(i, gary);/*78*/}
	    Safefree(gary);
	}
#else  /* HAS_SETGROUPS */
	new_egid = SvIV(sv);
#endif /* HAS_SETGROUPS */
	PL_delaymagic_egid = new_egid;
	SensorCall(5650);if (PL_delaymagic) {
	    PL_delaymagic |= DM_EGID;
	    SensorCall(5649);break;				/* don't do magic till later */
	}
#ifdef HAS_SETEGID
	SensorCall(5651);(void)setegid((Gid_t)new_egid);
#else
#ifdef HAS_SETREGID
	(void)setregid((Gid_t)-1, (Gid_t)new_egid);
#else
#ifdef HAS_SETRESGID
	(void)setresgid((Gid_t)-1, (Gid_t)new_egid, (Gid_t)-1);
#else
	if (new_egid == PerlProc_getgid())			/* special case $) = $( */
	    (void)PerlProc_setgid(new_egid);
	else {
	    Perl_croak(aTHX_ "setegid() not implemented");
	}
#endif
#endif
#endif
	SensorCall(5652);break;
	}
    case ':':
	PL_chopset = SvPV_force(sv,len);
	SensorCall(5653);break;
    case '$': /* $$ */
	/* Store the pid in mg->mg_obj so we can tell when a fork has
	   occurred.  mg->mg_obj points to *$ by default, so clear it. */
	SensorCall(5654);if (isGV(mg->mg_obj)) {
	    SensorCall(5655);if (mg->mg_flags & MGf_REFCOUNTED) /* probably never true */
		SvREFCNT_dec(mg->mg_obj);
	    SensorCall(5656);mg->mg_flags |= MGf_REFCOUNTED;
	    mg->mg_obj = newSViv((IV)PerlProc_getpid());
	}
	else sv_setiv(mg->mg_obj, (IV)PerlProc_getpid());
	SensorCall(5657);break;
    case '0':
	LOCK_DOLLARZERO_MUTEX;
#ifdef HAS_SETPROCTITLE
	/* The BSDs don't show the argv[] in ps(1) output, they
	 * show a string from the process struct and provide
	 * the setproctitle() routine to manipulate that. */
	if (PL_origalen != 1) {
	    s = SvPV_const(sv, len);
#   if __FreeBSD_version > 410001
	    /* The leading "-" removes the "perl: " prefix,
	     * but not the "(perl) suffix from the ps(1)
	     * output, because that's what ps(1) shows if the
	     * argv[] is modified. */
	    setproctitle("-%s", s);
#   else	/* old FreeBSDs, NetBSD, OpenBSD, anyBSD */
	    /* This doesn't really work if you assume that
	     * $0 = 'foobar'; will wipe out 'perl' from the $0
	     * because in ps(1) output the result will be like
	     * sprintf("perl: %s (perl)", s)
	     * I guess this is a security feature:
	     * one (a user process) cannot get rid of the original name.
	     * --jhi */
	    setproctitle("%s", s);
#   endif
	}
#elif defined(__hpux) && defined(PSTAT_SETCMD)
	if (PL_origalen != 1) {
	     union pstun un;
	     s = SvPV_const(sv, len);
	     un.pst_command = (char *)s;
	     pstat(PSTAT_SETCMD, un, len, 0, 0);
	}
#else
	SensorCall(5664);if (PL_origalen > 1) {
	    /* PL_origalen is set in perl_parse(). */
	    SensorCall(5658);s = SvPV_force(sv,len);
	    SensorCall(5660);if (len >= (STRLEN)PL_origalen-1) {
		/* Longer than original, will be truncated. We assume that
		 * PL_origalen bytes are available. */
		Copy(s, PL_origargv[0], PL_origalen-1, char);
	    }
	    else {
		/* Shorter than original, will be padded. */
#ifdef PERL_DARWIN
		/* Special case for Mac OS X: see [perl #38868] */
		const int pad = 0;
#else
		/* Is the space counterintuitive?  Yes.
		 * (You were expecting \0?)
		 * Does it work?  Seems to.  (In Linux 2.4.20 at least.)
		 * --jhi */
		SensorCall(5659);const int pad = ' ';
#endif
		Copy(s, PL_origargv[0], len, char);
		PL_origargv[0][len] = 0;
		memset(PL_origargv[0] + len + 1,
		       pad,  PL_origalen - len - 1);
	    }
	    PL_origargv[0][PL_origalen-1] = 0;
	    SensorCall(5661);for (i = 1; i < PL_origargc; i++)
		PL_origargv[i] = 0;
#ifdef HAS_PRCTL_SET_NAME
	    /* Set the legacy process name in addition to the POSIX name on Linux */
	    SensorCall(5663);if (prctl(PR_SET_NAME, (unsigned long)s, 0, 0, 0) != 0) {
		/* diag_listed_as: SKIPME */
		SensorCall(5662);Perl_croak(aTHX_ "Can't set $0 with prctl(): %s", Strerror(errno));
	    }
#endif
	}
#endif
	UNLOCK_DOLLARZERO_MUTEX;
	SensorCall(5665);break;
    }
    {int  ReplaceReturn1996 = 0; SensorCall(5667); return ReplaceReturn1996;}
}

I32
Perl_whichsig_sv(pTHX_ SV *sigsv)
{
    SensorCall(5668);const char *sigpv;
    STRLEN siglen;
    PERL_ARGS_ASSERT_WHICHSIG_SV;
    PERL_UNUSED_CONTEXT;
    sigpv = SvPV_const(sigsv, siglen);
    {I32  ReplaceReturn1995 = whichsig_pvn(sigpv, siglen); SensorCall(5669); return ReplaceReturn1995;}
}

I32
Perl_whichsig_pv(pTHX_ const char *sig)
{
SensorCall(5670);    PERL_ARGS_ASSERT_WHICHSIG_PV;
    PERL_UNUSED_CONTEXT;
    {I32  ReplaceReturn1994 = whichsig_pvn(sig, strlen(sig)); SensorCall(5671); return ReplaceReturn1994;}
}

I32
Perl_whichsig_pvn(pTHX_ const char *sig, STRLEN len)
{
    SensorCall(5672);register char* const* sigv;

    PERL_ARGS_ASSERT_WHICHSIG_PVN;
    PERL_UNUSED_CONTEXT;

    SensorCall(5675);for (sigv = (char* const*)PL_sig_name; *sigv; sigv++)
	{/*149*/SensorCall(5673);if (strlen(*sigv) == len && memEQ(sig,*sigv, len))
	    {/*151*/{I32  ReplaceReturn1993 = PL_sig_num[sigv - (char* const*)PL_sig_name]; SensorCall(5674); return ReplaceReturn1993;}/*152*/}/*150*/}
#ifdef SIGCLD
    SensorCall(5676);if (memEQs(sig, len, "CHLD"))
	return SIGCLD;
#endif
#ifdef SIGCHLD
    SensorCall(5677);if (memEQs(sig, len, "CLD"))
	return SIGCHLD;
#endif
    {I32  ReplaceReturn1992 = -1; SensorCall(5678); return ReplaceReturn1992;}
}

Signal_t
#if defined(HAS_SIGACTION) && defined(SA_SIGINFO)
Perl_sighandler(int sig, siginfo_t *sip, void *uap)
#else
Perl_sighandler(int sig)
#endif
{
#ifdef PERL_GET_SIG_CONTEXT
    dTHXa(PERL_GET_SIG_CONTEXT);
#else
    dTHX;
#endif
    dSP;
    GV *gv = NULL;
    SV *sv = NULL;
    SV * const tSv = PL_Sv;
    CV *cv = NULL;
    OP *myop = PL_op;
    U32 flags = 0;
    XPV * const tXpv = PL_Xpv;
    I32 old_ss_ix = PL_savestack_ix;


    SensorCall(5680);if (!PL_psig_ptr[sig]) {
		SensorCall(5679);PerlIO_printf(Perl_error_log, "Signal SIG%s received, but no signal handler set.\n",
				 PL_sig_name[sig]);
		exit(sig);
	}

    SensorCall(5683);if (PL_signals &  PERL_SIGNALS_UNSAFE_FLAG) {
	/* Max number of items pushed there is 3*n or 4. We cannot fix
	   infinity, so we fix 4 (in fact 5): */
	SensorCall(5681);if (PL_savestack_ix + 15 <= PL_savestack_max) {
	    SensorCall(5682);flags |= 1;
	    PL_savestack_ix += 5;		/* Protect save in progress. */
	    SAVEDESTRUCTOR_X(S_unwind_handler_stack, NULL);
	}
    }
    /* sv_2cv is too complicated, try a simpler variant first: */
    SensorCall(5685);if (!SvROK(PL_psig_ptr[sig]) || !(cv = MUTABLE_CV(SvRV(PL_psig_ptr[sig])))
	|| SvTYPE(cv) != SVt_PVCV) {
	SensorCall(5684);HV *st;
	cv = sv_2cv(PL_psig_ptr[sig], &st, &gv, GV_ADD);
    }

    SensorCall(5688);if (!cv || !CvROOT(cv)) {
	SensorCall(5686);Perl_ck_warner(aTHX_ packWARN(WARN_SIGNAL), "SIG%s handler \"%s\" not defined.\n",
		       PL_sig_name[sig], (gv ? GvENAME(gv)
					  : ((cv && CvGV(cv))
					     ? GvENAME(CvGV(cv))
					     : "__ANON__")));
	SensorCall(5687);goto cleanup;
    }

    SensorCall(5689);sv = PL_psig_name[sig]
	    ? SvREFCNT_inc_NN(PL_psig_name[sig])
	    : newSVpv(PL_sig_name[sig],0);
    flags |= 8;
    SAVEFREESV(sv);

    SensorCall(5690);if (PL_signals &  PERL_SIGNALS_UNSAFE_FLAG) {
	/* make sure our assumption about the size of the SAVEs are correct:
	 * 3 for SAVEDESTRUCTOR_X, 2 for SAVEFREESV */
	assert(old_ss_ix + 2 + ((flags & 1) ? 3+5 : 0)  == PL_savestack_ix);
    }

    PUSHSTACKi(PERLSI_SIGNAL);
    PUSHMARK(SP);
    PUSHs(sv);
#if defined(HAS_SIGACTION) && defined(SA_SIGINFO)
    {
	 SensorCall(5691);struct sigaction oact;

	 SensorCall(5694);if (sigaction(sig, 0, &oact) == 0 && oact.sa_flags & SA_SIGINFO) {
	      SensorCall(5692);if (sip) {
		   SensorCall(5693);HV *sih = newHV();
		   SV *rv  = newRV_noinc(MUTABLE_SV(sih));
		   /* The siginfo fields signo, code, errno, pid, uid,
		    * addr, status, and band are defined by POSIX/SUSv3. */
		   (void)hv_stores(sih, "signo", newSViv(sip->si_signo));
		   (void)hv_stores(sih, "code", newSViv(sip->si_code));
#if 0 /* XXX TODO: Configure scan for the existence of these, but even that does not help if the SA_SIGINFO is not implemented according to the spec. */
		   hv_stores(sih, "errno",      newSViv(sip->si_errno));
		   hv_stores(sih, "status",     newSViv(sip->si_status));
		   hv_stores(sih, "uid",        newSViv(sip->si_uid));
		   hv_stores(sih, "pid",        newSViv(sip->si_pid));
		   hv_stores(sih, "addr",       newSVuv(PTR2UV(sip->si_addr)));
		   hv_stores(sih, "band",       newSViv(sip->si_band));
#endif
		   EXTEND(SP, 2);
		   PUSHs(rv);
		   mPUSHp((char *)sip, sizeof(*sip));
	      }

	 }
    }
#endif
    PUTBACK;

    call_sv(MUTABLE_SV(cv), G_DISCARD|G_EVAL);

    POPSTACK;
    SensorCall(5697);if (SvTRUE(ERRSV)) {
#ifndef PERL_MICRO
	/* Handler "died", for example to get out of a restart-able read().
	 * Before we re-do that on its behalf re-enable the signal which was
	 * blocked by the system when we entered.
	 */
#ifdef HAS_SIGPROCMASK
#if defined(HAS_SIGACTION) && defined(SA_SIGINFO)
       SensorCall(5695);if (sip || uap)
#endif
	{
	    SensorCall(5696);sigset_t set;
	    sigemptyset(&set);
	    sigaddset(&set,sig);
	    sigprocmask(SIG_UNBLOCK, &set, NULL);
	}
#else
	/* Not clear if this will work */
	(void)rsignal(sig, SIG_IGN);
	(void)rsignal(sig, PL_csighandlerp);
#endif
#endif /* !PERL_MICRO */
	die_sv(ERRSV);
    }
cleanup:
    /* pop any of SAVEFREESV, SAVEDESTRUCTOR_X and "save in progress" */
    PL_savestack_ix = old_ss_ix;
    SensorCall(5698);if (flags & 8)
	SvREFCNT_dec(sv);
    PL_op = myop;			/* Apparently not needed... */

    PL_Sv = tSv;			/* Restore global temporaries. */
    PL_Xpv = tXpv;
    SensorCall(5699);return;
}


static void
S_restore_magic(pTHX_ const void *p)
{
SensorCall(5700);    dVAR;
    MGS* const mgs = SSPTR(PTR2IV(p), MGS*);
    SV* const sv = mgs->mgs_sv;
    bool bumped;

    SensorCall(5702);if (!sv)
        {/*155*/SensorCall(5701);return;/*156*/}

    SensorCall(5708);if (SvTYPE(sv) >= SVt_PVMG && SvMAGIC(sv))
    {
#ifdef PERL_OLD_COPY_ON_WRITE
	/* While magic was saved (and off) sv_setsv may well have seen
	   this SV as a prime candidate for COW.  */
	if (SvIsCOW(sv))
	    sv_force_normal_flags(sv, 0);
#endif

	SensorCall(5703);if (mgs->mgs_readonly)
	    SvREADONLY_on(sv);
	SensorCall(5704);if (mgs->mgs_magical)
	    SvFLAGS(sv) |= mgs->mgs_magical;
	else
	    mg_magical(sv);
	SensorCall(5707);if (SvGMAGICAL(sv)) {
	    /* downgrade public flags to private,
	       and discard any other private flags */

	    SensorCall(5705);const U32 pubflags = SvFLAGS(sv) & (SVf_IOK|SVf_NOK|SVf_POK);
	    SensorCall(5706);if (pubflags) {
		SvFLAGS(sv) &= ~( pubflags | (SVp_IOK|SVp_NOK|SVp_POK) );
		SvFLAGS(sv) |= ( pubflags << PRIVSHIFT );
	    }
	}
    }

    SensorCall(5709);bumped = mgs->mgs_bumped;
    mgs->mgs_sv = NULL;  /* mark the MGS structure as restored */

    /* If we're still on top of the stack, pop us off.  (That condition
     * will be satisfied if restore_magic was called explicitly, but *not*
     * if it's being called via leave_scope.)
     * The reason for doing this is that otherwise, things like sv_2cv()
     * may leave alloc gunk on the savestack, and some code
     * (e.g. sighandler) doesn't expect that...
     */
    SensorCall(5711);if (PL_savestack_ix == mgs->mgs_ss_ix)
    {
	SensorCall(5710);UV popval = SSPOPUV;
        assert(popval == SAVEt_DESTRUCTOR_X);
        PL_savestack_ix -= 2;
	popval = SSPOPUV;
        assert((popval & SAVE_MASK) == SAVEt_ALLOC);
        PL_savestack_ix -= popval >> SAVE_TIGHT_SHIFT;
    }
    SensorCall(5715);if (bumped) {
	SensorCall(5712);if (SvREFCNT(sv) == 1) {
	    /* We hold the last reference to this SV, which implies that the
	       SV was deleted as a side effect of the routines we called.
	       So artificially keep it alive a bit longer.
	       We avoid turning on the TEMP flag, which can cause the SV's
	       buffer to get stolen (and maybe other stuff). */
	    SensorCall(5713);int was_temp = SvTEMP(sv);
	    sv_2mortal(sv);
	    SensorCall(5714);if (!was_temp) {
		SvTEMP_off(sv);
	    }
	    SvOK_off(sv);
	}
	else
	    SvREFCNT_dec(sv); /* undo the inc in S_save_magic() */
    }
SensorCall(5716);}

/* clean up the mess created by Perl_sighandler().
 * Note that this is only called during an exit in a signal handler;
 * a die is trapped by the call_sv() and the SAVEDESTRUCTOR_X manually
 * skipped over. */

static void
S_unwind_handler_stack(pTHX_ const void *p)
{
SensorCall(5717);    dVAR;
    PERL_UNUSED_ARG(p);

    PL_savestack_ix -= 5; /* Unprotect save in progress. */
}

/*
=for apidoc magic_sethint

Triggered by a store to %^H, records the key/value pair to
C<PL_compiling.cop_hints_hash>.  It is assumed that hints aren't storing
anything that would need a deep copy.  Maybe we should warn if we find a
reference.

=cut
*/
int
Perl_magic_sethint(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5718);    dVAR;
    SV *key = (mg->mg_len == HEf_SVKEY) ? MUTABLE_SV(mg->mg_ptr)
	: newSVpvn_flags(mg->mg_ptr, mg->mg_len, SVs_TEMP);

    PERL_ARGS_ASSERT_MAGIC_SETHINT;

    /* mg->mg_obj isn't being used.  If needed, it would be possible to store
       an alternative leaf in there, with PL_compiling.cop_hints being used if
       it's NULL. If needed for threads, the alternative could lock a mutex,
       or take other more complex action.  */

    /* Something changed in %^H, so it will need to be restored on scope exit.
       Doing this here saves a lot of doing it manually in perl code (and
       forgetting to do it, and consequent subtle errors.  */
    PL_hints |= HINT_LOCALIZE_HH;
    CopHINTHASH_set(&PL_compiling,
	cophh_store_sv(CopHINTHASH_get(&PL_compiling), key, 0, sv, 0));
    {int  ReplaceReturn1991 = 0; SensorCall(5719); return ReplaceReturn1991;}
}

/*
=for apidoc magic_clearhint

Triggered by a delete from %^H, records the key to
C<PL_compiling.cop_hints_hash>.

=cut
*/
int
Perl_magic_clearhint(pTHX_ SV *sv, MAGIC *mg)
{
SensorCall(5720);    dVAR;

    PERL_ARGS_ASSERT_MAGIC_CLEARHINT;
    PERL_UNUSED_ARG(sv);

    assert(mg->mg_len == HEf_SVKEY);

    PERL_UNUSED_ARG(sv);

    PL_hints |= HINT_LOCALIZE_HH;
    CopHINTHASH_set(&PL_compiling,
	cophh_delete_sv(CopHINTHASH_get(&PL_compiling),
				 MUTABLE_SV(mg->mg_ptr), 0, 0));
    {int  ReplaceReturn1990 = 0; SensorCall(5721); return ReplaceReturn1990;}
}

/*
=for apidoc magic_clearhints

Triggered by clearing %^H, resets C<PL_compiling.cop_hints_hash>.

=cut
*/
int
Perl_magic_clearhints(pTHX_ SV *sv, MAGIC *mg)
{
    PERL_ARGS_ASSERT_MAGIC_CLEARHINTS;
    PERL_UNUSED_ARG(sv);
    PERL_UNUSED_ARG(mg);
    cophh_free(CopHINTHASH_get(&PL_compiling));
    CopHINTHASH_set(&PL_compiling, cophh_new_empty());
    {int  ReplaceReturn1989 = 0; SensorCall(5722); return ReplaceReturn1989;}
}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
