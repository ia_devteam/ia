#include "var/tmp/sensor.h"
/*    gv.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 *   'Mercy!' cried Gandalf.  'If the giving of information is to be the cure
 * of your inquisitiveness, I shall spend all the rest of my days in answering
 * you.  What more do you want to know?'
 *   'The names of all the stars, and of all living things, and the whole
 * history of Middle-earth and Over-heaven and of the Sundering Seas,'
 * laughed Pippin.
 *
 *     [p.599 of _The Lord of the Rings_, III/xi: "The Palantír"]
 */

/*
=head1 GV Functions

A GV is a structure which corresponds to to a Perl typeglob, ie *foo.
It is a structure that holds a pointer to a scalar, an array, a hash etc,
corresponding to $foo, @foo, %foo.

GVs are usually found as values in stashes (symbol table hashes) where
Perl stores its global variables.

=cut
*/

#include "EXTERN.h"
#define PERL_IN_GV_C
#include "perl.h"
#include "overload.c"
#include "keywords.h"
#include "feature.h"

static const char S_autoload[] = "AUTOLOAD";
static const STRLEN S_autolen = sizeof(S_autoload)-1;

GV *
Perl_gv_add_by_type(pTHX_ GV *gv, svtype type)
{
    SensorCall(2100);SV **where;

    SensorCall(2108);if (
        !gv
     || (
            SvTYPE((const SV *)gv) != SVt_PVGV
         && SvTYPE((const SV *)gv) != SVt_PVLV
        )
    ) {
	SensorCall(2101);const char *what;
	SensorCall(2106);if (type == SVt_PVIO) {
	    /*
	     * if it walks like a dirhandle, then let's assume that
	     * this is a dirhandle.
	     */
	    SensorCall(2102);what = OP_IS_DIRHOP(PL_op->op_type) ?
		"dirhandle" : "filehandle";
	} else {/*51*/SensorCall(2103);if (type == SVt_PVHV) {
	    SensorCall(2104);what = "hash";
	} else {
	    SensorCall(2105);what = type == SVt_PVAV ? "array" : "scalar";
	;/*52*/}}
	/* diag_listed_as: Bad symbol for filehandle */
	SensorCall(2107);Perl_croak(aTHX_ "Bad symbol for %s", what);
    }

    SensorCall(2115);if (type == SVt_PVHV) {
	SensorCall(2109);where = (SV **)&GvHV(gv);
    } else {/*53*/SensorCall(2110);if (type == SVt_PVAV) {
	SensorCall(2111);where = (SV **)&GvAV(gv);
    } else {/*55*/SensorCall(2112);if (type == SVt_PVIO) {
	SensorCall(2113);where = (SV **)&GvIOp(gv);
    } else {
	SensorCall(2114);where = &GvSV(gv);
    ;/*56*/}/*54*/}}

    SensorCall(2116);if (!*where)
	*where = newSV_type(type);
    {GV * ReplaceReturn1988 = gv; SensorCall(2117); return ReplaceReturn1988;}
}

GV *
Perl_gv_fetchfile(pTHX_ const char *name)
{
    PERL_ARGS_ASSERT_GV_FETCHFILE;
    {GV * ReplaceReturn1987 = gv_fetchfile_flags(name, strlen(name), 0); SensorCall(2118); return ReplaceReturn1987;}
}

GV *
Perl_gv_fetchfile_flags(pTHX_ const char *const name, const STRLEN namelen,
			const U32 flags)
{
SensorCall(2119);    dVAR;
    char smallbuf[128];
    char *tmpbuf;
    const STRLEN tmplen = namelen + 2;
    GV *gv;

    PERL_ARGS_ASSERT_GV_FETCHFILE_FLAGS;
    PERL_UNUSED_ARG(flags);

    SensorCall(2120);if (!PL_defstash)
	return NULL;

    SensorCall(2122);if (tmplen <= sizeof smallbuf)
	{/*65*/SensorCall(2121);tmpbuf = smallbuf;/*66*/}
    else
	Newx(tmpbuf, tmplen, char);
    /* This is where the debugger's %{"::_<$filename"} hash is created */
    SensorCall(2123);tmpbuf[0] = '_';
    tmpbuf[1] = '<';
    memcpy(tmpbuf + 2, name, namelen);
    gv = *(GV**)hv_fetch(PL_defstash, tmpbuf, tmplen, TRUE);
    SensorCall(2124);if (!isGV(gv)) {
	gv_init(gv, PL_defstash, tmpbuf, tmplen, FALSE);
#ifdef PERL_DONT_CREATE_GVSV
	GvSV(gv) = newSVpvn(name, namelen);
#else
	sv_setpvn(GvSV(gv), name, namelen);
#endif
    }
    SensorCall(2125);if ((PERLDB_LINE || PERLDB_SAVESRC) && !GvAV(gv))
	    hv_magic(GvHVn(gv_AVadd(gv)), NULL, PERL_MAGIC_dbfile);
    SensorCall(2126);if (tmpbuf != smallbuf)
	Safefree(tmpbuf);
    {GV * ReplaceReturn1986 = gv; SensorCall(2127); return ReplaceReturn1986;}
}

/*
=for apidoc gv_const_sv

If C<gv> is a typeglob whose subroutine entry is a constant sub eligible for
inlining, or C<gv> is a placeholder reference that would be promoted to such
a typeglob, then returns the value returned by the sub.  Otherwise, returns
NULL.

=cut
*/

SV *
Perl_gv_const_sv(pTHX_ GV *gv)
{
    PERL_ARGS_ASSERT_GV_CONST_SV;

    SensorCall(2128);if (SvTYPE(gv) == SVt_PVGV)
	return cv_const_sv(GvCVu(gv));
    {SV * ReplaceReturn1985 = SvROK(gv) ? SvRV(gv) : NULL; SensorCall(2129); return ReplaceReturn1985;}
}

GP *
Perl_newGP(pTHX_ GV *const gv)
{
    SensorCall(2130);GP *gp;
    U32 hash;
#ifdef USE_ITHREADS
    const char *const file
	= (PL_curcop && CopFILE(PL_curcop)) ? CopFILE(PL_curcop) : "";
    const STRLEN len = strlen(file);
#else
    SV *const temp_sv = CopFILESV(PL_curcop);
    const char *file;
    STRLEN len;

    PERL_ARGS_ASSERT_NEWGP;

    if (temp_sv) {
	file = SvPVX(temp_sv);
	len = SvCUR(temp_sv);
    } else {
	file = "";
	len = 0;
    }
#endif

    PERL_HASH(hash, file, len);

    Newxz(gp, 1, GP);

#ifndef PERL_DONT_CREATE_GVSV
    gp->gp_sv = newSV(0);
#endif

    gp->gp_line = PL_curcop ? CopLINE(PL_curcop) : 0;
    /* XXX Ideally this cast would be replaced with a change to const char*
       in the struct.  */
    gp->gp_file_hek = share_hek(file, len, hash);
    gp->gp_egv = gv;
    gp->gp_refcnt = 1;

    {GP * ReplaceReturn1984 = gp; SensorCall(2131); return ReplaceReturn1984;}
}

/* Assign CvGV(cv) = gv, handling weak references.
 * See also S_anonymise_cv_maybe */

void
Perl_cvgv_set(pTHX_ CV* cv, GV* gv)
{
    SensorCall(2132);GV * const oldgv = CvGV(cv);
    PERL_ARGS_ASSERT_CVGV_SET;

    SensorCall(2134);if (oldgv == gv)
	{/*35*/SensorCall(2133);return;/*36*/}

    SensorCall(2136);if (oldgv) {
	SensorCall(2135);if (CvCVGV_RC(cv)) {
	    SvREFCNT_dec(oldgv);
	    CvCVGV_RC_off(cv);
	}
	else {
	    sv_del_backref(MUTABLE_SV(oldgv), MUTABLE_SV(cv));
	}
    }

    SvANY(cv)->xcv_gv = gv;
    assert(!CvCVGV_RC(cv));

    SensorCall(2138);if (!gv)
	{/*37*/SensorCall(2137);return;/*38*/}

    SensorCall(2140);if (isGV_with_GP(gv) && GvGP(gv) && (GvCV(gv) == cv || GvFORM(gv) == cv))
	{/*39*/SensorCall(2139);Perl_sv_add_backref(aTHX_ MUTABLE_SV(gv), MUTABLE_SV(cv));/*40*/}
    else {
	CvCVGV_RC_on(cv);
	SvREFCNT_inc_simple_void_NN(gv);
    }
SensorCall(2141);}

/* Assign CvSTASH(cv) = st, handling weak references. */

void
Perl_cvstash_set(pTHX_ CV *cv, HV *st)
{
    SensorCall(2142);HV *oldst = CvSTASH(cv);
    PERL_ARGS_ASSERT_CVSTASH_SET;
    SensorCall(2144);if (oldst == st)
	{/*41*/SensorCall(2143);return;/*42*/}
    SensorCall(2145);if (oldst)
	sv_del_backref(MUTABLE_SV(oldst), MUTABLE_SV(cv));
    SvANY(cv)->xcv_stash = st;
    SensorCall(2147);if (st)
	{/*43*/SensorCall(2146);Perl_sv_add_backref(aTHX_ MUTABLE_SV(st), MUTABLE_SV(cv));/*44*/}
SensorCall(2148);}

/*
=for apidoc gv_init_pvn

Converts a scalar into a typeglob.  This is an incoercible typeglob;
assigning a reference to it will assign to one of its slots, instead of
overwriting it as happens with typeglobs created by SvSetSV.  Converting
any scalar that is SvOK() may produce unpredictable results and is reserved
for perl's internal use.

C<gv> is the scalar to be converted.

C<stash> is the parent stash/package, if any.

C<name> and C<len> give the name.  The name must be unqualified;
that is, it must not include the package name.  If C<gv> is a
stash element, it is the caller's responsibility to ensure that the name
passed to this function matches the name of the element.  If it does not
match, perl's internal bookkeeping will get out of sync.

C<flags> can be set to SVf_UTF8 if C<name> is a UTF8 string, or
the return value of SvUTF8(sv).  It can also take the
GV_ADDMULTI flag, which means to pretend that the GV has been
seen before (i.e., suppress "Used once" warnings).

=for apidoc gv_init

The old form of gv_init_pvn().  It does not work with UTF8 strings, as it
has no flags parameter.  If the C<multi> parameter is set, the
GV_ADDMULTI flag will be passed to gv_init_pvn().

=for apidoc gv_init_pv

Same as gv_init_pvn(), but takes a nul-terminated string for the name
instead of separate char * and length parameters.

=for apidoc gv_init_sv

Same as gv_init_pvn(), but takes an SV * for the name instead of separate
char * and length parameters.  C<flags> is currently unused.

=cut
*/

void
Perl_gv_init_sv(pTHX_ GV *gv, HV *stash, SV* namesv, U32 flags)
{
   SensorCall(2149);char *namepv;
   STRLEN namelen;
   PERL_ARGS_ASSERT_GV_INIT_SV;
   namepv = SvPV(namesv, namelen);
   SensorCall(2150);if (SvUTF8(namesv))
       flags |= SVf_UTF8;
   gv_init_pvn(gv, stash, namepv, namelen, flags);
}

void
Perl_gv_init_pv(pTHX_ GV *gv, HV *stash, const char *name, U32 flags)
{
SensorCall(2151);   PERL_ARGS_ASSERT_GV_INIT_PV;
   gv_init_pvn(gv, stash, name, strlen(name), flags);
SensorCall(2152);}

void
Perl_gv_init_pvn(pTHX_ GV *gv, HV *stash, const char *name, STRLEN len, U32 flags)
{
SensorCall(2153);    dVAR;
    const U32 old_type = SvTYPE(gv);
    const bool doproto = old_type > SVt_NULL;
    char * const proto = (doproto && SvPOK(gv))
	? ((void)(SvIsCOW(gv) && (sv_force_normal((SV *)gv), 0)), SvPVX(gv))
	: NULL;
    const STRLEN protolen = proto ? SvCUR(gv) : 0;
    const U32 proto_utf8  = proto ? SvUTF8(gv) : 0;
    SV *const has_constant = doproto && SvROK(gv) ? SvRV(gv) : NULL;
    const U32 exported_constant = has_constant ? SvPCS_IMPORTED(gv) : 0;

    PERL_ARGS_ASSERT_GV_INIT_PVN;
    assert (!(proto && has_constant));

    SensorCall(2156);if (has_constant) {
	/* The constant has to be a simple scalar type.  */
	SensorCall(2154);switch (SvTYPE(has_constant)) {
	case SVt_PVAV:
	case SVt_PVHV:
	case SVt_PVCV:
	case SVt_PVFM:
	case SVt_PVIO:
            SensorCall(2155);Perl_croak(aTHX_ "Cannot convert a reference to %s to typeglob",
		       sv_reftype(has_constant, 0));
	default: NOOP;
	}
	SvRV_set(gv, NULL);
	SvROK_off(gv);
    }


    SensorCall(2158);if (old_type < SVt_PVGV) {
	SensorCall(2157);if (old_type >= SVt_PV)
	    SvCUR_set(gv, 0);
	sv_upgrade(MUTABLE_SV(gv), SVt_PVGV);
    }
    SensorCall(2160);if (SvLEN(gv)) {
	SensorCall(2159);if (proto) {
	    SvPV_set(gv, NULL);
	    SvLEN_set(gv, 0);
	    SvPOK_off(gv);
	} else
	    Safefree(SvPVX_mutable(gv));
    }
    SvIOK_off(gv);
    isGV_with_GP_on(gv);

    GvGP_set(gv, Perl_newGP(aTHX_ gv));
    GvSTASH(gv) = stash;
    SensorCall(2162);if (stash)
	{/*145*/SensorCall(2161);Perl_sv_add_backref(aTHX_ MUTABLE_SV(stash), MUTABLE_SV(gv));/*146*/}
    gv_name_set(gv, name, len, GV_ADD | ( flags & SVf_UTF8 ? SVf_UTF8 : 0 ));
    SensorCall(2163);if (flags & GV_ADDMULTI || doproto)	/* doproto means it */
	GvMULTI_on(gv);			/* _was_ mentioned */
    SensorCall(2172);if (doproto) {			/* Replicate part of newSUB here. */
	SensorCall(2164);CV *cv;
	ENTER;
	SensorCall(2169);if (has_constant) {
	    /* newCONSTSUB takes ownership of the reference from us.  */
	    SensorCall(2165);cv = newCONSTSUB_flags(stash, name, len, flags, has_constant);
	    /* In case op.c:S_process_special_blocks stole it: */
	    SensorCall(2166);if (!GvCV(gv))
		GvCV_set(gv, (CV *)SvREFCNT_inc_simple_NN(cv));
	    assert(GvCV(gv) == cv); /* newCONSTSUB should have set this */
	    /* If this reference was a copy of another, then the subroutine
	       must have been "imported", by a Perl space assignment to a GV
	       from a reference to CV.  */
	    SensorCall(2167);if (exported_constant)
		GvIMPORTED_CV_on(gv);
	} else {
	    SensorCall(2168);(void) start_subparse(0,0);	/* Create empty CV in compcv. */
	    cv = PL_compcv;
	    GvCV_set(gv,cv);
	}
	LEAVE;

        mro_method_changed_in(GvSTASH(gv)); /* sub Foo::bar($) { (shift) } sub ASDF::baz($); *ASDF::baz = \&Foo::bar */
	CvGV_set(cv, gv);
	CvFILE_set_from_cop(cv, PL_curcop);
	CvSTASH_set(cv, PL_curstash);
	SensorCall(2171);if (proto) {
	    sv_usepvn_flags(MUTABLE_SV(cv), proto, protolen,
			    SV_HAS_TRAILING_NUL);
            SensorCall(2170);if ( proto_utf8 ) SvUTF8_on(MUTABLE_SV(cv));
	}
    }
SensorCall(2173);}

STATIC void
S_gv_init_svtype(pTHX_ GV *gv, const svtype sv_type)
{
    PERL_ARGS_ASSERT_GV_INIT_SVTYPE;

    SensorCall(2174);switch (sv_type) {
    case SVt_PVIO:
	SensorCall(2175);(void)GvIOn(gv);
	SensorCall(2176);break;
    case SVt_PVAV:
	SensorCall(2177);(void)GvAVn(gv);
	SensorCall(2178);break;
    case SVt_PVHV:
	SensorCall(2179);(void)GvHVn(gv);
	SensorCall(2180);break;
#ifdef PERL_DONT_CREATE_GVSV
    case SVt_NULL:
    case SVt_PVCV:
    case SVt_PVFM:
    case SVt_PVGV:
	SensorCall(2181);break;
    default:
	SensorCall(2182);if(GvSVn(gv)) {
	    /* Work round what appears to be a bug in Sun C++ 5.8 2005/10/13
	       If we just cast GvSVn(gv) to void, it ignores evaluating it for
	       its side effect */
	}
#endif
    }
SensorCall(2183);}

static void core_xsub(pTHX_ CV* cv);

static GV *
S_maybe_add_coresub(pTHX_ HV * const stash, GV *gv,
                          const char * const name, const STRLEN len)
{
    SensorCall(2184);const int code = keyword(name, len, 1);
    static const char file[] = __FILE__;
    CV *cv, *oldcompcv = NULL;
    int opnum = 0;
    SV *opnumsv;
    bool ampable = TRUE; /* &{}-able */
    COP *oldcurcop = NULL;
    yy_parser *oldparser = NULL;
    I32 oldsavestack_ix = 0;

    assert(gv || stash);
    assert(name);

    SensorCall(2185);if (code >= 0) return NULL; /* not overridable */
    SensorCall(2188);switch (-code) {
     /* no support for \&CORE::infix;
        no support for funcs that take labels, as their parsing is
        weird  */
    case KEY_and: case KEY_cmp: case KEY_CORE: case KEY_dump:
    case KEY_eq: case KEY_ge:
    case KEY_gt: case KEY_le: case KEY_lt: case KEY_ne:
    case KEY_or: case KEY_x: case KEY_xor:
	{GV * ReplaceReturn1983 = NULL; SensorCall(2186); return ReplaceReturn1983;}
    case KEY_chdir:
    case KEY_chomp: case KEY_chop:
    case KEY_each: case KEY_eof: case KEY_exec:
    case KEY_keys:
    case KEY_lstat:
    case KEY_pop:
    case KEY_push:
    case KEY_shift:
    case KEY_splice:
    case KEY_stat:
    case KEY_system:
    case KEY_truncate: case KEY_unlink:
    case KEY_unshift:
    case KEY_values:
	SensorCall(2187);ampable = FALSE;
    }
    SensorCall(2190);if (!gv) {
	SensorCall(2189);gv = (GV *)newSV(0);
	gv_init(gv, stash, name, len, TRUE);
    }
    GvMULTI_on(gv);
    SensorCall(2193);if (ampable) {
	ENTER;
	SensorCall(2191);oldcurcop = PL_curcop;
	oldparser = PL_parser;
	lex_start(NULL, NULL, 0);
	oldcompcv = PL_compcv;
	PL_compcv = NULL; /* Prevent start_subparse from setting
	                     CvOUTSIDE. */
	oldsavestack_ix = start_subparse(FALSE,0);
	cv = PL_compcv;
    }
    else {
	/* Avoid calling newXS, as it calls us, and things start to
	   get hairy. */
	SensorCall(2192);cv = MUTABLE_CV(newSV_type(SVt_PVCV));
	GvCV_set(gv,cv);
	GvCVGEN(gv) = 0;
	mro_method_changed_in(GvSTASH(gv));
	CvISXSUB_on(cv);
	CvXSUB(cv) = core_xsub;
    }
    CvGV_set(cv, gv); /* This stops new ATTRSUB from setting CvFILE
                         from PL_curcop. */
    SensorCall(2194);(void)gv_fetchfile(file);
    CvFILE(cv) = (char *)file;
    /* XXX This is inefficient, as doing things this order causes
           a prototype check in newATTRSUB.  But we have to do
           it this order as we need an op number before calling
           new ATTRSUB. */
    (void)core_prototype((SV *)cv, name, code, &opnum);
    SensorCall(2195);if (stash)
	(void)hv_store(stash,name,len,(SV *)gv,0);
    SensorCall(2197);if (ampable) {
	CvLVALUE_on(cv);
	newATTRSUB_flags(
		   oldsavestack_ix, (OP *)gv,
	           NULL,NULL,
	           coresub_op(
	             opnum
	               ? newSVuv((UV)opnum)
	               : newSVpvn(name,len),
	             code, opnum
	           ),
	           1
	);
	assert(GvCV(gv) == cv);
	SensorCall(2196);if (opnum != OP_VEC && opnum != OP_SUBSTR)
	    CvLVALUE_off(cv); /* Now *that* was a neat trick. */
	LEAVE;
	PL_parser = oldparser;
	PL_curcop = oldcurcop;
	PL_compcv = oldcompcv;
    }
    SensorCall(2198);opnumsv = opnum ? newSVuv((UV)opnum) : (SV *)NULL;
    cv_set_call_checker(
       cv, Perl_ck_entersub_args_core, opnumsv ? opnumsv : (SV *)cv
    );
    SvREFCNT_dec(opnumsv);
    {GV * ReplaceReturn1982 = gv; SensorCall(2199); return ReplaceReturn1982;}
}

/*
=for apidoc gv_fetchmeth

Like L</gv_fetchmeth_pvn>, but lacks a flags parameter.

=for apidoc gv_fetchmeth_sv

Exactly like L</gv_fetchmeth_pvn>, but takes the name string in the form
of an SV instead of a string/length pair.

=cut
*/

GV *
Perl_gv_fetchmeth_sv(pTHX_ HV *stash, SV *namesv, I32 level, U32 flags)
{
   SensorCall(2200);char *namepv;
   STRLEN namelen;
   PERL_ARGS_ASSERT_GV_FETCHMETH_SV;
   namepv = SvPV(namesv, namelen);
   SensorCall(2201);if (SvUTF8(namesv))
       flags |= SVf_UTF8;
   {GV * ReplaceReturn1981 = gv_fetchmeth_pvn(stash, namepv, namelen, level, flags); SensorCall(2202); return ReplaceReturn1981;}
}

/*
=for apidoc gv_fetchmeth_pv

Exactly like L</gv_fetchmeth_pvn>, but takes a nul-terminated string 
instead of a string/length pair.

=cut
*/

GV *
Perl_gv_fetchmeth_pv(pTHX_ HV *stash, const char *name, I32 level, U32 flags)
{
    PERL_ARGS_ASSERT_GV_FETCHMETH_PV;
    {GV * ReplaceReturn1980 = gv_fetchmeth_pvn(stash, name, strlen(name), level, flags); SensorCall(2203); return ReplaceReturn1980;}
}

/*
=for apidoc gv_fetchmeth_pvn

Returns the glob with the given C<name> and a defined subroutine or
C<NULL>.  The glob lives in the given C<stash>, or in the stashes
accessible via @ISA and UNIVERSAL::.

The argument C<level> should be either 0 or -1.  If C<level==0>, as a
side-effect creates a glob with the given C<name> in the given C<stash>
which in the case of success contains an alias for the subroutine, and sets
up caching info for this glob.

Currently, the only significant value for C<flags> is SVf_UTF8.

This function grants C<"SUPER"> token as a postfix of the stash name. The
GV returned from C<gv_fetchmeth> may be a method cache entry, which is not
visible to Perl code.  So when calling C<call_sv>, you should not use
the GV directly; instead, you should use the method's CV, which can be
obtained from the GV with the C<GvCV> macro.

=cut
*/

/* NOTE: No support for tied ISA */

GV *
Perl_gv_fetchmeth_pvn(pTHX_ HV *stash, const char *name, STRLEN len, I32 level, U32 flags)
{
SensorCall(2204);    dVAR;
    GV** gvp;
    AV* linear_av;
    SV** linear_svp;
    SV* linear_sv;
    HV* cstash;
    GV* candidate = NULL;
    CV* cand_cv = NULL;
    GV* topgv = NULL;
    const char *hvname;
    I32 create = (level >= 0) ? 1 : 0;
    I32 items;
    STRLEN packlen;
    U32 topgen_cmp;
    U32 is_utf8 = flags & SVf_UTF8;

    PERL_ARGS_ASSERT_GV_FETCHMETH_PVN;

    /* UNIVERSAL methods should be callable without a stash */
    SensorCall(2208);if (!stash) {
	SensorCall(2205);create = 0;  /* probably appropriate */
	SensorCall(2207);if(!(stash = gv_stashpvs("UNIVERSAL", 0)))
	    {/*67*/{GV * ReplaceReturn1979 = 0; SensorCall(2206); return ReplaceReturn1979;}/*68*/}
    }

    assert(stash);

    SensorCall(2209);hvname = HvNAME_get(stash);
    SensorCall(2211);if (!hvname)
      {/*69*/SensorCall(2210);Perl_croak(aTHX_ "Can't use anonymous symbol table for method lookup");/*70*/}

    assert(hvname);
    assert(name);

    DEBUG_o( Perl_deb(aTHX_ "Looking for method %s in package %s\n",name,hvname) );

    SensorCall(2212);topgen_cmp = HvMROMETA(stash)->cache_gen + PL_sub_generation;

    /* check locally for a real method or a cache entry */
    gvp = (GV**)hv_fetch(stash, name, is_utf8 ? -(I32)len : (I32)len, create);
    SensorCall(2223);if(gvp) {
        SensorCall(2213);topgv = *gvp;
      have_gv:
        assert(topgv);
        SensorCall(2214);if (SvTYPE(topgv) != SVt_PVGV)
            gv_init_pvn(topgv, stash, name, len, GV_ADDMULTI|is_utf8);
        SensorCall(2222);if ((cand_cv = GvCV(topgv))) {
            /* If genuine method or valid cache entry, use it */
            SensorCall(2215);if (!GvCVGEN(topgv) || GvCVGEN(topgv) == topgen_cmp) {
                {GV * ReplaceReturn1978 = topgv; SensorCall(2216); return ReplaceReturn1978;}
            }
            else {
                /* stale cache entry, junk it and move on */
	        SvREFCNT_dec(cand_cv);
	        GvCV_set(topgv, NULL);
		SensorCall(2217);cand_cv = NULL;
	        GvCVGEN(topgv) = 0;
            }
        }
        else {/*71*/SensorCall(2218);if (GvCVGEN(topgv) == topgen_cmp) {
            /* cache indicates no such method definitively */
            {GV * ReplaceReturn1977 = 0; SensorCall(2219); return ReplaceReturn1977;}
        }
	else {/*73*/SensorCall(2220);if (len > 1 /* shortest is uc */ && HvNAMELEN_get(stash) == 4
              && strnEQ(hvname, "CORE", 4)
              && S_maybe_add_coresub(aTHX_ NULL,topgv,name,len))
	    {/*75*/SensorCall(2221);goto have_gv;/*76*/}/*74*/}/*72*/}
    }

    SensorCall(2224);packlen = HvNAMELEN_get(stash);
    SensorCall(2227);if (packlen >= 7 && strEQ(hvname + packlen - 7, "::SUPER")) {
        SensorCall(2225);HV* basestash;
        packlen -= 7;
        basestash = gv_stashpvn(hvname, packlen,
                                GV_ADD | (HvNAMEUTF8(stash) ? SVf_UTF8 : 0));
        linear_av = mro_get_linear_isa(basestash);
    }
    else {
        SensorCall(2226);linear_av = mro_get_linear_isa(stash); /* has ourselves at the top of the list */
    }

    SensorCall(2228);linear_svp = AvARRAY(linear_av) + 1; /* skip over self */
    items = AvFILLp(linear_av); /* no +1, to skip over self */
    SensorCall(2246);while (items--) {
        SensorCall(2229);linear_sv = *linear_svp++;
        assert(linear_sv);
        cstash = gv_stashsv(linear_sv, 0);

        SensorCall(2232);if (!cstash) {
	    SensorCall(2230);Perl_ck_warner(aTHX_ packWARN(WARN_SYNTAX),
                           "Can't locate package %"SVf" for @%"HEKf"::ISA",
			   SVfARG(linear_sv),
                           HEKfARG(HvNAME_HEK(stash)));
            SensorCall(2231);continue;
        }

        assert(cstash);

        SensorCall(2233);gvp = (GV**)hv_fetch(cstash, name, is_utf8 ? -(I32)len : (I32)len, 0);
        SensorCall(2240);if (!gvp) {
            SensorCall(2234);if (len > 1 && HvNAMELEN_get(cstash) == 4) {
                SensorCall(2235);const char *hvname = HvNAME(cstash); assert(hvname);
                SensorCall(2237);if (strnEQ(hvname, "CORE", 4)
                 && (candidate =
                      S_maybe_add_coresub(aTHX_ cstash,NULL,name,len)
                    ))
                    {/*77*/SensorCall(2236);goto have_candidate;/*78*/}
            }
            SensorCall(2238);continue;
        }
        else {/*79*/SensorCall(2239);candidate = *gvp;/*80*/}
       have_candidate:
        assert(candidate);
        SensorCall(2241);if (SvTYPE(candidate) != SVt_PVGV)
            gv_init_pvn(candidate, cstash, name, len, GV_ADDMULTI|is_utf8);
        SensorCall(2245);if (SvTYPE(candidate) == SVt_PVGV && (cand_cv = GvCV(candidate)) && !GvCVGEN(candidate)) {
            /*
             * Found real method, cache method in topgv if:
             *  1. topgv has no synonyms (else inheritance crosses wires)
             *  2. method isn't a stub (else AUTOLOAD fails spectacularly)
             */
            SensorCall(2242);if (topgv && (GvREFCNT(topgv) == 1) && (CvROOT(cand_cv) || CvXSUB(cand_cv))) {
                  SensorCall(2243);CV *old_cv = GvCV(topgv);
                  SvREFCNT_dec(old_cv);
                  SvREFCNT_inc_simple_void_NN(cand_cv);
                  GvCV_set(topgv, cand_cv);
                  GvCVGEN(topgv) = topgen_cmp;
            }
	    {GV * ReplaceReturn1976 = candidate; SensorCall(2244); return ReplaceReturn1976;}
        }
    }

    /* Check UNIVERSAL without caching */
    SensorCall(2253);if(level == 0 || level == -1) {
        SensorCall(2247);candidate = gv_fetchmeth_pvn(NULL, name, len, 1, flags);
        SensorCall(2252);if(candidate) {
            SensorCall(2248);cand_cv = GvCV(candidate);
            SensorCall(2250);if (topgv && (GvREFCNT(topgv) == 1) && (CvROOT(cand_cv) || CvXSUB(cand_cv))) {
                  SensorCall(2249);CV *old_cv = GvCV(topgv);
                  SvREFCNT_dec(old_cv);
                  SvREFCNT_inc_simple_void_NN(cand_cv);
                  GvCV_set(topgv, cand_cv);
                  GvCVGEN(topgv) = topgen_cmp;
            }
            {GV * ReplaceReturn1975 = candidate; SensorCall(2251); return ReplaceReturn1975;}
        }
    }

    SensorCall(2254);if (topgv && GvREFCNT(topgv) == 1) {
        /* cache the fact that the method is not defined */
        GvCVGEN(topgv) = topgen_cmp;
    }

    {GV * ReplaceReturn1974 = 0; SensorCall(2255); return ReplaceReturn1974;}
}

/*
=for apidoc gv_fetchmeth_autoload

This is the old form of L</gv_fetchmeth_pvn_autoload>, which has no flags
parameter.

=for apidoc gv_fetchmeth_sv_autoload

Exactly like L</gv_fetchmeth_pvn_autoload>, but takes the name string in the form
of an SV instead of a string/length pair.

=cut
*/

GV *
Perl_gv_fetchmeth_sv_autoload(pTHX_ HV *stash, SV *namesv, I32 level, U32 flags)
{
   SensorCall(2256);char *namepv;
   STRLEN namelen;
   PERL_ARGS_ASSERT_GV_FETCHMETH_SV_AUTOLOAD;
   namepv = SvPV(namesv, namelen);
   SensorCall(2257);if (SvUTF8(namesv))
       flags |= SVf_UTF8;
   {GV * ReplaceReturn1973 = gv_fetchmeth_pvn_autoload(stash, namepv, namelen, level, flags); SensorCall(2258); return ReplaceReturn1973;}
}

/*
=for apidoc gv_fetchmeth_pv_autoload

Exactly like L</gv_fetchmeth_pvn_autoload>, but takes a nul-terminated string
instead of a string/length pair.

=cut
*/

GV *
Perl_gv_fetchmeth_pv_autoload(pTHX_ HV *stash, const char *name, I32 level, U32 flags)
{
    PERL_ARGS_ASSERT_GV_FETCHMETH_PV_AUTOLOAD;
    {GV * ReplaceReturn1972 = gv_fetchmeth_pvn_autoload(stash, name, strlen(name), level, flags); SensorCall(2259); return ReplaceReturn1972;}
}

/*
=for apidoc gv_fetchmeth_pvn_autoload

Same as gv_fetchmeth_pvn(), but looks for autoloaded subroutines too.
Returns a glob for the subroutine.

For an autoloaded subroutine without a GV, will create a GV even
if C<level < 0>.  For an autoloaded subroutine without a stub, GvCV()
of the result may be zero.

Currently, the only significant value for C<flags> is SVf_UTF8.

=cut
*/

GV *
Perl_gv_fetchmeth_pvn_autoload(pTHX_ HV *stash, const char *name, STRLEN len, I32 level, U32 flags)
{
    SensorCall(2260);GV *gv = gv_fetchmeth_pvn(stash, name, len, level, flags);

    PERL_ARGS_ASSERT_GV_FETCHMETH_PVN_AUTOLOAD;

    SensorCall(2271);if (!gv) {
	SensorCall(2261);CV *cv;
	GV **gvp;

	SensorCall(2262);if (!stash)
	    return NULL;	/* UNIVERSAL::AUTOLOAD could cause trouble */
	SensorCall(2263);if (len == S_autolen && memEQ(name, S_autoload, S_autolen))
	    return NULL;
	SensorCall(2264);if (!(gv = gv_fetchmeth_pvn(stash, S_autoload, S_autolen, FALSE, flags)))
	    return NULL;
	SensorCall(2265);cv = GvCV(gv);
	SensorCall(2266);if (!(CvROOT(cv) || CvXSUB(cv)))
	    return NULL;
	/* Have an autoload */
	SensorCall(2267);if (level < 0)	/* Cannot do without a stub */
	    gv_fetchmeth_pvn(stash, name, len, 0, flags);
	SensorCall(2268);gvp = (GV**)hv_fetch(stash, name,
                        (flags & SVf_UTF8) ? -(I32)len : (I32)len, (level >= 0));
	SensorCall(2269);if (!gvp)
	    return NULL;
	{GV * ReplaceReturn1971 = *gvp; SensorCall(2270); return ReplaceReturn1971;}
    }
    {GV * ReplaceReturn1970 = gv; SensorCall(2272); return ReplaceReturn1970;}
}

/*
=for apidoc gv_fetchmethod_autoload

Returns the glob which contains the subroutine to call to invoke the method
on the C<stash>.  In fact in the presence of autoloading this may be the
glob for "AUTOLOAD".  In this case the corresponding variable $AUTOLOAD is
already setup.

The third parameter of C<gv_fetchmethod_autoload> determines whether
AUTOLOAD lookup is performed if the given method is not present: non-zero
means yes, look for AUTOLOAD; zero means no, don't look for AUTOLOAD.
Calling C<gv_fetchmethod> is equivalent to calling C<gv_fetchmethod_autoload>
with a non-zero C<autoload> parameter.

These functions grant C<"SUPER"> token as a prefix of the method name. Note
that if you want to keep the returned glob for a long time, you need to
check for it being "AUTOLOAD", since at the later time the call may load a
different subroutine due to $AUTOLOAD changing its value. Use the glob
created via a side effect to do this.

These functions have the same side-effects and as C<gv_fetchmeth> with
C<level==0>.  C<name> should be writable if contains C<':'> or C<'
''>. The warning against passing the GV returned by C<gv_fetchmeth> to
C<call_sv> apply equally to these functions.

=cut
*/

STATIC HV*
S_gv_get_super_pkg(pTHX_ const char* name, I32 namelen, U32 flags)
{
    SensorCall(2273);AV* superisa;
    GV** gvp;
    GV* gv;
    HV* stash;

    PERL_ARGS_ASSERT_GV_GET_SUPER_PKG;

    stash = gv_stashpvn(name, namelen, flags);
    SensorCall(2275);if(stash) {/*161*/{HV * ReplaceReturn1969 = stash; SensorCall(2274); return ReplaceReturn1969;}/*162*/}

    /* If we must create it, give it an @ISA array containing
       the real package this SUPER is for, so that it's tied
       into the cache invalidation code correctly */
    SensorCall(2276);stash = gv_stashpvn(name, namelen, GV_ADD | flags);
    gvp = (GV**)hv_fetchs(stash, "ISA", TRUE);
    gv = *gvp;
    gv_init(gv, stash, "ISA", 3, TRUE);
    superisa = GvAVn(gv);
    GvMULTI_on(gv);
    sv_magic(MUTABLE_SV(superisa), MUTABLE_SV(gv), PERL_MAGIC_isa, NULL, 0);
#ifdef USE_ITHREADS
    av_push(superisa, newSVpvn_flags(CopSTASHPV(PL_curcop),
                                     CopSTASH_len(PL_curcop) < 0
					? -CopSTASH_len(PL_curcop)
					:  CopSTASH_len(PL_curcop),
                                     SVf_UTF8*(CopSTASH_len(PL_curcop) < 0)
                                    ));
#else
    av_push(superisa, newSVhek(CopSTASH(PL_curcop)
			       ? HvNAME_HEK(CopSTASH(PL_curcop)) : NULL));
#endif

    {HV * ReplaceReturn1968 = stash; SensorCall(2277); return ReplaceReturn1968;}
}

GV *
Perl_gv_fetchmethod_autoload(pTHX_ HV *stash, const char *name, I32 autoload)
{
    PERL_ARGS_ASSERT_GV_FETCHMETHOD_AUTOLOAD;

    {GV * ReplaceReturn1967 = gv_fetchmethod_flags(stash, name, autoload ? GV_AUTOLOAD : 0); SensorCall(2278); return ReplaceReturn1967;}
}

GV *
Perl_gv_fetchmethod_sv_flags(pTHX_ HV *stash, SV *namesv, U32 flags)
{
    SensorCall(2279);char *namepv;
    STRLEN namelen;
    PERL_ARGS_ASSERT_GV_FETCHMETHOD_SV_FLAGS;
    namepv = SvPV(namesv, namelen);
    SensorCall(2280);if (SvUTF8(namesv))
       flags |= SVf_UTF8;
    {GV * ReplaceReturn1966 = gv_fetchmethod_pvn_flags(stash, namepv, namelen, flags); SensorCall(2281); return ReplaceReturn1966;}
}

GV *
Perl_gv_fetchmethod_pv_flags(pTHX_ HV *stash, const char *name, U32 flags)
{
    PERL_ARGS_ASSERT_GV_FETCHMETHOD_PV_FLAGS;
    {GV * ReplaceReturn1965 = gv_fetchmethod_pvn_flags(stash, name, strlen(name), flags); SensorCall(2282); return ReplaceReturn1965;}
}

/* Don't merge this yet, as it's likely to get a len parameter, and possibly
   even a U32 hash */
GV *
Perl_gv_fetchmethod_pvn_flags(pTHX_ HV *stash, const char *name, const STRLEN len, U32 flags)
{
SensorCall(2283);    dVAR;
    register const char *nend;
    const char *nsplit = NULL;
    GV* gv;
    HV* ostash = stash;
    const char * const origname = name;
    SV *const error_report = MUTABLE_SV(stash);
    const U32 autoload = flags & GV_AUTOLOAD;
    const U32 do_croak = flags & GV_CROAK;
    const U32 is_utf8  = flags & SVf_UTF8;

    PERL_ARGS_ASSERT_GV_FETCHMETHOD_PVN_FLAGS;

    SensorCall(2284);if (SvTYPE(stash) < SVt_PVHV)
	stash = NULL;
    else {
	/* The only way stash can become NULL later on is if nsplit is set,
	   which in turn means that there is no need for a SVt_PVHV case
	   the error reporting code.  */
    }

    SensorCall(2289);for (nend = name; *nend || nend != (origname + len); nend++) {
	SensorCall(2285);if (*nend == '\'') {
	    SensorCall(2286);nsplit = nend;
	    name = nend + 1;
	}
	else {/*81*/SensorCall(2287);if (*nend == ':' && *(nend + 1) == ':') {
	    SensorCall(2288);nsplit = nend++;
	    name = nend + 1;
	;/*82*/}}
    }
    SensorCall(2295);if (nsplit) {
	SensorCall(2290);if ((nsplit - origname) == 5 && memEQ(origname, "SUPER", 5)) {
	    /* ->SUPER::method should really be looked up in original stash */
	    SensorCall(2291);SV * const tmpstr = sv_2mortal(Perl_newSVpvf(aTHX_
		     "%"HEKf"::SUPER",
		      HEKfARG(HvNAME_HEK((HV*)CopSTASH(PL_curcop)))
	    ));
	    /* __PACKAGE__::SUPER stash should be autovivified */
	    stash = gv_get_super_pkg(SvPVX_const(tmpstr), SvCUR(tmpstr), SvUTF8(tmpstr));
	    DEBUG_o( Perl_deb(aTHX_ "Treating %s as %s::%s\n",
			 origname, HvNAME_get(stash), name) );
	}
	else {
            /* don't autovifify if ->NoSuchStash::method */
            SensorCall(2292);stash = gv_stashpvn(origname, nsplit - origname, is_utf8);

	    /* however, explicit calls to Pkg::SUPER::method may
	       happen, and may require autovivification to work */
	    SensorCall(2293);if (!stash && (nsplit - origname) >= 7 &&
		strnEQ(nsplit - 7, "::SUPER", 7) &&
		gv_stashpvn(origname, nsplit - origname - 7, is_utf8))
	      stash = gv_get_super_pkg(origname, nsplit - origname, flags);
	}
	SensorCall(2294);ostash = stash;
    }

    SensorCall(2296);gv = gv_fetchmeth_pvn(stash, name, nend - name, 0, flags);
    SensorCall(2323);if (!gv) {
	SensorCall(2297);if (strEQ(name,"import") || strEQ(name,"unimport"))
	    gv = MUTABLE_GV(&PL_sv_yes);
	else if (autoload)
	    gv = gv_autoload_pvn(
		ostash, name, nend - name, GV_AUTOLOAD_ISMETHOD|flags
	    );
	SensorCall(2310);if (!gv && do_croak) {
	    /* Right now this is exclusively for the benefit of S_method_common
	       in pp_hot.c  */
	    SensorCall(2298);if (stash) {
		/* If we can't find an IO::File method, it might be a call on
		 * a filehandle. If IO:File has not been loaded, try to
		 * require it first instead of croaking */
		SensorCall(2299);const char *stash_name = HvNAME_get(stash);
		SensorCall(2303);if (stash_name && memEQs(stash_name, HvNAMELEN_get(stash), "IO::File")
		    && !Perl_hv_common(aTHX_ GvHVn(PL_incgv), NULL,
				       STR_WITH_LEN("IO/File.pm"), 0,
				       HV_FETCH_ISEXISTS, NULL, 0)
		) {
		    require_pv("IO/File.pm");
		    SensorCall(2300);gv = gv_fetchmeth_pvn(stash, name, nend - name, 0, flags);
		    SensorCall(2302);if (gv)
			{/*83*/{GV * ReplaceReturn1964 = gv; SensorCall(2301); return ReplaceReturn1964;}/*84*/}
		}
		SensorCall(2304);Perl_croak(aTHX_
			   "Can't locate object method \"%"SVf
			   "\" via package \"%"HEKf"\"",
			            SVfARG(newSVpvn_flags(name, nend - name,
                                           SVs_TEMP | is_utf8)),
                                    HEKfARG(HvNAME_HEK(stash)));
	    }
	    else {
                SensorCall(2305);SV* packnamesv;

		SensorCall(2308);if (nsplit) {
		    SensorCall(2306);packnamesv = newSVpvn_flags(origname, nsplit - origname,
                                                    SVs_TEMP | is_utf8);
		} else {
		    SensorCall(2307);packnamesv = sv_2mortal(newSVsv(error_report));
		}

		SensorCall(2309);Perl_croak(aTHX_
			   "Can't locate object method \"%"SVf"\" via package \"%"SVf"\""
			   " (perhaps you forgot to load \"%"SVf"\"?)",
			   SVfARG(newSVpvn_flags(name, nend - name,
                                SVs_TEMP | is_utf8)),
                           SVfARG(packnamesv), SVfARG(packnamesv));
	    }
	}
    }
    else {/*85*/SensorCall(2311);if (autoload) {
	SensorCall(2312);CV* const cv = GvCV(gv);
	SensorCall(2322);if (!CvROOT(cv) && !CvXSUB(cv)) {
	    SensorCall(2313);GV* stubgv;
	    GV* autogv;

	    SensorCall(2318);if (CvANON(cv))
		{/*87*/SensorCall(2314);stubgv = gv;/*88*/}
	    else {
		SensorCall(2315);stubgv = CvGV(cv);
		SensorCall(2317);if (GvCV(stubgv) != cv)		/* orphaned import */
		    {/*89*/SensorCall(2316);stubgv = gv;/*90*/}
	    }
            SensorCall(2319);autogv = gv_autoload_pvn(GvSTASH(stubgv),
                                  GvNAME(stubgv), GvNAMELEN(stubgv),
                                  GV_AUTOLOAD_ISMETHOD
                                   | (GvNAMEUTF8(stubgv) ? SVf_UTF8 : 0));
	    SensorCall(2321);if (autogv)
		{/*91*/SensorCall(2320);gv = autogv;/*92*/}
	}
    ;/*86*/}}

    {GV * ReplaceReturn1963 = gv; SensorCall(2324); return ReplaceReturn1963;}
}

GV*
Perl_gv_autoload_sv(pTHX_ HV *stash, SV* namesv, U32 flags)
{
   SensorCall(2325);char *namepv;
   STRLEN namelen;
   PERL_ARGS_ASSERT_GV_AUTOLOAD_SV;
   namepv = SvPV(namesv, namelen);
   SensorCall(2326);if (SvUTF8(namesv))
       flags |= SVf_UTF8;
   {GV * ReplaceReturn1962 = gv_autoload_pvn(stash, namepv, namelen, flags); SensorCall(2327); return ReplaceReturn1962;}
}

GV*
Perl_gv_autoload_pv(pTHX_ HV *stash, const char *namepv, U32 flags)
{
   PERL_ARGS_ASSERT_GV_AUTOLOAD_PV;
   {GV * ReplaceReturn1961 = gv_autoload_pvn(stash, namepv, strlen(namepv), flags); SensorCall(2328); return ReplaceReturn1961;}
}

GV*
Perl_gv_autoload_pvn(pTHX_ HV *stash, const char *name, STRLEN len, U32 flags)
{
SensorCall(2329);    dVAR;
    GV* gv;
    CV* cv;
    HV* varstash;
    GV* vargv;
    SV* varsv;
    SV *packname = NULL;
    U32 is_utf8 = flags & SVf_UTF8 ? SVf_UTF8 : 0;

    PERL_ARGS_ASSERT_GV_AUTOLOAD_PVN;

    SensorCall(2330);if (len == S_autolen && memEQ(name, S_autoload, S_autolen))
	return NULL;
    SensorCall(2333);if (stash) {
	SensorCall(2331);if (SvTYPE(stash) < SVt_PVHV) {
            SensorCall(2332);STRLEN packname_len = 0;
            const char * const packname_ptr = SvPV_const(MUTABLE_SV(stash), packname_len);
            packname = newSVpvn_flags(packname_ptr, packname_len,
                                      SVs_TEMP | SvUTF8(stash));
	    stash = NULL;
	}
	else
	    packname = sv_2mortal(newSVhek(HvNAME_HEK(stash)));
    }
    SensorCall(2334);if (!(gv = gv_fetchmeth_pvn(stash, S_autoload, S_autolen, FALSE, is_utf8)))
	return NULL;
    SensorCall(2335);cv = GvCV(gv);

    SensorCall(2336);if (!(CvROOT(cv) || CvXSUB(cv)))
	return NULL;

    /*
     * Inheriting AUTOLOAD for non-methods works ... for now.
     */
    SensorCall(2338);if (
        !(flags & GV_AUTOLOAD_ISMETHOD)
     && (GvCVGEN(gv) || GvSTASH(gv) != stash)
    )
	{/*57*/SensorCall(2337);Perl_ck_warner_d(aTHX_ packWARN(WARN_DEPRECATED),
			 "Use of inherited AUTOLOAD for non-method %"SVf"::%"SVf"() is deprecated",
			 SVfARG(packname),
                         SVfARG(newSVpvn_flags(name, len, SVs_TEMP | is_utf8)));/*58*/}

    SensorCall(2344);if (CvISXSUB(cv)) {
        /* Instead of forcing the XSUB do another lookup for $AUTOLOAD
         * and split that value on the last '::', pass along the same data
         * via the SvPVX field in the CV, and the stash in CvSTASH.
         *
         * Due to an unfortunate accident of history, the SvPVX field
         * serves two purposes.  It is also used for the subroutine's pro-
         * type.  Since SvPVX has been documented as returning the sub name
         * for a long time, but not as returning the prototype, we have
         * to preserve the SvPVX AUTOLOAD behaviour and put the prototype
         * elsewhere.
         *
         * We put the prototype in the same allocated buffer, but after
         * the sub name.  The SvPOK flag indicates the presence of a proto-
         * type.  The CvAUTOLOAD flag indicates the presence of a sub name.
         * If both flags are on, then SvLEN is used to indicate the end of
         * the prototype (artificially lower than what is actually allo-
         * cated), at the risk of having to reallocate a few bytes unneces-
         * sarily--but that should happen very rarely, if ever.
         *
         * We use SvUTF8 for both prototypes and sub names, so if one is
         * UTF8, the other must be upgraded.
         */
	CvSTASH_set(cv, stash);
	SensorCall(2343);if (SvPOK(cv)) { /* Ouch! */
	    SensorCall(2339);SV *tmpsv = newSVpvn_flags(name, len, is_utf8);
	    STRLEN ulen;
	    const char *proto = CvPROTO(cv);
	    assert(proto);
	    SensorCall(2340);if (SvUTF8(cv))
		sv_utf8_upgrade_flags_grow(tmpsv, 0, CvPROTOLEN(cv) + 2);
	    SensorCall(2341);ulen = SvCUR(tmpsv);
	    SvCUR(tmpsv)++; /* include null in string */
	    sv_catpvn_flags(
		tmpsv, proto, CvPROTOLEN(cv), SV_CATBYTES*!SvUTF8(cv)
	    );
	    SvTEMP_on(tmpsv); /* Allow theft */
	    sv_setsv_nomg((SV *)cv, tmpsv);
	    SvTEMP_off(tmpsv);
	    SvREFCNT_dec(tmpsv);
	    SvLEN(cv) = SvCUR(cv) + 1;
	    SvCUR(cv) = ulen;
	}
	else {
	  sv_setpvn((SV *)cv, name, len);
	  SvPOK_off(cv);
	  SensorCall(2342);if (is_utf8)
            SvUTF8_on(cv);
	  else SvUTF8_off(cv);
	}
	CvAUTOLOAD_on(cv);
    }

    /*
     * Given &FOO::AUTOLOAD, set $FOO::AUTOLOAD to desired function name.
     * The subroutine's original name may not be "AUTOLOAD", so we don't
     * use that, but for lack of anything better we will use the sub's
     * original package to look up $AUTOLOAD.
     */
    SensorCall(2345);varstash = GvSTASH(CvGV(cv));
    vargv = *(GV**)hv_fetch(varstash, S_autoload, S_autolen, TRUE);
    ENTER;

    SensorCall(2346);if (!isGV(vargv)) {
	gv_init_pvn(vargv, varstash, S_autoload, S_autolen, 0);
#ifdef PERL_DONT_CREATE_GVSV
	GvSV(vargv) = newSV(0);
#endif
    }
    LEAVE;
    SensorCall(2347);varsv = GvSVn(vargv);
    sv_setsv(varsv, packname);
    sv_catpvs(varsv, "::");
    /* Ensure SvSETMAGIC() is called if necessary. In particular, to clear
       tainting if $FOO::AUTOLOAD was previously tainted, but is not now.  */
    sv_catpvn_flags(
	varsv, name, len,
	SV_SMAGIC|(is_utf8 ? SV_CATUTF8 : SV_CATBYTES)
    );
    SensorCall(2348);if (is_utf8)
        SvUTF8_on(varsv);
    {GV * ReplaceReturn1960 = gv; SensorCall(2349); return ReplaceReturn1960;}
}


/* require_tie_mod() internal routine for requiring a module
 * that implements the logic of automatic ties like %! and %-
 *
 * The "gv" parameter should be the glob.
 * "varpv" holds the name of the var, used for error messages.
 * "namesv" holds the module name. Its refcount will be decremented.
 * "methpv" holds the method name to test for to check that things
 *   are working reasonably close to as expected.
 * "flags": if flag & 1 then save the scalar before loading.
 * For the protection of $! to work (it is set by this routine)
 * the sv slot must already be magicalized.
 */
STATIC HV*
S_require_tie_mod(pTHX_ GV *gv, const char *varpv, SV* namesv, const char *methpv,const U32 flags)
{
SensorCall(2350);    dVAR;
    HV* stash = gv_stashsv(namesv, 0);

    PERL_ARGS_ASSERT_REQUIRE_TIE_MOD;

    SensorCall(2358);if (!stash || !(gv_fetchmethod_autoload(stash, methpv, FALSE))) {
	SensorCall(2351);SV *module = newSVsv(namesv);
	char varname = *varpv; /* varpv might be clobbered by load_module,
				  so save it. For the moment it's always
				  a single char. */
	const char type = varname == '[' ? '$' : '%';
	dSP;
	ENTER;
	SensorCall(2352);if ( flags & 1 )
	    save_scalar(gv);
	PUSHSTACKi(PERLSI_MAGIC);
	SensorCall(2353);Perl_load_module(aTHX_ PERL_LOADMOD_NOIMPORT, module, NULL);
	POPSTACK;
	LEAVE;
	SPAGAIN;
	stash = gv_stashsv(namesv, 0);
	SensorCall(2357);if (!stash)
	    {/*163*/SensorCall(2354);Perl_croak(aTHX_ "panic: Can't use %c%c because %"SVf" is not available",
		    type, varname, SVfARG(namesv));/*164*/}
	else {/*165*/SensorCall(2355);if (!gv_fetchmethod(stash, methpv))
	    {/*167*/SensorCall(2356);Perl_croak(aTHX_ "panic: Can't use %c%c because %"SVf" does not support method %s",
		    type, varname, SVfARG(namesv), methpv);/*168*/}/*166*/}
    }
    SvREFCNT_dec(namesv);
    {HV * ReplaceReturn1959 = stash; SensorCall(2359); return ReplaceReturn1959;}
}

/*
=for apidoc gv_stashpv

Returns a pointer to the stash for a specified package.  Uses C<strlen> to
determine the length of C<name>, then calls C<gv_stashpvn()>.

=cut
*/

HV*
Perl_gv_stashpv(pTHX_ const char *name, I32 create)
{
    PERL_ARGS_ASSERT_GV_STASHPV;
    {HV * ReplaceReturn1958 = gv_stashpvn(name, strlen(name), create); SensorCall(2360); return ReplaceReturn1958;}
}

/*
=for apidoc gv_stashpvn

Returns a pointer to the stash for a specified package.  The C<namelen>
parameter indicates the length of the C<name>, in bytes.  C<flags> is passed
to C<gv_fetchpvn_flags()>, so if set to C<GV_ADD> then the package will be
created if it does not already exist.  If the package does not exist and
C<flags> is 0 (or any other setting that does not create packages) then NULL
is returned.


=cut
*/

HV*
Perl_gv_stashpvn(pTHX_ const char *name, U32 namelen, I32 flags)
{
    SensorCall(2361);char smallbuf[128];
    char *tmpbuf;
    HV *stash;
    GV *tmpgv;
    U32 tmplen = namelen + 2;

    PERL_ARGS_ASSERT_GV_STASHPVN;

    SensorCall(2363);if (tmplen <= sizeof smallbuf)
	{/*149*/SensorCall(2362);tmpbuf = smallbuf;/*150*/}
    else
	Newx(tmpbuf, tmplen, char);
    Copy(name, tmpbuf, namelen, char);
    SensorCall(2364);tmpbuf[namelen]   = ':';
    tmpbuf[namelen+1] = ':';
    tmpgv = gv_fetchpvn_flags(tmpbuf, tmplen, flags, SVt_PVHV);
    SensorCall(2365);if (tmpbuf != smallbuf)
	Safefree(tmpbuf);
    SensorCall(2366);if (!tmpgv)
	return NULL;
    SensorCall(2367);stash = GvHV(tmpgv);
    SensorCall(2368);if (!(flags & ~GV_NOADD_MASK) && !stash) return NULL;
    assert(stash);
    SensorCall(2370);if (!HvNAME_get(stash)) {
	hv_name_set(stash, name, namelen, flags & SVf_UTF8 ? SVf_UTF8 : 0 );
	
	/* FIXME: This is a repeat of logic in gv_fetchpvn_flags */
	/* If the containing stash has multiple effective
	   names, see that this one gets them, too. */
	SensorCall(2369);if (HvAUX(GvSTASH(tmpgv))->xhv_name_count)
	    mro_package_moved(stash, NULL, tmpgv, 1);
    }
    {HV * ReplaceReturn1957 = stash; SensorCall(2371); return ReplaceReturn1957;}
}

/*
=for apidoc gv_stashsv

Returns a pointer to the stash for a specified package.  See C<gv_stashpvn>.

=cut
*/

HV*
Perl_gv_stashsv(pTHX_ SV *sv, I32 flags)
{
    SensorCall(2372);STRLEN len;
    const char * const ptr = SvPV_const(sv,len);

    PERL_ARGS_ASSERT_GV_STASHSV;

    {HV * ReplaceReturn1956 = gv_stashpvn(ptr, len, flags | SvUTF8(sv)); SensorCall(2373); return ReplaceReturn1956;}
}


GV *
Perl_gv_fetchpv(pTHX_ const char *nambeg, I32 add, const svtype sv_type) {
    PERL_ARGS_ASSERT_GV_FETCHPV;
    {GV * ReplaceReturn1955 = gv_fetchpvn_flags(nambeg, strlen(nambeg), add, sv_type); SensorCall(2374); return ReplaceReturn1955;}
}

GV *
Perl_gv_fetchsv(pTHX_ SV *name, I32 flags, const svtype sv_type) {
    SensorCall(2375);STRLEN len;
    const char * const nambeg =
       SvPV_flags_const(name, len, flags & GV_NO_SVGMAGIC ? 0 : SV_GMAGIC);
    PERL_ARGS_ASSERT_GV_FETCHSV;
    {GV * ReplaceReturn1954 = gv_fetchpvn_flags(nambeg, len, flags | SvUTF8(name), sv_type); SensorCall(2376); return ReplaceReturn1954;}
}

STATIC void
S_gv_magicalize_isa(pTHX_ GV *gv)
{
    SensorCall(2377);AV* av;

    PERL_ARGS_ASSERT_GV_MAGICALIZE_ISA;

    av = GvAVn(gv);
    GvMULTI_on(gv);
    sv_magic(MUTABLE_SV(av), MUTABLE_SV(gv), PERL_MAGIC_isa,
	     NULL, 0);
}

STATIC void
S_gv_magicalize_overload(pTHX_ GV *gv)
{
    SensorCall(2378);HV* hv;

    PERL_ARGS_ASSERT_GV_MAGICALIZE_OVERLOAD;

    hv = GvHVn(gv);
    GvMULTI_on(gv);
    hv_magic(hv, NULL, PERL_MAGIC_overload);
}

GV *
Perl_gv_fetchpvn_flags(pTHX_ const char *nambeg, STRLEN full_len, I32 flags,
		       const svtype sv_type)
{
SensorCall(2379);    dVAR;
    register const char *name = nambeg;
    register GV *gv = NULL;
    GV**gvp;
    I32 len;
    register const char *name_cursor;
    HV *stash = NULL;
    const I32 no_init = flags & (GV_NOADD_NOINIT | GV_NOINIT);
    const I32 no_expand = flags & GV_NOEXPAND;
    const I32 add = flags & ~GV_NOADD_MASK;
    const U32 is_utf8 = flags & SVf_UTF8;
    bool addmg = !!(flags & GV_ADDMG);
    const char *const name_end = nambeg + full_len;
    const char *const name_em1 = name_end - 1;
    U32 faking_it;

    PERL_ARGS_ASSERT_GV_FETCHPVN_FLAGS;

    SensorCall(2382);if (flags & GV_NOTQUAL) {
	/* Caller promised that there is no stash, so we can skip the check. */
	SensorCall(2380);len = full_len;
	SensorCall(2381);goto no_stash;
    }

    SensorCall(2384);if (full_len > 2 && *name == '*' && isIDFIRST_lazy_if(name + 1, is_utf8)) {
	/* accidental stringify on a GV? */
	SensorCall(2383);name++;
    }

    SensorCall(2408);for (name_cursor = name; name_cursor < name_end; name_cursor++) {
	SensorCall(2385);if (name_cursor < name_em1 &&
	    ((*name_cursor == ':'
	     && name_cursor[1] == ':')
	    || *name_cursor == '\''))
	{
	    SensorCall(2386);if (!stash)
		stash = PL_defstash;
	    SensorCall(2387);if (!stash || !SvREFCNT(stash)) /* symbol table under destruction */
		return NULL;

	    SensorCall(2388);len = name_cursor - name;
	    SensorCall(2403);if (name_cursor > nambeg) { /* Skip for initial :: or ' */
		SensorCall(2389);const char *key;
		SensorCall(2392);if (*name_cursor == ':') {
		    SensorCall(2390);key = name;
		    len += 2;
		} else {
		    SensorCall(2391);char *tmpbuf;
		    Newx(tmpbuf, len+2, char);
		    Copy(name, tmpbuf, len, char);
		    tmpbuf[len++] = ':';
		    tmpbuf[len++] = ':';
		    key = tmpbuf;
		}
		SensorCall(2393);gvp = (GV**)hv_fetch(stash, key, is_utf8 ? -len : len, add);
		gv = gvp ? *gvp : NULL;
		SensorCall(2395);if (gv && gv != (const GV *)&PL_sv_undef) {
		    SensorCall(2394);if (SvTYPE(gv) != SVt_PVGV)
			gv_init_pvn(gv, stash, key, len, (add & GV_ADDMULTI)|is_utf8);
		    else
			GvMULTI_on(gv);
		}
		SensorCall(2396);if (key != name)
		    Safefree(key);
		SensorCall(2397);if (!gv || gv == (const GV *)&PL_sv_undef)
		    return NULL;

		SensorCall(2402);if (!(stash = GvHV(gv)))
		{
		    SensorCall(2398);stash = GvHV(gv) = newHV();
		    SensorCall(2401);if (!HvNAME_get(stash)) {
			SensorCall(2399);if (GvSTASH(gv) == PL_defstash && len == 6
			 && strnEQ(name, "CORE", 4))
			    hv_name_set(stash, "CORE", 4, 0);
			else
			    hv_name_set(
				stash, nambeg, name_cursor-nambeg, is_utf8
			    );
			/* If the containing stash has multiple effective
			   names, see that this one gets them, too. */
			SensorCall(2400);if (HvAUX(GvSTASH(gv))->xhv_name_count)
			    mro_package_moved(stash, NULL, gv, 1);
		    }
		}
		else if (!HvNAME_get(stash))
		    hv_name_set(stash, nambeg, name_cursor - nambeg, is_utf8);
	    }

	    SensorCall(2405);if (*name_cursor == ':')
		{/*93*/SensorCall(2404);name_cursor++;/*94*/}
	    SensorCall(2406);name = name_cursor+1;
	    SensorCall(2407);if (name == name_end)
		return gv
		    ? gv : MUTABLE_GV(*hv_fetchs(PL_defstash, "main::", TRUE));
	}
    }
    SensorCall(2409);len = name_cursor - name;

    /* No stash in name, so see how we can default */

    SensorCall(2425);if (!stash) {
    no_stash:
	SensorCall(2410);if (len && isIDFIRST_lazy(name)) {
	    bool global = FALSE;

	    SensorCall(2423);switch (len) {
	    case 1:
		SensorCall(2411);if (*name == '_')
		    global = TRUE;
		SensorCall(2412);break;
	    case 3:
		SensorCall(2413);if ((name[0] == 'I' && name[1] == 'N' && name[2] == 'C')
		    || (name[0] == 'E' && name[1] == 'N' && name[2] == 'V')
		    || (name[0] == 'S' && name[1] == 'I' && name[2] == 'G'))
		    global = TRUE;
		SensorCall(2414);break;
	    case 4:
		SensorCall(2415);if (name[0] == 'A' && name[1] == 'R' && name[2] == 'G'
		    && name[3] == 'V')
		    global = TRUE;
		SensorCall(2416);break;
	    case 5:
		SensorCall(2417);if (name[0] == 'S' && name[1] == 'T' && name[2] == 'D'
		    && name[3] == 'I' && name[4] == 'N')
		    global = TRUE;
		SensorCall(2418);break;
	    case 6:
		SensorCall(2419);if ((name[0] == 'S' && name[1] == 'T' && name[2] == 'D')
		    &&((name[3] == 'O' && name[4] == 'U' && name[5] == 'T')
		       ||(name[3] == 'E' && name[4] == 'R' && name[5] == 'R')))
		    global = TRUE;
		SensorCall(2420);break;
	    case 7:
		SensorCall(2421);if (name[0] == 'A' && name[1] == 'R' && name[2] == 'G'
		    && name[3] == 'V' && name[4] == 'O' && name[5] == 'U'
		    && name[6] == 'T')
		    global = TRUE;
		SensorCall(2422);break;
	    }

	    SensorCall(2424);if (global)
		stash = PL_defstash;
	    else if (IN_PERL_COMPILETIME) {
		stash = PL_curstash;
		if (add && (PL_hints & HINT_STRICT_VARS) &&
		    sv_type != SVt_PVCV &&
		    sv_type != SVt_PVGV &&
		    sv_type != SVt_PVFM &&
		    sv_type != SVt_PVIO &&
		    !(len == 1 && sv_type == SVt_PV &&
		      (*name == 'a' || *name == 'b')) )
		{
		    gvp = (GV**)hv_fetch(stash,name,is_utf8 ? -len : len,0);
		    if (!gvp ||
			*gvp == (const GV *)&PL_sv_undef ||
			SvTYPE(*gvp) != SVt_PVGV)
		    {
			stash = NULL;
		    }
		    else {/*95*/if ((sv_type == SVt_PV   && !GvIMPORTED_SV(*gvp)) ||
			     (sv_type == SVt_PVAV && !GvIMPORTED_AV(*gvp)) ||
			     (sv_type == SVt_PVHV && !GvIMPORTED_HV(*gvp)) )
		    {
                        SV* namesv = newSVpvn_flags(name, len, SVs_TEMP | is_utf8);
			/* diag_listed_as: Variable "%s" is not imported%s */
			Perl_ck_warner_d(
			    aTHX_ packWARN(WARN_MISC),
			    "Variable \"%c%"SVf"\" is not imported",
			    sv_type == SVt_PVAV ? '@' :
			    sv_type == SVt_PVHV ? '%' : '$',
			    SVfARG(namesv));
			if (GvCVu(*gvp))
			    {/*97*/Perl_ck_warner_d(
				aTHX_ packWARN(WARN_MISC),
				"\t(Did you mean &%"SVf" instead?)\n", SVfARG(namesv)
			    );/*98*/}
			stash = NULL;
		    ;/*96*/}}
		}
	    }
	    else
		stash = CopSTASH(PL_curcop);
	}
	else
	    stash = PL_defstash;
    }

    /* By this point we should have a stash and a name */

    SensorCall(2433);if (!stash) {
	SensorCall(2426);if (add) {
	    SensorCall(2427);SV * const err = Perl_mess(aTHX_
		 "Global symbol \"%s%"SVf"\" requires explicit package name",
		 (sv_type == SVt_PV ? "$"
		  : sv_type == SVt_PVAV ? "@"
		  : sv_type == SVt_PVHV ? "%"
		  : ""), SVfARG(newSVpvn_flags(name, len, SVs_TEMP | is_utf8)));
	    GV *gv;
	    SensorCall(2428);if (USE_UTF8_IN_NAMES)
		SvUTF8_on(err);
	    qerror(err);
	    SensorCall(2429);gv = gv_fetchpvs("<none>::", GV_ADDMULTI, SVt_PVHV);
	    SensorCall(2431);if(!gv) {
		/* symbol table under destruction */
		{GV * ReplaceReturn1953 = NULL; SensorCall(2430); return ReplaceReturn1953;}
	    }	
	    SensorCall(2432);stash = GvHV(gv);
	}
	else
	    return NULL;
    }

    SensorCall(2434);if (!SvREFCNT(stash))	/* symbol table under destruction */
	return NULL;

    SensorCall(2435);gvp = (GV**)hv_fetch(stash,name,is_utf8 ? -len : len,add);
    SensorCall(2438);if (!gvp || *gvp == (const GV *)&PL_sv_undef) {
	SensorCall(2436);if (addmg) gv = (GV *)newSV(0);
	else return NULL;
    }
    else {/*99*/SensorCall(2437);gv = *gvp, addmg = 0;/*100*/}
    /* From this point on, addmg means gv has not been inserted in the
       symtab yet. */

    SensorCall(2452);if (SvTYPE(gv) == SVt_PVGV) {
	SensorCall(2439);if (add) {
	    GvMULTI_on(gv);
	    gv_init_svtype(gv, sv_type);
	    SensorCall(2446);if (len == 1 && stash == PL_defstash) {
	      SensorCall(2440);if (sv_type == SVt_PVHV || sv_type == SVt_PVGV) {
	        SensorCall(2441);if (*name == '!')
		    require_tie_mod(gv, "!", newSVpvs("Errno"), "TIEHASH", 1);
		else if (*name == '-' || *name == '+')
		    require_tie_mod(gv, name, newSVpvs("Tie::Hash::NamedCapture"), "TIEHASH", 0);
	      }
	      SensorCall(2445);if (sv_type==SVt_PV || sv_type==SVt_PVGV) {
	       SensorCall(2442);if (*name == '[')
		require_tie_mod(gv,name,newSVpvs("arybase"),"FETCH",0);
	       else {/*101*/SensorCall(2443);if (*name == '&' || *name == '`' || *name == '\'') {
		PL_sawampersand = TRUE;
		SensorCall(2444);(void)GvSVn(gv);
	       ;/*102*/}}
	      }
	    }
	    else if (len == 3 && sv_type == SVt_PVAV
	          && strnEQ(name, "ISA", 3)
	          && (!GvAV(gv) || !SvSMAGICAL(GvAV(gv))))
		gv_magicalize_isa(gv);
	}
	{GV * ReplaceReturn1952 = gv; SensorCall(2447); return ReplaceReturn1952;}
    } else {/*103*/SensorCall(2448);if (no_init) {
	assert(!addmg);
	{GV * ReplaceReturn1951 = gv; SensorCall(2449); return ReplaceReturn1951;}
    } else {/*105*/SensorCall(2450);if (no_expand && SvROK(gv)) {
	assert(!addmg);
	{GV * ReplaceReturn1950 = gv; SensorCall(2451); return ReplaceReturn1950;}
    ;/*106*/}/*104*/}}

    /* Adding a new symbol.
       Unless of course there was already something non-GV here, in which case
       we want to behave as if there was always a GV here, containing some sort
       of subroutine.
       Otherwise we run the risk of creating things like GvIO, which can cause
       subtle bugs. eg the one that tripped up SQL::Translator  */

    SensorCall(2453);faking_it = SvOK(gv);

    SensorCall(2455);if (add & GV_ADDWARN)
	{/*107*/SensorCall(2454);Perl_ck_warner_d(aTHX_ packWARN(WARN_INTERNAL), "Had to create %"SVf" unexpectedly",
                SVfARG(newSVpvn_flags(nambeg, name_end-nambeg, SVs_TEMP | is_utf8 )));/*108*/}
    gv_init_pvn(gv, stash, name, len, (add & GV_ADDMULTI)|is_utf8);

    SensorCall(2456);if ( isIDFIRST_lazy_if(name, is_utf8)
                && ! (isLEXWARN_on ? ckWARN(WARN_ONCE) : (PL_dowarn & G_WARN_ON ) ) )
        GvMULTI_on(gv) ;

    /* set up magic where warranted */
    SensorCall(2565);if (stash != PL_defstash) { /* not the main stash */
	/* We only have to check for four names here: EXPORT, ISA, OVERLOAD
	   and VERSION. All the others apply only to the main stash or to
	   CORE (which is checked right after this). */
	SensorCall(2457);if (len > 2) {
	    SensorCall(2458);const char * const name2 = name + 1;
	    SensorCall(2468);switch (*name) {
	    case 'E':
		SensorCall(2459);if (strnEQ(name2, "XPORT", 5))
		    GvMULTI_on(gv);
		SensorCall(2460);break;
	    case 'I':
		SensorCall(2461);if (strEQ(name2, "SA"))
		    gv_magicalize_isa(gv);
		SensorCall(2462);break;
	    case 'O':
		SensorCall(2463);if (strEQ(name2, "VERLOAD"))
		    gv_magicalize_overload(gv);
		SensorCall(2464);break;
	    case 'V':
		SensorCall(2465);if (strEQ(name2, "ERSION"))
		    GvMULTI_on(gv);
		SensorCall(2466);break;
	    default:
		SensorCall(2467);goto try_core;
	    }
	    SensorCall(2469);goto add_magical_gv;
	}
      try_core:
	SensorCall(2473);if (len > 1 /* shortest is uc */ && HvNAMELEN_get(stash) == 4) {
	  /* Avoid null warning: */
	  SensorCall(2470);const char * const stashname = HvNAME(stash); assert(stashname);
	  SensorCall(2472);if (strnEQ(stashname, "CORE", 4))
	    {/*109*/SensorCall(2471);S_maybe_add_coresub(aTHX_ 0, gv, name, len);/*110*/}
	}
    }
    else {/*111*/SensorCall(2474);if (len > 1) {
#ifndef EBCDIC
	SensorCall(2475);if (*name > 'V' ) {
	    NOOP;
	    /* Nothing else to do.
	       The compiler will probably turn the switch statement into a
	       branch table. Make sure we avoid even that small overhead for
	       the common case of lower case variable names.  */
	} else
#endif
	{
	    SensorCall(2476);const char * const name2 = name + 1;
	    SensorCall(2531);switch (*name) {
	    case 'A':
		SensorCall(2477);if (strEQ(name2, "RGV")) {
		    IoFLAGS(GvIOn(gv)) |= IOf_ARGV|IOf_START;
		}
		else {/*113*/SensorCall(2478);if (strEQ(name2, "RGVOUT")) {
		    GvMULTI_on(gv);
		;/*114*/}}
		SensorCall(2479);break;
	    case 'E':
		SensorCall(2480);if (strnEQ(name2, "XPORT", 5))
		    GvMULTI_on(gv);
		SensorCall(2481);break;
	    case 'I':
		SensorCall(2482);if (strEQ(name2, "SA")) {
		    gv_magicalize_isa(gv);
		}
		SensorCall(2483);break;
	    case 'O':
		SensorCall(2484);if (strEQ(name2, "VERLOAD")) {
		    gv_magicalize_overload(gv);
		}
		SensorCall(2485);break;
	    case 'S':
		SensorCall(2486);if (strEQ(name2, "IG")) {
		    SensorCall(2487);HV *hv;
		    I32 i;
		    SensorCall(2488);if (!PL_psig_name) {
			Newxz(PL_psig_name, 2 * SIG_SIZE, SV*);
			Newxz(PL_psig_pend, SIG_SIZE, int);
			PL_psig_ptr = PL_psig_name + SIG_SIZE;
		    } else {
			/* I think that the only way to get here is to re-use an
			   embedded perl interpreter, where the previous
			   use didn't clean up fully because
			   PL_perl_destruct_level was 0. I'm not sure that we
			   "support" that, in that I suspect in that scenario
			   there are sufficient other garbage values left in the
			   interpreter structure that something else will crash
			   before we get here. I suspect that this is one of
			   those "doctor, it hurts when I do this" bugs.  */
			Zero(PL_psig_name, 2 * SIG_SIZE, SV*);
			Zero(PL_psig_pend, SIG_SIZE, int);
		    }
		    GvMULTI_on(gv);
		    SensorCall(2489);hv = GvHVn(gv);
		    hv_magic(hv, NULL, PERL_MAGIC_sig);
		    SensorCall(2492);for (i = 1; i < SIG_SIZE; i++) {
			SensorCall(2490);SV * const * const init = hv_fetch(hv, PL_sig_name[i], strlen(PL_sig_name[i]), 1);
			SensorCall(2491);if (init)
			    sv_setsv(*init, &PL_sv_undef);
		    }
		}
		SensorCall(2493);break;
	    case 'V':
		SensorCall(2494);if (strEQ(name2, "ERSION"))
		    GvMULTI_on(gv);
		SensorCall(2495);break;
            case '\003':        /* $^CHILD_ERROR_NATIVE */
		SensorCall(2496);if (strEQ(name2, "HILD_ERROR_NATIVE"))
		    {/*115*/SensorCall(2497);goto magicalize;/*116*/}
		SensorCall(2498);break;
	    case '\005':	/* $^ENCODING */
		SensorCall(2499);if (strEQ(name2, "NCODING"))
		    {/*117*/SensorCall(2500);goto magicalize;/*118*/}
		SensorCall(2501);break;
	    case '\007':	/* $^GLOBAL_PHASE */
		SensorCall(2502);if (strEQ(name2, "LOBAL_PHASE"))
		    {/*119*/SensorCall(2503);goto ro_magicalize;/*120*/}
		SensorCall(2504);break;
            case '\015':        /* $^MATCH */
                SensorCall(2505);if (strEQ(name2, "ATCH"))
		    {/*121*/SensorCall(2506);goto magicalize;/*122*/}
	    case '\017':	/* $^OPEN */
		SensorCall(2507);if (strEQ(name2, "PEN"))
		    {/*123*/SensorCall(2508);goto magicalize;/*124*/}
		SensorCall(2509);break;
	    case '\020':        /* $^PREMATCH  $^POSTMATCH */
	        SensorCall(2510);if (strEQ(name2, "REMATCH") || strEQ(name2, "OSTMATCH"))
		    {/*125*/SensorCall(2511);goto magicalize;/*126*/}
		SensorCall(2512);break;
	    case '\024':	/* ${^TAINT} */
		SensorCall(2513);if (strEQ(name2, "AINT"))
		    {/*127*/SensorCall(2514);goto ro_magicalize;/*128*/}
		SensorCall(2515);break;
	    case '\025':	/* ${^UNICODE}, ${^UTF8LOCALE} */
		SensorCall(2516);if (strEQ(name2, "NICODE"))
		    {/*129*/SensorCall(2517);goto ro_magicalize;/*130*/}
		SensorCall(2519);if (strEQ(name2, "TF8LOCALE"))
		    {/*131*/SensorCall(2518);goto ro_magicalize;/*132*/}
		SensorCall(2521);if (strEQ(name2, "TF8CACHE"))
		    {/*133*/SensorCall(2520);goto magicalize;/*134*/}
		SensorCall(2522);break;
	    case '\027':	/* $^WARNING_BITS */
		SensorCall(2523);if (strEQ(name2, "ARNING_BITS"))
		    {/*135*/SensorCall(2524);goto magicalize;/*136*/}
		SensorCall(2525);break;
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
	    {
		/* Ensures that we have an all-digit variable, ${"1foo"} fails
		   this test  */
		/* This snippet is taken from is_gv_magical */
		SensorCall(2526);const char *end = name + len;
		SensorCall(2529);while (--end > name) {
		    SensorCall(2527);if (!isDIGIT(*end))	{/*137*/SensorCall(2528);goto add_magical_gv;/*138*/}
		}
		SensorCall(2530);goto magicalize;
	    }
	    }
	}
    } else {
	/* Names of length 1.  (Or 0. But name is NUL terminated, so that will
	   be case '\0' in this switch statement (ie a default case)  */
	SensorCall(2532);switch (*name) {
	case '&':		/* $& */
	case '`':		/* $` */
	case '\'':		/* $' */
	    SensorCall(2533);if (!(
		sv_type == SVt_PVAV ||
		sv_type == SVt_PVHV ||
		sv_type == SVt_PVCV ||
		sv_type == SVt_PVFM ||
		sv_type == SVt_PVIO
		)) { PL_sawampersand = TRUE; }
	    SensorCall(2534);goto magicalize;

	case ':':		/* $: */
	    sv_setpv(GvSVn(gv),PL_chopset);
	    SensorCall(2535);goto magicalize;

	case '?':		/* $? */
#ifdef COMPLEX_STATUS
	    SvUPGRADE(GvSVn(gv), SVt_PVLV);
#endif
	    SensorCall(2536);goto magicalize;

	case '!':		/* $! */
	    GvMULTI_on(gv);
	    /* If %! has been used, automatically load Errno.pm. */

	    sv_magic(GvSVn(gv), MUTABLE_SV(gv), PERL_MAGIC_sv, name, len);

            /* magicalization must be done before require_tie_mod is called */
	    SensorCall(2539);if (sv_type == SVt_PVHV || sv_type == SVt_PVGV)
	    {
		SensorCall(2537);if (addmg) (void)hv_store(stash,name,len,(SV *)gv,0);
		SensorCall(2538);addmg = 0;
		require_tie_mod(gv, "!", newSVpvs("Errno"), "TIEHASH", 1);
	    }

	    SensorCall(2540);break;
	case '-':		/* $- */
	case '+':		/* $+ */
	GvMULTI_on(gv); /* no used once warnings here */
        {
            SensorCall(2541);AV* const av = GvAVn(gv);
	    SV* const avc = (*name == '+') ? MUTABLE_SV(av) : NULL;

	    sv_magic(MUTABLE_SV(av), avc, PERL_MAGIC_regdata, NULL, 0);
            sv_magic(GvSVn(gv), MUTABLE_SV(gv), PERL_MAGIC_sv, name, len);
            SensorCall(2542);if (avc)
                SvREADONLY_on(GvSVn(gv));
            SvREADONLY_on(av);

            SensorCall(2545);if (sv_type == SVt_PVHV || sv_type == SVt_PVGV)
	    {
		SensorCall(2543);if (addmg) (void)hv_store(stash,name,len,(SV *)gv,0);
		SensorCall(2544);addmg = 0;
                require_tie_mod(gv, name, newSVpvs("Tie::Hash::NamedCapture"), "TIEHASH", 0);
	    }

            SensorCall(2546);break;
	}
	case '*':		/* $* */
	case '#':		/* $# */
	    SensorCall(2547);if (sv_type == SVt_PV)
		/* diag_listed_as: $* is no longer supported */
		{/*139*/SensorCall(2548);Perl_ck_warner_d(aTHX_ packWARN2(WARN_DEPRECATED, WARN_SYNTAX),
				 "$%c is no longer supported", *name);/*140*/}
	    SensorCall(2549);break;
	case '\010':	/* $^H */
	    {
		SensorCall(2550);HV *const hv = GvHVn(gv);
		hv_magic(hv, NULL, PERL_MAGIC_hints);
	    }
	    SensorCall(2551);goto magicalize;
	case '[':		/* $[ */
	    SensorCall(2552);if ((sv_type == SVt_PV || sv_type == SVt_PVGV)
	     && FEATURE_ARYBASE_IS_ENABLED) {
		SensorCall(2553);if (addmg) (void)hv_store(stash,name,len,(SV *)gv,0);
		require_tie_mod(gv,name,newSVpvs("arybase"),"FETCH",0);
		SensorCall(2554);addmg = 0;
	    }
	    else {/*141*/SensorCall(2555);goto magicalize;/*142*/}
            SensorCall(2556);break;
	case '\023':	/* $^S */
	ro_magicalize:
	    SvREADONLY_on(GvSVn(gv));
	    /* FALL THROUGH */
	case '0':		/* $0 */
	case '1':		/* $1 */
	case '2':		/* $2 */
	case '3':		/* $3 */
	case '4':		/* $4 */
	case '5':		/* $5 */
	case '6':		/* $6 */
	case '7':		/* $7 */
	case '8':		/* $8 */
	case '9':		/* $9 */
	case '^':		/* $^ */
	case '~':		/* $~ */
	case '=':		/* $= */
	case '%':		/* $% */
	case '.':		/* $. */
	case '(':		/* $( */
	case ')':		/* $) */
	case '<':		/* $< */
	case '>':		/* $> */
	case '\\':		/* $\ */
	case '/':		/* $/ */
	case '|':		/* $| */
	case '$':		/* $$ */
	case '\001':	/* $^A */
	case '\003':	/* $^C */
	case '\004':	/* $^D */
	case '\005':	/* $^E */
	case '\006':	/* $^F */
	case '\011':	/* $^I, NOT \t in EBCDIC */
	case '\016':	/* $^N */
	case '\017':	/* $^O */
	case '\020':	/* $^P */
	case '\024':	/* $^T */
	case '\027':	/* $^W */
	magicalize:
	    sv_magic(GvSVn(gv), MUTABLE_SV(gv), PERL_MAGIC_sv, name, len);
	    SensorCall(2557);break;

	case '\014':	/* $^L */
	    sv_setpvs(GvSVn(gv),"\f");
	    PL_formfeed = GvSVn(gv);
	    SensorCall(2558);break;
	case ';':		/* $; */
	    sv_setpvs(GvSVn(gv),"\034");
	    SensorCall(2559);break;
	case ']':		/* $] */
	{
	    SensorCall(2560);SV * const sv = GvSV(gv);
	    SensorCall(2561);if (!sv_derived_from(PL_patchlevel, "version"))
		upg_version(PL_patchlevel, TRUE);
	    GvSV(gv) = vnumify(PL_patchlevel);
	    SvREADONLY_on(GvSV(gv));
	    SvREFCNT_dec(sv);
	}
	SensorCall(2562);break;
	case '\026':	/* $^V */
	{
	    SensorCall(2563);SV * const sv = GvSV(gv);
	    GvSV(gv) = new_version(PL_patchlevel);
	    SvREADONLY_on(GvSV(gv));
	    SvREFCNT_dec(sv);
	}
	SensorCall(2564);break;
	}
    ;/*112*/}}
  add_magical_gv:
    SensorCall(2567);if (addmg) {
	SensorCall(2566);if (GvAV(gv) || GvHV(gv) || GvIO(gv) || GvCV(gv) || (
	     GvSV(gv) && (SvOK(GvSV(gv)) || SvMAGICAL(GvSV(gv)))
	   ))
	    (void)hv_store(stash,name,len,(SV *)gv,0);
	else SvREFCNT_dec(gv), gv = NULL;
    }
    SensorCall(2568);if (gv) gv_init_svtype(gv, faking_it ? SVt_PVCV : sv_type);
    {GV * ReplaceReturn1949 = gv; SensorCall(2569); return ReplaceReturn1949;}
}

void
Perl_gv_fullname4(pTHX_ SV *sv, const GV *gv, const char *prefix, bool keepmain)
{
    SensorCall(2570);const char *name;
    const HV * const hv = GvSTASH(gv);

    PERL_ARGS_ASSERT_GV_FULLNAME4;

    sv_setpv(sv, prefix ? prefix : "");

    SensorCall(2573);if (hv && (name = HvNAME(hv))) {
      SensorCall(2571);const STRLEN len = HvNAMELEN(hv);
      SensorCall(2572);if (keepmain || strnNE(name, "main", len)) {
	sv_catpvn_flags(sv,name,len,HvNAMEUTF8(hv)?SV_CATUTF8:SV_CATBYTES);
	sv_catpvs(sv,"::");
      }
    }
    else sv_catpvs(sv,"__ANON__::");
    sv_catsv(sv,sv_2mortal(newSVhek(GvNAME_HEK(gv))));
}

void
Perl_gv_efullname4(pTHX_ SV *sv, const GV *gv, const char *prefix, bool keepmain)
{
    SensorCall(2574);const GV * const egv = GvEGVx(gv);

    PERL_ARGS_ASSERT_GV_EFULLNAME4;

    gv_fullname4(sv, egv ? egv : gv, prefix, keepmain);
}

void
Perl_gv_check(pTHX_ const HV *stash)
{
SensorCall(2575);    dVAR;
    register I32 i;

    PERL_ARGS_ASSERT_GV_CHECK;

    SensorCall(2577);if (!HvARRAY(stash))
	{/*59*/SensorCall(2576);return;/*60*/}
    SensorCall(2588);for (i = 0; i <= (I32) HvMAX(stash); i++) {
        SensorCall(2578);const HE *entry;
	SensorCall(2587);for (entry = HvARRAY(stash)[i]; entry; entry = HeNEXT(entry)) {
            SensorCall(2579);register GV *gv;
            HV *hv;
	    SensorCall(2586);if (HeKEY(entry)[HeKLEN(entry)-1] == ':' &&
		(gv = MUTABLE_GV(HeVAL(entry))) && isGV(gv) && (hv = GvHV(gv)))
	    {
		SensorCall(2580);if (hv != PL_defstash && hv != stash)
		     gv_check(hv);              /* nested package */
	    }
            else {/*61*/SensorCall(2581);if ( *HeKEY(entry) != '_'
                        && isIDFIRST_lazy_if(HeKEY(entry), HeUTF8(entry)) ) {
                SensorCall(2582);const char *file;
		gv = MUTABLE_GV(HeVAL(entry));
		SensorCall(2584);if (SvTYPE(gv) != SVt_PVGV || GvMULTI(gv))
		    {/*63*/SensorCall(2583);continue;/*64*/}
		SensorCall(2585);file = GvFILE(gv);
		CopLINE_set(PL_curcop, GvLINE(gv));
#ifdef USE_ITHREADS
		CopFILE(PL_curcop) = (char *)file;	/* set for warning */
#else
		CopFILEGV(PL_curcop)
		    = gv_fetchfile_flags(file, HEK_LEN(GvFILE_HEK(gv)), 0);
#endif
		Perl_warner(aTHX_ packWARN(WARN_ONCE),
			"Name \"%"HEKf"::%"HEKf
			"\" used only once: possible typo",
                            HEKfARG(HvNAME_HEK(stash)),
                            HEKfARG(GvNAME_HEK(gv)));
	    ;/*62*/}}
	}
    }
SensorCall(2589);}

GV *
Perl_newGVgen_flags(pTHX_ const char *pack, U32 flags)
{
SensorCall(2590);    dVAR;
    PERL_ARGS_ASSERT_NEWGVGEN_FLAGS;

    {GV * ReplaceReturn1948 = gv_fetchpv(Perl_form(aTHX_ "%"SVf"::_GEN_%ld",
                                    SVfARG(newSVpvn_flags(pack, strlen(pack),
                                            SVs_TEMP | flags)),
                                (long)PL_gensym++),
                      GV_ADD, SVt_PVGV); SensorCall(2591); return ReplaceReturn1948;}
}

/* hopefully this is only called on local symbol table entries */

GP*
Perl_gp_ref(pTHX_ GP *gp)
{
SensorCall(2592);    dVAR;
    SensorCall(2593);if (!gp)
	return NULL;
    SensorCall(2594);gp->gp_refcnt++;
    SensorCall(2597);if (gp->gp_cv) {
	SensorCall(2595);if (gp->gp_cvgen) {
	    /* If the GP they asked for a reference to contains
               a method cache entry, clear it first, so that we
               don't infect them with our cached entry */
	    SvREFCNT_dec(gp->gp_cv);
	    SensorCall(2596);gp->gp_cv = NULL;
	    gp->gp_cvgen = 0;
	}
    }
    {GP * ReplaceReturn1947 = gp; SensorCall(2598); return ReplaceReturn1947;}
}

void
Perl_gp_free(pTHX_ GV *gv)
{
SensorCall(2599);    dVAR;
    GP* gp;
    int attempts = 100;

    SensorCall(2601);if (!gv || !isGV_with_GP(gv) || !(gp = GvGP(gv)))
	{/*45*/SensorCall(2600);return;/*46*/}
    SensorCall(2604);if (gp->gp_refcnt == 0) {
	SensorCall(2602);Perl_ck_warner_d(aTHX_ packWARN(WARN_INTERNAL),
			 "Attempt to free unreferenced glob pointers"
			 pTHX__FORMAT pTHX__VALUE);
        SensorCall(2603);return;
    }
    SensorCall(2608);if (--gp->gp_refcnt > 0) {
	SensorCall(2605);if (gp->gp_egv == gv)
	    {/*47*/SensorCall(2606);gp->gp_egv = 0;/*48*/}
	GvGP_set(gv, NULL);
        SensorCall(2607);return;
    }

    SensorCall(2618);while (1) {
      /* Copy and null out all the glob slots, so destructors do not see
         freed SVs. */
      SensorCall(2609);HEK * const file_hek = gp->gp_file_hek;
      SV  * const sv       = gp->gp_sv;
      AV  * const av       = gp->gp_av;
      HV  * const hv       = gp->gp_hv;
      IO  * const io       = gp->gp_io;
      CV  * const cv       = gp->gp_cv;
      CV  * const form     = gp->gp_form;

      gp->gp_file_hek = NULL;
      gp->gp_sv       = NULL;
      gp->gp_av       = NULL;
      gp->gp_hv       = NULL;
      gp->gp_io       = NULL;
      gp->gp_cv       = NULL;
      gp->gp_form     = NULL;

      SensorCall(2610);if (file_hek)
	unshare_hek(file_hek);

      SvREFCNT_dec(sv);
      SvREFCNT_dec(av);
      /* FIXME - another reference loop GV -> symtab -> GV ?
         Somehow gp->gp_hv can end up pointing at freed garbage.  */
      SensorCall(2613);if (hv && SvTYPE(hv) == SVt_PVHV) {
        SensorCall(2611);const HEK *hvname_hek = HvNAME_HEK(hv);
        SensorCall(2612);if (PL_stashcache && hvname_hek)
           (void)hv_delete(PL_stashcache, HEK_KEY(hvname_hek),
                      (HEK_UTF8(hvname_hek) ? -HEK_LEN(hvname_hek) : HEK_LEN(hvname_hek)),
                      G_DISCARD);
	SvREFCNT_dec(hv);
      }
      SvREFCNT_dec(io);
      SvREFCNT_dec(cv);
      SvREFCNT_dec(form);

      SensorCall(2615);if (!gp->gp_file_hek
       && !gp->gp_sv
       && !gp->gp_av
       && !gp->gp_hv
       && !gp->gp_io
       && !gp->gp_cv
       && !gp->gp_form) {/*49*/SensorCall(2614);break;/*50*/}

      SensorCall(2617);if (--attempts == 0) {
	SensorCall(2616);Perl_die(aTHX_
	  "panic: gp_free failed to free glob pointer - "
	  "something is repeatedly re-creating entries"
	);
      }
    }

    Safefree(gp);
    GvGP_set(gv, NULL);
}

int
Perl_magic_freeovrld(pTHX_ SV *sv, MAGIC *mg)
{
    SensorCall(2619);AMT * const amtp = (AMT*)mg->mg_ptr;
    PERL_UNUSED_ARG(sv);

    PERL_ARGS_ASSERT_MAGIC_FREEOVRLD;

    SensorCall(2625);if (amtp && AMT_AMAGIC(amtp)) {
	SensorCall(2620);int i;
	SensorCall(2624);for (i = 1; i < NofAMmeth; i++) {
	    SensorCall(2621);CV * const cv = amtp->table[i];
	    SensorCall(2623);if (cv) {
		SvREFCNT_dec(MUTABLE_SV(cv));
		SensorCall(2622);amtp->table[i] = NULL;
	    }
	}
    }
 {int  ReplaceReturn1946 = 0; SensorCall(2626); return ReplaceReturn1946;}
}

/* Updates and caches the CV's */
/* Returns:
 * 1 on success and there is some overload
 * 0 if there is no overload
 * -1 if some error occurred and it couldn't croak
 */

int
Perl_Gv_AMupdate(pTHX_ HV *stash, bool destructing)
{
SensorCall(2627);  dVAR;
  MAGIC* const mg = mg_find((const SV *)stash, PERL_MAGIC_overload_table);
  AMT amt;
  const struct mro_meta* stash_meta = HvMROMETA(stash);
  U32 newgen;

  PERL_ARGS_ASSERT_GV_AMUPDATE;

  newgen = PL_sub_generation + stash_meta->pkg_gen + stash_meta->cache_gen;
  SensorCall(2631);if (mg) {
      SensorCall(2628);const AMT * const amtp = (AMT*)mg->mg_ptr;
      SensorCall(2630);if (amtp->was_ok_am == PL_amagic_generation
	  && amtp->was_ok_sub == newgen) {
	  {int  ReplaceReturn1945 = AMT_OVERLOADED(amtp) ? 1 : 0; SensorCall(2629); return ReplaceReturn1945;}
      }
      sv_unmagic(MUTABLE_SV(stash), PERL_MAGIC_overload_table);
  }

  DEBUG_o( Perl_deb(aTHX_ "Recalcing overload magic in package %s\n",HvNAME_get(stash)) );

  Zero(&amt,1,AMT);
  SensorCall(2632);amt.was_ok_am = PL_amagic_generation;
  amt.was_ok_sub = newgen;
  amt.fallback = AMGfallNO;
  amt.flags = 0;

  {
    int filled = 0, have_ovl = 0;
    int i, lim = 1;

    /* Work with "fallback" key, which we assume to be first in PL_AMG_names */

    /* Try to find via inheritance. */
    GV *gv = gv_fetchmeth_pvn(stash, PL_AMG_names[0], 2, -1, 0);
    SV * const sv = gv ? GvSV(gv) : NULL;
    CV* cv;

    SensorCall(2634);if (!gv)
	{/*1*/SensorCall(2633);lim = DESTROY_amg;/*2*/}		/* Skip overloading entries. */
#ifdef PERL_DONT_CREATE_GVSV
    else if (!sv) {
	NOOP;   /* Equivalent to !SvTRUE and !SvOK  */
    }
#endif
    else if (SvTRUE(sv))
	amt.fallback=AMGfallYES;
    else if (SvOK(sv))
	amt.fallback=AMGfallNEVER;

    SensorCall(2635);for (i = 1; i < lim; i++)
	amt.table[i] = NULL;
    SensorCall(2657);for (; i < NofAMmeth; i++) {
	SensorCall(2636);const char * const cooky = PL_AMG_names[i];
	/* Human-readable form, for debugging: */
	const char * const cp = (i >= DESTROY_amg ? cooky : AMG_id2name(i));
	const STRLEN l = PL_AMG_namelens[i];

	DEBUG_o( Perl_deb(aTHX_ "Checking overloading of \"%s\" in package \"%.256s\"\n",
		     cp, HvNAME_get(stash)) );
	/* don't fill the cache while looking up!
	   Creation of inheritance stubs in intermediate packages may
	   conflict with the logic of runtime method substitution.
	   Indeed, for inheritance A -> B -> C, if C overloads "+0",
	   then we could have created stubs for "(+0" in A and C too.
	   But if B overloads "bool", we may want to use it for
	   numifying instead of C's "+0". */
	SensorCall(2639);if (i >= DESTROY_amg)
	    {/*3*/SensorCall(2637);gv = Perl_gv_fetchmeth_pvn_autoload(aTHX_ stash, cooky, l, 0, 0);/*4*/}
	else				/* Autoload taken care of below */
	    {/*5*/SensorCall(2638);gv = Perl_gv_fetchmeth_pvn(aTHX_ stash, cooky, l, -1, 0);/*6*/}
        SensorCall(2640);cv = 0;
        SensorCall(2655);if (gv && (cv = GvCV(gv))) {
	    SensorCall(2641);if(GvNAMELEN(CvGV(cv)) == 3 && strEQ(GvNAME(CvGV(cv)), "nil")){
	      SensorCall(2642);const char * const hvname = HvNAME_get(GvSTASH(CvGV(cv)));
	      SensorCall(2649);if (hvname && HEK_LEN(HvNAME_HEK(GvSTASH(CvGV(cv)))) == 8
	       && strEQ(hvname, "overload")) {
		/* This is a hack to support autoloading..., while
		   knowing *which* methods were declared as overloaded. */
		/* GvSV contains the name of the method. */
		SensorCall(2643);GV *ngv = NULL;
		SV *gvsv = GvSV(gv);

		DEBUG_o( Perl_deb(aTHX_ "Resolving method \"%"SVf256\
			"\" for overloaded \"%s\" in package \"%.256s\"\n",
			     (void*)GvSV(gv), cp, HvNAME(stash)) );
		SensorCall(2647);if (!gvsv || !SvPOK(gvsv)
		    || !(ngv = gv_fetchmethod_sv_flags(stash, gvsv, 0)))
		{
		    /* Can be an import stub (created by "can"). */
		    SensorCall(2644);if (destructing) {
			{int  ReplaceReturn1944 = -1; SensorCall(2645); return ReplaceReturn1944;}
		    }
		    else {
			SensorCall(2646);const SV * const name = (gvsv && SvPOK(gvsv))
                                                    ? gvsv
                                                    : newSVpvs_flags("???", SVs_TEMP);
			/* diag_listed_as: Can't resolve method "%s" overloading "%s" in package "%s" */
			Perl_croak(aTHX_ "%s method \"%"SVf256
				    "\" overloading \"%s\" "\
				    "in package \"%"HEKf256"\"",
				   (GvCVGEN(gv) ? "Stub found while resolving"
				    : "Can't resolve"),
				   SVfARG(name), cp,
                                   HEKfARG(
					HvNAME_HEK(stash)
				   ));
		    }
		}
		SensorCall(2648);cv = GvCV(gv = ngv);
	      }
	    }
	    DEBUG_o( Perl_deb(aTHX_ "Overloading \"%s\" in package \"%.256s\" via \"%.256s::%.256s\"\n",
			 cp, HvNAME_get(stash), HvNAME_get(GvSTASH(CvGV(cv))),
			 GvNAME(CvGV(cv))) );
	    SensorCall(2650);filled = 1;
	    SensorCall(2652);if (i < DESTROY_amg)
		{/*7*/SensorCall(2651);have_ovl = 1;/*8*/}
	} else {/*9*/SensorCall(2653);if (gv) {		/* Autoloaded... */
	    SensorCall(2654);cv = MUTABLE_CV(gv);
	    filled = 1;
	;/*10*/}}
	SensorCall(2656);amt.table[i]=MUTABLE_CV(SvREFCNT_inc_simple(cv));
    }
    SensorCall(2660);if (filled) {
      AMT_AMAGIC_on(&amt);
      SensorCall(2658);if (have_ovl)
	  AMT_OVERLOADED_on(&amt);
      sv_magic(MUTABLE_SV(stash), 0, PERL_MAGIC_overload_table,
						(char*)&amt, sizeof(AMT));
      {int  ReplaceReturn1943 = have_ovl; SensorCall(2659); return ReplaceReturn1943;}
    }
  }
  /* Here we have no table: */
  /* no_table: */
  AMT_AMAGIC_off(&amt);
  sv_magic(MUTABLE_SV(stash), 0, PERL_MAGIC_overload_table,
						(char*)&amt, sizeof(AMTS));
  {int  ReplaceReturn1942 = 0; SensorCall(2661); return ReplaceReturn1942;}
}


CV*
Perl_gv_handler(pTHX_ HV *stash, I32 id)
{
SensorCall(2662);    dVAR;
    MAGIC *mg;
    AMT *amtp;
    U32 newgen;
    struct mro_meta* stash_meta;

    SensorCall(2663);if (!stash || !HvNAME_get(stash))
        return NULL;

    SensorCall(2664);stash_meta = HvMROMETA(stash);
    newgen = PL_sub_generation + stash_meta->pkg_gen + stash_meta->cache_gen;

    mg = mg_find((const SV *)stash, PERL_MAGIC_overload_table);
    SensorCall(2671);if (!mg) {
      do_update:
	/* If we're looking up a destructor to invoke, we must avoid
	 * that Gv_AMupdate croaks, because we might be dying already */
	SensorCall(2665);if (Gv_AMupdate(stash, cBOOL(id == DESTROY_amg)) == -1) {
	    /* and if it didn't found a destructor, we fall back
	     * to a simpler method that will only look for the
	     * destructor instead of the whole magic */
	    SensorCall(2666);if (id == DESTROY_amg) {
		SensorCall(2667);GV * const gv = gv_fetchmethod(stash, "DESTROY");
		SensorCall(2668);if (gv)
		    return GvCV(gv);
	    }
	    {CV * ReplaceReturn1941 = NULL; SensorCall(2669); return ReplaceReturn1941;}
	}
	SensorCall(2670);mg = mg_find((const SV *)stash, PERL_MAGIC_overload_table);
    }
    assert(mg);
    SensorCall(2672);amtp = (AMT*)mg->mg_ptr;
    SensorCall(2674);if ( amtp->was_ok_am != PL_amagic_generation
	 || amtp->was_ok_sub != newgen )
	{/*143*/SensorCall(2673);goto do_update;/*144*/}
    SensorCall(2680);if (AMT_AMAGIC(amtp)) {
	SensorCall(2675);CV * const ret = amtp->table[id];
	SensorCall(2678);if (ret && isGV(ret)) {		/* Autoloading stab */
	    /* Passing it through may have resulted in a warning
	       "Inherited AUTOLOAD for a non-method deprecated", since
	       our caller is going through a function call, not a method call.
	       So return the CV for AUTOLOAD, setting $AUTOLOAD. */
	    SensorCall(2676);GV * const gv = gv_fetchmethod(stash, PL_AMG_names[id]);

	    SensorCall(2677);if (gv && GvCV(gv))
		return GvCV(gv);
	}
	{CV * ReplaceReturn1940 = ret; SensorCall(2679); return ReplaceReturn1940;}
    }

    {CV * ReplaceReturn1939 = NULL; SensorCall(2681); return ReplaceReturn1939;}
}


/* Implement tryAMAGICun_MG macro.
   Do get magic, then see if the stack arg is overloaded and if so call it.
   Flags:
	AMGf_set     return the arg using SETs rather than assigning to
		     the targ
	AMGf_numeric apply sv_2num to the stack arg.
*/

bool
Perl_try_amagic_un(pTHX_ int method, int flags) {
SensorCall(2682);    dVAR;
    dSP;
    SV* tmpsv;
    SV* const arg = TOPs;

    SvGETMAGIC(arg);

    SensorCall(2686);if (SvAMAGIC(arg) && (tmpsv = amagic_call(arg, &PL_sv_undef, method,
					      AMGf_noright | AMGf_unary))) {
	SensorCall(2683);if (flags & AMGf_set) {
	    SETs(tmpsv);
	}
	else {
	    dTARGET;
	    SensorCall(2684);if (SvPADMY(TARG)) {
		sv_setsv(TARG, tmpsv);
		SETTARG;
	    }
	    else
		SETs(tmpsv);
	}
	PUTBACK;
	{_Bool  ReplaceReturn1938 = TRUE; SensorCall(2685); return ReplaceReturn1938;}
    }

    SensorCall(2687);if ((flags & AMGf_numeric) && SvROK(arg))
	*sp = sv_2num(arg);
    {_Bool  ReplaceReturn1937 = FALSE; SensorCall(2688); return ReplaceReturn1937;}
}


/* Implement tryAMAGICbin_MG macro.
   Do get magic, then see if the two stack args are overloaded and if so
   call it.
   Flags:
	AMGf_set     return the arg using SETs rather than assigning to
		     the targ
	AMGf_assign  op may be called as mutator (eg +=)
	AMGf_numeric apply sv_2num to the stack arg.
*/

bool
Perl_try_amagic_bin(pTHX_ int method, int flags) {
SensorCall(2689);    dVAR;
    dSP;
    SV* const left = TOPm1s;
    SV* const right = TOPs;

    SvGETMAGIC(left);
    SensorCall(2690);if (left != right)
	SvGETMAGIC(right);

    SensorCall(2698);if (SvAMAGIC(left) || SvAMAGIC(right)) {
	SensorCall(2691);SV * const tmpsv = amagic_call(left, right, method,
		    ((flags & AMGf_assign) && opASSIGN ? AMGf_assign: 0));
	SensorCall(2697);if (tmpsv) {
	    SensorCall(2692);if (flags & AMGf_set) {
		SensorCall(2693);(void)POPs;
		SETs(tmpsv);
	    }
	    else {
		dATARGET;
		SensorCall(2694);(void)POPs;
		SensorCall(2695);if (opASSIGN || SvPADMY(TARG)) {
		    sv_setsv(TARG, tmpsv);
		    SETTARG;
		}
		else
		    SETs(tmpsv);
	    }
	    PUTBACK;
	    {_Bool  ReplaceReturn1936 = TRUE; SensorCall(2696); return ReplaceReturn1936;}
	}
    }
    SensorCall(2702);if(left==right && SvGMAGICAL(left)) {
	SensorCall(2699);SV * const left = sv_newmortal();
	*(sp-1) = left;
	/* Print the uninitialized warning now, so it includes the vari-
	   able name. */
	SensorCall(2701);if (!SvOK(right)) {
	    SensorCall(2700);if (ckWARN(WARN_UNINITIALIZED)) report_uninit(right);
	    sv_setsv_flags(left, &PL_sv_no, 0);
	}
	else sv_setsv_flags(left, right, 0);
	SvGETMAGIC(right);
    }
    SensorCall(2705);if (flags & AMGf_numeric) {
	SensorCall(2703);if (SvROK(TOPm1s))
	    *(sp-1) = sv_2num(TOPm1s);
	SensorCall(2704);if (SvROK(right))
	    *sp     = sv_2num(right);
    }
    {_Bool  ReplaceReturn1935 = FALSE; SensorCall(2706); return ReplaceReturn1935;}
}

SV *
Perl_amagic_deref_call(pTHX_ SV *ref, int method) {
    SensorCall(2707);SV *tmpsv = NULL;

    PERL_ARGS_ASSERT_AMAGIC_DEREF_CALL;

    SensorCall(2713);while (SvAMAGIC(ref) && 
	   (tmpsv = amagic_call(ref, &PL_sv_undef, method,
				AMGf_noright | AMGf_unary))) { 
	SensorCall(2708);if (!SvROK(tmpsv))
	    {/*31*/SensorCall(2709);Perl_croak(aTHX_ "Overloaded dereference did not return a reference");/*32*/}
	SensorCall(2711);if (tmpsv == ref || SvRV(tmpsv) == SvRV(ref)) {
	    /* Bail out if it returns us the same reference.  */
	    {SV * ReplaceReturn1934 = tmpsv; SensorCall(2710); return ReplaceReturn1934;}
	}
	SensorCall(2712);ref = tmpsv;
    }
    {SV * ReplaceReturn1933 = tmpsv ? tmpsv : ref; SensorCall(2714); return ReplaceReturn1933;}
}

bool
Perl_amagic_is_enabled(pTHX_ int method)
{
      SensorCall(2715);SV *lex_mask = cop_hints_fetch_pvs(PL_curcop, "overloading", 0);

      assert(PL_curcop->cop_hints & HINT_NO_AMAGIC);

      SensorCall(2719);if ( !lex_mask || !SvOK(lex_mask) )
	  /* overloading lexically disabled */
	  return FALSE;
      else {/*33*/SensorCall(2716);if ( lex_mask && SvPOK(lex_mask) ) {
	  /* we have an entry in the hints hash, check if method has been
	   * masked by overloading.pm */
	  SensorCall(2717);STRLEN len;
	  const int offset = method / 8;
	  const int bit    = method % 8;
	  char *pv = SvPV(lex_mask, len);

	  /* Bit set, so this overloading operator is disabled */
	  SensorCall(2718);if ( (STRLEN)offset < len && pv[offset] & ( 1 << bit ) )
	      return FALSE;
      ;/*34*/}}
      {_Bool  ReplaceReturn1932 = TRUE; SensorCall(2720); return ReplaceReturn1932;}
}

SV*
Perl_amagic_call(pTHX_ SV *left, SV *right, int method, int flags)
{
SensorCall(2721);  dVAR;
  MAGIC *mg;
  CV *cv=NULL;
  CV **cvp=NULL, **ocvp=NULL;
  AMT *amtp=NULL, *oamtp=NULL;
  int off = 0, off1, lr = 0, notfound = 0;
  int postpr = 0, force_cpy = 0;
  int assign = AMGf_assign & flags;
  const int assignshift = assign ? 1 : 0;
  int use_default_op = 0;
#ifdef DEBUGGING
  int fl=0;
#endif
  HV* stash=NULL;

  PERL_ARGS_ASSERT_AMAGIC_CALL;

  SensorCall(2723);if ( PL_curcop->cop_hints & HINT_NO_AMAGIC ) {
      SensorCall(2722);if (!amagic_is_enabled(method)) return NULL;
  }

  SensorCall(2804);if (!(AMGf_noleft & flags) && SvAMAGIC(left)
      && (stash = SvSTASH(SvRV(left)))
      && (mg = mg_find((const SV *)stash, PERL_MAGIC_overload_table))
      && (ocvp = cvp = (AMT_AMAGIC((AMT*)mg->mg_ptr)
			? (oamtp = amtp = (AMT*)mg->mg_ptr)->table
			: NULL))
      && ((cv = cvp[off=method+assignshift])
	  || (assign && amtp->fallback > AMGfallNEVER && /* fallback to
						          * usual method */
		  (
#ifdef DEBUGGING
		   fl = 1,
#endif
		   cv = cvp[off=method])))) {
    SensorCall(2724);lr = -1;			/* Call method for left argument */
  } else {
    SensorCall(2725);if (cvp && amtp->fallback > AMGfallNEVER && flags & AMGf_unary) {
      SensorCall(2726);int logic;

      /* look for substituted methods */
      /* In all the covered cases we should be called with assign==0. */
	 SensorCall(2766);switch (method) {
	 case inc_amg:
	   SensorCall(2727);force_cpy = 1;
	   SensorCall(2729);if ((cv = cvp[off=add_ass_amg])
	       || ((cv = cvp[off = add_amg]) && (force_cpy = 0, postpr = 1))) {
	     SensorCall(2728);right = &PL_sv_yes; lr = -1; assign = 1;
	   }
	   SensorCall(2730);break;
	 case dec_amg:
	   SensorCall(2731);force_cpy = 1;
	   SensorCall(2733);if ((cv = cvp[off = subtr_ass_amg])
	       || ((cv = cvp[off = subtr_amg]) && (force_cpy = 0, postpr=1))) {
	     SensorCall(2732);right = &PL_sv_yes; lr = -1; assign = 1;
	   }
	   SensorCall(2734);break;
	 case bool__amg:
	   SensorCall(2735);(void)((cv = cvp[off=numer_amg]) || (cv = cvp[off=string_amg]));
	   SensorCall(2736);break;
	 case numer_amg:
	   SensorCall(2737);(void)((cv = cvp[off=string_amg]) || (cv = cvp[off=bool__amg]));
	   SensorCall(2738);break;
	 case string_amg:
	   SensorCall(2739);(void)((cv = cvp[off=numer_amg]) || (cv = cvp[off=bool__amg]));
	   SensorCall(2740);break;
         case not_amg:
           SensorCall(2741);(void)((cv = cvp[off=bool__amg])
                  || (cv = cvp[off=numer_amg])
                  || (cv = cvp[off=string_amg]));
           SensorCall(2743);if (cv)
               {/*11*/SensorCall(2742);postpr = 1;/*12*/}
           SensorCall(2744);break;
	 case copy_amg:
	   {
	     /*
		  * SV* ref causes confusion with the interpreter variable of
		  * the same name
		  */
	     SensorCall(2745);SV* const tmpRef=SvRV(left);
	     SensorCall(2748);if (!SvROK(tmpRef) && SvTYPE(tmpRef) <= SVt_PVMG) {
		/*
		 * Just to be extra cautious.  Maybe in some
		 * additional cases sv_setsv is safe, too.
		 */
		SensorCall(2746);SV* const newref = newSVsv(tmpRef);
		SvOBJECT_on(newref);
		/* As a bit of a source compatibility hack, SvAMAGIC() and
		   friends dereference an RV, to behave the same was as when
		   overloading was stored on the reference, not the referant.
		   Hence we can't use SvAMAGIC_on()
		*/
		SvFLAGS(newref) |= SVf_AMAGIC;
		SvSTASH_set(newref, MUTABLE_HV(SvREFCNT_inc(SvSTASH(tmpRef))));
		{SV * ReplaceReturn1931 = newref; SensorCall(2747); return ReplaceReturn1931;}
	     }
	   }
	   SensorCall(2749);break;
	 case abs_amg:
	   SensorCall(2750);if ((cvp[off1=lt_amg] || cvp[off1=ncmp_amg])
	       && ((cv = cvp[off=neg_amg]) || (cv = cvp[off=subtr_amg]))) {
	     SensorCall(2751);SV* const nullsv=sv_2mortal(newSViv(0));
	     SensorCall(2754);if (off1==lt_amg) {
	       SensorCall(2752);SV* const lessp = amagic_call(left,nullsv,
				       lt_amg,AMGf_noright);
	       logic = SvTRUE(lessp);
	     } else {
	       SensorCall(2753);SV* const lessp = amagic_call(left,nullsv,
				       ncmp_amg,AMGf_noright);
	       logic = (SvNV(lessp) < 0);
	     }
	     SensorCall(2758);if (logic) {
	       SensorCall(2755);if (off==subtr_amg) {
		 SensorCall(2756);right = left;
		 left = nullsv;
		 lr = 1;
	       }
	     } else {
	       {SV * ReplaceReturn1930 = left; SensorCall(2757); return ReplaceReturn1930;}
	     }
	   }
	   SensorCall(2759);break;
	 case neg_amg:
	   SensorCall(2760);if ((cv = cvp[off=subtr_amg])) {
	     SensorCall(2761);right = left;
	     left = sv_2mortal(newSViv(0));
	     lr = 1;
	   }
	   SensorCall(2762);break;
	 case int_amg:
	 case iter_amg:			/* XXXX Eventually should do to_gv. */
	 case ftest_amg:		/* XXXX Eventually should do to_gv. */
	 case regexp_amg:
	     /* FAIL safe */
	     {SV * ReplaceReturn1929 = NULL; SensorCall(2763); return ReplaceReturn1929;}	/* Delegate operation to standard mechanisms. */
	     break;
	 case to_sv_amg:
	 case to_av_amg:
	 case to_hv_amg:
	 case to_gv_amg:
	 case to_cv_amg:
	     /* FAIL safe */
	     {SV * ReplaceReturn1928 = left; SensorCall(2764); return ReplaceReturn1928;}	/* Delegate operation to standard mechanisms. */
	     break;
	 default:
	   SensorCall(2765);goto not_found;
	 }
	 SensorCall(2768);if (!cv) {/*13*/SensorCall(2767);goto not_found;/*14*/}
    } else {/*15*/SensorCall(2769);if (!(AMGf_noright & flags) && SvAMAGIC(right)
	       && (stash = SvSTASH(SvRV(right)))
	       && (mg = mg_find((const SV *)stash, PERL_MAGIC_overload_table))
	       && (cvp = (AMT_AMAGIC((AMT*)mg->mg_ptr)
			  ? (amtp = (AMT*)mg->mg_ptr)->table
			  : NULL))
	       && (cv = cvp[off=method])) { /* Method for right
					     * argument found */
      SensorCall(2770);lr=1;
    } else {/*17*/SensorCall(2771);if (((cvp && amtp->fallback > AMGfallNEVER)
                || (ocvp && oamtp->fallback > AMGfallNEVER))
	       && !(flags & AMGf_unary)) {
				/* We look for substitution for
				 * comparison operations and
				 * concatenation */
      SensorCall(2772);if (method==concat_amg || method==concat_ass_amg
	  || method==repeat_amg || method==repeat_ass_amg) {
	{SV * ReplaceReturn1927 = NULL; SensorCall(2773); return ReplaceReturn1927;}		/* Delegate operation to string conversion */
      }
      SensorCall(2774);off = -1;
      SensorCall(2779);switch (method) {
	 case lt_amg:
	 case le_amg:
	 case gt_amg:
	 case ge_amg:
	 case eq_amg:
	 case ne_amg:
             SensorCall(2775);off = ncmp_amg;
             SensorCall(2776);break;
	 case slt_amg:
	 case sle_amg:
	 case sgt_amg:
	 case sge_amg:
	 case seq_amg:
	 case sne_amg:
             SensorCall(2777);off = scmp_amg;
             SensorCall(2778);break;
	 }
      SensorCall(2784);if (off != -1) {
          SensorCall(2780);if (ocvp && (oamtp->fallback > AMGfallNEVER)) {
              SensorCall(2781);cv = ocvp[off];
              lr = -1;
          }
          SensorCall(2783);if (!cv && (cvp && amtp->fallback > AMGfallNEVER)) {
              SensorCall(2782);cv = cvp[off];
              lr = 1;
          }
      }
      SensorCall(2787);if (cv)
          {/*19*/SensorCall(2785);postpr = 1;/*20*/}
      else
          {/*21*/SensorCall(2786);goto not_found;/*22*/}
    } else {
    not_found:			/* No method found, either report or croak */
      SensorCall(2788);switch (method) {
	 case to_sv_amg:
	 case to_av_amg:
	 case to_hv_amg:
	 case to_gv_amg:
	 case to_cv_amg:
	     /* FAIL safe */
	     {SV * ReplaceReturn1926 = left; SensorCall(2789); return ReplaceReturn1926;}	/* Delegate operation to standard mechanisms. */
	     break;
      }
      SensorCall(2802);if (ocvp && (cv=ocvp[nomethod_amg])) { /* Call report method */
	SensorCall(2790);notfound = 1; lr = -1;
      } else {/*23*/SensorCall(2791);if (cvp && (cv=cvp[nomethod_amg])) {
	SensorCall(2792);notfound = 1; lr = 1;
      } else {/*25*/SensorCall(2793);if ((use_default_op =
                  (!ocvp || oamtp->fallback >= AMGfallYES)
                  && (!cvp || amtp->fallback >= AMGfallYES))
                 && !DEBUG_o_TEST) {
	/* Skip generating the "no method found" message.  */
	{SV * ReplaceReturn1925 = NULL; SensorCall(2794); return ReplaceReturn1925;}
      } else {
	SensorCall(2795);SV *msg;
	SensorCall(2797);if (off==-1) {/*27*/SensorCall(2796);off=method;/*28*/}
	SensorCall(2798);msg = sv_2mortal(Perl_newSVpvf(aTHX_
		      "Operation \"%s\": no method found,%sargument %s%"SVf"%s%"SVf,
 		      AMG_id2name(method + assignshift),
 		      (flags & AMGf_unary ? " " : "\n\tleft "),
 		      SvAMAGIC(left)?
 		        "in overloaded package ":
 		        "has no overloaded magic",
 		      SvAMAGIC(left)?
		        SVfARG(sv_2mortal(newSVhek(HvNAME_HEK(SvSTASH(SvRV(left)))))):
		        SVfARG(&PL_sv_no),
 		      SvAMAGIC(right)?
 		        ",\n\tright argument in overloaded package ":
 		        (flags & AMGf_unary
 			 ? ""
 			 : ",\n\tright argument has no overloaded magic"),
 		      SvAMAGIC(right)?
		        SVfARG(sv_2mortal(newSVhek(HvNAME_HEK(SvSTASH(SvRV(right)))))):
		        SVfARG(&PL_sv_no)));
        SensorCall(2800);if (use_default_op) {
	  DEBUG_o( Perl_deb(aTHX_ "%"SVf, SVfARG(msg)) );
	} else {
	  SensorCall(2799);Perl_croak(aTHX_ "%"SVf, SVfARG(msg));
	}
	{SV * ReplaceReturn1924 = NULL; SensorCall(2801); return ReplaceReturn1924;}
      ;/*26*/}/*24*/}}
      SensorCall(2803);force_cpy = force_cpy || assign;
    ;/*18*/}/*16*/}}
  }
#ifdef DEBUGGING
  if (!notfound) {
    DEBUG_o(Perl_deb(aTHX_
		     "Overloaded operator \"%s\"%s%s%s:\n\tmethod%s found%s in package %"SVf"%s\n",
		     AMG_id2name(off),
		     method+assignshift==off? "" :
		     " (initially \"",
		     method+assignshift==off? "" :
		     AMG_id2name(method+assignshift),
		     method+assignshift==off? "" : "\")",
		     flags & AMGf_unary? "" :
		     lr==1 ? " for right argument": " for left argument",
		     flags & AMGf_unary? " for argument" : "",
		     stash ? SVfARG(sv_2mortal(newSVhek(HvNAME_HEK(stash)))) : SVfARG(newSVpvs_flags("null", SVs_TEMP)),
		     fl? ",\n\tassignment variant used": "") );
  }
#endif
    /* Since we use shallow copy during assignment, we need
     * to dublicate the contents, probably calling user-supplied
     * version of copy operator
     */
    /* We need to copy in following cases:
     * a) Assignment form was called.
     * 		assignshift==1,  assign==T, method + 1 == off
     * b) Increment or decrement, called directly.
     * 		assignshift==0,  assign==0, method + 0 == off
     * c) Increment or decrement, translated to assignment add/subtr.
     * 		assignshift==0,  assign==T,
     *		force_cpy == T
     * d) Increment or decrement, translated to nomethod.
     * 		assignshift==0,  assign==0,
     *		force_cpy == T
     * e) Assignment form translated to nomethod.
     * 		assignshift==1,  assign==T, method + 1 != off
     *		force_cpy == T
     */
    /*	off is method, method+assignshift, or a result of opcode substitution.
     *	In the latter case assignshift==0, so only notfound case is important.
     */
  SensorCall(2807);if ( (lr == -1) && ( ( (method + assignshift == off)
	&& (assign || (method == inc_amg) || (method == dec_amg)))
      || force_cpy) )
  {
      /* newSVsv does not behave as advertised, so we copy missing
       * information by hand */
      SensorCall(2805);SV *tmpRef = SvRV(left);
      SV *rv_copy;
      SensorCall(2806);if (SvREFCNT(tmpRef) > 1 && (rv_copy = AMG_CALLunary(left,copy_amg))) {
	  SvRV_set(left, rv_copy);
	  SvSETMAGIC(left);
	  SvREFCNT_dec(tmpRef);  
      }
  }

  {
    dSP;
    SensorCall(2808);BINOP myop;
    SV* res;
    const bool oldcatch = CATCH_GET;

    CATCH_SET(TRUE);
    Zero(&myop, 1, BINOP);
    myop.op_last = (OP *) &myop;
    myop.op_next = NULL;
    myop.op_flags = OPf_WANT_SCALAR | OPf_STACKED;

    PUSHSTACKi(PERLSI_OVERLOAD);
    ENTER;
    SAVEOP();
    PL_op = (OP *) &myop;
    SensorCall(2809);if (PERLDB_SUB && PL_curstash != PL_debstash)
	PL_op->op_private |= OPpENTERSUB_DB;
    PUTBACK;
    SensorCall(2810);Perl_pp_pushmark(aTHX);

    EXTEND(SP, notfound + 5);
    PUSHs(lr>0? right: left);
    PUSHs(lr>0? left: right);
    PUSHs( lr > 0 ? &PL_sv_yes : ( assign ? &PL_sv_undef : &PL_sv_no ));
    SensorCall(2811);if (notfound) {
      PUSHs(newSVpvn_flags(AMG_id2name(method + assignshift),
			   AMG_id2namelen(method + assignshift), SVs_TEMP));
    }
    PUSHs(MUTABLE_SV(cv));
    PUTBACK;

    SensorCall(2812);if ((PL_op = PL_ppaddr[OP_ENTERSUB](aTHX)))
      CALLRUNOPS(aTHX);
    LEAVE;
    SPAGAIN;

    SensorCall(2813);res=POPs;
    PUTBACK;
    POPSTACK;
    CATCH_SET(oldcatch);

    SensorCall(2839);if (postpr) {
      SensorCall(2814);int ans;
      SensorCall(2832);switch (method) {
      case le_amg:
      case sle_amg:
	SensorCall(2815);ans=SvIV(res)<=0; SensorCall(2816);break;
      case lt_amg:
      case slt_amg:
	SensorCall(2817);ans=SvIV(res)<0; SensorCall(2818);break;
      case ge_amg:
      case sge_amg:
	SensorCall(2819);ans=SvIV(res)>=0; SensorCall(2820);break;
      case gt_amg:
      case sgt_amg:
	SensorCall(2821);ans=SvIV(res)>0; SensorCall(2822);break;
      case eq_amg:
      case seq_amg:
	SensorCall(2823);ans=SvIV(res)==0; SensorCall(2824);break;
      case ne_amg:
      case sne_amg:
	SensorCall(2825);ans=SvIV(res)!=0; SensorCall(2826);break;
      case inc_amg:
      case dec_amg:
	SvSetSV(left,res); {SV * ReplaceReturn1923 = left; SensorCall(2827); return ReplaceReturn1923;}
      case not_amg:
	SensorCall(2828);ans=!SvTRUE(res); SensorCall(2829);break;
      default:
        SensorCall(2830);ans=0; SensorCall(2831);break;
      }
      {SV * ReplaceReturn1922 = boolSV(ans); SensorCall(2833); return ReplaceReturn1922;}
    } else {/*29*/SensorCall(2834);if (method==copy_amg) {
      SensorCall(2835);if (!SvROK(res)) {
	SensorCall(2836);Perl_croak(aTHX_ "Copy method did not return a reference");
      }
      {SV * ReplaceReturn1921 = SvREFCNT_inc(SvRV(res)); SensorCall(2837); return ReplaceReturn1921;}
    } else {
      {SV * ReplaceReturn1920 = res; SensorCall(2838); return ReplaceReturn1920;}
    ;/*30*/}}
  }
SensorCall(2840);}

void
Perl_gv_name_set(pTHX_ GV *gv, const char *name, U32 len, U32 flags)
{
SensorCall(2841);    dVAR;
    U32 hash;

    PERL_ARGS_ASSERT_GV_NAME_SET;

    SensorCall(2843);if (len > I32_MAX)
	{/*147*/SensorCall(2842);Perl_croak(aTHX_ "panic: gv name too long (%"UVuf")", (UV) len);/*148*/}

    SensorCall(2844);if (!(flags & GV_ADD) && GvNAME_HEK(gv)) {
	unshare_hek(GvNAME_HEK(gv));
    }

    PERL_HASH(hash, name, len);
    GvNAME_HEK(gv) = share_hek(name, (flags & SVf_UTF8 ? -(I32)len : (I32)len), hash);
}

/*
=for apidoc gv_try_downgrade

If the typeglob C<gv> can be expressed more succinctly, by having
something other than a real GV in its place in the stash, replace it
with the optimised form.  Basic requirements for this are that C<gv>
is a real typeglob, is sufficiently ordinary, and is only referenced
from its package.  This function is meant to be used when a GV has been
looked up in part to see what was there, causing upgrading, but based
on what was found it turns out that the real GV isn't required after all.

If C<gv> is a completely empty typeglob, it is deleted from the stash.

If C<gv> is a typeglob containing only a sufficiently-ordinary constant
sub, the typeglob is replaced with a scalar-reference placeholder that
more compactly represents the same thing.

=cut
*/

void
Perl_gv_try_downgrade(pTHX_ GV *gv)
{
    SensorCall(2845);HV *stash;
    CV *cv;
    HEK *namehek;
    SV **gvp;
    PERL_ARGS_ASSERT_GV_TRY_DOWNGRADE;

    /* XXX Why and where does this leave dangling pointers during global
       destruction? */
    SensorCall(2847);if (PL_phase == PERL_PHASE_DESTRUCT) {/*151*/SensorCall(2846);return;/*152*/}

    SensorCall(2849);if (!(SvREFCNT(gv) == 1 && SvTYPE(gv) == SVt_PVGV && !SvFAKE(gv) &&
	    !SvOBJECT(gv) && !SvREADONLY(gv) &&
	    isGV_with_GP(gv) && GvGP(gv) &&
	    !GvINTRO(gv) && GvREFCNT(gv) == 1 &&
	    !GvSV(gv) && !GvAV(gv) && !GvHV(gv) && !GvIOp(gv) && !GvFORM(gv) &&
	    GvEGVx(gv) == gv && (stash = GvSTASH(gv))))
	{/*153*/SensorCall(2848);return;/*154*/}
    SensorCall(2856);if (SvMAGICAL(gv)) {
        SensorCall(2850);MAGIC *mg;
	/* only backref magic is allowed */
	SensorCall(2852);if (SvGMAGICAL(gv) || SvSMAGICAL(gv))
	    {/*155*/SensorCall(2851);return;/*156*/}
        SensorCall(2855);for (mg = SvMAGIC(gv); mg; mg = mg->mg_moremagic) {
            SensorCall(2853);if (mg->mg_type != PERL_MAGIC_backref)
                {/*157*/SensorCall(2854);return;/*158*/}
	}
    }
    SensorCall(2857);cv = GvCV(gv);
    SensorCall(2861);if (!cv) {
	SensorCall(2858);HEK *gvnhek = GvNAME_HEK(gv);
	(void)hv_delete(stash, HEK_KEY(gvnhek),
	    HEK_UTF8(gvnhek) ? -HEK_LEN(gvnhek) : HEK_LEN(gvnhek), G_DISCARD);
    } else {/*159*/SensorCall(2859);if (GvMULTI(gv) && cv &&
	    !SvOBJECT(cv) && !SvMAGICAL(cv) && !SvREADONLY(cv) &&
	    CvSTASH(cv) == stash && CvGV(cv) == gv &&
	    CvCONST(cv) && !CvMETHOD(cv) && !CvLVALUE(cv) && !CvUNIQUE(cv) &&
	    !CvNODEBUG(cv) && !CvCLONE(cv) && !CvCLONED(cv) && !CvANON(cv) &&
	    (namehek = GvNAME_HEK(gv)) &&
	    (gvp = hv_fetch(stash, HEK_KEY(namehek),
			HEK_LEN(namehek)*(HEK_UTF8(namehek) ? -1 : 1), 0)) &&
	    *gvp == (SV*)gv) {
	SensorCall(2860);SV *value = SvREFCNT_inc(CvXSUBANY(cv).any_ptr);
	SvREFCNT(gv) = 0;
	sv_clear((SV*)gv);
	SvREFCNT(gv) = 1;
	SvFLAGS(gv) = SVt_IV|SVf_ROK;
	SvANY(gv) = (XPVGV*)((char*)&(gv->sv_u.svu_iv) -
				STRUCT_OFFSET(XPVIV, xiv_iv));
	SvRV_set(gv, value);
    ;/*160*/}}
SensorCall(2862);}

#include "XSUB.h"

static void
core_xsub(pTHX_ CV* cv)
{
    SensorCall(2863);Perl_croak(aTHX_
       "&CORE::%s cannot be called directly", GvNAME(CvGV(cv))
    );
SensorCall(2864);}

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
