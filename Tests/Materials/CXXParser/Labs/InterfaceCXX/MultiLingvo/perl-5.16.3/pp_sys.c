#include "var/tmp/sensor.h"
/*    pp_sys.c
 *
 *    Copyright (C) 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003,
 *    2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 * But only a short way ahead its floor and the walls on either side were
 * cloven by a great fissure, out of which the red glare came, now leaping
 * up, now dying down into darkness; and all the while far below there was
 * a rumour and a trouble as of great engines throbbing and labouring.
 *
 *     [p.945 of _The Lord of the Rings_, VI/iii: "Mount Doom"]
 */

/* This file contains system pp ("push/pop") functions that
 * execute the opcodes that make up a perl program. A typical pp function
 * expects to find its arguments on the stack, and usually pushes its
 * results onto the stack, hence the 'pp' terminology. Each OP structure
 * contains a pointer to the relevant pp_foo() function.
 *
 * By 'system', we mean ops which interact with the OS, such as pp_open().
 */

#include "EXTERN.h"
#define PERL_IN_PP_SYS_C
#include "perl.h"
#include "time64.h"
#include "time64.c"

#ifdef I_SHADOW
/* Shadow password support for solaris - pdo@cs.umd.edu
 * Not just Solaris: at least HP-UX, IRIX, Linux.
 * The API is from SysV.
 *
 * There are at least two more shadow interfaces,
 * see the comments in pp_gpwent().
 *
 * --jhi */
#   ifdef __hpux__
/* There is a MAXINT coming from <shadow.h> <- <hpsecurity.h> <- <values.h>
 * and another MAXINT from "perl.h" <- <sys/param.h>. */
#       undef MAXINT
#   endif
#   include <shadow.h>
#endif

#ifdef I_SYS_RESOURCE
# include <sys/resource.h>
#endif

#ifdef NETWARE
NETDB_DEFINE_CONTEXT
#endif

#ifdef HAS_SELECT
# ifdef I_SYS_SELECT
#  include <sys/select.h>
# endif
#endif

/* XXX Configure test needed.
   h_errno might not be a simple 'int', especially for multi-threaded
   applications, see "extern int errno in perl.h".  Creating such
   a test requires taking into account the differences between
   compiling multithreaded and singlethreaded ($ccflags et al).
   HOST_NOT_FOUND is typically defined in <netdb.h>.
*/
#if defined(HOST_NOT_FOUND) && !defined(h_errno) && !defined(__CYGWIN__)
extern int h_errno;
#endif

#ifdef HAS_PASSWD
# ifdef I_PWD
#  include <pwd.h>
# else
#  if !defined(VMS)
    struct passwd *getpwnam (char *);
    struct passwd *getpwuid (Uid_t);
#  endif
# endif
# ifdef HAS_GETPWENT
#ifndef getpwent
  struct passwd *getpwent (void);
#elif defined (VMS) && defined (my_getpwent)
  struct passwd *Perl_my_getpwent (pTHX);
#endif
# endif
#endif

#ifdef HAS_GROUP
# ifdef I_GRP
#  include <grp.h>
# else
    struct group *getgrnam (char *);
    struct group *getgrgid (Gid_t);
# endif
# ifdef HAS_GETGRENT
#ifndef getgrent
    struct group *getgrent (void);
#endif
# endif
#endif

#ifdef I_UTIME
#  if defined(_MSC_VER) || defined(__MINGW32__)
#    include <sys/utime.h>
#  else
#    include <utime.h>
#  endif
#endif

#ifdef HAS_CHSIZE
# ifdef my_chsize  /* Probably #defined to Perl_my_chsize in embed.h */
#   undef my_chsize
# endif
# define my_chsize PerlLIO_chsize
#else
# ifdef HAS_TRUNCATE
#   define my_chsize PerlLIO_chsize
# else
I32 my_chsize(int fd, Off_t length);
# endif
#endif

#ifdef HAS_FLOCK
#  define FLOCK flock
#else /* no flock() */

   /* fcntl.h might not have been included, even if it exists, because
      the current Configure only sets I_FCNTL if it's needed to pick up
      the *_OK constants.  Make sure it has been included before testing
      the fcntl() locking constants. */
#  if defined(HAS_FCNTL) && !defined(I_FCNTL)
#    include <fcntl.h>
#  endif

#  if defined(HAS_FCNTL) && defined(FCNTL_CAN_LOCK)
#    define FLOCK fcntl_emulate_flock
#    define FCNTL_EMULATE_FLOCK
#  else /* no flock() or fcntl(F_SETLK,...) */
#    ifdef HAS_LOCKF
#      define FLOCK lockf_emulate_flock
#      define LOCKF_EMULATE_FLOCK
#    endif /* lockf */
#  endif /* no flock() or fcntl(F_SETLK,...) */

#  ifdef FLOCK
     static int FLOCK (int, int);

    /*
     * These are the flock() constants.  Since this sytems doesn't have
     * flock(), the values of the constants are probably not available.
     */
#    ifndef LOCK_SH
#      define LOCK_SH 1
#    endif
#    ifndef LOCK_EX
#      define LOCK_EX 2
#    endif
#    ifndef LOCK_NB
#      define LOCK_NB 4
#    endif
#    ifndef LOCK_UN
#      define LOCK_UN 8
#    endif
#  endif /* emulating flock() */

#endif /* no flock() */

#define ZBTLEN 10
static const char zero_but_true[ZBTLEN + 1] = "0 but true";

#if defined(I_SYS_ACCESS) && !defined(R_OK)
#  include <sys/access.h>
#endif

#if defined(HAS_FCNTL) && defined(F_SETFD) && !defined(FD_CLOEXEC)
#  define FD_CLOEXEC 1		/* NeXT needs this */
#endif

#include "reentr.h"

#ifdef __Lynx__
/* Missing protos on LynxOS */
void sethostent(int);
void endhostent(void);
void setnetent(int);
void endnetent(void);
void setprotoent(int);
void endprotoent(void);
void setservent(int);
void endservent(void);
#endif

#undef PERL_EFF_ACCESS	/* EFFective uid/gid ACCESS */

/* F_OK unused: if stat() cannot find it... */

#if !defined(PERL_EFF_ACCESS) && defined(HAS_ACCESS) && defined(EFF_ONLY_OK) && !defined(NO_EFF_ONLY_OK)
    /* Digital UNIX (when the EFF_ONLY_OK gets fixed), UnixWare */
#   define PERL_EFF_ACCESS(p,f) (access((p), (f) | EFF_ONLY_OK))
#endif

#if !defined(PERL_EFF_ACCESS) && defined(HAS_EACCESS)
#   ifdef I_SYS_SECURITY
#       include <sys/security.h>
#   endif
#   ifdef ACC_SELF
        /* HP SecureWare */
#       define PERL_EFF_ACCESS(p,f) (eaccess((p), (f), ACC_SELF))
#   else
        /* SCO */
#       define PERL_EFF_ACCESS(p,f) (eaccess((p), (f)))
#   endif
#endif

#if !defined(PERL_EFF_ACCESS) && defined(HAS_ACCESSX) && defined(ACC_SELF)
    /* AIX */
#   define PERL_EFF_ACCESS(p,f) (accessx((p), (f), ACC_SELF))
#endif


#if !defined(PERL_EFF_ACCESS) && defined(HAS_ACCESS)	\
    && (defined(HAS_SETREUID) || defined(HAS_SETRESUID)		\
	|| defined(HAS_SETREGID) || defined(HAS_SETRESGID))
/* The Hard Way. */
STATIC int
S_emulate_eaccess(pTHX_ const char* path, Mode_t mode)
{
    const Uid_t ruid = getuid();
    const Uid_t euid = geteuid();
    const Gid_t rgid = getgid();
    const Gid_t egid = getegid();
    int res;

#if !defined(HAS_SETREUID) && !defined(HAS_SETRESUID)
    Perl_croak(aTHX_ "switching effective uid is not implemented");
#else
#ifdef HAS_SETREUID
    if (setreuid(euid, ruid))
#else
#ifdef HAS_SETRESUID
    if (setresuid(euid, ruid, (Uid_t)-1))
#endif
#endif
	/* diag_listed_as: entering effective %s failed */
	Perl_croak(aTHX_ "entering effective uid failed");
#endif

#if !defined(HAS_SETREGID) && !defined(HAS_SETRESGID)
    Perl_croak(aTHX_ "switching effective gid is not implemented");
#else
#ifdef HAS_SETREGID
    if (setregid(egid, rgid))
#else
#ifdef HAS_SETRESGID
    if (setresgid(egid, rgid, (Gid_t)-1))
#endif
#endif
	/* diag_listed_as: entering effective %s failed */
	Perl_croak(aTHX_ "entering effective gid failed");
#endif

    res = access(path, mode);

#ifdef HAS_SETREUID
    if (setreuid(ruid, euid))
#else
#ifdef HAS_SETRESUID
    if (setresuid(ruid, euid, (Uid_t)-1))
#endif
#endif
	/* diag_listed_as: leaving effective %s failed */
	Perl_croak(aTHX_ "leaving effective uid failed");

#ifdef HAS_SETREGID
    if (setregid(rgid, egid))
#else
#ifdef HAS_SETRESGID
    if (setresgid(rgid, egid, (Gid_t)-1))
#endif
#endif
	/* diag_listed_as: leaving effective %s failed */
	Perl_croak(aTHX_ "leaving effective gid failed");

    return res;
}
#   define PERL_EFF_ACCESS(p,f) (S_emulate_eaccess(aTHX_ (p), (f)))
#endif

PP(pp_backtick)
{
SensorCall(16832);    dVAR; dSP; dTARGET;
    PerlIO *fp;
    const char * const tmps = POPpconstx;
    const I32 gimme = GIMME_V;
    const char *mode = "r";

    TAINT_PROPER("``");
    SensorCall(16836);if (PL_op->op_private & OPpOPEN_IN_RAW)
	{/*13*/SensorCall(16833);mode = "rb";/*14*/}
    else {/*15*/SensorCall(16834);if (PL_op->op_private & OPpOPEN_IN_CRLF)
	{/*17*/SensorCall(16835);mode = "rt";/*18*/}/*16*/}
    SensorCall(16837);fp = PerlProc_popen(tmps, mode);
    SensorCall(16852);if (fp) {
        SensorCall(16838);const char * const type = Perl_PerlIO_context_layers(aTHX_ NULL);
	SensorCall(16840);if (type && *type)
	    {/*19*/SensorCall(16839);PerlIO_apply_layers(aTHX_ fp,mode,type);/*20*/}

	SensorCall(16850);if (gimme == G_VOID) {
	    SensorCall(16841);char tmpbuf[256];
	    SensorCall(16842);while (PerlIO_read(fp, tmpbuf, sizeof tmpbuf) > 0)
		NOOP;
	}
	else {/*21*/SensorCall(16843);if (gimme == G_SCALAR) {
	    ENTER_with_name("backtick");
	    SAVESPTR(PL_rs);
	    PL_rs = &PL_sv_undef;
	    sv_setpvs(TARG, "");	/* note that this preserves previous buffer */
	    SensorCall(16844);while (sv_gets(TARG, fp, SvCUR(TARG)) != NULL)
		NOOP;
	    LEAVE_with_name("backtick");
	    XPUSHs(TARG);
	    SvTAINTED_on(TARG);
	}
	else {
	    SensorCall(16845);for (;;) {
		SensorCall(16846);SV * const sv = newSV(79);
		SensorCall(16848);if (sv_gets(sv, fp, 0) == NULL) {
		    SvREFCNT_dec(sv);
		    SensorCall(16847);break;
		}
		mXPUSHs(sv);
		SensorCall(16849);if (SvLEN(sv) - SvCUR(sv) > 20) {
		    SvPV_shrink_to_cur(sv);
		}
		SvTAINTED_on(sv);
	    }
	;/*22*/}}
	STATUS_NATIVE_CHILD_SET(PerlProc_pclose(fp));
	TAINT;		/* "I believe that this is not gratuitous!" */
    }
    else {
	STATUS_NATIVE_CHILD_SET(-1);
	SensorCall(16851);if (gimme == G_SCALAR)
	    RETPUSHUNDEF;
    }

    RETURN;
}

PP(pp_glob)
{
SensorCall(16853);    dVAR;
    OP *result;
    dSP;
    /* make a copy of the pattern if it is gmagical, to ensure that magic
     * is called once and only once */
    SensorCall(16854);if (SvGMAGICAL(TOPm1s)) TOPm1s = sv_2mortal(newSVsv(TOPm1s));

    tryAMAGICunTARGET(iter_amg, -1, (PL_op->op_flags & OPf_SPECIAL));

    SensorCall(16856);if (PL_op->op_flags & OPf_SPECIAL) {
	/* call Perl-level glob function instead. Stack args are:
	 * MARK, wildcard, csh_glob context index
	 * and following OPs should be: gv(CORE::GLOBAL::glob), entersub
	 * */
	{OP * ReplaceReturn1294 = NORMAL; SensorCall(16855); return ReplaceReturn1294;}
    }
    /* stack args are: wildcard, gv(_GEN_n) */

    SensorCall(16858);if (PL_globhook) {
	SETs(GvSV(TOPs));
	PL_globhook(aTHX);
	{OP * ReplaceReturn1293 = NORMAL; SensorCall(16857); return ReplaceReturn1293;}
    }

    /* Note that we only ever get here if File::Glob fails to load
     * without at the same time croaking, for some reason, or if
     * perl was built with PERL_EXTERNAL_GLOB */

    ENTER_with_name("glob");

#ifndef VMS
    SensorCall(16859);if (PL_tainting) {
	/*
	 * The external globbing program may use things we can't control,
	 * so for security reasons we must assume the worst.
	 */
	TAINT;
	taint_proper(PL_no_security, "glob");
    }
#endif /* !VMS */

    SAVESPTR(PL_last_in_gv);	/* We don't want this to be permanent. */
    PL_last_in_gv = MUTABLE_GV(*PL_stack_sp--);

    SAVESPTR(PL_rs);		/* This is not permanent, either. */
    PL_rs = newSVpvs_flags("\000", SVs_TEMP);
#ifndef DOSISH
#ifndef CSH
    *SvPVX(PL_rs) = '\n';
#endif	/* !CSH */
#endif	/* !DOSISH */

    SensorCall(16860);result = do_readline();
    LEAVE_with_name("glob");
    {OP * ReplaceReturn1292 = result; SensorCall(16861); return ReplaceReturn1292;}
}

PP(pp_rcatline)
{
SensorCall(16862);    dVAR;
    PL_last_in_gv = cGVOP_gv;
    {OP * ReplaceReturn1291 = do_readline(); SensorCall(16863); return ReplaceReturn1291;}
}

PP(pp_warn)
{
SensorCall(16864);    dVAR; dSP; dMARK;
    SV *exsv;
    STRLEN len;
    SensorCall(16869);if (SP - MARK > 1) {
	dTARGET;
	do_join(TARG, &PL_sv_no, MARK, SP);
	SensorCall(16865);exsv = TARG;
	SP = MARK + 1;
    }
    else {/*185*/SensorCall(16866);if (SP == MARK) {
	SensorCall(16867);exsv = &PL_sv_no;
	EXTEND(SP, 1);
	SP = MARK + 1;
    }
    else {
	SensorCall(16868);exsv = TOPs;
    ;/*186*/}}

    SensorCall(16875);if (SvROK(exsv) || (SvPV_const(exsv, len), len)) {
	/* well-formed exception supplied */
    }
    else {/*187*/SensorCall(16870);if (SvROK(ERRSV)) {
	SensorCall(16871);exsv = ERRSV;
    }
    else {/*189*/SensorCall(16872);if (SvPOK(ERRSV) && SvCUR(ERRSV)) {
	SensorCall(16873);exsv = sv_mortalcopy(ERRSV);
	sv_catpvs(exsv, "\t...caught");
    }
    else {
	SensorCall(16874);exsv = newSVpvs_flags("Warning: something's wrong", SVs_TEMP);
    ;/*190*/}/*188*/}}
    SensorCall(16877);if (SvROK(exsv) && !PL_warnhook)
	 {/*191*/SensorCall(16876);Perl_warn(aTHX_ "%"SVf, SVfARG(exsv));/*192*/}
    else warn_sv(exsv);
    RETSETYES;
}

PP(pp_die)
{
SensorCall(16878);    dVAR; dSP; dMARK;
    SV *exsv;
    STRLEN len;
#ifdef VMS
    VMSISH_HUSHED  = VMSISH_HUSHED || (PL_op->op_private & OPpHUSH_VMSISH);
#endif
    SensorCall(16881);if (SP - MARK != 1) {
	dTARGET;
	do_join(TARG, &PL_sv_no, MARK, SP);
	SensorCall(16879);exsv = TARG;
	SP = MARK + 1;
    }
    else {
	SensorCall(16880);exsv = TOPs;
    }

    SensorCall(16891);if (SvROK(exsv) || (SvPV_const(exsv, len), len)) {
	/* well-formed exception supplied */
    }
    else {/*27*/SensorCall(16882);if (SvROK(ERRSV)) {
	SensorCall(16883);exsv = ERRSV;
	SensorCall(16887);if (sv_isobject(exsv)) {
	    SensorCall(16884);HV * const stash = SvSTASH(SvRV(exsv));
	    GV * const gv = gv_fetchmethod(stash, "PROPAGATE");
	    SensorCall(16886);if (gv) {
		SensorCall(16885);SV * const file = sv_2mortal(newSVpv(CopFILE(PL_curcop),0));
		SV * const line = sv_2mortal(newSVuv(CopLINE(PL_curcop)));
		EXTEND(SP, 3);
		PUSHMARK(SP);
		PUSHs(exsv);
		PUSHs(file);
		PUSHs(line);
		PUTBACK;
		call_sv(MUTABLE_SV(GvCV(gv)),
			G_SCALAR|G_EVAL|G_KEEPERR);
		exsv = sv_mortalcopy(*PL_stack_sp--);
	    }
	}
    }
    else {/*29*/SensorCall(16888);if (SvPV_const(ERRSV, len), len) {
	SensorCall(16889);exsv = sv_mortalcopy(ERRSV);
	sv_catpvs(exsv, "\t...propagated");
    }
    else {
	SensorCall(16890);exsv = newSVpvs_flags("Died", SVs_TEMP);
    ;/*30*/}/*28*/}}
    {OP * ReplaceReturn1290 = die_sv(exsv); SensorCall(16892); return ReplaceReturn1290;}
}

/* I/O. */

OP *
Perl_tied_method(pTHX_ const char *const methname, SV **sp, SV *const sv,
		 const MAGIC *const mg, const U32 flags, U32 argc, ...)
{
    SensorCall(16893);SV **orig_sp = sp;
    I32 ret_args;

    PERL_ARGS_ASSERT_TIED_METHOD;

    /* Ensure that our flag bits do not overlap.  */
    assert((TIED_METHOD_MORTALIZE_NOT_NEEDED & G_WANT) == 0);
    assert((TIED_METHOD_ARGUMENTS_ON_STACK & G_WANT) == 0);
    assert((TIED_METHOD_SAY & G_WANT) == 0);

    PUTBACK; /* sp is at *foot* of args, so this pops args from old stack */
    PUSHSTACKi(PERLSI_MAGIC);
    EXTEND(SP, argc+1); /* object + args */
    PUSHMARK(sp);
    PUSHs(SvTIED_obj(sv, mg));
    SensorCall(16900);if (flags & TIED_METHOD_ARGUMENTS_ON_STACK) {
	Copy(orig_sp + 2, sp + 1, argc, SV*); /* copy args to new stack */
	SensorCall(16894);sp += argc;
    }
    else {/*1*/SensorCall(16895);if (argc) {
	SensorCall(16896);const U32 mortalize_not_needed
	    = flags & TIED_METHOD_MORTALIZE_NOT_NEEDED;
	va_list args;
	va_start(args, argc);
	SensorCall(16899);do {
	    SensorCall(16897);SV *const arg = va_arg(args, SV *);
	    SensorCall(16898);if(mortalize_not_needed)
		PUSHs(arg);
	    else
		mPUSHs(arg);
	} while (--argc);
	va_end(args);
    ;/*2*/}}

    PUTBACK;
    ENTER_with_name("call_tied_method");
    SensorCall(16901);if (flags & TIED_METHOD_SAY) {
	/* local $\ = "\n" */
	SAVEGENERICSV(PL_ors_sv);
	PL_ors_sv = newSVpvs("\n");
    }
    SensorCall(16902);ret_args = call_method(methname, flags & G_WANT);
    SPAGAIN;
    orig_sp = sp;
    POPSTACK;
    SPAGAIN;
    SensorCall(16904);if (ret_args) { /* copy results back to original stack */
	EXTEND(sp, ret_args);
	Copy(orig_sp - ret_args + 1, sp + 1, ret_args, SV*);
	SensorCall(16903);sp += ret_args;
	PUTBACK;
    }
    LEAVE_with_name("call_tied_method");
    {OP * ReplaceReturn1289 = NORMAL; SensorCall(16905); return ReplaceReturn1289;}
}

#define tied_method0(a,b,c,d)		\
    Perl_tied_method(aTHX_ a,b,c,d,G_SCALAR,0)
#define tied_method1(a,b,c,d,e)		\
    Perl_tied_method(aTHX_ a,b,c,d,G_SCALAR,1,e)
#define tied_method2(a,b,c,d,e,f)	\
    Perl_tied_method(aTHX_ a,b,c,d,G_SCALAR,2,e,f)

PP(pp_open)
{
SensorCall(16906);    dVAR; dSP;
    dMARK; dORIGMARK;
    dTARGET;
    SV *sv;
    IO *io;
    const char *tmps;
    STRLEN len;
    bool  ok;

    GV * const gv = MUTABLE_GV(*++MARK);

    SensorCall(16907);if (!isGV(gv) && !(SvTYPE(gv) == SVt_PVLV && isGV_with_GP(gv)))
	DIE(aTHX_ PL_no_usym, "filehandle");

    SensorCall(16914);if ((io = GvIOp(gv))) {
	SensorCall(16908);const MAGIC *mg;
	IoFLAGS(GvIOp(gv)) &= ~IOf_UNTAINT;

	SensorCall(16910);if (IoDIRP(io))
	    {/*79*/SensorCall(16909);Perl_ck_warner_d(aTHX_ packWARN2(WARN_IO, WARN_DEPRECATED),
			     "Opening dirhandle %"HEKf" also as a file",
			     HEKfARG(GvENAME_HEK(gv)));/*80*/}

	SensorCall(16911);mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	SensorCall(16913);if (mg) {
	    /* Method's args are same as ours ... */
	    /* ... except handle is replaced by the object */
	    {OP * ReplaceReturn1288 = Perl_tied_method(aTHX_ "OPEN", mark - 1, MUTABLE_SV(io), mg,
				    G_SCALAR | TIED_METHOD_ARGUMENTS_ON_STACK,
				    sp - mark); SensorCall(16912); return ReplaceReturn1288;}
	}
    }

    SensorCall(16917);if (MARK < SP) {
	SensorCall(16915);sv = *++MARK;
    }
    else {
	SensorCall(16916);sv = GvSVn(gv);
    }

    SensorCall(16918);tmps = SvPV_const(sv, len);
    ok = do_openn(gv, tmps, len, FALSE, O_RDONLY, 0, NULL, MARK+1, (SP-MARK));
    SP = ORIGMARK;
    SensorCall(16919);if (ok)
	PUSHi( (I32)PL_forkprocess );
    else if (PL_forkprocess == 0)		/* we are a new child */
	PUSHi(0);
    else
	RETPUSHUNDEF;
    RETURN;
}

PP(pp_close)
{
SensorCall(16920);    dVAR; dSP;
    GV * const gv =
	MAXARG == 0 || (!TOPs && !POPs) ? PL_defoutgv : MUTABLE_GV(POPs);

    SensorCall(16921);if (MAXARG == 0)
	EXTEND(SP, 1);

    SensorCall(16927);if (gv) {
	SensorCall(16922);IO * const io = GvIO(gv);
	SensorCall(16926);if (io) {
	    SensorCall(16923);const MAGIC * const mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	    SensorCall(16925);if (mg) {
		{OP * ReplaceReturn1287 = tied_method0("CLOSE", SP, MUTABLE_SV(io), mg); SensorCall(16924); return ReplaceReturn1287;}
	    }
	}
    }
    PUSHs(boolSV(do_close(gv, TRUE)));
    RETURN;
}

PP(pp_pipe_op)
{
SensorCall(16928);
#ifdef HAS_PIPE
    dVAR;
    dSP;
    register IO *rstio;
    register IO *wstio;
    int fd[2];

    GV * const wgv = MUTABLE_GV(POPs);
    GV * const rgv = MUTABLE_GV(POPs);

    SensorCall(16930);if (!rgv || !wgv)
	{/*87*/SensorCall(16929);goto badexit;/*88*/}

    SensorCall(16931);if (!isGV_with_GP(rgv) || !isGV_with_GP(wgv))
	DIE(aTHX_ PL_no_usym, "filehandle");
    SensorCall(16932);rstio = GvIOn(rgv);
    wstio = GvIOn(wgv);

    SensorCall(16933);if (IoIFP(rstio))
	do_close(rgv, FALSE);
    SensorCall(16934);if (IoIFP(wstio))
	do_close(wgv, FALSE);

    SensorCall(16936);if (PerlProc_pipe(fd) < 0)
	{/*89*/SensorCall(16935);goto badexit;/*90*/}

    IoIFP(rstio) = PerlIO_fdopen(fd[0], "r"PIPE_OPEN_MODE);
    IoOFP(wstio) = PerlIO_fdopen(fd[1], "w"PIPE_OPEN_MODE);
    IoOFP(rstio) = IoIFP(rstio);
    IoIFP(wstio) = IoOFP(wstio);
    IoTYPE(rstio) = IoTYPE_RDONLY;
    IoTYPE(wstio) = IoTYPE_WRONLY;

    SensorCall(16940);if (!IoIFP(rstio) || !IoOFP(wstio)) {
	SensorCall(16937);if (IoIFP(rstio))
	    PerlIO_close(IoIFP(rstio));
	else
	    PerlLIO_close(fd[0]);
	SensorCall(16938);if (IoOFP(wstio))
	    PerlIO_close(IoOFP(wstio));
	else
	    PerlLIO_close(fd[1]);
	SensorCall(16939);goto badexit;
    }
#if defined(HAS_FCNTL) && defined(F_SETFD)
    SensorCall(16941);fcntl(fd[0],F_SETFD,fd[0] > PL_maxsysfd);	/* ensure close-on-exec */
    fcntl(fd[1],F_SETFD,fd[1] > PL_maxsysfd);	/* ensure close-on-exec */
#endif
    RETPUSHYES;

badexit:
    RETPUSHUNDEF;
#else
    DIE(aTHX_ PL_no_func, "pipe");
#endif
}

PP(pp_fileno)
{
SensorCall(16942);    dVAR; dSP; dTARGET;
    GV *gv;
    IO *io;
    PerlIO *fp;
    const MAGIC *mg;

    SensorCall(16943);if (MAXARG < 1)
	RETPUSHUNDEF;
    SensorCall(16944);gv = MUTABLE_GV(POPs);
    io = GvIO(gv);

    SensorCall(16946);if (io
	&& (mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar)))
    {
	{OP * ReplaceReturn1286 = tied_method0("FILENO", SP, MUTABLE_SV(io), mg); SensorCall(16945); return ReplaceReturn1286;}
    }

    SensorCall(16947);if (!io || !(fp = IoIFP(io))) {
	/* Can't do this because people seem to do things like
	   defined(fileno($foo)) to check whether $foo is a valid fh.

	   report_evil_fh(gv);
	    */
	RETPUSHUNDEF;
    }

    PUSHi(PerlIO_fileno(fp));
    RETURN;
}

PP(pp_umask)
{
SensorCall(16948);    dVAR;
    dSP;
#ifdef HAS_UMASK
    dTARGET;
    Mode_t anum;

    SensorCall(16951);if (MAXARG < 1 || (!TOPs && !POPs)) {
	SensorCall(16949);anum = PerlLIO_umask(022);
	/* setting it to 022 between the two calls to umask avoids
	 * to have a window where the umask is set to 0 -- meaning
	 * that another thread could create world-writeable files. */
	SensorCall(16950);if (anum != 022)
	    (void)PerlLIO_umask(anum);
    }
    else
	anum = PerlLIO_umask(POPi);
    TAINT_PROPER("umask");
    XPUSHi(anum);
#else
    /* Only DIE if trying to restrict permissions on "user" (self).
     * Otherwise it's harmless and more useful to just return undef
     * since 'group' and 'other' concepts probably don't exist here. */
    if (MAXARG >= 1 && (TOPs||POPs) && (POPi & 0700))
	DIE(aTHX_ "umask not implemented");
    XPUSHs(&PL_sv_undef);
#endif
    RETURN;
}

PP(pp_binmode)
{
SensorCall(16952);    dVAR; dSP;
    GV *gv;
    IO *io;
    PerlIO *fp;
    SV *discp = NULL;

    SensorCall(16953);if (MAXARG < 1)
	RETPUSHUNDEF;
    SensorCall(16955);if (MAXARG > 1) {
	SensorCall(16954);discp = POPs;
    }

    SensorCall(16956);gv = MUTABLE_GV(POPs);
    io = GvIO(gv);

    SensorCall(16960);if (io) {
	SensorCall(16957);const MAGIC * const mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	SensorCall(16959);if (mg) {
	    /* This takes advantage of the implementation of the varargs
	       function, which I don't think that the optimiser will be able to
	       figure out. Although, as it's a static function, in theory it
	       could.  */
	    {OP * ReplaceReturn1285 = Perl_tied_method(aTHX_ "BINMODE", SP, MUTABLE_SV(io), mg,
				    G_SCALAR|TIED_METHOD_MORTALIZE_NOT_NEEDED,
				    discp ? 1 : 0, discp); SensorCall(16958); return ReplaceReturn1285;}
	}
    }

    SensorCall(16961);if (!io || !(fp = IoIFP(io))) {
	report_evil_fh(gv);
	SETERRNO(EBADF,RMS_IFI);
        RETPUSHUNDEF;
    }

    PUTBACK;
    {
	SensorCall(16962);STRLEN len = 0;
	const char *d = NULL;
	int mode;
	SensorCall(16963);if (discp)
	    d = SvPV_const(discp, len);
	SensorCall(16964);mode = mode_from_discipline(d, len);
	SensorCall(16967);if (PerlIO_binmode(aTHX_ fp, IoTYPE(io), mode, d)) {
	    SensorCall(16965);if (IoOFP(io) && IoOFP(io) != IoIFP(io)) {
		SensorCall(16966);if (!PerlIO_binmode(aTHX_ IoOFP(io), IoTYPE(io), mode, d)) {
		    SPAGAIN;
		    RETPUSHUNDEF;
		}
	    }
	    SPAGAIN;
	    RETPUSHYES;
	}
	else {
	    SPAGAIN;
	    RETPUSHUNDEF;
	}
    }
SensorCall(16968);}

PP(pp_tie)
{
SensorCall(16969);    dVAR; dSP; dMARK;
    HV* stash;
    GV *gv = NULL;
    SV *sv;
    const I32 markoff = MARK - PL_stack_base;
    const char *methname;
    int how = PERL_MAGIC_tied;
    U32 items;
    SV *varsv = *++MARK;

    SensorCall(16985);switch(SvTYPE(varsv)) {
	case SVt_PVHV:
	{
	    SensorCall(16970);HE *entry;
	    methname = "TIEHASH";
	    SensorCall(16971);if (HvLAZYDEL(varsv) && (entry = HvEITER((HV *)varsv))) {
		HvLAZYDEL_off(varsv);
		hv_free_ent((HV *)varsv, entry);
	    }
	    HvEITER_set(MUTABLE_HV(varsv), 0);
	    SensorCall(16972);break;
	}
	case SVt_PVAV:
	    SensorCall(16973);methname = "TIEARRAY";
	    SensorCall(16976);if (!AvREAL(varsv)) {
		SensorCall(16974);if (!AvREIFY(varsv))
		    {/*171*/SensorCall(16975);Perl_croak(aTHX_ "Cannot tie unreifiable array");/*172*/}
		av_clear((AV *)varsv);
		AvREIFY_off(varsv);
		AvREAL_on(varsv);
	    }
	    SensorCall(16977);break;
	case SVt_PVGV:
	case SVt_PVLV:
	    SensorCall(16978);if (isGV_with_GP(varsv) && !SvFAKE(varsv)) {
		SensorCall(16979);methname = "TIEHANDLE";
		how = PERL_MAGIC_tiedscalar;
		/* For tied filehandles, we apply tiedscalar magic to the IO
		   slot of the GP rather than the GV itself. AMS 20010812 */
		SensorCall(16980);if (!GvIOp(varsv))
		    GvIOp(varsv) = newIO();
		SensorCall(16981);varsv = MUTABLE_SV(GvIOp(varsv));
		SensorCall(16982);break;
	    }
	    /* FALL THROUGH */
	default:
	    SensorCall(16983);methname = "TIESCALAR";
	    how = PERL_MAGIC_tiedscalar;
	    SensorCall(16984);break;
    }
    SensorCall(16986);items = SP - MARK++;
    SensorCall(16991);if (sv_isobject(*MARK)) { /* Calls GET magic. */
	ENTER_with_name("call_TIE");
	PUSHSTACKi(PERLSI_MAGIC);
	PUSHMARK(SP);
	EXTEND(SP,(I32)items);
	SensorCall(16987);while (items--)
	    PUSHs(*MARK++);
	PUTBACK;
	call_method(methname, G_SCALAR);
    }
    else {
	/* Can't use call_method here, else this: fileno FOO; tie @a, "FOO"
	 * will attempt to invoke IO::File::TIEARRAY, with (best case) the
	 * wrong error message, and worse case, supreme action at a distance.
	 * (Sorry obfuscation writers. You're not going to be given this one.)
	 */
       SensorCall(16988);stash = gv_stashsv(*MARK, 0);
       SensorCall(16989);if (!stash || !(gv = gv_fetchmethod(stash, methname))) {
	    DIE(aTHX_ "Can't locate object method \"%s\" via package \"%"SVf"\"",
		 methname, SVfARG(SvOK(*MARK) ? *MARK : &PL_sv_no));
	}
	ENTER_with_name("call_TIE");
	PUSHSTACKi(PERLSI_MAGIC);
	PUSHMARK(SP);
	EXTEND(SP,(I32)items);
	SensorCall(16990);while (items--)
	    PUSHs(*MARK++);
	PUTBACK;
	call_sv(MUTABLE_SV(GvCV(gv)), G_SCALAR);
    }
    SPAGAIN;

    SensorCall(16992);sv = TOPs;
    POPSTACK;
    SensorCall(16995);if (sv_isobject(sv)) {
	sv_unmagic(varsv, how);
	/* Croak if a self-tie on an aggregate is attempted. */
	SensorCall(16994);if (varsv == SvRV(sv) &&
	    (SvTYPE(varsv) == SVt_PVAV ||
	     SvTYPE(varsv) == SVt_PVHV))
	    {/*173*/SensorCall(16993);Perl_croak(aTHX_
		       "Self-ties of arrays and hashes are not supported");/*174*/}
	sv_magic(varsv, (SvRV(sv) == varsv ? NULL : sv), how, NULL, 0);
    }
    LEAVE_with_name("call_TIE");
    SP = PL_stack_base + markoff;
    PUSHs(sv);
    RETURN;
}

PP(pp_untie)
{
SensorCall(16996);    dVAR; dSP;
    MAGIC *mg;
    SV *sv = POPs;
    const char how = (SvTYPE(sv) == SVt_PVHV || SvTYPE(sv) == SVt_PVAV)
		? PERL_MAGIC_tied : PERL_MAGIC_tiedscalar;

    SensorCall(16997);if (isGV_with_GP(sv) && !SvFAKE(sv) && !(sv = MUTABLE_SV(GvIOp(sv))))
	RETPUSHYES;

    SensorCall(17004);if ((mg = SvTIED_mg(sv, how))) {
	SensorCall(16998);SV * const obj = SvRV(SvTIED_obj(sv, mg));
        SensorCall(17003);if (obj) {
	    SensorCall(16999);GV * const gv = gv_fetchmethod_autoload(SvSTASH(obj), "UNTIE", FALSE);
	    CV *cv;
	    SensorCall(17002);if (gv && isGV(gv) && (cv = GvCV(gv))) {
	       PUSHMARK(SP);
	       PUSHs(SvTIED_obj(MUTABLE_SV(gv), mg));
	       mXPUSHi(SvREFCNT(obj) - 1);
	       PUTBACK;
	       ENTER_with_name("call_UNTIE");
	       call_sv(MUTABLE_SV(cv), G_VOID);
	       LEAVE_with_name("call_UNTIE");
	       SPAGAIN;
            }
	    else {/*183*/SensorCall(17000);if (mg && SvREFCNT(obj) > 1) {
		SensorCall(17001);Perl_ck_warner(aTHX_ packWARN(WARN_UNTIE),
			       "untie attempted while %"UVuf" inner references still exist",
			       (UV)SvREFCNT(obj) - 1 ) ;
	    ;/*184*/}}
        }
    }
    sv_unmagic(sv, how) ;
    RETPUSHYES;
}

PP(pp_tied)
{
SensorCall(17005);    dVAR;
    dSP;
    const MAGIC *mg;
    SV *sv = POPs;
    const char how = (SvTYPE(sv) == SVt_PVHV || SvTYPE(sv) == SVt_PVAV)
		? PERL_MAGIC_tied : PERL_MAGIC_tiedscalar;

    SensorCall(17006);if (isGV_with_GP(sv) && !SvFAKE(sv) && !(sv = MUTABLE_SV(GvIOp(sv))))
	RETPUSHUNDEF;

    SensorCall(17007);if ((mg = SvTIED_mg(sv, how))) {
	PUSHs(SvTIED_obj(sv, mg));
	RETURN;
    }
    RETPUSHUNDEF;
}

PP(pp_dbmopen)
{
SensorCall(17008);    dVAR; dSP;
    dPOPPOPssrl;
    HV* stash;
    GV *gv = NULL;

    HV * const hv = MUTABLE_HV(POPs);
    SV * const sv = newSVpvs_flags("AnyDBM_File", SVs_TEMP);
    stash = gv_stashsv(sv, 0);
    SensorCall(17010);if (!stash || !(gv = gv_fetchmethod(stash, "TIEHASH"))) {
	PUTBACK;
	require_pv("AnyDBM_File.pm");
	SPAGAIN;
	SensorCall(17009);if (!stash || !(gv = gv_fetchmethod(stash, "TIEHASH")))
	    DIE(aTHX_ "No dbm on this machine");
    }

    ENTER;
    PUSHMARK(SP);

    EXTEND(SP, 5);
    PUSHs(sv);
    PUSHs(left);
    SensorCall(17012);if (SvIV(right))
	mPUSHu(O_RDWR|O_CREAT);
    else
    {
	mPUSHu(O_RDWR);
	SensorCall(17011);if (!SvOK(right)) right = &PL_sv_no;
    }
    PUSHs(right);
    PUTBACK;
    call_sv(MUTABLE_SV(GvCV(gv)), G_SCALAR);
    SPAGAIN;

    SensorCall(17013);if (!sv_isobject(TOPs)) {
	SP--;
	PUSHMARK(SP);
	PUSHs(sv);
	PUSHs(left);
	mPUSHu(O_RDONLY);
	PUSHs(right);
	PUTBACK;
	call_sv(MUTABLE_SV(GvCV(gv)), G_SCALAR);
	SPAGAIN;
    }

    SensorCall(17014);if (sv_isobject(TOPs)) {
	sv_unmagic(MUTABLE_SV(hv), PERL_MAGIC_tied);
	sv_magic(MUTABLE_SV(hv), TOPs, PERL_MAGIC_tied, NULL, 0);
    }
    LEAVE;
    RETURN;
}

PP(pp_sselect)
{
SensorCall(17015);
#ifdef HAS_SELECT
    dVAR; dSP; dTARGET;
    register I32 i;
    register I32 j;
    register char *s;
    register SV *sv;
    NV value;
    I32 maxlen = 0;
    I32 nfound;
    struct timeval timebuf;
    struct timeval *tbuf = &timebuf;
    I32 growsize;
    char *fd_sets[4];
#if BYTEORDER != 0x1234 && BYTEORDER != 0x12345678
	I32 masksize;
	I32 offset;
	I32 k;

#   if BYTEORDER & 0xf0000
#	define ORDERBYTE (0x88888888 - BYTEORDER)
#   else
#	define ORDERBYTE (0x4444 - BYTEORDER)
#   endif

#endif

    SP -= 4;
    SensorCall(17029);for (i = 1; i <= 3; i++) {
	SensorCall(17016);SV * const sv = SP[i];
	SvGETMAGIC(sv);
	SensorCall(17018);if (!SvOK(sv))
	    {/*105*/SensorCall(17017);continue;/*106*/}
	SensorCall(17022);if (SvREADONLY(sv)) {
	    SensorCall(17019);if (SvIsCOW(sv))
		sv_force_normal_flags(sv, 0);
	    SensorCall(17021);if (SvREADONLY(sv) && !(SvPOK(sv) && SvCUR(sv) == 0))
		{/*107*/SensorCall(17020);Perl_croak_no_modify(aTHX);/*108*/}
	}
	SensorCall(17025);if (!SvPOK(sv)) {
	    SensorCall(17023);if (!SvPOKp(sv))
		{/*109*/SensorCall(17024);Perl_ck_warner(aTHX_ packWARN(WARN_MISC),
				    "Non-string passed as bitmask");/*110*/}
	    SvPV_force_nomg_nolen(sv);	/* force string conversion */
	}
	SensorCall(17026);j = SvCUR(sv);
	SensorCall(17028);if (maxlen < j)
	    {/*111*/SensorCall(17027);maxlen = j;/*112*/}
    }

/* little endians can use vecs directly */
#if BYTEORDER != 0x1234 && BYTEORDER != 0x12345678
#  ifdef NFDBITS

#    ifndef NBBY
#     define NBBY 8
#    endif

    masksize = NFDBITS / NBBY;
#  else
    masksize = sizeof(long);	/* documented int, everyone seems to use long */
#  endif
    Zero(&fd_sets[0], 4, char*);
#endif

#  if SELECT_MIN_BITS == 1
    growsize = sizeof(fd_set);
#  else
#   if defined(__GLIBC__) && defined(__FD_SETSIZE)
#      undef SELECT_MIN_BITS
#      define SELECT_MIN_BITS __FD_SETSIZE
#   endif
    /* If SELECT_MIN_BITS is greater than one we most probably will want
     * to align the sizes with SELECT_MIN_BITS/8 because for example
     * in many little-endian (Intel, Alpha) systems (Linux, OS/2, Digital
     * UNIX, Solaris, NeXT, Darwin) the smallest quantum select() operates
     * on (sets/tests/clears bits) is 32 bits.  */
    SensorCall(17030);growsize = maxlen + (SELECT_MIN_BITS/8 - (maxlen % (SELECT_MIN_BITS/8)));
#  endif

    sv = SP[4];
    SensorCall(17035);if (SvOK(sv)) {
	SensorCall(17031);value = SvNV(sv);
	SensorCall(17033);if (value < 0.0)
	    {/*113*/SensorCall(17032);value = 0.0;/*114*/}
	SensorCall(17034);timebuf.tv_sec = (long)value;
	value -= (NV)timebuf.tv_sec;
	timebuf.tv_usec = (long)(value * 1000000.0);
    }
    else
	tbuf = NULL;

    SensorCall(17046);for (i = 1; i <= 3; i++) {
	SensorCall(17036);sv = SP[i];
	SensorCall(17039);if (!SvOK(sv) || SvCUR(sv) == 0) {
	    SensorCall(17037);fd_sets[i] = 0;
	    SensorCall(17038);continue;
	}
	assert(SvPOK(sv));
	SensorCall(17040);j = SvLEN(sv);
	SensorCall(17041);if (j < growsize) {
	    Sv_Grow(sv, growsize);
	}
	SensorCall(17042);j = SvCUR(sv);
	s = SvPVX(sv) + j;
	SensorCall(17044);while (++j <= growsize) {
	    SensorCall(17043);*s++ = '\0';
	}

#if BYTEORDER != 0x1234 && BYTEORDER != 0x12345678
	s = SvPVX(sv);
	Newx(fd_sets[i], growsize, char);
	for (offset = 0; offset < growsize; offset += masksize) {
	    for (j = 0, k=ORDERBYTE; j < masksize; j++, (k >>= 4))
		fd_sets[i][j+offset] = s[(k % masksize) + offset];
	}
#else
	SensorCall(17045);fd_sets[i] = SvPVX(sv);
#endif
    }

#ifdef PERL_IRIX5_SELECT_TIMEVAL_VOID_CAST
    /* Can't make just the (void*) conditional because that would be
     * cpp #if within cpp macro, and not all compilers like that. */
    nfound = PerlSock_select(
	maxlen * 8,
	(Select_fd_set_t) fd_sets[1],
	(Select_fd_set_t) fd_sets[2],
	(Select_fd_set_t) fd_sets[3],
	(void*) tbuf); /* Workaround for compiler bug. */
#else
    SensorCall(17047);nfound = PerlSock_select(
	maxlen * 8,
	(Select_fd_set_t) fd_sets[1],
	(Select_fd_set_t) fd_sets[2],
	(Select_fd_set_t) fd_sets[3],
	tbuf);
#endif
    SensorCall(17050);for (i = 1; i <= 3; i++) {
	SensorCall(17048);if (fd_sets[i]) {
	    SensorCall(17049);sv = SP[i];
#if BYTEORDER != 0x1234 && BYTEORDER != 0x12345678
	    s = SvPVX(sv);
	    for (offset = 0; offset < growsize; offset += masksize) {
		for (j = 0, k=ORDERBYTE; j < masksize; j++, (k >>= 4))
		    s[(k % masksize) + offset] = fd_sets[i][j+offset];
	    }
	    Safefree(fd_sets[i]);
#endif
	    SvSETMAGIC(sv);
	}
    }

    PUSHi(nfound);
    SensorCall(17052);if (GIMME == G_ARRAY && tbuf) {
	SensorCall(17051);value = (NV)(timebuf.tv_sec) +
		(NV)(timebuf.tv_usec) / 1000000.0;
	mPUSHn(value);
    }
    RETURN;
#else
    DIE(aTHX_ "select not implemented");
#endif
}

/*
=for apidoc setdefout

Sets PL_defoutgv, the default file handle for output, to the passed in
typeglob. As PL_defoutgv "owns" a reference on its typeglob, the reference
count of the passed in typeglob is increased by one, and the reference count
of the typeglob that PL_defoutgv points to is decreased by one.

=cut
*/

void
Perl_setdefout(pTHX_ GV *gv)
{
    dVAR;
    SvREFCNT_inc_simple_void(gv);
    SvREFCNT_dec(PL_defoutgv);
    PL_defoutgv = gv;
}

PP(pp_select)
{
SensorCall(17053);    dVAR; dSP; dTARGET;
    HV *hv;
    GV * const newdefout = (PL_op->op_private > 0) ? (MUTABLE_GV(POPs)) : NULL;
    GV * egv = GvEGVx(PL_defoutgv);
    GV * const *gvp;

    SensorCall(17054);if (!egv)
	egv = PL_defoutgv;
    SensorCall(17055);hv = isGV_with_GP(egv) ? GvSTASH(egv) : NULL;
    gvp = hv && HvENAME(hv)
		? (GV**)hv_fetch(hv, GvNAME(egv), HEK_UTF8(GvNAME_HEK(egv)) ? -GvNAMELEN(egv) : GvNAMELEN(egv), FALSE)
		: NULL;
    SensorCall(17056);if (gvp && *gvp == egv) {
	    gv_efullname4(TARG, PL_defoutgv, NULL, TRUE);
	    XPUSHTARG;
    }
    else {
	    mXPUSHs(newRV(MUTABLE_SV(egv)));
    }

    SensorCall(17058);if (newdefout) {
	SensorCall(17057);if (!GvIO(newdefout))
	    gv_IOadd(newdefout);
	setdefout(newdefout);
    }

    RETURN;
}

PP(pp_getc)
{
SensorCall(17059);    dVAR; dSP; dTARGET;
    GV * const gv =
	MAXARG==0 || (!TOPs && !POPs) ? PL_stdingv : MUTABLE_GV(POPs);
    IO *const io = GvIO(gv);

    SensorCall(17060);if (MAXARG == 0)
	EXTEND(SP, 1);

    SensorCall(17066);if (io) {
	SensorCall(17061);const MAGIC * const mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	SensorCall(17065);if (mg) {
	    SensorCall(17062);const U32 gimme = GIMME_V;
	    Perl_tied_method(aTHX_ "GETC", SP, MUTABLE_SV(io), mg, gimme, 0);
	    SensorCall(17063);if (gimme == G_SCALAR) {
		SPAGAIN;
		SvSetMagicSV_nosteal(TARG, TOPs);
	    }
	    {OP * ReplaceReturn1284 = NORMAL; SensorCall(17064); return ReplaceReturn1284;}
	}
    }
    SensorCall(17068);if (!gv || do_eof(gv)) { /* make sure we have fp with something */
	SensorCall(17067);if (!io || (!IoIFP(io) && IoTYPE(io) != IoTYPE_WRONLY))
	    report_evil_fh(gv);
	SETERRNO(EBADF,RMS_IFI);
	RETPUSHUNDEF;
    }
    TAINT;
    sv_setpvs(TARG, " ");
    SensorCall(17069);*SvPVX(TARG) = PerlIO_getc(IoIFP(GvIOp(gv))); /* should never be EOF */
    SensorCall(17072);if (PerlIO_isutf8(IoIFP(GvIOp(gv)))) {
	/* Find out how many bytes the char needs */
	Size_t len = UTF8SKIP(SvPVX_const(TARG));
	SensorCall(17071);if (len > 1) {
	    SvGROW(TARG,len+1);
	    SensorCall(17070);len = PerlIO_read(IoIFP(GvIOp(gv)),SvPVX(TARG)+1,len-1);
	    SvCUR_set(TARG,1+len);
	}
	SvUTF8_on(TARG);
    }
    PUSHTARG;
    RETURN;
}

STATIC OP *
S_doform(pTHX_ CV *cv, GV *gv, OP *retop)
{
SensorCall(17073);    dVAR;
    register PERL_CONTEXT *cx;
    const I32 gimme = GIMME_V;

    PERL_ARGS_ASSERT_DOFORM;

    SensorCall(17074);if (cv && CvCLONE(cv))
	cv = MUTABLE_CV(sv_2mortal(MUTABLE_SV(cv_clone(cv))));

    ENTER;
    SAVETMPS;

    PUSHBLOCK(cx, CXt_FORMAT, PL_stack_sp);
SensorCall(17075);    PUSHFORMAT(cx, retop);
    SAVECOMPPAD();
    PAD_SET_CUR_NOSAVE(CvPADLIST(cv), 1);

    setdefout(gv);	    /* locally select filehandle so $% et al work */
    {OP * ReplaceReturn1283 = CvSTART(cv); SensorCall(17076); return ReplaceReturn1283;}
}

PP(pp_enterwrite)
{
SensorCall(17077);    dVAR;
    dSP;
    register GV *gv;
    register IO *io;
    GV *fgv;
    CV *cv = NULL;
    SV *tmpsv = NULL;

    SensorCall(17081);if (MAXARG == 0) {
	SensorCall(17078);gv = PL_defoutgv;
	EXTEND(SP, 1);
    }
    else {
	SensorCall(17079);gv = MUTABLE_GV(POPs);
	SensorCall(17080);if (!gv)
	    gv = PL_defoutgv;
    }
    SensorCall(17082);io = GvIO(gv);
    SensorCall(17083);if (!io) {
	RETPUSHNO;
    }
    SensorCall(17085);if (IoFMT_GV(io))
	fgv = IoFMT_GV(io);
    else
	{/*31*/SensorCall(17084);fgv = gv;/*32*/}

    SensorCall(17087);if (!fgv)
	{/*33*/SensorCall(17086);goto not_a_format_reference;/*34*/}

    SensorCall(17088);cv = GvFORM(fgv);
    SensorCall(17091);if (!cv) {
	SensorCall(17089);tmpsv = sv_newmortal();
	gv_efullname4(tmpsv, fgv, NULL, FALSE);
	SensorCall(17090);if (SvPOK(tmpsv) && *SvPV_nolen_const(tmpsv))
	    DIE(aTHX_ "Undefined format \"%"SVf"\" called", SVfARG(tmpsv));

	not_a_format_reference:
	DIE(aTHX_ "Not a format reference");
    }
    IoFLAGS(io) &= ~IOf_DIDTOP;
    {OP * ReplaceReturn1282 = doform(cv,gv,PL_op->op_next); SensorCall(17092); return ReplaceReturn1282;}
}

PP(pp_leavewrite)
{
SensorCall(17093);    dVAR; dSP;
    GV * const gv = cxstack[cxstack_ix].blk_format.gv;
    register IO * const io = GvIOp(gv);
    PerlIO *ofp;
    PerlIO *fp;
    SV **newsp;
    I32 gimme;
    register PERL_CONTEXT *cx;
    OP *retop;

    SensorCall(17095);if (!io || !(ofp = IoOFP(io)))
        {/*71*/SensorCall(17094);goto forget_top;/*72*/}

    DEBUG_f(PerlIO_printf(Perl_debug_log, "left=%ld, todo=%ld\n",
	  (long)IoLINES_LEFT(io), (long)FmLINES(PL_formtarget)));

    SensorCall(17126);if (IoLINES_LEFT(io) < FmLINES(PL_formtarget) &&
	PL_formtarget != PL_toptarget)
    {
	SensorCall(17096);GV *fgv;
	CV *cv;
	SensorCall(17106);if (!IoTOP_GV(io)) {
	    SensorCall(17097);GV *topgv;

	    SensorCall(17102);if (!IoTOP_NAME(io)) {
		SensorCall(17098);SV *topname;
		SensorCall(17099);if (!IoFMT_NAME(io))
		    IoFMT_NAME(io) = savepv(GvNAME(gv));
		SensorCall(17100);topname = sv_2mortal(Perl_newSVpvf(aTHX_ "%"HEKf"_TOP",
                                        HEKfARG(GvNAME_HEK(gv))));
		topgv = gv_fetchsv(topname, 0, SVt_PVFM);
		SensorCall(17101);if ((topgv && GvFORM(topgv)) ||
		  !gv_fetchpvs("top", GV_NOTQUAL, SVt_PVFM))
		    IoTOP_NAME(io) = savesvpv(topname);
		else
		    IoTOP_NAME(io) = savepvs("top");
	    }
	    SensorCall(17103);topgv = gv_fetchpv(IoTOP_NAME(io), 0, SVt_PVFM);
	    SensorCall(17105);if (!topgv || !GvFORM(topgv)) {
		IoLINES_LEFT(io) = IoPAGE_LEN(io);
		SensorCall(17104);goto forget_top;
	    }
	    IoTOP_GV(io) = topgv;
	}
	SensorCall(17117);if (IoFLAGS(io) & IOf_DIDTOP) {	/* Oh dear.  It still doesn't fit. */
	    SensorCall(17107);I32 lines = IoLINES_LEFT(io);
	    const char *s = SvPVX_const(PL_formtarget);
	    SensorCall(17109);if (lines <= 0)		/* Yow, header didn't even fit!!! */
		{/*73*/SensorCall(17108);goto forget_top;/*74*/}
	    SensorCall(17114);while (lines-- > 0) {
		SensorCall(17110);s = strchr(s, '\n');
		SensorCall(17112);if (!s)
		    {/*75*/SensorCall(17111);break;/*76*/}
		SensorCall(17113);s++;
	    }
	    SensorCall(17116);if (s) {
		SensorCall(17115);const STRLEN save = SvCUR(PL_formtarget);
		SvCUR_set(PL_formtarget, s - SvPVX_const(PL_formtarget));
		do_print(PL_formtarget, ofp);
		SvCUR_set(PL_formtarget, save);
		sv_chop(PL_formtarget, s);
		FmLINES(PL_formtarget) -= IoLINES_LEFT(io);
	    }
	}
	SensorCall(17118);if (IoLINES_LEFT(io) >= 0 && IoPAGE(io) > 0)
	    do_print(PL_formfeed, ofp);
	IoLINES_LEFT(io) = IoPAGE_LEN(io);
	IoPAGE(io)++;
	PL_formtarget = PL_toptarget;
	IoFLAGS(io) |= IOf_DIDTOP;
	SensorCall(17119);fgv = IoTOP_GV(io);
	SensorCall(17120);if (!fgv)
	    DIE(aTHX_ "bad top format reference");
	SensorCall(17121);cv = GvFORM(fgv);
	SensorCall(17124);if (!cv) {
	    SensorCall(17122);SV * const sv = sv_newmortal();
	    gv_efullname4(sv, fgv, NULL, FALSE);
	    SensorCall(17123);if (SvPOK(sv) && *SvPV_nolen_const(sv))
		DIE(aTHX_ "Undefined top format \"%"SVf"\" called", SVfARG(sv));
	    else
		DIE(aTHX_ "Undefined top format called");
	}
	{OP * ReplaceReturn1281 = doform(cv, gv, PL_op); SensorCall(17125); return ReplaceReturn1281;}
    }

  forget_top:
SensorCall(17127);    POPBLOCK(cx,PL_curpm);
    POPFORMAT(cx);
    retop = cx->blk_sub.retop;
    LEAVE;

    fp = IoOFP(io);
    SensorCall(17134);if (!fp) {
	SensorCall(17128);if (IoIFP(io))
	    report_wrongway_fh(gv, '<');
	else
	    report_evil_fh(gv);
	PUSHs(&PL_sv_no);
    }
    else {
	SensorCall(17129);if ((IoLINES_LEFT(io) -= FmLINES(PL_formtarget)) < 0) {
	    SensorCall(17130);Perl_ck_warner(aTHX_ packWARN(WARN_IO), "page overflow");
	}
	SensorCall(17133);if (!do_print(PL_formtarget, fp))
	    PUSHs(&PL_sv_no);
	else {
	    FmLINES(PL_formtarget) = 0;
	    SvCUR_set(PL_formtarget, 0);
	    SensorCall(17131);*SvEND(PL_formtarget) = '\0';
	    SensorCall(17132);if (IoFLAGS(io) & IOf_FLUSH)
		(void)PerlIO_flush(fp);
	    PUSHs(&PL_sv_yes);
	}
    }
    /* bad_ofp: */
    PL_formtarget = PL_bodytarget;
    PUTBACK;
    PERL_UNUSED_VAR(newsp);
    PERL_UNUSED_VAR(gimme);
    {OP * ReplaceReturn1280 = retop; SensorCall(17135); return ReplaceReturn1280;}
}

PP(pp_prtf)
{
SensorCall(17136);    dVAR; dSP; dMARK; dORIGMARK;
    PerlIO *fp;
    SV *sv;

    GV * const gv
	= (PL_op->op_flags & OPf_STACKED) ? MUTABLE_GV(*++MARK) : PL_defoutgv;
    IO *const io = GvIO(gv);

    SensorCall(17142);if (io) {
	SensorCall(17137);const MAGIC * const mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	SensorCall(17141);if (mg) {
	    SensorCall(17138);if (MARK == ORIGMARK) {
		MEXTEND(SP, 1);
		SensorCall(17139);++MARK;
		Move(MARK, MARK + 1, (SP - MARK) + 1, SV*);
		++SP;
	    }
	    {OP * ReplaceReturn1279 = Perl_tied_method(aTHX_ "PRINTF", mark - 1, MUTABLE_SV(io),
				    mg,
				    G_SCALAR | TIED_METHOD_ARGUMENTS_ON_STACK,
				    sp - mark); SensorCall(17140); return ReplaceReturn1279;}
	}
    }

    SensorCall(17143);sv = newSV(0);
    SensorCall(17153);if (!io) {
	report_evil_fh(gv);
	SETERRNO(EBADF,RMS_IFI);
	SensorCall(17144);goto just_say_no;
    }
    else {/*91*/SensorCall(17145);if (!(fp = IoOFP(io))) {
	SensorCall(17146);if (IoIFP(io))
	    report_wrongway_fh(gv, '<');
	else if (ckWARN(WARN_CLOSED))
	    report_evil_fh(gv);
	SETERRNO(EBADF,IoIFP(io)?RMS_FAC:RMS_IFI);
	SensorCall(17147);goto just_say_no;
    }
    else {
	do_sprintf(sv, SP - MARK, MARK + 1);
	SensorCall(17149);if (!do_print(sv, fp))
	    {/*93*/SensorCall(17148);goto just_say_no;/*94*/}

	SensorCall(17152);if (IoFLAGS(io) & IOf_FLUSH)
	    {/*95*/SensorCall(17150);if (PerlIO_flush(fp) == EOF)
		{/*97*/SensorCall(17151);goto just_say_no;/*98*/}/*96*/}
    ;/*92*/}}
    SvREFCNT_dec(sv);
    SP = ORIGMARK;
    PUSHs(&PL_sv_yes);
    RETURN;

  just_say_no:
    SvREFCNT_dec(sv);
    SP = ORIGMARK;
    PUSHs(&PL_sv_undef);
    RETURN;
}

PP(pp_sysopen)
{
SensorCall(17154);    dVAR;
    dSP;
    const int perm = (MAXARG > 3 && (TOPs || POPs)) ? POPi : 0666;
    const int mode = POPi;
    SV * const sv = POPs;
    GV * const gv = MUTABLE_GV(POPs);
    STRLEN len;

    /* Need TIEHANDLE method ? */
    const char * const tmps = SvPV_const(sv, len);
    /* FIXME? do_open should do const  */
    SensorCall(17155);if (do_open(gv, tmps, len, TRUE, mode, perm, NULL)) {
	IoLINES(GvIOp(gv)) = 0;
	PUSHs(&PL_sv_yes);
    }
    else {
	PUSHs(&PL_sv_undef);
    }
    RETURN;
}

PP(pp_sysread)
{
SensorCall(17156);    dVAR; dSP; dMARK; dORIGMARK; dTARGET;
    SSize_t offset;
    IO *io;
    char *buffer;
    STRLEN orig_size;
    SSize_t length;
    SSize_t count;
    SV *bufsv;
    STRLEN blen;
    int fp_utf8;
    int buffer_utf8;
    SV *read_target;
    Size_t got = 0;
    Size_t wanted;
    bool charstart = FALSE;
    STRLEN charskip = 0;
    STRLEN skip = 0;

    GV * const gv = MUTABLE_GV(*++MARK);
    SensorCall(17160);if ((PL_op->op_type == OP_READ || PL_op->op_type == OP_SYSREAD)
	&& gv && (io = GvIO(gv)) )
    {
	SensorCall(17157);const MAGIC *const mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	SensorCall(17159);if (mg) {
	    {OP * ReplaceReturn1278 = Perl_tied_method(aTHX_ "READ", mark - 1, MUTABLE_SV(io), mg,
				    G_SCALAR | TIED_METHOD_ARGUMENTS_ON_STACK,
				    sp - mark); SensorCall(17158); return ReplaceReturn1278;}
	}
    }

    SensorCall(17162);if (!gv)
	{/*135*/SensorCall(17161);goto say_undef;/*136*/}
    SensorCall(17163);bufsv = *++MARK;
    SensorCall(17164);if (! SvOK(bufsv))
	sv_setpvs(bufsv, "");
    SensorCall(17165);length = SvIVx(*++MARK);
    SETERRNO(0,0);
    SensorCall(17167);if (MARK < SP)
	offset = SvIVx(*++MARK);
    else
	{/*137*/SensorCall(17166);offset = 0;/*138*/}
    SensorCall(17168);io = GvIO(gv);
    SensorCall(17170);if (!io || !IoIFP(io)) {
	report_evil_fh(gv);
	SETERRNO(EBADF,RMS_IFI);
	SensorCall(17169);goto say_undef;
    }
    SensorCall(17173);if ((fp_utf8 = PerlIO_isutf8(IoIFP(io))) && !IN_BYTES) {
	SensorCall(17171);buffer = SvPVutf8_force(bufsv, blen);
	/* UTF-8 may not have been set if they are all low bytes */
	SvUTF8_on(bufsv);
	buffer_utf8 = 0;
    }
    else {
	SensorCall(17172);buffer = SvPV_force(bufsv, blen);
	buffer_utf8 = !IN_BYTES && SvUTF8(bufsv);
    }
    SensorCall(17174);if (length < 0)
	DIE(aTHX_ "Negative length");
    SensorCall(17175);wanted = length;

    charstart = TRUE;
    charskip  = 0;
    skip = 0;

#ifdef HAS_SOCKET
    SensorCall(17183);if (PL_op->op_type == OP_RECV) {
	Sock_size_t bufsize;
	SensorCall(17176);char namebuf[MAXPATHLEN];
#if (defined(VMS_DO_SOCKETS) && defined(DECCRTL_SOCKETS)) || defined(MPE) || defined(__QNXNTO__)
	bufsize = sizeof (struct sockaddr_in);
#else
	bufsize = sizeof namebuf;
#endif
#ifdef OS2	/* At least Warp3+IAK: only the first byte of bufsize set */
	if (bufsize >= 256)
	    bufsize = 255;
#endif
	buffer = SvGROW(bufsv, (STRLEN)(length+1));
	/* 'offset' means 'flags' here */
	count = PerlSock_recvfrom(PerlIO_fileno(IoIFP(io)), buffer, length, offset,
				  (struct sockaddr *)namebuf, &bufsize);
	SensorCall(17177);if (count < 0)
	    RETPUSHUNDEF;
	/* MSG_TRUNC can give oversized count; quietly lose it */
	SensorCall(17179);if (count > length)
	    {/*139*/SensorCall(17178);count = length;/*140*/}
#ifdef EPOC
        /* Bogus return without padding */
	bufsize = sizeof (struct sockaddr_in);
#endif
	SvCUR_set(bufsv, count);
	SensorCall(17180);*SvEND(bufsv) = '\0';
	(void)SvPOK_only(bufsv);
	SensorCall(17181);if (fp_utf8)
	    SvUTF8_on(bufsv);
	SvSETMAGIC(bufsv);
	/* This should not be marked tainted if the fp is marked clean */
	SensorCall(17182);if (!(IoFLAGS(io) & IOf_UNTAINT))
	    SvTAINTED_on(bufsv);
	SP = ORIGMARK;
	sv_setpvn(TARG, namebuf, bufsize);
	PUSHs(TARG);
	RETURN;
    }
#endif
    SensorCall(17185);if (DO_UTF8(bufsv)) {
	/* offset adjust in characters not bytes */
	SensorCall(17184);blen = sv_len_utf8(bufsv);
    }
    SensorCall(17188);if (offset < 0) {
	SensorCall(17186);if (-offset > (SSize_t)blen)
	    DIE(aTHX_ "Offset outside string");
	SensorCall(17187);offset += blen;
    }
    SensorCall(17192);if (DO_UTF8(bufsv)) {
	/* convert offset-as-chars to offset-as-bytes */
	SensorCall(17189);if (offset >= (int)blen)
	    {/*141*/SensorCall(17190);offset += SvCUR(bufsv) - blen;/*142*/}
	else
	    {/*143*/SensorCall(17191);offset = utf8_hop((U8 *)buffer,offset) - (U8 *) buffer;/*144*/}
    }
 more_bytes:
    SensorCall(17193);orig_size = SvCUR(bufsv);
    /* Allocating length + offset + 1 isn't perfect in the case of reading
       bytes from a byte file handle into a UTF8 buffer, but it won't harm us
       unduly.
       (should be 2 * length + offset + 1, or possibly something longer if
       PL_encoding is true) */
    buffer  = SvGROW(bufsv, (STRLEN)(length+offset+1));
    SensorCall(17194);if (offset > 0 && offset > (SSize_t)orig_size) { /* Zero any newly allocated space */
    	Zero(buffer+orig_size, offset-orig_size, char);
    }
    SensorCall(17195);buffer = buffer + offset;
    SensorCall(17198);if (!buffer_utf8) {
	SensorCall(17196);read_target = bufsv;
    } else {
	/* Best to read the bytes into a new SV, upgrade that to UTF8, then
	   concatenate it to the current buffer.  */

	/* Truncate the existing buffer to the start of where we will be
	   reading to:  */
	SvCUR_set(bufsv, offset);

	SensorCall(17197);read_target = sv_newmortal();
	SvUPGRADE(read_target, SVt_PV);
	buffer = SvGROW(read_target, (STRLEN)(length + 1));
    }

    SensorCall(17203);if (PL_op->op_type == OP_SYSREAD) {
#ifdef PERL_SOCK_SYSREAD_IS_RECV
	if (IoTYPE(io) == IoTYPE_SOCKET) {
	    count = PerlSock_recv(PerlIO_fileno(IoIFP(io)),
				   buffer, length, 0);
	}
	else
#endif
	{
	    SensorCall(17199);count = PerlLIO_read(PerlIO_fileno(IoIFP(io)),
				  buffer, length);
	}
    }
    else
#ifdef HAS_SOCKET__bad_code_maybe
    if (IoTYPE(io) == IoTYPE_SOCKET) {
	Sock_size_t bufsize;
	char namebuf[MAXPATHLEN];
#if defined(VMS_DO_SOCKETS) && defined(DECCRTL_SOCKETS)
	bufsize = sizeof (struct sockaddr_in);
#else
	bufsize = sizeof namebuf;
#endif
	count = PerlSock_recvfrom(PerlIO_fileno(IoIFP(io)), buffer, length, 0,
			  (struct sockaddr *)namebuf, &bufsize);
    }
    else
#endif
    {
	SensorCall(17200);count = PerlIO_read(IoIFP(io), buffer, length);
	/* PerlIO_read() - like fread() returns 0 on both error and EOF */
	SensorCall(17202);if (count == 0 && PerlIO_error(IoIFP(io)))
	    {/*145*/SensorCall(17201);count = -1;/*146*/}
    }
    SensorCall(17206);if (count < 0) {
	SensorCall(17204);if (IoTYPE(io) == IoTYPE_WRONLY)
	    report_wrongway_fh(gv, '>');
	SensorCall(17205);goto say_undef;
    }
    SvCUR_set(read_target, count+(buffer - SvPVX_const(read_target)));
    SensorCall(17207);*SvEND(read_target) = '\0';
    (void)SvPOK_only(read_target);
    SensorCall(17221);if (fp_utf8 && !IN_BYTES) {
	/* Look at utf8 we got back and count the characters */
	SensorCall(17208);const char *bend = buffer + count;
	SensorCall(17215);while (buffer < bend) {
	    SensorCall(17209);if (charstart) {
	        SensorCall(17210);skip = UTF8SKIP(buffer);
		charskip = 0;
	    }
	    SensorCall(17214);if (buffer - charskip + skip > bend) {
		/* partial character - try for rest of it */
		SensorCall(17211);length = skip - (bend-buffer);
		offset = bend - SvPVX_const(bufsv);
		charstart = FALSE;
		charskip += count;
		SensorCall(17212);goto more_bytes;
	    }
	    else {
		SensorCall(17213);got++;
		buffer += skip;
		charstart = TRUE;
		charskip  = 0;
	    }
        }
	/* If we have not 'got' the number of _characters_ we 'wanted' get some more
	   provided amount read (count) was what was requested (length)
	 */
	SensorCall(17218);if (got < wanted && count == length) {
	    SensorCall(17216);length = wanted - got;
	    offset = bend - SvPVX_const(bufsv);
	    SensorCall(17217);goto more_bytes;
	}
	/* return value is character count */
	SensorCall(17219);count = got;
	SvUTF8_on(bufsv);
    }
    else {/*147*/SensorCall(17220);if (buffer_utf8) {
	/* Let svcatsv upgrade the bytes we read in to utf8.
	   The buffer is a mortal so will be freed soon.  */
	sv_catsv_nomg(bufsv, read_target);
    ;/*148*/}}
    SvSETMAGIC(bufsv);
    /* This should not be marked tainted if the fp is marked clean */
    SensorCall(17222);if (!(IoFLAGS(io) & IOf_UNTAINT))
	SvTAINTED_on(bufsv);
    SP = ORIGMARK;
    PUSHi(count);
    RETURN;

  say_undef:
    SP = ORIGMARK;
    RETPUSHUNDEF;
}

PP(pp_syswrite)
{
SensorCall(17223);    dVAR; dSP; dMARK; dORIGMARK; dTARGET;
    SV *bufsv;
    const char *buffer;
    SSize_t retval;
    STRLEN blen;
    STRLEN orig_blen_bytes;
    const int op_type = PL_op->op_type;
    bool doing_utf8;
    U8 *tmpbuf = NULL;
    GV *const gv = MUTABLE_GV(*++MARK);
    IO *const io = GvIO(gv);

    SensorCall(17229);if (op_type == OP_SYSWRITE && io) {
	SensorCall(17224);const MAGIC * const mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	SensorCall(17228);if (mg) {
	    SensorCall(17225);if (MARK == SP - 1) {
		SensorCall(17226);SV *sv = *SP;
		mXPUSHi(sv_len(sv));
		PUTBACK;
	    }

	    {OP * ReplaceReturn1277 = Perl_tied_method(aTHX_ "WRITE", mark - 1, MUTABLE_SV(io), mg,
				    G_SCALAR | TIED_METHOD_ARGUMENTS_ON_STACK,
				    sp - mark); SensorCall(17227); return ReplaceReturn1277;}
	}
    }
    SensorCall(17231);if (!gv)
	{/*157*/SensorCall(17230);goto say_undef;/*158*/}

    SensorCall(17232);bufsv = *++MARK;

    SETERRNO(0,0);
    SensorCall(17236);if (!io || !IoIFP(io) || IoTYPE(io) == IoTYPE_RDONLY) {
	SensorCall(17233);retval = -1;
	SensorCall(17234);if (io && IoIFP(io))
	    report_wrongway_fh(gv, '<');
	else
	    report_evil_fh(gv);
	SETERRNO(EBADF,RMS_IFI);
	SensorCall(17235);goto say_undef;
    }

    /* Do this first to trigger any overloading.  */
    SensorCall(17237);buffer = SvPV_const(bufsv, blen);
    orig_blen_bytes = blen;
    doing_utf8 = DO_UTF8(bufsv);

    SensorCall(17245);if (PerlIO_isutf8(IoIFP(io))) {
	SensorCall(17238);if (!SvUTF8(bufsv)) {
	    /* We don't modify the original scalar.  */
	    SensorCall(17239);tmpbuf = bytes_to_utf8((const U8*) buffer, &blen);
	    buffer = (char *) tmpbuf;
	    doing_utf8 = TRUE;
	}
    }
    else {/*159*/SensorCall(17240);if (doing_utf8) {
	SensorCall(17241);STRLEN tmplen = blen;
	U8 * const result = bytes_from_utf8((const U8*) buffer, &tmplen, &doing_utf8);
	SensorCall(17244);if (!doing_utf8) {
	    SensorCall(17242);tmpbuf = result;
	    buffer = (char *) tmpbuf;
	    blen = tmplen;
	}
	else {
	    assert((char *)result == buffer);
	    SensorCall(17243);Perl_croak(aTHX_ "Wide character in %s", OP_DESC(PL_op));
	}
    ;/*160*/}}

#ifdef HAS_SOCKET
    SensorCall(17277);if (op_type == OP_SEND) {
	SensorCall(17246);const int flags = SvIVx(*++MARK);
	SensorCall(17249);if (SP > MARK) {
	    SensorCall(17247);STRLEN mlen;
	    char * const sockbuf = SvPVx(*++MARK, mlen);
	    retval = PerlSock_sendto(PerlIO_fileno(IoIFP(io)), buffer, blen,
				     flags, (struct sockaddr *)sockbuf, mlen);
	}
	else {
	    SensorCall(17248);retval
		= PerlSock_send(PerlIO_fileno(IoIFP(io)), buffer, blen, flags);
	}
    }
    else
#endif
    {
	Size_t length = 0; /* This length is in characters.  */
	SensorCall(17250);STRLEN blen_chars;
	IV offset;

	SensorCall(17257);if (doing_utf8) {
	    SensorCall(17251);if (tmpbuf) {
		/* The SV is bytes, and we've had to upgrade it.  */
		SensorCall(17252);blen_chars = orig_blen_bytes;
	    } else {
		/* The SV really is UTF-8.  */
		SensorCall(17253);if (SvGMAGICAL(bufsv) || SvAMAGIC(bufsv)) {
		    /* Don't call sv_len_utf8 again because it will call magic
		       or overloading a second time, and we might get back a
		       different result.  */
		    SensorCall(17254);blen_chars = utf8_length((U8*)buffer, (U8*)buffer + blen);
		} else {
		    /* It's safe, and it may well be cached.  */
		    SensorCall(17255);blen_chars = sv_len_utf8(bufsv);
		}
	    }
	} else {
	    SensorCall(17256);blen_chars = blen;
	}

	SensorCall(17261);if (MARK >= SP) {
	    SensorCall(17258);length = blen_chars;
	} else {
#if Size_t_size > IVSIZE
	    length = (Size_t)SvNVx(*++MARK);
#else
	    SensorCall(17259);length = (Size_t)SvIVx(*++MARK);
#endif
	    SensorCall(17260);if ((SSize_t)length < 0) {
		Safefree(tmpbuf);
		DIE(aTHX_ "Negative length");
	    }
	}

	SensorCall(17268);if (MARK < SP) {
	    SensorCall(17262);offset = SvIVx(*++MARK);
	    SensorCall(17266);if (offset < 0) {
		SensorCall(17263);if (-offset > (IV)blen_chars) {
		    Safefree(tmpbuf);
		    DIE(aTHX_ "Offset outside string");
		}
		SensorCall(17264);offset += blen_chars;
	    } else {/*161*/SensorCall(17265);if (offset > (IV)blen_chars) {
		Safefree(tmpbuf);
		DIE(aTHX_ "Offset outside string");
	    ;/*162*/}}
	} else
	    {/*163*/SensorCall(17267);offset = 0;/*164*/}
	SensorCall(17270);if (length > blen_chars - offset)
	    {/*165*/SensorCall(17269);length = blen_chars - offset;/*166*/}
	SensorCall(17275);if (doing_utf8) {
	    /* Here we convert length from characters to bytes.  */
	    SensorCall(17271);if (tmpbuf || SvGMAGICAL(bufsv) || SvAMAGIC(bufsv)) {
		/* Either we had to convert the SV, or the SV is magical, or
		   the SV has overloading, in which case we can't or mustn't
		   or mustn't call it again.  */

		SensorCall(17272);buffer = (const char*)utf8_hop((const U8 *)buffer, offset);
		length = utf8_hop((U8 *)buffer, length) - (U8 *)buffer;
	    } else {
		/* It's a real UTF-8 SV, and it's not going to change under
		   us.  Take advantage of any cache.  */
		SensorCall(17273);I32 start = offset;
		I32 len_I32 = length;

		/* Convert the start and end character positions to bytes.
		   Remember that the second argument to sv_pos_u2b is relative
		   to the first.  */
		sv_pos_u2b(bufsv, &start, &len_I32);

		buffer += start;
		length = len_I32;
	    }
	}
	else {
	    SensorCall(17274);buffer = buffer+offset;
	}
#ifdef PERL_SOCK_SYSWRITE_IS_SEND
	if (IoTYPE(io) == IoTYPE_SOCKET) {
	    retval = PerlSock_send(PerlIO_fileno(IoIFP(io)),
				   buffer, length, 0);
	}
	else
#endif
	{
	    /* See the note at doio.c:do_print about filesize limits. --jhi */
	    SensorCall(17276);retval = PerlLIO_write(PerlIO_fileno(IoIFP(io)),
				   buffer, length);
	}
    }

    SensorCall(17279);if (retval < 0)
	{/*167*/SensorCall(17278);goto say_undef;/*168*/}
    SP = ORIGMARK;
    SensorCall(17280);if (doing_utf8)
        retval = utf8_length((U8*)buffer, (U8*)buffer + retval);

    Safefree(tmpbuf);
#if Size_t_size > IVSIZE
    PUSHn(retval);
#else
    PUSHi(retval);
#endif
    RETURN;

  say_undef:
    Safefree(tmpbuf);
    SP = ORIGMARK;
    RETPUSHUNDEF;
}

PP(pp_eof)
{
SensorCall(17281);    dVAR; dSP;
    GV *gv;
    IO *io;
    const MAGIC *mg;
    /*
     * in Perl 5.12 and later, the additional parameter is a bitmask:
     * 0 = eof
     * 1 = eof(FH)
     * 2 = eof()  <- ARGV magic
     *
     * I'll rely on the compiler's trace flow analysis to decide whether to
     * actually assign this out here, or punt it into the only block where it is
     * used. Doing it out here is DRY on the condition logic.
     */
    unsigned int which;

    SensorCall(17286);if (MAXARG) {
	SensorCall(17282);gv = PL_last_in_gv = MUTABLE_GV(POPs);	/* eof(FH) */
	which = 1;
    }
    else {
	EXTEND(SP, 1);

	SensorCall(17285);if (PL_op->op_flags & OPf_SPECIAL) {
	    SensorCall(17283);gv = PL_last_in_gv = GvEGVx(PL_argvgv);	/* eof() - ARGV magic */
	    which = 2;
	}
	else {
	    SensorCall(17284);gv = PL_last_in_gv;			/* eof */
	    which = 0;
	}
    }

    SensorCall(17287);if (!gv)
	RETPUSHNO;

    SensorCall(17289);if ((io = GvIO(gv)) && (mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar))) {
	{OP * ReplaceReturn1276 = tied_method1("EOF", SP, MUTABLE_SV(io), mg, newSVuv(which)); SensorCall(17288); return ReplaceReturn1276;}
    }

    SensorCall(17293);if (!MAXARG && (PL_op->op_flags & OPf_SPECIAL)) {	/* eof() */
	SensorCall(17290);if (io && !IoIFP(io)) {
	    SensorCall(17291);if ((IoFLAGS(io) & IOf_START) && av_len(GvAVn(gv)) < 0) {
		IoLINES(io) = 0;
		IoFLAGS(io) &= ~IOf_START;
		do_open(gv, "-", 1, FALSE, O_RDONLY, 0, NULL);
		SensorCall(17292);if (GvSV(gv))
		    sv_setpvs(GvSV(gv), "-");
		else
		    GvSV(gv) = newSVpvs("-");
		SvSETMAGIC(GvSV(gv));
	    }
	    else if (!nextargv(gv))
		RETPUSHYES;
	}
    }

    PUSHs(boolSV(do_eof(gv)));
    RETURN;
}

PP(pp_tell)
{
SensorCall(17294);    dVAR; dSP; dTARGET;
    GV *gv;
    IO *io;

    SensorCall(17295);if (MAXARG != 0 && (TOPs || POPs))
	PL_last_in_gv = MUTABLE_GV(POPs);
    else
	EXTEND(SP, 1);
    SensorCall(17296);gv = PL_last_in_gv;

    io = GvIO(gv);
    SensorCall(17302);if (io) {
	SensorCall(17297);const MAGIC * const mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	SensorCall(17299);if (mg) {
	    {OP * ReplaceReturn1275 = tied_method0("TELL", SP, MUTABLE_SV(io), mg); SensorCall(17298); return ReplaceReturn1275;}
	}
    }
    else {/*169*/SensorCall(17300);if (!gv) {
	SensorCall(17301);if (!errno)
	    SETERRNO(EBADF,RMS_IFI);
	PUSHi(-1);
	RETURN;
    ;/*170*/}}

#if LSEEKSIZE > IVSIZE
    PUSHn( do_tell(gv) );
#else
    PUSHi( do_tell(gv) );
#endif
    RETURN;
}

PP(pp_sysseek)
{
SensorCall(17303);    dVAR; dSP;
    const int whence = POPi;
#if LSEEKSIZE > IVSIZE
    const Off_t offset = (Off_t)SvNVx(POPs);
#else
    const Off_t offset = (Off_t)SvIVx(POPs);
#endif

    GV * const gv = PL_last_in_gv = MUTABLE_GV(POPs);
    IO *const io = GvIO(gv);

    SensorCall(17308);if (io) {
	SensorCall(17304);const MAGIC * const mg = SvTIED_mg((const SV *)io, PERL_MAGIC_tiedscalar);
	SensorCall(17307);if (mg) {
#if LSEEKSIZE > IVSIZE
	    SV *const offset_sv = newSVnv((NV) offset);
#else
	    SensorCall(17305);SV *const offset_sv = newSViv(offset);
#endif

	    {OP * ReplaceReturn1274 = tied_method2("SEEK", SP, MUTABLE_SV(io), mg, offset_sv,
				newSViv(whence)); SensorCall(17306); return ReplaceReturn1274;}
	}
    }

    SensorCall(17312);if (PL_op->op_type == OP_SEEK)
	PUSHs(boolSV(do_seek(gv, offset, whence)));
    else {
	SensorCall(17309);const Off_t sought = do_sysseek(gv, offset, whence);
        SensorCall(17311);if (sought < 0)
            PUSHs(&PL_sv_undef);
        else {
            SensorCall(17310);SV* const sv = sought ?
#if LSEEKSIZE > IVSIZE
                newSVnv((NV)sought)
#else
                newSViv(sought)
#endif
                : newSVpvn(zero_but_true, ZBTLEN);
            mPUSHs(sv);
        }
    }
    RETURN;
}

PP(pp_truncate)
{
SensorCall(17313);    dVAR;
    dSP;
    /* There seems to be no consensus on the length type of truncate()
     * and ftruncate(), both off_t and size_t have supporters. In
     * general one would think that when using large files, off_t is
     * at least as wide as size_t, so using an off_t should be okay. */
    /* XXX Configure probe for the length type of *truncate() needed XXX */
    Off_t len;

#if Off_t_size > IVSIZE
    len = (Off_t)POPn;
#else
    len = (Off_t)POPi;
#endif
    /* Checking for length < 0 is problematic as the type might or
     * might not be signed: if it is not, clever compilers will moan. */
    /* XXX Configure probe for the signedness of the length type of *truncate() needed? XXX */
    SETERRNO(0,0);
    {
	SV * const sv = POPs;
	int result = 1;
	GV *tmpgv;
	IO *io;

	SensorCall(17327);if ((tmpgv = PL_op->op_flags & OPf_SPECIAL
	               ? gv_fetchsv(sv, 0, SVt_PVIO)
	               : MAYBE_DEREF_GV(sv) )) {
	    SensorCall(17314);io = GvIO(tmpgv);
	    SensorCall(17320);if (!io)
		{/*175*/SensorCall(17315);result = 0;/*176*/}
	    else {
		PerlIO *fp;
	    do_ftruncate_io:
		TAINT_PROPER("truncate");
		SensorCall(17319);if (!(fp = IoIFP(io))) {
		    SensorCall(17316);result = 0;
		}
		else {
		    PerlIO_flush(fp);
#ifdef HAS_TRUNCATE
		    SensorCall(17318);if (ftruncate(PerlIO_fileno(fp), len) < 0)
#else
		    if (my_chsize(PerlIO_fileno(fp), len) < 0)
#endif
			{/*177*/SensorCall(17317);result = 0;/*178*/}
		}
	    }
	}
	else {/*179*/SensorCall(17321);if (SvROK(sv) && SvTYPE(SvRV(sv)) == SVt_PVIO) {
		SensorCall(17322);io = MUTABLE_IO(SvRV(sv)); /* *main::FRED{IO} for example */
		SensorCall(17323);goto do_ftruncate_io;
	}
	else {
	    SensorCall(17324);const char * const name = SvPV_nomg_const_nolen(sv);
	    TAINT_PROPER("truncate");
#ifdef HAS_TRUNCATE
	    SensorCall(17326);if (truncate(name, len) < 0)
	        {/*181*/SensorCall(17325);result = 0;/*182*/}
#else
	    {
		const int tmpfd = PerlLIO_open(name, O_RDWR);

		if (tmpfd < 0)
		    result = 0;
		else {
		    if (my_chsize(tmpfd, len) < 0)
		        result = 0;
		    PerlLIO_close(tmpfd);
		}
	    }
#endif
	;/*180*/}}

	SensorCall(17328);if (result)
	    RETPUSHYES;
	SensorCall(17329);if (!errno)
	    SETERRNO(EBADF,RMS_IFI);
	RETPUSHUNDEF;
    }
}

PP(pp_ioctl)
{
SensorCall(17330);    dVAR; dSP; dTARGET;
    SV * const argsv = POPs;
    const unsigned int func = POPu;
    const int optype = PL_op->op_type;
    GV * const gv = MUTABLE_GV(POPs);
    IO * const io = gv ? GvIOn(gv) : NULL;
    char *s;
    IV retval;

    SensorCall(17331);if (!io || !argsv || !IoIFP(io)) {
	report_evil_fh(gv);
	SETERRNO(EBADF,RMS_IFI);	/* well, sort of... */
	RETPUSHUNDEF;
    }

    SensorCall(17337);if (SvPOK(argsv) || !SvNIOK(argsv)) {
	SensorCall(17332);STRLEN len;
	STRLEN need;
	s = SvPV_force(argsv, len);
	need = IOCPARM_LEN(func);
	SensorCall(17334);if (len < need) {
	    SensorCall(17333);s = Sv_Grow(argsv, need + 1);
	    SvCUR_set(argsv, need);
	}

	SensorCall(17335);s[SvCUR(argsv)] = 17;	/* a little sanity check here */
    }
    else {
	SensorCall(17336);retval = SvIV(argsv);
	s = INT2PTR(char*,retval);		/* ouch */
    }

    TAINT_PROPER(PL_op_desc[optype]);

    SensorCall(17339);if (optype == OP_IOCTL)
#ifdef HAS_IOCTL
	retval = PerlLIO_ioctl(PerlIO_fileno(IoIFP(io)), func, s);
#else
	DIE(aTHX_ "ioctl is not implemented");
#endif
    else
#ifndef HAS_FCNTL
      DIE(aTHX_ "fcntl is not implemented");
#else
#if defined(OS2) && defined(__EMX__)
	retval = fcntl(PerlIO_fileno(IoIFP(io)), func, (int)s);
#else
	{/*69*/SensorCall(17338);retval = fcntl(PerlIO_fileno(IoIFP(io)), func, s);/*70*/}
#endif
#endif

#if defined(HAS_IOCTL) || defined(HAS_FCNTL)
    SensorCall(17342);if (SvPOK(argsv)) {
	SensorCall(17340);if (s[SvCUR(argsv)] != 17)
	    DIE(aTHX_ "Possible memory corruption: %s overflowed 3rd argument",
		OP_NAME(PL_op));
	SensorCall(17341);s[SvCUR(argsv)] = 0;		/* put our null back */
	SvSETMAGIC(argsv);		/* Assume it has changed */
    }

    SensorCall(17343);if (retval == -1)
	RETPUSHUNDEF;
    SensorCall(17344);if (retval != 0) {
	PUSHi(retval);
    }
    else {
	PUSHp(zero_but_true, ZBTLEN);
    }
#endif
    RETURN;
}

PP(pp_flock)
{
SensorCall(17345);
#ifdef FLOCK
    dVAR; dSP; dTARGET;
    I32 value;
    const int argtype = POPi;
    GV * const gv = MUTABLE_GV(POPs);
    IO *const io = GvIO(gv);
    PerlIO *const fp = io ? IoIFP(io) : NULL;

    /* XXX Looks to me like io is always NULL at this point */
    SensorCall(17348);if (fp) {
	SensorCall(17346);(void)PerlIO_flush(fp);
	value = (I32)(PerlLIO_flock(PerlIO_fileno(fp), argtype) >= 0);
    }
    else {
	report_evil_fh(gv);
	SensorCall(17347);value = 0;
	SETERRNO(EBADF,RMS_IFI);
    }
    PUSHi(value);
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "flock()");
#endif
}

/* Sockets. */

#ifdef HAS_SOCKET

PP(pp_socket)
{
SensorCall(17349);    dVAR; dSP;
    const int protocol = POPi;
    const int type = POPi;
    const int domain = POPi;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = gv ? GvIOn(gv) : NULL;
    int fd;

    SensorCall(17351);if (!io) {
	report_evil_fh(gv);
	SensorCall(17350);if (io && IoIFP(io))
	    do_close(gv, FALSE);
	SETERRNO(EBADF,LIB_INVARG);
	RETPUSHUNDEF;
    }

    SensorCall(17352);if (IoIFP(io))
	do_close(gv, FALSE);

    TAINT_PROPER("socket");
    SensorCall(17353);fd = PerlSock_socket(domain, type, protocol);
    SensorCall(17354);if (fd < 0)
	RETPUSHUNDEF;
    IoIFP(io) = PerlIO_fdopen(fd, "r"SOCKET_OPEN_MODE);	/* stdio gets confused about sockets */
    IoOFP(io) = PerlIO_fdopen(fd, "w"SOCKET_OPEN_MODE);
    IoTYPE(io) = IoTYPE_SOCKET;
    SensorCall(17358);if (!IoIFP(io) || !IoOFP(io)) {
	SensorCall(17355);if (IoIFP(io)) PerlIO_close(IoIFP(io));
	SensorCall(17356);if (IoOFP(io)) PerlIO_close(IoOFP(io));
	SensorCall(17357);if (!IoIFP(io) && !IoOFP(io)) PerlLIO_close(fd);
	RETPUSHUNDEF;
    }
#if defined(HAS_FCNTL) && defined(F_SETFD)
    SensorCall(17359);fcntl(fd, F_SETFD, fd > PL_maxsysfd);	/* ensure close-on-exec */
#endif

#ifdef EPOC
    setbuf( IoIFP(io), NULL); /* EPOC gets confused about sockets */
#endif

    RETPUSHYES;
}
#endif

PP(pp_sockpair)
{
SensorCall(17360);
#if defined (HAS_SOCKETPAIR) || (defined (HAS_SOCKET) && defined(SOCK_DGRAM) && defined(AF_INET) && defined(PF_INET))
    dVAR; dSP;
    const int protocol = POPi;
    const int type = POPi;
    const int domain = POPi;
    GV * const gv2 = MUTABLE_GV(POPs);
    GV * const gv1 = MUTABLE_GV(POPs);
    register IO * const io1 = gv1 ? GvIOn(gv1) : NULL;
    register IO * const io2 = gv2 ? GvIOn(gv2) : NULL;
    int fd[2];

    SensorCall(17361);if (!io1)
	report_evil_fh(gv1);
    SensorCall(17362);if (!io2)
	report_evil_fh(gv2);

    SensorCall(17363);if (io1 && IoIFP(io1))
	do_close(gv1, FALSE);
    SensorCall(17364);if (io2 && IoIFP(io2))
	do_close(gv2, FALSE);

    SensorCall(17365);if (!io1 || !io2)
	RETPUSHUNDEF;

    TAINT_PROPER("socketpair");
    SensorCall(17366);if (PerlSock_socketpair(domain, type, protocol, fd) < 0)
	RETPUSHUNDEF;
    IoIFP(io1) = PerlIO_fdopen(fd[0], "r"SOCKET_OPEN_MODE);
    IoOFP(io1) = PerlIO_fdopen(fd[0], "w"SOCKET_OPEN_MODE);
    IoTYPE(io1) = IoTYPE_SOCKET;
    IoIFP(io2) = PerlIO_fdopen(fd[1], "r"SOCKET_OPEN_MODE);
    IoOFP(io2) = PerlIO_fdopen(fd[1], "w"SOCKET_OPEN_MODE);
    IoTYPE(io2) = IoTYPE_SOCKET;
    SensorCall(17373);if (!IoIFP(io1) || !IoOFP(io1) || !IoIFP(io2) || !IoOFP(io2)) {
	SensorCall(17367);if (IoIFP(io1)) PerlIO_close(IoIFP(io1));
	SensorCall(17368);if (IoOFP(io1)) PerlIO_close(IoOFP(io1));
	SensorCall(17369);if (!IoIFP(io1) && !IoOFP(io1)) PerlLIO_close(fd[0]);
	SensorCall(17370);if (IoIFP(io2)) PerlIO_close(IoIFP(io2));
	SensorCall(17371);if (IoOFP(io2)) PerlIO_close(IoOFP(io2));
	SensorCall(17372);if (!IoIFP(io2) && !IoOFP(io2)) PerlLIO_close(fd[1]);
	RETPUSHUNDEF;
    }
#if defined(HAS_FCNTL) && defined(F_SETFD)
    SensorCall(17374);fcntl(fd[0],F_SETFD,fd[0] > PL_maxsysfd);	/* ensure close-on-exec */
    fcntl(fd[1],F_SETFD,fd[1] > PL_maxsysfd);	/* ensure close-on-exec */
#endif

    RETPUSHYES;
#else
    DIE(aTHX_ PL_no_sock_func, "socketpair");
#endif
}

#ifdef HAS_SOCKET

PP(pp_bind)
{
SensorCall(17375);    dVAR; dSP;
    SV * const addrsv = POPs;
    /* OK, so on what platform does bind modify addr?  */
    const char *addr;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = GvIOn(gv);
    STRLEN len;
    const int op_type = PL_op->op_type;

    SensorCall(17377);if (!io || !IoIFP(io))
	{/*23*/SensorCall(17376);goto nuts;/*24*/}

    SensorCall(17378);addr = SvPV_const(addrsv, len);
    TAINT_PROPER(PL_op_desc[op_type]);
    SensorCall(17379);if ((op_type == OP_BIND
	 ? PerlSock_bind(PerlIO_fileno(IoIFP(io)), (struct sockaddr *)addr, len)
	 : PerlSock_connect(PerlIO_fileno(IoIFP(io)), (struct sockaddr *)addr, len))
	>= 0)
	RETPUSHYES;
    else
	RETPUSHUNDEF;

nuts:
    report_evil_fh(gv);
    SETERRNO(EBADF,SS_IVCHAN);
    RETPUSHUNDEF;
}

PP(pp_listen)
{
SensorCall(17380);    dVAR; dSP;
    const int backlog = POPi;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = gv ? GvIOn(gv) : NULL;

    SensorCall(17382);if (!io || !IoIFP(io))
	{/*77*/SensorCall(17381);goto nuts;/*78*/}

    SensorCall(17383);if (PerlSock_listen(PerlIO_fileno(IoIFP(io)), backlog) >= 0)
	RETPUSHYES;
    else
	RETPUSHUNDEF;

nuts:
    report_evil_fh(gv);
    SETERRNO(EBADF,SS_IVCHAN);
    RETPUSHUNDEF;
}

PP(pp_accept)
{
SensorCall(17384);    dVAR; dSP; dTARGET;
    register IO *nstio;
    register IO *gstio;
    char namebuf[MAXPATHLEN];
#if (defined(VMS_DO_SOCKETS) && defined(DECCRTL_SOCKETS)) || defined(MPE) || defined(__QNXNTO__)
    Sock_size_t len = sizeof (struct sockaddr_in);
#else
    Sock_size_t len = sizeof namebuf;
#endif
    GV * const ggv = MUTABLE_GV(POPs);
    GV * const ngv = MUTABLE_GV(POPs);
    int fd;

    SensorCall(17386);if (!ngv)
	{/*5*/SensorCall(17385);goto badexit;/*6*/}
    SensorCall(17388);if (!ggv)
	{/*7*/SensorCall(17387);goto nuts;/*8*/}

    SensorCall(17389);gstio = GvIO(ggv);
    SensorCall(17391);if (!gstio || !IoIFP(gstio))
	{/*9*/SensorCall(17390);goto nuts;/*10*/}

    SensorCall(17392);nstio = GvIOn(ngv);
    fd = PerlSock_accept(PerlIO_fileno(IoIFP(gstio)), (struct sockaddr *) namebuf, &len);
#if defined(OEMVS)
    if (len == 0) {
	/* Some platforms indicate zero length when an AF_UNIX client is
	 * not bound. Simulate a non-zero-length sockaddr structure in
	 * this case. */
	namebuf[0] = 0;        /* sun_len */
	namebuf[1] = AF_UNIX;  /* sun_family */
	len = 2;
    }
#endif

    SensorCall(17394);if (fd < 0)
	{/*11*/SensorCall(17393);goto badexit;/*12*/}
    SensorCall(17395);if (IoIFP(nstio))
	do_close(ngv, FALSE);
    IoIFP(nstio) = PerlIO_fdopen(fd, "r"SOCKET_OPEN_MODE);
    IoOFP(nstio) = PerlIO_fdopen(fd, "w"SOCKET_OPEN_MODE);
    IoTYPE(nstio) = IoTYPE_SOCKET;
    SensorCall(17400);if (!IoIFP(nstio) || !IoOFP(nstio)) {
	SensorCall(17396);if (IoIFP(nstio)) PerlIO_close(IoIFP(nstio));
	SensorCall(17397);if (IoOFP(nstio)) PerlIO_close(IoOFP(nstio));
	SensorCall(17398);if (!IoIFP(nstio) && !IoOFP(nstio)) PerlLIO_close(fd);
	SensorCall(17399);goto badexit;
    }
#if defined(HAS_FCNTL) && defined(F_SETFD)
    SensorCall(17401);fcntl(fd, F_SETFD, fd > PL_maxsysfd);	/* ensure close-on-exec */
#endif

#ifdef EPOC
    len = sizeof (struct sockaddr_in); /* EPOC somehow truncates info */
    setbuf( IoIFP(nstio), NULL); /* EPOC gets confused about sockets */
#endif
#ifdef __SCO_VERSION__
    len = sizeof (struct sockaddr_in); /* OpenUNIX 8 somehow truncates info */
#endif

    PUSHp(namebuf, len);
    RETURN;

nuts:
    report_evil_fh(ggv);
    SETERRNO(EBADF,SS_IVCHAN);

badexit:
    RETPUSHUNDEF;

}

PP(pp_shutdown)
{
SensorCall(17402);    dVAR; dSP; dTARGET;
    const int how = POPi;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = GvIOn(gv);

    SensorCall(17404);if (!io || !IoIFP(io))
	{/*103*/SensorCall(17403);goto nuts;/*104*/}

    PUSHi( PerlSock_shutdown(PerlIO_fileno(IoIFP(io)), how) >= 0 );
    RETURN;

nuts:
    report_evil_fh(gv);
    SETERRNO(EBADF,SS_IVCHAN);
    RETPUSHUNDEF;
}

PP(pp_ssockopt)
{
SensorCall(17405);    dVAR; dSP;
    const int optype = PL_op->op_type;
    SV * const sv = (optype == OP_GSOCKOPT) ? sv_2mortal(newSV(257)) : POPs;
    const unsigned int optname = (unsigned int) POPi;
    const unsigned int lvl = (unsigned int) POPi;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = GvIOn(gv);
    int fd;
    Sock_size_t len;

    SensorCall(17407);if (!io || !IoIFP(io))
	{/*115*/SensorCall(17406);goto nuts;/*116*/}

    SensorCall(17408);fd = PerlIO_fileno(IoIFP(io));
    SensorCall(17421);switch (optype) {
    case OP_GSOCKOPT:
	SvGROW(sv, 257);
	SensorCall(17409);(void)SvPOK_only(sv);
	SvCUR_set(sv,256);
	*SvEND(sv) ='\0';
	len = SvCUR(sv);
	SensorCall(17411);if (PerlSock_getsockopt(fd, lvl, optname, SvPVX(sv), &len) < 0)
	    {/*117*/SensorCall(17410);goto nuts2;/*118*/}
	SvCUR_set(sv, len);
	SensorCall(17412);*SvEND(sv) ='\0';
	PUSHs(sv);
	SensorCall(17413);break;
    case OP_SSOCKOPT: {
#if defined(__SYMBIAN32__)
# define SETSOCKOPT_OPTION_VALUE_T void *
#else
# define SETSOCKOPT_OPTION_VALUE_T const char *
#endif
	/* XXX TODO: We need to have a proper type (a Configure probe,
	 * etc.) for what the C headers think of the third argument of
	 * setsockopt(), the option_value read-only buffer: is it
	 * a "char *", or a "void *", const or not.  Some compilers
	 * don't take kindly to e.g. assuming that "char *" implicitly
	 * promotes to a "void *", or to explicitly promoting/demoting
	 * consts to non/vice versa.  The "const void *" is the SUS
	 * definition, but that does not fly everywhere for the above
	 * reasons. */
	    SETSOCKOPT_OPTION_VALUE_T buf;
	    int aint;
	    SensorCall(17417);if (SvPOKp(sv)) {
		SensorCall(17415);STRLEN l;
		buf = (SETSOCKOPT_OPTION_VALUE_T) SvPV_const(sv, l);
		len = l;
	    }
	    else {
		SensorCall(17416);aint = (int)SvIV(sv);
		buf = (SETSOCKOPT_OPTION_VALUE_T) &aint;
		len = sizeof(int);
	    }
	    SensorCall(17419);if (PerlSock_setsockopt(fd, lvl, optname, buf, len) < 0)
		{/*119*/SensorCall(17418);goto nuts2;/*120*/}
	    PUSHs(&PL_sv_yes);
	}
	SensorCall(17420);break;
    }
    RETURN;

nuts:
    report_evil_fh(gv);
    SETERRNO(EBADF,SS_IVCHAN);
nuts2:
    RETPUSHUNDEF;

}

PP(pp_getpeername)
{
SensorCall(17422);    dVAR; dSP;
    const int optype = PL_op->op_type;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = GvIOn(gv);
    Sock_size_t len;
    SV *sv;
    int fd;

    SensorCall(17424);if (!io || !IoIFP(io))
	{/*57*/SensorCall(17423);goto nuts;/*58*/}

    SensorCall(17425);sv = sv_2mortal(newSV(257));
    (void)SvPOK_only(sv);
    len = 256;
    SvCUR_set(sv, len);
    *SvEND(sv) ='\0';
    fd = PerlIO_fileno(IoIFP(io));
    SensorCall(17432);switch (optype) {
    case OP_GETSOCKNAME:
	SensorCall(17426);if (PerlSock_getsockname(fd, (struct sockaddr *)SvPVX(sv), &len) < 0)
	    {/*59*/SensorCall(17427);goto nuts2;/*60*/}
	SensorCall(17428);break;
    case OP_GETPEERNAME:
	SensorCall(17429);if (PerlSock_getpeername(fd, (struct sockaddr *)SvPVX(sv), &len) < 0)
	    {/*61*/SensorCall(17430);goto nuts2;/*62*/}
#if defined(VMS_DO_SOCKETS) && defined (DECCRTL_SOCKETS)
	{
	    static const char nowhere[] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
	    /* If the call succeeded, make sure we don't have a zeroed port/addr */
	    if (((struct sockaddr *)SvPVX_const(sv))->sa_family == AF_INET &&
		!memcmp(SvPVX_const(sv) + sizeof(u_short), nowhere,
			sizeof(u_short) + sizeof(struct in_addr))) {
		goto nuts2;	
	    }
	}
#endif
	SensorCall(17431);break;
    }
#ifdef BOGUS_GETNAME_RETURN
    /* Interactive Unix, getpeername() and getsockname()
      does not return valid namelen */
    if (len == BOGUS_GETNAME_RETURN)
	len = sizeof(struct sockaddr);
#endif
    SvCUR_set(sv, len);
    SensorCall(17433);*SvEND(sv) ='\0';
    PUSHs(sv);
    RETURN;

nuts:
    report_evil_fh(gv);
    SETERRNO(EBADF,SS_IVCHAN);
nuts2:
    RETPUSHUNDEF;
}

#endif

/* Stat calls. */

PP(pp_stat)
{
SensorCall(17434);    dVAR;
    dSP;
    GV *gv = NULL;
    IO *io = NULL;
    I32 gimme;
    I32 max = 13;
    SV* sv;

    SensorCall(17462);if (PL_op->op_flags & OPf_REF ? (gv = cGVOP_gv, 1)
                                  : !!(sv=POPs, gv = MAYBE_DEREF_GV(sv))) {
	SensorCall(17435);if (PL_op->op_type == OP_LSTAT) {
	    SensorCall(17436);if (gv != PL_defgv) {
	    do_fstat_warning_check:
		SensorCall(17437);Perl_ck_warner(aTHX_ packWARN(WARN_IO),
			       "lstat() on filehandle%s%"SVf,
				gv ? " " : "",
				SVfARG(gv
                                        ? sv_2mortal(newSVhek(GvENAME_HEK(gv)))
                                        : &PL_sv_no));
	    } else {/*121*/SensorCall(17438);if (PL_laststype != OP_LSTAT)
		/* diag_listed_as: The stat preceding %s wasn't an lstat */
		{/*123*/SensorCall(17439);Perl_croak(aTHX_ "The stat preceding lstat() wasn't an lstat");/*124*/}/*122*/}
	}

	SensorCall(17449);if (gv != PL_defgv) {
	    bool havefp;
          do_fstat_have_io:
	    SensorCall(17440);havefp = FALSE;
	    PL_laststype = OP_STAT;
	    PL_statgv = gv ? gv : (GV *)io;
	    sv_setpvs(PL_statname, "");
            SensorCall(17442);if(gv) {
                SensorCall(17441);io = GvIO(gv);
	    }
            SensorCall(17447);if (io) {
                    SensorCall(17443);if (IoIFP(io)) {
                        PL_laststatval = 
                            PerlLIO_fstat(PerlIO_fileno(IoIFP(io)), &PL_statcache);   
                        SensorCall(17444);havefp = TRUE;
                    } else {/*125*/SensorCall(17445);if (IoDIRP(io)) {
                        PL_laststatval =
                            PerlLIO_fstat(my_dirfd(IoDIRP(io)), &PL_statcache);
                        SensorCall(17446);havefp = TRUE;
                    } else {
                        PL_laststatval = -1;
                    ;/*126*/}}
            }
	    else PL_laststatval = -1;
	    SensorCall(17448);if (PL_laststatval < 0 && !havefp) report_evil_fh(gv);
        }

	SensorCall(17451);if (PL_laststatval < 0) {
	    SensorCall(17450);max = 0;
	}
    }
    else {
	SensorCall(17452);if (SvROK(sv) && SvTYPE(SvRV(sv)) == SVt_PVIO) { 
            SensorCall(17453);io = MUTABLE_IO(SvRV(sv));
            SensorCall(17455);if (PL_op->op_type == OP_LSTAT)
                {/*127*/SensorCall(17454);goto do_fstat_warning_check;/*128*/}
            SensorCall(17456);goto do_fstat_have_io; 
        }
        
	sv_setpv(PL_statname, SvPV_nomg_const_nolen(sv));
	PL_statgv = NULL;
	PL_laststype = PL_op->op_type;
	SensorCall(17457);if (PL_op->op_type == OP_LSTAT)
	    PL_laststatval = PerlLIO_lstat(SvPV_nolen_const(PL_statname), &PL_statcache);
	else
	    PL_laststatval = PerlLIO_stat(SvPV_nolen_const(PL_statname), &PL_statcache);
	SensorCall(17461);if (PL_laststatval < 0) {
	    SensorCall(17458);if (ckWARN(WARN_NEWLINE) && strchr(SvPV_nolen_const(PL_statname), '\n'))
		{/*129*/SensorCall(17459);Perl_warner(aTHX_ packWARN(WARN_NEWLINE), PL_warn_nl, "stat");/*130*/}
	    SensorCall(17460);max = 0;
	}
    }

    SensorCall(17463);gimme = GIMME_V;
    SensorCall(17465);if (gimme != G_ARRAY) {
	SensorCall(17464);if (gimme != G_VOID)
	    XPUSHs(boolSV(max));
	RETURN;
    }
    SensorCall(17466);if (max) {
	EXTEND(SP, max);
	EXTEND_MORTAL(max);
	mPUSHi(PL_statcache.st_dev);
#if ST_INO_SIZE > IVSIZE
	mPUSHn(PL_statcache.st_ino);
#else
#   if ST_INO_SIGN <= 0
	mPUSHi(PL_statcache.st_ino);
#   else
	mPUSHu(PL_statcache.st_ino);
#   endif
#endif
	mPUSHu(PL_statcache.st_mode);
	mPUSHu(PL_statcache.st_nlink);
#if Uid_t_size > IVSIZE
	mPUSHn(PL_statcache.st_uid);
#else
#   if Uid_t_sign <= 0
	mPUSHi(PL_statcache.st_uid);
#   else
	mPUSHu(PL_statcache.st_uid);
#   endif
#endif
#if Gid_t_size > IVSIZE
	mPUSHn(PL_statcache.st_gid);
#else
#   if Gid_t_sign <= 0
	mPUSHi(PL_statcache.st_gid);
#   else
	mPUSHu(PL_statcache.st_gid);
#   endif
#endif
#ifdef USE_STAT_RDEV
	mPUSHi(PL_statcache.st_rdev);
#else
	PUSHs(newSVpvs_flags("", SVs_TEMP));
#endif
#if Off_t_size > IVSIZE
	mPUSHn(PL_statcache.st_size);
#else
	mPUSHi(PL_statcache.st_size);
#endif
#ifdef BIG_TIME
	mPUSHn(PL_statcache.st_atime);
	mPUSHn(PL_statcache.st_mtime);
	mPUSHn(PL_statcache.st_ctime);
#else
	mPUSHi(PL_statcache.st_atime);
	mPUSHi(PL_statcache.st_mtime);
	mPUSHi(PL_statcache.st_ctime);
#endif
#ifdef USE_STAT_BLOCKS
	mPUSHu(PL_statcache.st_blksize);
	mPUSHu(PL_statcache.st_blocks);
#else
	PUSHs(newSVpvs_flags("", SVs_TEMP));
	PUSHs(newSVpvs_flags("", SVs_TEMP));
#endif
    }
    RETURN;
}

/* If the next filetest is stacked up with this one
   (PL_op->op_private & OPpFT_STACKING), we leave
   the original argument on the stack for success,
   and skip the stacked operators on failure.
   The next few macros/functions take care of this.
*/

static OP *
S_ft_stacking_return_false(pTHX_ SV *ret) {
SensorCall(17467);    dSP;
    OP *next = NORMAL;
    SensorCall(17469);while (OP_IS_FILETEST(next->op_type)
	&& next->op_private & OPpFT_STACKED)
	{/*207*/SensorCall(17468);next = next->op_next;/*208*/}
    SensorCall(17470);if (PL_op->op_flags & OPf_REF) PUSHs(ret);
    else			   SETs(ret);
    PUTBACK;
    {OP * ReplaceReturn1273 = next; SensorCall(17471); return ReplaceReturn1273;}
}

#define FT_RETURN_FALSE(X)			     \
    STMT_START {				      \
	if (PL_op->op_private & OPpFT_STACKING)	       \
	    return S_ft_stacking_return_false(aTHX_ X);	\
	RETURNX(PUSHs(X));				 \
    } STMT_END
#define FT_RETURN_TRUE(X)		 \
    RETURNX((void)(			  \
	PL_op->op_private & OPpFT_STACKING \
	    ? PL_op->op_flags & OPf_REF	    \
		? PUSHs((SV *)cGVOP_gv)	     \
		: 0			      \
	    : PUSHs(X)			       \
    ))

#define FT_RETURNNO	FT_RETURN_FALSE(&PL_sv_no)
#define FT_RETURNUNDEF	FT_RETURN_FALSE(&PL_sv_undef)
#define FT_RETURNYES	FT_RETURN_TRUE(&PL_sv_yes)

#define tryAMAGICftest_MG(chr) STMT_START { \
	if ( (SvFLAGS(TOPs) & (SVf_ROK|SVs_GMG)) \
		&& PL_op->op_flags & OPf_KIDS) {     \
	    OP *next = S_try_amagic_ftest(aTHX_ chr);	\
	    if (next) return next;			  \
	}						   \
    } STMT_END

STATIC OP *
S_try_amagic_ftest(pTHX_ char chr) {
SensorCall(17472);    dVAR;
    dSP;
    SV* const arg = TOPs;

    assert(chr != '?');
    SensorCall(17473);if (!(PL_op->op_private & OPpFT_STACKING)) SvGETMAGIC(arg);

    SensorCall(17479);if (SvAMAGIC(TOPs))
    {
	SensorCall(17474);const char tmpchr = chr;
	SV * const tmpsv = amagic_call(arg,
				newSVpvn_flags(&tmpchr, 1, SVs_TEMP),
				ftest_amg, AMGf_unary);

	SensorCall(17475);if (!tmpsv)
	    return NULL;

	SensorCall(17478);if (PL_op->op_private & OPpFT_STACKING) {
	    SensorCall(17476);if (SvTRUE(tmpsv)) return NORMAL;
	    {OP * ReplaceReturn1272 = S_ft_stacking_return_false(aTHX_ tmpsv); SensorCall(17477); return ReplaceReturn1272;}
	}

	SPAGAIN;

	RETURNX(SETs(tmpsv));
    }
    {OP * ReplaceReturn1271 = NULL; SensorCall(17480); return ReplaceReturn1271;}
}


PP(pp_ftrread)
{
SensorCall(17481);    dVAR;
    I32 result;
    /* Not const, because things tweak this below. Not bool, because there's
       no guarantee that OPp_FT_ACCESS is <= CHAR_MAX  */
#if defined(HAS_ACCESS) || defined (PERL_EFF_ACCESS)
    I32 use_access = PL_op->op_private & OPpFT_ACCESS;
    /* Giving some sort of initial value silences compilers.  */
#  ifdef R_OK
    int access_mode = R_OK;
#  else
    int access_mode = 0;
#  endif
#else
    /* access_mode is never used, but leaving use_access in makes the
       conditional compiling below much clearer.  */
    I32 use_access = 0;
#endif
    Mode_t stat_mode = S_IRUSR;

    bool effective = FALSE;
    char opchar = '?';
    dSP;

    SensorCall(17494);switch (PL_op->op_type) {
    case OP_FTRREAD:	SensorCall(17482);opchar = 'R'; SensorCall(17483);break;
    case OP_FTRWRITE:	SensorCall(17484);opchar = 'W'; SensorCall(17485);break;
    case OP_FTREXEC:	SensorCall(17486);opchar = 'X'; SensorCall(17487);break;
    case OP_FTEREAD:	SensorCall(17488);opchar = 'r'; SensorCall(17489);break;
    case OP_FTEWRITE:	SensorCall(17490);opchar = 'w'; SensorCall(17491);break;
    case OP_FTEEXEC:	SensorCall(17492);opchar = 'x'; SensorCall(17493);break;
    }
    tryAMAGICftest_MG(opchar);

    SensorCall(17505);switch (PL_op->op_type) {
    case OP_FTRREAD:
#if !(defined(HAS_ACCESS) && defined(R_OK))
	use_access = 0;
#endif
	SensorCall(17495);break;

    case OP_FTRWRITE:
#if defined(HAS_ACCESS) && defined(W_OK)
	SensorCall(17496);access_mode = W_OK;
#else
	use_access = 0;
#endif
	stat_mode = S_IWUSR;
	SensorCall(17497);break;

    case OP_FTREXEC:
#if defined(HAS_ACCESS) && defined(X_OK)
	SensorCall(17498);access_mode = X_OK;
#else
	use_access = 0;
#endif
	stat_mode = S_IXUSR;
	SensorCall(17499);break;

    case OP_FTEWRITE:
#ifdef PERL_EFF_ACCESS
	SensorCall(17500);access_mode = W_OK;
#endif
	stat_mode = S_IWUSR;
	/* fall through */

    case OP_FTEREAD:
#ifndef PERL_EFF_ACCESS
	use_access = 0;
#endif
	SensorCall(17501);effective = TRUE;
	SensorCall(17502);break;

    case OP_FTEEXEC:
#ifdef PERL_EFF_ACCESS
	SensorCall(17503);access_mode = X_OK;
#else
	use_access = 0;
#endif
	stat_mode = S_IXUSR;
	effective = TRUE;
	SensorCall(17504);break;
    }

    SensorCall(17512);if (use_access) {
#if defined(HAS_ACCESS) || defined (PERL_EFF_ACCESS)
	SensorCall(17506);const char *name = POPpx;
	SensorCall(17509);if (effective) {
#  ifdef PERL_EFF_ACCESS
	    SensorCall(17507);result = PERL_EFF_ACCESS(name, access_mode);
#  else
	    DIE(aTHX_ "panic: attempt to call PERL_EFF_ACCESS in %s",
		OP_NAME(PL_op));
#  endif
	}
	else {
#  ifdef HAS_ACCESS
	    SensorCall(17508);result = access(name, access_mode);
#  else
	    DIE(aTHX_ "panic: attempt to call access() in %s", OP_NAME(PL_op));
#  endif
	}
	SensorCall(17510);if (result == 0)
	    RETPUSHYES;
	SensorCall(17511);if (result < 0)
	    RETPUSHUNDEF;
	RETPUSHNO;
#endif
    }

    SensorCall(17513);result = my_stat_flags(0);
    SPAGAIN;
    SensorCall(17514);if (result < 0)
	FT_RETURNUNDEF;
    SensorCall(17515);if (cando(stat_mode, effective, &PL_statcache))
	FT_RETURNYES;
    FT_RETURNNO;
}

PP(pp_ftis)
{
SensorCall(17516);    dVAR;
    I32 result;
    const int op_type = PL_op->op_type;
    char opchar = '?';
    dSP;

    SensorCall(17527);switch (op_type) {
    case OP_FTIS:	SensorCall(17517);opchar = 'e'; SensorCall(17518);break;
    case OP_FTSIZE:	SensorCall(17519);opchar = 's'; SensorCall(17520);break;
    case OP_FTMTIME:	SensorCall(17521);opchar = 'M'; SensorCall(17522);break;
    case OP_FTCTIME:	SensorCall(17523);opchar = 'C'; SensorCall(17524);break;
    case OP_FTATIME:	SensorCall(17525);opchar = 'A'; SensorCall(17526);break;
    }
    tryAMAGICftest_MG(opchar);

    SensorCall(17528);result = my_stat_flags(0);
    SPAGAIN;
    SensorCall(17529);if (result < 0)
	FT_RETURNUNDEF;
    SensorCall(17530);if (op_type == OP_FTIS)
	FT_RETURNYES;
    {
	/* You can't dTARGET inside OP_FTIS, because you'll get
	   "panic: pad_sv po" - the op is not flagged to have a target.  */
	dTARGET;
	SensorCall(17535);switch (op_type) {
	case OP_FTSIZE:
#if Off_t_size > IVSIZE
	    sv_setnv(TARG, (NV)PL_statcache.st_size);
#else
	    sv_setiv(TARG, (IV)PL_statcache.st_size);
#endif
	    SensorCall(17531);break;
	case OP_FTMTIME:
	    sv_setnv(TARG,
		    ((NV)PL_basetime - PL_statcache.st_mtime) / 86400.0 );
	    SensorCall(17532);break;
	case OP_FTATIME:
	    sv_setnv(TARG,
		    ((NV)PL_basetime - PL_statcache.st_atime) / 86400.0 );
	    SensorCall(17533);break;
	case OP_FTCTIME:
	    sv_setnv(TARG,
		    ((NV)PL_basetime - PL_statcache.st_ctime) / 86400.0 );
	    SensorCall(17534);break;
	}
	SvSETMAGIC(TARG);
	SensorCall(17536);if (SvTRUE_nomg(TARG)) FT_RETURN_TRUE(TARG);
	else		       FT_RETURN_FALSE(TARG);
    }
SensorCall(17537);}

PP(pp_ftrowned)
{
SensorCall(17538);    dVAR;
    I32 result;
    char opchar = '?';
    dSP;

    SensorCall(17563);switch (PL_op->op_type) {
    case OP_FTROWNED:	SensorCall(17539);opchar = 'O'; SensorCall(17540);break;
    case OP_FTEOWNED:	SensorCall(17541);opchar = 'o'; SensorCall(17542);break;
    case OP_FTZERO:	SensorCall(17543);opchar = 'z'; SensorCall(17544);break;
    case OP_FTSOCK:	SensorCall(17545);opchar = 'S'; SensorCall(17546);break;
    case OP_FTCHR:	SensorCall(17547);opchar = 'c'; SensorCall(17548);break;
    case OP_FTBLK:	SensorCall(17549);opchar = 'b'; SensorCall(17550);break;
    case OP_FTFILE:	SensorCall(17551);opchar = 'f'; SensorCall(17552);break;
    case OP_FTDIR:	SensorCall(17553);opchar = 'd'; SensorCall(17554);break;
    case OP_FTPIPE:	SensorCall(17555);opchar = 'p'; SensorCall(17556);break;
    case OP_FTSUID:	SensorCall(17557);opchar = 'u'; SensorCall(17558);break;
    case OP_FTSGID:	SensorCall(17559);opchar = 'g'; SensorCall(17560);break;
    case OP_FTSVTX:	SensorCall(17561);opchar = 'k'; SensorCall(17562);break;
    }
    tryAMAGICftest_MG(opchar);

    /* I believe that all these three are likely to be defined on most every
       system these days.  */
#ifndef S_ISUID
    if(PL_op->op_type == OP_FTSUID) {
	if ((PL_op->op_flags & OPf_REF) == 0 && !(PL_op->op_private & OPpFT_STACKING))
	    (void) POPs;
	FT_RETURNNO;
    }
#endif
#ifndef S_ISGID
    if(PL_op->op_type == OP_FTSGID) {
	if ((PL_op->op_flags & OPf_REF) == 0 && !(PL_op->op_private & OPpFT_STACKING))
	    (void) POPs;
	FT_RETURNNO;
    }
#endif
#ifndef S_ISVTX
    if(PL_op->op_type == OP_FTSVTX) {
	if ((PL_op->op_flags & OPf_REF) == 0 && !(PL_op->op_private & OPpFT_STACKING))
	    (void) POPs;
	FT_RETURNNO;
    }
#endif

    SensorCall(17564);result = my_stat_flags(0);
    SPAGAIN;
    SensorCall(17565);if (result < 0)
	FT_RETURNUNDEF;
    SensorCall(17590);switch (PL_op->op_type) {
    case OP_FTROWNED:
	SensorCall(17566);if (PL_statcache.st_uid == PerlProc_getuid())
	    FT_RETURNYES;
	SensorCall(17567);break;
    case OP_FTEOWNED:
	SensorCall(17568);if (PL_statcache.st_uid == PerlProc_geteuid())
	    FT_RETURNYES;
	SensorCall(17569);break;
    case OP_FTZERO:
	SensorCall(17570);if (PL_statcache.st_size == 0)
	    FT_RETURNYES;
	SensorCall(17571);break;
    case OP_FTSOCK:
	SensorCall(17572);if (S_ISSOCK(PL_statcache.st_mode))
	    FT_RETURNYES;
	SensorCall(17573);break;
    case OP_FTCHR:
	SensorCall(17574);if (S_ISCHR(PL_statcache.st_mode))
	    FT_RETURNYES;
	SensorCall(17575);break;
    case OP_FTBLK:
	SensorCall(17576);if (S_ISBLK(PL_statcache.st_mode))
	    FT_RETURNYES;
	SensorCall(17577);break;
    case OP_FTFILE:
	SensorCall(17578);if (S_ISREG(PL_statcache.st_mode))
	    FT_RETURNYES;
	SensorCall(17579);break;
    case OP_FTDIR:
	SensorCall(17580);if (S_ISDIR(PL_statcache.st_mode))
	    FT_RETURNYES;
	SensorCall(17581);break;
    case OP_FTPIPE:
	SensorCall(17582);if (S_ISFIFO(PL_statcache.st_mode))
	    FT_RETURNYES;
	SensorCall(17583);break;
#ifdef S_ISUID
    case OP_FTSUID:
	SensorCall(17584);if (PL_statcache.st_mode & S_ISUID)
	    FT_RETURNYES;
	SensorCall(17585);break;
#endif
#ifdef S_ISGID
    case OP_FTSGID:
	SensorCall(17586);if (PL_statcache.st_mode & S_ISGID)
	    FT_RETURNYES;
	SensorCall(17587);break;
#endif
#ifdef S_ISVTX
    case OP_FTSVTX:
	SensorCall(17588);if (PL_statcache.st_mode & S_ISVTX)
	    FT_RETURNYES;
	SensorCall(17589);break;
#endif
    }
    FT_RETURNNO;
}

PP(pp_ftlink)
{
SensorCall(17591);    dVAR;
    dSP;
    I32 result;

    tryAMAGICftest_MG('l');
    result = my_lstat_flags(0);
    SPAGAIN;

    SensorCall(17592);if (result < 0)
	FT_RETURNUNDEF;
    SensorCall(17593);if (S_ISLNK(PL_statcache.st_mode))
	FT_RETURNYES;
    FT_RETURNNO;
}

PP(pp_fttty)
{
SensorCall(17594);    dVAR;
    dSP;
    int fd;
    GV *gv;
    char *name = NULL;
    STRLEN namelen;

    tryAMAGICftest_MG('t');

    SensorCall(17598);if (PL_op->op_flags & OPf_REF)
	gv = cGVOP_gv;
    else {
      SensorCall(17595);SV *tmpsv = PL_op->op_private & OPpFT_STACKING ? TOPs : POPs;
      SensorCall(17597);if (!(gv = MAYBE_DEREF_GV_nomg(tmpsv))) {
	SensorCall(17596);name = SvPV_nomg(tmpsv, namelen);
	gv = gv_fetchpvn_flags(name, namelen, SvUTF8(tmpsv), SVt_PVIO);
      }
    }

    SensorCall(17599);if (GvIO(gv) && IoIFP(GvIOp(gv)))
	fd = PerlIO_fileno(IoIFP(GvIOp(gv)));
    else if (name && isDIGIT(*name))
	    {/*55*/fd = atoi(name);/*56*/}
    else
	FT_RETURNUNDEF;
    SensorCall(17600);if (PerlLIO_isatty(fd))
	FT_RETURNYES;
    FT_RETURNNO;
}

#if defined(atarist) /* this will work with atariST. Configure will
			make guesses for other systems. */
# define FILE_base(f) ((f)->_base)
# define FILE_ptr(f) ((f)->_ptr)
# define FILE_cnt(f) ((f)->_cnt)
# define FILE_bufsiz(f) ((f)->_cnt + ((f)->_ptr - (f)->_base))
#endif

PP(pp_fttext)
{
SensorCall(17601);    dVAR;
    dSP;
    I32 i;
    I32 len;
    I32 odd = 0;
    STDCHAR tbuf[512];
    register STDCHAR *s;
    register IO *io;
    register SV *sv = NULL;
    GV *gv;
    PerlIO *fp;

    tryAMAGICftest_MG(PL_op->op_type == OP_FTTEXT ? 'T' : 'B');

    SensorCall(17605);if (PL_op->op_flags & OPf_REF)
    {
	SensorCall(17602);gv = cGVOP_gv;
	EXTEND(SP, 1);
    }
    else {
      SensorCall(17603);sv = PL_op->op_private & OPpFT_STACKING ? TOPs : POPs;
      SensorCall(17604);if ((PL_op->op_private & (OPpFT_STACKED|OPpFT_AFTER_t))
	     == OPpFT_STACKED)
	gv = PL_defgv;
      else gv = MAYBE_DEREF_GV_nomg(sv);
    }

    SensorCall(17633);if (gv) {
	SensorCall(17606);if (gv == PL_defgv) {
	    SensorCall(17607);if (PL_statgv)
		io = SvTYPE(PL_statgv) == SVt_PVIO
		    ? (IO *)PL_statgv
		    : GvIO(PL_statgv);
	    else {
		SensorCall(17608);goto really_filename;
	    }
	}
	else {
	    PL_statgv = gv;
	    sv_setpvs(PL_statname, "");
	    SensorCall(17609);io = GvIO(PL_statgv);
	}
	PL_laststatval = -1;
	PL_laststype = OP_STAT;
	SensorCall(17622);if (io && IoIFP(io)) {
	    SensorCall(17610);if (! PerlIO_has_base(IoIFP(io)))
		DIE(aTHX_ "-T and -B not implemented on filehandles");
	    PL_laststatval = PerlLIO_fstat(PerlIO_fileno(IoIFP(io)), &PL_statcache);
	    SensorCall(17611);if (PL_laststatval < 0)
		FT_RETURNUNDEF;
	    SensorCall(17613);if (S_ISDIR(PL_statcache.st_mode)) { /* handle NFS glitch */
		SensorCall(17612);if (PL_op->op_type == OP_FTTEXT)
		    FT_RETURNNO;
		else
		    FT_RETURNYES;
            }
	    SensorCall(17617);if (PerlIO_get_cnt(IoIFP(io)) <= 0) {
		SensorCall(17614);i = PerlIO_getc(IoIFP(io));
		SensorCall(17616);if (i != EOF)
		    {/*39*/SensorCall(17615);(void)PerlIO_ungetc(IoIFP(io),i);/*40*/}
	    }
	    SensorCall(17618);if (PerlIO_get_cnt(IoIFP(io)) <= 0)	/* null file is anything */
		FT_RETURNYES;
	    SensorCall(17619);len = PerlIO_get_bufsiz(IoIFP(io));
	    s = (STDCHAR *) PerlIO_get_base(IoIFP(io));
	    /* sfio can have large buffers - limit to 512 */
	    SensorCall(17621);if (len > 512)
		{/*41*/SensorCall(17620);len = 512;/*42*/}
	}
	else {
	    SETERRNO(EBADF,RMS_IFI);
	    report_evil_fh(gv);
	    SETERRNO(EBADF,RMS_IFI);
	    FT_RETURNUNDEF;
	}
    }
    else {
	sv_setpv(PL_statname, SvPV_nomg_const_nolen(sv));
      really_filename:
	PL_statgv = NULL;
	SensorCall(17626);if (!(fp = PerlIO_open(SvPVX_const(PL_statname), "r"))) {
	    SensorCall(17623);if (!gv) {
		PL_laststatval = -1;
		PL_laststype = OP_STAT;
	    }
	    SensorCall(17625);if (ckWARN(WARN_NEWLINE) && strchr(SvPV_nolen_const(PL_statname),
					       '\n'))
		{/*43*/SensorCall(17624);Perl_warner(aTHX_ packWARN(WARN_NEWLINE), PL_warn_nl, "open");/*44*/}
	    FT_RETURNUNDEF;
	}
	PL_laststype = OP_STAT;
	PL_laststatval = PerlLIO_fstat(PerlIO_fileno(fp), &PL_statcache);
	SensorCall(17628);if (PL_laststatval < 0)	{
	    SensorCall(17627);(void)PerlIO_close(fp);
	    FT_RETURNUNDEF;
	}
	SensorCall(17629);PerlIO_binmode(aTHX_ fp, '<', O_BINARY, NULL);
	len = PerlIO_read(fp, tbuf, sizeof(tbuf));
	(void)PerlIO_close(fp);
	SensorCall(17631);if (len <= 0) {
	    SensorCall(17630);if (S_ISDIR(PL_statcache.st_mode) && PL_op->op_type == OP_FTTEXT)
		FT_RETURNNO;		/* special case NFS directories */
	    FT_RETURNYES;		/* null file is anything */
	}
	SensorCall(17632);s = tbuf;
    }

    /* now scan s to look for textiness */
    /*   XXX ASCII dependent code */

#if defined(DOSISH) || defined(USEMYBINMODE)
    /* ignore trailing ^Z on short files */
    if (len && len < (I32)sizeof(tbuf) && tbuf[len-1] == 26)
	--len;
#endif

    SensorCall(17652);for (i = 0; i < len; i++, s++) {
	SensorCall(17634);if (!*s) {			/* null never allowed in text */
	    SensorCall(17635);odd += len;
	    SensorCall(17636);break;
	}
#ifdef EBCDIC
        else if (!(isPRINT(*s) || isSPACE(*s)))
            odd++;
#else
	else {/*45*/SensorCall(17637);if (*s & 128) {
#ifdef USE_LOCALE
	    SensorCall(17638);if (IN_LOCALE_RUNTIME && isALPHA_LC(*s))
		{/*47*/SensorCall(17639);continue;/*48*/}
#endif
	    /* utf8 characters don't count as odd */
	    SensorCall(17648);if (UTF8_IS_START(*s)) {
		SensorCall(17640);int ulen = UTF8SKIP(s);
		SensorCall(17647);if (ulen < len - i) {
		    SensorCall(17641);int j;
		    SensorCall(17644);for (j = 1; j < ulen; j++) {
			SensorCall(17642);if (!UTF8_IS_CONTINUATION(s[j]))
			    {/*49*/SensorCall(17643);goto not_utf8;/*50*/}
		    }
		    SensorCall(17645);--ulen;	/* loop does extra increment */
		    s += ulen;
		    i += ulen;
		    SensorCall(17646);continue;
		}
	    }
	  not_utf8:
	    SensorCall(17649);odd++;
	}
	else {/*51*/SensorCall(17650);if (*s < 32 &&
	  *s != '\n' && *s != '\r' && *s != '\b' &&
	  *s != '\t' && *s != '\f' && *s != 27)
	    {/*53*/SensorCall(17651);odd++;/*54*/}/*52*/}/*46*/}
#endif
    }

    SensorCall(17653);if ((odd * 3 > len) == (PL_op->op_type == OP_FTTEXT)) /* allow 1/3 odd */
	FT_RETURNNO;
    else
	FT_RETURNYES;
SensorCall(17654);}

/* File calls. */

PP(pp_chdir)
{
SensorCall(17655);    dVAR; dSP; dTARGET;
    const char *tmps = NULL;
    GV *gv = NULL;

    SensorCall(17659);if( MAXARG == 1 ) {
	SensorCall(17656);SV * const sv = POPs;
	SensorCall(17658);if (PL_op->op_flags & OPf_SPECIAL) {
	    SensorCall(17657);gv = gv_fetchsv(sv, 0, SVt_PVIO);
	}
        else if (!(gv = MAYBE_DEREF_GV(sv)))
		tmps = SvPV_nomg_const_nolen(sv);
    }

    SensorCall(17664);if( !gv && (!tmps || !*tmps) ) {
	SensorCall(17660);HV * const table = GvHVn(PL_envgv);
	SV **svp;

        SensorCall(17663);if (    (svp = hv_fetchs(table, "HOME", FALSE))
             || (svp = hv_fetchs(table, "LOGDIR", FALSE))
#ifdef VMS
             || (svp = hv_fetchs(table, "SYS$LOGIN", FALSE))
#endif
           )
        {
            SensorCall(17661);if( MAXARG == 1 )
                deprecate("chdir('') or chdir(undef) as chdir()");
            SensorCall(17662);tmps = SvPV_nolen_const(*svp);
        }
        else {
            PUSHi(0);
            TAINT_PROPER("chdir");
            RETURN;
        }
    }

    TAINT_PROPER("chdir");
    SensorCall(17669);if (gv) {
#ifdef HAS_FCHDIR
	SensorCall(17665);IO* const io = GvIO(gv);
	SensorCall(17668);if (io) {
	    SensorCall(17666);if (IoDIRP(io)) {
		PUSHi(fchdir(my_dirfd(IoDIRP(io))) >= 0);
	    } else {/*25*/SensorCall(17667);if (IoIFP(io)) {
                PUSHi(fchdir(PerlIO_fileno(IoIFP(io))) >= 0);
	    }
	    else {
		report_evil_fh(gv);
		SETERRNO(EBADF, RMS_IFI);
		PUSHi(0);
	    ;/*26*/}}
        }
	else {
	    report_evil_fh(gv);
	    SETERRNO(EBADF,RMS_IFI);
	    PUSHi(0);
	}
#else
	DIE(aTHX_ PL_no_func, "fchdir");
#endif
    }
    else 
        PUSHi( PerlDir_chdir(tmps) >= 0 );
#ifdef VMS
    /* Clear the DEFAULT element of ENV so we'll get the new value
     * in the future. */
    hv_delete(GvHVn(PL_envgv),"DEFAULT",7,G_DISCARD);
#endif
    RETURN;
}

PP(pp_chown)
{
SensorCall(17670);    dVAR; dSP; dMARK; dTARGET;
    const I32 value = (I32)apply(PL_op->op_type, MARK, SP);

    SP = MARK;
    XPUSHi(value);
    RETURN;
}

PP(pp_chroot)
{
SensorCall(17671);
#ifdef HAS_CHROOT
    dVAR; dSP; dTARGET;
    char * const tmps = POPpx;
    TAINT_PROPER("chroot");
    PUSHi( chroot(tmps) >= 0 );
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "chroot");
#endif
}

PP(pp_rename)
{
SensorCall(17672);    dVAR; dSP; dTARGET;
    int anum;
    const char * const tmps2 = POPpconstx;
    const char * const tmps = SvPV_nolen_const(TOPs);
    TAINT_PROPER("rename");
#ifdef HAS_RENAME
    anum = PerlLIO_rename(tmps, tmps2);
#else
    if (!(anum = PerlLIO_stat(tmps, &PL_statbuf))) {
	if (same_dirent(tmps2, tmps))	/* can always rename to same name */
	    anum = 1;
	else {
	    if (PerlProc_geteuid() || PerlLIO_stat(tmps2, &PL_statbuf) < 0 || !S_ISDIR(PL_statbuf.st_mode))
		(void)UNLINK(tmps2);
	    if (!(anum = link(tmps, tmps2)))
		anum = UNLINK(tmps);
	}
    }
#endif
    SETi( anum >= 0 );
    RETURN;
}

#if defined(HAS_LINK) || defined(HAS_SYMLINK)
PP(pp_link)
{
SensorCall(17673);    dVAR; dSP; dTARGET;
    const int op_type = PL_op->op_type;
    int result;

#  ifndef HAS_LINK
    if (op_type == OP_LINK)
	DIE(aTHX_ PL_no_func, "link");
#  endif
#  ifndef HAS_SYMLINK
    if (op_type == OP_SYMLINK)
	DIE(aTHX_ PL_no_func, "symlink");
#  endif

    {
	const char * const tmps2 = POPpconstx;
	const char * const tmps = SvPV_nolen_const(TOPs);
	TAINT_PROPER(PL_op_desc[op_type]);
	result =
#  if defined(HAS_LINK)
#    if defined(HAS_SYMLINK)
	    /* Both present - need to choose which.  */
	    (op_type == OP_LINK) ?
	    PerlLIO_link(tmps, tmps2) : symlink(tmps, tmps2);
#    else
    /* Only have link, so calls to pp_symlink will have DIE()d above.  */
	PerlLIO_link(tmps, tmps2);
#    endif
#  else
#    if defined(HAS_SYMLINK)
    /* Only have symlink, so calls to pp_link will have DIE()d above.  */
	symlink(tmps, tmps2);
#    endif
#  endif
    }

    SETi( result >= 0 );
    RETURN;
}
#else
PP(pp_link)
{
    /* Have neither.  */
    DIE(aTHX_ PL_no_func, PL_op_desc[PL_op->op_type]);
}
#endif

PP(pp_readlink)
{
SensorCall(17674);    dVAR;
    dSP;
#ifdef HAS_SYMLINK
    dTARGET;
    const char *tmps;
    char buf[MAXPATHLEN];
    int len;

#ifndef INCOMPLETE_TAINTS
    TAINT;
#endif
    tmps = POPpconstx;
    len = readlink(tmps, buf, sizeof(buf) - 1);
    SensorCall(17675);if (len < 0)
	RETPUSHUNDEF;
    PUSHp(buf, len);
    RETURN;
#else
    EXTEND(SP, 1);
    RETSETUNDEF;		/* just pretend it's a normal file */
#endif
}

#if !defined(HAS_MKDIR) || !defined(HAS_RMDIR)
STATIC int
S_dooneliner(pTHX_ const char *cmd, const char *filename)
{
    char * const save_filename = filename;
    char *cmdline;
    char *s;
    PerlIO *myfp;
    int anum = 1;
    Size_t size = strlen(cmd) + (strlen(filename) * 2) + 10;

    PERL_ARGS_ASSERT_DOONELINER;

    Newx(cmdline, size, char);
    my_strlcpy(cmdline, cmd, size);
    my_strlcat(cmdline, " ", size);
    for (s = cmdline + strlen(cmdline); *filename; ) {
	*s++ = '\\';
	*s++ = *filename++;
    }
    if (s - cmdline < size)
	my_strlcpy(s, " 2>&1", size - (s - cmdline));
    myfp = PerlProc_popen(cmdline, "r");
    Safefree(cmdline);

    if (myfp) {
	SV * const tmpsv = sv_newmortal();
	/* Need to save/restore 'PL_rs' ?? */
	s = sv_gets(tmpsv, myfp, 0);
	(void)PerlProc_pclose(myfp);
	if (s != NULL) {
	    int e;
	    for (e = 1;
#ifdef HAS_SYS_ERRLIST
		 e <= sys_nerr
#endif
		 ; e++)
	    {
		/* you don't see this */
		const char * const errmsg =
#ifdef HAS_SYS_ERRLIST
		    sys_errlist[e]
#else
		    strerror(e)
#endif
		    ;
		if (!errmsg)
		    break;
		if (instr(s, errmsg)) {
		    SETERRNO(e,0);
		    return 0;
		}
	    }
	    SETERRNO(0,0);
#ifndef EACCES
#define EACCES EPERM
#endif
	    if (instr(s, "cannot make"))
		SETERRNO(EEXIST,RMS_FEX);
	    else if (instr(s, "existing file"))
		SETERRNO(EEXIST,RMS_FEX);
	    else if (instr(s, "ile exists"))
		SETERRNO(EEXIST,RMS_FEX);
	    else if (instr(s, "non-exist"))
		SETERRNO(ENOENT,RMS_FNF);
	    else if (instr(s, "does not exist"))
		SETERRNO(ENOENT,RMS_FNF);
	    else if (instr(s, "not empty"))
		SETERRNO(EBUSY,SS_DEVOFFLINE);
	    else if (instr(s, "cannot access"))
		SETERRNO(EACCES,RMS_PRV);
	    else
		SETERRNO(EPERM,RMS_PRV);
	    return 0;
	}
	else {	/* some mkdirs return no failure indication */
	    anum = (PerlLIO_stat(save_filename, &PL_statbuf) >= 0);
	    if (PL_op->op_type == OP_RMDIR)
		anum = !anum;
	    if (anum)
		SETERRNO(0,0);
	    else
		SETERRNO(EACCES,RMS_PRV);	/* a guess */
	}
	return anum;
    }
    else
	return 0;
}
#endif

/* This macro removes trailing slashes from a directory name.
 * Different operating and file systems take differently to
 * trailing slashes.  According to POSIX 1003.1 1996 Edition
 * any number of trailing slashes should be allowed.
 * Thusly we snip them away so that even non-conforming
 * systems are happy.
 * We should probably do this "filtering" for all
 * the functions that expect (potentially) directory names:
 * -d, chdir(), chmod(), chown(), chroot(), fcntl()?,
 * (mkdir()), opendir(), rename(), rmdir(), stat(). --jhi */

#define TRIMSLASHES(tmps,len,copy) (tmps) = SvPV_const(TOPs, (len)); \
    if ((len) > 1 && (tmps)[(len)-1] == '/') { \
	do { \
	    (len)--; \
	} while ((len) > 1 && (tmps)[(len)-1] == '/'); \
	(tmps) = savepvn((tmps), (len)); \
	(copy) = TRUE; \
    }

PP(pp_mkdir)
{
SensorCall(17676);    dVAR; dSP; dTARGET;
    STRLEN len;
    const char *tmps;
    bool copy = FALSE;
    const int mode = (MAXARG > 1 && (TOPs||((void)POPs,0))) ? POPi : 0777;

    TRIMSLASHES(tmps,len,copy);

    TAINT_PROPER("mkdir");
#ifdef HAS_MKDIR
    SETi( PerlDir_mkdir(tmps, mode) >= 0 );
#else
    {
    int oldumask;
    SETi( dooneliner("mkdir", tmps) );
    oldumask = PerlLIO_umask(0);
    PerlLIO_umask(oldumask);
    PerlLIO_chmod(tmps, (mode & ~oldumask) & 0777);
    }
#endif
    SensorCall(17677);if (copy)
	Safefree(tmps);
    RETURN;
}

PP(pp_rmdir)
{
SensorCall(17678);    dVAR; dSP; dTARGET;
    STRLEN len;
    const char *tmps;
    bool copy = FALSE;

    TRIMSLASHES(tmps,len,copy);
    TAINT_PROPER("rmdir");
#ifdef HAS_RMDIR
    SETi( PerlDir_rmdir(tmps) >= 0 );
#else
    SETi( dooneliner("rmdir", tmps) );
#endif
    SensorCall(17679);if (copy)
	Safefree(tmps);
    RETURN;
}

/* Directory calls. */

PP(pp_open_dir)
{
SensorCall(17680);
#if defined(Direntry_t) && defined(HAS_READDIR)
    dVAR; dSP;
    const char * const dirname = POPpconstx;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = GvIOn(gv);

    SensorCall(17682);if (!io)
	{/*81*/SensorCall(17681);goto nope;/*82*/}

    SensorCall(17684);if ((IoIFP(io) || IoOFP(io)))
	{/*83*/SensorCall(17683);Perl_ck_warner_d(aTHX_ packWARN2(WARN_IO, WARN_DEPRECATED),
			 "Opening filehandle %"HEKf" also as a directory",
			     HEKfARG(GvENAME_HEK(gv)) );/*84*/}
    SensorCall(17685);if (IoDIRP(io))
	PerlDir_close(IoDIRP(io));
    SensorCall(17687);if (!(IoDIRP(io) = PerlDir_open(dirname)))
	{/*85*/SensorCall(17686);goto nope;/*86*/}

    RETPUSHYES;
nope:
    SensorCall(17688);if (!errno)
	SETERRNO(EBADF,RMS_DIR);
    RETPUSHUNDEF;
#else
    DIE(aTHX_ PL_no_dir_func, "opendir");
#endif
}

PP(pp_readdir)
{
SensorCall(17689);
#if !defined(Direntry_t) || !defined(HAS_READDIR)
    DIE(aTHX_ PL_no_dir_func, "readdir");
#else
#if !defined(I_DIRENT) && !defined(VMS)
    Direntry_t *readdir (DIR *);
#endif
    dVAR;
    dSP;

    SV *sv;
    const I32 gimme = GIMME;
    GV * const gv = MUTABLE_GV(POPs);
    register const Direntry_t *dp;
    register IO * const io = GvIOn(gv);

    SensorCall(17692);if (!io || !IoDIRP(io)) {
	SensorCall(17690);Perl_ck_warner(aTHX_ packWARN(WARN_IO),
		       "readdir() attempted on invalid dirhandle %"HEKf,
                            HEKfARG(GvENAME_HEK(gv)));
        SensorCall(17691);goto nope;
    }

    SensorCall(17698);do {
        SensorCall(17693);dp = (Direntry_t *)PerlDir_read(IoDIRP(io));
        SensorCall(17695);if (!dp)
            {/*99*/SensorCall(17694);break;/*100*/}
#ifdef DIRNAMLEN
        sv = newSVpvn(dp->d_name, dp->d_namlen);
#else
        SensorCall(17696);sv = newSVpv(dp->d_name, 0);
#endif
#ifndef INCOMPLETE_TAINTS
        SensorCall(17697);if (!(IoFLAGS(io) & IOf_UNTAINT))
            SvTAINTED_on(sv);
#endif
        mXPUSHs(sv);
    } while (gimme == G_ARRAY);

    SensorCall(17700);if (!dp && gimme != G_ARRAY)
        {/*101*/SensorCall(17699);goto nope;/*102*/}

    RETURN;

nope:
    SensorCall(17701);if (!errno)
	SETERRNO(EBADF,RMS_ISI);
    SensorCall(17702);if (GIMME == G_ARRAY)
	RETURN;
    else
	RETPUSHUNDEF;
#endif
SensorCall(17703);}

PP(pp_telldir)
{
SensorCall(17704);
#if defined(HAS_TELLDIR) || defined(telldir)
    dVAR; dSP; dTARGET;
 /* XXX does _anyone_ need this? --AD 2/20/1998 */
 /* XXX netbsd still seemed to.
    XXX HAS_TELLDIR_PROTO is new style, NEED_TELLDIR_PROTO is old style.
    --JHI 1999-Feb-02 */
# if !defined(HAS_TELLDIR_PROTO) || defined(NEED_TELLDIR_PROTO)
    long telldir (DIR *);
# endif
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = GvIOn(gv);

    SensorCall(17707);if (!io || !IoDIRP(io)) {
	SensorCall(17705);Perl_ck_warner(aTHX_ packWARN(WARN_IO),
		       "telldir() attempted on invalid dirhandle %"HEKf,
                            HEKfARG(GvENAME_HEK(gv)));
        SensorCall(17706);goto nope;
    }

    PUSHi( PerlDir_tell(IoDIRP(io)) );
    RETURN;
nope:
    SensorCall(17708);if (!errno)
	SETERRNO(EBADF,RMS_ISI);
    RETPUSHUNDEF;
#else
    DIE(aTHX_ PL_no_dir_func, "telldir");
#endif
}

PP(pp_seekdir)
{
SensorCall(17709);
#if defined(HAS_SEEKDIR) || defined(seekdir)
    dVAR; dSP;
    const long along = POPl;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = GvIOn(gv);

    SensorCall(17712);if (!io || !IoDIRP(io)) {
	SensorCall(17710);Perl_ck_warner(aTHX_ packWARN(WARN_IO),
		       "seekdir() attempted on invalid dirhandle %"HEKf,
                                HEKfARG(GvENAME_HEK(gv)));
        SensorCall(17711);goto nope;
    }
    SensorCall(17713);(void)PerlDir_seek(IoDIRP(io), along);

    RETPUSHYES;
nope:
    SensorCall(17714);if (!errno)
	SETERRNO(EBADF,RMS_ISI);
    RETPUSHUNDEF;
#else
    DIE(aTHX_ PL_no_dir_func, "seekdir");
#endif
}

PP(pp_rewinddir)
{
SensorCall(17715);
#if defined(HAS_REWINDDIR) || defined(rewinddir)
    dVAR; dSP;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = GvIOn(gv);

    SensorCall(17718);if (!io || !IoDIRP(io)) {
	SensorCall(17716);Perl_ck_warner(aTHX_ packWARN(WARN_IO),
		       "rewinddir() attempted on invalid dirhandle %"HEKf,
                                HEKfARG(GvENAME_HEK(gv)));
	SensorCall(17717);goto nope;
    }
    SensorCall(17719);(void)PerlDir_rewind(IoDIRP(io));
    RETPUSHYES;
nope:
    SensorCall(17720);if (!errno)
	SETERRNO(EBADF,RMS_ISI);
    RETPUSHUNDEF;
#else
    DIE(aTHX_ PL_no_dir_func, "rewinddir");
#endif
}

PP(pp_closedir)
{
SensorCall(17721);
#if defined(Direntry_t) && defined(HAS_READDIR)
    dVAR; dSP;
    GV * const gv = MUTABLE_GV(POPs);
    register IO * const io = GvIOn(gv);

    SensorCall(17724);if (!io || !IoDIRP(io)) {
	SensorCall(17722);Perl_ck_warner(aTHX_ packWARN(WARN_IO),
		       "closedir() attempted on invalid dirhandle %"HEKf,
                                HEKfARG(GvENAME_HEK(gv)));
        SensorCall(17723);goto nope;
    }
#ifdef VOID_CLOSEDIR
    PerlDir_close(IoDIRP(io));
#else
    SensorCall(17726);if (PerlDir_close(IoDIRP(io)) < 0) {
	IoDIRP(io) = 0; /* Don't try to close again--coredumps on SysV */
	SensorCall(17725);goto nope;
    }
#endif
    IoDIRP(io) = 0;

    RETPUSHYES;
nope:
    SensorCall(17727);if (!errno)
	SETERRNO(EBADF,RMS_IFI);
    RETPUSHUNDEF;
#else
    DIE(aTHX_ PL_no_dir_func, "closedir");
#endif
}

/* Process control. */

PP(pp_fork)
{
SensorCall(17728);
#ifdef HAS_FORK
    dVAR; dSP; dTARGET;
    Pid_t childpid;

    EXTEND(SP, 1);
    PERL_FLUSHALL_FOR_CHILD;
    childpid = PerlProc_fork();
    SensorCall(17729);if (childpid < 0)
	RETSETUNDEF;
    SensorCall(17730);if (!childpid) {
#ifdef PERL_USES_PL_PIDSTATUS
	hv_clear(PL_pidstatus);	/* no kids, so don't wait for 'em */
#endif
    }
    PUSHi(childpid);
    RETURN;
#else
#  if defined(USE_ITHREADS) && defined(PERL_IMPLICIT_SYS)
    dSP; dTARGET;
    Pid_t childpid;

    EXTEND(SP, 1);
    PERL_FLUSHALL_FOR_CHILD;
    childpid = PerlProc_fork();
    if (childpid == -1)
	RETSETUNDEF;
    PUSHi(childpid);
    RETURN;
#  else
    DIE(aTHX_ PL_no_func, "fork");
#  endif
#endif
}

PP(pp_wait)
{
SensorCall(17731);
#if (!defined(DOSISH) || defined(OS2) || defined(WIN32)) && !defined(__LIBCATAMOUNT__)
    dVAR; dSP; dTARGET;
    Pid_t childpid;
    int argflags;

    SensorCall(17733);if (PL_signals & PERL_SIGNALS_UNSAFE_FLAG)
        childpid = wait4pid(-1, &argflags, 0);
    else {
        SensorCall(17732);while ((childpid = wait4pid(-1, &argflags, 0)) == -1 &&
	       errno == EINTR) {
	  PERL_ASYNC_CHECK();
	}
    }
#  if defined(USE_ITHREADS) && defined(PERL_IMPLICIT_SYS)
    /* 0 and -1 are both error returns (the former applies to WNOHANG case) */
    STATUS_NATIVE_CHILD_SET((childpid && childpid != -1) ? argflags : -1);
#  else
    STATUS_NATIVE_CHILD_SET((childpid > 0) ? argflags : -1);
#  endif
    XPUSHi(childpid);
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "wait");
#endif
}

PP(pp_waitpid)
{
SensorCall(17734);
#if (!defined(DOSISH) || defined(OS2) || defined(WIN32)) && !defined(__LIBCATAMOUNT__)
    dVAR; dSP; dTARGET;
    const int optype = POPi;
    const Pid_t pid = TOPi;
    Pid_t result;
    int argflags;

    SensorCall(17736);if (PL_signals & PERL_SIGNALS_UNSAFE_FLAG)
        result = wait4pid(pid, &argflags, optype);
    else {
        SensorCall(17735);while ((result = wait4pid(pid, &argflags, optype)) == -1 &&
	       errno == EINTR) {
	  PERL_ASYNC_CHECK();
	}
    }
#  if defined(USE_ITHREADS) && defined(PERL_IMPLICIT_SYS)
    /* 0 and -1 are both error returns (the former applies to WNOHANG case) */
    STATUS_NATIVE_CHILD_SET((result && result != -1) ? argflags : -1);
#  else
    STATUS_NATIVE_CHILD_SET((result > 0) ? argflags : -1);
#  endif
    SETi(result);
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "waitpid");
#endif
}

PP(pp_system)
{
SensorCall(17737);    dVAR; dSP; dMARK; dORIGMARK; dTARGET;
#if defined(__LIBCATAMOUNT__)
    PL_statusvalue = -1;
    SP = ORIGMARK;
    XPUSHi(-1);
#else
    I32 value;
    int result;

    SensorCall(17742);if (PL_tainting) {
	TAINT_ENV();
	SensorCall(17741);while (++MARK <= SP) {
	    SensorCall(17738);(void)SvPV_nolen_const(*MARK);      /* stringify for taint check */
	    SensorCall(17740);if (PL_tainted)
		{/*149*/SensorCall(17739);break;/*150*/}
	}
	MARK = ORIGMARK;
	TAINT_PROPER("system");
    }
    PERL_FLUSHALL_FOR_CHILD;
#if (defined(HAS_FORK) || defined(AMIGAOS)) && !defined(VMS) && !defined(OS2) || defined(PERL_MICRO)
    {
	Pid_t childpid;
	SensorCall(17743);int pp[2];
	I32 did_pipes = 0;
#if (defined(HAS_SIGPROCMASK) && !defined(PERL_MICRO))
	sigset_t newset, oldset;
#endif

	SensorCall(17745);if (PerlProc_pipe(pp) >= 0)
	    {/*151*/SensorCall(17744);did_pipes = 1;/*152*/}
#if (defined(HAS_SIGPROCMASK) && !defined(PERL_MICRO))
	SensorCall(17746);sigemptyset(&newset);
	sigaddset(&newset, SIGCHLD);
	sigprocmask(SIG_BLOCK, &newset, &oldset);
#endif
	SensorCall(17752);while ((childpid = PerlProc_fork()) == -1) {
	    SensorCall(17747);if (errno != EAGAIN) {
		SensorCall(17748);value = -1;
		SP = ORIGMARK;
		XPUSHi(value);
		SensorCall(17749);if (did_pipes) {
		    PerlLIO_close(pp[0]);
		    PerlLIO_close(pp[1]);
		}
#if (defined(HAS_SIGPROCMASK) && !defined(PERL_MICRO))
		SensorCall(17750);sigprocmask(SIG_SETMASK, &oldset, NULL);
#endif
		RETURN;
	    }
	    SensorCall(17751);sleep(5);
	}
	SensorCall(17767);if (childpid > 0) {
	    SensorCall(17753);Sigsave_t ihand,qhand; /* place to save signals during system() */
	    int status;

	    SensorCall(17754);if (did_pipes)
		PerlLIO_close(pp[1]);
#ifndef PERL_MICRO
	    rsignal_save(SIGINT,  (Sighandler_t) SIG_IGN, &ihand);
	    rsignal_save(SIGQUIT, (Sighandler_t) SIG_IGN, &qhand);
#endif
	    SensorCall(17756);do {
		SensorCall(17755);result = wait4pid(childpid, &status, 0);
	    } while (result == -1 && errno == EINTR);
#ifndef PERL_MICRO
#ifdef HAS_SIGPROCMASK
	    SensorCall(17757);sigprocmask(SIG_SETMASK, &oldset, NULL);
#endif
	    (void)rsignal_restore(SIGINT, &ihand);
	    (void)rsignal_restore(SIGQUIT, &qhand);
#endif
	    STATUS_NATIVE_CHILD_SET(result == -1 ? -1 : status);
	    do_execfree();	/* free any memory child malloced on fork */
	    SP = ORIGMARK;
	    SensorCall(17766);if (did_pipes) {
		SensorCall(17758);int errkid;
		unsigned n = 0;
		SSize_t n1;

		SensorCall(17763);while (n < sizeof(int)) {
		    SensorCall(17759);n1 = PerlLIO_read(pp[0],
				      (void*)(((char*)&errkid)+n),
				      (sizeof(int)) - n);
		    SensorCall(17761);if (n1 <= 0)
			{/*153*/SensorCall(17760);break;/*154*/}
		    SensorCall(17762);n += n1;
		}
		PerlLIO_close(pp[0]);
		SensorCall(17765);if (n) {			/* Error */
		    SensorCall(17764);if (n != sizeof(int))
			DIE(aTHX_ "panic: kid popen errno read, n=%u", n);
		    errno = errkid;		/* Propagate errno from kid */
		    STATUS_NATIVE_CHILD_SET(-1);
		}
	    }
	    XPUSHi(STATUS_CURRENT);
	    RETURN;
	}
#if (defined(HAS_SIGPROCMASK) && !defined(PERL_MICRO))
	SensorCall(17768);sigprocmask(SIG_SETMASK, &oldset, NULL);
#endif
	SensorCall(17770);if (did_pipes) {
	    PerlLIO_close(pp[0]);
#if defined(HAS_FCNTL) && defined(F_SETFD)
	    SensorCall(17769);fcntl(pp[1], F_SETFD, FD_CLOEXEC);
#endif
	}
	SensorCall(17774);if (PL_op->op_flags & OPf_STACKED) {
	    SensorCall(17771);SV * const really = *++MARK;
	    value = (I32)do_aexec5(really, MARK, SP, pp[1], did_pipes);
	}
	else {/*155*/SensorCall(17772);if (SP - MARK != 1)
	    value = (I32)do_aexec5(NULL, MARK, SP, pp[1], did_pipes);
	else {
	    SensorCall(17773);value = (I32)do_exec3(SvPVx_nolen(sv_mortalcopy(*SP)), pp[1], did_pipes);
	;/*156*/}}
	PerlProc__exit(-1);
    }
#else /* ! FORK or VMS or OS/2 */
    PL_statusvalue = 0;
    result = 0;
    if (PL_op->op_flags & OPf_STACKED) {
	SV * const really = *++MARK;
#  if defined(WIN32) || defined(OS2) || defined(__SYMBIAN32__) || defined(__VMS)
	value = (I32)do_aspawn(really, MARK, SP);
#  else
	value = (I32)do_aspawn(really, (void **)MARK, (void **)SP);
#  endif
    }
    else if (SP - MARK != 1) {
#  if defined(WIN32) || defined(OS2) || defined(__SYMBIAN32__) || defined(__VMS)
	value = (I32)do_aspawn(NULL, MARK, SP);
#  else
	value = (I32)do_aspawn(NULL, (void **)MARK, (void **)SP);
#  endif
    }
    else {
	value = (I32)do_spawn(SvPVx_nolen(sv_mortalcopy(*SP)));
    }
    if (PL_statusvalue == -1)	/* hint that value must be returned as is */
	result = 1;
    STATUS_NATIVE_CHILD_SET(value);
    do_execfree();
    SP = ORIGMARK;
    XPUSHi(result ? value : STATUS_CURRENT);
#endif /* !FORK or VMS or OS/2 */
#endif
    RETURN;
}

PP(pp_exec)
{
SensorCall(17775);    dVAR; dSP; dMARK; dORIGMARK; dTARGET;
    I32 value;

    SensorCall(17780);if (PL_tainting) {
	TAINT_ENV();
	SensorCall(17779);while (++MARK <= SP) {
	    SensorCall(17776);(void)SvPV_nolen_const(*MARK);      /* stringify for taint check */
	    SensorCall(17778);if (PL_tainted)
		{/*35*/SensorCall(17777);break;/*36*/}
	}
	MARK = ORIGMARK;
	TAINT_PROPER("exec");
    }
    PERL_FLUSHALL_FOR_CHILD;
    SensorCall(17784);if (PL_op->op_flags & OPf_STACKED) {
	SensorCall(17781);SV * const really = *++MARK;
	value = (I32)do_aexec(really, MARK, SP);
    }
    else {/*37*/SensorCall(17782);if (SP - MARK != 1)
#ifdef VMS
	value = (I32)vms_do_aexec(NULL, MARK, SP);
#else
#  ifdef __OPEN_VM
	{
	   (void ) do_aspawn(NULL, MARK, SP);
	   value = 0;
	}
#  else
	value = (I32)do_aexec(NULL, MARK, SP);
#  endif
#endif
    else {
#ifdef VMS
	value = (I32)vms_do_exec(SvPVx_nolen(sv_mortalcopy(*SP)));
#else
#  ifdef __OPEN_VM
	(void) do_spawn(SvPVx_nolen(sv_mortalcopy(*SP)));
	value = 0;
#  else
	SensorCall(17783);value = (I32)do_exec(SvPVx_nolen(sv_mortalcopy(*SP)));
#  endif
#endif
    ;/*38*/}}

    SP = ORIGMARK;
    XPUSHi(value);
    RETURN;
}

PP(pp_getppid)
{
SensorCall(17785);
#ifdef HAS_GETPPID
    dVAR; dSP; dTARGET;
    XPUSHi( getppid() );
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "getppid");
#endif
}

PP(pp_getpgrp)
{
SensorCall(17786);
#ifdef HAS_GETPGRP
    dVAR; dSP; dTARGET;
    Pid_t pgrp;
    const Pid_t pid =
	(MAXARG < 1) ? 0 : TOPs ? SvIVx(POPs) : ((void)POPs, 0);

#ifdef BSD_GETPGRP
    pgrp = (I32)BSD_GETPGRP(pid);
#else
    if (pid != 0 && pid != PerlProc_getpid())
	DIE(aTHX_ "POSIX getpgrp can't take an argument");
    pgrp = getpgrp();
#endif
    XPUSHi(pgrp);
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "getpgrp()");
#endif
}

PP(pp_setpgrp)
{
SensorCall(17787);
#ifdef HAS_SETPGRP
    dVAR; dSP; dTARGET;
    Pid_t pgrp;
    Pid_t pid;
    pgrp = MAXARG == 2 && (TOPs||POPs) ? POPi : 0;
    SensorCall(17789);if (MAXARG > 0) pid = TOPs && TOPi;
    else {
	SensorCall(17788);pid = 0;
	XPUSHi(-1);
    }

    TAINT_PROPER("setpgrp");
#ifdef BSD_SETPGRP
    SETi( BSD_SETPGRP(pid, pgrp) >= 0 );
#else
    if ((pgrp != 0 && pgrp != PerlProc_getpid())
	|| (pid != 0 && pid != PerlProc_getpid()))
    {
	DIE(aTHX_ "setpgrp can't take arguments");
    }
    SETi( setpgrp() >= 0 );
#endif /* USE_BSDPGRP */
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "setpgrp()");
#endif
}

#if defined(__GLIBC__) && ((__GLIBC__ == 2 && __GLIBC_MINOR__ >= 3) || (__GLIBC__ > 2))
#  define PRIORITY_WHICH_T(which) (__priority_which_t)which
#else
#  define PRIORITY_WHICH_T(which) which
#endif

PP(pp_getpriority)
{
SensorCall(17790);
#ifdef HAS_GETPRIORITY
    dVAR; dSP; dTARGET;
    const int who = POPi;
    const int which = TOPi;
    SETi( getpriority(PRIORITY_WHICH_T(which), who) );
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "getpriority()");
#endif
}

PP(pp_setpriority)
{
SensorCall(17791);
#ifdef HAS_SETPRIORITY
    dVAR; dSP; dTARGET;
    const int niceval = POPi;
    const int who = POPi;
    const int which = TOPi;
    TAINT_PROPER("setpriority");
    SETi( setpriority(PRIORITY_WHICH_T(which), who, niceval) >= 0 );
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "setpriority()");
#endif
}

#undef PRIORITY_WHICH_T

/* Time calls. */

PP(pp_time)
{
SensorCall(17792);    dVAR; dSP; dTARGET;
#ifdef BIG_TIME
    XPUSHn( time(NULL) );
#else
    XPUSHi( time(NULL) );
#endif
    RETURN;
}

PP(pp_tms)
{
SensorCall(17793);
#ifdef HAS_TIMES
    dVAR;
    dSP;
    EXTEND(SP, 4);
#ifndef VMS
    (void)PerlProc_times(&PL_timesbuf);
#else
    (void)PerlProc_times((tbuffer_t *)&PL_timesbuf);  /* time.h uses different name for */
                                                   /* struct tms, though same data   */
                                                   /* is returned.                   */
#endif

    mPUSHn(((NV)PL_timesbuf.tms_utime)/(NV)PL_clocktick);
    SensorCall(17794);if (GIMME == G_ARRAY) {
	mPUSHn(((NV)PL_timesbuf.tms_stime)/(NV)PL_clocktick);
	mPUSHn(((NV)PL_timesbuf.tms_cutime)/(NV)PL_clocktick);
	mPUSHn(((NV)PL_timesbuf.tms_cstime)/(NV)PL_clocktick);
    }
    RETURN;
#else
#   ifdef PERL_MICRO
    dSP;
    mPUSHn(0.0);
    EXTEND(SP, 4);
    if (GIMME == G_ARRAY) {
	 mPUSHn(0.0);
	 mPUSHn(0.0);
	 mPUSHn(0.0);
    }
    RETURN;
#   else
    DIE(aTHX_ "times not implemented");
#   endif
#endif /* HAS_TIMES */
}

/* The 32 bit int year limits the times we can represent to these
   boundaries with a few days wiggle room to account for time zone
   offsets
*/
/* Sat Jan  3 00:00:00 -2147481748 */
#define TIME_LOWER_BOUND -67768100567755200.0
/* Sun Dec 29 12:00:00  2147483647 */
#define TIME_UPPER_BOUND  67767976233316800.0

PP(pp_gmtime)
{
SensorCall(17795);    dVAR;
    dSP;
    Time64_T when;
    struct TM tmbuf;
    struct TM *err;
    const char *opname = PL_op->op_type == OP_LOCALTIME ? "localtime" : "gmtime";
    static const char * const dayname[] =
	{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    static const char * const monname[] =
	{"Jan", "Feb", "Mar", "Apr", "May", "Jun",
	 "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    SensorCall(17800);if (MAXARG < 1 || (!TOPs && ((void)POPs, 1))) {
	SensorCall(17796);time_t now;
	(void)time(&now);
	when = (Time64_T)now;
    }
    else {
	SensorCall(17797);NV input = Perl_floor(POPn);
	when = (Time64_T)input;
	SensorCall(17799);if (when != input) {
	    /* diag_listed_as: gmtime(%f) too large */
	    SensorCall(17798);Perl_ck_warner(aTHX_ packWARN(WARN_OVERFLOW),
			   "%s(%.0" NVff ") too large", opname, input);
	}
    }

    SensorCall(17807);if ( TIME_LOWER_BOUND > when ) {
	/* diag_listed_as: gmtime(%f) too small */
	SensorCall(17801);Perl_ck_warner(aTHX_ packWARN(WARN_OVERFLOW),
		       "%s(%.0" NVff ") too small", opname, when);
	err = NULL;
    }
    else {/*63*/SensorCall(17802);if( when > TIME_UPPER_BOUND ) {
	/* diag_listed_as: gmtime(%f) too small */
	SensorCall(17803);Perl_ck_warner(aTHX_ packWARN(WARN_OVERFLOW),
		       "%s(%.0" NVff ") too large", opname, when);
	err = NULL;
    }
    else {
	SensorCall(17804);if (PL_op->op_type == OP_LOCALTIME)
	    {/*65*/SensorCall(17805);err = S_localtime64_r(&when, &tmbuf);/*66*/}
	else
	    {/*67*/SensorCall(17806);err = S_gmtime64_r(&when, &tmbuf);/*68*/}
    ;/*64*/}}

    SensorCall(17809);if (err == NULL) {
	/* XXX %lld broken for quads */
	SensorCall(17808);Perl_ck_warner(aTHX_ packWARN(WARN_OVERFLOW),
		       "%s(%.0" NVff ") failed", opname, when);
    }

    SensorCall(17814);if (GIMME != G_ARRAY) {	/* scalar context */
	SensorCall(17810);SV *tsv;
	/* XXX newSVpvf()'s %lld type is broken, so cheat with a double */
	double year = (double)tmbuf.tm_year + 1900;

        EXTEND(SP, 1);
        EXTEND_MORTAL(1);
	SensorCall(17811);if (err == NULL)
	    RETPUSHUNDEF;

	SensorCall(17812);tsv = Perl_newSVpvf(aTHX_ "%s %s %2d %02d:%02d:%02d %.0f",
			    dayname[tmbuf.tm_wday],
			    monname[tmbuf.tm_mon],
			    tmbuf.tm_mday,
			    tmbuf.tm_hour,
			    tmbuf.tm_min,
			    tmbuf.tm_sec,
			    year);
	mPUSHs(tsv);
    }
    else {			/* list context */
	SensorCall(17813);if ( err == NULL )
	    RETURN;

        EXTEND(SP, 9);
        EXTEND_MORTAL(9);
        mPUSHi(tmbuf.tm_sec);
	mPUSHi(tmbuf.tm_min);
	mPUSHi(tmbuf.tm_hour);
	mPUSHi(tmbuf.tm_mday);
	mPUSHi(tmbuf.tm_mon);
	mPUSHn(tmbuf.tm_year);
	mPUSHi(tmbuf.tm_wday);
	mPUSHi(tmbuf.tm_yday);
	mPUSHi(tmbuf.tm_isdst);
    }
    RETURN;
}

PP(pp_alarm)
{
SensorCall(17815);
#ifdef HAS_ALARM
    dVAR; dSP; dTARGET;
    int anum;
    anum = POPi;
    anum = alarm((unsigned int)anum);
    SensorCall(17816);if (anum < 0)
	RETPUSHUNDEF;
    PUSHi(anum);
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "alarm");
#endif
}

PP(pp_sleep)
{
SensorCall(17817);    dVAR; dSP; dTARGET;
    I32 duration;
    Time_t lasttime;
    Time_t when;

    (void)time(&lasttime);
    SensorCall(17819);if (MAXARG < 1 || (!TOPs && !POPs))
	PerlProc_pause();
    else {
	SensorCall(17818);duration = POPi;
	PerlProc_sleep((unsigned int)duration);
    }
    SensorCall(17820);(void)time(&when);
    XPUSHi(when - lasttime);
    RETURN;
}

/* Shared memory. */
/* Merged with some message passing. */

PP(pp_shmwrite)
{
SensorCall(17821);
#if defined(HAS_MSG) || defined(HAS_SEM) || defined(HAS_SHM)
    dVAR; dSP; dMARK; dTARGET;
    const int op_type = PL_op->op_type;
    I32 value;

    SensorCall(17830);switch (op_type) {
    case OP_MSGSND:
	SensorCall(17822);value = (I32)(do_msgsnd(MARK, SP) >= 0);
	SensorCall(17823);break;
    case OP_MSGRCV:
	SensorCall(17824);value = (I32)(do_msgrcv(MARK, SP) >= 0);
	SensorCall(17825);break;
    case OP_SEMOP:
	SensorCall(17826);value = (I32)(do_semop(MARK, SP) >= 0);
	SensorCall(17827);break;
    default:
	SensorCall(17828);value = (I32)(do_shmio(op_type, MARK, SP) >= 0);
	SensorCall(17829);break;
    }

    SP = MARK;
    PUSHi(value);
    RETURN;
#else
    return Perl_pp_semget(aTHX);
#endif
}

/* Semaphores. */

PP(pp_semget)
{
SensorCall(17831);
#if defined(HAS_MSG) || defined(HAS_SEM) || defined(HAS_SHM)
    dVAR; dSP; dMARK; dTARGET;
    const int anum = do_ipcget(PL_op->op_type, MARK, SP);
    SP = MARK;
    SensorCall(17832);if (anum == -1)
	RETPUSHUNDEF;
    PUSHi(anum);
    RETURN;
#else
    DIE(aTHX_ "System V IPC is not implemented on this machine");
#endif
}

PP(pp_semctl)
{
SensorCall(17833);
#if defined(HAS_MSG) || defined(HAS_SEM) || defined(HAS_SHM)
    dVAR; dSP; dMARK; dTARGET;
    const int anum = do_ipcctl(PL_op->op_type, MARK, SP);
    SP = MARK;
    SensorCall(17834);if (anum == -1)
	RETSETUNDEF;
    SensorCall(17835);if (anum != 0) {
	PUSHi(anum);
    }
    else {
	PUSHp(zero_but_true, ZBTLEN);
    }
    RETURN;
#else
    return Perl_pp_semget(aTHX);
#endif
}

/* I can't const this further without getting warnings about the types of
   various arrays passed in from structures.  */
static SV *
S_space_join_names_mortal(pTHX_ char *const *array)
{
    SensorCall(17836);SV *target;

    PERL_ARGS_ASSERT_SPACE_JOIN_NAMES_MORTAL;

    SensorCall(17842);if (array && *array) {
	SensorCall(17837);target = newSVpvs_flags("", SVs_TEMP);
	SensorCall(17840);while (1) {
	    sv_catpv(target, *array);
	    SensorCall(17839);if (!*++array)
		{/*3*/SensorCall(17838);break;/*4*/}
	    sv_catpvs(target, " ");
	}
    } else {
	SensorCall(17841);target = sv_mortalcopy(&PL_sv_no);
    }
    {SV * ReplaceReturn1270 = target; SensorCall(17843); return ReplaceReturn1270;}
}

/* Get system info. */

PP(pp_ghostent)
{
SensorCall(17844);
#if defined(HAS_GETHOSTBYNAME) || defined(HAS_GETHOSTBYADDR) || defined(HAS_GETHOSTENT)
    dVAR; dSP;
    I32 which = PL_op->op_type;
    register char **elem;
    register SV *sv;
#ifndef HAS_GETHOST_PROTOS /* XXX Do we need individual probes? */
    struct hostent *gethostbyaddr(Netdb_host_t, Netdb_hlen_t, int);
    struct hostent *gethostbyname(Netdb_name_t);
    struct hostent *gethostent(void);
#endif
    struct hostent *hent = NULL;
    unsigned long len;

    EXTEND(SP, 10);
    SensorCall(17846);if (which == OP_GHBYNAME) {
#ifdef HAS_GETHOSTBYNAME
	SensorCall(17845);const char* const name = POPpbytex;
	hent = PerlSock_gethostbyname(name);
#else
	DIE(aTHX_ PL_no_sock_func, "gethostbyname");
#endif
    }
    else if (which == OP_GHBYADDR) {
#ifdef HAS_GETHOSTBYADDR
	const int addrtype = POPi;
	SV * const addrsv = POPs;
	STRLEN addrlen;
	const char *addr = (char *)SvPVbyte(addrsv, addrlen);

	hent = PerlSock_gethostbyaddr(addr, (Netdb_hlen_t) addrlen, addrtype);
#else
	DIE(aTHX_ PL_no_sock_func, "gethostbyaddr");
#endif
    }
    else
#ifdef HAS_GETHOSTENT
	hent = PerlSock_gethostent();
#else
	DIE(aTHX_ PL_no_sock_func, "gethostent");
#endif

#ifdef HOST_NOT_FOUND
	SensorCall(17847);if (!hent) {
#ifdef USE_REENTRANT_API
#   ifdef USE_GETHOSTENT_ERRNO
	    h_errno = PL_reentrant_buffer->_gethostent_errno;
#   endif
#endif
	    STATUS_UNIX_SET(h_errno);
	}
#endif

    SensorCall(17851);if (GIMME != G_ARRAY) {
	PUSHs(sv = sv_newmortal());
	SensorCall(17850);if (hent) {
	    SensorCall(17848);if (which == OP_GHBYNAME) {
		SensorCall(17849);if (hent->h_addr)
		    sv_setpvn(sv, hent->h_addr, hent->h_length);
	    }
	    else
		sv_setpv(sv, (char*)hent->h_name);
	}
	RETURN;
    }

    SensorCall(17854);if (hent) {
	mPUSHs(newSVpv((char*)hent->h_name, 0));
	PUSHs(space_join_names_mortal(hent->h_aliases));
	mPUSHi(hent->h_addrtype);
	SensorCall(17852);len = hent->h_length;
	mPUSHi(len);
#ifdef h_addr
	SensorCall(17853);for (elem = hent->h_addr_list; elem && *elem; elem++) {
	    mXPUSHp(*elem, len);
	}
#else
	if (hent->h_addr)
	    mPUSHp(hent->h_addr, len);
	else
	    PUSHs(sv_mortalcopy(&PL_sv_no));
#endif /* h_addr */
    }
    RETURN;
#else
    DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
}

PP(pp_gnetent)
{
SensorCall(17855);
#if defined(HAS_GETNETBYNAME) || defined(HAS_GETNETBYADDR) || defined(HAS_GETNETENT)
    dVAR; dSP;
    I32 which = PL_op->op_type;
    register SV *sv;
#ifndef HAS_GETNET_PROTOS /* XXX Do we need individual probes? */
    struct netent *getnetbyaddr(Netdb_net_t, int);
    struct netent *getnetbyname(Netdb_name_t);
    struct netent *getnetent(void);
#endif
    struct netent *nent;

    SensorCall(17857);if (which == OP_GNBYNAME){
#ifdef HAS_GETNETBYNAME
	SensorCall(17856);const char * const name = POPpbytex;
	nent = PerlSock_getnetbyname(name);
#else
        DIE(aTHX_ PL_no_sock_func, "getnetbyname");
#endif
    }
    else if (which == OP_GNBYADDR) {
#ifdef HAS_GETNETBYADDR
	const int addrtype = POPi;
	const Netdb_net_t addr = (Netdb_net_t) (U32)POPu;
	nent = PerlSock_getnetbyaddr(addr, addrtype);
#else
	DIE(aTHX_ PL_no_sock_func, "getnetbyaddr");
#endif
    }
    else
#ifdef HAS_GETNETENT
	nent = PerlSock_getnetent();
#else
        DIE(aTHX_ PL_no_sock_func, "getnetent");
#endif

#ifdef HOST_NOT_FOUND
	SensorCall(17858);if (!nent) {
#ifdef USE_REENTRANT_API
#   ifdef USE_GETNETENT_ERRNO
	     h_errno = PL_reentrant_buffer->_getnetent_errno;
#   endif
#endif
	    STATUS_UNIX_SET(h_errno);
	}
#endif

    EXTEND(SP, 4);
    SensorCall(17861);if (GIMME != G_ARRAY) {
	PUSHs(sv = sv_newmortal());
	SensorCall(17860);if (nent) {
	    SensorCall(17859);if (which == OP_GNBYNAME)
		sv_setiv(sv, (IV)nent->n_net);
	    else
		sv_setpv(sv, nent->n_name);
	}
	RETURN;
    }

    SensorCall(17862);if (nent) {
	mPUSHs(newSVpv(nent->n_name, 0));
	PUSHs(space_join_names_mortal(nent->n_aliases));
	mPUSHi(nent->n_addrtype);
	mPUSHi(nent->n_net);
    }

    RETURN;
#else
    DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
}

PP(pp_gprotoent)
{
SensorCall(17863);
#if defined(HAS_GETPROTOBYNAME) || defined(HAS_GETPROTOBYNUMBER) || defined(HAS_GETPROTOENT)
    dVAR; dSP;
    I32 which = PL_op->op_type;
    register SV *sv;
#ifndef HAS_GETPROTO_PROTOS /* XXX Do we need individual probes? */
    struct protoent *getprotobyname(Netdb_name_t);
    struct protoent *getprotobynumber(int);
    struct protoent *getprotoent(void);
#endif
    struct protoent *pent;

    SensorCall(17865);if (which == OP_GPBYNAME) {
#ifdef HAS_GETPROTOBYNAME
	SensorCall(17864);const char* const name = POPpbytex;
	pent = PerlSock_getprotobyname(name);
#else
	DIE(aTHX_ PL_no_sock_func, "getprotobyname");
#endif
    }
    else if (which == OP_GPBYNUMBER) {
#ifdef HAS_GETPROTOBYNUMBER
	const int number = POPi;
	pent = PerlSock_getprotobynumber(number);
#else
	DIE(aTHX_ PL_no_sock_func, "getprotobynumber");
#endif
    }
    else
#ifdef HAS_GETPROTOENT
	pent = PerlSock_getprotoent();
#else
	DIE(aTHX_ PL_no_sock_func, "getprotoent");
#endif

    EXTEND(SP, 3);
    SensorCall(17868);if (GIMME != G_ARRAY) {
	PUSHs(sv = sv_newmortal());
	SensorCall(17867);if (pent) {
	    SensorCall(17866);if (which == OP_GPBYNAME)
		sv_setiv(sv, (IV)pent->p_proto);
	    else
		sv_setpv(sv, pent->p_name);
	}
	RETURN;
    }

    SensorCall(17869);if (pent) {
	mPUSHs(newSVpv(pent->p_name, 0));
	PUSHs(space_join_names_mortal(pent->p_aliases));
	mPUSHi(pent->p_proto);
    }

    RETURN;
#else
    DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
}

PP(pp_gservent)
{
SensorCall(17870);
#if defined(HAS_GETSERVBYNAME) || defined(HAS_GETSERVBYPORT) || defined(HAS_GETSERVENT)
    dVAR; dSP;
    I32 which = PL_op->op_type;
    register SV *sv;
#ifndef HAS_GETSERV_PROTOS /* XXX Do we need individual probes? */
    struct servent *getservbyname(Netdb_name_t, Netdb_name_t);
    struct servent *getservbyport(int, Netdb_name_t);
    struct servent *getservent(void);
#endif
    struct servent *sent;

    SensorCall(17872);if (which == OP_GSBYNAME) {
#ifdef HAS_GETSERVBYNAME
	SensorCall(17871);const char * const proto = POPpbytex;
	const char * const name = POPpbytex;
	sent = PerlSock_getservbyname(name, (proto && !*proto) ? NULL : proto);
#else
	DIE(aTHX_ PL_no_sock_func, "getservbyname");
#endif
    }
    else if (which == OP_GSBYPORT) {
#ifdef HAS_GETSERVBYPORT
	const char * const proto = POPpbytex;
	unsigned short port = (unsigned short)POPu;
#ifdef HAS_HTONS
	port = PerlSock_htons(port);
#endif
	sent = PerlSock_getservbyport(port, (proto && !*proto) ? NULL : proto);
#else
	DIE(aTHX_ PL_no_sock_func, "getservbyport");
#endif
    }
    else
#ifdef HAS_GETSERVENT
	sent = PerlSock_getservent();
#else
	DIE(aTHX_ PL_no_sock_func, "getservent");
#endif

    EXTEND(SP, 4);
    SensorCall(17875);if (GIMME != G_ARRAY) {
	PUSHs(sv = sv_newmortal());
	SensorCall(17874);if (sent) {
	    SensorCall(17873);if (which == OP_GSBYNAME) {
#ifdef HAS_NTOHS
		sv_setiv(sv, (IV)PerlSock_ntohs(sent->s_port));
#else
		sv_setiv(sv, (IV)(sent->s_port));
#endif
	    }
	    else
		sv_setpv(sv, sent->s_name);
	}
	RETURN;
    }

    SensorCall(17876);if (sent) {
	mPUSHs(newSVpv(sent->s_name, 0));
	PUSHs(space_join_names_mortal(sent->s_aliases));
#ifdef HAS_NTOHS
	mPUSHi(PerlSock_ntohs(sent->s_port));
#else
	mPUSHi(sent->s_port);
#endif
	mPUSHs(newSVpv(sent->s_proto, 0));
    }

    RETURN;
#else
    DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
}

PP(pp_shostent)
{
SensorCall(17877);    dVAR; dSP;
    const int stayopen = TOPi;
    SensorCall(17882);switch(PL_op->op_type) {
    case OP_SHOSTENT:
#ifdef HAS_SETHOSTENT
	PerlSock_sethostent(stayopen);
#else
	DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17878);break;
#ifdef HAS_SETNETENT
    case OP_SNETENT:
	PerlSock_setnetent(stayopen);
#else
	DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17879);break;
    case OP_SPROTOENT:
#ifdef HAS_SETPROTOENT
	PerlSock_setprotoent(stayopen);
#else
	DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17880);break;
    case OP_SSERVENT:
#ifdef HAS_SETSERVENT
	PerlSock_setservent(stayopen);
#else
	DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17881);break;
    }
    RETSETYES;
}

PP(pp_ehostent)
{
SensorCall(17883);    dVAR; dSP;
    SensorCall(17896);switch(PL_op->op_type) {
    case OP_EHOSTENT:
#ifdef HAS_ENDHOSTENT
	PerlSock_endhostent();
#else
	DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17884);break;
    case OP_ENETENT:
#ifdef HAS_ENDNETENT
	PerlSock_endnetent();
#else
	DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17885);break;
    case OP_EPROTOENT:
#ifdef HAS_ENDPROTOENT
	PerlSock_endprotoent();
#else
	DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17886);break;
    case OP_ESERVENT:
#ifdef HAS_ENDSERVENT
	PerlSock_endservent();
#else
	DIE(aTHX_ PL_no_sock_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17887);break;
    case OP_SGRENT:
#if defined(HAS_GROUP) && defined(HAS_SETGRENT)
	SensorCall(17888);setgrent();
#else
	DIE(aTHX_ PL_no_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17889);break;
    case OP_EGRENT:
#if defined(HAS_GROUP) && defined(HAS_ENDGRENT)
	SensorCall(17890);endgrent();
#else
	DIE(aTHX_ PL_no_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17891);break;
    case OP_SPWENT:
#if defined(HAS_PASSWD) && defined(HAS_SETPWENT)
	SensorCall(17892);setpwent();
#else
	DIE(aTHX_ PL_no_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17893);break;
    case OP_EPWENT:
#if defined(HAS_PASSWD) && defined(HAS_ENDPWENT)
	SensorCall(17894);endpwent();
#else
	DIE(aTHX_ PL_no_func, PL_op_desc[PL_op->op_type]);
#endif
	SensorCall(17895);break;
    }
    EXTEND(SP,1);
    RETPUSHYES;
}

PP(pp_gpwent)
{
SensorCall(17897);
#ifdef HAS_PASSWD
    dVAR; dSP;
    I32 which = PL_op->op_type;
    register SV *sv;
    struct passwd *pwent  = NULL;
    /*
     * We currently support only the SysV getsp* shadow password interface.
     * The interface is declared in <shadow.h> and often one needs to link
     * with -lsecurity or some such.
     * This interface is used at least by Solaris, HP-UX, IRIX, and Linux.
     * (and SCO?)
     *
     * AIX getpwnam() is clever enough to return the encrypted password
     * only if the caller (euid?) is root.
     *
     * There are at least three other shadow password APIs.  Many platforms
     * seem to contain more than one interface for accessing the shadow
     * password databases, possibly for compatibility reasons.
     * The getsp*() is by far he simplest one, the other two interfaces
     * are much more complicated, but also very similar to each other.
     *
     * <sys/types.h>
     * <sys/security.h>
     * <prot.h>
     * struct pr_passwd *getprpw*();
     * The password is in
     * char getprpw*(...).ufld.fd_encrypt[]
     * Mention HAS_GETPRPWNAM here so that Configure probes for it.
     *
     * <sys/types.h>
     * <sys/security.h>
     * <prot.h>
     * struct es_passwd *getespw*();
     * The password is in
     * char *(getespw*(...).ufld.fd_encrypt)
     * Mention HAS_GETESPWNAM here so that Configure probes for it.
     *
     * <userpw.h> (AIX)
     * struct userpw *getuserpw();
     * The password is in
     * char *(getuserpw(...)).spw_upw_passwd
     * (but the de facto standard getpwnam() should work okay)
     *
     * Mention I_PROT here so that Configure probes for it.
     *
     * In HP-UX for getprpw*() the manual page claims that one should include
     * <hpsecurity.h> instead of <sys/security.h>, but that is not needed
     * if one includes <shadow.h> as that includes <hpsecurity.h>,
     * and pp_sys.c already includes <shadow.h> if there is such.
     *
     * Note that <sys/security.h> is already probed for, but currently
     * it is only included in special cases.
     *
     * In Digital UNIX/Tru64 if using the getespw*() (which seems to be
     * be preferred interface, even though also the getprpw*() interface
     * is available) one needs to link with -lsecurity -ldb -laud -lm.
     * One also needs to call set_auth_parameters() in main() before
     * doing anything else, whether one is using getespw*() or getprpw*().
     *
     * Note that accessing the shadow databases can be magnitudes
     * slower than accessing the standard databases.
     *
     * --jhi
     */

#   if defined(__CYGWIN__) && defined(USE_REENTRANT_API)
    /* Cygwin 1.5.3-1 has buggy getpwnam_r() and getpwuid_r():
     * the pw_comment is left uninitialized. */
    PL_reentrant_buffer->_pwent_struct.pw_comment = NULL;
#   endif

    SensorCall(17904);switch (which) {
    case OP_GPWNAM:
      {
	SensorCall(17898);const char* const name = POPpbytex;
	pwent  = getpwnam(name);
      }
      SensorCall(17899);break;
    case OP_GPWUID:
      {
	Uid_t uid = POPi;
	SensorCall(17900);pwent = getpwuid(uid);
      }
	SensorCall(17901);break;
    case OP_GPWENT:
#   ifdef HAS_GETPWENT
	SensorCall(17902);pwent  = getpwent();
#ifdef POSIX_BC   /* In some cases pw_passwd has invalid addresses */
	if (pwent) pwent = getpwnam(pwent->pw_name);
#endif
#   else
	DIE(aTHX_ PL_no_func, "getpwent");
#   endif
	SensorCall(17903);break;
    }

    EXTEND(SP, 10);
    SensorCall(17907);if (GIMME != G_ARRAY) {
	PUSHs(sv = sv_newmortal());
	SensorCall(17906);if (pwent) {
	    SensorCall(17905);if (which == OP_GPWNAM)
#   if Uid_t_sign <= 0
		sv_setiv(sv, (IV)pwent->pw_uid);
#   else
		sv_setuv(sv, (UV)pwent->pw_uid);
#   endif
	    else
		sv_setpv(sv, pwent->pw_name);
	}
	RETURN;
    }

    SensorCall(17911);if (pwent) {
	mPUSHs(newSVpv(pwent->pw_name, 0));

	SensorCall(17908);sv = newSViv(0);
	mPUSHs(sv);
	/* If we have getspnam(), we try to dig up the shadow
	 * password.  If we are underprivileged, the shadow
	 * interface will set the errno to EACCES or similar,
	 * and return a null pointer.  If this happens, we will
	 * use the dummy password (usually "*" or "x") from the
	 * standard password database.
	 *
	 * In theory we could skip the shadow call completely
	 * if euid != 0 but in practice we cannot know which
	 * security measures are guarding the shadow databases
	 * on a random platform.
	 *
	 * Resist the urge to use additional shadow interfaces.
	 * Divert the urge to writing an extension instead.
	 *
	 * --jhi */
	/* Some AIX setups falsely(?) detect some getspnam(), which
	 * has a different API than the Solaris/IRIX one. */
#   if defined(HAS_GETSPNAM) && !defined(_AIX)
	{
	    dSAVE_ERRNO;
	    const struct spwd * const spwent = getspnam(pwent->pw_name);
			  /* Save and restore errno so that
			   * underprivileged attempts seem
			   * to have never made the unsuccessful
			   * attempt to retrieve the shadow password. */
	    RESTORE_ERRNO;
	    SensorCall(17909);if (spwent && spwent->sp_pwdp)
		sv_setpv(sv, spwent->sp_pwdp);
	}
#   endif
#   ifdef PWPASSWD
	SensorCall(17910);if (!SvPOK(sv)) /* Use the standard password, then. */
	    sv_setpv(sv, pwent->pw_passwd);
#   endif

#   ifndef INCOMPLETE_TAINTS
	/* passwd is tainted because user himself can diddle with it.
	 * admittedly not much and in a very limited way, but nevertheless. */
	SvTAINTED_on(sv);
#   endif

#   if Uid_t_sign <= 0
	mPUSHi(pwent->pw_uid);
#   else
	mPUSHu(pwent->pw_uid);
#   endif

#   if Uid_t_sign <= 0
	mPUSHi(pwent->pw_gid);
#   else
	mPUSHu(pwent->pw_gid);
#   endif
	/* pw_change, pw_quota, and pw_age are mutually exclusive--
	 * because of the poor interface of the Perl getpw*(),
	 * not because there's some standard/convention saying so.
	 * A better interface would have been to return a hash,
	 * but we are accursed by our history, alas. --jhi.  */
#   ifdef PWCHANGE
	mPUSHi(pwent->pw_change);
#   else
#       ifdef PWQUOTA
	mPUSHi(pwent->pw_quota);
#       else
#           ifdef PWAGE
	mPUSHs(newSVpv(pwent->pw_age, 0));
#	    else
	/* I think that you can never get this compiled, but just in case.  */
	PUSHs(sv_mortalcopy(&PL_sv_no));
#           endif
#       endif
#   endif

	/* pw_class and pw_comment are mutually exclusive--.
	 * see the above note for pw_change, pw_quota, and pw_age. */
#   ifdef PWCLASS
	mPUSHs(newSVpv(pwent->pw_class, 0));
#   else
#       ifdef PWCOMMENT
	mPUSHs(newSVpv(pwent->pw_comment, 0));
#	else
	/* I think that you can never get this compiled, but just in case.  */
	PUSHs(sv_mortalcopy(&PL_sv_no));
#       endif
#   endif

#   ifdef PWGECOS
	PUSHs(sv = sv_2mortal(newSVpv(pwent->pw_gecos, 0)));
#   else
	PUSHs(sv = sv_mortalcopy(&PL_sv_no));
#   endif
#   ifndef INCOMPLETE_TAINTS
	/* pw_gecos is tainted because user himself can diddle with it. */
	SvTAINTED_on(sv);
#   endif

	mPUSHs(newSVpv(pwent->pw_dir, 0));

	PUSHs(sv = sv_2mortal(newSVpv(pwent->pw_shell, 0)));
#   ifndef INCOMPLETE_TAINTS
	/* pw_shell is tainted because user himself can diddle with it. */
	SvTAINTED_on(sv);
#   endif

#   ifdef PWEXPIRE
	mPUSHi(pwent->pw_expire);
#   endif
    }
    RETURN;
#else
    DIE(aTHX_ PL_no_func, PL_op_desc[PL_op->op_type]);
#endif
}

PP(pp_ggrent)
{
SensorCall(17912);
#ifdef HAS_GROUP
    dVAR; dSP;
    const I32 which = PL_op->op_type;
    const struct group *grent;

    SensorCall(17914);if (which == OP_GGRNAM) {
	SensorCall(17913);const char* const name = POPpbytex;
	grent = (const struct group *)getgrnam(name);
    }
    else if (which == OP_GGRGID) {
	const Gid_t gid = POPi;
	grent = (const struct group *)getgrgid(gid);
    }
    else
#ifdef HAS_GETGRENT
	grent = (struct group *)getgrent();
#else
        DIE(aTHX_ PL_no_func, "getgrent");
#endif

    EXTEND(SP, 4);
    SensorCall(17918);if (GIMME != G_ARRAY) {
	SensorCall(17915);SV * const sv = sv_newmortal();

	PUSHs(sv);
	SensorCall(17917);if (grent) {
	    SensorCall(17916);if (which == OP_GGRNAM)
#if Gid_t_sign <= 0
		sv_setiv(sv, (IV)grent->gr_gid);
#else
		sv_setuv(sv, (UV)grent->gr_gid);
#endif
	    else
		sv_setpv(sv, grent->gr_name);
	}
	RETURN;
    }

    SensorCall(17919);if (grent) {
	mPUSHs(newSVpv(grent->gr_name, 0));

#ifdef GRPASSWD
	mPUSHs(newSVpv(grent->gr_passwd, 0));
#else
	PUSHs(sv_mortalcopy(&PL_sv_no));
#endif

#if Gid_t_sign <= 0
	mPUSHi(grent->gr_gid);
#else
	mPUSHu(grent->gr_gid);
#endif

#if !(defined(_CRAYMPP) && defined(USE_REENTRANT_API))
	/* In UNICOS/mk (_CRAYMPP) the multithreading
	 * versions (getgrnam_r, getgrgid_r)
	 * seem to return an illegal pointer
	 * as the group members list, gr_mem.
	 * getgrent() doesn't even have a _r version
	 * but the gr_mem is poisonous anyway.
	 * So yes, you cannot get the list of group
	 * members if building multithreaded in UNICOS/mk. */
	PUSHs(space_join_names_mortal(grent->gr_mem));
#endif
    }

    RETURN;
#else
    DIE(aTHX_ PL_no_func, PL_op_desc[PL_op->op_type]);
#endif
}

PP(pp_getlogin)
{
SensorCall(17920);
#ifdef HAS_GETLOGIN
    dVAR; dSP; dTARGET;
    char *tmps;
    EXTEND(SP, 1);
    SensorCall(17921);if (!(tmps = PerlProc_getlogin()))
	RETPUSHUNDEF;
    sv_setpv_mg(TARG, tmps);
    PUSHs(TARG);
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "getlogin");
#endif
}

/* Miscellaneous. */

PP(pp_syscall)
{
SensorCall(17922);
#ifdef HAS_SYSCALL
    dVAR; dSP; dMARK; dORIGMARK; dTARGET;
    register I32 items = SP - MARK;
    unsigned long a[20];
    register I32 i = 0;
    IV retval = -1;

    SensorCall(17926);if (PL_tainting) {
	SensorCall(17923);while (++MARK <= SP) {
	    SensorCall(17924);if (SvTAINTED(*MARK)) {
		TAINT;
		SensorCall(17925);break;
	    }
	}
	MARK = ORIGMARK;
	TAINT_PROPER("syscall");
    }

    /* This probably won't work on machines where sizeof(long) != sizeof(int)
     * or where sizeof(long) != sizeof(char*).  But such machines will
     * not likely have syscall implemented either, so who cares?
     */
    SensorCall(17930);while (++MARK <= SP) {
	SensorCall(17927);if (SvNIOK(*MARK) || !i)
	    a[i++] = SvIV(*MARK);
	else if (*MARK == &PL_sv_undef)
	    {/*131*/a[i++] = 0;/*132*/}
	else
	    a[i++] = (unsigned long)SvPV_force_nolen(*MARK);
	SensorCall(17929);if (i > 15)
	    {/*133*/SensorCall(17928);break;/*134*/}
    }
    SensorCall(17947);switch (items) {
    default:
	DIE(aTHX_ "Too many args to syscall");
    case 0:
	DIE(aTHX_ "Too few args to syscall");
    case 1:
	SensorCall(17931);retval = syscall(a[0]);
	SensorCall(17932);break;
    case 2:
	SensorCall(17933);retval = syscall(a[0],a[1]);
	SensorCall(17934);break;
    case 3:
	SensorCall(17935);retval = syscall(a[0],a[1],a[2]);
	SensorCall(17936);break;
    case 4:
	SensorCall(17937);retval = syscall(a[0],a[1],a[2],a[3]);
	SensorCall(17938);break;
    case 5:
	SensorCall(17939);retval = syscall(a[0],a[1],a[2],a[3],a[4]);
	SensorCall(17940);break;
    case 6:
	SensorCall(17941);retval = syscall(a[0],a[1],a[2],a[3],a[4],a[5]);
	SensorCall(17942);break;
    case 7:
	SensorCall(17943);retval = syscall(a[0],a[1],a[2],a[3],a[4],a[5],a[6]);
	SensorCall(17944);break;
    case 8:
	SensorCall(17945);retval = syscall(a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7]);
	SensorCall(17946);break;
#ifdef atarist
    case 9:
	retval = syscall(a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8]);
	break;
    case 10:
	retval = syscall(a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9]);
	break;
    case 11:
	retval = syscall(a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],
	  a[10]);
	break;
    case 12:
	retval = syscall(a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],
	  a[10],a[11]);
	break;
    case 13:
	retval = syscall(a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],
	  a[10],a[11],a[12]);
	break;
    case 14:
	retval = syscall(a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],
	  a[10],a[11],a[12],a[13]);
	break;
#endif /* atarist */
    }
    SP = ORIGMARK;
    PUSHi(retval);
    RETURN;
#else
    DIE(aTHX_ PL_no_func, "syscall");
#endif
}

#ifdef FCNTL_EMULATE_FLOCK

/*  XXX Emulate flock() with fcntl().
    What's really needed is a good file locking module.
*/

static int
fcntl_emulate_flock(int fd, int operation)
{
    int res;
    struct flock flock;

    switch (operation & ~LOCK_NB) {
    case LOCK_SH:
	flock.l_type = F_RDLCK;
	break;
    case LOCK_EX:
	flock.l_type = F_WRLCK;
	break;
    case LOCK_UN:
	flock.l_type = F_UNLCK;
	break;
    default:
	errno = EINVAL;
	return -1;
    }
    flock.l_whence = SEEK_SET;
    flock.l_start = flock.l_len = (Off_t)0;

    res = fcntl(fd, (operation & LOCK_NB) ? F_SETLK : F_SETLKW, &flock);
    if (res == -1 && ((errno == EAGAIN) || (errno == EACCES)))
	errno = EWOULDBLOCK;
    return res;
}

#endif /* FCNTL_EMULATE_FLOCK */

#ifdef LOCKF_EMULATE_FLOCK

/*  XXX Emulate flock() with lockf().  This is just to increase
    portability of scripts.  The calls are not completely
    interchangeable.  What's really needed is a good file
    locking module.
*/

/*  The lockf() constants might have been defined in <unistd.h>.
    Unfortunately, <unistd.h> causes troubles on some mixed
    (BSD/POSIX) systems, such as SunOS 4.1.3.

   Further, the lockf() constants aren't POSIX, so they might not be
   visible if we're compiling with _POSIX_SOURCE defined.  Thus, we'll
   just stick in the SVID values and be done with it.  Sigh.
*/

# ifndef F_ULOCK
#  define F_ULOCK	0	/* Unlock a previously locked region */
# endif
# ifndef F_LOCK
#  define F_LOCK	1	/* Lock a region for exclusive use */
# endif
# ifndef F_TLOCK
#  define F_TLOCK	2	/* Test and lock a region for exclusive use */
# endif
# ifndef F_TEST
#  define F_TEST	3	/* Test a region for other processes locks */
# endif

static int
lockf_emulate_flock(int fd, int operation)
{
    int i;
    Off_t pos;
    dSAVE_ERRNO;

    /* flock locks entire file so for lockf we need to do the same	*/
    pos = PerlLIO_lseek(fd, (Off_t)0, SEEK_CUR);    /* get pos to restore later */
    if (pos > 0)	/* is seekable and needs to be repositioned	*/
	if (PerlLIO_lseek(fd, (Off_t)0, SEEK_SET) < 0)
	    pos = -1;	/* seek failed, so don't seek back afterwards	*/
    RESTORE_ERRNO;

    switch (operation) {

	/* LOCK_SH - get a shared lock */
	case LOCK_SH:
	/* LOCK_EX - get an exclusive lock */
	case LOCK_EX:
	    i = lockf (fd, F_LOCK, 0);
	    break;

	/* LOCK_SH|LOCK_NB - get a non-blocking shared lock */
	case LOCK_SH|LOCK_NB:
	/* LOCK_EX|LOCK_NB - get a non-blocking exclusive lock */
	case LOCK_EX|LOCK_NB:
	    i = lockf (fd, F_TLOCK, 0);
	    if (i == -1)
		if ((errno == EAGAIN) || (errno == EACCES))
		    errno = EWOULDBLOCK;
	    break;

	/* LOCK_UN - unlock (non-blocking is a no-op) */
	case LOCK_UN:
	case LOCK_UN|LOCK_NB:
	    i = lockf (fd, F_ULOCK, 0);
	    break;

	/* Default - can't decipher operation */
	default:
	    i = -1;
	    errno = EINVAL;
	    break;
    }

    if (pos > 0)      /* need to restore position of the handle	*/
	PerlLIO_lseek(fd, pos, SEEK_SET);	/* ignore error here	*/

    return (i);
}

#endif /* LOCKF_EMULATE_FLOCK */

/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
