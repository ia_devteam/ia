#include "var/tmp/sensor.h"
/*    pp.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
 *    2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 *
 */

/*
 * 'It's a big house this, and very peculiar.  Always a bit more
 *  to discover, and no knowing what you'll find round a corner.
 *  And Elves, sir!'                            --Samwise Gamgee
 *
 *     [p.225 of _The Lord of the Rings_, II/i: "Many Meetings"]
 */

/* This file contains general pp ("push/pop") functions that execute the
 * opcodes that make up a perl program. A typical pp function expects to
 * find its arguments on the stack, and usually pushes its results onto
 * the stack, hence the 'pp' terminology. Each OP structure contains
 * a pointer to the relevant pp_foo() function.
 */

#include "EXTERN.h"
#define PERL_IN_PP_C
#include "perl.h"
#include "keywords.h"

#include "reentr.h"

/* XXX I can't imagine anyone who doesn't have this actually _needs_
   it, since pid_t is an integral type.
   --AD  2/20/1998
*/
#ifdef NEED_GETPID_PROTO
extern Pid_t getpid (void);
#endif

/*
 * Some BSDs and Cygwin default to POSIX math instead of IEEE.
 * This switches them over to IEEE.
 */
#if defined(LIBM_LIB_VERSION)
    _LIB_VERSION_TYPE _LIB_VERSION = _IEEE_;
#endif

/* variations on pp_null */

PP(pp_stub)
{
SensorCall(11455);    dVAR;
    dSP;
    SensorCall(11456);if (GIMME_V == G_SCALAR)
	XPUSHs(&PL_sv_undef);
    RETURN;
}

/* Pushy stuff. */

PP(pp_padav)
{
SensorCall(11457);    dVAR; dSP; dTARGET;
    I32 gimme;
    assert(SvTYPE(TARG) == SVt_PVAV);
    SensorCall(11458);if (PL_op->op_private & OPpLVAL_INTRO)
	if (!(PL_op->op_private & OPpPAD_STATE))
	    SAVECLEARSV(PAD_SVl(PL_op->op_targ));
    EXTEND(SP, 1);
    SensorCall(11464);if (PL_op->op_flags & OPf_REF) {
	PUSHs(TARG);
	RETURN;
    } else {/*199*/SensorCall(11459);if (PL_op->op_private & OPpMAYBE_LVSUB) {
       SensorCall(11460);const I32 flags = is_lvalue_sub();
       SensorCall(11463);if (flags && !(flags & OPpENTERSUB_INARGS)) {
	SensorCall(11461);if (GIMME == G_SCALAR)
	    /* diag_listed_as: Can't return %s to lvalue scalar context */
	    {/*201*/SensorCall(11462);Perl_croak(aTHX_ "Can't return array to lvalue scalar context");/*202*/}
	PUSHs(TARG);
	RETURN;
       }
    ;/*200*/}}
    SensorCall(11465);gimme = GIMME_V;
    SensorCall(11473);if (gimme == G_ARRAY) {
	SensorCall(11466);const I32 maxarg = AvFILL(MUTABLE_AV(TARG)) + 1;
	EXTEND(SP, maxarg);
	SensorCall(11470);if (SvMAGICAL(TARG)) {
	    SensorCall(11467);U32 i;
	    SensorCall(11469);for (i=0; i < (U32)maxarg; i++) {
		SensorCall(11468);SV * const * const svp = av_fetch(MUTABLE_AV(TARG), i, FALSE);
		SP[i+1] = (svp) ? *svp : &PL_sv_undef;
	    }
	}
	else {
	    Copy(AvARRAY((const AV *)TARG), SP+1, maxarg, SV*);
	}
	SP += maxarg;
    }
    else {/*203*/SensorCall(11471);if (gimme == G_SCALAR) {
	SensorCall(11472);SV* const sv = sv_newmortal();
	const I32 maxarg = AvFILL(MUTABLE_AV(TARG)) + 1;
	sv_setiv(sv, maxarg);
	PUSHs(sv);
    ;/*204*/}}
    RETURN;
}

PP(pp_padhv)
{
SensorCall(11474);    dVAR; dSP; dTARGET;
    I32 gimme;

    assert(SvTYPE(TARG) == SVt_PVHV);
    XPUSHs(TARG);
    SensorCall(11475);if (PL_op->op_private & OPpLVAL_INTRO)
	if (!(PL_op->op_private & OPpPAD_STATE))
	    SAVECLEARSV(PAD_SVl(PL_op->op_targ));
    SensorCall(11481);if (PL_op->op_flags & OPf_REF)
	RETURN;
    else {/*205*/SensorCall(11476);if (PL_op->op_private & OPpMAYBE_LVSUB) {
      SensorCall(11477);const I32 flags = is_lvalue_sub();
      SensorCall(11480);if (flags && !(flags & OPpENTERSUB_INARGS)) {
	SensorCall(11478);if (GIMME == G_SCALAR)
	    /* diag_listed_as: Can't return %s to lvalue scalar context */
	    {/*207*/SensorCall(11479);Perl_croak(aTHX_ "Can't return hash to lvalue scalar context");/*208*/}
	RETURN;
      }
    ;/*206*/}}
    SensorCall(11482);gimme = GIMME_V;
    SensorCall(11485);if (gimme == G_ARRAY) {
	RETURNOP(Perl_do_kv(aTHX));
    }
    else {/*209*/SensorCall(11483);if (gimme == G_SCALAR) {
	SensorCall(11484);SV* const sv = Perl_hv_scalar(aTHX_ MUTABLE_HV(TARG));
	SETs(sv);
    ;/*210*/}}
    RETURN;
}

/* Translations. */

static const char S_no_symref_sv[] =
    "Can't use string (\"%" SVf32 "\"%s) as %s ref while \"strict refs\" in use";

/* In some cases this function inspects PL_op.  If this function is called
   for new op types, more bool parameters may need to be added in place of
   the checks.

   When noinit is true, the absence of a gv will cause a retval of undef.
   This is unrelated to the cv-to-gv assignment case.
*/

static SV *
S_rv2gv(pTHX_ SV *sv, const bool vivify_sv, const bool strict,
              const bool noinit)
{
SensorCall(11486);    dVAR;
    SensorCall(11487);if (!isGV(sv) || SvFAKE(sv)) SvGETMAGIC(sv);
    SensorCall(11516);if (SvROK(sv)) {
	SensorCall(11488);if (SvAMAGIC(sv)) {
	    SensorCall(11489);sv = amagic_deref_call(sv, to_gv_amg);
	}
      wasref:
	SensorCall(11490);sv = SvRV(sv);
	SensorCall(11494);if (SvTYPE(sv) == SVt_PVIO) {
	    SensorCall(11491);GV * const gv = MUTABLE_GV(sv_newmortal());
	    gv_init(gv, 0, "__ANONIO__", 10, 0);
	    GvIOp(gv) = MUTABLE_IO(sv);
	    SvREFCNT_inc_void_NN(sv);
	    sv = MUTABLE_SV(gv);
	}
	else {/*397*/SensorCall(11492);if (!isGV_with_GP(sv))
	    {/*399*/{SV * ReplaceReturn1023 = (SV *)Perl_die(aTHX_ "Not a GLOB reference"); SensorCall(11493); return ReplaceReturn1023;}/*400*/}/*398*/}
    }
    else {
	SensorCall(11495);if (!isGV_with_GP(sv)) {
	    SensorCall(11496);if (!SvOK(sv)) {
		/* If this is a 'my' scalar and flag is set then vivify
		 * NI-S 1999/05/07
		 */
		SensorCall(11497);if (vivify_sv && sv != &PL_sv_undef) {
		    SensorCall(11498);GV *gv;
		    SensorCall(11500);if (SvREADONLY(sv))
			{/*401*/SensorCall(11499);Perl_croak_no_modify(aTHX);/*402*/}
		    SensorCall(11503);if (cUNOP->op_targ) {
			SensorCall(11501);SV * const namesv = PAD_SV(cUNOP->op_targ);
			gv = MUTABLE_GV(newSV(0));
			gv_init_sv(gv, CopSTASH(PL_curcop), namesv, 0);
		    }
		    else {
			SensorCall(11502);const char * const name = CopSTASHPV(PL_curcop);
			gv = newGVgen_flags(name,
                                        HvNAMEUTF8(CopSTASH(PL_curcop)) ? SVf_UTF8 : 0 );
		    }
		    prepare_SV_for_RV(sv);
		    SvRV_set(sv, MUTABLE_SV(gv));
		    SvROK_on(sv);
		    SvSETMAGIC(sv);
		    SensorCall(11504);goto wasref;
		}
		SensorCall(11506);if (PL_op->op_flags & OPf_REF || strict)
		    {/*403*/{SV * ReplaceReturn1022 = (SV *)Perl_die(aTHX_ PL_no_usym, "a symbol"); SensorCall(11505); return ReplaceReturn1022;}/*404*/}
		SensorCall(11507);if (ckWARN(WARN_UNINITIALIZED))
		    report_uninit(sv);
		{SV * ReplaceReturn1021 = &PL_sv_undef; SensorCall(11508); return ReplaceReturn1021;}
	    }
	    SensorCall(11515);if (noinit)
	    {
		SensorCall(11509);if (!(sv = MUTABLE_SV(gv_fetchsv_nomg(
		           sv, GV_ADDMG, SVt_PVGV
		   ))))
		    return &PL_sv_undef;
	    }
	    else {
		SensorCall(11510);if (strict)
		    {/*405*/SensorCall(11511);return
		     (SV *)Perl_die(aTHX_
		            S_no_symref_sv,
		            sv,
		            (SvPOK(sv) && SvCUR(sv)>32 ? "..." : ""),
		            "a symbol"
		           );/*406*/}
		SensorCall(11513);if ((PL_op->op_private & (OPpLVAL_INTRO|OPpDONT_INIT_GV))
		    == OPpDONT_INIT_GV) {
		    /* We are the target of a coderef assignment.  Return
		       the scalar unchanged, and let pp_sasssign deal with
		       things.  */
		    {SV * ReplaceReturn1020 = sv; SensorCall(11512); return ReplaceReturn1020;}
		}
		SensorCall(11514);sv = MUTABLE_SV(gv_fetchsv_nomg(sv, GV_ADD, SVt_PVGV));
	    }
	    /* FAKE globs in the symbol table cause weird bugs (#77810) */
	    SvFAKE_off(sv);
	}
    }
    SensorCall(11518);if (SvFAKE(sv) && !(PL_op->op_private & OPpALLOW_FAKE)) {
	SensorCall(11517);SV *newsv = sv_newmortal();
	sv_setsv_flags(newsv, sv, 0);
	SvFAKE_off(newsv);
	sv = newsv;
    }
    {SV * ReplaceReturn1019 = sv; SensorCall(11519); return ReplaceReturn1019;}
}

PP(pp_rv2gv)
{
SensorCall(11520);    dVAR; dSP; dTOPss;

    sv = S_rv2gv(aTHX_
          sv, PL_op->op_private & OPpDEREF,
          PL_op->op_private & HINT_STRICT_REFS,
          ((PL_op->op_flags & OPf_SPECIAL) && !(PL_op->op_flags & OPf_MOD))
             || PL_op->op_type == OP_READLINE
         );
    SensorCall(11521);if (PL_op->op_private & OPpLVAL_INTRO)
	save_gp(MUTABLE_GV(sv), !(PL_op->op_flags & OPf_SPECIAL));
    SETs(sv);
    RETURN;
}

/* Helper function for pp_rv2sv and pp_rv2av  */
GV *
Perl_softref2xv(pTHX_ SV *const sv, const char *const what,
		const svtype type, SV ***spp)
{
SensorCall(11522);    dVAR;
    GV *gv;

    PERL_ARGS_ASSERT_SOFTREF2XV;

    SensorCall(11526);if (PL_op->op_private & HINT_STRICT_REFS) {
	SensorCall(11523);if (SvOK(sv))
	    {/*65*/SensorCall(11524);Perl_die(aTHX_ S_no_symref_sv, sv, (SvPOK(sv) && SvCUR(sv)>32 ? "..." : ""), what);/*66*/}
	else
	    {/*67*/SensorCall(11525);Perl_die(aTHX_ PL_no_usym, what);/*68*/}
    }
    SensorCall(11535);if (!SvOK(sv)) {
	SensorCall(11527);if (
	  PL_op->op_flags & OPf_REF &&
	  PL_op->op_next->op_type != OP_BOOLKEYS
	)
	    {/*69*/SensorCall(11528);Perl_die(aTHX_ PL_no_usym, what);/*70*/}
	SensorCall(11529);if (ckWARN(WARN_UNINITIALIZED))
	    report_uninit(sv);
	SensorCall(11532);if (type != SVt_PV && GIMME_V == G_ARRAY) {
	    SensorCall(11530);(*spp)--;
	    {GV * ReplaceReturn1018 = NULL; SensorCall(11531); return ReplaceReturn1018;}
	}
	SensorCall(11533);**spp = &PL_sv_undef;
	{GV * ReplaceReturn1017 = NULL; SensorCall(11534); return ReplaceReturn1017;}
    }
    SensorCall(11540);if ((PL_op->op_flags & OPf_SPECIAL) &&
	!(PL_op->op_flags & OPf_MOD))
	{
	    SensorCall(11536);if (!(gv = gv_fetchsv_nomg(sv, GV_ADDMG, type)))
		{
		    SensorCall(11537);**spp = &PL_sv_undef;
		    {GV * ReplaceReturn1016 = NULL; SensorCall(11538); return ReplaceReturn1016;}
		}
	}
    else {
	SensorCall(11539);gv = gv_fetchsv_nomg(sv, GV_ADD, type);
    }
    {GV * ReplaceReturn1015 = gv; SensorCall(11541); return ReplaceReturn1015;}
}

PP(pp_rv2sv)
{
SensorCall(11542);    dVAR; dSP; dTOPss;
    GV *gv = NULL;

    SvGETMAGIC(sv);
    SensorCall(11552);if (SvROK(sv)) {
	SensorCall(11543);if (SvAMAGIC(sv)) {
	    SensorCall(11544);sv = amagic_deref_call(sv, to_sv_amg);
	}

	SensorCall(11545);sv = SvRV(sv);
	SensorCall(11546);switch (SvTYPE(sv)) {
	case SVt_PVAV:
	case SVt_PVHV:
	case SVt_PVCV:
	case SVt_PVFM:
	case SVt_PVIO:
	    DIE(aTHX_ "Not a SCALAR reference");
	default: NOOP;
	}
    }
    else {
	SensorCall(11547);gv = MUTABLE_GV(sv);

	SensorCall(11550);if (!isGV_with_GP(gv)) {
	    SensorCall(11548);gv = Perl_softref2xv(aTHX_ sv, "a SCALAR", SVt_PV, &sp);
	    SensorCall(11549);if (!gv)
		RETURN;
	}
	SensorCall(11551);sv = GvSVn(gv);
    }
    SensorCall(11557);if (PL_op->op_flags & OPf_MOD) {
	SensorCall(11553);if (PL_op->op_private & OPpLVAL_INTRO) {
	    SensorCall(11554);if (cUNOP->op_first->op_type == OP_NULL)
		sv = save_scalar(MUTABLE_GV(TOPs));
	    else {/*255*/SensorCall(11555);if (gv)
		sv = save_scalar(gv);
	    else
		{/*257*/SensorCall(11556);Perl_croak(aTHX_ "%s", PL_no_localize_ref);/*258*/}/*256*/}
	}
	else if (PL_op->op_private & OPpDEREF)
	    sv = vivify_ref(sv, PL_op->op_private & OPpDEREF);
    }
    SETs(sv);
    RETURN;
}

PP(pp_av2arylen)
{
SensorCall(11558);    dVAR; dSP;
    AV * const av = MUTABLE_AV(TOPs);
    const I32 lvalue = PL_op->op_flags & OPf_MOD || LVRET;
    SensorCall(11562);if (lvalue) {
	SensorCall(11559);SV ** const sv = Perl_av_arylen_p(aTHX_ MUTABLE_AV(av));
	SensorCall(11561);if (!*sv) {
	    SensorCall(11560);*sv = newSV_type(SVt_PVMG);
	    sv_magic(*sv, MUTABLE_SV(av), PERL_MAGIC_arylen, NULL, 0);
	}
	SETs(*sv);
    } else {
	SETs(sv_2mortal(newSViv(AvFILL(MUTABLE_AV(av)))));
    }
    RETURN;
}

PP(pp_pos)
{
SensorCall(11563);    dVAR; dSP; dPOPss;

    SensorCall(11570);if (PL_op->op_flags & OPf_MOD || LVRET) {
	SensorCall(11564);SV * const ret = sv_2mortal(newSV_type(SVt_PVLV));  /* Not TARG RT#67838 */
	sv_magic(ret, NULL, PERL_MAGIC_pos, NULL, 0);
	LvTYPE(ret) = '.';
	LvTARG(ret) = SvREFCNT_inc_simple(sv);
	PUSHs(ret);    /* no SvSETMAGIC */
	RETURN;
    }
    else {
	SensorCall(11565);if (SvTYPE(sv) >= SVt_PVMG && SvMAGIC(sv)) {
	    SensorCall(11566);const MAGIC * const mg = mg_find(sv, PERL_MAGIC_regex_global);
	    SensorCall(11569);if (mg && mg->mg_len >= 0) {
		dTARGET;
		SensorCall(11567);I32 i = mg->mg_len;
		SensorCall(11568);if (DO_UTF8(sv))
		    sv_pos_b2u(sv, &i);
		PUSHi(i);
		RETURN;
	    }
	}
	RETPUSHUNDEF;
    }
SensorCall(11571);}

PP(pp_rv2cv)
{
SensorCall(11572);    dVAR; dSP;
    GV *gv;
    HV *stash_unused;
    const I32 flags = (PL_op->op_flags & OPf_SPECIAL)
	? GV_ADDMG
	: ((PL_op->op_private & (OPpLVAL_INTRO|OPpMAY_RETURN_CONSTANT)) == OPpMAY_RETURN_CONSTANT)
	    ? GV_ADD|GV_NOEXPAND
	    : GV_ADD;
    /* We usually try to add a non-existent subroutine in case of AUTOLOAD. */
    /* (But not in defined().) */

    CV *cv = sv_2cv(TOPs, &stash_unused, &gv, flags);
    SensorCall(11574);if (cv) {
	SensorCall(11573);if (CvCLONE(cv))
	    cv = MUTABLE_CV(sv_2mortal(MUTABLE_SV(cv_clone(cv))));
    }
    else if ((flags == (GV_ADD|GV_NOEXPAND)) && gv && SvROK(gv)) {
	cv = MUTABLE_CV(gv);
    }    
    else
	cv = MUTABLE_CV(&PL_sv_undef);
    SETs(MUTABLE_SV(cv));
    RETURN;
}

PP(pp_prototype)
{
SensorCall(11575);    dVAR; dSP;
    CV *cv;
    HV *stash;
    GV *gv;
    SV *ret = &PL_sv_undef;

    SensorCall(11585);if (SvPOK(TOPs) && SvCUR(TOPs) >= 7) {
	SensorCall(11576);const char * s = SvPVX_const(TOPs);
	SensorCall(11584);if (strnEQ(s, "CORE::", 6)) {
	    SensorCall(11577);const int code = keyword(s + 6, SvCUR(TOPs) - 6, 1);
	    SensorCall(11578);if (!code || code == -KEY_CORE)
		DIE(aTHX_ "Can't find an opnumber for \"%s\"", s+6);
	    SensorCall(11582);if (code < 0) {	/* Overridable. */
		SensorCall(11579);SV * const sv = core_prototype(NULL, s + 6, code, NULL);
		SensorCall(11581);if (sv) {/*213*/SensorCall(11580);ret = sv;/*214*/}
	    }
	    SensorCall(11583);goto set;
	}
    }
    SensorCall(11586);cv = sv_2cv(TOPs, &stash, &gv, 0);
    SensorCall(11587);if (cv && SvPOK(cv))
	ret = newSVpvn_flags(
	    CvPROTO(cv), CvPROTOLEN(cv), SVs_TEMP | SvUTF8(cv)
	);
  set:
    SETs(ret);
    RETURN;
}

PP(pp_anoncode)
{
SensorCall(11588);    dVAR; dSP;
    CV *cv = MUTABLE_CV(PAD_SV(PL_op->op_targ));
    SensorCall(11589);if (CvCLONE(cv))
	cv = MUTABLE_CV(sv_2mortal(MUTABLE_SV(cv_clone(cv))));
    EXTEND(SP,1);
    PUSHs(MUTABLE_SV(cv));
    RETURN;
}

PP(pp_srefgen)
{
SensorCall(11590);    dVAR; dSP;
    *SP = refto(*SP);
    RETURN;
}

PP(pp_refgen)
{
SensorCall(11591);    dVAR; dSP; dMARK;
    SensorCall(11594);if (GIMME != G_ARRAY) {
	SensorCall(11592);if (++MARK <= SP)
	    *MARK = *SP;
	else
	    *MARK = &PL_sv_undef;
	SensorCall(11593);*MARK = refto(*MARK);
	SP = MARK;
	RETURN;
    }
    EXTEND_MORTAL(SP - MARK);
    SensorCall(11595);while (++MARK <= SP)
	*MARK = refto(*MARK);
    RETURN;
}

STATIC SV*
S_refto(pTHX_ SV *sv)
{
SensorCall(11596);    dVAR;
    SV* rv;

    PERL_ARGS_ASSERT_REFTO;

    SensorCall(11602);if (SvTYPE(sv) == SVt_PVLV && LvTYPE(sv) == 'y') {
	SensorCall(11597);if (LvTARGLEN(sv))
	    vivify_defelem(sv);
	SensorCall(11598);if (!(sv = LvTARG(sv)))
	    sv = &PL_sv_undef;
	else
	    SvREFCNT_inc_void_NN(sv);
    }
    else {/*61*/SensorCall(11599);if (SvTYPE(sv) == SVt_PVAV) {
	SensorCall(11600);if (!AvREAL((const AV *)sv) && AvREIFY((const AV *)sv))
	    av_reify(MUTABLE_AV(sv));
	SvTEMP_off(sv);
	SvREFCNT_inc_void_NN(sv);
    }
    else {/*63*/SensorCall(11601);if (SvPADTMP(sv) && !IS_PADGV(sv))
        sv = newSVsv(sv);
    else {
	SvTEMP_off(sv);
	SvREFCNT_inc_void_NN(sv);
    ;/*64*/}/*62*/}}
    SensorCall(11603);rv = sv_newmortal();
    sv_upgrade(rv, SVt_IV);
    SvRV_set(rv, sv);
    SvROK_on(rv);
    {SV * ReplaceReturn1014 = rv; SensorCall(11604); return ReplaceReturn1014;}
}

PP(pp_ref)
{
SensorCall(11605);    dVAR; dSP; dTARGET;
    SV * const sv = POPs;

    SensorCall(11606);if (sv)
	SvGETMAGIC(sv);

    SensorCall(11607);if (!sv || !SvROK(sv))
	RETPUSHNO;

    SensorCall(11608);(void)sv_ref(TARG,SvRV(sv),TRUE);
    PUSHTARG;
    RETURN;
}

PP(pp_bless)
{
SensorCall(11609);    dVAR; dSP;
    HV *stash;

    SensorCall(11619);if (MAXARG == 1)
      curstash:
	stash = CopSTASH(PL_curcop);
    else {
	SensorCall(11610);SV * const ssv = POPs;
	STRLEN len;
	const char *ptr;

	SensorCall(11612);if (!ssv) {/*79*/SensorCall(11611);goto curstash;/*80*/}
	SensorCall(11614);if (!SvGMAGICAL(ssv) && !SvAMAGIC(ssv) && SvROK(ssv))
	    {/*81*/SensorCall(11613);Perl_croak(aTHX_ "Attempt to bless into a reference");/*82*/}
	SensorCall(11615);ptr = SvPV_const(ssv,len);
	SensorCall(11617);if (len == 0)
	    {/*83*/SensorCall(11616);Perl_ck_warner(aTHX_ packWARN(WARN_MISC),
			   "Explicit blessing to '' (assuming package main)");/*84*/}
	SensorCall(11618);stash = gv_stashpvn(ptr, len, GV_ADD|SvUTF8(ssv));
    }

    SensorCall(11620);(void)sv_bless(TOPs, stash);
    RETURN;
}

PP(pp_gelem)
{
SensorCall(11621);    dVAR; dSP;

    SV *sv = POPs;
    STRLEN len;
    const char * const elem = SvPV_const(sv, len);
    GV * const gv = MUTABLE_GV(POPs);
    SV * tmpRef = NULL;

    sv = NULL;
    SensorCall(11644);if (elem) {
	/* elem will always be NUL terminated.  */
	SensorCall(11622);const char * const second_letter = elem + 1;
	SensorCall(11643);switch (*elem) {
	case 'A':
	    SensorCall(11623);if (len == 5 && strEQ(second_letter, "RRAY"))
		tmpRef = MUTABLE_SV(GvAV(gv));
	    SensorCall(11624);break;
	case 'C':
	    SensorCall(11625);if (len == 4 && strEQ(second_letter, "ODE"))
		tmpRef = MUTABLE_SV(GvCVu(gv));
	    SensorCall(11626);break;
	case 'F':
	    SensorCall(11627);if (len == 10 && strEQ(second_letter, "ILEHANDLE")) {
		/* finally deprecated in 5.8.0 */
		deprecate("*glob{FILEHANDLE}");
		SensorCall(11628);tmpRef = MUTABLE_SV(GvIOp(gv));
	    }
	    else
		if (len == 6 && strEQ(second_letter, "ORMAT"))
		    tmpRef = MUTABLE_SV(GvFORM(gv));
	    SensorCall(11629);break;
	case 'G':
	    SensorCall(11630);if (len == 4 && strEQ(second_letter, "LOB"))
		tmpRef = MUTABLE_SV(gv);
	    SensorCall(11631);break;
	case 'H':
	    SensorCall(11632);if (len == 4 && strEQ(second_letter, "ASH"))
		tmpRef = MUTABLE_SV(GvHV(gv));
	    SensorCall(11633);break;
	case 'I':
	    SensorCall(11634);if (*second_letter == 'O' && !elem[2] && len == 2)
		tmpRef = MUTABLE_SV(GvIOp(gv));
	    SensorCall(11635);break;
	case 'N':
	    SensorCall(11636);if (len == 4 && strEQ(second_letter, "AME"))
		sv = newSVhek(GvNAME_HEK(gv));
	    SensorCall(11637);break;
	case 'P':
	    SensorCall(11638);if (len == 7 && strEQ(second_letter, "ACKAGE")) {
		SensorCall(11639);const HV * const stash = GvSTASH(gv);
		const HEK * const hek = stash ? HvNAME_HEK(stash) : NULL;
		sv = hek ? newSVhek(hek) : newSVpvs("__ANON__");
	    }
	    SensorCall(11640);break;
	case 'S':
	    SensorCall(11641);if (len == 6 && strEQ(second_letter, "CALAR"))
		tmpRef = GvSVn(gv);
	    SensorCall(11642);break;
	}
    }
    SensorCall(11645);if (tmpRef)
	sv = newRV(tmpRef);
    SensorCall(11646);if (sv)
	sv_2mortal(sv);
    else
	sv = &PL_sv_undef;
    XPUSHs(sv);
    RETURN;
}

/* Pattern matching */

PP(pp_study)
{
SensorCall(11647);    dVAR; dSP; dPOPss;
    register unsigned char *s;
    char *sfirst_raw;
    STRLEN len;
    MAGIC *mg = SvMAGICAL(sv) ? mg_find(sv, PERL_MAGIC_study) : NULL;
    U8 quanta;
    STRLEN size;

    SensorCall(11648);if (mg && SvSCREAM(sv))
	RETPUSHYES;

    SensorCall(11649);s = (unsigned char*)(SvPV(sv, len));
    SensorCall(11650);if (len == 0 || len > I32_MAX || !SvPOK(sv) || SvUTF8(sv) || SvVALID(sv)) {
	/* No point in studying a zero length string, and not safe to study
	   anything that doesn't appear to be a simple scalar (and hence might
	   change between now and when the regexp engine runs without our set
	   magic ever running) such as a reference to an object with overloaded
	   stringification.  Also refuse to study an FBM scalar, as this gives
	   more flexibility in SV flag usage.  No real-world code would ever
	   end up studying an FBM scalar, so this isn't a real pessimisation.
	   Endemic use of I32 in Perl_screaminstr makes it hard to safely push
	   the study length limit from I32_MAX to U32_MAX - 1.
	*/
	RETPUSHNO;
    }

    /* Make study a no-op. It's no longer useful and its existence
       complicates matters elsewhere. This is a low-impact band-aid.
       The relevant code will be neatly removed in a future release. */
    RETPUSHYES;

    SensorCall(11655);if (len < 0xFF) {
	SensorCall(11651);quanta = 1;
    } else {/*351*/SensorCall(11652);if (len < 0xFFFF) {
	SensorCall(11653);quanta = 2;
    } else
	{/*353*/SensorCall(11654);quanta = 4;/*354*/}/*352*/}

    SensorCall(11656);size = (256 + len) * quanta;
    sfirst_raw = (char *)safemalloc(size);

    SensorCall(11657);if (!sfirst_raw)
	DIE(aTHX_ "do_study: out of memory");

    SvSCREAM_on(sv);
    SensorCall(11658);if (!mg)
	mg = sv_magicext(sv, NULL, PERL_MAGIC_study, &PL_vtbl_regexp, NULL, 0);
    SensorCall(11659);mg->mg_ptr = sfirst_raw;
    mg->mg_len = size;
    mg->mg_private = quanta;

    memset(sfirst_raw, ~0, 256 * quanta);

    /* The assumption here is that most studied strings are fairly short, hence
       the pain of the extra code is worth it, given the memory savings.
       80 character string, 336 bytes as U8, down from 1344 as U32
       800 character string, 2112 bytes as U16, down from 4224 as U32
    */
       
    SensorCall(11670);if (quanta == 1) {
	SensorCall(11660);U8 *const sfirst = (U8 *)sfirst_raw;
	U8 *const snext = sfirst + 256;
	SensorCall(11662);while (len-- > 0) {
	    SensorCall(11661);const U8 ch = s[len];
	    snext[len] = sfirst[ch];
	    sfirst[ch] = len;
	}
    } else {/*355*/SensorCall(11663);if (quanta == 2) {
	SensorCall(11664);U16 *const sfirst = (U16 *)sfirst_raw;
	U16 *const snext = sfirst + 256;
	SensorCall(11666);while (len-- > 0) {
	    SensorCall(11665);const U8 ch = s[len];
	    snext[len] = sfirst[ch];
	    sfirst[ch] = len;
	}
    } else  {
	SensorCall(11667);U32 *const sfirst = (U32 *)sfirst_raw;
	U32 *const snext = sfirst + 256;
	SensorCall(11669);while (len-- > 0) {
	    SensorCall(11668);const U8 ch = s[len];
	    snext[len] = sfirst[ch];
	    sfirst[ch] = len;
	}
    ;/*356*/}}

    RETPUSHYES;
}

PP(pp_trans)
{
SensorCall(11671);    dVAR; dSP; dTARG;
    SV *sv;

    SensorCall(11674);if (PL_op->op_flags & OPf_STACKED)
	sv = POPs;
    else {/*381*/SensorCall(11672);if (PL_op->op_private & OPpTARGET_MY)
	sv = GETTARGET;
    else {
	SensorCall(11673);sv = DEFSV;
	EXTEND(SP,1);
    ;/*382*/}}
    TARG = sv_newmortal();
    SensorCall(11676);if(PL_op->op_type == OP_TRANSR) {
	SensorCall(11675);STRLEN len;
	const char * const pv = SvPV(sv,len);
	SV * const newsv = newSVpvn_flags(pv, len, SVs_TEMP|SvUTF8(sv));
	do_trans(newsv);
	PUSHs(newsv);
    }
    else PUSHi(do_trans(sv));
    RETURN;
}

/* Lvalue operators. */

static void
S_do_chomp(pTHX_ SV *retval, SV *sv, bool chomping)
{
SensorCall(11677);    dVAR;
    STRLEN len;
    char *s;

    PERL_ARGS_ASSERT_DO_CHOMP;

    SensorCall(11679);if (chomping && (RsSNARF(PL_rs) || RsRECORD(PL_rs)))
	{/*27*/SensorCall(11678);return;/*28*/}
    SensorCall(11692);if (SvTYPE(sv) == SVt_PVAV) {
	SensorCall(11680);I32 i;
	AV *const av = MUTABLE_AV(sv);
	const I32 max = AvFILL(av);

	SensorCall(11683);for (i = 0; i <= max; i++) {
	    SensorCall(11681);sv = MUTABLE_SV(av_fetch(av, i, FALSE));
	    SensorCall(11682);if (sv && ((sv = *(SV**)sv), sv != &PL_sv_undef))
		do_chomp(retval, sv, chomping);
	}
        SensorCall(11684);return;
    }
    else {/*29*/SensorCall(11685);if (SvTYPE(sv) == SVt_PVHV) {
	SensorCall(11686);HV* const hv = MUTABLE_HV(sv);
	HE* entry;
        (void)hv_iterinit(hv);
        SensorCall(11687);while ((entry = hv_iternext(hv)))
            do_chomp(retval, hv_iterval(hv,entry), chomping);
	SensorCall(11688);return;
    }
    else {/*31*/SensorCall(11689);if (SvREADONLY(sv)) {
        SensorCall(11690);if (SvFAKE(sv)) {
            /* SV is copy-on-write */
	    sv_force_normal_flags(sv, 0);
        }
        else
            {/*33*/SensorCall(11691);Perl_croak_no_modify(aTHX);/*34*/}
    ;/*32*/}/*30*/}}

    SensorCall(11694);if (PL_encoding) {
	SensorCall(11693);if (!SvUTF8(sv)) {
	    /* XXX, here sv is utf8-ized as a side-effect!
	       If encoding.pm is used properly, almost string-generating
	       operations, including literal strings, chr(), input data, etc.
	       should have been utf8-ized already, right?
	    */
	    sv_recode_to_utf8(sv, PL_encoding);
	}
    }

    SensorCall(11695);s = SvPV(sv, len);
    SensorCall(11734);if (chomping) {
	SensorCall(11696);char *temp_buffer = NULL;
	SV *svrecode = NULL;

	SensorCall(11725);if (s && len) {
	    SensorCall(11697);s += --len;
	    SensorCall(11723);if (RsPARA(PL_rs)) {
		SensorCall(11698);if (*s != '\n')
		    {/*35*/SensorCall(11699);goto nope;/*36*/}
		SensorCall(11700);++SvIVX(retval);
		SensorCall(11702);while (len && s[-1] == '\n') {
		    SensorCall(11701);--len;
		    --s;
		    ++SvIVX(retval);
		}
	    }
	    else {
		SensorCall(11703);STRLEN rslen, rs_charlen;
		const char *rsptr = SvPV_const(PL_rs, rslen);

		rs_charlen = SvUTF8(PL_rs)
		    ? sv_len_utf8(PL_rs)
		    : rslen;

		SensorCall(11713);if (SvUTF8(PL_rs) != SvUTF8(sv)) {
		    /* Assumption is that rs is shorter than the scalar.  */
		    SensorCall(11704);if (SvUTF8(PL_rs)) {
			/* RS is utf8, scalar is 8 bit.  */
			bool is_utf8 = TRUE;
			SensorCall(11705);temp_buffer = (char*)bytes_from_utf8((U8*)rsptr,
							     &rslen, &is_utf8);
			SensorCall(11708);if (is_utf8) {
			    /* Cannot downgrade, therefore cannot possibly match
			     */
			    assert (temp_buffer == rsptr);
			    SensorCall(11706);temp_buffer = NULL;
			    SensorCall(11707);goto nope;
			}
			SensorCall(11709);rsptr = temp_buffer;
		    }
		    else {/*37*/SensorCall(11710);if (PL_encoding) {
			/* RS is 8 bit, encoding.pm is used.
			 * Do not recode PL_rs as a side-effect. */
			SensorCall(11711);svrecode = newSVpvn(rsptr, rslen);
			sv_recode_to_utf8(svrecode, PL_encoding);
			rsptr = SvPV_const(svrecode, rslen);
			rs_charlen = sv_len_utf8(svrecode);
		    }
		    else {
			/* RS is 8 bit, scalar is utf8.  */
			SensorCall(11712);temp_buffer = (char*)bytes_to_utf8((U8*)rsptr, &rslen);
			rsptr = temp_buffer;
		    ;/*38*/}}
		}
		SensorCall(11722);if (rslen == 1) {
		    SensorCall(11714);if (*s != *rsptr)
			{/*39*/SensorCall(11715);goto nope;/*40*/}
		    SensorCall(11716);++SvIVX(retval);
		}
		else {
		    SensorCall(11717);if (len < rslen - 1)
			{/*41*/SensorCall(11718);goto nope;/*42*/}
		    SensorCall(11719);len -= rslen - 1;
		    s -= rslen - 1;
		    SensorCall(11721);if (memNE(s, rsptr, rslen))
			{/*43*/SensorCall(11720);goto nope;/*44*/}
		    SvIVX(retval) += rs_charlen;
		}
	    }
	    SensorCall(11724);s = SvPV_force_nomg_nolen(sv);
	    SvCUR_set(sv, len);
	    *SvEND(sv) = '\0';
	    SvNIOK_off(sv);
	    SvSETMAGIC(sv);
	}
    nope:

	SvREFCNT_dec(svrecode);

	Safefree(temp_buffer);
    } else {
	SensorCall(11726);if (len && !SvPOK(sv))
	    s = SvPV_force_nomg(sv, len);
	SensorCall(11733);if (DO_UTF8(sv)) {
	    SensorCall(11727);if (s && len) {
		SensorCall(11728);char * const send = s + len;
		char * const start = s;
		s = send - 1;
		SensorCall(11730);while (s > start && UTF8_IS_CONTINUATION(*s))
		    {/*45*/SensorCall(11729);s--;/*46*/}
		SensorCall(11732);if (is_utf8_string((U8*)s, send - s)) {
		    sv_setpvn(retval, s, send - s);
		    SensorCall(11731);*s = '\0';
		    SvCUR_set(sv, s - start);
		    SvNIOK_off(sv);
		    SvUTF8_on(retval);
		}
	    }
	    else
		sv_setpvs(retval, "");
	}
	else if (s && len) {
	    s += --len;
	    sv_setpvn(retval, s, 1);
	    *s = '\0';
	    SvCUR_set(sv, len);
	    SvUTF8_off(sv);
	    SvNIOK_off(sv);
	}
	else
	    sv_setpvs(retval, "");
	SvSETMAGIC(sv);
    }
SensorCall(11735);}

PP(pp_schop)
{
SensorCall(11736);    dVAR; dSP; dTARGET;
    const bool chomping = PL_op->op_type == OP_SCHOMP;

    SensorCall(11737);if (chomping)
	sv_setiv(TARG, 0);
    do_chomp(TARG, TOPs, chomping);
    SETTARG;
    RETURN;
}

PP(pp_chop)
{
SensorCall(11738);    dVAR; dSP; dMARK; dTARGET; dORIGMARK;
    const bool chomping = PL_op->op_type == OP_CHOMP;

    SensorCall(11739);if (chomping)
	sv_setiv(TARG, 0);
    SensorCall(11740);while (MARK < SP)
	do_chomp(TARG, *++MARK, chomping);
    SP = ORIGMARK;
    XPUSHTARG;
    RETURN;
}

PP(pp_undef)
{
SensorCall(11741);    dVAR; dSP;
    SV *sv;

    SensorCall(11742);if (!PL_op->op_private) {
	EXTEND(SP, 1);
	RETPUSHUNDEF;
    }

    SensorCall(11743);sv = POPs;
    SensorCall(11744);if (!sv)
	RETPUSHUNDEF;

    SV_CHECK_THINKFIRST_COW_DROP(sv);

    SensorCall(11763);switch (SvTYPE(sv)) {
    case SVt_NULL:
	SensorCall(11745);break;
    case SVt_PVAV:
	av_undef(MUTABLE_AV(sv));
	SensorCall(11746);break;
    case SVt_PVHV:
	hv_undef(MUTABLE_HV(sv));
	SensorCall(11747);break;
    case SVt_PVCV:
	SensorCall(11748);if (cv_const_sv((const CV *)sv))
	    {/*393*/SensorCall(11749);Perl_ck_warner(aTHX_ packWARN(WARN_MISC),
                          "Constant subroutine %"SVf" undefined",
			   SVfARG(CvANON((const CV *)sv)
                             ? newSVpvs_flags("(anonymous)", SVs_TEMP)
                             : sv_2mortal(newSVhek(GvENAME_HEK(CvGV((const CV *)sv))))));/*394*/}
	/* FALLTHROUGH */
    case SVt_PVFM:
	{
	    /* let user-undef'd sub keep its identity */
	    SensorCall(11750);GV* const gv = CvGV((const CV *)sv);
	    cv_undef(MUTABLE_CV(sv));
	    CvGV_set(MUTABLE_CV(sv), gv);
	}
	SensorCall(11751);break;
    case SVt_PVGV:
	SensorCall(11752);if (SvFAKE(sv)) {
	    SvSetMagicSV(sv, &PL_sv_undef);
	    SensorCall(11753);break;
	}
	else {/*395*/SensorCall(11754);if (isGV_with_GP(sv)) {
	    SensorCall(11755);GP *gp;
            HV *stash;

            /* undef *Pkg::meth_name ... */
            bool method_changed
             =   GvCVu((const GV *)sv) && (stash = GvSTASH((const GV *)sv))
	      && HvENAME_get(stash);
            /* undef *Foo:: */
            SensorCall(11757);if((stash = GvHV((const GV *)sv))) {
                SensorCall(11756);if(HvENAME_get(stash))
                    SvREFCNT_inc_simple_void_NN(sv_2mortal((SV *)stash));
                else stash = NULL;
            }

	    gp_free(MUTABLE_GV(sv));
	    Newxz(gp, 1, GP);
	    GvGP_set(sv, gp_ref(gp));
	    GvSV(sv) = newSV(0);
	    GvLINE(sv) = CopLINE(PL_curcop);
	    GvEGV(sv) = MUTABLE_GV(sv);
	    GvMULTI_on(sv);

            SensorCall(11758);if(stash)
                mro_package_moved(NULL, stash, (const GV *)sv, 0);
            SensorCall(11759);stash = NULL;
            /* undef *Foo::ISA */
            SensorCall(11760);if( strEQ(GvNAME((const GV *)sv), "ISA")
             && (stash = GvSTASH((const GV *)sv))
             && (method_changed || HvENAME(stash)) )
                mro_isa_changed_in(stash);
            else if(method_changed)
                mro_method_changed_in(
                 GvSTASH((const GV *)sv)
                );

	    SensorCall(11761);break;
	;/*396*/}}
	/* FALL THROUGH */
    default:
	SensorCall(11762);if (SvTYPE(sv) >= SVt_PV && SvPVX_const(sv) && SvLEN(sv)) {
	    SvPV_free(sv);
	    SvPV_set(sv, NULL);
	    SvLEN_set(sv, 0);
	}
	SvOK_off(sv);
	SvSETMAGIC(sv);
    }

    RETPUSHUNDEF;
}

PP(pp_postinc)
{
SensorCall(11764);    dVAR; dSP; dTARGET;
    const bool inc =
	PL_op->op_type == OP_POSTINC || PL_op->op_type == OP_I_POSTINC;
    SensorCall(11766);if (SvTYPE(TOPs) >= SVt_PVAV || (isGV_with_GP(TOPs) && !SvFAKE(TOPs)))
	{/*211*/SensorCall(11765);Perl_croak_no_modify(aTHX);/*212*/}
    SensorCall(11767);if (SvROK(TOPs))
	TARG = sv_newmortal();
    sv_setsv(TARG, TOPs);
    SensorCall(11768);if (!SvREADONLY(TOPs) && SvIOK_notUV(TOPs) && !SvNOK(TOPs) && !SvPOK(TOPs)
        && SvIVX(TOPs) != (inc ? IV_MAX : IV_MIN))
    {
	SvIV_set(TOPs, SvIVX(TOPs) + (inc ? 1 : -1));
	SvFLAGS(TOPs) &= ~(SVp_NOK|SVp_POK);
    }
    else if (inc)
	sv_inc_nomg(TOPs);
    else sv_dec_nomg(TOPs);
    SvSETMAGIC(TOPs);
    /* special case for undef: see thread at 2003-03/msg00536.html in archive */
    SensorCall(11769);if (inc && !SvOK(TARG))
	sv_setiv(TARG, 0);
    SETs(TARG);
    {OP * ReplaceReturn1013 = NORMAL; SensorCall(11770); return ReplaceReturn1013;}
}

/* Ordinary operators. */

PP(pp_pow)
{
SensorCall(11771);    dVAR; dSP; dATARGET; SV *svl, *svr;
#ifdef PERL_PRESERVE_IVUV
    bool is_int = 0;
#endif
    tryAMAGICbin_MG(pow_amg, AMGf_assign|AMGf_numeric);
    svr = TOPs;
    svl = TOPm1s;
#ifdef PERL_PRESERVE_IVUV
    /* For integer to integer power, we do the calculation by hand wherever
       we're sure it is safe; otherwise we call pow() and try to convert to
       integer afterwards. */
    {
	SvIV_please_nomg(svr);
	SensorCall(11810);if (SvIOK(svr)) {
	    SvIV_please_nomg(svl);
	    SensorCall(11809);if (SvIOK(svl)) {
		SensorCall(11772);UV power;
		bool baseuok;
		UV baseuv;

		SensorCall(11778);if (SvUOK(svr)) {
		    SensorCall(11773);power = SvUVX(svr);
		} else {
		    SensorCall(11774);const IV iv = SvIVX(svr);
		    SensorCall(11777);if (iv >= 0) {
			SensorCall(11775);power = iv;
		    } else {
			SensorCall(11776);goto float_it; /* Can't do negative powers this way.  */
		    }
		}

		SensorCall(11779);baseuok = SvUOK(svl);
		SensorCall(11785);if (baseuok) {
		    SensorCall(11780);baseuv = SvUVX(svl);
		} else {
		    SensorCall(11781);const IV iv = SvIVX(svl);
		    SensorCall(11784);if (iv >= 0) {
			SensorCall(11782);baseuv = iv;
			baseuok = TRUE; /* effectively it's a UV now */
		    } else {
			SensorCall(11783);baseuv = -iv; /* abs, baseuok == false records sign */
		    }
		}
                /* now we have integer ** positive integer. */
                SensorCall(11786);is_int = 1;

                /* foo & (foo - 1) is zero only for a power of 2.  */
                SensorCall(11808);if (!(baseuv & (baseuv - 1))) {
                    /* We are raising power-of-2 to a positive integer.
                       The logic here will work for any base (even non-integer
                       bases) but it can be less accurate than
                       pow (base,power) or exp (power * log (base)) when the
                       intermediate values start to spill out of the mantissa.
                       With powers of 2 we know this can't happen.
                       And powers of 2 are the favourite thing for perl
                       programmers to notice ** not doing what they mean. */
                    SensorCall(11787);NV result = 1.0;
                    NV base = baseuok ? baseuv : -(NV)baseuv;

		    SensorCall(11789);if (power & 1) {
			SensorCall(11788);result *= base;
		    }
		    SensorCall(11793);while (power >>= 1) {
			SensorCall(11790);base *= base;
			SensorCall(11792);if (power & 1) {
			    SensorCall(11791);result *= base;
			}
		    }
                    SP--;
                    SETn( result );
                    SvIV_please_nomg(svr);
                    RETURN;
		} else {
		    SensorCall(11794);register unsigned int highbit = 8 * sizeof(UV);
		    register unsigned int diff = 8 * sizeof(UV);
		    SensorCall(11798);while (diff >>= 1) {
			SensorCall(11795);highbit -= diff;
			SensorCall(11797);if (baseuv >> highbit) {
			    SensorCall(11796);highbit += diff;
			}
		    }
		    /* we now have baseuv < 2 ** highbit */
		    SensorCall(11807);if (power * highbit <= 8 * sizeof(UV)) {
			/* result will definitely fit in UV, so use UV math
			   on same algorithm as above */
			SensorCall(11799);register UV result = 1;
			register UV base = baseuv;
			const bool odd_power = cBOOL(power & 1);
			SensorCall(11801);if (odd_power) {
			    SensorCall(11800);result *= base;
			}
			SensorCall(11805);while (power >>= 1) {
			    SensorCall(11802);base *= base;
			    SensorCall(11804);if (power & 1) {
				SensorCall(11803);result *= base;
			    }
			}
			SP--;
			SensorCall(11806);if (baseuok || !odd_power)
			    /* answer is positive */
			    SETu( result );
			else if (result <= (UV)IV_MAX)
			    /* answer negative, fits in IV */
			    SETi( -(IV)result );
			else if (result == (UV)IV_MIN) 
			    /* 2's complement assumption: special case IV_MIN */
			    SETi( IV_MIN );
			else
			    /* answer negative, doesn't fit */
			    SETn( -(NV)result );
			RETURN;
		    } 
		}
	    }
	}
    }
  float_it:
#endif    
    {
	SensorCall(11811);NV right = SvNV_nomg(svr);
	NV left  = SvNV_nomg(svl);
	(void)POPs;

#if defined(USE_LONG_DOUBLE) && defined(HAS_AIX_POWL_NEG_BASE_BUG)
    /*
    We are building perl with long double support and are on an AIX OS
    afflicted with a powl() function that wrongly returns NaNQ for any
    negative base.  This was reported to IBM as PMR #23047-379 on
    03/06/2006.  The problem exists in at least the following versions
    of AIX and the libm fileset, and no doubt others as well:

	AIX 4.3.3-ML10      bos.adt.libm 4.3.3.50
	AIX 5.1.0-ML04      bos.adt.libm 5.1.0.29
	AIX 5.2.0           bos.adt.libm 5.2.0.85

    So, until IBM fixes powl(), we provide the following workaround to
    handle the problem ourselves.  Our logic is as follows: for
    negative bases (left), we use fmod(right, 2) to check if the
    exponent is an odd or even integer:

	- if odd,  powl(left, right) == -powl(-left, right)
	- if even, powl(left, right) ==  powl(-left, right)

    If the exponent is not an integer, the result is rightly NaNQ, so
    we just return that (as NV_NAN).
    */

	if (left < 0.0) {
	    NV mod2 = Perl_fmod( right, 2.0 );
	    if (mod2 == 1.0 || mod2 == -1.0) {	/* odd integer */
		SETn( -Perl_pow( -left, right) );
	    } else if (mod2 == 0.0) {		/* even integer */
		SETn( Perl_pow( -left, right) );
	    } else {				/* fractional power */
		SETn( NV_NAN );
	    }
	} else {
	    SETn( Perl_pow( left, right) );
	}
#else
	SETn( Perl_pow( left, right) );
#endif  /* HAS_AIX_POWL_NEG_BASE_BUG */

#ifdef PERL_PRESERVE_IVUV
	SensorCall(11812);if (is_int)
	    SvIV_please_nomg(svr);
#endif
	RETURN;
    }
}

PP(pp_multiply)
{
SensorCall(11813);    dVAR; dSP; dATARGET; SV *svl, *svr;
    tryAMAGICbin_MG(mult_amg, AMGf_assign|AMGf_numeric);
    svr = TOPs;
    svl = TOPm1s;
#ifdef PERL_PRESERVE_IVUV
    SvIV_please_nomg(svr);
    SensorCall(11843);if (SvIOK(svr)) {
	/* Unless the left argument is integer in range we are going to have to
	   use NV maths. Hence only attempt to coerce the right argument if
	   we know the left is integer.  */
	/* Left operand is defined, so is it IV? */
	SvIV_please_nomg(svl);
	SensorCall(11842);if (SvIOK(svl)) {
	    bool auvok = SvUOK(svl);
	    bool buvok = SvUOK(svr);
	    SensorCall(11814);const UV topmask = (~ (UV)0) << (4 * sizeof (UV));
	    const UV botmask = ~((~ (UV)0) << (4 * sizeof (UV)));
	    UV alow;
	    UV ahigh;
	    UV blow;
	    UV bhigh;

	    SensorCall(11820);if (auvok) {
		SensorCall(11815);alow = SvUVX(svl);
	    } else {
		SensorCall(11816);const IV aiv = SvIVX(svl);
		SensorCall(11819);if (aiv >= 0) {
		    SensorCall(11817);alow = aiv;
		    auvok = TRUE; /* effectively it's a UV now */
		} else {
		    SensorCall(11818);alow = -aiv; /* abs, auvok == false records sign */
		}
	    }
	    SensorCall(11826);if (buvok) {
		SensorCall(11821);blow = SvUVX(svr);
	    } else {
		SensorCall(11822);const IV biv = SvIVX(svr);
		SensorCall(11825);if (biv >= 0) {
		    SensorCall(11823);blow = biv;
		    buvok = TRUE; /* effectively it's a UV now */
		} else {
		    SensorCall(11824);blow = -biv; /* abs, buvok == false records sign */
		}
	    }

	    /* If this does sign extension on unsigned it's time for plan B  */
	    SensorCall(11827);ahigh = alow >> (4 * sizeof (UV));
	    alow &= botmask;
	    bhigh = blow >> (4 * sizeof (UV));
	    blow &= botmask;
	    SensorCall(11841);if (ahigh && bhigh) {
		NOOP;
		/* eg 32 bit is at least 0x10000 * 0x10000 == 0x100000000
		   which is overflow. Drop to NVs below.  */
	    } else {/*175*/SensorCall(11828);if (!ahigh && !bhigh) {
		/* eg 32 bit is at most 0xFFFF * 0xFFFF == 0xFFFE0001
		   so the unsigned multiply cannot overflow.  */
		SensorCall(11829);const UV product = alow * blow;
		SensorCall(11831);if (auvok == buvok) {
		    /* -ve * -ve or +ve * +ve gives a +ve result.  */
		    SP--;
		    SETu( product );
		    RETURN;
		} else {/*177*/SensorCall(11830);if (product <= (UV)IV_MIN) {
		    /* 2s complement assumption that (UV)-IV_MIN is correct.  */
		    /* -ve result, which could overflow an IV  */
		    SP--;
		    SETi( -(IV)product );
		    RETURN;
		;/*178*/}} /* else drop to NVs below. */
	    } else {
		/* One operand is large, 1 small */
		SensorCall(11832);UV product_middle;
		SensorCall(11834);if (bhigh) {
		    /* swap the operands */
		    SensorCall(11833);ahigh = bhigh;
		    bhigh = blow; /* bhigh now the temp var for the swap */
		    blow = alow;
		    alow = bhigh;
		}
		/* now, ((ahigh * blow) << half_UV_len) + (alow * blow)
		   multiplies can't overflow. shift can, add can, -ve can.  */
		SensorCall(11835);product_middle = ahigh * blow;
		SensorCall(11840);if (!(product_middle & topmask)) {
		    /* OK, (ahigh * blow) won't lose bits when we shift it.  */
		    SensorCall(11836);UV product_low;
		    product_middle <<= (4 * sizeof (UV));
		    product_low = alow * blow;

		    /* as for pp_add, UV + something mustn't get smaller.
		       IIRC ANSI mandates this wrapping *behaviour* for
		       unsigned whatever the actual representation*/
		    product_low += product_middle;
		    SensorCall(11839);if (product_low >= product_middle) {
			/* didn't overflow */
			SensorCall(11837);if (auvok == buvok) {
			    /* -ve * -ve or +ve * +ve gives a +ve result.  */
			    SP--;
			    SETu( product_low );
			    RETURN;
			} else {/*179*/SensorCall(11838);if (product_low <= (UV)IV_MIN) {
			    /* 2s complement assumption again  */
			    /* -ve result, which could overflow an IV  */
			    SP--;
			    SETi( -(IV)product_low );
			    RETURN;
			;/*180*/}} /* else drop to NVs below. */
		    }
		} /* product_middle too large */
	    ;/*176*/}} /* ahigh && bhigh */
	} /* SvIOK(svl) */
    } /* SvIOK(svr) */
#endif
    {
      SensorCall(11844);NV right = SvNV_nomg(svr);
      NV left  = SvNV_nomg(svl);
      (void)POPs;
      SETn( left * right );
      RETURN;
    }
}

PP(pp_divide)
{
SensorCall(11845);    dVAR; dSP; dATARGET; SV *svl, *svr;
    tryAMAGICbin_MG(div_amg, AMGf_assign|AMGf_numeric);
    svr = TOPs;
    svl = TOPm1s;
    /* Only try to do UV divide first
       if ((SLOPPYDIVIDE is true) or
           (PERL_PRESERVE_IVUV is true and one or both SV is a UV too large
            to preserve))
       The assumption is that it is better to use floating point divide
       whenever possible, only doing integer divide first if we can't be sure.
       If NV_PRESERVES_UV is true then we know at compile time that no UV
       can be too large to preserve, so don't need to compile the code to
       test the size of UVs.  */

#ifdef SLOPPYDIVIDE
#  define PERL_TRY_UV_DIVIDE
    /* ensure that 20./5. == 4. */
#else
#  ifdef PERL_PRESERVE_IVUV
#    ifndef NV_PRESERVES_UV
#      define PERL_TRY_UV_DIVIDE
#    endif
#  endif
#endif

#ifdef PERL_TRY_UV_DIVIDE
    SvIV_please_nomg(svr);
    SensorCall(11866);if (SvIOK(svr)) {
        SvIV_please_nomg(svl);
        SensorCall(11865);if (SvIOK(svl)) {
            bool left_non_neg = SvUOK(svl);
            bool right_non_neg = SvUOK(svr);
            SensorCall(11846);UV left;
            UV right;

            SensorCall(11852);if (right_non_neg) {
                SensorCall(11847);right = SvUVX(svr);
            }
	    else {
		SensorCall(11848);const IV biv = SvIVX(svr);
                SensorCall(11851);if (biv >= 0) {
                    SensorCall(11849);right = biv;
                    right_non_neg = TRUE; /* effectively it's a UV now */
                }
		else {
                    SensorCall(11850);right = -biv;
                }
            }
            /* historically undef()/0 gives a "Use of uninitialized value"
               warning before dieing, hence this test goes here.
               If it were immediately before the second SvIV_please, then
               DIE() would be invoked before left was even inspected, so
               no inspection would give no warning.  */
            SensorCall(11853);if (right == 0)
                DIE(aTHX_ "Illegal division by zero");

            SensorCall(11859);if (left_non_neg) {
                SensorCall(11854);left = SvUVX(svl);
            }
	    else {
		SensorCall(11855);const IV aiv = SvIVX(svl);
                SensorCall(11858);if (aiv >= 0) {
                    SensorCall(11856);left = aiv;
                    left_non_neg = TRUE; /* effectively it's a UV now */
                }
		else {
                    SensorCall(11857);left = -aiv;
                }
            }

            SensorCall(11864);if (left >= right
#ifdef SLOPPYDIVIDE
                /* For sloppy divide we always attempt integer division.  */
#else
                /* Otherwise we only attempt it if either or both operands
                   would not be preserved by an NV.  If both fit in NVs
                   we fall through to the NV divide code below.  However,
                   as left >= right to ensure integer result here, we know that
                   we can skip the test on the right operand - right big
                   enough not to be preserved can't get here unless left is
                   also too big.  */

                && (left > ((UV)1 << NV_PRESERVES_UV_BITS))
#endif
                ) {
                /* Integer division can't overflow, but it can be imprecise.  */
		SensorCall(11860);const UV result = left / right;
                SensorCall(11863);if (result * right == left) {
                    SP--; /* result is valid */
                    SensorCall(11861);if (left_non_neg == right_non_neg) {
                        /* signs identical, result is positive.  */
                        SETu( result );
                        RETURN;
                    }
                    /* 2s complement assumption */
                    SensorCall(11862);if (result <= (UV)IV_MIN)
                        SETi( -(IV)result );
                    else {
                        /* It's exact but too negative for IV. */
                        SETn( -(NV)result );
                    }
                    RETURN;
                } /* tried integer divide but it was not an integer result */
            } /* else (PERL_ABS(result) < 1.0) or (both UVs in range for NV) */
        } /* left wasn't SvIOK */
    } /* right wasn't SvIOK */
#endif /* PERL_TRY_UV_DIVIDE */
    {
	SensorCall(11867);NV right = SvNV_nomg(svr);
	NV left  = SvNV_nomg(svl);
	(void)POPs;(void)POPs;
#if defined(NAN_COMPARE_BROKEN) && defined(Perl_isnan)
	if (! Perl_isnan(right) && right == 0.0)
#else
	SensorCall(11868);if (right == 0.0)
#endif
	    DIE(aTHX_ "Illegal division by zero");
	PUSHn( left / right );
	RETURN;
    }
}

PP(pp_modulo)
{
SensorCall(11869);    dVAR; dSP; dATARGET;
    tryAMAGICbin_MG(modulo_amg, AMGf_assign|AMGf_numeric);
    {
	UV left  = 0;
	UV right = 0;
	bool left_neg = FALSE;
	bool right_neg = FALSE;
	bool use_double = FALSE;
	bool dright_valid = FALSE;
	NV dright = 0.0;
	NV dleft  = 0.0;
	SV * const svr = TOPs;
	SV * const svl = TOPm1s;
	SvIV_please_nomg(svr);
        SensorCall(11883);if (SvIOK(svr)) {
            SensorCall(11870);right_neg = !SvUOK(svr);
            SensorCall(11876);if (!right_neg) {
                SensorCall(11871);right = SvUVX(svr);
            } else {
		SensorCall(11872);const IV biv = SvIVX(svr);
                SensorCall(11875);if (biv >= 0) {
                    SensorCall(11873);right = biv;
                    right_neg = FALSE; /* effectively it's a UV now */
                } else {
                    SensorCall(11874);right = -biv;
                }
            }
        }
        else {
	    SensorCall(11877);dright = SvNV_nomg(svr);
	    right_neg = dright < 0;
	    SensorCall(11879);if (right_neg)
		{/*161*/SensorCall(11878);dright = -dright;/*162*/}
            SensorCall(11882);if (dright < UV_MAX_P1) {
                SensorCall(11880);right = U_V(dright);
                dright_valid = TRUE; /* In case we need to use double below.  */
            } else {
                SensorCall(11881);use_double = TRUE;
            }
	}

        /* At this point use_double is only true if right is out of range for
           a UV.  In range NV has been rounded down to nearest UV and
           use_double false.  */
        SvIV_please_nomg(svl);
	SensorCall(11902);if (!use_double && SvIOK(svl)) {
            SensorCall(11884);if (SvIOK(svl)) {
                SensorCall(11885);left_neg = !SvUOK(svl);
                SensorCall(11891);if (!left_neg) {
                    SensorCall(11886);left = SvUVX(svl);
                } else {
		    SensorCall(11887);const IV aiv = SvIVX(svl);
                    SensorCall(11890);if (aiv >= 0) {
                        SensorCall(11888);left = aiv;
                        left_neg = FALSE; /* effectively it's a UV now */
                    } else {
                        SensorCall(11889);left = -aiv;
                    }
                }
            }
        }
	else {
	    SensorCall(11892);dleft = SvNV_nomg(svl);
	    left_neg = dleft < 0;
	    SensorCall(11894);if (left_neg)
		{/*163*/SensorCall(11893);dleft = -dleft;/*164*/}

            /* This should be exactly the 5.6 behaviour - if left and right are
               both in range for UV then use U_V() rather than floor.  */
	    SensorCall(11901);if (!use_double) {
                SensorCall(11895);if (dleft < UV_MAX_P1) {
                    /* right was in range, so is dleft, so use UVs not double.
                     */
                    SensorCall(11896);left = U_V(dleft);
                }
                /* left is out of range for UV, right was in range, so promote
                   right (back) to double.  */
                else {
                    /* The +0.5 is used in 5.6 even though it is not strictly
                       consistent with the implicit +0 floor in the U_V()
                       inside the #if 1. */
                    SensorCall(11897);dleft = Perl_floor(dleft + 0.5);
                    use_double = TRUE;
                    SensorCall(11900);if (dright_valid)
                        {/*165*/SensorCall(11898);dright = Perl_floor(dright + 0.5);/*166*/}
                    else
                        {/*167*/SensorCall(11899);dright = right;/*168*/}
                }
            }
        }
	SensorCall(11903);sp -= 2;
	SensorCall(11918);if (use_double) {
	    SensorCall(11904);NV dans;

	    SensorCall(11905);if (!dright)
		DIE(aTHX_ "Illegal modulus zero");

	    SensorCall(11906);dans = Perl_fmod(dleft, dright);
	    SensorCall(11908);if ((left_neg != right_neg) && dans)
		{/*169*/SensorCall(11907);dans = dright - dans;/*170*/}
	    SensorCall(11910);if (right_neg)
		{/*171*/SensorCall(11909);dans = -dans;/*172*/}
	    sv_setnv(TARG, dans);
	}
	else {
	    SensorCall(11911);UV ans;

	    SensorCall(11912);if (!right)
		DIE(aTHX_ "Illegal modulus zero");

	    SensorCall(11913);ans = left % right;
	    SensorCall(11915);if ((left_neg != right_neg) && ans)
		{/*173*/SensorCall(11914);ans = right - ans;/*174*/}
	    SensorCall(11917);if (right_neg) {
		/* XXX may warn: unary minus operator applied to unsigned type */
		/* could change -foo to be (~foo)+1 instead	*/
		SensorCall(11916);if (ans <= ~((UV)IV_MAX)+1)
		    sv_setiv(TARG, ~ans+1);
		else
		    sv_setnv(TARG, -(NV)ans);
	    }
	    else
		sv_setuv(TARG, ans);
	}
	PUSHTARG;
	RETURN;
    }
}

PP(pp_repeat)
{
SensorCall(11919);    dVAR; dSP; dATARGET;
    register IV count;
    SV *sv;

    SensorCall(11922);if (GIMME == G_ARRAY && PL_op->op_private & OPpREPEAT_DOLIST) {
	/* TODO: think of some way of doing list-repeat overloading ??? */
	SensorCall(11920);sv = POPs;
	SvGETMAGIC(sv);
    }
    else {
	tryAMAGICbin_MG(repeat_amg, AMGf_assign);
	SensorCall(11921);sv = POPs;
    }

    SensorCall(11931);if (SvIOKp(sv)) {
	 SensorCall(11923);if (SvUOK(sv)) {
	      SensorCall(11924);const UV uv = SvUV_nomg(sv);
	      SensorCall(11926);if (uv > IV_MAX)
		   count = IV_MAX; /* The best we can do? */
	      else
		   {/*233*/SensorCall(11925);count = uv;/*234*/}
	 } else {
	      SensorCall(11927);const IV iv = SvIV_nomg(sv);
	      SensorCall(11930);if (iv < 0)
		   {/*235*/SensorCall(11928);count = 0;/*236*/}
	      else
		   {/*237*/SensorCall(11929);count = iv;/*238*/}
	 }
    }
    else if (SvNOKp(sv)) {
	 const NV nv = SvNV_nomg(sv);
	 if (nv < 0.0)
	      {/*239*/count = 0;/*240*/}
	 else
	      {/*241*/count = (IV)nv;/*242*/}
    }
    else
	 count = SvIV_nomg(sv);

    SensorCall(11950);if (GIMME == G_ARRAY && PL_op->op_private & OPpREPEAT_DOLIST) {
	dMARK;
	SensorCall(11932);static const char oom_list_extend[] = "Out of memory during list extend";
	const I32 items = SP - MARK;
	const I32 max = items * count;

	MEM_WRAP_CHECK_1(max, SV*, oom_list_extend);
	/* Did the max computation overflow? */
	SensorCall(11934);if (items > 0 && max > 0 && (max < items || max < count))
	   {/*243*/SensorCall(11933);Perl_croak(aTHX_ oom_list_extend);/*244*/}
	MEXTEND(MARK, max);
	SensorCall(11938);if (count > 1) {
	    SensorCall(11935);while (SP > MARK) {
#if 0
	      /* This code was intended to fix 20010809.028:

	         $x = 'abcd';
		 for (($x =~ /./g) x 2) {
		     print chop; # "abcdabcd" expected as output.
		 }

	       * but that change (#11635) broke this code:

	       $x = [("foo")x2]; # only one "foo" ended up in the anonlist.

	       * I can't think of a better fix that doesn't introduce
	       * an efficiency hit by copying the SVs. The stack isn't
	       * refcounted, and mortalisation obviously doesn't
	       * Do The Right Thing when the stack has more than
	       * one pointer to the same mortal value.
	       * .robin.
	       */
		if (*SP) {
		    *SP = sv_2mortal(newSVsv(*SP));
		    SvREADONLY_on(*SP);
		}
#else
               SensorCall(11936);if (*SP)
		   SvTEMP_off((*SP));
#endif
		SP--;
	    }
	    MARK++;
	    repeatcpy((char*)(MARK + items), (char*)MARK,
		items * sizeof(const SV *), count - 1);
	    SP += max;
	}
	else {/*245*/SensorCall(11937);if (count <= 0)
	    SP -= items;/*246*/}
    }
    else {	/* Note: mark already snarfed by pp_list */
	SensorCall(11939);SV * const tmpstr = POPs;
	STRLEN len;
	bool isutf;
	static const char oom_string_extend[] =
	  "Out of memory during string extend";

	SensorCall(11940);if (TARG != tmpstr)
	    sv_setsv_nomg(TARG, tmpstr);
	SvPV_force_nomg(TARG, len);
	SensorCall(11941);isutf = DO_UTF8(TARG);
	SensorCall(11947);if (count != 1) {
	    SensorCall(11942);if (count < 1)
		SvCUR_set(TARG, 0);
	    else {
		SensorCall(11943);const STRLEN max = (UV)count * len;
		SensorCall(11945);if (len > MEM_SIZE_MAX / count)
		     {/*247*/SensorCall(11944);Perl_croak(aTHX_ oom_string_extend);/*248*/}
	        MEM_WRAP_CHECK_1(max, char, oom_string_extend);
		SvGROW(TARG, max + 1);
		repeatcpy(SvPVX(TARG) + len, SvPVX(TARG), len, count - 1);
		SvCUR_set(TARG, SvCUR(TARG) * count);
	    }
	    SensorCall(11946);*SvEND(TARG) = '\0';
	}
	SensorCall(11948);if (isutf)
	    (void)SvPOK_only_UTF8(TARG);
	else
	    (void)SvPOK_only(TARG);

	SensorCall(11949);if (PL_op->op_private & OPpREPEAT_DOLIST) {
	    /* The parser saw this as a list repeat, and there
	       are probably several items on the stack. But we're
	       in scalar context, and there's no pp_list to save us
	       now. So drop the rest of the items -- robin@kitsite.com
	     */
	    dMARK;
	    SP = MARK;
	}
	PUSHTARG;
    }
    RETURN;
}

PP(pp_subtract)
{
SensorCall(11951);    dVAR; dSP; dATARGET; bool useleft; SV *svl, *svr;
    tryAMAGICbin_MG(subtr_amg, AMGf_assign|AMGf_numeric);
    svr = TOPs;
    svl = TOPm1s;
    useleft = USE_LEFT(svl);
#ifdef PERL_PRESERVE_IVUV
    /* See comments in pp_add (in pp_hot.c) about Overflow, and how
       "bad things" happen if you rely on signed integers wrapping.  */
    SvIV_please_nomg(svr);
    SensorCall(11983);if (SvIOK(svr)) {
	/* Unless the left argument is integer in range we are going to have to
	   use NV maths. Hence only attempt to coerce the right argument if
	   we know the left is integer.  */
	SensorCall(11952);register UV auv = 0;
	bool auvok = FALSE;
	bool a_valid = 0;

	SensorCall(11961);if (!useleft) {
	    SensorCall(11953);auv = 0;
	    a_valid = auvok = 1;
	    /* left operand is undef, treat as zero.  */
	} else {
	    /* Left operand is defined, so is it IV? */
	    SvIV_please_nomg(svl);
	    SensorCall(11960);if (SvIOK(svl)) {
		SensorCall(11954);if ((auvok = SvUOK(svl)))
		    auv = SvUVX(svl);
		else {
		    SensorCall(11955);register const IV aiv = SvIVX(svl);
		    SensorCall(11958);if (aiv >= 0) {
			SensorCall(11956);auv = aiv;
			auvok = 1;	/* Now acting as a sign flag.  */
		    } else { /* 2s complement assumption for IV_MIN */
			SensorCall(11957);auv = (UV)-aiv;
		    }
		}
		SensorCall(11959);a_valid = 1;
	    }
	}
	SensorCall(11982);if (a_valid) {
	    bool result_good = 0;
	    SensorCall(11962);UV result;
	    register UV buv;
	    bool buvok = SvUOK(svr);
	
	    SensorCall(11967);if (buvok)
		buv = SvUVX(svr);
	    else {
		SensorCall(11963);register const IV biv = SvIVX(svr);
		SensorCall(11966);if (biv >= 0) {
		    SensorCall(11964);buv = biv;
		    buvok = 1;
		} else
		    {/*375*/SensorCall(11965);buv = (UV)-biv;/*376*/}
	    }
	    /* ?uvok if value is >= 0. basically, flagged as UV if it's +ve,
	       else "IV" now, independent of how it came in.
	       if a, b represents positive, A, B negative, a maps to -A etc
	       a - b =>  (a - b)
	       A - b => -(a + b)
	       a - B =>  (a + b)
	       A - B => -(a - b)
	       all UV maths. negate result if A negative.
	       subtract if signs same, add if signs differ. */

	    SensorCall(11978);if (auvok ^ buvok) {
		/* Signs differ.  */
		SensorCall(11968);result = auv + buv;
		SensorCall(11970);if (result >= auv)
		    {/*377*/SensorCall(11969);result_good = 1;/*378*/}
	    } else {
		/* Signs same */
		SensorCall(11971);if (auv >= buv) {
		    SensorCall(11972);result = auv - buv;
		    /* Must get smaller */
		    SensorCall(11974);if (result <= auv)
			{/*379*/SensorCall(11973);result_good = 1;/*380*/}
		} else {
		    SensorCall(11975);result = buv - auv;
		    SensorCall(11977);if (result <= buv) {
			/* result really should be -(auv-buv). as its negation
			   of true value, need to swap our result flag  */
			SensorCall(11976);auvok = !auvok;
			result_good = 1;
		    }
		}
	    }
	    SensorCall(11981);if (result_good) {
		SP--;
		SensorCall(11980);if (auvok)
		    SETu( result );
		else {
		    /* Negate result */
		    SensorCall(11979);if (result <= (UV)IV_MIN)
			SETi( -(IV)result );
		    else {
			/* result valid, but out of range for IV.  */
			SETn( -(NV)result );
		    }
		}
		RETURN;
	    } /* Overflow, drop through to NVs.  */
	}
    }
#endif
    {
	SensorCall(11984);NV value = SvNV_nomg(svr);
	(void)POPs;

	SensorCall(11985);if (!useleft) {
	    /* left operand is undef, treat as zero - value */
	    SETn(-value);
	    RETURN;
	}
	SETn( SvNV_nomg(svl) - value );
	RETURN;
    }
}

PP(pp_left_shift)
{
SensorCall(11986);    dVAR; dSP; dATARGET; SV *svl, *svr;
    tryAMAGICbin_MG(lshift_amg, AMGf_assign|AMGf_numeric);
    svr = POPs;
    svl = TOPs;
    {
      const IV shift = SvIV_nomg(svr);
      SensorCall(11989);if (PL_op->op_private & HINT_INTEGER) {
	SensorCall(11987);const IV i = SvIV_nomg(svl);
	SETi(i << shift);
      }
      else {
	SensorCall(11988);const UV u = SvUV_nomg(svl);
	SETu(u << shift);
      }
      RETURN;
    }
}

PP(pp_right_shift)
{
SensorCall(11990);    dVAR; dSP; dATARGET; SV *svl, *svr;
    tryAMAGICbin_MG(rshift_amg, AMGf_assign|AMGf_numeric);
    svr = POPs;
    svl = TOPs;
    {
      const IV shift = SvIV_nomg(svr);
      SensorCall(11993);if (PL_op->op_private & HINT_INTEGER) {
	SensorCall(11991);const IV i = SvIV_nomg(svl);
	SETi(i >> shift);
      }
      else {
	SensorCall(11992);const UV u = SvUV_nomg(svl);
	SETu(u >> shift);
      }
      RETURN;
    }
}

PP(pp_lt)
{
SensorCall(11994);    dVAR; dSP;
    SV *left, *right;

    tryAMAGICbin_MG(lt_amg, AMGf_set|AMGf_numeric);
    right = POPs;
    left  = TOPs;
    SETs(boolSV(
	(SvIOK_notUV(left) && SvIOK_notUV(right))
	? (SvIVX(left) < SvIVX(right))
	: (do_ncmp(left, right) == -1)
    ));
    RETURN;
}

PP(pp_gt)
{
SensorCall(11995);    dVAR; dSP;
    SV *left, *right;

    tryAMAGICbin_MG(gt_amg, AMGf_set|AMGf_numeric);
    right = POPs;
    left  = TOPs;
    SETs(boolSV(
	(SvIOK_notUV(left) && SvIOK_notUV(right))
	? (SvIVX(left) > SvIVX(right))
	: (do_ncmp(left, right) == 1)
    ));
    RETURN;
}

PP(pp_le)
{
SensorCall(11996);    dVAR; dSP;
    SV *left, *right;

    tryAMAGICbin_MG(le_amg, AMGf_set|AMGf_numeric);
    right = POPs;
    left  = TOPs;
    SETs(boolSV(
	(SvIOK_notUV(left) && SvIOK_notUV(right))
	? (SvIVX(left) <= SvIVX(right))
	: (do_ncmp(left, right) <= 0)
    ));
    RETURN;
}

PP(pp_ge)
{
SensorCall(11997);    dVAR; dSP;
    SV *left, *right;

    tryAMAGICbin_MG(ge_amg, AMGf_set|AMGf_numeric);
    right = POPs;
    left  = TOPs;
    SETs(boolSV(
	(SvIOK_notUV(left) && SvIOK_notUV(right))
	? (SvIVX(left) >= SvIVX(right))
	: ( (do_ncmp(left, right) & 2) == 0)
    ));
    RETURN;
}

PP(pp_ne)
{
SensorCall(11998);    dVAR; dSP;
    SV *left, *right;

    tryAMAGICbin_MG(ne_amg, AMGf_set|AMGf_numeric);
    right = POPs;
    left  = TOPs;
    SETs(boolSV(
	(SvIOK_notUV(left) && SvIOK_notUV(right))
	? (SvIVX(left) != SvIVX(right))
	: (do_ncmp(left, right) != 0)
    ));
    RETURN;
}

/* compare left and right SVs. Returns:
 * -1: <
 *  0: ==
 *  1: >
 *  2: left or right was a NaN
 */
I32
Perl_do_ncmp(pTHX_ SV* const left, SV * const right)
{
    dVAR;

    PERL_ARGS_ASSERT_DO_NCMP;
#ifdef PERL_PRESERVE_IVUV
    SvIV_please_nomg(right);
    /* Fortunately it seems NaN isn't IOK */
    SensorCall(12017);if (SvIOK(right)) {
	SvIV_please_nomg(left);
	SensorCall(12016);if (SvIOK(left)) {
	    SensorCall(11999);if (!SvUOK(left)) {
		SensorCall(12000);const IV leftiv = SvIVX(left);
		SensorCall(12003);if (!SvUOK(right)) {
		    /* ## IV <=> IV ## */
		    SensorCall(12001);const IV rightiv = SvIVX(right);
		    {I32  ReplaceReturn1012 = (leftiv > rightiv) - (leftiv < rightiv); SensorCall(12002); return ReplaceReturn1012;}
		}
		/* ## IV <=> UV ## */
		SensorCall(12005);if (leftiv < 0)
		    /* As (b) is a UV, it's >=0, so it must be < */
		    {/*1*/{I32  ReplaceReturn1011 = -1; SensorCall(12004); return ReplaceReturn1011;}/*2*/}
		{
		    SensorCall(12006);const UV rightuv = SvUVX(right);
		    {I32  ReplaceReturn1010 = ((UV)leftiv > rightuv) - ((UV)leftiv < rightuv); SensorCall(12007); return ReplaceReturn1010;}
		}
	    }

	    SensorCall(12010);if (SvUOK(right)) {
		/* ## UV <=> UV ## */
		SensorCall(12008);const UV leftuv = SvUVX(left);
		const UV rightuv = SvUVX(right);
		{I32  ReplaceReturn1009 = (leftuv > rightuv) - (leftuv < rightuv); SensorCall(12009); return ReplaceReturn1009;}
	    }
	    /* ## UV <=> IV ## */
	    {
		SensorCall(12011);const IV rightiv = SvIVX(right);
		SensorCall(12013);if (rightiv < 0)
		    /* As (a) is a UV, it's >=0, so it cannot be < */
		    {/*3*/{I32  ReplaceReturn1008 = 1; SensorCall(12012); return ReplaceReturn1008;}/*4*/}
		{
		    SensorCall(12014);const UV leftuv = SvUVX(left);
		    {I32  ReplaceReturn1007 = (leftuv > (UV)rightiv) - (leftuv < (UV)rightiv); SensorCall(12015); return ReplaceReturn1007;}
		}
	    }
	    /* NOTREACHED */
	}
    }
#endif
    {
      SensorCall(12018);NV const rnv = SvNV_nomg(right);
      NV const lnv = SvNV_nomg(left);

#if defined(NAN_COMPARE_BROKEN) && defined(Perl_isnan)
      if (Perl_isnan(lnv) || Perl_isnan(rnv)) {
	  return 2;
       }
      return (lnv > rnv) - (lnv < rnv);
#else
      SensorCall(12020);if (lnv < rnv)
	{/*5*/{I32  ReplaceReturn1006 = -1; SensorCall(12019); return ReplaceReturn1006;}/*6*/}
      SensorCall(12022);if (lnv > rnv)
	{/*7*/{I32  ReplaceReturn1005 = 1; SensorCall(12021); return ReplaceReturn1005;}/*8*/}
      SensorCall(12024);if (lnv == rnv)
	{/*9*/{I32  ReplaceReturn1004 = 0; SensorCall(12023); return ReplaceReturn1004;}/*10*/}
      {I32  ReplaceReturn1003 = 2; SensorCall(12025); return ReplaceReturn1003;}
#endif
    }
}


PP(pp_ncmp)
{
SensorCall(12026);    dVAR; dSP;
    SV *left, *right;
    I32 value;
    tryAMAGICbin_MG(ncmp_amg, AMGf_numeric);
    right = POPs;
    left  = TOPs;
    value = do_ncmp(left, right);
    SensorCall(12027);if (value == 2) {
	SETs(&PL_sv_undef);
    }
    else {
	dTARGET;
	SETi(value);
    }
    RETURN;
}

PP(pp_sle)
{
SensorCall(12028);    dVAR; dSP;

    int amg_type = sle_amg;
    int multiplier = 1;
    int rhs = 1;

    SensorCall(12035);switch (PL_op->op_type) {
    case OP_SLT:
	SensorCall(12029);amg_type = slt_amg;
	/* cmp < 0 */
	rhs = 0;
	SensorCall(12030);break;
    case OP_SGT:
	SensorCall(12031);amg_type = sgt_amg;
	/* cmp > 0 */
	multiplier = -1;
	rhs = 0;
	SensorCall(12032);break;
    case OP_SGE:
	SensorCall(12033);amg_type = sge_amg;
	/* cmp >= 0 */
	multiplier = -1;
	SensorCall(12034);break;
    }

    tryAMAGICbin_MG(amg_type, AMGf_set);
    {
      dPOPTOPssrl;
      SensorCall(12036);const int cmp = (IN_LOCALE_RUNTIME
		 ? sv_cmp_locale_flags(left, right, 0)
		 : sv_cmp_flags(left, right, 0));
      SETs(boolSV(cmp * multiplier < rhs));
      RETURN;
    }
}

PP(pp_seq)
{
SensorCall(12037);    dVAR; dSP;
    tryAMAGICbin_MG(seq_amg, AMGf_set);
    {
      dPOPTOPssrl;
      SETs(boolSV(sv_eq_flags(left, right, 0)));
      RETURN;
    }
}

PP(pp_sne)
{
SensorCall(12038);    dVAR; dSP;
    tryAMAGICbin_MG(sne_amg, AMGf_set);
    {
      dPOPTOPssrl;
      SETs(boolSV(!sv_eq_flags(left, right, 0)));
      RETURN;
    }
}

PP(pp_scmp)
{
SensorCall(12039);    dVAR; dSP; dTARGET;
    tryAMAGICbin_MG(scmp_amg, 0);
    {
      dPOPTOPssrl;
      const int cmp = (IN_LOCALE_RUNTIME
		 ? sv_cmp_locale_flags(left, right, 0)
		 : sv_cmp_flags(left, right, 0));
      SETi( cmp );
      RETURN;
    }
}

PP(pp_bit_and)
{
SensorCall(12040);    dVAR; dSP; dATARGET;
    tryAMAGICbin_MG(band_amg, AMGf_assign);
    {
      dPOPTOPssrl;
      SensorCall(12047);if (SvNIOKp(left) || SvNIOKp(right)) {
	SensorCall(12041);const bool left_ro_nonnum  = !SvNIOKp(left) && SvREADONLY(left);
	const bool right_ro_nonnum = !SvNIOKp(right) && SvREADONLY(right);
	SensorCall(12044);if (PL_op->op_private & HINT_INTEGER) {
	  SensorCall(12042);const IV i = SvIV_nomg(left) & SvIV_nomg(right);
	  SETi(i);
	}
	else {
	  SensorCall(12043);const UV u = SvUV_nomg(left) & SvUV_nomg(right);
	  SETu(u);
	}
	SensorCall(12045);if (left_ro_nonnum && left != TARG) SvNIOK_off(left);
	SensorCall(12046);if (right_ro_nonnum) SvNIOK_off(right);
      }
      else {
	do_vop(PL_op->op_type, TARG, left, right);
	SETTARG;
      }
      RETURN;
    }
}

PP(pp_bit_or)
{
SensorCall(12048);    dVAR; dSP; dATARGET;
    const int op_type = PL_op->op_type;

    tryAMAGICbin_MG((op_type == OP_BIT_OR ? bor_amg : bxor_amg), AMGf_assign);
    {
      dPOPTOPssrl;
      SensorCall(12055);if (SvNIOKp(left) || SvNIOKp(right)) {
	SensorCall(12049);const bool left_ro_nonnum  = !SvNIOKp(left) && SvREADONLY(left);
	const bool right_ro_nonnum = !SvNIOKp(right) && SvREADONLY(right);
	SensorCall(12052);if (PL_op->op_private & HINT_INTEGER) {
	  SensorCall(12050);const IV l = (USE_LEFT(left) ? SvIV_nomg(left) : 0);
	  const IV r = SvIV_nomg(right);
	  const IV result = op_type == OP_BIT_OR ? (l | r) : (l ^ r);
	  SETi(result);
	}
	else {
	  SensorCall(12051);const UV l = (USE_LEFT(left) ? SvUV_nomg(left) : 0);
	  const UV r = SvUV_nomg(right);
	  const UV result = op_type == OP_BIT_OR ? (l | r) : (l ^ r);
	  SETu(result);
	}
	SensorCall(12053);if (left_ro_nonnum && left != TARG) SvNIOK_off(left);
	SensorCall(12054);if (right_ro_nonnum) SvNIOK_off(right);
      }
      else {
	do_vop(op_type, TARG, left, right);
	SETTARG;
      }
      RETURN;
    }
}

PP(pp_negate)
{
SensorCall(12056);    dVAR; dSP; dTARGET;
    tryAMAGICun_MG(neg_amg, AMGf_numeric);
    {
	SV * const sv = TOPs;
	const int flags = SvFLAGS(sv);

        SensorCall(12057);if( !SvNIOK( sv ) && looks_like_number( sv ) ){
           SvIV_please( sv );
        }   

	SensorCall(12062);if ((flags & SVf_IOK) || ((flags & (SVp_IOK | SVp_NOK)) == SVp_IOK)) {
	    /* It's publicly an integer, or privately an integer-not-float */
	oops_its_an_int:
	    SensorCall(12058);if (SvIsUV(sv)) {
		SensorCall(12059);if (SvIVX(sv) == IV_MIN) {
		    /* 2s complement assumption. */
		    SETi(SvIVX(sv));	/* special case: -((UV)IV_MAX+1) == IV_MIN */
		    RETURN;
		}
		else {/*181*/SensorCall(12060);if (SvUVX(sv) <= IV_MAX) {
		    SETi(-SvIVX(sv));
		    RETURN;
		;/*182*/}}
	    }
	    else {/*183*/SensorCall(12061);if (SvIVX(sv) != IV_MIN) {
		SETi(-SvIVX(sv));
		RETURN;
	    }
#ifdef PERL_PRESERVE_IVUV
	    else {
		SETu((UV)IV_MIN);
		RETURN;
	    ;/*184*/}}
#endif
	}
	SensorCall(12063);if (SvNIOKp(sv))
	    SETn(-SvNV_nomg(sv));
	else if (SvPOKp(sv)) {
	    STRLEN len;
	    const char * const s = SvPV_nomg_const(sv, len);
	    if (isIDFIRST(*s)) {
		sv_setpvs(TARG, "-");
		sv_catsv(TARG, sv);
	    }
	    else {/*185*/if (*s == '+' || *s == '-') {
		sv_setsv_nomg(TARG, sv);
		*SvPV_force_nomg(TARG, len) = *s == '-' ? '+' : '-';
	    }
	    else {/*187*/if (DO_UTF8(sv)) {
		SvIV_please_nomg(sv);
		if (SvIOK(sv))
		    {/*189*/goto oops_its_an_int;/*190*/}
		if (SvNOK(sv))
		    sv_setnv(TARG, -SvNV_nomg(sv));
		else {
		    sv_setpvs(TARG, "-");
		    sv_catsv(TARG, sv);
		}
	    }
	    else {
		SvIV_please_nomg(sv);
		if (SvIOK(sv))
		  {/*191*/goto oops_its_an_int;/*192*/}
		sv_setnv(TARG, -SvNV_nomg(sv));
	    ;/*188*/}/*186*/}}
	    SETTARG;
	}
	else
	    SETn(-SvNV_nomg(sv));
    }
    RETURN;
}

PP(pp_not)
{
SensorCall(12064);    dVAR; dSP;
    tryAMAGICun_MG(not_amg, AMGf_set);
    *PL_stack_sp = boolSV(!SvTRUE_nomg(*PL_stack_sp));
    {OP * ReplaceReturn1002 = NORMAL; SensorCall(12065); return ReplaceReturn1002;}
}

PP(pp_complement)
{
SensorCall(12066);    dVAR; dSP; dTARGET;
    tryAMAGICun_MG(compl_amg, AMGf_numeric);
    {
      dTOPss;
      SensorCall(12096);if (SvNIOKp(sv)) {
	SensorCall(12067);if (PL_op->op_private & HINT_INTEGER) {
	  SensorCall(12068);const IV i = ~SvIV_nomg(sv);
	  SETi(i);
	}
	else {
	  SensorCall(12069);const UV u = ~SvUV_nomg(sv);
	  SETu(u);
	}
      }
      else {
	SensorCall(12070);register U8 *tmps;
	register I32 anum;
	STRLEN len;

	(void)SvPV_nomg_const(sv,len); /* force check for uninit var */
	sv_setsv_nomg(TARG, sv);
	tmps = (U8*)SvPV_force_nomg(TARG, len);
	anum = len;
	SensorCall(12086);if (SvUTF8(TARG)) {
	  /* Calculate exact length, let's not estimate. */
	  SensorCall(12071);STRLEN targlen = 0;
	  STRLEN l;
	  UV nchar = 0;
	  UV nwide = 0;
	  U8 * const send = tmps + len;
	  U8 * const origtmps = tmps;
	  const UV utf8flags = UTF8_ALLOW_ANYUV;

	  SensorCall(12075);while (tmps < send) {
	    SensorCall(12072);const UV c = utf8n_to_uvchr(tmps, send-tmps, &l, utf8flags);
	    tmps += l;
	    targlen += UNISKIP(~c);
	    nchar++;
	    SensorCall(12074);if (c > 0xff)
		{/*85*/SensorCall(12073);nwide++;/*86*/}
	  }

	  /* Now rewind strings and write them. */
	  SensorCall(12076);tmps = origtmps;

	  SensorCall(12085);if (nwide) {
	      SensorCall(12077);U8 *result;
	      U8 *p;

	      Newx(result, targlen + 1, U8);
	      p = result;
	      SensorCall(12079);while (tmps < send) {
		  SensorCall(12078);const UV c = utf8n_to_uvchr(tmps, send-tmps, &l, utf8flags);
		  tmps += l;
		  p = uvchr_to_utf8_flags(p, ~c, UNICODE_ALLOW_ANY);
	      }
	      SensorCall(12080);*p = '\0';
	      sv_usepvn_flags(TARG, (char*)result, targlen,
			      SV_HAS_TRAILING_NUL);
	      SvUTF8_on(TARG);
	  }
	  else {
	      SensorCall(12081);U8 *result;
	      U8 *p;

	      Newx(result, nchar + 1, U8);
	      p = result;
	      SensorCall(12083);while (tmps < send) {
		  SensorCall(12082);const U8 c = (U8)utf8n_to_uvchr(tmps, send-tmps, &l, utf8flags);
		  tmps += l;
		  *p++ = ~c;
	      }
	      SensorCall(12084);*p = '\0';
	      sv_usepvn_flags(TARG, (char*)result, nchar, SV_HAS_TRAILING_NUL);
	      SvUTF8_off(TARG);
	  }
	  SETTARG;
	  RETURN;
	}
#ifdef LIBERAL
	{
	    SensorCall(12087);register long *tmpl;
	    SensorCall(12089);for ( ; anum && (unsigned long)tmps % sizeof(long); anum--, tmps++)
		{/*87*/SensorCall(12088);*tmps = ~*tmps;/*88*/}
	    SensorCall(12090);tmpl = (long*)tmps;
	    SensorCall(12092);for ( ; anum >= (I32)sizeof(long); anum -= (I32)sizeof(long), tmpl++)
		{/*89*/SensorCall(12091);*tmpl = ~*tmpl;/*90*/}
	    SensorCall(12093);tmps = (U8*)tmpl;
	}
#endif
	SensorCall(12095);for ( ; anum > 0; anum--, tmps++)
	    {/*91*/SensorCall(12094);*tmps = ~*tmps;/*92*/}
	SETTARG;
      }
      RETURN;
    }
}

/* integer versions of some of the above */

PP(pp_i_multiply)
{
SensorCall(12097);    dVAR; dSP; dATARGET;
    tryAMAGICbin_MG(mult_amg, AMGf_assign);
    {
      dPOPTOPiirl_nomg;
      SETi( left * right );
      RETURN;
    }
}

PP(pp_i_divide)
{
    SensorCall(12098);IV num;
    dVAR; dSP; dATARGET;
    tryAMAGICbin_MG(div_amg, AMGf_assign);
    {
      dPOPTOPssrl;
      IV value = SvIV_nomg(right);
      SensorCall(12099);if (value == 0)
	  DIE(aTHX_ "Illegal division by zero");
      SensorCall(12100);num = SvIV_nomg(left);

      /* avoid FPE_INTOVF on some platforms when num is IV_MIN */
      SensorCall(12103);if (value == -1)
          {/*125*/SensorCall(12101);value = - num;/*126*/}
      else
          {/*127*/SensorCall(12102);value = num / value;/*128*/}
      SETi(value);
      RETURN;
    }
}

#if defined(__GLIBC__) && IVSIZE == 8
STATIC
PP(pp_i_modulo_0)
#else
PP(pp_i_modulo)
#endif
{
SensorCall(12104);     /* This is the vanilla old i_modulo. */
     dVAR; dSP; dATARGET;
     tryAMAGICbin_MG(modulo_amg, AMGf_assign);
     {
	  dPOPTOPiirl_nomg;
	  SensorCall(12105);if (!right)
	       DIE(aTHX_ "Illegal modulus zero");
	  /* avoid FPE_INTOVF on some platforms when left is IV_MIN */
	  SensorCall(12106);if (right == -1)
	      SETi( 0 );
	  else
	      SETi( left % right );
	  RETURN;
     }
}

#if defined(__GLIBC__) && IVSIZE == 8
STATIC
PP(pp_i_modulo_1)

{
SensorCall(12107);     /* This is the i_modulo with the workaround for the _moddi3 bug
      * in (at least) glibc 2.2.5 (the PERL_ABS() the workaround).
      * See below for pp_i_modulo. */
     dVAR; dSP; dATARGET;
     tryAMAGICbin_MG(modulo_amg, AMGf_assign);
     {
	  dPOPTOPiirl_nomg;
	  SensorCall(12108);if (!right)
	       DIE(aTHX_ "Illegal modulus zero");
	  /* avoid FPE_INTOVF on some platforms when left is IV_MIN */
	  SensorCall(12109);if (right == -1)
	      SETi( 0 );
	  else
	      SETi( left % PERL_ABS(right) );
	  RETURN;
     }
}

PP(pp_i_modulo)
{
SensorCall(12110);     dVAR; dSP; dATARGET;
     tryAMAGICbin_MG(modulo_amg, AMGf_assign);
     {
	  dPOPTOPiirl_nomg;
	  SensorCall(12111);if (!right)
	       DIE(aTHX_ "Illegal modulus zero");
	  /* The assumption is to use hereafter the old vanilla version... */
	  PL_op->op_ppaddr =
	       PL_ppaddr[OP_I_MODULO] =
	           Perl_pp_i_modulo_0;
	  /* .. but if we have glibc, we might have a buggy _moddi3
	   * (at least glicb 2.2.5 is known to have this bug), in other
	   * words our integer modulus with negative quad as the second
	   * argument might be broken.  Test for this and re-patch the
	   * opcode dispatch table if that is the case, remembering to
	   * also apply the workaround so that this first round works
	   * right, too.  See [perl #9402] for more information. */
	  {
	       SensorCall(12112);IV l =   3;
	       IV r = -10;
	       /* Cannot do this check with inlined IV constants since
		* that seems to work correctly even with the buggy glibc. */
	       SensorCall(12114);if (l % r == -3) {
		    /* Yikes, we have the bug.
		     * Patch in the workaround version. */
		    PL_op->op_ppaddr =
			 PL_ppaddr[OP_I_MODULO] =
			     &Perl_pp_i_modulo_1;
		    /* Make certain we work right this time, too. */
		    SensorCall(12113);right = PERL_ABS(right);
	       }
	  }
	  /* avoid FPE_INTOVF on some platforms when left is IV_MIN */
	  SensorCall(12115);if (right == -1)
	      SETi( 0 );
	  else
	      SETi( left % right );
	  RETURN;
     }
}
#endif

PP(pp_i_add)
{
SensorCall(12116);    dVAR; dSP; dATARGET;
    tryAMAGICbin_MG(add_amg, AMGf_assign);
    {
      dPOPTOPiirl_ul_nomg;
      SETi( left + right );
      RETURN;
    }
}

PP(pp_i_subtract)
{
SensorCall(12117);    dVAR; dSP; dATARGET;
    tryAMAGICbin_MG(subtr_amg, AMGf_assign);
    {
      dPOPTOPiirl_ul_nomg;
      SETi( left - right );
      RETURN;
    }
}

PP(pp_i_lt)
{
SensorCall(12118);    dVAR; dSP;
    tryAMAGICbin_MG(lt_amg, AMGf_set);
    {
      dPOPTOPiirl_nomg;
      SETs(boolSV(left < right));
      RETURN;
    }
}

PP(pp_i_gt)
{
SensorCall(12119);    dVAR; dSP;
    tryAMAGICbin_MG(gt_amg, AMGf_set);
    {
      dPOPTOPiirl_nomg;
      SETs(boolSV(left > right));
      RETURN;
    }
}

PP(pp_i_le)
{
SensorCall(12120);    dVAR; dSP;
    tryAMAGICbin_MG(le_amg, AMGf_set);
    {
      dPOPTOPiirl_nomg;
      SETs(boolSV(left <= right));
      RETURN;
    }
}

PP(pp_i_ge)
{
SensorCall(12121);    dVAR; dSP;
    tryAMAGICbin_MG(ge_amg, AMGf_set);
    {
      dPOPTOPiirl_nomg;
      SETs(boolSV(left >= right));
      RETURN;
    }
}

PP(pp_i_eq)
{
SensorCall(12122);    dVAR; dSP;
    tryAMAGICbin_MG(eq_amg, AMGf_set);
    {
      dPOPTOPiirl_nomg;
      SETs(boolSV(left == right));
      RETURN;
    }
}

PP(pp_i_ne)
{
SensorCall(12123);    dVAR; dSP;
    tryAMAGICbin_MG(ne_amg, AMGf_set);
    {
      dPOPTOPiirl_nomg;
      SETs(boolSV(left != right));
      RETURN;
    }
}

PP(pp_i_ncmp)
{
SensorCall(12124);    dVAR; dSP; dTARGET;
    tryAMAGICbin_MG(ncmp_amg, 0);
    {
      dPOPTOPiirl_nomg;
      I32 value;

      SensorCall(12129);if (left > right)
	{/*129*/SensorCall(12125);value = 1;/*130*/}
      else {/*131*/SensorCall(12126);if (left < right)
	{/*133*/SensorCall(12127);value = -1;/*134*/}
      else
	{/*135*/SensorCall(12128);value = 0;/*136*/}/*132*/}
      SETi(value);
      RETURN;
    }
}

PP(pp_i_negate)
{
SensorCall(12130);    dVAR; dSP; dTARGET;
    tryAMAGICun_MG(neg_amg, 0);
    {
	SV * const sv = TOPs;
	IV const i = SvIV_nomg(sv);
	SETi(-i);
	RETURN;
    }
}

/* High falutin' math. */

PP(pp_atan2)
{
SensorCall(12131);    dVAR; dSP; dTARGET;
    tryAMAGICbin_MG(atan2_amg, 0);
    {
      dPOPTOPnnrl_nomg;
      SETn(Perl_atan2(left, right));
      RETURN;
    }
}

PP(pp_sin)
{
SensorCall(12132);    dVAR; dSP; dTARGET;
    int amg_type = sin_amg;
    const char *neg_report = NULL;
    NV (*func)(NV) = Perl_sin;
    const int op_type = PL_op->op_type;

    SensorCall(12141);switch (op_type) {
    case OP_COS:
	SensorCall(12133);amg_type = cos_amg;
	func = Perl_cos;
	SensorCall(12134);break;
    case OP_EXP:
	SensorCall(12135);amg_type = exp_amg;
	func = Perl_exp;
	SensorCall(12136);break;
    case OP_LOG:
	SensorCall(12137);amg_type = log_amg;
	func = Perl_log;
	neg_report = "log";
	SensorCall(12138);break;
    case OP_SQRT:
	SensorCall(12139);amg_type = sqrt_amg;
	func = Perl_sqrt;
	neg_report = "sqrt";
	SensorCall(12140);break;
    }


    tryAMAGICun_MG(amg_type, 0);
    {
      SensorCall(12142);SV * const arg = POPs;
      const NV value = SvNV_nomg(arg);
      SensorCall(12144);if (neg_report) {
	  SensorCall(12143);if (op_type == OP_LOG ? (value <= 0.0) : (value < 0.0)) {
	      SET_NUMERIC_STANDARD();
	      /* diag_listed_as: Can't take log of %g */
	      DIE(aTHX_ "Can't take %s of %"NVgf, neg_report, value);
	  }
      }
      XPUSHn(func(value));
      RETURN;
    }
}

/* Support Configure command-line overrides for rand() functions.
   After 5.005, perhaps we should replace this by Configure support
   for drand48(), random(), or rand().  For 5.005, though, maintain
   compatibility by calling rand() but allow the user to override it.
   See INSTALL for details.  --Andy Dougherty  15 July 1998
*/
/* Now it's after 5.005, and Configure supports drand48() and random(),
   in addition to rand().  So the overrides should not be needed any more.
   --Jarkko Hietaniemi	27 September 1998
 */

#ifndef HAS_DRAND48_PROTO
extern double drand48 (void);
#endif

PP(pp_rand)
{
SensorCall(12145);    dVAR; dSP; dTARGET;
    NV value;
    SensorCall(12147);if (MAXARG < 1)
	{/*229*/SensorCall(12146);value = 1.0;/*230*/}
    else if (!TOPs) {
	value = 1.0; (void)POPs;
    }
    else
	value = POPn;
    SensorCall(12149);if (value == 0.0)
	{/*231*/SensorCall(12148);value = 1.0;/*232*/}
    SensorCall(12151);if (!PL_srand_called) {
	SensorCall(12150);(void)seedDrand01((Rand_seed_t)seed());
	PL_srand_called = TRUE;
    }
    SensorCall(12152);value *= Drand01();
    XPUSHn(value);
    RETURN;
}

PP(pp_srand)
{
SensorCall(12153);    dVAR; dSP; dTARGET;
    const UV anum = (MAXARG < 1 || (!TOPs && !POPs)) ? seed() : POPu;
    (void)seedDrand01((Rand_seed_t)anum);
    PL_srand_called = TRUE;
    SensorCall(12154);if (anum)
	XPUSHu(anum);
    else {
	/* Historically srand always returned true. We can avoid breaking
	   that like this:  */
	sv_setpvs(TARG, "0 but true");
	XPUSHTARG;
    }
    RETURN;
}

PP(pp_int)
{
SensorCall(12155);    dVAR; dSP; dTARGET;
    tryAMAGICun_MG(int_amg, AMGf_numeric);
    {
      SV * const sv = TOPs;
      const IV iv = SvIV_nomg(sv);
      /* XXX it's arguable that compiler casting to IV might be subtly
	 different from modf (for numbers inside (IV_MIN,UV_MAX)) in which
	 else preferring IV has introduced a subtle behaviour change bug. OTOH
	 relying on floating point to be accurate is a bug.  */

      SensorCall(12162);if (!SvOK(sv)) {
        SETu(0);
      }
      else {/*149*/SensorCall(12156);if (SvIOK(sv)) {
	SensorCall(12157);if (SvIsUV(sv))
	    SETu(SvUV_nomg(sv));
	else
	    SETi(iv);
      }
      else {
	  SensorCall(12158);const NV value = SvNV_nomg(sv);
	  SensorCall(12161);if (value >= 0.0) {
	      SensorCall(12159);if (value < (NV)UV_MAX + 0.5) {
		  SETu(U_V(value));
	      } else {
		  SETn(Perl_floor(value));
	      }
	  }
	  else {
	      SensorCall(12160);if (value > (NV)IV_MIN - 0.5) {
		  SETi(I_V(value));
	      } else {
		  SETn(Perl_ceil(value));
	      }
	  }
      ;/*150*/}}
    }
    RETURN;
}

PP(pp_abs)
{
SensorCall(12163);    dVAR; dSP; dTARGET;
    tryAMAGICun_MG(abs_amg, AMGf_numeric);
    {
      SV * const sv = TOPs;
      /* This will cache the NV value if string isn't actually integer  */
      const IV iv = SvIV_nomg(sv);

      SensorCall(12170);if (!SvOK(sv)) {
        SETu(0);
      }
      else {/*71*/SensorCall(12164);if (SvIOK(sv)) {
	/* IVX is precise  */
	SensorCall(12165);if (SvIsUV(sv)) {
	  SETu(SvUV_nomg(sv));	/* force it to be numeric only */
	} else {
	  SensorCall(12166);if (iv >= 0) {
	    SETi(iv);
	  } else {
	    SensorCall(12167);if (iv != IV_MIN) {
	      SETi(-iv);
	    } else {
	      /* 2s complement assumption. Also, not really needed as
		 IV_MIN and -IV_MIN should both be %100...00 and NV-able  */
	      SETu(IV_MIN);
	    }
	  }
	}
      } else{
	SensorCall(12168);const NV value = SvNV_nomg(sv);
	SensorCall(12169);if (value < 0.0)
	  SETn(-value);
	else
	  SETn(value);
      ;/*72*/}}
    }
    RETURN;
}

PP(pp_oct)
{
SensorCall(12171);    dVAR; dSP; dTARGET;
    const char *tmps;
    I32 flags = PERL_SCAN_ALLOW_UNDERSCORES;
    STRLEN len;
    NV result_nv;
    UV result_uv;
    SV* const sv = POPs;

    tmps = (SvPV_const(sv, len));
    SensorCall(12173);if (DO_UTF8(sv)) {
	 /* If Unicode, try to downgrade
	  * If not possible, croak. */
	 SensorCall(12172);SV* const tsv = sv_2mortal(newSVsv(sv));
	
	 SvUTF8_on(tsv);
	 sv_utf8_downgrade(tsv, FALSE);
	 tmps = SvPV_const(tsv, len);
    }
    SensorCall(12175);if (PL_op->op_type == OP_HEX)
	{/*193*/SensorCall(12174);goto hex;/*194*/}

    SensorCall(12177);while (*tmps && len && isSPACE(*tmps))
        {/*195*/SensorCall(12176);tmps++, len--;/*196*/}
    SensorCall(12179);if (*tmps == '0')
        {/*197*/SensorCall(12178);tmps++, len--;/*198*/}
    SensorCall(12181);if (*tmps == 'x' || *tmps == 'X') {
    hex:
        SensorCall(12180);result_uv = grok_hex (tmps, &len, &flags, &result_nv);
    }
    else if (*tmps == 'b' || *tmps == 'B')
        result_uv = grok_bin (tmps, &len, &flags, &result_nv);
    else
        result_uv = grok_oct (tmps, &len, &flags, &result_nv);

    SensorCall(12182);if (flags & PERL_SCAN_GREATER_THAN_UV_MAX) {
        XPUSHn(result_nv);
    }
    else {
        XPUSHu(result_uv);
    }
    RETURN;
}

/* String stuff. */

PP(pp_length)
{
SensorCall(12183);    dVAR; dSP; dTARGET;
    SV * const sv = TOPs;

    SensorCall(12190);if (SvGAMAGIC(sv)) {
	/* For an overloaded or magic scalar, we can't know in advance if
	   it's going to be UTF-8 or not. Also, we can't call sv_len_utf8 as
	   it likes to cache the length. Maybe that should be a documented
	   feature of it.
	*/
	SensorCall(12184);STRLEN len;
	const char *const p
	    = sv_2pv_flags(sv, &len,
			   SV_UNDEF_RETURNS_NULL|SV_CONST_RETURN|SV_GMAGIC);

	SensorCall(12186);if (!p) {
	    SensorCall(12185);if (!SvPADTMP(TARG)) {
		sv_setsv(TARG, &PL_sv_undef);
		SETTARG;
	    }
	    SETs(&PL_sv_undef);
	}
	else if (DO_UTF8(sv)) {
	    SETi(utf8_length((U8*)p, (U8*)p + len));
	}
	else
	    SETi(len);
    } else {/*153*/SensorCall(12187);if (SvOK(sv)) {
	/* Neither magic nor overloaded.  */
	SensorCall(12188);if (DO_UTF8(sv))
	    SETi(sv_len_utf8(sv));
	else
	    SETi(sv_len(sv));
    } else {
	SensorCall(12189);if (!SvPADTMP(TARG)) {
	    sv_setsv_nomg(TARG, &PL_sv_undef);
	    SETTARG;
	}
	SETs(&PL_sv_undef);
    ;/*154*/}}
    RETURN;
}

/* Returns false if substring is completely outside original string.
   No length is indicated by len_iv = 0 and len_is_uv = 0.  len_is_uv must
   always be true for an explicit 0.
*/
bool
Perl_translate_substr_offsets(pTHX_ STRLEN curlen, IV pos1_iv,
				    bool pos1_is_uv, IV len_iv,
				    bool len_is_uv, STRLEN *posp,
				    STRLEN *lenp)
{
    SensorCall(12191);IV pos2_iv;
    int    pos2_is_uv;

    PERL_ARGS_ASSERT_TRANSLATE_SUBSTR_OFFSETS;

    SensorCall(12193);if (!pos1_is_uv && pos1_iv < 0 && curlen) {
	SensorCall(12192);pos1_is_uv = curlen-1 > ~(UV)pos1_iv;
	pos1_iv += curlen;
    }
    SensorCall(12194);if ((pos1_is_uv || pos1_iv > 0) && (UV)pos1_iv > curlen)
	return FALSE;

    SensorCall(12207);if (len_iv || len_is_uv) {
	SensorCall(12195);if (!len_is_uv && len_iv < 0) {
	    SensorCall(12196);pos2_iv = curlen + len_iv;
	    SensorCall(12199);if (curlen)
		{/*11*/SensorCall(12197);pos2_is_uv = curlen-1 > ~(UV)len_iv;/*12*/}
	    else
		{/*13*/SensorCall(12198);pos2_is_uv = 0;/*14*/}
	} else {  /* len_iv >= 0 */
	    SensorCall(12200);if (!pos1_is_uv && pos1_iv < 0) {
		SensorCall(12201);pos2_iv = pos1_iv + len_iv;
		pos2_is_uv = (UV)len_iv > (UV)IV_MAX;
	    } else {
		SensorCall(12202);if ((UV)len_iv > curlen-(UV)pos1_iv)
		    {/*15*/SensorCall(12203);pos2_iv = curlen;/*16*/}
		else
		    {/*17*/SensorCall(12204);pos2_iv = pos1_iv+len_iv;/*18*/}
		SensorCall(12205);pos2_is_uv = 1;
	    }
	}
    }
    else {
	SensorCall(12206);pos2_iv = curlen;
	pos2_is_uv = 1;
    }

    SensorCall(12212);if (!pos2_is_uv && pos2_iv < 0) {
	SensorCall(12208);if (!pos1_is_uv && pos1_iv < 0)
	    return FALSE;
	SensorCall(12209);pos2_iv = 0;
    }
    else {/*19*/SensorCall(12210);if (!pos1_is_uv && pos1_iv < 0)
	{/*21*/SensorCall(12211);pos1_iv = 0;/*22*/}/*20*/}

    SensorCall(12214);if ((UV)pos2_iv < (UV)pos1_iv)
	{/*23*/SensorCall(12213);pos2_iv = pos1_iv;/*24*/}
    SensorCall(12216);if ((UV)pos2_iv > curlen)
	{/*25*/SensorCall(12215);pos2_iv = curlen;/*26*/}

    /* pos1_iv and pos2_iv both in 0..curlen, so the cast is safe */
    SensorCall(12217);*posp = (STRLEN)( (UV)pos1_iv );
    *lenp = (STRLEN)( (UV)pos2_iv - (UV)pos1_iv );

    {_Bool  ReplaceReturn1001 = TRUE; SensorCall(12218); return ReplaceReturn1001;}
}

PP(pp_substr)
{
SensorCall(12219);    dVAR; dSP; dTARGET;
    SV *sv;
    STRLEN curlen;
    STRLEN utf8_curlen;
    SV *   pos_sv;
    IV     pos1_iv;
    int    pos1_is_uv;
    SV *   len_sv;
    IV     len_iv = 0;
    int    len_is_uv = 0;
    I32 lvalue = PL_op->op_flags & OPf_MOD || LVRET;
    const bool rvalue = (GIMME_V != G_VOID);
    const char *tmps;
    SV *repl_sv = NULL;
    const char *repl = NULL;
    STRLEN repl_len;
    int num_args = PL_op->op_private & 7;
    bool repl_need_utf8_upgrade = FALSE;
    bool repl_is_utf8 = FALSE;

    SensorCall(12226);if (num_args > 2) {
	SensorCall(12220);if (num_args > 3) {
	  SensorCall(12221);if(!(repl_sv = POPs)) {/*357*/SensorCall(12222);num_args--;/*358*/}
	}
	SensorCall(12225);if ((len_sv = POPs)) {
	    SensorCall(12223);len_iv    = SvIV(len_sv);
	    len_is_uv = len_iv ? SvIOK_UV(len_sv) : 1;
	}
	else {/*359*/SensorCall(12224);num_args--;/*360*/}
    }
    SensorCall(12227);pos_sv     = POPs;
    pos1_iv    = SvIV(pos_sv);
    pos1_is_uv = SvIOK_UV(pos_sv);
    sv = POPs;
    SensorCall(12229);if (PL_op->op_private & OPpSUBSTR_REPL_FIRST) {
	assert(!repl_sv);
	SensorCall(12228);repl_sv = POPs;
    }
    PUTBACK;
    SensorCall(12235);if (repl_sv) {
	SensorCall(12230);repl = SvPV_const(repl_sv, repl_len);
	repl_is_utf8 = DO_UTF8(repl_sv) && repl_len;
	SensorCall(12232);if (repl_is_utf8) {
	    SensorCall(12231);if (!DO_UTF8(sv))
		sv_utf8_upgrade(sv);
	}
	else if (DO_UTF8(sv))
	    repl_need_utf8_upgrade = TRUE;
    }
    else {/*361*/SensorCall(12233);if (lvalue) {
	SensorCall(12234);SV * ret;
	ret = sv_2mortal(newSV_type(SVt_PVLV));  /* Not TARG RT#67838 */
	sv_magic(ret, NULL, PERL_MAGIC_substr, NULL, 0);
	LvTYPE(ret) = 'x';
	LvTARG(ret) = SvREFCNT_inc_simple(sv);
	LvTARGOFF(ret) =
	    pos1_is_uv || pos1_iv >= 0
		? (STRLEN)(UV)pos1_iv
		: (LvFLAGS(ret) |= 1, (STRLEN)(UV)-pos1_iv);
	LvTARGLEN(ret) =
	    len_is_uv || len_iv > 0
		? (STRLEN)(UV)len_iv
		: (LvFLAGS(ret) |= 2, (STRLEN)(UV)-len_iv);

	SPAGAIN;
	PUSHs(ret);    /* avoid SvSETMAGIC here */
	RETURN;
    ;/*362*/}}
    SensorCall(12236);tmps = SvPV_const(sv, curlen);
    SensorCall(12242);if (DO_UTF8(sv)) {
        SensorCall(12237);utf8_curlen = sv_len_utf8(sv);
	SensorCall(12240);if (utf8_curlen == curlen)
	    {/*363*/SensorCall(12238);utf8_curlen = 0;/*364*/}
	else
	    {/*365*/SensorCall(12239);curlen = utf8_curlen;/*366*/}
    }
    else
	{/*367*/SensorCall(12241);utf8_curlen = 0;/*368*/}

    {
	SensorCall(12243);STRLEN pos, len, byte_len, byte_pos;

	SensorCall(12245);if (!translate_substr_offsets(
		curlen, pos1_iv, pos1_is_uv, len_iv, len_is_uv, &pos, &len
	)) {/*369*/SensorCall(12244);goto bound_fail;/*370*/}

	SensorCall(12246);byte_len = len;
	byte_pos = utf8_curlen
	    ? sv_pos_u2b_flags(sv, pos, &byte_len, SV_CONST_RETURN) : pos;

	tmps += byte_pos;

	SensorCall(12248);if (rvalue) {
	    SvTAINTED_off(TARG);			/* decontaminate */
	    SvUTF8_off(TARG);			/* decontaminate */
	    sv_setpvn(TARG, tmps, byte_len);
#ifdef USE_LOCALE_COLLATE
	    sv_unmagic(TARG, PERL_MAGIC_collxfrm);
#endif
	    SensorCall(12247);if (utf8_curlen)
		SvUTF8_on(TARG);
	}

	SensorCall(12256);if (repl) {
	    SensorCall(12249);SV* repl_sv_copy = NULL;

	    SensorCall(12251);if (repl_need_utf8_upgrade) {
		SensorCall(12250);repl_sv_copy = newSVsv(repl_sv);
		sv_utf8_upgrade(repl_sv_copy);
		repl = SvPV_const(repl_sv_copy, repl_len);
		repl_is_utf8 = DO_UTF8(repl_sv_copy) && repl_len;
	    }
	    SensorCall(12253);if (SvROK(sv))
		{/*371*/SensorCall(12252);Perl_ck_warner(aTHX_ packWARN(WARN_SUBSTR),
			    "Attempt to use reference as lvalue in substr"
		);/*372*/}
	    SensorCall(12254);if (!SvOK(sv))
		sv_setpvs(sv, "");
	    sv_insert_flags(sv, byte_pos, byte_len, repl, repl_len, 0);
	    SensorCall(12255);if (repl_is_utf8)
		SvUTF8_on(sv);
	    SvREFCNT_dec(repl_sv_copy);
	}
    }
    SPAGAIN;
    SensorCall(12257);if (rvalue) {
	SvSETMAGIC(TARG);
	PUSHs(TARG);
    }
    RETURN;

bound_fail:
    SensorCall(12259);if (repl)
	{/*373*/SensorCall(12258);Perl_croak(aTHX_ "substr outside of string");/*374*/}
    SensorCall(12260);Perl_ck_warner(aTHX_ packWARN(WARN_SUBSTR), "substr outside of string");
    RETPUSHUNDEF;
}

PP(pp_vec)
{
SensorCall(12261);    dVAR; dSP;
    register const IV size   = POPi;
    register const IV offset = POPi;
    register SV * const src = POPs;
    const I32 lvalue = PL_op->op_flags & OPf_MOD || LVRET;
    SV * ret;

    SensorCall(12264);if (lvalue) {			/* it's an lvalue! */
	SensorCall(12262);ret = sv_2mortal(newSV_type(SVt_PVLV));  /* Not TARG RT#67838 */
	sv_magic(ret, NULL, PERL_MAGIC_vec, NULL, 0);
	LvTYPE(ret) = 'v';
	LvTARG(ret) = SvREFCNT_inc_simple(src);
	LvTARGOFF(ret) = offset;
	LvTARGLEN(ret) = size;
    }
    else {
	dTARGET;
	SvTAINTED_off(TARG);		/* decontaminate */
	SensorCall(12263);ret = TARG;
    }

    sv_setuv(ret, do_vecget(src, offset, size));
    PUSHs(ret);
    RETURN;
}

PP(pp_index)
{
SensorCall(12265);    dVAR; dSP; dTARGET;
    SV *big;
    SV *little;
    SV *temp = NULL;
    STRLEN biglen;
    STRLEN llen = 0;
    I32 offset;
    I32 retval;
    const char *big_p;
    const char *little_p;
    bool big_utf8;
    bool little_utf8;
    const bool is_index = PL_op->op_type == OP_INDEX;
    const bool threeargs = MAXARG >= 3 && (TOPs || ((void)POPs,0));

    SensorCall(12266);if (threeargs)
	offset = POPi;
    SensorCall(12267);little = POPs;
    big = POPs;
    big_p = SvPV_const(big, biglen);
    little_p = SvPV_const(little, llen);

    big_utf8 = DO_UTF8(big);
    little_utf8 = DO_UTF8(little);
    SensorCall(12279);if (big_utf8 ^ little_utf8) {
	/* One needs to be upgraded.  */
	SensorCall(12268);if (little_utf8 && !PL_encoding) {
	    /* Well, maybe instead we might be able to downgrade the small
	       string?  */
	    SensorCall(12269);char * const pv = (char*)bytes_from_utf8((U8 *)little_p, &llen,
						     &little_utf8);
	    SensorCall(12272);if (little_utf8) {
		/* If the large string is ISO-8859-1, and it's not possible to
		   convert the small string to ISO-8859-1, then there is no
		   way that it could be found anywhere by index.  */
		SensorCall(12270);retval = -1;
		SensorCall(12271);goto fail;
	    }

	    /* At this point, pv is a malloc()ed string. So donate it to temp
	       to ensure it will get free()d  */
	    SensorCall(12273);little = temp = newSV(0);
	    sv_usepvn(temp, pv, llen);
	    little_p = SvPVX(little);
	} else {
	    SensorCall(12274);temp = little_utf8
		? newSVpvn(big_p, biglen) : newSVpvn(little_p, llen);

	    SensorCall(12275);if (PL_encoding) {
		sv_recode_to_utf8(temp, PL_encoding);
	    } else {
		sv_utf8_upgrade(temp);
	    }
	    SensorCall(12278);if (little_utf8) {
		SensorCall(12276);big = temp;
		big_utf8 = TRUE;
		big_p = SvPV_const(big, biglen);
	    } else {
		SensorCall(12277);little = temp;
		little_p = SvPV_const(little, llen);
	    }
	}
    }
    SensorCall(12281);if (SvGAMAGIC(big)) {
	/* Life just becomes a lot easier if I use a temporary here.
	   Otherwise I need to avoid calls to sv_pos_u2b(), which (dangerously)
	   will trigger magic and overloading again, as will fbm_instr()
	*/
	SensorCall(12280);big = newSVpvn_flags(big_p, biglen,
			     SVs_TEMP | (big_utf8 ? SVf_UTF8 : 0));
	big_p = SvPVX(big);
    }
    SensorCall(12283);if (SvGAMAGIC(little) || (is_index && !SvOK(little))) {
	/* index && SvOK() is a hack. fbm_instr() calls SvPV_const, which will
	   warn on undef, and we've already triggered a warning with the
	   SvPV_const some lines above. We can't remove that, as we need to
	   call some SvPV to trigger overloading early and find out if the
	   string is UTF-8.
	   This is all getting to messy. The API isn't quite clean enough,
	   because data access has side effects.
	*/
	SensorCall(12282);little = newSVpvn_flags(little_p, llen,
				SVs_TEMP | (little_utf8 ? SVf_UTF8 : 0));
	little_p = SvPVX(little);
    }

    SensorCall(12288);if (!threeargs)
	{/*137*/SensorCall(12284);offset = is_index ? 0 : biglen;/*138*/}
    else {
	SensorCall(12285);if (big_utf8 && offset > 0)
	    sv_pos_u2b(big, &offset, 0);
	SensorCall(12287);if (!is_index)
	    {/*139*/SensorCall(12286);offset += llen;/*140*/}
    }
    SensorCall(12292);if (offset < 0)
	{/*141*/SensorCall(12289);offset = 0;/*142*/}
    else {/*143*/SensorCall(12290);if (offset > (I32)biglen)
	{/*145*/SensorCall(12291);offset = biglen;/*146*/}/*144*/}
    SensorCall(12296);if (!(little_p = is_index
	  ? fbm_instr((unsigned char*)big_p + offset,
		      (unsigned char*)big_p + biglen, little, 0)
	  : rninstr(big_p,  big_p  + offset,
		    little_p, little_p + llen)))
	{/*147*/SensorCall(12293);retval = -1;/*148*/}
    else {
	SensorCall(12294);retval = little_p - big_p;
	SensorCall(12295);if (retval > 0 && big_utf8)
	    sv_pos_b2u(big, &retval);
    }
    SvREFCNT_dec(temp);
 fail:
    PUSHi(retval);
    RETURN;
}

PP(pp_sprintf)
{
SensorCall(12297);    dVAR; dSP; dMARK; dORIGMARK; dTARGET;
    SvTAINTED_off(TARG);
    do_sprintf(TARG, SP-MARK, MARK+1);
    TAINT_IF(SvTAINTED(TARG));
    SP = ORIGMARK;
    PUSHTARG;
    RETURN;
}

PP(pp_ord)
{
SensorCall(12298);    dVAR; dSP; dTARGET;

    SV *argsv = POPs;
    STRLEN len;
    const U8 *s = (U8*)SvPV_const(argsv, len);

    SensorCall(12300);if (PL_encoding && SvPOK(argsv) && !DO_UTF8(argsv)) {
        SensorCall(12299);SV * const tmpsv = sv_2mortal(newSVsv(argsv));
        s = (U8*)sv_recode_to_utf8(tmpsv, PL_encoding);
        argsv = tmpsv;
    }

    XPUSHu(DO_UTF8(argsv) ?
	   utf8n_to_uvchr(s, UTF8_MAXBYTES, 0, UTF8_ALLOW_ANYUV) :
	   (UV)(*s & 0xff));

    RETURN;
}

PP(pp_chr)
{
SensorCall(12301);    dVAR; dSP; dTARGET;
    char *tmps;
    UV value;

    SensorCall(12306);if (((SvIOK_notUV(TOPs) && SvIV(TOPs) < 0)
	 ||
	 (SvNOK(TOPs) && SvNV(TOPs) < 0.0))) {
	SensorCall(12302);if (IN_BYTES) {
	    SensorCall(12303);value = POPu; /* chr(-1) eq chr(0xff), etc. */
	} else {
	    SensorCall(12304);(void) POPs; /* Ignore the argument value. */
	    value = UNICODE_REPLACEMENT;
	}
    } else {
	SensorCall(12305);value = POPu;
    }

    SvUPGRADE(TARG,SVt_PV);

    SensorCall(12308);if (value > 255 && !IN_BYTES) {
	SvGROW(TARG, (STRLEN)UNISKIP(value)+1);
	SensorCall(12307);tmps = (char*)uvchr_to_utf8_flags((U8*)SvPVX(TARG), value, 0);
	SvCUR_set(TARG, tmps - SvPVX_const(TARG));
	*tmps = '\0';
	(void)SvPOK_only(TARG);
	SvUTF8_on(TARG);
	XPUSHs(TARG);
	RETURN;
    }

    SvGROW(TARG,2);
    SvCUR_set(TARG, 1);
    SensorCall(12309);tmps = SvPVX(TARG);
    *tmps++ = (char)value;
    *tmps = '\0';
    (void)SvPOK_only(TARG);

    SensorCall(12313);if (PL_encoding && !IN_BYTES) {
        sv_recode_to_utf8(TARG, PL_encoding);
	SensorCall(12310);tmps = SvPVX(TARG);
	SensorCall(12312);if (SvCUR(TARG) == 0 || !is_utf8_string((U8*)tmps, SvCUR(TARG)) ||
	    UNICODE_IS_REPLACEMENT(utf8_to_uvchr_buf((U8*)tmps, (U8*) tmps + SvCUR(TARG), NULL))) {
	    SvGROW(TARG, 2);
	    SensorCall(12311);tmps = SvPVX(TARG);
	    SvCUR_set(TARG, 1);
	    *tmps++ = (char)value;
	    *tmps = '\0';
	    SvUTF8_off(TARG);
	}
    }

    XPUSHs(TARG);
    RETURN;
}

PP(pp_crypt)
{
SensorCall(12314);
#ifdef HAS_CRYPT
    dVAR; dSP; dTARGET;
    dPOPTOPssrl;
    STRLEN len;
    const char *tmps = SvPV_const(left, len);

    SensorCall(12316);if (DO_UTF8(left)) {
         /* If Unicode, try to downgrade.
	  * If not possible, croak.
	  * Yes, we made this up.  */
	 SensorCall(12315);SV* const tsv = sv_2mortal(newSVsv(left));

	 SvUTF8_on(tsv);
	 sv_utf8_downgrade(tsv, FALSE);
	 tmps = SvPV_const(tsv, len);
    }
#   ifdef USE_ITHREADS
#     ifdef HAS_CRYPT_R
    SensorCall(12318);if (!PL_reentrant_buffer->_crypt_struct_buffer) {
      /* This should be threadsafe because in ithreads there is only
       * one thread per interpreter.  If this would not be true,
       * we would need a mutex to protect this malloc. */
        PL_reentrant_buffer->_crypt_struct_buffer =
	  (struct crypt_data *)safemalloc(sizeof(struct crypt_data));
#if defined(__GLIBC__) || defined(__EMX__)
	SensorCall(12317);if (PL_reentrant_buffer->_crypt_struct_buffer) {
	    PL_reentrant_buffer->_crypt_struct_buffer->initialized = 0;
	    /* work around glibc-2.2.5 bug */
	    PL_reentrant_buffer->_crypt_struct_buffer->current_saltbits = 0;
	}
#endif
    }
#     endif /* HAS_CRYPT_R */
#   endif /* USE_ITHREADS */
#   ifdef FCRYPT
    sv_setpv(TARG, fcrypt(tmps, SvPV_nolen_const(right)));
#   else
    sv_setpv(TARG, PerlProc_crypt(tmps, SvPV_nolen_const(right)));
#   endif
    SETTARG;
    RETURN;
#else
    DIE(aTHX_
      "The crypt() function is unimplemented due to excessive paranoia.");
#endif
}

/* Generally UTF-8 and UTF-EBCDIC are indistinguishable at this level.  So 
 * most comments below say UTF-8, when in fact they mean UTF-EBCDIC as well */

/* Generates code to store a unicode codepoint c that is known to occupy
 * exactly two UTF-8 and UTF-EBCDIC bytes; it is stored into p and p+1,
 * and p is advanced to point to the next available byte after the two bytes */
#define CAT_UNI_TO_UTF8_TWO_BYTE(p, c)					    \
    STMT_START {							    \
	*(p)++ = UTF8_TWO_BYTE_HI(c);					    \
	*((p)++) = UTF8_TWO_BYTE_LO(c);					    \
    } STMT_END

PP(pp_ucfirst)
{
SensorCall(12319);    /* Actually is both lcfirst() and ucfirst().  Only the first character
     * changes.  This means that possibly we can change in-place, ie., just
     * take the source and change that one character and store it back, but not
     * if read-only etc, or if the length changes */

    dVAR;
    dSP;
    SV *source = TOPs;
    STRLEN slen; /* slen is the byte length of the whole SV. */
    STRLEN need;
    SV *dest;
    bool inplace;   /* ? Convert first char only, in-place */
    bool doing_utf8 = FALSE;		   /* ? using utf8 */
    bool convert_source_to_utf8 = FALSE;   /* ? need to convert */
    const int op_type = PL_op->op_type;
    const U8 *s;
    U8 *d;
    U8 tmpbuf[UTF8_MAXBYTES_CASE+1];
    STRLEN ulen;    /* ulen is the byte length of the original Unicode character
		     * stored as UTF-8 at s. */
    STRLEN tculen;  /* tculen is the byte length of the freshly titlecased (or
		     * lowercased) character stored in tmpbuf.  May be either
		     * UTF-8 or not, but in either case is the number of bytes */
    bool tainted = FALSE;

    SvGETMAGIC(source);
    SensorCall(12323);if (SvOK(source)) {
	SensorCall(12320);s = (const U8*)SvPV_nomg_const(source, slen);
    } else {
	SensorCall(12321);if (ckWARN(WARN_UNINITIALIZED))
	    report_uninit(source);
	SensorCall(12322);s = (const U8*)"";
	slen = 0;
    }

    /* We may be able to get away with changing only the first character, in
     * place, but not if read-only, etc.  Later we may discover more reasons to
     * not convert in-place. */
    SensorCall(12324);inplace = SvPADTMP(source) && !SvREADONLY(source) && SvTEMP(source);

    /* First calculate what the changed first character should be.  This affects
     * whether we can just swap it out, leaving the rest of the string unchanged,
     * or even if have to convert the dest to UTF-8 when the source isn't */

    SensorCall(12346);if (! slen) {   /* If empty */
	SensorCall(12325);need = 1; /* still need a trailing NUL */
	ulen = 0;
    }
    else {/*387*/SensorCall(12326);if (DO_UTF8(source)) {	/* Is the source utf8? */
	SensorCall(12327);doing_utf8 = TRUE;
        ulen = UTF8SKIP(s);
        SensorCall(12328);if (op_type == OP_UCFIRST) {
	    _to_utf8_title_flags(s, tmpbuf, &tculen,
				 cBOOL(IN_LOCALE_RUNTIME), &tainted);
	}
        else {
	    _to_utf8_lower_flags(s, tmpbuf, &tculen,
				 cBOOL(IN_LOCALE_RUNTIME), &tainted);
	}

        /* we can't do in-place if the length changes.  */
        SensorCall(12329);if (ulen != tculen) inplace = FALSE;
        SensorCall(12330);need = slen + 1 - ulen + tculen;
    }
    else { /* Non-zero length, non-UTF-8,  Need to consider locale and if
	    * latin1 is treated as caseless.  Note that a locale takes
	    * precedence */ 
	SensorCall(12331);ulen = 1;	/* Original character is 1 byte */
	tculen = 1;	/* Most characters will require one byte, but this will
			 * need to be overridden for the tricky ones */
	need = slen + 1;

	SensorCall(12345);if (op_type == OP_LCFIRST) {

	    /* lower case the first letter: no trickiness for any character */
	    SensorCall(12332);*tmpbuf = (IN_LOCALE_RUNTIME) ? toLOWER_LC(*s) :
			((IN_UNI_8_BIT) ? toLOWER_LATIN1(*s) : toLOWER(*s));
	}
	/* is ucfirst() */
	else {/*389*/SensorCall(12333);if (IN_LOCALE_RUNTIME) {
	    SensorCall(12334);*tmpbuf = toUPPER_LC(*s);	/* This would be a bug if any locales
					 * have upper and title case different
					 */
	}
	else {/*391*/SensorCall(12335);if (! IN_UNI_8_BIT) {
	    SensorCall(12336);*tmpbuf = toUPPER(*s);	/* Returns caseless for non-ascii, or
					 * on EBCDIC machines whatever the
					 * native function does */
	}
	else { /* is ucfirst non-UTF-8, not in locale, and cased latin1 */
	    SensorCall(12337);UV title_ord = _to_upper_title_latin1(*s, tmpbuf, &tculen, 's');
	    SensorCall(12344);if (tculen > 1) {
		assert(tculen == 2);

                /* If the result is an upper Latin1-range character, it can
                 * still be represented in one byte, which is its ordinal */
		SensorCall(12338);if (UTF8_IS_DOWNGRADEABLE_START(*tmpbuf)) {
		    SensorCall(12339);*tmpbuf = (U8) title_ord;
		    tculen = 1;
		}
		else {
                    /* Otherwise it became more than one ASCII character (in
                     * the case of LATIN_SMALL_LETTER_SHARP_S) or changed to
                     * beyond Latin1, so the number of bytes changed, so can't
                     * replace just the first character in place. */
		    SensorCall(12340);inplace = FALSE;

		    /* If the result won't fit in a byte, the entire result will
		     * have to be in UTF-8.  Assume worst case sizing in
		     * conversion. (all latin1 characters occupy at most two bytes
		     * in utf8) */
		    SensorCall(12343);if (title_ord > 255) {
			SensorCall(12341);doing_utf8 = TRUE;
			convert_source_to_utf8 = TRUE;
			need = slen * 2 + 1;

                        /* The (converted) UTF-8 and UTF-EBCDIC lengths of all
                         * (both) characters whose title case is above 255 is
                         * 2. */
			ulen = 2;
		    }
                    else { /* LATIN_SMALL_LETTER_SHARP_S expands by 1 byte */
			SensorCall(12342);need = slen + 1 + 1;
		    }
		}
	    }
	;/*392*/}/*390*/}} /* End of use Unicode (Latin1) semantics */
    ;/*388*/}} /* End of changing the case of the first character */

    /* Here, have the first character's changed case stored in tmpbuf.  Ready to
     * generate the result */
    SensorCall(12349);if (inplace) {

	/* We can convert in place.  This means we change just the first
	 * character without disturbing the rest; no need to grow */
	SensorCall(12347);dest = source;
	s = d = (U8*)SvPV_force_nomg(source, slen);
    } else {
	dTARGET;

	SensorCall(12348);dest = TARG;

	/* Here, we can't convert in place; we earlier calculated how much
	 * space we will need, so grow to accommodate that */
	SvUPGRADE(dest, SVt_PV);
	d = (U8*)SvGROW(dest, need);
	(void)SvPOK_only(dest);

	SETs(dest);
    }

    SensorCall(12366);if (doing_utf8) {
	SensorCall(12350);if (! inplace) {
	    SensorCall(12351);if (! convert_source_to_utf8) {

		/* Here  both source and dest are in UTF-8, but have to create
		 * the entire output.  We initialize the result to be the
		 * title/lower cased first character, and then append the rest
		 * of the string. */
		sv_setpvn(dest, (char*)tmpbuf, tculen);
		SensorCall(12352);if (slen > ulen) {
		    sv_catpvn(dest, (char*)(s + ulen), slen - ulen);
		}
	    }
	    else {
		SensorCall(12353);const U8 *const send = s + slen;

		/* Here the dest needs to be in UTF-8, but the source isn't,
		 * except we earlier UTF-8'd the first character of the source
		 * into tmpbuf.  First put that into dest, and then append the
		 * rest of the source, converting it to UTF-8 as we go. */

		/* Assert tculen is 2 here because the only two characters that
		 * get to this part of the code have 2-byte UTF-8 equivalents */
		*d++ = *tmpbuf;
		*d++ = *(tmpbuf + 1);
		s++;	/* We have just processed the 1st char */

		SensorCall(12355);for (; s < send; s++) {
		    SensorCall(12354);d = uvchr_to_utf8(d, *s);
		}
		SensorCall(12356);*d = '\0';
		SvCUR_set(dest, d - (U8*)SvPVX_const(dest));
	    }
	    SvUTF8_on(dest);
	}
	else {   /* in-place UTF-8.  Just overwrite the first character */
	    Copy(tmpbuf, d, tculen, U8);
	    SvCUR_set(dest, need - 1);
	}

	SensorCall(12357);if (tainted) {
	    TAINT;
	    SvTAINTED_on(dest);
	}
    }
    else {  /* Neither source nor dest are in or need to be UTF-8 */
	SensorCall(12358);if (slen) {
	    SensorCall(12359);if (IN_LOCALE_RUNTIME) {
		TAINT;
		SvTAINTED_on(dest);
	    }
	    SensorCall(12362);if (inplace) {  /* in-place, only need to change the 1st char */
		SensorCall(12360);*d = *tmpbuf;
	    }
	    else {	/* Not in-place */

		/* Copy the case-changed character(s) from tmpbuf */
		Copy(tmpbuf, d, tculen, U8);
		SensorCall(12361);d += tculen - 1; /* Code below expects d to point to final
				  * character stored */
	    }
	}
	else {	/* empty source */
	    /* See bug #39028: Don't taint if empty  */
	    SensorCall(12363);*d = *s;
	}

	/* In a "use bytes" we don't treat the source as UTF-8, but, still want
	 * the destination to retain that flag */
	SensorCall(12364);if (SvUTF8(source))
	    SvUTF8_on(dest);

	SensorCall(12365);if (!inplace) {	/* Finish the rest of the string, unchanged */
	    /* This will copy the trailing NUL  */
	    Copy(s + 1, d + 1, slen, U8);
	    SvCUR_set(dest, need - 1);
	}
    }
    SensorCall(12367);if (dest != source && SvTAINTED(source))
	SvTAINT(dest);
    SvSETMAGIC(dest);
    RETURN;
}

/* There's so much setup/teardown code common between uc and lc, I wonder if
   it would be worth merging the two, and just having a switch outside each
   of the three tight loops.  There is less and less commonality though */
PP(pp_uc)
{
SensorCall(12368);    dVAR;
    dSP;
    SV *source = TOPs;
    STRLEN len;
    STRLEN min;
    SV *dest;
    const U8 *s;
    U8 *d;

    SvGETMAGIC(source);

    SensorCall(12376);if (SvPADTMP(source) && !SvREADONLY(source) && !SvAMAGIC(source)
	&& SvTEMP(source) && !DO_UTF8(source)
	&& (IN_LOCALE_RUNTIME || ! IN_UNI_8_BIT)) {

	/* We can convert in place.  The reason we can't if in UNI_8_BIT is to
	 * make the loop tight, so we overwrite the source with the dest before
	 * looking at it, and we need to look at the original source
	 * afterwards.  There would also need to be code added to handle
	 * switching to not in-place in midstream if we run into characters
	 * that change the length.
	 */
	SensorCall(12369);dest = source;
	s = d = (U8*)SvPV_force_nomg(source, len);
	min = len + 1;
    } else {
	dTARGET;

	SensorCall(12370);dest = TARG;

	/* The old implementation would copy source into TARG at this point.
	   This had the side effect that if source was undef, TARG was now
	   an undefined SV with PADTMP set, and they don't warn inside
	   sv_2pv_flags(). However, we're now getting the PV direct from
	   source, which doesn't have PADTMP set, so it would warn. Hence the
	   little games.  */

	SensorCall(12374);if (SvOK(source)) {
	    SensorCall(12371);s = (const U8*)SvPV_nomg_const(source, len);
	} else {
	    SensorCall(12372);if (ckWARN(WARN_UNINITIALIZED))
		report_uninit(source);
	    SensorCall(12373);s = (const U8*)"";
	    len = 0;
	}
	SensorCall(12375);min = len + 1;

	SvUPGRADE(dest, SVt_PV);
	d = (U8*)SvGROW(dest, min);
	(void)SvPOK_only(dest);

	SETs(dest);
    }

    /* Overloaded values may have toggled the UTF-8 flag on source, so we need
       to check DO_UTF8 again here.  */

    SensorCall(12414);if (DO_UTF8(source)) {
	SensorCall(12377);const U8 *const send = s + len;
	U8 tmpbuf[UTF8_MAXBYTES+1];
	bool tainted = FALSE;

	/* All occurrences of these are to be moved to follow any other marks.
	 * This is context-dependent.  We may not be passed enough context to
	 * move the iota subscript beyond all of them, but we do the best we can
	 * with what we're given.  The result is always better than if we
	 * hadn't done this.  And, the problem would only arise if we are
	 * passed a character without all its combining marks, which would be
	 * the caller's mistake.  The information this is based on comes from a
	 * comment in Unicode SpecialCasing.txt, (and the Standard's text
	 * itself) and so can't be checked properly to see if it ever gets
	 * revised.  But the likelihood of it changing is remote */
	bool in_iota_subscript = FALSE;

	SensorCall(12388);while (s < send) {
	    SensorCall(12378);STRLEN u;
	    STRLEN ulen;
	    UV uv;
	    SensorCall(12380);if (in_iota_subscript && ! is_utf8_mark(s)) {

		/* A non-mark.  Time to output the iota subscript */
#define GREEK_CAPITAL_LETTER_IOTA 0x0399
#define COMBINING_GREEK_YPOGEGRAMMENI 0x0345

		CAT_UNI_TO_UTF8_TWO_BYTE(d, GREEK_CAPITAL_LETTER_IOTA);
		SensorCall(12379);in_iota_subscript = FALSE;
            }

            /* Then handle the current character.  Get the changed case value
             * and copy it to the output buffer */

            SensorCall(12381);u = UTF8SKIP(s);
            uv = _to_utf8_upper_flags(s, tmpbuf, &ulen,
				      cBOOL(IN_LOCALE_RUNTIME), &tainted);
            SensorCall(12386);if (uv == GREEK_CAPITAL_LETTER_IOTA
                && utf8_to_uvchr_buf(s, send, 0) == COMBINING_GREEK_YPOGEGRAMMENI)
            {
                SensorCall(12382);in_iota_subscript = TRUE;
            }
            else {
                SensorCall(12383);if (ulen > u && (SvLEN(dest) < (min += ulen - u))) {
                    /* If the eventually required minimum size outgrows the
                     * available space, we need to grow. */
                    SensorCall(12384);const UV o = d - (U8*)SvPVX_const(dest);

                    /* If someone uppercases one million U+03B0s we SvGROW()
                     * one million times.  Or we could try guessing how much to
                     * allocate without allocating too much.  Such is life.
                     * See corresponding comment in lc code for another option
                     * */
                    SvGROW(dest, min);
                    d = (U8*)SvPVX(dest) + o;
                }
                Copy(tmpbuf, d, ulen, U8);
                SensorCall(12385);d += ulen;
            }
            SensorCall(12387);s += u;
	}
	SensorCall(12389);if (in_iota_subscript) {
	    CAT_UNI_TO_UTF8_TWO_BYTE(d, GREEK_CAPITAL_LETTER_IOTA);
	}
	SvUTF8_on(dest);
	SensorCall(12390);*d = '\0';

	SvCUR_set(dest, d - (U8*)SvPVX_const(dest));
	SensorCall(12391);if (tainted) {
	    TAINT;
	    SvTAINTED_on(dest);
	}
    }
    else {	/* Not UTF-8 */
	SensorCall(12392);if (len) {
	    SensorCall(12393);const U8 *const send = s + len;

	    /* Use locale casing if in locale; regular style if not treating
	     * latin1 as having case; otherwise the latin1 casing.  Do the
	     * whole thing in a tight loop, for speed, */
	    SensorCall(12411);if (IN_LOCALE_RUNTIME) {
		TAINT;
		SvTAINTED_on(dest);
		SensorCall(12394);for (; s < send; d++, s++)
		    *d = toUPPER_LC(*s);
	    }
	    else {/*383*/SensorCall(12395);if (! IN_UNI_8_BIT) {
		SensorCall(12396);for (; s < send; d++, s++) {
		    SensorCall(12397);*d = toUPPER(*s);
		}
	    }
	    else {
		SensorCall(12398);for (; s < send; d++, s++) {
		    SensorCall(12399);*d = toUPPER_LATIN1_MOD(*s);
		    SensorCall(12401);if (LIKELY(*d != LATIN_SMALL_LETTER_Y_WITH_DIAERESIS)) {/*385*/SensorCall(12400);continue;/*386*/}

		    /* The mainstream case is the tight loop above.  To avoid
		     * extra tests in that, all three characters that require
		     * special handling are mapped by the MOD to the one tested
		     * just above.  
		     * Use the source to distinguish between the three cases */

		    SensorCall(12406);if (*s == LATIN_SMALL_LETTER_SHARP_S) {

			/* uc() of this requires 2 characters, but they are
			 * ASCII.  If not enough room, grow the string */
			SensorCall(12402);if (SvLEN(dest) < ++min) {	
			    SensorCall(12403);const UV o = d - (U8*)SvPVX_const(dest);
			    SvGROW(dest, min);
			    d = (U8*)SvPVX(dest) + o;
			}
			SensorCall(12404);*d++ = 'S'; *d = 'S'; /* upper case is 'SS' */
			SensorCall(12405);continue;   /* Back to the tight loop; still in ASCII */
		    }

		    /* The other two special handling characters have their
		     * upper cases outside the latin1 range, hence need to be
		     * in UTF-8, so the whole result needs to be in UTF-8.  So,
		     * here we are somewhere in the middle of processing a
		     * non-UTF-8 string, and realize that we will have to convert
		     * the whole thing to UTF-8.  What to do?  There are
		     * several possibilities.  The simplest to code is to
		     * convert what we have so far, set a flag, and continue on
		     * in the loop.  The flag would be tested each time through
		     * the loop, and if set, the next character would be
		     * converted to UTF-8 and stored.  But, I (khw) didn't want
		     * to slow down the mainstream case at all for this fairly
		     * rare case, so I didn't want to add a test that didn't
		     * absolutely have to be there in the loop, besides the
		     * possibility that it would get too complicated for
		     * optimizers to deal with.  Another possibility is to just
		     * give up, convert the source to UTF-8, and restart the
		     * function that way.  Another possibility is to convert
		     * both what has already been processed and what is yet to
		     * come separately to UTF-8, then jump into the loop that
		     * handles UTF-8.  But the most efficient time-wise of the
		     * ones I could think of is what follows, and turned out to
		     * not require much extra code.  */

		    /* Convert what we have so far into UTF-8, telling the
		     * function that we know it should be converted, and to
		     * allow extra space for what we haven't processed yet.
		     * Assume the worst case space requirements for converting
		     * what we haven't processed so far: that it will require
		     * two bytes for each remaining source character, plus the
		     * NUL at the end.  This may cause the string pointer to
		     * move, so re-find it. */

		    SensorCall(12407);len = d - (U8*)SvPVX_const(dest);
		    SvCUR_set(dest, len);
		    len = sv_utf8_upgrade_flags_grow(dest,
						SV_GMAGIC|SV_FORCE_UTF8_UPGRADE,
						(send -s) * 2 + 1);
		    d = (U8*)SvPVX(dest) + len;

		    /* Now process the remainder of the source, converting to
		     * upper and UTF-8.  If a resulting byte is invariant in
		     * UTF-8, output it as-is, otherwise convert to UTF-8 and
		     * append it to the output. */
		    SensorCall(12409);for (; s < send; s++) {
			SensorCall(12408);(void) _to_upper_title_latin1(*s, d, &len, 'S');
			d += len;
		    }

		    /* Here have processed the whole source; no need to continue
		     * with the outer loop.  Each character has been converted
		     * to upper case and converted to UTF-8 */

		    SensorCall(12410);break;
		} /* End of processing all latin1-style chars */
	    ;/*384*/}} /* End of processing all chars */
	} /* End of source is not empty */

	SensorCall(12413);if (source != dest) {
	    SensorCall(12412);*d = '\0';  /* Here d points to 1 after last char, add NUL */
	    SvCUR_set(dest, d - (U8*)SvPVX_const(dest));
	}
    } /* End of isn't utf8 */
    SensorCall(12415);if (dest != source && SvTAINTED(source))
	SvTAINT(dest);
    SvSETMAGIC(dest);
    RETURN;
}

PP(pp_lc)
{
SensorCall(12416);    dVAR;
    dSP;
    SV *source = TOPs;
    STRLEN len;
    STRLEN min;
    SV *dest;
    const U8 *s;
    U8 *d;

    SvGETMAGIC(source);

    SensorCall(12424);if (SvPADTMP(source) && !SvREADONLY(source) && !SvAMAGIC(source)
	&& SvTEMP(source) && !DO_UTF8(source)) {

	/* We can convert in place, as lowercasing anything in the latin1 range
	 * (or else DO_UTF8 would have been on) doesn't lengthen it */
	SensorCall(12417);dest = source;
	s = d = (U8*)SvPV_force_nomg(source, len);
	min = len + 1;
    } else {
	dTARGET;

	SensorCall(12418);dest = TARG;

	/* The old implementation would copy source into TARG at this point.
	   This had the side effect that if source was undef, TARG was now
	   an undefined SV with PADTMP set, and they don't warn inside
	   sv_2pv_flags(). However, we're now getting the PV direct from
	   source, which doesn't have PADTMP set, so it would warn. Hence the
	   little games.  */

	SensorCall(12422);if (SvOK(source)) {
	    SensorCall(12419);s = (const U8*)SvPV_nomg_const(source, len);
	} else {
	    SensorCall(12420);if (ckWARN(WARN_UNINITIALIZED))
		report_uninit(source);
	    SensorCall(12421);s = (const U8*)"";
	    len = 0;
	}
	SensorCall(12423);min = len + 1;

	SvUPGRADE(dest, SVt_PV);
	d = (U8*)SvGROW(dest, min);
	(void)SvPOK_only(dest);

	SETs(dest);
    }

    /* Overloaded values may have toggled the UTF-8 flag on source, so we need
       to check DO_UTF8 again here.  */

    SensorCall(12444);if (DO_UTF8(source)) {
	SensorCall(12425);const U8 *const send = s + len;
	U8 tmpbuf[UTF8_MAXBYTES_CASE+1];
	bool tainted = FALSE;

	SensorCall(12430);while (s < send) {
	    SensorCall(12426);const STRLEN u = UTF8SKIP(s);
	    STRLEN ulen;

	    _to_utf8_lower_flags(s, tmpbuf, &ulen,
				 cBOOL(IN_LOCALE_RUNTIME), &tainted);

	    /* Here is where we would do context-sensitive actions.  See the
	     * commit message for this comment for why there isn't any */

	    SensorCall(12428);if (ulen > u && (SvLEN(dest) < (min += ulen - u))) {

		/* If the eventually required minimum size outgrows the
		 * available space, we need to grow. */
		SensorCall(12427);const UV o = d - (U8*)SvPVX_const(dest);

		/* If someone lowercases one million U+0130s we SvGROW() one
		 * million times.  Or we could try guessing how much to
		 * allocate without allocating too much.  Such is life.
		 * Another option would be to grow an extra byte or two more
		 * each time we need to grow, which would cut down the million
		 * to 500K, with little waste */
		SvGROW(dest, min);
		d = (U8*)SvPVX(dest) + o;
	    }

	    /* Copy the newly lowercased letter to the output buffer we're
	     * building */
	    Copy(tmpbuf, d, ulen, U8);
	    SensorCall(12429);d += ulen;
	    s += u;
	}   /* End of looping through the source string */
	SvUTF8_on(dest);
	SensorCall(12431);*d = '\0';
	SvCUR_set(dest, d - (U8*)SvPVX_const(dest));
	SensorCall(12432);if (tainted) {
	    TAINT;
	    SvTAINTED_on(dest);
	}
    } else {	/* Not utf8 */
	SensorCall(12433);if (len) {
	    SensorCall(12434);const U8 *const send = s + len;

	    /* Use locale casing if in locale; regular style if not treating
	     * latin1 as having case; otherwise the latin1 casing.  Do the
	     * whole thing in a tight loop, for speed, */
	    SensorCall(12441);if (IN_LOCALE_RUNTIME) {
		TAINT;
		SvTAINTED_on(dest);
		SensorCall(12435);for (; s < send; d++, s++)
		    *d = toLOWER_LC(*s);
	    }
	    else {/*151*/SensorCall(12436);if (! IN_UNI_8_BIT) {
		SensorCall(12437);for (; s < send; d++, s++) {
		    SensorCall(12438);*d = toLOWER(*s);
		}
	    }
	    else {
		SensorCall(12439);for (; s < send; d++, s++) {
		    SensorCall(12440);*d = toLOWER_LATIN1(*s);
		}
	    ;/*152*/}}
	}
	SensorCall(12443);if (source != dest) {
	    SensorCall(12442);*d = '\0';
	    SvCUR_set(dest, d - (U8*)SvPVX_const(dest));
	}
    }
    SensorCall(12445);if (dest != source && SvTAINTED(source))
	SvTAINT(dest);
    SvSETMAGIC(dest);
    RETURN;
}

PP(pp_quotemeta)
{
SensorCall(12446);    dVAR; dSP; dTARGET;
    SV * const sv = TOPs;
    STRLEN len;
    register const char *s = SvPV_const(sv,len);

    SvUTF8_off(TARG);				/* decontaminate */
    SensorCall(12476);if (len) {
	SensorCall(12447);register char *d;
	SvUPGRADE(TARG, SVt_PV);
	SvGROW(TARG, (len * 2) + 1);
	d = SvPVX(TARG);
	SensorCall(12474);if (DO_UTF8(sv)) {
	    SensorCall(12448);while (len) {
		SensorCall(12449);STRLEN ulen = UTF8SKIP(s);
		bool to_quote = FALSE;

		SensorCall(12457);if (UTF8_IS_INVARIANT(*s)) {
		    SensorCall(12450);if (_isQUOTEMETA(*s)) {
			SensorCall(12451);to_quote = TRUE;
		    }
		}
		else {/*215*/SensorCall(12452);if (UTF8_IS_DOWNGRADEABLE_START(*s)) {

		    /* In locale, we quote all non-ASCII Latin1 chars.
		     * Otherwise use the quoting rules */
		    SensorCall(12453);if (IN_LOCALE_RUNTIME
			|| _isQUOTEMETA(TWO_BYTE_UTF8_TO_UNI(*s, *(s + 1))))
		    {
			SensorCall(12454);to_quote = TRUE;
		    }
		}
		else {/*217*/SensorCall(12455);if (_is_utf8_quotemeta((U8 *) s)) {
		    SensorCall(12456);to_quote = TRUE;
		;/*218*/}/*216*/}}

		SensorCall(12459);if (to_quote) {
		    SensorCall(12458);*d++ = '\\';
		}
		SensorCall(12461);if (ulen > len)
		    {/*219*/SensorCall(12460);ulen = len;/*220*/}
		SensorCall(12462);len -= ulen;
		SensorCall(12464);while (ulen--)
		    {/*221*/SensorCall(12463);*d++ = *s++;/*222*/}
	    }
	    SvUTF8_on(TARG);
	}
	else {/*223*/SensorCall(12465);if (IN_UNI_8_BIT) {
	    SensorCall(12466);while (len--) {
		SensorCall(12467);if (_isQUOTEMETA(*s))
		    {/*225*/SensorCall(12468);*d++ = '\\';/*226*/}
		SensorCall(12469);*d++ = *s++;
	    }
	}
	else {
	    /* For non UNI_8_BIT (and hence in locale) just quote all \W
	     * including everything above ASCII */
	    SensorCall(12470);while (len--) {
		SensorCall(12471);if (!isWORDCHAR_A(*s))
		    {/*227*/SensorCall(12472);*d++ = '\\';/*228*/}
		SensorCall(12473);*d++ = *s++;
	    }
	;/*224*/}}
	SensorCall(12475);*d = '\0';
	SvCUR_set(TARG, d - SvPVX_const(TARG));
	(void)SvPOK_only_UTF8(TARG);
    }
    else
	sv_setpvn(TARG, s, len);
    SETTARG;
    RETURN;
}

PP(pp_fc)
{
SensorCall(12477);    dVAR;
    dTARGET;
    dSP;
    SV *source = TOPs;
    STRLEN len;
    STRLEN min;
    SV *dest;
    const U8 *s;
    const U8 *send;
    U8 *d;
    U8 tmpbuf[UTF8_MAXBYTES * UTF8_MAX_FOLD_CHAR_EXPAND + 1];
    const bool full_folding = TRUE;
    const U8 flags = ( full_folding      ? FOLD_FLAGS_FULL   : 0 )
                   | ( IN_LOCALE_RUNTIME ? FOLD_FLAGS_LOCALE : 0 );

    /* This is a facsimile of pp_lc, but with a thousand bugs thanks to me.
     * You are welcome(?) -Hugmeir
     */

    SvGETMAGIC(source);

    dest = TARG;

    SensorCall(12481);if (SvOK(source)) {
        SensorCall(12478);s = (const U8*)SvPV_nomg_const(source, len);
    } else {
        SensorCall(12479);if (ckWARN(WARN_UNINITIALIZED))
	    report_uninit(source);
	SensorCall(12480);s = (const U8*)"";
	len = 0;
    }

    SensorCall(12482);min = len + 1;

    SvUPGRADE(dest, SVt_PV);
    d = (U8*)SvGROW(dest, min);
    (void)SvPOK_only(dest);

    SETs(dest);

    send = s + len;
    SensorCall(12510);if (DO_UTF8(source)) { /* UTF-8 flagged string. */
        bool tainted = FALSE;
        SensorCall(12487);while (s < send) {
            SensorCall(12483);const STRLEN u = UTF8SKIP(s);
            STRLEN ulen;

            _to_utf8_fold_flags(s, tmpbuf, &ulen, flags, &tainted);

            SensorCall(12485);if (ulen > u && (SvLEN(dest) < (min += ulen - u))) {
                SensorCall(12484);const UV o = d - (U8*)SvPVX_const(dest);
                SvGROW(dest, min);
                d = (U8*)SvPVX(dest) + o;
            }

            Copy(tmpbuf, d, ulen, U8);
            SensorCall(12486);d += ulen;
            s += u;
        }
        SvUTF8_on(dest);
	SensorCall(12488);if (tainted) {
	    TAINT;
	    SvTAINTED_on(dest);
	}
    } /* Unflagged string */
    else {/*117*/SensorCall(12489);if (len) {
        /* For locale, bytes, and nothing, the behavior is supposed to be the
         * same as lc().
         */
        SensorCall(12490);if ( IN_LOCALE_RUNTIME ) { /* Under locale */
            TAINT;
            SvTAINTED_on(dest);
            SensorCall(12491);for (; s < send; d++, s++)
                *d = toLOWER_LC(*s);
        }
        else {/*119*/SensorCall(12492);if ( !IN_UNI_8_BIT ) { /* Under nothing, or bytes */
            SensorCall(12493);for (; s < send; d++, s++)
                *d = toLOWER(*s);
        }
        else {
            /* For ASCII and the Latin-1 range, there's only two troublesome folds,
            * \x{DF} (\N{LATIN SMALL LETTER SHARP S}), which under full casefolding
            * becomes 'ss', and \x{B5} (\N{MICRO SIGN}), which under any fold becomes
            * \x{3BC} (\N{GREEK SMALL LETTER MU}) -- For the rest, the casefold is
            * their lowercase.
            */
            SensorCall(12494);for (; s < send; d++, s++) {
                SensorCall(12495);if (*s == MICRO_SIGN) {
                    /* \N{MICRO SIGN}'s casefold is \N{GREEK SMALL LETTER MU}, which
                    * is outside of the latin-1 range. There's a couple of ways to
                    * deal with this -- khw discusses them in pp_lc/uc, so go there :)
                    * What we do here is upgrade what we had already casefolded,
                    * then enter an inner loop that appends the rest of the characters
                    * as UTF-8.
                    */
                    SensorCall(12496);len = d - (U8*)SvPVX_const(dest);
                    SvCUR_set(dest, len);
                    len = sv_utf8_upgrade_flags_grow(dest,
                                                SV_GMAGIC|SV_FORCE_UTF8_UPGRADE,
						/* The max expansion for latin1
						 * chars is 1 byte becomes 2 */
                                                (send -s) * 2 + 1);
                    d = (U8*)SvPVX(dest) + len;

                    CAT_UNI_TO_UTF8_TWO_BYTE(d, GREEK_SMALL_LETTER_MU);
                    s++;
                    SensorCall(12503);for (; s < send; s++) {
                        SensorCall(12497);STRLEN ulen;
                        UV fc = _to_uni_fold_flags(*s, tmpbuf, &ulen, flags);
                        SensorCall(12502);if UNI_IS_INVARIANT(fc) {
                            SensorCall(12498);if ( full_folding && *s == LATIN_SMALL_LETTER_SHARP_S) {
                                SensorCall(12499);*d++ = 's';
                                *d++ = 's';
                            }
                            else
                                {/*121*/SensorCall(12500);*d++ = (U8)fc;/*122*/}
                        }
                        else {
                            Copy(tmpbuf, d, ulen, U8);
                            SensorCall(12501);d += ulen;
                        }
                    }
                    SensorCall(12504);break;
                }
                else {/*123*/SensorCall(12505);if (full_folding && *s == LATIN_SMALL_LETTER_SHARP_S) {
                    /* Under full casefolding, LATIN SMALL LETTER SHARP S becomes "ss",
                    * which may require growing the SV.
                    */
                    SensorCall(12506);if (SvLEN(dest) < ++min) {
                        SensorCall(12507);const UV o = d - (U8*)SvPVX_const(dest);
                        SvGROW(dest, min);
                        d = (U8*)SvPVX(dest) + o;
                     }
                    SensorCall(12508);*(d)++ = 's';
                    *d = 's';
                }
                else { /* If it's not one of those two, the fold is their lower case */
                    SensorCall(12509);*d = toLOWER_LATIN1(*s);
                ;/*124*/}}
             }
        ;/*120*/}}
    ;/*118*/}}
    SensorCall(12511);*d = '\0';
    SvCUR_set(dest, d - (U8*)SvPVX_const(dest));

    SensorCall(12512);if (SvTAINTED(source))
	SvTAINT(dest);
    SvSETMAGIC(dest);
    RETURN;
}

/* Arrays. */

PP(pp_aslice)
{
SensorCall(12513);    dVAR; dSP; dMARK; dORIGMARK;
    register AV *const av = MUTABLE_AV(POPs);
    register const I32 lval = (PL_op->op_flags & OPf_MOD || LVRET);

    SensorCall(12534);if (SvTYPE(av) == SVt_PVAV) {
	SensorCall(12514);const bool localizing = PL_op->op_private & OPpLVAL_INTRO;
	bool can_preserve = FALSE;

	SensorCall(12516);if (localizing) {
	    SensorCall(12515);MAGIC *mg;
	    HV *stash;

	    can_preserve = SvCANEXISTDELETE(av);
	}

	SensorCall(12523);if (lval && localizing) {
	    SensorCall(12517);register SV **svp;
	    I32 max = -1;
	    SensorCall(12521);for (svp = MARK + 1; svp <= SP; svp++) {
		SensorCall(12518);const I32 elem = SvIV(*svp);
		SensorCall(12520);if (elem > max)
		    {/*77*/SensorCall(12519);max = elem;/*78*/}
	    }
	    SensorCall(12522);if (max > AvMAX(av))
		av_extend(av, max);
	}

	SensorCall(12533);while (++MARK <= SP) {
	    SensorCall(12524);register SV **svp;
	    I32 elem = SvIV(*MARK);
	    bool preeminent = TRUE;

	    SensorCall(12526);if (localizing && can_preserve) {
		/* If we can determine whether the element exist,
		 * Try to preserve the existenceness of a tied array
		 * element by using EXISTS and DELETE if possible.
		 * Fallback to FETCH and STORE otherwise. */
		SensorCall(12525);preeminent = av_exists(av, elem);
	    }

	    SensorCall(12527);svp = av_fetch(av, elem, lval);
	    SensorCall(12531);if (lval) {
		SensorCall(12528);if (!svp || *svp == &PL_sv_undef)
		    DIE(aTHX_ PL_no_aelem, elem);
		SensorCall(12530);if (localizing) {
		    SensorCall(12529);if (preeminent)
			save_aelem(av, elem, svp);
		    else
			SAVEADELETE(av, elem);
		}
	    }
	    SensorCall(12532);*MARK = svp ? *svp : &PL_sv_undef;
	}
    }
    SensorCall(12536);if (GIMME != G_ARRAY) {
	MARK = ORIGMARK;
	SensorCall(12535);*++MARK = SP > ORIGMARK ? *SP : &PL_sv_undef;
	SP = MARK;
    }
    RETURN;
}

/* Smart dereferencing for keys, values and each */
PP(pp_rkeys)
{
SensorCall(12537);    dVAR;
    dSP;
    dPOPss;

    SvGETMAGIC(sv);

    SensorCall(12538);if (
         !SvROK(sv)
      || (sv = SvRV(sv),
            (SvTYPE(sv) != SVt_PVHV && SvTYPE(sv) != SVt_PVAV)
          || SvOBJECT(sv)
         )
    ) {
	DIE(aTHX_
	   "Type of argument to %s must be unblessed hashref or arrayref",
	    PL_op_desc[PL_op->op_type] );
    }

    SensorCall(12539);if (PL_op->op_flags & OPf_SPECIAL && SvTYPE(sv) == SVt_PVAV)
	DIE(aTHX_
	   "Can't modify %s in %s",
	    PL_op_desc[PL_op->op_type], PL_op_desc[PL_op->op_next->op_type]
	);

    /* Delegate to correct function for op type */
    PUSHs(sv);
    SensorCall(12542);if (PL_op->op_type == OP_RKEYS || PL_op->op_type == OP_RVALUES) {
	{OP * ReplaceReturn1000 = (SvTYPE(sv) == SVt_PVHV) ? Perl_do_kv(aTHX) : Perl_pp_akeys(aTHX); SensorCall(12540); return ReplaceReturn1000;}
    }
    else {
	{OP * ReplaceReturn999 = (SvTYPE(sv) == SVt_PVHV) ? Perl_pp_each(aTHX) : Perl_pp_aeach(aTHX); SensorCall(12541); return ReplaceReturn999;}
    }
SensorCall(12543);}

PP(pp_aeach)
{
SensorCall(12544);    dVAR;
    dSP;
    AV *array = MUTABLE_AV(POPs);
    const I32 gimme = GIMME_V;
    IV *iterp = Perl_av_iter_p(aTHX_ array);
    const IV current = (*iterp)++;

    SensorCall(12547);if (current > av_len(array)) {
	SensorCall(12545);*iterp = 0;
	SensorCall(12546);if (gimme == G_SCALAR)
	    RETPUSHUNDEF;
	else
	    RETURN;
    }

    EXTEND(SP, 2);
    mPUSHi(current);
    SensorCall(12549);if (gimme == G_ARRAY) {
	SensorCall(12548);SV **const element = av_fetch(array, current, 0);
        PUSHs(element ? *element : &PL_sv_undef);
    }
    RETURN;
}

PP(pp_akeys)
{
SensorCall(12550);    dVAR;
    dSP;
    AV *array = MUTABLE_AV(POPs);
    const I32 gimme = GIMME_V;

    *Perl_av_iter_p(aTHX_ array) = 0;

    SensorCall(12557);if (gimme == G_SCALAR) {
	dTARGET;
	PUSHi(av_len(array) + 1);
    }
    else {/*73*/SensorCall(12551);if (gimme == G_ARRAY) {
        SensorCall(12552);IV n = Perl_av_len(aTHX_ array);
        IV i;

        EXTEND(SP, n + 1);

	SensorCall(12556);if (PL_op->op_type == OP_AKEYS || PL_op->op_type == OP_RKEYS) {
	    SensorCall(12553);for (i = 0;  i <= n;  i++) {
		mPUSHi(i);
	    }
	}
	else {
	    SensorCall(12554);for (i = 0;  i <= n;  i++) {
		SensorCall(12555);SV *const *const elem = Perl_av_fetch(aTHX_ array, i, 0);
		PUSHs(elem ? *elem : &PL_sv_undef);
	    }
	}
    ;/*74*/}}
    RETURN;
}

/* Associative arrays. */

PP(pp_each)
{
SensorCall(12558);    dVAR;
    dSP;
    HV * hash = MUTABLE_HV(POPs);
    HE *entry;
    const I32 gimme = GIMME_V;

    PUTBACK;
    /* might clobber stack_sp */
    entry = hv_iternext(hash);
    SPAGAIN;

    EXTEND(SP, 2);
    SensorCall(12562);if (entry) {
	SensorCall(12559);SV* const sv = hv_iterkeysv(entry);
	PUSHs(sv);	/* won't clobber stack_sp */
	SensorCall(12561);if (gimme == G_ARRAY) {
	    SensorCall(12560);SV *val;
	    PUTBACK;
	    /* might clobber stack_sp */
	    val = hv_iterval(hash, entry);
	    SPAGAIN;
	    PUSHs(val);
	}
    }
    else if (gimme == G_SCALAR)
	RETPUSHUNDEF;

    RETURN;
}

STATIC OP *
S_do_delete_local(pTHX)
{
SensorCall(12563);    dVAR;
    dSP;
    const I32 gimme = GIMME_V;
    const MAGIC *mg;
    HV *stash;

    SensorCall(12623);if (PL_op->op_private & OPpSLICE) {
	dMARK; dORIGMARK;
	SensorCall(12564);SV * const osv = POPs;
	const bool tied = SvRMAGICAL(osv)
			    && mg_find((const SV *)osv, PERL_MAGIC_tied);
	const bool can_preserve = SvCANEXISTDELETE(osv)
				    || mg_find((const SV *)osv, PERL_MAGIC_env);
	const U32 type = SvTYPE(osv);
	SensorCall(12594);if (type == SVt_PVHV) {			/* hash element */
	    SensorCall(12565);HV * const hv = MUTABLE_HV(osv);
	    SensorCall(12577);while (++MARK <= SP) {
		SensorCall(12566);SV * const keysv = *MARK;
		SV *sv = NULL;
		bool preeminent = TRUE;
		SensorCall(12567);if (can_preserve)
		    preeminent = hv_exists_ent(hv, keysv, 0);
		SensorCall(12571);if (tied) {
		    SensorCall(12568);HE *he = hv_fetch_ent(hv, keysv, 1, 0);
		    SensorCall(12569);if (he)
			sv = HeVAL(he);
		    else
			preeminent = FALSE;
		}
		else {
		    SensorCall(12570);sv = hv_delete_ent(hv, keysv, 0, 0);
		    SvREFCNT_inc_simple_void(sv); /* De-mortalize */
		}
		SensorCall(12576);if (preeminent) {
		    save_helem_flags(hv, keysv, &sv, SAVEf_KEEPOLDELEM);
		    SensorCall(12574);if (tied) {
			SensorCall(12572);*MARK = sv_mortalcopy(sv);
			mg_clear(sv);
		    } else
			{/*47*/SensorCall(12573);*MARK = sv;/*48*/}
		}
		else {
		    SAVEHDELETE(hv, keysv);
		    SensorCall(12575);*MARK = &PL_sv_undef;
		}
	    }
	}
	else {/*49*/SensorCall(12578);if (type == SVt_PVAV) {                  /* array element */
	    SensorCall(12579);if (PL_op->op_flags & OPf_SPECIAL) {
		SensorCall(12580);AV * const av = MUTABLE_AV(osv);
		SensorCall(12593);while (++MARK <= SP) {
		    SensorCall(12581);I32 idx = SvIV(*MARK);
		    SV *sv = NULL;
		    bool preeminent = TRUE;
		    SensorCall(12582);if (can_preserve)
			preeminent = av_exists(av, idx);
		    SensorCall(12587);if (tied) {
			SensorCall(12583);SV **svp = av_fetch(av, idx, 1);
			SensorCall(12585);if (svp)
			    {/*51*/SensorCall(12584);sv = *svp;/*52*/}
			else
			    preeminent = FALSE;
		    }
		    else {
			SensorCall(12586);sv = av_delete(av, idx, 0);
		        SvREFCNT_inc_simple_void(sv); /* De-mortalize */
		    }
		    SensorCall(12592);if (preeminent) {
		        save_aelem_flags(av, idx, &sv, SAVEf_KEEPOLDELEM);
			SensorCall(12590);if (tied) {
			    SensorCall(12588);*MARK = sv_mortalcopy(sv);
			    mg_clear(sv);
			} else
			    {/*53*/SensorCall(12589);*MARK = sv;/*54*/}
		    }
		    else {
		        SAVEADELETE(av, idx);
		        SensorCall(12591);*MARK = &PL_sv_undef;
		    }
		}
	    }
	}
	else
	    DIE(aTHX_ "Not a HASH reference");/*50*/}
	SensorCall(12597);if (gimme == G_VOID)
	    SP = ORIGMARK;
	else {/*55*/SensorCall(12595);if (gimme == G_SCALAR) {
	    MARK = ORIGMARK;
	    SensorCall(12596);if (SP > MARK)
		*++MARK = *SP;
	    else
		*++MARK = &PL_sv_undef;
	    SP = MARK;
	;/*56*/}}
    }
    else {
	SensorCall(12598);SV * const keysv = POPs;
	SV * const osv   = POPs;
	const bool tied = SvRMAGICAL(osv)
			    && mg_find((const SV *)osv, PERL_MAGIC_tied);
	const bool can_preserve = SvCANEXISTDELETE(osv)
				    || mg_find((const SV *)osv, PERL_MAGIC_env);
	const U32 type = SvTYPE(osv);
	SV *sv = NULL;
	SensorCall(12620);if (type == SVt_PVHV) {
	    SensorCall(12599);HV * const hv = MUTABLE_HV(osv);
	    bool preeminent = TRUE;
	    SensorCall(12600);if (can_preserve)
		preeminent = hv_exists_ent(hv, keysv, 0);
	    SensorCall(12604);if (tied) {
		SensorCall(12601);HE *he = hv_fetch_ent(hv, keysv, 1, 0);
		SensorCall(12602);if (he)
		    sv = HeVAL(he);
		else
		    preeminent = FALSE;
	    }
	    else {
		SensorCall(12603);sv = hv_delete_ent(hv, keysv, 0, 0);
		SvREFCNT_inc_simple_void(sv); /* De-mortalize */
	    }
	    SensorCall(12607);if (preeminent) {
		save_helem_flags(hv, keysv, &sv, SAVEf_KEEPOLDELEM);
		SensorCall(12606);if (tied) {
		    SensorCall(12605);SV *nsv = sv_mortalcopy(sv);
		    mg_clear(sv);
		    sv = nsv;
		}
	    }
	    else
		SAVEHDELETE(hv, keysv);
	}
	else {/*57*/SensorCall(12608);if (type == SVt_PVAV) {
	    SensorCall(12609);if (PL_op->op_flags & OPf_SPECIAL) {
		SensorCall(12610);AV * const av = MUTABLE_AV(osv);
		I32 idx = SvIV(keysv);
		bool preeminent = TRUE;
		SensorCall(12611);if (can_preserve)
		    preeminent = av_exists(av, idx);
		SensorCall(12616);if (tied) {
		    SensorCall(12612);SV **svp = av_fetch(av, idx, 1);
		    SensorCall(12614);if (svp)
			{/*59*/SensorCall(12613);sv = *svp;/*60*/}
		    else
			preeminent = FALSE;
		}
		else {
		    SensorCall(12615);sv = av_delete(av, idx, 0);
		    SvREFCNT_inc_simple_void(sv); /* De-mortalize */
		}
		SensorCall(12619);if (preeminent) {
		    save_aelem_flags(av, idx, &sv, SAVEf_KEEPOLDELEM);
		    SensorCall(12618);if (tied) {
			SensorCall(12617);SV *nsv = sv_mortalcopy(sv);
			mg_clear(sv);
			sv = nsv;
		    }
		}
		else
		    SAVEADELETE(av, idx);
	    }
	    else
		DIE(aTHX_ "panic: avhv_delete no longer supported");
	}
	else
	    DIE(aTHX_ "Not a HASH reference");/*58*/}
	SensorCall(12621);if (!sv)
	    sv = &PL_sv_undef;
	SensorCall(12622);if (gimme != G_VOID)
	    PUSHs(sv);
    }

    RETURN;
}

PP(pp_delete)
{
SensorCall(12624);    dVAR;
    dSP;
    I32 gimme;
    I32 discard;

    SensorCall(12625);if (PL_op->op_private & OPpLVAL_INTRO)
	return do_delete_local();

    SensorCall(12626);gimme = GIMME_V;
    discard = (gimme == G_VOID) ? G_DISCARD : 0;

    SensorCall(12644);if (PL_op->op_private & OPpSLICE) {
	dMARK; dORIGMARK;
	SensorCall(12627);HV * const hv = MUTABLE_HV(POPs);
	const U32 hvtype = SvTYPE(hv);
	SensorCall(12634);if (hvtype == SVt_PVHV) {			/* hash element */
	    SensorCall(12628);while (++MARK <= SP) {
		SensorCall(12629);SV * const sv = hv_delete_ent(hv, *MARK, discard, 0);
		*MARK = sv ? sv : &PL_sv_undef;
	    }
	}
	else {/*109*/SensorCall(12630);if (hvtype == SVt_PVAV) {                  /* array element */
            SensorCall(12631);if (PL_op->op_flags & OPf_SPECIAL) {
                SensorCall(12632);while (++MARK <= SP) {
                    SensorCall(12633);SV * const sv = av_delete(MUTABLE_AV(hv), SvIV(*MARK), discard);
                    *MARK = sv ? sv : &PL_sv_undef;
                }
            }
	}
	else
	    DIE(aTHX_ "Not a HASH reference");/*110*/}
	SensorCall(12637);if (discard)
	    SP = ORIGMARK;
	else {/*111*/SensorCall(12635);if (gimme == G_SCALAR) {
	    MARK = ORIGMARK;
	    SensorCall(12636);if (SP > MARK)
		*++MARK = *SP;
	    else
		*++MARK = &PL_sv_undef;
	    SP = MARK;
	;/*112*/}}
    }
    else {
	SensorCall(12638);SV *keysv = POPs;
	HV * const hv = MUTABLE_HV(POPs);
	SV *sv = NULL;
	SensorCall(12641);if (SvTYPE(hv) == SVt_PVHV)
	    sv = hv_delete_ent(hv, keysv, discard, 0);
	else {/*113*/SensorCall(12639);if (SvTYPE(hv) == SVt_PVAV) {
	    SensorCall(12640);if (PL_op->op_flags & OPf_SPECIAL)
		sv = av_delete(MUTABLE_AV(hv), SvIV(keysv), discard);
	    else
		DIE(aTHX_ "panic: avhv_delete no longer supported");
	}
	else
	    DIE(aTHX_ "Not a HASH reference");/*114*/}
	SensorCall(12642);if (!sv)
	    sv = &PL_sv_undef;
	SensorCall(12643);if (!discard)
	    PUSHs(sv);
    }
    RETURN;
}

PP(pp_exists)
{
SensorCall(12645);    dVAR;
    dSP;
    SV *tmpsv;
    HV *hv;

    SensorCall(12649);if (PL_op->op_private & OPpEXISTS_SUB) {
	SensorCall(12646);GV *gv;
	SV * const sv = POPs;
	CV * const cv = sv_2cv(sv, &hv, &gv, 0);
	SensorCall(12647);if (cv)
	    RETPUSHYES;
	SensorCall(12648);if (gv && isGV(gv) && GvCV(gv) && !GvCVGEN(gv))
	    RETPUSHYES;
	RETPUSHNO;
    }
    SensorCall(12650);tmpsv = POPs;
    hv = MUTABLE_HV(POPs);
    SensorCall(12655);if (SvTYPE(hv) == SVt_PVHV) {
	SensorCall(12651);if (hv_exists_ent(hv, tmpsv, 0))
	    RETPUSHYES;
    }
    else {/*115*/SensorCall(12652);if (SvTYPE(hv) == SVt_PVAV) {
	SensorCall(12653);if (PL_op->op_flags & OPf_SPECIAL) {		/* array element */
	    SensorCall(12654);if (av_exists(MUTABLE_AV(hv), SvIV(tmpsv)))
		RETPUSHYES;
	}
    }
    else {
	DIE(aTHX_ "Not a HASH reference");
    ;/*116*/}}
    RETPUSHNO;
}

PP(pp_hslice)
{
SensorCall(12656);    dVAR; dSP; dMARK; dORIGMARK;
    register HV * const hv = MUTABLE_HV(POPs);
    register const I32 lval = (PL_op->op_flags & OPf_MOD || LVRET);
    const bool localizing = PL_op->op_private & OPpLVAL_INTRO;
    bool can_preserve = FALSE;

    SensorCall(12659);if (localizing) {
        SensorCall(12657);MAGIC *mg;
        HV *stash;

	SensorCall(12658);if (SvCANEXISTDELETE(hv) || mg_find((const SV *)hv, PERL_MAGIC_env))
	    can_preserve = TRUE;
    }

    SensorCall(12669);while (++MARK <= SP) {
        SensorCall(12660);SV * const keysv = *MARK;
        SV **svp;
        HE *he;
        bool preeminent = TRUE;

        SensorCall(12662);if (localizing && can_preserve) {
	    /* If we can determine whether the element exist,
             * try to preserve the existenceness of a tied hash
             * element by using EXISTS and DELETE if possible.
             * Fallback to FETCH and STORE otherwise. */
            SensorCall(12661);preeminent = hv_exists_ent(hv, keysv, 0);
        }

        SensorCall(12663);he = hv_fetch_ent(hv, keysv, lval, 0);
        svp = he ? &HeVAL(he) : NULL;

        SensorCall(12667);if (lval) {
            SensorCall(12664);if (!svp || !*svp || *svp == &PL_sv_undef) {
                DIE(aTHX_ PL_no_helem_sv, SVfARG(keysv));
            }
            SensorCall(12666);if (localizing) {
		SensorCall(12665);if (HvNAME_get(hv) && isGV(*svp))
		    save_gp(MUTABLE_GV(*svp), !(PL_op->op_flags & OPf_SPECIAL));
		else if (preeminent)
		    save_helem_flags(hv, keysv, svp,
			 (PL_op->op_flags & OPf_SPECIAL) ? 0 : SAVEf_SETMAGIC);
		else
		    SAVEHDELETE(hv, keysv);
            }
        }
        SensorCall(12668);*MARK = svp && *svp ? *svp : &PL_sv_undef;
    }
    SensorCall(12671);if (GIMME != G_ARRAY) {
	MARK = ORIGMARK;
	SensorCall(12670);*++MARK = SP > ORIGMARK ? *SP : &PL_sv_undef;
	SP = MARK;
    }
    RETURN;
}

/* List operators. */

PP(pp_list)
{
SensorCall(12672);    dVAR; dSP; dMARK;
    SensorCall(12674);if (GIMME != G_ARRAY) {
	SensorCall(12673);if (++MARK <= SP)
	    *MARK = *SP;		/* unwanted list, return last item */
	else
	    *MARK = &PL_sv_undef;
	SP = MARK;
    }
    RETURN;
}

PP(pp_lslice)
{
SensorCall(12675);    dVAR;
    dSP;
    SV ** const lastrelem = PL_stack_sp;
    SV ** const lastlelem = PL_stack_base + POPMARK;
    SV ** const firstlelem = PL_stack_base + POPMARK + 1;
    register SV ** const firstrelem = lastlelem + 1;
    I32 is_something_there = FALSE;

    register const I32 max = lastrelem - lastlelem;
    register SV **lelem;

    SensorCall(12681);if (GIMME != G_ARRAY) {
	SensorCall(12676);I32 ix = SvIV(*lastlelem);
	SensorCall(12678);if (ix < 0)
	    {/*155*/SensorCall(12677);ix += max;/*156*/}
	SensorCall(12680);if (ix < 0 || ix >= max)
	    *firstlelem = &PL_sv_undef;
	else
	    {/*157*/SensorCall(12679);*firstlelem = firstrelem[ix];/*158*/}
	SP = firstlelem;
	RETURN;
    }

    SensorCall(12682);if (max == 0) {
	SP = firstlelem - 1;
	RETURN;
    }

    SensorCall(12689);for (lelem = firstlelem; lelem <= lastlelem; lelem++) {
	SensorCall(12683);I32 ix = SvIV(*lelem);
	SensorCall(12685);if (ix < 0)
	    {/*159*/SensorCall(12684);ix += max;/*160*/}
	SensorCall(12688);if (ix < 0 || ix >= max)
	    *lelem = &PL_sv_undef;
	else {
	    SensorCall(12686);is_something_there = TRUE;
	    SensorCall(12687);if (!(*lelem = firstrelem[ix]))
		*lelem = &PL_sv_undef;
	}
    }
    SensorCall(12690);if (is_something_there)
	SP = lastlelem;
    else
	SP = firstlelem - 1;
    RETURN;
}

PP(pp_anonlist)
{
SensorCall(12691);    dVAR; dSP; dMARK; dORIGMARK;
    const I32 items = SP - MARK;
    SV * const av = MUTABLE_SV(av_make(items, MARK+1));
    SP = ORIGMARK;		/* av_make() might realloc stack_sp */
    mXPUSHs((PL_op->op_flags & OPf_SPECIAL)
	    ? newRV_noinc(av) : av);
    RETURN;
}

PP(pp_anonhash)
{
SensorCall(12692);    dVAR; dSP; dMARK; dORIGMARK;
    HV* const hv = newHV();

    SensorCall(12697);while (MARK < SP) {
	SensorCall(12693);SV * const key = *++MARK;
	SV * const val = newSV(0);
	SensorCall(12695);if (MARK < SP)
	    sv_setsv(val, *++MARK);
	else
	    {/*75*/SensorCall(12694);Perl_ck_warner(aTHX_ packWARN(WARN_MISC), "Odd number of elements in anonymous hash");/*76*/}
	SensorCall(12696);(void)hv_store_ent(hv,key,val,0);
    }
    SP = ORIGMARK;
    mXPUSHs((PL_op->op_flags & OPf_SPECIAL)
	    ? newRV_noinc(MUTABLE_SV(hv)) : MUTABLE_SV(hv));
    RETURN;
}

static AV *
S_deref_plain_array(pTHX_ AV *ary)
{
    SensorCall(12698);if (SvTYPE(ary) == SVt_PVAV) {/*407*/{AV * ReplaceReturn998 = ary; SensorCall(12699); return ReplaceReturn998;}/*408*/}
    SvGETMAGIC((SV *)ary);
    SensorCall(12703);if (!SvROK(ary) || SvTYPE(SvRV(ary)) != SVt_PVAV)
	{/*409*/SensorCall(12700);Perl_die(aTHX_ "Not an ARRAY reference");/*410*/}
    else {/*411*/SensorCall(12701);if (SvOBJECT(SvRV(ary)))
	{/*413*/SensorCall(12702);Perl_die(aTHX_ "Not an unblessed ARRAY reference");/*414*/}/*412*/}
    {AV * ReplaceReturn997 = (AV *)SvRV(ary); SensorCall(12704); return ReplaceReturn997;}
}

#if defined(__GNUC__) && !defined(PERL_GCC_BRACE_GROUPS_FORBIDDEN)
# define DEREF_PLAIN_ARRAY(ary)       \
   ({                                  \
     AV *aRrRay = ary;                  \
     SvTYPE(aRrRay) == SVt_PVAV          \
      ? aRrRay                            \
      : S_deref_plain_array(aTHX_ aRrRay); \
   })
#else
# define DEREF_PLAIN_ARRAY(ary)            \
   (                                        \
     PL_Sv = (SV *)(ary),                    \
     SvTYPE(PL_Sv) == SVt_PVAV                \
      ? (AV *)PL_Sv                            \
      : S_deref_plain_array(aTHX_ (AV *)PL_Sv)  \
   )
#endif

PP(pp_splice)
{
SensorCall(12705);    dVAR; dSP; dMARK; dORIGMARK;
    int num_args = (SP - MARK);
    register AV *ary = DEREF_PLAIN_ARRAY(MUTABLE_AV(*++MARK));
    register SV **src;
    register SV **dst;
    register I32 i;
    register I32 offset;
    register I32 length;
    I32 newlen;
    I32 after;
    I32 diff;
    const MAGIC * const mg = SvTIED_mg((const SV *)ary, PERL_MAGIC_tied);

    SensorCall(12707);if (mg) {
	{OP * ReplaceReturn996 = Perl_tied_method(aTHX_ "SPLICE", mark - 1, MUTABLE_SV(ary), mg,
				    GIMME_V | TIED_METHOD_ARGUMENTS_ON_STACK,
				    sp - mark); SensorCall(12706); return ReplaceReturn996;}
    }

    SP++;

    SensorCall(12720);if (++MARK < SP) {
	SensorCall(12708);offset = i = SvIV(*MARK);
	SensorCall(12710);if (offset < 0)
	    {/*259*/SensorCall(12709);offset += AvFILLp(ary) + 1;/*260*/}
	SensorCall(12711);if (offset < 0)
	    DIE(aTHX_ PL_no_aelem, i);
	SensorCall(12718);if (++MARK < SP) {
	    SensorCall(12712);length = SvIVx(*MARK++);
	    SensorCall(12716);if (length < 0) {
		SensorCall(12713);length += AvFILLp(ary) - offset + 1;
		SensorCall(12715);if (length < 0)
		    {/*261*/SensorCall(12714);length = 0;/*262*/}
	    }
	}
	else
	    {/*263*/SensorCall(12717);length = AvMAX(ary) + 1;/*264*/}		/* close enough to infinity */
    }
    else {
	SensorCall(12719);offset = 0;
	length = AvMAX(ary) + 1;
    }
    SensorCall(12724);if (offset > AvFILLp(ary) + 1) {
	SensorCall(12721);if (num_args > 2)
	    {/*265*/SensorCall(12722);Perl_ck_warner(aTHX_ packWARN(WARN_MISC), "splice() offset past end of array" );/*266*/}
	SensorCall(12723);offset = AvFILLp(ary) + 1;
    }
    SensorCall(12725);after = AvFILLp(ary) + 1 - (offset + length);
    SensorCall(12728);if (after < 0) {				/* not that much array */
	SensorCall(12726);length += after;			/* offset+length now in array */
	after = 0;
	SensorCall(12727);if (!AvALLOC(ary))
	    av_extend(ary, 0);
    }

    /* At this point, MARK .. SP-1 is our new LIST */

    SensorCall(12729);newlen = SP - MARK;
    diff = newlen - length;
    SensorCall(12730);if (newlen && !AvREAL(ary) && AvREIFY(ary))
	av_reify(ary);

    /* make new elements SVs now: avoid problems if they're from the array */
    SensorCall(12732);for (dst = MARK, i = newlen; i; i--) {
        SensorCall(12731);SV * const h = *dst;
	*dst++ = newSVsv(h);
    }

    SensorCall(12771);if (diff < 0) {				/* shrinking the area */
	SensorCall(12733);SV **tmparyval = NULL;
	SensorCall(12734);if (newlen) {
	    Newx(tmparyval, newlen, SV*);	/* so remember insertion */
	    Copy(MARK, tmparyval, newlen, SV*);
	}

	MARK = ORIGMARK + 1;
	SensorCall(12741);if (GIMME == G_ARRAY) {			/* copy return vals to stack */
	    MEXTEND(MARK, length);
	    Copy(AvARRAY(ary)+offset, MARK, length, SV*);
	    SensorCall(12737);if (AvREAL(ary)) {
		EXTEND_MORTAL(length);
		SensorCall(12736);for (i = length, dst = MARK; i; i--) {
		    sv_2mortal(*dst);	/* free them eventually */
		    SensorCall(12735);dst++;
		}
	    }
	    MARK += length - 1;
	}
	else {
	    SensorCall(12738);*MARK = AvARRAY(ary)[offset+length-1];
	    SensorCall(12740);if (AvREAL(ary)) {
		sv_2mortal(*MARK);
		SensorCall(12739);for (i = length - 1, dst = &AvARRAY(ary)[offset]; i > 0; i--)
		    SvREFCNT_dec(*dst++);	/* free them now */
	    }
	}
	AvFILLp(ary) += diff;

	/* pull up or down? */

	SensorCall(12750);if (offset < after) {			/* easier to pull up */
	    SensorCall(12742);if (offset) {			/* esp. if nothing to pull */
		SensorCall(12743);src = &AvARRAY(ary)[offset-1];
		dst = src - diff;		/* diff is negative */
		SensorCall(12745);for (i = offset; i > 0; i--)	/* can't trust Copy */
		    {/*267*/SensorCall(12744);*dst-- = *src--;/*268*/}
	    }
	    SensorCall(12746);dst = AvARRAY(ary);
	    AvARRAY(ary) = AvARRAY(ary) - diff; /* diff is negative */
	    AvMAX(ary) += diff;
	}
	else {
	    SensorCall(12747);if (after) {			/* anything to pull down? */
		SensorCall(12748);src = AvARRAY(ary) + offset + length;
		dst = src + diff;		/* diff is negative */
		Move(src, dst, after, SV*);
	    }
	    SensorCall(12749);dst = &AvARRAY(ary)[AvFILLp(ary)+1];
						/* avoid later double free */
	}
	SensorCall(12751);i = -diff;
	SensorCall(12752);while (i)
	    dst[--i] = &PL_sv_undef;
	
	SensorCall(12753);if (newlen) {
 	    Copy( tmparyval, AvARRAY(ary) + offset, newlen, SV* );
	    Safefree(tmparyval);
	}
    }
    else {					/* no, expanding (or same) */
	SensorCall(12754);SV** tmparyval = NULL;
	SensorCall(12755);if (length) {
	    Newx(tmparyval, length, SV*);	/* so remember deletion */
	    Copy(AvARRAY(ary)+offset, tmparyval, length, SV*);
	}

	SensorCall(12764);if (diff > 0) {				/* expanding */
	    /* push up or down? */
	    SensorCall(12756);if (offset < after && diff <= AvARRAY(ary) - AvALLOC(ary)) {
		SensorCall(12757);if (offset) {
		    SensorCall(12758);src = AvARRAY(ary);
		    dst = src - diff;
		    Move(src, dst, offset, SV*);
		}
		AvARRAY(ary) = AvARRAY(ary) - diff;/* diff is positive */
		AvMAX(ary) += diff;
		AvFILLp(ary) += diff;
	    }
	    else {
		SensorCall(12759);if (AvFILLp(ary) + diff >= AvMAX(ary))	/* oh, well */
		    av_extend(ary, AvFILLp(ary) + diff);
		AvFILLp(ary) += diff;

		SensorCall(12763);if (after) {
		    SensorCall(12760);dst = AvARRAY(ary) + AvFILLp(ary);
		    src = dst - diff;
		    SensorCall(12762);for (i = after; i; i--) {
			SensorCall(12761);*dst-- = *src--;
		    }
		}
	    }
	}

	SensorCall(12765);if (newlen) {
	    Copy( MARK, AvARRAY(ary) + offset, newlen, SV* );
	}

	MARK = ORIGMARK + 1;
	SensorCall(12770);if (GIMME == G_ARRAY) {			/* copy return vals to stack */
	    SensorCall(12766);if (length) {
		Copy(tmparyval, MARK, length, SV*);
		SensorCall(12769);if (AvREAL(ary)) {
		    EXTEND_MORTAL(length);
		    SensorCall(12768);for (i = length, dst = MARK; i; i--) {
			sv_2mortal(*dst);	/* free them eventually */
			SensorCall(12767);dst++;
		    }
		}
	    }
	    MARK += length - 1;
	}
	else if (length--) {
	    *MARK = tmparyval[length];
	    if (AvREAL(ary)) {
		sv_2mortal(*MARK);
		while (length-- > 0)
		    SvREFCNT_dec(tmparyval[length]);
	    }
	}
	else
	    *MARK = &PL_sv_undef;
	Safefree(tmparyval);
    }

    SensorCall(12772);if (SvMAGICAL(ary))
	mg_set(MUTABLE_SV(ary));

    SP = MARK;
    RETURN;
}

PP(pp_push)
{
SensorCall(12773);    dVAR; dSP; dMARK; dORIGMARK; dTARGET;
    register AV * const ary = DEREF_PLAIN_ARRAY(MUTABLE_AV(*++MARK));
    const MAGIC * const mg = SvTIED_mg((const SV *)ary, PERL_MAGIC_tied);

    SensorCall(12779);if (mg) {
	SensorCall(12774);*MARK-- = SvTIED_obj(MUTABLE_SV(ary), mg);
	PUSHMARK(MARK);
	PUTBACK;
	ENTER_with_name("call_PUSH");
	call_method("PUSH",G_SCALAR|G_DISCARD);
	LEAVE_with_name("call_PUSH");
	SPAGAIN;
    }
    else {
	PL_delaymagic = DM_DELAY;
	SensorCall(12777);for (++MARK; MARK <= SP; MARK++) {
	    SensorCall(12775);SV * const sv = newSV(0);
	    SensorCall(12776);if (*MARK)
		sv_setsv(sv, *MARK);
	    av_store(ary, AvFILLp(ary)+1, sv);
	}
	SensorCall(12778);if (PL_delaymagic & DM_ARRAY_ISA)
	    mg_set(MUTABLE_SV(ary));

	PL_delaymagic = 0;
    }
    SP = ORIGMARK;
    SensorCall(12780);if (OP_GIMME(PL_op, 0) != G_VOID) {
	PUSHi( AvFILL(ary) + 1 );
    }
    RETURN;
}

PP(pp_shift)
{
SensorCall(12781);    dVAR;
    dSP;
    AV * const av = PL_op->op_flags & OPf_SPECIAL
	? MUTABLE_AV(GvAV(PL_defgv)) : DEREF_PLAIN_ARRAY(MUTABLE_AV(POPs));
    SV * const sv = PL_op->op_type == OP_SHIFT ? av_shift(av) : av_pop(av);
    EXTEND(SP, 1);
    assert (sv);
    SensorCall(12782);if (AvREAL(av))
	(void)sv_2mortal(sv);
    PUSHs(sv);
    RETURN;
}

PP(pp_unshift)
{
SensorCall(12783);    dVAR; dSP; dMARK; dORIGMARK; dTARGET;
    register AV *ary = DEREF_PLAIN_ARRAY(MUTABLE_AV(*++MARK));
    const MAGIC * const mg = SvTIED_mg((const SV *)ary, PERL_MAGIC_tied);

    SensorCall(12788);if (mg) {
	SensorCall(12784);*MARK-- = SvTIED_obj(MUTABLE_SV(ary), mg);
	PUSHMARK(MARK);
	PUTBACK;
	ENTER_with_name("call_UNSHIFT");
	call_method("UNSHIFT",G_SCALAR|G_DISCARD);
	LEAVE_with_name("call_UNSHIFT");
	SPAGAIN;
    }
    else {
	SensorCall(12785);register I32 i = 0;
	av_unshift(ary, SP - MARK);
	SensorCall(12787);while (MARK < SP) {
	    SensorCall(12786);SV * const sv = newSVsv(*++MARK);
	    (void)av_store(ary, i++, sv);
	}
    }
    SP = ORIGMARK;
    SensorCall(12789);if (OP_GIMME(PL_op, 0) != G_VOID) {
	PUSHi( AvFILL(ary) + 1 );
    }
    RETURN;
}

PP(pp_reverse)
{
SensorCall(12790);    dVAR; dSP; dMARK;

    SensorCall(12835);if (GIMME == G_ARRAY) {
	SensorCall(12791);if (PL_op->op_private & OPpREVERSE_INPLACE) {
	    SensorCall(12792);AV *av;

	    /* See pp_sort() */
	    assert( MARK+1 == SP && *SP && SvTYPE(*SP) == SVt_PVAV);
	    (void)POPMARK; /* remove mark associated with ex-OP_AASSIGN */
	    av = MUTABLE_AV((*SP));
	    /* In-place reversing only happens in void context for the array
	     * assignment. We don't need to push anything on the stack. */
	    SP = MARK;

	    SensorCall(12810);if (SvMAGICAL(av)) {
		SensorCall(12793);I32 i, j;
		register SV *tmp = sv_newmortal();
		/* For SvCANEXISTDELETE */
		HV *stash;
		const MAGIC *mg;
		bool can_preserve = SvCANEXISTDELETE(av);

		SensorCall(12804);for (i = 0, j = av_len(av); i < j; ++i, --j) {
		    SensorCall(12794);register SV *begin, *end;

		    SensorCall(12802);if (can_preserve) {
			SensorCall(12795);if (!av_exists(av, i)) {
			    SensorCall(12796);if (av_exists(av, j)) {
				SensorCall(12797);register SV *sv = av_delete(av, j, 0);
				begin = *av_fetch(av, i, TRUE);
				sv_setsv_mg(begin, sv);
			    }
			    SensorCall(12798);continue;
			}
			else {/*249*/SensorCall(12799);if (!av_exists(av, j)) {
			    SensorCall(12800);register SV *sv = av_delete(av, i, 0);
			    end = *av_fetch(av, j, TRUE);
			    sv_setsv_mg(end, sv);
			    SensorCall(12801);continue;
			;/*250*/}}
		    }

		    SensorCall(12803);begin = *av_fetch(av, i, TRUE);
		    end   = *av_fetch(av, j, TRUE);
		    sv_setsv(tmp,      begin);
		    sv_setsv_mg(begin, end);
		    sv_setsv_mg(end,   tmp);
		}
	    }
	    else {
		SensorCall(12805);SV **begin = AvARRAY(av);

		SensorCall(12809);if (begin) {
		    SensorCall(12806);SV **end   = begin + AvFILLp(av);

		    SensorCall(12808);while (begin < end) {
			SensorCall(12807);register SV * const tmp = *begin;
			*begin++ = *end;
			*end--   = tmp;
		    }
		}
	    }
	}
	else {
	    SensorCall(12811);SV **oldsp = SP;
	    MARK++;
	    SensorCall(12813);while (MARK < SP) {
		SensorCall(12812);register SV * const tmp = *MARK;
		*MARK++ = *SP;
		*SP--   = tmp;
	    }
	    /* safe as long as stack cannot get extended in the above */
	    SP = oldsp;
	}
    }
    else {
	SensorCall(12814);register char *up;
	register char *down;
	register I32 tmp;
	dTARGET;
	STRLEN len;

	SvUTF8_off(TARG);				/* decontaminate */
	SensorCall(12816);if (SP - MARK > 1)
	    do_join(TARG, &PL_sv_no, MARK, SP);
	else {
	    sv_setsv(TARG, SP > MARK ? *SP : find_rundefsv());
	    SensorCall(12815);if (! SvOK(TARG) && ckWARN(WARN_UNINITIALIZED))
		report_uninit(TARG);
	}

	SensorCall(12817);up = SvPV_force(TARG, len);
	SensorCall(12834);if (len > 1) {
	    SensorCall(12818);if (DO_UTF8(TARG)) {	/* first reverse each character */
		SensorCall(12819);U8* s = (U8*)SvPVX(TARG);
		const U8* send = (U8*)(s + len);
		SensorCall(12828);while (s < send) {
		    SensorCall(12820);if (UTF8_IS_INVARIANT(*s)) {
			SensorCall(12821);s++;
			SensorCall(12822);continue;
		    }
		    else {
			SensorCall(12823);if (!utf8_to_uvchr_buf(s, send, 0))
			    {/*251*/SensorCall(12824);break;/*252*/}
			SensorCall(12825);up = (char*)s;
			s += UTF8SKIP(s);
			down = (char*)(s - 1);
			/* reverse this character */
			SensorCall(12827);while (down > up) {
			    SensorCall(12826);tmp = *up;
			    *up++ = *down;
			    *down-- = (char)tmp;
			}
		    }
		}
		SensorCall(12829);up = SvPVX(TARG);
	    }
	    SensorCall(12830);down = SvPVX(TARG) + len - 1;
	    SensorCall(12832);while (down > up) {
		SensorCall(12831);tmp = *up;
		*up++ = *down;
		*down-- = (char)tmp;
	    }
	    SensorCall(12833);(void)SvPOK_only_UTF8(TARG);
	}
	SP = MARK + 1;
	SETTARG;
    }
    RETURN;
}

PP(pp_split)
{
SensorCall(12836);    dVAR; dSP; dTARG;
    AV *ary;
    register IV limit = POPi;			/* note, negative is forever */
    SV * const sv = POPs;
    STRLEN len;
    register const char *s = SvPV_const(sv, len);
    const bool do_utf8 = DO_UTF8(sv);
    const char *strend = s + len;
    register PMOP *pm;
    register REGEXP *rx;
    register SV *dstr;
    register const char *m;
    I32 iters = 0;
    const STRLEN slen = do_utf8 ? utf8_length((U8*)s, (U8*)strend) : (STRLEN)(strend - s);
    I32 maxiters = slen + 10;
    I32 trailing_empty = 0;
    const char *orig;
    const I32 origlimit = limit;
    I32 realarray = 0;
    I32 base;
    const I32 gimme = GIMME_V;
    bool gimme_scalar;
    const I32 oldsave = PL_savestack_ix;
    U32 make_mortal = SVs_TEMP;
    bool multiline = 0;
    MAGIC *mg = NULL;

#ifdef DEBUGGING
    Copy(&LvTARGOFF(POPs), &pm, 1, PMOP*);
#else
    pm = (PMOP*)POPs;
#endif
    SensorCall(12837);if (!pm || !s)
	DIE(aTHX_ "panic: pp_split, pm=%p, s=%p", pm, s);
    SensorCall(12838);rx = PM_GETRE(pm);

    TAINT_IF(get_regex_charset(RX_EXTFLAGS(rx)) == REGEX_LOCALE_CHARSET &&
	     (RX_EXTFLAGS(rx) & (RXf_WHITE | RXf_SKIPWHITE)));

    RX_MATCH_UTF8_set(rx, do_utf8);

#ifdef USE_ITHREADS
    SensorCall(12840);if (pm->op_pmreplrootu.op_pmtargetoff) {
	SensorCall(12839);ary = GvAVn(MUTABLE_GV(PAD_SVl(pm->op_pmreplrootu.op_pmtargetoff)));
    }
#else
    if (pm->op_pmreplrootu.op_pmtargetgv) {
	ary = GvAVn(pm->op_pmreplrootu.op_pmtargetgv);
    }
#endif
    else
	ary = NULL;
    SensorCall(12847);if (ary && (gimme != G_ARRAY || (pm->op_pmflags & PMf_ONCE))) {
	SensorCall(12841);realarray = 1;
	PUTBACK;
	av_extend(ary,0);
	av_clear(ary);
	SPAGAIN;
	SensorCall(12846);if ((mg = SvTIED_mg((const SV *)ary, PERL_MAGIC_tied))) {
	    PUSHMARK(SP);
	    XPUSHs(SvTIED_obj(MUTABLE_SV(ary), mg));
	}
	else {
	    SensorCall(12842);if (!AvREAL(ary)) {
		SensorCall(12843);I32 i;
		AvREAL_on(ary);
		AvREIFY_off(ary);
		SensorCall(12844);for (i = AvFILLp(ary); i >= 0; i--)
		    AvARRAY(ary)[i] = &PL_sv_undef;	/* don't free mere refs */
	    }
	    /* temporarily switch stacks */
	    SAVESWITCHSTACK(PL_curstack, ary);
	    SensorCall(12845);make_mortal = 0;
	}
    }
    SensorCall(12848);base = SP - PL_stack_base;
    orig = s;
    SensorCall(12856);if (RX_EXTFLAGS(rx) & RXf_SKIPWHITE) {
	SensorCall(12849);if (do_utf8) {
	    SensorCall(12850);while (*s == ' ' || is_utf8_space((U8*)s))
		s += UTF8SKIP(s);
	}
	else {/*269*/SensorCall(12851);if (get_regex_charset(RX_EXTFLAGS(rx)) == REGEX_LOCALE_CHARSET) {
	    SensorCall(12852);while (isSPACE_LC(*s))
		{/*271*/SensorCall(12853);s++;/*272*/}
	}
	else {
	    SensorCall(12854);while (isSPACE(*s))
		{/*273*/SensorCall(12855);s++;/*274*/}
	;/*270*/}}
    }
    SensorCall(12858);if (RX_EXTFLAGS(rx) & RXf_PMf_MULTILINE) {
	SensorCall(12857);multiline = 1;
    }

    SensorCall(12859);gimme_scalar = gimme == G_SCALAR && !ary;

    SensorCall(12861);if (!limit)
	{/*275*/SensorCall(12860);limit = maxiters + 2;/*276*/}
    SensorCall(12979);if (RX_EXTFLAGS(rx) & RXf_WHITE) {
	SensorCall(12862);while (--limit) {
	    SensorCall(12863);m = s;
	    /* this one uses 'm' and is a negative test */
	    SensorCall(12874);if (do_utf8) {
		SensorCall(12864);while (m < strend && !( *m == ' ' || is_utf8_space((U8*)m) )) {
		    SensorCall(12865);const int t = UTF8SKIP(m);
		    /* is_utf8_space returns FALSE for malform utf8 */
		    SensorCall(12868);if (strend - m < t)
			{/*277*/SensorCall(12866);m = strend;/*278*/}
		    else
			{/*279*/SensorCall(12867);m += t;/*280*/}
		}
	    }
	    else {/*281*/SensorCall(12869);if (get_regex_charset(RX_EXTFLAGS(rx)) == REGEX_LOCALE_CHARSET) {
	        SensorCall(12870);while (m < strend && !isSPACE_LC(*m))
		    {/*283*/SensorCall(12871);++m;/*284*/}
            } else {
                SensorCall(12872);while (m < strend && !isSPACE(*m))
                    {/*285*/SensorCall(12873);++m;/*286*/}
            ;/*282*/}}  
	    SensorCall(12876);if (m >= strend)
		{/*287*/SensorCall(12875);break;/*288*/}

	    SensorCall(12882);if (gimme_scalar) {
		SensorCall(12877);iters++;
		SensorCall(12880);if (m-s == 0)
		    {/*289*/SensorCall(12878);trailing_empty++;/*290*/}
		else
		    {/*291*/SensorCall(12879);trailing_empty = 0;/*292*/}
	    } else {
		SensorCall(12881);dstr = newSVpvn_flags(s, m-s,
				      (do_utf8 ? SVf_UTF8 : 0) | make_mortal);
		XPUSHs(dstr);
	    }

	    /* skip the whitespace found last */
	    SensorCall(12884);if (do_utf8)
		s = m + UTF8SKIP(m);
	    else
		{/*293*/SensorCall(12883);s = m + 1;/*294*/}

	    /* this one uses 's' and is a positive test */
	    SensorCall(12891);if (do_utf8) {
		SensorCall(12885);while (s < strend && ( *s == ' ' || is_utf8_space((U8*)s) ))
	            s +=  UTF8SKIP(s);
	    }
	    else {/*295*/SensorCall(12886);if (get_regex_charset(RX_EXTFLAGS(rx)) == REGEX_LOCALE_CHARSET) {
	        SensorCall(12887);while (s < strend && isSPACE_LC(*s))
		    {/*297*/SensorCall(12888);++s;/*298*/}
            } else {
                SensorCall(12889);while (s < strend && isSPACE(*s))
                    {/*299*/SensorCall(12890);++s;/*300*/}
            ;/*296*/}} 	    
	}
    }
    else {/*301*/SensorCall(12892);if (RX_EXTFLAGS(rx) & RXf_START_ONLY) {
	SensorCall(12893);while (--limit) {
	    SensorCall(12894);for (m = s; m < strend && *m != '\n'; m++)
		;
	    SensorCall(12895);m++;
	    SensorCall(12897);if (m >= strend)
		{/*305*/SensorCall(12896);break;/*306*/}

	    SensorCall(12903);if (gimme_scalar) {
		SensorCall(12898);iters++;
		SensorCall(12901);if (m-s == 0)
		    {/*307*/SensorCall(12899);trailing_empty++;/*308*/}
		else
		    {/*309*/SensorCall(12900);trailing_empty = 0;/*310*/}
	    } else {
		SensorCall(12902);dstr = newSVpvn_flags(s, m-s,
				      (do_utf8 ? SVf_UTF8 : 0) | make_mortal);
		XPUSHs(dstr);
	    }
	    SensorCall(12904);s = m;
	}
    }
    else {/*311*/SensorCall(12905);if (RX_EXTFLAGS(rx) & RXf_NULL && !(s >= strend)) {
        /*
          Pre-extend the stack, either the number of bytes or
          characters in the string or a limited amount, triggered by:

          my ($x, $y) = split //, $str;
            or
          split //, $str, $i;
        */
	SensorCall(12906);if (!gimme_scalar) {
	    SensorCall(12907);const U32 items = limit - 1;
	    SensorCall(12908);if (items < slen)
		EXTEND(SP, items);
	    else
		EXTEND(SP, slen);
	}

        SensorCall(12927);if (do_utf8) {
            SensorCall(12909);while (--limit) {
                /* keep track of how many bytes we skip over */
                SensorCall(12910);m = s;
                s += UTF8SKIP(s);
		SensorCall(12916);if (gimme_scalar) {
		    SensorCall(12911);iters++;
		    SensorCall(12914);if (s-m == 0)
			{/*313*/SensorCall(12912);trailing_empty++;/*314*/}
		    else
			{/*315*/SensorCall(12913);trailing_empty = 0;/*316*/}
		} else {
		    SensorCall(12915);dstr = newSVpvn_flags(m, s-m, SVf_UTF8 | make_mortal);

		    PUSHs(dstr);
		}

                SensorCall(12918);if (s >= strend)
                    {/*317*/SensorCall(12917);break;/*318*/}
            }
        } else {
            SensorCall(12919);while (--limit) {
	        SensorCall(12920);if (gimme_scalar) {
		    SensorCall(12921);iters++;
		} else {
		    SensorCall(12922);dstr = newSVpvn(s, 1);


		    SensorCall(12923);if (make_mortal)
			sv_2mortal(dstr);

		    PUSHs(dstr);
		}

                SensorCall(12924);s++;

                SensorCall(12926);if (s >= strend)
                    {/*319*/SensorCall(12925);break;/*320*/}
            }
        }
    }
    else {/*321*/SensorCall(12928);if (do_utf8 == (RX_UTF8(rx) != 0) &&
	     (RX_EXTFLAGS(rx) & RXf_USE_INTUIT) && !RX_NPARENS(rx)
	     && (RX_EXTFLAGS(rx) & RXf_CHECK_ALL)
	     && !(RX_EXTFLAGS(rx) & RXf_ANCH)) {
	SensorCall(12929);const int tail = (RX_EXTFLAGS(rx) & RXf_INTUIT_TAIL);
	SV * const csv = CALLREG_INTUIT_STRING(rx);

	len = RX_MINLENRET(rx);
	SensorCall(12952);if (len == 1 && !RX_UTF8(rx) && !tail) {
	    SensorCall(12930);const char c = *SvPV_nolen_const(csv);
	    SensorCall(12942);while (--limit) {
		SensorCall(12931);for (m = s; m < strend && *m != c; m++)
		    ;
		SensorCall(12933);if (m >= strend)
		    {/*325*/SensorCall(12932);break;/*326*/}
		SensorCall(12939);if (gimme_scalar) {
		    SensorCall(12934);iters++;
		    SensorCall(12937);if (m-s == 0)
			{/*327*/SensorCall(12935);trailing_empty++;/*328*/}
		    else
			{/*329*/SensorCall(12936);trailing_empty = 0;/*330*/}
		} else {
		    SensorCall(12938);dstr = newSVpvn_flags(s, m-s,
					  (do_utf8 ? SVf_UTF8 : 0) | make_mortal);
		    XPUSHs(dstr);
		}
		/* The rx->minlen is in characters but we want to step
		 * s ahead by bytes. */
 		SensorCall(12941);if (do_utf8)
		    s = (char*)utf8_hop((U8*)m, len);
 		else
		    {/*331*/SensorCall(12940);s = m + len;/*332*/} /* Fake \n at the end */
	    }
	}
	else {
	    SensorCall(12943);while (s < strend && --limit &&
	      (m = fbm_instr((unsigned char*)s, (unsigned char*)strend,
			     csv, multiline ? FBMrf_MULTILINE : 0)) )
	    {
		SensorCall(12944);if (gimme_scalar) {
		    SensorCall(12945);iters++;
		    SensorCall(12948);if (m-s == 0)
			{/*333*/SensorCall(12946);trailing_empty++;/*334*/}
		    else
			{/*335*/SensorCall(12947);trailing_empty = 0;/*336*/}
		} else {
		    SensorCall(12949);dstr = newSVpvn_flags(s, m-s,
					  (do_utf8 ? SVf_UTF8 : 0) | make_mortal);
		    XPUSHs(dstr);
		}
		/* The rx->minlen is in characters but we want to step
		 * s ahead by bytes. */
 		SensorCall(12951);if (do_utf8)
		    s = (char*)utf8_hop((U8*)m, len);
 		else
		    {/*337*/SensorCall(12950);s = m + len;/*338*/} /* Fake \n at the end */
	    }
	}
    }
    else {
	SensorCall(12953);maxiters += slen * RX_NPARENS(rx);
	SensorCall(12978);while (s < strend && --limit)
	{
	    SensorCall(12954);I32 rex_return;
	    PUTBACK;
	    rex_return = CALLREGEXEC(rx, (char*)s, (char*)strend, (char*)orig, 1 ,
				     sv, NULL, SvSCREAM(sv) ? REXEC_SCREAM : 0);
	    SPAGAIN;
	    SensorCall(12956);if (rex_return == 0)
		{/*339*/SensorCall(12955);break;/*340*/}
	    TAINT_IF(RX_MATCH_TAINTED(rx));
	    SensorCall(12958);if (RX_MATCH_COPIED(rx) && RX_SUBBEG(rx) != orig) {
		SensorCall(12957);m = s;
		s = orig;
		orig = RX_SUBBEG(rx);
		s = orig + (m - s);
		strend = s + (strend - m);
	    }
	    SensorCall(12959);m = RX_OFFS(rx)[0].start + orig;

	    SensorCall(12965);if (gimme_scalar) {
		SensorCall(12960);iters++;
		SensorCall(12963);if (m-s == 0)
		    {/*341*/SensorCall(12961);trailing_empty++;/*342*/}
		else
		    {/*343*/SensorCall(12962);trailing_empty = 0;/*344*/}
	    } else {
		SensorCall(12964);dstr = newSVpvn_flags(s, m-s,
				      (do_utf8 ? SVf_UTF8 : 0) | make_mortal);
		XPUSHs(dstr);
	    }
	    SensorCall(12976);if (RX_NPARENS(rx)) {
		SensorCall(12966);I32 i;
		SensorCall(12975);for (i = 1; i <= (I32)RX_NPARENS(rx); i++) {
		    SensorCall(12967);s = RX_OFFS(rx)[i].start + orig;
		    m = RX_OFFS(rx)[i].end + orig;

		    /* japhy (07/27/01) -- the (m && s) test doesn't catch
		       parens that didn't match -- they should be set to
		       undef, not the empty string */
		    SensorCall(12974);if (gimme_scalar) {
			SensorCall(12968);iters++;
			SensorCall(12971);if (m-s == 0)
			    {/*345*/SensorCall(12969);trailing_empty++;/*346*/}
			else
			    {/*347*/SensorCall(12970);trailing_empty = 0;/*348*/}
		    } else {
			SensorCall(12972);if (m >= orig && s >= orig) {
			    SensorCall(12973);dstr = newSVpvn_flags(s, m-s,
						 (do_utf8 ? SVf_UTF8 : 0)
						  | make_mortal);
			}
			else
			    dstr = &PL_sv_undef;  /* undef, not "" */
			XPUSHs(dstr);
		    }

		}
	    }
	    SensorCall(12977);s = RX_OFFS(rx)[0].end + orig;
	}
    ;/*322*/}/*312*/}/*302*/}}

    SensorCall(12981);if (!gimme_scalar) {
	SensorCall(12980);iters = (SP - PL_stack_base) - base;
    }
    SensorCall(12982);if (iters > maxiters)
	DIE(aTHX_ "Split loop");

    /* keep field after final delim? */
    SensorCall(12992);if (s < strend || (iters && origlimit)) {
	SensorCall(12983);if (!gimme_scalar) {
	    SensorCall(12984);const STRLEN l = strend - s;
	    dstr = newSVpvn_flags(s, l, (do_utf8 ? SVf_UTF8 : 0) | make_mortal);
	    XPUSHs(dstr);
	}
	SensorCall(12985);iters++;
    }
    else {/*349*/SensorCall(12986);if (!origlimit) {
	SensorCall(12987);if (gimme_scalar) {
	    SensorCall(12988);iters -= trailing_empty;
	} else {
	    SensorCall(12989);while (iters > 0 && (!TOPs || !SvANY(TOPs) || SvCUR(TOPs) == 0)) {
		SensorCall(12990);if (TOPs && !make_mortal)
		    sv_2mortal(TOPs);
		SensorCall(12991);*SP-- = &PL_sv_undef;
		iters--;
	    }
	}
    ;/*350*/}}

    PUTBACK;
    LEAVE_SCOPE(oldsave); /* may undo an earlier SWITCHSTACK */
    SPAGAIN;
    SensorCall(13001);if (realarray) {
	SensorCall(12993);if (!mg) {
	    SensorCall(12994);if (SvSMAGICAL(ary)) {
		PUTBACK;
		mg_set(MUTABLE_SV(ary));
		SPAGAIN;
	    }
	    SensorCall(12995);if (gimme == G_ARRAY) {
		EXTEND(SP, iters);
		Copy(AvARRAY(ary), SP + 1, iters, SV*);
		SP += iters;
		RETURN;
	    }
	}
	else {
	    PUTBACK;
	    ENTER_with_name("call_PUSH");
	    call_method("PUSH",G_SCALAR|G_DISCARD);
	    LEAVE_with_name("call_PUSH");
	    SPAGAIN;
	    SensorCall(12999);if (gimme == G_ARRAY) {
		SensorCall(12996);I32 i;
		/* EXTEND should not be needed - we just popped them */
		EXTEND(SP, iters);
		SensorCall(12998);for (i=0; i < iters; i++) {
		    SensorCall(12997);SV **svp = av_fetch(ary, i, FALSE);
		    PUSHs((svp) ? *svp : &PL_sv_undef);
		}
		RETURN;
	    }
	}
    }
    else {
	SensorCall(13000);if (gimme == G_ARRAY)
	    RETURN;
    }

    GETTARGET;
    PUSHi(iters);
    RETURN;
}

PP(pp_once)
{
SensorCall(13002);    dSP;
    SV *const sv = PAD_SVl(PL_op->op_targ);

    SensorCall(13003);if (SvPADSTALE(sv)) {
	/* First time. */
	SvPADSTALE_off(sv);
	RETURNOP(cLOGOP->op_other);
    }
    RETURNOP(cLOGOP->op_next);
}

PP(pp_lock)
{
SensorCall(13004);    dVAR;
    dSP;
    dTOPss;
    SV *retsv = sv;
    SvLOCK(sv);
    SensorCall(13006);if (SvTYPE(retsv) == SVt_PVAV || SvTYPE(retsv) == SVt_PVHV
     || SvTYPE(retsv) == SVt_PVCV) {
	SensorCall(13005);retsv = refto(retsv);
    }
    SETs(retsv);
    RETURN;
}


PP(unimplemented_op)
{
SensorCall(13007);    dVAR;
    const Optype op_type = PL_op->op_type;
    /* Using OP_NAME() isn't going to be helpful here. Firstly, it doesn't cope
       with out of range op numbers - it only "special" cases op_custom.
       Secondly, as the three ops we "panic" on are padmy, mapstart and custom,
       if we get here for a custom op then that means that the custom op didn't
       have an implementation. Given that OP_NAME() looks up the custom op
       by its pp_addr, likely it will return NULL, unless someone (unhelpfully)
       registers &PL_unimplemented_op as the address of their custom op.
       NULL doesn't generate a useful error message. "custom" does. */
    const char *const name = op_type >= OP_max
	? "[out of range]" : PL_op_name[PL_op->op_type];
    SensorCall(13008);if(OP_IS_SOCKET(op_type))
	DIE(aTHX_ PL_no_sock_func, name);
    DIE(aTHX_ "panic: unimplemented op %s (#%d) called", name,	op_type);
}

PP(pp_boolkeys)
{
SensorCall(13009);    dVAR;
    dSP;
    HV * const hv = (HV*)POPs;
    
    SensorCall(13010);if (SvTYPE(hv) != SVt_PVHV) { XPUSHs(&PL_sv_no); RETURN; }

    SensorCall(13013);if (SvRMAGICAL(hv)) {
	SensorCall(13011);MAGIC * const mg = mg_find((SV*)hv, PERL_MAGIC_tied);
	SensorCall(13012);if (mg) {
            XPUSHs(magic_scalarpack(hv, mg));
	    RETURN;
        }	    
    }

    XPUSHs(boolSV(HvUSEDKEYS(hv) != 0));
    RETURN;
}

/* For sorting out arguments passed to a &CORE:: subroutine */
PP(pp_coreargs)
{
SensorCall(13014);    dSP;
    int opnum = SvIOK(cSVOP_sv) ? (int)SvUV(cSVOP_sv) : 0;
    int defgv = PL_opargs[opnum] & OA_DEFGV, whicharg = 0;
    AV * const at_ = GvAV(PL_defgv);
    SV **svp = AvARRAY(at_);
    I32 minargs = 0, maxargs = 0, numargs = AvFILLp(at_)+1;
    I32 oa = opnum ? PL_opargs[opnum] >> OASHIFT : 0;
    bool seen_question = 0;
    const char *err = NULL;
    const bool pushmark = PL_op->op_private & OPpCOREARGS_PUSHMARK;

    /* Count how many args there are first, to get some idea how far to
       extend the stack. */
    SensorCall(13024);while (oa) {
	SensorCall(13015);if ((oa & 7) == OA_LIST) { SensorCall(13016);maxargs = I32_MAX; SensorCall(13017);break; }
	SensorCall(13018);maxargs++;
	SensorCall(13020);if (oa & OA_OPTIONAL) {/*93*/SensorCall(13019);seen_question = 1;/*94*/}
	SensorCall(13022);if (!seen_question) {/*95*/SensorCall(13021);minargs++;/*96*/}
	SensorCall(13023);oa >>= 4;
    }

    SensorCall(13028);if(numargs < minargs) {/*97*/SensorCall(13025);err = "Not enough";/*98*/}
    else {/*99*/SensorCall(13026);if(numargs > maxargs) {/*101*/SensorCall(13027);err = "Too many";/*102*/}/*100*/}
    SensorCall(13030);if (err)
	/* diag_listed_as: Too many arguments for %s */
	{/*103*/SensorCall(13029);Perl_croak(aTHX_
	  "%s arguments for %s", err,
	   opnum ? OP_DESC(PL_op->op_next) : SvPV_nolen_const(cSVOP_sv)
	);/*104*/}

    /* Reset the stack pointer.  Without this, we end up returning our own
       arguments in list context, in addition to the values we are supposed
       to return.  nextstate usually does this on sub entry, but we need
       to run the next op with the caller's hints, so we cannot have a
       nextstate. */
    SP = PL_stack_base + cxstack[cxstack_ix].blk_oldsp;

    SensorCall(13031);if(!maxargs) RETURN;

    /* We do this here, rather than with a separate pushmark op, as it has
       to come in between two things this function does (stack reset and
       arg pushing).  This seems the easiest way to do it. */
    SensorCall(13033);if (pushmark) {
	PUTBACK;
	SensorCall(13032);(void)Perl_pp_pushmark(aTHX);
    }

    EXTEND(SP, maxargs == I32_MAX ? numargs : maxargs);
    PUTBACK; /* The code below can die in various places. */

    SensorCall(13034);oa = PL_opargs[opnum] >> OASHIFT;
    SensorCall(13055);for (; oa&&(numargs||!pushmark); (void)(numargs&&(++svp,--numargs))) {
	SensorCall(13035);whicharg++;
	SensorCall(13053);switch (oa & 7) {
	case OA_SCALAR:
	    SensorCall(13036);if (!numargs && defgv && whicharg == minargs + 1) {
		SensorCall(13037);PERL_SI * const oldsi = PL_curstackinfo;
		I32 const oldcxix = oldsi->si_cxix;
		CV *caller;
		SensorCall(13039);if (oldcxix) {/*105*/SensorCall(13038);oldsi->si_cxix--;/*106*/}
		else PL_curstackinfo = oldsi->si_prev;
		SensorCall(13040);caller = find_runcv(NULL);
		PL_curstackinfo = oldsi;
		oldsi->si_cxix = oldcxix;
		PUSHs(find_rundefsv2(
		    caller,cxstack[cxstack_ix].blk_oldcop->cop_seq
		));
	    }
	    else PUSHs(numargs ? svp && *svp ? *svp : &PL_sv_undef : NULL);
	    SensorCall(13041);break;
	case OA_LIST:
	    SensorCall(13042);while (numargs--) {
		PUSHs(svp && *svp ? *svp : &PL_sv_undef);
		SensorCall(13043);svp++;
	    }
	    RETURN;
	case OA_HVREF:
	    SensorCall(13044);if (!svp || !*svp || !SvROK(*svp)
	     || SvTYPE(SvRV(*svp)) != SVt_PVHV)
		DIE(aTHX_
		/* diag_listed_as: Type of arg %d to &CORE::%s must be %s*/
		 "Type of arg %d to &CORE::%s must be hash reference",
		  whicharg, OP_DESC(PL_op->op_next)
		);
	    PUSHs(SvRV(*svp));
	    SensorCall(13045);break;
	case OA_FILEREF:
	    SensorCall(13046);if (!numargs) PUSHs(NULL);
	    else {/*107*/SensorCall(13047);if(svp && *svp && SvROK(*svp) && isGV_with_GP(SvRV(*svp)))
		/* no magic here, as the prototype will have added an extra
		   refgen and we just want what was there before that */
		PUSHs(SvRV(*svp));
	    else {
		SensorCall(13048);const bool constr = PL_op->op_private & whicharg;
		PUSHs(S_rv2gv(aTHX_
		    svp && *svp ? *svp : &PL_sv_undef,
		    constr, CopHINTS_get(PL_curcop) & HINT_STRICT_REFS,
		    !constr
		));
	    ;/*108*/}}
	    SensorCall(13049);break;
	case OA_SCALARREF:
	  {
	    SensorCall(13050);const bool wantscalar =
		PL_op->op_private & OPpCOREARGS_SCALARMOD;
	    SensorCall(13051);if (!svp || !*svp || !SvROK(*svp)
	        /* We have to permit globrefs even for the \$ proto, as
	           *foo is indistinguishable from ${\*foo}, and the proto-
	           type permits the latter. */
	     || SvTYPE(SvRV(*svp)) > (
	             wantscalar       ? SVt_PVLV
	           : opnum == OP_LOCK ? SVt_PVCV
	           :                    SVt_PVHV
	        )
	       )
		DIE(aTHX_
		/* diag_listed_as: Type of arg %d to &CORE::%s must be %s*/
		 "Type of arg %d to &CORE::%s must be %s",
		  whicharg, OP_DESC(PL_op->op_next),
		  wantscalar
		    ? "scalar reference"
		    : opnum == OP_LOCK
		       ? "reference to one of [$@%&*]"
		       : "reference to one of [$@%*]"
		);
	    PUSHs(SvRV(*svp));
	    SensorCall(13052);break;
	  }
	default:
	    DIE(aTHX_ "panic: unknown OA_*: %x", (unsigned)(oa&7));
	}
	SensorCall(13054);oa = oa >> 4;
    }

    RETURN;
}

PP(pp_runcv)
{
SensorCall(13056);    dSP;
    CV *cv;
    SensorCall(13061);if (PL_op->op_private & OPpOFFBYONE) {
	SensorCall(13057);PERL_SI * const oldsi = PL_curstackinfo;
	I32 const oldcxix = oldsi->si_cxix;
	SensorCall(13059);if (oldcxix) {/*253*/SensorCall(13058);oldsi->si_cxix--;/*254*/}
	else PL_curstackinfo = oldsi->si_prev;
	SensorCall(13060);cv = find_runcv(NULL);
	PL_curstackinfo = oldsi;
	oldsi->si_cxix = oldcxix;
    }
    else cv = find_runcv(NULL);
    XPUSHs(CvEVAL(cv) ? &PL_sv_undef : sv_2mortal(newRV((SV *)cv)));
    RETURN;
}


/*
 * Local variables:
 * c-indentation-style: bsd
 * c-basic-offset: 4
 * indent-tabs-mode: t
 * End:
 *
 * ex: set ts=8 sts=4 sw=4 noet:
 */
