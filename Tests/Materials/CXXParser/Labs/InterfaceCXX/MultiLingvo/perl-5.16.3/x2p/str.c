#include "var/tmp/sensor.h"
/*    str.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1999,
 *    2001, 2002, 2005 by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 */

#include "EXTERN.h"
#include "a2p.h"
#include "util.h"

void
str_numset(register STR *str, double num)
{
    SensorCall();str->str_nval = num;
    str->str_pok = 0;		/* invalidate pointer */
    str->str_nok = 1;		/* validate number */
SensorCall();}

char *
str_2ptr(register STR *str)
{
    SensorCall();register char *s;

    SensorCall();if (!str)
	{/*1*/{char * ReplaceReturn = (char *)""; SensorCall(); return ReplaceReturn;}/*2*/}	/* probably safe - won't be written to */
    GROWSTR(&(str->str_ptr), &(str->str_len), 24);
    SensorCall();s = str->str_ptr;
    SensorCall();if (str->str_nok) {
	SensorCall();sprintf(s,"%.20g",str->str_nval);
	SensorCall();while (*s) {/*3*/SensorCall();s++;/*4*/}
    }
    SensorCall();*s = '\0';
    str->str_cur = s - str->str_ptr;
    str->str_pok = 1;
#ifdef DEBUGGING
    if (debug & 32)
	fprintf(stderr,"0x%lx ptr(%s)\n",(unsigned long)str,str->str_ptr);
#endif
    {char * ReplaceReturn = str->str_ptr; SensorCall(); return ReplaceReturn;}
}

void
str_sset(STR *dstr, register STR *sstr)
{
    SensorCall();if (!sstr)
	{/*33*/SensorCall();str_nset(dstr,No,0);/*34*/}
    else {/*35*/SensorCall();if (sstr->str_nok)
	{/*37*/SensorCall();str_numset(dstr,sstr->str_nval);/*38*/}
    else {/*39*/SensorCall();if (sstr->str_pok)
	{/*41*/SensorCall();str_nset(dstr,sstr->str_ptr,sstr->str_cur);/*42*/}
    else
	{/*43*/SensorCall();str_nset(dstr,"",0);/*44*/}/*40*/}/*36*/}
SensorCall();}

void
str_nset(register STR *str, register const char *ptr, register int len)
{
SensorCall();    GROWSTR(&(str->str_ptr), &(str->str_len), len + 1);
    SensorCall();memcpy(str->str_ptr,ptr,len);
    str->str_cur = len;
    *(str->str_ptr+str->str_cur) = '\0';
    str->str_nok = 0;		/* invalidate number */
    str->str_pok = 1;		/* validate pointer */
SensorCall();}

void
str_set(register STR *str, register const char *ptr)
{
    SensorCall();register int len;

    SensorCall();if (!ptr)
	{/*31*/SensorCall();ptr = "";/*32*/}
    SensorCall();len = strlen(ptr);
    GROWSTR(&(str->str_ptr), &(str->str_len), len + 1);
    memcpy(str->str_ptr,ptr,len+1);
    str->str_cur = len;
    str->str_nok = 0;		/* invalidate number */
    str->str_pok = 1;		/* validate pointer */
SensorCall();}

void
str_ncat(register STR *str, register const char *ptr, register int len)
{
    SensorCall();if (!(str->str_pok))
	{/*25*/SensorCall();str_2ptr(str);/*26*/}
    GROWSTR(&(str->str_ptr), &(str->str_len), str->str_cur + len + 1);
    SensorCall();memcpy(str->str_ptr+str->str_cur, ptr, len);
    str->str_cur += len;
    *(str->str_ptr+str->str_cur) = '\0';
    str->str_nok = 0;		/* invalidate number */
    str->str_pok = 1;		/* validate pointer */
SensorCall();}

void
str_scat(STR *dstr, register STR *sstr)
{
    SensorCall();if (!(sstr->str_pok))
	{/*27*/SensorCall();str_2ptr(sstr);/*28*/}
    SensorCall();if (sstr)
	{/*29*/SensorCall();str_ncat(dstr,sstr->str_ptr,sstr->str_cur);/*30*/}
SensorCall();}

void
str_cat(register STR *str, register const char *ptr)
{
    SensorCall();register int len;

    SensorCall();if (!ptr)
	{/*5*/SensorCall();return;/*6*/}
    SensorCall();if (!(str->str_pok))
	{/*7*/SensorCall();str_2ptr(str);/*8*/}
    SensorCall();len = strlen(ptr);
    GROWSTR(&(str->str_ptr), &(str->str_len), str->str_cur + len + 1);
    memcpy(str->str_ptr+str->str_cur, ptr, len+1);
    str->str_cur += len;
    str->str_nok = 0;		/* invalidate number */
    str->str_pok = 1;		/* validate pointer */
SensorCall();}

STR *
str_new(int len)
{
    SensorCall();register STR *str;
    
    SensorCall();if (freestrroot) {
	SensorCall();str = freestrroot;
	freestrroot = str->str_link.str_next;
    }
    else {
	SensorCall();str = (STR *) safemalloc(sizeof(STR));
	memset((char*)str,0,sizeof(STR));
    }
    SensorCall();if (len)
	GROWSTR(&(str->str_ptr), &(str->str_len), len + 1);
    {STR * ReplaceReturn = str; SensorCall(); return ReplaceReturn;}
}

/* make str point to what nstr did */

void
str_free(register STR *str)
{
    SensorCall();if (!str)
	{/*9*/SensorCall();return;/*10*/}
    SensorCall();if (str->str_len)
	{/*11*/SensorCall();str->str_ptr[0] = '\0';/*12*/}
    SensorCall();str->str_cur = 0;
    str->str_nok = 0;
    str->str_pok = 0;
    str->str_link.str_next = freestrroot;
    freestrroot = str;
SensorCall();}

int
str_len(register STR *str)
{
    SensorCall();if (!str)
	{/*17*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*18*/}
    SensorCall();if (!(str->str_pok))
	{/*19*/SensorCall();str_2ptr(str);/*20*/}
    SensorCall();if (str->str_len)
	{/*21*/{int  ReplaceReturn = str->str_cur; SensorCall(); return ReplaceReturn;}/*22*/}
    else
	{/*23*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*24*/}
SensorCall();}

char *
str_gets(register STR *str, register FILE *fp)
{
#if defined(USE_STDIO_PTR) && defined(STDIO_PTR_LVALUE) && defined(STDIO_CNT_LVALUE)
    /* Here is some breathtakingly efficient cheating */

    register char *bp;		/* we're going to steal some values */
    register int cnt;		/*  from the stdio struct and put EVERYTHING */
    register STDCHAR *ptr;	/*   in the innermost loop into registers */
    register char newline = '\n';	/* (assuming at least 6 registers) */
    int i;
    int bpx;

#if defined(VMS)
    /* An ungetc()d char is handled separately from the regular
     * buffer, so we getc() it back out and stuff it in the buffer.
     */
    i = getc(fp);
    if (i == EOF) return NULL;
    *(--((*fp)->_ptr)) = (unsigned char) i;
    (*fp)->_cnt++;
#endif

    cnt = FILE_cnt(fp);			/* get count into register */
    str->str_nok = 0;			/* invalidate number */
    str->str_pok = 1;			/* validate pointer */
    if (str->str_len <= cnt)		/* make sure we have the room */
	GROWSTR(&(str->str_ptr), &(str->str_len), cnt+1);
    bp = str->str_ptr;			/* move these two too to registers */
    ptr = (STDCHAR*)FILE_ptr(fp);
    for (;;) {
	while (--cnt >= 0) {
	    if ((*bp++ = *ptr++) == newline) {
		if (bp <= str->str_ptr || bp[-2] != '\\')
		    goto thats_all_folks;
		else {
		    line++;
		    bp -= 2;
		}
	    }
	}
	
	FILE_cnt(fp) = cnt;		/* deregisterize cnt and ptr */
	FILE_ptr(fp) = (void*)ptr; /* LHS STDCHAR* cast non-portable */
	i = getc(fp);		/* get more characters */
	cnt = FILE_cnt(fp);
	ptr = (STDCHAR*)FILE_ptr(fp);		/* reregisterize cnt and ptr */

	bpx = bp - str->str_ptr;	/* prepare for possible relocation */
	GROWSTR(&(str->str_ptr), &(str->str_len), str->str_cur + cnt + 1);
	bp = str->str_ptr + bpx;	/* reconstitute our pointer */

	if (i == newline) {		/* all done for now? */
	    *bp++ = i;
	    goto thats_all_folks;
	}
	else if (i == EOF)		/* all done for ever? */
	    goto thats_all_folks;
	*bp++ = i;			/* now go back to screaming loop */
    }

thats_all_folks:
    FILE_cnt(fp) = cnt;			/* put these back or we're in trouble */
    FILE_ptr(fp) = (void*)ptr; /* LHS STDCHAR* cast non-portable */
    *bp = '\0';
    str->str_cur = bp - str->str_ptr;	/* set length */

#else /* USE_STDIO_PTR && STDIO_PTR_LVALUE && STDIO_CNT_LVALUE */
    /* The big, slow, and stupid way */

    SensorCall();static char buf[4192];

    SensorCall();if (fgets(buf, sizeof buf, fp) != NULL)
	{/*13*/SensorCall();str_set(str, buf);/*14*/}
    else
	{/*15*/SensorCall();str_set(str, No);/*16*/}

#endif /* USE_STDIO_PTR && STDIO_PTR_LVALUE && STDIO_CNT_LVALUE */

    {char * ReplaceReturn = str->str_cur ? str->str_ptr : NULL; SensorCall(); return ReplaceReturn;}
}

STR *
str_make(const char *s)
{
    SensorCall();register STR *str = str_new(0);

    str_set(str,s);
    {STR * ReplaceReturn = str; SensorCall(); return ReplaceReturn;}
}

