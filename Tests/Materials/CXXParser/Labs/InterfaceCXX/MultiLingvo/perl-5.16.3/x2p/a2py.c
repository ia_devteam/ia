#include "var/tmp/sensor.h"
/*    a2py.c
 *
 *    Copyright (C) 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
 *    2000, 2001, 2002, by Larry Wall and others
 *
 *    You may distribute under the terms of either the GNU General Public
 *    License or the Artistic License, as specified in the README file.
 */

#if defined(OS2) || defined(WIN32) || defined(NETWARE)
#if defined(WIN32)
#include <io.h>
#endif
#if defined(NETWARE)
#include "../netware/clibstuf.h"
#endif
#include "../patchlevel.h"
#endif
#include "util.h"

const char *filename;
const char *myname;

int checkers = 0;

int oper0(int type);
int oper1(int type, int arg1);
int oper2(int type, int arg1, int arg2);
int oper3(int type, int arg1, int arg2, int arg3);
int oper4(int type, int arg1, int arg2, int arg3, int arg4);
int oper5(int type, int arg1, int arg2, int arg3, int arg4, int arg5);
STR *walk(int useval, int level, register int node, int *numericptr, int minprec);
#ifdef NETWARE
char *savestr(char *str);
char *cpy2(register char *to, register char *from, register int delim);
#endif

#if defined(OS2) || defined(WIN32) || defined(NETWARE)
static void usage(void);

static void
usage()
{
    printf("\nThis is the AWK to PERL translator, revision %d.0, version %d\n", PERL_REVISION, PERL_VERSION);
    printf("\nUsage: %s [-D<number>] [-F<char>] [-n<fieldlist>] [-<number>] filename\n", myname);
    printf("\n  -D<number>      sets debugging flags."
           "\n  -F<character>   the awk script to translate is always invoked with"
           "\n                  this -F switch."
           "\n  -n<fieldlist>   specifies the names of the input fields if input does"
           "\n                  not have to be split into an array."
           "\n  -<number>       causes a2p to assume that input will always have that"
           "\n                  many fields.\n");
    exit(1);
}
#endif

#ifdef __osf__
#pragma message disable (mainparm) /* We have the envp in main(). */
#endif

int
main(register int argc, register const char **argv, register const char **env)
{
    SensorCall();register STR *str;
    int i;
    STR *tmpstr;
    /* char *namelist;    */

	#ifdef NETWARE
		fnInitGpfGlobals();	/* For importing the CLIB calls in place of Watcom calls */
	#endif	/* NETWARE */

    myname = argv[0];
    linestr = str_new(80);
    str = str_new(0);		/* first used for -I flags */
    SensorCall();for (argc--,argv++; argc; argc--,argv++) {
	SensorCall();if (argv[0][0] != '-' || !argv[0][1])
	    {/*217*/SensorCall();break;/*218*/}
	SensorCall();switch (argv[0][1]) {
#ifdef DEBUGGING
	case 'D':
	    debug = atoi(argv[0]+2);
#if YYDEBUG
	    yydebug = (debug & 1);
#endif
	    break;
#endif
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
	    SensorCall();maxfld = atoi(argv[0]+1);
	    absmaxfld = TRUE;
	    SensorCall();break;
	case 'F':
	    SensorCall();fswitch = argv[0][2];
	    SensorCall();break;
	case 'n':
	    SensorCall();namelist = savestr(argv[0]+2);
	    SensorCall();break;
	case 'o':
	    SensorCall();old_awk = TRUE;
	    SensorCall();break;
	case '-':
	    SensorCall();argc--,argv++;
	    SensorCall();goto switch_end;
	case 0:
	    SensorCall();break;
	default:
#if defined(OS2) || defined(WIN32) || defined(NETWARE)
	    fprintf(stderr, "Unrecognized switch: %s\n",argv[0]);
            usage();
#else
	    fatal("Unrecognized switch: %s\n",argv[0]);
#endif
	}
    }
  switch_end:

    /* open script */

    SensorCall();if (argv[0] == NULL) {
#if defined(OS2) || defined(WIN32) || defined(NETWARE)
	if ( isatty(fileno(stdin)) )
	    usage();
#endif
        SensorCall();argv[0] = "-";
    }
    SensorCall();filename = savestr(argv[0]);

    SensorCall();if (strEQ(filename,"-"))
	{/*219*/SensorCall();argv[0] = "";/*220*/}
    SensorCall();if (!*argv[0])
	rsfp = stdin;
    else
	{/*221*/SensorCall();rsfp = fopen(argv[0],"r");/*222*/}
    SensorCall();if (rsfp == NULL)
	fatal("Awk script \"%s\" doesn't seem to exist.\n",filename);

    /* init tokener */

    SensorCall();bufptr = str_get(linestr);
    symtab = hnew();
    curarghash = hnew();

    /* now parse the report spec */

    SensorCall();if (yyparse())
	fatal("Translation aborted due to syntax errors.\n");

#ifdef DEBUGGING
    if (debug & 2) {
	int type, len;

	for (i=1; i<mop;) {
	    type = ops[i].ival;
	    len = type >> 8;
	    type &= 255;
	    printf("%d\t%d\t%d\t%-10s",i++,type,len,opname[type]);
	    if (type == OSTRING)
		printf("\t\"%s\"\n",ops[i].cval),i++;
	    else {
		while (len--) {
		    printf("\t%d",ops[i].ival),i++;
		}
		putchar('\n');
	    }
	}
    }
    if (debug & 8)
	dump(root);
#endif

    /* first pass to look for numeric variables */

    SensorCall();prewalk(0,0,root,&i);

    /* second pass to produce new program */

    tmpstr = walk(0,0,root,&i,P_MIN);
    str = str_make(STARTPERL);
    str_cat(str, "\neval 'exec ");
    str_cat(str, BIN);
    str_cat(str, "/perl -S $0 ${1+\"$@\"}'\n\
    if $running_under_some_shell;\n\
			# this emulates #! processing on NIH machines.\n\
			# (remove #! line above if indigestible)\n\n");
    str_cat(str,
      "eval '$'.$1.'$2;' while $ARGV[0] =~ /^([A-Za-z_0-9]+=)(.*)/ && shift;\n");
    str_cat(str,
      "			# process any FOO=bar switches\n\n");
    SensorCall();if (do_opens && opens) {
	SensorCall();str_scat(str,opens);
	str_free(opens);
	str_cat(str,"\n");
    }
    SensorCall();str_scat(str,tmpstr);
    str_free(tmpstr);
#ifdef DEBUGGING
    if (!(debug & 16))
#endif
    fixup(str);
    putlines(str);
    SensorCall();if (checkers) {
	SensorCall();fprintf(stderr,
	  "Please check my work on the %d line%s I've marked with \"#???\".\n",
		checkers, checkers == 1 ? "" : "s" );
	fprintf(stderr,
	  "The operation I've selected may be wrong for the operand types.\n");
    }
    SensorCall();exit(0);
    /* by ANSI specs return is needed. This also shuts up VC++ and his warnings */
    SensorCall();return(0);
}

#define RETURN(retval) return (bufptr = s,retval)
#define XTERM(retval) return (expectterm = TRUE,bufptr = s,retval)
#define XOP(retval) return (expectterm = FALSE,bufptr = s,retval)
#define ID(x) return (yylval=string(x,0),expectterm = FALSE,bufptr = s,idtype)

int idtype;

int
yylex(void)
{
    SensorCall();register char *s = bufptr;
    register char *d;
    register int tmp;

  retry:
#if YYDEBUG
    if (yydebug) {
	if (strchr(s,'\n'))
	    fprintf(stderr,"Tokener at %s",s);
	else
	    fprintf(stderr,"Tokener at %s\n",s);
    }
#endif
    SensorCall();switch (*s) {
    default:
	SensorCall();fprintf(stderr,
	    "Unrecognized character %c in file %s line %d--ignoring.\n",
	     *s++,filename,line);
	SensorCall();goto retry;
    case '\\':
	SensorCall();s++;
	SensorCall();if (*s && *s != '\n') {
	    SensorCall();yyerror("Ignoring spurious backslash");
	    SensorCall();goto retry;
	}
	/*FALLSTHROUGH*/
    case 0:
	SensorCall();s = str_get(linestr);
	*s = '\0';
	if (!rsfp)
	    RETURN(0);
	SensorCall();line++;
	SensorCall();if ((s = str_gets(linestr, rsfp)) == NULL) {
	    SensorCall();if (rsfp != stdin)
		{/*93*/SensorCall();fclose(rsfp);/*94*/}
	    SensorCall();rsfp = NULL;
	    s = str_get(linestr);
	    RETURN(0);
	}
	SensorCall();goto retry;
    case ' ': case '\t':
	SensorCall();s++;
	SensorCall();goto retry;
    case '\n':
	SensorCall();*s = '\0';
	XTERM(NEWLINE);
    case '#':
	SensorCall();yylval = string(s,0);
	*s = '\0';
	XTERM(COMMENT);
    case ';':
	SensorCall();tmp = *s++;
	SensorCall();if (*s == '\n') {
	    SensorCall();s++;
	    XTERM(SEMINEW);
	}
	XTERM(tmp);
    case '(':
	SensorCall();tmp = *s++;
	XTERM(tmp);
    case '{':
    case '[':
    case ')':
    case ']':
    case '?':
    case ':':
	SensorCall();tmp = *s++;
	XOP(tmp);
#ifdef EBCDIC
    case 7:
#else
    case 127:
#endif
	SensorCall();s++;
	XTERM('}');
    case '}':
	SensorCall();for (d = s + 1; isSPACE(*d); d++) ;
	SensorCall();if (!*d)
	    {/*97*/SensorCall();s = d - 1;/*98*/}
	SensorCall();*s = 127;
	XTERM(';');
    case ',':
	SensorCall();tmp = *s++;
	XTERM(tmp);
    case '~':
	SensorCall();s++;
	yylval = string("~",1);
	XTERM(MATCHOP);
    case '+':
    case '-':
	SensorCall();if (s[1] == *s) {
	    SensorCall();s++;
	    if (*s++ == '+')
		XTERM(INCR);
	    else
		XTERM(DECR);
	}
	/* FALL THROUGH */
    case '*':
    case '%':
    case '^':
	SensorCall();tmp = *s++;
	SensorCall();if (*s == '=') {
	    SensorCall();if (tmp == '^')
		{/*103*/SensorCall();yylval = string("**=",3);/*104*/}
	    else
		{/*105*/SensorCall();yylval = string(s-1,2);/*106*/}
	    SensorCall();s++;
	    XTERM(ASGNOP);
	}
	XTERM(tmp);
    case '&':
	SensorCall();s++;
	tmp = *s++;
	if (tmp == '&')
	    XTERM(ANDAND);
	SensorCall();s--;
	XTERM('&');
    case '|':
	SensorCall();s++;
	tmp = *s++;
	if (tmp == '|')
	    XTERM(OROR);
	SensorCall();s--;
	SensorCall();while (*s == ' ' || *s == '\t')
	    {/*111*/SensorCall();s++;/*112*/}
	if (strnEQ(s,"getline",7))
	    XTERM('p');
	else
	    XTERM('|');
    case '=':
	SensorCall();s++;
	tmp = *s++;
	SensorCall();if (tmp == '=') {
	    SensorCall();yylval = string("==",2);
	    XTERM(RELOP);
	}
	SensorCall();s--;
	yylval = string("=",1);
	XTERM(ASGNOP);
    case '!':
	SensorCall();s++;
	tmp = *s++;
	SensorCall();if (tmp == '=') {
	    SensorCall();yylval = string("!=",2);
	    XTERM(RELOP);
	}
	SensorCall();if (tmp == '~') {
	    SensorCall();yylval = string("!~",2);
	    XTERM(MATCHOP);
	}
	SensorCall();s--;
	XTERM(NOT);
    case '<':
	SensorCall();s++;
	tmp = *s++;
	SensorCall();if (tmp == '=') {
	    SensorCall();yylval = string("<=",2);
	    XTERM(RELOP);
	}
	SensorCall();s--;
	XTERM('<');
    case '>':
	SensorCall();s++;
	tmp = *s++;
	SensorCall();if (tmp == '>') {
	    SensorCall();yylval = string(">>",2);
	    XTERM(GRGR);
	}
	SensorCall();if (tmp == '=') {
	    SensorCall();yylval = string(">=",2);
	    XTERM(RELOP);
	}
	SensorCall();s--;
	XTERM('>');

#define SNARFWORD \
	d = tokenbuf; \
	while (isALPHA(*s) || isDIGIT(*s) || *s == '_') \
	    *d++ = *s++; \
	*d = '\0'; \
	d = tokenbuf; \
	if (*s == '(') \
	    idtype = USERFUN; \
	else \
	    idtype = VAR;

    case '$':
	SensorCall();s++;
	SensorCall();if (*s == '0') {
	    SensorCall();s++;
	    do_chop = TRUE;
	    need_entire = TRUE;
	    idtype = VAR;
	    ID("0");
	}
	SensorCall();do_split = TRUE;
	SensorCall();if (isDIGIT(*s)) {
	    SensorCall();for (d = s; isDIGIT(*s); s++) ;
	    SensorCall();yylval = string(d,s-d);
	    tmp = atoi(d);
	    SensorCall();if (tmp > maxfld)
		{/*119*/SensorCall();maxfld = tmp;/*120*/}
	    XOP(FIELD);
	}
	SensorCall();for (d = s; isALPHA(*s) || isDIGIT(*s) || *s == '_'; )
	    {/*121*/SensorCall();s++;/*122*/}
	SensorCall();split_to_array = TRUE;
	SensorCall();if (d != s)
	{
	    SensorCall();yylval = string(d,s-d);
	    XTERM(SVFIELD);
	}
	XOP(VFIELD);

    case '/':			/* may either be division or pattern */
	SensorCall();if (expectterm) {
	    SensorCall();s = scanpat(s);
	    XTERM(REGEX);
	}
	SensorCall();tmp = *s++;
	SensorCall();if (*s == '=') {
	    SensorCall();yylval = string("/=",2);
	    s++;
	    XTERM(ASGNOP);
	}
	XTERM(tmp);

    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9': case '.':
	SensorCall();s = scannum(s);
	XOP(NUMBER);
    case '"':
	SensorCall();s++;
	s = cpy2(tokenbuf,s,s[-1]);
	SensorCall();if (!*s)
	    fatal("String not terminated:\n%s",str_get(linestr));
	SensorCall();s++;
	yylval = string(tokenbuf,0);
	XOP(STRING);

    case 'a': case 'A':
	SNARFWORD;
	SensorCall();if (strEQ(d,"ARGV")) {
	    SensorCall();yylval=numary(string("ARGV",0));
	    XOP(VAR);
	}
	SensorCall();if (strEQ(d,"atan2")) {
	    SensorCall();yylval = OATAN2;
	    XTERM(FUNN);
	}
	ID(d);
    case 'b': case 'B':
	SNARFWORD;
	if (strEQ(d,"break"))
	    XTERM(BREAK);
	if (strEQ(d,"BEGIN"))
	    XTERM(BEGIN);
	ID(d);
    case 'c': case 'C':
	SNARFWORD;
	if (strEQ(d,"continue"))
	    XTERM(CONTINUE);
	SensorCall();if (strEQ(d,"cos")) {
	    SensorCall();yylval = OCOS;
	    XTERM(FUN1);
	}
	SensorCall();if (strEQ(d,"close")) {
	    SensorCall();do_fancy_opens = 1;
	    yylval = OCLOSE;
	    XTERM(FUN1);
	}
	SensorCall();if (strEQ(d,"chdir"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"crypt"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"chop"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"chmod"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"chown"))
	    *d = toUPPER(*d);
	ID(d);
    case 'd': case 'D':
	SNARFWORD;
	if (strEQ(d,"do"))
	    XTERM(DO);
	if (strEQ(d,"delete"))
	    XTERM(DELETE);
	SensorCall();if (strEQ(d,"die"))
	    *d = toUPPER(*d);
	ID(d);
    case 'e': case 'E':
	SNARFWORD;
	if (strEQ(d,"END"))
	    XTERM(END);
	if (strEQ(d,"else"))
	    XTERM(ELSE);
	SensorCall();if (strEQ(d,"exit")) {
	    SensorCall();saw_line_op = TRUE;
	    XTERM(EXIT);
	}
	SensorCall();if (strEQ(d,"exp")) {
	    SensorCall();yylval = OEXP;
	    XTERM(FUN1);
	}
	SensorCall();if (strEQ(d,"elsif"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"eq"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"eval"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"eof"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"each"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"exec"))
	    *d = toUPPER(*d);
	ID(d);
    case 'f': case 'F':
	SNARFWORD;
	SensorCall();if (strEQ(d,"FS")) {
	    SensorCall();saw_FS++;
	    SensorCall();if (saw_FS == 1 && in_begin) {
		SensorCall();for (d = s; *d && isSPACE(*d); d++) ;
		SensorCall();if (*d == '=') {
		    SensorCall();for (d++; *d && isSPACE(*d); d++) ;
		    SensorCall();if (*d == '"' && d[2] == '"')
			{/*153*/SensorCall();const_FS = d[1];/*154*/}
		}
	    }
	    ID(tokenbuf);
	}
	if (strEQ(d,"for"))
	    XTERM(FOR);
	else if (strEQ(d,"function"))
	    XTERM(FUNCTION);
	if (strEQ(d,"FILENAME"))
	    ID("ARGV");
	SensorCall();if (strEQ(d,"foreach"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"format"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"fork"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"fh"))
	    *d = toUPPER(*d);
	ID(d);
    case 'g': case 'G':
	SNARFWORD;
	if (strEQ(d,"getline"))
	    XTERM(GETLINE);
	if (strEQ(d,"gsub"))
	    XTERM(GSUB);
	SensorCall();if (strEQ(d,"ge"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"gt"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"goto"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"gmtime"))
	    *d = toUPPER(*d);
	ID(d);
    case 'h': case 'H':
	SNARFWORD;
	SensorCall();if (strEQ(d,"hex"))
	    *d = toUPPER(*d);
	ID(d);
    case 'i': case 'I':
	SNARFWORD;
	if (strEQ(d,"if"))
	    XTERM(IF);
	if (strEQ(d,"in"))
	    XTERM(IN);
	SensorCall();if (strEQ(d,"index")) {
	    XTERM(INDEX);
	}
	SensorCall();if (strEQ(d,"int")) {
	    SensorCall();yylval = OINT;
	    XTERM(FUN1);
	}
	ID(d);
    case 'j': case 'J':
	SNARFWORD;
	SensorCall();if (strEQ(d,"join"))
	    *d = toUPPER(*d);
	ID(d);
    case 'k': case 'K':
	SNARFWORD;
	SensorCall();if (strEQ(d,"keys"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"kill"))
	    *d = toUPPER(*d);
	ID(d);
    case 'l': case 'L':
	SNARFWORD;
	SensorCall();if (strEQ(d,"length")) {
	    SensorCall();yylval = OLENGTH;
	    XTERM(FUN1);
	}
	SensorCall();if (strEQ(d,"log")) {
	    SensorCall();yylval = OLOG;
	    XTERM(FUN1);
	}
	SensorCall();if (strEQ(d,"last"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"local"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"lt"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"le"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"locatime"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"link"))
	    *d = toUPPER(*d);
	ID(d);
    case 'm': case 'M':
	SNARFWORD;
	SensorCall();if (strEQ(d,"match")) {
	    XTERM(MATCH);
	}
	SensorCall();if (strEQ(d,"m"))
	    *d = toUPPER(*d);
	ID(d);
    case 'n': case 'N':
	SNARFWORD;
	SensorCall();if (strEQ(d,"NF"))
	    do_chop = do_split = split_to_array = TRUE;
	SensorCall();if (strEQ(d,"next")) {
	    SensorCall();saw_line_op = TRUE;
	    XTERM(NEXT);
	}
	SensorCall();if (strEQ(d,"ne"))
	    *d = toUPPER(*d);
	ID(d);
    case 'o': case 'O':
	SNARFWORD;
	SensorCall();if (strEQ(d,"ORS")) {
	    SensorCall();saw_ORS = TRUE;
	    ID("\\");
	}
	SensorCall();if (strEQ(d,"OFS")) {
	    SensorCall();saw_OFS = TRUE;
	    ID(",");
	}
	SensorCall();if (strEQ(d,"OFMT")) {
	    ID("#");
	}
	SensorCall();if (strEQ(d,"open"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"ord"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"oct"))
	    *d = toUPPER(*d);
	ID(d);
    case 'p': case 'P':
	SNARFWORD;
	SensorCall();if (strEQ(d,"print")) {
	    XTERM(PRINT);
	}
	SensorCall();if (strEQ(d,"printf")) {
	    XTERM(PRINTF);
	}
	SensorCall();if (strEQ(d,"push"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"pop"))
	    *d = toUPPER(*d);
	ID(d);
    case 'q': case 'Q':
	SNARFWORD;
	ID(d);
    case 'r': case 'R':
	SNARFWORD;
	SensorCall();if (strEQ(d,"RS")) {
	    SensorCall();saw_RS = TRUE;
	    ID("/");
	}
	SensorCall();if (strEQ(d,"rand")) {
	    SensorCall();yylval = ORAND;
	    XTERM(FUN1);
	}
	if (strEQ(d,"return"))
	    XTERM(RET);
	SensorCall();if (strEQ(d,"reset"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"redo"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"rename"))
	    *d = toUPPER(*d);
	ID(d);
    case 's': case 'S':
	SNARFWORD;
	SensorCall();if (strEQ(d,"split")) {
	    XOP(SPLIT);
	}
	SensorCall();if (strEQ(d,"substr")) {
	    XTERM(SUBSTR);
	}
	if (strEQ(d,"sub"))
	    XTERM(SUB);
	SensorCall();if (strEQ(d,"sprintf")) {
            /* In old awk, { print sprintf("str%sg"),"in" } prints
             * "string"; in new awk, "in" is not considered an argument to
             * sprintf, so the statement breaks.  To support both, the
             * grammar treats arguments to SPRINTF_OLD like old awk,
             * SPRINTF_NEW like new.  Here we return the appropriate one.
             */
	    XTERM(old_awk ? SPRINTF_OLD : SPRINTF_NEW);
        }
	SensorCall();if (strEQ(d,"sqrt")) {
	    SensorCall();yylval = OSQRT;
	    XTERM(FUN1);
	}
	SensorCall();if (strEQ(d,"SUBSEP")) {
	    ID(";");
	}
	SensorCall();if (strEQ(d,"sin")) {
	    SensorCall();yylval = OSIN;
	    XTERM(FUN1);
	}
	SensorCall();if (strEQ(d,"srand")) {
	    SensorCall();yylval = OSRAND;
	    XTERM(FUN1);
	}
	SensorCall();if (strEQ(d,"system")) {
	    SensorCall();yylval = OSYSTEM;
	    XTERM(FUN1);
	}
	SensorCall();if (strEQ(d,"s"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"shift"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"select"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"seek"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"stat"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"study"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"sleep"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"symlink"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"sort"))
	    *d = toUPPER(*d);
	ID(d);
    case 't': case 'T':
	SNARFWORD;
	SensorCall();if (strEQ(d,"tr"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"tell"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"time"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"times"))
	    *d = toUPPER(*d);
	ID(d);
    case 'u': case 'U':
	SNARFWORD;
	SensorCall();if (strEQ(d,"until"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"unless"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"umask"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"unshift"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"unlink"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"utime"))
	    *d = toUPPER(*d);
	ID(d);
    case 'v': case 'V':
	SNARFWORD;
	SensorCall();if (strEQ(d,"values"))
	    *d = toUPPER(*d);
	ID(d);
    case 'w': case 'W':
	SNARFWORD;
	if (strEQ(d,"while"))
	    XTERM(WHILE);
	SensorCall();if (strEQ(d,"write"))
	    *d = toUPPER(*d);
	else if (strEQ(d,"wait"))
	    *d = toUPPER(*d);
	ID(d);
    case 'x': case 'X':
	SNARFWORD;
	SensorCall();if (strEQ(d,"x"))
	    *d = toUPPER(*d);
	ID(d);
    case 'y': case 'Y':
	SNARFWORD;
	SensorCall();if (strEQ(d,"y"))
	    *d = toUPPER(*d);
	ID(d);
    case 'z': case 'Z':
	SNARFWORD;
	ID(d);
    }
SensorCall();}

char *
scanpat(register char *s)
{
    SensorCall();register char *d;

    SensorCall();switch (*s++) {
    case '/':
	SensorCall();break;
    default:
	fatal("Search pattern not found:\n%s",str_get(linestr));
    }

    SensorCall();d = tokenbuf;
    SensorCall();for (; *s; s++,d++) {
	SensorCall();if (*s == '\\') {
	    SensorCall();if (s[1] == '/')
		{/*69*/SensorCall();*d++ = *s++;/*70*/}
	    else {/*71*/SensorCall();if (s[1] == '\\')
		{/*73*/SensorCall();*d++ = *s++;/*74*/}
	    else {/*75*/SensorCall();if (s[1] == '[')
		{/*77*/SensorCall();*d++ = *s++;/*78*/}/*76*/}/*72*/}
	}
	else {/*79*/SensorCall();if (*s == '[') {
	    SensorCall();*d++ = *s++;
	    SensorCall();do {
		SensorCall();if (*s == '\\' && s[1])
		    {/*81*/SensorCall();*d++ = *s++;/*82*/}
		SensorCall();if (*s == '/' || (*s == '-' && s[1] == ']'))
		    {/*83*/SensorCall();*d++ = '\\';/*84*/}
		SensorCall();*d++ = *s++;
	    } while (*s && *s != ']');
	}
	else {/*85*/SensorCall();if (*s == '/')
	    {/*87*/SensorCall();break;/*88*/}/*80*/;/*86*/}}
	SensorCall();*d = *s;
    }
    SensorCall();*d = '\0';

    SensorCall();if (!*s)
	fatal("Search pattern not terminated:\n%s",str_get(linestr));
    SensorCall();s++;
    yylval = string(tokenbuf,0);
    {char * ReplaceReturn = s; SensorCall(); return ReplaceReturn;}
}

void
yyerror(const char *s)
{
    SensorCall();fprintf(stderr,"%s in file %s at line %d\n",
      s,filename,line);
SensorCall();}

char *
scannum(register char *s)
{
    SensorCall();register char *d;

    SensorCall();switch (*s) {
    case '1': case '2': case '3': case '4': case '5':
    case '6': case '7': case '8': case '9': case '0' : case '.':
	SensorCall();d = tokenbuf;
	SensorCall();while (isDIGIT(*s)) {
	    SensorCall();*d++ = *s++;
	}
	SensorCall();if (*s == '.') {
	    SensorCall();if (isDIGIT(s[1])) {
		SensorCall();*d++ = *s++;
		SensorCall();while (isDIGIT(*s)) {
		    SensorCall();*d++ = *s++;
		}
	    }
	    else
		{/*63*/SensorCall();s++;/*64*/}
	}
	SensorCall();if (strchr("eE",*s) && strchr("+-0123456789",s[1])) {
	    SensorCall();*d++ = *s++;
	    SensorCall();if (*s == '+' || *s == '-')
		{/*65*/SensorCall();*d++ = *s++;/*66*/}
	    SensorCall();while (isDIGIT(*s))
		{/*67*/SensorCall();*d++ = *s++;/*68*/}
	}
	SensorCall();*d = '\0';
	yylval = string(tokenbuf,0);
	SensorCall();break;
    }
    {char * ReplaceReturn = s; SensorCall(); return ReplaceReturn;}
}

int
string(const char *ptr, int len)
{
    SensorCall();int retval = mop;

    ops[mop++].ival = OSTRING + (1<<8);
    SensorCall();if (!len)
	{/*89*/SensorCall();len = strlen(ptr);/*90*/}
    SensorCall();ops[mop].cval = (char *) safemalloc(len+1);
    strncpy(ops[mop].cval,ptr,len);
    ops[mop++].cval[len] = '\0';
    SensorCall();if (mop >= OPSMAX)
	fatal("Recompile a2p with larger OPSMAX\n");
    {int  ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

int
oper0(int type)
{
    SensorCall();int retval = mop;

    SensorCall();if (type > 255)
	fatal("type > 255 (%d)\n",type);
    SensorCall();ops[mop++].ival = type;
    SensorCall();if (mop >= OPSMAX)
	fatal("Recompile a2p with larger OPSMAX\n");
    {int  ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

int
oper1(int type, int arg1)
{
    SensorCall();int retval = mop;

    SensorCall();if (type > 255)
	fatal("type > 255 (%d)\n",type);
    SensorCall();ops[mop++].ival = type + (1<<8);
    ops[mop++].ival = arg1;
    SensorCall();if (mop >= OPSMAX)
	fatal("Recompile a2p with larger OPSMAX\n");
    {int  ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

int
oper2(int type, int arg1, int arg2)
{
    SensorCall();int retval = mop;

    SensorCall();if (type > 255)
	fatal("type > 255 (%d)\n",type);
    SensorCall();ops[mop++].ival = type + (2<<8);
    ops[mop++].ival = arg1;
    ops[mop++].ival = arg2;
    SensorCall();if (mop >= OPSMAX)
	fatal("Recompile a2p with larger OPSMAX\n");
    {int  ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

int
oper3(int type, int arg1, int arg2, int arg3)
{
    SensorCall();int retval = mop;

    SensorCall();if (type > 255)
	fatal("type > 255 (%d)\n",type);
    SensorCall();ops[mop++].ival = type + (3<<8);
    ops[mop++].ival = arg1;
    ops[mop++].ival = arg2;
    ops[mop++].ival = arg3;
    SensorCall();if (mop >= OPSMAX)
	fatal("Recompile a2p with larger OPSMAX\n");
    {int  ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

int
oper4(int type, int arg1, int arg2, int arg3, int arg4)
{
    SensorCall();int retval = mop;

    SensorCall();if (type > 255)
	fatal("type > 255 (%d)\n",type);
    SensorCall();ops[mop++].ival = type + (4<<8);
    ops[mop++].ival = arg1;
    ops[mop++].ival = arg2;
    ops[mop++].ival = arg3;
    ops[mop++].ival = arg4;
    SensorCall();if (mop >= OPSMAX)
	fatal("Recompile a2p with larger OPSMAX\n");
    {int  ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

int
oper5(int type, int arg1, int arg2, int arg3, int arg4, int arg5)
{
    SensorCall();int retval = mop;

    SensorCall();if (type > 255)
	fatal("type > 255 (%d)\n",type);
    SensorCall();ops[mop++].ival = type + (5<<8);
    ops[mop++].ival = arg1;
    ops[mop++].ival = arg2;
    ops[mop++].ival = arg3;
    ops[mop++].ival = arg4;
    ops[mop++].ival = arg5;
    SensorCall();if (mop >= OPSMAX)
	fatal("Recompile a2p with larger OPSMAX\n");
    {int  ReplaceReturn = retval; SensorCall(); return ReplaceReturn;}
}

int depth = 0;

void
dump(int branch)
{
    SensorCall();register int type;
    register int len;
    register int i;

    type = ops[branch].ival;
    len = type >> 8;
    type &= 255;
    SensorCall();for (i=depth; i; i--)
	{/*15*/SensorCall();printf(" ");/*16*/}
    SensorCall();if (type == OSTRING) {
	SensorCall();printf("%-5d\"%s\"\n",branch,ops[branch+1].cval);
    }
    else {
	SensorCall();printf("(%-5d%s %d\n",branch,opname[type],len);
	depth++;
	SensorCall();for (i=1; i<=len; i++)
	    {/*17*/SensorCall();dump(ops[branch+i].ival);/*18*/}
	SensorCall();depth--;
	SensorCall();for (i=depth; i; i--)
	    {/*19*/SensorCall();printf(" ");/*20*/}
	SensorCall();printf(")\n");
    }
SensorCall();}

int
bl(int arg, int maybe)
{
    SensorCall();if (!arg)
	{/*3*/{int  ReplaceReturn = 0; SensorCall(); return ReplaceReturn;}/*4*/}
    else {/*5*/SensorCall();if ((ops[arg].ival & 255) != OBLOCK)
	{/*7*/{int  ReplaceReturn = oper2(OBLOCK,arg,maybe); SensorCall(); return ReplaceReturn;}/*8*/}
    else {/*9*/SensorCall();if ((ops[arg].ival >> 8) < 2)
	{/*11*/{int  ReplaceReturn = oper2(OBLOCK,ops[arg+1].ival,maybe); SensorCall(); return ReplaceReturn;}/*12*/}
    else
	{/*13*/{int  ReplaceReturn = arg; SensorCall(); return ReplaceReturn;}/*14*/}/*10*/}/*6*/}
SensorCall();}

void
fixup(STR *str)
{
    SensorCall();register char *s;
    register char *t;

    SensorCall();for (s = str->str_ptr; *s; s++) {
	SensorCall();if (*s == ';' && s[1] == ' ' && s[2] == '\n') {
	    SensorCall();strcpy(s+1,s+2);
	    s++;
	}
	else {/*27*/SensorCall();if (*s == '\n') {
	    SensorCall();for (t = s+1; isSPACE(*t & 127); t++) ;
	    SensorCall();t--;
	    SensorCall();while (isSPACE(*t & 127) && *t != '\n') {/*31*/SensorCall();t--;/*32*/}
	    SensorCall();if (*t == '\n' && t-s > 1) {
		SensorCall();if (s[-1] == '{')
		    {/*33*/SensorCall();s--;/*34*/}
		SensorCall();strcpy(s+1,t);
	    }
	    SensorCall();s++;
	;/*28*/}}
    }
SensorCall();}

void
putlines(STR *str)
{
    SensorCall();register char *d, *s, *t, *e;
    register int pos, newpos;

    d = tokenbuf;
    pos = 0;
    SensorCall();for (s = str->str_ptr; *s; s++) {
	SensorCall();*d++ = *s;
	pos++;
	SensorCall();if (*s == '\n') {
	    SensorCall();*d = '\0';
	    d = tokenbuf;
	    pos = 0;
	    putone();
	}
	else {/*35*/SensorCall();if (*s == '\t')
	    {/*37*/SensorCall();pos += 7;/*38*/}/*36*/}
	SensorCall();if (pos > 78) {		/* split a long line? */
	    SensorCall();*d-- = '\0';
	    newpos = 0;
	    SensorCall();for (t = tokenbuf; isSPACE(*t & 127); t++) {
		SensorCall();if (*t == '\t')
		    {/*39*/SensorCall();newpos += 8;/*40*/}
		else
		    {/*41*/SensorCall();newpos += 1;/*42*/}
	    }
	    SensorCall();e = d;
	    SensorCall();while (d > tokenbuf && (*d != ' ' || d[-1] != ';'))
		{/*43*/SensorCall();d--;/*44*/}
	    SensorCall();if (d < t+10) {
		SensorCall();d = e;
		SensorCall();while (d > tokenbuf &&
		  (*d != ' ' || d[-1] != '|' || d[-2] != '|') )
		    {/*45*/SensorCall();d--;/*46*/}
	    }
	    SensorCall();if (d < t+10) {
		SensorCall();d = e;
		SensorCall();while (d > tokenbuf &&
		  (*d != ' ' || d[-1] != '&' || d[-2] != '&') )
		    {/*47*/SensorCall();d--;/*48*/}
	    }
	    SensorCall();if (d < t+10) {
		SensorCall();d = e;
		SensorCall();while (d > tokenbuf && (*d != ' ' || d[-1] != ','))
		    {/*49*/SensorCall();d--;/*50*/}
	    }
	    SensorCall();if (d < t+10) {
		SensorCall();d = e;
		SensorCall();while (d > tokenbuf && *d != ' ')
		    {/*51*/SensorCall();d--;/*52*/}
	    }
	    SensorCall();if (d > t+3) {
                SensorCall();char save[2048];
                strcpy(save, d);
		*d = '\n';
                d[1] = '\0';
		putone();
		putchar('\n');
		SensorCall();if (d[-1] != ';' && !(newpos % 4)) {
		    SensorCall();*t++ = ' ';
		    *t++ = ' ';
		    newpos += 2;
		}
		SensorCall();strcpy(t,save+1);
		newpos += strlen(t);
		d = t + strlen(t);
		pos = newpos;
	    }
	    else
		{/*53*/SensorCall();d = e + 1;/*54*/}
	}
    }
SensorCall();}

void
putone(void)
{
    SensorCall();register char *t;

    SensorCall();for (t = tokenbuf; *t; t++) {
	SensorCall();*t &= 127;
	SensorCall();if (*t == 127) {
	    SensorCall();*t = ' ';
	    strcpy(t+strlen(t)-1, "\t#???\n");
	    checkers++;
	}
    }
    SensorCall();t = tokenbuf;
    SensorCall();if (*t == '#') {
	SensorCall();if (strnEQ(t,"#!/bin/awk",10) || strnEQ(t,"#! /bin/awk",11))
	    {/*55*/SensorCall();return;/*56*/}
	SensorCall();if (strnEQ(t,"#!/usr/bin/awk",14) || strnEQ(t,"#! /usr/bin/awk",15))
	    {/*57*/SensorCall();return;/*58*/}
    }
    SensorCall();fputs(tokenbuf,stdout);
SensorCall();}

int
numary(int arg)
{
    SensorCall();STR *key;
    int dummy;

    key = walk(0,0,arg,&dummy,P_MIN);
    str_cat(key,"[]");
    hstore(symtab,key->str_ptr,str_make("1"));
    str_free(key);
    {int  ReplaceReturn = arg; SensorCall(); return ReplaceReturn;}
}

int
rememberargs(int arg)
{
    SensorCall();int type;
    STR *str;

    SensorCall();if (!arg)
	{/*59*/{int  ReplaceReturn = arg; SensorCall(); return ReplaceReturn;}/*60*/}
    SensorCall();type = ops[arg].ival & 255;
    SensorCall();if (type == OCOMMA) {
	SensorCall();rememberargs(ops[arg+1].ival);
	rememberargs(ops[arg+3].ival);
    }
    else {/*61*/SensorCall();if (type == OVAR) {
	SensorCall();str = str_new(0);
	hstore(curarghash,ops[ops[arg+1].ival+1].cval,str);
    }
    else
	fatal("panic: unknown argument type %d, line %d\n",type,line);/*62*/}
    {int  ReplaceReturn = arg; SensorCall(); return ReplaceReturn;}
}

int
aryrefarg(int arg)
{
    SensorCall();int type = ops[arg].ival & 255;
    STR *str;

    SensorCall();if (type != OSTRING)
	fatal("panic: aryrefarg %d, line %d\n",type,line);
    SensorCall();str = hfetch(curarghash,ops[arg+1].cval);
    SensorCall();if (str)
	{/*1*/SensorCall();str_set(str,"*");/*2*/}
    {int  ReplaceReturn = arg; SensorCall(); return ReplaceReturn;}
}

int
fixfargs(int name, int arg, int prevargs)
{
    SensorCall();int type;
    STR *str;
    int numargs = 0;

    SensorCall();if (!arg)
	{/*21*/{int  ReplaceReturn = prevargs; SensorCall(); return ReplaceReturn;}/*22*/}
    SensorCall();type = ops[arg].ival & 255;
    SensorCall();if (type == OCOMMA) {
	SensorCall();numargs = fixfargs(name,ops[arg+1].ival,prevargs);
	numargs = fixfargs(name,ops[arg+3].ival,numargs);
    }
    else {/*23*/SensorCall();if (type == OVAR) {
	SensorCall();str = hfetch(curarghash,ops[ops[arg+1].ival+1].cval);
	SensorCall();if (strEQ(str_get(str),"*")) {
	    SensorCall();char tmpbuf[128];

	    str_set(str,"");		/* in case another routine has this */
	    ops[arg].ival &= ~255;
	    ops[arg].ival |= OSTAR;
	    sprintf(tmpbuf,"%s:%d",ops[name+1].cval,prevargs);
	    fprintf(stderr,"Adding %s\n",tmpbuf);
	    str = str_new(0);
	    str_set(str,"*");
	    hstore(curarghash,tmpbuf,str);
	}
	SensorCall();numargs = prevargs + 1;
    }
    else
	fatal("panic: unknown argument type %d, arg %d, line %d\n",
	  type,prevargs+1,line);/*24*/}
    {int  ReplaceReturn = numargs; SensorCall(); return ReplaceReturn;}
}

int
fixrargs(char *name, int arg, int prevargs)
{
    SensorCall();int type;
    STR *str;
    int numargs;

    SensorCall();if (!arg)
	{/*25*/{int  ReplaceReturn = prevargs; SensorCall(); return ReplaceReturn;}/*26*/}
    SensorCall();type = ops[arg].ival & 255;
    SensorCall();if (type == OCOMMA) {
	SensorCall();numargs = fixrargs(name,ops[arg+1].ival,prevargs);
	numargs = fixrargs(name,ops[arg+3].ival,numargs);
    }
    else {
	SensorCall();char *tmpbuf = (char *) safemalloc(strlen(name) + (sizeof(prevargs) * 3) + 5);
	sprintf(tmpbuf,"%s:%d",name,prevargs);
	str = hfetch(curarghash,tmpbuf);
	safefree(tmpbuf);
	SensorCall();if (str && strEQ(str->str_ptr,"*")) {
	    SensorCall();if (type == OVAR || type == OSTAR) {
		SensorCall();ops[arg].ival &= ~255;
		ops[arg].ival |= OSTAR;
	    }
	    else
		fatal("Can't pass expression by reference as arg %d of %s\n",
		    prevargs+1, name);
	}
	SensorCall();numargs = prevargs + 1;
    }
    {int  ReplaceReturn = numargs; SensorCall(); return ReplaceReturn;}
}
